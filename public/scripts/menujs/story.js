	var searchFlag = false;
	var updatedLinkId = "";
 	var updatedDocId = "0";

var numberOfTasksLabel = "";
var sprintTaskLabel=""; 
var TeamMembersLabel ="";
var NumberOfHoursLabel = "";
var numberOfTasksLabel = "";
var storyStatusLabel = "";
var sprintGroupsLabels= "";


var loadFrom = 'Agile'; //----------------> used in taskUicode.js file to call the scrollbar initialization function .(in agile using different function to initialize the scrollbar)
	

	/*function loadProjectTabData(mAct){
       window.location.href = path+"/Redirect.do?pAct="+mAct;
	}*/
		 
	
  
	//---------------------------------------------------------------------------// Epic and story load functions //--------------   
	function showEpics(){
		var id = prjid;
      	var projIdeatitle = projtName;
	  	var imgSrc = projImgSrc
	  	var htmlsp = '<span style="font-weight:bold;float:left;cursor:pointer;" class="Agile_cLabelText" onclick="gotoProject();"></span><span style="float:left;">&nbsp;>&nbsp;</span><img onerror=javascript:imageOnProjErrorReplace(this); onclick="loadProjectDescription('+id+')";  id="type_image" style="margin-top:-5px;cursor:pointer;float:left;width:30px;height:30px;border: 2px solid #FFFFFF;box-shadow: 0 0 5px 2px rgba(0, 0, 0, 0.35);border-radius: 2px;" src="" /><span style="float:left;margin-left:5px;cursor:pointer;" onclick=epicList('+id+',"");>'+projIdeatitle+'</span>&nbsp;|&nbsp;<span style="cursor:pointer;" id="tabSpan"></span>';

	  	$('#topicSpan').append(htmlsp);   
	  	$('#type_image').attr('src',imgSrc);
	  	$('#ideaLogoTitle').text(projIdeatitle);
	  	$('#hiddenSortVal').val('Sort');
	  	$('#epicSortDropDown').show();
	  	//loadAgileCustomLabel("onload");
	  	epicList(id,'');
  	} 
 	
 	function epicList(typeId,sortVal){
		var jsonResult = '';
 		var notfSplitEpicIdVal="";
 		vertiScrollBar = false;
 		var localOffsetTime=getTimeOffset(new Date());
		$("#loadingBar").show();
      	timerControl("start");
      	searchFlag = false;
      	var hiddenSortVal = $('#hiddenSortVal').val();
      	$('#tabSpan').text("");
      	if(hiddenSortVal=='Sort'){
	     	$('.epicSearch').val('');
	     	$('input.searchIcon').attr('src', 'images/searchTwo.png').removeClass('Clear_cLabelTitle').addClass('Search_cLabelTitle');
	     	$('select#epicSortDropDown option[value=Sort]').prop('selected','selected');
	  	}
      	$('div.agileTab a').css('color','#A7A7A7');
      	$('div.agileTab img').css('opacity','0.6');
	  	$('div#epicTabDiv').children('a').css('color','#FFFFFF');
	  	$('div#epicTabDiv').children('img').css('opacity','1');
	  
	  	var tabspan = '<span class="STORY_cLabelText" onclick = epicList('+typeId+',"");></span>';
	  	$('#tabSpan').append(tabspan);
	  	loadAgileCustomLabel("onload");
		let jsonbody = {
			"user_id":userIdglb,
			"project_id":typeId,
			"company_id":companyIdglb,
			"sortVal":"",
			"searchTxt":"",
			"localOffsetTime":localOffsetTime
		}  
	  	
	  	$.ajax({
        	url: apiPath+"/"+myk+"/v1/getEpics",
			type:"POST",
			dataType:'json',
            contentType:"application/json",
            data: JSON.stringify(jsonbody),
            //data:{action:"loadEpics",projId:typeId,localOffsetTime:localOffsetTime},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
					$("#loadingBar").hide();
					timerControl("");
			},
			success:function(result){
			    
			    ///alert("loadEpis:..."+result);
			    jsonResult = result;
				result = prepareAgileUI(jsonResult);
				$("div#epicContentMain").html(result);	
				$('#proj_ID').val(typeId);
				$('#projUserStatus').val(projectUsersStatus);
				
				$("div#epicContentMain").on("mouseover","div.actFeedHover",function(){  //----- to hide option popup
					if(!$(this).find('div.hideEpicOptionDiv').is(':visible')){
						$('div.actFeedHover').find('div.hideEpicOptionDiv').hide();
					}
				});
				if($(document).height() > $(window).height()){ 
    				vertiScrollBar = true;
    			}else{
    				vertiScrollBar = false;
    			}
    			checkStatus();
    			loadAgileCustomLabel("Story");
    			initEpicStoryDragDrop();
			//alert('result-----' +result);
			    /*  
			   
			    //$('img#expandColImg').removeAttr('class').addClass('Expand_All_cLabelTitle').attr("src","${path}/images/Idea/expandAll.png");
                initEpicStoryDragDrop();
			    */
				//loadAgileCustomLabel("Story");
				if(agileNotificationType != ""){ 
				  if(agileNotificationType == "Sprint"){
				     //viewTask(epicCmtDocId,'myTasks');
					 newUicreateTaskUI('update',epicCmtDocId,'myTasks','','','','','','SBsprintTask','','SB');
				  }else{
				    if(notAgilePPId =="0" || notAgilePPId ==""){
				       if(notAgilePId == "0"){
				         if(notAgilePPId ==""){}else{
				            if($("#bandDiv_"+agileNotificationId).html() == ""){
				               if(agileNotificationType == "agile_document"){
					              showIdeaDocs('docIcon_'+agileNotificationId,agileNotificationId);
					           }else if(agileNotificationType == "agile_comment"){	 
		  					      showIdeaIconCmts('cmtIcon_'+agileNotificationId,agileNotificationId); 
					           }
				            }
				            loadStoriesForEpic('epicImg_'+agileNotificationId,agileNotificationId);
				         }
				       }else{
				         loadStoriesForEpic('epicImg_'+notAgilePId,notAgilePId);
				     } 
				    }else{
				          var splitEpicIds=notfEpicIds.split(',');
				             loadStoriesForEpic('epicImg_'+splitEpicIds[0],splitEpicIds[0]);
				      }
				     
				  }
				}
			    $("#loadingBar").hide();
			    timerControl("");
			 }
        });  
	}
	
	function prepareAgileUI(result){
		var agileUI = '';
		var pointImgCss = '';
		var collapseExpandSrc = "";
		var epicStatusImage = "";
		var epicStoryStatus = "";
		var userAccessClass = "";
		if(result){
		   
			var json = eval(result);
			for(var i = 0; i < json.length; i++){

				if(json[i].storyCount != 0){
					if(agileaction == "loadEpicStory"){
						collapseExpandSrc="minus.png";
					}else{ 
						collapseExpandSrc="plus.png";
					}
				}
				epicStoryStatus = json[i].epic_status;
				epicStatusImage = epicStoryStatus == "" ? "":( epicStoryStatus == "Backlog" ? "images/idea/Backlog.png" : epicStoryStatus == "In Progress" ? "images/idea/Assigned.png" : epicStoryStatus == "Blocked" ? "images/idea/Blocked.png" : epicStoryStatus == "Hold" ? "images/idea/Hold.png" : epicStoryStatus == "Done" ? "images/idea/Done.png" : "images/idea/cancel.png" );


				agileUI +=	"<div id = \"epicContentDiv_"+json[i].epic_id+"\" class = \"row epicContentMainDiv epicContentDivs\">"
							+"<div id = \"epic_"+json[i].epic_id+"\" type=\""+json[i].epic_type+"\" parent_epic_id=\""+json[i].parent_epic_id+"\" shared=\""+json[i].share_type+"\" class = \"col-sm-12 col-xs-12 defaultPaddingCls epicContentMarginCls \">"
							    +"  <span class=\"hidden_searchName\" style=\"display:none;text-transform: lowercase;\">"+ replaceSpecialCharacter(json[i].epic_title)+""+ replaceSpecialCharacter(json[i].unique_cust_id)+"</span>"
						   		+"<div align = \"center\" id=\"bandDiv_"+json[i].epic_id+"\" class = \"epicContentBandCss drag-handle\">"
						   		if(json[i].storyCount != 0){
						   				agileUI += "<img id = \"epicImg_"+json[i].epic_id+"\" title=\"Expand\" onclick = \"loadStoriesForEpic('epicImg_"+json[i].epic_id+"', "+json[i].epic_id+")\" class = \"expandCollapseImg\" src=\"images/idea/"+collapseExpandSrc+"\">"
		   				   		}
						   		agileUI += "</div>"
		   
							+"<div class = \"col-sm-12 col-xs-12 epicMainDiv defaultCls\">"
								+"<div class = \"col-sm-12 col-xs-12 epicContentIconDiv defaultCls\" style='margin-bottom:0px;'>"
		    						+"<div class = \"col-sm-1 col-xs-1 defaultCls\">"
		          						+"<span class = \"epicIDCls text-uppercase\">Id</span>"
		          						+"<span id = \"customIdSpan_"+json[i].epic_id+"\" title = \""+replaceSpecialCharacter(json[i].unique_cust_id)+"\" onclick=\"AddCustomId("+json[i].epic_id+");\" class=\"col-sm-9 col-xs-9 epicIdDivCls epicCustomIdSpan\">"+replaceSpecialCharacter(json[i].unique_cust_id)+"</span>"
		          						+"<input type=\"text\" class=\"epicCustomIdInput Custom_Id_cLabelTitle col-sm-9 col-xs-9 epicIdDivCls\" id=\"customIdInput_"+json[i].epic_id+"\" maxlength=\"15\"  style=\"display: none;\" onkeydown=\"setCustomId(event,"+json[i].epic_id+");\" value=\"\">"
		       						+"</div>"
		       
		       						+"<div class = \"col-sm-2 col-xs-2 progressContDiv\" style = \"padding-right: 0px;margin-top: 3px;\">"
		          						+"<span><img id = \"epicProgressImg_"+json[i].epic_id+"\" class = \"epicProgressImg\"  style=\""+(epicStoryStatus=='Backlog' ? "display:none;":"")+"\" src = \""+epicStatusImage+"\"></span>"
		          						+"<div id = \"epicStoryStatus_"+json[i].epic_id+"\"  style=\""+(epicStoryStatus=='Backlog' ? "display:none;":"")+"\" class = \"col-sm-9 col-xs-9 epicProgressDiv defaultCls\">"+epicStoryStatus+"</div>"
		       						+"</div>"
		       
				       				+"<div id = \"epicStageDiv_"+json[i].epic_id+"\" class = \"col-sm-2 col-xs-2 epicStatusDiv\" style = \"padding-right: 0px;margin-top: 3px;\">"
				       					
				       					if(json[i].stage_name != ""){
				          					agileUI += "<span style=\"float: left; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 100%;\" title=\""+replaceSpecialCharacter(json[i].stage_name)+"\">Value&nbsp;:&nbsp; "+replaceSpecialCharacter(json[i].stage_name)+"</span>"
				          				}
				       				agileUI +="</div>"
		       
				       				+"<div id = \"storyStatus_"+json[i].epic_id+"\" class = \" col-sm-5 col-xs-10\" style = \"margin-top: 3px;\">"
					          			+"<span class = \"epicStatusDiv\">"+getValues(companyLabels,"Stories")+":</span>"
					          			+"<ul class = \"epicStoriesImgDiv\" style = \"margin-bottom: 0px !important\">"
							            	+"<li><img src = \"images/idea/Backlog.png\" title='' class = \"Backlog_cLabelTitle\"><span class = \"epicStoriesValueDiv backlogSpan\">"+json[i].backlogCount+"</span></li>"
							            	+"<li class = \"storyPipeCls\"></li>"
							            	+"<li><img src = \"images/idea/Assigned.png\" title=''class = \"In_Progress_cLabelTitle\"><span class = \"epicStoriesValueDiv assignedSpan\">"+json[i].assignedCount+"</span></li>"
							            	+"<li class = \"storyPipeCls\"></li>"
							            	+"<li><img src = \"images/idea/Blocked.png\" title='' class = \"Blocked_cLabelTitle\"><span class = \"epicStoriesValueDiv blockedSpan\">"+json[i].blockedCount+"</span></li>"
							            	+"<li class = \"storyPipeCls\"></li>"
							            	+"<li><img src = \"images/idea/Hold.png\" title='' class = \"Hold_cLabelTitle\"><span class = \"epicStoriesValueDiv holdSpan\">"+json[i].holdCount+"</span></li>"
							            	+"<li class = \"storyPipeCls\"></li>"
							            	+"<li><img src = \"images/idea/Done.png\" title='' class = \"Done_cLabelTitle\"><span class = \"epicStoriesValueDiv doneSpan\">"+json[i].doneCount+"</span></li>"
							            	+"<li class = \"storyPipeCls\"></li>"
							            	+"<li><img src = \"images/idea/cancel.png\" title='' class = \"Cancel_cLabelTitle\"><span class = \"epicStoriesValueDiv cancelSpan\">"+json[i].cancelCount+"</span></li>"
					       				+"</ul>"
				       				+"</div>"
				       				+"<div class = \"col-sm-2 col-xs-2 col-md-2 defaultCls\" style = \"padding-right: 0px;float: right;\">"
						           		agileUI += "   <div class=\"hideUpdIcons pointContentDiv\" id='epicPointDiv_"+json[i].epic_id+"' style=\""+( json[i].fpoint!=0 ? "":"display:none;" )+"cursor:pointer;float:right;margin-top:3px;margin-right:5px;font-size:12px;\" >"
										agileUI += "        <img id=\"epicPointImg_"+json[i].epic_id+"\" class='epicShareImgDiv Agile_Points_cLabelTitle' style='cursor: default; height: 15px; width: 15px;float:left;'  src=\"images/idea/points.png\" />"
										agileUI += "        <span id=\"epicPointValSpan_"+json[i].epic_id+"\" title=\""+json[i].fpoint+"\" class='epicStoriesValueDiv "+json[i].share_type+"' style='float: left;margin-left:2px;'  >"+json[i].fpoint+"</span>" 
										agileUI += "  </div>"
						           		 if(json[i].epicShare != 0){ 
						           				agileUI += "<span style=\"float:right;\"><img  id=\"shareIcon_"+json[i].epic_id+"\" class = \"epicShareImgDiv Share_cLabelTitle\"  title=\"\" onclick=\"showCollaborators(this,'Epic',"+json[i].epic_id+");\" src = \"images/idea/share1.png\"></span>"
						           		 }
						           		
						           		agileUI +="   <div class=\"hideUpdIcons\" id='docImgDiv_"+json[i].epic_id+"' style=\"cursor:pointer;float:right;margin-top:3px;\" >"
										
										if(json[i].epicLink != 0){
											agileUI += "       <img id=\"docIcon_"+json[i].epic_id+"\"  class='epicShareImgDiv Documents_cLabelTitle' src=\"images/idea/Document.png\" onclick=\"showIdeaDocs('docIcon_"+json[i].epic_id+"','"+json[i].epic_id+"');\" />"
										}else if(json[i].epicDocs != 0){ 
											agileUI += "       <img id=\"docIcon_"+json[i].epic_id+"\"  class='epicShareImgDiv Documents_cLabelTitle' src=\"images/idea/Document.png\" onclick=\"showIdeaDocs('docIcon_"+json[i].epic_id+"','"+json[i].epic_id+"');\" />"
										}
										agileUI += "  </div>"
						           		
						           		agileUI += "  <div class=\"hideUpdIcons\" id='cmtImgDiv_"+json[i].epic_id+"' style=\"cursor:pointer;float:right;margin-top:3px;margin-right: 4px;\" >"
										if(json[i].epicCmts != 0){  
											agileUI += "       <img id=\"cmtIcon_"+json[i].epic_id+"\"  style='cursor: pointer; float: right;margin-right: 3px;' class='Comments_cLabelTitle' src=\"images/idea/comment1.png\" onclick=\"showIdeaIconCmts('cmtIcon_"+json[i].epic_id+"',"+json[i].epic_id+");\" />"
										}
										agileUI += "  </div>"
					           		    
					           		    
					           		agileUI += "</div>"
					           		
				       				
				       				
					      
									/*
		       						+"<div id = \"epicPoints_"+json[i].epicId+"\" class = \"col-sm-2 col-xs-2 col-md-2 defaultCls\" style = \"padding-right: 0px;margin-top: 3px;float: right;\">"
					           			if(json[i].epicType == "S"){
					           				if(json[i].epic_point != 0){
					           					agileUI += "<span class = \"epicPointDiv\">"+json[i].epic_point+"</span>"
					           							   +"<span><img class = \"epicPointImgDiv\" src = \""+path+"/images/idea/points.png\"></span>"
					           				}
					           			}else{
					           				if(json[i].fPoint != 0){
					           					agileUI += "<span class = \"epicPointDiv\">"+json[i].fPoint+"</span>"
					           							+"<span><img class = \"epicPointImgDiv\" src = \""+path+"/images/idea/points.png\"></span>"
					           				}
					           			}
					           			
					           			
				       				agileUI += "</div>"
				       				
				       				*/
				       				
								agileUI += "</div>"
								
								+"<div id = \"epicTitle_"+json[i].epic_id+"\" class = \"col-sm-12 col-xs-12 epicContentTextareaDiv defaultCls actFeedHover\">"
									//+"<div class = \"epicContent_Div\" style='margin:3px 0px;padding:0px;height:36px;'>"+json[i].title+"</div>" ;
						       		
						       		if(userId==json[i].created_by || projectUsersStatus =='PO') {    
							                 agileUI+="        <textarea class=\"epicContent_Div\" id=\"editIdea_"+json[i].epic_id+"\" readonly style='margin:3px 0px;padding:0px;height:36px;background:none;' place='Epic' onkeyup=\"enterKeyValidateEpic(event,'keyup','editEpic',"+json[i].epic_id+");\" onkeypress=\"enterKeyValidateEpic(event,'keypress','editEpic',"+json[i].epic_id+");\" onClick=\"editEpic(this);\" >"+replaceSpecialCharacter(json[i].epic_title)+"</textarea>";
							          }else{
							            	 agileUI+="        <textarea class=\"epicContent_Div\" id=\"editIdea_"+json[i].epic_id+"\" readonly style='margin:3px 0px;padding:0px;height:36px;background:none;'  place='Epic'  onClick=\"\" onBlur=\"\" >"+replaceSpecialCharacter(json[i].epic_title)+"</textarea>";
								      } 
								    agileUI+="<input id=\"hiddenTitle_"+json[i].epic_id+"\" type=\"hidden\" value=\""+replaceSpecialCharacter(json[i].epic_title)+"\">"
						       		agileUI+="<div id=\"optionDiv_"+json[i].epic_id+"\" class = \"epicMoreImgDiv actFeedHoverImgDiv\" createdById=\""+json[i].created_by+"\"  userAccessClass=\""+userAccessClass+"\" onclick=\"epicOptionsPopup(this);\"><img align = \"center\" class = \"img-responsive\" style=\"cursor:pointer;\" src = \"images/more.png\" ></div>"
						       		+"<div class=\"hideEpicOptionDiv\" id=\"optionsDiv_"+json[i].epic_id+"\" style=\"right: 40px; width: 130px;\"></div> "
								+"</div>"
			
								+"<div id = \"epicDetails_"+json[i].epic_id+"\" class = \"col-sm-12 col-xs-12 epicContentCreatedByDiv defaultCls\">"
									+ "<p class = \"defaultNameDateTimestamp\" style = \"margin: 0px !important\">"+getValues(companyLabels,"Created_by")+" "+json[i].createdBy+" /"+json[i].created_time+"";
									
									if(json[i].updatedBy != "-"){
										agileUI += "&nbsp;&nbsp; " +getValues(companyLabels,"Modified_By")+" "+json[i].updatedBy+" /"+json[i].updated_time+""
									}
									agileUI +=  "</p>"
									agileUI += "</div>"
							+"</div>"
						+"</div>"
						+"<div id=\"comment_"+json[i].epic_id+"\" class=\"epicCommonListDivCss\" style='padding-top:5px;' ></div>"
						+"<div id=\"createTaskListUiDiv_"+json[i].epic_id+"\" class=\"col-sm-12 col-xs-12\" style=\"border-radius: 5px;padding-left:0px; padding-right:0px;margin-top:2px;clear: both;margin-left: 29px;border-radius: 0px 0px 5px 5px;width: 97.5%;\"></div>"
						+"<div id=\"epicListCommonDiv_"+json[i].epic_id+"\" class=\"epicCommonListDivCss\" ></div>"
						+"<div id = \"epicSubContainerDiv_"+json[i].epic_id+"\" class = \"agileSubDivs\" style=\"padding-left: 30px;\">";
						if(json[i].storiesData != ""){
						  agileUI += prepareAgileStoryUI(json[i].storiesData);
						}
						agileUI +="</div>"
				
					+"</div>"
					
					
					$('#epicFeatStageList').val(json[i].epicFeatureStagesList);
					$('#getPlist').val(json[i].prior_list);
					$('#getSlist').val(json[i].status_list);
					
				}
		}
		
		return agileUI;
	}
	
	function loadStoriesForEpic(id, epicId){
	    var jsonResult = '';
		var src = $("#"+id).attr('src');
		var typeOfList = $("#"+id).attr('typeOfList');
		var localOffsetTime=getTimeOffset(new Date());
		if(src.indexOf('plus.png') == '-1'){
			$('div#epicSubContainerDiv_'+epicId).empty();
			$("#"+id).attr('src', "images/idea/plus.png").attr("title","Expand");
			/*$('li#epic_'+epicId).children('button[data-action=expand]').show();
			if(!isiPad){
				$("#epicList").mCustomScrollbar("update");
				//epicListCustomScroll();
			}*/
     	}else{
	    	$("#loadingBar").show();
			timerControl("start");
			var projId = $("#proj_ID").val(); 
			var projUserStatus = $("#projUserStatus").val(); 
			var sortVal = '';
			var searchTxt = '';
			var sortVal = $("select[id=epicSortDropDown] option:selected").val();
	        var searchTxt = $('#searchDiv').find('.epicSearch').val().toLowerCase();
			$("#"+id).attr('src', "images/idea/minus.png").attr("title","Collapse");
			$.ajax({
				url: apiPath+"/"+myk+"/v1/getStories?epicId="+epicId+"&sortVal="+sortVal+"&searchTxt="+searchTxt+"&localoffsetTimeZone="+localOffsetTime+"",
				type:"GET",
				//data:{action:"loadStories", epicId:epicId, projId:projId, projUserStatus:projUserStatus, sortVal:sortVal, searchTxt:searchTxt,localOffsetTime:localOffsetTime },
				error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
				},
				success:function(result){
						agileaction == "loadEpicStory";
					    jsonResult = result;
					    result = prepareAgileStoryUI(jsonResult);
					    var splitEpicIds=notfEpicIds.split(',');
					      
					    $("div#epicSubContainerDiv_"+epicId).append(result);
						loadAgileCustomLabel("Story");
						checkStatus();
						if(sortVal!='Sort' || searchTxt!='' ){
					      searchFlag = true;
					      filterStoryOptionsOnSearch();
				   		}else{
				   		  searchFlag = false;
				   		  initEpicStoryDragDrop();
						}
					    //$('li#epic_'+epicId+' > ol > li').show();
					     if(agileNotificationType != ""){ 
					      var splitEpicIds=notfEpicIds.split(',');
					      if($("#epicContentDiv_"+splitEpicIds[2]).length == 0){
					      if($("#bandDiv_"+splitEpicIds[2]).html() == ""){
					      	 if(agileNotificationType == "agile_document"){
					          showIdeaDocs('docIcon_'+agileNotificationId,agileNotificationId);
					         }else if(agileNotificationType == "agile_comment"){	 
		  					  showIdeaIconCmts('cmtIcon_'+agileNotificationId,agileNotificationId); 
					         }
						  }
					         loadStoriesForEpic('epicImg_'+splitEpicIds[1],splitEpicIds[1]);
					      }else{
					         if(splitEpicIds[0] != splitEpicIds[1]){
						      if($("#bandDiv_"+splitEpicIds[2]).html() == ""){
						      	  if(agileNotificationType == "agile_document"){
						            showIdeaDocs('docIcon_'+agileNotificationId,agileNotificationId);
						          }else if(agileNotificationType == "agile_comment"){	 
			  					     showIdeaIconCmts('cmtIcon_'+agileNotificationId,agileNotificationId); 
						          }
						      }
					         loadStoriesForEpic('epicImg_'+splitEpicIds[2],splitEpicIds[2]);
					         }
					      }
					      if(agileNotificationType == "agile_document"){
					          showIdeaDocs('docIcon_'+agileNotificationId,agileNotificationId);
					       }else if(agileNotificationType == "agile_comment"){	 
		  					  showIdeaIconCmts('cmtIcon_'+agileNotificationId,agileNotificationId); 
					       }
					     }
					   
					    
					    initEpicStoryDragDropForSubLevel(epicId);    
					    
					$("#loadingBar").hide();
					timerControl("");
					}
			});   
     	}
 	}
 	
 	function prepareAgileStoryUI(result,act){
 		var agileUI = '';
		var pointImgCss = '';
		var titled = '';
		var Priority_image = "";
		var priority = "";
		var collapseExpandSrc ="";
		var epicStoryStatus = "";
		var epicStatusImage = "";
		var userAccessClass = "";
		var userAccess = "";
		if(result){
			var json = eval(result);
			for(var i = 0; i < json.length; i++){

				if(json[i].storyCount != 0){
					if(agileaction=="loadEpicStory"){
						collapseExpandSrc="minus.png";
					}else{
						collapseExpandSrc="plus.png";
					}
				}

				//needed
				if(userAccess!="" && userAccess == "R"){
					userAccessClass="readAccess"; 
				}else{
					userAccessClass="";
				}

				epicStoryStatus=json[i].epic_status;
				epicStatusImage =epicStoryStatus == "" ? "":( epicStoryStatus == "Backlog" ? "images/idea/Backlog.png" : epicStoryStatus == "In Progress" ? "images/idea/Assigned.png" : epicStoryStatus == "Blocked" ? "images/idea/Blocked.png" : epicStoryStatus == "Hold" ? "images/idea/Hold.png" : epicStoryStatus == "Done" ? "images/idea/Done.png" : "images/idea/cancel.png" );

				priority = json[i].epic_priority;
				Priority_image = (priority=="")? "" : priority == "1" ? "images/idea/VeryImportant1.png" : priority == "2" ? "images/idea/Important1.png":priority == "3" ? "images/idea/medium1.png" :priority == "4" ? "images/idea/Low1.png": "images/idea/VeryLow1.png";
				titled = Priority_image == "" ? "": Priority_image.indexOf("idea/VeryImportant.png") != -1 ? "Very important" : Priority_image.indexOf("idea/Important.png") != -1 ?"Important": Priority_image.indexOf("idea/medium.png") != -1 ? "Medium": Priority_image.indexOf("idea/Low.png") != -1 ? "Low": Priority_image.indexOf("idea/VeryLow.png") != -1 ? "Very low": Priority_image.indexOf("idea/none.png") != -1? "None" :"";
                
				json[i].sprintName = replaceSpecialCharacter(json[i].sprintName);	    	
				json[i].sprintName = unicodeTonotificationValue(json[i].sprintName);

				      agileUI += "<div id = \"epicContentDiv_"+json[i].epic_id+"\" class = \"epicContentDivs\" style='float:left; width:100%;'>"
				      agileUI +=	"<div id = \"epic_"+json[i].epic_id+"\" type=\""+json[i].epic_type+"\" parent_epic_id=\""+json[i].parent_epic_id+"\" sprintStatus=\""+json[i].storySprintStatus+"\" storyStatus=\""+json[i].epic_status+"\" class = \"col-sm-12 col-xs-12 defaultPaddingCls epicContentMarginCls\" >"
								+"  <span class=\"hidden_searchName\" style=\"display:none;text-transform: lowercase;\">"+replaceSpecialCharacter(json[i].epic_title)+""+replaceSpecialCharacter(json[i].unique_cust_id)+"</span>"
						   		+" <div align = \"center\" id=\"bandDiv_"+json[i].epic_id+"\" class = \""+(json[i].epic_type == "F" ? "epicContentPurpleBandCss" : "epicContentYellowBandCss")+" drag-handle\">"
		   						if(Priority_image != ""){ 
		   							agileUI += "<img class = \"priorityImgCls\" style=\"left:0;position: absolute; margin-left: -8px; margin-top: -4px;\" src = \""+Priority_image+"\" title=\""+titled+"\" onclick=\"showPriorityPopUp(this,"+json[i].epic_id+");ideaPriorityPopup("+json[i].epic_id+");\">"
		   							//<div style="height: 20px; width: 20px; float: left; cursor: pointer; position: absolute; margin-top: -7px; margin-left: -5px;"><img class="mCS_img_loaded" onclick="showPriorityPopUp(this,7349);ideaPriorityPopup(7349);" style="border: 1px solid rgb(255, 255, 255); border-radius: 4px;" src="/collaborationApp/images/Idea/VeryImportant.png"></div>
						   		}
		   						if(json[i].storyCount != 0){
		   							agileUI += "<img id = \"epicImg_"+json[i].epic_id+"\" title=\"Expand\" onclick = \"loadStoriesForEpic('epicImg_"+json[i].epic_id+"', "+json[i].epic_id+")\" class = \"expandCollapseImg\" src=\"images/idea/"+collapseExpandSrc+"\">"
		   				   		}
						   		agileUI += "</div>"
		   						+"<div class = \"col-sm-12 col-xs-12 epicMainDiv defaultCls\">"
		    						+"<div class = \"col-sm-12 col-xs-12 epicContentIconDiv defaultCls\" style='margin-bottom:0px;'>"
		       							+"<div class = \"col-sm-1 col-xs-1 defaultCls\">"
		          							+"<span class = \"epicIDCls text-uppercase\">Id</span>"
		          							+"<span id = \"customIdSpan_"+json[i].epic_id+"\" title = \""+replaceSpecialCharacter(json[i].unique_cust_id)+"\"  onclick=\"AddCustomId("+json[i].epic_id+");\"  class = \"col-sm-9 col-xs-9 epicIdDivCls epicCustomIdSpan\">"+replaceSpecialCharacter(json[i].unique_cust_id)+"</span> "
		          							+"<input type=\"text\" class=\"epicCustomIdInput Custom_Id_cLabelTitle col-sm-9 col-xs-9 epicIdDivCls\" id=\"customIdInput_"+json[i].epic_id+"\" maxlength=\"15\" style=\"display: none;\" onkeydown=\"setCustomId(event,"+json[i].epic_id+");\" value=\"\">"
		       							+"</div>"
		       /*
		       							+"<div  style = \"padding-right: 0px;margin-top: 3px;width:40%;float:left;padding-left:1%;\">"
		          							+"<span><img class = \"epicProgressImg\" src = \""+path+"/images/idea/Done.png\"></span>"
		          							+"<div style=\"width:18%;\" id = \"epicStoryStatus_"+json[i].epicId+"\" class = \"epicProgressDiv defaultCls\">"+json[i].epicStoryStatus+"</div>"
		          							+"<div class = \"storyPipeCls\" style = \"float: left;margin: 0px 5px 0px 5px;\"></div>"
		          							+"<div style=\"width:29%;\" id = \"sprintSpan_"+json[i].epicId+"\" class = \"epicProgressDiv defaultCls defaultNameDateTimestamp\">"+json[i].sprintName+"</div>"
		       							+"</div>"
		       */
		       
		       
		       					 if(json[i].epic_type == "S"){
		       						agileUI +="<div  class = \"progressContDiv\" style = \"padding-right: 0px;margin-top: 3px;width:40%;float:left;padding-left:1%;\">"
		          							    +"<span><img id = \"epicProgressImg_"+json[i].epic_id+"\" class = \"epicProgressImg\" src = \""+epicStatusImage+"\"></span>"
		          							    +"<div style=\"width:18%;\" id = \"epicStoryStatus_"+json[i].epic_id+"\" class = \"epicProgressDiv defaultCls\">"+epicStoryStatus+"</div>"
		          						   if(json[i].sprintName != ""){	
		          							  agileUI +="<div class = \"storyPipeCls\" style = \"float: left;margin: 0px 5px 0px 5px;\"></div>"
		          							          +"<div style=\"width:50%;\" id = \"sprintSpan_"+json[i].epic_id+"\" class = \"epicProgressDiv defaultCls defaultNameDateTimestamp defaultExceedCls\">"+json[i].sprintName+"</div>"
		       							   }
		       							agileUI +="</div>"
		       					}else{
		                     		agileUI +="<div class = \"col-sm-2 col-xs-2 progressContDiv\" style = \"padding-right: 0px;margin-top: 3px;\">"
		          						        +"<span><img id = \"epicProgressImg_"+json[i].epic_id+"\" class = \"epicProgressImg\" style=\""+(epicStoryStatus=='Backlog' ? "display:none;":"")+"\" src = \""+epicStatusImage+"\"></span>"
		          						        +"<div id = \"epicStoryStatus_"+json[i].epic_id+"\" style=\""+(epicStoryStatus=='Backlog' ? "display:none;":"")+"\" class = \"col-sm-9 col-xs-9 epicProgressDiv defaultCls\">"+epicStoryStatus+"</div>"
		       								+"</div>"
		       
				       				        +"<div id = \"epicStageDiv_"+json[i].epic_id+"\" class = \"col-sm-2 col-xs-2 epicStatusDiv\" style = \"padding-right: 0px;margin-top: 3px;\">"
				       						if(json[i].stage_name != ""){
				          					   agileUI += "<span style=\"float: left; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 100%;\" title=\""+replaceSpecialCharacter(json[i].stage_name)+"\">Functionality&nbsp;:&nbsp; "+replaceSpecialCharacter(json[i].stage_name)+"</span>"
				          				     }
				       				        agileUI +="</div>"
				       			   }
		       
		       
		       							//--------
		       							agileUI +="<div class = \"col-sm-2 col-xs-2 col-md-2 defaultCls\" style = \"padding-right: 0px;float: right;\">"
						           		 
						           		 agileUI += "   <div class=\"hideUpdIcons pointContentDiv\" id='epicPointDiv_"+json[i].epic_id+"' " 
										 if(json[i].epic_type == "S"){
										    agileUI += "   style=\""+( json[i].epic_point!=0 ? "":"display:none;" )+"cursor:pointer;float:right;margin-top:3px;margin-right:5px;font-size:12px;\" >"
				           				    agileUI += "        <img id=\"epicPointImg_"+json[i].epic_id+"\" class='epicShareImgDiv Agile_Points_cLabelTitle' style='cursor: default; height: 15px; width: 15px;float:left;'  src=\"images/idea/points.png\" />"
										    agileUI += "        <span id=\"epicPointValSpan_"+json[i].epic_id+"\" title=\""+json[i].epic_point+"\" class='epicStoriesValueDiv epicPointSpan "+userAccessClass+"' style='float: left;margin-left:2px;font-size:10px;' onclick=\"AddPointsToStory("+json[i].epic_id+");\"  >"+json[i].epic_point+"</span>" 
										}else{
				           					agileUI += "   style=\""+( json[i].fpoint!=0 ? "":"display:none;" )+"cursor:pointer;float:right;margin-top:3px;margin-right:5px;font-size:12px;\" >"
				           					agileUI += "        <img id=\"epicPointImg_"+json[i].epic_id+"\" class='epicShareImgDiv Agile_Points_cLabelTitle' style='cursor: default; height: 15px; width: 15px;float:left;'  src=\"images/idea/points.png\" />"
				           					agileUI += "        <span id=\"epicPointValSpan_"+json[i].epic_id+"\" title=\""+json[i].fpoint+"\" class='epicStoriesValueDiv "+userAccessClass+"' style='float:left;;margin-left:2px;'  >"+json[i].fpoint+"</span>" 
										}
						           		 agileUI += "  </div>"
						           		
						           		agileUI +="   <div class=\"hideUpdIcons\" id='docImgDiv_"+json[i].epic_id+"' style=\"cursor:pointer;float:right;margin-top:3px;\" >"
										
										if(json[i].storyLink != 0){
											agileUI += "       <img id=\"docIcon_"+json[i].epic_id+"\"  class='epicShareImgDiv Documents_cLabelTitle' src=\"images/idea/Document.png\" onclick=\"showIdeaDocs('docIcon_"+json[i].epic_id+"','"+json[i].epic_id+"');\" />"
										}else if(json[i].storyDocs != 0){ 
											agileUI += "       <img id=\"docIcon_"+json[i].epic_id+"\"  class='epicShareImgDiv Documents_cLabelTitle' src=\"images/idea/Document.png\" onclick=\"showIdeaDocs('docIcon_"+json[i].epic_id+"','"+json[i].epic_id+"');\" />"
										}
										agileUI += "  </div>"
						           		
						           		agileUI +="   <div class=\"hideUpdIcons\" id='taskImgDiv_"+json[i].epic_id+"' style=\"cursor:pointer;float:right;margin-top:3px;\" >"
										if(json[i].storyTasks != 0){  
											agileUI +="       <img id=\"taskIcon_"+json[i].epic_id+"\" class='epicShareImgDiv Tasks_cLabelTitle' src=\"images/idea/task1.png\"  onclick=\"showIdeaTasks(this,'"+json[i].epic_id+"');\" />"
										}
										agileUI +="   </div>" 
						           		
						           		agileUI += "  <div class=\"hideUpdIcons\" id='cmtImgDiv_"+json[i].epic_id+"' style=\"cursor:pointer;float:right;margin-top:3px;margin-right: 4px;\" >"
										if(json[i].storyCmts != 0){  
											agileUI += "       <img id=\"cmtIcon_"+json[i].epic_id+"\"  style='cursor: pointer; float: right;margin-right: 3px;' class='Comments_cLabelTitle' src=\"images/idea/comment1.png\" onclick=\"showIdeaIconCmts('cmtIcon_"+json[i].epic_id+"',"+json[i].epic_id+");\" />"
										}
										agileUI += "  </div>"
										
										agileUI += "  <div class=\"hideUpdIcons\" id='defectIconDiv_"+json[i].epic_id+"' style=\"cursor:pointer;float:right;margin-top:2px;margin-right: 4px;\" >"
										if(json[i].storyDefects != 0){  
											agileUI += "       <img id=\"defectIcon_"+json[i].epic_id+"\" class='Defect_cLabelTitle' src=\"images/idea/defect.png\" style=\"cursor: pointer; float: right;margin-right: 3px;height:18px;\" onclick=\"showDefects(this,'"+json[i].epic_id+"');\" />"
										}
										agileUI += "  </div>" 
										
						           		agileUI += "</div>"
					           		
				       				//--------
		       							
		       							var place  = (json[i].epic_type == "S") ? "Story":"Feature";
										
		     						agileUI +="</div>"
		     						
		     						+"<div id = \"epicTitle_"+json[i].epic_id+"\"  class = \"col-sm-12 col-xs-12 epicContentTextareaDiv defaultCls actFeedHover\">"
		       							//+"<div class = \"epicContent_Div\" style='margin:3px 0px;padding:0px;height:36px;'>"+json[i].title+"</div>"
		       							if(userIdglb==json[i].created_by || projectUsersStatus =='PO') {    
								                 agileUI+="        <textarea class=\"epicContent_Div\" id=\"editIdea_"+json[i].epic_id+"\" readonly style='margin:3px 0px;padding:0px;height:36px;background:none;' place='"+place+"' onkeyup=\"enterKeyValidateEpic(event,'keyup','editEpic',"+json[i].epic_id+");\" onkeypress=\"enterKeyValidateEpic(event,'keypress','editEpic',"+json[i].epic_id+");\" onClick=\"editEpic(this);\" >"+replaceSpecialCharacter(json[i].epic_title)+"</textarea>";
								          }else{
								            	 agileUI+="        <textarea class=\"epicContent_Div\" id=\"editIdea_"+json[i].epic_id+"\" readonly style='margin:3px 0px;padding:0px;height:36px;background:none;'  place='"+place+"'  onClick=\"\" onBlur=\"\" >"+replaceSpecialCharacter(json[i].epic_title)+"</textarea>";
									      } 
		       							agileUI +="<input id=\"hiddenTitle_"+json[i].epic_id+"\" type=\"hidden\" value=\""+replaceSpecialCharacter(json[i].epic_title)+"\">"
						       		    agileUI +="<div id=\"optionDiv_"+json[i].epic_id+"\" createdById=\""+json[i].created_by+"\"  userAccessClass=\""+userAccessClass+"\" class = \"epicMoreImgDiv actFeedHoverImgDiv\" onclick=\"epicOptionsPopup(this);\"><img align = \"center\" class = \"img-responsive\"  style=\"cursor:pointer;\" src = \"images/more.png\"></div>"
		       							+"<div class=\"hideEpicOptionDiv\" id=\"optionsDiv_"+json[i].epic_id+"\" style=\"right: 40px; width: 130px;\"> </div>"
								    +"</div>"
		     
		       
		     						+"<div id = \"epicDetails_"+json[i].epic_id+"\"  class = \"col-sm-12 col-xs-12 epicContentCreatedByDiv defaultCls\">"
		     							+ "<p class = \"defaultNameDateTimestamp\" style = \"margin: 0px !important\">"+getValues(companyLabels,"Created_by")+" "+json[i].createdBy+" /"+json[i].created_time+"";
										if(json[i].updatedBy != ""){
											agileUI += "&nbsp;&nbsp; "+getValues(companyLabels,"Modified_By")+" "+json[i].updatedBy+" /"+json[i].updated_time+"";
										}
										agileUI +=  "</p>"
									agileUI += "</div>"
		   						+"</div>"
							+"</div>"
							+"<div id=\"comment_"+json[i].epic_id+"\" class=\"epicCommonListDivCss\"  style='padding-top:5px;'></div>"
							+"<div id=\"createTaskListUiDiv_"+json[i].epic_id+"\" class=\"col-sm-12 col-xs-12\" style=\"padding-left:0px; padding-right:0px;margin-top:2px;clear: both;margin-left: 29px;border-radius: 0px 0px 5px 5px;width: 97.5%;\"></div>"
							+"<div id=\"epicListCommonDiv_"+json[i].epic_id+"\" class=\"epicCommonListDivCss\" ></div>"
							+"<div id = \"epicSubContainerDiv_"+json[i].epic_id+"\" class = \"agileSubDivs\" style=\"padding-left: 30px;\">"
							if(json[i].storiesData != ""){
							  agileUI += prepareAgileStoryUI(json[i].storiesData);
							}
							agileUI += "</div>"
							+"<input type = \"hidden\" id = \"storyNameHidden_"+json[i].epic_id+"\" value = \""+json[i].epic_title+"\" >"
							+"<input type = \"hidden\" id = \"sprintIdHidden_"+json[i].epic_id+"\" value = \""+json[i].sprint_id+"\" >"
							+"<input type = \"hidden\" id = \"sprintGrpIdHidden_"+json[i].epic_id+"\" value = \""+json[i].sprint_group_id+"\" >"
							+"<input type = \"hidden\" id = \"sprintStartDateHidden_"+json[i].epic_id+"\" value = \""+json[i].sprint_start_date+"\" >"
							+"<input type = \"hidden\" id = \"sprintEndDateHidden_"+json[i].epic_id+"\" value = \""+json[i].sprint_end_date+"\" >"
						+"</div>"
					}
			}
		return agileUI;
 	}
 
 //----------------------------------------------------> Options functions //------------------------------------>
 	
 function epicOptionsPopup(obj){
     var idd=$(obj).attr('id').split('_')[1];
	  global_id=idd;
	  
	  //$('div.roundDiv').css('border','none');
	  if ($('#optionsDiv_'+idd).is(":hidden")) {
	        var userAccessClass = $(obj).attr("userAccessClass");
	        var type = $('#epic_'+idd).attr('type');
	        var optionList ="";
	        var optionList= " <div style=\"margin-top: 10px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\"><img src=\""+path+"/images/arrow.png\"></div>";
		        optionList+= "    <div id=\"statusDiv_"+idd+"\" class=\"status-left\"></div>";
			 
	    if(type=='S'){
	        var sprintStatus = $('#epic_'+idd).attr("sprintStatus");
	        var storyStatus = $('#epic_'+idd).attr("storyStatus");
	        
	         optionList+= "    <div id=\"priorityListDiv_"+idd+"\" class=\"status-left\" style=\"margin-top:35px;margin-left:-132px;width:125px;\"></div>";
			 optionList+= "    <div id=\"documentOptionDiv_"+idd+"\"  class=\"status-left\" style=\"width: 165px; margin-left: -172px;margin-top: 155px;\"></div>";
			 optionList+= "    <div id=\"defectOptionDiv_"+idd+"\" class=\"status-left\"></div>";
			 optionList+= "    <div  id=\"optionPopContainer_"+idd+"\" style=\"height: 100%; overflow: hidden;\">";
			 if($(obj).attr("createdById")== userId){ 
				optionList+= "         <div style=\"width:100%;height:20px;margin-bottom:3px;cursor:pointer;display:none;\"> ";
				optionList+= "           <span style=\"float: left;font-size: 14px; height: 20px;margin-left: 5px; overflow: hidden;width: 180px;\" class='Story_Menu_cLabelText'></span></div>";
				optionList+= "         <div id=\"createSubIdea_"+idd+"\" class=\"optionMenuCss \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=\"showCreateStoryDiv("+idd+",'S');\">";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Add_Story_cLabelText\"></span></div>";
				optionList+= "         <div id=\"reorder_"+idd+"\" class=\"optionMenuCss optionMenuOpacity iReorder \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=\"showIdeaReorder('"+idd+"');\">";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Reorder_cLabelText\"></span></div>";
				optionList+= "         <div id=\"completion_"+idd+"\" class=\"optionMenuCss optionMenuOpacity  "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','Y');showDefectOptionDiv('"+idd+"','N');\" >";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Status_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> "; 
				optionList+= "         <div id=\"priority_"+idd+"\" class=\"optionMenuCss optionMenuOpacity \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','Y');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=''>";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Priority_cLabelText\" ></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div>";
				optionList+= "         <div id=\"project_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity storyPointClass \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick='AddPointsToStory("+idd+");'>";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Points_cLabelText\"></span></div>";
				optionList+= "         <div id=\"commentOption_"+idd+"\" class=\"optionMenuCss \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=\"showEpicComments('"+idd+"');\">";
				optionList+= "           <span   class=\"optionSpanHover agileSpanCss Agile_Comments_cLabelText\"></span></div>";
				//console.log('sprintStatus :'+sprintStatus+' userAccessClass:'+userAccessClass);
				if(sprintStatus!="" && sprintStatus=="Expired"){
				optionList+= "         <div id=\"ideaTask_"+idd+"\" class=\"optionMenuCss optionMenuOpacity1\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" >";
				optionList+= "           <span  class=\"optionSpanHoverFade agileSpanCss Agile_Task_cLabelText\"></span></div>";
				}else if(storyStatus=="Done" || storyStatus=="Cancel"){
				optionList+= "         <div id=\"ideaTask_"+idd+"\" class=\"optionMenuCss optionMenuOpacity1\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" >";
				optionList+= "            <span  class=\"optionSpanHoverFade agileSpanCss Agile_Task_cLabelText\"></span></div>";
				}else{
				optionList+= "         <div id=\"ideaTask_"+idd+"\" class=\"optionMenuCss optionMenuOpacity "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=\"newUicreateTaskUI('','0','Sprint',"+idd+",'','','','','sprintTask','','Epic');\" >";		//createTaskUI('', 'Sprint', '"+idd+"', 'Epic');
				optionList+= "            <span  class=\"optionSpanHover agileSpanCss Agile_Task_cLabelText\"></span></div>";
				}
				optionList+= "         <div id=\"ideaDoc_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','Y');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" >";
				optionList+= "            <span  class=\"optionSpanHover agileSpanCss Agile_Documents_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
				if(logbug_integrated=="Y"){
				optionList+= "         <div id=\"defect_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity logbug_"+logbug_integrated+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','Y');\" onclick=''>";
				optionList+= "            <span  class=\"optionSpanHover agileSpanCss Defect_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
				}else{
				optionList+= "         <div id=\"defect_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity1 logbug_"+logbug_integrated+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=''>";
				optionList+= "            <span  class=\"optionSpanHoverFade agileSpanCss Defect_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
				}
				optionList+= "         <div id=\"ideaDelete_"+idd+"\" class=\"optionMenuCss optionMenuOpacity \"   style=\"cursor: pointer; border-bottom: none; height: auto; padding: 4px 0 6px;\"  onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick='deleteEpicStory("+idd+");'>";
				optionList+= "            <span  class=\"optionSpanHover agileSpanCss Agile_Delete_cLabelText\"></span></div>";
						                  
			 }else{
				optionList+= "        <div style=\"width:100%;height:20px;margin-bottom:3px;cursor:pointer;display:none;\"> ";
				optionList+= "            <span style=\"margin-left:5px;font-size:14px;\">Story Menu</span></div>";
				optionList+= "        <div id=\"createSubIdea_"+idd+"\" class=\"optionMenuCss \"   style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=\"showCreateStoryDiv("+idd+",'S');\">";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Add_Story_cLabelText\"></span></div>";
				optionList+= "        <div id=\"reorder_"+idd+"\" class=\"optionMenuCss optionMenuOpacity iReorder "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=\"showIdeaReorder('"+idd+"');\">";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss  Agile_Reorder_cLabelText\"></span></div>";
				optionList+= "        <div id=\"completion_"+idd+"\" class=\"optionMenuCss optionMenuOpacity "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','Y');showDefectOptionDiv('"+idd+"','N');\"  onclick=\"ideaCompletion('"+idd+"','Y');\">";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Status_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\"/></div> "; 
				optionList+= "        <div id=\"priority_"+idd+"\" class=\"optionMenuCss optionMenuOpacity "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','Y');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=''>";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Priority_cLabelText\" ></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div>";
				optionList+= "        <div id=\"project_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity storyPointClass "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick='AddPointsToStory("+idd+");'>";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Points_cLabelText\"></span></div>";
				optionList+= "        <div id=\"commentOption_"+idd+"\" class=\"optionMenuCss \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=\"showEpicComments('"+idd+"');\">";
				optionList+= "           <span   class=\"optionSpanHover agileSpanCss Agile_Comments_cLabelText\"></span></div>";
				if(sprintStatus!="" && sprintStatus=="Expired"){
				optionList+= "        <div id=\"ideaTask_"+idd+"\" class=\"optionMenuCss optionMenuOpacity1\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick='' >";
				optionList+= "           <span  class=\"optionSpanHoverFade agileSpanCss Agile_Task_cLabelText\"></span></div>";
				}else if(storyStatus =="Done" || storyStatus=="Cancel"){
				optionList+= "        <div id=\"ideaTask_"+idd+"\" class=\"optionMenuCss optionMenuOpacity1\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick='' >";
				optionList+= "           <span  class=\"optionSpanHoverFade agileSpanCss Agile_Task_cLabelText\"></span></div>";
				}else{
				optionList+= "        <div id=\"ideaTask_"+idd+"\" class=\"optionMenuCss optionMenuOpacity "+userAccessClass+"\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\"  onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=\"newUicreateTaskUI('','0','Sprint',"+idd+",'','','','','sprintTask','','Epic');\" >"; 		//createTaskUI('' ,'Sprint', '"+idd+"', 'Epic');
				optionList+= "            <span  class=\"optionSpanHover agileSpanCss Agile_Task_cLabelText\"></span></div>";
				}
				optionList+= "        <div id=\"ideaDoc_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','Y');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=''>";
				optionList+= "            <span  class=\"optionSpanHover agileSpanCss Agile_Documents_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
				if(logbug_integrated=="Y"){
				optionList+= "        <div id=\"defect_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity logbug_"+logbug_integrated+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','Y');\" onclick=''>";
				optionList+= "            <span  class=\"optionSpanHover agileSpanCss Defect_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
				}else{
				optionList+= "        <div id=\"defect_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity1 logbug_"+logbug_integrated+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=''>";
				optionList+= "            <span  class=\"optionSpanHoverFade agileSpanCss Defect_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
				}
				optionList+= "        <div id=\"ideaDelete_"+idd+"\" class=\"optionMenuCss optionMenuOpacity "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: none; height: auto; padding: 4px 0 6px;\" onmouseover=\"showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick='deleteEpicStory("+idd+");'>";
				optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Delete_cLabelText\"></span></div>";
			 }    
			optionList+= "    </div>";
	   }else if(type=='E'){
	       optionList+= "    <div id=\"valueDiv_"+idd+"\" class=\"status-left\" style='margin-top:60px;'></div>";
           optionList+= "    <div id=\"documentOptionDiv_"+idd+"\"  class=\"status-left\" style=\"width: 165px; margin-left: -172px;margin-top: 105px;\"></div>";
           optionList+= "    <div  id=\"optionPopContainer_"+idd+"\" style=\"height: 100%; overflow: hidden;\">"; 
         	optionList+= "           <div style=\"width:100%;height:20px;margin-bottom:3px;display:none;\"> ";
           optionList+= "               <span style=\"float: left;font-size: 14px; height: 20px;overflow: hidden;\" class='Epic_Menu_cLabelText'></span></div>";
           if($(obj).attr("createdById")==userId){
               optionList+= "        <div id=\"addFeature_"+idd+"\" class=\"optionMenuCss\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showCreateStoryDiv("+idd+",'F');\">";
               optionList+= "          <span class=\"optionSpanHover agileSpanCss Add_Feature_cLabelText\"></span></div>";
               optionList+= "        <div id=\"createSubIdea_"+idd+"\" class=\"optionMenuCss\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showCreateStoryDiv("+idd+",'S');\">";
               optionList+= "          <span class=\"optionSpanHover agileSpanCss Add_Story_cLabelText\"></span></div>";
               optionList+= "        <div id=\"reorder_"+idd+"\" class=\"optionMenuCss optionMenuOpacity iReorder \" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\"   onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showIdeaReorder('"+idd+"');\">";
               optionList+= "          <span class=\"optionSpanHover agileSpanCss Agile_Reorder_cLabelText\"></span></div>";
               optionList+= "        <div id=\"completion_"+idd+"\" class=\"optionMenuCss optionMenuOpacity  "+userAccessClass+"\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','Y');\"  onclick=\"ideaCompletion('"+idd+"','Y');\">";
				optionList+= "         <span  class=\"optionSpanHover agileSpanCss Agile_Status_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> "; 
				optionList+= "       <div id=\"commentOption_"+idd+"\" class=\"optionMenuCss \" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onclick=\"showEpicComments('"+idd+"');\">";
               optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Comments_cLabelText\"></span></div>";
               optionList+= "        <div id=\"ideaVal_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity "+userAccessClass+"\" onmouseover=\"showEpicStageDiv('"+idd+"','Y');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  >";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Value_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
               optionList+= "        <div id=\"ideaDoc_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','Y');showEpicStatusDiv('"+idd+"','N');\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  >";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Documents_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
               optionList+= "        <div id=\"ideaShare_"+idd+"\" class=\"optionMenuCss optionMenuOpacity "+userAccessClass+"\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onclick='shareIdea(this);' >";
               optionList+= "          <span class=\"optionSpanHover agileSpanCss Agile_Collaborators_cLabelText\"></span></div>";
               optionList+= "        <div id=\"ideaDelete_"+idd+"\" class=\"optionMenuCss optionMenuOpacity defaultEpic_\" style=\"cursor: pointer;height: auto; padding: 4px 0 6px;border-bottom:none;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick='deleteEpicStory("+idd+");'>";
               optionList+= "          <span class=\"optionSpanHover agileSpanCss Agile_Delete_cLabelText\"></span></div>";
           }else{
               optionList+= "        <div id=\"addFeature_"+idd+"\" class=\"optionMenuCss\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showCreateStoryDiv("+idd+",'F');\">";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Add_Feature_cLabelText\">Add Feature</span></div>";
               optionList+= "        <div id=\"createSubIdea_"+idd+"\" class=\"optionMenuCss\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showCreateStoryDiv("+idd+",'S');\">";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Add_Story_cLabelText\"></span></div>";
               optionList+= "        <div id=\"reorder_"+idd+"\" class=\"optionMenuCss optionMenuOpacity iReorder "+userAccessClass+"\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showIdeaReorder('"+idd+"');\">";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Reorder_cLabelText\"></span></div>";
               optionList+= "        <div id=\"completion_"+idd+"\" class=\"optionMenuCss optionMenuOpacity  "+userAccessClass+"\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','Y');\"  onclick=\"ideaCompletion('"+idd+"','Y');\">";
			   optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Status_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> "; 
			   optionList+= "        <div id=\"commentOption_"+idd+"\" class=\"optionMenuCss \" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showEpicComments('"+idd+"');\">";
               optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Comments_cLabelText\"></span></div>";
               
               optionList+= "        <div id=\"ideaVal_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity "+userAccessClass+"\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','Y');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=''>";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_EpicValue_cLabelText\">Value</span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
               optionList+= "        <div id=\"ideaDoc_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity \" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','Y');showEpicStatusDiv('"+idd+"','N');\" onclick=''>";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Documents_cLabelText\"></span></div> ";
               optionList+= "        <div id=\"ideaShare_"+idd+"\" class=\"optionMenuCss optionMenuOpacity "+userAccessClass+"\" style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick='shareIdea(this);' >";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Collaborators_cLabelText\"></span></div>";
               optionList+= "        <div id=\"ideaDelete_"+idd+"\" class=\"optionMenuCss optionMenuOpacity "+userAccessClass+" defaultEpic_\" style=\"cursor: pointer; border-bottom: none; height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick='deleteEpicStory("+idd+");'>";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Delete_cLabelText\"></span></div>";
           }   
           optionList+= "        </div>";  
	    }else{
	        optionList+= "    <div id=\"priorityListDiv_"+idd+"\" class=\"status-left\" style=\"margin-top:40px;margin-left:-132px;width:125px;\"></div>";
			optionList+= "    <div id=\"documentOptionDiv_"+idd+"\"  class=\"status-left\" style=\"width: 165px; margin-left: -172px;margin-top: 105px;\"></div>";
            optionList+= "    <div id=\"valueDiv_"+idd+"\" class=\"status-left\" style='margin-top:60px;'></div>";
            optionList+= "    <div  id=\"optionPopContainer_"+idd+"\" style=\"height: 100%; overflow: hidden;\">"; 
         	optionList+= "           <div style=\"width:100%;height:20px;margin-bottom:3px;cursor:pointer;display:none;\"> ";
            optionList+= "               <span style=\"float: left;font-size: 14px; height: 20px;margin-left: 5px; overflow: hidden;width: 180px;\" class='Feature_Menu_cLabelText'>Feature Menu</span></div>";
           if($(obj).attr("createdById")=='${userId}'){
               optionList+= "        <div id=\"createSubIdea_"+idd+"\" class=\"optionMenuCss\"   style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showCreateStoryDiv("+idd+",'S');\">";
               optionList+= "          <span class=\"optionSpanHover agileSpanCss Add_Story_cLabelText\"></span></div>";
               optionList+= "        <div id=\"reorder_"+idd+"\" class=\"optionMenuCss optionMenuOpacity iReorder \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showIdeaReorder('"+idd+"');\">";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Reorder_cLabelText\"></span></div>";
               optionList+= "        <div id=\"completion_"+idd+"\" class=\"optionMenuCss optionMenuOpacity  "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','Y');showDefectOptionDiv('"+idd+"','N');\"  onclick=\"ideaCompletion('"+idd+"','Y');\">";
			   optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Status_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> "; 
			   optionList+= "         <div id=\"priority_"+idd+"\" class=\"optionMenuCss optionMenuOpacity \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','Y');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=''>";
			   optionList+= "            <span  class=\"optionSpanHover agileSpanCss Agile_Priority_cLabelText\" ></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div>";
			   optionList+= "         <div id=\"function_"+idd+"\" class=\"optionMenuCss optionMenuOpacity \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','Y');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=''>";
			   optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Function_cLabelText\" >Functionality</span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\"  /></div>";
			   optionList+= "        <div id=\"commentOption_"+idd+"\" class=\"optionMenuCss \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showEpicComments('"+idd+"');\">";
               optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Comments_cLabelText\"></span></div>";
               optionList+= "        <div id=\"ideaDoc_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','Y');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=''>";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Documents_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\"  /></div> ";
               optionList+= "        <div id=\"ideaDelete_"+idd+"\" class=\"optionMenuCss optionMenuOpacity defaultEpic_\"  style=\"cursor: pointer; border-bottom: none;height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick='deleteEpicStory("+idd+");'>";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Delete_cLabelText\"></span></div>";
           }else{
               optionList+= "        <div id=\"createSubIdea_"+idd+"\" class=\"optionMenuCss\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showCreateStoryDiv("+idd+",'S');\">";
               optionList+= "            <span class=\"optionSpanHover agileSpanCss Add_Story_cLabelText\"></span></div>";
               optionList+= "        <div id=\"reorder_"+idd+"\" class=\"optionMenuCss optionMenuOpacity iReorder "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\"  onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showIdeaReorder('"+idd+"');\">";
               optionList+= "            <span class=\"optionSpanHover agileSpanCss Agile_Reorder_cLabelText\"></span></div>";
               optionList+= "        <div id=\"completion_"+idd+"\" class=\"optionMenuCss optionMenuOpacity  "+userAccessClass+"\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','Y');showDefectOptionDiv('"+idd+"','N');\"  onclick=\"ideaCompletion('"+idd+"','Y');\">";
			   optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Status_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\"  /></div> "; 
			   optionList+= "         <div id=\"priority_"+idd+"\" class=\"optionMenuCss optionMenuOpacity \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','Y');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=''>";
			   optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Priority_cLabelText\" ></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\"  /></div>";
			   optionList+= "         <div id=\"function_"+idd+"\" class=\"optionMenuCss optionMenuOpacity \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','Y');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');showDefectOptionDiv('"+idd+"','N');\" onclick=''>";
			   optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Function_cLabelText\" ></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\"  /></div>";
			   optionList+= "        <div id=\"commentOption_"+idd+"\" class=\"optionMenuCss \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204);height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=\"showEpicComments('"+idd+"');\">";
               optionList+= "           <span  class=\"optionSpanHover agileSpanCss Agile_Comments_cLabelText\"></span></div>";
               optionList+= "        <div id=\"ideaDoc_"+idd+"\"  class=\"optionMenuCss optionMenuOpacity \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','Y');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick=''>";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Documents_cLabelText\"></span><img style=\"float: right; transform: rotate(-90deg); margin-top: 7px;\" src=\""+path+"/images/downarrow.png\" /></div> ";
               optionList+= "        <div id=\"ideaDelete_"+idd+"\" class=\"optionMenuCss optionMenuOpacity "+userAccessClass+" defaultEpic_\"  style=\"cursor: pointer; border-bottom: none; height: auto; padding: 4px 0 6px;\" onmouseover=\"showEpicStageDiv('"+idd+"','N');showDocOptionDiv('"+idd+"','N');showPriorityDiv('"+idd+"','N');showEpicStatusDiv('"+idd+"','N');\" onclick='deleteEpicStory("+idd+");'>";
               optionList+= "           <span class=\"optionSpanHover agileSpanCss Agile_Delete_cLabelText\"></span></div>";
           }   
           optionList+= "        </div>";  
	     }
	        $('#optionsDiv_'+idd).html(optionList);
	        loadAgileCustomLabel("Story");
	        loadAgileCustomLabel("onload");
	        $('.hideEpicOptionDiv').hide();
	        // showOptionPop(obj);                              // ---------------------> to align the option popup. 
	        filterStoryOptionsOnSearch();                 //--------------------- to check search mode is active or not. If it is active then disabling some options.
			$('#optionsDiv_'+idd).slideToggle("slow", function(){
			     $(this).css('overflow','');
			});
			checkStatus();
				
	} else {
	       $('#optionsDiv_'+idd).slideToggle("slow");
  }
}

 function showEpicStatusDiv(epic_id,act){
    if (act=='Y') {
              var epicType = $('#epic_'+epic_id).attr('type');
          	  var sprintStatus = $('#epic_'+epic_id).attr("sprintStatus");
	          var storyStatus = $('#epicStoryStatus_'+epic_id).text();
       if($('#statusDiv_'+epic_id).children().length <1){
               var statusListArray = $('#getSlist').val().split(',');
			   var storyLi="  <div class=\"optionMenuCss\"   style=\"display:none;\" ></div>";
			   for(var i=0;i<statusListArray.length-1;i++){    
			       var sCode = statusListArray[i].split('-')[0];
			       var sName = statusListArray[i].split('-')[1]; 
			       var sImg = (sName=="")? "" : sName=="Backlog" ? path+"/images/idea/Backlog.png" : sName=="Blocked" ? path+"/images/idea/Blocked.png":sName=="Hold" ? path+"/images/idea/Hold.png" :sName=="Done" ? path+"/images/idea/Done.png": path+"/images/idea/cancel.png";
				   
				   storyLi+="                  <div class=\"optionMenuCss optionMenuOpacity\"  style=\"height:auto;padding:4px 0px 6px;\" onclick=\"updateEpicStatus(this,'"+sName+"',"+epic_id+");\">";
				   storyLi+="                     <span  class=\"optionSpanHover agileSpanCss1\">"+sName+"</span></div>";
				   if(i==0){
				        sCode = statusListArray[statusListArray.length -1].split('-')[0];
	        	        sName = statusListArray[statusListArray.length -1].split('-')[1];
		        		storyLi+="       <div class=\"optionMenuCss optionMenuOpacity Assigned\"   style=\"display:none;height:auto;padding:4px 0px 6px;\"  onclick=\"updateEpicStatus(this,'"+sName+"',"+epic_id+");\">";
		        		storyLi+="           <span class=\"optionSpanHover agileSpanCss1\">"+sName+"</span></div>";
				 }
		     }
		    
		     $('#statusDiv_'+epic_id).html(storyLi);
		     $('#statusDiv_'+epic_id).children('div:last').css('border','none');
		}			
		if(epicType!='S'){
	       $('#statusDiv_'+epic_id).children('div:eq(1)').hide();
	       $('#statusDiv_'+epic_id).children('div.Assigned').show();
	     }else{
	        //console.log(storyStatus+"--"+sprintStatus);
		     if(storyStatus!="Backlog" && storyStatus!="In Progress"){
	        	  if(sprintStatus!="" && sprintStatus=="Progress"){
	        	     $('#statusDiv_'+epic_id).children('div.Assigned').show();
		          }
		     }
	     }	  
    	$('#statusDiv_'+epic_id).show();
        //if($("#optionPopContainer_"+epic_id).hasClass("mCustomScrollbar")){
        //   var mainDivH=$('#optionsDiv_'+epic_id).height();
        //   var statusDivH=$('#statusDiv_'+epic_id).height();
	    //   $('#statusDiv_'+epic_id).css('margin-top',mainDivH-statusDivH-5+'px');
	    //}
 	}else{
 	  if (!$('#statusDiv_'+epic_id).is(':hidden')) {
 		 $('#statusDiv_'+epic_id).hide();
 	  }
 	}
 }
 
 function showEpicStageDiv(epic_id,act){
    if (act=='Y') {
       if($('#valueDiv_'+epic_id).children().length <1){
               var epicType = $('#epic_'+epic_id).attr('type');
               var statusListArray = $('#epicFeatStageList').val().split(',');
               var lastHtml = '';
               var customHtml=''; 
			   //var storyLi="  <div class=\"optionMenuCss\"></div>";
			   var storyLi="   <div class=\"optionMenuCss optionMenuOpacity\" style=\"height:auto;padding:4px 0px 6px;\" stageId='0' stageType='Default' stageOrder='' onclick=\"updateEpicStage(this,"+epic_id+");\">";
			   storyLi+="       <span  class=\"optionSpanHover agileSpanCss1\">Backlog</span></div>";
			   if(epicType=="E"){
				   for(var i=0;i<=statusListArray.length-1;i++){    
				       var stageList = statusListArray[i].split(':');
				       if(stageList[4] =='E' ){
					        var sName = stageList[2]; 
					        sName = sName.replaceAll("CHR(39)","'");
					        if(stageList[1]=="Default" && parseInt(stageList[3])==2 ){
					           lastHtml+="     <div class=\"optionMenuCss optionMenuOpacity\" style=\"height:auto;padding:4px 0px 6px;\"  stageId="+stageList[0]+" stageType='"+stageList[1]+"' stageOrder="+stageList[3]+" onclick=\"updateEpicStage(this,"+epic_id+");\">";
						       lastHtml+="       <span  class=\"optionSpanHover agileSpanCss1\"  style=\"width: 130px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;\" >"+sName+"</span></div>";
						    }else{
					          storyLi+="     <div class=\"optionMenuCss optionMenuOpacity\" style=\"height:auto;padding:4px 0px 6px;\" stageId="+stageList[0]+" stageType='"+stageList[1]+"' stageOrder="+stageList[3]+" onclick=\"updateEpicStage(this,"+epic_id+");\">";
						      storyLi+="       <span  class=\"optionSpanHover agileSpanCss1\"  style=\"width: 130px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;\" >"+sName+"</span></div>";
						    }
					   } 
					}
			       storyLi+= lastHtml ;
		       }else{
			       for(var i=0;i<=statusListArray.length-1;i++){    
				       var stageList = statusListArray[i].split(':');
				       if(stageList[4] =='F' ){
					        var sName = stageList[2]; 
					        if(stageList[1]=="Default" && parseInt(stageList[3])==2 ){
					           lastHtml+="     <div class=\"optionMenuCss optionMenuOpacity\" style=\"height:auto;padding:4px 0px 6px;\"  stageId="+stageList[0]+" stageType='"+stageList[1]+"' stageOrder="+stageList[3]+" onclick=\"updateEpicStage(this,"+epic_id+");\">";
						       lastHtml+="       <span  class=\"optionSpanHover agileSpanCss1\" style=\"width: 130px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;\" >"+sName+"</span></div>";
						    }else if(stageList[1]=="Default"){
						       storyLi+="     <div class=\"optionMenuCss optionMenuOpacity\" style=\"height:auto;padding:4px 0px 6px;\"   stageId="+stageList[0]+" stageType='"+stageList[1]+"' stageOrder="+stageList[3]+" onclick=\"updateEpicStage(this,"+epic_id+");\">";
						       storyLi+="       <span  class=\"optionSpanHover agileSpanCss1\"  style=\"width: 130px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;\" >"+sName+"</span></div>";
						    }else{
					          customHtml+="     <div class=\"optionMenuCss optionMenuOpacity\" style=\"height:auto;padding:4px 0px 6px;\"   stageId="+stageList[0]+" stageType='"+stageList[1]+"' stageOrder="+stageList[3]+" onclick=\"updateEpicStage(this,"+epic_id+");\">";
						      customHtml+="       <span  class=\"optionSpanHover agileSpanCss1\"  style=\"width: 130px; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;\" >"+sName+"</span></div>";
						    }
					   } 
					}
			       storyLi= storyLi+customHtml+lastHtml ;
		       }
		       $('#valueDiv_'+epic_id).html(storyLi);
		       $('#valueDiv_'+epic_id).children('div:last').css('border','none');
		}					  
    	$('#valueDiv_'+epic_id).show();
        //if($("#optionPopContainer_"+epic_id).hasClass("mCustomScrollbar")){
        //   var mainDivH=$('#optionsDiv_'+epic_id).height();
        //   var statusDivH=$('#valueDiv_'+epic_id).height();
	    //  $('#valueDiv_'+epic_id).css('margin-top',mainDivH-statusDivH-5+'px');
	    //}
 	}else{
 	  if (!$('#valueDiv_'+epic_id).is(':hidden')) {
 		 $('#valueDiv_'+epic_id).hide();
 	  }
 	}
 }  
 
 function showPriorityDiv(epic_id,act){
    if (act=='Y') {
       if($('#priorityListDiv_'+epic_id).children().length <1){
          var priorListArray = $('#getPlist').val().split(',');
		  var storyLi="";  
		     for(var i=0;i<priorListArray.length;i++){    
		       var pCode = priorListArray[i].split('-')[0];
		       var pName = priorListArray[i].split('-')[1]; 
		       var pImg = (pCode=="")? "" : pCode=="1" ? path+"/images/idea/VeryImportant.png" : pCode=="2" ? path+"/images/idea/Important.png":pCode=="3" ? path+"/images/idea/medium.png" :pCode=="4" ? path+"/images/idea/Low.png": path+"/images/idea/VeryLow.png";
			   storyLi+="                <div class=\"optionMenuCss optionMenuOpacity\" style=\"height:auto;padding:4px 0px 6px;\"   onclick=\"addIdeaPriority1(this,"+pCode+","+epic_id+");\">";    
			   storyLi+="                  <img style=\"float:left;margin-top:2px;display:none;\" src=\""+pImg+"\" class=\"imgCss\"/><span style=\"float:left;\" class=\"optionSpanHover agileSpanCss1\">"+pName+"</span></div>";    
			 }
		 	 $('#priorityListDiv_'+epic_id).html(storyLi);  
		}
        $('#priorityListDiv_'+epic_id).show();
        $('#priorityListDiv_'+epic_id).children('div:last').css('border','none');
        //if($("#optionPopContainer_"+epic_id).hasClass("mCustomScrollbar")){
        //   var mainDivH=$('#optionsDiv_'+epic_id).height();
        //   var priorDivH=$('#priorityListDiv_'+epic_id).height();
	    //   $('#priorityListDiv_'+epic_id).css('margin-top',mainDivH-priorDivH-5+'px');
	    //}
   }else{
 	  if (!$('#priorityListDiv_'+epic_id).is(':hidden')) {
 		 $('#priorityListDiv_'+epic_id).hide();
 	  }
   }
 } 
 function showDocOptionDiv(epic_id,act){
    if (act=='Y') {
        if($('#documentOptionDiv_'+epic_id).children().length <1){
        
      		  var storyLi="";
			  storyLi+="         	  <div class=\"optionMenuCss optionMenuUploadOpacity\"  onclick=\"\">";
			  storyLi+="				<form id=\"docUploadForm_"+epic_id+"\" name=\"docUploadForm_"+epic_id+"\" method=\"post\" enctype=\"multipart/form-data\" action=\""+path+"/AgileRepositoryFileUpload\">";
			  storyLi+="					<label class=\"topicImageUpload\" style=\"float: left;height: 20px;margin-top: 0;width: 195px;position:absolute;\">";
			  storyLi+="                       <input type=\"hidden\" value=\"\" id=\"detectBrowser_"+epic_id+"\" name=\"detectBrowser\">";
			  storyLi+="                       <input type=\"hidden\" value=\"\" id=\"size_"+epic_id+"\" name=\"size\">";
			  storyLi+="						<input type=\"hidden\" id=\"\" name=\"uploadEpicId\" value=\""+epic_id+"\"/>";
			  storyLi+="						<input type=\"hidden\" id=\"\" name=\"viewPlace\" value=\"Agile\" />";
			  storyLi+="						<input type=\"hidden\" id=\"\" name=\"type_upload\" value=\"Project\"/>";
			  storyLi+="						<input type=\"file\" id=\"\" name=\"filename\" value=\"\" class=\"topic_file\" style=\"left:0px;top:0px;\" onchange=\"submitAgileRepositoryFileForm(this,"+epic_id+");\"/>";
			  storyLi+="					</label>";
			  storyLi+="				</form>";
			  storyLi+="                    <span class=\"Upload_From_System_cLabelText\" style='width:150px;float:left;overflow:hidden;height:20px;'></span></div>";
			  storyLi+="             <div class=\"optionMenuCss optionMenuOpacity\"  onclick=\"attachDocToIdea("+epic_id+");\">";
			  storyLi+="                    <span class=\"optionSpanHover agileSpanCss1 Upload_From_Repository_cLabelText\" style='width:150px;overflow:hidden;height:20px;'></span></div>";
			  storyLi+="             <div class=\"optionMenuCss optionMenuOpacity\" style='border:none;' onclick=\"attachLinkToIdea("+epic_id+");\">";
			  storyLi+="                    <span style='width:150px;overflow:hidden;height:20px;' class=\"optionSpanHover agileSpanCss1 Attach_Link_cLabelText\"></span></div>";
			
			$('#documentOptionDiv_'+epic_id).html(storyLi);	
			loadAgileCustomLabel("Story");		
        }
        $('#documentOptionDiv_'+epic_id).show();
        //if($("#optionPopContainer_"+epic_id).hasClass("mCustomScrollbar")){
        //   var mainDivH=$('#optionsDiv_'+epic_id).height();
        //   var docDivH=$('#documentOptionDiv_'+epic_id).height();
	    //   $('#documentOptionDiv_'+epic_id).css('margin-top',mainDivH-docDivH-5+'px');
	    //}
   }else{
 	  if (!$('#documentOptionDiv_'+epic_id).is(':hidden')) {
 		 $('#documentOptionDiv_'+epic_id).hide();
 	  }
 	}
 }  
 function showDefectOptionDiv(epic_id,act){
    if (act=='Y') {
       if($('#defectOptionDiv_'+epic_id).children().length <1){
		   var storyLi="";
           storyLi+="             <div class=\"optionMenuCss optionMenuOpacity\"  onclick=\"ReportBug("+epic_id+");\">";
           storyLi+="                 <span class=\"optionSpanHover agileSpanCss1\">Create</span></div>";
           storyLi+="             <div class=\"optionMenuCss optionMenuOpacity\"  style=\"border-bottom:none;\"  onclick=\"associateDefect("+epic_id+");\">";
		   storyLi+="                 <span class=\"optionSpanHover agileSpanCss1\">Associate</span></div>";
      	   $('#defectOptionDiv_'+epic_id).html(storyLi);				  
        }
        $('#defectOptionDiv_'+epic_id).show().css('margin-top','200px');
        //if($("#optionPopContainer_"+epic_id).hasClass("mCustomScrollbar")){
        //   var mainDivH=$('#optionsDiv_'+epic_id).height();
        //   var docDivH=$('#defectOptionDiv_'+epic_id).height();
	    //   $('#defectOptionDiv_'+epic_id).css('margin-top',mainDivH-docDivH-5+'px');
	    //}
   }else{
 	  if (!$('#defectOptionDiv_'+epic_id).is(':hidden')) {
 		 $('#defectOptionDiv_'+epic_id).hide();
 	  }
 	}
 }


 //-------------------------------------------------------------------------------------// document functions//-----------------------------	
 var uploadFormForAgile="";
 function submitAgileRepositoryFileForm(e,epicId){
		$('#loadingBar').show();
		timerControl("start");
		var formname = e.form.name;
		uploadFormForAgile = document.getElementById(formname);
		uploadFormForAgile.target = 'upload_agile_target';
		var browserName  = navigator.appName;
			if(browserName == 'Microsoft Internet Explorer'){
				$('#detectBrowser_'+epicId).val('ie');
				uploadFormForAgile.submit();
			}
			else{
				var fileSize = e.files[0].size;
				$('#size_'+epicId).val(fileSize);
	    		if(fileSize >= 25000000) {
	    		    confirmReset(getValues(companyAlerts,"Alert_ResetMoreSize"),'delete','docUploadInAgile','cancelTheAgileUpload');
	    		    $('.alertMainCss').css({'width': '28.5%'});
					$('.alertMainCss').css({'left': '45%'});
					$('#BoxConfirmBtnOk').css({'width': '21%'});
	    			$('#BoxConfirmBtnOk').val('Continue');
	    		}else{
	    			uploadFormForAgile.submit();
	    		}
			}
		 		
	}

function docUploadInAgile(){
	uploadFormForAgile.submit();
}
function cancelTheAgileUpload(){
		$("#loadingBar").hide();	
  	    timerControl("");
}
 function uploadedAgileDoc(epicId,docData){
           closeEpicPopup();
       
           if($('#docImgDiv_'+epicId).children('img').size()){
 				$('#docIcon_'+epicId).attr('src',path+'/images/idea/DocumentToggle.png');
 			}else{
 				var imgData="<img id=\"docIcon_"+epicId+"\"  class='epicShareImgDiv Documents_cLabelTitle' src=\""+path+"/images/idea/DocumentToggle.png\" onclick=\"showIdeaDocs('docIcon_"+epicId+"','"+epicId+"');\" />";
 				$('#docImgDiv_'+epicId).html(imgData);
 			}
 			fetchEpicDocs(epicId);
			sendToSlack($("#proj_ID").val(),'agile_document',epicId);
			sendToSpark($("#proj_ID").val(),'agile_document',epicId);
   }

 function fetchEpicDocs(epicId){
		$.ajax({
				url: path+"/agileActionNew.do",
				type:"POST",
				data:{action:"listIdeaDocs",ideaId:epicId},
				error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
				},
				success:function(result){
					checkSessionTimeOut(result);
					showHideListDivs(epicId,"document");
					$('#epicListCommonDiv_'+epicId).html(prepareStoryDocUIHeader());
					$('#epicListCommonDiv_'+epicId).append(prepareStoryDocUI(jQuery.parseJSON(result)));
					loadAgileCustomLabel("AttachDoc");
					$('#epicListCommonDiv_'+epicId).slideDown(600);
					if(agileNotificationType != ""){  
						$("#agileDoc_"+epicCmtDocId).addClass("notHightlightCls");
						agileNotificationType="";
				    }
					$("#loadingBar").hide();
					timerControl("");
			  }
		  });
 }

 function showIdeaDocs(docId,epicId){
	 if($("#"+docId).attr('src').indexOf("Toggle.png") == -1 ){
	     $("#"+docId).attr('src', path+'/images/idea/DocumentToggle.png');
	     $("#loadingBar").show();
	     timerControl("start");
	     fetchEpicDocs(epicId);
	 }else{
	     $('#epicListCommonDiv_'+epicId).slideUp(function(){
	         $("#"+docId).attr('src', path+'/images/idea/Document.png'); 
	     });
	 }
	 
 }


	function attachDocToIdea(idd){
          closeEpicPopup();
          $('#ideaDocPopUp').css('display','block');
     	  var landH=$('#ideaDocPopUp').height();
		  var mainH=$(window).height();
		  if(mainH>landH){
								   //alert('1st');
		  }else{
			  $('#ideaDocPopUp').css({'width':'920px','margin-left':'-460px','top':'0%','margin-top':'0px','height':mainH-10+'px'});
			  popScroll('ideaDocPopUp');
		  }
          $('#docIdeaId').val(idd);
 		  $('#transparentDiv').show(); 
 		  $("#ideaDocPopUp").find("#attachDocBtn").find("#attachSaveBtnDiv").attr("onClick", "attachDocumentforEpic()");
	      loadAgileCustomLabel("AttachDoc");
	      loadFolder(); 
 		 
   }  
  
  
  
  
  
 function attachDocumentforEpic(){
       var docIdList="";
       var ideaId=$('#docIdeaId').val();
       $('#folderDiv').find('div.docSelected').each(function(){
        var docId=$(this).parent().attr('id').split('_');
        docIdList=docIdList+docId[1]+",";
       })
            
        if($('#docImgDiv_'+ideaId).children('img').size()){
			$('#docIcon_'+ideaId).attr('src',path+'/images/idea/DocumentToggle.png');
		}else{
			var imgData="<img id=\"docIcon_"+ideaId+"\"  class='epicShareImgDiv Documents_cLabelTitle' src=\""+path+"/images/idea/DocumentToggle.png\" onclick=\"showIdeaDocs('docIcon_"+ideaId+"','"+ideaId+"');\" />";
			$('#docImgDiv_'+ideaId).html(imgData);
		}
 		$("#loadingBar").show();
        timerControl("start");
       	
 		$.ajax({
	  	     url: path+"/agileActionNew.do",
	  	     type:"POST",
	  		 data:{action:"insertEpicDoc",docSplitId:docIdList,ideaId:ideaId},
	  		 error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
			},
	  		 success:function(result){
	  			    checkSessionTimeOut(result);
  				    cancelAttachDoc();
  				    showHideListDivs(ideaId,"document");
  					$('#epicListCommonDiv_'+ideaId).html(prepareStoryDocUIHeader());
					$('#epicListCommonDiv_'+ideaId).append(prepareStoryDocUI(jQuery.parseJSON(result)));
					loadAgileCustomLabel("AttachDoc");
					$('#epicListCommonDiv_'+ideaId).slideDown(600);
					$("#loadingBar").hide();
					timerControl("");
	  				sendToSlack($("#proj_ID").val(),'agile_document',ideaId);
	  				sendToSpark($("#proj_ID").val(),'agile_document',ideaId);	
	  	   }
	   }); 
  }
	  	
  function attachLinkToIdea(idd){
          closeEpicPopup();
          loadAgileCustomLabel("AttachLink");
          $('#ideaLinkPopUp').css('display','block');
	      $('#linkIdeaId').val(idd);
 		  $('#transparentDiv').css('display','block'); 
  }


 function cancelAgileAttachLink(){
    $('#ideaLinkPopUp').hide();
    $('#linkTitle').val('');
	$('#linkLink').val('');
    $('#transparentDiv').hide(); 
 }
 	
 
 function AttachAgileLink(){
		if( $("#linkLink").val().trim() == '' ){
			alertFun(getValues(companyAlerts,"Alert_enterLink"),'warning');
			$("#linkLink").focus();
			return false;
		}
		var key = $("#linkLink").val().trim();
	    if (!isValidURL(key)) {
	       alertFun(getValues(companyAlerts,"Alert_invalidUrl"),'warning');
	       return false;
	    }
		var title1=$("#linkTitle").val().trim();
		var title = $("#linkTitle").val().trim();
		var link = $("#linkLink").val().trim();
		if(title==""){
		    title=link;
		    title1=link;
	      }
	    var ideaId=$('#linkIdeaId').val();   	
	    if($('#docImgDiv_'+ideaId).children('img').size()){
			$('#docIcon_'+ideaId).attr('src',path+'/images/idea/DocumentToggle.png');
		}else{
			var imgData="<img id=\"docIcon_"+ideaId+"\"  class='epicShareImgDiv Documents_cLabelTitle' src=\""+path+"/images/idea/DocumentToggle.png\" onclick=\"showIdeaDocs('docIcon_"+ideaId+"','"+ideaId+"');\" />";
			$('#docImgDiv_'+ideaId).html(imgData);
		}
 		
         		
	    $("#loadingBar").show();
	    timerControl("start");
        $.ajax({
			   	url: path+"/agileActionNew.do",
				type:"POST",
				data:{action:"insertEpicLink",ideaId:ideaId,link:link,title:title},
				error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
				},
				success:function(result){
		               checkSessionTimeOut(result);
		               showHideListDivs(ideaId,'document');
		          	   cancelAgileAttachLink();
				       $('#epicListCommonDiv_'+ideaId).html(prepareStoryDocUIHeader());
						$('#epicListCommonDiv_'+ideaId).append(prepareStoryDocUI(jQuery.parseJSON(result)));
						loadAgileCustomLabel("AttachDoc");
						$('#epicListCommonDiv_'+ideaId).slideDown(600);
			           $("#linkTitle").val("");
					   $("#linkLink").val("");
				       $("#loadingBar").hide();	
		               timerControl("");
		               sendToSlack($("#proj_ID").val(),'agile_link',ideaId);
		               //sendToSpark($("#proj_ID").val(),'agile_link',ideaId);	
		  	   }
         });
 }
	
  function funAttachLinkUpdate(linkId){
		 var imgSrc = $("#editLink_"+linkId).attr("src");
		 if( imgSrc.indexOf("edit.png") != -1 ){
		    $("#editLink_"+linkId).removeClass('Edit_cLabelTitle').addClass('Save_cLabelTitle').attr("src", path+"/images/idea/save.png");
		    var title=$("#title_"+linkId).attr('title');
		    $("#title_"+linkId).val(title).css("border","1px solid #FEAB3E").removeAttr("readOnly").focus();
		 	$("#spanLinkIdea_"+linkId).hide();
		 	$("#spanLink_"+linkId).css("border","1px solid #FEAB3E").removeAttr("readOnly").show();
		 	loadAgileCustomLabel("AttachDoc");
		 }else if( imgSrc.indexOf("save.png") != -1 ){
		    var title=$("#title_"+linkId).val();
		    var link=$("#spanLink_"+linkId).val();
		    if (!isValidURL(link)) {
		       alertFun(getValues(companyAlerts,"Alert_invalidUrl"),'warning');
		       return false;
		    }
			$("#loadingBar").show();
		    timerControl("start");
 			       $.ajax({
 						   	url: path+"/agileActionNew.do",
 							type:"POST",
 							data:{action:"updateEpicLink",epicLinkId:linkId,title:title,link:link},
 							error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
							},
 							success:function(result){
 					            checkSessionTimeOut(result);
 					            $("#title_"+linkId).attr('title',title);
 					           	
							    $("#title_"+linkId).val(title).css("border","0px").attr("readOnly","readOnly");
							 	$("#spanLink_"+linkId).css("border","0px").attr("readOnly","readOnly").hide();
							 	$("#editLink_"+linkId).attr("src", path+"/images/idea/edit.png").removeClass('Save_cLabelTitle').addClass('Edit_cLabelTitle');
		    					loadAgileCustomLabel("AttachDoc");
							 	$("#spanLinkIdea_"+linkId).show();
							    $("#spanLinkIdea_"+linkId).text(link).attr('title',$("#spanLink_"+linkId).val());
 					            $("#loadingBar").hide();
 					            timerControl("");
 							}
 			      });
		}
		
  }
  
 	 	 
 function funAttachLinkDelete(linkId,epicId){
		confirmId=epicId;
		confirmLinkId=linkId
		confirmFun(getValues(companyAlerts,"Alert_delete"),"delete","agileAttachLinkDeleteConfirm");
 }		
		
 function agileAttachLinkDeleteConfirm(){
		  $("#loadingBar").show();
		  timerControl("start");
	       $.ajax({
				   	url: path+"/agileActionNew.do",
					type:"POST",
					data:{action:"deleteEpicLink",linkId:confirmLinkId},
					error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
					success:function(result){
			           checkSessionTimeOut(result);
			           $("#epicLink_"+confirmLinkId).remove();
			           if(!$('#epicListCommonDiv_'+confirmId).children('div').size()){
                         $('#docImgDiv_'+confirmId).html('');
                         $('#epicListCommonDiv_'+confirmId).hide();
                       }
                       if($('#epicListCommonDiv_'+confirmId).children('div').size()== 1){
                    	   $('#epicListCommonDiv_'+confirmId).hide();
                    	   $('#docImgDiv_'+confirmId).children('img').remove();
                       }
                       $("#loadingBar").hide();
                       timerControl("");
                }
 		});
 }

 function funAttachDocUpdate(docId){
		var imgSrc = $("#editDoc_"+docId).attr("src");
		if( imgSrc.indexOf("edit.png") != -1 ){
		    $("#editDoc_"+docId).removeClass('Edit_cLabelTitle').addClass('Save_cLabelTitle').attr("src",path+"/images/idea/save.png");
			var title=$("#titleDoc_"+docId).attr('title');
			$("#titleDoc_"+docId).val(title).css("border","1px solid #FEAB3E").removeAttr("readOnly").focus();
		    loadAgileCustomLabel("AttachDoc");
		}else if( imgSrc.indexOf("save.png") != -1 ){
			var title=$("#titleDoc_"+docId).val();
			
		    $("#loadingBar").show();
			timerControl("start");
 			       $.ajax({
 						   	url: path+"/agileActionNew.do",
 							type:"POST",
 							data:{action:"updateEpicDoc",epicDocId:docId,title:title},
 							error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
							},
 							success:function(result){
 					           checkSessionTimeOut(result);
 					           $("#titleDoc_"+docId).attr('title',title);
 					           
							   $("#titleDoc_"+docId).val(title).css("border","0px");
							   $("#editDoc_"+docId).attr("src", path+"/images/idea/edit.png").removeClass('Save_cLabelTitle').addClass('Edit_cLabelTitle');
		    				   loadAgileCustomLabel("AttachDoc");
							   $("#titleDoc_"+updatedDocId).attr("readOnly","readOnly");
 					           $("#loadingBar").hide();
 					           timerControl("");
 							}
 			      });
		}
		
 }	
  
 	  
 function delteDocLink(epicDocId,epicId){
        confirmId=epicDocId;
        confirmEpicId=epicId;
		confirmFun(getValues(companyAlerts,"Alert_delete"),"delete","agileAttachDocDeleteConfirm");
 }
 

 function attachDocDeleteConfirm(){
		 	 
		 	 parent.$("#loadingBar").show();
		 	 parent.timerControl("start");
 			       $.ajax({
 						   	url: path + "/agileActionNew.do",
 							type:"POST",
 							data:{action:"deleteEpicDoc",epicDocId:confirmId},
 							error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
							},
 							success:function(result){
 					           parent.checkSessionTimeOut(result);
 					           $("div#epicListCommonDiv_"+confirmEpicId).find("#agileDoc_"+confirmId).remove();
 		                       if(!isiPad){ 
 		                          $("#epicList").mCustomScrollbar("update");
 		                       } 
 		                       if(!$('#epicListCommonDiv_'+confirmEpicId).children('div').size()){
 		                         $('#docImgDiv_'+confirmEpicId).html('');
 		                         $('#epicListCommonDiv_'+confirmEpicId).hide();
 		                         $('#epicCreatedBy_'+confirmEpicId).show();
 		                       }
 		                       parent.$("#loadingBar").hide();
 		                       parent.timerControl("");
 		                       
 							}
 					});
}

 function agileAttachDocDeleteConfirm(){
		 	 $("#loadingBar").show();
		 	 timerControl("start");
		       $.ajax({
					   	url: path+"/agileActionNew.do",
						type:"POST",
						data:{action:"deleteEpicDoc",epicDocId:confirmId},
						error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
							},
						success:function(result){
				           checkSessionTimeOut(result);
				           $("div#epicListCommonDiv_"+confirmEpicId).find("#agileDoc_"+confirmId).remove();
	                       if(!$('#epicListCommonDiv_'+confirmEpicId).children('div').size()){
	                         $('#docImgDiv_'+confirmEpicId).html('');
	                         $('#epicListCommonDiv_'+confirmEpicId).hide();
	                       }
	                       if($('#epicListCommonDiv_'+confirmEpicId).children('div').size()== 1){
	                    	   $('#epicListCommonDiv_'+confirmEpicId).hide();
	                    	   $('#docImgDiv_'+confirmEpicId).children('img').remove();
	                       }
	                       $("#loadingBar").hide();
	                       timerControl("");
	                   }
				});
	}
	

	  
 function funAttachDocIdea(){
         $("#attachIdeaDocDiv,#attachIdeaLinkDiv,#ideaDocBtnDiv").hide();
	     $("#attachFilesProjectDiv").show();
	 	 $("#folderDiv").css({"height":"74%","overflow-y":"auto"});
	 	 loadFolder();
 } 

 function selectDoc(obj) {
		var classObj = $(obj).attr('class');
		
		if(classObj == 'unCheck') {
		    $(obj).addClass('check').removeClass('unCheck');
			$(obj).parent().parent().addClass('docSelected').removeClass('docUnSelected');
			$('#attachSaveBtnDiv').show();
			$('#TaskLevelattachSaveBtnDiv').show();
		}else{ 
		    $(obj).parent().parent().removeClass('docSelected').addClass('docUnSelected');
			$(obj).addClass('unCheck').removeClass('check');
			if($('div.docSelected').length<1){
			  $('#attachSaveBtnDiv').hide();
			  $('#TaskLevelattachSaveBtnDiv').show();
			}
		}		
 }
 	  		 
	  

 
 
 

//-------------------------------------------------------------------// priority add & update functions //------------------
   function ideaPriorityPopup(epic_id){
 		$('.ideaTransparent').css('display','none');
 		parent.$('#transparentDiv').css('display','none');
 		$('#optionsDiv_'+epic_id).hide();
 		global_id=epic_id;
 	   if ($('#priorityOptionDiv_'+epic_id).is(':hidden')) {
 	       if($('#priorityOptionDiv_'+epic_id).children().length <1){
	          var priorListArray = $('#getPlist').val().split(',');
			  var storyLi="";  
			     for(var i=0;i<priorListArray.length;i++){    
			       var pCode = priorListArray[i].split('-')[0];
			       var pName = priorListArray[i].split('-')[1]; 
			       var pImg = (pCode=="")? "" : pCode=="1" ? "${path}/images/Idea/VeryImportant.png" : pCode=="2" ? "${path}/images/Idea/Important.png":pCode=="3" ? "${path}/images/Idea/medium.png" :pCode=="4" ? "${path}/images/Idea/Low.png": "${path}/images/Idea/VeryLow.png";
				   storyLi+="             <div class=\"optionMenuCss optionMenuOpacity\"  onclick=\"addIdeaPriority(this,"+pCode+","+epic_id+");\">";
				   storyLi+="                 <img style=\"float:left;\" src=\""+pImg+"\"  class=\"imgCss\"/><span class=\"optionSpanHover\">"+pName+"</span></div>";
				 }
			 	 $('#priorityOptionDiv_'+epic_id).html(storyLi);  
			}
 	        
 		   $('.ideaTransparent').css('display','block');
 		   parent.$('#transparentDiv').css('display','block');
 		   $('#priorityDiv_'+epic_id).css('z-index','5000');
 		   $('#priorityOptionDiv_'+epic_id).css('display','block');
 		}else{
 		   $('#priorityDiv_'+epic_id).css('z-index','100');
 		   $('#priorityOptionDiv_'+epic_id).css('display','none');
 		}	
 	   
  }		
    	
 function addIdeaPriority(obj,priority,ideaId){
	  $('.ideaTransparent').css('display','none');
	  parent.$('#transparentDiv').css('display','none');
	  $('#priorityDiv_'+ideaId).css('z-index','100');
	  $('#priorityOptionDiv_'+ideaId).css('display','none');
	  var imgSrc=$(obj).children('img').attr('src');
	  var pTitle = $(obj).children('span').text();
	  
	  var imgObj = '<img src="'+imgSrc+'" title="'+pTitle+'" style="border: 1px solid rgb(255, 255, 255); border-radius: 4px;" onclick="showPriorityPopUp(this,'+ideaId+');ideaPriorityPopup('+ideaId+');"/>';
	  $(obj).parent().prev().html(imgObj).css('cursor','pointer');
	  $("#loadingBar").show();
	  timerControl("start");
	       $.ajax({
				   	url:"${path}/agileActionNew.do",
					type:"POST",
					data:{action:"insertEpicPriority",priority:priority,ideaID:ideaId },
					error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
					success:function(result){
			            checkSessionTimeOut(result);
			            $("#loadingBar").hide();
			            timerControl("");
					}
			});
 } 
 
 function addIdeaPriority1(obj,priority,storyId){
	  closeEpicPopup();
	  var imgSrc=$(obj).children('img').attr('src');
	  var pTitle = $(obj).children('span').text();
	  
	  if($('#bandDiv_'+storyId).find('img.priorityImgCls').size()){
	    $('#bandDiv_'+storyId).find('img.priorityImgCls').attr('src',imgSrc).attr('title',pTitle);
	  }else{
	    var imgObj = '<img src="'+imgSrc+'"   class="priorityImgCls" title="'+pTitle+'" style="left:0;position: absolute; margin-left: -8px; margin-top: -4px;" onclick="showPriorityPopUp(this,'+storyId+');ideaPriorityPopup('+storyId+');"/>';
	    $('#bandDiv_'+storyId).prepend(imgObj);
	  }
	  $("#loadingBar").show();
	  timerControl("start");
	       $.ajax({
				   	url: path+"/agileActionNew.do",
					type:"POST",
					data:{action:"insertEpicPriority",priority:priority,ideaID:storyId },
					error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
					success:function(result){
			            checkSessionTimeOut(result);
			            $("#loadingBar").hide();
			            timerControl("");
					}
			});
 } 
 

//--------------------------------------------------------------------// story status update functions //------------------------------  
 
 function updateEpicStatus(obj,epicStatus,epicId){
	   confirmStoryStatus=epicStatus;
       confirmId=epicId;
       confirmObj=obj;
       
	  var prevStatus=$('#epicStoryStatus_'+epicId).text();
	  
	  if(prevStatus!='Backlog' && epicStatus=='Backlog'){
        confirmFun(getValues(companyAlerts,"Alert_deassociate"),"clear","confirmUpdateEpicStatus");
      }else{
        confirmUpdateEpicStatus();
      }
 }	  

 function confirmUpdateEpicStatus(){
      closeEpicPopup();
      $('#addCommentPopDiv').show();
      $('#transparentDiv').css('display','block');
 	  $('#addCommentPopDiv').find('#savePopComment').attr('onclick','saveUpdateEpicStatus();');
 	  $('#addCommentPopDiv').find('.popUpCloseIcon').attr('onclick','cancelUpdateEpicStatus();');
 	  
 }

 function cancelUpdateEpicStatus(){
      $('#addCommentPopDiv').hide();
      $('#transparentDiv').hide();  
 }
 
 function saveUpdateEpicStatusComment(){
    var comment=$('#popCommentTextArea').val().trim();
 	  if(comment==''|| comment==null){
 		//parent.alertFun("<bean:message key='Agile_Comment.message'/>",'warning');
 		parent.alertFun(getValues(companyAlerts,"Alert_comment"),'warning');
 		$('#popCommentTextArea').val('');
 		return false;		
 	  }
 	  saveUpdateEpicStatus();
 }	
   
 function saveUpdateEpicStatus(){	
      var comment=$('#popCommentTextArea').val().trim();
      $('#addCommentPopDiv').hide();
      $('#transparentDiv').hide();  
 	  
 	  var epicStatus=confirmStoryStatus;
	  var epicId=confirmId;
	  var type = $('#epic_'+epicId).attr('type');
	  var imgSrc=(epicStatus=='Backlog')? path+"/images/idea/Backlog.png" : (epicStatus=='In Progress')? path+"/images/idea/Assigned.png" : (epicStatus=='Blocked')? path+"/images/idea/Blocked.png" : (epicStatus=='Hold')? path+"/images/idea/Hold.png" : (epicStatus=='Done')? path+"/images/idea/Done.png" : path+"/images/idea/cancel.png"
	  //if(type=='S'){
	  	$('#epic_'+epicId).find('img.epicProgressImg').attr('src',imgSrc);
	  	$('#epicStoryStatus_'+epicId).text(epicStatus);
	  //}else{
	  //  var html ="  <img id=\"epicStatusImg_"+epicId+"\"  style='float:left;width:18px;height:18px;' src=\""+imgSrc+"\"/>"
	//	         +"  <span id=\"epicStatusSpan_"+epicId+"\"  style='float:left;margin-left:5px;font-style:italic;font-size:13px;font-weight:normal;' >"+epicStatus+"</span>";
	//	$('#epicStatusDiv_'+epicId).html(html);
	 // }
	  $("#loadingBar").show();
	  timerControl("start");
	       $.ajax({
				   	url: path+"/agileActionNew.do",
					type:"POST",
					data:{action:"updateEpicStatus",epicStatus:epicStatus,epicID:epicId ,epicComment:comment},
					error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
					success:function(result){
			            checkSessionTimeOut(result);
			            $('#popCommentTextArea').val("");
			            
			            if(type=='S'){
				            if(epicStatus=='In Progress' ){
				               $('#statusDiv_'+epicId).children('div.Assigned').hide();
				            }
				            if((epicStatus!='In Progress' && epicStatus!='Backlog') && result=='OnProgress'){
				               $('#statusDiv_'+epicId).children('div.Assigned').show();
				            }
				            if(epicStatus=='Done' || epicStatus=='Cancel'){
				               $('#ideaTask_'+epicId).removeClass('optionMenuOpacity').addClass('optionMenuOpacity1').removeAttr('onclick');
				               $('#ideaTask_'+epicId).children('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
				            }else{
				               $('#ideaTask_'+epicId).removeClass('optionMenuOpacity1').addClass('optionMenuOpacity').attr('onclick','newUicreateTaskUI("","0","Sprint","'+epicId+'","","","","","sprintTask","","Epic");');		//createTaskUI("", "Sprint", "'+epicId+'", "Epic");
				               $('#ideaTask_'+epicId).children('span').removeClass('optionSpanHoverFade').addClass('optionSpanHover');
				            }
				            if(epicStatus=='Backlog'){
				               $('#epicStoryStatus_'+epicId).nextAll().remove();
				               $('#statusDiv_'+epicId).find('div.Assigned').hide();
				            }
				            getStatusCountForEpic($('#epic_'+epicId).parents('div.epicContentMainDiv').attr('id').split('_')[1]); 
			            }else{
			               $('#epicStoryStatus_'+epicId).show();
			               $('#epicProgressImg_'+epicId).show();
			            }
			            $('#epic_'+epicId).attr('storystatus',epicStatus)
			            if(comment!='' && comment!=null){
				            if($('#Action_'+epicId+' #cmtImgDiv_'+epicId).children('img').size()){
	 					         //$('#cmtIcon_'+epicId).attr('src','${path}/images/Idea/comment1Toggle.png');
	 					    }else{
								var imgData="<img id=\"cmtIcon_"+epicId+"\" style='cursor: pointer; float: right;margin-right: 3px;' class='Comments_cLabelTitle' src=\""+path+"/images/idea/comment1.png\" onclick=\"showIdeaIconCmts('cmtIcon_"+epicId+"','"+epicId+"');\" />";
								$('#cmtImgDiv_'+epicId).html(imgData);
								if($('#cmtIcon_'+epicId).attr('src').indexOf("comment1.png")!= -1){
									showIdeaIconCmts("cmtIcon_"+epicId,epicId);
	       						}
	 					   }
	 					}   
	 					
			           $("#loadingBar").hide();
			           timerControl("");
					}
			});
 }
//--------------------------------------------------------------------// epic & feature stage update functions //------------------------------  
 

 
 function updateEpicStage(obj,epicId){
	  closeEpicPopup();
 	  var stageId = $(obj).attr('stageId');
	  $("#loadingBar").show();
	  timerControl("start");
	       $.ajax({
				   	url: path+"/agileActionNew.do",
					type:"POST",
					data:{action:"updateEpicStage", stageId:stageId, storyId:epicId},
					error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
					success:function(result){
			            checkSessionTimeOut(result);
			            var epicType = $('#epic_'+epicId).attr('type');
			            var html ='';
			            //alert('stageId:'+stageId);
			            if(stageId !="0"){
			                //alert('epicType:'+epicType);
				            if(epicType=='E'){
				              var stageName = $('#valueDiv_'+epicId).find('div[stageId='+stageId+']').find('span').text();
				              html = " <span style=\"float: left; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 100%;\" title=\""+stageName+"\" >Value&nbsp;:&nbsp;"+stageName+"</span> ";
				            }else{
				              var stageName = $('#valueDiv_'+epicId).find('div[stageId='+stageId+']').find('span').text();
				              html = " <span style=\"float: left; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 100%;\" title=\""+stageName+"\" >Functionality&nbsp;:&nbsp;"+stageName+"</span> ";
				            }
				            
				            $('#epicStageDiv_'+epicId).html(html);
				            $('#epic_'+epicId).find('img.epicProgressImg').attr('src',path+"/images/idea/Assigned.png").show();
	  						$('#epicStoryStatus_'+epicId).html("In Progress").show();
	  					
	  					}else{
			               $('#epicStageDiv_'+epicId).text("");
			               $('#epic_'+epicId).find('img.epicProgressImg').hide();
	  					   $('#epicStoryStatus_'+epicId).hide();
			            }
			            
			            
			            $("#loadingBar").hide();
			            timerControl("");
					}
			});
 }
 
 /*New Functionality for inserting and editing the epic, feature and stories 02-07-2019 */
 
 function enterKeyValidateEpic(event,type,epicType,epicId,parentEpicId){
	 if(type=='keypress' && (event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey){	
			if(epicType == 'mainEpic'){
				createNewEpic();
			}else if(epicType == 'editEpic'){
				updateEpic(epicId);
			}else{
				createSubNewIdea(epicId,parentEpicId);
			}
    }
 }
 

 
 //------------------------------------------------------------------------// Epic create functions //------------------------ 
 
   function createNewEpic(){	
        var epicName = $("#main_epicTxtArea").val().trim(); 
		var epicID=$("#epicID").val(); 
		var projID= prjid; // global value
		var localOffsetTime=getTimeOffset(new Date());
		
		if(epicName==''|| epicName==null){
				alertFun(getValues(companyAlerts,"Alert_EpicEmpty"),'warning');
				$("#main_epicTxtArea").focus();
				return false;		
			}
		if(epicName.length>500){
		        alertFun(getValues(companyAlerts,"Alert_ideaNameExceed"),'warning');
				$("#main_epicTxtArea").focus();
				return false;		
		}
		//if($('div#nestable >ol').length < 1){
		//   $('div#nestable').prepend("<ol class='dd-list'></ol>");
		//}
		//$('div#epicContentMain').prepend('<li id="epic_0" data-id="0" style="display:none;"></li>');
		let jsonbody = {
			    "user_id":userIdglb,
			    "company_id":companyIdglb,
			    "project_id":projID,
			    "epic_title":epicName,
			    "epic_type":"E",
			    "localOffsetTime":localOffsetTime
		}
		$("#loadingBar").show();
      	timerControl("start");
		 $.ajax({
				url: apiPath+"/"+myk+"/v1/postAgile",
				type:"POST",
				contentType:"application/json",
				dataType:'json',
				data: JSON.stringify(jsonbody),
				///data:{action:"insertEpic",epicName:epicName, epicId:epicID,projId:projID,localOffsetTime:localOffsetTime},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
		            checkSessionTimeOut(result);
		            //var data=result.split('@@@@Epic@@@@');
		            jsonResult = result;
					result = prepareAgileUI(jsonResult);
					$("div#epicContentMain").prepend(result);	
		            
		            
		            getStatusCountForEpic(jsonResult);
		            //$('div#nestable >ol >li#epic_0').replaceWith(newEpic);
		            
		            //$('#dragDiv_'+data[1]).css('border','2px solid #0485A3');
					loadAgileCustomLabel("Story");
					
					initEpicStoryDragDrop();
					//$('li#epic_'+data[1]).show();//--search
					confirmClearCreateEpic();
					$("#loadingBar").hide();
					timerControl("");
				}
			});   
  }  
  	
  function confirmClearCreateEpic(){        
        $('#main_epicTxtArea').val('');
        ch_count = '500';
        //limiter();
 } 	
 
 function limiter(){
 		var tex = $('#epicTextarea').val();
 		var len = tex.length;
 		if(len > ch_count){
 			  $('#chCount').val("0");
 			  return false;
 		}
 		$('#chCount').val(ch_count-len);
 }	
 
  //------------------------------------------------------------------------// story create functions //-------------------- 
 	
 function showCreateStoryDiv(epicId,createType){
 		  closeEpicPopup();
 		  showHideListDivs(epicId,"addStory");
 		  
 		  var prevCreateType = $('#createType_'+epicId).val();
 		  if( prevCreateType != createType){
 		    $('div#rowTab_'+epicId).hide().remove();
 		  }
 		  
 		 if ($('div#epic_'+epicId).parent().children('div#rowTab_'+epicId).length < 1) {
 	 	        var parentEpicId='';
 	 	        var parentEpicType = $('div#epic_'+epicId).attr('type');
 	 	        if(parentEpicType=="F"){
 	 	            parentEpicId=epicId;
 	 	        }else{
 	 	            parentEpicId=$('div#epic_'+epicId).attr('parent_epic_id');
 	 	        }
 	 	        
 	 	       var html="  <div class=\"row epicTextareaMainDiv\" id ='rowTab_"+epicId+"' style=\"display:none;padding-left:30px;\" >"
							  +"<div class=\"col-sm-12 col-xs-12 defaultPaddingCls\" style=\"margin-bottom:5px;\">"
							   +"  <div align=\"center\" class=\""+( createType=="S" ? "epicContentYellowBandCss" : "epicContentPurpleBandCss")+"\" style=\"height:50px;\" ></div>"
							   +"  <textarea id=\"subIdeaTextarea_"+epicId+"\" onkeyup=\"enterKeyValidateEpic(event,'keyup','subEpic',"+epicId+","+parentEpicId+");\" onkeypress=\"enterKeyValidateEpic(event,'keypress','subEpic',"+epicId+","+parentEpicId+");\"  class=\"main_epicTxtArea "+( createType=="S" ? "Enter_a_Story_cLabelTitle Enter_a_Story_cLabelPlaceholder" : "Enter a feature")+"\"></textarea>"
							   +"  <div align=\"center\" class=\"main_epicTxtArea_btnDiv\" >"
							   +"     <div><img class=\"img-responsive epicSaveImg Post_cLabelTitle\" title=\"\" onclick=\"createSubNewIdea("+epicId+","+parentEpicId+");\" src=\""+path+"/images/workspace/post.png\" alt=\"Image\" /></div>"
							   +"     <input id=\"epicID\" type=\"hidden\" value=\"0\" />"
							   +"     <input type='hidden' id='createType_"+epicId+"' value='"+createType+"' />"
							   +"  </div>"
							  +"</div>"
						  +"</div>";
						 
						 
				//if($('div#epicSubContainerDiv_'+epicId).children('div').length < 1){		 
 	 	          $('div#epic_'+epicId).after(html);
 	 	        //}else{
 	 	        //  $(html).insertBefore('ol#drag_idea_'+epicId);
 	 	        //}  
 	 	        loadAgileCustomLabel("Story");
 	 	   } 
 	 	       
 	 	   if ($('div#rowTab_'+epicId).is(":hidden")) {
 		    	//$('div.addStoryCreateDiv').hide();
	 		    //$('div.epicCreatedByDiv').show();
	 		    //$('#epicCreatedBy_'+epicId).hide();
	 		  /* if(!isiPad){
	 		        if($('#epicList').find('div.mCSB_container').hasClass('mCS_no_scrollbar_y')){
		 		        $('div#addSubIdea_'+epicId).show();
			 		    $("#epicList").mCustomScrollbar("destroy");
			 		    epicListCustomScroll();
			 		    setTimeout(function(){
						           $('#subIdeaTextarea_'+epicId).focus();
						 },500);
		 		    }else{
			 		    $('div#addSubIdea_'+epicId).show();
			 		    $("#epicList").mCustomScrollbar("update");
			 		    //$("div#epicList").mCustomScrollbar("scrollTo",'#chCount_'+epicId);
			 		    //$("div#epicList").mCustomScrollbar("scrollTo",100);
			 		    setTimeout(function(){
						           $('#subIdeaTextarea_'+epicId).focus();
						 },500);
		 		    }
		 		    
	 		  }else{ */
	 		       $('div#rowTab_'+epicId).show();
	 		       //setTimeout(function(){
						   $('#subIdeaTextarea_'+epicId).focus();
					//},500);
	 		  //} 
	     }else{
 		    $('div#rowTab_'+epicId).hide();
 		    //$('#epicCreatedBy_'+epicId).show();
 		    //if(!isiPad){ 
 		    //	$("#epicList").mCustomScrollbar("update");
 		    //}	
 		  }
  }	
   
   function closeEpicPopup(){
           //$('#optionsDiv_'+global_id).css('display','none');
           $('.hideEpicOptionDiv').hide();
          /* $('.ideaTransparent').css('display','none');
           parent.$('#transparentDiv').css('display','none');
 		  if (!$('#priorityOptionDiv_'+global_id).is(':hidden')) {
 		      $('#priorityDiv_'+global_id).css('z-index','100');
 		      $('#priorityOptionDiv_'+global_id).css('display','none');
 		   }
 	
		 if (!$('#ideaSharePopUp').is(':hidden')) {
		     $('#ideaSharePopUp').html("").css('display','none');
		   }
		 if (!$('#ideaCreateTask').is(':hidden')) {
		     $('#ideaCreateTask').html("").css('display','none');
		   }  
		 if (!$('#ideaDocPopUp').is(':hidden')) {
		     $('#ideaDocPopUp').html("").css('display','none');
		   }
		 if (!$('#ideaMyTask').is(':hidden')) {
		     $('#ideaMyTask').html("").css('display','none');
		   }     */
		  
  }
  
  function createSubNewIdea(epicId,parentEpicId){
        var orderXml="";
        var storyName = $("#subIdeaTextarea_"+epicId).val().trim(); 
        var projID=$("#proj_ID").val(); 
        var projUserStatus = $("#projUserStatus").val(); 
        var createEpicType = $('#createType_'+epicId).val();
        var localOffsetTime=getTimeOffset(new Date());
        if(storyName==''|| storyName==null){
				alertFun(getValues(companyAlerts,"Alert_StoryEmpty"),'warning');
				$("#subIdeaTextarea_"+epicId).focus();
				return false;		
		}
		if(storyName.length>500){
				alertFun(getValues(companyAlerts,"Alert_ideaNameExceed"),'warning');
				$("#subIdeaTextarea_"+epicId).focus();
				return false;		
		}
		var loadType="";
		var parentEpicType = $('div#epic_'+epicId).attr('type');
		
		  if(parentEpicId=='0' || parentEpicType == 'F'){
		  	if($('div#epicSubContainerDiv_'+epicId).children().length==0){
		      loadType="allStory";
		    }else{
		      loadType="newStory";
		      $('div#epicSubContainerDiv_'+epicId).prepend('<div id = "epicContentDiv_0" class ="epicContentDivs"></div>');
		      //orderXml=getStoryOrderXml(parentEpicId);
		    }
		  }else{
		    loadType="newStory";
		    $('div#epicContentDiv_'+epicId).after('<div id = "epicContentDiv_0" class ="epicContentDivs"></div>');
		    orderXml=getStoryOrderXml();
		  } 
		
		let jsonbody = {
			    "user_id":userIdglb,
			    "company_id":companyIdglb,
			    "project_id":projID,
			    "epic_title":storyName,
			    "epic_type":createEpicType,
				"parent_epic_id":epicId,
			    "localOffsetTime":localOffsetTime
		}   
		   
		$("#loadingBar").show();
		timerControl("start");
		   	$.ajax({
				url: apiPath+"/"+myk+"/v1/postAgile",
				type:"POST",
				contentType:"application/json",
				dataType:'json',
				data: JSON.stringify(jsonbody),
				///data:{action:"insertStory", epicName:storyName, epicId:epicId,projId:projID,projUserStatus:projUserStatus,loadType:loadType,parentEpicId:parentEpicId,orderXml:orderXml,epicType:createEpicType,localOffsetTime:localOffsetTime},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
		            
		            $('#rowTab_'+epicId).hide();
		            $("#subIdeaTextarea_"+epicId).val("");
		            
		            jsonResult = result;
					result = prepareAgileStoryUI(jsonResult);
					 
		            if(parentEpicId=='0' || parentEpicType == 'F' ){
		                $('div#epicSubContainerDiv_'+epicId).html('');
		                if($('div#epicSubContainerDiv_'+epicId).children().length==0){
						    if($('div#epic_'+epicId+' div#bandDiv_'+epicId).children('img.expandCollapseImg').length<1){
			                  var buttonData="<img id=\"epicImg_"+epicId+"\" title=\"Collapse\" class='expandCollapseImg' src=\"images/idea/minus.png\" onclick='loadStoriesForEpic(\"epicImg_"+epicId+"\","+epicId+")' />";
			                  $('div#epic_'+epicId+' div#bandDiv_'+epicId).append(buttonData);
			                }else{
			                  $('div#epic_'+epicId+' div#bandDiv_'+epicId).children('img.expandCollapseImg').attr('src','images/idea/minus.png');
			                }
			                
							$("div#epicSubContainerDiv_"+epicId).append(result);
					       
					    }else{
					      $('div#epicSubContainerDiv_'+epicId+' >div#epicContentDiv_0').replaceWith(result);
					    }
					}else{
					    
					    $('div#epicContentDiv_'+epicId).next('div#epicContentDiv_0').replaceWith(result);
					}  
		            console.log("aaa--->>>"+$('#epic_'+epicId).parents('div.epicContentMainDiv').attr('id').split('_')[1]);
		            loadAgileCustomLabel("Story");
					checkStatus();
					initEpicStoryDragDrop();
					//$('li#epic_'+epicId+' >ol >li').show();//--search
					getStatusCountForEpic(jsonResult);
		            $("#loadingBar").hide();
				    timerControl("");
				    //createJiraStory(storyName,projID,jsonResult[0].epicId);
				}
			});   
  }
 
  //------------------------------------------------------------------------// reorder functions //-------------------- 
 
 function showIdeaReorder(epicId){
      closeEpicPopup();
      showHideListDivs(epicId,"reorder");
      if($('#epic_'+epicId).find('div.ideaReorderDivs').length < 1){
          var storyLi="  <div id=\"epicReorderDiv_"+epicId+"\" class=\"ideaReorderDivs\" style=\"width:45px;border-radius:50px;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.6);border:1px solid #A1A1A1;position:absolute;display:none;height:45px;background-color:#ffffff;top:25px;right:50%;padding:5px;z-index:1;\" >";
			  storyLi+="    <div style=\"height:10px;width:10px;margin:0px auto;\"><img src=\""+path+"/images/idea/arrow-top.png\" onclick=\"reorder('top','"+epicId+"');\" style=\"cursor:pointer;height:10px;width:10px;float:left;\"/></div>";
			  storyLi+="    <div style=\"height:12px;\"><img src=\""+path+"/images/idea/arrow-left.png\" onclick=\"\" style=\"opacity:0.3;cursor:pointer;float:left;height:10px;width:10px;margin-top:2px;\"/><img src=\""+path+"/images/idea/arrow-right.png\" onclick=\"\" style=\"opacity:0.3;cursor:pointer;float:right;height:10px;width:10px;margin-top:2px;\"/></div>";
			  storyLi+="    <div style=\"height:10px;width:10px;margin:0px auto;\"><img src=\""+path+"/images/idea/arrow-down.png\" onclick=\"reorder('down','"+epicId+"');\" style='cursor:pointer;height:10px;width:10px;float:left;'/></div>";
			  storyLi+=" </div>";
			 
			 $('#epic_'+epicId).find('div.epicMainDiv').append(storyLi);
      }
      
      if ($('#epicReorderDiv_'+epicId).is(":hidden")) {
         $(".ideaReorderDivs").hide();
         $('#epicReorderDiv_'+epicId).show();
      }else{
         $('#epicReorderDiv_'+epicId).hide();
      }
 } 
  
 function reorder(position,epicId){
    var nextLi="";
    
	  if(position=="top"){
	      if($('#epic_'+epicId).parent().prevAll('div.epicContentDivs:visible').length){
			    nextLi=$('#epic_'+epicId).parent().prevAll('div.epicContentDivs:visible').attr('id').trim().split('_')[1];
			    if(nextLi!=='undefined'){
				      var liData=$('div#epicContentDiv_'+epicId).clone();
				      $('div#epicContentDiv_'+epicId).remove();
				      $(liData).insertBefore('div#epicContentDiv_'+nextLi);
				      updateOrder(epicId,nextLi);
				}
			}else{
		      alertFun(getValues(companyAlerts,"Alert_reorderNotPermissible"),'warning');
		    }
		    
	  }else if(position=="down"){
	      if($('#epic_'+epicId).parent().nextAll('div.epicContentDivs:visible').length){
		       nextLi=$('#epic_'+epicId).parent().nextAll('div.epicContentDivs:visible').attr('id').trim().split('_')[1];
			   if(nextLi!=='undefined'){
				      var liData=$('div#epicContentDiv_'+epicId).clone();
				      $('div#epicContentDiv_'+epicId).remove();
				      $(liData).insertAfter('div#epicContentDiv_'+nextLi);
				      updateOrder(epicId,nextLi);
			   }
		   }else{
		      alertFun(getValues(companyAlerts,"Alert_reorderNotPermissible"),'warning');
		    }
	 }
  
 } 
 
 function updateOrder(currentId,targetId){
             $("#loadingBar").show();
 			 timerControl("start");
 			 $.ajax({
 					url: path+"/agileActionNew.do",
 					type:"POST",
 					data:{action:"updateReorder",currentId:currentId,targetId:targetId  },
 					error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
 					success:function(result){
 					   checkSessionTimeOut(result);
 					   $("#loadingBar").hide();
 					   timerControl("");
 					}
 			});  
   }
 
 //--------------------------------------- delete functions --------------------------------->	

  function deleteEpicStory(epicId){
          confirmId=epicId;
		  confirmFun(getValues(companyAlerts,"Alert_delete"),"delete","confirmDeleteEpic");
 }
 

 function confirmDeleteEpic(){	
       closeEpicPopup();
       statusCountEpicId = $('#epic_'+confirmId).parents('div.epicContentMainDiv').attr('id').split('_')[1];
	   parEpicId = $('div#epic_'+confirmId).attr('parent_epic_id');
	   $('div#epicContentDiv_'+confirmId).remove();
	   if((parEpicId !='0') && ($('#epicSubContainerDiv_'+parEpicId).children().length < 1 )){
	      $('#bandDiv_'+parEpicId).find('img.expandCollapseImg').remove();
	   }
	   deleteEpic(confirmId);
  }
  
  function deleteEpic(epicId){ 
          var orderXml = getStoryOrderXml();
           //alert(orderXml);
		  $("#loadingBar").show();
		  timerControl("start");
		  $.ajax({
					   	url: path+"/agileActionNew.do",
						type:"POST",
						data:{action:"deleteEpic",epicID:epicId,orderXml:orderXml },
						error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
						},
						success:function(result){
				            checkSessionTimeOut(result);
				            if(parEpicId!= "0"){
				            	getStatusCountForEpic(statusCountEpicId); 
				            	updateEpicFeatureTotPoint(parEpicId);
				            }
				            $("#loadingBar").hide();
				            timerControl("");
						}
				});  
  }
 
 //---------------------------------epic and feature stages / expand all /  functions------------------- > 
 
 function gotoEpicFeatureStages(){
    $('#storyContentDiv').hide();
    $('#stageContentDiv').show();
    var projId = $('#proj_ID').val();
    var type = $('#stageSelectType option:selected').attr('value');
    $('#stageType').val(type);
    $('#stageProjId').val(projId);
    if(type=='E'){
     $('div.ef_stages_cLabelHtml').html(getValues(companyLabels,"Agile_Value"));
    }else{
     $('div.ef_stages_cLabelHtml').html(getValues(companyLabels,"Functionality"));
    }
    $("#loadingBar").show();
    timerControl("start");
   	$.ajax({
	   	url: path+"/agileActionNew.do",
		type:"POST",
		data:{action:"fetchEpicFeatureStages", projId:projId, type:type},
		error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
		success:function(result){
            checkSessionTimeOut(result);
            result = jQuery.parseJSON(result);
            $('#stageContainer').html(prepareStageUI(result));
            loadAgileCustomLabel('Stages_'+type);
            //$('.stageOptionsDiv').children('div:last').css('border','none');
            $("div#stageContainer").on("mouseover","div.actFeedHover",function(){  //----- to hide option popup
				if(!$(this).find('div.stageOptionsDiv').is(':visible')){
					$('div.actFeedHover').find('div.stageOptionsDiv').hide();
				}
			});
			$('.stageOptionsDiv').children('div:last').css('border-bottom','none');
            $("#loadingBar").hide();
   			timerControl("");
		}
	}); 
 } 
 
 function IdeaExpandColAll(obj){
     var imgSrc = $(obj).attr("src");
		 if( imgSrc.indexOf("expandAll.png") != -1 ){
		 	$(obj).attr("src", "images/collapseAll.png").addClass('Collapse_All_cLabelTitle').removeClass('Expand_All_cLabelTitle');
		 	var projId = $('#proj_ID').val();
		 	var projUserStatus = $("#projUserStatus").val(); 
		 	var sortVal = $("select[id=epicSortDropDown] option:selected").val();
		 	var searchTxt = $('#searchDiv').find('input.epicSearch').val().toLowerCase();
			var localOffsetTime=getTimeOffset(new Date());
	        let jsonbody = {
				"user_id":userIdglb,
				"project_id":projId,
				"company_id":companyIdglb,
				"sortVal":sortVal,
				"searchTxt":searchTxt,
				"localOffsetTime":localOffsetTime,
				"action":"loadEpicStories"
			}  
			$("#loadingBar").show();
			timerControl("start");
			$.ajax({
					url: apiPath+"/"+myk+"/v1/getEpics",
					type:"POST",
					dataType:'json',
					contentType:"application/json",
					data: JSON.stringify(jsonbody),
					//data:{action:"loadEpicStory",projId:projId,sortVal:sortVal,searchTxt:searchTxt},
					error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
					success:function(result){	
					    agileaction="loadEpicStory";
					    jsonResult = result;
						result = prepareAgileUI(jsonResult);
						$("div#epicContentMain").html(result);
					    loadAgileCustomLabel("Story");
					    if(sortVal!='Sort' || searchTxt!='' ){
					      searchFlag = true;
					      filterStoryOptionsOnSearch();
				   		}else{
				   		  searchFlag = false;
				   		  initEpicStoryDragDrop();
				   		}
					     
					    checkStatus();
					    $("#loadingBar").hide();
					    timerControl("");
					 }
		        });  
		 	
	     }else if( imgSrc.indexOf("collapseAll.png") != -1 ){
			 	$(obj).attr("src", path+"/images/expandAll.png").addClass('Expand_All_cLabelTitle').removeClass('Collapse_All_cLabelTitle');
			 	$('#epicContentMain >div.epicContentMainDiv').children('div[id^=epic_]').each(function(){
			 	    $('div#epicSubContainerDiv_'+$(this).attr('id').split('_')[1]).empty();
			 	    $(this).find('img.expandCollapseImg').attr('src', path+"/images/idea/plus.png");
			 	});
			 	loadAgileCustomLabel("Story");
	     }
	     
 }
 
 function gotoProject(){
     if(!$('#storyContentDiv').is(':visible')){
        showEpicData();
     }else{
        //parent.menuFrame.location.href = "${path}/colAuth?pAct=azMenu";
     }
  } 
 function showEpicData(){
	 $('#stageContentDiv').hide();
     $('#epicScrumboardDiv').hide();
	 //$('#hiddenSortVal').val('Sort');
	 //$('#epicSortDropDown').show();
	 $('#AgileStoryDashBoardDiv').hide();
	 $('#storyContentDiv').show();
	 $('li.rightImgDiv').show();
	 var projID=$("#proj_ID").val(); 
	 epicList(projID,'');
 }
 //---------------------------------------  task listing functions ---------------------------->
  var showIdeaTasksEpicId = '';
  function showIdeaTasks(obj,ideaId){
  	  showIdeaTasksEpicId = ideaId;	
  	  SB_task_viewType="Epic";
  	  storyIdForCal = ideaId;
      if($(obj).attr('src').indexOf("Toggle.png") == -1 ){
		  $(obj).attr('src', path+'/images/idea/task1Toggle.png');
		  fetchStoryTask(ideaId);
	  }else{
	     $('#epicListCommonDiv_'+ideaId).slideUp(function(){
	          $(obj).attr('src', path+'/images/idea/task1.png'); 
	          $('#epicListCommonDiv_'+ideaId).html('');
	     });
	  }
 }
 
 function fetchStoryTask(storyId){	 
     $("#loadingBar").show();
     timerControl("start");
    
     storyIdForCal = storyId; //storyIdForCal is declared globally and it is used to store epic Id value while updating the time in story task. 
     SB_task_viewType = "Epic";
     
     $.ajax({
			url: path+"/agileActionNew.do",
			type:"POST",
			data:{action:"listEpicTask",ideaID:storyId,viewPlace:"Epic",type:""},
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			success:function(result){
				checkSessionTimeOut(result);
				showHideListDivs(storyId,"task");
				//alert(result);
				$('#epicListCommonDiv_'+storyId).html(prepareTaskListUI(jQuery.parseJSON(result),"Epic"));
				
				loadAgileCustomLabel("TaskList");
				$('#epicListCommonDiv_'+storyId).slideDown(600);
				/*This condition is to hide the header of the task and task icon. When there is no any task to list.*/
				if($('#epicListCommonDiv_'+storyId).children('div').children('div').size() == 1){
					$('#epicListCommonDiv_'+storyId).css({'display':'none'});
			    	$('#taskIcon_'+storyId).remove();
		        }
				$("#loadingBar").hide();
				timerControl("");
		  }
	  });
 }
 
 //--------------------------------------- comments functions ---------------------------------->
 
 function showEpicComments(idd){
	  epicagileid=idd;
	  closeEpicPopup();
	  showHideListDivs(idd,'commentList');
	  if($('#epic_'+idd).find('#cmtImgDiv_'+idd).children('img').size()){
          $('#cmtIcon_'+idd).attr('src', 'images/idea/comment1Toggle.png');
       }else{
		 var imgData="<img id=\"cmtIcon_"+idd+"\"  style='cursor: pointer; float: right;margin-right: 3px;' class='Comments_cLabelTitle' src=\"images/idea/comment1Toggle.png\" onclick=\"showIdeaIconCmts('cmtIcon_"+idd+"',"+idd+");\" />";
		 $('#cmtImgDiv_'+idd).html(imgData);
       }
	  
	  $('div#epicListCommonDiv_'+idd).empty();
	  commentPlace='Story'; //--------used in js to decide in which tab adding/updating/deleting the comments.
      addComment(idd,'agile');	  
 }
 
 var pgId="";
 function showIdeaIconCmts(cmtId,idd){
	 pgId=idd;	
   if($("#"+cmtId).attr('src').indexOf("Toggle.png") == -1 ){
	   	   $("#"+cmtId).attr('src', 'images/idea/comment1Toggle.png');
		   $('#epicListCommonDiv_'+idd).show();
		   commentPlace='Story'; //--------used in js to decide in which tab adding/updating/deleting the comments.
		   var epiid = cmtId.split("_")[1];
		   fetchAgileComments(epiid);
  }else{
	  $('#epicListCommonDiv_'+idd).slideUp(function(){
          $("#"+cmtId).attr('src', 'images/idea/comment1.png');
          $(this).html('');
          $('div#comment_'+idd).empty().hide();
      });  
  }
  
 }

function fetchAgileComments(idd){
	//alert("fetchAgileComments idd::>>"+idd);
    $("#loadingBar").show();
    timerControl("start");
    fileSharingForComments("main_commentTextarea" , "0");
    $('#mainUpload').click(function(e){
    	 e.preventDefault();
	     e.stopPropagation();  
      	 $('#FileUpload').trigger('click');
	});
    var localOffsetTime=getTimeOffset(new Date());
	let jsonbody = {
		"menuTypeId":idd,
		"project_id":prjid,
		"feedId":"0", ////   in case of create 0 in case of update the activity_feed_id
		"localOffsetTime":localOffsetTime,
		"feedType":menuutype,
		"subMenuType":commentPlace
	}
	$.ajax({
	   	url: apiPath+"/"+myk+"/v1/getFeedDetails",
		type:"POST",
		dataType:'json',
		contentType:"application/json",
		data: JSON.stringify(jsonbody),
		//data:{action:"fetchComments", epicID:idd, subMenuType:commentPlace, localOffsetTime:localOffsetTime },
		error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
		success:function(result){
			//alert(" fetchAgileComments result::::===="+result);
             ///checkSessionTimeOut(result);
             showHideListDivs(idd,"commentList");
             $('#epicListCommonDiv_'+idd).append(prepareCommentsUI(result,"agile",idd));//-----------------..>>>>
             loadAgileCustomLabel("fetchComments");
             $('#epicListCommonDiv_'+idd).on("mouseover","div.actFeedHover",function(){  //----- to hide option popup
				if(!$(this).find('div.actFeedOptionsDiv').is(':visible')){
					$('div.actFeedHover').find('div.actFeedOptionsDiv').hide();
				}
		     });
		     if(agileNotificationType != ""){  
				$("#actFeedTot_"+epicCmtDocId).addClass("notHightlightCls");
				 $('html, body').animate({
        			scrollTop:$('#actFeedTot_'+epicCmtDocId).offset().top - $('#storyContentDiv').offset().top - $('#rowTab').height() - 15
   						 }, 600);
				agileNotificationType="";
			 }
           $("#loadingBar").hide();
           timerControl("");
		}
  });

}
//---------------- epic scrumboard functions ---------------------------------------------->

 function gotoEpicScrumboard(){
    $('div.contentMainDiv').hide();
    $('#epicScrumboardDiv').show();
    $('.workSpace_arrow_right').css({'margin-right':'-21px'});
    var projId = $('#proj_ID').val();
    $("#loadingBar").show();
    timerControl("start");
   	$.ajax({
	   	url: path+"/agileActionNew.do",
		type:"POST",
		data:{action:"fetchEpicScrumData", projId:projId},
		error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
		success:function(result){
            checkSessionTimeOut(result);
            $('#epicScrumboardContainer').html(result);
            removePrevNext();
            initDragAndDrop();
            findStoryCount();
            setHeightToTR();
            loadAgileCustomLabel('ScrumBoard');
            showEpicFeaturesOnclick();
            $('table#SB_table').find('p[id^=sbStory_]').removeAttr('onclick');
            if(!isiPad){
               popXYScroll('groupSprintContainer');
	        }
            //$('.epicSearch').val("");
            $("#loadingBar").hide();
   			timerControl("");
		}
	});
 }

  function showSBEpicStatusPopup(obj,epicId){
     if($('#epicStatusPopDiv_'+epicId).is(':hidden')){
       $('div.storyStatusPopDiv').hide();
       $('#epicStatusPopDiv_'+epicId).slideDown(function(){
         $(this).css('overflow','');
       });
	   //$('div#groupSprintContainer div.mCS_no_scrollbar_y').css('height','100%');
	 }else{
       $('#epicStatusPopDiv_'+epicId).slideUp();
     }
 }
 
 function changeSBEpicStatus(obj,epicStatus,epicId){
       confirmStoryStatus=epicStatus;
       confirmId=epicId;
       confirmObj=obj;
      if(epicStatus=='Backlog'){
        //parent.confirmFun("<bean:message key='Agile_deassociate.message'/>","clear","confirmSBUpdateStoryStatus");
        //parent.confirmFun(getValues(companyAlerts,"Alert_deassociate"),"clear","confirmSBUpdateStoryStatus");
      }else{
        confirmSBUpdateEpicStatus();
      }
 }
  function confirmSBUpdateEpicStatus(){
      $('.storyStatusPopDiv,.stageOptionCss').hide();
      $('#addCommentPopDiv').show();
      $('#transparentDiv').css('display','block');
 	  $('#addCommentPopDiv').find('#savePopComment,#popCommentSave').attr('onclick','saveUpdateSBEpicStatus();');
 	  $('#addCommentPopDiv').find('.popUpCloseIcon,.popCommentCancel').attr('onclick','cancelUpdateEpicStatus();');
 	  
 }
 
  function saveUpdateSBEpicStatus(){	
      var comment=$('#popCommentTextArea').val().trim();
      $('#addCommentPopDiv').hide();
      $('#transparentDiv').hide();  
 	  
 	  var epicStatus=confirmStoryStatus;
	  var epicId=confirmId;
	  var imgSrc=(epicStatus=='Backlog')? path+"/images/idea/Backlog.png" : (epicStatus=='In Progress')? path+"/images/idea/Assigned.png" : (epicStatus=='Blocked')? path+"/images/idea/Blocked.png" : (epicStatus=='Hold')? path+"/images/idea/Hold.png" : (epicStatus=='Done')? path+"/images/idea/Done.png" : path+"/images/idea/cancel.png"
	  $('#SB_story_'+epicId).find('img.storyStatusPopIcon').attr('src',imgSrc).attr('title',epicStatus);
	 
	  $("#loadingBar").show();
	  timerControl("start");
       $.ajax({
			   	url: path +"/agileActionNew.do",
				type:"POST",
				data:{action:"updateEpicStatus",epicStatus:epicStatus,epicID:epicId ,epicComment:comment},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
		            checkSessionTimeOut(result);
		            $('#popCommentTextArea').val("");
		            $("#loadingBar").hide();
		            timerControl("");
				}
		});
 } 
 
 function exportToExcel(){
    var projectId = $('#proj_ID').val();
    var url = '';
    url = path+"/agileActionNew.do?action=ExportData&projectId="+projectId+"";
	window.open(url);
 }
 
//------------------------------------------------------------// epic and story custom id functions //----------------------------
 
 function AddCustomId(epic_id){
    $('#customIdSpan_'+epic_id).hide();
    $('#customIdInput_'+epic_id).val($('#customIdSpan_'+epic_id).attr('title')).show().focus();
  }
  
  function setCustomId(event,epic_id){
       var code = null;
   		code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
   		var folderName = event.target || event.srcElement;
   		if (code == 13) {
   		          
           var custId=$('#customIdInput_'+epic_id).val().trim();
   		   if(custId==''){
   		       confirmId=epic_id;
   		       confirmReset(getValues(companyAlerts,"Alert_Idempty"),'reset','customIdConfirm','customIdExit');
   		       return false;
   		    }
   		       
	       var cId=$('#customIdInput_'+epic_id).val().trim();
	       var regex = new RegExp("^[a-zA-Z0-9-:_]+$");
		       
           if(!regex.test(cId)) {
              alertFun(getValues(companyAlerts,"Alert_specialCharacter"),'warning');
		       return false;
           }
           saveEpicStoryCustomId(epic_id,cId);
	 }
  }
	
  function saveEpicStoryCustomId(epic_id,customId){	
            $("#loadingBar").show();
 		    timerControl("start");
 		    var projId=$('#proj_ID').val();
 		    var epicType = $('#epic_'+epic_id).attr('type');
			let jsonbody = {
				"user_id":userIdglb,
				"company_id":companyIdglb,
				"epic_id":epic_id,
				"project_id":projId,
				"epic_type":epicType,
				"unique_cust_id":customId,
			}
	        $.ajax({
				url: apiPath+"/"+myk+"/v1/updateCustomId",
				type:"PUT",
				contentType:"application/json",
				data: JSON.stringify(jsonbody),
				//data:{action:"updateCustomId",epicID:epic_id,customId:customId,projId:projId,epicType:epicType},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
					
				    if(result=="faliure"){
				        if(epicType =="S"){
				           alertFun(getValues(companyAlerts,"Alert_Idexist"),'error');
				        }else if(epicType =="E"){
				           alertFun(getValues(companyAlerts,"Alert_EIdexist"),'error');
				        }else if(epicType =="F"){
				           alertFun(getValues(companyAlerts,"Alert_FIdexist"),'error');
				        }
				        
				    }else{
				        $('#customIdInput_'+epic_id).hide();
				        $('#customIdSpan_'+epic_id).attr('title',customId).text(customId).show();
				        var searchText=$('#editIdea_'+epic_id).val().trim().toLowerCase()+""+customId.toLowerCase();  
				        $('#epic_'+epic_id).children('span.hidden_searchName').text(searchText);
				    }
					$("#loadingBar").hide();
					timerControl("");
			  }
		  });  
  }

 function customIdExit(){
	  var storyId=confirmId;
	  $('#customIdInput_'+storyId).hide();
	  $('#customIdSpan_'+storyId).show();
 } 
 
 function customIdConfirm(){
    var cId=$('#customIdInput_'+confirmId).val().trim();
    saveEpicStoryCustomId(confirmId,cId);
 } 
  

//------------------------------------------------------------------------// edit & update of story and epic functions //-------------------- 
 var BodyClickEpicID = '';
    function editEpic(obj){
          cancelEditEpic(BodyClickEpicID);
 		  var idd=$(obj).attr('id').split('_')[1];
 		  BodyClickEpicID = idd;//-------used for body click event function
 		  if($('#epicTitle_'+idd).children('div.main_epicTxtArea_btnDiv').length < 1){
 		        var place = $(obj).attr('place');
	            var html="   <div align=\"center\" class=\"main_epicTxtArea_btnDiv\" style=\"width: 3%; height: 36px; margin: 3px 0px;\">"
		                +"     <div><img class=\"img-responsive epicSaveImg EditSave\" title=\"post\" onclick=\"updateEpic("+idd+");\" src=\"images/workspace/post.png\" style=\"margin-top: 6px; width: 70%;\"></div>"
		                +"     <input type='hidden' id=\"updatePlace_"+idd+"\" value=\""+place+"\"/>"
		        		+"   </div>";			
				
				$('#epicTitle_'+idd).append(html);
	      }
	      loadAgileCustomLabel("Story");
 		  $('#editIdea_'+idd).removeAttr('readonly').addClass('epicContent_EditDiv').focus();
 		  $('#epicTitle_'+idd+' .epicMoreImgDiv').hide();
 		  $('#epicTitle_'+idd+' .main_epicTxtArea_btnDiv').show();
 		  showHideListDivs(idd,"editEpic");
 		  
  }

  function cancelEditEpic(epicId){
     if(epicId !=''){
 		 var original_title=$('#hiddenTitle_'+epicId).val();
 		 var title=$('#editIdea_'+epicId).val();
 		 if(title==original_title){
 		     $('#editIdea_'+epicId).attr('readonly','true').removeClass('epicContent_EditDiv').val(original_title);
	 		 $('#epicTitle_'+epicId+' .epicMoreImgDiv').show();
	 		 $('#epicTitle_'+epicId+' .main_epicTxtArea_btnDiv').hide();
	 		 BodyClickEpicID = '';
	 	}
	 }
 }	
 var updatepepicid="";
 function updateEpic(epicId){
 		    var epicName = $('#editIdea_'+epicId).val().trim();
 		    var localOffsetTime=getTimeOffset(new Date());
			var place=$('#updatePlace_'+epicId).val();
			var etype="";
 			if(epicName==''|| epicName==null){
 			            if(place=='Epic'){
 			              alertFun(getValues(companyAlerts,"Alert_EpicEmpty"),'warning');
 			            }else  if(place=='Story'){
 			              alertFun(getValues(companyAlerts,"Alert_StoryEmpty"),'warning');
 			            }else  if(place=='Feature'){
 			              alertFun(getValues(companyAlerts,"Alert_FeatureEmpty"),'warning');
 			            }
 			            $('#editIdea_'+epicId).focus();
 						return false;		
 			}
 			if(epicName.length>500){
 			    alertFun(getValues(companyAlerts,"Alert_ideaNameExceed"),'warning');
				$('#editIdea_'+epicId).focus();
				return false;		
			}
			var parentepicid="";	
			if(place=='Epic'){
				etype="E";
			}else  if(place=='Story'){
				etype="S";
				parentepicid=$('#epicContentDiv_'+epicId).parents('div.agileSubDivs').attr('id').split('_')[1];;
			}else  if(place=='Feature'){
				etype="F";
				parentepicid=$('#epic_'+epicId).parents('div.epicContentMainDiv').attr('id').split('_')[1];
			}
			let jsonbody = {
			    "user_id":userIdglb,
			    "company_id":companyIdglb,
			    "project_id":prjid,
			    "epic_title":epicName,
			    "epic_type":etype,
			    "localOffsetTime":localOffsetTime,
				"epic_id":epicId,
				"parent_epic_id":parentepicid
			} 
 		    $("#loadingBar").show();
 		    timerControl("start");
 		       $.ajax({
 					   	url: apiPath+"/"+myk+"/v1/updateAgile",
 						type:"PUT",
						contentType:"application/json",
						data: JSON.stringify(jsonbody),
 						//data:{action:"updateEpic",epicName:epicName,epicId:epicId,localOffsetTime:localOffsetTime },
 						error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
						},
 						success:function(result){
 							//console.log("updateepic:"+result);
							jsonResult = result;
							
 				            $('#hiddenTitle_'+epicId).val(epicName);
 				            $('#epic_'+epicId+" > span.hidden_searchName").text(epicName.toLowerCase()+""+$('#customIdSpan_'+epicId).text().toLowerCase());
 				            for(var i=0;i<jsonResult.length;i++){
								if(jsonResult[i].epic_id == epicId){
									var html = "<p class = \"defaultNameDateTimestamp\" style = \"margin: 0px !important\">"+getValues(companyLabels,"Created_by")+" "+jsonResult[i].createdBy+" /"+jsonResult[i].created_time+"&nbsp;&nbsp; "+getValues(companyLabels,"Modified_By")+" "+jsonResult[i].updatedBy+" /"+jsonResult[i].updated_time+"";
									$('#epicDetails_'+epicId).children("p.defaultNameDateTimestamp").html(html); 	
								} 
							}
							cancelEditEpic(epicId);
 				            $("#loadingBar").hide();
 				            timerControl("");
 						}
 				});
  }
 
//----------------------------------------- share related functions ---------------------------->

 
  function showCollaborators(obj,type,ideaId){
  
 		if($(obj).attr('src').indexOf("Toggle.png") == -1 ){
 			     $(obj).attr('src', path+'/images/idea/shareToggle.png');
 			     var projId = $('#proj_ID').val();
 			     $("#loadingBar").show();
 			     timerControl("start");
 			     $.ajax({
 						url: apiPath+"/"+myk+"/v1/fetchContributor?epic_id="+ideaId+"&project_id="+prjid+"",
 						type:"GET",
 						///data:{action:"listEpicCollaborators",ideaID:ideaId,type:type,projId:projId},
 						error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
						},
 						success:function(result){
 							
 							showHideListDivs(ideaId,"collaborators");
 							$('#epicListCommonDiv_'+ideaId).html(prepareCollaboratorsUI(result));
 							loadAgileCustomLabel("EpicCollaborators");
 							$('#epicListCommonDiv_'+ideaId).slideDown(600);
 		                    $("#loadingBar").hide();
 							timerControl("");
 					  }
 				  });
		  }else{
		     $('#epicListCommonDiv_'+ideaId).slideUp(function(){
		          $(obj).attr('src', 'images/idea/share1.png'); 
		          $(this).html('');
		     });
		  }
 }
 
 function prepareCollaboratorsUI(result){
    var agileUI = '';
	var ImgUrl = "";
   if(result){
		var json = eval(result);
		for(var i = 0; i < json.length; i++){
		   ImgUrl = lighttpdpath+"/userimages/"+json[i].imageUrl;
		   shareTypeImg = json[i].shared_type == "R" ? "images/repository/read.png" : "images/repository/write.png";
		   userRole = json[i].proj_user_status == "PO" ? "Project Manager" : json[i].proj_user_status == "TM" ? "Team Member" : "Subscribed User";
		   
		   agileUI+="<div id=\"\" class=\"col-lg-12 defaultPaddingCls \" >"
		           +"   <div class=\"docListViewBorderCls\" style=\"padding-top: 4px;font-size:12px;margin:0px;\">"
		           +"      <div class=\"tabContentHeaderName\" align=\"center\" style=\"width:7%;float: left;\"><img onerror=\"javascript:userImageOnErrorReplace(this);\" class=\"teamUserImg\" title=\""+json[i].name+"\" src=\""+ImgUrl+"\" style=\"width:40px;height:40px;padding:3px;float:left;border-radius:50%;\" /></div>"
			       +"      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:24%;margin-top: 8px;float:left;\" ><div class=\"TeamStyle\" style=\" text-align:left; \" ><b>"+json[i].name+"</b></div></div>"
	         	   +"      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:4%;margin-top: 8px;float:left;\" ><div class=\"TeamStyle\" align=\"center\" ><img width=\"16\" height=\"16\" src=\""+shareTypeImg+"\" /></div></div>"
	        	   +"      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:15%;margin-top: 8px;float:left;\"><div class=\"TeamStyle\" align=\"left\"  >"+userRole+"</div></div>"
			       +"      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:13%;float:left;\"><div class=\"TeamStyle User_Mobile_cLabelHtml\" style=\"text-align:left;height:20px;overflow:hidden;\" ></div><div style=\"text-align:left;\">"+json[i].user_phone+"</div></div>"
				   +"      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:14%;float:left;\"><div class=\"TeamStyle Home_cLabelHtml\" style=\"text-align:left;height:20px;overflow:hidden;\" ></div><div style=\"text-align:left;\">"+json[i].work_number+"</div></div>"
				   +"      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:23%;float:left;\"><div class=\"TeamStyle Email_cLabelHtml\" style=\"text-align:left;height:20px;overflow:hidden;\" ></div><div style=\"text-align:left;\">"+json[i].user_email+"</div></div>"
				   +"  </div>" 
		           +"</div>";
		}
   }
   return agileUI;
 } 
//------------------------------------------------------------// story add And update points functions //----------------------------

 function AddPointsToStory(epic_id){
    closeEpicPopup();
    if($('#epicPointDiv_'+epic_id).children('input#epicPointEdit_'+epic_id).length < 1){
      var html = '<input type="text" value="" style="display:none;width: 70px;padding:0 5px; float: left; margin-top: -2px; border: 1px solid rgb(204, 204, 204);border-radius: 2px; height: 19px;" onkeydown="setStoryPoint(event,'+epic_id+');" maxlength="3" id="epicPointEdit_'+epic_id+'" class="epicPointClass">'; 
      $('#epicPointDiv_'+epic_id).append(html);
    }
    $('#epicPointDiv_'+epic_id).show();
    $('#epicPointValSpan_'+epic_id).hide();
    $('#epicPointEdit_'+epic_id).val($('#epicPointValSpan_'+epic_id).attr('title')).focus();
    $('#epicPointDiv_'+epic_id+' .epicPointClass').show();
    $('#epicPointImg_'+epic_id).show();
 }

 function pointContinue(){
	   var epic_id = confirmId;
	   var point="0";
	   $("#loadingBar").show();
	   timerControl("start");
	     $.ajax({
				url: path+"/agileActionNew.do",
				type:"POST",
				data:{action:"updatePoint",epicID:epic_id,epicPoint:point},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
					checkSessionTimeOut(result);
					if(point=='0'){
					  $('#epicPointValSpan_'+epic_id).removeAttr('class').text("").attr("title","");
					  $('#epicPointImg_'+epic_id).hide();
					}else{
						if($('#epicPointValSpan_'+epic_id).text().trim()==''){
						   $('#epicPointValSpan_'+epic_id).addClass('epicPointSpan').attr('onclick','AddPointsToStory("'+epic_id+'")');
						}
						$('#epicPointValSpan_'+epic_id).text(point);
						$('#epicPointImg_'+epic_id).show();
					}
					$('#epicPointDiv_'+epic_id+' .epicPointClass').hide();
 					$('#epicPointValSpan_'+epic_id).show();
 					
					$("#loadingBar").hide();
					timerControl("");
			  }
		  }); 
 }
 
 function pointExit(){
	  var storyId = confirmId;
	  $('#epicPointDiv_'+storyId+' .epicPointClass').hide();
	  $('#epicPointValSpan_'+storyId).show();
	  if($('#epicPointValSpan_'+storyId).text().trim()==''){
	    $('#epicPointImg_'+storyId).hide();
	  }
 } 
 
 function setStoryPoint(event,epic_id){
        var code = null;
   		code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
   		var folderName = event.target || event.srcElement;
   		if (code == 13) {
   		   keyDwn="false";
   		    
		      var point=$('#epicPointEdit_'+epic_id).val().trim();
		   		   if(point==''){
		   		        confirmReset(getValues(companyAlerts,"Alert_EmyPtCont"),'reset','pointContinue','pointExit');
		   		        confirmId = epic_id;
		   		        return false;
		   		        //$('#pointStoryId').val(epic_id);
		   		       
		   		   }else{
		   		       
		   		       var point=$('#epicPointEdit_'+epic_id).val();
		   		       var intRegex = /^\d+$/;
		               var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
		
		               var str = $('#epicPointEdit_'+epic_id).val();
		               if(intRegex.test(str) || floatRegex.test(str)) {
		                
		   		        $("#loadingBar").show();
		   		        timerControl("start");
		 			     $.ajax({
		 						url: path+"/agileActionNew.do",
		 						type:"POST",
		 						data:{action:"updatePoint",epicID:epic_id,epicPoint:point},
		 						error: function(jqXHR, textStatus, errorThrown) {
					                checkError(jqXHR,textStatus,errorThrown);
					                $("#loadingBar").hide();
									timerControl("");
								},
		 						success:function(result){
		 							checkSessionTimeOut(result);
		 							if(parseInt(point)== 0){
		 							  $('#epicPointValSpan_'+epic_id).removeAttr('class').text("");
		 							  $('#epicPointImg_'+epic_id).hide();
		 							}else{
			 							if($('#epicPointValSpan_'+epic_id).text().trim()==''){
			 							   $('#epicPointValSpan_'+epic_id).addClass('epicPointSpan').attr('onclick','AddPointsToStory("'+epic_id+'")');
			 							}
			 							$('#epicPointValSpan_'+epic_id).attr('title',point);
			 							if(point.length>10){
			 							 point=point.substring(0,10)+' ..';
			 							}
			 							$('#epicPointValSpan_'+epic_id).text(point);
			 							$('#epicPointImg_'+epic_id).show();
		 	 						}
		 	 						$('#epicPointDiv_'+epic_id+' .epicPointClass').hide();
			 	 					$('#epicPointValSpan_'+epic_id).show();
			 	 					var pId = $('#epic_'+epic_id).attr('parent_epic_id');
			 	 					updateEpicFeatureTotPoint(pId);
		 							$("#loadingBar").hide();
		 							timerControl("");
		 					  }
		 				  });   
		   		   }else{
		   		       alertFun(getValues(companyAlerts,"Alert_numericValues"),'warning');
		   		       $('#epicPointValSpan_'+epic_id).hide();
		    		   $('#epicPointDiv_'+epic_id+' .epicPointClass').show();
		    		   $('#epicPointEdit_'+epic_id).focus();
		    		   keyDwn = true;
		   		    }
		   }
		}
  }  
  
 function updateEpicFeatureTotPoint(pId){
   //var pId = $('#epic_'+storyId).attr('parent_epic_id');
   var totPoint=0;
   if(pId != '0'){
     $('#epicSubContainerDiv_'+pId).children('div[id^=epicContentDiv_]').each(function(){
        var sId = $(this).attr('id').split('_')[1];
        var point = $(this).find('span#epicPointValSpan_'+sId).text().trim();
        //alert('point:'+point);
        if(point!=''){
          totPoint = parseInt(totPoint) + parseInt(point);
        }
     });
     //alert('totPoint:'+totPoint);
     if(totPoint!=0){
        if($('#epicPointValSpan_'+pId).text().trim()==''){
		   $('#epicPointValSpan_'+pId).addClass('epicPointSpan').css('border','none');
		}
		$('#epicPointValSpan_'+pId).attr('title',totPoint);
		if(totPoint.length>10){
		 totPoint=totPoint.substring(0,10)+' ..';
		}
		$('#epicPointDiv_'+pId).show();
		$('#epicPointValSpan_'+pId).text(totPoint);
		$('#epicPointImg_'+pId).show();
     }else{
       $('#epicPointValSpan_'+pId).removeAttr('class').text("");
       $('#epicPointImg_'+pId).hide();
     } 
     //------> for the epic level
      var parentId = $('#epic_'+pId).attr('parent_epic_id');
      updateEpicFeatureTotPoint(parentId);
    }
  }
 
 //------------------------------------------------------------------------// collaborators functions //-------------------- 
  function shareIdea(obj){
          var idd=$(obj).attr('id').split('_')[1];
 		  closeEpicPopup();
 		  var ideaDesc=$('#editIdea_'+idd).val();
 		  if(ideaDesc.length>135){
 		     ideaDesc=ideaDesc.substring(0,132)+'...';
 		  }
 		  var projId = $('#proj_ID').val();
 		  var mainEpicId = $('#epic_'+idd).attr('parent_epic_id');
 		  var ideaImgSrc = $('.breadcrumContent img').attr('src');
 		  //$('#shareImg').attr('src',ideaImgSrc);
          $('#shareIdeaDesc').attr('title',$('#editIdea_'+idd).val()).html(ideaDesc);
          $('#shareIdeaID').val(idd);
 		  
 		  $("#loadingBar").show();
 		  timerControl("start");
       $.ajax({
			   	url: path+"/agileActionNew.do",
				type:"POST",
				data:{action:"fetchShareIdeaUI",epicId:idd,mainEpicId:mainEpicId,projId:projId },
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
					   checkSessionTimeOut(result);
					   loadAgileCustomLabel("EpicShare");
					   prepareCollaboratorsUpdateUI(jQuery.parseJSON(result));
					   $('#ideaSharePopUp').show();
					   $('#transparentDiv').show();
					   if(!isiPad){
                         popScroll('collaboratorsListDiv');
                         $('div#collaboratorsListDiv .mCSB_container').css('margin-right','0px');
					   } 
					   
				/*
		           parent.checkSessionTimeOut(result);
		           $('#ideaSharePopUp').html(result);//-----------------..>>>>
		           loadAgileCustomLabel("EpicShare");
                      $('#ideaSharePopUp').css('display','block');
                      $('#ideaTransparent').css('display','block');
                      parent.$('#transparentDiv').css('display','block');
                      $('#shareImg').attr('src',ideaImgSrc);
                      $('#shareIdeaDesc').attr('title',$('#editIdea_'+idd).val()).html(ideaDesc);
                      $('#shareIdeaID').val(idd);
                      initIdeaShareDropDown();
                      if(!isiPad){
                        popScroll('ideaSharePraticpantsDiv');
                        $('div#ideaSharePraticpantsDiv .mCSB_container').css('margin-right','10px');

                      }  
                      
                      var landH=$('#ideaSharePopUp').height();
			    var mainH=parent.$('#menuFrame').height();
			    if(mainH>landH){
				   //alert('1st');
				}else{
				  $('#ideaSharePopUp').css({'width':'820px','margin-left':'-410px','top':'0%','margin-top':'0px','height':mainH-10+'px'});
				  $('#taskUserListContainer').css('float','left');
				  popScroll('ideaSharePopUp');
				}
                      */
		           $("#loadingBar").hide();
		           timerControl("");
				}
		});
    } 
  
 function prepareCollaboratorsUpdateUI(result){
    var agileUI = '';
   if(result){
		var json = eval(result);
		for(var i = 0; i < json.length; i++){
		
		   //shareTypeImg = json[i].sharedAccess == "R" ? path+"/images/repository/read.png" : path+"/images/repository/write.png";
		   agileUI='<div style="float:left;width:100%;border-bottom: 1px solid rgb(193, 197, 200);font-size:12px;" id="userDetails_'+json[i].sharedUserId+'">'
        	       +'	<div id="userImage" style="float: left;">'
        	       +'	 <img onerror="userImageOnErrorReplace(this);" title=\"'+json[i].sharedWith+'\" style="height:30px;width:30px;margin:10px;border-radius:50%;" src="'+json[i].ImgUrl+'">'
        	       +'	</div>'
        	       +'	<div id="userName" style="float: left; margin-top: 15px;">'+json[i].sharedWith+'</div>'
        	   	   +'  <div  id="" style="float: right;margin-top: 15px;margin-right: 2px;">';
        	   	   if(json[i].permission=="R")
        	   	     agileUI+='    <img id="readCheck_'+json[i].sharedUserId+'" class="userAccessImg" accessType="R" onclick="userSharedType(this);"  src="'+path+'/images/read.png" style="cursor:pointer;">'
        	       else
        	         agileUI+='    <img id="readCheck_'+json[i].sharedUserId+'" class="userAccessImg" accessType="W" onclick="userSharedType(this);"  src="'+path+'/images/write.png" style="cursor:pointer;">'
        	       agileUI+='  </div>'
            	   +'</div>';
          if(json[i].userRole=='PO'){
            $('#sharedAdminsDiv').append(agileUI);
          }else if(json[i].userRole=='TM'){
            $('#sharedTeamMembersDiv').append(agileUI);
          }else{
            $('#sharedObserversDiv').append(agileUI);
          }
	  }
   }
   return agileUI;
 }
 
  function userSharedType(obj){
    var selectId=$(obj).attr('accessType');
    if(selectId=='R'){
      $(obj).attr('src', path+'/images/write.png');
      $(obj).attr('accessType','W');
     }else{
      $(obj).attr('src', path+'/images/read.png');
      $(obj).attr('accessType','R');
    }
 }
 
  function saveEpicShare(){
          var userShareType = getUserShareType();	
	 	  var projID = $("#proj_ID").val();
	 	  var epicId = $('#shareIdeaID').val();
	 	  var mainEpicId=$('li#epic_'+epicId).attr('parent_epic_id');
	 	  
	 	  $("#loadingBar").show(); 
	 	  timerControl("start");
	 	  
	 	 $.ajax({
		    url: path +"/agileActionNew.do",
		    type: "POST",
		    data: { action: "shareIdea",epicId:epicId,projId:projID,mainEpicId:mainEpicId,userShareType:userShareType },
		    error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
		    success: function (result) {
			       checkSessionTimeOut(result);
			       $('#epicContentDiv_'+epicId).replaceWith(prepareAgileUI(jQuery.parseJSON(result)));
			       
			       cancelSharePopup();
			       checkStatus();
			       loadAgileCustomLabel("Story");
			   	   filterIdeas($('#epicSearch'));//-search
			       $("#loadingBar").hide();
			       timerControl("");
		    }
		});   
 }
 
 function getUserShareType(){
   var userShareType="";
    
   $('div.epicCollaboratorDivs').children('div[id^=userDetails_]').each(function(){
     var userId = $(this).attr('id').split("_")[1];
     var shareType = $(this).find('.userAccessImg').attr('accesstype');
     userShareType = userShareType+userId+":"+shareType+","
   });
   
   return userShareType;
 }
 
 function cancelSharePopup(){
    $('#ideaSharePopUp').hide();
	$('#transparentDiv').hide();
	$('#ideaTransparent').hide();
    $('#sharedAdminsDiv, #sharedTeamMembersDiv, #sharedObserversDiv').html('');
 }
 
 //------------------------------------------------------------> Dashboard function start here ------------------------------->
 
  function loadCompStoryDashBoard(){
    $('#loadingBar').show();
  	timerControl("start");
    $("#storyContentDiv").hide();
    $('#stageContentDiv').hide();
    $('#epicScrumboardDiv').hide();
    $("#AgileStoryDashBoardDiv").show();
    $('li.rightImgDiv').hide();
    $('#storyPrev').parent('li').show();
    loadOverallTaskProgress();
    loadProjectDataList();
    loadprojectStoryStatusData();
    loadprojectTaskStatus();
    loadSprintGroupStatus();
    loadWorkLoad();
    loadTaskPerUser();
   
  }
 
 //---------------------------------------------------// search and sort functions //-----------------------------------------------
 
 function showStorySearchNsort(){
    if($('#storyContentDiv').is(':visible')){
    	$('#epicSearchSortDiv').find('#sortDiv').show();
    	$('#epicSearchSortDiv').find('.workSpace_arrow_right').css('margin-top','15px');
        $('#epicSearchSortDiv').addClass('epicSearchSortCss').removeClass('epicSBSearchCss');
        /*$('#epicSearchSortDiv').find('#searchDiv').css('margin-top','0px');*/
    }else{
        $('#epicSearchSortDiv').find('#sortDiv').hide();
        $('#epicSearchSortDiv').find('.workSpace_arrow_right').css('margin-top','-2px');
        $('#epicSearchSortDiv').find('#searchDiv').css('margin-top','3px');
        $('#epicSearchSortDiv').addClass('epicSBSearchCss').removeClass('epicSearchSortCss');
    }
	$('#epicSearchSortDiv').slideToggle(function(){
	  $(this).css('overflow','');
	});
 }
	
 function getEpicSortValue(){
	var sortVal=$("select[id=epicSortDropDown] option:selected").val();
	var kw = $('#searchDiv').find('input.epicSearch').val().toLowerCase();
	showEpicSearchSort(sortVal,kw);
 }
 
 var searchFlag = false;
 function showEpicSearchSort(sortVal,searchTxt){
    var projId = $('#proj_ID').val();
    var projUserStatus = $("#projUserStatus").val(); 
 	$("#loadingBar").show();
	timerControl("start");
	if(sortVal!='Sort' || searchTxt!='' ){
		agileaction = 'loadEpicStory';
	}else{
		agileaction = 'loadEpics';
	} 
	var localOffsetTime=getTimeOffset(new Date());
	let jsonbody = {
		"user_id":userIdglb,
		"project_id":prjid,
		"company_id":companyIdglb,
		"sortVal":sortVal,
		"searchTxt":searchTxt,
		"localOffsetTime":localOffsetTime,
		"action":"loadEpicStories"
	}  
    $.ajax({
			url: apiPath+"/"+myk+"/v1/getEpics",
			type:"POST",
			dataType:'json',
			contentType:"application/json",
			data: JSON.stringify(jsonbody),
			///data:{action:act,projId:projId,projUserStatus:projUserStatus,sortVal:sortVal,searchTxt:searchTxt},
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			success:function(result){	
			    
			    jsonResult = result;
				result = prepareAgileUI(jsonResult);
				if($.trim(result)=="")
					result="Result Not Found";
				$("div#epicContentMain").html(result);
			    loadAgileCustomLabel("Story");
			    checkStatus();
			    if(sortVal!='Sort' || searchTxt!='' ){
			      searchFlag = true;
			      filterStoryOptionsOnSearch(); 
		   		}else{
		   		  searchFlag = false;
		   		  initEpicStoryDragDrop();
		   		}
			    $("#loadingBar").hide();
			    timerControl("");
			 }
    		});
  }
	
 function filterStoryOptionsOnSearch(){
        if(searchFlag){
		    $('div#epicContentMain').find('div[id^=epic_]').each(function(){
		   		    var li_id=$(this).attr('id').split('_')[1];
			        $('#reorder_'+li_id).removeAttr('onmouseover').removeAttr('onclick');
					$('#reorder_'+li_id).find('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
					$('#reorder_'+li_id).addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
					$('#createSubIdea_'+li_id).removeAttr('onmouseover').removeAttr('onclick');
					$('#createSubIdea_'+li_id).find('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
					$('#createSubIdea_'+li_id).addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
	   		});
	   		$('div#epicContentMain').find('div[id^=epic_]').each(function(){
	   		     var li_id = $(this).attr('id').split('_')[1];
	   		    if($('div#epicSubContainerDiv_'+li_id).children('div.epicContentDivs').size()<1){
		          $(this).find('div.epicContentBandCss, div.epicContentPurpleBandCss').children('img').hide();
		        }else{
		          $(this).find('div.epicContentBandCss, div.epicContentPurpleBandCss').children('img').show();
		        }
		       
		        $('#reorder_'+li_id).removeAttr('onmouseover').removeAttr('onclick');
				$('#reorder_'+li_id).find('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
				$('#reorder_'+li_id).addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
				$('#createSubIdea_'+li_id).removeAttr('onmouseover').removeAttr('onclick');
				$('#createSubIdea_'+li_id).find('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
				$('#createSubIdea_'+li_id).addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
			});
		}
						    
  }	

 function searchOnEnterStory(event){
   var code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
   if(code==13){
      filterIdeas();
   }
 }

 function filterIdeas(){
   if($('#storyContentDiv').is(':visible')){
	    $('div#epicContentMain').find('.epicContentDivs').hide();
		var txt = $('#searchDiv').find('input.epicSearch').val().toLowerCase();
		var sortVal=$("select[id=epicSortDropDown] option:selected").val();
	    showEpicSearchSort(sortVal,txt);
    }else{
        var searchTxt = $('#searchDiv').find('input.epicSearch').val().toLowerCase();
		filterSBdata(searchTxt);
    }
 }

 function filterSBdata(txt){
        $('div#groupSprintContainer').find('div.SB_epic, div.SB_feature').hide();
		var txt = $('#searchDiv').find('input.epicSearch').val().toLowerCase();
		if(txt!=''){
			$('div#groupSprintContainer').find('div.SB_epic').find('span.hidden_searchName:contains("'+txt+'")').parent('div.SB_epic').show();
			$('div#groupSprintContainer').find('div.SB_feature').find('span.hidden_searchName:contains("'+txt+'")').parent('div.SB_feature').show();
			if(!isiPad){
		       $("div#groupSprintContainer").mCustomScrollbar("update");
		    }
		}else{
		    $('div#groupSprintContainer').find('div.SB_epic, div.SB_feature').show();
			if(!isiPad){
				$("div#groupSprintContainer").mCustomScrollbar("update");
			}
		}
  }
 
 function clearEpicStorySearch(){
   $('input#epicSearch').val('');
   $('input.searchIcon').attr('src', path+'/images/searchTwo.png').addClass('Search_cLabelTitle').removeClass('Clear_cLabelTitle').removeAttr('onclick');
   $('div#storyContentDiv').find('div.epicContentDivs').show();
   loadAgileCustomLabel("Story");
 }
 
 //---------------------------------------- drag & drop functions ----------------------------->
 //.epicContentDivs
  function initEpicStoryDragDrop(){
      var flag = true ;
      $("#epicContentMain").sortable({
	                connectWith: "#epicContentMain",
	                placeholder: "ui-state-highlight",
	                handle:".drag-handle" ,
	                cursor:"move",
	                items:"> div",
	                cancel:"div[shared=R]",
	                scroll: true,
	                start: function( event, ui ) {
	                  draggingId = ui.item.parents('div[id^="epicContentDiv_"]').attr('id');
	                //  console.log("draggingId----"+draggingId);
	                  flag = true;
	                  //$('.ui-state-highlight').css({'border' : '1px solid white','background' : 'none'});
	                },
		            receive: function(ev, ui) {
		              droppingId= ui.item.parents('div[id^="epicContentDiv_"]').attr("id"); 
		            //  console.log("droppingId----"+droppingId);
		              if(draggingId!=droppingId){
		                 parent.alertFun(getValues(customalertData,"Alert_reorderNotPermissible"),'warning');
		                 flag = false;
			             ui.sender.sortable("cancel");
		              }
		            },
		            stop: function(event, ui){
		                   if(flag){
					          updateDrag_dropOrder();
					       }   
					   }
	    });
	 
	  $("#epicContentMain .epicContentDivs").bind("sortstart", function(event, ui) {
	       ui.placeholder.css({"border" : "1px solid #ffffff","height":"60px","background" : "none"}); 
	  });
	  $('#epicContentMain').find('div.epicContentDivs div.drag-handle').css('cursor','move');
	  $('#epicContentMain').find('div[shared=R]').find('div.drag-handle').css('cursor','default');
 }
  
  /*
   * This function is written for sorting the sub level of story
   */
  function initEpicStoryDragDropForSubLevel(epicId){
      var flag = true ;

     $(".agileSubDivs").sortable({
	                connectWith: ".agileSubDivs",
	                placeholder: "ui-state-highlight",
	                handle:".drag-handle" ,
	                cursor:"move",
	                items:"> div",
	                cancel:"div[shared=R]",
	                scroll: true,
	                start: function( event, ui ) {
	                  draggingId = ui.item.parents('div[id^="epicContentDiv_"]').attr('id');
	               
	                  flag = true;
	                 
	                },
		            receive: function(ev, ui) {
		              droppingId= ui.item.parents('div[id^="epicContentDiv_"]').attr("id"); 
		            //  console.log("droppingId----"+droppingId);
		             /* if(draggingId!=droppingId){
		                 parent.alertFun(getValues(customalertData,"Alert_reorderNotPermissible"),'warning');
		                 flag = false;
			             ui.sender.sortable("cancel");
		              }*/
		            },
		            stop: function(event, ui){
		                   if(flag){
					          updateDrag_dropOrder(epicId);
					       }   
					   }
	    });
	 
	  $("#epicContentMain .epicContentDivs").bind("sortstart", function(event, ui) {
	       ui.placeholder.css({"border" : "1px solid #ffffff","height":"60px","background" : "none"}); 
	  });
	  $('#epicContentMain').find('div.epicContentDivs div.drag-handle').css('cursor','move');
	  $('#epicContentMain').find('div[shared=R]').find('div.drag-handle').css('cursor','default');
 }
 
  function updateDrag_dropOrder(epicId){ 
      var orderXml=getOrderXml(epicId);
     $.ajax({
		   	url: path+"/agileActionNew.do",
			type:"POST",
			data:{action:"updateDragDropOrder",orderXml:orderXml },
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			success:function(result){
	            checkSessionTimeOut(result);
	            	$("div#epicList").mCustomScrollbar("update"); 
	        }
		});
} 
  /*
   * We are sending epicId for getting the children of sub level
   * there is a if else condition which is checking for the epicId if its undefined 
   * that means we have to get the children for main level else for child story. 
   */
  function getOrderXml(epicId){
	    var data="<xml>";
	   if(epicId != undefined){
		   if($('#epicSubContainerDiv_'+epicId+'').children().length > 0){
			      data += prepareXml($('#epicSubContainerDiv_'+epicId+'').children());
			 } 
	   }else{
		   if($('#epicContentMain').children().length > 0){
			      data += prepareXml($('#epicContentMain').children());
			 }
	   } 
		   data +="</xml>";
		   return data;
	 }
  
  function prepareXml(obj){
      var data='';
      $(obj).each(function(){
		  var epicId=$(this).attr('id').split("_")[1];	
		  var epicIndex=$(this).index();
		  data +="<epic id=\""+epicId+"\">";
		  data +="<id>"+epicId+"</id>";
		  data +="<order>"+epicIndex+"</order>";
		  data +="</epic>"
		  
		 if($(this).children('ol').length > 0){
		    data+= prepareXml($(this).children('ol').children('li'));
		 } 
	   });
		   
	return data;
 }
  
 //----------------------------------------   Create/ Associate Defects ---------------------->
  	var bug_story_id;
 function ReportBug(storyId){
       $('.hideEpicOptionDiv').hide();
 	   bug_story_id = storyId;
 	   var storyName = $('#editIdea_'+storyId).val().trim();
 	   var crossRef = $('#customIdSpan_'+storyId).text().trim();
 	   if(crossRef==''){
 	     crossRef = storyName;
 	   }else{
 	     crossRef= crossRef+' - '+storyName;
 	   } 
 	   var domain = location.protocol + "//" + location.host;
 	   
 	     
 	   //--->dynamic url -------->
 	     var logbug_domain="";
 	     var logbug_compname="";
 	     var logbug_orgname="";
 	     var logbug_appname="";
 	     var api="";
 	      	    
 	    $.ajax({
			   url:path + '/adminUserAction.do',
			   type: "post",
			   data:{act:"getIntegrationDetails"},
			   error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			   success:function(result){
			   		
				   checkSessionTimeOut(result);
				   result = $.parseJSON(result);
				   
				   for(var i=0; i<result.length; i++){
					   logbug_domain = result[i].logbug_domain_name;
					   logbug_compname = result[i].logbug_company_name;
					   logbug_orgname = result[i].logbug_organization_name;
					   logbug_appname = result[i].logbug_app_name;
					   api = result[i].logBugKey;
				   }
				   
				   if (!logbug_domain.match(/^https?:\/\//i)) {
				       	logbug_domain = 'http://' + logbug_domain;
				  		 }
				   
				   
				   //var url =   ''+logbug_domain+'//jsp/directLink1.jsp?companyName='+logbug_compname+'&orgName='+logbug_orgname+'&appName='+logbug_appname+'&userId=18&ip=123&priority=3&severity=33&bugstatus=23&bugGroup=49&rName='+userFullName+'&rEmail='+userEmail+'&rId='+userId+'&crossRef='+encodeURIComponent(crossRef)+'&storyName='+encodeURIComponent(storyName)+'&redirect=Y&domain='+domain+'&storyId='+bug_story_id+'';
			 	   
			 	   //--> local testing url -->
			 	      //var url =   'http://localhost:8080/Logbug//jsp/directLink1.jsp?companyName='+logbug_compname+'&orgName='+logbug_orgname+'&appName='+logbug_appname+'&userId=18&ip=123&priority=3&severity=33&bugstatus=23&bugGroup=49&rName='+userFullName+'&rEmail='+userEmail+'&rId='+userId+'&crossRef='+encodeURIComponent(crossRef)+'&storyName='+encodeURIComponent(storyName)+'&redirect=Y&domain='+domain+'&storyId='+bug_story_id+'';
				   
				   
				   
				 	 
				      var url =   ''+logbug_domain+'//jsp/directLink1.jsp?companyName='+logbug_compname+'&orgName='+logbug_orgname+'&appName='+logbug_appname+'&userId=18&ip=123&priority=3&severity=33&bugstatus=23&bugGroup=49&rName='+userFullName+'&rEmail='+userEmail+'&rId='+userId+'&crossRef='+encodeURIComponent(crossRef)+'&storyName='+encodeURIComponent(storyName)+'&redirect=Y&domain='+domain+'&storyId='+bug_story_id+'&api='+api+'';
				 	   
				 	   //--> local testing url -->
				 	   		
				     // var url =   'http://localhost:8080/Logbug//jsp/directLink1.jsp?companyName='+logbug_compname+'&orgName='+logbug_orgname+'&appName='+logbug_appname+'&userId=18&ip=123&priority=3&severity=33&bugstatus=23&bugGroup=49&rName='+userFullName+'&rEmail='+userEmail+'&rId='+userId+'&crossRef='+encodeURIComponent(crossRef)+'&storyName='+encodeURIComponent(storyName)+'&redirect=Y&domain='+domain+'&storyId='+bug_story_id+'&api='+api+'';
				 	   
				 	  
				 	   //window.resizeTo(783,543);
					   var h= ( window.screen.height/2 ) - (680/2);
					   var w= ( window.screen.width/2 ) - (800/2);
					   //window.moveTo(w,h);
					   //alert(url);
				 	   var bugWindow = window.open(url,"","width=783px,height=570px,top="+h+",left="+w+"");
				 	   
				 	   function checkWindowClosed(){
				 	       if(bugWindow && bugWindow.closed){
				 	          clearInterval(winClosedInt);
				 	          checkDefectExistForStory(storyId);
				 	       }
				 	   }
				 	   
				 	   var winClosedInt = setInterval(checkWindowClosed, 100);
				}
			}); 
 	   
 }	
 	
 function showDefects(obj,idd){
   
      if($(obj).attr('src').indexOf("Toggle.png") == -1 ){
               $(obj).attr('src', path+'/images/idea/defectToggle.png');
 			   $("#loadingBar").show();
 			   timerControl("start");
		       $.ajax({
					   	url: path+"/agileActionNew.do",
						type:"POST",
						data:{action:"fetchDefects",storyId:idd },
						error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
						},
						success:function(result){
				           checkSessionTimeOut(result);
				           showHideListDivs(idd,"defectList");
				           $('#epicListCommonDiv_'+idd).html(prepareDefectslistUI(jQuery.parseJSON(result)));//-----------------..>>>>
	                       loadAgileCustomLabel("defectList");
	                       $('#epicListCommonDiv_'+idd).slideDown(600);
	                       $("#loadingBar").hide();
				           timerControl("");
						}
				});
      }else{
          $('#epicListCommonDiv_'+idd).slideUp(function(){
		          $(obj).attr('src', path+'/images/idea/defect.png');
		          $('#epicListCommonDiv_'+idd).html('');
 		  });  
      }
 }	
  
 function prepareDefectslistUI(data){
    var taskUI='';
    taskUI+="<div id=\"\" class=\"tabContentHeader\" >";
    taskUI+=" <div class=\"tabContentSubHeader\" style='margin: 0px;'>";
        taskUI+="	<div class=\"tabContentHeaderName \" style=\"width: 2%;font-size:13px;\"><span style=\"float: left; height: 20px; margin-left: 5%; width: 95%; overflow: hidden;\" class=\"\"></span></div>";
		taskUI+="	<div class=\"tabContentHeaderName Defect_Id_cLabelHtml\" style=\"width:12%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName Defect_Name_cLabelHtml\" style=\"width: 45%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName Type_cLabelHtml\" style=\"width: 10%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName Created_By_cLabelHtml\" style=\"width: 15%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName Created_Date_cLabelHtml\" style=\"width: 12%;font-size:13px;height:20px;overflow:hidden;\"></div>";
    taskUI+=" </div>";
	taskUI+="</div>";
	
	taskUI+="<div id=\"\" style='width:100%;'>";
    if(data){
     var json = eval(data);
     for(var i=0; i<json.length; i++){
	        taskUI+="<div id=\"bug_"+json[i].defectId+"\" class=\"col-lg-12 defaultPaddingCls defectDiv\" >"
		           +"  <div class='docListViewBorderCls actFeedHover' style='margin:0px;padding-top:6px;font-size: 12px;'>" 
		           +"    <div class='tabContentHeaderName' style='width:2%;margin-top: 6px;float: left;' onclick=\"showBugDetails("+json[i].bugTicketId+");\"><div style=\"float:left;margin-left:8px;\"></div></div>" 
		           +"    <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:12%;margin-top: 6px;float:left;\" onclick=\"showBugDetails("+json[i].bugTicketId+");\" title=\""+json[i].bugTicketId+"\">"+json[i].bugTicketId+"</div>" 
		           +"    <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:45%;margin-top: 6px;float:left;\" onclick=\"showBugDetails("+json[i].bugTicketId+");\" title=\""+json[i].defectName+"\">"+json[i].defectName+"</div>" 
		           +"    <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:10%;margin-top: 6px;float:left;\" onclick=\"showBugDetails("+json[i].bugTicketId+");\" title=\""+json[i].defectType+"\">"+json[i].defectType+"</div>" 
		           +"    <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:15%;margin-top: 6px;float:left;\" onclick=\"showBugDetails("+json[i].bugTicketId+");\" title=\""+json[i].createdBy+"\">"+json[i].createdBy+"</div>" 
		           +"    <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:12%;margin-top: 6px;float:left;\" onclick=\"showBugDetails("+json[i].bugTicketId+");\" title=\""+json[i].createdDate+"\">"+json[i].createdDate+"</div>" 
		           +"    <div class='tabContentHeaderName actFeedHoverImgDiv stageMoreImgDiv' style='width:4%;margin-top: 6px;float:left;' ><img align = \"right\" id=\"defectOptionDiv_"+json[i].defectId+"\" class = \"img-responsive\" style=\"cursor: pointer; margin-right: 10px;\" src = \""+path+"/images/more.png\" onclick=\"openDefectOptionPopup(this);\"></div>" 
		           +"    <div class=\"stageOptionsDiv\" id=\"defectOptionPopDiv_"+json[i].defectId+"\" style=\"right: 28px; width: 130px;display:none;\">"
		           +"      <div style=\"margin-top: 0px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\"><img src=\""+path+"/images/arrow.png\"></div>"
	   			   +"      <div  class=\"optionMenuCss \" onclick=\"deleteDefect('"+json[i].storyId+"','"+json[i].defectId+"');\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" >"
		           +"        <span  class=\"optionSpanHover agileSpanCss Delete_cLabelText\"></span></div>"
		           +"    </div> "
		           +"  </div>" 
		           +"</div>";
	     }
    }
    taskUI+="</div>";
   return taskUI;
 } 
  
  
  function openDefectOptionPopup(obj){
         var idd=$(obj).attr('id').split('_')[1];
	     var t=$(obj).offset().top;
		 var oh=$(window).height()-t;
		 var h=$('#defectOptionPopDiv_'+idd).height();
	     $('#defectOptionPopDiv_'+idd).css({'margin-top':'-5px','margin-right':'1%'});
	     $('#defectOptionPopDiv_'+idd+' #arrowDiv_'+idd).css('margin-top','0px');
	     //if(oh<h){
	     //    var fh=h-oh+10;
		 //    var fh1=h-oh+8;
		 //    $('#defectOptionPopDiv_'+idd).css('margin-top','-'+fh+'px');
		 //    $('#defectOptionPopDiv_'+idd+' #arrowDiv_'+idd).css('margin-top',fh1+'px');
		 //} 
		 
		 if ($('#defectOptionPopDiv_'+idd).is(":hidden")) {
	       $('.stageOptionsDiv').hide();
	       //$('div#stageContainer').find('div.mCS_no_scrollbar_y').css('height','100%');
      	   $('#defectOptionPopDiv_'+idd).slideDown('slow',function(){
      	     $(this).css('overflow','');
      	   });
		 }
		 else{
		    $('#defectOptionPopDiv_'+idd).slideUp('slow');
		    //$('div#stageContainer').find('div.mCS_no_scrollbar_y').css('height','100%');
      	 }  
  }
  
  var confDefectId="";
  function deleteDefect(storyId,defectId){
     confirmEpicId = storyId;
     confDefectId = defectId;
     confirmFun(getValues(companyAlerts,"Alert_delete"),"delete","deleteDefectConfirm");
  }
   function deleteDefectConfirm(){	
        var defectId = confDefectId; 
		$("#loadingBar").show();
		timerControl("start");
		 $.ajax({
			   	url: path+"/agileActionNew.do",
				type:"POST",
				data:{action:"deleteDefect",defectId:defectId},
				error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
				},
				success:function(result){
		            checkSessionTimeOut(result);
		            $('#bug_'+defectId).remove();
		            if($('#epicListCommonDiv_'+confirmEpicId).find('div.defectDiv').length){
 		    		//-
 		    		}else{
 		    		   $('#defectIconDiv_'+confirmEpicId).html("");
 		    		   $('#epicListCommonDiv_'+confirmEpicId).hide();
 		    		   $('#epicCreatedBy_'+confirmEpicId).show();
 		    		}
 		    		
 		    		if(!isiPad){ 
 		    		  $("#epicList").mCustomScrollbar("update");
 		    		}
	                $("#loadingBar").hide();
					timerControl("");
				}
			});   
  }
  
  function associateDefect(storyId){
      $('.hideEpicOptionDiv').hide();
      $('#ascStory_ID').val(storyId);
      $('#ticket_no').val('');
      $('#associatePopDiv, #transparentDiv').show();
  }
  	  
  function closeAssociateDiv(){
	 $('#associatePopDiv,#transparentDiv').css('display','none');
  } 
  
  function checkDefectExistForStory(storyId){
       $("#loadingBar").show();
	  timerControl("start");
	  $.ajax({
	     url: path+'/agileActionNew.do',
	     type: 'POST',
	     data:{action:"checkDefectExistForStory",storyId:storyId},
		 error: function(jqXHR, textStatus, errorThrown) {
               checkError(jqXHR,textStatus,errorThrown);
               $("#loadingBar").hide();
			   timerControl("");
		 },
		 success:function(result){
		       checkSessionTimeOut(result);
		       //alert(result);
		       if(result=='Exist'){
			       if($('#defectIconDiv_'+storyId).children().size()){
	 					$('#defectIcon_'+storyId).attr('src', path+'/images/idea/defect.png');
	 			   }else{
					   var imgData="<img id=\"defectIcon_"+storyId+"\" style='cursor: pointer; float: right;margin-right: 3px;height:18px;' class='Defect_cLabelTitle' src=\""+path+"/images/idea/defect.png\" onclick=\"showDefects(this,'"+storyId+"');\" />";
					   $('#defectIconDiv_'+storyId).html(imgData);
	 			   }
			       showDefects($("#defectIcon_"+storyId), storyId);
		       }
		       $("#loadingBar").hide();
			   timerControl("");
		 }
	  });
  }
  
  function associateDefectToStory(){
      var ticketNo = $('#ticket_no').val().trim();
      if(ticketNo == ''){
          alertFun(getValues(companyAlerts,"Alert_EntTicket"),'warning');
		  $("#ticket_no").focus();
		  return false;		
      }
      if(isNaN(ticketNo)){
        alertFun(getValues(companyAlerts,"Alert_numericValues"),'warning');
		return false;
      }
      var storyId = $('#ascStory_ID').val();
      $("#loadingBar").show();
	  timerControl("start");
	  $.ajax({
			url: path+"/agileActionNew.do",
			type:"POST",
			data:{action:"associateDefect",ticketNo:ticketNo,storyId:storyId},
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			success:function(result){
		        checkSessionTimeOut(result);
		        if(result =="success"){  	
 		    	  alertFun(getValues(companyAlerts,"Alert_DefAsso"),'warning');
 		    	  if($('#defectIconDiv_'+storyId).children().size()){
	 					$('#defectIcon_'+storyId).attr('src', path+'/images/idea/defect.png');
	 			  }else{
					   var imgData="<img id=\"defectIcon_"+storyId+"\" style='cursor: pointer; float: right;margin-right: 3px;height:18px;' class='Defect_cLabelTitle' src=\""+path+"/images/idea/defect.png\" onclick=\"showDefects(this,'"+storyId+"');\" />";
					   $('#defectIconDiv_'+storyId).html(imgData);
	 			  }
 		    	  showDefects($("#defectIcon_"+storyId), storyId);
 		    	  closeAssociateDiv();
 		    	}else if(result=="Exist"){
 		    	  alertFun(getValues(companyAlerts,"Alert_DefEx"),'warning');
 		    	}else if(result=="failed"){
 		    	  alertFun(getValues(companyAlerts,"Alert_DefNotAvail"),'warning');
 		    	}
	            $("#loadingBar").hide();
				timerControl("");
			}
	  });
  }
  
  function showBugDetails(ticketNo){
      $("#loadingBar").show();
	  timerControl("start");
	  $.ajax({
			url: path+"/agileActionNew.do",
			type:"POST",
			data:{action:"fetchDefectDetails",ticketNo:ticketNo},
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			success:function(result){
		        checkSessionTimeOut(result);
		        
		        if( result == 'failed'){
		          alertFun(getValues(companyAlerts,"Alert_Error"),'warning');
		        }else if(result != '[]'){
			        var jsonData=jQuery.parseJSON(result);
			          var bugName = jsonData[0].BugName;
			          bugName = bugName.replaceAll('CHR(39)','\'');
					  bugName = bugName.replaceAll('CHR(40)','"');
					  var bugDesc = jsonData[0].BugDescription;
					  bugDesc = bugDesc.replaceAll('CHR(39)','\'');
					  bugDesc = bugDesc.replaceAll('CHR(40)','"');
					  var crossRef = jsonData[0].CrossReference;
					  crossRef = crossRef.replaceAll('CHR(39)','\'');
					  crossRef = crossRef.replaceAll('CHR(40)','"');
					  var creatorComments = jsonData[0].CreatorComments;
					  creatorComments = creatorComments.replaceAll('CHR(39)','\'');
					  creatorComments = creatorComments.replaceAll('CHR(40)','"');
					  var category1 = jsonData[0].CATEGORY1;
					  category1 = category1.replaceAll('CHR(39)','\'');
					  category1 = category1.replaceAll('CHR(40)','"');
					  var category2 = jsonData[0].CATEGORY2;
					  category2 = category2.replaceAll('CHR(39)','\'');
					  category2 = category2.replaceAll('CHR(40)','"');
					  
					  
					  
			         $('#bugName').val(bugName);
			         $('#bugDetails').val(bugDesc);
			         $('#crosRef').val(crossRef);
			         $('#bugTicketNo').val(ticketNo);
			         $('#bugStartDate').val(jsonData[0].StartDate);
			         $('#bugEndDate').val(jsonData[0].EndDate);
			         $('#BugIssueGroup').val(jsonData[0].IssueGroup);
			         $('#BugSeverity').val(jsonData[0].Severity);
			         $('#bugReportedBy').val(jsonData[0].ReportedByName);
			         $('#BugPriority').val(jsonData[0].Priority);
			         $('#BugStatus').val(jsonData[0].BugStatus);
			         $('#bugReportedByEmail').val(jsonData[0].ReportedByEmail);
			         $('#bugReportedDate').val(jsonData[0].StartDate);
			         $('#bugCompletionDate').val(jsonData[0].EndDate);
			         $('#bugComment').val(creatorComments);
			         $('#category_1').val(category1);
			         $('#category_2').val(category2);
			         $('#bugDetailPopup').css('display','block');
	 		         $('#transparentDiv').css('display','block');
 		         }else{
 		            alertFun(getValues(companyAlerts,"Alert_DefNotAvail"),'warning');
				 }
				 
	            $("#loadingBar").hide();
				timerControl("");
			}
	  });
  } 
 
  function cancelDefectPopup(){
    $('#bugDetailPopup').hide();
	$('#transparentDiv').hide();
 }
 
 
 
 
 	  
 //--------------------------------------- reusable functions --------------------------------->	
  
 
  function getStatusCountForEpic(jsonData){
        var epicId = jsonData[0].epic_id;
		$('#storyStatus_'+epicId).find('span.backlogSpan').text(jsonData[0].backlogCount);
		$('#storyStatus_'+epicId).find('span.assignedSpan').text(jsonData[0].assignedCount);
		$('#storyStatus_'+epicId).find('span.blockedSpan').text(jsonData[0].blockedCount);
		$('#storyStatus_'+epicId).find('span.holdSpan').text(jsonData[0].holdCount);
		$('#storyStatus_'+epicId).find('span.doneSpan').text(jsonData[0].doneCount);
		$('#storyStatus_'+epicId).find('span.cancelSpan').text(jsonData[0].cancelCount);
		console.log("jsonData-->"+JSON.stringify(jsonData));
 }
  
  function showHideListDivs(epicId,listDivType){
        /*
         if (listDivType!="comment" && !$('#epicComment_'+epicId).is(":hidden")) {  
		    $('#epicComment_'+epicId).hide();
		 } 
		 */
		 
		if (listDivType!="addStory" && !$('#addSubIdea_'+epicId).is(":hidden")) {  
		    $('#addSubIdea_'+epicId).hide();
		 }
		   
		if (listDivType!="task" ) {  
		    $('#taskIcon_'+epicId).attr('src', 'images/idea/task1.png');
		    $('#epicListCommonDiv_'+epicId).html('');
		}   
		  
		if (listDivType!="document" ) {  
		    $('#docIcon_'+epicId).attr('src', 'images/idea/Document.png'); 
		    $('#epicListCommonDiv_'+epicId).html('');
		  }
		  
	    if (listDivType!="commentList" ) {  
	        $('#cmtIcon_'+epicId).attr('src', 'images/idea/comment1.png');
	  	    $('#epicListCommonDiv_'+epicId).html('');
	  	    $('#comment_'+epicId).html('').hide();
		  }
		
	    if (listDivType!="collaborators" ) {
	        $('#shareIcon_'+epicId).attr('src', 'images/idea/share1.png'); 
	  	    $('#epicListCommonDiv_'+epicId).html('');
	    }
	    
	    if (listDivType!="defectList" ) {  
	        $('#defectIcon_'+epicId).attr('src', 'images/idea/defect.png');
	  	    $('#epicListCommonDiv_'+epicId).html('');
		  }
 		   
 }
 
   function checkStatus(){
         
         $('#epicContentMain').find('.logbug_N').removeAttr('onmouseover').removeAttr('onclick').addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
		 $('#epicContentMain').find('.logbug_N').find('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
		 
		 	
      if($('#epicContentMain').find('.suBlocked').length){
			$('#epicContentMain').find('.suBlocked').removeAttr('onmouseover').removeAttr('onclick');
			$('#epicContentMain').find('.suBlocked').find('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
			if($('#epicContentMain').find('.suBlocked').hasClass('optionMenuOpacity')){
			   $('#epicContentMain').find('.suBlocked').addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
			}else{
			   $('#epicContentMain').find('.suBlocked').addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
			}
	  }
	  
	  if($('#epicContentMain').find('.tmBlocked').length){
			$('#epicContentMain').find('.tmBlocked').removeAttr('onmouseover').removeAttr('onclick');
			$('#epicContentMain').find('.tmBlocked').find('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
			if($('#epicContentMain').find('.tmBlocked').hasClass('optionMenuOpacity')){
			   $('#epicContentMain').find('.tmBlocked').addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
			}else{
			   $('#epicContentMain').find('.tmBlocked').addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
			}
	  }
	  
	  if($('#epicContentMain').find('.readAccess').length){
	  		$('#epicContentMain').find('span.readAccess').removeAttr('onclick').css('cursor','default');
	  		$('#epicContentMain').find('img.readAccess').removeAttr('onclick').css({'opacity':'0.8','cursor':'default'});
	        $('#epicContentMain').find('textarea.readAccess').removeAttr('onclick').removeAttr('onkeyup').removeAttr('onBlur').css('color','#999999');
			$('#epicContentMain').find('div.readAccess').removeAttr('onmouseover').removeAttr('onclick');
			$('#nestable').find('div.readAccess').find('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
			if($('#epicContentMain').find('div.readAccess').hasClass('optionMenuOpacity')){
			   $('#epicContentMain').find('div.readAccess').addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
			}else{
			   $('#epicContentMain').find('div.readAccess').addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
			}
	  }
	  
	  if($('#epicContentMain').find('.shareHideDefault').length){
			$('#epicContentMain').find('.shareHideDefault').removeAttr('onmouseover').removeAttr('onclick');
			$('#epicContentMain').find('.shareHideDefault').find('span').removeClass('optionSpanHover').addClass('optionSpanHoverFade');
			if($('#epicContentMain').find('.shareHideDefault').hasClass('optionMenuOpacity')){
			   $('#epicContentMain').find('.shareHideDefault').addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
			}else{
			   $('#epicContentMain').find('.shareHideDefault').addClass('optionMenuOpacity1').removeClass('optionMenuOpacity');
			}
	  }
	  
 }
 
 function custlabel(){
	   
	$.ajax({
		  url:apiPath+"/"+myk+"/v1/loadCustomlebels/"+companyIdglb+"",
		  type:"GET",
		  dataType:"json",
		  error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				  ///alert("jqXHR-->"+jqXHR.status);
				   $("#loadingBar").hide();
				  timerControl("");
				  },
		  success:function(result){
			  ///alert("result-->"+result);
			  companyLabels = result;
			  loadAgileCustomLabel("onload");
			  
		  } 
		  
	  });
	  
			  
  } 	
   
function custalert(){
	 
	$.ajax({
		  url:apiPath+"/"+myk+"/v1/loadCompanyAlerts/"+companyIdglb+"",
		  type:"GET",
		  dataType:"json",
		  error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				  ///alert("jqXHR-->"+jqXHR.status);
				   $("#loadingBar").hide();
				  timerControl("");
				  },
		  success:function(result){
			  ///console.log("companyAlerts-->"+JSON.stringify(result));
			  companyAlerts = result;
			  loadAgileCustomLabel("onload");
			  
		  } 
		  
	  });
	  
			  
  }
    
  function loadAgileCustomLabel(place){
	  //console.log("story--------"+place);
	  var keyList="";
	  if(place=='onload'){
	     keyList="Agile,Upload from repository,Upload from system,Attach Link,AGILE PROJECTS,Search and sort,Search,Clear,Sort,Sort Name,Created Date,Show hidden projects,Show active projects,Agile comment,Enter your comments,Save,Cancel,Agile,STORY,SPRINT,Email,Conversations,Documents,Tasks,Ideas,Team,Agile Definition,Agile Sprint,Team Zone,Back,db Complete Dashboard,Upload csv file,Create stages,sprintgroup scrumboard,Export,db Blocked,db Hold,db Assigned,db Done,db Cancel,Sprint Groups,Add groups,Type,Epic and Feature Settings,Epic,Feature,Options,Post,Download sample CSV file,Upload CSV file,SAVE";	
	  }else if(place=="Projects"){
    		keyList="Shared by,Owned by,No projects available";	
      }else if(place=="Project Details"){
    		keyList="Shared by,Close,Owned by";	
      }else if(place=="Story"){
      		keyList="Agile,STORY,SPRINT,Search and sort,Search,Clear,Back,Collapse All,Expand All,Sprint Calender View,"+
      				"show hidden groups,show active groups,Detail View,chart Daily,chart Burndown,"+
      				"Sort,Sort Backlog,Sort Assigned,Sort Blocked,Sort Hold,Sort Done,Sort Cancel,Created Date,Created by,Updated By,Modified by,"+
      				"Enter a epic,Upload from repository,Upload from system,Attach Link,Enter a Story,Remaining,Save,Cancel,Options,Collaborators,Comments,Documents,Tasks,Add Id,Custom Id,"+
      				"Epic Menu,Story Menu,Add Feature,Add Story,Sprint_Delete,Agile Reorder,Agile Function,Agile Comments,Agile Documents,Agile Collaborators,"+
      				"Agile Delete,Agile Status,Agile Value,Agile Priority,Agile Points,Agile Task,Attach Link,Upload From Repository,Upload From System,"+
      				"Enter your comments,Post,Close,Assign Point,Defect,Stories,Share,Post,Backlog,In Progress,Blocked,Hold,Done,Cancel,"+
      				"Team Zone,My Zone,Enterprise Zone,Start New inZone,Existing inZone,Contacts,Tasks,DOCUMENTS,Messages,Notes,Blogs,Gallery,Analytics,System Admin,Integration,All Scrums";
    		
      }else if(place=="Sprint"){
      		keyList="SPRINT,Search,Clear,show hidden groups,show active groups,Sprint Groups,Add group,Sprint group,"+
      				"No sprint groups available,Sprint Details,Create Sprint,Sprint Update,Sprint Active,Sprint Inactive,"+
      				"Sprint Hide,Sprint Delete,Sprint Unhide,Sprint Chart";	
    		
      }else if(place=="fetchComments"){
        	keyList="Comments,Posted by,on,Reply,Delete,Enter your comments,Post,Close,Edit";
      }else if(place=="TaskUI"){
        	keyList="Task,Instructions,Sprint group,Sprint name,From,To,starts at,ends at,start time,end time,Select start date,Select end date,Title," +
        			"Est hrs,h,m,More,Reminder,Type,Task Project,Participants,Save,Cancel,Priority," +
        			"Date,Estimated Hour,Reset,OK,Select,Close,Delete,"+
        			"Select,Minutes,Hour,Daily,Weekly,Monthly,Yearly,none,by email,by sms,Progress,Action,Act Hrs,Comments,"+
        			"Completed,Canceled,NotCompleted,InProgress,Created,Spent,Budgeted,No previous comments,All Users,Search,Clear,Add Participants,No Stories available,Users All,Act Time,Est Time";
      }else if(place=="TaskList"){
        	keyList="Type,Priority,Task,Assigned to,Task Id,Created by,Start date,End date,Status,ASSIGNED TASKS,MY TASKS,"+
        	        "Completed,InProgress,NotCompleted,No Tasks available,Tasks";
      }else if(place=="AttachDoc"){
        	keyList="Close,All folders,Up,Repo Name,Repo Size,Repo Owner,Repo Created,Repo Modifier,Repo Modified,Cancel,OK,Documents,"+
        			"Created by,Created On,Edit,Delete,Save,Link,Title";
      }else if(place=="AttachLink"){ 
        	keyList="Close,Attach Link,Link Title,Link,Attach,Documents,Created by,Created On,Edit,Delete";
      }else if(place=="EpicShare"){
        	keyList="Participants,Invite,Message,Save,Cancel,Select,All Users,Search,Close,Add Participants,"+
        	        "Administrators,Team Members,Observers,R,W,Delete,Collaborators";
      }else if(place=="EpicCollaborators"){
        	keyList="User Mobile,Home,Email";
      }else if(place=="SprintUI"){
        	keyList="SPRINT,Description,Sprint group,Sprint Start date,Sprint End date,starts at,ends at,Select start date,Select end date," +
        			"More,STORY,Story Id,Agile Points,Priority,Status,Total,Delete,Save,Cancel,OK,Close,Date,Day,Estimated Hour,Remove Unchecked Dates," +
        			"Show All Dates,Select,Search,Add Stories,Sprint Pts,Sprint Prty,Sprint Sts,No Stories available"
      }else if(place=="Detail view"){
        	keyList="Sprint group,SPRINT,Sprint Created By,Task Id,Start date,End date,Story Id,STORY,Sprint Pts,Est hrs,Status,"+
        			"No Stories available,No sprints available,No Tasks available,Type,Task,Assigned to,Act Hrs,"+
        			"Close,Created by,Instructions,Sprint group,Sprint name,From,To,Est hrs,h,Participants,Progress,Action,Act Hrs,"+
        			"Story Assign,Group,Description,Sprint show description,Sprint hide description,No previous comments,Completed,InProgress,NotCompleted";
      }else if(place=="TaskUserAdd"){
        	keyList="Delete,Completed,Canceled,NotCompleted,InProgress,Created,h,Comments";
      }else if(place=="TaskUserSearch"){
        	keyList="Search,Clear,Completed,Canceled,NotCompleted,InProgress,Created,Spent,Budgeted,No previous comments,Delete,Comments,h";
      }else if(place=="defectList"){
        	keyList="Created By,Created Date,Defect Id,Defect Name,No Defects Available,Delete,Type";
      }else if(place=="Stages_E"){
        	keyList="No results found,Enter a value,Options,Edit,Agile Reorder,Delete,epic stages";
      }else if(place=="Stages_F"){
        	keyList="No results found,Enter a functionality,Options,Edit,Agile Reorder,Delete,feature stages";
      }else if(place=="ScrumBoard"){
        	keyList="Next Stage,Previous Stage,Agile Points,Comments";
      }
        
        var labelKeyList=keyList.split(',');
		var labelValue="";
		var labelKey="";
		for(var i=0;i<=labelKeyList.length-1;i++){
			labelKey=labelKeyList[i].replaceAll(' ','_');
			labelKey=labelKey.replaceAll('/','_');
			labelKey=labelKey.replaceAll('-','_');
			labelValue=getValues(companyLabels,labelKey);
			
			$('.'+labelKey+'_cLabelTitle').attr('title',labelValue);
			$('.'+labelKey+'_cLabelText').text(labelValue);
			$('.'+labelKey+'_cLabelLink').text(labelValue);
			$('.'+labelKey+'_cLabelPlaceholder').attr('placeholder',labelValue);
			$('.'+labelKey+'_cLabelHtml').html(labelValue);
			$('.'+labelKey+'_cLabelValue').val(labelValue);
	
	}

 }
 
 function getStoryOrderXml(){
    var data="<xml>";
    $('div#epicContentMain').find('div.epicContentDivs').each(function(){
		 var liId=$(this).attr('id').split('_')[1];	
		 var ind=$(this).index();
		 //alert('li-id:'+liId+ ' index:'+ind);	
		  data +="<epic id=\""+liId+"\">";
		  data +="<id>"+liId+"</id>";
		  data +="<order>"+ind+"</order>";
		  data +="</epic>"
		
	   });
	   data +="</xml>";
	   return data;
 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 //----this method is calling from taskUIcode.js file after sprint task is created
 function reloadStoryUI(storyIdGlo, sprintNameGlo, sprintStatusCheck, status, sprintGrpName, sprintId, sprintGrpId, sprintStartDate, sprintEndDate){
 	//alert(storyIdGlo);
 	if(sprintNameGlo != ""){
	 	var imgSrc =  path + "/images/idea/Assigned.png";
	 	$('#epic_'+storyIdGlo).find('img.epicProgressImg').attr('src',imgSrc);
	 	$('#epic_'+storyIdGlo).find('#epicStoryStatus_'+storyIdGlo).text("In Progress");
	 	
	 	if($('#epic_'+storyIdGlo).find('div.storyPipeCls').length < 1){
	 	 	var progLi = 	"<div style=\"float: left;margin: 0px 5px 0px 5px;\" class=\"storyPipeCls\"></div>"
	 	 	 				+"<div class=\"epicProgressDiv defaultCls defaultNameDateTimestamp defaultExceedCls\" id = \"sprintSpan_"+storyIdGlo+"\"  style=\"width:50%;\">"+sprintNameGlo+"</div>";
	         $('#epic_'+storyIdGlo).find('div.progressContDiv').append(progLi);
	    }
	    
	    $('#sprintIdHidden_'+storyIdGlo).val(sprintId);
		$('#sprintGrpIdHidden_'+storyIdGlo).val(sprintGrpId);
		$('#sprintStartDateHidden_'+storyIdGlo).val(sprintStartDate);
		$('#sprintEndDateHidden_'+storyIdGlo).val(sprintEndDate);
		
	 }	
	 
 	 if($('#taskImgDiv_'+storyIdGlo).children('img').length < 1){
	 	var taskImgUI = "<img onclick=\"showIdeaTasks(this, "+storyIdGlo+");\" src=\""+path+"/images/idea/task1Toggle.png\"  class=\"epicShareImgDiv Tasks_cLabelTitle\" id=\"taskIcon_"+storyIdGlo+"\" title=\"Tasks\">"
	 	$('#taskImgDiv_'+storyIdGlo).append(taskImgUI);
	 }else{
	    $('#taskIcon_'+storyIdGlo).attr('src', path+'/images/idea/task1Toggle.png');
	 }
	 
	 fetchStoryTask(storyIdGlo);
 }
  
 
 
 
 function uploadCsvFile(){	
 	$("div#CreateNewCsvPopUp").css("display","block");
 	$("#transparentDiv").show();
    $("#noteColumn").attr("href",lighttpdPath+"EpicCsv/csv_sample_agile.csv");
    loadAgileCustomLabel('onload');
 }
 
 function submitCsvFileFormForAgile(e){
        $("#csvUploadFileName").text('');
		var FileName = $('#CsvFileName').val();
		var extension = FileName.split(".");
		if(extension[1].toLowerCase() == "csv" ){
			$("#csvUploadFileName").text(FileName);
			$("#uploadCsvFileButton").click(function(){
			        var formname = e.form.name;
					var colform = document.getElementById(formname);
					colform.target = 'upload_target';
					$('#loadingBar').show();
					colform.submit();
		    });
		}
		else{
				parent.alertFun(getValues(companyAlerts,"Alert_UpdCsv"),"warning");
				return false;
		}
 }
 
 function saveCsvFileAgile(filepath,fileName,extn){
 	
 		var notValidProject="";
 		var data="";
		$("#csvUploadFileName").text("");
		$("div#CreateNewCsvPopUp").css("display","none");
		var projectName = projtName;
		var id = projectId;
		$("#loadingBar").show();
        timerControl("start");
        $("#transparentDiv").hide();
        var projId = $("#proj_ID").val(); 
        var data = "";
		$.ajax({
			url: path+"/agileActionNew.do",
			type:"POST",
			data:{action:"saveCsvFileDetails", filepath:filepath,fileName:fileName,projectName:projectName,projId:projId},
			error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
			},
			success:function(result){
					checkSessionTimeOut(result);
					$("#loadingBar").hide();
       				timerControl("");
       				epicList(id,'');
 	 				
       				///setTimeout(function(){
	       				var data1 = result.split("#@@#")[0];
	       				var data2 = result.split("#@@#")[1];
	       				var data3 = result.split("#@@#")[2];
	       				
	       				if(data1=="success" && data2 == "" ){
					    	alertFun(getValues(companyAlerts,"Alert_Created"),'warning');
					    }
					    else if(data2!=""){
					    	confirmFun(getValues(companyAlerts,"Alert_RecErrLog"),"close","csvDownload");
					        //alertFun("Row(s) "+data2+" did not get created due to invalid data.",'warning');
					    }
					    
					    fullFileName=data3;
				   	//},5000);
       			 }
			  });
			 
		
 }
 
 function csvDownload(){				
 	 window.location.href = lighttpdPath + "/EpicCsv/"+fullFileName+"";
 }
 
 function showStoryIconCmts(idd){
   if($("#cmtIcon_"+idd).attr('src').indexOf("Toggle.png") == -1 ){
           $("#cmtIcon_"+idd).attr('src', path+'/images/idea/comment1Toggle.png');
		   $('#epicListCommonDiv_'+idd).show();
		   fetchAgileComments(idd);
  }else{
      $('#epicListCommonDiv_'+idd).slideUp(function(){
          $("#cmtIcon_"+idd).attr('src', path+'/images/idea/comment1.png');
          $(this).html('');
          $('div#comment_'+idd).empty().hide();
      });  
  }
 }
 //clear fun //shiv
 		 function clearStory(){
 		    $("input#searchBox").val('');
 		    $("input#clearImgDefination").hide();
		 	 showEpics();
	     }
	     
	      function filterStory(){
	 		txt = $("input#searchBox").val().toLowerCase();
	 		if(txt!=''){
			   $("#clearImgDefination").show();
	  		}else{
	  		   $("#clearImgDefination").hide();
		 	 showEpics();
	  		}
	     }
	     
	     
	function createJiraStory(title,projId,colStoryId){
		
		   	$.ajax({ 
					url: path+'/adminUserAction.do',
					type:"POST",
				 	data:{act:'addjiratask',title:title,projId:projId,colStoryId:colStoryId,
				 	},
				error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
							},
				success:function(result){
				
				}
		   	});
	   }
	
	/*function jiraStoryComment(projId,colStoryId,comment){
		var comment =  "Created by -  "+userFullName+" : "+comment
		//var comment = comment + " | Created by : "+userFullName;
		$.ajax({ 
			url: path+'/adminUserAction.do',
			type:"POST",
		 	data:{act:'addjirastorycomment',projId:projId,colStoryId:colStoryId,comment:comment,
		 	},
		error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
		success:function(result){
		
		}
		});
	}*/