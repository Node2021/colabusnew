

	
	/*----------------------------------------- preparing UI for comments ------------------------*/ 
	var jsonData="";
	var jsonDataSub="";
	var jsonDataSub2Sub="";
	var limit="";
	var reloadDelTaskIdForConvo = "";
	var commentval= "";
	var feedEditId="";
	var LoginId = "";
	var Proje = "";
	var MenuTyp = "";
	var MenuTypId = "";
	var SuperPId = "";
	var level=0;
	var elementindex='';
	var elementhashindex='';
	var resultset='';
	var hashResult="";
	var blogCommentScroll=false;
	var notifStatus ="";
	var projIdForVideo="";
	var commentPlace= ""; // this variable used in agile js files.
	var feedIdForCal = ""; //This variable is used to store conversation feedId value while updating the time in conversation task. 
	var actfedid="";
	var activityfeedid = "";
	var hashInd="";

	var convoTask="";

	function imgicon(commentData){
		var iconimg="";
		iconimg=commentData.substring(commentData.lastIndexOf(".")+1,commentData.length);
		if(iconimg == "mp4" || iconimg == "JPG"){
			iconimg = "jpeg";
		}
		var iconimgdisplay = iconimg+".png";
		return iconimgdisplay;
	}
	function prepareCommentsUI(jsonData,menuType,menuTypeId,countforreply,hightlight){
		
		if(notifStatus=="N"){
			$("#loadingBar").hide();
		}
		var i = 0;
    	limit = 20;
	    var j = limit;
		var ui = "";
		var imgName = "";
		var userName = "";
		var commentData = "";
		var createdElapTime = "";
		var modifiedElapTime = "";
		var activityfeed_type = "";
		var updatedBy = "";
		var updatedTime = "";
		var CreatedTime="";
		var feedId = "";
		var subCommentsData = "";
		var userid = "";

		var taskPresentId="";
		var filenamevoice="";
		var voiceno="";
		var docId="";
		var ImgExt="";
		var ext =""; 

		if(typeof(countforreply) == "undefined"){
			countforreply=0;
		}
		

		for (i = 0; i < jsonData.length ; i++) {
		
			subCommentsData = jsonData[i].subCommentsData;
			hightlight = typeof (hightlight) != 'undefined' && hightlight != '' ? 'notHightlightCls' : '';
			feedId = jsonData[i].activityfeed_id;
			//alert(i+"-->"+feedId);
			imgName = lighttpdpath+"/userimages/"+jsonData[i].user_image+"?"+d.getTime();
			userName = jsonData[i].name;
			activityfeed_type = jsonData[i].activityfeed_type;
			updatedBy = jsonData[i].upd_NAME;
    		updatedTime = jsonData[i].last_updated_time;
			CreatedTime = jsonData[i].created_time;
			userid = jsonData[i].user_id;
			if(typeof(userName)=="undefined" || userName == ""){
				userName = jsonData[i].userFullName;
			}
			commentData = jsonData[i].activityfeed;
    		commentData = commentData.replaceAll("CHR(50)","'");
    		commentData = commentData.replaceAll("CH(70)","\\");
			
			if(typeof(activityfeed_type) == "undefined")
    			activityfeed_type="text";


			if(typeof(jsonData[i].created_elapsed_time) != "undefined"){
				createdElapTime = jsonData[i].created_elapsed_time; 
				createdElapTime = calculateElapseTime(createdElapTime);
			}else{
				createdElapTime = CreatedTime;
			}
				
			if(typeof(jsonData[i].modified_elapsed_time) != "undefined" && jsonData[i].modified_elapsed_time!=""){
				modifiedElapTime = jsonData[i].modified_elapsed_time; 
				modifiedElapTime = calculateElapseTime(modifiedElapTime);
			}else{
				modifiedElapTime = updatedTime;
			}
			var marginleft = jsonData[i].level == 2 && countforreply ==0 ? 'pl-5' : '';
			let level='';
			if( jsonData[i].level == 0 && jsonData[i].parent_feed_id==0 ){
				level = 0;
			}else if(jsonData[i].level != 0){
				level=jsonData[i].level 
			} else if (jsonData[i].level == 0 && jsonData[i].parent_feed_id!=0 ) {
				level = parseInt($("#actFeedDiv_" + jsonData[i].parent_feed_id).attr('level')) + 1;
			}
			
			var borderbottom = jsonData[i].level == 0 && level == 0 ? 'border' : '';
		
			var px = jsonData[i].level == 0  && jsonData[i].parent_feed_id==0 ? 'px-3' : '';
		
			var paddingleft = jsonData[i].level == 2 ? 'pl-5' : '';

			paddingleft= jsonData[i].level == 0  && level > 1 ? 'pl-5' : paddingleft;
			
			var parentFeedId=jsonData[i].parent_feed_id;

			var working_hours = jsonData[i].working_hours;var working_minutes = jsonData[i].working_minutes;
			var percentage_users=jsonData[i].user_percentage;

			if(jsonData[i].user_status=="Pending approval"){
				working_hours="-";
				working_minutes="-";
				percentage_users="-";
			}else if(jsonData[i].user_status=="Approved"){
				working_hours="-";
				working_minutes="-";
				percentage_users="-";
			}else if(jsonData[i].user_status=="Rejected"){
				working_hours="-";
				working_minutes="-";
				percentage_users="-";
			}else if(jsonData[i].user_status=="On hold"){
				working_hours="-";
				working_minutes="-";
				percentage_users="-";
			}else if(jsonData[i].user_status=="Cancel"){
				working_hours="-";
				working_minutes="-";
				percentage_users="-";
			}

			taskPresentId = jsonData[i].taskForActFeed;
			docId = jsonData[i].doc_id;
			subCommentsData = jsonData[i].subCommentsData;
			if(activityfeed_type=="file"){
				ImgExt = imgicon(commentData);
			}

			if(jsonData[i].level == 0  && jsonData[i].parent_feed_id==0 ){
			//div 1 starts
			ui+='<div id="actFeedMainDiv_'+feedId+'" class="actFeedTot d-block media border-top-0 border-left-0 border-right-0 pt-2 w-100 pb-3 '+px+' border-bottom-1 '+borderbottom+' " >'
			ui+='<div id="deleteFeed_'+feedId+'">'		
			}
			if(jsonData[i].level == 0  && !jsonData[i].parent_feed_id==0 && subCommentsData.length<1){

					ui+='<div id="deleteFeed_'+feedId+'">'		
			}
			//div 2 starts
			ui+='<div id="actFeedDiv_'+feedId+'"  level="'+level+'" parentFeedId="'+parentFeedId+'" class="media conversation actFeedHover border-top-0 border-left-0 border-right-0 px-1 pb-3 pt-3 w-100 '+paddingleft+'  '+hightlight+' " style="color:#000;"'
			+"ondblclick = \"AddSubComments("+feedId+",'"+menuType+"');event.stopPropagation();\">"
			if(userid == userIdglb){
				ui+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+userid+', this);" data-src="'+imgName+'" src="/images/profile/userImage.svg" title="'+userName+'" onerror="userImageOnErrorReplace(this);" alt="'+userName+'" class="lozad mr-3 mt-0 rounded-circle" style="width:45px;">'
			}
			else{
				ui+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+userid+', this);" data-src="'+imgName+'" src="/images/profile/userImage.svg" title="'+userName+'" onerror="userImageOnErrorReplace(this);" alt="'+userName+'" class="lozad mr-3 mt-0 rounded-circle cursor" style="width:45px;">'
			}
				//div 3 starts
				ui+="<div  class=\"media-body\" id='aFeed1_"+feedId+"' onmouseover=\"showOptionsUi("+feedId+",moreoptionimage_"+feedId+",'"+activityfeed_type+"',"+jsonData[i].user_id+","+jsonData[i].parent_feed_id+");event.stopPropagation();\">"//triggermoreoptions("+feedId+")
					+'<p id="personname" class="mb-0 font-weight-bold" style="font-size: 13px;">'+userName+'<span class="ml-2" style="color:#999999;font-size:12px;"><span>'+getValues(companyLabels,"Posted_on")+' : '+createdElapTime+'</span>'
					if(updatedTime!="-" && updatedTime!=""){
						ui+='&nbsp;|&nbsp;<span>'+getValues(companyLabels,"Modified_On")+' : '+modifiedElapTime+'</span>';
					}
					if(typeof(percentage_users)!="undefined" && percentage_users!="-" && percentage_users!=""){
						ui+='&nbsp;|&nbsp;<span>Progress: '+jsonData[i].user_percentage+' %</span>';
					}
					if((typeof(working_hours) != "undefined" && typeof(working_minutes) != "undefined" && working_hours!="-" && working_hours!="" && working_minutes!="-" && working_minutes!="")){
						ui+='&nbsp;|&nbsp;<span>Actual Time : '+working_hours+' h '+working_minutes+' m</span>';
					}
					ui+='</span></p>'
					+'<p id="conversationcontent" class="mb-2 text-dark" style="font-size: 15px;word-break:break-word;margin-top:-2px;">'+commentData+'</p>'
					if(activityfeed_type == "voice"){
						voiceno = commentData.lastIndexOf("/");
						filenamevoice =  $.trim(commentData.substr(voiceno));
						filenamevoice = filenamevoice.replace('/',"");
						ui+="<div id='audioArea_"+feedId+"' class='mb-2'>"
							+"<div class='d-flex align-items-center'>"
							//+"<img class='mr-1' id='pauseaudio_"+feedId+"' onclick='pauseAudio(this, "+feedId+", \"audio\")' src='images/conversation/pause.svg'  feedId='"+feedId+"'style='cursor:pointer;width:26px;height:26px;'>"
							+"<img src='images/conversation/play.svg' onclick='playAudio(this, "+feedId+", \"audio\")' class='vDoc_"+feedId+"' feedId='"+feedId+"' id='customPlayer_"+feedId+"' style='cursor:pointer;width:26px;height:26px;'>"
								+"<div class='timeline w-50' id='timeline_"+feedId+"' class='vDoc_"+feedId+"' onclick= \"stateChangeForActFeeds(event, 'timeline_"+feedId+"' , 'playhead_"+feedId+"' , 'audio_"+feedId+"')\">"
									+"<img id='playhead_"+feedId+"' src='images/conversation/sliderdot.svg' style='width: 15px;height: 15px;border-radius: 50%;top: -6px;position:absolute;'/>"
								+"</div>"
								+"<img id='vDownload_"+feedId+"' onclick=\"downloadActFileForVoice("+feedId+",'"+prjid+"','"+globalmenu+"','"+filenamevoice+"','"+commentPlace+"')\" class=\"Download_cLabelTitle vDoc_"+feedId+"\" title=\"\" style='cursor: pointer; float: right; height: 18px; width: 18px;margin-left:15px;' src='images/conversation/download.svg'>"
							+"</div>"
							+"<audio  src='"+lighttpdpath+""+$.trim(commentData)+"' id='audio_"+feedId+"' ontimeupdate='updateTimeSub("+feedId+")' onended='onVoiceEndedSub("+feedId+")' class='w-100 p-1'></audio>"
				      +"</div>"
					}else if(activityfeed_type == "file"){
						ext =  $.trim(commentData.substr(commentData.lastIndexOf(".")));
						extension = ext.split(".")[1];
						if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif" || ext.toLowerCase() == ".svg"){

							ui+= "<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_"+docId+"'>"
			    		    +"<div id='thumbnailOpenUi' class='defaultWordBreak float-left position-relative'>"
							
								+"<img id='uploadedImage' onclick=\"thumbnailOpenUi(this,"+docId+",'"+extension+"',"+feedId+");event.stopPropagation();\" ondblclick=\"expandImage("+docId+",'"+extension+"');event.stopPropagation();\"  class='float-left rounded vDoc_"+feedId+"' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='"+lighttpdpath+"//projectDocuments//"+docId+""+ext+"' id='view_"+docId+"' class='downCalIcon '>"
								
							+"</div>"
							+"</div>"	
						}else if(ext.toLowerCase() == ".mp3" || ext.toLowerCase() == ".wav" || ext.toLowerCase() == ".wma" || ext.toLowerCase() == ".mp4" || ext.toLowerCase() == ".m4v" || ext.toLowerCase() == ".mov" || ext.toLowerCase() == ".flv" || 
						  ext.toLowerCase() == ".f4v" || ext.toLowerCase() == ".ogg" || ext.toLowerCase() == ".ogv" || ext.toLowerCase() == ".wmv" ||
						  ext.toLowerCase() == ".vp6" || ext.toLowerCase() == ".vp5" || ext.toLowerCase() == ".mpg" || ext.toLowerCase() == ".avi" ||
						  ext.toLowerCase() == ".mpeg" || ext.toLowerCase() == ".webm"){
							

							ui+= "<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_"+docId+"'>"
			    		    +"<div id='videoOpenUi' onclick=\"openVideo("+docId+",'"+extension+"',"+prjid+");\" class='defaultWordBreak float-left position-relative border vDoc_"+feedId+"'>"
								+"<video id='uploadedVideo' class='float-left' style='cursor: pointer; height:80px; width:130px;background-color: #000;'  id='view_"+docId+"'><source src='"+lighttpdpath+"//projectDocuments//"+docId+"."+extension+"'></video>"
								+"<img class='videobutton' src='images/conversation/videoplay.svg' style='cursor:pointer;'>"
								//+"<img id='uploadedVideo' onclick=\"openVideo("+docId+",'"+ext+"',"+prjid+");\" class='float-left mt-1 downCalIcon' style='cursor: pointer; height:80px; width:100px;' src='"+lighttpdpath+"//projectDocuments//"+docId+"."+ext+"' id='view_"+docId+"'>"
								
							+"</div>"
							+"</div>"	

						}else{
							ext = ext.substr(ext.lastIndexOf(".")+1).toLowerCase();
							ui+= "<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_"+docId+"'>"
			    		    +"<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
							
								+"<img onclick=\"viewordownloadUi("+docId+",this,'"+extension+"',"+feedId+")\" class='float-left mt-1 vDoc_"+feedId+"' style='cursor: pointer;width:42px;' src='images/document/"+ext+".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_"+docId+"' class='downCalIcon '>"
								
							+"</div>"
							+"</div>"	
						}

						

					} 
				
				ui+='</div>'//div 3 ends&& menuType == 'activityFeed'
				if(taskPresentId != "0" && globalmenu == 'activityFeed' && convoTask != "convoTask"){
					
					ui+="<div class=\"\" style='align-self: end;'><img src=\"images/idea/task_nofill.svg\" id='listTaskoption_"+feedId+"' onclick=\"commentsTask("+feedId+",'listConvo');event.stopPropagation();\" class=\"pl-0 mr-1\" title=\"\" style=\"cursor:pointer;width:20px;height:20px;\" alt=\"Image\"></div>"
				}
				ui+= "<div id=\"moreoption\" class=\"actFeedHoverImgDiv position-relative mr-2\" style=\"cursor:pointer;\" ><img id=\"moreoptionimage_"+feedId+"\" onclick=\"\" style=\"width: 5px;\" src=\"images/conversation/three_dots.svg\" class=\"pl-0  d-none\" title=\"Options\" alt=\"Image\"></div>"
				
			+'</div>'//div 2 ends

			+"<div id=\"TaskListDiv_"+feedId+"\" class=\"\" style=\"\"></div>"
			    
			
			if(subCommentsData.length>0){
				if(jsonData[i].level == 2) countforreply++;
					ui+='<div id="deleteFeed_'+jsonData[i].subCommentsData[0].activityfeed_id+'">'			
					ui += prepareCommentsUI(jsonData[i].subCommentsData, menuType, menuTypeId, countforreply);
					ui+='</div>'
					countforreply = 0;
				
			}
			
			
			if(jsonData[i].level == 0  && jsonData[i].parent_feed_id==0 ){
				ui+='</div>'
			ui+='</div>'//div 1 ends
			}
			if(jsonData[i].level == 0  && !jsonData[i].parent_feed_id==0 && subCommentsData.length<1){

						ui+='</div>'	
			}

			

		}

		ui=ui.replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(26)",":").replaceAll("CHR(40)","\"");
		$('#loadingBar').addClass('d-none').removeClass('d-flex');
		return ui;
	}
	
	// function triggermoreoptions(feedId){
	// 	alert("---");
	// 	if($('#viewthumbnail').is(':visible') || $('#viewdownloadfileinconversation').is(':visible')){
			
	// 	}else{
	// 		showOptionsUi(47983,this,'text',4102);
	// 	}
	// }
	

	/* function prepareCommentsUI(jsonData,menuType,menuTypeId){
		if(notifStatus=="N"){
			$("#loadingBar").hide();
		}
		var i = 0;
    	limit = 20;
	    var j = limit;
		var UI = "";

		UI = commentsUiNew(jsonData,menuType,menuTypeId);
		

		//$('#latestFeed').val(latestFeed);
    	UI=UI.replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(26)",":").replaceAll("CHR(40)","\"");
    	return UI;

	} */
	
	function prepareCommentsUIold(jsonData,menuType,menuTypeId){	
			//alert("notifStatus::::::::comment::::::"+notifStatus);
		if(notifStatus=="N"){
			$("#loadingBar").hide();
		}
   		var UI = "";
    	var feedId = "";
    	var imgName = "";
    	var commentData = "";
    	var commentData2 = "";
    	var projId = ""; 
    	var displayDelete = "";
    	var userName = "";
    	var CreatedDate = "";
    	var CreatedTime = "";
    	var updatedBy = "";
    	var updatedDate = "";
    	var updatedTime = "";
    	var subCommentsFetchData="";
    	var createdById = "";
    	var activityfeed_type = "";
    	var blgType= "";
    	var duration = '';
    	var docId="";
    	var ImgExt = '';
    	var i = 0;
    	limit = 20;
	    var j = limit;
	    var no = '';
	    var ext = '';
	    var voiceno = '';
	    var filenamevoice = '';
	    var taskPresentId = "";
	    var replycomment="";
	    var latestFeed='';
		var createdElapTime='';
		var modifiedElapTime="";
	    if(j > jsonData.length)
	    j = jsonData.length;
	  	
		for (i = 0; i < j ; i++) {
          try{
            feedId = jsonData[i].activityfeed_id;
    		imgName = lighttpdpath+"/userimages/"+jsonData[i].user_image+"?"+d.getTime();///jsonData[i].imgName;
    		commentData = jsonData[i].activityfeed;
    		commentData = commentData.replaceAll("CHR(50)","'");
    		commentData = commentData.replaceAll("CH(70)","\\");
    		projId = jsonData[i].project_id; 
			if(typeof(projId) == "undefined"){
				projId = prjid;
			}
    		///displayDelete = jsonData[i].displayDelete;
    		userName = jsonData[i].name;
			if(typeof(userName)=="undefined"){
				userName = jsonData[i].userFullName;
			}
    		CreatedDate = jsonData[i].created_date;
    		CreatedTime = jsonData[i].created_time;
    		createdById = jsonData[i].user_id;
    		updatedBy = jsonData[i].upd_NAME;
    		updatedTime = jsonData[i].last_updated_time;
    		updatedDate = jsonData[i].last_updated_date;
    		activityfeed_type = jsonData[i].activityfeed_type;
    		//alert("feedId-----"+feedId);
    		//blgType = jsonData[i].blog_post_type;
    		docId = jsonData[i].doc_id;
    		///ImgExt = jsonData[i].ImageExt;
    		commentData2 = '';
    		taskPresentId = jsonData[i].taskForActFeed;
    		latestFeed = jsonData[i].latestFeed; 
    		
			if(activityfeed_type=="file"){
				ImgExt = imgicon(commentData);
			}
			
    		if(typeof(activityfeed_type) == "undefined")
    			activityfeed_type="text";


			if(typeof(jsonData[i].created_elapsed_time) != "undefined"){
				createdElapTime = jsonData[i].created_elapsed_time; 
				createdElapTime = calculateElapseTime(createdElapTime);
			}else{
				createdElapTime = CreatedTime;
			}
				
			if(typeof(jsonData[i].modified_elapsed_time) != "undefined"){
				modifiedElapTime = jsonData[i].modified_elapsed_time; 
				modifiedElapTime = calculateElapseTime(modifiedElapTime);
			}else{
				modifiedElapTime = updatedTime;
			}
			
    	if(activityfeed_type == "voice"){ 
			voiceno = commentData.lastIndexOf("/");
    		filenamevoice =  $.trim(commentData.substr(voiceno));
    		filenamevoice = filenamevoice.replace('/',"");
			
			commentData2 +=  "<div id='audioArea_"+feedId+"' style='margin-top:8px;'>"
				        +"<div style='position: absolute;'>"
				            +"<img src='images/play.png' onclick='playAudio(this, "+feedId+", \"audio\")' feedId='"+feedId+"' id='customPlayer_"+feedId+"' style='cursor:pointer;'>"
				            +"<img onclick=\"downloadActFileForVoice("+feedId+",'"+projId+"','"+globalmenu+"','"+filenamevoice+"','"+commentPlace+"')\" class=\"Download_cLabelTitle\" title=\"\" style='cursor: pointer; float: right; height: 18px; width: 18px;margin-left:15px;' src='images/downloading.png'>"
				            	+"<div class='timeline' id='timeline_"+feedId+"' onclick= \"stateChangeForActFeeds(event, 'timeline_"+feedId+"' , 'playhead_"+feedId+"' , 'audio_"+feedId+"')\">"
			               		+"<div id='playhead_"+feedId+"' style='width: 15px;height: 15px;border-radius: 50%;margin-top: -6px;background: #64696f;'></div>"
			                   +"</div>"
				        +"</div>"
				        +"<audio  src='"+lighttpdpath+""+$.trim(commentData)+"' id='audio_"+feedId+"' ontimeupdate='updateTimeSub("+feedId+")' onended='onVoiceEndedSub("+feedId+")' style='width: 100%; height: 100%; padding: 0.7%;'></audio>"
				        +"<div style='padding: 1%; float: right; margin-right: 22%;'></div>"
				      +"</div>";
		 
    	 }else if(activityfeed_type == "file"){
    		 no = commentData.lastIndexOf(".");
    		 ext =  $.trim(commentData.substr(no));
			commentData2 += "<div style='margin-right: 0px; margin-left: 0px; width: 50%; border-bottom: 0px none; height: 25px;' class='docListViewBorderCls' id='doc_"+docId+"'>"
			    		    +"<div style='float:left;padding: 10px 0 0 0;width: 57.7%;' class='defaultWordBreak'>"
			    		    if(ext.toLowerCase() == ".mp3" || ext.toLowerCase() == ".wav" || ext.toLowerCase() == ".wma" || ext.toLowerCase() == ".mp4" || ext.toLowerCase() == ".m4v" || ext.toLowerCase() == ".mov" || ext.toLowerCase() == ".flv" || 
		    		    		    ext.toLowerCase() == ".f4v" || ext.toLowerCase() == ".ogg" || ext.toLowerCase() == ".ogv" || ext.toLowerCase() == ".wmv" ||
		    		    		    ext.toLowerCase() == ".vp6" || ext.toLowerCase() == ".vp5" || ext.toLowerCase() == ".mpg" || ext.toLowerCase() == ".avi" ||
		    		    		    ext.toLowerCase() == ".mpeg" || ext.toLowerCase() == ".webm"){
		    		    	   		ext = ext.replace('.', "");
		    		    	   		commentData2 +="<img onclick=\"openVideo("+docId+",'"+ext+"',"+projId+")\" style='cursor: pointer; float: left; margin-top: -5px; height: 20px; width: 20px; margin-right: 6px;' src='images/repository/"+ImgExt+"' id='view_"+docId+"' class='downCalIcon'>"
		    		    	   		+"<span style='float: left; margin-top: -3px; margin-left: 2px; display: none;'>:&nbsp;</span>"
					    		    +"<span style='float: left; max-width: 75%; margin-top: -3px;' onclick=\"openVideo("+docId+",'"+ext+"',"+projId+")\" class='defaultExceedCls' id='docFullNameTaskLvl_"+docId+"'>"+commentData+"</span>"
					    		    +"<img onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\" title=\"Download\" style='cursor: pointer; float: left; margin-top: -5px; height: 18px; width: 18px; margin-left: 10px;' src='images/downloading.png'>"
					    	  +"</div>"
  			    		    }else{
  			    		    	commentData2 +="<img onclick='veiwOrdownloadTask(this)' style='cursor: pointer; float: left; margin-top: -5px; height: 20px; width: 20px;' src='images/repository/"+ImgExt+"' id='view_"+docId+"' class='downCalIcon'>"
  			    		    				 +"<span style='float: left; margin-top: -3px; margin-left: 2px;'>:&nbsp;</span>"
  			    		    				 +"<span style='float: left; width: 75%; margin-top: -3px;' onclick='veiwOrdownloadTask(this)' class='defaultExceedCls' id='docFullNameTaskLvl_"+docId+"'>"+commentData+"</span>"
			    		     +"</div>"
  			    		    }
			    		     /*
			    		     +"<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 5em; margin-top: -1%; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
			    		       +"<div style='margin-top:1px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
			    		         +"<img src='images/repository/arrowleft.png' style='height:23px'>"
			    		       +"</div>";
			    		       */			    		       
		   if(ext.toLowerCase() == ".csv"){  		       
		   commentData2 += "<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 4em; margin-top: -1%; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
			    		       +"<div style='margin-top:1px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
			    		         +"<img src='images/repository/arrowleft.png' style='height:23px'>"
			    		       +"</div>"			    		      	    		       
			    		        +"<div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\">"
			    		           + "<div class='OptionsFont'>Download</div></div></div>"
			    		        + "</div>";
			    		       }else if(menuType=="Task"){
    	   commentData2 +=  "<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 5em; margin-top: 2px; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
	       					  +"<div style='margin-top:10px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
	       						  +"<img src='images/repository/arrowleft.png' style='height:23px'>"
	       					  +"</div>";
			    	   if(projId == "0"){
								if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif"){
									commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//uploadedDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
								}else{   			    	
									commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdpath+"//uploadedDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
								}
							}else{
								if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif"){
									commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
								}else{   			    	
									commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
								}
							}
	      commentData2 += "<div class='OptionsFont'>View</div></a></div>"
	                       +"<div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\">"
	                       	+ "<div class='OptionsFont'>Download</div></div></div>"
	                       		+ "</div>";    		    	   
			    		    }else{
		  commentData2 +=  "<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 5em; margin-top: -1%; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
			    		       +"<div style='margin-top:1px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
			    		         +"<img src='images/repository/arrowleft.png' style='height:23px'>"
			    		       +"</div>";
			    		       if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif"){
		  			    		 commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
			    		       }else{   			    	
		  	  				     commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
		  	  				    }
    	       commentData2 += "<div class='OptionsFont'>View</div></a></div>"
    	                       +"<div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\">"
			    		    + "<div class='OptionsFont'>Download</div></div></div>"
			    		+ "</div>";
			    		       }
			    		      /* 
			    		       if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif" || ext == ".mp3" || ext == ".mp4"){
		  			    		 commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
			    		       }else{   
			    	
		  	  				     commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdPath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
		  	  				    }
    	       commentData2 += "<div class='OptionsFont'>View</div></a></div>"
    	                       +"<div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick='downloadActFile("+docId+","+projId+")'>"
			    		    + "<div class='OptionsFont'>Download</div></div></div>"
			    		+ "</div>";
    		 */
    	 }else{
    	  commentData2 = commentData;
    	 }
    	
    	UI += "<div class=\"col-sm-12 col-xs-12 actFeedTot\" id=\"actFeedMainDiv_"+feedId+"\"  style=\"padding: 15px 0px 12px 0px; border-bottom: 1px solid #c1c5c8;\">"
    	 	//+"<div id = \"actFeedTot_"+feedId+"\" class=\"col-sm-12 col-xs-12 actFeedHover actualComments\" style=\"width: 100%; padding: 2px 0px 0px 8px;\" >"
    	 		if(menuType=="Task"){
       	         UI  +="<div id = \"actFeedTot_"+feedId+"\" class=\"col-sm-12 col-xs-12 actFeedHover actualComments\" style=\"width: 98%; padding: 2px 0px 0px 8px;\" >"
       	        }else{
       	            UI  +="<div id = \"actFeedTot_"+feedId+"\" class=\"col-sm-12 col-xs-12 actFeedHover actualComments\" style=\"width: 100%; padding: 2px 0px 0px 8px;\" >"
     			}
    	 		if(menuType=="galleryComment"){
    	         UI  +="<div id = \"actFeedImg_"+feedId+"\" class='col-sm-1 col-md-1 col-xs-1 hideDivsOnEdit actualComments mainCmtUicon' style=\"height:50px;float: left;padding-left: 0px;max-width: 50px;min-width: 50px;padding-right: 0px;margin: 5px 0px;\">"
    	        }else{
    	            UI  +="<div id = \"actFeedImg_"+feedId+"\" class='col-sm-1 col-md-1 col-xs-1  hideDivsOnEdit actualComments mainCmtUicon' style=\"height:50px;float: left;padding-left: 0px;max-width: 50px;min-width: 50px;padding-right: 0px;margin: 5px 0px;\">"
  				}
    	        UI+="<img src=\""+imgName+"\" title=\""+userName+"\" onerror=\"userImageOnErrorReplace(this);\"  class=\"img-responsive actFeedUserIcon userIcon_"+createdById+"\"  alt=\"Image\" style=\"height: 45px;width: 45px;border-radius:50%;\" >"
		        +"</div>"
		       	if(menuType=="galleryComment"){
			       	 if ((screen.width==1024) && (screen.height>=600)){
			       	   if($('div#gCommentDiv').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
			       	     UI+="<div id=\"actFeedDiv_"+feedId+"\" ondblclick = \"AddSubComments("+feedId+",'"+menuType+"')\" class=\"col-sm-11 col-xs-10 col-md-10 col-lg-11 actualComments\" style=\"padding:5px 0px 5px 15px;cursor: pointer;float:left;\">"
			       	   }else{
			       	     UI+="<div id=\"actFeedDiv_"+feedId+"\" ondblclick = \"AddSubComments("+feedId+",'"+menuType+"')\" class=\"col-sm-11 col-xs-10 col-md-10 col-lg-11 actualComments\" style=\"padding:5px 0px 5px 15px;cursor: pointer;width:82%;float:left;\">"
			       	   }
			       	 }else{
			       	   UI+="<div id=\"actFeedDiv_"+feedId+"\" ondblclick = \"AddSubComments("+feedId+",'"+menuType+"')\" class=\"col-sm-11 col-xs-10 col-md-10 col-lg-11 actualComments\" style=\"padding:5px 0px 5px 15px;cursor: pointer;width:82%;float:left;\">"
			       	 }
		        }else{
		            UI+="<div id=\"actFeedDiv_"+feedId+"\" ondblclick = \"AddSubComments("+feedId+",'"+menuType+"')\" class=\"col-sm-11 col-xs-10 col-md-10 col-lg-11 actualComments\" style=\"padding:5px 0px 5px 15px;cursor: pointer;float:left;\">"
		        }
    	        /*UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;&nbsp;"+CreatedTime+"</div>";*/
		        UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;|&nbsp;<span class=\"Posted_on_cLabelText\">"+getValues(companyLabels,"Posted_on")+"</span>&nbsp;:&nbsp;<span title='"+CreatedTime+"'>"+createdElapTime+"</span>";
		        if(updatedTime!="-"){
		           UI+="&nbsp;|&nbsp;<span class=\"Modified_On_cLabelText\">"+getValues(companyLabels,"Modified_On")+"</span>&nbsp;:&nbsp;<span class='modifData' title='"+updatedTime+"'>"+modifiedElapTime+"</span>";
		        }
		        UI+="</div>" 
		        	if(activityfeed_type == "file" && (menuType == "activityFeed" || menuType == "activityfeed" || menuType == "Task")){
		        		UI+="<div style=\"font-size:14px;float:left\" class=\"actFeed\">File "+commentData+" uploaded by "+userName+" on "+CreatedTime+"</div>"
		        	}else if(activityfeed_type == "voice" && (menuType == "activityFeed" || menuType == "activityfeed" || menuType == "Task")){
		        		UI+="<div style=\"font-size:14px;float:left\" class=\"actFeed\">Audio "+commentData+" uploaded by "+userName+" on "+CreatedTime+"</div><br/>"
		        	}
		        		 
		        UI+="<div class=\"actFeed\" >"+commentData2+"</div>" 
		        		
		        		+"</div>"
		        
		        
			   	UI += "<div class='hideDivsOnEdit actFeedHoverImgDiv' id=\"subcommentDiv_"+feedId+"\" align=\"right\" style=\"float:right;padding:10px 0px;margin-right:5px;\" onclick=\"replyDivData(this);\" >"
			       +" <img src=\"images/more.png\" class=\"img-responsive margin\" title=\""+getValues(companyLabels,"Options")+"\" style=\"padding-left:0px;cursor:pointer;\" alt=\"Image\">"
			    +"</div>" 
			    if(taskPresentId != "0" && globalmenu == 'activityFeed'){
		        	UI+="<div class = \"tasksIcon\" style=\"width: 25px;float: right;margin-top: 10px;margin-right: 25px;position: absolute;right: 0px;\">"
		        		+"<img id=\"tIcon_"+feedId+"\" style=\"cursor:pointer;\" title=\"View Tasks\" src=\"images/task/taskdropdown.svg\" onclick=\"showConversationTasks(this, "+feedId+")\">"
		     		+"</div>"
		        }
		        
			    UI+="<div id=\"subcommentDiv1_"+feedId+"\" class=\"actFeedOptionsDiv\" > "
		        +"<div style=\"margin-top: -2px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\"><img src=\"images/arrow.png\"></div>"
		        +"<div id=\"rmhover\" onclick=\"AddSubComments("+feedId+",'"+menuType+"');\" style=\"width:99%;height:auto;padding:0 0 3px ;cursor:pointer;float:left\"> <span class=\"Reply_cLabelText\" style=\"float: left; height: 20px;margin-left: 5px; overflow: hidden; width: 60px;\">"+getValues(companyLabels,"Reply")+"</span></div>"
			   if(userIdglb==createdById  && activityfeed_type == "text"){
			     UI +="<div onclick=\"EditActComments("+feedId+",'"+menuType+"',"+menuTypeId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"Edit_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">"+getValues(companyLabels,"Edit")+"</span></div>"
			    }
			    if(menuType=="wsDocument"){
			       if(userIdglb==createdById){
				      UI+="<div onclick=\"shareRepocomments("+feedId+","+menuTypeId+","+feedId+",'');\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Share</span></div>"
				    }else{
				       UI+="<div onclick=\"shareRepocomments("+feedId+","+menuTypeId+","+feedId+",'shared');\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Share</span></div>"
				   }
			    }
			    if((menuType=="activityFeed" || menuType=="activityfeed") && (activityfeed_type == "text" || activityfeed_type == "file" || activityfeed_type == "voice")){
			    	 //    UI +="<div onclick=\"createTaskActFeed("+feedId+","+projId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"Task_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Task</span></div>"
			  	 	UI +="<div onclick=\"newUicreateTaskUI('','0','Tasks',"+feedId+",'','','','','convoTask');\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"Task_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Task</span></div>"
			 	} //createTaskUI('','Tasks', "+feedId+", '', '','ActFeedFrCreateTask', '"+feedId+"');
			//newUicreateTaskUI('','0','Tasks',"+feedId+",'','Conversation Task','','','convoTask');
			     
			  
	   
		if((userIdglb==createdById)||(projectUsersStatus=="PO")){
			UI += "<div onclick=\"deleteMainActFeed("+feedId+",'"+projId+"','"+menuType+"',"+menuTypeId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"><span class=\"Delete_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">"+getValues(companyLabels,"Delete")+"</span></div>"
		}
	   	UI+="</div> "
	         +"</div>"
	         if(menuType == "Task"){
	        	 UI+=" <div id=\"replyDivContainer_"+feedId+"\" class=\"row replycommentDiv\" style=\"margin-left: 0px; margin-right: 0px;padding-left:5px; background: white none repeat scroll 0% 0%; margin-bottom: 10px;margin-top:5px;width:98%; float:left;display:none;\">"
	         }else{
	        	 UI+=" <div id=\"replyDivContainer_"+feedId+"\" class=\"row replycommentDiv\" style=\"margin-left: 0px; margin-right: 0px;padding-left:5px; background: white none repeat scroll 0% 0%; margin-bottom: 10px;margin-top:5px;width:100%; float:left;display:none;\">"
	         }
	   			UI+=" <div class=\"col-sm-12 col-xs-12\" style=\"height:45px;padding-left:0px; padding-right:0px;position:static;\">"
				 if(menuType == "galleryComment"){
				UI+=" <textarea id=\"replyBlock_"+feedId+"\" class=\"main_commentTxtArea replyCmntDiv\" style=\"width:83%;\"  onkeyup=\"enterKeyValidreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+",'keyup');\" onkeypress=\"enterKeyValidreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+",'keypress');\"></textarea>"
						+"<div  class=\"projUserListMain proTagReplyNameId\">"
		        			+ "<ul class=\"projuserList\"></ul>"
		      			+"</div>"
		      			+" <div  class=\"activityfieldUserListMain activityfieldReplyUserList\">"
							+" 		<ul class=\"activityfielduserList\"></ul>"
						+" </div>"
			}else if(menuType == "activityFeed"){
				 UI+=" <textarea id=\"replyBlock_"+feedId+"\" class=\"main_commentTxtArea replyCmntDiv\"  onkeyup=\"enterKeyValidreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+",'keyup');\" onkeypress=\"enterKeyValidreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+",'keypress');\"></textarea>"
				 
		        +"<div class='main_commentTxtArea' id='audioContainerSub_"+feedId+"' style='display:none;'>"
			     +"<div id='audioAreaSub_"+feedId+"' style='display:none;'>"
			        +"<div style='position: absolute; padding: 0.7%; margin-left: 28%;'>"
			            +"<img src='images/play.png' onclick='playAudio(this,"+feedId+", \"preview\")' id='customPlayerSub_"+feedId+"' style='cursor:pointer;'>"
			            	+"<div class='timeline' id='timelineSub_"+feedId+"' onclick=\"stateChangeForActFeeds(event , 'timelineSub_"+feedId+"', 'playheadSub_"+feedId+"', 'audioSub_"+feedId+"')\" >"
	                    		+"<div id='playheadSub_"+feedId+"' style='width: 15px;height: 15px;border-radius: 50%;margin-top: -7px;background: #64696f;'></div>"
		                    +"</div>"
			        +"</div>"
			        +"<audio  src='' id='audioSub_"+feedId+"' ontimeupdate='updateTimeSubReply("+feedId+")' onended='onVoiceEndedSubReply("+feedId+")' style='width: 100%; height: 100%; padding: 0.7%;'></audio>"
			         
			         +"<div style='padding: 1%; float: right; margin-right: 37%;'><img src='images/circle.png' onclick='cancelVoiceSub("+feedId+")' style='width: 20px;cursor:pointer;' id=''></div>"
			      +"</div>"
			      
			      +"<div class='timer' style='float: right; display: block; height: 60%; margin-top: 1%; width: 18%;'>"
			         +"<label style='color:red; float: left;'>Recording</label>"
			         +"<div style='font-size: 14px; float: left; padding-left: 11%;' class='timerSpan'></div>"
			      +"</div> "
		       +"</div>"
				 
				 
			    +"<div  class=\"projUserListMain proTagReplyNameId\">"
		        + "<ul class=\"projuserList\"></ul>"
		      +"</div>"
		      +"<div  class=\"activityfieldUserListMain activityfieldReplyUserList\">"
		        + "<ul class=\"activityfielduserList\"></ul>"
		      +"</div>"
			}else{
				 UI+=" <textarea id=\"replyBlock_"+feedId+"\" class=\"main_commentTxtArea replyCmntDiv\"  onkeyup=\"enterKeyValidreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+",'keyup');\" onkeypress=\"enterKeyValidreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+",'keypress');\"></textarea>"
				 
			        +"<div class='main_commentTxtArea' id='audioContainerSub_"+feedId+"' style='display:none;'>"
				     +"<div id='audioAreaSub_"+feedId+"' style='display:none;'>"
				        +"<div style='position: absolute; padding: 0.7%; margin-left: 21%;'>"
				            +"<img src='images/play.png' onclick='playAudio(this,"+feedId+", \"preview\")' id='customPlayerSub_"+feedId+"' style='cursor:pointer;'>"
				            	+"<div class='timeline' id='timelineSub_"+feedId+"' onclick=\"stateChangeForActFeeds(event , 'timelineSub_"+feedId+"', 'playheadSub_"+feedId+"', 'audioSub_"+feedId+"')\" >"
		                    		+"<div id='playheadSub_"+feedId+"' style='width: 15px;height: 15px;border-radius: 50%;margin-top: -7px;background: #64696f;'></div>"
			                    +"</div>"
				        +"</div>"
				        +"<audio  src='' id='audioSub_"+feedId+"' ontimeupdate='updateTimeSubReply("+feedId+")' onended='onVoiceEndedSubReply("+feedId+")' style='width: 100%; height: 100%; padding: 0.7%;'></audio>"
				         
				         +"<div style='padding: 1%; float: right; margin-right: 37%;'><img src='images/circle.png' onclick='cancelVoiceSub("+feedId+")' style='width: 20px;cursor:pointer;' id=''></div>"
				      +"</div>"
				      
				      +"<div class='timer' style='float: right; display: block; height: 60%; margin-top: 1%; width: 18%;'>"
				         +"<label style='color:red; float: left;'>Recording</label>"
				         +"<div style='font-size: 14px; float: left; padding-left: 11%;' class='timerSpan'></div>"
				      +"</div> "
			       +"</div>"
					 
					 
				    +"<div  class=\"projUserListMain proTagReplyNameId\">"
			        + "<ul class=\"projuserList\"></ul>"
			      +"</div>"
			      +"<div  class=\"activityfieldUserListMain activityfieldReplyUserList\">"
			        + "<ul class=\"activityfielduserList\"></ul>"
			      +"</div>"
				
			}
			   if(menuType == "galleryComment"){
			       UI+="   <div align='center' style=\"width:8%;\" id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			     }else{
			       UI+="   <div align='center' id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			     }
			UI+="<div style=\"width: 42%;float: left;display:none;\"  onclick=\"closeReplyComment('"+feedId+"');\"><img class=\"img-responsive\" src=\"images/workspace/remove.png\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" alt=\"Image\"></div>"
				       +"  <div style=\"width: 55%;\" id =\"replyFeed_"+feedId+"\" onclick=\"replyActComments('"+feedId+"', '"+userIdglb+"', '"+projId+"','"+menuType+"','"+menuTypeId+"');\" ><img class=\"img-responsive\" title=\"Post\" src=\"images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\"></div>"
				       +"</div>"
				       if(menuType == "galleryComment"){
			      UI+="   <div align=\"center\" style=\"width:8%;\" class=\"main_commentTxtArea_optDiv\">"
			    }else{
			       UI+="   <div align=\"center\" class=\"main_commentTxtArea_optDiv\">"
			    }
			if(menuType == "blog"){
				UI+="<div align=\"right\" onclick=\"showActFeedMore("+feedId+","+menuTypeId+",'"+menuType+"')\"  class = \"actFeedMore\" style=\"float: right; width: 40%;display:none;\">"
	             +"<img alt=\"Image\" title=\"Options\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/more.png\" class=\"img-responsive\">"
	         +"</div>"
	         +"<div class= \"mainActfeed_OptPopDiv\"  id=\"actFeedOptPopDiv_"+feedId+"\" style=\"right:16px;\">"            
	            +" <div style=\"position: absolute; float: left; margin-top: -2px;\" class=\"workSpace_arrow_right\">"
	            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(0deg); width: 10px; height: 20px;\">"
	            +" </div>"         
	            +" <div onclick=\"initWebcam("+feedId+","+menuTypeId+",'"+projId+"','"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;\" > "
	            +"   <img style=\"border: medium none;\" src=\"images/takepicture.png\" class=\"imgCss\">"
	            +" </div>"          
	            +" <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
		           // +"<form action='"+path+"/WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
			         // +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
			       // +"</form>"
	            //+"   <img style=\"border: medium none;\" onclick='uploadDoc("+feedId+")' src=\""+path+"/images/upload.png\" class=\"imgCss uploadFile\">"
	           //+"   <img style=\"border: medium none;\" src=\""+path+"/images/upload.png\" class=\"imgCss uploadFile\">"
	            +" </div> "        
	          +"</div>"
	          //+"<div class = 'recAudioMsg' onclick='replyVoiceFeed(this,"+feedId+");' style=\"float: right; width: 40%;\">"
	        //  +"<div class = 'recAudioMsg' onclick='replyVoiceFeed(this,"+feedId+");' style=\"width: 40%;\">"
	          +"<div class = 'recAudioMsg' title=\""+getValues(companyLabels,"Rec_audio")+"\" onclick='replyVoiceFeed(this,"+feedId+");' style=\"margin-right: 15px;margin-left: 7px;\">"
	          +"  <img alt=\"Image\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/record.png\" class=\"img-responsive\">"
	          +"</div>"
	       +"</div>"
	    +"  </div>"	     
+" </div> "
  +"<div  id=\"EditCmtDivContainer_"+feedId+"\" style=\"height:45px;padding-left:0px; padding-right:0px;display : none;\" class=\"col-sm-12 col-xs-12  editTextArea\">"
  +"<div  class=\"projUserListMain proTagReplyNameId\">"
   + "<ul class=\"projuserList\"></ul>"
  	+"</div>" 
  +"<div  class=\"activityfieldUserListMain activityfieldReplyUserList\">"
   + "<ul class=\"activityfielduserList\"></ul>"
 	+"</div>"
			    }else{			      			    
			        // UI+="<div align=\"right\" onclick=\"showActFeedMore2("+feedId+")\"  class = \"actFeedMore\" style=\"float: right; width: 40%;\">"
			        if(menuType == "document" || menuType == "galleryComment"){ 
			        	UI+="<div align=\"right\" onclick=\"showActFeedMore("+feedId+","+menuTypeId+",'"+commentPlace+"')\"  class = \"actFeedMore\" style=\"float: right; width: 43%;display:none;\">"
				             +"<img alt=\"Image\" title=\"Options\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/more.png\" class=\"img-responsive\">"
				         +"</div>";
			        }else{
			        	 UI+="<div align=\"right\" onclick=\"showActFeedMore("+feedId+","+menuTypeId+",'"+commentPlace+"')\"  class = \"actFeedMore\" style=\"float: right; width: 43%;\">"
			             	+"<img alt=\"Image\" title=\"Options\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/more.png\" class=\"img-responsive\">"
			             +"</div>";
			         }
			         if(menuType == 'blog'){
			        UI+="<div class= \"mainActfeed_OptPopDiv\"  id=\"actFeedOptPopDiv_"+feedId+"\" style=\" margin-top: -6em;right:0px\" >"            
				            +" <div style=\"position: absolute; float: left; margin-top: -2px;\" class=\"workSpace_arrow_right\">"
				            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(180deg); width: 10px; height: 20px;\">"
				            +" </div>";
			         }if(menuType == 'Task'){
			        	 UI+= "<div class= \"mainActfeed_OptPopDiv\"  id=\"actFeedOptPopDiv_"+feedId+"\" style=\"right: 36px;\">"            
				            +" <div style=\"position: absolute; float: left; margin-top: -2px;\" class=\"workSpace_arrow_right\">"
				            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(0deg); width: 10px; height: 20px;\">"
				            +" </div>";
			         }else{
			        	 UI+= "<div class= \"mainActfeed_OptPopDiv\"  id=\"actFeedOptPopDiv_"+feedId+"\" >"            
				            +" <div style=\"position: absolute; float: left; margin-top: -2px;\" class=\"workSpace_arrow_right\">"
				            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(0deg); width: 10px; height: 20px;\">"
				            +" </div>"; 
			         }
				        
			         if(menuType == 'document'){
			        	 UI+=" <div onclick=\"initWebcam("+feedId+","+menuTypeId+",'','"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;\" > ";
			         }else if(menuType == 'activityFeed' || menuType == 'activityfeed'){
			        	UI+= " <div onclick=\"initWebcam("+feedId+",'',"+projId+",'"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\" > ";
			         }else if(menuType == 'Task' || menuType == 'wsIdea' || menuType == 'agile'){
			        	 UI+= " <div onclick=\"initWebcam("+feedId+","+pgId+","+projId+",'"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\" > ";
			         }else{
			        	 UI+= " <div onclick=\"initWebcam("+feedId+","+pgId+","+projId+",'"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;\" > ";
			         }
				            
				        UI+="   <img style=\"border: medium none;\" src=\"images/takepicture.png\" class=\"imgCss\">"
				            +" </div>";
				      if(menuType == 'activityFeed' || menuType == 'activityfeed' || menuType == 'Task' || menuType == 'wsIdea' || menuType == 'agile'){
				    	UI+=  " <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
				            +"<form action='WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
					          +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
					        +"</form>"
					        +"<img style=\"border: medium none;\" onclick='uploadDoc("+feedId+")' title=\"Upload\" src=\"images/upload.png\" class=\"imgCss uploadFile\">";
				      }else{
				    	  UI+= " <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
				           // +"<form action='WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
					         // +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
					        //+"</form>"
				      }      
				            //+" <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
					          //  +"<form action='"+path+"/WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
						         // +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
						       // +"</form>"
				            //UI+="   <img style=\"border: medium none;\" onclick='uploadDoc("+feedId+")' src=\""+path+"/images/upload.png\" class=\"imgCss uploadFile\">"
				            UI+= " </div> "        
				          +"</div>"
				          if(menuType == 'galleryComment'){
				        	  UI+="<div class = 'recAudioMsg' title=\""+getValues(companyLabels,"Rec_audio")+"\" onclick='replyVoiceFeed(this,"+feedId+");' style=\"float: right; width: 50%;height: 36px; margin-top: 5px;  padding-right: 2px;display:none;\">"
					          +"  <img alt=\"Image\" style=\"margin-top: 5px;cursor:pointer;\" src=\"images/record.png\" class=\"img-responsive\">"
					          +"</div>"
				          }else{
				        	  UI+="<div class = 'recAudioMsg' title=\""+getValues(companyLabels,"Rec_audio")+"\" onclick='replyVoiceFeed(this,"+feedId+");' style=\"float: right; width: 50%;height: 36px; margin-top: 5px;  padding-right: 2px;\">"
					          +"  <img alt=\"Image\" style=\"margin-top: 5px;cursor:pointer;\" src=\"images/record.png\" class=\"img-responsive\">"
					          +"</div>"
				          }
				            UI+="</div>"
				    +"  </div>"
				     
			 +" </div> "
			   +"<div  id=\"EditCmtDivContainer_"+feedId+"\" style=\"height:45px;padding-left:0px; padding-right:0px;display : none;\" class=\"col-sm-12 col-xs-12  editTextArea\">"
			   +"<div  class=\"projUserListMain proTagReplyNameId\">"
		       + "<ul class=\"projuserList\"></ul>"
		       +"</div>" 
			   +"<div  class=\"activityfieldUserListMain activityfieldReplyUserList\">"
		       + "<ul class=\"activityfielduserList\"></ul>"
		       +"</div>"
			}
			    if(menuType=="galleryComment"){
			       UI+="<textarea id=\"EdidCmtBlock_"+feedId+"\" onkeyup=\"specialCharAt(event,"+feedId+",'"+menuType+"','keyup');\" onkeypress=\"specialCharAt(event,"+feedId+",'"+menuType+"','keypress');\" class=\"main_commentTxtArea editConvoClass\" style=\"width:93%;\" id=\"main_commentTextarea\"></textarea><div align='center' id=\"topDiv_"+feedId+"\" style=\"width:7%;\" class=\"main_commentTxtArea_btnDiv\" >"
			    }else{
			       UI+="<textarea id=\"EdidCmtBlock_"+feedId+"\" onkeyup=\"specialCharAt(event,"+feedId+",'"+menuType+"','keyup');\" onkeypress=\"specialCharAt(event,"+feedId+",'"+menuType+"','keypress');\" class=\"main_commentTxtArea editConvoClass\" style=\"width:96%;\" id=\"main_commentTextarea\"></textarea><div align='center' id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			    }
			    
			    UI+="<div onclick=\"HideMainFeedData()\" style=\"display:none;width: 42%;float: left;\"><img alt=\"Image\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" src=\"images/workspace/remove.png\" class=\"img-responsive\"></div>"
			   if(menuType=="galleryComment"){
			      UI+="<div id=\"pstFeed_"+feedId+"\" onclick=\"postEditedCmt('"+feedId+"', '"+userIdglb+"', '"+projId+"','"+menuType+"',"+menuTypeId+");\" style=\"width: 70%;\"><img alt=\"Image\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/workspace/post.png\" class=\"img-responsive\"></div>"
			   }else{
			      UI+="<div id=\"pstFeed_"+feedId+"\" onclick=\"postEditedCmt('"+feedId+"', '"+userIdglb+"', '"+projId+"','"+menuType+"',"+menuTypeId+");\" style=\"width: 55%;\"><img alt=\"Image\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/workspace/post.png\" class=\"img-responsive\"></div>"
			   }
			   UI+="</div>"
			    +"</div>"
				+"<div id=\"createTaskListUiDiv_"+feedId+"\" class=\"col-sm-12 col-xs-12\" style=\"border-radius: 5px;padding-left:0px; padding-right:0px;margin-top:2px;\"></div>"
			    +"<div id=\"actFeedTaskDiv_"+feedId+"\" class=\"col-sm-12 col-xs-12 taskShow\" style=\"border-radius: 5px;padding-left:0px; padding-right:0px;margin-top:2px;\"></div>"
			    +"<div id=\"actFeedSubDiv_"+feedId+"\" class=\"col-sm-12 col-xs-12\" style=\"border-radius: 5px;padding-left:0px; padding-right:0px;\">"
			   
		if(jsonData[i].subCommentsData.length>0){
		   
	    	UI += prepareUISubFeeds(jsonData[i].subCommentsData,menuType,menuTypeId,feedId);
	    }
		UI += "</div>"
				+"</div>";  
		
			feedID = feedId;
			LoginId = userIdglb;
			Proje	= projId;
			MenuTyp = menuType;
			MenuTypId = menuTypeId;
			feedID="";
			
			}catch(e){
    	  console.log(e);
    	}
    	}
    	
    	$('#latestFeed').val(latestFeed);
    	UI=UI.replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(26)",":").replaceAll("CHR(40)","\"");
    	return UI;
    }
 
	
	
	
	/*New function here for voicefeed downloads*/
	
	function downloadActFileForVoice(docId,projId,menuType,filenamevoice,commentPlace){
		///URLForDownLoad = path+"/WorkspaceDocumentAction.do?act=downloadfileforvoice&docId="+docId+"&projId="+projId+"&menuType="+menuType+"&filenamevoice="+filenamevoice+"&commentPlace="+commentPlace+"";
		var downloadpath="";
		
		if(menuType == "activityFeed"){
			downloadpath="conversationMedias";
		}else if(menuType == "Task"){
			downloadpath="conversationMedias/tasksMedias";
		}
		
		URLForDownLoad = apiPath+"/"+myk+"/v1/download?fileName="+filenamevoice+"&Path="+downloadpath+"";
		window.open(URLForDownLoad);
		//parent.confirmFun(getValues(companyAlerts,"Alert_Download_Doc"),'clear','dowloadActFile');
		
	}
	
 
	
	var k = 0;
   	function prepareUISubFeeds(jsonDataSub,menuType,menuTypeId,superParentId) {
   		//alert("prepareUISubFeeds menuType---"+menuType);
		var UI = "";
   	    k = jsonDataSub.length;
   	    var createdById = '';
   	    var feedId ='';
   	    var imgName = '';
   	    var sub_commentData='';
   	    var projId='';
   	    var userName = '';
   	    var CreatedDate='';
   	    var CreatedTime='';
   	    var updatedBy = "";
    	var updatedDate = "";
    	var updatedTime = "";
   	    var pId='';
   	    var latestFeed='';
   	    var activityfeed_type = "";
   	    var commentData2 ='';
   	    var docId = '';
		var ImgExt = '';
		var no = '';
	    var ext = '';
	    var voiceno = '';
	    var filenamevoice = '';
	    var taskPresentId = "";
		var task_comment_voice_type="";
   	    var createdElapTime="";
		var modifiedElapTime="";

   		for (var l = 0; l < k ; l++) {
   		//jsonDataSub2Sub = jsonDataSub;

   		feedId = jsonDataSub[l].activityfeed_id;
   		imgName = lighttpdpath+"/userimages/"+jsonDataSub[l].user_image+"?"+d.getTime();	/////jsonDataSub[l].imgName;
   		sub_commentData = jsonDataSub[l].activityfeed;
   		sub_commentData = sub_commentData.replaceAll("CHR(50)","'");
   		sub_commentData = sub_commentData.replaceAll("CH(70)","\\");
    	projId = jsonDataSub[l].project_id; 
		if(typeof(projId) == "undefined"){
			projId = prjid;
		}
    		///displayDelete = jsonData[i].displayDelete;
    	userName = jsonDataSub[l].name;
		if(typeof(userName)=="undefined"){
			userName = jsonDataSub[l].userFullName;
		} 
	    CreatedDate = jsonDataSub[l].created_date;
   		CreatedTime = jsonDataSub[l].created_time;
   		createdById = jsonDataSub[l].user_id;
   		updatedBy = jsonDataSub[l].upd_NAME;
   		updatedTime = jsonDataSub[l].last_updated_time;
   		updatedDate = jsonDataSub[l].last_updated_date;
    	latestFeed = jsonDataSub[l].latestFeed; 	
    	pId= jsonDataSub[l].activityfeed_id;
        activityfeed_type = jsonDataSub[l].activityfeed_type;
        docId = jsonDataSub[l].doc_id;
		//ImgExt = jsonDataSub[l].ImageExt;
        commentData2 ='';
        taskPresentId = jsonDataSub[l].taskForActFeed;
		//task_comment_voice_type=jsonDataSub[l].task_comment_voice_type;

		if(activityfeed_type=="file"){
			ImgExt = imgicon(sub_commentData);
		}	

        if(typeof(activityfeed_type) == "undefined")
    			activityfeed_type="text";


		if(typeof(jsonDataSub[l].created_elapsed_time) != "undefined"){
			createdElapTime = jsonDataSub[l].created_elapsed_time; 
			createdElapTime = calculateElapseTime(createdElapTime)
		}else{
			createdElapTime = CreatedTime;
		}
			
		if(typeof(jsonDataSub[l].modified_elapsed_time) != "undefined"){
			modifiedElapTime = jsonDataSub[l].modified_elapsed_time; 
			modifiedElapTime = calculateElapseTime(modifiedElapTime);
		}else{
			modifiedElapTime = updatedTime;
		}

    	if(activityfeed_type == "voice"){ 
    		voiceno = sub_commentData.lastIndexOf("//");
    		filenamevoice =  $.trim(sub_commentData.substr(voiceno));
    		filenamevoice = filenamevoice.replace('//',"");
			commentData2 +=  "<div id='audioArea_"+feedId+"' style='margin-top:8px;'>"
				        +"<div style='position: absolute;'>"
				            +"<img src='images/play.png' onclick='playAudio(this, "+feedId+", \"audio\")' feedId='"+feedId+"' id='customPlayer_"+feedId+"' style='cursor:pointer;'>"
				            +"<img onclick=\"downloadActFileForVoice("+feedId+",'"+projId+"','"+globalmenu+"','"+filenamevoice+"','"+commentPlace+"')\" class=\"Download_cLabelTitle\" title=\"\" style='cursor: pointer; float: right; height: 18px; width: 18px;margin-left:15px;' src='images/downloading.png'>"
				            	+"<div class='timeline' id='timeline_"+feedId+"' onclick= \"stateChangeForActFeeds(event , 'timeline_"+feedId+"' , 'playhead_"+feedId+"' , 'audio_"+feedId+"')\" >"
			               		+"<div id='playhead_"+feedId+"' style='width: 15px;height: 15px;border-radius: 50%;margin-top: -6px;background: #64696f;'></div>"
			                   +"</div>"
				        +"</div>"
				        +"<audio  src='"+lighttpdpath+""+$.trim(sub_commentData)+"' id='audio_"+feedId+"' ontimeupdate='updateTimeSub("+feedId+")' onended='onVoiceEndedSub("+feedId+")' style='width: 100%; height: 100%; padding: 0.7%;'></audio>"
				         
				         +"<div style='padding: 1%; float: right; margin-right: 22%;'></div>"
				      +"</div>";
		 
    	 }else if(activityfeed_type == "file"){
    		   
    		 no = sub_commentData.lastIndexOf(".");
    		 ext =  $.trim(sub_commentData.substr(no));
    	
      	   commentData2 += "<div style='margin-right: 0px; margin-left: 0px; width: 50%; border-bottom: 0px none; height: 25px;' class='docListViewBorderCls' id='doc_"+docId+"'>"
  			    		    +"<div style='float:left;padding: 10px 0 0 0;width: 57.7%;' class='defaultWordBreak'>"
  			    		    if(ext.toLowerCase() == ".mp3" || ext.toLowerCase() == ".wav" || ext.toLowerCase() == ".wma" || ext.toLowerCase() == ".mp4" || ext.toLowerCase() == ".m4v" || ext.toLowerCase() == ".mov" || ext.toLowerCase() == ".flv" || 
		    		    		    ext.toLowerCase() == ".f4v" || ext.toLowerCase() == ".ogg" || ext.toLowerCase() == ".ogv" || ext.toLowerCase() == ".wmv" ||
		    		    		    ext.toLowerCase() == ".vp6" || ext.toLowerCase() == ".vp5" || ext.toLowerCase() == ".mpg" || ext.toLowerCase() == ".avi" ||
		    		    		    ext.toLowerCase() == ".mpeg" || ext.toLowerCase() == ".webm"){
		    		    	   		ext = ext.replace('.', "");
		    		    	   		commentData2 +="<img onclick=\"openVideo("+docId+",'"+ext+"',"+projId+")\" style='cursor: pointer; float: left; margin-top: -5px; height: 20px; width: 20px; margin-right: 6px;' src='"+path+"/images/repository/"+ImgExt+"' id='view_"+docId+"' class='downCalIcon'>"
		    		    	   		+"<span style='float: left; margin-top: -3px; margin-left: 2px; display:none;'>:&nbsp;</span>"
					    		    +"<span style='float: left; max-width: 75%; margin-top: -3px;' onclick=\"openVideo("+docId+",'"+ext+"',"+projId+")\" class='defaultExceedCls' id='docFullNameTaskLvl_"+docId+"'>"+sub_commentData+"</span>"
					    		    +"<img onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\" title=\"Download\" style='cursor: pointer; float: left; margin-top: -5px; height: 18px; width: 18px; margin-left: 10px;' src='images/downloading.png'>"
					    	  +"</div>"
			    		    }else{
			    		    	commentData2 +="<img onclick='veiwOrdownloadTask(this)' style='cursor: pointer; float: left; margin-top: -5px; height: 20px; width: 20px;' src='images/repository/"+ImgExt+"' id='view_"+docId+"' class='downCalIcon'>"
			    		    				 +"<span style='float: left; margin-top: -3px; margin-left: 2px;'>:&nbsp;</span>"
			    		    				 +"<span style='float: left; width: 75%; margin-top: -3px;' onclick='veiwOrdownloadTask(this)' class='defaultExceedCls' id='docFullNameTaskLvl_"+docId+"'>"+sub_commentData+"</span>"
			    		     +"</div>"
			    		    }
  			    		     /*
  			    		     +"<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 5em; margin-top: -1%; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
  			    		       +"<div style='margin-top:1px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
  			    		         +"<img src='"+path+"/images/repository/arrowleft.png' style='height:23px'>"
  			    		       +"</div>";
  			    		       */
  			if(ext.toLowerCase() == ".csv"){  		       
		   commentData2 += "<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 4em; margin-top: -1%; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
			    		       +"<div style='margin-top:1px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
			    		         +"<img src='images/repository/arrowleft.png' style='height:23px'>"
			    		       +"</div>"			    		      	    		       
			    		        +"<div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\">"
			    		           + "<div class='OptionsFont'>Download</div></div></div>"
			    		        + "</div>";			    		      
			    		       
			    		   }else if(menuType=="Task"){
          commentData2 += "<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 5em; margin-top: 2px; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
			    		       +"<div style='margin-top:10px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
			    		       		+"<img src='images/repository/arrowleft.png' style='height:23px'>"
			    		       +"</div>"
			    		       if(projId == "0"){
					    		   if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif" ){
			  			    		     commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//uploadedDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
					    		   }else{   
			  	  				    	 commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdpath+"//uploadedDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
			  	  				   }
				    		  }else{
				    			  if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif" ){
			  			    		     commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
					    		   }else{   
			  	  				    	 commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
			  	  				   } 
				    		  }
          commentData2 += "<div class='OptionsFont'>View</div></a></div><div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\">"
			    		    + "<div class='OptionsFont'>Download</div></div></div>"
			    		+ "</div>";
			    		   }else{
		  commentData2 += "<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 5em; margin-top: -1%; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
			    		       +"<div style='margin-top:1px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
			    		         +"<img src='images/repository/arrowleft.png' style='height:23px'>"
			    		       +"</div>"
			    		     if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif" ){
	  			    		     commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
  			    		     }else{   
	  	  				    	 commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
	  	  				    	}
            commentData2 += "<div class='OptionsFont'>View</div></a></div><div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\">"
  			    		    + "<div class='OptionsFont'>Download</div></div></div>"
  			    		+ "</div>";
			    		       }
			    		       /*	       
  			    		     if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif" || ext == ".mp3" || ext == ".mp4"){
	  			    		     commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdPath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
  			    		     }else{   
	  	  				    	 commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdPath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
	  	  				    	}
            	 commentData2 += "<div class='OptionsFont'>View</div></a></div><div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick='downloadActFile("+docId+","+projId+")'>"
  			    		    + "<div class='OptionsFont'>Download</div></div></div>"
  			    		+ "</div>";
  			    		*/
      		 
      		 
      	 }else{
    	  commentData2 = sub_commentData;
    	 }
        
        
        
        UI+="<div class=\"col-sm-12 col-xs-12 actFeedTot\" id=\"actFeedMainDiv_"+feedId+"\"  style=\"padding: 12px 0px 0px 0px; \">"
    	    //+"<div id = \"actFeedTot_"+feedId+"\" class=\"col-sm-12 col-xs-12 actFeedHover actualComments\" style=\"width: 100%; padding: 2px 0px 0px 8px;\" onmouseout=\"\">"
	        	if(menuType=="Task"){
	        		UI  +="<div id = \"actFeedTot_"+feedId+"\" class=\"col-sm-12 col-xs-12 actFeedHover actualComments\" style=\"width: 98%; padding: 2px 0px 0px 8px;\" >"
	  	        }else{
	  	            UI  +="<div id = \"actFeedTot_"+feedId+"\" class=\"col-sm-12 col-xs-12 actFeedHover actualComments\" style=\"width: 100%; padding: 2px 0px 0px 8px;\" >"
				}
        		if(menuType == "galleryComment"){
    	           UI +="<div id = \"actFeedImg_"+feedId+"\"  class=\"col-sm-1 col-md-1 col-xs-1 hideDivsOnEdit actualComments mainCmtUicon\"  style=\"height:50px;float: left;padding-left: 0px;max-width: 50px;min-width: 50px;padding-right: 0px;margin: 5px 0px;\">"
    	         }else{
    	          UI +="<div id = \"actFeedImg_"+feedId+"\"  class=\"col-sm-1 col-md-1 col-xs-1 hideDivsOnEdit actualComments mainCmtUicon\"  style=\"height:50px;float: left;padding-left: 0px;max-width: 50px;min-width: 50px;padding-right: 0px;margin: 5px 0px;\">"
    	        }
    	          UI +="<img src=\""+imgName+"\" title=\""+userName+"\" onerror=\"userImageOnErrorReplace(this);\"  class=\"img-responsive actFeedUserIcon userIcon_"+createdById+"\"  style=\"height: 45px;width: 45px;border-radius:50%;\" alt=\"Image\">"
		        +"</div>"
		        if(menuType == "galleryComment"){
			        if ((screen.width==1024) && (screen.height>=600)){
				       	 if($('div#gCommentDiv').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
				       	    UI +="<div id=\"actFeedDiv_"+feedId+"\"  ondblclick = \"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+")\" class=\"col-xs-10 col-sm-11 col-md-11 col-lg-11 actualComments\" style=\"padding:5px 0px 5px 15px;cursor: pointer;\">"
				       	 }else{
				       	    UI +="<div id=\"actFeedDiv_"+feedId+"\"  ondblclick = \"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+")\" class=\"col-xs-10 col-sm-11 col-md-11 col-lg-11 actualComments\" style=\"padding:5px 0px 5px 15px;cursor: pointer;width:82%;\">"
				       	 }
			       	}else{
			       	       UI +="<div id=\"actFeedDiv_"+feedId+"\"  ondblclick = \"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+")\" class=\"col-xs-10 col-sm-11 col-md-11 col-lg-11 actualComments\" style=\"padding:5px 0px 5px 15px;cursor: pointer;width:82%;\">"
			       	}
			        /*UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;&nbsp;"+CreatedTime+"</div>";*/
			       	UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;|&nbsp;<span class=\"Posted_on_cLabelText\">"+getValues(companyLabels,"Posted_on")+"</span>&nbsp;:&nbsp;<span title='"+CreatedTime+"'>"+createdElapTime+"</span>";
				        if(updatedTime!="-"){
				           UI+="&nbsp;|&nbsp;<span class=\"Modified_On_cLabelText\">"+getValues(companyLabels,"Modified_On")+"</span>&nbsp;:&nbsp;<span class='modifData' title='"+updatedTime+"'>"+ modifiedElapTime+"</span>";
				        }
				    UI+="</div>"
				        UI+= "<div class=\"actFeed\">"+sub_commentData+"</div>"
				        UI +="</div>"
			       	
		        }else{
		        	UI +="<div id=\"actFeedDiv_"+feedId+"\"  ondblclick = \"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+")\" class=\"col-sm-11 col-xs-10 actualComments\" style=\"padding:5px 0px 5px 15px;cursor: pointer;\">"
		        	/*UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;&nbsp;"+CreatedTime+"</div>";*/
	                   UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;|&nbsp;<span class=\"Posted_on_cLabelText\">"+getValues(companyLabels,"Posted_on")+"</span>&nbsp;:&nbsp;<span title='"+CreatedTime+"'>"+createdElapTime+"</span>";
				        if(updatedTime!="-"){
				           UI+="&nbsp;|&nbsp;<span class=\"Modified_On_cLabelText\">"+getValues(companyLabels,"Modified_On")+"</span>&nbsp;:&nbsp;<span class='modifData' title='"+updatedTime+"'>"+ modifiedElapTime+"</span>";
				        }
				        UI+="</div>"
				        if((activityfeed_type == "file") && (menuType == "activityFeed" || menuType == "activityfeed" || menuType == "Task")){
			        		UI+="<div style=\"font-size:14px;float:left\" class=\"actFeed\">File "+sub_commentData+" uploaded by "+userName+" on "+CreatedTime+"</div>"
			        	}else if((activityfeed_type == "voice") && (menuType == "activityFeed" || menuType == "activityfeed" || menuType == "Task")){
			        		UI+="<div style=\"font-size:14px;float:left\" class=\"actFeed\">Audio "+sub_commentData+" uploaded by "+userName+" on "+CreatedTime+"</div><br/>"
			        	}
				        UI+="<div class=\"actFeed\" style=\"font-size:14px;\">"+commentData2+"</div>"
				       +"</div>"
		          }
		        
			     UI+="<div id=\"subcommentDiv_"+feedId+"\"  class=\"hideDivsOnEdit actFeedHoverImgDiv\" align=\"right\" style=\"float:right;padding:10px 0px;margin-right:5px;\" onclick=\"replyDivData(this);\">"
			       +" <img src=\"images/more.png\" class=\"img-responsive margin Options_cLabelTitle\" title=\""+getValues(companyLabels,"Options")+"\" style=\"padding-left:0px;cursor:pointer;\" alt=\"Image\">"
			    +"</div>" 
			     if(taskPresentId != "0" && globalmenu=="activityFeed"){
		        	UI+="<div class = \"tasksIcon\" style=\"width: 25px;float: right;margin-top: 10px;margin-right: 25px;position: absolute;right: 0px;\">"
		        		+"<img id=\"tIcon_"+feedId+"\" style=\"cursor:pointer;\" title=\"View Tasks\" src=\"images/task/taskdropdown.svg\" onclick=\"showConversationTasks(this, "+feedId+")\">"
		     		+"</div>"
		        }
			    UI+= " <div id=\"subcommentDiv1_"+feedId+"\" class=\"actFeedOptionsDiv hideDivsOnEdit\" > "
		        +"      <div style=\"margin-top: -2px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\"><img src=\"images/arrow.png\"></div>"
		        +"      <div id=\"replySubCommentDiv_"+feedId+"\" onclick=\"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+");\" style=\"width:99%;height:auto;padding:0 0 3px ;cursor:pointer;float:left;border-bottom:1px solid #ccc;\"> <span class=\"Reply_cLabelText\" style=\"float: left; height: 20px;margin-left: 5px; overflow: hidden; width: 60px;\">"+getValues(companyLabels,"Reply")+"</span></div>"
			  if(userIdglb==createdById && activityfeed_type == "text" ){
			     UI +="<div onclick=\"EditActComments("+feedId+",'"+menuType+"',"+menuTypeId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;\"> <span class=\"Edit_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">"+getValues(companyLabels,"Edit")+"</span></div>"
			   }
		     if((menuType=="activityFeed" || menuType=="activityfeed") && (activityfeed_type == "text" || activityfeed_type == "file" || activityfeed_type == "voice")){
		     	 //  UI +="<div onclick=\"createTaskActFeed("+feedId+","+projId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"Task_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Task</span></div>"
			  	  UI +="<div onclick=\"newUicreateTaskUI('','0','Tasks',"+feedId+",'','','','','convoTask');\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"Task_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Task</span></div>"
			    //newUicreateTaskUI('','0','Tasks',"+feedId+",'','','','','convoTask');//createTaskUI('','Tasks', "+feedId+", '', '','ActFeedFrCreateTask', '"+feedId+"');
			 } 
			 
			   if(menuType=="wsDocument"){
				     if(userIdglb==createdById){
				       UI+="<div onclick=\"shareRepocomments("+feedId+","+menuTypeId+","+superParentId+",'');\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Share</span></div>"
				    }else{
				     UI+="<div onclick=\"shareRepocomments("+feedId+","+menuTypeId+","+superParentId+",'shared');\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Share</span></div>"
				    }
			    }
			   
			  if((userIdglb==createdById)||(projectUsersStatus=="PO")){
				  //alert("subcomments++++++>"+projectUsersStatus);
			    UI+="   <div onclick=\"deleteMainActFeed("+feedId+",'"+projId+"','"+menuType+"',"+menuTypeId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top: 1px solid #ccc;\"><span class=\"Delete_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">"+getValues(companyLabels,"Delete")+"</span></div>"
			  }
			   UI+="</div> "
			    
			   
			  +"</div> "
			  if(menuType == "Task"){
		        	 UI+=" <div id=\"replySubCommentDiv1_"+feedId+"\" class=\"row replycommentDiv\" style=\"margin-left: 0px; margin-right: 0px;padding-left:5px; background: white none repeat scroll 0% 0%; margin-bottom: 10px;margin-top:5px;width:98%; float:left;display:none\">"
		      }else{
		        	 UI+=" <div id=\"replySubCommentDiv1_"+feedId+"\" class=\"row replycommentDiv\" style=\"margin-left: 0px; margin-right: 0px;padding-left:5px; background: white none repeat scroll 0% 0%; margin-bottom: 10px;margin-top:5px;width:100%; float:left;display:none\">"
		      }
			   UI+=" <div class=\"col-sm-12 col-xs-12\" style=\"height:45px;padding-left:0px; padding-right:0px;position:static;\">"
			    if(menuType == "galleryComment"){
					 UI+=" <textarea id=\"replyBlock_"+feedId+"\" class=\"main_commentTxtArea replyCmntDiv\" style=\"width:83%;\"  onkeyup=\"enterKeyValidentersubreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+","+superParentId+",'keyup');\" onkeypress=\"enterKeyValidentersubreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+","+superParentId+",'keypress');\"></textarea>"
					 	+"<div  class=\"projUserListMain proTagSubReplyNameId\">"
		        			+ "<ul class=\"projuserList\"></ul>"
		      			+"</div>"
		      			+" <div  class=\"activityfieldUserListMain activityfieldSubReplyUserList\">"
							+" 		<ul class=\"activityfielduserList\"></ul>"
						+" </div>"
				}else{
					UI+=" <textarea id=\"replyBlock_"+feedId+"\" class=\"main_commentTxtArea replyCmntDiv\"  onkeyup=\"enterKeyValidentersubreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+","+superParentId+",'keyup');\" onkeypress=\"enterKeyValidentersubreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+","+superParentId+",'keypress');\"></textarea>"
				
					  +"<div class='main_commentTxtArea' id='audioContainerSub_"+feedId+"' style='display:none;'>"
					     +"<div id='audioAreaSub_"+feedId+"' style='display:none;'>"
					        +"<div style='position: absolute; padding: 0.7%; margin-left: 21%;'>"
					            +"<img src='images/play.png' onclick='playAudio(this,"+feedId+", \"preview\")' id='customPlayerSub_"+feedId+"' style='cursor:pointer;'>"
					            	+"<div class='timeline' id='timelineSub_"+feedId+"'  onclick=\"stateChangeForActFeeds(event, 'timelineSub_"+feedId+"', 'playheadSub_"+feedId+"', 'audioSub_"+feedId+"')\" >"
			                    		+"<div id='playheadSub_"+feedId+"' style='width: 15px;height: 15px;border-radius: 50%;margin-top: -7px;background: #64696f;'></div>"
				                    +"</div>"
					        +"</div>"
					        +"<audio  src='' id='audioSub_"+feedId+"' ontimeupdate='updateTimeSubReply("+feedId+")' onended='onVoiceEndedSubReply("+feedId+")' style='width: 100%; height: 100%; padding: 0.7%;'></audio>"
					         
					         +"<div style='padding: 1%; float: right; margin-right: 37%;'><img src='images/circle.png' onclick='cancelVoiceSub("+feedId+")' style='width: 20px;cursor:pointer;' id=''></div>"
					      +"</div>"
					      
					      +"<div class='timer' style='float: right; display: block; height: 60%; margin-top: 1%; width: 18%;'>"
					         +"<label style='color:red; float: left;'>Recording</label>"
					         +"<div style='font-size: 14px; float: left; padding-left: 11%;' class='timerSpan'></div>"
					      +"</div> "
				       +"</div>"
					
				       
					+"<div  class=\"projUserListMain proTagSubReplyNameId\">"
		        + "<ul class=\"projuserList\"></ul>"
		      +"</div>"
		      +"<div  class=\"activityfieldUserListMain activityfieldSubReplyUserList\">"
		        + "<ul class=\"activityfielduserList\"></ul>"
		      +"</div>"
				}
			    if(menuType == "galleryComment"){
			       UI+="   <div align='center' style=\"width:8%;\" id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			     }else{
			       UI+="   <div align='center' id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			     }
			    UI+="     <div style=\"width: 42%;float: left;display:none;\"  onclick=\"closeReplyComment('"+feedId+"');\"><img class=\"img-responsive\" src=\"images/workspace/remove.png\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" alt=\"Image\"></div>"
			    +"     <div style=\"width: 55%;\" id=\"replyFeed_"+feedId+"\" onclick=\"subreplyActComments('"+feedId+"', '"+userIdglb+"', '"+projId+"','"+menuType+"','"+menuTypeId+"',"+superParentId+");\" ><img class=\"img-responsive\" title=\"Post\" src=\"images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\"></div>"
			    +"   </div>"
			    if(menuType == "galleryComment"){
			      UI+="   <div align=\"center\" style=\"width:8%;\" class=\"main_commentTxtArea_optDiv\">"
			    }else{
			       UI+="   <div align=\"center\" class=\"main_commentTxtArea_optDiv\">"
			    }
			    if(menuType == "document" || menuType == "blog" || menuType == "galleryComment"){     
			    	 UI+=" <div align=\"right\" onclick=\"showActFeedMore("+feedId+","+menuTypeId+",'"+menuType+"')\" class = \"actFeedMore\" style=\"float: right; width: 42%;display:none;\">"
			             +"<img alt=\"Image\" title=\"Options\"  style=\"margin-top: 10px;cursor:pointer;\" src=\"images/more.png\" class=\"img-responsive\">"
			         +" </div>";
			    }else{
			    	UI+=" <div align=\"right\" onclick=\"showActFeedMore("+feedId+","+menuTypeId+",'"+menuType+"')\" class = \"actFeedMore\" style=\"float: right; width: 42%;\">"
		             	+"<img alt=\"Image\" title=\"Options\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/more.png\" class=\"img-responsive\">"
		             +" </div>";
			    }
			    if(menuType == "blog"){ 
			    	UI+=" <div id=\"actFeedOptPopDiv_"+feedId+"\" class=\"mainActfeed_OptPopDiv\" style=\" margin-top: 0em; right: 15px;\" >"            
		            +" <div style=\"position: absolute; float: left; margin-top: -2px; right: -11px;\" class=\"workSpace_arrow_right\">"
		            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(0deg); width: 12px; height: 30px;margin-top:3px;margin-right:-1px;\">"
		            +" </div>";
			    }else if(menuType == "Task"){ 
			    	UI+=" <div id=\"actFeedOptPopDiv_"+feedId+"\" class=\"mainActfeed_OptPopDiv\" style=\"right: 36px;\">"            
		            +" <div style=\"position: absolute; float: left; margin-top: -2px; right: -10px;\" class=\"workSpace_arrow_right\">"
		            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(0deg); width: 10px; height: 20px;\">"
		            +" </div>";
			    }else{
			    	UI+=" <div id=\"actFeedOptPopDiv_"+feedId+"\" class=\"mainActfeed_OptPopDiv\" >"            
		            +" <div style=\"position: absolute; float: left; margin-top: -2px; right: -10px;\" class=\"workSpace_arrow_right\">"
		            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(0deg); width: 10px; height: 20px;\">"
		            +" </div>";
			    }  
			         
			      if(menuType == "document"){
			    	 UI+= " <div onclick=\"initWebcam("+feedId+","+menuTypeId+",'','"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;\" > ";
			      }else if(menuType == "activityFeed" || menuType == "activityfeed"){
			    	  UI+= " <div onclick=\"initWebcam("+feedId+",'',"+projId+",'"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\" > ";
			      }else if(menuType == "Task" || menuType == 'wsIdea' || menuType == 'agile'){
			    	  UI+= " <div onclick=\"initWebcam("+feedId+","+pgId+","+projId+",'"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\" > ";
			      }else{
			    	  UI+= " <div onclick=\"initWebcam("+feedId+","+pgId+","+projId+",'"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;\" > ";
			      }
			           // +" <div onclick=\"initWebcam("+feedId+","+menuTypeId+","+projId+",'"+menuType+"')\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\" > "
			        UI+=   "   <img style=\"border: medium none;\" src=\"images/takepicture.png\" class=\"imgCss\">"
			            +" </div>" 
			            
			        if(menuType == "activityFeed" || menuType == "activityfeed" || menuType == "Task" || menuType == 'wsIdea' || menuType == 'agile'){
			        	UI+=  " <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
			            +"<form action='WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
				          +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
				        +"</form>"
				        +"<img style=\"border: medium none;\" onclick='uploadDoc("+feedId+")' title=\"Upload\" src=\"images/upload.png\" class=\"imgCss uploadFile\">";
			        }else{
			        	UI+=  " <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
			            //+"<form action='"+path+"/WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
				         // +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
				       // +"</form>"
			        }    
			         // " <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
				           // +"<form action='"+path+"/WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
					        //  +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
					        //+"</form>"
			           // UI+= "   <img style=\"border: medium none;\" onclick='uploadDoc("+feedId+")' src=\""+path+"/images/upload.png\" class=\"imgCss uploadFile\">"
			            UI+= " </div> "        
			          +"</div>"
			          if(menuType == "galleryComment"){
			        	  UI+="<div class = 'recAudioMsg' title=\""+getValues(companyLabels,"Rec_audio")+"\" style=\"float: right; width: 50%;height: 36px; margin-top: 5px;  padding-right: 2px;display:none;\" onclick='replyVoiceFeed(this,"+feedId+");'>"
				          	+"  <img alt=\"Image\" style=\"margin-top: 5px;cursor:pointer;\" src=\"images/record.png\" class=\"img-responsive\">"
				          +"</div>"
			          }else{
			        	  UI+="<div class = 'recAudioMsg' title=\""+getValues(companyLabels,"Rec_audio")+"\" style=\"float: right; width: 50%;height: 36px; margin-top: 5px;  padding-right: 2px;\" onclick='replyVoiceFeed(this,"+feedId+");'>"
				          	+"  <img alt=\"Image\" style=\"margin-top: 5px;cursor:pointer;\" src=\"images/record.png\" class=\"img-responsive\">"
				          +"</div>"
			          }
			            UI+="  </div>"
			  +"  </div>"
			  +" </div> "
			   UI +="<div  id=\"EditCmtDivContainer_"+feedId+"\" style=\"height:45px;padding-left:0px; padding-right:0px;display : none;\" class=\"col-sm-12 col-xs-12 editTextArea\">"
			   +"<div  class=\"projUserListMain proTagReplyNameId\">"
		        		+ "<ul class=\"projuserList\"></ul>"
		        		+"</div>"		
			   +"<div  class=\"activityfieldUserListMain activityfieldReplyUserList\">"
		        		+ "<ul class=\"activityfielduserList\"></ul>"
		      			+"</div>"
			   if(menuType=="galleryComment"){
			    UI +="  <textarea id=\"EdidCmtBlock_"+feedId+"\" onkeyup=\"specialCharAt(event,"+feedId+",'"+menuType+"','keyup');\" onkeypress=\"specialCharAt(event,"+feedId+",'"+menuType+"','keypress');\" class=\"main_commentTxtArea\" style=\"width:93%;\" id=\"main_commentTextarea\"></textarea> <div align='center' style=\"width:7%;\" id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			    +"    <div onclick=\"HideMainFeedData()\" style=\"display:none;width: 42%;float: left;\"><img alt=\"Image\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" src=\"images/workspace/remove.png\" class=\"img-responsive\"></div>"
			    +"    <div id=\"pstFeed_"+feedId+"\" onclick=\"postEditedCmt('"+feedId+"', '"+userIdglb+"', '"+projId+"','"+menuType+"',"+menuTypeId+");\" style=\"width: 70%;\"><img alt=\"Image\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/workspace/post.png\" class=\"img-responsive\"></div>"
			    +"  </div>"
			   +"  </div>"
			  
			   }else{
			    UI +="  <textarea id=\"EdidCmtBlock_"+feedId+"\" class=\"main_commentTxtArea\" onkeyup=\"specialCharAt(event,"+feedId+",'"+menuType+"','keyup');\" onkeypress=\"specialCharAt(event,"+feedId+",'"+menuType+"','keypress');\" style=\"width:96%;\" id=\"main_commentTextarea\"></textarea> <div align='center' id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			    +"    <div onclick=\"HideMainFeedData()\" style=\"display:none;width: 42%;float: left;\"><img alt=\"Image\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" src=\"images/workspace/remove.png\" class=\"img-responsive\"></div>"
			    +"    <div id=\"pstFeed_"+feedId+"\" onclick=\"postEditedCmt('"+feedId+"', '"+userIdglb+"', '"+projId+"','"+menuType+"',"+menuTypeId+");\" style=\"width: 55%;\"><img alt=\"Image\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/workspace/post.png\" class=\"img-responsive\"></div>"
			    +"  </div>"
			    +"  </div>"
			   }
			  UI+= "<div id=\"createTaskListUiDiv_"+feedId+"\" class=\"col-sm-12 col-xs-12\" style=\"border-radius: 5px;padding-left:0px; padding-right:0px;margin-top:2px;\"></div>";
			    
			  UI+= "<div id=\"actFeedTaskDiv_"+feedId+"\" class=\"col-sm-12 col-xs-12 taskShow\" style=\"border-radius: 5px;padding-left:0px; padding-right:0px;margin-top:2px;\"></div>";
			     
			  UI+="<div id=\"actFeedSubDiv_"+feedId+"\" class=\"col-sm-12 col-xs-12\" style=\"border-radius: 5px;padding-left:35px; padding-right:0px;\">";
			  if(jsonDataSub[l].subCommentsData.length>0){
	        	  UI+=prepareUISub2SubFeeds(jsonDataSub[l].subCommentsData,menuType,menuTypeId,superParentId);
	    	     }
			   UI+="</div>" 
		  +"</div><span class=\"spanCls\" style=\"display:none;\"></span>";  
		
		
    	}
		level=0;
		$('#latestFeed').val(latestFeed);
    	UI=UI.replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(26)",":").replaceAll("CHR(40)","\"");
    	return UI;
   	}
    
     
	function prepareUISub2SubFeeds(jsonDataSub2Sub,menuType,menuTypeId,superParentId) {
   		var UI = "";
   		 //   k=10;
	    	//if(k>jsonDataSub.length)
	  	var j = jsonDataSub2Sub.length;
	    var createdById = '';
   	    var feedId = '';
   	    var imgName = '';
   	    var sub_commentData='';
   	    var projId = '';
   	    var userName = '';
   	    var CreatedDate = '';
   	    var CreatedTime = '';
   	    var updatedBy = "";
    	var updatedDate = "";
    	var updatedTime = "";
   	    var pId = '';    
   	    var latestFeed='';
   	    var commentData2 = '';
   	    var activityfeed_type = '';
   	    var docId = '';
		var ImgExt = '';
		var no = '';
	    var ext = '';
	    var voiceno = '';
	    var filenamevoice = '';
   	    var taskPresentId = '';
		var task_comment_voice_type="";
		var createdElapTime="";
		var modifiedElapTime="";


   		for (var l = 0; l < j ; l++) {
			feedId = jsonDataSub2Sub[l].activityfeed_id;
	   		imgName = lighttpdpath+"/userimages/"+jsonDataSub2Sub[l].user_image+"?"+d.getTime();	////jsonDataSub2Sub[l].imgName;
	   		sub_commentData = jsonDataSub2Sub[l].activityfeed;
	   		sub_commentData = sub_commentData.replaceAll("CHR(50)","'");
   		    sub_commentData = sub_commentData.replaceAll("CH(70)","\\");
	   		projId = jsonDataSub2Sub[l].project_id; 
	   		userName = jsonDataSub2Sub[l].name;
			if(typeof(projId) == "undefined"){
				projId = prjid;
			}
			if(typeof(userName)=="undefined"){
				userName = jsonDataSub2Sub[l].userFullName;
			}
	   		CreatedDate = jsonDataSub2Sub[l].created_date;
	   		CreatedTime = jsonDataSub2Sub[l].created_time;
	   		createdById = jsonDataSub2Sub[l].user_id;
			updatedBy = jsonDataSub2Sub[l].user_id;
    		updatedTime = jsonDataSub2Sub[l].last_updated_time;
    		updatedDate = jsonDataSub2Sub[l].last_updated_date;
	        pId= jsonDataSub2Sub[l].activityfeed_id;
	        activityfeed_type = jsonDataSub2Sub[l].activityfeed_type; 
	        docId = jsonDataSub2Sub[l].doc_id;
    		///ImgExt = jsonDataSub2Sub[l].ImageExt;
    		latestFeed = jsonDataSub2Sub[l].latestFeed; 
    		taskPresentId = jsonDataSub2Sub[l].taskForActFeed;
			///task_comment_voice_type = jsonDataSub2Sub[l].task_comment_voice_type;
			
			if(activityfeed_type=="file"){
				ImgExt = imgicon(sub_commentData);
			}
	        commentData2 = '';
	        if(typeof(activityfeed_type) == "undefined")
    			activityfeed_type="text";

				if(typeof(jsonDataSub2Sub[l].created_elapsed_time) != "undefined"){
					createdElapTime = jsonDataSub2Sub[l].created_elapsed_time; 
					createdElapTime = calculateElapseTime(createdElapTime);
				}else{
					createdElapTime = CreatedTime;
				}
				
				if(typeof(jsonDataSub2Sub[l].modified_elapsed_time) != "undefined"){
					modifiedElapTime = jsonDataSub2Sub[l].modified_elapsed_time; 
					modifiedElapTime = calculateElapseTime(modifiedElapTime);
				}else{
					modifiedElapTime = updatedTime;
				}
	

	        if(activityfeed_type == "voice"){ 
	        	voiceno = sub_commentData.lastIndexOf("//");
	    		filenamevoice =  $.trim(sub_commentData.substr(voiceno));
	    		filenamevoice = filenamevoice.replace('//',"");
				commentData2 +=  "<div id='audioArea_"+feedId+"' style='margin-top:8px;'>"
					        +"<div style='position: absolute;'>"
					            +"<img src='images/play.png' onclick='playAudio(this, "+feedId+", \"audio\")' feedId='"+feedId+"' id='customPlayer_"+feedId+"' style='cursor:pointer;'>"
					            +"<img onclick=\"downloadActFileForVoice("+feedId+",'"+projId+"','"+globalmenu+"','"+filenamevoice+"','"+commentPlace+"')\" class=\"Download_cLabelTitle\" title=\"\" style='cursor: pointer; float: right; height: 18px; width: 18px;margin-left:15px;' src='images/downloading.png'>"
					            	+"<div class='timeline' onclick= \"stateChangeForActFeeds(event, 'timeline_"+feedId+"' , 'playhead_"+feedId+"' , 'audio_"+feedId+"')\" >"
				               		+"<div id='playhead_"+feedId+"' style='width: 15px;height: 15px;border-radius: 50%;margin-top: -6px;background: #64696f;'></div>"
				                   +"</div>"
					        +"</div>"
					        +"<audio  src='"+lighttpdpath+""+$.trim(sub_commentData)+"' id='audio_"+feedId+"' ontimeupdate='updateTimeSub("+feedId+")' onended='onVoiceEndedSub("+feedId+")' style='width: 100%; height: 100%; padding: 0.7%;'></audio>"
					         
					         +"<div style='padding: 1%; float: right; margin-right: 22%;'></div>"
					      +"</div>";
			 
	    	 }else if(activityfeed_type == "file"){
	    		   
	    		 no = sub_commentData.lastIndexOf(".");
	    		 ext =  $.trim(sub_commentData.substr(no));
	    		
	      	   commentData2 += "<div style='margin-right: 0px; margin-left: 0px; width: 50%; border-bottom: 0px none; height: 25px;' class='docListViewBorderCls' id='doc_"+docId+"'>"
	  			    		    +"<div style='float:left;padding: 10px 0 0 0;width: 57.7%;' class='defaultWordBreak'>"
	  			    		    if(ext.toLowerCase() == ".mp3" || ext.toLowerCase() == ".wav" || ext.toLowerCase() == ".wma" || ext.toLowerCase() == ".mp4" || ext.toLowerCase() == ".m4v" || ext.toLowerCase() == ".mov" || ext.toLowerCase() == ".flv" || 
			    		    		    ext.toLowerCase() == ".f4v" || ext.toLowerCase() == ".ogg" || ext.toLowerCase() == ".ogv" || ext.toLowerCase() == ".wmv" ||
			    		    		    ext.toLowerCase() == ".vp6" || ext.toLowerCase() == ".vp5" || ext.toLowerCase() == ".mpg" || ext.toLowerCase() == ".avi" ||
			    		    		    ext.toLowerCase() == ".mpeg" || ext.toLowerCase() == ".webm"){
			    		    	   		ext = ext.replace('.', "");
			    		    	   		commentData2 +="<img onclick=\"openVideo("+docId+",'"+ext+"',"+projId+")\" style='cursor: pointer; float: left; margin-top: -5px; height: 20px; width: 20px; margin-right: 6px;' src='images/repository/"+ImgExt+"' id='view_"+docId+"' class='downCalIcon'>"
			    		    	   		+"<span style='float: left; margin-top: -3px; margin-left: 2px; display:none;'>:&nbsp;</span>"
						    		    +"<span style='float: left; max-width: 75%; margin-top: -3px;' onclick=\"openVideo("+docId+",'"+ext+"',"+projId+")\" class='defaultExceedCls' id='docFullNameTaskLvl_"+docId+"'>"+sub_commentData+"</span>"
						    		    +"<img onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\" title=\"Download\" style='cursor: pointer; float: left; margin-top: -5px; height: 18px; width: 18px; margin-left: 10px;' src='images/downloading.png'>"
						    	  +"</div>"
	  			    		    }else{
	  			    		    	commentData2 +="<img onclick='veiwOrdownloadTask(this)' style='cursor: pointer; float: left; margin-top: -5px; height: 20px; width: 20px;' src='images/repository/"+ImgExt+"' id='view_"+docId+"' class='downCalIcon'>"
	  			    		    				 +"<span style='float: left; margin-top: -3px; margin-left: 2px;'>:&nbsp;</span>"
	  			    		    				 +"<span style='float: left; width: 75%; margin-top: -3px;' onclick='veiwOrdownloadTask(this)' class='defaultExceedCls' id='docFullNameTaskLvl_"+docId+"'>"+sub_commentData+"</span>"
				    		     +"</div>"
	  			    		    }
	  			    		     /*
	  			    		     +"<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 5em; margin-top: -1%; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
	  			    		       +"<div style='margin-top:1px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
	  			    		         +"<img src='"+path+"/images/repository/arrowleft.png' style='height:23px'>"
	  			    		       +"</div>";
	  			    		       */
	  			   if(ext.toLowerCase() == ".csv"){  		       
		        commentData2 += "<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 4em; margin-top: -1%; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
			    		       +"<div style='margin-top:1px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
			    		         +"<img src='images/repository/arrowleft.png' style='height:23px'>"
			    		       +"</div>"			    		      	    		       
			    		        +"<div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\">"
			    		           + "<div class='OptionsFont'>Download</div></div></div>"
			    		        + "</div>";			    		          		       
			    		       }else if(menuType=="Task"){
		    	 commentData2 += "<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 5em; margin-top: 2px; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
    		       				+"<div style='margin-top:10px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
    		       						+"<img src='images/repository/arrowleft.png' style='height:23px'>"
    		       				+"</div>"
    		       				if(projId == "0"){
        		       				if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif"){
        		       					commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//uploadedDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
        		       				}else{   
        		       					commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdpath+"//uploadedDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
        		       				}
        		       			}else{
        		       				if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif"){
        		       					commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
        		       				}else{   
        		       					commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
        		       				}
        		       			}
	    		    
		    	 				commentData2 += "<div class='OptionsFont'>View</div></a></div><div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\">"
	    		    			+ "<div class='OptionsFont'>Download</div></div></div>"
	    		    			+ "</div>";
			    		       }else{
			     commentData2 += "<div style='background-color: rgb(255, 255, 255); border: 1px solid rgb(161, 161, 161); border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); float: left; height: 5em; margin-top: -1%; min-width: 100px; padding: 1% 1% 1% 5px; position: absolute; left: 3%; width: 113px; z-index: 100; display: none;' class='hideOptionDiv' id='dowloadOption_"+docId+"'>"
			    		       +"<div style='margin-top:1px;float:left;margin-left:-18px;' class='iOption_arrow-right' id='arrowDiv_"+docId+"'>"
			    		         +"<img src='images/repository/arrowleft.png' style='height:23px'>"
			    		       +"</div>"
			    		        if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif"){
	  			    		     commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
	  			    		    }else{   
	  	  				    	 commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdpath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
	  	  				    	}
	  			    		    
	  			    		    commentData2 += "<div class='OptionsFont'>View</div></a></div><div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick=\"downloadActFile("+docId+","+projId+",'"+ext+"','"+feedId+"')\">"
	  			    		    + "<div class='OptionsFont'>Download</div></div></div>"
	  			    		+ "</div>";
			    		       } 
			    		       /*		       
	  			    		    if(ext.toLowerCase() == ".png" || ext.toLowerCase() == ".jpg" || ext.toLowerCase() == ".jpeg" || ext.toLowerCase() == ".gif" || ext == ".mp3" || ext == ".mp4"){
	  			    		     commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='"+lighttpdPath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
	  			    		    }else{   
	  	  				    	 commentData2 +="<div style='float:left;width:100%;padding-bottom:9px' class='optionList'><a onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' href='https://docs.google.com/gview?url="+lighttpdPath+"//projectDocuments//"+docId+''+ext+"' target='_blank' class='optionList'>";
	  	  				    	}
	  			    		    
	  			    		    commentData2 += "<div class='OptionsFont'>View</div></a></div><div onmouseout='onMouseOutDiv(this)' onmouseover='onMouseOverDiv(this)' onclick='downloadActFile("+docId+","+projId+")'>"
	  			    		    + "<div class='OptionsFont'>Download</div></div></div>"
	  			    		+ "</div>";
	  			    		*/
	      		 
	      		 
	      	 }else{
	    	  commentData2 = sub_commentData;
	    	 }
	        
	        

         UI+="<div class=\"col-sm-12 col-xs-12 actFeedTot\"  id=\"actFeedMainDiv_"+feedId+"\"  style=\"border-radius: 5px;padding-top:12px;padding-left:0px; padding-right:0px;\">"
    	       // +"<div id = \"actFeedTot_"+feedId+"\" class=\"col-sm-12 col-xs-12 actFeedHover actualComments\" style=\"width: 100%; padding: 2px 0px 0px 8px\" onmouseout=\"\">"
	         	if(menuType=="Task"){
	         		UI  +="<div id = \"actFeedTot_"+feedId+"\" class=\"col-sm-12 col-xs-12 actFeedHover actualComments\" style=\"width: 98%; padding: 2px 0px 0px 8px;\" >"
	   	        }else{
	   	            UI  +="<div id = \"actFeedTot_"+feedId+"\" class=\"col-sm-12 col-xs-12 actFeedHover actualComments\" style=\"width: 100%; padding: 2px 0px 0px 8px;\" >"
	 			}
    	        if(menuType == "galleryComment"){
    	          if ((screen.width==1024) && (screen.height>=600)){
    	             UI +="<div id = \"actFeedImg_"+feedId+"\"  class=\"col-lg-1 hideDivsOnEdit actualComments mainCmtUicon\" style=\"height:50px;float: left;min-width: 50px;margin: 5px 0px;padding: 0px;max-width: 50px;\">"
    	             +"<img src=\""+imgName+"\" title=\""+userName+"\" style=\"width:100%;height:30px;\" onerror=\"userImageOnErrorReplace(this);\"  class=\"img-responsive actFeedReplyUserIcon \"  alt=\"Image\">"
    	          
    	          }else{
    	           UI +="<div id = \"actFeedImg_"+feedId+"\"  class=\"col-lg-1 hideDivsOnEdit actualComments mainCmtUicon\" style=\"height:50px;float: left;min-width: 50px;margin: 5px 0px;padding: 0px;max-width: 50px;\">"
    	           +"<img src=\""+imgName+"\" title=\""+userName+"\" onerror=\"userImageOnErrorReplace(this);\"  class=\"img-responsive actFeedReplyUserIcon\"  alt=\"Image\" style=\"height: 45px;width: 45px;border-radius:50%;\" >"
    	           
    	          }
    	        }else{
    	       		 UI +="<div id = \"actFeedImg_"+feedId+"\"  class=\"col-lg-1 hideDivsOnEdit actualComments mainCmtUicon\" style=\"height:50px;float: left;min-width: 50px;margin: 5px 0px;padding: 0px;max-width: 50px;\">"
    	    	     +"<img src=\""+imgName+"\" title=\""+userName+"\" onerror=\"userImageOnErrorReplace(this);\"  class=\"img-responsive actFeedReplyUserIcon userIcon_"+createdById+"\"  alt=\"Image\" style=\"height: 45px;width: 45px;border-radius:50%;\" >"
    	    	
    	    	}
    	        UI +="</div>"
		        if(menuType == "galleryComment"){
			          if ((screen.width==1024) && (screen.height>=600)){
			          if($('div#gCommentDiv').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
			            UI +="<div style=\"width:79%;padding:5px 0px 5px 0px;\" id=\"actFeedDiv_"+feedId+"\" ondblclick = \"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+");\" class=\"col-sm-10 col-xs-10 actualComments\" style=\"padding:5px 0px 5px 0px;cursor: pointer;\">"
			          }else{
			           	UI +="<div style=\"width:78%;padding:5px 0px 5px 0px;\" id=\"actFeedDiv_"+feedId+"\" ondblclick = \"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+");\" class=\"col-sm-10 col-xs-10 actualComments\" style=\"padding:5px 0px 5px 0px;cursor: pointer;\">"
			          }
			          }else{
			           UI +="<div id=\"actFeedDiv_"+feedId+"\" ondblclick = \"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+");\" class=\"col-sm-10 col-xs-10 actualComments\" style=\"padding:5px 0px 5px 0px;cursor: pointer;width:79%;\">"
			          }
			          /*UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;&nbsp;"+CreatedTime+"</div>";*/
			          UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;|&nbsp;<span class=\"Posted_on_cLabelText\">"+getValues(companyLabels,"Posted_on")+"</span>&nbsp;:&nbsp;<span title='"+CreatedTime+"' >"+createdElapTime+"</span>";
			          if(updatedTime!="-"){
			              UI+="&nbsp;|&nbsp;<span class=\"Modified_On_cLabelText\">"+getValues(companyLabels,"Modified_On")+"</span>&nbsp;:&nbsp;<span class='modifData' title='"+updatedTime+"'>"+ modifiedElapTime+"</span>";
			           }
			          UI+="</div>"
			          UI +="<div class=\"actFeed\">"+sub_commentData+"</div>"
					  UI +="</div>"
			          
			          
		        }else{
		              UI +="<div id=\"actFeedDiv_"+feedId+"\" ondblclick = \"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+");\" class=\"col-sm-11 col-xs-10 col-lg-11 actualComments convTask\" style=\"padding:5px 0px 5px 15px;cursor: pointer;\">"
		              /*UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;&nbsp;"+CreatedTime+"</div>";*/
		              UI+="<div class=\"defaultNameDateTimestamp\"><b>"+userName+"</b>&nbsp;|&nbsp;<span class=\"Posted_on_cLabelText\">"+getValues(companyLabels,"Posted_on")+"</span>&nbsp;:&nbsp;<span title='"+CreatedTime+"' >"+createdElapTime+"</span>";
			          if(updatedTime!="-"){
			              UI+="&nbsp;|&nbsp;<span class=\"Modified_On_cLabelText\">"+getValues(companyLabels,"Modified_On")+"</span>&nbsp;:&nbsp;<span class='modifData' title='"+updatedTime+"'>"+ modifiedElapTime+"</span>";
			           }
			          UI+="</div>" 
			        	  if((activityfeed_type == "file") && (menuType == "activityFeed" || menuType == "activityfeed" || menuType == "Task")){
				        		UI+="<div style=\"font-size:14px;float:left\" class=\"actFeed\">File "+sub_commentData+" uploaded by "+userName+" on "+CreatedTime+"</div>"
				        	}else if((activityfeed_type == "voice") && (menuType == "activityFeed" || menuType == "activityfeed" || menuType == "Task")){
				        		UI+="<div style=\"font-size:14px;float:left\" class=\"actFeed\">Audio "+sub_commentData+" uploaded by "+userName+" on "+CreatedTime+"</div><br/>"
				        	}
			          
			          UI+="<div class=\"actFeed\">"+commentData2+"</div>"
					  UI +="</div>"
		        }
		        
				 UI+="<div id=\"subcommentDiv_"+feedId+"\" class=\"hideDivsOnEdit actFeedHoverImgDiv\" align=\"right\" style=\"float:right; padding:10px 0px;margin-right:5px; \" onclick=\"replyDivData(this);\">"
				       +" <img src=\"images/more.png\" class=\"img-responsive margin Options_cLabelTitle\" title=\""+getValues(companyLabels,"Options")+"\" style=\"padding-left:10px;cursor:pointer;\" alt=\"Image\">"
				    +"</div>" 
				     if(taskPresentId != "0" && globalmenu=="activityFeed" ){
		        		UI+="<div class = \"tasksIcon\" style=\"width: 25px;float: right;margin-top: 10px;margin-right: 25px;position: absolute;right: 0px;\">"
		        			+"<img id=\"tIcon_"+feedId+"\" style=\"cursor:pointer;\" title=\"View Tasks\" src=\"images/task/taskdropdown.svg\" onclick=\"showConversationTasks(this, "+feedId+")\">"
		     			+"</div>"
		        	}
				    UI+= "<div id=\"subcommentDiv1_"+feedId+"\" class=\"actFeedOptionsDiv hideDivsOnEdit\" > "
			        +"<div style=\"margin-top: -2px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\"><img src=\"images/arrow.png\"></div>"
			       // +"           <div onclick=\"AddSubComments("+pId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-bottom:1px solid #ccc;\"> <img class=\"imgCss\" src=\""+path+"/images/Projects/feedCommIcon.png\" style=\"float:left;height:20px;width:20px;border:none;\"><span class=\"Reply_cLabelText\" style=\"float: left; height: 20px;margin-left: 5px; overflow: hidden; width: 60px;\">"+Reply+"</span></div>"
				    +"<div id=\"replySubCommentDiv_"+feedId+"\" a=\"a\" onclick=\"AddSubComments4reply("+feedId+",'"+menuType+"',"+menuTypeId+");\" style=\"width:99%;height:auto;padding: 0 0 3px ;cursor:pointer;float:left;\"> <span class=\"Reply_cLabelText\" style=\"float: left; height: 20px;margin-left: 5px; overflow: hidden; width: 60px;\">"+getValues(companyLabels,"Reply")+"</span></div>"
				   if(userIdglb==createdById && activityfeed_type == "text" ){
				     UI +="<div onclick=\"EditActComments("+feedId+",'"+menuType+"',"+menuTypeId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"><span class=\"Edit_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">"+getValues(companyLabels,"Edit")+"</span></div>"
			       }
			 if((menuType=="activityFeed" || menuType=="activityfeed") && (activityfeed_type == "text" || activityfeed_type == "file" || activityfeed_type == "voice")){
			 	 
			   //   UI +="<div onclick=\"createTaskActFeed("+feedId+","+projId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"Task_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Task</span></div>"
			  	   UI +="<div onclick=\"newUicreateTaskUI('','0','Tasks',"+feedId+",'','','','','convoTask');\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"Task_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Task</span></div>"
			  } 	//newUicreateTaskUI('','0','Tasks',"+feedId+",'','','','','convoTask');//    
			   if(menuType=="wsDocument"){
			      if(userIdglb==createdById){
				      UI+="<div onclick=\"shareRepocomments("+feedId+","+menuTypeId+","+superParentId+",'');\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Share</span></div>"
				    }else{
				      UI+="<div onclick=\"shareRepocomments("+feedId+","+menuTypeId+","+superParentId+",'shared');\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Share</span></div>"
				     }
			    }
			   
			   if((userIdglb==createdById)||(projectUsersStatus=="PO")){
				  // alert("subsubcomm"+projectUsersStatus);
				UI += "<div onclick=\"deleteMainActFeed("+feedId+",'"+projId+"','"+menuType+"',"+menuTypeId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\"> <span class=\"Delete_cLabelText\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">"+getValues(companyLabels,"Delete")+"</span></div>"
			}
				UI += "</div> "
			     +"</div> "
			     if(menuType == "Task"){
		        	 UI+=" <div id=\"replySubCommentDiv1_"+feedId+"\" class=\"row replycommentDiv\" style=\"margin-left: 0px; margin-right: 0px;padding-left:14px; background: white none repeat scroll 0% 0%; margin-bottom: 10px;margin-top:5px;width:98%; float:left;display:none;\">"
			     }else{
		        	 UI+=" <div id=\"replySubCommentDiv1_"+feedId+"\" class=\"row replycommentDiv\" style=\"margin-left: 0px; margin-right: 0px;padding-left:14px; background: white none repeat scroll 0% 0%; margin-bottom: 10px;margin-top:5px;width:100%; float:left;display:none;\">"
			     }
					UI+=" <div class=\"col-sm-12 col-xs-12\" style=\"height:45px;padding-left:0px; padding-right:0px;position:static;\">"
				      if(menuType == "galleryComment"){
					 UI+=" <textarea id=\"replyBlock_"+feedId+"\" class=\"main_commentTxtArea replyCmntDiv\" style=\"width:83%;\"  onkeyup=\"enterKeyValidentersubreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+","+superParentId+",'keyup');\" onkeypress=\"enterKeyValidentersubreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+","+superParentId+",'keypress');\"></textarea>"
					 		+"<div  class=\"projUserListMain proTagSubReplyNameId\">"
		        				+ "<ul class=\"projuserList\"></ul>"
		      				+"</div>"
		      				+" <div  class=\"activityfieldUserListMain activityfieldSubReplyUserList\">"
								+" 		<ul class=\"activityfielduserList\"></ul>"
							+" </div>"
				}else{
					UI+=" <textarea id=\"replyBlock_"+feedId+"\" class=\"main_commentTxtArea replyCmntDiv\"  onkeyup=\"enterKeyValidentersubreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+","+superParentId+",'keyup');\" onkeypress=\"enterKeyValidentersubreplyActComments(event,"+feedId+", "+userIdglb+", '"+projId+"','"+menuType+"',"+menuTypeId+","+superParentId+",'keypress')\"></textarea>"
				   
					 +"<div class='main_commentTxtArea' id='audioContainerSub_"+feedId+"' style='display:none;'>"
				     +"<div id='audioAreaSub_"+feedId+"' style='display:none;'>"
				        +"<div style='position: absolute; padding: 0.7%; margin-left: 28%;'>"
				            +"<img src='images/play.png' onclick='playAudio(this,"+feedId+", \"preview\")' id='customPlayerSub_"+feedId+"' style='cursor:pointer;'>"
				            	+"<div class='timeline' id='timelineSub_"+feedId+"'  onclick=\"stateChangeForActFeeds(event ,'timelineSub_"+feedId+"', 'playheadSub_"+feedId+"', 'audioSub_"+feedId+"')\" >"
		                    		+"<div id='playheadSub_"+feedId+"' style='width: 15px;height: 15px;border-radius: 50%;margin-top: -7px;background: #64696f;'></div>"
			                    +"</div>"
				        +"</div>"
				        +"<audio  src='' id='audioSub_"+feedId+"' ontimeupdate='updateTimeSubReply("+feedId+")' onended='onVoiceEndedSubReply("+feedId+")' style='width: 100%; height: 100%; padding: 0.7%;'></audio>"
				         
				         +"<div style='padding: 1%; float: right; margin-right: 28%;'><img src='images/circle.png' onclick='cancelVoiceSub("+feedId+")' style='width: 20px;cursor:pointer;' id=''></div>"
				      +"</div>"
				      
				      +"<div class='timer' style='float: right; display: block; height: 60%; margin-top: 1%; width: 18%;'>"
				         +"<label style='color:red; float: left;'>Recording</label>"
				         +"<div style='font-size: 14px; float: left; padding-left: 11%;' class='timerSpan'></div>"
				      +"</div> "
			       +"</div>"
					 
					+"<div  class=\"projUserListMain proTagSubReplyNameId\">"
		        + "<ul class=\"projuserList\"></ul>"
		      +"</div>"
		      +"<div  class=\"activityfieldUserListMain activityfieldSubReplyUserList\">"
		        + "<ul class=\"activityfielduserList\"></ul>"
		      +"</div>"
				}
			    
			     if(menuType == "galleryComment"){
			       UI+="   <div align='center' style=\"width:8%;\" id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			     }else{
			       UI+="   <div align='center' id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			     }
			     UI+="     <div style=\"width: 42%;float: left;display:none;\"  onclick=\"closeReplyComment('"+feedId+"');\"><img class=\"img-responsive\" src=\"images/workspace/remove.png\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" alt=\"Image\"></div>"
				    +"     <div style=\"width: 55%;\" id=\"replyFeed_"+feedId+"\" onclick=\"subreplyActComments('"+feedId+"', '"+userIdglb+"', '"+projId+"','"+menuType+"','"+menuTypeId+"',"+superParentId+");\" ><img class=\"img-responsive\" title=\"Post\" src=\"images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\"></div>"
				    +"   </div>"
				  if(menuType == "galleryComment"){
			      UI+="   <div align=\"center\" style=\"width:8%;\" class=\"main_commentTxtArea_optDiv\">"
			    }else{
			       UI+="   <div align=\"center\" class=\"main_commentTxtArea_optDiv\">"
			    }
			     if(menuType == "document" || menuType == "blog" || menuType == "galleryComment"){
			         UI+=" <div align=\"right\" onclick=\"showActFeedMore("+feedId+","+menuTypeId+",'"+menuType+"')\" class = \"actFeedMore\" style=\"float: right; width: 41%;display:none;\">"
				             +"<img alt=\"Image\" title=\"Options\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/more.png\" class=\"img-responsive\">"
				         +" </div>";
			     }else{
			    	 UI+=" <div align=\"right\" onclick=\"showActFeedMore("+feedId+","+menuTypeId+",'"+menuType+"')\" class = \"actFeedMore\" style=\"float: right; width: 41%;\">"
		             	+"<img alt=\"Image\" title=\"Options\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/more.png\" class=\"img-responsive\">"
		             +" </div>";
			     }
			         if(menuType == "blog"){
			        	 UI+=" <div id=\"actFeedOptPopDiv_"+feedId+"\" class=\"mainActfeed_OptPopDiv\" style=\" margin-top: 0em; right: 15px;\" >"            
				            +" <div style=\"position: absolute; float: left; margin-top: -2px; right: 48px;\" class=\"workSpace_arrow_right\">"
				            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(0deg); width: 12px; height: 29px;margin-top:0em;margin-right:-106px;\">"
				            +" </div>";
			         }if(menuType == "Task"){
			        	 UI+=" <div id=\"actFeedOptPopDiv_"+feedId+"\" class=\"mainActfeed_OptPopDiv\" style=\"right: 36px;\">"            
				            +" <div style=\"position: absolute; float: left; margin-top: -2px; right: -10px;\" class=\"workSpace_arrow_right\">"
				            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(0deg); width: 10px; height: 20px;\">"
				            +" </div>" ; 
			         }else{
			        	UI+=" <div id=\"actFeedOptPopDiv_"+feedId+"\" class=\"mainActfeed_OptPopDiv\" >"            
				            +" <div style=\"position: absolute; float: left; margin-top: -2px; right: -10px;\" class=\"workSpace_arrow_right\">"
				            +"   <img src=\"images/arrow.png\" style=\"transform: rotate(0deg); width: 10px; height: 20px;\">"
				            +" </div>" ; 
			         }       
				         
			         if(menuType == "document"){
			        	 UI+= " <div onclick=\"initWebcam("+feedId+","+menuTypeId+",'','"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;\" > ";
			         }else if(menuType == "activityFeed" || menuType == "activityfeed"){
			        	 UI+= " <div onclick=\"initWebcam("+feedId+",'',"+projId+",'"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\" > ";
			         }else if(menuType == "Task" || menuType == 'wsIdea' || menuType == 'agile'){
			        	 UI+= " <div onclick=\"initWebcam("+feedId+","+pgId+","+projId+",'"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\" > "; 
			         }else{
			        	 UI+= " <div onclick=\"initWebcam("+feedId+","+pgId+","+projId+",'"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;\" > "; 
			         }
				           // +" <div onclick=\"initWebcam("+feedId+","+menuTypeId+","+projId+",'"+menuType+"')\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\" > "
				         UI+="   <img style=\"border: medium none;\" src=\"images/takepicture.png\" class=\"imgCss\">"
				            +" </div>" 
				       if(menuType == "activityFeed" || menuType == "activityfeed" || menuType == "Task" || menuType == 'wsIdea' || menuType == 'agile'){
				    	  UI+= " <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
				            +"<form action='WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
					          +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
					        +"</form>"
					        +"<img style=\"border: medium none;\" onclick='uploadDoc("+feedId+")' title=\"Upload\" src=\"images/upload.png\" class=\"imgCss uploadFile\">";
				       }else{
				    	   UI+=  " <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
				          //  +"<form action='"+path+"/WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
					         // +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
					        //+"</form>" 
				       }    
				           // +" <div style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\" >" 
					          //  +"<form action='"+path+"/WorkSpaceProjImgUpload' enctype='multipart/form-data' style='position:absolute;top:-100px;' method='post' name='convFileUpload' id='convFileUpload_"+feedId+"' >"				
						       //   +"<input type='file' title='upload File' value='' onchange='readFileUrl(this , "+feedId+");' class='topic_file' value='' name='FileUpload' id='FileUpload_"+feedId+"'>"	
						        //+"</form>"
				            //UI+= "   <img style=\"border: medium none;\" onclick='uploadDoc("+feedId+")' src=\""+path+"/images/upload.png\" class=\"imgCss uploadFile\">"
				            UI+=" </div> "        
				          +"</div>"
				          if(menuType == "galleryComment"){
				        	  UI+="<div class = 'recAudioMsg' title=\""+getValues(companyLabels,"Rec_audio")+"\" style=\"float: right; width: 50%;height: 36px; margin-top: 5px;  padding-right: 2px;display:none;\" onclick='replyVoiceFeed(this,"+feedId+");'>"
					          	+"  <img alt=\"Image\" style=\"margin-top: 5px;cursor:pointer;\" src=\"images/record.png\" class=\"img-responsive\">"
					          +"</div>"
				          }else{
				        	  UI+="<div class = 'recAudioMsg' title=\""+getValues(companyLabels,"Rec_audio")+"\" style=\"float: right; width: 50%;height: 36px; margin-top: 5px;  padding-right: 2px;\" onclick='replyVoiceFeed(this,"+feedId+");'>"
					          	+"  <img alt=\"Image\" style=\"margin-top: 5px;cursor:pointer;\" src=\"images/record.png\" class=\"img-responsive\">"
					          +"</div>"
				          }
				            UI+="  </div>"
				  +"  </div>"
				  +" </div> "
				   
				    UI +="<div  id=\"EditCmtDivContainer_"+feedId+"\" style=\"height:45px;padding-left:0px; padding-right:0px;display : none;\" class=\"col-sm-12 col-xs-12 editTextArea\">"
				    +"<div  class=\"projUserListMain proTagReplyNameId\">"
			        			+ "<ul class=\"projuserList\"></ul>"
			        		+"</div>"		
				    +"<div  class=\"activityfieldUserListMain activityfieldReplyUserList\">"
		        				+ "<ul class=\"activityfielduserList\"></ul>"
		      				+"</div>"
				    		
			   if(menuType == "galleryComment"){
			   UI +="  <textarea id=\"EdidCmtBlock_"+feedId+"\" onkeyup=\"specialCharAt(event,"+feedId+",'"+menuType+"','keyup');\" onkeypress=\"specialCharAt(event,"+feedId+",'"+menuType+"','keypress');\" class=\"main_commentTxtArea\" style=\"width:93%;\" id=\"main_commentTextarea\"></textarea>"
			    +"  <div align='center' style=\"width:7%;\" id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			    +"    <div onclick=\"HideMainFeedData()\" style=\"display:none;width: 42%;float: left;\"><img alt=\"Image\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" src=\"images/workspace/remove.png\" class=\"img-responsive\"></div>"
			    +"    <div id=\"pstFeed_"+feedId+"\" onclick=\"postEditedCmt('"+feedId+"', '"+userIdglb+"', '"+projId+"','"+menuType+"',"+menuTypeId+");\" style=\"width: 70%;\"><img alt=\"Image\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/workspace/post.png\" class=\"img-responsive\"></div>"
			    +"  </div>"
			   +"  </div>"
			    
			   }else{
			   UI +="  <textarea id=\"EdidCmtBlock_"+feedId+"\" class=\"main_commentTxtArea\" onkeyup=\"specialCharAt(event,"+feedId+",'"+menuType+"','keyup');\" onkeypress=\"specialCharAt(event,"+feedId+",'"+menuType+"','keypress');\" style=\"width:96%;\" id=\"main_commentTextarea\"></textarea>"
			    +"  <div align='center' id=\"topDiv_"+feedId+"\" class=\"main_commentTxtArea_btnDiv\" >"
			    +"    <div onclick=\"HideMainFeedData()\" style=\"display:none;width: 42%;float: left;\"><img alt=\"Image\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" src=\"images/workspace/remove.png\" class=\"img-responsive\"></div>"
			    +"    <div id=\"pstFeed_"+feedId+"\" onclick=\"postEditedCmt('"+feedId+"', '"+userIdglb+"', '"+projId+"','"+menuType+"',"+menuTypeId+");\" style=\"width: 55%;\" ><img alt=\"Image\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/workspace/post.png\" class=\"img-responsive\"></div>"
			    +"  </div>"
			   +"  </div>"
			   }

			   UI+= "<div id=\"createTaskListUiDiv_"+feedId+"\" class=\"col-sm-12 col-xs-12\" style=\"border-radius: 5px;padding-left:0px; padding-right:0px;margin-top:2px;\"></div>";
			    
			   UI+= "<div id=\"actFeedTaskDiv_"+feedId+"\" class=\"col-sm-12 col-xs-12 taskShow\" style=\"border-radius: 5px;padding-left:0px; padding-right:0px;margin-top:2px;\"></div>";
			 
			    UI+="<div id=\"actFeedSubDiv_"+feedId+"\" class=\"col-sm-12 col-xs-12\" style=\"border-radius: 5px;padding-left:0px; padding-right:0px;\">";
			    if(jsonDataSub2Sub[l].subCommentsData.length>0){
	        	  UI+=prepareUISub2SubFeeds(jsonDataSub2Sub[l].subCommentsData,menuType,menuTypeId,superParentId);
	    	     }
			   UI+="</div>" 
			    
			    +"</div>";  
			 
   	}
	   level=0;
   		$('#latestFeed').val(latestFeed);
   		UI=UI.replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(26)",":").replaceAll("CHR(40)","\"");
       return UI;
   }
     
    function replyDivData(obj){
	var id = $(obj).attr('id').split('_')[1];
	if ($('#subcommentDiv1_'+id).is(":hidden")) {
  			$('.actFeedOptionsDiv').hide();
  			$('.actFeedReplyOptionsDiv').hide();
  			$('#subcommentDiv1_'+id).slideToggle(function(){
  				$(this).css('overflow','');
  				if($(this).parents('div#commentListDiv').find('div.mCSB_container').hasClass('mCS_no_scrollbar_y') || $(this).parents('div#commentListDiv').find('div.mCSB_container').hasClass('div.mCS_no_scrollbar')){
  				   $(this).parents('div#commentListDiv').find('div.mCSB_container').css('height','100%');
  				}
  			});
  			if($('div#gCommentDiv').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
			       $('div#gCommentDiv').children().children('.mCSB_container').css("position","static");
			        $('div#gCommentDiv').children().children('.mCSB_container').css("height","100%");
	    	       $("div#gCommentDiv").mCustomScrollbar("update"); 
	    		}
 		}else{
 			$('#subcommentDiv1_'+id).slideToggle('slow',function(){
  				$(this).parents('div#commentListDiv').find('div.mCSB_container').css('height','');
  			});
  			 if($('div#gCommentDiv').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
			       $('div#gCommentDiv').children().children('.mCSB_container').css("position","static");
			        $('div#gCommentDiv').children().children('.mCSB_container').css("height","100%");
	    	       $("div#gCommentDiv").mCustomScrollbar("update"); 
	    		}
 		} 
	} 
	function chkreplycommentfeed(){							
			$("#replyBlock_"+feedID).val('');
			$("#replyBlock_"+feedID+"").css('background-image','none');	
			$("body").find(".replycommentDiv").hide();
			$("body").find(".mainActfeed_OptPopDiv").hide();
			replycomment="";
			feedID="";
			dataURL="";
			imageShre="";
			
	}
	
	
	function postreplyfeed(){
		/* if(level == 1){
				replyActComments(feedID,LoginId,Proje,MenuTyp,MenuTypId);
		}else{
				subreplyActComments(feedID,LoginId,Proje,MenuTyp,MenuTypId,SuperPId);
		} */
		postReply(feedID);
		$('div#replyDivContainer_'+feedID).hide();
		$('#replySubCommentDiv1_'+SuperPId).hide();
		$('.replycommentDiv').hide();
		//subreplyActComments(feedID,LoginId,Proje,MenuTyp,MenuTypId,SuperPId);
	}
	
	/*add comments to activity feeds reply start*/
	
	var feedID ="";
	var voiceType="";

	
	function AddSubComments(Id, type,taskId) {
				
				imgfeedId= Id;
				activityfeedid = Id;
				actfedid = Id;
				voiceType = type;
				level = 1;
				//  $('#actFeedTot_'+Id).off('hover');
				// 	$('#replyBlock_'+Id).val('');
				// $('#replyBlock_'+Id).show();
				// $('.editTextArea').hide();
				// $('#EdidCmtBlock_'+Id).hide();
				// $('.actualComments').show();
				let classforreply="replyDiv"
				if((globalmenu=='Task' || globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument" || convoTask=="convoTask") && feedId==0){
					classforreply="";
				}
				
				const UI =
					'<div id="replyDivContainer_' +Id +'"   class=" '+classforreply+' py-3 " style="display:block">' +
					'  <div class="col-12 p-0" >'+
					'    <textarea id="replyBlock_' +Id +'" class="main_commentTxtArea rounded"  style="background-image: none;" onpaste ="onPasteScreenShot(event,this);" onkeyup=\'checkTag(event,"reply",this,'+prjid+',"activityFeed");commentAutoGrow(event,this,"reply",'+Id+');\'   onblur =\'commentAutoGrow(event,this,"reply",'+Id+');\' oninput =\'commentAutoGrow(event,this, "reply", '+Id+');\' onkeypress=\'checkTag(event,"reply",this,'+prjid+',"activityFeed");commentAutoGrow(event,this,"reply",'+Id+');\'></textarea>' +
					'    <input type="hidden" id="menuTypeForCommentreply" value="activityfeed">'+
					'    <input type="hidden" id="ProjIdForCommentreply" value="' +prjid +'">'+
					'    <div class="atHashContainerReply" style="display: table; width: 100%;height: 2px;"></div>'+
					'  </div> ' +
					//'<div class="col-12  p-0 d-flex justify-content-between" style="margin-top: 10px;"><div class="d-flex justify-content-between "> ' +
				//	' <img src="/images/conversation/mic.svg" class="image-fluid mx-2 audioreply" title="Record Audio" style="height:22px;"  onclick="VoiceRecordingConversationNew(true,\'reply\','+Id+')"><img src="/images/conversation/attach.svg" title="Upload" class="image-fluid mx-2" style="height:20px;"><img src="/images/conversation/image.svg" title="From Gallery" class="image-fluid mx-2" style="height:20px;"><img src="/images/conversation/cam2.svg" title="Camera" class="image-fluid mx-2" style="height:20px;"></div><div class="d-flex justify-content-between"><img src="/images/conversation/smile.svg" id="smilePop_'+Id+'" onclick="smilePop();" class="image-fluid mx-2" style="height:20px;"><img onclick="postReply( '+Id+' );" src="/images/conversation/post.svg" title="Post" class="image-fluid mx-2 postButton" style="height:20px;"></div></div> ' +
					
					
					"<div class=\"col-12  p-0 d-flex justify-content-between mt-1\" >"
						+'<div class="d-flex justify-content-between ">'
							+'<div class="py-1 px-2 audioreply convoSubIcons" onclick="VoiceRecordingConversationNew(true,\'reply\','+Id+')"><img src="/images/conversation/mic.svg" class="image-fluid" title="Record audio" style="height:22px;cursor:pointer;"></div>'
							+'<div class="py-1 px-2 position-relative convoSubIcons"><img src="/images/conversation/attach.svg" class="image-fluid fileAttachIcon" style="height:20px;cursor:pointer;" title="Upload" onclick="openAttachOptions(this,\'reply\','+Id+')"></div>'
							+'<div class="py-1 px-2 convoSubIcons"><img src="/images/conversation/image.svg" class="image-fluid" title="From Gallery" style="height:20px;cursor:pointer;"></div>'
							+'<div class="py-1 px-2 convoSubIcons"><img src="/images/conversation/cam.svg" onclick="camUiPopup('+Id+');" class="image-fluid" title="Camera" style="height:20px;cursor:pointer;"></div>'
							+"<div class=\"py-1 px-2 convoSubIcons\" onmouseover=\"changeSmileyellow('edit',"+Id+");\" onmouseout=\"changeSmilewhite('edit',"+Id+");\"><img src=\"/images/conversation/smile.svg\"  id=\"smilePop_"+Id+"\"  title=\"\" class=\"image-fluid\" style=\"height:22px;cursor:pointer;\"  onclick=\"smilePop();\" ></div>"
							+'<div class="py-1 px-2 convoSubIcons"><img src="/images/conversation/at.svg" id="" title="" class="atImgExt image-fluid" style="height:20px;cursor:pointer;" onclick="externalAtHashClick(\'reply\', '+Id+', \'@\')"></div>'
							+'<div class="py-1 px-2 convoSubIcons" id="hashtagConv"><img src="/images/conversation/hash.svg" title="" class="hashImgExt image-fluid" style="height:19px;cursor:pointer;" onclick="externalAtHashClick(\'reply\', '+Id+', \'#\')"></div>'
						  +'</div>'
						+'<div class="d-flex justify-content-between">'
							+"<div class='p-1 mx-1'><img  class='postButton' src=\"/images/conversation/post1.svg\" title='Post' onclick='postReply( "+Id+",\"\",\""+taskId+"\" );' class=\"image-fluid\" style=\"height:26px;cursor:pointer;\"></div>"
						+'</div>'
              +"</div>"
					
					
					
					
					
					+"</div>";
					
					if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
						if(Id==0){
							$('body').find(".task_comment_div").find('.task_comment_list_div').append(UI);
						}else{
							if ($("#replyDivContainer_"+Id).length==0) {
								$(UI).insertAfter('#actFeedDiv_' +Id).show('slow');
							}
							else{
								$('#replyDivContainer_' +Id).toggle();
							}
							
						}
						if(taskId!='0' && type=='list'){
							//alert(taskId)
							$(".task_comment_div_"+taskId).append(UI);
							$(".task_comment_div_"+taskId).show();
						}
						var elmnt = document.getElementById("replyBlock_" + Id);
						elmnt.scrollIntoView({
							  behavior: 'smooth', block: 'nearest', inline: 'start' 
						 });
						
						$("#replyBlock_" + Id).focus();
						emojiClick('reply',Id);

					}else if(globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){

						
						if(Id==0){
							$('body').find('#popupCommentsDivbody').append(UI);
						}else{
							if ($("#replyDivContainer_"+Id).length==0) {
								$(UI).insertAfter('#actFeedDiv_' +Id).show('slow');
							}
							else{
								$('#replyDivContainer_' +Id).toggle();
							}
							
						}
						emojiClick('reply',Id);
					}else{
					
						if ($("#replyDivContainer_"+Id).length==0) {
							$('.replyDiv').remove();
							$(UI).insertAfter('#actFeedDiv_' +Id).show('slow');
							
							$("#replyBlock_" + Id).focus();
							emojiClick('reply',Id);
						} else {
							$('.replyDiv').slideUp("slow", function() { $('.replyDiv').remove();});
						}
					}
				fileSharingForComments("replyBlock_"+Id+"", Id);
				feedID = Id;
		}
function postReply(feedId,dragndrop,Tid) {
	var localOffsetTime = getTimeOffset(new Date());
				
	var Comment = $("#replyBlock_" + feedId + "").val().trim();
	var jsonData = '';
	if(imageShre!= undefined && imageShre!= ''){
		/* if(menuutype=='wsDocument'){
			
			$("#topDiv_"+feedId+"").children().eq(1).attr('onclick',"postCanvasToURLImage('"+feedId+"','"+userId+"','"+menuType+"','"+pgId+"','"+postCanvaslevel+"')");//"#topDiv_"+jsonData[i].pId+""
				postCanvasToURLImage('',prjid,menuutype,feedId,postCanvaslevel); 
		}else if(menuutype=='document'){
			
			$("#topDiv_"+feedId+"").children().eq(1).attr('onclick',"postCanvasToURLImage("+feedId+","+prjid+","+menuType+","+pgId+",'"+postCanvaslevel+"')");//"#topDiv_"+jsonData[i].pId+""			 
			postCanvasToURLImage('',prjid,menuutype,feedId,postCanvaslevel);			
		}else{ */
			$("#topDiv_"+feedId+"").children().eq(1).attr('onclick',"postCanvasToURLImage("+feedId+","+prjid+","+menuutype+",'','')");//"#topDiv_"+jsonData[i].pId+""			 
			postCanvasToURLImage(feedId,prjid,menuutype);
		//}
		imageShre="";
	}
	
	if (Comment.length == 0) {
		//  parent.alertFun("<bean:message key='activity_empty.message'/>","warning")
		$('textarea#replyBlock_' + feedId).val('');
		$('textarea#replyBlock_' + feedId).focus();
		//parent.$('#loadingBar').hide();	
	} else {
					
		//----------------Below code is to check English or Non English and also to whether it is exceeding toal characters length.
		//var LengthFlag = false;
		//var nonFlag = isNonLatinCharacters(Comment);
		//console.log('nonFlag:'+nonFlag);
		/* if (!charExceedFlag) {
			if (nonFlag) {
				if (Comment.length > 665) {
					LengthFlag = true;
				}
			} else {
				if (Comment.length > 3900) {
					LengthFlag = true;
				}
			}
		} else {
			if (nonFlag) {
				if (Comment.length > 665) {
					LengthFlag = false;
					Comment = Comment.substring(0, 665);
				}
			} else {
				if (Comment.length > 3900) {
					LengthFlag = false;
					Comment = Comment.substring(0, 3900);
				}
			}
		}
		if (LengthFlag) {
			confirmFun(getValues(companyAlerts, "Alert_Conv_Exceeding"), "delete", "ExceedCharactersConfirmForReply");
			charExceedFeedId = feedId;
			return false;
		} */
		//---------------Above code is to check English or Non English and also to whether it is exceeding toal characters length.
					
					
		var projHashTag = null;
		var selectedUser = null;
		var pattern1 = /@\w+/g;
		var selectedUserName = "";
		var patt = /#[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+/g;
		var projHashTagName = "";
					
		if (menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'document' || menuType == 'galleryComment') {
			while (projHashTag = patt.exec(Comment)) {
				projHashTagName = projHashTagName + projHashTag + ",";
			}
		}
		if (menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'document' || menuType == 'galleryComment') {
			while (selectedUser = pattern1.exec(Comment)) {
				selectedUserName = selectedUserName + selectedUser + ",";
			}
		}
		var feedType = "";
		let menuTypeId="";
		/* if (menuType == 'agile') {
			feedType = menuType;
		} else if (menuType == 'Task') {
			feedType = menuType;
		} */
		let level = $("actFeedDiv_" + feedId).attr('level');
		//let menu=globalmenu=='Task'?globalmenu:globalmenu=='Idea'?'wsIdea':menuutype;
		let menu = globalmenu == 'Task' ? globalmenu : $('#otherModuleTaskListDiv').is(':visible') == true ? "Task" : globalmenu == 'Idea' ? 'wsIdea' : globalmenu == 'agile' || SBmenu=="SBstories" || globalmenu == 'sprint' ? 'agile' :  globalmenu == 'wsdocument' ? "wsDocument" : menuutype;
		menuTypeId = (globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true) ? $("#task_id").val() : globalmenu == 'Idea' || globalmenu == 'agile' || SBmenu=="SBstories" || globalmenu == 'sprint' || globalmenu == 'wsdocument' ? $('.popupComments').attr('sourceid') : '0';
		if($("#task_id").length==0 && globalmenu=='Task'){
			menuTypeId=$("#taskAdd").attr('task_id');
			//alert(menuTypeId)
		}
		feedType = globalmenu == 'Task' ? globalmenu : $('#otherModuleTaskListDiv').is(':visible') == true ? "Task" : globalmenu == 'Idea' ? globalmenu : globalmenu == 'agile' || SBmenu=="SBstories" || globalmenu == 'sprint' ? "agile" : globalmenu == 'wsdocument' ? "wsDocument" : feedType;
		commentPlace = globalmenu == 'agile' || SBmenu=="SBstories" ? "Story" : globalmenu == 'sprint' ? "Sprint" : "";
		let jsonbody = "";
		if(menu == "wsIdea"){
			jsonbody = {
				"menuType" : menu,
				"feedId" : "0",
				"user_id" : userIdglb,
				"company_id" : companyIdglb,
				"localOffsetTime" : localOffsetTime,
				"selectedUserName" : selectedUserName,
				"comments" : Comment,
				"projHashTagName" :projHashTagName,
				"menuTypeId" : menuTypeId,
				"parent_feed_id" : feedId
			}
		}else if(menu == "wsDocument"){
			jsonbody = {
				"menuType": menu,
				"feedId": "0",
				"user_id": userIdglb,
				"company_id": companyIdglb,
				"localOffsetTime": localOffsetTime,
				"selectedUserName": selectedUserName,
				"comments": Comment,
				"projHashTagName": projHashTagName,
				"menuTypeId": menuTypeId,
				"parent_feed_id": feedId,
				"project_id" : prjid
			}
		}else{
			jsonbody = {
				"menuType": menu,
				"project_id": prjid,
	
				"feedId": "0", ////   in case of create 0 in case of update the activity_feed_id
				"localOffsetTime": localOffsetTime,
				"projHashTagName": projHashTagName,
				"selectedUserName": selectedUserName,
				"user_id": userIdglb,
				"company_id": companyIdglb,
				"comments": Comment,
				"action": "fetchinsertFeed",
				"parent_feed_id": feedId,
				//"menuTypeId": menuTypeId,
				"subMenuType": commentPlace,
				"feedType": feedType,
				"menuTypeId":menuTypeId,
				"type":"SubComment"
			}
		}
		
		console.log(jsonbody)
		$('#loadingBar').addClass('d-flex');

			$.ajax({
						url: apiPath+"/"+myk+"/v1/postConversation",
						//url:"http://localhost:8080/v1/postConversation",
						type:"POST",
						dataType:'json',
						contentType:"application/json",
						data: JSON.stringify(jsonbody),
						//data:{act:"replyOnUserComment", userId: userId,projectId:projId, comment: Comment,localOffsetTime:localOffsetTime, feedId: feedId, 
						//menuType:menuType, subMenuType:commentPlace, menuTypeId:menuTypeId,superParentId:feedId,projHashTagName:projHashTagName,selectedUserName:selectedUserName},
						error: function(jqXHR, textStatus, errorThrown) {
								checkError(jqXHR,textStatus,errorThrown);
								$('#loadingBar').addClass('d-none').removeClass('d-flex');
								//timerControl("");
								},		 
						success: function (result) {
							
							let replyUi = prepareCommentsUI(result, menuutype, prjid,'');
						
							if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
								$("#replyBlock_"+feedId).val('');
								if(feedId==0){
									
									//$(replyUi).insertAfter("#replyDivContainer_"+feedID).show();
									
									if($("#task_id").length==0){
										$(".task_comment_div_"+menuTypeId).append(replyUi)
									}else{
										
										$(replyUi).insertAfter("#replyDivContainer_"+feedId).show();
									}
								}else{
									$("#replyDivContainer_"+feedId).remove();
									$(replyUi).insertAfter('#actFeedDiv_' +feedId).show('slow');
								}
								changeImage('comment');
							}else if(globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
								$("#replyBlock_"+feedId).val('');
								if(feedId==0){
									$(replyUi).insertAfter("#replyDivContainer_"+feedId).show();
									$('#replyDivContainer_0').find('.postButton').attr('src','/images/conversation/post1.svg');
								}else{
									$("#replyDivContainer_"+feedId).remove();
									$(replyUi).insertAfter('#actFeedDiv_' +feedId).show('slow');
								}
								if(globalmenu=='Idea'){
									if(!$('#ideacomment_'+menuTypeId).is(':visible')){
										$('#ideacomment_'+menuTypeId).css('display','block');
									}
								}
							}else{
								$('.replyDiv').remove();
								callsocket(result);
								$(replyUi).insertAfter('#actFeedDiv_' +feedId).show('slow');
							// alert("In side in postreply")
								
								var postCanvaslevel = '1';
								// level = 1;		
								
								$('#loadingBar').addClass('d-none').removeClass('d-flex');
								if(from=='Task'){
									fetchPostedThingFiles(0,"Task",result[0].parent_feed_id,'',place)
								}
							}
							
							if(dragndrop=='dragndrop'){
								var actfedid = result[0].activityfeed_id;
								handleFileUploadForComments(files,actfedid,dragndrop);
							}
						}
					});
					feedID='';
		elementindex = '';
		hashInd='';
					// imgfeedId= feedId;
			
		}
	}



	function AddSubCommentsold(Id,type){
		activityfeedid = Id;
		actfedid = Id;
		voiceType=type;
		level = 1;
		/*if(archivedstatus == 'A'){
		// parent.alertFun("Project is Inactive.",'warning');
			return false;
	    }*/
	    if(type == 'activityFeed' || type == 'activityfeed' || type == 'Task' || type == 'wsIdea' || type == 'agile'){
	    	fileSharingForComments("replyBlock_"+Id+"", Id);
	    }
	    
	    
	    $('#actFeedTot_'+Id).off('hover');
		$('#replyBlock_'+Id).val('');
		$('#replyBlock_'+Id).show();
		$('.editTextArea').hide();
	  	$('#EdidCmtBlock_'+Id).hide();
	  	$('.actualComments').show();
		if ($('div#replyDivContainer_'+Id).is(":hidden")) {
		        $('.replycommentDiv').hide();
		 	    $('.actFeedOptionsDiv').hide();
		 	    $("textarea#replyBlock_"+Id).val('');
     			$('div#replyDivContainer_'+Id).slideToggle('slow',function(){
     			   $(this).find('textarea').focus();
     			});
     			$("#actFeedTextArea_"+Id).show();
     			
			} else {
			    $('div#replyDivContainer_'+Id).slideToggle();
       	        $("#actFeedTextArea_"+Id).slideToggle();
				
       		}
		 feedID = Id;
		MenuTyp = type;
	 }	 
	 
	 var voiceType="";





	 function AddSubComments4reply(Id,type){
		activityfeedid = Id;
		actfedid = Id;
        voiceType=type;
	 	level = 2;
	   // $('.actFeedHover').css('background','none');
		$('#replyBlock_'+Id).val('');
		$('#replyBlock_'+Id).show();
		$('.editTextArea').hide();
	  	$('#EdidCmtBlock_'+Id).hide();
	  	$('.actualComments').show();
		//$('.editTextArea').hide();
	  	if(type == 'Task' || type == 'activityFeed' || type == 'activityfeed' || type == 'wsIdea' || type == 'agile'){
	    	fileSharingForComments("replyBlock_"+Id+"", Id);
	    }
	  	
	  	
		if ($('div#replySubCommentDiv1_'+Id).is(":hidden")) {
				 	   $('.actFeedOptionsDiv').hide();
				 	   $('.actFeedReplyOptionsDiv').hide();
				 	   $('.replycommentDiv').hide();
			 		   $('div#replySubCommentDiv1_'+Id).slideToggle(function(){
			 		     $(this).find('textarea').focus();
			 		   });
			 		   
	        		} else {
	        	        $('div#replySubCommentDiv1_'+Id).slideToggle('slow');
	        	        $('.actFeedOptionsDiv').hide();
	        		}
				 feedID = Id;
	}	
     
     /*reply on the feeds*/
	 var pgId ="";
	function replyActComments(feedId, userId,projId,menuType,menuTypeId,dragndrop,clevel){
	//alert("inide replyact");
		//alert("replyActComments----menuTypeId"+menuTypeId);
		var postCanvaslevel = '1';
		level = 1;		
		 if(imageShre!= undefined && imageShre!= ''){
			 if(menuType=='wsDocument'){
				
				 $("#topDiv_"+feedId+"").children().eq(1).attr('onclick',"postCanvasToURLImage('"+feedId+"','"+userId+"','"+menuType+"','"+pgId+"','"+postCanvaslevel+"')");//"#topDiv_"+jsonData[i].pId+""
			    	postCanvasToURLImage('',projId,menuType,menuTypeId,postCanvaslevel); 
			 }else if(menuType=='document'){
				 
				 $("#topDiv_"+feedId+"").children().eq(1).attr('onclick',"postCanvasToURLImage("+feedId+","+projId+","+menuType+","+pgId+",'"+postCanvaslevel+"')");//"#topDiv_"+jsonData[i].pId+""			 
		    	 postCanvasToURLImage('',projId,menuType,menuTypeId,postCanvaslevel);			
		    }else{
		    	$("#topDiv_"+feedId+"").children().eq(1).attr('onclick',"postCanvasToURLImage("+feedId+","+projId+","+menuType+","+menuTypeId+",'"+postCanvaslevel+"')");//"#topDiv_"+jsonData[i].pId+""			 
		    	 postCanvasToURLImage('',projId,menuType,menuTypeId,postCanvaslevel);
		    }
		 }	 
		var localOffsetTime=getTimeOffset(new Date());
		
		var Comment = $("textarea#replyBlock_"+feedId+"").val().trim();
		
		var jsonData = '';
		if(Comment.length == 0){
	       //  parent.alertFun("<bean:message key='activity_empty.message'/>","warning")
        	 $('textarea#replyBlock_'+feedId).val('');
        	 $('textarea#replyBlock_'+feedId).focus();
        	//parent.$('#loadingBar').hide();	
	    }else{
			
	    	//----------------Below code is to check English or Non English and also to whether it is exceeding toal characters length.
			//var LengthFlag = false;
			//var nonFlag = isNonLatinCharacters(Comment);
			//console.log('nonFlag:'+nonFlag);
			/* if(!charExceedFlag){
					if(nonFlag){
						if(Comment.length >665){
							LengthFlag = true;
						}
					}else{
						if(Comment.length >3900){
							LengthFlag = true;
						}
					}
			}else{
					if(nonFlag){
						if(Comment.length>665){
							LengthFlag = false;
							Comment = Comment.substring(0, 665);
						}
					}else{
						if(Comment.length>3900){
							LengthFlag = false;
							Comment = Comment.substring(0, 3900);
						}
					}
			}
			if(LengthFlag){
				confirmFun(getValues(companyAlerts,"Alert_Conv_Exceeding"),"delete","ExceedCharactersConfirmForReply");
				charExceedFeedId = feedId ;
				return false;
			} */
			//---------------Above code is to check English or Non English and also to whether it is exceeding toal characters length.
			
			
		    var projHashTag = null;
		    var selectedUser=null;
			var pattern1 = /@\w+/g;
			var selectedUserName="";
			var patt = /#[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+/g;
			var projHashTagName="";
			
			if(menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'document' || menuType == 'galleryComment'){
		        while(projHashTag=patt.exec(Comment)){
				    projHashTagName=projHashTagName+projHashTag+",";
				}
			}
			if(menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'document' || menuType == 'galleryComment'){
		        while(selectedUser=pattern1.exec(Comment)){
				    selectedUserName=selectedUserName+selectedUser+",";
				 	}
			}
			var feedType="";
			if(menuType == 'agile'){
				feedType=menuType;
			}else if(menuType == 'Task'){
				feedType=menuType;
			}
			let jsonbody = {
				"menuType":menuType,
				"project_id":projId,
				"feedId":"0", ////   in case of create 0 in case of update the activity_feed_id
				"localOffsetTime":localOffsetTime,
				"projHashTagName":projHashTagName,
				"selectedUserName":selectedUserName,
				"user_id":userIdglb,
				"company_id":companyIdglb,
				"comments":Comment,
				"action":"fetchinsertFeed",
				"parent_feed_id":feedId,
				"menuTypeId":menuTypeId,
				"subMenuType":commentPlace,
				"feedType":feedType
			}
			
			 $('#loadingBar').show();	
			 timerControl("start");	
		        $.ajax({
					url: apiPath+"/"+myk+"/v1/postConversation",
					type:"POST",
					dataType:'json',
					contentType:"application/json",
					data: JSON.stringify(jsonbody),
		        	//data:{act:"replyOnUserComment", userId: userId,projectId:projId, comment: Comment,localOffsetTime:localOffsetTime, feedId: feedId, 
					//menuType:menuType, subMenuType:commentPlace, menuTypeId:menuTypeId,superParentId:feedId,projHashTagName:projHashTagName,selectedUserName:selectedUserName},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							},		 
					success:function(result){
							//alert("Documents Result---:::"+result);
							//checkSessionTimeOut(result);
						    $("div.replyClass").hide();	
						    
						    // Below variables are used in case of total characters exceeds the length
						   charExceedFeedId = "";
						   charExceedFlag = false;
				 		    	
						   	jsonDataSub = result;
						    for(var i = 0; i < jsonDataSub.length ; i++) {
						    	if($('#actFeedMainDiv_'+jsonDataSub[i].activityfeed_id).length < 1){
									result =  prepareUISubFeeds(jsonDataSub,menuType,menuTypeId,feedId);
							        result = result.replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(26)",":");
							        $("div#actFeedSubDiv_"+feedId).prepend(result);
							     	$("div#replyDivContainer_"+feedId).hide();
							     	feedId ="";
							     }
							}
						   
							//alert("menuType---"+menuType);
							if(menuType == "blog"){
					     	    $(".mainCmtUicon").css('width','3.5%');
					     	    h = $(window).height()-$("#header_outer").height()-$("#tabMainDiv").height();
				           		var hCHeight = eval(h); 
					      		if($("#articleDetailData").height() > hCHeight){
								  $("#blogArticleDiv").css("height","");
								}else{
								  $("#blogArticleDiv").css("height",hCHeight-6);
								}
								var isiPad = navigator.userAgent.match(/iPad/i) != null;
							    if(!isiPad){
							      $('div#articleDetailData').mCustomScrollbar("update");
								}
					     	}
					     	else if(menuType == "galleryComment"){
					     	  if(!isiPad){
					     	    popUpScrollBar('gCommentDiv');
				              	$("#gCommentDiv").mCustomScrollbar("update");
				             }
					     	}
							//$("div#replyDivContainer_"+pId).hide();
							//loadCustomLabel('actFeed');
							$('#loadingBar').hide();
							timerControl("");
							//loadActivityFeed(notFolid);
						//   parent.$('#loadingBar').hide();	
						 //  parent.timerControl("");
						 	
						 if(menuType.toLowerCase() == "activityfeed"){
						// alert("insdie actiy");
						   //sendToSlack(projId,'conversations');
						   // sendToSpark(projId,'conversations');
						 }else if(menuType=="agile"){
						   //sendToSlack(projectId,'agile_comment',menuTypeId);
						   // sendToSpark(projectId,'agile_comment',menuTypeId);
						 }else if(menuType=="wsIdea"){
						 //alert("inside");
						   sendToSlack(projectId,'idea_comment',menuTypeId);
						   //sendToSpark(projectId,'idea_comment',menuTypeId);
						 }else if(menuType=="Task"){
							 pgId=menuTypeId; 
							 //sendToSlack(projectId,'task_comment',menuTypeId);
						 }else if(menuType=="document"){
							 pgId=menuTypeId; 
							 //alert("Reply pgId---->>>>>>>>>>>>>>"+pgId);
						 }
						 if(patt.exec(Comment)){
		 					ajaxCallforHashCode(menuType,feedId,'reply');
				   		 }
						
						if(dragndrop == "dragndrop"){
							if(clevel == 1){
								level=2;
							}
							actfedid = jsonDataSub[0].activityfeed_id;
							activityfeedid = jsonDataSub[0].activityfeed_id;
							handleFileUploadForComments(files,actfedid);
							$('#loadingBar').show();
							timerControl("start");
						}	
						
				}
	        });
	   }
	   pgId= menuTypeId;
	   //alert("pgId------------Vikas----"+pgId);	
	  imgfeedId= feedId;      
      feedID="";
      replycomment = "";
	  level=0;
	}
		
   
	function subreplyActComments(feedId, userId,projId,menuType,menuTypeId,superParentId,dragndrop,clevel){
		level = 2;
		var postCanvaslevel = '2';
		if(imageShre!= undefined && imageShre!= ''){	

			if(menuType == 'wsDocument'){
				pgId=menuTypeId;

				$("#topDiv_"+feedId+"").children().eq(1).attr('onclick',"postCanvasToURLImage('',"+projId+","+menuType+","+pgId+",'"+postCanvaslevel+"')");
				postCanvasToURLImage('',projId,menuType,menuTypeId,postCanvaslevel); 
			}else{
				$("#topDiv_"+feedId+"").children().eq(1).attr('onclick',"postCanvasToURLImage('',"+projId+","+menuType+","+menuTypeId+",'"+postCanvaslevel+"')");
				postCanvasToURLImage('',projId,menuType,menuTypeId,postCanvaslevel);
			}
		}
		var localOffsetTime=getTimeOffset(new Date());
		
		var jsonData = '';
		var Comment = $("textarea#replyBlock_"+feedId+"").val().trim();

		var projHashTag = null;
		var patt = /#[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+/g;
		var projHashTagName="";
		var selectedUser=null;
		var pattern1 = /@\w+/g;
		var selectedUserName="";



		if(Comment.length == 0){
			//  parent.alertFun("<bean:message key='activity_empty.message'/>","warning")
			$('textarea#replyBlock_'+feedId).val('');
			$('textarea#replyBlock_'+feedId).focus();

		}else{

			//----------------Below code is to check English or Non English and also to whether it is exceeding toal characters length.
			var LengthFlag = false;
			var nonFlag = isNonLatinCharacters(Comment);
			//console.log('nonFlag:'+nonFlag);
			if(!charExceedFlag){
				if(nonFlag){
					if(Comment.length >665){
						LengthFlag = true;
					}
				}else{
					if(Comment.length >3900){
						LengthFlag = true;
					}
				}
			}else{
				if(nonFlag){
					if(Comment.length>665){
						LengthFlag = false;
						Comment = Comment.substring(0, 665);
					}
				}else{
					if(Comment.length>3900){
						LengthFlag = false;
						Comment = Comment.substring(0, 3900);
					}
				}
			}
			if(LengthFlag){
				confirmFun(getValues(companyAlerts,"Alert_Conv_Exceeding"),"delete","ExceedCharactersConfirmForReply");
				charExceedFeedId = feedId ;
				return false;
			}
			//---------------Above code is to check English or Non English and also to whether it is exceeding toal characters length.



			if(menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'document' || menuType == 'galleryComment'){
				while(projHashTag=patt.exec(Comment)){
					projHashTagName=projHashTagName+projHashTag+",";
				}
			}
			if(menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'document' || menuType == 'galleryComment'){
				while(selectedUser=pattern1.exec(Comment)){
					selectedUserName=selectedUserName+selectedUser+",";
				}
			}
			var isiPad = navigator.userAgent.match(/iPad/i) != null;
			if(isiPad){
				ipadVal = "YES";	
			}else{
				ipadVal = "NO"
			}
			var feedType="";
			if(menuType == 'agile'){
				feedType=menuType;
			}else if(menuType == 'Task'){
				feedType=menuType;
			}

			let jsonbody = {
				"menuType":menuType,
				"project_id":projId,
				"feedId":"0", ////   in case of create 0 in case of update the activity_feed_id
				"localOffsetTime":localOffsetTime,
				"projHashTagName":projHashTagName,
				"selectedUserName":selectedUserName,
				"user_id":userIdglb,
				"company_id":companyIdglb,
				"comments":Comment,
				"action":"fetchinsertFeed",
				"parent_feed_id":feedId,
				"menuTypeId":menuTypeId,
				"subMenuType":commentPlace,
				"feedType":feedType
			}
			
			 $('#loadingBar').show();	
			 timerControl("start");	
		        $.ajax({
					url: apiPath+"/"+myk+"/v1/postConversation",
					type:"POST",
					dataType:'json',
					contentType:"application/json",
					data: JSON.stringify(jsonbody),
					//data:{act:"replyOnUserComment", userId: userId,projectId:projId, comment: Comment, feedId: feedId,localOffsetTime:localOffsetTime,sortType:'', 
					//limit:'null', index:'null', ipadVal:ipadVal,menuType:menuType, subMenuType:commentPlace, menuTypeId:menuTypeId,superParentId:superParentId,projHashTagName:projHashTagName,selectedUserName:selectedUserName},
					error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$("#loadingBar").hide();
						timerControl("");
					}, 
					success:function(result){
						
						$("div.replyClass").hide();	
						$('.replycommentDiv').hide();

						// Below variables are used in case of total characters exceeds the length
						charExceedFeedId = "";
						charExceedFlag = false;


						jsonDataSub2Sub = result;
						for (var i = 0; i < jsonDataSub2Sub.length ; i++) {
							var activityfeed_id = jsonDataSub2Sub[i].activityfeed_id;
							if($('#actFeedMainDiv_'+activityfeed_id).length < 1){
								result = prepareUISub2SubFeeds(jsonDataSub2Sub,menuType,menuTypeId,feedId);
								result = result.replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(26)",":");
								$("div#actFeedSubDiv_"+feedId).prepend(result);
								$("div#replySubCommentDiv1_"+feedId).hide();
								activityfeed_id ="";
							}
						}
						
						$('#loadingBar').hide();
						timerControl("");
						if(menuType.toLowerCase()=="activityfeed"){
							//sendToSlack(projId,'conversations');
							//sendToSpark(projId,'conversations');
						}else if(menuType=="agile"){
							sendToSlack(projectId,'agile_comment',menuTypeId);
							//sendToSpark(projectId,'agile_comment',menuTypeId);
						}else if(menuType=="wsIdea"){
							sendToSlack(projectId,'idea_comment',menuTypeId);
							//sendToSpark(projectId,'idea_comment',menuTypeId);
						}else if(menuType=="Task"){
							//  sendToSlack(projectId,'task_comment',menuTypeId);
						}else if(menuType=="blog"){
							var isiPad = navigator.userAgent.match(/iPad/i) != null;
							if(!isiPad){
								$('div#articleDetailData').mCustomScrollbar("update");
							}
						}
						if(patt.exec(Comment)){
							ajaxCallforHashCode(menuType,feedId,'subreply');
						} 

						if(dragndrop == "dragndrop"){
							if(clevel == 2){
								level=2;
							}
							actfedid = jsonDataSub2Sub[0].activityfeed_id;
							activityfeedid = jsonDataSub2Sub[0].activityfeed_id;
							handleFileUploadForComments(files,actfedid);
							$('#loadingBar').show();
							timerControl("start");
						}

					}
			});
		}
		imgfeedId= feedId;
		feedID = "";
		if(dragndrop != "dragndrop"){
			level=0;
		}
		
	}
   
		/*deleting activity feeds starts */
	var dMenuType="";
	var dMenuTypeId="";
	var archivedstatus="";
  function deleteMainActFeed(feedId,projId,menuType,menuTypeId){
     if(archivedstatus== 'A'){
	      alertFun(getValues(companyAlerts,"Alert_ProjectInactive"),'warning');
		  return false;
  	 }else{
  		 mainFeedId = feedId;
	  	 mainFeedProjId= projId;
	  	 dMenuType=menuType;
	  	 dMenuTypeId=menuTypeId;
	  	 if(menuType == "activityFeed"){
	  	    confirmFun(getValues(companyAlerts,"Alert_DelActFeed"),"delete","deleteMainActivityFeed");
	  	 }else{
	  	    confirmFun(getValues(companyAlerts,"Alert_DelComment"),"delete","deleteMainActivityFeed");
	  	 }
  		 
	  	//deleteMainActivityFeed();
  	 }
}		
	
	function deleteMainActivityFeed(){
		
		$('#loadingBar').show();
	  	timerControl("start");
		$.ajax({
			url: apiPath+"/"+myk+"/v1/deleteFeed?feed_id="+mainFeedId+"&user_id="+userIdglb+"&place="+menuutype+"",
	    	type: "DELETE", 
	    	error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					},  
	    	success:function(data) {
	    		
				$('#loadingBar').hide();
				timerControl("");
				if(data == 'failure'){
					//parent.alertFun("You dont have permission to delete this feed","warning");
					// parent.$('#loadingBar').hide();
				    // parent.timerControl("");
				}else{
					
				}
				$('#actFeedMainDiv_'+mainFeedId).remove();
			
			    if(dMenuType =='agile'){
			        if(commentPlace=='Story'){  
	 		            if($('#epicListCommonDiv_'+dMenuTypeId).children('div').length < 1){
	 		    		   var noComments =  "<div style=\"text-align:center;font-size:12px;\">" ;
				            noComments+="  <b><span>"+getValues(companyLabels,"Nocmtavail")+"</span></b>";
				 			noComments+="</div>";
				 			$('#epicListCommonDiv_'+dMenuTypeId).html(noComments);
	 		    		}
 		    		}else{
 		    		  if($('#commentListDiv').find('div.ideaCommentListDiv').children('div.actFeedsIdea').length < 1){
 		    		    var noComments =  "<div style=\"text-align:center;font-size:12px;\">" ;
				            noComments+="  <b><span>"+getValues(companyLabels,"Nocmtavail")+"</span></b>";
				 			noComments+="</div>";
				 		$('#commentListDiv').find('div.ideaCommentListDiv').html(noComments);
 		    		  }
 		    		}
			    } 
			    
			    if(dMenuType =='galleryComment'){
			       fetchPhotoCommentCount(dMenuTypeId,mainFeedProjId);
			    }
			    
			    if(dMenuType =='Task'){
			    	if($('#Taskcomment_'+dMenuTypeId).children('div.TaskCommentListDiv').children().length < 1){
 		    		    var noComments =  "<div id=\"taskNoComments\" style=\"text-align:center;font-size:12px;margin-right: 15px;margin-top: 4px;\">" ;
				        noComments+="  <b><span>"+getValues(companyLabels,"Nocmtavail")+"</span></b>";
				        noComments+="</div>";
				 		$('#Taskcomment_'+dMenuTypeId).children('div.TaskCommentListDiv').html(noComments);
				 		$("#Taskcomment_"+dMenuTypeId+"").children('div.TaskCommentMainTextareaDiv').show();
				 		$('#taskCommentNewUI_'+dMenuTypeId).attr('src','images/Commentdot2.png');
				 		$('.cmntsIcon').find('img#cIcon_'+dMenuTypeId).show().attr('src','images/commentWhite2.png').addClass("hover-comment-icons");
						$('.cmntsIcon').find('img#cIcon_'+dMenuTypeId).attr('title','Add comments')
		    		}else{
		    			$("#Taskcomment_"+dMenuTypeId+"").children('div.TaskCommentMainTextareaDiv').hide();
		    			$('#taskCommentNewUI_'+dMenuTypeId).attr('src','images/commenton2.png');
		    			$(".docListViewBorderCls").find('.cmntsIcon').find("img#cIcon_"+dMenuTypeId+"").attr('src','images/commentBlack2.png').removeClass("hover-comment-icons");
		    		}     
				}
			    
			    if(dMenuType =='blog'){
			       var isiPad = navigator.userAgent.match(/iPad/i) != null;
			       if(!isiPad){
						$('div#articleDetailData').mCustomScrollbar("update");
				    }
			    }
			     if(dMenuType =='document'){
			     $("#comment_"+dMenuTypeId).css('padding','0');
			       if(data == ''){
			          $("#cIcon_"+dMenuTypeId).removeAttr('src');
			          $("#cIcon_"+dMenuTypeId).attr('title','');
			       };
			    }
			 
			  /*This condition is to hide the comments icon in ideas module, when there is no comments. 02-05-2019*/ 
			     
			    if(dMenuType == 'wsIdea'){
			  		if($('div[id^="comment_"]').find('div[id^="actFeedMainDiv_"]').length <1){
			  			$('#comment_'+dMenuTypeId).hide();
			  			$('#cIcon_'+dMenuTypeId).remove();
			  		}
			    }
			    
				$(".loadingBar").hide();
				timerControl("");
				feedID="";
				//reloadComments(mainFeedProjId);
				//loadActivityFeed(notFolid);
	    	}
	  	});
  	}	
  	
  	function chkeditcommentfeed(){
  		$("body").find(".editTextArea").hide();
         	$("body").find(".actFeedHoverImgDiv").show();
         	$("body").find(".actualComments").show();
	         commentval = "";
	         feedEditId = "";  	
  			
  	}
  	function posteditfeed(){
  		//console.log("1 "+feedID+"2 "+LoginId+"3 "+Proje+"4 "+MenuTyp+"5 "+MenuTypId+"6 "+SuperPId);
  		postEditedCmt(feedEditId);//,LoginId,Proje,MenuTyp,MenuTypId
  			//$("#EdidCmtBlock_"+feedEditId).focus();
  	}

	  function editComment(Id,menuType){
		// $('actFeedDiv_'+Id).css({'display' :'none'});
		//alert("HIIIII");
		feedEditId = Id;
		var userId="";
		const UI =
					'<div id="editDivContainer_' +Id +'"   class="editDiv py-3" style="display:block">' +
						"<div class=\"col-12 p-0\">"+
							"<textarea id=\"editBlock_"+Id+"\" class=\"main_commentTxtArea rounded\" onkeyup='checkTag(event,\"edit\",this,"+prjid+",\"activityFeed\"); commentAutoGrow(event,this,\"edit\","+Id+");' onblur ='commentAutoGrow(event,this,\"edit\","+Id+");' oninput ='commentAutoGrow(event,this,\"edit\","+Id+");' onkeypress='checkTag(event,\"edit\",this,"+prjid+",\"activityFeed\"); commentAutoGrow(event,this,\"edit\","+Id+");' ></textarea>" +
							'<input type="hidden" id="menuTypeForComment" value="activityfeed"><input type="hidden" id="ProjIdForCommentEdit" value="' +prjid +'">'+
							'<div class="atHashContainerEdit" style="display: table; width: 100%;height: 2px;"></div>'+					
						'</div> ' +
						'<div class="col-12  p-0 d-flex justify-content-between mt-1" >'+
							'<div class="d-flex justify-content-between "> '
								+"<div class=\"py-1 px-2 convoSubIcons\" onmouseover=\"changeSmileyellow('edit',"+Id+");\" onmouseout=\"changeSmilewhite('edit',"+Id+");\"><img src=\"/images/conversation/smile.svg\"  id=\"smilePop_"+Id+"\"  title=\"\" class=\"image-fluid\" style=\"height:22px;cursor:pointer;\"  onclick=\"smilePop();\" ></div>"
								+'<div class="py-1 px-2 convoSubIcons"><img src="/images/conversation/at.svg" id="" title="" class="atImgExt image-fluid" style="height:20px;cursor:pointer;" onclick="externalAtHashClick(\'edit\', '+Id+', \'@\')"></div>'
								+'<div class="py-1 px-2 convoSubIcons" id="hashtagConvedit"><img src="/images/conversation/hash.svg" id="" title="" class="hashImgExt image-fluid" style="height:19px;cursor:pointer;" onclick="externalAtHashClick(\'edit\', '+Id+', \'#\')"></div>'+
								
							'</div>'+
							'<div class="d-flex justify-content-between">'+
								'<div class="p-1 mx-1"><img src="/images/conversation/post.svg" onclick="postEditedCmt('+Id+');" title="Post" class="postButton " style="height:26px;cursor:pointer;"></div>'+
							'</div>'+
						'</div> ' +
					"</div>";
					
					if ($("#editDivContainer_"+Id).length==0) {
						commentval=$('#actFeedDiv_'+Id).find('#conversationcontent').html();
						// alert(commentval);
						commentval=commentval.replaceAll("<br>","\n");
						commentval=commentval.replace(/<a[^>]*>|<\/a>/g, "");
						commentval=commentval.replaceAll("&amp;","&");
						// alert(Id);
						$(UI).insertAfter('#actFeedDiv_' + Id).show('slow')
						
						// $('#actFeedDiv_'+Id).replaceWith(UI);
						$('#actFeedDiv_'+Id).hide();
					//	$('#actFeedDiv_'+Id).replaceWith(UI);
						
						$("textarea#editBlock_"+Id).val(commentval).focus();
						
						$("#editBlock_" + Id).focus();
						emojiClick('edit',Id);
					} 
	
					
	}
  	
  	function EditActComments(Id,menuType,menuTypeId){
  		feedEditId = Id;
  		$('#actFeedTot_'+Id).find('.tasksIcon').hide();
  		$('#actFeedMainDiv_'+Id).children('.actFeedHover').find('.hideDivsOnEdit').hide();
		var tempH ="";
		if(menuType == 'activityFeed'){
			tempH = $("#actFeedDiv_"+Id+"").height();
		}
		else if(menuType == 'document'){
			tempH = $("#actFeedDiv_"+Id+"").height();
		}
		else if(menuType == 'galleryComment' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'Task' || menuType == 'wsIdea' || menuType == 'agile'){
			tempH = $("#actFeedTot_"+Id+"").height();
		}
		if(tempH <45){
		   tempH=45;
		}
		$("#EdidCmtBlock_"+Id+"").css('height',tempH+'px');
		$("#EditCmtDivContainer_"+Id+"").find("div[id^=topDiv_]").css('height',tempH+'px');
		tempH /= 2;
		tempH -= 15;
		$("#EditCmtDivContainer_"+Id+"").find("div[id^=pstFeed_]").css('margin-top',tempH+'px');
		$("#EditCmtDivContainer_"+Id+"").css('height','');
		$('.replycommentDiv').hide();
		$('.editTextArea').hide();
		$('.actualComments').show();
		$('.actFeedHoverImgDiv').show();
		$('#actFeedDiv_'+Id).hide();
		$('#actFeedImg_'+Id).hide();
		$('#subcommentDiv_'+Id).hide();
		$('.actFeedOptionsDiv').hide();
		commentval=$('#actFeedDiv_'+Id).find('.actFeed').html();
		commentval=commentval.replaceAll("<br>","\n");
		commentval=commentval.replace(/<a[^>]*>|<\/a>/g, "");
		commentval=commentval.replaceAll("&amp;","&");
		
		$("textarea#EdidCmtBlock_"+Id).val(commentval).focus();
		$('div#EditCmtDivContainer_'+Id).show();
		$('#EdidCmtBlock_'+Id).show();
		$("#actFeedTextArea_"+Id).show();
		$("#EditDiv_"+Id).show();
		$("#EditComment_"+Id).show();
		$("textarea#EdidCmtBlock_"+Id).focus();
	 }
	
	 function postEditedCmt(actFeedId){
		var menuType = menuutype;
		var projId = prjid;
		var menuTypeId = prjid;
	   var userEditedCmt = $("#editBlock_"+actFeedId+"").val().trim();
	   if(userEditedCmt ==""){
		   $("#editBlock_"+actFeedId+"").focus();
		   return false;
	   }
	   
	   var jsonData = '';
	   var localOffsetTime=getTimeOffset(new Date());
	   var selectedUser=null;
	   var patt = /#[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+/g;
	   var pattern1 = /@\w+/g;
	   var selectedUserName="";
	   var projHashTag=null;
	   var projHashTagName="";
	   
	   //-------Below code is to check English or Non English and also to whether it is exceeding toal characters length.
	  // var LengthFlag = false;
	  // var nonFlag = isNonLatinCharacters(userEditedCmt);
	   //console.log('nonFlag:'+nonFlag);
	  /*  if(!charExceedFlag){
			   if(nonFlag){
				   if(userEditedCmt.length >665){
					   LengthFlag = true;
				   }
			   }else{
				   if(userEditedCmt.length >3900){
					   LengthFlag = true;
				   }
			   }
	   }else{
			   if(nonFlag){
				   if(userEditedCmt.length>665){
					   LengthFlag = false;
					   userEditedCmt = userEditedCmt.substring(0, 665);
				   }
			   }else{
				   if(userEditedCmt.length>3900){
					   LengthFlag = false;
					   userEditedCmt = userEditedCmt.substring(0, 3900);
				   }
			   }
	   }
	   if(LengthFlag){
		   confirmFun(getValues(companyAlerts,"Alert_Conv_Exceeding"),"delete","ExceedCharactersConfirmForEdit");
		   charExceedFeedId = actFeedId ;
		   return false;
	   } */
	   //-------Above code is to check English or Non English and also to whether it is exceeding toal characters length.
	   
	   
	   if(menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'document' || menuType == 'galleryComment'){
		   while(projHashTag=patt.exec(userEditedCmt)){
			   projHashTagName=projHashTagName+projHashTag+",";
		   }
	   }
	   
	   if(menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'document' || menuType == 'galleryComment'){
		   while(selectedUser=pattern1.exec(userEditedCmt)){
			   selectedUserName=selectedUserName+selectedUser+",";
			}
	   }
	   var feedType="";
		   if(menuType == 'agile'){
			   feedType=menuType;
		   }else if(menuType == 'Task'){
			   feedType=menuType;
		   }
	   
	   $('#loadingBar').show();
	   timerControl("start");
	  var parentFeedId = $('#actFeedDiv_'+actFeedId).attr('parentfeedid');
	  // var userEditedCmt = $('div.EditCmtContents').find('div#EditCmtDiv_'+actFeedId).find('textarea#EdidCmtBlock_'+actFeedId).text();

	  let menu = globalmenu == 'Task' ? globalmenu : $('#otherModuleTaskListDiv').is(':visible') == true ? "Task" : globalmenu == 'Idea' ? 'wsIdea' : globalmenu == 'agile' || SBmenu=="SBstories" || globalmenu == 'sprint' ? "agile" : globalmenu == 'wsdocument' ? "wsDocument" : menuutype;
	  menuTypeId = (globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true) ? $("#task_id").val() : globalmenu == 'Idea' || globalmenu == 'agile' || SBmenu=="SBstories" || globalmenu == 'sprint' || globalmenu=='wsdocument' ? $('.popupComments').attr('sourceid') : '0';
	  feedType = globalmenu == 'Task' ? globalmenu : $('#otherModuleTaskListDiv').is(':visible') == true ? "Task" : globalmenu == 'Idea' ? globalmenu : globalmenu == 'agile' || SBmenu=="SBstories" || globalmenu == 'sprint' ? "agile" : globalmenu == 'wsdocument' ? "wsDocument" : feedType;
	  if(globalmenu == 'agile' || SBmenu=="SBstories"){
		commentPlace="Story";
	  }else if(globalmenu == 'sprint'){
		commentPlace="Sprint";
	  }

	  let jsonbody = {
		   "menuType":menu,
		   "project_id":projId,
		   "feedId":actFeedId, ////   in case of create 0 in case of update the activity_feed_id
		   "localOffsetTime":localOffsetTime,
		   "projHashTagName":projHashTagName,
		   "selectedUserName":selectedUserName,
		   "user_id":userIdglb,
		   "company_id":companyIdglb,
		   "comments":userEditedCmt,
		   "action":"fetchinsertFeed",
		   "menuTypeId":menuTypeId,
		   "subMenuType":commentPlace,
		   "feedType":feedType,
		   "parent_feed_id":parentFeedId,
		   }

		   $('#loadingBar').addClass('d-flex');
		   //timerControl("start");
		   $.ajax({
			   url: apiPath+"/"+myk+"/v1/postConversation",
			   type:"POST",
			   dataType:'json',
			   contentType:"application/json",
			   data: JSON.stringify(jsonbody),
			   /* data:{act:"postEditedComment",comments:userEditedCmt,projectId:projectId,feedId:actFeedId,sortType:'',localOffsetTime:localOffsetTime,
				   limit:'null', index:'null',menuType:menuType, subMenuType:commentPlace, menuTypeId:menuTypeId,projHashTagName:projHashTagName,
				   selectedUserName:selectedUserName}, */
			   error: function(jqXHR, textStatus, errorThrown) {
					   checkError(jqXHR,textStatus,errorThrown);
					   $('#loadingBar').addClass('d-none').removeClass('d-flex');
					   timerControl("");
					   }, 
				success:function(result){
							//  alert("result---"+result);
							
						  // Below variables are used in case of total characters exceeds the length
						  charExceedFeedId = "";
						  charExceedFlag = false;
							
						   commentval = "";
						   feedEditId = "";
						   
							var CreatedTime = result[0].created_time;

							if(typeof(result[0].created_elapsed_time) != "undefined"){
								var createdElapTime = result[0].created_elapsed_time; 
								createdElapTime = calculateElapseTime(createdElapTime);
							}else{
								createdElapTime = CreatedTime;
							}
							// alert("createdElapTime :"+createdElapTime);
							// var updatedTime = result[0].last_updated_time;
						   var last_updated_timestamp= result[0].last_updated_time;
						   var modifiedElapTime = result[0].modified_elapsed_time; 

						   if(typeof(result[0].modified_elapsed_time) != "undefined"){
							   modifiedElapTime = result[0].modified_elapsed_time; 
							   modifiedElapTime = calculateElapseTime(modifiedElapTime);
						   }else{
							   modifiedElapTime = last_updated_timestamp;
						   }
						//    alert("modifiedElapTime :"+modifiedElapTime);
					   
						   var commentedName= result[0].name;
						   $('#editDivContainer_'+actFeedId).remove();
						   $('#actFeedDiv_'+actFeedId).show();
						   // $('#editDivContainer_'+actFeedId).replaceWith('#actFeedDiv_'+actFeedId);
						   $('#actFeedTot_'+actFeedId).find('.tasksIcon').show();
						   
						   $('#actFeedMainDiv_'+actFeedId).find('#actFeedTot_'+actFeedId).show();
						   $('#actFeedDiv_'+actFeedId).show();
						   $('#actFeedImg_'+actFeedId).show();
						   
						//    if($('#actFeedDiv_'+actFeedId).find('#personname').length<1){
							$('#actFeedDiv_'+actFeedId).find('#personname').empty();
							  var modifHtml = ''+commentedName+'<span class="ml-2" style="color:#999999;font-size:12px;"><span>'+getValues(companyLabels,"Posted_on")+' : '+createdElapTime+'</span>&nbsp;|&nbsp;<span>'+getValues(companyLabels,"Modified_On")+' : '+modifiedElapTime+'</span></span>';
							  $('#actFeedDiv_'+actFeedId).find('#personname').append(modifHtml);
						//    }
						//    else{
						// 	  $('#actFeedDiv_'+actFeedId).find('.defaultNameDateTimestamp').find('span.modifData').attr('title',last_updated_timestamp).text(modifiedElapTime);
						//    } 
							   
						   $('#subcommentDiv_'+actFeedId).show();
						   if(userEditedCmt.indexOf("http:")>=0 ||userEditedCmt.indexOf("www.")>=0){
							   var temp="";
							   //userEditedCmt=userEditedCmt.replace("\n"," \n");
							   var dataArray=userEditedCmt.split("\n");
							   var subdataArray="";
							   for(var j=0; j<dataArray.length; j++){
								 subdataArray=dataArray[j].split(" ");
								 for(var i=0; i<subdataArray.length; i++){
								   if(subdataArray[i].indexOf("http:")>=0)
									   temp+=" <a style=\"text-decoration:underline; color:blue;\" target=\"_blank\" href=\"" + subdataArray[i] + "\">"+ subdataArray[i] + "</a> ";
								   else if(subdataArray[i].indexOf("www.")>=0)
									   temp+=" <a style=\"text-decoration:underline; color:blue;\" target=\"_blank\" href=\"http://" + subdataArray[i] + "\">"+ subdataArray[i] + "</a> ";
								   else if(subdataArray[i].indexOf("https:")>=0)
									   temp+=" <a style=\"text-decoration:underline; color:blue;\" target=\"_blank\" href=\""+ subdataArray[i] + "\">"+ subdataArray[i] + "</a> ";
								   else
									   temp+=" "+subdataArray[i];
								 }temp+="</br>";
							   }
							   $('#actFeedDiv_'+actFeedId).find('.actFeed').html(temp);
						   }else{
							   userEditedCmt=userEditedCmt.replaceAll("\n","<br>");
							   $('#actFeedDiv_'+actFeedId).find('#conversationcontent').html(userEditedCmt);
							}
							   //loadActivityFeed('', 'firstClick');
					elementindex = '';
					hashInd='';
					if(globalmenu == 'agile' || SBmenu=="SBstories" || globalmenu == 'sprint'){
						commentPlace="";
					}
						$('#loadingBar').addClass('d-none').removeClass('d-flex');
					   //timerControl("");
			 }
		 });
		  
   }
	
 var charExceedMenuTypeId;
 var charExceedMenuType;
 var charExceedFeedId;
 var charExceedFlag = false;

 function CommentActFeed(menuTypeId,menuType,dragndrop,clevel){	
	
		if(menuTypeId == "conversationplace"){
			menuTypeId = prjid;
		}
		///alert(menuType+"<--menuType-->"+menuTypeId+"<--menuTypeId--projId-->"+projId);
		var postCanvaslevel = '0';
		
	  
        var localOffsetTime=getTimeOffset(new Date());
		var userComment = "";
		
		if(menuType == 'Task'){
			userComment =  $('#Taskcomment_'+myTaskId).find('textarea#main_commentTextarea').val().trim();
		}else{
			userComment =  $('textarea#main_commentTextarea').val().trim();
		}

		if(typeof(imageShre)!= "undefined" && imageShre!= ""){
			postCanvasToURLImage('0',prjid,menuType,menuTypeId,postCanvaslevel);
			// if(commentPlace=='ScrumBoard'){
				//projId=projId;
				//alert("inside ScrumBoard scrProjId:"+projectId)
				// alert("ScrumBoard is valid")
				// $('#convoPost').children().eq(1).attr('onclick',"postCanvasToURLImage('',"+prjid+",'"+menuType+"',"+menuTypeId+",'"+postCanvaslevel+"')");
				//  postCanvasToURLImage('',prjid,menuType,menuTypeId,postCanvaslevel);	
			// }else{
			// 	// alert("ScrumBoard is not valid")
			// 	// $('#convoPost').children().eq(1).attr('onclick',"postCanvasToURLImage('',"+prjid+",'"+menuType+"',"+menuTypeId+",'"+postCanvaslevel+"')");
			// 	 postCanvasToURLImage('',prjid,menuType,menuTypeId,postCanvaslevel);	
			// }
			 //$('#convoPost').children().eq(1).attr('onclick',"postCanvasToURLImage('',"+projId+","+menuType+","+menuTypeId+")");
			 //postCanvasToURLImage('',projId,menuType,menuTypeId);	  
			 
			 imageShre=""; 
		}
		
		var projHashTag = null;
		var selectedUser=null;
		var patt = /#[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+/g;
		var pattern1 = /@\w+/g;
		var projHashTagName="";
		var selectedUserName="";
		var taskid = "";
		if(menuType == 'Task'){
			taskid = myTaskId;
		}else if(menuType == 'agile'){
			taskid = menuTypeId;
		}
		if(menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'blog' || menuType == 'wsDocument' || menuType == 'document' || menuType == 'galleryComment'){
			while(projHashTag=patt.exec(userComment)){
				projHashTagName=projHashTagName+projHashTag+",";
			 }
		}
		if(menuType == 'activityFeed' || menuType == 'wsIdea' || menuType == 'Task' || menuType == 'agile' || menuType == 'wsDocument' || menuType == 'blog' || menuType == 'document' || menuType == 'galleryComment'){
			while(selectedUser=pattern1.exec(userComment)){
		    	selectedUserName=selectedUserName+selectedUser+",";
			}
		}
		
		/*Added this condition because after the comments get posted. Comments icon should be displayed. 02-05-2019*/
		
		if(menuType == 'wsIdea'){
			var imgHtml =  "<img id=\"cIcon_"+menuTypeId+"\" onclick = \"showIdeaIconComments('cIcon_"+menuTypeId+"', "+menuTypeId+")\" src=\"images/commentToggle.png\"  title=\"View Comments\" class = \"ideaIconsContainerCss commentsIconPresentCss\" style=\"cursor:pointer;\" />"
	  		
	  		if($("#epicContainer_"+menuTypeId).children().hasClass("commentsIconPresentCss")){
	   		}else{	
	   			$("#epicContainer_"+menuTypeId+"").append(imgHtml);
	   		}
		}
      	var jsonData = '';
		

		/*if(archivedstatus == 'A'){
			// parent.alertFun("Project is Inactive.",'warning');
			return false;
		}*/
		if(userComment.length == 0){
			//parent.alertFun("<bean:message key='activity_empty.message'/>","warning")
			$('textarea#main_commentTextarea').val('');
			$('textarea#main_commentTextarea').focus();
			return false;
		}
		
			//-------Below code is to check English or Non English and also to whether it is exceeding toal characters length.
			//var LengthFlag = false;
			//var nonFlag = isNonLatinCharacters(userComment);
			//console.log("nonFlag:"+nonFlag);
			/* if(!charExceedFlag){
					if(nonFlag){
						if(userComment.length >665){
							LengthFlag = true;
						}
					}else{
						if(userComment.length >3900){
							LengthFlag = true;
						}
					}
			}else{
					if(nonFlag){
						if(userComment.length>665){
							LengthFlag = false;
							userComment = userComment.substring(0, 665);
						}
					}else{
						if(userComment.length>3900){
							LengthFlag = false;
							userComment = userComment.substring(0, 3900);
						}
					}
			}
			if(LengthFlag){
				confirmFun(getValues(companyAlerts,"Alert_Conv_Exceeding"),"delete","ExceedCharactersConfirmMain");
				charExceedMenuTypeId = menuTypeId;
				charExceedMenuType = menuType;
				return false;
			} */
			//-------Above code is to check English or Non English and also to whether it is exceeding toal characters length.
			if(menuType  == 'agile' && (commentPlace == 'Story' || commentPlace == 'ScrumBoard')){
				//jiraStoryComment(projectId,menuTypeId,userComment);
			}
			var feedType="";
			if(menuType == 'agile'){
				feedType=menuType;
			}else if(menuType == 'Task'){
				feedType=menuType;
			}
			let jsonbody = {
				"menuType":menuType,
				"project_id":prjid,
				"feedId":"0", ////   in case of create 0 in case of update the activity_feed_id
				"localOffsetTime":localOffsetTime,
				"projHashTagName":projHashTagName,
				"selectedUserName":selectedUserName,
				"user_id":userIdglb,
				"company_id":companyIdglb,
				"comments":userComment,
				"action":"fetchinsertFeed",
				"menuTypeId":taskid,
				"subMenuType":commentPlace,
				"feedType":feedType
			}
			//console.log("jsonbody-->"+JSON.stringify(jsonbody));
			$('#loadingBar').addClass('d-flex');
			timerControl("start");
			$.ajax({
	        	url: apiPath+"/"+myk+"/v1/postConversation",
				type:"POST",
				dataType:'json',
				contentType:"application/json",
				data: JSON.stringify(jsonbody),
				/* data:{act:"postActivityFeed", comments: userComment, projectId: projectId, feedId:"0",localOffsetTime:localOffsetTime,
						menuType:menuType,subMenuType:commentPlace,menuTypeId:menuTypeId,sortType:'', limit:'null', index:'null',
						projHashTagName:projHashTagName,selectedUserName:selectedUserName}, */
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
						//alert("status->"+jqXHR.status);
		                $('#loadingBar').addClass('d-none').removeClass('d-flex');
						timerControl("");
						}, 
						success:function(result){
									
							        callsocket(result);
								   // Below 3 variables used in case of total characters exceeds the length
								   /* charExceedMenuTypeId = "";
								   charExceedMenuType = "";
								   charExceedFlag = false; */
								  
								    commentval = "";
									feedEditId = "";
									jsonData = result;
									
									for (var i = 0; i < jsonData.length ; i++) {
										feedId = jsonData[i].activityfeed_id;
									}
									if(menuType == "activityFeed"){
										if($('#actFeedMainDiv_'+feedId).length < 1){
											result = prepareCommentsUI(jsonData,menuType,menuTypeId);
											feedId ="";
										}else{
											$('#actFeedTot_'+feedId).removeClass('notHightlightCls');
											$('#actFeedTot_'+feedId).find('div[id^=actFeedTot_]').removeClass('notHightlightCls');
										}
									}else{
										result = prepareCommentsUI(jsonData,menuType,menuTypeId);
									}
	
									if(menuType=="activityFeed"){
										$("#comment_"+menuTypeId).html("");
										// $("div#activityFeedList").html("");
										$('textarea#main_commentTextarea').val('').trigger('blur');
										
										if($('#actFeedMainDiv_'+feedId).length < 1){		
											$("div#activityFeedList").prepend(result);
										}
										//loadCustomLabel('actFeed');
										//sendToSlack(projectId,'conversations');
										//sendToSpark(projectId,'conversations');
									}else if(menuType=="wsDocument" || menuType=="document"){
										$("#comment_"+menuTypeId).html("");
										$("#comment_"+menuTypeId).append(result);
										$("#commentTextarea").hide();
									}else if(menuType == "blog"){
										$('textarea#main_commentTextarea').val('');
										$("#blogCommentsDiv_"+menuTypeId).html(result);
										$.ajax({ 
											url: path+"/blogAction.do",
											type:"POST",
											data:{act:"blogCommentCount",projectId: projectId,menuTypeId:menuTypeId},
											error: function(jqXHR, textStatus, errorThrown) {
												checkError(jqXHR,textStatus,errorThrown);
												$("#loadingBar").hide();
												timerControl("");
											}, 
											success:function(result){
												checkSessionTimeOut(result);
												//alert("menuTypeId::"+menuTypeId);
												//alert("Blog result:::"+result);
												$("#blogCommentshow_"+menuTypeId).html(result);
												var isiPad = navigator.userAgent.match(/iPad/i) != null;
												if(!isiPad){
													$('div#articleDetailData').mCustomScrollbar("update");
												}
											}
										});
										$(".mainCmtUicon").css('width','3.5%');
										h = $(window).height()-$("#header_outer").height()-$("#tabMainDiv").height();
										var hCHeight = eval(h); 
										if($("#articleDetailData").height() > hCHeight){
											$("#blogArticleDiv").css("height","");
										}else{
											$("#blogArticleDiv").css("height",hCHeight-6);
										}
									}else if(menuType== "galleryComment"){
										$('textarea#main_commentTextarea').val('');
										$("#gCommentDiv").html(result);
										if(!isiPad){
											popUpScrollBar('gCommentDiv');
											$("#gCommentDiv").mCustomScrollbar("update");
										}
										fetchPhotoCommentCount(menuTypeId,projectId);
	
									}else if(menuType== "wsIdea"){
										$("#comment_"+menuTypeId).html(result);
										$("#commentTextarea").hide();
										sendToSlack(projectId,'idea_comment');
										//sendToSpark(projectId,'idea_comment');
	
									}else if(menuType== "Task"){
										$("#Taskcomment_"+menuTypeId+"").children('div.TaskCommentMainTextareaDiv').hide();
										$("#Taskcomment_"+menuTypeId).children('div.TaskCommentListDiv').html(result);
										$('#taskCommentNewUI_'+menuTypeId).attr('src','images/commenton2.png');
										$('.cmntsIcon').find('img#cIcon_'+menuTypeId).show().attr('src','images/commentBlack2.png').removeClass("hover-comment-icons");
										$('.cmntsIcon').find('img#cIcon_'+menuTypeId).attr('title','View comments')
										$('textarea#main_commentTextarea').val('');
										$("#Taskcomment_"+menuTypeId).find(".TaskCommentListDiv").children('div[id^="actFeedMainDiv_"]').css("border-bottom","none");
									}else if(menuType== "agile"){
										$('textarea#main_commentTextarea').val('');
										if(commentPlace=='Story'){
											$("#epicListCommonDiv_"+menuTypeId).html(result);
										}else{
											if(!isiPad){ 
												$('#commentListDiv .mCSB_container').html(result);
												$("#commentListDiv").mCustomScrollbar("update");
											}else{
												$('#commentListDiv').html(result);
											}
										}
										loadAgileCustomLabel("fetchComments");
										//sendToSlack(projectId,'agile_comment',menuTypeId);
										//sendToSpark(projectId,'agile_comment',menuTypeId);
	
									}
									if(patt.exec(userComment)){
										ajaxCallforHashCode(menuType,'','main');
									}
	
									if(menuType == "Task"){
										/*if($('#Taskcomment_'+taskId).children('div.TaskCommentListDiv').children().length < 1){
				 					$(".docListViewBorderCls").find('.cmntsIcon').find("img#cIcon_"+taskId+"").attr('src',path+'/images/commentWhite.png');
				 				}else{
				 					$(".docListViewBorderCls").find('.cmntsIcon').find("img#cIcon_"+taskId+"").attr('src',path+'/images/commentBlack.png');
				 				}
							    $('.cmntsIcon').find('img#cIcon_'+menuTypeId).removeAttr('onclick').attr('onclick','hideTaskCmnts("'+menuTypeId+'");event.stopPropagation();'); */
									}else{
										$('.cmntsIcon').find('img#cIcon_'+menuTypeId).show().attr('src','images/commentToggle.png');
										$('.cmntsIcon').find('img#cIcon_'+menuTypeId).removeAttr('onclick').attr('onclick','hideCmnts("'+menuTypeId+'")');
									}
									
									if(dragndrop == "dragndrop"){
										if(clevel == 0){
											level=1;
										}
										actfedid = jsonData[0].activityfeed_id;
										activityfeedid = jsonData[0].activityfeed_id;
										//alert("here");
										handleFileUploadForComments(files,actfedid,dragndrop);
										
									}

								imageShre="";
								$('#main_commentTextarea').css('background-image','none');
								$('#rowTab').find('.convopost').attr('src', '/images/conversation/post1.svg');
	
								$('#loadingBar').addClass('d-none').removeClass('d-flex');
								//loadActivityFeed(notFolid);
								//reloadComments(projectId);
								//   parent.$('#loadingBar').hide();
								//	parent.timerControl("");
						}
				//}
	       	 });
				elementindex= '';
				hashInd='';
				// imgfeedId= feedId;
	}
 
 function ExceedCharactersConfirmMain(){
	 charExceedFlag = true;
	 CommentActFeed(charExceedMenuTypeId, charExceedMenuType);
 }
 
 function ExceedCharactersConfirmForEdit(){
	 charExceedFlag = true;
	 $("#pstFeed_"+charExceedFeedId).trigger("click");
 }
 
 function ExceedCharactersConfirmForReply(){
	 charExceedFlag = true;
	 $("#replyFeed_"+charExceedFeedId).trigger("click");
 }
 
 function hideActFeedOptDiv(obj){
   if($(obj).is(':visible')){
     //console.log("visible");
    }else{ 
     $(obj).parent().children('div.actFeedOptionsDiv').hide();
   }
 }
 
  function commentAutoGrow(e, obj, level, feedId){
	// console.log('keyCode:'+e.keyCode);
	// console.log('keyCode:'+e.key);
	if (e.keyCode == 13 || e.keyCode == 8 || e.keyCode == 91 ){
		let scrollHeight = $(obj).get(0).scrollHeight-20;
        //console.log('scrollHeight:'+scrollHeight);
		//alert('scrollHeight:'+scrollHeight);
        if(level=='task' && scrollHeight >= 35 && scrollHeight < 240){
            $(obj).css('height', scrollHeight+10+'px');
			$(obj).css('padding-top','15px');
        }else if(level!="task" && scrollHeight < 240){
			$(obj).css('height', scrollHeight+10+'px');
		}
		if(scrollHeight >= 240){
            $(obj).css('overflow-y', 'auto');
        }else{
			$(obj).css('overflow-y', 'hidden');
		}
	}
	if($(obj).val().length <1 && $(obj).css('background-image') == "none"){
		if(level=='task'){
			$(obj).css('height', '35px');
			$(obj).css('padding-top','8px');
		}else{
			$(obj).css('height', 'auto');
		}
		if(level=='main'){
			$('#rowTab').find('.convopost').attr('src', '/images/conversation/post1.svg');
		}else if(level=='chat'){
			$('#cmecontent').find('.chatpost').attr('src', '/images/conversation/post1.svg');
			// $('#cmecontent').find('.chatpost2').attr('src', '/images/conversation/post1.svg');
		}else if(level=='reply'){
			$('#replyDivContainer_'+feedId).find('.postButton').attr('src', '/images/conversation/post1.svg');
		}else if(level=='edit'){
			$('#editDivContainer_'+feedId).find('.postButton').attr('src', '/images/conversation/post1.svg');
		}
	}else{
		if(level=='main'){
			$('#rowTab').find('.convopost').attr('src', '/images/conversation/post.svg');
		}else if(level=='chat'){
			$('#cmecontent').find('.chatpost').attr('src', '/images/conversation/post.svg');
			// $('#cmecontent').find('.chatpost2').attr('src', '/images/conversation/post.svg');
		}else if(level=='reply'){
			$('#replyDivContainer_'+feedId).find('.postButton').attr('src', '/images/conversation/post.svg');
		}else if(level=='edit'){
			$('#editDivContainer_'+feedId).find('.postButton').attr('src', '/images/conversation/post.svg');
		}
	}
  }


 var atIndex = "";
 var hashIndex = "";
 var regex = new RegExp("^[a-zA-Z0-9]+$");

 	//Function Added for @ AND # For Comment Level

	 function checkTag(e,type,obj,menuTypeId,menuType){
			//var testAtData = $(this).val();
			// var testAtChar = document.getElementById('main_commentTextarea').selectionStart;
			// var testAtIndex = parseInt(testAtChar)-1;
			//elementindex='',elementhashindex='';atIndex="";hashIndex = "";
			if ((e.keyCode == 10) && e.ctrlKey){		
				
				if(type=='main'){
					CommentActFeed(menuTypeId,menuType);
					$('#tabContentDiv').find('.atTag').attr('class','atTag col-12 d-none justify-content-center');
					$('.activityfieldUserListMain').hide();
					$('.projUserListMain').hide();
				}else{
					var feedId = $(obj).attr('id').split("_")[1];
					postReply(feedId);
					$('replyDivContainer_'+feedId).find('.atTag').attr('class','atTag col-12 d-none justify-content-center');
					$('.activityfieldUserListMain').hide();
					$('#hashTagSearchInputBox').hide();
					$('#hashTagSearchInputBox1').hide();
				}
			   

			   
		    }else if(e.keyCode ==50 && e.key  =='@'){
				
							//alert(testAtIndex)
					//var a=$(obj).val();|| !$("#ProjIdForComment").next().attr('class').contains('atTag')
					if(type=='main'){
							getAtHashUI("main", "", "@");

					}else if(type=='reply'){
							var feedId = $(obj).attr('id').split("_")[1];
							getAtHashUI("reply", feedId, "@");

					}else if(type=='edit'){
							var feedId = $(obj).attr('id').split("_")[1];
							getAtHashUI("edit", feedId, "@");

					}
				
			 }else if(e.keyCode ==51 && e.key  =='#'){
					
					if(type=='main'){
							getAtHashUI("main", "", "#");

					}else if(type=='reply'){
							var feedId = $(obj).attr('id').split("_")[1];
							getAtHashUI("reply", feedId, "#");

					}else if(type=='edit'){
							var feedId = $(obj).attr('id').split("_")[1];
							getAtHashUI("edit", feedId, "#");

					}

			 }else{
				  var value = $(obj).val().toLowerCase();
				  value = value.substring(elementindex + 1, value.length);
				  //console.log(value)
				   if(typeof(value)!='undefined' && value!=''){
									value = value.substring(value.lastIndexOf("@")+1,value.length);
									$(".participant  *").filter(function() {
									//	console.log(value + "-----" + $(this).find(".names").text());
										$(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1);
  						    });
							//   var $mediaElements = $('.aTList');
							//    var filterVal = $(this).data('filter');

						}else{
							$(".participant  *").filter(function() {
										//console.log(value + "-----" + $(this).find(".names").text());
										$(this).parent().css('display','flex');
  						    });
						}
			 }
		 	if ($(obj).val() == '') {
				  elementindex='';
				  $("body").find(".atTag").remove();
				  $("body").find(".atTagHash").remove();
			 }
			
	 }
function externalAtHashClick(type, feedId, atOrHash){
	if(atOrHash =='@'){
			
		if(type=='main'){
			if(($('div.atHashContainerMain').find('.atListPop').length > 0) && ($('div.atHashContainerMain').find('.atListPop').is(':visible'))){
				$("#ProjIdForComment").next('div.atHashContainerMain').empty();
			}else{
				getAtHashUI(type, feedId, atOrHash);
			}
			
		}else if(type=='reply'){
			if(($('div.atHashContainerReply').find('.atListPop').length > 0) && ($('div.atHashContainerReply').find('.atListPop').is(':visible'))){
				$("#ProjIdForCommentreply").next('div.atHashContainerReply').empty();
			}else{
				getAtHashUI(type, feedId, atOrHash);
			}	

		}
		else if(type=='edit'){
			if(($('div.atHashContainerEdit').find('.atListPop').length > 0) && ($('div.atHashContainerEdit').find('.atListPop').is(':visible'))){
				$("#ProjIdForCommentEdit").next('div.atHashContainerEdit').empty();
			}else{
				getAtHashUI(type, feedId, atOrHash);
			}
			
		}

	}else if(atOrHash =='#'){
				
			if(type=='main'){
				if(($('div.atHashContainerMain').find('.hashListPop').length > 0) && ($('div.atHashContainerMain').find('.hashListPop').is(':visible'))){
					$("#ProjIdForComment").next('div.atHashContainerMain').empty();
				}else{
					getAtHashUI(type, feedId, atOrHash);
				}
			}else if(type=='reply'){
				if(($('div.atHashContainerReply').find('.hashListPop').length > 0) && ($('div.atHashContainerReply').find('.hashListPop').is(':visible'))){
					$("#ProjIdForCommentreply").next('div.atHashContainerReply').empty();
				}else{
					getAtHashUI(type, feedId, atOrHash);
				}	
			}
			else if(type=='edit'){
				if(($('div.atHashContainerEdit').find('.hashListPop').length > 0) && ($('div.atHashContainerEdit').find('.hashListPop').is(':visible'))){
					$("#ProjIdForCommentEdit").next('div.atHashContainerEdit').empty();
				}else{
					getAtHashUI(type, feedId, atOrHash);
				}
			}

	}
} 
 function getAtHashUI(type, feedId, atOrHash){
	if(atOrHash =='@'){
			
			if(type=='main'){
				elementindex= document.getElementById("main_commentTextarea").selectionStart;
				$("#ProjIdForComment").next('div.atHashContainerMain').empty();
				memberfind("main", "");
				
			}else if(type=='reply'){
					elementindex= document.getElementById("replyBlock_"+feedId).selectionStart;
					$("#ProjIdForCommentreply").next('div.atHashContainerReply').empty();
					memberfind("reply",feedId);
			}
			else if(type=='edit'){
					elementindex= document.getElementById("editBlock_"+feedId).selectionStart;
					$("#ProjIdForCommentEdit").next('div.atHashContainerEdit').empty();
					memberfind("edit",feedId);
			}

	}else if(atOrHash =='#'){
				
			if(type=='main'){
					elementindex= document.getElementById("main_commentTextarea").selectionStart;
					if (typeof $("#ProjIdForComment").next().attr('class')=='undefined' ) {
						wrkSpaceHashCodeFind("","main");
					}else{
						$("#ProjIdForComment").next('div.atHashContainerMain').empty();
						wrkSpaceHashCodeFind("","main");
					}
			}else if(type=='reply'){
					elementindex= document.getElementById("replyBlock_"+feedId).selectionStart;
					$("#ProjIdForCommentreply").next('div.atHashContainerReply').empty();
					wrkSpaceHashCodeFind(feedId,"reply");
			}
			else if(type=='edit'){
					elementindex= document.getElementById("editBlock_"+feedId).selectionStart;
					$("#ProjIdForCommentEdit").next('div.atHashContainerEdit').empty();
					wrkSpaceHashCodeFind(feedId,"edit");
			}

	}
 }

function memberfind(type,feedId){
	console.log(type+"--"+feedId);
  var jsonData="";
  $.ajax({ 
    url: apiPath+"/"+myk+"/v1/fetchParticipants?user_id="+userIdglb+"&company_id="+companyIdglb+"&project_id="+prjid,
    type:"GET",
    ///data:{act:'fetchAllUsersListForSpecChar',projectId:projectId,userId: userId,menuType:menuType,docFolId:docFolId,taskId:taskId},
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function(jqXHR, textStatus, errorThrown) {
        checkError(jqXHR,textStatus,errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        timerControl("");
    }, 
    success:function(result){
     
      jsonData = result;
      result = prepareUIforspecialcharacterNew(jsonData,prjid,type,feedId);
      resultset = jsonData;
      if(type == 'main' || type == 'mytaskComments' || type == 'mytaskCommentsNewUI' || type == 'Task'){
			$("body").find(".atTag").remove();
			$(result).appendTo("div.atHashContainerMain")
					.show('slow')
						.children('.touchablePop')
						.css('left', $('#atTagImg').offset().left-90+'px');
		 
      }else if(type == 'reply'){
			$("body").find(".atTag").remove();
			// $(result).appendTo("div.atHashContainerReply")
			// 		.show('slow')
			// 			.children('.touchablePop')
			// 			.css('left', $('#replyDivContainer_'+feedId+' .atImgExt').offset().left-90+'px');
			

	
			$("#replyDivContainer_"+feedId).find(".atHashContainerReply").append(result) 
											.show('slow')
											.find('.touchablePop')
											.css('left', $('#replyDivContainer_'+feedId+' .atImgExt').offset().left-90+'px');

      }else if(type=='edit'){
			$("body").find(".atTag").remove();
			$(result).appendTo("div.atHashContainerEdit")
					.show('slow')
						.children('.touchablePop')
						.css('left', $('#editDivContainer_'+feedId+' .atImgExt').offset().left-90+'px');
      }
	  	var elmnt = document.getElementById("tag");
		elmnt.scrollIntoView({
			  behavior: 'smooth', block: 'nearest', inline: 'start' 
		 });
        $('#loadingBar').addClass('d-none').removeClass('d-flex'); 
    }
  });
    // $('#newId').append(UI);
}

function prepareUIforspecialcharacterNew(jsonData,prjid,type,feedId){
  var sessionuserId = userIdglb;

  //console.log(jsonData)
  var UI="";
  UI = "<div class=\"atTag col-12 d-flex justify-content-center position-relative\" id='tag' style='z-index:1'>"
    +'<div class="border border-secondary py-3 pl-3 pr-2 touchablePop atListPop position-absolute rounded" style="background-color:white;margin-top: -2px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;">'
  +"<div class=\"header pb-1\" style='font-weight: normal;'>Select participants</div>"
  +"<div class=\"aTList\" style='margin-top:5px;height:190px;overflow-y:auto;overflow-x: hidden;width: 290px;'>"
  for(i=0;i<jsonData.length;i++){
    // var selecteduserId = jsonData[i].user_id
      let imgName = lighttpdpath+"/userimages/"+jsonData[i].user_image+"?"+d.getTime();
    if(jsonData[i].user_id != sessionuserId){
      var username=jsonData[i].user_full_name;
      UI+="<div class=\"participant  align-items-center\" style ='display:flex; padding:10px 5px;' onclick=\"selectUserNameNew('"+prjid+"','"+username+"','"+type+"','"+feedId+"',this)\">"
	  if(jsonData[i].user_id == userIdglb){
		UI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);"  src="'+imgName+'" title="'+username+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle" style="width:30px;height:30px;">'
	  }else{
		UI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);"  src="'+imgName+'" title="'+username+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle cursor" style="width:30px;height:30px;">'
	  }
	  UI+='<div class="names defaultExceedCls" style="font-size: 14px;font-weight: normal;" title="'+ jsonData[i].user_full_name+'">'
        + jsonData[i].user_full_name
        +'</div>'
              + "</div > ";
    }
  }
   UI+="</div>" 
  UI+="</div>"
  +"</div>"
  return UI;
}

function selectUserNameNew(projid,username,type,feedId,obj){
	if(type == "main"){
       //$('.activityfieldMainuserslist').hide();
	  
    //    var data=$('#main_commentTextarea').val();
    //    var focusIndex = document.getElementById('main_commentTextarea').selectionStart;
	//    alert(focusIndex);
    //    var bTxt= data.substring(0,elementindex);
    //    var aTxt = data.substring(focusIndex,data.length);
	//     bTxt = bTxt.charAt(bTxt.length-1) != '@' ? bTxt+' @' : bTxt;
    //    data = bTxt+username+" "+aTxt;

		var data=$('#main_commentTextarea').val();
		var bTxt= data.substring(0,elementindex-1);
		data = bTxt+"@"+username+"  ";
		
       $('#main_commentTextarea').val(data).focus();
	   elementindex= document.getElementById("main_commentTextarea").selectionStart;
     }else if(type=='reply'){
		  //$('.activityfieldMainuserslist').hide();
		var data=$("#replyBlock_"+feedId).val();
		var bTxt= data.substring(0,elementindex-1);
		data = bTxt+"@"+username+"  ";
      	$('#replyBlock_'+feedId).val(data).focus();
	 	elementindex= document.getElementById("replyBlock_"+feedId).selectionStart;
		  
     
	 }else if(type=='edit'){
		var data=$("#editBlock_"+feedId).val();
		var bTxt= data.substring(0,elementindex-1);
		data = bTxt+"@"+username+"  ";
      	$('#editBlock_'+feedId).val(data).focus();
	 	elementindex= document.getElementById("editBlock_"+feedId).selectionStart;
	 }
	$(obj).css('background-color', "#5DA3FA");
	$(".participant  *").filter(function() {
			//console.log(value + "-----" + $(this).find(".names").text());
			$(this).parent().css('display','flex');
  	});
}



function wrkSpaceHashCodeFind(feedId,type){


  if (typeof prjid == "undefined") {
    prjid = "";
   }
  $.ajax({
          url: apiPath+"/"+myk+"/v1/getProjectTagName?user_id="+userIdglb+"&company_id="+companyIdglb+"&project_id="+prjid+"",
          type:"GET",
          //data:{act:"featchProjectTagName", projId: projectId,menuType:menuType,type:type,feedId:feedId},
          error: function(jqXHR, textStatus, errorThrown) {
                      checkError(jqXHR,textStatus,errorThrown);
                      }, 
          success:function(result){
            
            var jsonData = result;
            result = preparingUIforHashNew(jsonData,prjid,type,feedId);
            if(type == 'main' || type == 'mytaskComments' || type == 'mytaskCommentsNewUI' || type == 'Task'){
				$("body").find(".atTagHash").remove();
				$(result).appendTo("div.atHashContainerMain")
						  .show('slow')
						  .children('.touchablePop')
						  .css('left', $('#hashTagImg').offset().left-90+'px');

            }else if(type == 'reply'){
				$("body").find(".atTagHash").remove();
				// $(result).appendTo("div.atHashContainerReply")
				// 		  .show('slow')
				// 		  .children('.touchablePop')
				// 		  .css('left', $('#replyDivContainer_'+feedId+' .hashImgExt').offset().left-90+'px');
				//alert(feedId);
				$("#replyDivContainer_"+feedId).find(".atHashContainerReply").append(result) 
						  .show('slow')
						  .find('.touchablePop')
						  .css('left', $('#replyDivContainer_'+feedId+' .hashImgExt').offset().left-90+'px');
						
				
            }else if(type=='edit'){
				$("body").find(".atTagHash").remove();
				$(result).appendTo("div.atHashContainerEdit")
						  .show('slow')
						  .children('.touchablePop')
						  .css('left', $('#editDivContainer_'+feedId+' .hashImgExt').offset().left-90+'px');
            }
            hashResult = jsonData;
            var elmnt = document.getElementById("tag");
			elmnt.scrollIntoView({
				behavior: 'smooth', block: 'nearest', inline: 'start' 
			});
          }
  });

}

function preparingUIforHashNew(jsonData,prjid,type,feedId){



  // $('.proTagNameId').append(UI);

   let UI="";
  UI = "<div class=\"atTagHash col-12 d-flex justify-content-center position-relative\" id='tag' style='z-index:1'>"
    +'<div class="border border-secondary py-3 pl-3 pr-2 touchablePop hashListPop position-absolute rounded" style="background-color:white;margin-top: -2px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;">'
  	+"<div class=\"header pb-1\" style='font-weight: normal;'>Tags</div>"
  	+"<div class=\"aTList aTListHash\" style='margin-top:5px;height:190px;overflow-y:auto;overflow-x: hidden; width: 290px;'>"
  for(i=0;i<jsonData.length;i++){
    // var selecteduserId = jsonData[i].user_id
      let imgName = lighttpdpath+"/projectimages/"+jsonData[i].project_id+"."+jsonData[i].project_image_type+"?"+d.getTime();
    if(jsonData[i].project_tag_name != ''){
      UI+="<div class=\" participant align-items-center\" style ='display:flex;padding:10px 5px;' onclick=\"addProjHashTagNew('"+jsonData[i].project_tag_name+"','"+type+"','"+feedId+"',this)\">"
	  	if(jsonData[i].user_id == userIdglb){
			UI+='<img id="personimagedisplay"  onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);" src="'+imgName+'" title="'+username+'" onerror="userImageOnErrorReplace(this);" class="mr-3 mt-0 rounded-circle " style="width:30px; height:30px;">'
		}else{
			UI+='<img id="personimagedisplay"  onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);" src="'+imgName+'" title="'+username+'" onerror="userImageOnErrorReplace(this);" class="mr-3 mt-0 rounded-circle cursor" style="width:30px; height:30px;">'
	  	}
	  	UI+= '<div class="names defaultExceedCls" style="font-size: 13px;font-weight: normal;" title="'+jsonData[i].project_tag_name+'">' + jsonData[i].project_tag_name+'</div>'
     
              + "</div > ";
    }
  }
   UI+="</div>" 
  UI+="</div>"
  +"</div>"
  return UI;

}

function  addProjHashTagNew(projHashTagName,type,feedId,obj){
			if(type == "main"){
       //$('.activityfieldMainuserslist').hide();
	  
    //    var data=$('#main_commentTextarea').val();
    //    var focusIndex = document.getElementById('main_commentTextarea').selectionStart;
	//    alert(focusIndex);
    //    var bTxt= data.substring(0,elementindex);
    //    var aTxt = data.substring(focusIndex,data.length);
	//     bTxt = bTxt.charAt(bTxt.length-1) != '@' ? bTxt+' @' : bTxt;
    //    data = bTxt+username+" "+aTxt;

		var data=$('#main_commentTextarea').val();
		var bTxt= data.substring(0,elementindex-1);
		data = bTxt+"#"+projHashTagName+"  ";
		
       $('#main_commentTextarea').val(data).focus();
	   elementindex= document.getElementById("main_commentTextarea").selectionStart;
     }else if(type=='reply'){
		  //$('.activityfieldMainuserslist').hide();
		var data=$("#replyBlock_"+feedId).val();
		var bTxt= data.substring(0,elementindex-1);
		data = bTxt+"#"+projHashTagName+"  ";
      	$('#replyBlock_'+feedId).val(data).focus();
	 	elementindex= document.getElementById("replyBlock_"+feedId).selectionStart;
		  
     
	 }else if(type=='edit'){
		var data=$("#editBlock_"+feedId).val();
		var bTxt= data.substring(0,elementindex-1);
		data = bTxt+"#"+projHashTagName+"  ";
      	$('#editBlock_'+feedId).val(data).focus();
	 	elementindex= document.getElementById("editBlock_"+feedId).selectionStart;
	 }
	$(obj).css('background-color', "#2B2C4B");
	$(".participant  *").filter(function() {
			//console.log(value + "-----" + $(this).find(".names").text());
			$(this).parent().css('display','flex');
  	});

	$(obj).css('background-color', "#5DA3FA");
}






 function enterKeyValidCommentActFeedNew(event,menuTypeId,menuType,type,replyType){
	var testAtData = $('#main_commentTextarea').val();
	var testAtChar = document.getElementById('main_commentTextarea').selectionStart;
	var testAtIndex = parseInt(testAtChar)-1;

	if (type=='keypress' && (event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey){		
		elementindex='',elementhashindex='';atIndex="";hashIndex = "";
	   CommentActFeed(menuTypeId,menuType);
	   $('#tabContentDiv').find('.atTag').attr('class','atTag col-12 d-none justify-content-center');
	   $('.activityfieldUserListMain').hide();
	   $('.projUserListMain').hide();
   }else{

		if (testAtData.charAt(testAtIndex)=="#") {
			elementhashindex = document.getElementById('main_commentTextarea').selectionStart;
			hashIndex = parseInt(elementhashindex)-1 ;
			// $('.proTagNameId').show();
			if($('#tabContentDiv').find('.atTag').length){
				$('.atTag').attr('class','atTag col-12 d-none justify-content-center');
			}else{
				wrkSpaceHashCodeFind("",replyType);
			}
			testAtData=testAtData.toLowerCase();
		}
		else if(elementhashindex!=""){
					// $('#hashTagSearchInputBox').hide();
					// $('#hashTagSearchInputBox1').hide();
					var data=$('#main_commentTextarea').val();
					//console.log('hashIndex:'+data.charAt(hashIndex)+":");
					if(data.charAt(hashIndex)=="#"){
							var focusIndex = document.getElementById('main_commentTextarea').selectionStart;
							var aTxt = data.substring(elementhashindex,focusIndex).toLowerCase().trim();
							//console.log('hashTxt:'+aTxt);
							// $('.projuserList li').show();
							$('.proTagNameId').show();
							$(".projuserList li").filter(function() {
								$(this).toggle($(this).text().toLowerCase().indexOf(aTxt) > -1)
							}); 
				}else{
					$('#hashTagSearchInputBox').hide();
					$('#hashTagSearchInputBox1').hide();
					$('.proTagNameId').css('display','none');
					elementhashindex = "";
					hashIndex ="";
				}
		}

		if (testAtData.charAt(testAtIndex)=="@" && !regex.test(testAtData.charAt(testAtIndex-1)) ) {
			elementindex = document.getElementById('main_commentTextarea').selectionStart;
			atIndex = parseInt(elementindex)-1 ;
			if($('#tabContentDiv').find('.atTag').length){
				$('.atTag').attr('class','atTag col-12 d-none justify-content-center')
			}else{
				memberfind(replyType,"");
			}
		}
		else if(elementindex!=""){
			var data=$('#main_commentTextarea').val();
			if(data.charAt(atIndex)=="@" && !regex.test(data.charAt(atIndex-1))){
				var focusIndex1 = document.getElementById('main_commentTextarea').selectionStart;
				var aTxt1 = data.substring(elementindex,focusIndex1).toLowerCase().trim();
				//console.log('aTxt:'+aTxt);
				// $('.activityfielduserList li').show();
				// $('.activityfieldMainuserslist').show();
				// memberfind("main","");
				$(".activityfielduserList li").filter(function() {
					$(this).toggle($(this).text().toLowerCase().indexOf(aTxt1) > -1)
				});
			}
		}
		
   }

 }
 	   
 function enterKeyValidCommentActFeed(event,menuTypeId,menuType,type){
	 imgfeedId="";
	 //alert("imgfeedId::::::enterKeyValidCommentActFeed:::::"+imgfeedId);
	if(menuTypeId == 'conversationplace'){
		menuTypeId = prjid; 
	} 
    if (type=='keypress' && (event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey){		
 		elementindex='',elementhashindex='';atIndex="";hashIndex = "";
		CommentActFeed(menuTypeId,menuType);
		$('.activityfieldUserListMain').hide();
		$('.projUserListMain').hide();
	}else{
		   
			    var testAtData = $('#main_commentTextarea').val();
				var testAtChar = document.getElementById('main_commentTextarea').selectionStart;
				var testAtIndex = parseInt(testAtChar)-1;
			
			
			//if(menuType == 'activityFeed'){    // this condition is commented bcoz in other modules we have to show trending codes
			
				//----------------------------code for # tag--------------------------------->
				if (testAtData.charAt(testAtIndex)=="#") {
					 $('#hashTagSearchInputBox').hide();
					 $('#hashTagSearchInputBox1').hide();
					 $('.activityfieldMainuserslist').hide();
			         $('.activityfieldUserListMain').hide();
			         elementhashindex = document.getElementById('main_commentTextarea').selectionStart;
					 hashIndex = parseInt(elementhashindex)-1 ;
					 $('.proTagNameId').css('display','block');
					 $('.proTagReplyNameId').hide();
					 $('.proTagSubReplyNameId').hide();
					// $('.proTagNameId').css('margin-top','45px');
					 $('#commentListDiv').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
					if(hashResult == '[]' || hashResult == ''){	
						ajaxCallforHashCode(menuType,'','main');
					}else{
						// $('.projUserListMain:visible ul').html(preparingUIforHash(hashResult,projectId,'main'));
						$('.proTagNameId').show();
					}
						
				}else if( event.keyCode == 32){
					elementhashindex='';
					hashIndex =""; 
					$('#hashTagSearchInputBox').hide();
					 $('#hashTagSearchInputBox1').hide();
					$('.proTagNameId:visible').hide();
				}else if(elementhashindex!=""){
							$('#hashTagSearchInputBox').hide();
							 $('#hashTagSearchInputBox1').hide();
							var data=$('#main_commentTextarea').val();
			                //console.log('hashIndex:'+data.charAt(hashIndex)+":");
			     			if(data.charAt(hashIndex)=="#"){
						     		var focusIndex = document.getElementById('main_commentTextarea').selectionStart;
						    		var aTxt = data.substring(elementhashindex,focusIndex).toLowerCase().trim();
						    		//console.log('hashTxt:'+aTxt);
						    		$('.projuserList li').show();
						    		$('.proTagNameId').show();
						    		if(aTxt !=""){
						    		    $('.projuserList li').each(function(){
						    		    	val = $(this).text().toLowerCase();
						    		    	if(val.indexOf(aTxt)!= -1 ){
						    		    		$(this).show();
										    }else{
										   		$(this).hide();
										    }
						    		    });
						    		    
						    		    
						    		    if($('.projuserList li:visible').length < 1)
						    		       $('.proTagNameId').hide();
						    		   	else
						    		   	   $('.proTagNameId').show();
					    		 }	 
			    		  }else{
			    		  	 $('#hashTagSearchInputBox').hide();
			    		  	 $('#hashTagSearchInputBox1').hide();
			    		     $('.proTagNameId').css('display','none');
			    		     elementhashindex = "";
			    			 hashIndex ="";
			    		  } 
				}else{
			    	$('.proTagNameId:visible').hide();
			    	$('#hashTagSearchInputBox').hide();
			    	 $('#hashTagSearchInputBox1').hide();
				}	
		
		   //}
		 
				//-----------------code for @ tag ------------------------------------>
				
				if (testAtData.charAt(testAtIndex)=="@" && !regex.test(testAtData.charAt(testAtIndex-1)) ) {
							elementhashindex='';
							$('.proTagNameId').hide();
							$('#hashTagSearchInputBox').hide();
							 $('#hashTagSearchInputBox1').hide();
							$('.proTagSubReplyNameId').hide();
						   	elementindex = document.getElementById('main_commentTextarea').selectionStart;
						   	//console.log('elementindex:'+elementindex);
							var data=$('#main_commentTextarea').val();
			                
							$('.activityfieldReplyUserList').hide();
						 	$('.activityfieldSubReplyUserList').hide();
						//	$('.activityfieldMainuserslist').css('margin-top','45px');
							$('#commentListDiv').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
							
				    	    atIndex = parseInt(elementindex)-1 ;
				    	    //console.log("atIndex==>>> "+atIndex);
							if(atIndex==0 || !regex.test(data.charAt(atIndex-1))){
				     		//    $('.activityfieldMainuserslist').css('display','block');
				     		   if(resultset == '' || resultset == '[]'){
				     		      //alert("if user");
				    	    	  commonFunctionForSpecialChar('main','',menuType);
				    	   	   }else{
				    	   	     // alert("else user");
				    	   	    //   $('.activityfieldMainuserslist:visible ul').html(prepareUIforspecialcharacter(resultset,projectId,'main'));
								   $('.activityfieldMainuserslist').show();
				    	   	   }
						   }
				 }else if(event.keyCode == 32){ 
				 			$('#hashTagSearchInputBox').hide();
				 			 $('#hashTagSearchInputBox1').hide();
			    			$('.activityfieldMainuserslist').hide();
			    			elementindex = "";
			    			atIndex ="";
			     }else if(elementindex!=""){
			     			$('#hashTagSearchInputBox').hide();
			     			 $('#hashTagSearchInputBox1').hide();
			     			var data=$('#main_commentTextarea').val();
			               //alert("data====>>> "+data);
			               // alert('atIndex:'+data.charAt(atIndex)+":");
			     			if(data.charAt(atIndex)=="@" && !regex.test(data.charAt(atIndex-1))){
						     		var focusIndex = document.getElementById('main_commentTextarea').selectionStart;
						    		var aTxt = data.substring(elementindex,focusIndex).toLowerCase().trim();
						    		//console.log('aTxt:'+aTxt);
						    		$('.activityfielduserList li').show();
						    		$('.activityfieldMainuserslist').show();
						    		if(aTxt !=""){
						    		    $('.activityfielduserList li').each(function(){
						    		    	val = $(this).text().toLowerCase();
						    		    	if(val.indexOf(aTxt)!= -1 ){
						    		    		$(this).show();
										    }else{
										   		$(this).hide();
										    }
						    		    });
						    		    
						    		    //console.log('user length:'+$('.activityfielduserList li:visible').length);
						    		    
						    		    if($('.activityfielduserList li:visible').length < 1)
						    		       $('.activityfieldMainuserslist').hide();
						    		   	else
						    		   	   $('.activityfieldMainuserslist').show();
					    		   }	 
			    		  }else{
			    		  	$('#hashTagSearchInputBox').hide();
			    		  	 $('#hashTagSearchInputBox1').hide();
			    		  	$('.activityfieldMainuserslist').css('display','none');
			    		     elementindex = "";
			    			 atIndex ="";
			    		  }   
			    }else {
			    	$('.activityfieldMainuserslist:visible').hide();
			    	$('#hashTagSearchInputBox').hide();
			    	 $('#hashTagSearchInputBox1').hide();
				}
		}
    
    //imgfeedId="";
    
 }

 function selectUserName(projId,username,type,feedId){
	if(type == "main"){
       $('.activityfieldMainuserslist').hide();
       var data=$('#main_commentTextarea').val();
       var focusIndex = document.getElementById('main_commentTextarea').selectionStart;
       var bTxt= data.substring(0,elementindex);
       var aTxt = data.substring(focusIndex,data.length);
       data = bTxt+username+" "+aTxt;
       $('#main_commentTextarea').val(data).focus();
     }
     else if(type == "editField"){
       $('.activityfieldMainuserslist').hide();
       var data=$('#EdidCmtBlock_'+feedId).val();
       var focusIndex = document.getElementById('EdidCmtBlock_'+feedId).selectionStart;
       var bTxt= data.substring(0,elementindex);
       var aTxt = data.substring(focusIndex,data.length);
       data = bTxt+username+" "+aTxt;
       $('#EdidCmtBlock_'+feedId).val(data).focus();
      }
    else if(type == "mytaskComments"){
       $('.activityfieldMainuserslist').hide();
       var data=$('#myTaskComment').val();
       var focusIndex = document.getElementById('myTaskComment').selectionStart;
       var bTxt= data.substring(0,elementindex);
       var aTxt = data.substring(focusIndex,data.length);
       data = bTxt+username+" "+aTxt;
       $('#myTaskComment').val(data).focus();
    }else if(type == "mytaskCommentsNewUI"){
        $('.activityfieldMainuserslist').hide();
        var data=$('#UserCommentNewUI').val();
        var focusIndex = document.getElementById('UserCommentNewUI').selectionStart;
        var bTxt= data.substring(0,elementindex);
        var aTxt = data.substring(focusIndex,data.length);
        data = bTxt+username+" "+aTxt;
        $('#UserCommentNewUI').val(data).focus();
     }else{
       $('.activityfieldMainuserslist').hide();
       var data=$("#replyBlock_"+feedId).val();
       var focusIndex = document.getElementById("replyBlock_"+feedId).selectionStart;
       var bTxt= data.substring(0,elementindex);
       var aTxt = data.substring(focusIndex,data.length);
       data = bTxt+username+" "+aTxt;
      $("#replyBlock_"+feedId).val(data).focus();
	}
	elementindex = "";
	atIndex ="";
  }	
 var proId='';
 function commonFunctionForSpecialChar(type,feedId,menuType){
            resultset='';
 			var limitAct=100;
			var index = 0;
			var txt = '';
			var SelectedVal ='';
			var level="";
			var docFolId = $('#folderId').val();
			var taskId = $('#taskIdValue').text();
			if(menuType == "mytaskComments"){
			     type=menuType;
			     projectId = $('#projIdInView').val();
			}
			 if( menuType == "mytaskCommentsNewUI" ||  menuType == "Task"){
			     type=menuType;
			     projectId = $('#projIdInView').val();
			     taskId =  $('#taskIdForTempHiddenFeild').val();
			}
			 if(menuType == "Task" ){
				 type='main';
			     taskId = pgId; //---  This variable is declared in showTaskComments function in taskUICodeNew.js file
			     projectId = taskprojId; //--- This variable is declared in showTaskComments function in taskUICodeNew.js file
			 }
			 //alert("taskId--"+taskId);
			$('#loadingBar').addClass('d-flex');
		    timerControl("start");
		    $.ajax({ 
	         	  url: apiPath+"/"+myk+"/v1/fetchParticipants?user_id="+userIdglb+"&company_id="+companyIdglb+"&project_id="+prjid,
            	  type:"GET",
		       	  ///data:{act:'fetchAllUsersListForSpecChar',projectId:projectId,userId: userId,menuType:menuType,docFolId:docFolId,taskId:taskId},
			      beforeSend: function (jqXHR, settings) {
				      xhrPool.push(jqXHR);
				  },
		       	  error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $('#loadingBar').addClass('d-none').removeClass('d-flex');
						timerControl("");
						}, 
		          	success:function(result){
		          	
					jsonData = result;
		    		result = prepareUIforspecialcharacter(jsonData,projectId,type,feedId);
		    		resultset = jsonData;
		    		//console.log('type:'+type);
		    		if(type == 'main' || type == 'mytaskComments' || type == 'mytaskCommentsNewUI' || type == 'Task'){
		    			if(result == ''){
					  	 	$('.activityfieldMainuserslist ul').html('No data found.').show();
						}else{
				   			$('.activityfieldMainuserslist ul').html(result).show();
					 	} 
		    		}else if(type == 'reply'){
		    			if(result == ''){
					  	 	$('.activityfieldReplyUserList ul').html('No data found.');
						}else{
				   			$('.activityfieldReplyUserList ul').html(result);
					 	} 
		    		}else if(type == 'editField'){
		    		    if(result == ''){
					  	 	$('#EditCmtDivContainer_'+feedId).children('.activityfieldReplyUserList').find('ul.activityfielduserList').html('No data found.');
						}else{
				   			$('#EditCmtDivContainer_'+feedId).children('.activityfieldReplyUserList').find('ul.activityfielduserList').html(result);
					 	} 
		    		}else{
		    			if(result == ''){
					  	 	$('.activityfieldSubReplyUserList ul').html('No data found.');
						}else{
							$('.activityfieldSubReplyUserList ul').html(result);
					 	} 
		    		}
		    		$('#loadingBar').addClass('d-none').removeClass('d-flex');
		  			//timerControl(""); 
		    	}
		    }); 
}
function prepareUIforspecialcharacter(jsonData,projectId,level,feedId){
  		
 
}
 var imgfeedId="";
 function enterKeyValidreplyActCommentsNew(event,feedId,userId,projId,menuType,menuTypeId,type,replyType){
	if (type=='keypress' && (event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey){		
		elementindex='',elementhashindex='';atIndex="";hashIndex = "";
	   //replyActComments(feedId,userId,projId,menuType,menuTypeId);
	   postReply(feedId);
	   $('replyDivContainer_'+feedId).find('.atTag').attr('class','atTag col-12 d-none justify-content-center');
	   $('.activityfieldUserListMain').hide();
	   $('#hashTagSearchInputBox').hide();
	   $('#hashTagSearchInputBox1').hide();
   }else{
	// elementindex='',elementhashindex='';atIndex="";hashIndex = "";
	var testAtData = $("#replyBlock_"+feedId).val();
 	var testAtChar = document.getElementById("replyBlock_"+feedId).selectionStart;
 	var testAtIndex = parseInt(testAtChar)-1;
	if (testAtData.charAt(testAtIndex)=="#") {
		elementhashindex = document.getElementById("replyBlock_"+feedId).selectionStart;
		hashIndex = parseInt(elementhashindex)-1 ;
		if($('replyDivContainer_'+feedId).find('.atTag').length){
			$$('replyDivContainer_'+feedId).find('.atTag').attr('class','atTag col-12 d-none justify-content-center');
		}else{
			wrkSpaceHashCodeFind(feedId,replyType);
		}
	}else{

	}
	if (testAtData.charAt(testAtIndex)=="@" && !regex.test(testAtData.charAt(testAtIndex-1)) ) {
		elementindex= document.getElementById("replyBlock_"+feedId).selectionStart;
		var data=$("#replyBlock_"+feedId).val();
		atIndex = parseInt(elementindex)-1 ;
		//console.log('element@index:'+atIndex);
		if(atIndex==0 || !regex.test(data.charAt(atIndex-1))){
			memberfind(replyType,feedId);
		}
	}else if(elementindex!=""){

	}else{
		$('.proTagReplyNameId:visible').hide();
		$('#hashTagSearchInputBox').hide();
		$('#hashTagSearchInputBox1').hide();
	}
	}
 }
 function enterKeyValidreplyActComments(event,feedId,userId,projId,menuType,menuTypeId,type,replyType){
	 if (type=='keypress' && (event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey){		
 	    elementindex='',elementhashindex='';atIndex="";hashIndex = "";
		//replyActComments(feedId,userId,projId,menuType,menuTypeId);
		postReply(feedId);
		$('.activityfieldUserListMain').hide();
		$('#hashTagSearchInputBox').hide();
		$('#hashTagSearchInputBox1').hide();
	}else{
		   	var testAtData = $("#replyBlock_"+feedId).val();
 			var testAtChar = document.getElementById("replyBlock_"+feedId).selectionStart;
 			var testAtIndex = parseInt(testAtChar)-1;
		  
		  //if(menuType == 'activityFeed'){  // this condition is commented bcoz in other modules we have to show trending codes
			
			    //----------------------------code for # tag--------------------------------->
			    if (testAtData.charAt(testAtIndex)=="#") {
			    	$('#hashTagSearchInputBox').hide();
			    	 $('#hashTagSearchInputBox1').hide();
			    	$('.proTagReplyNameId').css('display','block');
				  	$('.proTagReplyNameId').css('margin-top','45px');
				  	$('#commentListDiv').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
				  	$('.proTagNameId').hide();
				  	$('.activityfieldUserListMain').hide();
				  	elementhashindex = document.getElementById("replyBlock_"+feedId).selectionStart;
				  	hashIndex = parseInt(elementhashindex)-1 ;
					//console.log('element@hashIndex:'+hashIndex);
					
					$('.proTagSubReplyNameId').hide();
					
				  	if(hashResult == '[]' || hashResult == ''){
				  		ajaxCallforHashCode(menuType,feedId,'reply');
				  	}else{
				  		$('.proTagReplyNameId:visible ul').html(preparingUIforHash(hashResult,projectId,'reply',feedId));
				  	}
				  	
				}else if(event.keyCode == 32){ 
			    			$('.proTagReplyNameId').hide();
			    			$('#hashTagSearchInputBox').hide();
			    			 $('#hashTagSearchInputBox1').hide();
			    			elementhashindex='';
							hashIndex ="";
				}else if(elementhashindex!=""){
							$('#hashTagSearchInputBox').hide();
							 $('#hashTagSearchInputBox1').hide();
			    		    var data=$("#replyBlock_"+feedId).val();
			                //console.log('hashIndex:'+data.charAt(hashIndex)+":");
			     			if(data.charAt(hashIndex)=="#"){
						     		var focusIndex = document.getElementById("replyBlock_"+feedId).selectionStart;
						    		var aTxt = data.substring(elementhashindex,focusIndex).toLowerCase().trim();
						    		//console.log('hashTxt:'+aTxt);
						    		$('.projuserList li').show();
						    		$('.proTagReplyNameId').show();
						    		if(aTxt !=""){
						    		    $('.projuserList li').each(function(){
						    		    	val = $(this).text().toLowerCase();
						    		    	if(val.indexOf(aTxt)!= -1 ){
						    		    		$(this).show();
										    }else{
										   		$(this).hide();
										    }
						    		    });
						    		    
						    		    
						    		    if($('.projuserList li:visible').length < 1)
						    		       $('.proTagReplyNameId').hide();
						    		   	else
						    		   	   $('.proTagReplyNameId').show();
					    		 }	 
			    		  }else{
			    		  	 $('#hashTagSearchInputBox').hide();
			    		  	 $('#hashTagSearchInputBox1').hide();
			    		     $('.proTagReplyNameId').css('display','none');
			    		     elementhashindex = "";
			    			 hashIndex ="";
			    		  } 
			    		    
			    }else{
					$('.proTagReplyNameId:visible').hide();
					$('#hashTagSearchInputBox').hide();
					$('#hashTagSearchInputBox1').hide();
				}
			//}
		
				//-----------------code for @ tag ------------------------------------>
				
				if (testAtData.charAt(testAtIndex)=="@" && !regex.test(testAtData.charAt(testAtIndex-1)) ) {
							$('.activityfieldMainuserslist').hide();
							$('.activityfieldSubReplyUserList').hide();
							$('.proTagNameId:visible').hide();
							$('.activityfieldMainuserslist').hide();
							$('#hashTagSearchInputBox').hide();
							$('#hashTagSearchInputBox1').hide();
							$('.activityfieldReplyUserList').css('margin-top','45px');
							$('#commentListDiv').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
							elementindex= document.getElementById("replyBlock_"+feedId).selectionStart;
							//console.log('elementindex:'+elementindex);
							var data=$("#replyBlock_"+feedId).val();
			                atIndex = parseInt(elementindex)-1 ;
							//console.log('element@index:'+atIndex);
							
							if(atIndex==0 || !regex.test(data.charAt(atIndex-1))){
				     		   $('.activityfieldReplyUserList').css('display','block');
				     		   if(resultset == '' || resultset == '[]'){
				     		      //console.log("if user");
				    	    	  commonFunctionForSpecialChar('reply',feedId,menuType);
				    	   	   }else{
				    	   	       //console.log("else user");
				    	   		//   $('.activityfieldReplyUserList:visible ul').html(prepareUIforspecialcharacter(resultset,projectId,'reply',feedId));
								   memberfind(replyType,feedId);
				    	   		  
				    	   	   }
							}
				    	   	
				    	   	
				  }else if(event.keyCode == 32){ 
			    			$('.activityfieldReplyUserList').hide();
			    			$('#hashTagSearchInputBox').hide();
			    			$('#hashTagSearchInputBox1').hide();
			    			elementindex = "";
			    			atIndex ="";
			      }else if(elementindex!=""){
			      			$('#hashTagSearchInputBox').hide();
			      			$('#hashTagSearchInputBox1').hide();
			      			var data = $("#replyBlock_"+feedId).val();
			                //console.log('atIndex:'+data.charAt(atIndex)+":");
			     			if(data.charAt(atIndex)=="@" && !regex.test(data.charAt(atIndex-1))){
						     		var focusIndex = document.getElementById("replyBlock_"+feedId).selectionStart;
						    		var aTxt = data.substring(elementindex,focusIndex).toLowerCase().trim();
						    		//console.log('aTxt:'+aTxt);
						    		$('.activityfielduserList li').show();
						    		$('.activityfieldReplyUserList').show();
						    		if(aTxt !=""){
						    		    $('.activityfielduserList li').each(function(){
						    		    	val = $(this).text().toLowerCase();
						    		    	if(val.indexOf(aTxt)!= -1 ){
						    		    		$(this).show();
										    }else{
										   		$(this).hide();
										    }
						    		    });
						    		    
						    		    //console.log('user length:'+$('.activityfielduserList li:visible').length);
						    		    
						    		    if($('.activityfielduserList li:visible').length < 1)
						    		       $('.activityfieldReplyUserList').hide();
						    		   	else
						    		   	   $('.activityfieldReplyUserList').show();
					    		   }	 
			    		  }else{
			    		     $('.activityfieldReplyUserList').css('display','none');
			    		     $('#hashTagSearchInputBox').hide();
			    		     $('#hashTagSearchInputBox1').hide();
			    		     elementindex = "";
			    			 atIndex ="";
			    		  } 
			      
			    }else {
					$('.activityfieldReplyUserList:visible').hide();
					$('#hashTagSearchInputBox').hide();
					 $('#hashTagSearchInputBox1').hide();
				}
			
	 }
	imgfeedId= feedId;
 }
 
 function specialCharAtNew(event,feedId,menuType,type,replyType){
	 if(type=='keypress' && (event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey){
		$('editDivContainer_'+feedId).find('.atTag').attr('class','atTag col-12 d-none justify-content-center');
	 }else{
	var testAtData = $("#editBlock_"+feedId).val();
	var testAtChar = document.getElementById("editBlock_"+feedId).selectionStart;
	var testAtIndex = parseInt(testAtChar)-1;
	if (testAtData.charAt(testAtIndex)=="@" && !regex.test(testAtData.charAt(testAtIndex-1)) ) {
		elementindex=  document.getElementById("editBlock_"+feedId).selectionStart;
		var data=$("#editBlock_"+feedId).val();
	    atIndex = parseInt(elementindex)-1 ;
		//console.log('element@index:'+atIndex);
					
		if(atIndex==0 || !regex.test(data.charAt(atIndex-1))){
			if($('editDivContainer_'+feedId).find('.atTag').length>1){
				$('.atTag').attr('class','atTag col-12 d-none justify-content-center')
			}else{
				memberfind(replyType,feedId);
			}
		}
		}else if(elementindex!=""){
			var data=$("#editBlock_"+feedId).val();
	    	//console.log('atIndex:'+data.charAt(atIndex)+":");
	    	if(data.charAt(atIndex)=="@" && !regex.test(data.charAt(atIndex-1))){
			var focusIndex = document.getElementById("editBlock_"+feedId).selectionStart;
			var aTxt = data.substring(elementindex,focusIndex).toLowerCase().trim();
			$(".aTList").children(".names").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(aTxt) > -1)
			}); 
		}
		}

	if (testAtData.charAt(testAtIndex)=="#"){
		elementhashindex = document.getElementById("editBlock_"+feedId).selectionStart;
		hashIndex = parseInt(elementhashindex)-1 ;
		if(hashResult == '' || hashResult == '[]'){
			ajaxCallforHashCode(menuType,feedId,'editField');
		}else{
			if($('editDivContainer_'+feedId).find('.atTag').length){
				$('.atTag').attr('class','atTag col-12 d-none justify-content-center')
			}else{
				wrkSpaceHashCodeFind(feedId,replyType);
			}
			// $('.proTagReplyNameId:visible ul').html(preparingUIforHash(hashResult,projectId,'editField',feedId));
		}
	}else if(elementindex!=""){
		var data=$("#EdidCmtBlock_"+feedId).val();
		      
			if(data.charAt(hashIndex)=="#"){
				var focusIndex = document.getElementById("EdidCmtBlock_"+feedId).selectionStart;
				var aTxt = data.substring(elementhashindex,focusIndex).toLowerCase().trim();
				if($('editDivContainer_'+feedId).find('.atTag').length){
					$('.atTag').attr('class','atTag col-12 d-none justify-content-center')
				}else{
					wrkSpaceHashCodeFind(feedId,replyType);
					$(".aTList").children(".names").filter(function() {
						$(this).toggle($(this).text().toLowerCase().indexOf(aTxt) > -1)
					});
				} 
			}

	}
}


 }
 
 function specialCharAt(event,feedId,menuType,type,replyType){
			 var testAtData = $("#editBlock_"+feedId).val();
			 var testAtChar = document.getElementById("editBlock_"+feedId).selectionStart;
			 var testAtIndex = parseInt(testAtChar)-1;
			 
//----------------------------code for @ tag--------------------------------->
			 
			if (testAtData.charAt(testAtIndex)=="@" && !regex.test(testAtData.charAt(testAtIndex-1)) ) {
		    	   	$('.activityfieldMainuserslist').hide();
					$('.proTagNameId:visible').hide();
					$('.proTagSubReplyNameId').hide();
					$('.activityfieldReplyUserList').hide();
					$('.activityfieldReplyUserList').css('margin-top','45px');
					$('#hashTagSearchInputBox').hide();
					$('#hashTagSearchInputBox1').hide();
					elementindex=  document.getElementById("editBlock_"+feedId).selectionStart;
					var data=$("#editBlock_"+feedId).val();
	                atIndex = parseInt(elementindex)-1 ;
					//console.log('element@index:'+atIndex);
					
					if(atIndex==0 || !regex.test(data.charAt(atIndex-1))){
		     		   $('#editDivContainer_'+feedId).children('.activityfieldReplyUserList').css('display','block');
		     		   if(resultset == '' || resultset == '[]'){
		     		      //console.log("if user");
		    	    	  commonFunctionForSpecialChar('editField',feedId,menuType);
		    	   	   }else{
		    	   	       //console.log("else user");
							memberfind(replyType,feedId);
		    	   	       $('#editDivContainer_'+feedId).children('.activityfieldReplyUserList').find('ul.activityfielduserList').html(prepareUIforspecialcharacter(resultset,projectId,'editField',feedId));
		    	   	    }
					}
		    	   	
		    	   	
	    }else if(event.keyCode == 32){ 
	    			$('#hashTagSearchInputBox').hide();
	    			$('#hashTagSearchInputBox1').hide();
	    			$('.activityfieldReplyUserList').hide();
	    			elementindex = "";
	    			atIndex ="";
	    }else if(type=='keypress' && (event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey){	    	
	    	postEditedCmt(feedEditId,LoginId,Proje,MenuTyp,MenuTypId);
	      }else if(elementindex!=""){
	      			$('#hashTagSearchInputBox').hide();
	      			$('#hashTagSearchInputBox1').hide();
	                var data=$("#editBlock_"+feedId).val();
	                //console.log('atIndex:'+data.charAt(atIndex)+":");
	     			if(data.charAt(atIndex)=="@" && !regex.test(data.charAt(atIndex-1))){
				     		var focusIndex = document.getElementById("editBlock_"+feedId).selectionStart;
				    		var aTxt = data.substring(elementindex,focusIndex).toLowerCase().trim();
				    		//console.log('aTxt:'+aTxt);
				    		$('#editDivContainer_'+feedId).children('.activityfieldReplyUserList').find('.activityfielduserList li').show();
				    		$('#editDivContainer_'+feedId).children('.activityfieldReplyUserList').show();
				    		if(aTxt !=""){
				    		    $('#editDivContainer_'+feedId).children('.activityfieldReplyUserList').find('.activityfielduserList li').each(function(){
				    		    	val = $(this).text().toLowerCase();
				    		    	if(val.indexOf(aTxt)!= -1 ){
				    		    		$(this).show();
								    }else{
								   		$(this).hide();
								    }
				    		    });
				    		    
				    		    
				    		    if($('#editDivContainer_'+feedId).children('.activityfieldReplyUserList').find('.activityfielduserList li:visible').length < 1)
				    		       $('#editDivContainer_'+feedId).children('.activityfieldReplyUserList').hide();
				    		   	else
				    		   	   $('#editDivContainer_'+feedId).children('.activityfieldReplyUserList').show();
			    		   }	 
	    		  }else{
	    		  	 $('#hashTagSearchInputBox').hide();
	    		  	 $('#hashTagSearchInputBox1').hide();
	    		     $('.activityfieldReplyUserList').css('display','none');
	    		     elementindex = "";
	    			 atIndex ="";
	    		  }
	      
	    }else {
	    	elementindex='';
			$('.activityfieldReplyUserList:visible').hide();
			$('#hashTagSearchInputBox').hide();
			$('#hashTagSearchInputBox1').hide();
		}
			
//----------------------------code for # tag--------------------------------->	
			
			if (testAtData.charAt(testAtIndex)=="#"){
				$('#hashTagSearchInputBox').hide();
				$('#hashTagSearchInputBox1').hide();
				$('.proTagReplyNameId').css('display','block');
				$('.proTagReplyNameId').css('margin-top','45px');
				$('.proTagNameId').hide();
				$('.activityfieldUserListMain').hide();
				elementhashindex = document.getElementById("EdidCmtBlock_"+feedId).selectionStart;
				hashIndex = parseInt(elementhashindex)-1 ;
				$('.proTagSubReplyNameId').hide();
	
					if(hashResult == '' || hashResult == '[]'){
						ajaxCallforHashCode(menuType,feedId,'editField');
					}else{
						$('.proTagReplyNameId:visible ul').html(preparingUIforHash(hashResult,projectId,'editField',feedId));
					}
			
			}else if(event.keyCode == 32){ 
				$('#hashTagSearchInputBox').hide();
				$('#hashTagSearchInputBox1').hide();
				$('.proTagReplyNameId').hide();
				elementhashindex='';
				hashIndex ="";
			}else if(type=='keypress' && (event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey){	    	
				postEditedCmt(feedEditId,LoginId,Proje,MenuTyp,MenuTypId);
			}else if(elementhashindex!=""){
				$('#hashTagSearchInputBox').hide();
				$('#hashTagSearchInputBox1').hide();
				var data=$("#EdidCmtBlock_"+feedId).val();
		      
					if(data.charAt(hashIndex)=="#"){
						var focusIndex = document.getElementById("EdidCmtBlock_"+feedId).selectionStart;
						var aTxt = data.substring(elementhashindex,focusIndex).toLowerCase().trim();
						$('.projuserList li').show();
						$('.proTagReplyNameId').show();
							
								if(aTxt !=""){
									$('.projuserList li').each(function(){
										val = $(this).text().toLowerCase();
								
											if(val.indexOf(aTxt)!= -1 ){
												$(this).show();
											}else{
												$(this).hide();
											}
									});
		    
									if($('.projuserList li:visible').length < 1)
										$('.proTagReplyNameId').hide();
									else
										$('.proTagReplyNameId').show();
								}	 		 
					}else{
						$('#hashTagSearchInputBox').hide();
						$('#hashTagSearchInputBox1').hide();
						$('.proTagReplyNameId').css('display','none');
						elementhashindex = "";
						hashIndex ="";
					}
			}else {
			   $('.proTagReplyNameId:visible').hide();
			   $('#hashTagSearchInputBox').hide();
			   $('#hashTagSearchInputBox1').hide();
			}
 	}
 
 
 function enterKeyValidentersubreplyActComments(event,feedId,userId,projId,menuType,menuTypeId,superParentId,type){
	 if (type=='keypress' && (event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey){		
		elementindex='',elementhashindex='';atIndex="";hashIndex = "";
		subreplyActComments(feedId,userId,projId,menuType,menuTypeId,superParentId);
		$('.activityfieldUserListMain').hide();
		$('#hashTagSearchInputBox').hide();
		$('#hashTagSearchInputBox1').hide();
	}else{
		   
		    var testAtData = $("#replyBlock_"+feedId).val();
 			var testAtChar = document.getElementById("replyBlock_"+feedId).selectionStart;
 			var testAtIndex = parseInt(testAtChar)-1;
		   
		   //if(menuType == 'activityFeed'){   // this condition is commented bcoz in other modules we have to show trending codes
			
					//----------------------------code for # tag--------------------------------->
				    if (testAtData.charAt(testAtIndex)=="#") {
				 		$('#hashTagSearchInputBox').hide();
				 		$('#hashTagSearchInputBox1').hide();
						$('.activityfieldUserListMain').hide();
						$('.activityfieldSubReplyUserList').hide();
						$('.proTagSubReplyNameId').css('display','block');
					  	$('.proTagSubReplyNameId').css('margin-top','45px');
					  	$('.proTagNameId').hide();
					  	$('.proTagReplyNameId').hide();
					  	$('#commentListDiv').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
					  	//$('#commentListDiv').find('.mCSB_container').css('height','100%');
					  	elementhashindex = document.getElementById("replyBlock_"+feedId).selectionStart;
					  	hashIndex = parseInt(elementhashindex)-1 ;
						//console.log('element@hashIndex:'+hashIndex);
						if(hashResult == '[]' || hashResult == ''){
					  		ajaxCallforHashCode(menuType,feedId,'subreply');
					  	}else{
					  		$('.proTagSubReplyNameId:visible ul').html(preparingUIforHash(hashResult,projectId,'subreply',feedId));
					  	}
						
					}else if(event.keyCode == 32){ 
				    			$('.proTagSubReplyNameId').hide();
				    			$('#hashTagSearchInputBox').hide();
				    			$('#hashTagSearchInputBox1').hide();
				    			elementhashindex='';
								hashIndex ="";
					}else if(elementhashindex!=""){
								$('#hashTagSearchInputBox').hide();
								$('#hashTagSearchInputBox1').hide();
								var data=$("#replyBlock_"+feedId).val();
				                //console.log('hashIndex:'+data.charAt(hashIndex)+":");
				     			if(data.charAt(hashIndex)=="#"){
							     		var focusIndex = document.getElementById("replyBlock_"+feedId).selectionStart;
							    		var aTxt = data.substring(elementhashindex,focusIndex).toLowerCase().trim();
							    		//console.log('hashTxt:'+aTxt);
							    		$('.projuserList li').show();
							    		$('.proTagSubReplyNameId').show();
							    		if(aTxt !=""){
							    		    $('.projuserList li').each(function(){
							    		    	val = $(this).text().toLowerCase();
							    		    	if(val.indexOf(aTxt)!= -1 ){
							    		    		$(this).show();
											    }else{
											   		$(this).hide();
											    }
							    		    });
							    		    
							    		    
							    		    if($('.projuserList li:visible').length < 1)
							    		       $('.proTagSubReplyNameId').hide();
							    		   	else
							    		   	   $('.proTagSubReplyNameId').show();
						    		 }	 
				    		  }else{
				    		     $('.proTagSubReplyNameId').css('display','none');
				    		     elementhashindex = "";
				    			 hashIndex ="";
				    		  } 
				    }else{
						   $('.proTagSubReplyNameId:visible').hide();
						   $('#hashTagSearchInputBox').hide();
						   $('#hashTagSearchInputBox1').hide();
					}
					
			//}
				//----------------------------code for @ tag--------------------------------->
			if (testAtData.charAt(testAtIndex)=="@" && !regex.test(testAtData.charAt(testAtIndex-1)) ) {
							$('#hashTagSearchInputBox').hide();
							$('#hashTagSearchInputBox1').hide();
				    	   	$('.activityfieldMainuserslist').hide();
							$('.proTagNameId:visible').hide();
							$('.proTagSubReplyNameId').hide();
							$('.activityfieldReplyUserList').hide();
							$('.activityfieldSubReplyUserList').css('margin-top','45px');
							$('#commentListDiv').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
						elementindex=  document.getElementById("replyBlock_"+feedId).selectionStart;
						//console.log('elementindex:'+elementindex);
						var data=$("#replyBlock_"+feedId).val();
		                atIndex = parseInt(elementindex)-1 ;
						//console.log('element@index:'+atIndex);
						
						if(atIndex==0 || !regex.test(data.charAt(atIndex-1))){
			     		   $('.activityfieldSubReplyUserList').css('display','block');
			     		   if(resultset == '' || resultset == '[]'){
			     		      //console.log("if user");
			    	    	  commonFunctionForSpecialChar('subreply',feedId,menuType);
			    	   	   }else{
			    	   	      // console.log("else user");
			    	   		  $('.activityfieldSubReplyUserList:visible ul').html(prepareUIforspecialcharacter(resultset,projectId,'subreply',feedId));
			    	   		}
						}
			    	   	
			    	   	
		    }else if(event.keyCode == 32){ 
		    			$('.activityfieldSubReplyUserList').hide();
		    			$('#hashTagSearchInputBox').hide();
		    			$('#hashTagSearchInputBox1').hide();
		    			elementindex = "";
		    			atIndex ="";
		      }else if(elementindex!=""){
		      			$('#hashTagSearchInputBox').hide();
		      			$('#hashTagSearchInputBox1').hide();
		                var data=$("#replyBlock_"+feedId).val();
		               // console.log('atIndex:'+data.charAt(atIndex)+":");
		     			if(data.charAt(atIndex)=="@" && !regex.test(data.charAt(atIndex-1))){
					     		var focusIndex = document.getElementById("replyBlock_"+feedId).selectionStart;
					    		var aTxt = data.substring(elementindex,focusIndex).toLowerCase().trim();
					    		//console.log('aTxt:'+aTxt);
					    		$('.activityfielduserList li').show();
					    		$('.activityfieldSubReplyUserList').show();
					    		if(aTxt !=""){
					    		    $('.activityfielduserList li').each(function(){
					    		    	val = $(this).text().toLowerCase();
					    		    	if(val.indexOf(aTxt)!= -1 ){
					    		    		$(this).show();
									    }else{
									   		$(this).hide();
									    }
					    		    });
					    		    
					    		    //console.log('user length:'+$('.activityfielduserList li:visible').length);
					    		    
					    		    if($('.activityfielduserList li:visible').length < 1)
					    		       $('.activityfieldSubReplyUserList').hide();
					    		   	else
					    		   	   $('.activityfieldSubReplyUserList').show();
				    		   }	 
		    		  }else{
		    		     $('.activityfieldSubReplyUserList').css('display','none');
		    		     elementindex = "";
		    			 atIndex ="";
		    		  }
		      
		    }else {
		    	elementindex='';
				$('.activityfieldSubReplyUserList:visible').hide();
				$('#hashTagSearchInputBox').hide();
				$('#hashTagSearchInputBox1').hide();
			}
		}
	imgfeedId= feedId;
}
  		
  var pgId="";
  var ideaId="";
  var taskId="";
  var voiceType="";
  var scrumProjId="";
function addComment(menuTypeId,menuType){
	
	//alert("menuType--"+menuType);
	//alert("menuTypeId--"+menuTypeId);
  $('div#commentTextarea').remove();
  $('div#mainCommentArea').remove(); 
  $('#dOptions_'+menuTypeId).hide();
   var html="";
    
   /*html+="<div  id=\"commentTextarea\" class=\"commentDiv\" style=\"height:44px;width:96%;\">"
	             +"<textarea  id=\"main_commentTextarea\" class=\"commentTextarea\"  contenteditable=\"true\" onKeyPress=\"enterKeyValidCommentActFeed(event,"+menuTypeId+",'"+menuType+"');\"></textarea>" 
	             
	       +"<div class=\"main_commentTxtArea\" id=\"audioContainer\" style=\"display: none;\">"
			 +"<div id=\"audioArea\" style=\"display:none;\">"
			   +"<div style=\"position: absolute; padding: 0.7%; margin-left: 28%;\">"
			    +"<img src=src='"+path+"/images/play.png' onclick=\"\" id=\"customPlayer\" style=\"cursor:pointer;\">"
			       +"<div id=\"timeline\">"
	                 +"<div id=\"playhead\"></div>"
		           +"</div>"
			     +"</div>"
			      +"<audio src=\"\" id=\"audio\" ontimeupdate=\"updateTime()\" onended=\"onVoiceEnded()\" style=\"width: 100%; height: 100%; padding: 0.7%;\"></audio>"			         
			        +"<div style=\"padding: 1%; float: right; margin-right: 22%;\"><img src=\""+path+"/images/circle.png\" onclick=\"\" style=\"width: 20px;cursor:pointer;\" id=\"cancelVoice\"></div>"
			      +"</div>"			      
			        +"<div class=\"timer\" style=\"float: right; display: block; height: 60%; margin-top: 1%; width: 11%;\">" 
			          +"<label style=\"color:red; float: left;\">Recording</label>"
			            +" <div style=\"font-size: 14px; float: left; padding-left: 11%;\" class=\"timerSpan\"></div>"
			          +"</div>" 
		         +"</div>"
		         
		       +"<div class=\"projUserListMain proTagNameId\">"
			       +"<ul class=\"projuserList\"></ul>"
			   +"</div>"
			   
			   +"<div id=\"convoPost\" class=\"main_commentTxtArea_btnDiv\" align=\"center\">"
		        +"<div style=\"width: 42%;float: left;display:none;\" onclick=\"clearMainFeedData()\"><img class=\"img-responsive\" src=\""+path+"/images/workspace/remove.png\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" alt=\"Image\"></div>"
		          +"<div style=\"width: 55%;\" onclick=\"CommentActFeed("+menuTypeId+",'"+menuType+"')\"><img class=\"img-responsive\" src=\""+path+"/images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\"></div>"
		       +"</div>"
		       
		       +"<div class=\"main_commentTxtArea_optDiv\" style=\"height: 59px;\" align=\"center\">"
		           +"<div id=\"actFeedMoreOpt\" style=\"float: right; width: 35%;\" onclick=\"showActFeedMore("+feedId+")\" align=\"right\">"
		             
		              +" <img src=\""+path+"/images/more.png\" class=\"img-responsive margin Options_cLabelTitle\" style=\"padding-left:0px;padding-top:9px;cursor:pointer;\" alt=\"Image\">"
		           +"</div>"		        
		            +"<div id=\"actFeedOptPopDiv_0\" class=\"actOpt\">"            
		               +"<div class=\"workSpace_arrow_right\" style=\"position: absolute; float: left; margin-top: 2px; right: 48px;\">"
		                   +"<img style=\"transform: rotate(180deg); width: 10px; height: 20px;\" src=\""+path+"/images/arrow.png\">"
		               +"</div>"         
		                 +"<div onclick=\"\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\">" 
		                   +"<img class=\"imgCss\" src=\""+path+"/images/takepicture.png\" style=\"border: medium none;\">"
		                 +"</div>"          
		                   +"<div onclick=\"\" style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\">" 		           
		                    +"<form action=\'${path}/WorkSpaceProjImgUpload\' enctype=\"multipart/form-data\" style=\"position:absolute;top:-100px;\" method=\"post\" name=\"convFileUpload\" id=\"convFileUpload\">"				
				              +"<input title=\"upload File\" value=\"\" onchange=\"readFileUrl(this,0);\" class=\"topic_file\" name=\"FileUpload\" id=\"FileUpload\" type=\"file\">"	
				            +"</form>"		          
		                  +"<img class=\"imgCss\" src=\""+path+"/images/upload.png\" id=\"mainUpload\" style=\"border: medium none;\">"
		                +"</div>"         
		              +"</div>"
		           +"<div id=\"recAudioMsg\" style=\"float: right; width: 40%;\">"
		            +"<img class=\"img-responsive\" src=\""+path+"/images/record.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
		           +"</div>"		       
		         +"</div>"			   
		         
	      +"</div>";
	      */
   
   
   html+="<div  id=\"commentTextarea\" class=\"commentDiv\" style=\"height:44px;width:96%;\">"
	   +" <div  class=\"activityfieldUserListMain activityfieldMainuserslist\">"
		+" 		<ul class=\"activityfielduserList\"></ul>"
		+" </div>"
		+"<div  class=\"projUserListMain proTagNameId\">"
		   + "<ul class=\"projuserList\"></ul>"
		 +"</div>";
			   if(menuType == "blog"){  //onfocus attribute should work only for blog comments
			 		html += "<textarea  id=\"main_commentTextarea\" class=\"main_commentTxtArea\" style=\"width:96%;\" contenteditable=\"true\" onfocus='setOffsetDiv(this.event)' onkeyup=\"enterKeyValidCommentActFeed(event,"+menuTypeId+",'"+menuType+"','keyup');\" onkeypress=\"enterKeyValidCommentActFeed(event,"+menuTypeId+",'"+menuType+"','keypress');\"></textarea>"
				}else{
			 		html += "<textarea  id=\"main_commentTextarea\" class=\"main_commentTxtArea\" style=\"width:96%;\" contenteditable=\"true\" onkeyup=\"enterKeyValidCommentActFeed(event,"+menuTypeId+",'"+menuType+"','keyup');\" onkeypress=\"enterKeyValidCommentActFeed(event,"+menuTypeId+",'"+menuType+"','keypress');\"></textarea>"
				}
			html +="<input type=\"hidden\" id=\"menuTypeForComment\" value='"+menuType+"'>"
				  +"<input type=\"hidden\" id=\"menuTypeIdForComment\" value='"+menuTypeId+"'>"
			 	
			  + "<div id=\"convoPost\" class=\"main_commentTxtArea_btnDiv\" align=\"center\" style=\"float:right;\">"
			  +"<div style=\"width: 42%;float: left;display:none;\" onclick=\"clearMainFeedData();\"><img class=\"img-responsive\" src=\"images/workspace/remove.png\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" alt=\"Image\">"
             +"</div>"
           +"<div style=\"width: 55%;\"onclick=\"CommentActFeed('"+menuTypeId+"','"+menuType+"')\"><img class=\"img-responsive Post_cLabelTitle\" title=\"\" src=\"images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
          +"</div>"
         +"</div>"
	      +"<div class=\"main_commentTxtArea\" id=\"audioContainer\" style=\"display: none; width:96%;\">"
		   + "<div id=\"audioArea\" style=\"display:none;\">"
		    + "<div style=\"position: absolute; padding: 0.7%; margin-left: 17%;\">"
		     +"<img src='images/play.png' onclick=\"\" id=\"customPlayer\" style=\"cursor:pointer;\">"
		      +"<div id=\"timeline\">"
            + "<div id=\"playhead\">"
                +"</div>"
	           +"</div>"
		     +"</div>"
		        +"<audio src=\"\" id=\"audio\" ontimeupdate=\"updateTime()\" onended=\"onVoiceEnded()\" style=\"width: 100%; height: 100%; padding: 0.7%;\"></audio>"		         
		          + "<div style=\"padding: 1%; float: right; margin-right: 22%;\"><img src=\"images/circle.png\" onclick=\"\" style=\"width: 20px;cursor:pointer;\" id=\"cancelVoice\">"
		           +"</div>"
		            +"</div>"		      
		             +"<div class=\"timer\" style=\"float: right; display: block; height: 60%; margin-top: 1%; width: 15%;\">" 
		         +"<label style=\"color:red; float: left;\">Recording</label>"
		       +"<div style=\"font-size: 14px; float: left; padding-left: 11%;\" class=\"timerSpan\">"
		     +"</div>"
		   +"</div>" 
	     +"</div>"
	    // +"<div class=\"projUserListMain proTagNameId\" style=\"display: none;\">"
	      // +" <ul class=\"projuserList\"></ul>"
	     //   + "</div>"
	     /* +"<div class=\"commentIconId\" style=\"width:3%;float:left;padding-left: 8px;\" onclick=\"CommentActFeed("+menuTypeId+",'"+menuType+"')\" ><img class=\"img-responsive\" src=\""+path+"/images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\Image\">"     
		 +"</div>"*/
	         /*+"<div id=\"convoPost\" class=\"main_commentTxtArea_btnDiv\" align=\"center\">"
	            +"<div style=\"width: 42%;float: left;display:none;\" onclick=\"clearMainFeedData();\"><img class=\"img-responsive\" src=\""+path+"/images/workspace/remove.png\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" alt=\"Image\">"
	              +"</div>"
	                +"<div style=\"width: 55%;\"onclick=\"CommentActFeed("+menuTypeId+",'"+menuType+"')\"><img class=\"img-responsive\" src=\""+path+"/images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
	                  +"</div>"
	                   +"</div>"*/
	                    +"</div>"
		              +"<div align=\"center\" class=\"main_commentTxtArea_optDiv\"  id=\"mainCommentArea\" style=\"height: 59px;\">"
		if(menuType == "document" || menuType == "blog" || menuType == "galleryComment"){
		         html += "<div id = \"actFeedMoreOpt\" align=\"right\" style=\"float: right; width: 35%;display:none;\" onclick=\"showActFeedMore(0,"+menuTypeId+",'"+menuType+"');\">"
	       //+  "<img id=\"moreOption\" class=\"img-responsive\" src=\"${path}/images/more.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
	              +" <img src=\""+path+"/images/more.png\" title=\"Options\" class=\"img-responsive margin Options_cLabelTitle\" style=\"padding-right:0px;padding-top:9px;cursor:pointer;\" alt=\"Image\">"
	            + "</div>"
		}else{
			    html += "<div id = \"actFeedMoreOpt\" align=\"right\" style=\"float: right; width: 35%;\" onclick=\"showActFeedMore(0,"+menuTypeId+",'"+menuType+"');\">"
		       //+  "<img id=\"moreOption\" class=\"img-responsive\" src=\"${path}/images/more.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
		              +" <img src=\"images/more.png\" title=\"Options\" class=\"img-responsive margin Options_cLabelTitle\" style=\"padding-right:0px;padding-top:9px;cursor:pointer;\" alt=\"Image\">"
		            + "</div>"
		}
	            if(menuType == "Task"){
	            	html += "<div id=\"actFeedOptPopDiv_0\" class = \"actOpt\" style=\"right: 85px;\">"   
	            }else{
	            	html += "<div id=\"actFeedOptPopDiv_0\" class = \"actOpt\" >" 
	            }
	       html +="<div class=\"workSpace_arrow_right\" style=\"position: absolute; float: left; margin-top: 2px; right: -10px;\">"
	                  +"<img style=\"transform: rotate(0deg); width: 10px; height: 20px;\" src=\"images/arrow.png\">"
	                      +"</div>" ;
	       if(menuType == "document"){  //onfocus attribute should work only for blog comments
	      		html += "<div onclick=\"initWebcam('',"+menuTypeId+",'','"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;\">";
	      // }else if(commentPlace == "ScrumBoard"){
	       //html += "<div onclick=\"initWebcam('',"+menuTypeId+","+scrProjId+",'"+menuType+"')\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\">" ;
	       }else if(menuType == "Task" || menuType == 'wsIdea' || menuType == 'agile'){
	    	   projId = projIdForVideo;
	    	   html += "<div onclick=\"initWebcam('',"+menuTypeId+",'"+projId+"','"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\">" ;  
	       }else{
	    	   projId = projIdForVideo;
	    	   html += "<div onclick=\"initWebcam('',"+menuTypeId+",'"+projId+"','"+menuType+"')\" title=\"Camera\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"width: 99%; height: auto; cursor: pointer; float: left;\">" ;  
	       }
	                          html += "<img class=\"imgCss \" src=\"images/takepicture.png\" style=\"border: medium none;\">"
	                            +"</div>"
	                            +"<div onclick=\"\" style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\">" 
	                            
	                            if(menuType == "activityFeed" || menuType == "Task" || menuType == 'wsIdea' || menuType == 'agile'){
	                            	html+= "<form action=\'WorkSpaceProjImgUpload\' enctype=\'multipart/form-data'\ style=\'position:absolute;top:-100px;\' method=\'post\' name=\'convFileUpload\' id=\'convFileUpload\' >"				
				                     + "<input type=\'file\' title=\'upload File\' value=\'\' onchange=\'readFileUrl(this,0);\' class=\'topic_file\' value=\'\' name=\'FileUpload\' id=\'FileUpload\'>"	
				                    +  "</form>"
				                    +"<img class=\"imgCss\" src=\"images/upload.png\" title=\"Upload\" id=\"mainUpload\" style=\"border: medium none;\">";
	                            }
	                          html+="</div>"         
	                      +"</div>"
	                   // +"<div id=\"recAudioMsg\" style=\"float: right; width: 40%;\">"
	                      //+"<div id=\"recAudioMsg\" style=\"width: 40%;\">"
	                      +"<div id=\"recAudioMsg\" class=\"Rec_audio_cLabelTitle\" title=\"\" style=\"padding-left: 0px;padding-right: 2px;margin-right: 15px;margin-left:5px;\">"
	                 + "<img class=\"img-responsive\" src=\"images/record.png\" style=\"margin-top: 5px;cursor: pointer;padding-top: 5px;padding-left: 1px;\" alt=\"Image\">"
	              +"</div>"	       
	          +"</div>";
 
   /*html+="<div  id=\"commentTextarea\" class=\"commentDiv\" style=\"height:44px;width:96%;\">"
      +"    <textarea  id=\"main_commentTextarea\" class=\"commentTextarea\"  contenteditable=\"true\" onKeyPress=\"enterKeyValidCommentActFeed(event,"+menuTypeId+",'"+menuType+"');\"></textarea>"
      +"    <div class=\"commentIconId\" style=\"width:3%;float:left;padding-left: 8px;\" onclick=\"CommentActFeed("+menuTypeId+",'"+menuType+"')\" ><img class=\"img-responsive\" src=\""+path+"/images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\Image\"></div>"     
	 +"</div>"
     +" <div id=\"recAudioMsg\" style=\"width:5%;margin-top:-4%;float:right;box-shadow: none;\"><img class=\"img-responsive\" src=\""+path+"/images/record.png\" style=\"cursor: pointer; float:right; \" alt=\Image\">"
     	+"</div>"; */ 
//alert("menuType:"+menuType);
//alert("commentPlace:"+commentPlace);
	  if(menuType == "agile"){
	         if(commentPlace=='Story'){
		     	$("#comment_"+menuTypeId+"").show();
			    $("#comment_"+menuTypeId+"").prepend(html);
			    $("#comment_"+menuTypeId+"").find("#commentTextarea").show();
			    $("#comment_"+menuTypeId+"").find('textarea').focus();
		        $('#epicListCommonDiv_'+menuTypeId).show();
		   		fetchAgileComments(menuTypeId);
		   	 }else{
		   	 	$("#sbCommentListPopUp .epicCommentDiv").html(html);
			    $("#sbCommentListPopUp .epicCommentDiv").find("#commentTextarea").show();
			    $("#sbCommentListPopUp .epicCommentDiv").find('textarea').focus();
			    if(commentPlace=='Sprint'){
			        $('#typeOfSprintCmt').text('').text(getValues(companyLabels,"Sprint_Comment"));
			        fetchSprintComments(menuTypeId);
			    }else if(commentPlace=='ScrumBoard'){	
			         var type = $('#SB_story_'+menuTypeId).attr('type');
			         if(type == "E"){
			             $('#typeOfSprintCmt').text('').text(getValues(companyLabels,"Epic_Comment"));
			         }else if(type == "F"){
			              $('#typeOfSprintCmt').text('').text(getValues(companyLabels,"Feature_Comment"));
			         }else{
			             $('#typeOfSprintCmt').text('').text(getValues(companyLabels,"Story_Comment"));
			         }
		   	    	fetchSBStoryComments(menuTypeId);
		   	    }	
		   	 }	
		     pgId = menuTypeId;
		     projId = projectId;
		     voiceType=menuType;
		     //alert("Agile voiceType::"+voiceType);
	   }else if(menuType == "Task"){
			   // $("#Taskcomment_"+menuTypeId+"").show();
	    	   $("#Taskcomment_"+menuTypeId+"").children("div.TaskCommentMainTextareaDiv").html(html);
	    	   $("#Taskcomment_"+menuTypeId+"").find("#commentTextarea").show();
	    	   $("#Taskcomment_"+menuTypeId+"").find('textarea').focus();
	    	   pgId = menuTypeId;
			   taskId = menuTypeId;			   
		   	   //pgId = menuTypeId;
		       projId = projectId;	
		       voiceType=menuType;
			      	
	   }else{
		   $("#comment_"+menuTypeId+"").show();
		   $("#comment_"+menuTypeId+"").prepend(html);
		   $("#comment_"+menuTypeId+"").find("#commentTextarea").show();
		   $("#comment_"+menuTypeId+"").find('textarea').focus();
		   pgId = menuTypeId; 
		   //alert("agile else pgId--"+pgId);
		   
		   if(menuType == "wsDocument"){
			   	  /* fileSharingForComments("main_commentTextarea" , "0");
				   $('#mainUpload').click(function(e){
			    	   e.preventDefault();
				       e.stopPropagation();  
			      	   $('#FileUpload').trigger('click');
				   })*/;
			       pgId = menuTypeId;
			       projId=projectId;			      			     
			       voiceType=menuType;
				   //alert("wsDocument voiceType::"+voiceType);	
		   }else if(menuType == "document"){
			   voiceType=menuType;
			   //alert("document voiceType::"+voiceType);			   
		   }else if(menuType == "blog"){
		   		blogCommentScroll = true;
		   		$('.cDiv').hide();
		   		//$('.blgCommentCls').attr('onclick','addComment('+menuTypeId+',"blog")');
		   		$('.blgCommentImgCls').attr('src','images/comment.png');
		   		$("#comment_"+menuTypeId+"").show();
		   		$("#comment_"+menuTypeId+"").find('textarea').focus();
		   		$("#blogCommentsDiv_"+menuTypeId+"").show();
				$("#blogComment_"+menuTypeId).attr('onclick','hideComments('+menuTypeId+')');
				$("#commentIcon_"+menuTypeId).attr('onclick','hideComments('+menuTypeId+')');
				$("#commentIcon_"+menuTypeId).attr('src','images/commentToggle.png');
				
		      	pgId = menuTypeId;
		      	projId=projectId;
		      	loadBlogPostData(projectId);
		      	voiceType=menuType;
		      	//alert("blog voiceType::"+voiceType);
		       
		   }else if(menuType == "wsIdea"){
			    fileSharingForComments("main_commentTextarea" , "0");
			    $('#mainUpload').click(function(e){
			    	 e.preventDefault();
				     e.stopPropagation();  
			      	 $('#FileUpload').trigger('click');
				});
		   		$("#comment_"+menuTypeId+"").show();
		   		/*var imgHtml =  "<img id=\"cIcon_"+menuTypeId+"\" onclick = \"showIdeaIconComments('cIcon_"+menuTypeId+"', "+menuTypeId+")\" src=\""+path+"/images/commentToggle.png\"  title=\"View Comments\" class = \"ideaIconsContainerCss commentsIconPresentCss\" style=\"cursor:pointer;\" />"
		  		
		  		if($("#epicContainer_"+menuTypeId).children().hasClass("commentsIconPresentCss")){
		   		}else{	
		   			$("#epicContainer_"+menuTypeId+"").append(imgHtml);
		   		}*/
			    $("#cIcon_"+menuTypeId).attr('src','images/commentToggle.png').attr('onclick',"showIdeaIconComments('cIcon_"+menuTypeId+"','"+menuTypeId+"');");
			    $("#comment_"+menuTypeId+"").children("div.commentTextarea").html(html);
	    	    $("#comment_"+menuTypeId+"").find("#commentTextarea").show();
	    	    $("#comment_"+menuTypeId+"").find('textarea').focus();
			    ideaId = menuTypeId;			   
		   		pgId = menuTypeId;
		      	projId = projectId;	
		      	voiceType=menuType;
		      	loadCustomLabelIdeas("Ideas");
		      	//alert("wsIdea voiceType::"+voiceType);
		      	//showIdeaIconComments('cIcon_'+menuTypeId,menuTypeId, 'loadCommAdd');
		   }else if(menuType == "galleryComment"){			  
			   projId = projectId;			   
		     
		   var html="";
		      html+="<div  id=\"commentTextarea\" class=\"commentDiv\" style=\"height:44px;width:100%\">"
		      		+"<div  class=\"projUserListMain proTagNameId\">"
		   				+ "<ul class=\"projuserList\"></ul>"
		 			+"</div>"
		      		+" <div  class=\"activityfieldUserListMain activityfieldMainuserslist\">"
						+" 		<ul class=\"activityfielduserList\"></ul>"
					+" </div>"
      +"    <textarea style=\"width:93%\" id=\"main_commentTextarea\" class=\"commentTextarea\"  contenteditable=\"true\" onkeyup=\"enterKeyValidCommentActFeed(event,"+menuTypeId+",'"+menuType+"','keyup');\" onkeypress=\"enterKeyValidCommentActFeed(event,"+menuTypeId+",'"+menuType+"','keypress');\"></textarea>"
      +"    <div class=\"commentIconId\" style=\"width: 5%;float:left;margin-left: 4px;\" onclick=\"CommentActFeed("+menuTypeId+",'"+menuType+"')\" ><img class=\"img-responsive\" title=\"Post\" src=\"images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\Image\"></div>"
	 +"</div>" ;
		   
   /*		   html+="<div  id=\"commentTextarea\" class=\"commentDiv\" style=\"height:44px;width:96%;\">"
			   +" <div  class=\"activityfieldUserListMain activityfieldMainuserslist\">"
				+" 		<ul class=\"activityfielduserList\"></ul>"
				+" </div>"
				+"<div  class=\"projUserListMain proTagNameId\">"
				   + "<ul class=\"projuserList\"></ul>"
				 +"</div>"
			      +"<textarea  id=\"main_commentTextarea\" class=\"main_commentTxtArea\" style=\"width:93%;\" contenteditable=\"true\" onfocus='setOffsetDiv(this.event)' onkeyup=\"enterKeyValidCommentActFeed(event,"+menuTypeId+",'"+menuType+"');\"></textarea>"
			       +"<div id=\"convoPost\" class=\"main_commentTxtArea_btnDiv\" align=\"center\" style=\"float:right;width:7%;\">"
		            +"<div style=\"width: 42%;float: left;display:none;\" onclick=\"clearMainFeedData();\"><img class=\"img-responsive\" src=\""+path+"/images/workspace/remove.png\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" alt=\"Image\">"
		             +"</div>"
		           +"<div style=\"width: 55%;\"onclick=\"CommentActFeed("+menuTypeId+",'"+menuType+"')\"><img class=\"img-responsive\" src=\""+path+"/images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
		          +"</div>"
		         +"</div>"
			      +"<div class=\"main_commentTxtArea\" id=\"audioContainer\" style=\"display: none; width:93%;\">"
				   + "<div id=\"audioArea\" style=\"display:none;\">"
				    + "<div style=\"position: absolute; padding: 0.7%; margin-left: 3%;\">"
				     +"<img src='"+path+"/images/play.png' onclick=\"\" id=\"customPlayer\" style=\"cursor:pointer;\">"
				      +"<div id=\"timeline\" style=\"width:200px;\">"
		            + "<div id=\"playhead\">"
		                +"</div>"
			           +"</div>"
				     +"</div>"
				        +"<audio src=\"\" id=\"audio\" ontimeupdate=\"updateTime()\" onended=\"onVoiceEnded()\" style=\"width: 100%; height: 100%; padding: 0.7%;\"></audio>"		         
				          + "<div style=\"padding: 3%; float: right; margin-right: 8%;\"><img src=\""+path+"/images/circle.png\" onclick=\"\" style=\"width: 20px;cursor:pointer;\" id=\"cancelVoice\">"
				           +"</div>"
				            +"</div>"		      
				             +"<div class=\"timer\" style=\"float: right; display: block; height: 60%; margin-top: 2%; width: 35%;\">" 
				         +"<label style=\"color:red; float: left;\">Recording</label>"
				       +"<div style=\"font-size: 14px; float: left; padding-left: 11%;\" class=\"timerSpan\">"
				     +"</div>"
				   +"</div>" 
			     +"</div>"
			    // +"<div class=\"projUserListMain proTagNameId\" style=\"display: none;\">"
			      // +" <ul class=\"projuserList\"></ul>"
			     //   + "</div>"
			      +"<div class=\"commentIconId\" style=\"width:3%;float:left;padding-left: 8px;\" onclick=\"CommentActFeed("+menuTypeId+",'"+menuType+"')\" ><img class=\"img-responsive\" src=\""+path+"/images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\Image\">"     
				 +"</div>"
			         +"<div id=\"convoPost\" class=\"main_commentTxtArea_btnDiv\" align=\"center\">"
			            +"<div style=\"width: 42%;float: left;display:none;\" onclick=\"clearMainFeedData();\"><img class=\"img-responsive\" src=\""+path+"/images/workspace/remove.png\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" alt=\"Image\">"
			              +"</div>"
			                +"<div style=\"width: 55%;\"onclick=\"CommentActFeed("+menuTypeId+",'"+menuType+"')\"><img class=\"img-responsive\" src=\""+path+"/images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
			                  +"</div>"
			                   +"</div>"
			                    +"</div>"
				              +"<div align=\"center\" class=\"main_commentTxtArea_optDiv\"  id=\"mainCommentArea\" style=\"height: 59px;\">"
			                + "<div id = \"actFeedMoreOpt\" align=\"right\" style=\"float: right; width: 35%; display:none;\" onclick=\"showActFeedMore(0);\">"
			       //+  "<img id=\"moreOption\" class=\"img-responsive\" src=\"${path}/images/more.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
			              +" <img src=\""+path+"/images/more.png\" class=\"img-responsive margin Options_cLabelTitle\" style=\"padding-left:0px;padding-top:9px;cursor:pointer;\" alt=\"Image\">"
			            + "</div>"	       
			           + "<div id=\"actFeedOptPopDiv_0\" class = \"actOpt\">"            
			         +"<div class=\"workSpace_arrow_right\" style=\"position: absolute; float: left; margin-top: 2px; right: 48px;\">"
			                  +"<img style=\"transform: rotate(180deg); width: 10px; height: 20px;\" src=\""+path+"/images/arrow.png\">"
			                      +"</div>"         
			                        +"<div onclick=\"\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\">" 
			                          + "<img class=\"imgCss\" src=\""+path+"/images/takepicture.png\" style=\"border: medium none;\">"
			                            +"</div>"          
			                              +"<div onclick=\"\" style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\">" 	           
			                                + "<form action=\'${path}/WorkSpaceProjImgUpload\' enctype=\'multipart/form-data'\ style=\'position:absolute;top:-100px;\' method=\'post\' name=\'convFileUpload\' id=\'convFileUpload\' >"				
					                     + "<input type=\'file\' title=\'upload File\' value=\'\' onchange=\'readFileUrl(this,0);\' class=\'topic_file\' value=\'\' name=\'FileUpload\' id=\'FileUpload\'>"	
					                    +  "</form>"	          
			                          + "<img class=\"imgCss\" src=\""+path+"/images/upload.png\" id=\"mainUpload\" style=\"border: medium none;\">"
			                        +"</div>"         
			                      +"</div>"
			                   // +"<div id=\"recAudioMsg\" style=\"float: right; width: 40%;\">"
			                      +"<div id=\"recAudioMsg\" style=\"width: 40%;\">"
			                 + "<img class=\"img-responsive\" src=\""+path+"/images/record.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
			              +"</div>"	       
			          +"</div>";*/
			   $("#galcomment").show();
			   $("#galcomment").prepend(html);
			   $("#galcomment").find("#commentTextarea").show();
			   $("#galcomment").find('textarea').focus();
			   
			  $("#blogCommentsDiv_"+menuTypeId+"").show();
		      $("#blogComment_"+menuTypeId).attr('onclick','hideComments('+menuTypeId+')');
		      $("#commentIcon_"+menuTypeId).attr('onclick','hideComments('+menuTypeId+')');
		      $("#commentIcon_"+menuTypeId).attr('src','images/commentToggle.png');
		      pgId = menuTypeId;
		      //alert("gallery menuTypeId:::"+menuTypeId);
		      projId=projectId;
		      //alert("gallery projId:::"+projId);
		      loadAlbumCommentData(projectId);
		   }
	  } 
	  /*-------------------Voice recording-------------*/
	  VoiceRecording();	
	 
	   $('#customPlayer').attr('src','/images/conversation/play.svg');
	     
	     $('#customPlayer').click(function(){
	     	//alert("customPlayer");
	     	
	     	if($(this).attr('src').indexOf('play.svg') > 0){
	     		$(this).attr('src','images/conversation/pause.svg');
	     		 $("#audio")[0].play();
	     	  }	
	     	else{
	     		$(this).attr('src','/images/conversation/play.svg');
	     		 $("#audio")[0].pause(); 
	     	 }        	
	     });
	       
	    $("#timeline").click( function (event) { 
	 	   //alert("timeline");
	 	   var  duration = $("#audio")[0].duration;
	     	moveplayhead(event);
	     	
	     	//alert("duration------"+duration+"---clickPercent(event) -"+clickPercent(event));

	     	$("#audio")[0].currentTime = duration * clickPercent(event);
	     });  
	     
	    function clickPercent(e) {
	    	return (e.pageX - $("#timeline").offset().left) / 400 ; // 400 is the width of the time line
	      }
	    
	   $('#cancelVoice').click(function(){ // On cancel of Voice player in preview section 	 
	 	  $("#audio").attr("src", '');  // discard the audio source
	 	  $('#convoPost').children().eq(1).attr('onclick',"CommentActFeed('conversationplace','activityFeed')"); // change the audio post onclick method
	 	  $('.actFeedMore').parent().prev().children().eq(3).attr('onclick',replyFunction); // to change for subreply
	 	  $('#audioContainer , div.timer').hide(); //hiding audio container
	       $('#main_commentTextarea').show(); // showing textarea
	       $('div.timer').find('div.timerSpan').text(''); // clearing the counter
	   });
	   

	function moveplayhead(e) {
	    	var timelineWidth = 400;
	    	var newMargLeft = e.pageX - Math.ceil($("#timeline").offset().left);
	    	
	        var $playhead =  $("#playhead");   
	    	
	    	if (newMargLeft != 0 && newMargLeft != timelineWidth) {
	    		$playhead.css('marginLeft', newMargLeft + "px");
	    	}
	    	if (newMargLeft == 0) {
	    		$playhead.css('marginLeft', "0px");
	    		//playhead.style.marginLeft = "0px";
	    	}
	    	if (newMargLeft == timelineWidth) {
	    		//playhead.style.marginLeft = timelineWidth + "px";
	    		$playhead.css('marginLeft', timelineWidth + "0px");
	    	}
	    }

	
 }

	function TaskCommentsHideandShowFunc(menuType){
		if(menuType == "Task"){
			$('#normalDepTaskNewUI').hide();
		    $('#conversationTaskViewListNewUI').hide();
		    
		    if($('.epicCommonListDivCss').css('display') == 'block'){
		        $('#normalTaskAttachNewUI').hide();
		        $('#attachTaskHeader').hide();
		        $('#participantsImgListNewUI').show();
		        $('#attachTaskListNewUI').hide();
		        $('#participantsDetailsHeader').hide();
		        $('#prtcpntSection').hide();
		        $('#attachRepoHeader').hide();
		        $('#attachDocFromRepoNewUI').hide();
		    }else{
		        $('#normalTaskAttachNewUI').hide();
		        $('#attachLinkDivArea').hide();
		        if($('#prtcpntSection').find($('[id ^= emailPrtcpnt_]')).length > 0 ){
		        	$('#participantsImgListNewUI').show();
		     	    $('#participantsDetailsHeader').show();
		            $('#prtcpntSection').show();
		        }
		        $('#attachTaskHeader').hide();
		    }
		    $('#attachLinkDivArea').hide();
		}
	}

var imgCmntType="";
var feedDocId="";
  function showActFeedMore(feedId,menuTypeId,imagType){	
  	if ($('#actFeedOptPopDiv_'+feedId).is(":hidden")) {
			$('#actFeedOptPopDiv_'+feedId).slideToggle(function(){
				$(this).css('overflow','');
			});
	}else{
		$('#actFeedOptPopDiv_'+feedId).slideToggle();
	} 
  	imgCmntType=imagType;  	
  	feedDocId=menuTypeId; 
 }

 

/***** For blog ********/
  var pgId="";
function hideComments(menuTypeId){
	pgId=menuTypeId;
$("#actFeedTot_"+notblogCommentId).removeClass('notHightlightCls');
   $("#blogCommentsDiv_"+menuTypeId+"").hide();
   $("#comment_"+menuTypeId+"").hide();
   $("#commentIcon_"+menuTypeId).attr('src','images/comment.png');
   $("#commentIcon_"+menuTypeId).attr('onclick','addComment('+menuTypeId+',"blog")');
   $("#blogComment_"+menuTypeId).attr('onclick','addComment('+menuTypeId+',"blog")');
   h = $(window).height()-$("#header_outer").height()-$("#tabMainDiv").height();
           		var hCHeight = eval(h); 
	      		if($("#articleDetailData").height() > hCHeight){
				  $("#blogArticleDiv").css("height","");
				}else{
				  $("#blogArticleDiv").css("height",hCHeight-6);
				}
			    var isiPad = navigator.userAgent.match(/iPad/i) != null;
		       if(!isiPad){
					$('div#articleDetailData').mCustomScrollbar("update");
			    }
}
var blogNotificationRedirect = "";
function loadBlogPostData(blgId){
  		$('#loadingBar').show();
  		timerControl("start");
  		var localOffsetTime=getTimeOffset(new Date());
		$.ajax({
	      	url: path+"/blogAction.do",
			type:"POST",
			data:{act:"loadBlogPostData", blgId:blgId, pgId:pgId, localOffsetTime:localOffsetTime},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
				jsonData = jQuery.parseJSON(result);
			    result = prepareCommentsUI(jsonData,"blog",pgId);
				$("#blogCommentsDiv_"+pgId).html(result);
				$("div.cDiv").on("mouseover","div.actFeedHover",function(){
						   if(!$(this).find('div.actFeedOptionsDiv').is(':visible')){
								$('div.actFeedHover').find('div.actFeedOptionsDiv').hide();
							}
					  });
				$(".mainCmtUicon").css('width','3.5%');
				h = $(window).height()-$("#header_outer").height()-$("#tabMainDiv").height();
           		var hCHeight = eval(h); 
	      		if($("#articleDetailData").height() > hCHeight){
				  $("#blogArticleDiv").css("height","");
				}else{
				  $("#blogArticleDiv").css("height",hCHeight-6);
				}
				
				$("#actFeedTot_"+notblogCommentId).addClass('notHightlightCls');
				notblogCommentId='';
				 var isiPad = navigator.userAgent.match(/iPad/i) != null;
			    if(!isiPad){
			    	$('div#articleDetailData').mCustomScrollbar("destroy");
					popUpScrollBar('articleDetailData');
					$('div#articleDetailData').mCustomScrollbar("update");
					if($('div#articleDetailData').children().find('.mCSB_container').hasClass('mCS_no_scrollbar')){
					   $('div#articleDetailData').find('.mCSB_container').css("height","100%");
					 }
				}
				
				if(blogCommentScroll && blogNotificationRedirect != 'blogCommentShare'){
					 if(!$('div#articleDetailData').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
					 	//$('div#articleDetailData').mCustomScrollbar("update");
					 	setTimeout(function(){
						 	var eleTop = $('#comment_'+pgId).offset().top - $("#articleDetailData .mCSB_container").offset().top;
							var offset=50;
							resetOffset =  eleTop-offset;
					    $("#articleDetailData").mCustomScrollbar("scrollTo", eleTop-offset)
						},100);
					}
				}
				$("#comment_"+pgId+"").find('textarea').focus();
				blogCommentScroll = false;
				$('#loadingBar').hide();
				timerControl("");
			   
			}
		});
	}	
  var resetOffset =""; 	
  function setOffsetDiv(e){
  	if(blogNotificationRedirect != 'blogCommentShare'){
  		$("#articleDetailData").mCustomScrollbar("scrollTo", resetOffset-10) ;
  	}
  }
  
  	function loadAlbumCommentData(albumId){
    $("#gCommentDiv").show();
    $("#loadingBar").show();
    timerControl("start");
    $.ajax({
      		url: path+"/myzoneAction.do",
			type:"POST",
			data:{act:"gfetchComments",albumId:albumId,photoId:pgId},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
			    checkSessionTimeOut(result);
			    jsonData = jQuery.parseJSON(result);
			    result = prepareCommentsUI(jsonData,"galleryComment",pgId);
				$("#gCommentDiv").html(result);
				$("div.cDiv").on("mouseover","div.actFeedHover",function(){
						   if(!$(this).find('div.actFeedOptionsDiv').is(':visible')){
								$('div.actFeedHover').find('div.actFeedOptionsDiv').hide();
							}
					  });
			    popUpScrollBar('gCommentDiv');
			    $("div#gCommentDiv").mCustomScrollbar("update");
			    if($('div#gCommentDiv').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
			       $('div#gCommentDiv').children().children('.mCSB_container').css("position","static");
			        $('div#gCommentDiv').children().children('.mCSB_container').css("height","100%");
	    	       $("div#gCommentDiv").mCustomScrollbar("update"); 
	    		}
			    $("#loadingBar").hide();
     			timerControl("");
		   }
		});
}
/***** For blog ********/	    

  	
/* For listing Tasks*/
	/* function showConversationTasks(obj, feedId){
		conversationThreadShow = false;//used to hide the conversation thread icon in task detail pop up for conversation task it is declared in taskUIcode.js
		feedIdForCal = feedId; //feedIdForCal is declared globally and it is used to store conversation feedId value while updating the time in conversation task. 
		
		let jsonbody = {
			"user_id":userIdglb,
			"company_id":companyIdglb,
			"source_id":feedId
		}
		if(obj != "showConvoTasks"){
  			if($(obj).attr('src').indexOf("Toggle.png") == -1 ){
				$(obj).attr('src', 'images/idea/task1Toggle.png');
				$("#loadingBar").show();
				timerControl("start");
				$.ajax({
		    		url: apiPath+"/"+myk+"/v1/getTaskDetails/conversation",
					type:"POST",
					dataType:'json',
					contentType:"application/json",
					data: JSON.stringify(jsonbody),
	 				///data:{act:"listConvoTask",feedId:feedId},
	 				error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
	 				success:function(result){
	 					checkSessionTimeOut(result);
						$('#actFeedTaskDiv_'+feedId).html(prepareConvoTaskListUI(result));
						loadNewAccountLabel('conversationactFeed');
						$('#actFeedTaskDiv_'+feedId).slideDown(600);
		        		$("#loadingBar").hide();
						timerControl("");
					}
				});
			}else{
				$('#actFeedTaskDiv_'+feedId).slideUp(function(){
	     			$(obj).attr('src', 'images/idea/task1.png'); 
	     			$('#actFeedTaskDiv_'+feedId).html('');
				});
			}
	  	}else if (obj == "showConvoTasks"){
	  		$("#loadingBar").show();
			timerControl("start");
			$.ajax({
		    	url: apiPath+"/"+myk+"/v1/getTaskDetails/conversation",
				type:"POST",
				dataType:'json',
				contentType:"application/json",
				data: JSON.stringify(jsonbody),
	 			//data:{act:"listConvoTask",feedId:feedId},
	 			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
	 			success:function(result){
	 				checkSessionTimeOut(result);
					$('#actFeedTaskDiv_'+feedId).html(prepareConvoTaskListUI(result));
					loadNewAccountLabel('conversationactFeed');
					$('#actFeedTaskDiv_'+feedId).slideDown(600);
		       		$("#loadingBar").hide();
					timerControl("");
				}
			});
	  	}else{
			$('#actFeedTaskDiv_'+feedId).slideUp(function(){
	    		$(obj).attr('src', 'images/idea/task1.png'); 
	    		$('#actFeedTaskDiv_'+feedId).html('');
			});
		}
	} */
	
	function prepareConvoTaskListUI(data){
    	var taskUI='';
	    taskUI+="<div id=\"assignedtaskheaders\" class=\"tabContentHeader defaultExceedCls\" style='margin-top:0px;'>";
	    taskUI+=" <div class=\"tabContentSubHeader\">";
		taskUI+="	<div class=\"tabContentHeaderName \" style=\"width: 5%;font-size:14px;\"><span style=\"float: left; height: 20px; margin-left: 5%; width: 95%; overflow: hidden;\" class=\"Type_cLabelText Type_cLabelTitle defaultExceedCls\"></span></div>";
		taskUI+="	<div class=\"tabContentHeaderName \" style=\"width: 5%;font-size:14px;\"><span style=\"float: left; height: 20px; margin-left: 5%; width: 95%; overflow: hidden;\" class=\"defaultExceedCls\">Action</span></div>";
		taskUI+="	<div class=\"tabContentHeaderName Task_Id_cLabelHtml Task_Id_cLabelTitle defaultExceedCls\" style=\"width: 5%;font-size:14px;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 34%;font-size:14px;\"><span style=\"margin-left: 2%; float: left; width: 95%;\" class=\"Task_cLabelText Task_cLabelTitle defaultExceedCls\"></span></div>";
		/*taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls Assigned_to_cLabelHtml Assigned_to_cLabelTitle\" style=\"width:21%;font-size:14px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls Created_by_cLabelHtml Created_by_cLabelTitle\" style=\"width: 13%;font-size:14px;height:20px;overflow:hidden;\"></div>";*/
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls\" style=\"width: 10%;font-size:14px;height:20px;overflow:hidden;\">Start</div>";
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls\" style=\"width: 10%;font-size:14px;height:20px;overflow:hidden;\">Due</div>";
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls\" style=\"width: 8%;font-size:14px;height:20px;overflow:hidden;\" title=\"Estimated Time\">Estimated</div>";
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls\" style=\"width: 8%;font-size:14px;height:20px;overflow:hidden;\" title=\"Actual Time\">Actual</div>";
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls Status_cLabelHtml Status_cLabelTitle\" style=\"width: 5%;font-size:14px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls Trend_cLabelHtml Trend_cLabelTitle\" style=\"width: 5%;font-size:14px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls\" style=\"width: 5%;font-size:14px;height:20px;overflow:hidden;\">Priority</div>";
		taskUI+=" </div>";
		taskUI+="</div>";
    	var agileUI = '';
    	
   		if(data){
     		var json = eval(data);
     		var assignedTo='';
     		for(var i=0; i<json.length; i++){
     			var trending = json[i].trending;
				var createdByDetail = json[i].createdByDetails;
     			var sentimentBgCol =""; 
				var createdByImg="";
				var userTaskStatus = json[i].user_task_status;
				var statusImg="";
				var userStatus="";
				
				var taskStartDate=json[i].start_date;
		    	var taskEndDate=json[i].end_date;
				if(taskStartDate == "00-00-0000"){
		    	taskStartDate = "-";
			    }else{
			    	 taskStartDate = taskStartDate.replaceAll("-", " ");
			    }
			    if(taskEndDate == "00-00-0000"){
			    	taskEndDate = "-";
			    }else{
			    	taskEndDate = taskEndDate.replaceAll("-", " ");
			    }
				var taskstarttime = json[i].start_time;
				var taskendtime = json[i].end_time;
				if(userTaskStatus=="Created"){
					userStatus="Created";
					statusImg = "images/task_created.svg";
				}else if(userTaskStatus=="Inprogress"){
					userStatus="Inprogress";
					statusImg = "images/task_inprogress.svg";
				}else if(userTaskStatus=="Paused"){
					userStatus="Paused";
					statusImg = "images/task_paused.svg";
				}else if(userTaskStatus=="Completed"){
					userStatus="Completed";
					statusImg = "images/task_completed.svg";
				}else{
					statusImg = "images/task_nostatus.svg";
				}
				taskSubject = json[i].task_name;	
		 		taskSubject = replaceSpecialCharacter(taskSubject);		 		
		 		taskSubject= unicodeTonotificationValue(taskSubject);	

				var estHour = json[i].total_estimated_hour ==null ? "0" :json[i].total_estimated_hour;
				var actualHour=json[i].act_hour ==null ? "0" :json[i].act_hour;
				var estMinute=json[i].total_estimated_minute ==null ? "0" :json[i].total_estimated_minute;
				var actualMinute=json[i].act_minute ==null ? "0" :json[i].act_minute;
				if(actualHour == "" || actualHour == "-"){
					 actualHour="0";
				}
				if(actualMinute == "" || actualMinute == "-"){
					 actualMinute="0";
				}
				if(estHour == "" || estHour == "-"){
					 actualMinute="0";
				}
				if(estMinute == "" || estMinute == ""){
					 actualMinute="0";
				}
				var totalMinuteAll=0;var totalActualhour=0;var totalActualminute=0;
				var totalEstMinAll=0;var totalEstHr=0;var totalEstMin=0;
					 
				if(json[i].task_type != "eventTask"){
						 
					 totalEstMinAll = (parseInt(estHour)*60)+parseInt(estMinute);
					 totalEstHr = (totalEstMinAll)/(60);
					 totalEstMin = (totalEstMinAll)%60;
					 totalEstHr = Math.trunc(totalEstHr);
					 totalEstMin = Math.trunc(totalEstMin);
	 
					 totalMinuteAll=(parseInt(actualHour)*60)+parseInt(actualMinute);
					 totalActualhour=(totalMinuteAll)/(60);
					 totalActualminute=(totalMinuteAll)%60;
					 totalActualhour = Math.trunc(totalActualhour);
					 totalActualminute = Math.trunc(totalActualminute);
				}

				var taskStatusImageTitle=json[i].display_status;
		    	var taskStatusImage = taskStatusImageTitle == "Completed" ? "images/calender/Completed.png" : taskStatusImageTitle == "InProgress" ? "images/calender/InProgress.png" : taskStatusImageTitle == "Created" ? "images/calender/InProgress.png" : taskStatusImageTitle == "InComplete" ? "images/calender/incompleteDetailPic.png" : taskStatusImageTitle == "NotCompleted" ? "images/calender/NotCompleted.png"  : "images/calender/NotCompleted.png";
				
				var Priority=json[i].priority;
				var Priority_image = (Priority == "")? "" : Priority == "1" ? "/images/idea/VeryImportant.png" : Priority == "2" ? "/images/idea/Important.png":Priority == "3" ? "/images/idea/medium.png" :Priority == "4" ? "/images/idea/Low.png": "/images/idea/VeryLow.png";
				var titled = Priority_image == "" ? "": Priority_image.indexOf("idea/VeryImportant.png") != -1 ? "Very important" : Priority_image.indexOf("idea/Important.png") != -1 ?"Important": Priority_image.indexOf("idea/medium.png") != -1 ? "Medium": Priority_image.indexOf("idea/Low.png") != -1 ? "Low": Priority_image.indexOf("idea/VeryLow.png") != -1 ? "Very low": Priority_image.indexOf("idea/none.png") != -1? "None" :"";
			


				taskUI+="<div id=\"task_"+json[i].task_id+"\" style=\"width:100%;float:left;\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"newUicreateTaskUI('update',"+json[i].task_id+",'myTasks',"+json[i].source_id+",'Present','Conversation Task','','','convoTask');\">";//newUicreateTaskUI('update','26805','myTasks','43801','Present','Conversation Task')  //viewTask("+json[i].taskId+",'myTasks', "+json[i].feedId+", 'taskFromConvo')
		 		taskUI+="<div class=\"docListViewBorderCls taskId_"+json[i].task_id+"\" style=\"padding-top: 10px;height: 82px;\">";
			 	
			/********************************************************************  First Row columns ********************************************************************/
				taskUI+="<div style=\"\">";
					taskUI+="<div class=\"tabContentHeaderName\" align=\"center\" style=\"width:5%;margin-top: 5px;float: left;\"><div style=\"float:left;margin:-4px 0 0 2px;\"><img title = \"Conversation Task\" class=\"taskimgClss\" style = \"height: 100%;width: 100%\" src=\"images/calender/conTask.png\" /></div></div>";
				 	if(userTaskStatus != ""){
						taskUI+="<div  class=\"tabContentHeaderName\" align=\"center\" style=\"margin-top: 3px;width:5%;padding-left: 3px;\"> <img title=\"\" id=\"taskPlay_"+json[i].task_id+"\" onclick=\"changeStatusOfUserTask("+json[i].task_id+","+json[i].project_id+",'listView');event.stopPropagation();\" src=\""+statusImg+"\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 15px;margin-top: 1px;\" class=\"img-responsive\"> </div>"; 
					}else if(userTaskStatus == ""){
						taskUI+="<div  class=\"tabContentHeaderName\" align=\"center\" style=\"margin-top: 3px;width:5%;padding-left: 3px;\"> <img title=\"\" id=\"\"  src=\""+statusImg+"\" style = \"width: 20px;margin-top: 1px;\" class=\"img-responsive\"> </div>"; 
					}
					taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" title=\""+json[i].task_id+"\" style=\"width:5%;margin-top: 5px;float: left;\">"+json[i].task_id+"</div>";
	         		taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:70%;margin-top: 5px;float:left;padding-left: 15px;\" title=\""+taskSubject+"\">"+TextLink(taskSubject)+"</div>";
	         		taskUI+="<div class=\"tabContentHeaderName\" align=\"center\" valign=\"middle\" style=\"float:left;margin-top:5px;width:5%;padding-right: 15px;\"><img title=\""+json[i].display_status+"\" style=\"width:20px;\" src=\""+taskStatusImage+"\" /></div>";
		 			taskUI+="<div align=\"center\" style=\"width:4%;padding-top: 0px;padding-left:0px;padding-right:0px;\"  class=\"col-sm-3 col-xs-3 tabContentHeaderName defaultExceedCls\">"
		 			
		 			if(trending >=-1 && trending <=-0.49){
		        		sentimentBgCol='images/tasksNewUI/meterred.png';
		        	}else if(trending >=-0.50 && trending <0){
		        		sentimentBgCol='images/tasksNewUI/meteryellow.png';
		        	}else if(trending == 0 || trending == 0.0){
		        		sentimentBgCol='images/tasksNewUI/meteryellow_center.png';
				    }else if((trending >0 || trending > 0.0)&& trending <=0.49){
		        		sentimentBgCol='images/tasksNewUI/meteryellow_Lightgreen.png';
		        	}else if(trending >=0.5 && trending <=1){
		        		sentimentBgCol='images/tasksNewUI/meter_fullgreen.png';
		        	}else{
		        		trending = 'No Data';
		        		sentimentBgCol='images/tasksNewUI/meterDefault.png';  
		        	}
			 		taskUI += "<img title=\""+trending+"\"  src=\""+sentimentBgCol+"\" style=\"width:30px;height:30px;\">";
					taskUI += "</div>";
					
					taskUI+="<div  class=\"tabContentHeaderName\" align=\"center\" style=\"width:6%;padding-top: 6px;padding-left:0px;padding-right:0px;\"> <img title=\""+titled+"\" src=\""+Priority_image+"\" style = \"\" class=\"img-responsive\"></div>";
			
				taskUI+="</div>";
				
			/********************************************************************  Second Row columns ********************************************************************/			   
				taskUI+="<div style=\"\">";
					
					taskUI +="<div style=\"width: 7%;float: left;padding-left: 5px;color:darkgrey;font-size: 14px;padding-top: 6px;\"><span style=\"\">Created</span></div>";
					taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" style=\"float:left;width:3%;margin-left: -15px;padding-top: 2px;\">";
	         		createdByImg = createdByDetail;
			 		if(createdByImg==""){
				    		taskUI+="<div style=\"float:left;margin-left: 12.5%; padding-top: 1.8%;\">-</div>"
				    }else{
					   	for(var j=0; j<createdByImg.length; j++){
						    userImg = createdByImg[j].imagePathUrl+'?'+d.getTime();
						    taskUI+='<img class="img-responsive" style="float:left;margin-right:5px;width:25px;height:25px;border-radius:50%;" src="'+userImg+'" title="'+createdByImg[j].userName+'" onerror="javascript:userImageOnErrorReplace(this);">';
						}
				    }
		 			taskUI+="</div>";
		 			
					taskUI +="<div style=\"width: 5%;float: left;padding-left: 16px;color:darkgrey;font-size: 14px;text-align: right;padding-top: 6px;\"><span style=\"\">Assigned</span></div>";
			       	taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" style=\"float:left;width:35%;padding-left: 30px;padding-top: 2px;\">";
					assignedTo = json[i].assignedIdDetails;
			 		if(assignedTo==""){
				    	taskUI+="<div style=\"float:left;margin-left: 12.5%; padding-top: 1.8%;\">-</div>"
				    }else{
					   	for(var j=0; j<assignedTo.length; j++){
						    userImg = assignedTo[j].imagePathUrl+'?'+d.getTime();
						    taskUI+='<img class="img-responsive" style="float:left;margin-right:5px;width:25px;height:25px;border-radius:50%;" src="'+userImg+'" title="'+assignedTo[j].userName+'" onerror="javascript:userImageOnErrorReplace(this);">';
						}
				    }
	         		taskUI+="</div>";
					
					taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:10%;float:left;padding-top: 6px;color: darkgrey;font-size: 14px\" title=\""+taskStartDate+" "+taskstarttime+"\"><div>"+taskStartDate+"</div></div>";
					taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:10%;float:left;padding-top: 6px;color: darkgrey;font-size: 14px\" title=\""+taskEndDate+" "+taskendtime+"\"><div>"+taskEndDate+"</div></div>";
				
					taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:8%;float:left;padding-top: 6px;padding-left: 10px;color: darkgrey;font-size: 14px\"><div>"+totalEstHr+" h "+totalEstMin+" m</div></div>";
			 		taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:8%;float:left;padding-top: 6px;color: darkgrey;font-size: 14px\"><div>"+totalActualhour+" h "+totalActualminute+" m</div></div>";
					
					taskUI+="<div align=\"right\" class=\"cmntsIcon\" style=\"display:none;width: 15%;float: left;padding-right: 23px;padding-top: 6px;\"><img id=\"cIcon_"+json[i].task_id+"\" style=\"cursor:pointer;width:20px;display:none;\" title=\""+getValues(companyLabels,"View_comments")+"\" src=\"images/commentBlack2.png\" onclick=\"showTaskComments("+json[i].task_id+","+json[i].project_id+");event.stopPropagation();\"></div>";
					
				taskUI+="</div>";
				
				taskUI+="</div>";
				
				taskUI+="<div id=\"Taskcomment_"+json[i].task_id+"\" class=\"epicCommonListDivCss\" style=\'padding-top: 20px;padding-right: 20px;width: 100%;margin-left: 0px;padding-left: 20px;float: left;margin-bottom: 0px;\'>"
 	            +"<div class=\"TaskCommentMainTextareaDiv\" style=\"float:left;width:100%;padding-right: 20px;\">"
 	            +"</div>"
 	            +"<div class=\"TaskCommentListDiv\" style=\"float:left;width:100%;border-top: 1px solid #c1c5c8;\">"
 	            +"</div>"
     	        +"</div>"
						
				+"<div id=\"TaskStatus_"+json[i].task_id+"\" class=\"epicCommonListDivCss TaskCommentContainer\" onclick=\"event.stopPropagation();\" style=\"padding: 5px 14px 0px;float: left;width: 100%;margin-left: 0px;margin-right: 0px;margin-bottom: 0px;border-radius: 0px;display: none;overflow: hidden;\">"
				+"<div class=\"TaskStatusMainTextareaDiv\" style=\"float:left;width:100%;padding-bottom: 5px;border-bottom: 1px solid rgb(193, 197, 200);\">"
				+"</div>"
			    +"</div>";
			
				
				taskUI+="</div>";
     		}
    	}
   		return taskUI;
	}
	
	
	
	function reloadConvoUI(feedId, result){
		reloadDelTaskIdForConvo = feedId;
		$('img#tIcon_'+feedId+'').remove();
		var imgSrc =  "images/idea/task1Toggle.png";
		//if($('img#tIcon_'+feedId+'').length <= 1){
 			var taskImgUI = "<div class = \"tasksIcon\" style=\"width: 25px;float: right;margin-top: 10px;margin-right: 25px;position: absolute;right: 0px;\">"
		        				+"<img id=\"tIcon_"+feedId+"\" style=\"cursor:pointer;\" title=\"View Tasks\" src=\"images/task1Toggle.png\" onclick=\"showConversationTasks(this, "+feedId+")\">"
		     				+"</div>"
		    $('#subcommentDiv_'+feedId).after(taskImgUI);
 		/*}else{
 			$('img#taskIcon_'+feedId+'').attr('src',imgSrc);
 		}*/
 		showConversationTasks("showConvoTasks",feedId);
	}
	
	var flagReply = true;
	var replyFunction = '';
	 function replyVoiceFeedold(obj , feedId){
		 //alert("inside replyVoiceFeed");
		 //var voiceMenuType = voiceType;
		// alert("replyVoiceFeed voiceType::"+voiceType);
		// alert("feedId--:"+feedId);
	 	   //alert("Reply Voice Conversation");
	      	if(flagReply){
	      		
	      	    Fr.voice.record(false, function(){
	      	    	
	      	    },"subreply", feedId);
	      	    $('div.timerSpan').val('');
	      	   	$('div.timer').show();
	      	   
	      	}else{
	 
	      		Fr.voice.pause();
	      		
	    	    $('#replyBlock_'+feedId+'').hide();
			    $('#audioAreaSub_'+feedId+'').show();
			    $('div.timer').hide();
			
			    var $obj = $(obj).parent().prev().children().eq(1);
			        replyFunction = $obj.attr('onclick');
			        if(voiceType=='activityfeed' || voiceType=='wsDocument' || voiceType=='Task' || voiceType=='wsIdea' || voiceType=='agile' || voiceType=='blog'){
			        	if(scrumProjId==""){
		 			    	projId=projectId;		 			    	
		 			    }else{
		 			    	projId=scrumProjId;		 			    	
		 			    }
			        	// projId=projectId;
			        	$obj.attr('onclick',"CommentVoiceFeed('"+projId+"','voiceFeed',"+feedId+",'"+voiceType+"')"); 
			        	
		 			   }else{
		 				   
		 				$obj.attr('onclick',"CommentVoiceFeed('','voiceFeed',"+feedId+",'"+voiceType+"')");
		 				
		 			   }

			        //$obj.attr('onclick',"CommentVoiceFeed('','voiceFeed',"+feedId+",'"+voiceType+"')"); 
			        //$obj.attr('onclick',"CommentVoiceFeed(${projId},'voiceFeed',"+feedId+",'activityfeed')");
			        //$obj.attr('onclick',"CommentVoiceFeed(${projId},'voiceFeed',"+feedId+",'wsDocument')");
			        //$obj.attr('onclick',"CommentVoiceFeed(${projId},'voiceFeed',"+feedId+",'agile')"); 
			        //$obj.attr('onclick',"CommentVoiceFeed(${projId},'voiceFeed',"+feedId+",'wsIdea')");
			        //$obj.attr('onclick',"CommentVoiceFeed('','voiceFeed',"+feedId+",'blog')"); 
			        //$obj.attr('onclick',"CommentVoiceFeed('','voiceFeed',"+feedId+",'document')");
			  
	      		  Fr.voice.export(function(url){
	      		
	      			    Fr.voice.pause();
	        	        $("#audioSub_"+feedId+"").attr("src", url);
	        	       // $("#audioSub_"+feedId+"")[0].play();
	        	          
	        	        $('#convoPost').data('url',url);
	        	        
	        	        $('div.recAudioMsg').css('box-shadow','none'); 
	        	         flagReply = true;
	    	      		 clearTimeout(timeOut);	
	        	          
	        	      }, "URL");
	      
	      	}  
	      	  
} 
	

	 
	 function Timer(){  // time calculation to show the time at recording time
	       
			var endTime = new Date();
			var diff = endTime - startTime;
			    diff /= 1000;

				let time=$('div.timer').find('div.timerSpan').text();
				
				let minute=time.substring(0,2);
				let second=time.substring(5,7);
				minute = parseInt(minute)
				second = parseInt(second)
				//console.log(second)
				second=second+1;
				if(second>59){
					second=0;
					minute=minute+1;
				}
				

			var sec = Math.round(diff % 60);
			    diff = Math.floor( diff / 60 );
			
			var min = Math.round(diff % 60);    
			    diff = Math.floor( diff / 60 );
		
				minute = parseInt(minute) < 10 ? "0" + minute : minute;
				second = parseInt(second) < 10 ? "0" + second : second;   
			$('div.timer').find('div.timerSpan').text(minute+"m  "+second+"s"); // we are showing timer for every feed
			    //console.log(sec);
				
			timeOut = setTimeout(Timer,1000);
		}


   function VoiceRecordingConversationNew(voiceFlagNew,place,feedId,from){
    
      //$('#recAudioMsg').click(function(){

          if(voiceFlagNew==true){ // start recoding means recording not started yet
			
			if(from!='onplaypause'){
				if (place =='main'){
					Fr.voice.record(false, function(){
					}, "post", "0");
					
					$(".voiceFeed").attr('onclick',"VoiceRecordingConversationNew(false,'main',0)");
					$(".voiceFeed").css('background-color','#CFE6F2')
			  }else{
					Fr.voice.record(false, function(){
					}, "reply",feedId);
					
					$(".audioreply").attr('onclick',"VoiceRecordingConversationNew(false,'reply',"+feedId+")");
					$(".audioreply").css('background-color','#CFE6F2')
			  }
			}else if(typeof(from)!='undefined'&&from=='onplaypause'){
				Timer();
				$(".playI").find('img').attr('src','images/conversation/pause.svg');
				$(".playI").attr('title','Pause recording');
				if(place=='main'){
					Fr.voice.resume(false, function(){
					}, "post", "0");
					$(".playI").attr('onclick',"VoiceRecordingConversationNew(false,'main',0,'onplaypause')")
		
				}else{
					Fr.voice.resume(false, function(){
					}, "reply",feedId);
					$(".playI").attr('onclick',"VoiceRecordingConversationNew(false,'reply',"+feedId+",'onplaypause')")
		
				}
				
			}
			
			  
          }else{ // already recoding started now pause the recoding and preview
              Fr.voice.pause();
			 
			  
			  //For main level comment
			if(from!='onplaypause'){
				if(place=='main'){
					$(".voiceFeed").attr('onclick',"VoiceRecordingConversationNew(true,'main',0)");
					  $(".voiceFeed").css('background-color','')
					  $('#main_commentTextarea').hide();
					  $('.stopI').hide();
					  $('.playI').hide();
					  $('#audioArea').removeClass('d-none').addClass("d-flex");
					  $(".timertime").text($('.timer').text());
					  $('.timer').removeClass('d-flex').addClass("d-none");
					  //$('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed('"+prjid+"','voiceFeed',0,'activityfeed')");
					  $('.convopost').attr('onclick',"postAudio('"+prjid+"',0,'"+globalmenu+"',0)").attr('src','images/conversation/post.svg');
			   } else{
					  $(".audioreply").attr('onclick',"VoiceRecordingConversationNew(true,'reply',"+feedId+")");
					  $(".audioreply").css('background-color','')
					  $('.stopI').hide();
					  $('.playI').hide();
					  //$('#main_commentTextarea').hide();
					  $('#audioArea').removeClass('d-none').addClass("d-flex");
					  $(".timertime").text($('.timer').text());
					  $('.timer').removeClass('d-flex').addClass("d-none");
					  //$('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed('"+prjid+"','voiceFeed',0,'activityfeed')");
					  $('.postButton').attr('onclick',"postAudio('"+prjid+"',0,'"+globalmenu+"',"+feedId+")").attr('src','images/conversation/post.svg');;
					  clearTimeout(timeOut);	
			   }
  
  
  
				  Fr.voice.export(function(url){ // calling this method(Fr.voice.js) To get the Url of recoded voice
				
						Fr.voice.pause();
					  $("#audio").attr("src", url);  // setting the url to the audio player.
					 // $("#audio")[0].play();
						
					  $('#convoPost').data('url',url); // saving the URl of the blob data.
					  $('#recAudioMsg').css('box-shadow','none'); // reomoving the box shadow(red shadow around the button)
						 voiceFlag = true; // To indicate that voice recording have to start.
						 clearTimeout(timeOut);	 // clearing the timer call.
						
					}, "URL");
			}else if(typeof(from)!="undefined" && from=='onplaypause'){
				
				clearTimeout(timeOut);	
				$(".playI").find('img').attr('src','images/conversation/play.svg')
				$(".playI").attr('title','Resume recording');
				if(place=='main'){
					$(".playI").attr('onclick',"VoiceRecordingConversationNew(true,'main',0,'onplaypause')")
				}else{
					$(".playI").attr('onclick',"VoiceRecordingConversationNew(true,'reply',"+feedId+",'onplaypause')")
				}
			}
              
          /* 	  $('#recAudioMsg').css('box-shadow','none'); 
                voiceFlag = true;
                window.clearTimeOut(timeOut); */
                
          
          }  
            
      // });  
}  

	
	//  function VoiceRecording(){
	// 	 if(voiceType=="wsIdea"){
	// 		 //alert("voiceType:::"+voiceType);
	// 		 setInterval(function(){				
	// 		      $('#recAudioMsg').click(function(){				    	
	// 			      	if(voiceFlag){ // start recoding means recording not started yet		      		
	// 			      	    Fr.voice.record(false, function(){
	// 			      	    }, "post", "0");
				     	   
	// 			      	}else{ // already recoding started now pause the recoding and preview				      		
	// 			      		Fr.voice.pause();
	// 			    	    $('#main_commentTextarea').hide();
	// 		 			    $('#audioArea').show();
	// 		 			    $('div.timer').hide();			 			    
	// 		 			    projId=projectId;			 			    
	// 		 			    $('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed('"+projId+"','voiceFeed',0,'"+voiceType+"')");
	// 			      		  Fr.voice.export(function(url){ // calling this method(Fr.voice.js) To get the Url of recoded voice
				      		
	// 			      			    Fr.voice.pause();
	// 			        	        $("#audio").attr("src", url);  // setting the url to the audio player.
	// 			        	       // $("#audio")[0].play();
				        	          
	// 			        	        $('#convoPost').data('url',url); // saving the URl of the blob data.
	// 			        	        $('#recAudioMsg').css('box-shadow','none'); // reomoving the box shadow(red shadow around the button)
	// 			    	      		 voiceFlag = true; // To indicate that voice recording have to start.
	// 			    	      		 clearTimeout(timeOut);	 // clearing the timer call.
				        	          
	// 			        	      }, "URL"); 
				      		
	// 			      	 	 /* $('#recAudioMsg').css('box-shadow','none'); 
	// 			      		  voiceFlag = true;
	// 			      		  window.clearTimeOut(timeOut); */				      		  				      	
	// 			      	}  
				      	  
	// 			       });  
	// 		    }, 10000);
	// 	 }else{
	// 	      $('#recAudioMsg').click(function(){			    	 		    	   
	// 		      	if(voiceFlag){ // start recoding means recording not started yet
	// 		      	    Fr.voice.record(false, function(){
	// 		      	    }, "post", "0");

			     	   
	// 		      	}else{ // already recoding started now pause the recoding and preview
			      		
	// 		      		Fr.voice.pause();
	// 		    	    $('#main_commentTextarea').hide();
	// 	 			    $('#audioArea').show();
	// 	 			    $('div.timer').hide();
	// 	 			   // alert("voiceType:::"+voiceType);
	// 	 			   if(voiceType=='activityfeed' || voiceType=='wsDocument' || voiceType=='Task' || voiceType=='wsIdea' || voiceType=='agile' || voiceType=='blog'){
	// 	 				   if(scrumProjId==""){
	// 		 			    	projId=projectId;
	// 		 			    }else{
	// 		 			    	projId=scrumProjId;	
	// 		 			    }
			 			    
			 			   
	// 	 				 // projId=projectId;
	// 	 				  //alert("inside voice");
	// 	 				 //alert("inside voice projectId::::"+projId);
	// 	 				  $('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed('"+projId+"','voiceFeed',0,'"+voiceType+"')");
	// 	 			   }else{
	// 	 				  $('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed('','voiceFeed',0, '"+voiceType+"')");
	// 	 			   }
		 			    
	// 		      		  Fr.voice.export(function(url){ // calling this method(Fr.voice.js) To get the Url of recoded voice
			      		
	// 		      			    Fr.voice.pause();
	// 		        	        $("#audio").attr("src", url);  // setting the url to the audio player.
	// 		        	       // $("#audio")[0].play();
			        	          
	// 		        	        $('#convoPost').data('url',url); // saving the URl of the blob data.
	// 		        	        $('#recAudioMsg').css('box-shadow','none'); // reomoving the box shadow(red shadow around the button)
	// 		    	      		 voiceFlag = true; // To indicate that voice recording have to start.
	// 		    	      		 clearTimeout(timeOut);	 // clearing the timer call.
			        	          
	// 		        	      }, "URL");
			      		
	// 		      	 	  /*$('#recAudioMsg').css('box-shadow','none'); 
	// 		      		  voiceFlag = true;
	// 		      		  window.clearTimeOut(timeOut);*/ 
			      		  
			      	
	// 		      	}  
			      	  
	// 		       }); 
	// 	 }
	    
	// }
	 
	 
	 
/*	 function VoiceRecording(){
		//alert("inside 1");
		//var voiceMenuType= voiceType;
		//voiceMenuType=voiceType;
		//alert("voiceType>>>>::::"+voiceType);
	      $('#recAudioMsg').click(function(){
	    	 
	    	   
	      	if(voiceFlag){ // start recoding means recording not started yet
	      		
	      	    Fr.voice.record(false, function(){
	      	    }, "post", "0");

	     	   
	      	}else{ // already recoding started now pause the recoding and preview
	      		
	      		Fr.voice.pause();
	    	    $('#main_commentTextarea').hide();
 			    $('#audioArea').show();
 			    $('div.timer').hide();
 			   if(voiceType=='activityfeed' || voiceType=='wsDocument' || voiceType=='wsIdea' || voiceType=='agile'){
 				  projId=projectId;
 				  //alert("inside voice");
 				 //alert("inside voice projectId::::"+projId);
 				  $('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed("+projId+",'voiceFeed',0,'"+voiceType+"')");
 			   }else{
 				  $('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed('','voiceFeed',0, '"+voiceType+"')");
 			   }
 			    
 			   // $('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed('','voiceFeed',0, '"+voiceType+"')");
 			    //$('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed('','voiceFeed',0,'document')");
 			    //$('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed(${projId},'voiceFeed',0,'wsDocument')");
 			    //$('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed('','voiceFeed',0,'blog')");
 			    //$('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed(${projId},'voiceFeed',0,'agile')");
 			    //$obj.attr('onclick',"CommentVoiceFeed(${projId},'voiceFeed',"+feedId+",'wsIdea')");
 			    //$('#convoPost').children().eq(1).attr('onclick',"CommentVoiceFeed(${projId},'voiceFeed',0,'activityfeed')");
 			    
	      		  Fr.voice.export(function(url){ // calling this method(Fr.voice.js) To get the Url of recoded voice
	      		
	      			    Fr.voice.pause();
	        	        $("#audio").attr("src", url);  // setting the url to the audio player.
	        	       // $("#audio")[0].play();
	        	          
	        	        $('#convoPost').data('url',url); // saving the URl of the blob data.
	        	        $('#recAudioMsg').css('box-shadow','none'); // reomoving the box shadow(red shadow around the button)
	    	      		 voiceFlag = true; // To indicate that voice recording have to start.
	    	      		 clearTimeout(timeOut);	 // clearing the timer call.
	        	          
	        	      }, "URL");*/
	      		
	      	 	  /*$('#recAudioMsg').css('box-shadow','none'); 
	      		  voiceFlag = true;
	      		  window.clearTimeOut(timeOut);*/ 
	      		  
	      	
	      	//}  
	      	  
	       //});  
	//}
	 
	 function updateTimeSub(feedId) { // used for subfeeds
	  	  //alert(feedId);
	  	  var  duration = $("#audio_"+feedId)[0].duration; // To get the length of the audio clip
	  	  var playPercent = 100 * ($("#audio_"+feedId)[0].currentTime / duration);
	  	  $('#playhead_'+feedId).css('marginLeft', playPercent + "%");
	     }
	    
	    function onVoiceEndedSub(feedId){
	    	
		     $("#audio_"+feedId)[0].currentTime = 0;
		     $('#customPlayer_'+feedId).attr('src','images/conversation/play.svg');
		     $('#pauseaudio_'+feedId).attr('src','images/conversation/pause.svg');
		     //console.log("ended");
		     
		}

		async function playAudio(obj, feedId, type){
			//	console.log("type-------"+type);
				
				
				if(type == "audio"){
				
					/**
					   This function call is to make audio pointer movable by the users , User can move the pointer upto desirable point
					   we are sending Id according to the senario because in voice activityfeed two players will be there one two listen
					   alredy recorded and one to preview the reply voice data if there is any
					**/
					//stateChangeForActFeeds( 'timeline_'+feedId+'', 'playhead_'+feedId+'', 'audio_'+feedId+''); 
					
					  if($(obj).attr('src').indexOf('play.svg') > 0){  // if play button is showing
						 $(obj).attr('src','images/conversation/pause.svg'); // while playing button will change to pause
						
						 await $('#audio_'+feedId)[0].play(); // this line will play the audio
						 
					  }	
					else{
						 $(obj).attr('src','/images/conversation/play.svg');
						 await $("#audio_"+feedId)[0].pause(); 
					 }
					
				}else if(type == "preview"){
					
					if($(obj).attr('src').indexOf('play.svg') > 0){
						 $(obj).attr('src','images/conversation/pause.svg');
						 $('#audioSub_'+feedId)[0].play();
						 
					  }	
					else{
						 $(obj).attr('src','/images/conversation/play.svg');
						 $("#audioSub_"+feedId)[0].pause(); 
					 }
			
				}
				
				
			}  
	    
	   
			function playAudiooldd(obj, feedId, type){
				if(type == "audio"){
					/*
					   This function call is to make audio pointer movable by the users , User can move the pointer upto desirable point
					   we are sending Id according to the senario because in voice activityfeed two players will be there one two listen
					   alredy recorded and one to preview the reply voice data if there is any
					*/
					//stateChangeForActFeeds( 'timeline_'+feedId+'', 'playhead_'+feedId+'', 'audio_'+feedId+''); 
					
					if($(obj).attr('src').indexOf('play.svg') > 0){  // if play button is showing
						 $(obj).attr('src','images/conversation/pause.svg'); // while playing button will change to pause
						 $('#audio_'+feedId)[0].play(); // this line will play the audio
						 
					  }	
					else{
						 $(obj).attr('src','/images/conversation/play.svg');
						 $("#audio_"+feedId)[0].pause(); 
					 }
				}else if(type == "preview"){
					if($(obj).attr('src').indexOf('play.png') > 0){
						 $(obj).attr('src','images/conversation/pause.svg');
						 $('#audioSub_'+feedId)[0].play();
						 
					  }	
					else{
						 $(obj).attr('src','/images/conversation/play.svg');
						 $("#audioSub_"+feedId)[0].pause(); 
					 }
				}
			}
			/* async function pauseAudio(obj, feedId, type){
				if(type == "audio"){
	        		if($(obj).attr('src').indexOf('pause.svg') > 0){  
	    	    		 $('#pauseaudio_'+feedId).attr('src','images/bars.png');
	    	    		 await $('#audio_'+feedId)[0].play(); 
	    	    	}	
	    	    	else{
	    	    		 $('#pauseaudio_'+feedId).attr('src','images/conversation/pause.svg');
	    	    		 $("#audio_"+feedId)[0].pause(); 
	    	    	} 
				}
			} */
	
	    function cancelVoiceSub(feedId){
			
		 	 // alert(replyFunction);
	    	  $("#audio_"+feedId+"").attr("src", '');
	    	  $("#audioSub_"+feedId+"").attr("src", '');
	    	  $('.actFeedMore').parent().prev().children().eq(3).attr('onclick',replyFunction);
	    	  $('#audioContainerSub_'+feedId+' , div.timer').hide();
	          $('#replyBlock_'+feedId+'').show(); 
	          $('div.timer').find('div.timerSpan').text(''); // clear the timer
			
		}
	    
	    function updateTimeSubReply(feedId) {
	     	 // alert(feedId);
	     	  var duration = $("#audioSub_"+feedId)[0].duration;
	     	  var playPercent = 100 * ($("#audioSub_"+feedId)[0].currentTime / duration);
	     	  $('#playheadSub_'+feedId).css('marginLeft', playPercent + "%");
	        }
	    
	    function stateChangeForActFeeds(event , timelineId , playheadId , audioId){
	    	
	    	//$("#"+timelineId).click( function () {
	    		
	     	   var  duration = $("#"+audioId)[0].duration;  //get the duration of the Audio clip
	     	   moveplayhead_sub(event, timelineId, playheadId);
	         
	           $("#"+audioId)[0].currentTime = duration * clickPercent_sub(event , timelineId);
	  	
	    }
	    function clickPercent_sub(e , timelineId) {
        	return (e.pageX - $("#"+timelineId).offset().left) / 250 ; // 400 is the width of the time line
          }
	    
	    function onVoiceEndedSubReply(feedId){
	   		//alert(feedId);
	   	     $("#audioSub_"+feedId)[0].currentTime = 0;
	   	     $('#customPlayerSub_'+feedId).attr('src','/images/conversation/play.svg');
	   	     //console.log("ended");
	   	       
	   	}
	    
	    function CommentVoiceFeed(projId,type,feedId,postMenuType){	 
		    $("#loadingBar").show();
		    //timerControl("start");
		
		    var blob = $('#convoPost').data('url');
		    
		    sendingBlobDataToBackend(projId, blob, feedId,postMenuType);
		    
	}
	    function sendingBlobDataToBackend(projId, blob, feedId,postMenuType){ // sending the blob data to server	    	
	    	$('#loadingBar').addClass('d-flex');
			var mainFeedId="";
	    	var localOffsetTime=getTimeOffset(new Date());
	    	mainFeedId = feedId;
			feedaction = "fetchinsertFeed";
			feedplace = "audioupload";
			
			if(typeof(myTaskId)=="undefined"){
				myTaskId="";
			}
			if(typeof(pgId)!="undefined" || pgId != ""){
				myTaskId=pgId;
			}
			if(typeof(projId) == "undefined" || projId == ""){
				projId=prjid;
			}
			if(typeof(postMenuType)=="undefined"){
				postMenuType="";
			}
			if(postMenuType=='agile'){
				//alert("inside sendingBlobDataToBackend --Agile--");	        	
				//feedId= pgId;
				projId=prjid;
				myTaskId = pgId;	
									
			}
			//alert("myTaskId-->"+myTaskId);
			Fr.voice.export(function(blob){
		    	
		        var formData = new FormData();
		        formData.append('file', blob);
				formData.append('user_id', userIdglb);
				formData.append('company_id', companyIdglb);
				formData.append('resourceId', feedId);
				formData.append('projId', projId);
				formData.append('place', "convodoc");
				formData.append('convotype', postMenuType);
				formData.append('menuTypeId', myTaskId);
				formData.append('subMenuType', commentPlace);
		        if(postMenuType=='blog'){
		        	//alert("inside sendingBlobDataToBackend --Blog--");
		        	projId=projectId;
		        	//feedId=pgId;
		        	menutypeId=pgId;
		        	//alert("Blog pgId--Blog:::"+menutypeId);
		        }else if(postMenuType=='wsDocument'){		        		        	
		        	menutypeId= pgId;
		        	projId=projectId;
		        	
		        }else if(postMenuType=='agile'){
		        	//alert("inside sendingBlobDataToBackend --Agile--");	        	
		        	//feedId= pgId;
		        	projId=projectId;
		        	menutypeId = pgId;	
		        			        	
		        }else if(postMenuType=='activityfeed'){
		        	menutypeId = "";
		        }else if(postMenuType=='document'){
		        	menutypeId= pgId;		        	
		        	projId=projectId;		        	
		        }else if(postMenuType=='wsIdea'){		        	
		        	menutypeId= pgId;		        				    
				    projId=ideaProjId;
				    				   		      	
		        }else if(postMenuType=='Task'){		        	
		        	menutypeId= pgId;		        				    
		        	projId=taskprojId;
				    				   		      	
		        }else if(postMenuType=='galleryComment'){
		        	menutypeId= pgId;
		        	//alert("Gallery:::"+menutypeId);
		        	 projId=projectId;
				     //alert("gallery projId:::"+projId);
		        }
		       
		       // alert("menutypeId----->>"+menutypeId);
		        $.ajax({
		          ///url: path+'/DummyFileUpload?projId='+projId+'&userId='+userId+'&feedId='+feedId+'&postMenuType='+postMenuType+'&subMenuType='+commentPlace+'&menutypeId='+menutypeId+'&localOffsetTime='+localOffsetTime,
		          url: apiPath+"/"+myk+"/v1/upload", 
				  type: 'POST',
		          //dataType:'text',
				  processData: false,
                  contentType: false,
                  cache: false,
		          data: formData,
		          error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $('#loadingBar').addClass('d-none').removeClass('d-flex');
							//timerControl("");
							},
		          contentType: false,
		          processData: false,
		          success: function(url) {
		        	  	
		          //alert("URL Activityfeed----"+url);
		          
		          /*if(postMenuType=='blog'){
		        	    checkSessionTimeOut(url);		        		
		        		loadBlogPostData(projectId);		        				        		
		        		$('#audioContainer , div.timer').hide(); //hiding audio container
		     	     	$('#main_commentTextarea').show();
		             }else{*/
		            	//alert("Ok done...."+url);
			             // $("div#activityFeedList").html('');
			             $('#loadingBar').addClass('d-none').removeClass('d-flex');
						 //timerControl("");
			        	 
			        	 //url = jQuery.parseJSON(url)
			        	  //alert("Conversation Url---"+url);
						  feedactionid = url;
			        	  //var jsonData = eval(url);
			        	  //alert("jsonData---"+jsonData);
			        	  $('div#audioContainer , div.timer').hide();
			        	  $('#main_commentTextarea').show();
			        	  $('textarea#main_commentTextarea').val('');
			        	  $("div#replySubCommentDiv1_"+feedId).hide();
			        	  //alert("postMenuType--1--:::::"+postMenuType);
						  //if(feedId == "0"){
							
						 /*  }else{
							loadActivityFeed('','');  
						} */
						if(postMenuType=='Task'){
							loadfeednew(postMenuType);
							//showTaskComments(myTaskId,prjid);
						}else if(postMenuType=='agile'){
							fetchAgileComments(myTaskId);
						}else{
							loadActivityFeed('', 'firstClick',postMenuType);
						}
			        	  
		            	  
		             //}
		          
		          
		          
		          }
		        });
		      }, "blob");

		}
	
	    function clearMainFeedData(){	    	
			if($('textarea#main_commentTextarea').val() == ''){
				}else{
				$('textarea#main_commentTextarea').val('');
	     // 	parent.confirmFun("<bean:message key='workspace_clear.message'/>","clear","clearActivityFeed");
	        }
	 } 

	function moveplayhead_sub(e, timelineId, playheadId) {
    	
    	var timelineWidth = 250;
    	var newMargLeft = e.pageX - Math.ceil($("#"+timelineId).offset().left);
    //	alert(newMargLeft+"------newMargLeft");
        var $playhead =  $("#"+playheadId);   
    	
    	if (newMargLeft != 0 && newMargLeft != timelineWidth) {
    		$playhead.css('marginLeft', newMargLeft + "px");
    	}
    	if (newMargLeft == 0) {
    		$playhead.css('marginLeft', "0px");
    		//playhead.style.marginLeft = "0px";
    	}
    	if (newMargLeft == timelineWidth) {
    		//playhead.style.marginLeft = timelineWidth + "px";
    		$playhead.css('marginLeft', timelineWidth + "0px");
    	}
    }
	
	    
	function onVoiceEnded(){
		
	     $("#audio")[0].currentTime = 0;
	     $('#customPlayer').attr('src','images/conversation/play.svg');
	     //console.log("ended");
	       
	}
	function updateTime() { // for main feed(To update the time in player)
  	  //alert("update time");
  	  var  duration = $("#audio")[0].duration;  // To get the length of the audio clip
  	  var playPercent = 100 * ($("#audio")[0].currentTime / duration);
  	  $('#playhead').css('marginLeft', playPercent + "%");
  }
	
/* ***************Code Starts For Screeen Sharing********************* */
	
	// (function($) {			
	// 	 // alert("inside onload with copy");
	// 	  var defaults;
	// 	  $.event.fix = (function(originalFix) {
	// 	    return function(event) {
	// 	      event = originalFix.apply(this, arguments);		   
	// 		      if (event.type.indexOf('copy') === 0 || event.type.indexOf('paste') === 0) {
	// 		        event.clipboardData = event.originalEvent.clipboardData;
	// 		      }
	// 	        return event;
	// 	      };
	// 	  })($.event.fix);
	// 	  defaults = {
	// 	    callback: $.noop,
	// 	    matchType: /image.*/
	// 	  };
		  
	// 	  return $.fn.pasteImageReader = function(options) {
	// 	    if (typeof options === "function") {		    	
	// 	    //alert("inside typeof");
	// 	      options = {
	// 	        callback: options
	// 	      };
	// 	    }
	// 	    options = $.extend({}, defaults, options);
	// 	    return this.each(function() {
	// 		      var $this, element;
	// 		      element = this;
	// 		      $this = $(this);
	// 	      return $this.bind('paste', function(event) {
	// 		        var clipboardData, found;
	// 		        found = false;
	// 		        clipboardData = event.clipboardData;
	// 	       return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
	// 		          var file, reader;
	// 		          if (found) {
	// 		            return;
	// 		          }
	// 	          if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
	// 		            file = clipboardData.items[i].getAsFile();
	// 		            reader = new FileReader();
	// 	           		reader.onload = function(evt) {
	// 	              return options.callback.call(element, {
	// 		                dataURL: evt.target.result,
	// 		                event: evt,
	// 		                file: file,
	// 		                name: file.name
	// 		              });
	// 	            };
	// 	            reader.readAsDataURL(file);
	// 	            //snapshoot();
	// 	            return found = true;
	// 	          }
	// 	        });
	// 	      });
	// 	    });
	// 	  };
	// 	})(jQuery);


	var dataURL, filename, fileShare, imageShre;
		// $("html").pasteImageReader(function(results) {
		//  // alert("imgfeedId:::::::"+imgfeedId);
		// 	  filename = results.filename;
		// 	  dataURL = results.dataURL;			
			  
		// 	  fileShare = dataURItoBlob(dataURL);
		// 	 // alert("fileShare::::::::"+fileShare);
		// 	  imageShre=fileShare;
		// 	  $('#rowTab').find('.convopost').attr('src','/images/conversation/post.svg');
			  		 
		// 	  var img = document.createElement('img');			
		// 	  img.src= dataURL;
		// 	  var w = img.width;
		// 	  var h = img.height;
		// 	  $width.val(w)
		// 	  $height.val(h);
		//   //return $(".active").css({
			
		// 	  if(globalmenu=='Idea' || globalmenu=='Task' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint'){
		// 		imgfeedId=parseInt(imgfeedId);  
		// 		return $("#replyBlock_"+imgfeedId).css("background-image", "url(" + dataURL + ")").data({'width':w, 'height':h});
		// 	  }else if(imgfeedId==""){
		// 		  return $("#main_commentTextarea").css({
		// 			    backgroundImage: "url(" + dataURL + ")"
		// 			  }).data({'width':w, 'height':h});	
		// 	  }else{
		// 		  return $("#replyBlock_"+imgfeedId).css({
		// 		    backgroundImage: "url(" + dataURL + ")"
		// 		  }).data({'width':w, 'height':h});
		// 	}
		// });



		function onPasteScreenShot(event, obj){
			console.log('testing clipboard---->');
			const items = (event.clipboardData || event.originalEvent.clipboardData).items;
			let blob = null;
		
			for (const item of items) {
			  if (item.type.indexOf('image') === 0) {
				blob = item.getAsFile();
			  }
			}
		
			// load image if there is a pasted image
			if (blob !== null) {
			  imageShre = blob;
			  
			  $(obj).parents('#rowTab').find('.convopost').attr('src','/images/conversation/post.svg');
			  //find('.convopost').attr('src','/images/conversation/post.svg')
			//   alert($(obj).parents('#rowTab').find('.convopost').attr('src'));
			  $(obj).parents('.replyDiv').find('.postButton').attr('src','/images/conversation/post.svg');
			//   alert($(obj).parents('.replyDiv').find('.postButton').attr('src'));
			//   $('#rowTab').find('.convopost').attr('src','/images/conversation/post.svg');
			  console.log("imageShre:"+imageShre); // data url!
			  const reader = new FileReader();
			  reader.onload = function(evt){
				console.log("data:"+evt.target.result); // data url!
				//this.imgRenderer.nativeElement.src = evt.target.result;
				$(obj).css({backgroundImage: "url(" + evt.target.result + ")"});
			  };
			  reader.readAsDataURL(blob);
			}
		}

	var $width, $height;
	$(function() {
		  $width = $('#width');
		  $height = $('#height');
		  //alert("$data:::::::"+$data);
		  $('.target').on('click', function() {		
		    var $this = $(this);	
		    var bi = $this.css('background-image');					                    		                    
		    $('.active').removeClass('active');
		    $this.addClass('active');		    		 
		    
		  })
		})
		
	  
	function postCanvasToURLImage(feedId,projId,menuType,menuTypeId,postCanvaslevel){  
		var copyPastePlace="copyPaste";
         var file = imageShre;        
		//  feedplace = "documentupload";
		//  feedaction = "fetchinsertFeed";
        //  var feedId;                            
    	 var mainFeedId = "0";
		 var menuId = "";
    	 var localOffsetTime=getTimeOffset(new Date());   
		//   alert(imgfeedId);     	 
    	//  if(imgfeedId==""){
    	// 	 feedId=0;
    	//  }else {
    	// 	 feedId=imgfeedId;
    	//  }
    	 mainFeedId = feedId;
    	 
    	//  if(commentPlace == 'ScrumBoard'){
    	// 	 projId=menuTypeId;
    	//  }
    	 
    	//  if(menuType == 'activityFeed' || menuType == 'activityfeed'){
    	// 	 projId=menuTypeId;
    		 
    	//  }else if(menuType == 'wsDocument'){
    	// 	 menuTypeId=pgId;    		
    		 
    	//  }else if(menuType == 'document'){
    	// 	 projId=menuTypeId;
    	// 	 menuTypeId=menuTypeId;   
    	//  } else{
    	// 	 menuTypeId=menuTypeId; 
    	//  }   
    	 if(menuType == 'Task'){
			menuId = pgId;
			menuTypeId=menuId;
		 }
		
		if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
			menuTypeId=$("#task_id").val();
			menuType="Task";
		}else if(globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
			menuTypeId=$(".popupComments").attr('sourceid');
			menuType = globalmenu=='Idea'?"wsIdea":globalmenu=='agile'||SBmenu=="SBstories" || globalmenu == 'sprint'?"agile":globalmenu=="wsdocument"?"wsDocument":"";
			commentPlace = globalmenu=='agile' || SBmenu=="SBstories" ? "Story" : globalmenu == 'sprint' ? "Sprint" : "";
		}
    	 
    	 $('#loadingBar').addClass('d-flex');
		 //timerControl("start");
		 var name ="";		
		 var formData = new FormData();
		 var today = new Date();
		 var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
			var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			var dateTime = date+'_'+time;
		 formData.append('file',  file, "image_"+dateTime+".png");
		 formData.append('user_id', userIdglb);
		 formData.append('company_id', companyIdglb);
		 formData.append('resourceId', feedId);
		 formData.append('projId', prjid);
		 formData.append('place', "convofile");
		 formData.append('menuTypeId', menuTypeId);
		 formData.append('convotype', menuType);
		 formData.append('subMenuType', commentPlace);
		 
      	 $.ajax({
		   	          //url: path+'/ActivityFeedAction.do?act=uploadFromSystem&projId='+projId+'&userId='+userId+'&companyId='+companyId+'&feedDocumentId='+menuTypeId+'&imgComntType='+menuType+'&feedId='+feedId+'&subMenuType='+commentPlace+'&name='+name+'&localOffsetTime='+localOffsetTime,
		   	          url: apiPath+"/"+myk+"/v1/upload", 
			      	  type: 'POST',
		              processData: false,
                      contentType: false,
                      cache: false,
		              data: formData,	   	          		   	          
		   	          error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $('#loadingBar').addClass('d-none').removeClass('d-flex');
								timerControl("");
								},
		   	          success: function(url) {	
						
					  	feedactionid = url;

		   	        	/* if(name != "" && mainFeedId != 0){
		   	          		fee = url.split('#@#@')[0];
		   	          		fee = fee.substring(1, fee.length-1);
		   	          		fee1 = url.split('#@#@')[1];
		   	          		fee1 = fee1.substring(1, fee1.length-1);
		   	          		url = "[" + fee +","+ fee1 + "]";
		   	          		//console.log("==============>>>>>>>>>>>>>"+url);
		   	          	}else{
		   	          		url = url;
		   	          	} */ 	
						if(postCanvaslevel == '0'){
							level=0;
						}else if(postCanvaslevel == '1'){
							level=1;
						}else{
							level=2;
						}  
		   	        	
						$('#loadingBar').addClass('d-none').removeClass('d-flex');
						//timerControl("");  
						$("#main_commentTextarea").val('');
						$("textarea.main_commentTxtArea").val('');
						$("replyBlock_"+feedId+"").val('');
						$("#actFeedOptPopDiv_"+mainFeedId).hide();  
						$(".replycommentDiv").hide();
						
		   	        	$('#FileUpload').val('');
						$('input[id^=FileUpload_]').val('');	
						
						/* if(menuType=='Task'){
							loadfeednew(menuType);
						}else if(menuType=='agile'){
							fetchAgileComments(menuTypeId);
						} */
						if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
							fetchPostedThingFiles(0,'files');
						}else if(globalmenu=='Idea'){
							$("#replyBlock_"+feedId).css('background-image','none');
							$("#replyDivContainer_"+feedId).find('.postButton').attr('src','/images/conversation/post.svg');
							openComments(menuTypeId,url,feedId);
							if(!$('#ideacomment_'+menuTypeId).is(':visible')){
								$('#ideacomment_'+menuTypeId).css('display','block');
							}
							$("#transparentDiv").show();
						}else if(globalmenu=='agile' || SBmenu=="SBstories"){
							$("#replyBlock_"+feedId).css('background-image','none');
							$("#replyDivContainer_"+feedId).find('.postButton').attr('src','/images/conversation/post.svg');
							agileComments(menuTypeId,"",url,feedId,'sub');
							commentPlace="";
						}else if(globalmenu=='sprint'){
							// alert(feedId);
							$("#replyBlock_"+feedId).css('background-image','none');
							$("#replyDivContainer_"+feedId).find('.postButton').attr('src','/images/conversation/post.svg');
							sprintComments(menuTypeId,"",url,feedId,'sub');
							commentPlace="";
							// feedId="";
						}else if(globalmenu=="wsdocument"){
							var typeDoc1 = feedId==0||feedId=="0"?"":"sub";
							$("#replyBlock_"+feedId).css('background-image','none');
							$("#replyDivContainer_"+feedId).find('.postButton').attr('src','/images/conversation/post.svg');
							wsDocComments(menuTypeId,"",url,feedId,typeDoc1);
						}else{
							// loadActivityFeed('', 'firstClick',menuType);
							let place = feedId == 0?'main': "reply";
							fetchPostedThingFiles(url,place,feedId,copyPastePlace);
							$('#main_commentTextarea').val('').empty();
							$('#main_commentTextarea').removeAttr("style");
						}						        	 
			        	 
		   	          }
		   	          
		   	        });	
      	 
      }	
	
	function dataURItoBlob(dataURI) {
	    var byteString;
	    if (dataURI.split(',')[0].indexOf('base64') >= 0)
	        byteString = atob(dataURI.split(',')[1]);
	    else
	        byteString = unescape(dataURI.split(',')[1]);
	    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
	    var ia = new Uint8Array(byteString.length);
	    for (var i = 0; i < byteString.length; i++) {
	        ia[i] = byteString.charCodeAt(i);
	    }
	    return new Blob([ia], {type:mimeString});
	}
	
	/* ***************Code Ends For Screeen Sharing********************* */
	
	
/* ****************Function starts for Image Capture and Video******************* */
	
	navigator.getUserMedia = ( navigator.getUserMedia ||
		              navigator.webkitGetUserMedia ||
		              navigator.mozGetUserMedia ||
		              navigator.msGetUserMedia);
		
		var video;
		var webcamStream;		
		function startWebcam() {			
		uploadCancel();
			if (navigator.getUserMedia) {
			   navigator.getUserMedia (
			{
			  video: true,
			  audio: false
			},
			
		function(localMediaStream) {
			  video = document.getElementById('video');
			  video.srcObject = localMediaStream;
			  webcamStream = localMediaStream;
			 // video.srcObject = localMediaStream;
			},
			
		function(err) {
				  alert(getValues(companyAlerts,"Alert_EnableCamera"));
				 }
			  );
			} else {
				alert(getValues(companyAlerts,"Alert_NotSupport"));
		    }  
		}
		
		
		function stopWebcam() {		
			$("img#stop1").css({'display':'none'});
			var tracks;
			var track = webcamStream.getTracks()[0];
			track.stop();
			takePic = 0;
			takeVid = 0;
			ctx.clearRect(0, 0, canvas.width, canvas.height);	
			if (video.srcObject) {
				video.srcObject.getTracks().forEach(track => track.stop());
				video.srcObject = null;
				tracks = webcamStream.getVideoTracks()[0].stop();
			}
			localMediaStream = null;
			$('#camUiPopup').html('');
			if($('#otherModuleTaskListDiv').is(':visible') == true || globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
				$("#transparentDiv").show();
			}else{
				$('#transparentDiv').hide();
			}
		}
		var canvas, ctx, imageProjId, imageMenuType , imgfeedId , imgmenuTypeId;
		var takePic = "0";
		var takeVid = "0";
		
	   function initWebcam(feedId,menuTypeId,projectId,menuType) {
			//alert(feedId+"--"+menuTypeId+"--"+projectId+"--"+menuType);
		    if(projectId == "conversationplace"){
				projectId = prjid;
			}
			takePic = "0";
			//$('.fade').css({'opacity':'','display':'block'});
			//$('#camBody').css({'display':'block','margin':'0'});
			$("#myModal2").attr('aria-hidden','false');
			$("#myModal2").css({'display':'block'});
			//$('#myModal2').css({'ddopacity':''});
			$('#videoBody').css({'display':'none'});
			canvas = document.getElementById("myCanvas");
			ctx = canvas.getContext('2d');
			canvas.height = 480;
			canvas.width = 640;
			startWebcam();
			imageProjId=projectId;
			imageMenuType=menuType;
			imgfeedId=feedId;
			imgmenuTypeId=menuTypeId;
			//console.log("INFO:::imageProjId"+imageProjId+"imageMenuType"+imageMenuType+"imgfeedId"+imgfeedId+"imgmenuTypeId"+imgmenuTypeId);
		}
		
		function snapshot() {
			//if($('#camBody').is(':visible')){
				$("#videoCam").css({'display':'none'});
				$("#cam").attr('src','images/conversation/cancel.svg');
				$("#cam").attr('title','Cancel');
				$('#camBody').css({'display':'block'});
				$('#videoBody').css({'display':'none'});
				$("#modalclose").attr('src','images/conversation/save.svg');
				$("#modalclose").attr('title','Save');
				$("#cam").attr('onClick','uploadCancel()');
				$("#modalclose").css({'display':'block','display': 'inline-block'});
				$("#modalclose").attr('onClick','postCanvasToURL()');
				//$("#cam").css({'height':'30px','width':'30px'});
				//$("#videoCam").css({'height':'30px','width':'30px'});
				//$("#modalclose").css({'height':'30px','width':'30px'});
				$('#record1').hide();
				$('#recordImage').hide();
				$('#camImage').show();
				$('.modalclose').attr("data-dismiss", "modal");
				ctx.drawImage(video, 0,0, canvas.width, canvas.height);
				takePic="1";
			/* }else{
				$('#videoBody').hide();
				$('#camBody').show();
			} */
			
		}
		
		function uploadCancel(){
			$("#modalclose").css({'display':'none'});
			$('#videoCapture').css({'display':'block'});			
			$('#recorded').css({'display':'none'});
			$('#videocancel').css({'display':'none'});
			$('#download').css({'display':'none'});
			$('#record1').css({'display':'block'});
			$('#recordImage').show();
			$('#camImage').hide();
			$("#cam").attr('onClick','snapshot()');
			$("#videoCam").attr('onClick','videoType("video")');
			$("#cam").attr('src','images/conversation/cam1.svg');
			$("#cam").attr('title','Camera');
			$("#videoCam").attr('src','images/conversation/cam2.svg').css({'display':'none','display': 'inline-block'});
			$("#cam").show();
			$("#videoCam").show();
			/* $("#cam1").show();
			$("#videoCam1").show();
			$("#cam1").attr('onClick','snapshot()');
			$("#videoCam1").attr('onClick','videoType("video")');
			$("#cam1").attr('src','images/conversation/cam1.svg');
			$("#videoCam1").attr('src','images/document/cam.svg').css({'display':'none','display': 'inline-block','background-color':''}); */
			//$("#cam").css({'height':'40px','width':'40px'});
			//$("#videoCam").css({'height':'40px','width':'40px'});
			takeVid="1";
		}
		
  function postCanvasToURLold() {	
		  var projId=prjid;	
		  var menuId="";
		  if(takePic == "1"){		
			var imageData =  canvas.toDataURL('image/png');
			var file = dataURItoBlob(imageData);
			var feedId;		
			 if(imgfeedId === ""){				
	    		 feedId=0;	    		
	    	 }else {	    		 
	    		 feedId=imgfeedId;	    		 
	    	 }
    	 				
		  if(imageMenuType === "activityFeed"){			
			 imageProjId=imageProjId;			
		  }else if(imageMenuType === "document"){
			 imageProjId=imgmenuTypeId;
			 projId = imageProjId;
		  }else if(imageMenuType === "blog"){
			 imageProjId=imgmenuTypeId;
		  }else if(imageMenuType === "wsDocument"){
			 imageProjId=imgmenuTypeId;
		  }else if(imageMenuType === "wsIdea"){
			 imageProjId=imgmenuTypeId;
			 //projId=imageProjId;
			 projId = $("#proj_ID").val();
		  }else if(imageMenuType === "agile"){
			 imageProjId=imgmenuTypeId;
			 //projId=imageProjId;
			 projId = $("#proj_ID").val();
		  }else if(imageMenuType === "Task"){
			 imageProjId=imgmenuTypeId;
			 projId = taskprojId;
			 menuId = pgId;
		  }else{
   	    	 imgmenuTypeId=imgmenuTypeId;   	    	
   	      } 
		  if(typeof(imageMenuType)=="undefined"){
			imageMenuType="";
		  }
			feedaction = "fetchinsertFeed";
			feedplace = "documentupload"; 
			var mainFeedId = "";
			var localOffsetTime=getTimeOffset(new Date());
			mainFeedId = feedId;
			$('#loadingBar').show();
			timerControl("start");
			var name ="";
			//var projIdUpload = ${projId};			
			var formData = new FormData();
			//var imgComntType='activityFeed';
			var imgComntType;
			imgComntType=imageMenuType;
			//var commentPlace="Story";
			var today = new Date();
			var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
			var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			var dateTime = date+'_'+time;
			formData.append('file',  file, "image_"+dateTime+".png");
			formData.append('user_id', userIdglb);
			formData.append('company_id', companyIdglb);
			formData.append('resourceId', feedId);
			formData.append('projId', prjid);
			formData.append('place', "convofile");
			formData.append('menuTypeId', imgmenuTypeId);
			formData.append('convotype', imageMenuType);
			formData.append('subMenuType', commentPlace);
		$.ajax({
				  url: apiPath+"/"+myk+"/v1/upload", 
				  type: 'POST',
				  processData: false,
				  contentType: false,
				  cache: false,
				  data: formData,
				  dataType:'text',
				  data: formData,
		          error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							},
		          contentType: false,
		          processData: false,
		          success: function(url) {		         
		        	
		        	//alert("Success URL::>>"+url);
		        	/* if(name != "" && mainFeedId != 0){
		          		fee = url.split('#@#@')[0];
		          		fee = fee.substring(1, fee.length-1);
		          		fee1 = url.split('#@#@')[1];
		          		fee1 = fee1.substring(1, fee1.length-1);
		          		url = "[" + fee +","+ fee1 + "]";
		          		 
		          	}else{
		          		url = url;
		          	} 	  */         
		        	$("#loadingBar").hide();
					timerControl("");  
					$("#main_commentTextarea").val('');
					$("textarea.main_commentTxtArea").val('');
					$("replyBlock_"+feedId+"").val('');
					$("#actFeedOptPopDiv_"+mainFeedId).hide();  
					$(".replycommentDiv").hide();
					 
		        	$('#FileUpload').val('');
					$('input[id^=FileUpload_]').val('');
					//$("#actFeedOptPopDiv_"+feedId).hide();
					//url = jQuery.parseJSON(url)
					feedactionid = url;
					if(imageMenuType=='Task'){
						loadfeednew(imageMenuType);
					}else if(imageMenuType=='agile'){
						fetchAgileComments(imgmenuTypeId);
					}else{
						loadActivityFeed('', 'firstClick',imageMenuType);
					}
					 
					
		        	//loadActivityFeed(notFolid, 'firstClick');
		          }
		          
		        }); 
		        stopWebcam();
		     
		}else{
			$('.modalclose').attr("data-dismiss", "");
			alertFun(getValues(companyAlerts,"Alert_Snapshot"),'warning'); 
		
		}
		
   }
		function dataURItoBlob(dataURI) {
			var byteString;
			if (dataURI.split(',')[0].indexOf('base64') >= 0)
			byteString = atob(dataURI.split(',')[1]);
			else
			byteString = unescape(dataURI.split(',')[1]);
			var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
			var ia = new Uint8Array(byteString.length);
			for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
			}
		 return new Blob([ia], {type:mimeString});
		}
			
	   var captureVideo="";	
	   
		function videoType(type){	
			if(type=="video"){		
				$('#camBody').css({'display':'none'});
				$('#videoBody').css({'display':'block','margin':'0'});
				$('#recorded').css({'display':'none'});
				$('#videoCapture').css({'display':'block'});			
				$('#record1').css({'display':'block'});
				initVideoCapture();		
			}else{	
				$('#camBody').css({'display':'block'});
				$('#videoBody').css({'display':'none'});
				initWebcam(feedId,'',projectId,menuType);
			
			}
		}
		
		function showVideo(){
			webcamStream.getVideoTracks()[0].stop();
			$("img#record1").show();
			$("img#stop1").hide();
		}
		
	function uploadVideoold(){			
			var projId=prjid;
			var menuId="";
		if(takeVid == "1"){
			var feedId;			
			 if(imgfeedId === ""){				
	    		 feedId=0;	    		
	    	 }else {	    		 
	    		 feedId=imgfeedId;	    		 
	    	 }			
			//console.log("feedId>>>>>>>>>>>>>>>"+feedId);			
			//console.log("imageMenuType:::::::::::::"+imageMenuType);
			//console.log("userId:::::::::::::"+userId);
			//console.log("companyId:::::::::::::"+companyId);
			//console.log("imageProjId:::::::::::::"+imageProjId);
			 
			 if(imageMenuType === "activityFeed"){			
					imageProjId=imageProjId;			
				}else if(imageMenuType === "document"){
					imageProjId=imgmenuTypeId;
					projId = imageProjId;
				}else if(imageMenuType === "blog"){
					imageProjId=imgmenuTypeId;
				}else if(imageMenuType === "wsDocument"){
					imageProjId=imgmenuTypeId;
				}else if(imageMenuType === "wsIdea"){
					imageProjId=imgmenuTypeId;
					//projId=imageProjId;
					 projId = $("#proj_ID").val();
				}else if(imageMenuType === "agile"){
					imageProjId=imgmenuTypeId;
					//projId=imageProjId;
					projId = $("#proj_ID").val();
				}else if(imageMenuType === "Task"){
					imageProjId=imgmenuTypeId;
					projId = taskprojId;
					menuId = pgId;
				}else{
		   	    	imgmenuTypeId=imgmenuTypeId;   	    	
		   	    } 
				if(typeof(imageMenuType)=="undefined"){
					imageMenuType="";
				}   
			
			//var feedId=0;
			feedaction = "fetchinsertFeed";
			feedplace = "documentupload";
			var mainFeedId = "";
			var localOffsetTime=getTimeOffset(new Date());
			mainFeedId = feedId;
			$('#loadingBar').show();
			timerControl("start");
			var name ="";			
			//var projIdUpload = ${projId};			
			var formData = new FormData();
			var imgComntType;
			imgComntType=imageMenuType;
			var today = new Date();
			var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
			var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			var dateTime = date+'_'+time;  
			formData.append('file',  captureVideo, "video_"+dateTime+".mp4");
			formData.append('user_id', userIdglb);
			formData.append('company_id', companyIdglb);
			formData.append('resourceId', feedId);
			formData.append('projId', projId);
			formData.append('place', "convofile");
			formData.append('menuTypeId', imgmenuTypeId);
			formData.append('convotype', imageMenuType);
			formData.append('subMenuType', commentPlace);
		$.ajax({		         			     
				  url: apiPath+"/"+myk+"/v1/upload", 
				  type: 'POST',
				  //dataType:'text',
				  processData: false,
				  contentType: false,
				  cache: false,
				  data: formData,
		          error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							},
		          contentType: false,
		          processData: false,
		          success: function(url) {
		        	
		        	/* if(name != "" && mainFeedId != 0){
		          		fee = url.split('#@#@')[0];
		          		fee = fee.substring(1, fee.length-1);
		          		fee1 = url.split('#@#@')[1];
		          		fee1 = fee1.substring(1, fee1.length-1);
		          		url = "[" + fee +","+ fee1 + "]";
		          	}else{
		          		url = url;
		          	} 	 */          
		        	$("#loadingBar").hide();
					timerControl("");  
					$("#main_commentTextarea").val('');
					$("textarea.main_commentTxtArea").val('');
					$("replyBlock_"+feedId+"").val('');
					$("#actFeedOptPopDiv_"+mainFeedId).hide();  
					$(".replycommentDiv").hide();
					
		        	$('#FileUpload').val('');
					$('input[id^=FileUpload_]').val('');

					feedactionid = url;
					if(imageMenuType=='Task'){
						loadfeednew(imageMenuType);
					}else if(imageMenuType=='agile'){
						fetchAgileComments(imgmenuTypeId);
					}else{
						loadActivityFeed('', 'firstClick',imageMenuType);
					}

					
			
        	//loadActivityFeed(notFolid, 'firstClick');
          }
          
        }); 
	        stopWebcam();
	        $('#videoBody').hide();
	        $('.fade').css({'opacity':'0','display':'none'});
	        $('#myModal').attr('aria-hidden','true');
	        $('.modal-open').css({'overflow-y':'auto'});
	     
	}else{
		$('.modalclose').attr("data-dismiss", "");
		alertFun(getValues(companyAlerts,"Alert_Snapshot"),'warning'); 
	
	}
	
 }
	
	/* ****************Function ends for Image Capture and Video******************* */
						
		function initVideoCapture(){
			var mediaSource = new MediaSource();
			mediaSource.addEventListener('sourceopen', handleSourceOpen, false);
			var mediaRecorder;
			var recordedBlobs;
			var sourceBuffer;		
			var initialVideo = document.querySelector('video#videoCapture');
			var recordedVideo = document.querySelector('video#recorded');									
			var recordButton = document.querySelector('img#record1');
			var stopButton = document.querySelector('img#stop1');
			var downloadButton = document.querySelector('img#download');
			recordButton.onclick = toggleRecording;
			stopButton.onclick = toggleRecording1;
			downloadButton.onclick = download;
			
			var isSecureOrigin = location.protocol === 'https:' || location.hostname === 'localhost';
				if (!isSecureOrigin) {
				  alert('getUserMedia() must be run from a secure origin: HTTPS or localhost.' +
				    '\n\nChanging protocol to HTTPS');
				  location.protocol = 'HTTPS';
				}			
				var constraints = {
				  audio: true,
				  video: true
				};
		
			function handleSuccess(stream) {
			  recordButton.disabled = false;
			 // console.log('getUserMedia() got stream: ', stream);
			  window.stream = stream;
			  webcamStream = stream;
			  initialVideo.srcObject = stream;
			}		
			function handleError(error) {
			  console.log('navigator.getUserMedia error: ', error);
			}
		
			navigator.mediaDevices.getUserMedia(constraints).
			    then(handleSuccess).catch(handleError);
			
			function handleSourceOpen(event) {
			  //console.log('MediaSource opened');
			  sourceBuffer = mediaSource.addSourceBuffer('video/mp4; codecs="vp8"');
			  //console.log('Source buffer: ', sourceBuffer);
			}
		
			recordedVideo.addEventListener('error', function(ev) {
			  console.error('MediaRecording.recordedMedia.error()');
			  alert('Your browser can not play\n\n' + recordedVideo.src
			    + '\n\n media clip. event: ' + JSON.stringify(ev));
			}, true);
			
			function handleDataAvailable(event) {
			  if (event.data && event.data.size > 0) {
			    recordedBlobs.push(event.data);
			  }
			}
		
			function handleStop(event) {
			  console.log('Recorder stopped: ', event);
			}
		
			function toggleRecording() {			
			    startRecording();
				$("img#cam1").show();
				$("img#videoCam1").show();
				$("#videoCam1").css('background-color','rgb(42 148 203)');
			    $("img#record1").hide();
			    $("img#stop1").show();
			 }
			 function toggleRecording1() {				
			 	$("img#record1").show();
			    $("img#stop1").hide();
				$("img#cam1").hide();
				$("img#videoCam1").hide();
				$("#videoCam1").css('background-color','');
			
			    stopRecording();
			    play();
			    downloadButton.disabled = true;	
			   // downloadButton.disabled = false;	
			 }
		
			function startRecording() {
			  recordedBlobs = [];
			  var options = {mimeType: 'video/mp4;codecs=VP8'};
			  if (!MediaRecorder.isTypeSupported(options.mimeType)) {
			    console.log(options.mimeType + ' is not Supported');
			    options = {mimeType: 'video/mp4;codecs=VP8'};
			    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
			      console.log(options.mimeType + ' is not Supported');
			      options = {mimeType: 'video/mp4'};
			      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
			        console.log(options.mimeType + ' is not Supported');
			        options = {mimeType: ''};
			      }
			    }
			  }
				  try {
				    mediaRecorder = new MediaRecorder(window.stream, options);
				  } catch (e) {
				    console.error('Exception while creating MediaRecorder: ' + e);
				    alert('Exception while creating MediaRecorder: '
				      + e + '. mimeType: ' + options.mimeType);
				    return;
				  }
				  
			  recordButton.textContent = 'Stop Recording';
			  mediaRecorder.onstop = handleStop;
			  mediaRecorder.ondataavailable = handleDataAvailable;
			  mediaRecorder.start(10); // collect 10ms of data
			}
		
			function stopRecording() {
			  $('#videocancel').css({'display':'block'});
			  $('#download').css({'display':'block'});	
			  $('#videoCapture').css({'display':'none'});		
			  $('#recorded').css({'display':'block'});	
			  $('#record1').css({'display':'none'});
			  mediaRecorder.stop();
			  recordedVideo.controls = true;
			}
		
			function play() {
				//alert("recordedBlobs-->"+recordedBlobs);
			  var superBuffer = new Blob(recordedBlobs, {type: 'video/mp4'});
			  recordedVideo.src = window.URL.createObjectURL(superBuffer);
			}
		
		function download() {
		  var blob = new Blob(recordedBlobs, {type: 'video/mp4'});
		  captureVideo = blob;		   
		  uploadVideo();
		}
		
}

/* codes for file sharing Drag & drop */
var files = "";		
function fileSharingForComments(id, feedId){
	// alert(feedId);
  	var obj = $('#'+id);
  	//var menuType =obj.attr("menuType");
  	obj.off();
  	obj.on('dragenter', function (e) 
  	{
  		e.stopPropagation();
  	    e.preventDefault();
  	});
  	obj.on('dragover', function (e) 
  	{
  	     e.stopPropagation();
  	     e.preventDefault();
  	});
  	obj.on('drop', function (e) 
  	{
		
		//e.preventDefault();
		var comment = $(obj).val();
		var comment1 = $('#replyBlock_'+feedId).val();
		files = e.originalEvent.dataTransfer.files;

		if(globalmenu=='Idea' || globalmenu=='Task' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
			if((typeof(comment1) != "undefined" && comment1 != "") ){
				postReply(feedId,"dragndrop");
				
			}else{
				handleFileUploadForComments(files,feedId);
			}
		}else{
		
			if(typeof(comment) != "undefined" && comment != "" && feedId==0 ){
			
				CommentActFeed(prjid,menuutype,"dragndrop",0)
			}
		
			else if((typeof(comment1) != "undefined" && comment1 != "") ){
			
				postReply(feedId,"dragndrop",1);
				
			}else{
				handleFileUploadForComments(files,feedId);
			}
		}
			

		 
  	});
  	$(document).on('dragenter', function (e) 
  	{
	    e.stopPropagation();
	    e.preventDefault();
  	});
  	$(document).on('dragover', function (e) 
  	{
  	  e.stopPropagation();
  	  e.preventDefault();
  	});
  	$(document).on('drop', function (e) 
  	{
  		e.stopPropagation();
  	    e.preventDefault();
  	});
 }

 
					 
var fileUploadSharingForTaskComments = "";
var dragNDropFeedIdForTaskComments="";
 function handleFileUploadForComments(files, feedId,dragndrop){
	  $("#loadingBar").show();	
	  timerControl("start");
	  
	  fileUploadSharingForTaskComments = files;
	  dragNDropFeedIdForTaskComments = feedId;
	  var localOffsetTime=getTimeOffset(new Date());
	  var menuType = $('#menuTypeForComment').val();
      var menuTypeId =  $('#menuTypeIdForComment').val();
	  
		for (var i = 0; i < files.length; i++){
			//alert("files[i].size:::>>"+files[i].size);
			if(files[i].size >= 25000000){
				confirmReset(getValues(companyAlerts,"Alert_ResetMoreSize"),'delete','dragAndDropUploadHugeDataFileForComment','cancelTheDragAndDropUploadForComment');
				$('.alertMainCss').css({'width': '28.5%'});
				$('.alertMainCss').css({'left': '45%'});
				$('#BoxConfirmBtnOk').css({'width': '21%'});
				$('#BoxConfirmBtnOk').val('Continue');
			}else{
				if (window.File && window.FileReader && window.FileList && window.Blob){
					docUploadInComments(files[i], feedId,dragndrop);
				}else { 
				  alertFun(getValues(companyAlerts,"Alert_APInotSupport"),'warning'); 
				}
			}
	    }

	  
 
}
					  
 function cancelTheDragAndDropUploadForComment(){
 	//alert("cancelTheDragAndDropUpload::>>");
  	$("#loadingBar").hide();	
  	timerControl("");
 }
					  
 function dragAndDropUploadHugeDataFileForComment(){
	//alert("fileUploadSharingForTaskComments::>>"+fileUploadSharingForTaskComments);
	for (var i = 0; i < fileUploadSharingForTaskComments.length; i++){
		if (window.File && window.FileReader && window.FileList && window.Blob){
			docUploadInComments(fileUploadSharingForTaskComments[i], dragNDropFeedIdForTaskComments);
   		}else { 
      		alertFun(getValues(companyAlerts,"Alert_APInotSupport"),'warning'); 
       	}
	 }
	 fileUploadSharingForTaskComments = "";
	 dragNDropFeedIdForTaskComments="";
 }
					  
 function sendFileToServerForComment(obj, feedId){
	$('#loadingBar').addClass('d-flex');
	var localOffsetTime=getTimeOffset(new Date());
	var name ="";
	var fee = "";
   	var fee1 = "";
    var mainFeedId= feedId;
    var menuType = $('#menuTypeForComment').val();
    var menuTypeId =  $('#menuTypeIdForComment').val();
    var projIdUpload="";
	var menuId = "";
	feedaction = "fetchinsertFeed";
	feedplace = "documentupload";
	
	if(menuType == 'Task'){
		projIdUpload = taskprojId;
		menuId = pgId;
		menuTypeId=menuId;
		//feedId = pgId;
	}else if(menuType == 'activityFeed'){
			projIdUpload = $('#ProjIdForComment').val();
	}/*else if(menuType == 'wsDocument'){
			projIdUpload = $('#projId').val();
	}*/else if(menuType == 'wsIdea'){
			projIdUpload = $('#ProjIdForComment').val();
			menuTypeId = pgId;
	}else if(menuType == 'agile'){
			projIdUpload = $('#proj_ID').val();
	}
	if(typeof(menuType)=="undefined"){
		menuType="";
	}
	if(menuType == "activityFeed"){
	  if(feedId == '0'){
  			name = $("textarea#main_commentTextarea").val().trim();
	  }else{
	    	name = $("textarea#replyBlock_"+feedId+"").val().trim();
	  }
			          	  
	  if(name != ""){
	         name = name.replaceAll("#", "ch(H)");
	         name = name.replaceAll("=", "ch(Eq)");
	         name = name.replaceAll("&", "ch(amp)");
	         name = name.replaceAll("%", "ch(per)");
	  }
	}				     
	  	 
	      


		  var formData = new FormData();
	      formData.append('file', obj);
		  var formData = new FormData();
		  formData.append('file', obj);
		  formData.append('user_id', userIdglb);
		  formData.append('company_id', companyIdglb);
		  formData.append('resourceId', feedId);
		  formData.append('projId', prjid);
		  formData.append('place', "convofile");
		  formData.append('menuTypeId', menuTypeId);
		  formData.append('convotype', menuType);
	      formData.append('subMenuType', commentPlace);
	      var imgComntType;
	  	  imgComntType=menuType;
	      
	         $.ajax({
				  url: apiPath+"/"+myk+"/v1/upload", 
				  type: 'POST',
				  processData: false,
				  contentType: false,
				  cache: false,
				  data: formData,
				  dataType:'text',
				  data: formData,
	   	          error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					$('#loadingBar').addClass('d-none').removeClass('d-flex');
					timerControl("");
				  },	
	   	          success: function(url) {
	   	          
	   	          	/* if(name != "" && mainFeedId != 0){
	   	          		fee = url.split('#@#@')[0];
	   	          		fee = fee.substring(1, fee.length-1);
	   	          		fee1 = url.split('#@#@')[1];
	   	          		fee1 = fee1.substring(1, fee1.length-1);
	   	          		url = "[" + fee +","+ fee1 + "]";
	   	          		//console.log("==============>>>>>>>>>>>>>"+url);
	   	          	}else{
	   	          		url = url;
	   	          	} */
	   	          	$('#loadingBar').addClass('d-none').removeClass('d-flex');
					//timerControl("");
					$("#main_commentTextarea").val('');
					$("textarea.main_commentTxtArea").val('');
					$("replyBlock_"+feedId+"").val('');
					$("#actFeedOptPopDiv_"+feedId).hide();  
					$(".replycommentDiv").hide();
					//url = jQuery.parseJSON(url);
					feedactionid = url;
					if(menuType=='Task'){
						loadfeednew(menuType);
					}else if(menuType=='agile'){
						fetchAgileComments(menuTypeId);
					}else{
						// loadActivityFeed('', 'firstClick',menuType);
						let place = feedId == 0?'main': "reply";
						fetchPostedThingFiles(url,place,feedId)
					}
					

	   	         }
	   	    });  
  }	
 
 /* **************** Function for Document upload ******************* */
 function setUp(Id){
	//  console.log("in setup");
	// document.getElementById('mainUpload').addEventListener('click', openDialog);
	// $('#mainUpload').click(function() {
		openDialog(Id);
	//  });
 }
 function openDialog(Id) {
	// console.log("in openDialog");
	  document.getElementById('FileUpload_'+Id+'').click();
}
 var docUploadFeedId="";
 var inputFiles="";
 function readFileUrl(input, feedId){
	docUploadFeedId = feedId;
	inputFiles = input.files[0];
	if (input.files && input.files[0]) {
    	  //var fileSize = input.files[0].size;
    	   if(input.files[0].size >= 25000000){
    	  		confirmReset(getValues(companyAlerts,"Alert_ResetMoreSize"),'delete','docUploadInComments','cancelTheDragAndDropUpload');
    	  		$('.alertMainCss').css({'width': '28.5%'});
				$('.alertMainCss').css({'left': '45%'});
				$('#BoxConfirmBtnOk').css({'width': '21%'});
    			$('#BoxConfirmBtnOk').val('Continue');
    	  }else{
    	  		docUploadInComments(inputFiles,docUploadFeedId);
    	  }
    }
  }
 	  
  function docUploadInComments(obj,Id,dragndrop){
	var feedId=Id;
   	var mainFeedId = "";
   	var localOffsetTime=getTimeOffset(new Date());
    mainFeedId = feedId;
    var menuType = $('#menuTypeForComment').val();
    var menuTypeId =  $('#menuTypeIdForComment').val();
    var projIdUpload="";
	var menuId="";
	// feedaction = "fetchinsertFeed";
	// feedplace = "documentupload";
	$('#loadingBar').addClass('d-flex');
  	//   timerControl("start");
     	var name ="";
     
        //   if(feedId == '0'){
		//     	name = $("textarea#main_commentTextarea").val().trim();
		//     	//mainFeedId = feedId;
		//   }else{
		//     	name = $("textarea#replyBlock_"+feedId+"").val().trim(); 
		//   }
		//   if(name != ""){
		//         name = name.replaceAll("#", "ch(H)");
		//         name = name.replaceAll("=", "ch(Eq)");
		//         name = name.replaceAll("&", "ch(amp)");
		//         name = name.replaceAll("%", "ch(per)");
		//   }
		 	
  	  if(menuType == 'Task'){
  		    projIdUpload = taskprojId;
			  menuId=pgId;
			  menuTypeId=menuId;
  	  }else if(menuType == 'activityfeed'){
	        projIdUpload = $('#ProjIdForComment').val();
      }/*else if(menuType == 'wsDocument'){
	    	projIdUpload = $('#projId').val();
      }*/else if(menuType == 'wsIdea'){
      	    projIdUpload = $('#ProjIdForComment').val();
      	    menuTypeId = pgId;
      }else if(menuType == 'agile'){
      	    projIdUpload = $('#proj_ID').val();
			  
      }
	  if(typeof(menuType)=="undefined"){
		menuType="";
	  }
  	  
  	  var imgComntType;
  	  imgComntType=menuType;
	  var place="";
	  if(globalmenu=='Task'  || $('#otherModuleTaskListDiv').is(':visible') == true){
		menuTypeId=$("#task_id").val();
		menuType="Task";
	  }else if(globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
		menuTypeId=$(".popupComments").attr('sourceid');
		menuType = globalmenu=='Idea'?"wsIdea":globalmenu=='agile'||SBmenu=="SBstories"||globalmenu == 'sprint'?"agile":globalmenu=="wsdocument"?"wsDocument":"";
		commentPlace = globalmenu=='agile' || SBmenu=="SBstories" ? "Story" : globalmenu == 'sprint' ? "Sprint" : "";
	  }
      var formData = new FormData();
      formData.append('file', obj);
	  formData.append('user_id', userIdglb);
	  formData.append('company_id', companyIdglb);
	  formData.append('resourceId', feedId);
	  formData.append('projId', prjid);
	  formData.append('place', "convofile");
	  formData.append('menuTypeId', menuTypeId);
	  formData.append('convotype', menuType);
	  formData.append('subMenuType', commentPlace);
	  
      
        $.ajax({
		  url: apiPath+"/"+myk+"/v1/upload", 
		  type: 'POST',
		  processData: false,
		  contentType: false,
		  cache: false,
		  data: formData,
		  dataType:'text',
          data: formData,
          error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $('#loadingBar').addClass('d-none').removeClass('d-flex');
					//timerControl("");
					},
          success: function(url) {
          
        	$("#main_commentTextarea").val('');
			$("textarea.main_commentTxtArea").val('');
			$("replyBlock_"+feedId+"").val('');
			$("#actFeedOptPopDiv_"+mainFeedId).hide();  
			$(".replycommentDiv").hide();
			$('#FileUpload').val('');
			$('input[id^=FileUpload_]').val('');
			feedactionid = url;
			
			if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
				fetchPostedThingFiles(0,'files');
			}else if(globalmenu=='Idea'){
				openComments(menuTypeId,url,feedId);
				if(!$('#ideacomment_'+menuTypeId).is(':visible')){
					$('#ideacomment_'+menuTypeId).css('display','block');
				}
		    }else if(globalmenu=='agile' || SBmenu=="SBstories"){
				agileComments(menuTypeId,"",url,feedId,'sub');
				commentPlace="";
			}else if(globalmenu=='sprint'){
				sprintComments(menuTypeId,"",url,feedId,'sub');
				commentPlace="";
			}else if(globalmenu=="wsdocument"){
				var typeDoc1 = feedId==0||feedId=="0"?"":"sub";
				wsDocComments(menuTypeId,"",url,feedId,typeDoc1);
			}else{
				if(menuType=='Task'){
					loadfeednew(menuType);
				}else if(menuType=='agile'){
					fetchAgileComments(menuTypeId);
				}else{
					// loadActivityFeed('', 'firstClick',menuType);
					let place = Id == 0?'main': "reply";
					// alert("place :"+place)
					fetchPostedThingFiles(url,place,feedId,dragndrop)
				}
			}

			if($('#otherModuleTaskListDiv').is(':visible') == true && (globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument")){
				$("#transparentDiv").show();
			}
			
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			//timerControl("");  
          }
        }); 
}  
  
 function uploadDoc(feedId){  
 		$('#FileUpload_'+feedId+'').trigger('click');
 }

 function loadfeednew(postMenuType){
	
	replycomment = "";
	 feedEditId = "";
	$("#loadingBar").show();
	timerControl("start");
	vertiScrollBar = false;
	var localOffsetTime=getTimeOffset(new Date());
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	var searchText="";
	var sortBy="";
	if(level == 0){
		activityfeedid="";
	}
	
	/* let jsonbody = {
		"user_id":userIdglb,
		"company_id":companyIdglb,
		"project_id":prjid,
		"sortType":sortBy,
		"index":index,
		"limit":limitAct,
		"localOffsetTime":localOffsetTime,
		"txt":searchText,
		"status":status,
		"feedId":"0",
		"from":"",//from getting from session previously
		"index1":"",
		"latestFeed":feedactionid,
		"action":feedaction,
		"parent_feed_id":""+activityfeedid+""
	} */
 
	let jsonbody = {
		"task_id":myTaskId,
		"project_id":prjid,
		"localOffsetTime":localOffsetTime,
		"type":"Main",
		"feedType":menuutype
	} 
	
		

		$.ajax({
			url: apiPath+"/"+myk+"/v1/getFeedDetails",
			type:"POST",
			dataType:'json',
			contentType:"application/json",
			data: JSON.stringify(jsonbody),
			//data:{act:"loadWrkspaceActivityFeeds", userId:userId, projectId:projectId, sortType:sortBy, currFeedId:"0",
			//index:index, limit:limitAct,localOffsetTime:localOffsetTime,txt:searchText,status:status,feedId:feedId,from:from,index1:index1},
			beforeSend: function (jqXHR, settings) {
				xhrPool.push(jqXHR);
			},
			error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$("#loadingBar").hide();
						timerControl("");
						},
			success:function(result){
			
			jsonData = result;
			if(postMenuType=='Task'){
				$("#Taskcomment_"+myTaskId+"").children('.TaskCommentListDiv').html('');
				result = prepareCommentsUI(jsonData,postMenuType,prjid);
				result = result.replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(26)",":");	
				$("#Taskcomment_"+myTaskId+"").children('div.TaskCommentMainTextareaDiv').hide();
				$("#Taskcomment_"+myTaskId).children('div.TaskCommentListDiv').prepend(result);
				$("#Taskcomment_"+myTaskId+"").find('div#taskNoComments').remove();
				$('#taskCommentNewUI_'+myTaskId).attr('src','images/commenton2.png');
				$('.cmntsIcon').find('img#cIcon_'+myTaskId).show().attr('src','images/commentBlack2.png').removeClass("hover-comment-icons");
				$('.cmntsIcon').find('img#cIcon_'+myTaskId).attr('title','View comments')
				$('textarea#main_commentTextarea').val('');
				if(feedplace == "audioupload"){	
					$("#Taskcomment_"+myTaskId).find(".TaskCommentListDiv").children('div[id^="actFeedMainDiv_"]').css("border-bottom","none");
					$("#audio").attr("src", '');  // discard the audio source
					$('#convoPost').children().eq(1).attr('onclick',"CommentActFeed('"+prjid+"','postMenuType')"); // change the audio post onclick method
					$('.actFeedMore').parent().prev().children().eq(3).attr('onclick',replyFunction); // to change for subreply
					$('#audioContainer , div.timer').hide(); //hiding audio container
					$('#main_commentTextarea').show(); // showing textarea
					$('div.timer').find('div.timerSpan').text(''); 
				}
			}
			
				
		}
	});
	
	if(resultset == '' || resultset == '[]'){
		commonFunctionForSpecialChar('','',postMenuType);
	}
	
	if(hashResult == '' || hashResult == '[]'){
		ajaxCallforHashCode(postMenuType,feedId,'');
	}
	$('#main_commentTextarea').css('background-image','none');	

	$("#loadingBar").hide();
	timerControl("");
	
 }

  /// emoji plugin implementaion

 function emojiClick(place,Id){
	if(place=='main'){
		$('#smilePop').click(function(e){
			e.preventDefault();
			$('#main_commentTextarea').emojiPicker();
			//console.log('off left:'+$('#smilePop').offset().left);
			$('.emojiPicker').css('left',$('#smilePop').offset().left+40+'px');
		});
		$('#smilePop').click(function(e){
			e.preventDefault();
			$('#main_commentTextarea').emojiPicker('smilePop');
			//console.log('off left:'+$('#smilePop').offset().left);
			$('.emojiPicker').css('left',$('#smilePop').offset().left+40+'px');
		});
	}else if(place=='reply'){
		$('#smilePop_'+Id).click(function(e){
			e.preventDefault();
			$("#replyBlock_" +Id).emojiPicker();
			$('.emojiPicker').css('left',$('#smilePop_'+Id).offset().left+40+'px');
		});
		$('#smilePop_'+Id).click(function(e){
			e.preventDefault();
			$("#replyBlock_" +Id).emojiPicker('smilePop');
			$('.emojiPicker').css('left',$('#smilePop_'+Id).offset().left+40+'px');
		});
	}
	else if(place=='edit'){
		$('#smilePop_'+Id).click(function(e){
			e.preventDefault();
			$("#editBlock_" +Id).emojiPicker();
			$('.emojiPicker').css('left',$('#smilePop_'+Id).offset().left+40+'px');
		});
		$('#smilePop_'+Id).click(function(e){
			e.preventDefault();
			$("#editBlock_" +Id).emojiPicker('smilePop');
			$('.emojiPicker').css('left',$('#smilePop_'+Id).offset().left+40+'px');
		});
	}
	
 }

 function showOptionsUi(feedid,obj,feedType,commentedPersonId,parentfeedid){
	var ui="";
	$('#subcommentDiv1_'+feedid).remove();

	ui='<div id="subcommentDiv1_'+feedid+'" class="actFeedOptionsDiv" style="display:block;border-radius:8px;">' 
		//+'<div class="arrow-right"></div>
		+'<div class="d-flex align-items-center">'
		+'<img src="/images/conversation/reply.svg" title="Reply" onclick="AddSubComments('+feedid+');" id="replyOptionImg_'+feedid+'"  class="image-fluid" style="height: 20px; width:20px;margin: 0px 6px;">'
		if((userIdglb==commentedPersonId) && (feedType == "text")){
			ui+="<img src=\"/images/conversation/edit.svg\" title=\"Edit\" onclick=\"editComment("+feedid+",'"+feedType+"');\" id='editOptionImg_"+feedid+"' class=\"image-fluid\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
		}
		if(globalmenu!='Task' && globalmenu!='Idea' && globalmenu!='agile' && SBmenu!="SBstories" && globalmenu!='sprint' && globalmenu!='wsdocument' && convoTask!="convoTask"){
			ui+='<img src="/images/task/task.svg" title="Task"  id="taskOptionImg_'+feedid+'" onclick="commentsTask('+feedid+',\'feedoption\');event.stopPropagation();" class="image-fluid" style="height: 20px;width:20px;margin: 0px 6px;">'
		}
		if(globalmenu=='wsdocument'){
			ui+='<img src="images/idea/share_nofill.svg" title="Share"  id="taskOptionShareImg_'+feedid+'" onclick="documentCommentShare('+feedid+','+parentfeedid+');event.stopPropagation();" class="image-fluid" style="height: 20px;width:20px;margin: 0px 6px;">'
		}
		if((userIdglb==commentedPersonId)||(projectUsersStatus=="PO")){
			ui+='<img src="/images/conversation/delete.svg" title="Delete" id="deleteOptionImg_'+feedid+'" class="image-fluid" style="margin: 0px 6px;height: 15px;width:15px;" onclick="deleteMainActivityFeedNew('+feedid+','+userIdglb+',\''+globalmenu+'\')">'
		}
		ui+='</div>'
	+'</div>'

	$(obj).parent().append(ui);

 }

 function viewordownloadUi(docid,obj,ext,feedid,place){
	var ui="";
	
	$('#viewdownloadfileinconversation').remove();

	ui='<div id="viewdownloadfileinconversation" class="viewdownloadfileinconversationCls actFeedOptionsDiv d-block" style="left: 48px;top: 10px;right: unset;border-radius: 8px;">' 
		//+'<div class="arrow-left"></div>'
		+'<div class="d-flex align-items-center">'
		+"<img src=\"images/conversation/expand.svg\" title=\"View\" id='viewOptionImg_"+feedid+"' onclick=\"viewdocument("+docid+",'"+ext+"','"+place+"');\" class=\"image-fluid\" style=\"height: 16px; width:16px; cursor:pointer;margin:0px 6px;\">"
		+"<img src=\"images/conversation/download2.svg\" title=\"Download\" id='downloadOptionImg_"+feedid+"'  onclick=\"downloadActFile("+docid+","+prjid+",'"+ext+"',"+feedid+",'"+place+"')\" class=\"image-fluid\" style=\"margin:0px 6px;height: 18px; width:18px; cursor:pointer;\">"
		
		+'</div>'
	+'</div>'
	$(obj).parent().append(ui);
	if(place=="task"){
		$('#viewdownloadfileinconversation').css('left','100px');
		$('#viewdownloadfileinconversation').css('top','5px');
	}else if(place=="WorkspaceDocument" || place=="Document"){
		$('#viewdownloadfileinconversation').css('left','235px');
		$('#viewdownloadfileinconversation').css('top','0px');
	}
	if(globalmenu=="wsdocument" && $("#popupcreateTaskDivbody").length!=0){
		$("#viewdownloadfileinconversation").removeClass("viewdownloadfileinconversationCls");
		$('#viewdownloadfileinconversation').css('left','230px');
		$('#viewdownloadfileinconversation').css('top','0px');
	}	
  
	
	
 }

 function thumbnailOpenUi(obj,docid,ext,feedid,place){
	var ui="";
	$('#viewthumbnail').remove();
	$('#subcommentDiv1_'+feedid).remove();
	
	ui='<div id="viewthumbnail" class="actFeedOptionsDiv d-block" style="left: 108px;top: 33%;display: none;right: unset;border-radius: 8px;">' 
		//+'<div class="arrow-left"></div>
		+'<div class="d-flex align-items-center">'
		+"<img src=\"images/conversation/expand.svg\" title=\"View\" id='expandOptionImg_"+feedid+"' onclick=\"expandImage("+docid+",'"+ext+"','"+place+"');\" class=\"image-fluid mx-2\" style=\"height: 16px; width:16px; cursor:pointer;\">"
		+"<img src=\"images/conversation/download2.svg\" title=\"Download\" id='downloadOptionImg_"+feedid+"' onclick=\"downloadActFile("+docid+","+prjid+",'"+ext+"',"+feedid+",'"+place+"')\" class=\"image-fluid mr-2 ml-1\" style=\"width: 18px; height: 18px; cursor:pointer;\">"
		+'</div>'
	+'</div>'

	$(obj).parent().append(ui);
	if(place=="task"){
		$('#viewthumbnail').css('top','0px');
	}else if(place=="WorkspaceDocument" || place=="Document"){
		$('#viewthumbnail').css('left','235px');
		$('#viewthumbnail').css('top','0px');
	}
 }

function openAttachOptions(obj,place,Id){
	
	if($(obj).parent().find('div.attachListOptions').length < 1 ){
		
		var ui='<div class="attachListOptions position-absolute rounded p-1 border" style="width: 140px;background-color: #fff;font-size: 11px;top: -58px;border-color: #c1c5c8 !important;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);left: 0px;">'

				+'<div class="d-flex p-1 justify-content-start" style="height: 24px; cursor:pointer;">'
				+'  <form enctype="multipart/form-data" style="position:absolute;top:-100px;" method="post" name="convFileUpload" id="convFileUpload">'
				+'  <input type="file" class="Upload_cLabelTitle" title="Upload" value="" onchange="readFileUrl(this,'+Id+');" name="FileUpload" id="FileUpload_'+Id+'" hidden></form>'
				+' 	<img class="img-fluid" src="images/conversation/computer.svg" style="width:20px;" onclick="setUp('+Id+')" id="mainUpload">'
				+'	<span class="pl-2" style="white-space: nowrap;" onclick="setUp('+Id+')">From Computer</span>'
				+'</div>'
				+'<div class="d-flex p-1 justify-content-start" style="height: 24px;">'
				+' 	<img class="img-fluid" style="width:20px;" src="images/conversation/folder.svg">'
				+'	<span class="pl-2" style="white-space: nowrap;" >From Repository</span>'
				+'</div>'
			+'</div>'
		$(obj).parent().append(ui);
			
	}else{
		$(obj).parent().find('div.attachListOptions').toggle();
	}
}
function TriggerAudioUI(type,id) {
	let passType=type=='post'?'main':type;
	let	UI=  "<div class=\"main_commentTxtArea    d-flex   justify-content-center  align-items-center  audioDivision rounded \" id=\"audioContainer\" style=\"display:none;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;\">"
			     +"<div  class='d-none justify-content-center  align-items-center w-100' id=\"audioArea\" style=\"display:none;\">"
			        +"<div>"
			            +"<img src=\"/images/conversation/play.svg\" onclick=\"\" id=\"customPlayer\" style=\"cursor:pointer;width:24px;\">"
			        +"</div>"
					+"<div id=\"timeline\" class='w-75 mx-2 position-relative'>"
	                    +"<img id=\"playhead\" src='images/conversation/sliderdot.svg' />"
		            +"</div>"
			        +"<audio  src=\"\" id=\"audio\" ontimeupdate=\"updateTime()\" onended=\"onVoiceEnded()\" style=\"width: 100%; height: 100%; padding: 0.7%;\"></audio>"
			         
			         +"<div id='cancelaudiodiv'><img src=\"/images/conversation/audiocancel.svg\" onclick=\"cancelVoice( '"+type+"',"+id+");\" style=\"width: 22px;cursor:pointer;\" id=\"cancelVoice\"></div>"
					 +"<div class='ml-2'>"
					 +"<div class=\"timertime\" style='white-space: nowrap;'></div>"
					 +"</div>"
			      +"</div>"
				  
				  +'<div class="playI" title="Pause recording" style=" margin-top:-20px;cursor:pointer;z-index:1;" onclick="VoiceRecordingConversationNew(false,\''+passType+'\','+id+',\'onplaypause\')">'
				  	+'<img src="images/conversation/pause.svg" style=" height: 20px;">' 
				  +'</div>'
			      
			      +"<div class=\"timer d-flex flex-column  align-items-center\" style='min-width:46px;'>" 
			       	+'<img src="/images/conversation/voiceRecord.svg" class="image-fluid" style="height:29px;cursor:pointer;"> '
			         +"<div class=\"timerSpan\">00m 00s</div>"
			      +"</div>" 
				  +'<div class="stopI" title="Stop recording" style=" margin-top:-20px;cursor:pointer;z-index:1;" onclick="VoiceRecordingConversationNew(false,\''+passType+'\','+id+')">'
   					+'<img src="images/conversation/camrecord.svg" style=" height: 20px;">' 
				  +'</div>'
		+ "</div>"
	  
	   $(".audioDivision").remove();

	   if(globalmenu=='Idea' || globalmenu=='Task' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
			
			if(id == 0){
				$(UI).insertAfter('#ProjIdForCommentreply').show('slow');
			}else{
				$(UI).insertAfter('#actFeedDiv_'+id).show('slow');
				$('#audioArea').css('z-index','10');
			}
			
	   }else{
		if(type=='post'){
			cancelVoice('reply',0);
		   $(UI).insertAfter('#ProjIdForComment').show('slow');
		}else{
				cancelVoice('post',id);
			$(UI).insertAfter('#ProjIdForCommentreply').show('slow');
		}
	   }
	  	
	   $('#customPlayer').attr('src','images/conversation/play.svg');
    
	   $('#customPlayer').click(function(){
				if($(this).attr('src').indexOf('play.svg') > 0){
					$(this).attr('src','images/conversation/pause.svg');
					$("#audio")[0].play();
				}	
				else{
					$(this).attr('src','images/conversation/play.svg');
					$("#audio")[0].pause(); 
				}        	
		});
}
function postAudio(projId,feedId,postMenuType,parent_feed_id){
	if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
		imgmenuTypeId=$("#task_id").val();
		postMenuType="Task";
	}else if(globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
		imgmenuTypeId=$(".popupComments").attr('sourceid');
		postMenuType = globalmenu=='Idea'?"wsIdea":globalmenu=='agile'||SBmenu=="SBstories"||globalmenu == 'sprint'?"agile":globalmenu=="wsdocument"?"wsDocument":"";
		commentPlace = globalmenu=='agile' || SBmenu=="SBstories" ? "Story" : globalmenu == 'sprint' ? "Sprint" : "";
	}
		Fr.voice.export(function(blob){
		    	
		        var formData = new FormData();
		        formData.append('file', blob);
				formData.append('user_id', userIdglb);
				formData.append('company_id', companyIdglb);
				formData.append('resourceId', parent_feed_id);
				formData.append('projId', projId);
				formData.append('place', "convodoc");
				formData.append('convotype', postMenuType);
				formData.append('menuTypeId',imgmenuTypeId);
				formData.append('subMenuType', commentPlace);
		      
		       // alert("menutypeId----->>"+menutypeId);
			   $('#loadingBar').addClass('d-flex');
			    timerControl("start");
		        $.ajax({
		          ///url: path+'/DummyFileUpload?projId='+projId+'&userId='+userId+'&feedId='+feedId+'&postMenuType='+postMenuType+'&subMenuType='+commentPlace+'&menutypeId='+menutypeId+'&localOffsetTime='+localOffsetTime,
		          url: apiPath+"/"+myk+"/v1/upload", 
				  type: 'POST',
		          //dataType:'text',
				  processData: false,
                  contentType: false,
                  cache: false,
		          data: formData,
		          error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $('#loadingBar').addClass('d-none').removeClass('d-flex');
							//timerControl("");
							},
		          contentType: false,
		          processData: false,
		          success: function(url) {
		        	  	
		          //alert("URL Activityfeed----"+url);
		          
		          /*if(postMenuType=='blog'){
		        	    checkSessionTimeOut(url);		        		
		        		loadBlogPostData(projectId);		        				        		
		        		$('#audioContainer , div.timer').hide(); //hiding audio container
		     	     	$('#main_commentTextarea').show();
		             }else{*/
		            	//alert("Ok done...."+url);
			             // $("div#activityFeedList").html('');
			             $('#loadingBar').addClass('d-none').removeClass('d-flex');
						 //timerControl("");
			        	 
			        	 //url = jQuery.parseJSON(url)
			        	  //alert("Conversation Url---"+url);
						  feedactionid = url;
			        	  //var jsonData = eval(url);
			        	  //alert("jsonData---"+jsonData);
			        	 // $('div#audioContainer , div.timer').removeClass('d-flex').addClass("d-none");
						 if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
							fetchPostedThingFiles(0,'files')
						 }else if(globalmenu=='Idea'){
							openComments(imgmenuTypeId,url,parent_feed_id);
							//cancelVoice('reply',parent_feed_id);replyDivContainer_3066
							/* if(parent_feed_id!=0){
								$('#replyDivContainer_'+parent_feed_id).hide();
							} */
							handleAudiopost(imgmenuTypeId,parent_feed_id);
							if(!$('#ideacomment_'+imgmenuTypeId).is(':visible')){
								$('#ideacomment_'+imgmenuTypeId).css('display','block');
							}
						}else if(globalmenu=='agile' || SBmenu=="SBstories"){
							agileComments(imgmenuTypeId,"",url,parent_feed_id,'sub');
							commentPlace="";
							handleAudiopost(imgmenuTypeId,parent_feed_id);
						}else if(globalmenu=='sprint'){
							sprintComments(imgmenuTypeId,"",url,parent_feed_id,'sub');
							commentPlace="";
							handleAudiopost(imgmenuTypeId,parent_feed_id);
						}else if(globalmenu=="wsdocument"){
							var typeDoc1 = parent_feed_id==0||parent_feed_id=="0"?"":"sub";
							wsDocComments(imgmenuTypeId,"",url,parent_feed_id,typeDoc1);
							handleAudiopost(imgmenuTypeId,parent_feed_id);
						}else{
								$('div#audioContainer , div.timer').remove();
								$('#main_commentTextarea').show();
								$('textarea#main_commentTextarea').val('');
								$("div#replySubCommentDiv1_"+feedId).hide();
								$('.convopost').attr('onclick',"CommentActFeed("+prjid+",'activityFeed')");
								//alert("postMenuType--1--:::::"+postMenuType);
								//if(feedId == "0"){
								  
							   /*  }else{
								  loadActivityFeed('','');  
							  } */
							  if(postMenuType=='Task'){
								  loadfeednew(postMenuType);
								  //showTaskComments(myTaskId,prjid);
							  }else if(postMenuType=='agile'){
								  fetchAgileComments(myTaskId);
							  }else{
							  //	loadActivityFeed('', 'firstClick',postMenuType);
								  let place = parent_feed_id == 0?'main': "reply";
								fetchPostedThingFiles(url,place,parent_feed_id)
							  }
						}
			        	  
		            	  
		             //}
		          
		          
		          
		          }
		        });
		      }, "blob");
}

function handleAudiopost(imgmenuTypeId,parent_feed_id){
	$('div#audioContainer , div.timer').remove();
	$('#main_commentTextarea, #replyBlock_0').show();
	$('textarea#main_commentTextarea').val('');
	$(".voiceFeed").attr('onclick',"VoiceRecordingConversationNew(true,'main',0)");
	$(".voiceFeed").css('background-color','')
	$('#replyDivContainer_'+parent_feed_id).find('.postButton').attr('onclick',"postReply("+parent_feed_id+",'',"+imgmenuTypeId+")").attr('src', '/images/conversation/post1.svg');
	$('#replyDivContainer_0').find('.postButton').attr('onclick',"postReply(0,'',"+imgmenuTypeId+")").attr('src', '/images/conversation/post1.svg');
	$('#replyDivContainer_0').find('.audioreply').attr('onclick','VoiceRecordingConversationNew(true,"reply",0)');
	$('#replyDivContainer_0').find('.postButton').attr('onclick',"postReply(0,'',"+$('.ideaComments').attr('ideaid')+")").attr('src', '/images/conversation/post1.svg');
	if(parent_feed_id!=0){
		$('#replyDivContainer_'+parent_feed_id).hide();
	}
}

function fetchPostedThingFiles(latestId,place,parent_feed_id,dragndrop,from,project,listTaskId) {
    var localOffsetTime=getTimeOffset(new Date());
	let feedType="";
	let task_id="";
	
	//alert("globalmenu"+globalmenu);
	if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
		feedType="Task";
		task_id=$("#task_id").val();
		if($("#task_id").length==0){
			task_id=listTaskId;
		}
	}
	
	var  jsonbody ="";
	
	if(from!='Task'){
	  jsonbody = {
            "user_id":userIdglb,
            "company_id":companyIdglb,
            "project_id":prjid,
            "localOffsetTime":localOffsetTime,
            "action":"fetchinsertFeed",
			"latestFeed":latestId,
            "parent_feed_id":""+parent_feed_id+"",
			"feedType":feedType,
			"task_id":task_id
        }
	}else{
		jsonbody = {
            "user_id":userIdglb,
            "company_id":companyIdglb,
            "project_id":project,
            "localOffsetTime":localOffsetTime,
            "action":"FetchConvTaskActivityFeed",
			"feedId":latestId,

        }
	}
	$('#loadingBar').addClass('d-flex');	
	 $.ajax({
            url: apiPath+"/"+myk+"/v1/getFeedDetails",
            type:"POST",
            dataType:'json',
            contentType:"application/json",
            data: JSON.stringify(jsonbody),
            //data:{act:"loadWrkspaceActivityFeeds", userId:userId, projectId:projectId, sortType:sortBy, currFeedId:"0",
            //index:index, limit:limitAct,localOffsetTime:localOffsetTime,txt:searchText,status:status,feedId:feedId,from:from,index1:index1},
            beforeSend: function (jqXHR, settings) {
                xhrPool.push(jqXHR);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                        checkError(jqXHR,textStatus,errorThrown);
                        $('#loadingBar').addClass('d-none').removeClass('d-flex');
                        //timerControl("");
                        },
		 success: function (result) {
			 let UI=prepareCommentsUI(result, menuutype, prjid,'');
			 
			 if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){

				if(from==='list'){

					if($(".task_comment_div_"+listTaskId).html()!=''){
						$(".task_comment_div_"+listTaskId).hide(); 
						$(".task_comment_div_"+listTaskId).html('');
					}else{
						if(result!=""){
							$(".task_comment_div_"+listTaskId).html(''); 
							$(".task_comment_div_"+listTaskId).append(UI);
							$(".task_comment_div_"+listTaskId).show();
						 }else{
							AddSubComments('0','list',listTaskId);
						 }
					}
				}else{
					if($(".task_comment_div").find('.task_comment_list_div').html()!='' && place!='files'){	
						$(".task_comment_div").hide().find('.task_comment_list_div').html('');
						$(".task_comment_div").find('.task_comment_list_div').css('max-height','270px');
					}else if($(".task_comment_div").find('.task_comment_list_div').html()!='' && place=='files'){
						$(".task_comment_div").find('.task_comment_list_div').html('');
							AddSubComments(0,'','Task');
							//$(".task_comment_div").find('.task_comment_list_div').html(''); 
							$(".task_comment_div").find('.task_comment_list_div').append(UI);
							$(".task_comment_div").find('.task_comment_list_div').css('max-height','470px');
							var elmnt = document.getElementById("taskcommentmain");
							elmnt.scrollIntoView({
						 		 behavior: 'smooth', block: 'nearest', inline: 'start' 
					 		});
					}
					else{
						$(".task_comment_div").show();
						$(".task_user_history").hide();
						$(".dependency_comment_div").hide();
						if(result!=""){
							AddSubComments(0,'','Task');
							//$(".task_comment_div").find('.task_comment_list_div').html(''); 
							$(".task_comment_div").find('.task_comment_list_div').append(UI);
							// console.log(result);
							
							var taskfedd= result[0].taskForActFeed;
							var feddiid= result[0].activityfeed_id;
							// console.log(feddiid+"---------"+taskfedd);
							if(feddiid==taskfedd){
								$('#actFeedDiv_'+feddiid).addClass('changeColorActFeed');
							}
							//$('#aFeed1_'+feddiid).removeAttr('onmouseover');//aFeed1_49479
							//commentDataValidation(result[0].subCommentsData);

							$(".task_comment_div").find('.task_comment_list_div').css('max-height','470px');
							var elmnt = document.getElementById("taskcommentmain");
							elmnt.scrollIntoView({
						 		 behavior: 'smooth', block: 'nearest', inline: 'start' 
					 		});
							
						 }else{
							if(from!='Task'){
								AddSubComments(0,'','Task');
							}
						 }
					}
				}

				
			 }else{
				callsocket(result);
				if(place=='main'){
					$("#activityFeedList").prepend(UI);
				}else{
					if(dragndrop=="dragndrop" || dragndrop=="copyPaste"){
							   $(UI).insertAfter("#actFeedDiv_" + result[0].parent_feed_id);
							   $('#replyDivContainer_' + parent_feed_id).remove();
					}
					else{
					   $('#replyDivContainer_' + parent_feed_id).replaceWith(UI);
					}
					
				}
			 }
			 $('#loadingBar').addClass('d-none').removeClass('d-flex');
             //timerControl("");
			 if(from=='Task'){
				$(".actFeedHover").removeAttr('ondblclick') ;
				$(".actFeedHover").removeClass('actFeedHover');

			 }
		     
		 }
	 });
	
}

function commentDataValidation(json,feedid){
	if(json!=""){
		for(i=0;i<json.length;i++){
			var actFeedIdNew = json[i].activityfeed_id;
			if(actFeedIdNew==feedid){
				$("#addTaskDiv").find('#actFeedDiv_'+actFeedIdNew).addClass('changeColorActFeed');
			}
			
			if(json[i].subCommentsData!=[]){
				commentDataValidation(json[i].subCommentsData,feedid);
			}
		}
		
	}
}

function viewdocument(docid,ext,place){
	var fileName = "";
	var ui="";
	let space="projectDocuments";
	if(place=='Document'){
		space="uploadedDocuments"
	}
	if(ext == "docx" || ext == "doc" || ext == "xlsx" || ext == "xls"){
		fileName =  "https://docs.google.com/gview?url="+lighttpdpath+"//"+space+"//"+docid+"."+ext+"&embedded=true";
		ui='<iframe id="viewdocumentembed" src='+fileName+' frameborder="0" class="w-100 h-100"></iframe>'
	}else{ 
		fileName =  lighttpdpath+"//"+space+"//"+docid+"."+ext;
		ui='<embed id="viewdocumentembed" src='+fileName+' frameborder="0" class="w-100 h-100">'
	}
	
	$('#modalbodyviewdocumentembed').append(ui);
	$('#triggerviewdocument').trigger("click");
	//window.open(fileName,'_blank');
	
 }

 function cleardocument(){
	if(placefordocview!="idea" && placefordocview!="agile"){
		$('#viewdocumentembed').remove();
		$('#myModal4').hide();
		$('#transparentDiv').hide();
	}else{
		$('#viewdocumentembed').remove();
		$('#myModal4').hide();
	}
	if(globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
		$('#transparentDiv').show();
	}
	if(globalwb == 'wb'){
		$('#transparentDiv').show();
	}
	
 }

 function downloadActFile(docId,projId,ext,id,place){
	var filename = id+"."+ext;
	if(place=="WorkspaceDocument"){
		URLForDownLoad = lighttpdpath+"/projectDocuments/"+filename;
	}else if(place=="Document"){
		URLForDownLoad = lighttpdpath+"/uploadedDocuments/"+filename;
	}
	else{
		if(projId == "0"){
			//URLForDownLoad = path+"/repositoryAction.do?action=downloadfile&docId="+docId;
		}else{
			URLForDownLoad = apiPath+"/"+myk+"/v1/download?fileName="+filename+"&Path=projectDocuments&place=coversation&docId="+docId+"&companyId="+companyIdglb+"&userId="+userIdglb+"&projectId="+projId+"";
		}
	}
	window.open(URLForDownLoad);
	//parent.confirmFun(getValues(companyAlerts,"Alert_Download_Doc"),'clear','dowloadActFile');
}

/* function dowloadActFile(){
	
	window.open(URLForDownLoad);
} */

function expandImage(docid,ext,place){
	var modal = document.getElementById("myModal1");
	var tdiv = document.getElementById("transparentDiv");
	var modalImg = document.getElementById("img01");
	let space="projectDocuments"
	if(place=='Document'){
		space="uploadedDocuments";
	}
	var imgsrc = lighttpdpath+"//"+space+"//"+docid+"."+ext;
	modal.style.display = "block";
	tdiv.style.display = "block";
	modalImg.src = imgsrc;
	
}

function closeimagepreview(){
	if(placefordocview!="idea" && placefordocview!="agile"){
		var modal = document.getElementById("myModal1");
		var tdiv = document.getElementById("transparentDiv");
		modal.style.display = "none";
		tdiv.style.display = "none";
		$('#img01').css('width','500px');
	}else{
		var modal = document.getElementById("myModal1");
		modal.style.display = "none";
		$('#img01').css('width','500px');
	}	
	if(globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
		$("#transparentDiv").show();
	}
	if(globalwb == 'wb'){
		$("#transparentDiv").show();
	}
	
}

 function cancelVoice(type,id){
		if(type.trim()=='post'){
			    $('div#audioContainer , div.timer').remove();
			    $('#main_commentTextarea').show();
			    $('textarea#main_commentTextarea').val('');
				$(".voiceFeed").attr('onclick',"VoiceRecordingConversationNew(true,'main',0)");
				$(".voiceFeed").css('background-color','')
				$('.convopost').attr('onclick',"CommentActFeed("+prjid+",'activityFeed')").attr('src', '/images/conversation/post1.svg');
		}else if((globalmenu=='Idea' || globalmenu=='Task'  || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument") && type.trim()=='reply'){
			$('div#audioContainer , div.timer').remove();
			$('#main_commentTextarea, #replyBlock_'+id+'').show();
			$('textarea#main_commentTextarea').val('');
			$(".voiceFeed").attr('onclick',"VoiceRecordingConversationNew(true,'main',0)");
			$(".voiceFeed").css('background-color','')
			$('#replyDivContainer_0').find('.audioreply').attr('onclick','VoiceRecordingConversationNew(true,"reply",0)');
			if(globalmenu=="wsdocument" || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint'){
				$('#replyDivContainer_0').find('.postButton').attr('onclick',"postReply(0,'',"+$('.popupComments').attr('sourceid')+")").attr('src', '/images/conversation/post1.svg');
			}else{
				$('#replyDivContainer_0').find('.postButton').attr('onclick',"postReply(0,'',"+$('.ideaComments').attr('ideaid')+")").attr('src', '/images/conversation/post1.svg');
			}
			
			if(id!=0){
				$('#replyDivContainer_'+id).remove();
			}  
			
			
		} else {
			 
			$('div#audioContainer , div.timer').remove();
			//  $('#main_commentTextarea').show();
			//  $("[id^=replyDivContainer_]").show();
			//  $('.postButton').attr('onclick',"postReply("+id+")");
			//  $(".audioreply").attr('onclick',"VoiceRecordingConversationNew(true,'reply',"+feedId+")");
			//  $(".audioreply").css('background-color','')
		//	$("#replyDivContainer_"+id).find('.audioDivision').remove();
			$('#replyBlock_'+id).show();
			$('#replyBlock_'+id).val('');
			$('.postButton').attr('onclick',"postReply("+id+")").attr('src', '/images/conversation/post1.svg');;
			$(".audioreply").attr('onclick',"VoiceRecordingConversationNew(true,'reply',"+id+")");
			$(".audioreply").css('background-color','');
		}
}
 
function callsocket(result,type){
	if(type=='delete'){
		socket.emit('delete', result);
	}else{
		socket.emit('newfeed', result);
	}
}
  function prepareUIForLatestFeedNew(result) {
	 	 let UI = prepareCommentsUI(result, "activityfeed",prjid, '','highlight');
		 
			if (result[0].parent_feed_id == '0') {
				$("#activityFeedList").prepend(UI);
			} else {
				$(UI).insertAfter("#actFeedDiv_" + result[0].parent_feed_id);
			}
    }


 function openVideo(id,ext,projectId,src){
	var source = "";
	   
   if(projectId == "0"){
	   source =  lighttpdpath+"//uploadedDocuments/"+id+"."+ext;
   }
   else if(projectId == "1"){
		source =  src;
	}else{
	   source =  lighttpdpath+"//projectDocuments/"+id+"."+ext;
   }
   
   if( ext.toLowerCase() == "mp4" || ext.toLowerCase() == "m4v" || ext.toLowerCase() == "mov" || ext.toLowerCase() == "flv" || ext.toLowerCase() == "f4v" || ext.toLowerCase() == "ogg" || ext.toLowerCase() == "ogv" || ext.toLowerCase() == "wmv" || ext.toLowerCase() == "vp6" || ext.toLowerCase() == "vp5" || ext.toLowerCase() == "mpg" || ext.toLowerCase() == "avi" || ext.toLowerCase() == "mpeg" || ext.toLowerCase() == "webm"){
		  document.getElementById('recAudio').pause();
		  $("#recVideo").html(""); 
		  //$("#previewDiv").show().children('#recAudio').hide().parent().children('#recVideo').attr('src',source).show();
		  $("#previewDiv").show() 
		  $("#previewDiv").find("#recVideo").html("<source src='"+source+"'>").show(); 
		  document.getElementById('recVideo').play();
   }else{
	   
		  document.getElementById('recVideo').pause();
		  //$("#previewDiv").css({"height":""});
		  $("#previewDiv").show().children('#recVideo').hide().parent().children('#recAudio').attr('src',source).show();
		  //$('#recAudio').css({"height":"","width":"50%"});
		  document.getElementById('recAudio').play();
   }
}

function camUiPopup(feedid){
	var ui="";
	$('#camUiPopup').html('');
	$('#transparentDiv').show();

	ui="<div  id=\"myModal2\" class=\"modal d-block\" role=\"dialog\" style=\"right:0px !important;\">"
	+"<div class=\"modal-dialog modal-dialog-centered\" style=\"width: 800px;max-width: fit-content;\">"
		+"<div class=\"modal-content w-100\" id =\"camBody\" style=\"display:block;border-radius:0px;margin: 0px auto;background-color: #2B2C4B;\">"
			+"<div class=\"modal-header d-flex justify-content-center\" style=\"border-bottom:0px\">"
			  +"<button onclick=\"stopWebcam()\" type=\"button\" style=\"color: white;padding-top: 22px;padding-right: 0px;outline: none;\" class=\"close\" data-dismiss=\"modal\">&times;</button>"
			  +"<h5 class=\"modal-title\" style=\"font-weight:bold;color: white;\">Photo Capture</h5>"
			+"</div>"
			
			+"<div class=\"modal-body w-100\" >"
				+"<div  class=\"\" style=\"\">"
					+"<div id=\"recordImage\" class=\"row\" style=\"display:none;padding-left: 15px;padding-right: 15px;\">"
						+"<video class=\"w-100\" style=\"border: 1px solid white;\" onclick=\"snapshot(this);\" class=\"mysnapShot\" id=\"video\" autoplay muted></video>"
					+"</div>"
					+"<div id=\"camImage\" class=\"row\" style=\"display:none;padding-left: 15px;padding-right: 15px;\">"
						+"<canvas  id=\"myCanvas\" style=\"border: 1px solid white;\" class=\"canvasForSnap\" style=\"\"></canvas>"  
					+"</div>"
			   +"</div>"
			
			+"</div>"
			
			+"<div class=\"\" style=\"border-top:0px;padding-top: 0px;height: 65px;\" align=\"center\">"
				+"<div class=\"\">"
					+"<img id = \"cam\" title=\"Camera\" class=\"mx-2\" onclick=\"snapshot(this);\" style = \"cursor:pointer;height:30px;\" src=\"images/conversation/cam1.svg\">"
					+"<img id = \"videoCam\" title=\"Video Record\" class=\"mx-2\" onclick=\"videoType('video');\" style = \"cursor:pointer;height:30px;margin-top: 2px;width: 35px;\" src=\"images/document/cam.svg\">"
					+"<img id = \"modalclose\" title=\"\" class=\"mx-2\" class=\"modalclose\" onclick=\"\" style = \"display:none;cursor:pointer;height:30px;width:35px\" src=\"\">"
					//+"<img id = "videoCam"  class=\"modalclose" onclick=\"test('video');\" style = "cursor:pointer;height:40px;width:40px" src=\"'+path+"/images/videoIcon.svg">" 
				+"</div>"
				
				
			+"</div>"
			
			
		+"</div>"
		
		+"<div class=\"modal-content w-100\" id=\"videoBody\" style=\"display:none;margin: 0px auto;background-color: #2B2C4B;\" >"
			+"<div class=\"modal-header d-flex justify-content-center border-0\">"
			  //<button onclick=\"so()" type=\"button" class=\"close" data-dismiss=\"modal">&times;</button>" 
			  +"<button onclick=\"stopWebcam()\" type=\"button\" class=\"close\" style=\"color: white;padding-top: 22px;padding-right: 0px;\" data-dismiss=\"modal\">&times;</button>"
			  +"<h5 class=\"modal-title\" style=\"font-weight:bold;color: white;\">Video Capture</h5>"
			+"</div>"
			
			+"<div class=\"modal-body w-100\" >"
				+"<div class=\"\">"
				+"<video class=\"w-100\" style=\"border: 1px solid white;\" id=\"videoCapture\" autoplay muted></video>"
				+"</div>"
				
				+"<div class=\"\">"
			 		+"<video class=\"w-100\" style=\"border: 1px solid white;\" id=\"recorded\" style=\"display:none;float:right\" controls></video>"
				+"</div>"
			+"</div>"
			
			    +"<div class=\"modal-footer border-0\" style=\"display: flex;place-content: center;border-top:0px;padding-top: 0px;height: 65px;\">"
					+"<img id =\"videocancel\" title=\"Cancel\" class=\"mx-2\" onclick=\"uploadCancel();\" style=\"cursor: pointer; height: 30px; width: 30px;\" src=\"images/conversation/cancel.svg\">"
					+"<img id=\"download\" title=\"Save\" class=\"mx-2\" class=\"modalclose\" onclick=\"uploadVideo();\" style=\"margin-left: 4px;display: inline-block; cursor: pointer; height: 30px; width: 30px;\" src=\"images/conversation/save.svg\" data-dismiss=\"modal\">"
					//+"<img id = \"cam1\" title=\"Camera\" class=\"mx-2\" onclick=\"snapshot(this);\" style = \"cursor:pointer;height:30px;width:35px\" src=\"images/conversation/cam1.svg\">"
					//+"<img id = \"videoCam1\" title=\"Video Record\" class=\"mx-2\" onclick=\"videoType('video');\" style = \"cursor:pointer;height:40px;width:35px;margin-top: 1px;\" src=\"images/conversation/cam2.svg\">"
					+"<img src=\"images/conversation/recordicon-inline.svg\" class=\"mx-2\" title=\"Start Recording\" style=\"float:right;cursor:pointer;height: 30px;\" id=\"record1\" >"
				    +"<img src=\"images/conversation/camrecord.svg\" class=\"mx-2\" title=\"Stop Recording\" style=\"float:right;cursor:pointer;display:none;height: 30px;\" id=\"stop1\" >"
				+"</div>"
		  +"</div>"
		
	+"</div>"
	+"</div>"

	$('#camUiPopup').append(ui).show();

	if(typeof(feedid)=="undefined"){
		feedid="";
	}

	initWebcam(feedid,"",prjid,menuutype);

}

function postCanvasToURL() {
	var projId=prjid;	
	var menuId="";
	if(takePic == "1"){		
	  var imageData =  canvas.toDataURL('image/png');
	  var file = dataURItoBlob(imageData);
	  var feedId;		
	   if(imgfeedId === ""){				
		   feedId=0;	    		
	   }else {	    		 
		   feedId=imgfeedId;	    		 
	   }
	
	  var localOffsetTime=getTimeOffset(new Date());
	  
	  
	  var formData = new FormData();
	  
	  var imgComntType;
	  imgComntType=imageMenuType;
	  if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
		imgmenuTypeId=$("#task_id").val();
		imageMenuType="Task";
	  }else if(globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
		imgmenuTypeId = $(".popupComments").attr('sourceid');
		imageMenuType = globalmenu=='Idea'?"wsIdea":globalmenu=='agile'||SBmenu=="SBstories"||globalmenu == 'sprint'?"agile":globalmenu=="wsdocument"?"wsDocument":"";
		commentPlace = globalmenu=='agile' || SBmenu=="SBstories" ? "Story" : globalmenu == 'sprint' ? "Sprint" : "";
	  }	
	  var today = new Date();
	  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	  var dateTime = date+'_'+time;
	  formData.append('file',  file, "image_"+dateTime+".png");
	  formData.append('user_id', userIdglb);
	  formData.append('company_id', companyIdglb);
	  formData.append('resourceId', feedId);
	  formData.append('projId', prjid);
	  formData.append('place', "convofile");
	  formData.append('menuTypeId', imgmenuTypeId);
	  formData.append('convotype', imageMenuType);
	  formData.append('subMenuType', commentPlace);
	  $('#loadingBar').addClass('d-flex');	
  $.ajax({
			url: apiPath+"/"+myk+"/v1/upload", 
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			data: formData,
			dataType:'text',
			data: formData,
			error: function(jqXHR, textStatus, errorThrown) {
					  checkError(jqXHR,textStatus,errorThrown);
					  $('#loadingBar').addClass('d-none').removeClass('d-flex');
					  //timerControl("");
					  },
			contentType: false,
			processData: false,
			success: function(url) {		         
			  
			     
				$('#loadingBar').addClass('d-none').removeClass('d-flex');
			 // timerControl(""); 
			 if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
				fetchPostedThingFiles(0,'files');
			 }else if(globalmenu=='Idea'){
				openComments(imgmenuTypeId,url,feedId);
				if(!$('#ideacomment_'+imgmenuTypeId).is(':visible')){
					$('#ideacomment_'+imgmenuTypeId).css('display','block');
				}
				$("#transparentDiv").show();
			 }else if(globalmenu=='agile' || SBmenu=="SBstories"){
				agileComments(imgmenuTypeId,"",url,feedId,'sub');
				commentPlace="";
			 }else if(globalmenu=='sprint'){
				sprintComments(imgmenuTypeId,"",url,feedId,'sub');
				commentPlace="";
			 }else if(globalmenu=="wsdocument"){
				var typeDoc1 = feedId==0||feedId=="0"?"":"sub";
				wsDocComments(imgmenuTypeId,"",url,feedId,typeDoc1);
			 }else{
				$("#main_commentTextarea").val('');
				$("textarea.main_commentTxtArea").val('');
				$("replyBlock_"+feedId+"").val('');
				$("#actFeedOptPopDiv_"+feedId).hide();  
				$(".replycommentDiv").hide();
				 
				$('#FileUpload').val('');
				$('input[id^=FileUpload_]').val('');
				//$("#actFeedOptPopDiv_"+feedId).hide();
				//url = jQuery.parseJSON(url)
				feedactionid = url;
				if(imageMenuType=='Task'){
					loadfeednew(imageMenuType);
				}else if(imageMenuType=='agile'){
					fetchAgileComments(imgmenuTypeId);
				}else{
					//loadActivityFeed('', 'firstClick',imageMenuType);
					let parent_feed_id=feedId;
					let place = parent_feed_id == 0?'main': "reply";
					 fetchPostedThingFiles(url,place,parent_feed_id)
				}
			 }

			 if($('#otherModuleTaskListDiv').is(':visible') == true || globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
				$("#transparentDiv").show();
			 }
			  
			  //loadActivityFeed(notFolid, 'firstClick');
			}
			
		  }); 
		  stopWebcam();
	   
  }else{
	  $('.modalclose').attr("data-dismiss", "");
	  alertFun(getValues(companyAlerts,"Alert_Snapshot"),'warning'); 
  
  }
  
}

function uploadVideo(){			
	if(takeVid == "1"){
		var feedId;			
		 if(imgfeedId === ""){				
			 feedId=0;	    		
		 }else{	    		 
			 feedId=imgfeedId;	    		 
		 }			
		//alert("feedId>>>>>>>>>>>>>>>"+feedId);			
		//console.log("imageMenuType:::::::::::::"+imageMenuType);
		//console.log("userId:::::::::::::"+userId);
		//console.log("companyId:::::::::::::"+companyId);
		//console.log("imageProjId:::::::::::::"+imageProjId);
		 
		 
		if(typeof(imageMenuType)=="undefined"){
			imageMenuType="";
		}   
		
		
		
		var localOffsetTime=getTimeOffset(new Date());

		if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
			imgmenuTypeId=$("#task_id").val();
			imageMenuType="Task";
		  }else if(globalmenu=='Idea' || globalmenu=='agile' || SBmenu=="SBstories" || globalmenu=='sprint' || globalmenu=="wsdocument"){
			imgmenuTypeId=$(".popupComments").attr('sourceid');
			imageMenuType = globalmenu=='Idea'?"wsIdea":globalmenu=='agile'||SBmenu=="SBstories"||globalmenu == 'sprint'?"agile":globalmenu=="wsdocument"?"wsDocument":"";
			commentPlace = globalmenu=='agile' || SBmenu=="SBstories" ? "Story" : globalmenu == 'sprint' ? "Sprint" : "";
		  }	
		
			
		var formData = new FormData();
		
		var today = new Date();
		var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
		var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		var dateTime = date+'_'+time;  
		formData.append('file',  captureVideo, "video_"+dateTime+".mp4");
		formData.append('user_id', userIdglb);
		formData.append('company_id', companyIdglb);
		formData.append('resourceId', feedId);
		formData.append('projId', prjid);
		formData.append('place', "convofile");
		formData.append('menuTypeId', imgmenuTypeId);
		formData.append('convotype', imageMenuType);
		formData.append('subMenuType', commentPlace);
		$('#loadingBar').addClass('d-flex');
	$.ajax({		         			     
			  url: apiPath+"/"+myk+"/v1/upload", 
			  type: 'POST',
			  //dataType:'text',
			  processData: false,
			  contentType: false,
			  cache: false,
			  data: formData,
			  error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$('#loadingBar').addClass('d-none').removeClass('d-flex');
						timerControl("");
						},
			  contentType: false,
			  processData: false,
			  success: function(url) {
				
				/* if(name != "" && mainFeedId != 0){
					  fee = url.split('#@#@')[0];
					  fee = fee.substring(1, fee.length-1);
					  fee1 = url.split('#@#@')[1];
					  fee1 = fee1.substring(1, fee1.length-1);
					  url = "[" + fee +","+ fee1 + "]";
				  }else{
					  url = url;
				  } 	 */          
				$('#loadingBar').addClass('d-none').removeClass('d-flex');
				//timerControl("");
				if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
					fetchPostedThingFiles(0,'files')
				}else if(globalmenu=='Idea'){
					openComments(imgmenuTypeId,url,feedId);
					if(!$('#ideacomment_'+imgmenuTypeId).is(':visible')){
						$('#ideacomment_'+imgmenuTypeId).css('display','block');
					}
					$("#transparentDiv").show();
				}else if(globalmenu=='agile' || SBmenu=="SBstories"){
					agileComments(imgmenuTypeId,"",url,feedId,'sub');
					commentPlace="";
				}else if(globalmenu=='sprint'){
					sprintComments(imgmenuTypeId,"",url,feedId,'sub');
					commentPlace="";
				}else if(globalmenu=="wsdocument"){
					var typeDoc1 = feedId==0||feedId=="0"?"":"sub";
					wsDocComments(imgmenuTypeId,"",url,feedId,typeDoc1);
				}else{
					$("#main_commentTextarea").val('');
					$("textarea.main_commentTxtArea").val('');
					$("replyBlock_"+feedId+"").val('');
					$("#actFeedOptPopDiv_"+feedId).hide();  
					$(".replycommentDiv").hide();
					
					
					feedactionid = url;
					if(imageMenuType=='Task'){
						loadfeednew(imageMenuType);
					}else if(imageMenuType=='agile'){
						fetchAgileComments(imgmenuTypeId);
					}else{
						//loadActivityFeed('', 'firstClick',imageMenuType);
						let parent_feed_id = feedId;
						let place = parent_feed_id == 0?'main': "reply";
						fetchPostedThingFiles(url,place,parent_feed_id);
					}
				}

				
		
		//loadActivityFeed(notFolid, 'firstClick');
	  }
	  
	}); 
		stopWebcam();
		$('#videoBody').hide();
		//$('.fade').css({'opacity':'0','display':'none'});
		$('#myModal2').attr('aria-hidden','true');
		//$('.modal-open').css({'overflow-y':'auto'});
	 
}else{
	$('.modalclose').attr("data-dismiss", "");
	alertFun(getValues(companyAlerts,"Alert_Snapshot"),'warning'); 

}

}
function deleteMainActivityFeedNew(mainFeedId ,userIdglb,menuutype) {
	 confirmFunNew(getValues(companyAlerts,"Alert_DelActFeed"),"delete","deleteActFeed",mainFeedId,userIdglb,menuutype);	
	
}
function deleteActFeed(mainFeedId,userIdglb,menuutype){
	$('#loadingBar').addClass('d-flex').removeClass('d-none');  
	if(menuutype=="Idea"){
		menuutype="wsIdea";
	}
	if(menuutype=="sprint"){
		menuutype="agile";
	}
	if(menuutype=="wsdocument"){
		menuutype="wsDocument";
	}
	var subMenuType = globalmenu=='agile' || SBmenu=="SBstories" ? "Story" : globalmenu == 'sprint' ? "Sprint" : "";
	$.ajax({
		url: apiPath + "/" + myk + "/v1/deleteFeed?feed_id=" + mainFeedId + "&user_id=" + userIdglb + "&place=" + menuutype + "&subMenuType="+subMenuType+"",
		type: "DELETE",
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$("#loadingBar").hide();
			timerControl("");
		},
		success: function (data) {
	    		
		$('#loadingBar').addClass('d-none').removeClass('d-flex');  
			
			if (data == 'failure') {
				//parent.alertFun("You dont have permission to delete this feed","warning");
				// parent.$('#loadingBar').hide();
				// parent.timerControl("");
			} else {
				callsocket(mainFeedId, 'delete');
				$('#deleteFeed_' + mainFeedId).remove();
				$("#actFeedMainDiv_" + mainFeedId).remove();
				if(globalmenu == 'sprint'){
					menuutype="sprint";
				}
				
			}
			if(menuutype=="wsDocument"){
				menuutype="wsdocument";
			}
			if(globalmenu=='Task' || globalmenu=='Idea'){
				changeImage('comment');
			}
		}
	});
}

function zoomin(){
	var myImg = document.getElementById("img01");
	var currWidth = myImg.clientWidth;
	var hg1 = $("#img01parent").height()-10;
	var hg2 = $("#img01").height();
	if(hg1<=hg2){
		return false;
	}else{
		myImg.style.width = (currWidth + 100) + "px";
	} 
}
function zoomout(){
	var myImg = document.getElementById("img01");
	var currWidth = myImg.clientWidth;
	if(currWidth == 100) return false;
	 else{
		myImg.style.width = (currWidth - 100) + "px";
	}
}

function popupCommentsUI(sourceid,title,place){
	var ui="";
	var imgsrc = place=="Idea"?"/images/menus/ideas.svg":place=="Agile"?"/images/menus/agile.svg":place=="Sprint"?"images/agile/sprint_white.svg":place=="Story"?"images/agile/story_white.svg":place=="Document"?"/images/menus/doc.svg":"/images/menus/agile.svg";
	
	$('#glbCommentsDiv').html('');
    $("#transparentDiv").show();
    
    ui='<div class="popupComments" sourceid='+sourceid+'>'
        +'<div class="" style="">'
        +'<div class="modal-content pl-2 pr-1 wsScrollBar" style="overflow: auto;z-index:1000 !important;max-height: 550px;height: 550px;width:88%;margin-left: 120px;font-size:12px;">'
            +'<div class="modal-header pt-2 pb-0 px-0" style="border-bottom: 1px solid #b4adad;min-height:27px;background-color: #fff;position: sticky;top: 0px;letter-spacing: 2px;font-size: 14px;z-index: 20;">'
                //+'<button type="button" onclick="closeAgileComents();" class="close p-0 ideaCloseButton" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
                +'<img src="images/menus/close3.svg" onclick="closepopupComments();" class="ml-auto" style="cursor:pointer;width:10px;height:10px;margin-top:1px;margin-right:3px;">'
            +'</div>'
            +'<div class="d-flex  align-items-center py-1 w-100" style="background-color: #003A5D;position: sticky;top: 27px;letter-spacing: 1px;font-size: 14px;z-index: 10;color:#fff;">'
            +'<div class="defaultExceedCls d-flex align-items-center" style="width:99%;">'
                +'<img src="'+imgsrc+'" class="ml-1" style="width:22px;height:22px;">'
                +'<span class="pl-2" style="">'+place+' Comments - </span>'
                +'<span class="pl-1" style="width:88%;">'+title+'</span>'
            +'</div>'
            +'</div>'
            +'<div id="popupCommentsDivbody" class="modal-body py-0 px-1 justify-content-center" style="height: auto;">'
            +'</div>'
        +'</div>'
        +'</div>'
    +'</div>'

    $('#glbCommentsDiv').append(ui).show();
}

function closepopupComments(){
	$('#glbCommentsDiv').html('').hide();
    $("#transparentDiv").hide();
}

function VoiceIconOnClick(type, feedId){  
    
    startTime = new Date();
    timeOut = setTimeout(Timer,1000); // changing the timer by one second while recoding
    
    
if(type == 'post'){	  // This means that recoding is happening at the main level
    
    //console.log(type+"----"+feedId);
    voiceFlag = false;
    $('#audioContainer, div.timer').show();
    $('#audioArea, #main_commentTextarea').hide();
    $('#recAudioMsg').css('box-shadow','0 0 7px 4px red');
    
  }else if(type == undefined){
      //console.log("else if---"+type);
      /*  var track = stream.getTracks()[0];  // if only one media track
       track.stop();	 */
     // recorder.stop();
  }else{ // this means recoding for sub level
      //console.log("else---"+type+"----"+feedId);
     
      $("#replyBlock_" + feedId).hide();
      $('#audioContainerSub_'+feedId+'').show();
      $('#audioAreaSub_'+feedId+', #replyBlock_'+feedId+' ').hide();
      $('#replyDivContainer_'+feedId+'').find('div.timer').show();
      $('.recAudioMsg').css('box-shadow','0 0 7px 4px red');
       flagReply = false;
     
  }
    
}

function replyVoiceFeed(obj , feedId){
            
	if(flagReply){
		
		Fr.voice.record(false, function(){
			
		},"subreply", feedId);
		$('div.timerSpan').val('');
		   $('div.timer').show();
		   
	}else{

		Fr.voice.pause();
		
	  $('#replyBlock_'+feedId+'').hide();
		$('#audioAreaSub_'+feedId+'').show();
		$('div.timer').hide();
	
		var $obj = $(obj).parent().prev().children().eq(1);
			replyFunction = $obj.attr('onclick');

			$obj.attr('onclick',"CommentVoiceFeed('"+prjid+"','voiceFeed',"+feedId+",'activityfeed')");  
	  
		  Fr.voice.export(function(url){
		
				Fr.voice.pause();
			  $("#audioSub_"+feedId+"").attr("src", url);
			 // $("#audioSub_"+feedId+"")[0].play();
				
			  $('#convoPost').data('url',url);
			  
			  $('div.recAudioMsg').css('box-shadow','none'); 
			   flagReply = true;
				 clearTimeout(timeOut);	
				
			}, "URL");

	}  
	  
} 

async function commentsTask(feedid,place){
	var feedname = $('#actFeedDiv_'+feedid).find('#conversationcontent').text();
	let jsonbody = {
		"source_id" : feedid,
		"user_id" : userIdglb,
		"project_id" : prjid
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
    await $.ajax({
          url: apiPath + "/" + myk + "/v1/getTaskDetails/conversation",
          type: "POST",
          contentType: "application/json",
          data: JSON.stringify(jsonbody),
          error: function (jqXHR, textStatus, errorThrown) {
              checkError(jqXHR, textStatus, errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');  
          },
          success: function (result) {
			convoTask="convoTask";
			$('#otherModuleTaskListDiv').append(popupTaskUI(feedid,feedname)).show();
            $('#popupTasksListHeaderDiv').append(wsTaskUi("conversation"));
      
            $("#popupTasksListDivbody").html(createTaskUI(result));

			if(place=="feedoption"){
				$(".creatPopupTaskbutton").trigger("click");
				$("#taskname").val(feedname);
			}

			$('#loadingBar').addClass('d-none').removeClass('d-flex');  
		  }
		});	  	
}


function showTaskFeed(feedid,type,projectid){
	if($(".task_comment_div").find('.task_comment_list_div').children().length==0){
		if(globalmenu=='Task' || $('#otherModuleTaskListDiv').is(':visible') == true){
			var feedType="Task";
			var task_id=$("#task_id").val();
		}
		convoid = $("#actFeedDiv_"+feedid).parents(".actFeedTot").attr("id").split("_")[1];
		var localOffsetTime=getTimeOffset(new Date());
		var jsonbody = {
			"user_id":userIdglb,
			"company_id":companyIdglb,
			"project_id":projectid,
			"localOffsetTime":localOffsetTime,
			"action":"FetchConvTaskActivityFeed",
			"feedId":convoid
		}
		$(".task_comment_div").find('.task_comment_list_div').html('');
		$.ajax({
			url: apiPath+"/"+myk+"/v1/getFeedDetails",
			type:"POST",
			dataType:'json',
			contentType:"application/json",
			data: JSON.stringify(jsonbody),
			error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$('#loadingBar').addClass('d-none').removeClass('d-flex');
						//timerControl("");
						},
			 success: function (result) {
				$(".task_comment_div").show();
				$(".task_user_history").hide();
				$(".dependency_comment_div").hide();
				$(".task_comment_div").find('.task_comment_list_div').append(prepareCommentsUI(result, menuutype, projectid,''));
				$("#addTaskDiv").find('[id^=aFeed1_]').removeAttr('onmouseover');
				$("#addTaskDiv").find('[id^=actFeedDiv_]').removeAttr('ondblclick');
				commentDataValidation(result,feedid);
				$(".task_comment_div").find('.task_comment_list_div').css('max-height','470px');
				$(".popupTask").find('.task_comment_list_div').css('height','auto');
				
	
			 }
		});  
	}else{
		$(".task_comment_div").find('.task_comment_list_div').html('');
		$(".task_comment_div").hide();
	} 
	
}


