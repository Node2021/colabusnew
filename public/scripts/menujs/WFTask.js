//Workflow task code by kalpan

var datawF = [];
var globalUIArray = [];
var stepMaintainArray = [];
var sublevelArray = new Map();
var globalSubUIArray = [];
function showWorkFlow(place) {
    //alert(datawF.length);

    if (datawF.length == 0) {
        if ($("#workflowname").val() != '') {
            datawF.push({

                id: 1,
                description: "Step 1",
                children: [],
                status: "Y"

            });
            // console.log(datawF)
            $('#my-container').hortree({
                data: datawF
            });
            $("#block_1").addClass('hortree-label-active');
            showTaskUI(1, '');
            stepMaintainArray.push("");
            stepMaintainArray.push(1);
            removeIcons();
            validateUI(1);
            //$('div[class^=line]').css('top', '37px');
            //$('div[class^=line]').css('width', '64px');
        }
    } else {
        // if( datawF[0].children.length>0){
        //     let child=datawF[0].children;
        //     let  count=1;
        //     while(typeof(child)!='undefined' && child.length >= 1) {

        //             child = child[0].children;   
        //             count++;
        //     }
        //     child.push({
        //         id:count+1,
        //         description:"Step "+(parseInt(count)+1)+"",
        //         children:[] 
        //     })
        // }else{
        //     datawF[0].children.push({

        //         id:2,
        //         description:"Step 2",
        //         children:[] 
        //     })
        // }
        // $('#my-container').hortree({
        //     data: datawF
        // });
    }



    //  $('div[class^=line]').css('top', '37px');
    $('div[class^=line]').css('width', '64px');
}

function uploadWorkflowData() {

    document.getElementById('uploadForTaskwf').click();
}

function changeUItoWF() {
    let UI = "<div class='d-flex' style='font-size:12px'>";
    UI += '<div  style="width: 65%;">'
        + '<div class="px-3 mt-3" style="">'
        // +'<label style="width:100%;" class="pure-material-textfield-outlined" >'
        // +'<input id="taskname" placeholder=" " style="height:40px;    font-size: 12px;">'
        // +'<span id="taskspan" style="margin-top:-8px !important;font-size:12px">Task Name</span>'
        // +'</label>'
        // +'<div class="material-textfield" ><textarea  id="taskname" class="theightdemo material-textfield-input taskfield" onkeyup=" commentAutoGrow(event, this, \'task\', \'\'); ></textarea><label class="material-textfield-label">Task Name</label></div>'
        + '<div class="material-textfield" ><textarea id="workflowname" type="text" class="theightdemo material-textfield-input"  onblur="showWorkFlow();"  placeholder=" "  ></textarea><label class="material-textfield-label" style="    font-size: 12px;top:18px;">Workflow Name</label></div>'
        + '</div>'
    UI += "</div>"
    UI += '<div class="d-none taskappendDiv"></div>'
    UI += "<div class='pr-5 mt-4 d-flex'>"
        + '<input id="uploadForTaskwf" type="file" onchange="uploadFileForwfTask(this)" class="d-none"/>'
        

        +'<div class="px-1 py-1 convoSubIcons" style="margin-top: -6px;"><img onclick="uploadWorkflowData();" id="wfupload" title="Attach" class="img-responsive task_icon" src="/images/task/upload_cir.svg"></div>'

        +'<div class="px-1 py-1 convoSubIcons" style="margin-top: -6px;"><img   onclick="fetchWFTemplatedetails();" id="wfTemplateViewImg"  class="img-responsive task_icon" src="/images/task/workflow.svg"></div>'

    UI += "</div>"

        + '<div class="groupDivwf ml-1 mt-4 d-flex  justify-content-end " style="cursor:pointer ; width:90px;">'

        + '<div class="group_textwf">'
        + '<div class="d-flex">'
        + '<div>Group </div>'

        + '<div class="ml-1"><img src="images/task/dowarrow.svg" style="height:12px;width:12px"></div>'

        + '</div>'

        + '</div>'
        + '<div class="group_boxwf" align="center" style="width:26px;">'
        + '<div id= "changeGroupwf" title="Web Development" class="task_group   rounded-circle" style="background:#0000ff ;margin-top:2px;" group_id="3"></div>'
        + '</div>'



        + '</div>'//groupDiv div ends here 
        + '<div class="priorityDivWf d-flex ml-3 mt-4   justify-content-end " style="cursor:pointer; width:90px;">'

        + '<div class="d-flex">'
        + '<div>Priority </div>'

        + '<div class="ml-1"><img src="images/task/dowarrow.svg" style="height:12px;width:12px"></div>'

        + '</div>'
        + '<div align="center" style="width:26px;">'//ml-2 17px
        + '<img priority="6" class="changePriorityWf" src="images/task/p-six.svg" style="width:16px;margin-top: -1px;" />'
        + '</div>'




        + '</div>'






    UI += "</div>"
    UI += '<div class=" d-none" id="projectlistingDiv" style="margin-top: 9px;/* margin-left: -33px; */justify-content: flex-end;/* width: 90%; */font-size: 12px;margin-right: 35px;"><div style="text-align:right;margin-right: 10px;">Workspace :</div><select id="projectselect" onchange="changeProjectid();" style="margin-top: -3px;/* margin-left: -6px; */width: 16%;"></select></div>'
    UI += '<div id="my-container" class="defaultScrollDiv position-relative  w-100 px-3 mt-3 pt-3" style="overflow:auto;padding-bottom: 60px;"></div>'

    UI += '<div id="workflowdepTask">'



        + '</div>'

    UI += '<div id="workflowTemplate" style="max-height:300px;overflow:auto">'



        + '</div>'

    $('.taskAdd').html(UI);
    if (typeof ($("#Workflow").attr('workflowid')) != 'undefined' && $("#Workflow").attr('workflowid') != "") {

    } else {
        fetchGroups('workflowfrom');
    }

    $(".group_textwf").on('click', () => {
        $("#priorityDivcustomwf").hide();
        $("#groupDivwf").remove();
        fetchGroups('workflow');
    })
    $(".group_boxwf").on('click', () => {
        $("#priorityDivcustomwf").hide();
        $("#groupDivwf").remove();
        fetchGroups('workflow');
    })
    $(".priorityDivWf").on('click', () => {
        $("#priorityDivcustomwf").toggle();
    })

    //fetchGroups('workflow');
    addPriority('workflow');


}

function showOptions(id) {
    showTaskUI(id)

    if ($("#WfOption_" + id).is(":hidden")) {
        $('div[id^=WfOption_]').hide();
        $("#WfOption_" + id).show();

    } else {
        $("#WfOption_" + id).find("#groupdivison").remove();
        $("#WfOption_" + id).hide();

    }
}

function addStep(id) {
    // alert(id)
    if (id.toString().indexOf("_") > -1) {
        showTaskUI(id)
        let mainid = id.toString().split("_")[0];
        let subid = id.toString().split("_")[1];

        let json = sublevelArray.get(mainid);
        let child = json;
        let js=json;
        let counter=1;
        console.log(js)
        while (typeof (js) != 'undefined' && js.length >= 1) {
            
            js = js[0].children;
            counter++
           
        }
       
        //   console.log(child);
        //  child[1].children.push(sublevelArray.get(mainid));
        while (typeof (child[0]) != 'undefined' && child[0].id.toString().split("_")[1] != subid) {
            child = child[0].children;
        }

       
       
       
       
        let childspare = child[0].children;
        child[0].children = []
        child[0].children.push({
            id: mainid + "_" + (counter ),
            description: "Step " + (counter ),
            children: [],
            status: "N"
        })
        // console.log(child[0].children);
        if (typeof (childspare[0]) != 'undefined') {
            child[0].children[0].children.push(childspare[0]);
        }


        sublevelArray.set(mainid, json);
        child = datawF[0].children;
        if (datawF[0].id == mainid) {
            child = datawF;
            datawF[0].description = $("#stepdesc_1").text();
        } else {
            while (typeof (child) != 'undefined' && typeof (child[0]) != 'undefined' && child[0].id != mainid) {
                child = child[0].children;
            }
        }
        child[0].children[1] = sublevelArray.get(mainid)[0];
        // console.log(datawF);
        $('#my-container').hortree({
            data: datawF
        });
        
        $("#block_undefined").remove();
        $(".hortree-label-active").removeClass('hortree-label-active');
        $("#block_" +mainid + "_" + counter).addClass('hortree-label-active');
        // $('div[class^=line]').css('top', '37px');
        //$('div[class^=line]').css('width', '64px');
        $('div[id^=WfOption_]').hide();
        $("#active_" +mainid + "_" + (counter)).show();
       
      
        validateUI(mainid + "_" + counter);
        removeIcons();
        addBox(mainid)

    } else {
        let dId = "";
        prepareReorderUI(id, 'show');
        if (datawF[0].children.length == 0) {
            datawF[0].children.push({

                id: 2,
                description: "Step 2",
                children: [],
                status: "Y"
            });
            dId = "2";
            showTaskUI(2, '');
            stepMaintainArray.push(2);
        } else {
            let count = 1;
            let child = datawF[0].children;
            let max_id = child[0].id;
            while (typeof (child) != 'undefined' && child.length >= 1) {
                if (parseInt(child[0].id) > parseInt(max_id)) {
                    max_id = child[0].id;
                }
                child = child[0].children;
                count++;
            }
            max_id = count;
            child = datawF[0].children;
            while (typeof (child) != 'undefined' && child[0].id != id) {
                child = child[0].children;
            }
            let spareChild = child[0].children;

            child[0].children = [];
            child[0].children.push({

                id: parseInt(max_id) + 1,
                description: "Step " + (parseInt(max_id) + 1) + "",
                children: [],
                status: "Y"
            });
            //  console.log(child[0].children[0]);
            child[0].children[0].children = spareChild;

            showTaskUI(parseInt(max_id) + 1, '');

            if (stepMaintainArray[parseInt(stepMaintainArray.indexOf(id)) + 1] != '' && stepMaintainArray[parseInt(stepMaintainArray.indexOf(id)) + 1] != null && typeof (stepMaintainArray[parseInt(stepMaintainArray.indexOf(id)) + 1]) != 'undefined') {
                stepMaintainArray.splice(parseInt(stepMaintainArray.indexOf(id)) + 1, 0, parseInt(max_id) + 1);
            } else {
                stepMaintainArray.push(parseInt(max_id) + 1);
            }

            dId = parseInt(max_id) + 1;

        }
        $('#my-container').hortree({
            data: datawF
        });
        removeIcons();
        $(".hortree-label-active").removeClass('hortree-label-active');
        $("#block_" + dId).addClass('hortree-label-active');
        // $('div[class^=line]').css('top', '37px');
        //$('div[class^=line]').css('width', '64px');
        $('div[id^=WfOption_]').hide();
        $("#active_" + dId).show();
        validateUI(dId);
    }

}

function removeStep(id) {

    if (id.toString().indexOf("_") > -1) {
        let mainid = id.toString().split("_")[0];
        let subid = id.toString().split("_")[1];

        if ($(".workflowUI").attr('levelstatus') == "E") {
            deleteSteps($(".workflowUI").attr('task_id'), $(".workflowUI").attr('wflevelid'));
        }
        let json = sublevelArray.get(mainid);
        let child = json;

        let prev = json;

        //console.log(child);
        //  child[1].children.push(sublevelArray.get(mainid));
        while (typeof (child[0]) != 'undefined' && child[0].id.toString().split("_")[1] != subid) {
            prev = child;
            child = child[0].children;
        }
        let childspare = [];
        try {
            childspare = child[0].children;
        } catch (error) {

        }
        prev[0].children = childspare;
        sublevelArray.set(mainid, json);
        child = datawF[0].children;
        if (datawF[0].id == mainid) {
            child = datawF;
            datawF[0].description = $("#stepdesc_1").text();
        } else {
            while (typeof (child) != 'undefined' && typeof (child[0]) != 'undefined' && child[0].id != mainid) {
                child = child[0].children;
            }
        }
        child[0].children[1] = sublevelArray.get(mainid)[0];
        //console.log(datawF);
        $('#my-container').hortree({
            data: datawF
        });
        removeIcons();
        $("#block_undefined").remove();
        addBox(mainid)

    } else {
        prepareReorderUI(id, 'show');
        if (datawF[0].id == id) {
            alertFun("First step can't be deleted", 'warning');
            $('div[id^=WfOption_]').hide();
        } else {
            $(".taskappendDiv").html(globalUIArray[id]);
            if ($(".workflowUI").attr('levelstatus') == "E") {
                deleteSteps($(".workflowUI").attr('task_id'), $(".workflowUI").attr('wflevelid'));
            }
            $(".taskappendDiv").html('');
            let child = datawF[0].children;
            let prev = datawF;
            while (typeof (child) != 'undefined' && child[0].id != id) {
                prev = child;
                child = child[0].children;
            }
            let spareChild = child[0].children;
            prev[0].children = spareChild;
            $('#my-container').hortree({
                data: datawF
            });
            // $('div[class^=line]').css('top', '37px');
            //$('div[class^=line]').css('width', '64px');
            $('div[id^=WfOption_]').hide();
            removeIcons();
            if ($(".workflowUI").length > 0) {
                let presentid = $(".workflowUI").attr('idoftask');
                if (presentid == id) {
                    showTaskUI(prev[0].id, prev[0].description);

                }

            }
            globalUIArray[id] = '';
            try {
                stepMaintainArray.splice(stepMaintainArray.indexOf(id), 1);
            } catch (error) {
                console.log(error);
            }

        }
    }
}
function showTaskUI(id, name) {
    let UI = "";



    $(".hortree-label-active").removeClass('hortree-label-active');
    $(".activecom").hide();
    $("#active_" + id).show();

    $("#block_" + id).addClass('hortree-label-active');
    let flag = false;


    if (id.toString().indexOf("_") > -1) {

        let flag = true;
        let count = 0;
        if ($(".workflowUI").length > 0 && name != "from") {
            let presentid = $(".workflowUI").attr('idoftask');
            if (presentid.toString().indexOf("_") > -1) {
                findAndReplaceUI(presentid);

            } else {
                globalUIArray[presentid] = $(".workflowUI").clone();
                if (typeof ($(".workflowUI").attr('depid')) != 'undefined' && $(".workflowUI").attr('depid') != "0" && $("#WfOption_" + presentid).find("#groupdivision").length == 0) {

                    //prepareReorderUI(id, "show");
                }
            }
            $(".workflowUI").remove();
        }

        for (let i = 0; i < globalSubUIArray.length; i++) {

            $(".taskappendDiv").html(globalSubUIArray[i]);
            // alert($(".taskappendDiv").find(".workflowUI").attr('idoftask') + "--" + id)
            if ($(".taskappendDiv").find(".workflowUI").attr('idoftask') == id) {
                //  alert("matched")
                $(".workflowUI").remove();
                flag = false;
                break;
            }
            count++;
        }

        if (flag) {
            $(".workflowUI").remove();
            let UI = "<div class='col-12 p-0 workflowUI'  style='' idoftask='" + id + "' levelstatus='N'>"
            UI += prepareAddTaskUiNew('workflow');
            UI += '</div>'
            globalSubUIArray.push(UI);
            $(globalSubUIArray[globalSubUIArray.length - 1]).insertAfter(".documentAdd");
            flag = true;
            createPopupDiv();
            $("#upload").on('click', () => {
                document.getElementById('uploadForTask').click();
            });
            $("#comments").removeClass('d-block').addClass('d-none');
            $("#dependencyTask").removeClass('d-block').addClass('d-none');
            $("#comments").parent().removeClass('d-block').addClass('d-none');
            $("#dependencyTask").parent().removeClass('d-block').addClass('d-none');

            if (id.toString().indexOf("_") == -1) {
                $(".workflowUI").find("#taskname").val(name);
                globalUIArray[id] = $(".workflowUI").clone();

            } else {
                let UI1 = $(".workflowUI").clone()
                for (i = 0; i < globalSubUIArray.length; i++) {
                    $(".taskappendDiv").html(globalSubUIArray[i]);
                    console.log("---------------------------------in pepare");
                    console.log("finding id" + $(".taskappendDiv").find(".workflowUI").attr('idoftask') + "----flag---->" + flag);
                    if ($(".taskappendDiv").find(".workflowUI").attr('idoftask') == id) {

                        globalSubUIArray.splice(i, 1);
                        break;
                    }

                }
                globalSubUIArray.push(UI1);
            }
            $("#taskspan").on('click', () => {
                $("#taskspan").css('margin-top', '0px')
            });

        } else {
            $(".workflowUI").remove();
            $(".task_user_new_div").remove();
            $(".task_user_new_div1").remove();
            console.log(globalSubUIArray[count])
            $(globalSubUIArray[count]).insertAfter(".documentAdd");

            $(".groupDiv").css('visibility', 'hidden');
            $(".groupDiv").html('');
        }
        $(".workflowUI").find("#taskname").val($("#stepdesc_" + id).text());
        $(".dhtmlxcalendar_material").remove();
        $(".dhtmlxcalendar_material").remove();
        console.log(globalSubUIArray)
    } else {
        if ($(".workflowUI").length > 0 && name != "from") {
            let presentid = $(".workflowUI").attr('idoftask');
            if (presentid.toString().indexOf("_") > -1) {
                // globalSubUIArray[presentid.toString().split("_")[0]] = findAndReplaceUI(presentid, $(".workflowUI").clone())
                findAndReplaceUI(presentid)
            } else {
                globalUIArray[presentid] = $(".workflowUI").clone();
                if (typeof ($(".workflowUI").attr('depid')) != 'undefined' && $(".workflowUI").attr('depid') != "0" && $("#WfOption_" + presentid).find("#groupdivision").length == 0) {

                    prepareReorderUI(id, "show");
                }
            }
            $(".workflowUI").remove();
        }

        if (globalUIArray[id] != '' && globalUIArray[id] != null && typeof (globalUIArray[id]) != 'undefined') {
            UI = globalUIArray[id];
            $(".workflowUI").remove();

        } else {
            let UI = "<div class='col-12 p-0 workflowUI'  style='' idoftask='" + id + "' levelstatus='N'>"
            UI += prepareAddTaskUiNew('workflow');
            UI += '</div>'
            globalUIArray[id] = UI;
            flag = true;
            $(".workflowUI").remove();
        }
        $(".workflowUI").remove();
        $(".task_user_new_div").remove();
        $(".task_user_new_div1").remove();
        $(globalUIArray[id]).insertAfter(".documentAdd");

        $(".groupDiv").css('visibility', 'hidden');
        $(".groupDiv").html('');
    }








    if (flag) {
        createPopupDiv();
        $("#upload").on('click', () => {
            document.getElementById('uploadForTask').click();
        });
        $("#comments").removeClass('d-block').addClass('d-none');
        $("#dependencyTask").removeClass('d-block').addClass('d-none');
        $("#comments").parent().removeClass('d-block').addClass('d-none');
        $("#dependencyTask").parent().removeClass('d-block').addClass('d-none');

        if (id.toString().indexOf("_") == -1) {
            $(".workflowUI").find("#taskname").val(name);
            globalUIArray[id] = $(".workflowUI").clone();

        } else {
            findAndReplaceUI(id)
        }
        $("#taskspan").on('click', () => {
            $("#taskspan").css('margin-top', '0px')
        });


    } else {
        $(".group_text").on('click', () => {
            $("#priorityDivcustom").hide();
            $("#groupdivision").remove();
            fetchGroups();
        })
        $(".group_box").on('click', () => {
            $("#priorityDivcustom").hide();
            $("#groupdivision").remove();
            fetchGroups();
        })
        $(".priorityDiv").on('click', () => {
            $("#groupdivision").hide();
            $("#priorityDivcustom").toggle();
        })
        $("#upload").on('click', () => {
            document.getElementById('uploadForTask').click();
        });
        //$("#comments").removeClass('d-block').addClass('d-none');
        $("#dependencyTask").removeClass('d-block').addClass('d-none');
        //  $("#comments").parent().removeClass('d-block').addClass('d-none');
        $("#dependencyTask").parent().removeClass('d-block').addClass('d-none');

    }
    $(".dhtmlxcalendar_material").remove();
    calenderForTask();



    let levelstatus = $(".workflowUI").attr('levelstatus');



    if (levelstatus == 'E') {
        //fetchGroups();
        getDocumentDetails($("#task_id").val(), 'upload', 'cal', '', '');
        $(".selectedit").remove();
        $(".userselect").html('');
        $('div[class^=selectedit]').remove();
        if ($("#user_id_" + userIdglb).length > 0) {
            $(".selectedit").remove();
            // editSelect(userIdglb,$("#task_id").val());
            $("#userupload").on('click', () => {

                document.getElementById('uploadForTaskuser').click();
            });
            $(".selectedit").remove();
            $(".userselect").html('');
        }
    }
    $("#block_" + id).find(".activecom").show();
}

function clearWFData() {
    globalUIArray = [];
    datawF = [];
    stepMaintainArray = [];
    stepMaintainArray = [];
    sublevelArray = new Map();
    globalSubUIArray = [];

    if (valueForView == "list") {
        $("#WFtask_" + $("#Workflow").attr('workflowid')).css('background-color', '#FFF');
    } else {
        $("#innerTaskList").find("div[id^=WFtask_]").removeClass("taskNodeSelect");
    }

    $("#Workflow").attr('workflowmode', '');
    $("#Workflow").attr('workflowid', '');

}
function createWorkFlowTask() {

    if ($("#workflowname").val() == "") {
        alertFunNew("Workflow name cannot be Empty", 'error');
        return false;
    }
    if ($(".workflowUI").length > 0) {
        let presentid = $(".workflowUI").attr('idoftask');
        if (presentid.toString().indexOf('_') > -1) {
            findAndReplaceUI(presentid);
        } else {
            globalUIArray[presentid] = '';
            globalUIArray[presentid] = $(".workflowUI").clone();
            $(".workflowUI").remove();
        }

    }
    $(".workflowUI").remove();
    // alert("stepMa" + stepMaintainArray.length)
    let userData = "[";
    let counter = 1;
    for (i = 1; i < stepMaintainArray.length; i++) {
        // alert('i' + i)
        if (globalUIArray[stepMaintainArray[i]] != 'null' && globalUIArray[stepMaintainArray[i]] != '' && typeof (globalUIArray[stepMaintainArray[i]]) != 'undefined') {
            $(".taskappendDiv").html(globalUIArray[stepMaintainArray[i]]);

            //console.log($(".taskappendDiv").html());
            let presentid = $(".workflowUI").attr('idoftask');
            userData += "{";
            userData += "\"defination\":\"" + $(".taskappendDiv").find("#taskname").val().toString().trim() + "\",";
            userData += "\"instructions\":\"" + $(".taskappendDiv").find("#taskdesc").val().toString().trim() + "\",";



            let startDate = $(".taskappendDiv").find("#startDateCalNewUI").text();
            var monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            let startdateMonth = monthNames.indexOf(startDate.substring(0, 3)) < 10 ? "0" + monthNames.indexOf(startDate.substring(0, 3)) : monthNames.indexOf(startDate.substring(0, 3));
            let startdateDate = startDate.substring(4, 6)
            let start = startdateMonth.toString().trim() + "-" + startdateDate.trim() + "-" + startDate.split(",")[1].trim();

            let endDate = $(".taskappendDiv").find("#endDateCalNewUI").text();
            let enddateMonth = monthNames.indexOf(endDate.substring(0, 3)) < 10 ? "0" + monthNames.indexOf(endDate.substring(0, 3)) : monthNames.indexOf(endDate.substring(0, 3));
            let enddateDate = endDate.substring(4, 6)
            let end = enddateMonth.toString().trim() + "-" + enddateDate.trim() + "-" + endDate.split(",")[1].trim();


            let startfullTime = $(".taskappendDiv").find("#startTimeCalNewUI").text();
            let hour = startfullTime.substring(startfullTime.length - 2, startfullTime.length) == "PM" ? (parseInt(startfullTime.split(":")[0].trim()) + 12) : startfullTime.split(":")[0].trim();
            let startTime = hour + ":" + startfullTime.split(":")[1].trim().trim().substring(0, 2) + ":00";


            let endfullTime = $(".taskappendDiv").find("#endTimeCalNewUI").text();
            let endhour = endfullTime.substring(endfullTime.length - 2, endfullTime.length) == "PM" ? (parseInt(endfullTime.split(":")[0].trim()) + 12) : endfullTime.split(":")[0].trim();
            let endTime = endhour + ":" + endfullTime.split(":")[1].trim().substring(0, 2) + ":00";

            let estimateHour = $(".taskappendDiv").find(".estimateHour").val();
            let estimateMin = $(".taskappendDiv").find(".estimateMinute").val();

            if (estimateMin > 59) {
                estimateHour = parseInt(estimateHour) + Math.floor(parseInt(estimateMin) / 60);
                estimateMin = Math.floor(parseInt(estimateMin) % 60);
            }

            let saveDocTaskIds = "";
            $('.taskDocument > div').each(function () {
                saveDocTaskIds = saveDocTaskIds + $(this).attr('id').split("_")[1] + ","
            });
            userData += "\"stepLevelDocId\":\"" + saveDocTaskIds + "\",";
            userData += "\"start_time\":\"" + startTime + "\",";
            userData += "\"end_time\":\"" + endTime + "\",";

            userData += "\"start_date\":\"" + start + "\",";
            userData += "\"end_date\":\"" + end + "\",";

            userData += "\"totalEstHr\":\"" + estimateHour + "\",";
            userData += "\"totalEstmin\":\"" + estimateMin + "\",";

            userData += "\"levelordertemplate\":\"" + counter + "\",";

            //let group_id = $(".taskappendDiv").find("#changeGroup").attr('group_id');
            let group_id = typeof ($(".workflowUI").attr('groupid')) != 'undefined' ? $(".workflowUI").attr('groupid') : $("#changeGroupwf").attr('group_id');
            userData += "\"task_group_id\":\"" + group_id + "\",";

            counter++;
            let priority = $(".taskappendDiv").find(".changePriority").attr('priority');
            let priorityId = priority == 0 ? "6" : priority;

            let depid = $(".taskappendDiv").find(".workflowUI").attr('depid');

            if (typeof (depid) == 'undefined') {

                depid = "";
            }
            userData += "\"depId\":\"" + depid + "\",";

            let documentmandatory = "N"
            if ($(".taskappendDiv").find('.workflowUI').attr('documentmandatory') == 'Y') {
                documentmandatory = "Y"
            }
            userData += "\"leveldocattach\":\"" + documentmandatory + "\",";
            userData += "\"priority\":\"" + priorityId + "\",";

            if ($(".taskappendDiv").find('.workflowUI').attr('active') == 'grayedOut') {
                userData += "\"taskSpecificClass\":\"grayedOut\",";
            } else {
                userData += "\"taskSpecificClass\":\"\",";
            }
            //if(place=='update'){
            let levelStatus = $(".workflowUI").attr('levelstatus');

            let levelcolor = $(".workflowUI").attr('colorcode');

            if (levelcolor) {
                userData += "\"step_color\":\"" + levelcolor + "\",";
            } else {
                userData += "\"step_color\":\"\",";
            }

            if (levelStatus == "E") {
                userData += "\"level_name\":\"" + $(".taskappendDiv").find("#taskname").val() + "\",";
                userData += "\"levelStatus\":\"" + levelStatus + "\",";
                userData += "\"wfId\":\"" + $(".workflowUI").attr('task_id') + "\",";
                userData += "\"level_id\":\"" + $(".workflowUI").attr('wflevelid') + "\",";
            } else {
                userData += "\"level_name\":\"" + $(".taskappendDiv").find("#taskname").val() + "\",";
                userData += "\"levelStatus\":\"N\",";
            }
            //}

            let user_ids = "";
            $(".taskappendDiv").find('.userDetails > div').each(function () {
                let localid = "";
                if ($(this).attr('id') != 'ignorediv') {
                    var id = $(this).attr('id').split('_')[2];

                    let estH = $(this).find('.estimateHour').val();
                    let estM = $(this).find('.estimateMin').val()
                    if (estM > 59) {
                        estH = parseInt(estH) + Math.floor(parseInt(estM) / 60);
                        estM = Math.floor(parseInt(estM) % 60);
                    }

                    let taskAction = 'Created';
                    if (typeof ($("#persontaskstatusdisplay_" + id).attr('statusoftaskuser')) != 'undefined' && $("#persontaskstatusdisplay_" + id).attr('statusoftaskuser') != '') {
                        taskAction = $("#persontaskstatusdisplay_" + id).attr('statusoftaskuser');

                    }
                    // if (parseInt($(this).find('#taskSlideAmount_'+id).val()) == 100) {
                    //   taskAction = 'Completed';

                    // }else if(parseInt($(this).find('#taskSlideAmount_'+id).val()) >0 && parseInt($(this).find('#taskSlideAmount_'+id).val()) <100){
                    //     taskAction = 'Inprogress';
                    // }
                    localid = $(this).attr('userstatus') + ":" + id + ":" + estH + ":" + estM + ":" + $(this).find('#taskSlideAmount_' + id).val() + ":" + taskAction;
                    user_ids = user_ids + localid + ",";

                }



            });
            user_ids = user_ids.length > 1 ? user_ids.substring(0, user_ids.length - 1) : "";

            let userArray1=[];
            let userData1="[";
            $(".taskappendDiv").find('.userDetails1 > div').each(function () { 
                if($(this).attr('id')!='ignorediv'){
                var id = $(this).attr('id').split('_')[2];

                let startDate1=$("#resquestDateCalNewUI_"+id).text();
                var monthNames1 = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                let startdateMonth1=monthNames1.indexOf(startDate1.substring(0,3))<10?"0"+monthNames1.indexOf(startDate1.substring(0,3)):monthNames1.indexOf(startDate1.substring(0,3));
                let startdateDate1=startDate1.substring(4,6)
                let start1=startdateMonth1.toString().trim()+"-"+startdateDate1.trim()+"-"+startDate1.split(",")[1].trim();

                let startfullTime1=$("#resquestTimeCalNewUI_"+id).text();
                // var role = $(this).find('#commentApprove_'+id).val();
                // console.log("role-->>"+role);
                if(userArray1.includes(id)==false){
                    userArray1.push(id);
                    
                    userData1+="{";
                    userData1+="\"user_id\":\""+id+"\",";
                    userData1+="\"task_user_status\":\""+$(this).find('#approveStatusImg_'+id).attr('title')+"\",";
                    userData1+="\"sequence\":\""+$(this).attr('secuenceNumber')+"\",";
                    userData1+="\"type\":\""+$(this).attr('appStatus')+"\",";
                    userData1+="\"requested_date\":\""+start1+"\",";
                    userData1+="\"requested_time\":\""+startfullTime1+"\"";
                    userData1+="},";

                }
                }
                
            });
            userArray1=[];
            userData1=userData1.length>1?userData1.substring(0,userData1.length-1)+"]":"[]";

            let dependencyTaskId = "";
            // $(".taskappendDiv").find('.dependenctTaskIds > div').each(function () { 

            //   //if(typeof($(this).find(".imagecheck").attr('src'))!='undefined'){

            //     if($(this).find('.check').prop("checked")==true){
            //       let depTask=$(this).attr('id').split("_")[1];
            //       dependencyTaskId=dependencyTaskId+depTask+":N"+",";
            //     }
            //   //}
            // });
            // dependencyTaskId=dependencyTaskId.substring(0,dependencyTaskId.length-1);
            //alert( $(".taskappendDiv").find("#remindericon").attr('reminder'));

            let reminder = $(".taskappendDiv").find("#remindericon").attr('reminder');

            reminder = typeof (reminder) == 'undefined' ? "" : reminder;
            userData += "\"reminder\":\"" + reminder + "\",";


            userData += "\"reminderType\":\"" + reminder + "\",";


            userData += "\"id\":\"" + user_ids + "\",";
            userData += "\"approval_id\":" + userData1 + ",";
            //  alert("here outside")
            if (typeof (sublevelArray.get(stepMaintainArray[i].toString())) != 'undefined' && sublevelArray.get(stepMaintainArray[i].toString()) != '' && sublevelArray.get(stepMaintainArray[i].toString()) != null) {
                //  alert("here")
                if(typeof($(".workflowUI").attr('workflow_name'))!='undefined' && $(".workflowUI").attr('workflow_name')!='')
                        {
                            userData += "\"subLevelData\":[" + fetchStepLevelData(stepMaintainArray[i],$(".workflowUI").attr('workflow_name')); + "]";
                        }else{
                           
                         
                            try {
                                fetchWorkflowDependency(stepMaintainArray[i],'sublev');
                                alertFunNew("Attched workflow name cannot be Empty", 'warning', 'min', $(".wfInput"));
                            } catch (error) {
                                
                            }
                            return false;
                            
                        }
            } else {
                userData += "\"subLevelData\":[]";
            }
            userData += "},"
            //console.log(sublevelArray)
            //alert(stepMaintainArray[i] + "---" + sublevelArray.get(1))

        }
    }
    userData = userData.length > 1 ? userData.substring(0, userData.length - 1) + "]" : "[]";



    console.log(userData);
    workflowTaskValidation(JSON.parse(userData))

}
function workflowTaskValidation(result) {
    let userUncheck = $(".taskappendDiv").attr('userUncheck');
    let estimateUncheck = $(".taskappendDiv").attr('estimateUncheck');

    for (i = 0; i < result.length; i++) {
        //alert(result[0])
        if (result[i].defination == '') {
            alertFunNew("Task name cannot be Empty", 'error');
            showTaskUI(result[i].levelordertemplate, result[i].description);
            break;
        } else if (result[i].id == '' && userUncheck != "true") {
            $(".taskappendDiv").attr('userUncheck', "true");
            confirmFunNew(getValues(companyAlerts, "Alert_IncompleteTask"), "delete", "workflowTaskValidation", result);
            showTaskUI(result[i].levelordertemplate, result[i].description);

            //  break;
            return false;
        } else {
            let totalestHour = result[i].totalEstHr;
            let totalestMin = result[i].totalEstmin;

            //N:4102:0:0:0:Created
            let idParticipant = result[i].id.split(",");
            let partcipantHour = 0;
            let partcipantMin = 0;
            for (j = 0; j < idParticipant.length; j++) {
                partcipantHour = parseInt(partcipantHour) + parseInt(idParticipant[j].split(":")[2]);
                partcipantMin = parseInt(partcipantMin) + parseInt(idParticipant[j].split(":")[3]);
            }
            let totalestimatecaltask = (parseInt(totalestHour) * 60) + parseInt(totalestMin);
            let totalestimatecaluser = (parseInt(partcipantHour) * 60) + parseInt(partcipantMin);

            if ((totalestimatecaltask > totalestimatecaluser) && estimateUncheck != "true") {
                $(".taskappendDiv").attr('estimateUncheck', "true");
                var message = "Task Estimate exceeds sum of Participant Estimates.";
                conFunNew(message, 'warning', "workflowTaskValidation", '', result);
                showTaskUI(result[i].levelordertemplate, result[i].description);
                break;
            } else if ((totalestimatecaltask < totalestimatecaluser) && estimateUncheck != true) {
                // $(".taskappendDiv").attr('estimateUncheck',"true");
                // var message = "Total Participant Estimate exceeds Task Estimate.</br></br>Do you want to update Task Estimate?";
                // confirmResetNew(message,'warning','resetEstimateTimeWf',onclick,'Yes','No',useresimateHour,userestimateMin,taskId);
                //break;
            }
        }


    }
    if (i > result.length - 1) {
        let place = $("#Workflow").attr('workflowmode');
        if (place == 'update') {
            updateWFTask(result)
        } else {
            workflowCreateApi(result)
        }
    }
}

function workflowCreateApi(userData) {

    let workflowname = $("#workflowname").val();
    let priority = $(".changePriorityWf").attr('priority');
    let group_id = $("#changeGroupwf").attr('group_id');
    let priorityId = priority == 0 ? "6" : priority;
    let saveDocTaskIds = "";//attachWFTemplate
    let refModelId = "";
    let attachTemplate = ""
    if ($("#attachWFTemplate").prop('checked')) {
        refModelId = "0";
        attachTemplate = "Project"
    }
    $('.WfDocument > div').each(function () {
        saveDocTaskIds = saveDocTaskIds + $(this).attr('id').split("_")[1] + ","
    });
    let jsonData = {
        "refModelId": refModelId,
        "priority": priorityId,
        "task_id": "0",
        "status": "",
        "taskFlowName": workflowname,
        "taskDate": [],
        "user_id": userIdglb,
        "company_id": companyIdglb,
        "project_id": prjid,
        "saveDocTaskIds": "",
        "attachTemplate": attachTemplate,
        "task_group_id": group_id,
        "userData": userData,
        "saveDocTaskIds": saveDocTaskIds
    }
    //console.log(jsonData)
    $.ajax({
        //Task/workflow",
        url: apiPath + "/" + myk + "/v1/createTask/workflow",
       // url:"http://localhost:8080/v1/createTask/workflow",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonData),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $("#loadingBar").hide();
            timerControl("");
        },
        success: function (resp) {
            $(".addTaskDiv").fadeOut(300, function () { $(this).remove(); });
            if (valueForView == "list") {
                $("#taskList").prepend(createTaskUI(resp));
            } else if (valueForView == "grid") {
                $("#innerTaskList").prepend(createTaskGridUI(resp));
            } else {
                $("#taskList").prepend(createTaskUI(resp));
            }
            clearWFData();

            if (gloablmenufor == 'Myzone') {
                $(".tasklistProjectImage").show();
            }
        }
    });

}



function fetchWFdetails(workflowid) {
    clearWFData();
    if (valueForView == "list") {
        $("#WFtask_" + workflowid).css('background-color', '#eef5f7');
    } else if (valueForView == 'grid') {
        $("#WFtask_" + workflowid).addClass("taskNodeSelect");
    }

    $.ajax({ // 
        url: apiPath + "/" + myk + "/v1/getWFDetails/" + workflowid,
        type: "GET",
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $("#loadingBar").hide();
            timerControl("");
        },
        success: function (result) {

            let UI = prepareAddTaskUiNew();
            $("#addTaskHolderDiv").html(UI).show('slow');
            $("#Workflow").attr('workflowid', workflowid);
            $("#Workflow").attr('workflowmode', "update");
            $("#Workflow").trigger('onclick');
            $('#savetask').attr("onclick", "createWorkFlowTask()");
            $("#workflowname").val(result[0].workflow_name);
            $("#changeGroupwf").attr('group_id', result[0].task_group_id);
            $("#changeGroupwf").css('background', result[0].group_code);
            $("#changeGroupwf").attr('title', result[0].group_name);
            $("#wfupload").show();
            $("#wfTemplateViewImg").hide();
            $("#wfTemplateViewImg").parent().hide();
            $("#attachWFTemplateDiv").css('visibility', 'hidden');
            let priority = result[0].priority == "6" ? "0" : result[0].priority
            var Priority_image = (priority == "") ? "" : priority == "1" ? "images/task/p-one.svg" : priority == "2" ? "images/task/p-two.svg" : priority == "3" ? "images/task/p-three.svg" : priority == "4" ? "images/task/p-four.svg" : priority == "5" ? "images/task/p-five.svg" : "images/task/p-six.svg";
            $(".changePriorityWf").attr('priority', priority);
            $('.changePriorityWf').attr('src', Priority_image);
            fetchWFDocDetails();
            fetchWFStepdetails(workflowid);
            if (gloablmenufor == 'Myzone') {
                $("#projectlistingDiv").addClass('d-flex').removeClass('d-none');
                let UI = "";
                if (result[0].project_id == "0") {
                    UI = '<option >Select Workspace</option>'
                } else {
                    UI = '<option value="' + result[0].project_id + '">' + result[0].project_name + '</option>'
                }
                $("#projectselect").html(UI);
                $("#projectselect").attr('disabled', 'disabled');
                prjid = result[0].project_id;

            }
        }
    });
}


function fetchWFStepdetails(workflowid, place,from) {
    let sprintHistoryValue = "";
    let id = $(".workflowUI").attr('idoftask');
    //
   
    if ($(".workflowUI").attr('depid') != workflowid) {


        for (i = 0; i < globalSubUIArray.length; i++) {
            $(".taskappendDiv").html(globalSubUIArray[i]);
            console.log("---------------------------------in pepare");
            console.log("finding id" + $(".taskappendDiv").find(".workflowUI").attr('idoftask') + "----flag---->" + flag);
            if ($(".taskappendDiv").find(".workflowUI").attr('idoftask') == id) {

                globalSubUIArray.splice(i, 1);

            }

        }
        sublevelArray.set(id, '');
    }
    if (typeof (id) != 'undefined' && id != '' && id != null) {
        prepareReorderUI(id);
    }

    if (typeof (sublevelArray.get(id)) != 'undefined' && sublevelArray.get(id) != '' && sublevelArray.get(id) != null) {

        let child = datawF[0].children;

        if (datawF[0].id == id) {
            child = datawF;
            datawF[0].description = $("#stepdesc_1").text();
        } else {
            while (typeof (child) != 'undefined' && typeof (child[0]) != 'undefined' && child[0].id != id) {
                child = child[0].children;
            }
        }
        if (child[0].children.length == 0) {
            child[0].children[0] = { children: [] }

        }
        //console.log(sublevelArray.get(id));
        child[0].children.push(sublevelArray.get(id)[0]);
        $('#my-container').hortree({
            data: datawF
        });
        removeIcons();
        $("#block_undefined").remove();
       
        if (typeof (id) != 'undefined' && id != '' && id != null) {
            //alert(sublevelArray.get(id)[0].id)
            showTaskUI(sublevelArray.get(id)[0].id)
        }
       
        addBox( id );
    } else {
        $.ajax({ //
            url:  apiPath + "/" + myk +"/v1/getHistoryMyTaskDetailView/workflow?sprintHistoryValue=" + sprintHistoryValue + "&task_id=" + workflowid + "&user_id=" + userIdglb + "&company_id=" + companyIdglb + "",
            type: "GET",
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $("#loadingBar").hide();
                timerControl("");
            },
            success: function (result) {
                if (place == 'dependency') {
                    $(".workflowUI").attr('depid', workflowid);
                    showTaskUI(id);
                    $(".workflowUI").attr("workflow_name",result[0].workflow_name);
                    addSubSteps(result,from);
                    

                } else {
                    prepareEditWorflowUI(result);
                }
            }
        });
    }
}

function prepareEditWorflowUI(result) {

    $("#Tasks").removeAttr('onclick');
    $("#Workflow").removeAttr('onclick');
    $("#Event").removeAttr('onclick');
    $(".Active").removeClass('Active');
    $("#Workflow").addClass('Active');
    let count = 1;
    let activeArray = [];

    for (i = 0; i < result.length; i++) {
        let activeinactive = result[i].workflowstep_blur;
        let className = "";
        let style = "";

        if (activeinactive == 'Y') {
            className = "grayedOut";
            style = "pointer-events:none;"
        }

        let UI = "<div class='col-12 p-0 workflowUI' task_id='" + result[i].task_id + "'  style='" + style + "' idoftask='" + count + "' levelstatus='E' wflevelid='" + result[i].workflow_level_id + "'  active='" + className + "'  documentcount='" + result[i].documentcount + "'  documentmandatory='" + result[i].level_document + "' depid='" + result[i].child_workflow_id + "'  colorcode='" + result[i].step_color + "' group_color='" + result[i].group_code + "' groupid='" + result[i].task_group_id + "'>"
        UI += prepareAddTaskUiNew('workflow', result[i].task_id)
        UI += '</div>';
        $(".taskappendDiv").html(UI);
        createPopupDiv('fromworkflowedit');

        $(".groupDiv").css('visibility', 'hidden');
        $(".groupDiv").html('');

        $(".taskappendDiv").find("#task_id").val(result[i].task_id);
        $(".selectedit").remove();
        $(".userselect").html('');
        $('div[class^=selectedit]').remove();

        $("#comments").removeClass('d-none').addClass('d-block');
        //$("#dependencyTask").removeClass('d-block').addClass('d-none');
        $("#comments").parent().removeClass('d-none').addClass('d-block');

        let startDate = returnFormatedDate(result[i].task_start_date)
        let startTime = returnFormatedTime(result[i].start_time);
        let endDate = returnFormatedDate(result[i].task_end_date)
        let endTime = returnFormatedTime(result[i].end_time);

        $(".taskappendDiv").find("#taskname").val(result[i].task_name);
        $(".taskappendDiv").find("#taskdesc").val(result[i].task_desc);
        if (result[i].task_desc.toString().trim() != "") {
            $(".taskappendDiv").find("#instruction").attr('src', '/images/task/intr.svg');
        }
        $(".taskappendDiv").find("#endDateCalNewUI").attr('permanentValue', endDate);
        $(".taskappendDiv").find("#endTimeCalNewUI").attr('permanentValue', endTime);
        $(".taskappendDiv").find("#startDateCalNewUI").attr('permanentValue', startDate);
        $(".taskappendDiv").find("#startTimeCalNewUI").attr('permanentValue', startTime);

        $(".taskappendDiv").find("#endDateCalNewUI").text(endDate);
        $(".taskappendDiv").find("#endTimeCalNewUI").text(endTime);
        $(".taskappendDiv").find("#startDateCalNewUI").text(startDate);
        $(".taskappendDiv").find("#startTimeCalNewUI").text(startTime);

        $(".taskappendDiv").find(".estimateHour").val(result[i].total_estimated_hour);
        $(".taskappendDiv").find(".estimateMinute").val(result[i].total_estimated_minute);

        if ($(".taskappendDiv").find(".estimateMinute").val() != "0" || $(".taskappendDiv").find(".estimateHour").val() != "0") {
            $(".taskappendDiv").find(".estimateHour").attr('modified', 'modified');
            $(".taskappendDiv").find(".estimateMinute").attr('modified', 'modified');
        }
        $(".taskappendDiv").find(".estimateHour").attr('permanentValue', result[i].total_estimated_hour);
        $(".taskappendDiv").find(".estimateMinute").attr('permanentValue', result[i].total_estimated_minute);
        $(".taskappendDiv").find("#totalHourIn").val($('#estimateHour').text());
        $(".taskappendDiv").find("#totalMinIn").val($('#estimateMinute').text());
        $(".taskappendDiv").find("#task_type").val('Workflow');

        let priority = result[i].priority == "6" ? "0" : result[i].priority
        var Priority_image = (priority == "") ? "" : priority == "1" ? "images/task/p-one.svg" : priority == "2" ? "images/task/p-two.svg" : priority == "3" ? "images/task/p-three.svg" : priority == "4" ? "images/task/p-four.svg" : priority == "5" ? "images/task/p-five.svg" : "images/task/p-six.svg";
        $(".changePriority").attr('priority', priority);
        $('.changePriority').attr('src', Priority_image);

        let reminderType = result[i].reminder == "" ? "select" : result[i].reminder


        if (reminderType != "select") {
            $("#remindericon").attr('src', '/images/task/clock-Invt.svg');
            $("#remindericon").attr('reminder', reminderType);
        }

        if (typeof (result[i].task_group_id) != 'undefined') {
            $("#changeGroup").attr('group_id', result[i].task_group_id);
            $("#changeGroup").css('background', result[i].group_code);
            $("#changeGroup").attr('title', result[i].group_name);
        }

        if (result[i].taskParticipant.length > 0) {
            $("#participantsUIdivArea").attr('src', '/images/task/assignee_cir2.svg')
        }

        for (j = 0; j < result[i].taskParticipant.length; j++) {
            let imgName = lighttpdpath + "/userimages/" + result[i].taskParticipant[j].user_id + "." + result[i].taskParticipant[j].user_image_type + "?" + d.getTime();
            addUserToData(prjid, result[i].taskParticipant[j].user_full_name, result[i].taskParticipant[j].user_id, '', imgName, 'edit', result[i].task_id, calculateElapseTime(result[i].taskParticipant[j].elapsed_time));

            let image = result[i].taskParticipant[j].user_task_status;
            if (image == "Paused") {
                image = "images/task/user-pause.svg"
            } else if (image == 'Inprogress') {
                image = "images/task/user-inprogress.svg"
            } else if (image == 'Completed') {
                image = "images/task/user-completed.svg"
            } else {
                image = "images/task/user-start.svg"
            }
            $("#persontaskstatusdisplay_" + result[i].taskParticipant[j].user_id).attr('src', image);
            $("#persontaskstatusdisplay_" + result[i].taskParticipant[j].user_id).attr("statusOfTaskUser", result[i].taskParticipant[j].user_task_status);
            if (result[i].taskParticipant[j].user_id == userIdglb) {
                $("#persontaskstatusdisplay_" + result[i].taskParticipant[j].user_id).attr('onclick', 'changeStatusOfUserTask(' + result[i].task_id + ',' + prjid + ',\'editView\',\'' + result[i].task_type + '\');');
                $("#taskSlideAmount_" + userIdglb).attr('permanentvalue', result[i].taskParticipant[j].task_user_percentage);
                if (result[i].taskParticipant[j].user_task_status != "Inprogress") {


                    let mouseup = 'changeStatusOfTaskToProgress(' + result[i].task_id + ', ' + prjid + ',\'Inprogress\', \'editslide\', ' + result[i].task_type + ')'
                    $("#slide_" + userIdglb).attr('onchange', mouseup)

                }


            }
            $("#user_id_" + result[i].taskParticipant[j].user_id).attr('userstatus', 'E');
            $("#user_id_" + result[i].taskParticipant[j].user_id).find("#taskSlideAmount_" + result[i].taskParticipant[j].user_id).val(result[i].taskParticipant[j].task_user_percentage);
            $("#user_id_" + result[i].taskParticipant[j].user_id).find("#taskSlideAmount_" + result[i].taskParticipant[j].user_id).trigger('onblur');

            $("#user_id_" + result[i].taskParticipant[j].user_id).find(".estimateHour").val(result[i].taskParticipant[j].task_est_hours);

            $("#user_id_" + result[i].taskParticipant[j].user_id).find(".estimateMin").val(result[i].taskParticipant[j].task_est_mins);

            if (parseInt(result[i].taskParticipant[j].task_est_hours) > 0 || parseInt(result[i].taskParticipant[j].task_est_mins) > 0) {
                $("#user_id_" + result[i].taskParticipant[j].user_id).attr('modified', 'modified');
            }


            let actualTime = (parseInt(result[i].taskParticipant[j].working_hours) * 60) + parseInt(result[i].taskParticipant[j].working_minutes);

            let actualtimehour = Math.floor(actualTime / 60);
            let actualtimeMin = Math.floor(actualTime % 60);
            // $("#user_id_"+result[0].taskParticipant[i].user_id).find(".actualhour").text(actualTime);
            $("#user_id_" + result[i].taskParticipant[j].user_id).find(".acthour").val(actualtimehour);
            $("#user_id_" + result[i].taskParticipant[j].user_id).find(".actmin").val(actualtimeMin);


        }
        for(let m=0;m<result[i].approval_id.length;m++){
            let imgName = lighttpdpath + "/userimages/" + result[i].approval_id[m].user_id+"."+result[i].approval_id[m].user_image_type + "?" + d.getTime();

            // let stD=result[i].approval_id[m].requested_date.split(" ")[1]+" "+result[i].approval_id[m].requested_date.split(" ")[0]+","+" "+result[i].approval_id[m].requested_date.split(" ")[2];
            // console.log("stD-->"+stD);
            let stTime=result[i].approval_id[m].requested_time.split(" ")[0];
            let stM=result[i].approval_id[m].requested_time.split(" ")[1];
            let stTime1=stTime+":00"+" "+stM;
            let startDate1=returnFormatedDate(result[i].approval_id[m].requested_date);
            let startTime1=returnFormatedTime(stTime1);


            let appD="";
            let ap1Time1="";

            if(result[i].approval_id[m].approved_date=="" && result[i].approval_id[m].approved_time==""){
                appD="";
                ap1Time1="";
            }else{
                appD=returnFormatedDate(result[i].approval_id[m].approved_date);

                let apTime=result[i].approval_id[m].approved_time.split(" ")[0];
                let apM=result[i].approval_id[m].approved_time.split(" ")[1];
                let apTime1=apTime+":00"+" "+apM;
                ap1Time1=returnFormatedTime(apTime1);
            }

            // console.log(startDate1+"----"+startTime1);
          //   // alert(calculateElapseTime(result[0].taskParticipant[i].elapsed_time) +"----"+result[0].taskParticipant[i].elapsed_time)
            addUserToData1(prjid, result[i].approval_id[m].user_full_name, result[i].approval_id[m].user_id, '',imgName,'edit',taskId,calculateElapseTime(result[i].approval_id[m].elapsed_time),startDate1,appD,"","E",result[i].approval_id[m].sequence,result[i].approval_id[m].user_task_status,startTime1,ap1Time1);
            
            if(result[i].approval_id[m].user_task_status=="Approved"){
                $("#calenderForTask2_"+result[i].approval_id[m].user_id).removeAttr("onclick");
            }

          }

        // globalUIArray[count]=$(".taskappendDiv").html();
        //  console.log($(".taskappendDiv").html())
        globalUIArray[count] = $(".workflowUI").clone();

        $(".workflowUI").remove();
        $(".task_user_new_div").remove();
        $(".task_user_new_div1").remove();


        stepMaintainArray[count] = count;
        if (datawF.length == 0) {
            datawF.push({
                id: 1,
                description: result[i].task_name,
                children: [],
                className: className,
            })

        } else {
            if (datawF[0].children.length == 0) {
                datawF[0].children.push({
                    id: 2,
                    description: result[i].task_name,
                    children: [],
                    className: className,
                })
            } else {
                let child = datawF[0].children;
                while (typeof (child) != 'undefined' && child.length >= 1) {
                    child = child[0].children;
                }

                child.push({
                    id: count,
                    description: result[i].task_name,
                    children: [],
                    className: className,
                })
            }

        }
        // datawF=datawF[0].children;

        count++;
        $(".taskappendDiv").html('');

    }
    $('#my-container').hortree({
        data: datawF
    });
    removeIcons();
    //$('div[class^=line]').css('top', '37px');
    //$('div[class^=line]').css('width', '64px');
    $(".taskappendDiv").html('');
    showTaskUI('1', result[0].task_name);
    $(".selectedit").remove();
    $(".userselect").html('');
    $('div[class^=selectedit]').remove();

}


function updateWFTask(userData) {
    let workflowname = $("#workflowname").val();
    let priority = $(".changePriorityWf").attr('priority');
    let group_id = $("#changeGroupwf").attr('group_id');
    let priorityId = priority == 0 ? "6" : priority;
    let wfId = $("#Workflow").attr('workflowid');
    let jsonData = {
        "workflow_id": wfId,
        "priority": priorityId,
        "workflow_name": workflowname,
        "user_id": userIdglb,
        "company_id": companyIdglb,
        "taskProject": prjid,
        "task_group_id": group_id,
        "taskDate": userData,
        "removeEmailId": ""
    }
    console.log(jsonData)
    $.ajax({
        url: apiPath + "/" + myk + "/v1/updateWFTask",
       // url: "http://localhost:8080/v1/updateWFTask",
        type: "PUT",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonData),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $("#loadingBar").hide();
            timerControl("");
        },
        success: function (resp) {
            $(".addTaskDiv").fadeOut(300, function () { $(this).remove(); });
            //$("#taskList").prepend(createTaskUI(resp));
            fetchTasks('', 'firstClick');
            $("#Workflow").attr('workflowmode', '');
            if (gloablmenufor == 'Myzone') {
                $(".tasklistProjectImage").show();
            }
            clearWFData();
        }
    });
}

function uploadFileForwfTask(obj) {
    let inputFiles = obj.files[0];
    let resourceId = $("#Workflow").attr('workflowid');
    resourceId = typeof (resourceId) != 'undefined' ? resourceId : "0";
    if (obj.files && obj.files[0]) {
        var formData = new FormData();
        formData.append('file', inputFiles);
        formData.append('user_id', userIdglb);
        formData.append('company_id', companyIdglb);
        formData.append('resourceId', resourceId);
        formData.append('projId', prjid);
        formData.append('place', "CalenderWorkFlowDocUpload");
        formData.append('taskType', "Project");
        formData.append('docType', 'flowlevel');

        $.ajax({
            url: apiPath + "/" + myk + "/v1/upload",
            type: 'POST',
            processData: false,
            contentType: false,
            cache: false,
            data: formData,
            dataType: 'text',
            data: formData,
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
            },
            success: function (url) {
                fetchWFDocDetails(url);

            }
        });
    }
}

function fetchWFDocDetails(resp) {
    let jsonData = "";
    let wfId = $("#Workflow").attr('workflowid');
    let action = "update"
    if (typeof (wfId) != 'undefined' && wfId != '') {
        jsonData = {
            "Wrkflowtype": "flowlevel",
            "workflow_id": wfId
        }
        action = "create"
    } else {
        jsonData = {
            "Wrkflowtype": "flowlevel",
            "documentIds": resp.split("@@##")[1] + ","
        }
    }

    $.ajax({
        url: apiPath + "/" + myk + "/v1/FetchWorkflowTaskLevelDocuments",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonData),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $("#loadingBar").hide();
            timerControl("");
        },
        success: function (resp) {
            let UI = prepareWfDocUI(resp);
            if (action == 'update') {
                $(".WfDocument").html(UI);
            } else {
                $(".WfDocument").append(UI);
            }

        }
    });
}

function prepareWfDocUI(resp) {
    let UI = "";
    for (i = 0; i < resp.length; i++) {
        let ext = resp[i].document_physical_name;
        ext = ext.substr(ext.lastIndexOf(".") + 1).toLowerCase();
        // let onclick="";
        // let ondoubleckick="";

        if (ext.toLowerCase() == "png" || ext.toLowerCase() == "jpg" || ext.toLowerCase() == "jpeg" || ext.toLowerCase() == "gif" || ext.toLowerCase() == "svg") {
            onclickexpand = "expandImage(" + resp[i].document_id + ",'" + ext + "','task')";
            onclickDownload = " downloadActFile(" + resp[i].document_id + "," + prjid + ",'" + ext + "'," + resp[i].workflow_document_id + ",'task')";
        } else if (ext.toLowerCase() == "mp3" || ext.toLowerCase() == "wav" || ext.toLowerCase() == "wma" || ext.toLowerCase() == "mp4" || ext.toLowerCase() == ".m4v" || ext.toLowerCase() == "mov" || ext.toLowerCase() == "flv" ||
            ext.toLowerCase() == "f4v" || ext.toLowerCase() == "ogg" || ext.toLowerCase() == "ogv" || ext.toLowerCase() == "wmv" ||
            ext.toLowerCase() == "vp6" || ext.toLowerCase() == "vp5" || ext.toLowerCase() == "mpg" || ext.toLowerCase() == "avi" ||
            ext.toLowerCase() == "mpeg" || ext.toLowerCase() == "webm") {

        } else {

            onclickexpand = "viewdocument(" + resp[i].document_id + ",'" + ext + "','task')";
            onclickDownload = " downloadActFile(" + resp[i].document_id + "," + prjid + ",'" + ext + "'," + resp[i].workflow_document_id + ",'task')";
        }
        UI += '<div class="d-flex pb-2 mt-2 position-relative taskdoc" onmouseleave="hideGroupEdit(' + resp[i].workflow_document_id + ',\'userdocument\');" onmouseover="showGroupEdit(' + resp[i].workflow_document_id + ',\'userdocument\');" style="cursor: pointer;border-bottom:1px solid #EAEAEA" id="doc_' + resp[i].workflow_document_id + '">'
            + '<div ><img  class="float-left mt-1 ml-2 " style="width:24px;" src="/images/document/' + ext + '.svg" onerror="imageOnFileNotErrorReplace(this)" id="view_27623"></div>'
            + '<div class="ml-2 mt-1 defaultExceedCls" style="font-size:12px">' + resp[i].document_physical_name + '</div>'
            + '<div id="subcommentDiv_userdoc_' + resp[i].workflow_document_id + '" class="actFeedOptionsDiv" style="border-radius: 8px; top: 1px; margin-right: 5px; display: none;"><div class="d-flex align-items-center"><img src="images/conversation/expand.svg" onclick="' + onclickexpand + '" title="View" style="width:15px;height:15px;" class="mx-1" /> <img src="images/conversation/download2.svg"  title="dounload" style="width:15px;height:15px;"  class="mx-1"  onclick="' + onclickDownload + '" /><img class="mx-1" title="Delete" onclick="deleteUserDoc(' + resp[i].workflow_document_id + ')" src="/images/task/minus.svg" id="delete_group" style="width:18px;height:18px;"></div></div>'
            + '</div>'
    }
    return UI;
}

function deleteSteps(task_id, leve_id) {
    $.ajax({
        url: apiPath + "/" + myk + "/v1/wfTaskLevelDelete?taskId=" + task_id + "&levelId=" + leve_id + "&userId=" + userIdglb + "",
        //url:  "http://localhost:8080/v1/wfTaskLevelDelete?taskId="+task_id+"&levelId="+leve_id+"&userId="+userIdglb+"",
        type: "DELETE",
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $("#loadingBar").hide();
            timerControl("");
        },
        success: function (resp) {

        }
    });
}

function reOrder(id, order) {
    if (order == 'forward') {
        if (id.toString().indexOf("_") > -1) {
            // let mainid = id.toString().split("_")[0];
            // let json = sublevelArray.get(mainid);
            // let child = json;
            // let prev = json
            // while (typeof (child) != 'undefined' && child[0].id != id) {
            //     prev = child
            //     child = child[0].children;
            // }
            // let store = child[0].children[0].children
            // prev[0].children = child[0].children;
            // prev[0].children[0].children = child
            // child[0].children = store
            // sublevelArray.set(mainid, json);
            // child = datawF[0].children;
            // if (datawF[0].id == mainid) {
            //     child = datawF;
            //     datawF[0].description = $("#stepdesc_1").text();
            // } else {
            //     while (typeof (child) != 'undefined' && typeof (child[0]) != 'undefined' && child[0].id != mainid) {
            //         child = child[0].children;
            //     }
            // }
            // child[0].children[1] = sublevelArray.get(mainid)[0];
            // $('#my-container').hortree({
            //     data: datawF
            // });
            // removeIcons();
            // $("#block_undefined").remove();
            // $(".hortree-label-active").removeClass('hortree-label-active');
            // showTaskUI(id)
            subStepReorder(id,'forward')

        } else {
            id=parseInt(id)
            let nextindex = stepMaintainArray.indexOf(id) + 1
            console.log(stepMaintainArray)
            stepMaintainArray.splice(stepMaintainArray.indexOf(id), 1);
            stepMaintainArray.splice(nextindex, 0, id);
            console.log(stepMaintainArray)
            prepareReorderUI(id);
        }
        //   console.log(stepMaintainArray)
    } else {
        if (id.toString().indexOf("_") > -1) {
            //alert("here");
            subStepReorder(id,'backward')
        } else {
            id=parseInt(id)
            let value = stepMaintainArray[stepMaintainArray.indexOf(id) - 1]
            stepMaintainArray.splice(stepMaintainArray.indexOf(id) - 1, 1);
            stepMaintainArray.splice(parseInt(stepMaintainArray.indexOf(id)) + 1, 0, value);
            prepareReorderUI(id);
        }

        // console.log(stepMaintainArray)
    }


}


function prepareReorderUI(id, place) {
    datawF = [];

    //$("#workflowdepTask").html('')
    for (i = 1; i < stepMaintainArray.length; i++) {
        $(".taskappendDiv").html(globalUIArray[stepMaintainArray[i]]);

        if (datawF.length == 0) {
            datawF.push({
                id: stepMaintainArray[i],
                description: $(".taskappendDiv").find("#taskname").val(),
                children: []
            })

        } else {
            if (datawF[0].children.length == 0) {
                datawF[0].children.push({
                    id: stepMaintainArray[i],
                    description: $(".taskappendDiv").find("#taskname").val(),
                    children: []
                })
            } else {
                let child = datawF[0].children;
                while (typeof (child) != 'undefined' && child.length >= 1) {
                    child = child[0].children;
                }

                child.push({
                    id: stepMaintainArray[i],
                    description: $(".taskappendDiv").find("#taskname").val(),
                    children: []
                })
            }

        }
    }
    console.log(datawF)
    $(".taskappendDiv").html('');
    $('#my-container').hortree({
        data: datawF
    });
    removeIcons();
    $(".hortree-label-active").removeClass('hortree-label-active');
    $("#block_" + id).addClass('hortree-label-active');
    // $('div[class^=line]').css('top', '37px');
    //$('div[class^=line]').css('width', '64px');
    $('div[id^=WfOption_]').hide();
    if (place != "show" && typeof (id) != 'undefined') {
        showTaskUI(id);
    } else if (typeof (id) == 'undefined') {
        showTaskUI(datawF[0].id);
    }
}
function showReorder(id) {

    $("#back_" + id).toggle();
    $("#forward_" + id).toggle();
    $('div[id^=WfOption_]').hide();
    $("#back_" + stepMaintainArray[1]).hide();
    $("#forward_" + stepMaintainArray[stepMaintainArray.length - 1]).hide();

    if(id.toString().indexOf("_")>-1){

        let json=sublevelArray.get(id.toString().split("_")[0]);
        let fistone=json[0].id;
        let prev=json;
        while(typeof(json)!='undefined' && json.length>=1){
            prev=json
            json=json[0].children;
        }
        
        let lastone=prev[0].id
        $("#back_" +fistone).hide();
        $("#forward_" +lastone).hide();
    
    }
}
function removeIcons() {
    validateAllsteps();
    $("#back_" + stepMaintainArray[1]).hide();
    $("#forward_" + stepMaintainArray[stepMaintainArray.length - 1]).hide();
    $('div[class^=line]').css('z-index', '-1');
}

function checkDocumentMandate(id) {
    let obj = $("#documentMand_" + id);
    if ($(obj).attr('src').includes('mand')) {
        conFunNew("Mandatory Attachment not required ?", 'warning', "changeImageofdoc", '', obj, id);
    } else {
        conFunNew("Confirm Mandatory Attachment?", 'warning', "changeImageofdoc", '', obj, id);
    }
}
function changeImageofdoc(obj, id) {
    showTaskUI(id);
    if (!$("#documentMand_" + id).attr('src').includes('mand')) {
        $(obj).attr('src', '/images/idea/document_mand.svg');
        $(".workflowUI").attr('documentmandatory', 'Y');
        $(obj).show();
    } else {
        $(obj).attr('src', '/images/idea/document.svg');
        $(".workflowUI").attr('documentmandatory', 'N');
        $(obj).hide();
    }
    validateAllsteps();
}

function makeActiveInactive(obj, id) {
    showTaskUI(id);

    if ($(".workflowUI").attr('active') != 'grayedOut') {
        $(".workflowUI").css('pointer-events', 'none');
        $("#active_block_" + id).attr('src', '/images/task/inactivestep.svg');
        $(".workflowUI").attr('active', 'grayedOut')
    } else {
        $(".workflowUI").css('pointer-events', 'auto');
        $("#active_block_" + id).attr('src', '/images/task/activestep.svg');
        $(".workflowUI").attr('active', '')
    }
}

function validateAllsteps() {
    for (i = 1; i < stepMaintainArray.length; i++) {
        $(".taskappendDiv").html(globalUIArray[stepMaintainArray[i]]);
        if ($(".taskappendDiv").find('.workflowUI').attr('active') == 'grayedOut') {

            $("#active_block_" + stepMaintainArray[i]).attr('src', '/images/task/inactivestep.svg');
        }
        if (parseInt($(".taskappendDiv").find('.workflowUI').attr('documentcount')) > 0) {
            $("#docstep_" + stepMaintainArray[i]).show();
        }
        if ($(".taskappendDiv").find('.workflowUI').attr('documentmandatory') == 'Y') {
            $("#documentMand_" + stepMaintainArray[i]).attr('src', '/images/idea/document_mand.svg');
            $("#documentMand_" + stepMaintainArray[i]).show();
        } else {
            $("#documentMand_" + stepMaintainArray[i]).hide();
        }
        if (typeof ($(".taskappendDiv").find('.workflowUI').attr('depid')) != 'undefined' && $(".taskappendDiv").find('.workflowUI').attr('depid') != "0") {
            $("#depWF_" + stepMaintainArray[i]).show();
        }
        if (typeof ($(".taskappendDiv").find('.workflowUI').attr('colorcode')) != 'undefined' && $(".taskappendDiv").find('.workflowUI').attr('colorcode') != "") {
            $("#block_" + stepMaintainArray[i]).css('background', $(".taskappendDiv").find('.workflowUI').attr('colorcode'));
            $("#block_" + stepMaintainArray[i]).css('border-color', $(".taskappendDiv").find('.workflowUI').attr('colorcode'));
        }
        if (typeof ($(".taskappendDiv").find('.workflowUI').attr('group_color')) != 'undefined' && $(".taskappendDiv").find('.workflowUI').attr('group_color') != "") {
            $("#block_" + stepMaintainArray[i]).css('background', $(".taskappendDiv").find('.workflowUI').attr('group_color'));
            $("#block_" + stepMaintainArray[i]).css('border', "2px solid "+ $(".taskappendDiv").find('.workflowUI').attr('group_color'));
          
        }
        else {
            let bd="2px solid "+ $("#changeGroupwf").css('background-color');
            $("#block_" + stepMaintainArray[i]).css('background', $("#changeGroupwf").css('background'));
            $("#block_" + stepMaintainArray[i]).css('border',bd );
        }

    }
    $(".taskappendDiv").html('');
    validateSubStep()
    //$('div[class^=line]').css('top', '37px');
    // $('div[class^=line]').css('width', '64px');
}

function showInputStep(id, obj) {
    showTaskUI(id, 'from');

    $("#stepdesc_" + id).hide();
    $("#stepdesc_" + id).next().show();
    $("#stepdesc_" + id).next().val($("#stepdesc_" + id).text().trim());
    $("#stepdesc_" + id).next().focus();

}
function hideInputStep(id, obj) {
    showTaskUI(id);
    if ($(obj).val() == '') {

       // alertFunNew("Task name cannot be Empty", 'warning', 'min', obj);
        conFunNew("Would you like to change this step name ?",'warning','showInputStep','changeStepname',id)
        return false;
    } else {
        $(obj).hide();
        $(obj).prev().show();
        $(obj).prev().text($(obj).val());
        $(".workflowUI").find("#taskname").val($(obj).val());
        $(obj).prev().attr('title', $(obj).val())
    }
}
function fetchWorkflowDependency(id, place) {
    if (id.toString().indexOf("_") > -1) {
        return false;
    }
    let workflowid = $("#Workflow").attr('workflowid');
    showTaskUI(id)
    $('div[id^=WfOption_]').hide();

    if (typeof ($(".workflowUI").attr('depid')) != 'undefined' && $(".workflowUI").attr('depid') != "0" && place == "sublev") {
        if ($(".workflowUI").attr('showdep') != "active") {
            fetchWFStepdetails($(".workflowUI").attr('depid'), 'dependency');
            $("#depTask_" + $(".workflowUI").attr('depid')).remove();
            $("#workflowdepTask").html('');
            $(".workflowUI").attr('showdep', 'active');
        } else {
            $(".workflowUI").attr('showdep', '');
            prepareReorderUI(id);
        }
    } else {
        if (typeof (workflowid) == 'undefined') {
            workflowid = "0";
        }
        let jsonData = {
            "project_id": prjid,
            "user_id": userIdglb,
            "workflow_id": workflowid
        }
        $.ajax({ //
            url: apiPath + "/" + myk + "/v1/getTaskDetails/workflowDependencyList",
            // url: "http://localhost:8080/v1/getTaskDetails/workflowDependencyList",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonData),
            success: function (result) {
                let resu = false;
                if (typeof ($(".workflowUI").attr('depid')) != 'undefined' && $(".workflowUI").attr('depid') != "0") {
                    resu = true;
                    fetchWFStepdetails($(".workflowUI").attr('depid'), 'dependency');
                    //  $("#depTask_" + $(".workflowUI").attr('depid')).remove();
                    $("#cWF_" + $(".workflowUI").attr('depid')).trigger('onchange');
                    $(".workflowUI").attr('showdep', 'active');
                }
                let ui = prepareWFDependency(result);
                $("#workflowdepTask").html(ui);
                $("#backsearch").on('click', () => {
                    $("#workflowdepTask").html('');
                })
                if (resu) {
                    $("#cWF_" + $(".workflowUI").attr('depid')).prop('checked', true);
                  
                }

            }
        });
    }
}

function prepareWFDependency(result) {
    let ui = "";
    ui +=
        "<div class='DependentTaskDiv' style=' '>"
        + "<div class=' d-flex justify-content-between align-items-center pb-1' style='    border-bottom: 1px solid darkgray;' >"
        + '<div class="d-flex w-75">'
        + '<div id="" class="pl-1 mr-1" style="font-size:12px;" >Workflow Task</div>'
        + '<img class="" id="backsearch" src="images/task/dowarrow.svg" style="height:15px;width:15px; cursor:pointer;margin-top:2px">'
        + '</div>'
        //+'<input type="text" class="pl-1"  id="dependencysearch" style="display:none;border: 0;width: 90%;outline: none;border-bottom:1px solid #b4adad"/>'
        + '<div>'
        //   +'<img class="mt-1 mr-1" id="entersearch" src="images/menus/search2.svg" style="height:18px;width:18px; cursor:pointer;margin-top:2px">'
        // +'<img class="" id="backsearch" src="/images/back1.png" style="height:15px;width:15px; cursor:pointer;margin-top:2px">'
        + '</div>'
        + "</div>"
        + "<div class='d-flex py-1 headerBar w-100 mt-1' style='z-index: 1;'> " +

        "<div class='defaultExceedCls pl-2' id='headertaskType' style='width: 8%;'></div>" +
        "<div class='defaultExceedCls pl-2' id='headertaskType' style='width: 8%;'>Type</div>" +
        "<div class='defaultExceedCls' id='headertaskgroup' style='width: 8%;'>Group</div>" +
        "<div class='defaultExceedCls' id='headertaskgroup' style='width: 12%;'>Created by</div>" +
        "<div class='defaultExceedCls' style='width: 6%;'>ID</div>" +
        "<div class='defaultExceedCls' id='headerTaskname' style='width: 35%;'>Task</div>" +
        //"<div class='defaultExceedCls' style='width: 9%;'>Start</div>"+
        

        "<div class='defaultExceedCls' style='width: 8%;text-align: center;'>Priority</div>" +
        //"<div class='defaultExceedCls' style='width: 8%;text-align: center;'>Status</div>"+
        // "<div class='defaultExceedCls' style='width: 6%;text-align: center;'>Trend</div>"+



        "</div>"

        + "<div class='dependenctTaskIds defaultScrollDiv' id='DependenttaskList' style='font-size: 12px;height:200px;overflow:auto;'>"

    for (i = 0; i < result.length; i++) {



        let Priority = result[i].priority;
        let Priority_image = (Priority == "") ? "" : Priority == "1" ? "images/task/p-one.svg" : Priority == "2" ? "images/task/p-two.svg" : Priority == "3" ? "images/task/p-three.svg" : Priority == "4" ? "images/task/p-four.svg" : Priority == "5" ? "images/task/p-five.svg" : "images/task/p-six.svg";
        let priorityTitle = Priority == "" ? "" : Priority == "1" ? "Very important" : Priority == "2" ? "Important" : Priority == "3" ? "Medium" : Priority == "4" ? "Low" : Priority == "5" ? "Very low" : Priority == "6" ? "None" : "";



        ui += "<div class='d-flex py-1  w-100'   style='z-index: 1;font-size:12px;border-bottom:1px solid darkgray' id='depTask_" + result[i].workflow_id + "' > " +

            "<div class='defaultExceedCls pl-4' id='' style='width: 8%;'>"
            + '<input  class="checkWF mt-1" id="cWF_' + result[i].workflow_id + '" type="checkbox" style="cursor:pointer"  onchange="checkWFdependency(this,' + result[i].workflow_id + ')" />'
            + "</div>" +
            "<div class='defaultExceedCls pl-3' id='' style='width: 8%;'>"
            + '<img id="" src="images/task/workflow.svg" class="task-img img-responsive tTypeImage" />'
            + "</div>" +
            "<div class='defaultExceedCls pl-3' id='' style='width: 8%;'>"
            + "<div title='" + result[i].group_name + "' class='rounded-circle' style='background:" + result[i].group_code + " ;width:15px;height:15px;box-shadow: 0 6px 10px rgb(0 0 0 / 18%);'></div>"

            + "</div>" +
            "<div class='defaultExceedCls pl-3' id='' style='width: 12%;'>"

            + '<img id="personimagedisplay" src="https://testbeta.colabus.com:1933///userimages/' + result[i].created_by + '.jpeg" onerror="imageOnProjNotErrorReplace(this);" class="mr-3 rounded-circle " style="width:25px;height:25px;">'
            + "</div>" +
            "<div class='defaultExceedCls' style='width: 6%;'>" + result[i].workflow_id + "</div>" +
            "<div class='defaultExceedCls' id='wfTask1_"+result[i].workflow_id+"' style='width: 35%;'>" + result[i].task_name + "</div>" +
            //"<div class='defaultExceedCls' style='width: 9%;'>Start</div>"+
            //"<div class='defaultExceedCls' style='width: 9%;'></div>" +

            "<div class='defaultExceedCls pl-3' style='width: 8%;text-align: center;'>"
            + '<img id="personimagedisplay" src="' + Priority_image + '" onerror="imageOnProjNotErrorReplace(this);" class="mr-3  "  title="' + priorityTitle + '" style="width:20px;height:20px;">'
            + "</div>" +
            "<div class='defaultExceedCls pl-3' style='width: 8%;text-align: center;'>"
            //+'<img id="personimagedisplay" src="'+taskStatusImage+'" onerror="imageOnProjNotErrorReplace(this);" class="mr-3  "  title="'+taskStatusImageTitle+'" style="width:20px;height:20px;">'
            + "</div>" +
            "<div class='defaultExceedCls pl-3' style='width: 6%;text-align: center;'>"
            //  +'<img id="personimagedisplay" src="'+sentimentBgCol+'" onerror="imageOnProjNotErrorReplace(this);" class="mr-3  "  title="'+trending+'" style="width:20px;height:20px;">'
            + "</div>"

            + "</div>"

    }

    ui += "</div>" +
        "</div>";



    return ui;
}

function addSubSteps(result,from) {

    let childernJSON = [];
    let id = $(".workflowUI").attr('idoftask');
    $(".workflowUI").attr('showdep', 'active');
    showTaskUI(id);
    $(".workflowUI").remove();
    $(".taskappendDiv").html('');
    let count = 1;
    for (let i = 0; i < result.length; i++) {
        let idofnew = "";
        if (childernJSON.length == 0) {
            childernJSON.push({
                id: id + "_" + count,
                description: result[i].task_name,
                children: [],
                pointer_event: "N"
            });
            idofnew = id + "_" + count;
        } else {

            if (childernJSON[0].children.length == 0) {
                childernJSON[0].children.push({
                    id: id + "_" + count,
                    description: result[i].task_name,
                    children: [],
                    pointer_event: "N"
                })
                idofnew = id + "_" + count;
            } else {
                let child = childernJSON[0].children;
                while (typeof (child) != 'undefined' && child.length >= 1) {
                    child = child[0].children;
                }

                child.push({
                    id: id + "_" + count,
                    description: result[i].task_name,
                    children: [],
                    pointer_event: "N"
                })
                idofnew = id + "_" + count;
            }


        }
        prepareStepUiandAppend(idofnew, result[i],from);
        count++;
    }
 
    sublevelArray.set(id, childernJSON);
    let child = datawF[0].children;

    if (datawF[0].id == id) {
        child = datawF;
        datawF[0].description = $("#stepdesc_1").text();
    } else {
        while (typeof (child) != 'undefined' && typeof (child[0]) != 'undefined' && child[0].id != id) {
            child = child[0].children;
        }
    }

    console.log(childernJSON);
    //console.log(child);
    if (child[0].children.length > 1) {

        child[0].children.length = 1;
    }
    let res = false;
    if (child[0].children.length == 0) {
        child[0].children[0] = { children: [] }
        res = true;
    }
    if (typeof (child[0].children[0]) == 'undefined') {
        child[0].children[0] = {}
    } else {
        child[0].children.push(childernJSON[0]);
    }

    $('#my-container').hortree({
        data: datawF
    });
    if (res) {

    }
    $("#block_undefined").remove();
    removeIcons();
    // $('div[class^=line]').css('top', '37px');
    // $('div[class^=line]').css('width', '64px');
    showTaskUI(childernJSON[0].id)
    for (i = 0; i < result.length; i++) {

        if (result[i].group_code != "") {
            $("#block_" + result[i].task_id).css('background-color', result[i].group_code);
            $("#block_" + result[i].task_id).css('border-color', result[i].group_code);
        }
    }
    addBox(id);
}
function validateUI(id) {

    $("#stepdesc_" + id).text('');
    try {
        $("#stepdesc_" + id).trigger('onclick');
    } catch (error) {
        console.log(error)
    }
    let idnew=id;
    if(id.toString().indexOf("_")>-1){
        idnew=id.split("_")[1];
    }
    $("#input_" + id).attr('placeholder', 'Step ' + idnew);

}

function fetchWFTemplatedetails() {
    if ($("#wftemplates").length > 0) {
        $("#wftemplates").toggle();
    } else {
        $.ajax({ //
            url: apiPath + "/" + myk + "/v1/getworkflowTemplate?user_id=" + userIdglb + "&company_id=" + companyIdglb,
            type: "GET",
            dataType: 'json',
            contentType: "application/json",
            success: function (result) {
                let UI = prepareTemplateUI(result);
                $(UI).insertAfter("#wfTemplateViewImg");
            }
        });
    }
}
function prepareTemplateUI(result) {


    let UI = "";
    UI += '<div id="wftemplates" class="defaultScrollDiv wftemplates" style="margin-top: 21px; position: absolute; color: black; background: rgb(255, 255, 255); border: 1px solid rgb(166, 166, 166); padding: 7px;height: 330px; overflow: auto; border-radius: 2px; font-size: 12px; width: 295px; z-index: 12; text-align: left;"><div style="padding: 2px 0px;">	'

        + '<div  style="font-size: 14px; border-bottom: 1px solid darkgray; margin-bottom: 10px;">Workflow Templates</div>'
    for (i = 0; i < result.length; i++) {
        UI += '<div class="defaultExceedCls templates px-2 pb-1 pt-2 align-items-center"  style="width: 100%;cursor: pointer;float:left;justify-content: space-between;display:flex;border-bottom:1px solid #DADBDB" title="' + result[i].wf_template_name + '" onclick="fetchStepandShowStep(' + result[i].wf_template_id + ',\'task\');"><div class="defaultExceedCls"style=" width: 90%;float: left;margin-right: 8px;margin-top: -1px;">' + result[i].wf_template_name + ' </div><div class="ml-3"  onclick="fetchStepandShowStep(' + result[i].wf_template_id + ',\'view\');"><img class="wfTempCss wfTempViewIcon" src="/images/conversation/view.svg"  title="View template" style="float:left;margin-top:-2px;border:none;width:18px"></div></div>'
    }
    UI += '</div>'
    return UI;
}

function fetchStepandShowStep(tId, place) {
    $.ajax({ //
        url: apiPath + "/" + myk + "/v1/getTemplatestepDetails?fileNameId=" + tId + "&company_id=" + companyIdglb + "&user_id=" + userIdglb,
        type: "GET",
        dataType: 'json',
        contentType: "application/json",
        success: function (result) {
            $("#wftemplates").hide();
            if (place == 'view') {
                prepareWfTemplateShowUI(result)
            } else {
                getStepandConvertTask(result)
            }
        }
    });
}
function prepareWfTemplateShowUI(result) {
    let dataTem = [];
    for (i = 0; i < result.length; i++) {


        if (dataTem.length == 0) {
            dataTem.push({
                id: result[i].level_id,
                description: result[i].level_name,
                children: [],
                pointer_event: "N"
            })

        } else {
            if (dataTem[0].children.length == 0) {
                dataTem[0].children.push({
                    id: result[i].level_id,
                    description: result[i].level_name,
                    children: [],
                    pointer_event: "N"
                })
            } else {
                let child = dataTem[0].children;
                while (typeof (child) != 'undefined' && child.length >= 1) {
                    child = child[0].children;
                }

                child.push({
                    id: result[i].level_id,
                    description: result[i].level_name,
                    children: [],
                    pointer_event: "N"
                })
            }

        }
    }
    //console.log(datawF)
    //$(".taskappendDiv").html('');
    $('#my-container').hortree({
        data: dataTem
    });
    $('div[id^=block_]').css('pointer-events', 'none');
}
function getStepandConvertTask(result) {
    let count = 1;
    clearWFData();
    $(".workflowUI").remove();
    for (i = 0; i < result.length; i++) {
        let UI = "<div class='col-12 p-0 workflowUI'  style='' idoftask='" + count + "'  documentmandatory='" + result[i].level_document + "'>"
        UI += prepareAddTaskUiNew('workflow')
        UI += '</div>';
        $(".taskappendDiv").html(UI);
        createPopupDiv('fromworkflowedit');

        $(".taskappendDiv").find("#taskname").val(result[i].level_name);


        $(".selectedit").remove();
        $(".userselect").html('');
        $('div[class^=selectedit]').remove();

        $("#comments").removeClass('d-none').addClass('d-block');
        //$("#dependencyTask").removeClass('d-block').addClass('d-none');
        $("#comments").parent().removeClass('d-none').addClass('d-block');

        globalUIArray[count] = $(".workflowUI").clone();

        $(".workflowUI").remove();
        $(".task_user_new_div").remove();
        $(".task_user_new_div1").remove();


        stepMaintainArray[count] = count;
        if (datawF.length == 0) {
            datawF.push({
                id: 1,
                description: result[i].level_name,
                children: [],
            })

        } else {
            if (datawF[0].children.length == 0) {
                datawF[0].children.push({
                    id: 2,
                    description: result[i].level_name,
                    children: [],
                })
            } else {
                let child = datawF[0].children;
                while (typeof (child) != 'undefined' && child.length >= 1) {
                    child = child[0].children;
                }

                child.push({
                    id: count,
                    description: result[i].level_name,
                    children: [],
                })
            }

        }
        // datawF=datawF[0].children;

        count++;
        $(".taskappendDiv").html('');
    }
    $('#my-container').hortree({
        data: datawF
    });
    removeIcons();
    //$('div[class^=line]').css('top', '37px');
    //$('div[class^=line]').css('width', '64px');
    $(".taskappendDiv").html('');
    showTaskUI('1', result[0].level_name);
    $(".selectedit").remove();
    $(".userselect").html('');
    $('div[class^=selectedit]').remove();

}


function changeColorBlock(obj, id) {
    showTaskUI(id);
    $("#block_" + id).css('background', $(obj).attr('nodecolor'));
    $("#block_" + id).css('border-color', $(obj).attr('nodecolor'));
    $('div[id^=WfOption_]').hide();
    $(".workflowUI").attr('colorcode', $(obj).attr('nodecolor'));
}

function checkWFdependency(obj, id) {
    $('.checkWF').not(obj).prop('checked', false);

    //    if($(obj).prop('checked')==false){
    //        $(".workflowUI").attr('depid','0');
    //        prepareReorderUI($(".workflowUI").attr('idoftask'));
    //        $(".workflowUI").attr('showdep','');
    //    }
    if ($(obj).prop('checked')) {
        fetchWFStepdetails(id, 'dependency','first');
        $(".workflowUI").attr('workflow_name','')
        $(".workflowUI").attr("workflow_name",$("#wfTask1_"+id).text())
    } else {
        $(".workflowUI").attr('depid', '0');
        prepareReorderUI($(".workflowUI").attr('idoftask'));
        $(".workflowUI").attr('showdep', '');
        $(".workflowUI").attr("workflow_name",'')

    }
}

function showColor(status) {

    if (status == "show") {
        $(".depAddDiv").show();
    } else {
        $(".depAddDiv").hide();
    }
}

function fetchWFChild(task_id, Wid) {

    if ($("#wfChildListDiv_" + task_id).length > 0) {
        $("#wfChildListDiv_" + task_id).toggle();
        if ($("#wfChildListDiv_" + task_id).is(":hidden")) {
            $('#wfListImage_' + Wid).attr('src', 'images/task/dependencydown.svg')
        } else {
            $('#wfListImage_' + Wid).attr('src', 'images/task/dependencyup.svg')
        }
        $('#wfChildListDiv_' + task_id).find('.Wflisting').children().last().css('border-bottom', '0');
    } else {
        let jsonbody = {
            "user_id": userIdglb,
            "company_id": companyIdglb,
            "project_id": prjid,
            "workflow_id": task_id
        }
        $.ajax({
            url: apiPath + "/" + myk + "/v1/getTaskDetails/TeamZone",
            //url: "http://localhost:8080/v1/getTaskDetails/TeamZone",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            beforeSend: function (jqXHR, settings) {
                xhrPool.push(jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
                timerControl("");
            },
            success: function (result) {
                let UI = ""
                UI += "<div class='ml-5 pb-2' id='wfChildListDiv_" + task_id + "'> "
                UI += wsTaskUi('subList');
                UI += "<div class='Wflisting'>"
                UI += createTaskUI(result);
                UI += "</div>"
                UI += "</div>"
                $(UI).insertAfter("#TaskStatus_" + Wid);
                $('#wfListImage_' + Wid).attr('src', 'images/task/dependencyup.svg')
                $('#wfChildListDiv_' + task_id).find('.Wflisting').children().last().css('border-bottom', '0');
            }
        });
    }



}

function findAndReplaceUI(id) {
    //console.log(globalSubUIArray)
    for (i = 0; i < globalSubUIArray.length; i++) {
        $(".taskappendDiv").html(globalSubUIArray[i]);
        console.log("---------------------------------in pepare");
        console.log("finding id" + $(".taskappendDiv").find(".workflowUI").attr('idoftask') + "----flag---->" + flag);
        if ($(".taskappendDiv").find(".workflowUI").attr('idoftask') == id) {

            globalSubUIArray.splice(i, 1);
            break;
        }

    }


    // let UI1 =
    // console.log($(".taskappendDiv").find(".workflowUI").clone())

    globalSubUIArray.push($(".taskappendDiv").find(".workflowUI").clone());
    //console.log(UI);
    $(".taskappendDiv").html('');

}
function fetchStepLevelData(id,workflow_name) {
    workflow_name=typeof(workflow_name)=='undefined'?"":workflow_name;
    let json = sublevelArray.get(id.toString());
    let ids = [];
    userData = "";
    let counter = 1;
    console.log(id)
    console.log(json)
    while (typeof (json) != 'undefined' && json.length >= 1) {
        ids.push(json[0].id);
        json = json[0].children
    }

    let user_ids = "";
    console.log("ids-->" + ids)
    $(".workflowUI").remove();
    for (let k = 0; k < ids.length; k++) {

        for (let j = 0; j < globalSubUIArray.length; j++) {
            $(".taskappendDiv").html(globalSubUIArray[j]);
            console.log($(".taskappendDiv").find(".workflowUI").attr('idoftask'))
            if ($(".taskappendDiv").find(".workflowUI").attr('idoftask') == ids[k]) {

                $(".workflowUI").remove();
                $(".taskappendDiv").html('');
                // console.log(globalSubUIArray[j])
                $(".taskappendDiv").html(globalSubUIArray[j]);
                let presentid = $(".workflowUI").attr('idoftask');
                userData += "{";
                userData += "\"defination\":\"" + $(".taskappendDiv").find("#taskname").val().toString().trim() + "\",";
                userData += "\"instructions\":\"" + $(".taskappendDiv").find("#taskdesc").val().toString().trim() + "\",";



                let startDate = $(".taskappendDiv").find("#startDateCalNewUI").text();
                var monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                let startdateMonth = monthNames.indexOf(startDate.substring(0, 3)) < 10 ? "0" + monthNames.indexOf(startDate.substring(0, 3)) : monthNames.indexOf(startDate.substring(0, 3));
                let startdateDate = startDate.substring(4, 6)
                let start = startdateMonth.toString().trim() + "-" + startdateDate.trim() + "-" + startDate.split(",")[1].toString().trim();

                let endDate = $(".taskappendDiv").find("#endDateCalNewUI").text();
                let enddateMonth = monthNames.indexOf(endDate.substring(0, 3)) < 10 ? "0" + monthNames.indexOf(endDate.substring(0, 3)) : monthNames.indexOf(endDate.substring(0, 3));
                let enddateDate = endDate.substring(4, 6)
                let end = enddateMonth.toString().trim() + "-" + enddateDate.trim() + "-" + endDate.split(",")[1].toString().trim();


                let startfullTime = $(".taskappendDiv").find("#startTimeCalNewUI").text();
                let hour = startfullTime.substring(startfullTime.length - 2, startfullTime.length) == "PM" ? (parseInt(startfullTime.split(":")[0].trim()) + 12) : startfullTime.split(":")[0].trim();
                let startTime = hour + ":" + startfullTime.split(":")[1].trim().trim().substring(0, 2) + ":00";


                let endfullTime = $(".taskappendDiv").find("#endTimeCalNewUI").text();
                let endhour = endfullTime.substring(endfullTime.length - 2, endfullTime.length) == "PM" ? (parseInt(endfullTime.split(":")[0].trim()) + 12) : endfullTime.split(":")[0].trim();
                let endTime = endhour + ":" + endfullTime.split(":")[1].trim().substring(0, 2) + ":00";

                let estimateHour = $(".taskappendDiv").find(".estimateHour").val();
                let estimateMin = $(".taskappendDiv").find(".estimateMinute").val();

                if (estimateMin > 59) {
                    estimateHour = parseInt(estimateHour) + Math.floor(parseInt(estimateMin) / 60);
                    estimateMin = Math.floor(parseInt(estimateMin) % 60);
                }

                let saveDocTaskIds = "";
                $('.taskDocument > div').each(function () {
                    saveDocTaskIds = saveDocTaskIds + $(this).attr('id').split("_")[1] + ","
                });
                userData += "\"stepLevelDocId\":\"" + saveDocTaskIds + "\",";
                userData += "\"start_time\":\"" + startTime + "\",";
                userData += "\"end_time\":\"" + endTime + "\",";

                userData += "\"start_date\":\"" + start + "\",";
                userData += "\"end_date\":\"" + end + "\",";

                userData += "\"totalEstHr\":\"" + estimateHour + "\",";
                userData += "\"totalEstmin\":\"" + estimateMin + "\",";

                userData += "\"levelordertemplate\":\"" + counter + "\",";

                //let group_id = $(".taskappendDiv").find("#changeGroup").attr('group_id');
                let group_id = typeof ($(".workflowUI").attr('groupid')) != 'undefined' ? $(".workflowUI").attr('groupid') : $("#changeGroupwf").attr('group_id');
                userData += "\"task_group_id\":\"" + group_id + "\",";

                counter++;
                let priority = $(".taskappendDiv").find(".changePriority").attr('priority');
                let priorityId = priority == 0 ? "6" : priority;

                let depid = $(".taskappendDiv").find(".workflowUI").attr('depid');

                if (typeof (depid) == 'undefined') {

                    depid = "";
                }
                // userData += "\"depId\":\"" + depid + "\",";

                let documentmandatory = "N"
                if ($(".taskappendDiv").find('.workflowUI').attr('documentmandatory') == 'Y') {
                    documentmandatory = "Y"
                }
                userData += "\"leveldocattach\":\"" + documentmandatory + "\",";
                userData += "\"priority\":\"" + priorityId + "\",";

                if ($(".taskappendDiv").find('.workflowUI').attr('active') == 'grayedOut') {
                    userData += "\"taskSpecificClass\":\"grayedOut\",";
                } else {
                    userData += "\"taskSpecificClass\":\"\",";
                }
                //if(place=='update'){
                let levelStatus = $(".workflowUI").attr('levelstatus');

                let levelcolor = $(".workflowUI").attr('colorcode');

                if (levelcolor) {
                    userData += "\"step_color\":\"" + levelcolor + "\",";
                } else {
                    userData += "\"step_color\":\"\",";
                }

                if (levelStatus == "E") {
                    userData += "\"level_name\":\"" + $(".taskappendDiv").find("#taskname").val() + "\",";
                    userData += "\"levelStatus\":\"" + levelStatus + "\",";
                    userData += "\"wfId\":\"" + $(".workflowUI").attr('task_id') + "\",";
                    userData += "\"level_id\":\"" + $(".workflowUI").attr('wflevelid') + "\",";
                } else {
                    userData += "\"level_name\":\"" + $(".taskappendDiv").find("#taskname").val() + "\",";
                    userData += "\"levelStatus\":\"N\",";
                }
                //}
                // console.log(userData);
                let user_ids = "";
                $(".taskappendDiv").find('.userDetails > div').each(function () {
                    let localid = "";
                    if ($(this).attr('id') != 'ignorediv') {
                        var id = $(this).attr('id').split('_')[2];

                        let estH = $(this).find('.estimateHour').val();
                        let estM = $(this).find('.estimateMin').val()
                        if (estM > 59) {
                            estH = parseInt(estH) + Math.floor(parseInt(estM) / 60);
                            estM = Math.floor(parseInt(estM) % 60);
                        }

                        let taskAction = 'Created';
                        if (typeof ($("#persontaskstatusdisplay_" + id).attr('statusoftaskuser')) != 'undefined' && $("#persontaskstatusdisplay_" + id).attr('statusoftaskuser') != '') {
                            taskAction = $("#persontaskstatusdisplay_" + id).attr('statusoftaskuser');

                        }
                        // if (parseInt($(this).find('#taskSlideAmount_'+id).val()) == 100) {
                        //   taskAction = 'Completed';

                        // }else if(parseInt($(this).find('#taskSlideAmount_'+id).val()) >0 && parseInt($(this).find('#taskSlideAmount_'+id).val()) <100){
                        //     taskAction = 'Inprogress';
                        // }
                        localid = $(this).attr('userstatus') + ":" + id + ":" + estH + ":" + estM + ":" + $(this).find('#taskSlideAmount_' + id).val() + ":" + taskAction;
                        user_ids = user_ids + localid + ",";



                    }

                });
                user_ids = user_ids.length > 1 ? user_ids.substring(0, user_ids.length - 1) : "";
                let reminder = $(".taskappendDiv").find("#remindericon").attr('reminder');

                reminder = typeof (reminder) == 'undefined' ? "" : reminder;
                userData += "\"reminder\":\"" + reminder + "\",";
                
                userData += "\"workflow_name\":\"" + workflow_name + "\",";
                userData += "\"reminderType\":\"" + reminder + "\",";


                userData += "\"id\":\"" + user_ids + "\"";
                userData += "},"

                break;
            }


        }
        console.log(id + "--->userdata->" + userData);

    }
    userData = userData.length > 1 ? userData.substring(0, userData.length - 1) : "";
    userData += "]"
    return userData;

}
function prepareStepUiandAppend(id, result,from) {
    //alert("here")
    let activeinactive = result.workflowstep_blur;
    let className = "";
    let style = "";
    let levelstatus = "N"
    if (activeinactive == 'Y') {
        className = "grayedOut";
        style = "pointer-events:none;"
    }
    if ($("#Workflow").attr('workflowmode') == 'update') {
        levelstatus = "E";
    }
    let UI = "<div class='col-12 p-0 workflowUI' task_id='" + result.task_id + "'  style='" + style + "' idoftask='" + id + "' levelstatus='" + levelstatus + "' wflevelid='" + result.workflow_level_id + "'  active='" + className + "'  documentcount='" + result.documentcount + "'  documentmandatory='" + result.level_document + "' depid='" + result.child_workflow_id + "'  colorcode='" + result.step_color + "' group_color='" + result.group_code + "' groupid='" + result.task_group_id + "'>"
    UI += prepareAddTaskUiNew('workflow', result.task_id)
    UI += '</div>';
    $(".taskappendDiv").html(UI);
    createPopupDiv('fromworkflowedit');
    let userStatus="edit";
    if(from=="first"){
        userStatus="create"
    }
    $(".groupDiv").css('visibility', 'hidden');
    $(".groupDiv").html('');

    $(".taskappendDiv").find("#task_id").val(result.task_id);
    $(".selectedit").remove();
    $(".userselect").html('');
    $('div[class^=selectedit]').remove();

    $("#comments").removeClass('d-none').addClass('d-block');
    //$("#dependencyTask").removeClass('d-block').addClass('d-none');
    $("#comments").parent().removeClass('d-none').addClass('d-block');

    let startDate = returnFormatedDate(result.task_start_date)
    let startTime = returnFormatedTime(result.start_time);
    let endDate = returnFormatedDate(result.task_end_date)
    let endTime = returnFormatedTime(result.end_time);

    $(".taskappendDiv").find("#taskname").val(result.task_name);
    $(".taskappendDiv").find("#taskdesc").val(result.task_desc);
    if (result.task_desc.toString().trim() != "") {
        $(".taskappendDiv").find("#instruction").attr('src', '/images/task/intr.svg');
    }
    $(".taskappendDiv").find("#endDateCalNewUI").attr('permanentValue', endDate);
    $(".taskappendDiv").find("#endTimeCalNewUI").attr('permanentValue', endTime);
    $(".taskappendDiv").find("#startDateCalNewUI").attr('permanentValue', startDate);
    $(".taskappendDiv").find("#startTimeCalNewUI").attr('permanentValue', startTime);

    $(".taskappendDiv").find("#endDateCalNewUI").text(endDate);
    $(".taskappendDiv").find("#endTimeCalNewUI").text(endTime);
    $(".taskappendDiv").find("#startDateCalNewUI").text(startDate);
    $(".taskappendDiv").find("#startTimeCalNewUI").text(startTime);

    $(".taskappendDiv").find(".estimateHour").val(result.total_estimated_hour);
    $(".taskappendDiv").find(".estimateMinute").val(result.total_estimated_minute);

    if ($(".taskappendDiv").find(".estimateMinute").val() != "0" || $(".taskappendDiv").find(".estimateHour").val() != "0") {
        $(".taskappendDiv").find(".estimateHour").attr('modified', 'modified');
        $(".taskappendDiv").find(".estimateMinute").attr('modified', 'modified');
    }
    $(".taskappendDiv").find(".estimateHour").attr('permanentValue', result.total_estimated_hour);
    $(".taskappendDiv").find(".estimateMinute").attr('permanentValue', result.total_estimated_minute);
    $(".taskappendDiv").find("#totalHourIn").val($('#estimateHour').text());
    $(".taskappendDiv").find("#totalMinIn").val($('#estimateMinute').text());
    $(".taskappendDiv").find("#task_type").val('Workflow');

    let priority = result.priority == "6" ? "0" : result.priority
    var Priority_image = (priority == "") ? "" : priority == "1" ? "images/task/p-one.svg" : priority == "2" ? "images/task/p-two.svg" : priority == "3" ? "images/task/p-three.svg" : priority == "4" ? "images/task/p-four.svg" : priority == "5" ? "images/task/p-five.svg" : "images/task/p-six.svg";
    $(".changePriority").attr('priority', priority);
    $('.changePriority').attr('src', Priority_image);

    let reminderType = result.reminder == "" ? "select" : result.reminder


    if (reminderType != "select") {
        $("#remindericon").attr('src', '/images/task/clock-Invt.svg');
        $("#remindericon").attr('reminder', reminderType);
    }

    if (typeof (result.task_group_id) != 'undefined') {
        $("#changeGroup").attr('group_id', result.task_group_id);
        $("#changeGroup").css('background', result.group_code);
        $("#changeGroup").attr('title', result.group_name);
    }

    if (result.taskParticipant.length > 0) {
        $("#participantsUIdivArea").attr('src', '/images/task/assignee_cir2.svg')
    }

    for (let j = 0; j < result.taskParticipant.length; j++) {
        let imgName = lighttpdpath + "/userimages/" + result.taskParticipant[j].user_id + "." + result.taskParticipant[j].user_image_type + "?" + d.getTime();
        addUserToData(prjid, result.taskParticipant[j].user_full_name, result.taskParticipant[j].user_id, '', imgName, userStatus, result.task_id, calculateElapseTime(result.taskParticipant[j].elapsed_time));

        let image = result.taskParticipant[j].user_task_status;
        if (image == "Paused") {
            image = "images/task/user-pause.svg"
        } else if (image == 'Inprogress') {
            image = "images/task/user-inprogress.svg"
        } else if (image == 'Completed') {
            image = "images/task/user-completed.svg"
        } else {
            image = "images/task/user-start.svg"
        }
        $("#persontaskstatusdisplay_" + result.taskParticipant[j].user_id).attr('src', image);
        $("#persontaskstatusdisplay_" + result.taskParticipant[j].user_id).attr("statusOfTaskUser", result.taskParticipant[j].user_task_status);
        if (result.taskParticipant[j].user_id == userIdglb) {
            $("#persontaskstatusdisplay_" + result.taskParticipant[j].user_id).attr('onclick', 'changeStatusOfUserTask(' + result.task_id + ',' + prjid + ',\'editView\',\'' + result.task_type + '\');');
            $("#taskSlideAmount_" + userIdglb).attr('permanentvalue', result.taskParticipant[j].task_user_percentage);
            if (result.taskParticipant[j].user_task_status != "Inprogress") {


                let mouseup = 'changeStatusOfTaskToProgress(' + result.task_id + ', ' + prjid + ',\'Inprogress\', \'editslide\', ' + result.task_type + ')'
                $("#slide_" + userIdglb).attr('onchange', mouseup)

            }


        }
       
        if(from!="first"){
            $("#user_id_" + result.taskParticipant[j].user_id).attr('userstatus', 'E');
        }
        $("#user_id_" + result.taskParticipant[j].user_id).find("#taskSlideAmount_" + result.taskParticipant[j].user_id).val(result.taskParticipant[j].task_user_percentage);
        $("#user_id_" + result.taskParticipant[j].user_id).find("#taskSlideAmount_" + result.taskParticipant[j].user_id).trigger('onblur');

        $("#user_id_" + result.taskParticipant[j].user_id).find(".estimateHour").val(result.taskParticipant[j].task_est_hours);

        $("#user_id_" + result.taskParticipant[j].user_id).find(".estimateMin").val(result.taskParticipant[j].task_est_mins);

        if (parseInt(result.taskParticipant[j].task_est_hours) > 0 || parseInt(result.taskParticipant[j].task_est_mins) > 0) {
            $("#user_id_" + result.taskParticipant[j].user_id).attr('modified', 'modified');
        }


        let actualTime = (parseInt(result.taskParticipant[j].working_hours) * 60) + parseInt(result.taskParticipant[j].working_minutes);

        let actualtimehour = Math.floor(actualTime / 60);
        let actualtimeMin = Math.floor(actualTime % 60);
        // $("#user_id_"+result[0].taskParticipant[i].user_id).find(".actualhour").text(actualTime);
        $("#user_id_" + result.taskParticipant[j].user_id).find(".acthour").val(actualtimehour);
        $("#user_id_" + result.taskParticipant[j].user_id).find(".actmin").val(actualtimeMin);


    }

    // globalUIArray[count]=$(".taskappendDiv").html();
    //  console.log($(".taskappendDiv").html())
    let UI1 = $(".taskappendDiv").find(".workflowUI").clone()
    for (i = 0; i < globalSubUIArray.length; i++) {
        $(".taskappendDiv").html(globalSubUIArray[i]);
        console.log("---------------------------------in pepare");
        console.log("finding id" + $(".taskappendDiv").find(".workflowUI").attr('idoftask') + "----flag---->" + flag);
        if ($(".taskappendDiv").find(".workflowUI").attr('idoftask') == id) {

            globalSubUIArray.splice(i, 1);
            break;
        }

    }
    globalSubUIArray.push(UI1);
    $(".workflowUI").remove();
    $(".task_user_new_div").remove();
    $(".task_user_new_div1").remove();


}
function validateSubStep() {
    for (let i = 0; i < globalSubUIArray.length; i++) {
        $(".taskappendDiv").html(globalSubUIArray[i]);
        let id = $(".taskappendDiv").find('.workflowUI').attr('idoftask')
        if ($(".taskappendDiv").find('.workflowUI').attr('active') == 'grayedOut') {

            $("#active_block_" + id).attr('src', '/images/task/inactivestep.svg');
        }
        if (parseInt($(".taskappendDiv").find('.workflowUI').attr('documentcount')) > 0) {
            $("#docstep_" + id).show();
        }
        if ($(".taskappendDiv").find('.workflowUI').attr('documentmandatory') == 'Y') {
            $("#documentMand_" + id).attr('src', '/images/idea/document_mand.svg');
            $("#documentMand_" + id).show();
        } else {
            $("#documentMand_" + id).hide();
        }
        if (typeof ($(".taskappendDiv").find('.workflowUI').attr('depid')) != 'undefined' && $(".taskappendDiv").find('.workflowUI').attr('depid') != "0") {
            $("#depWF_" + id).show();
        }
        if (typeof ($(".taskappendDiv").find('.workflowUI').attr('colorcode')) != 'undefined' && $(".taskappendDiv").find('.workflowUI').attr('colorcode') != "") {
            $("#block_" + id).css('background', $(".taskappendDiv").find('.workflowUI').attr('colorcode'));
            $("#block_" + id).css('border-color', $(".taskappendDiv").find('.workflowUI').attr('colorcode'));
        }
        if (typeof ($(".taskappendDiv").find('.workflowUI').attr('group_color')) != 'undefined' && $(".taskappendDiv").find('.workflowUI').attr('group_color') != "") {
            $("#block_" + id).css('background', $(".taskappendDiv").find('.workflowUI').attr('group_color'));
            $("#block_" + id).css('border-color', $(".taskappendDiv").find('.workflowUI').attr('group_color'));
        }
        else {
            let bd="2px solid "+ $("#changeGroupwf").css('background-color');
            $("#block_" + id).css('background', $("#changeGroupwf").css('background'));
            $("#block_" + id).css('border', bd);
        }
        if ($(".taskappendDiv").find('.workflowUI').find("#taskname").val() != "") {
            $("#stepdesc_" + id).text($(".taskappendDiv").find('.workflowUI').find("#taskname").val());
        }

    }
    $(".taskappendDiv").html('');
}
function addNewDependency(id){
    prepareReorderUI(id);
   // showTaskUI(id)
    if(typeof(sublevelArray.get(id.toString))!='undefined' && sublevelArray.get(id.toString)!=null && sublevelArray.get(id.toString)!=''){
        return false;
    }
    $(".workflowUI").attr('depid','1234');
    //$(".workflowUI").attr('workflow_name','1234');
    let insertData={
        id:id+"_1",
        description:"Step 1",
        children:[]
    }
    let child = datawF[0].children;
   
    if(datawF[0].id==id){
        if( datawF[0].children.length==0){
            datawF[0].children.push({children:[]})
        }
        datawF[0].children.push(insertData)
    }else{
        while (typeof (child) != 'undefined' && child[0].id != id) {
            child = child[0].children;
        }
        if( child[0].children.length==0){
            child[0].children.push({children:[]})
        }
        child[0].children.push(insertData);
    }
    sublevelArray.set(id,[{
        id:id+"_1",
        description:"Step 1",
        children:[]
    }]);
    console.log(sublevelArray.get(id))
    $('#my-container').hortree({
        data: datawF
    });
    showTaskUI(id+"_1");
    removeIcons();
    $("#block_undefined").remove();
    $(".hortree-label-active").removeClass('hortree-label-active');
    $("#block_"+id+"_1").addClass('hortree-label-active');
    validateUI(id+"_1");
    addBox( id );

}
function addBox(id){
   
    //$("#block_"+id+"_1").parent().prepend(UI);
    let UI="";

    UI += '<div style=" height: 23px; position: absolute; width: 100px; /* background: red; */  top: 103px;  left: 0px;" >'
    +'<div class="d-flex">'

            +'<img src="images/task/workflow.svg" class="task-img  img-responsive">'

            +'<input type="text"class="wfInput"   id="workflow_name_'+id+'" style="border:0;outline:none;margin-left:6px;background:transparent" placeholder="Enter Workflow name" onblur="setWorkflowname('+id+',this)"/>'


    +'</div>'

    let json=sublevelArray.get(id.toString());
   
    $(UI).insertBefore($("#block_"+json[0].id).parent())
   // alert($(".workflowUI").attr('workflow_name'))
   $(".taskappendDiv").html(globalUIArray[id]);
 
    // if( $(".taskappendDiv").find(".workflowUI").attr('workflow_name')==''){

    // }else{
    //     $("#workflow_name_"+id).val( $(".taskappendDiv").find(".workflowUI").attr('workflow_name'));
    // }

    if( $(".taskappendDiv").find(".workflowUI").attr('workflow_name')=='' || typeof($(".taskappendDiv").find(".workflowUI").attr('workflow_name'))=='undefined'){
        
        $("#workflow_name_"+id).val( $("#workflowname").val()+"-"+id);
    }else{
        $("#workflow_name_"+id).val( $(".taskappendDiv").find(".workflowUI").attr('workflow_name'));
    }
    $(".taskappendDiv").html('');
    
}
function setWorkflowname(id,obj){
    //showTaskUI(id)
    $(".taskappendDiv").html(globalUIArray[id]);
    $(".taskappendDiv").find(".workflowUI").attr('workflow_name',$("#workflow_name_"+id).val())
    //$(".workflowUI").attr('workflow_name',$("#workflow_name_"+id).val());
    globalUIArray[id]=$(".taskappendDiv").find(".workflowUI").clone();
    
}
function changeStepname(){
    $("#input_"+$(".workflowUI").attr('idoftask')).val(  $("#input_"+$(".workflowUI").attr('idoftask')).attr('placeholder'));
    $("#input_"+$(".workflowUI").attr('idoftask')).trigger('onblur');
}
function subStepReorder(id,place){

    let idArray=[];
    let descriptionArray=[];

    let json=sublevelArray.get(id.toString().split("_")[0]);
    while(typeof(json)!='undefined' && json.length>=1){
        idArray.push(json[0].id);
        descriptionArray.push(json[0].description);
        json=json[0].children;
    }
    if(place=='backward'){
       // console.log("-->BeforeIdarray--->"+idArray);
       // console.log("-->BeforedescriptionArray--->"+descriptionArray);

        let value = descriptionArray[idArray.indexOf(id)]
        //console.log(value);
       // console.log("before spliceing   "+descriptionArray)
        descriptionArray.splice(idArray.indexOf(id), 1);
        //console.log("after spliceing   "+descriptionArray)
        descriptionArray.splice(parseInt(idArray.indexOf(id))-1 , 0, value);


        value = idArray[idArray.indexOf(id) - 1]
       
        idArray.splice(idArray.indexOf(id) - 1, 1);
      
        idArray.splice(parseInt(idArray.indexOf(id)) + 1, 0, value);

       

        //console.log("-->Idarray--->"+idArray);
        //console.log("-->descriptionArray--->"+descriptionArray);
    }else{
        
       // console.log("-->BeforeIdarrayforward--->"+idArray);
       // console.log("-->BeforedescriptionArrayforward--->"+descriptionArray);
        
        let nextindex = idArray.indexOf(id) + 1
        let value = descriptionArray[idArray.indexOf(id)];
        //console.log(value);
        descriptionArray.splice(idArray.indexOf(id), 1);
        descriptionArray.splice(nextindex, 0, value);

       
        nextindex = idArray.indexOf(id) + 1
        idArray.splice(idArray.indexOf(id), 1);
        idArray.splice(nextindex, 0, id);

        


       // console.log("-->Idarrayforward--->"+idArray);
       // console.log("-->descriptionArrayforward--->"+descriptionArray);
    }
    let childernJSON=[];
    for (let i = 0; i < idArray.length; i++) {
       
        if (childernJSON.length == 0) {
            childernJSON.push({
                id:idArray[i],
                description: descriptionArray[i],
                children: [],
                pointer_event: "N"
            });
          
        } else {

            if (childernJSON[0].children.length == 0) {
                childernJSON[0].children.push({
                    id:idArray[i],
                    description: descriptionArray[i],
                    children: [],
                    pointer_event: "N"
                })
                
            } else {
                let child = childernJSON[0].children;
                while (typeof (child) != 'undefined' && child.length >= 1) {
                    child = child[0].children;
                }

                child.push({
                    id:idArray[i],
                    description: descriptionArray[i],
                    children: [],
                    pointer_event: "N"
                })
               
            }


        }
    }
    sublevelArray.set(id.toString().split("_")[0],childernJSON);
    if(datawF[0].id==id.toString().split("_")[0]){
        datawF[0].children[1]=childernJSON[0];
    }else{
        let child=datawF[0].children;
        while(typeof(child)!='undefined' && child[0].id!=id.toString().split("_")[0]){
            child=child[0].children;
        }

        child[0].children[1]=childernJSON[0];
    }
    $('#my-container').hortree({
        data: datawF
    });
    $("#block_"+id).addClass('hortree-label-active');
    $("#block_undefined").remove();
    showTaskUI(id);
    removeIcons();
    addBox(id.toString().split('_')[0]);
   
}