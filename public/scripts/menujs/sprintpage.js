var localOffsetTime="";
var menuutype="";
var glbepicid="";
var calSprintDateSelectedJSON="[]";
var sprintDelStatus="";
var confirmAction="";
var currentaction="";
var movedirection="";
var sprintcheckflag=false;
var flagdate1="";
var flagdate2=""; 
var sprintviewtype="";
var sprintListJsonGlb="";

(function ($) {
    menuutype="sprint";
    localOffsetTime=getTimeOffset(new Date());
    
    // listSprint("grid");
    listCalendarSprint();
    
    $("body").click(function(event ){
        var $target = $(event.target);
        if(!$target.is(".SBstorypopupDivcls") && !$target.parents().is(".SBstorypopupDivcls")){
            $(".SBstorypopupDivcls").remove();
            $('div.SBstory').removeClass('SBpopupDivclk');
        }
        
        if(!$target.is(".editStageCls") && !$target.parents().is(".editStageCls") && !$target.is(".editStagenameOptionCls") && !$target.parents().is(".editStagenameOptionCls")){
            $('.contentstageCls').addClass('d-flex').removeClass('d-none');
            $('.editStageCls').addClass('d-none').removeClass('d-block');
            
        }
        if(!$target.is(".SBstagereorderCls") && !$target.parents().is(".SBstagereorderCls") && !$target.is(".sprintfloatoptions") && !$target.parents().is(".sprintfloatoptions")){
            $(".SBstagereorderCls").addClass('d-none').removeClass('d-block');
        }
        if($target.is("#sprintContentListDiv") || $target.parents().is("#sprintContentListDiv")){
            if(sprintcheckflag==true){
                var plac = $('#sprintPlaceHidden').val();
                var clkfunc = plac=="create"?"insertCreateSprint":"updatesprint";
                confirmResetNew(getValues(companyAlerts,"Alert_Save"),'clear',clkfunc,"cancelsprintcreatedone",'OK','CANCEL');
            }
        }
        if(!$target.is(".gridstoryshowoption ") && !$target.parents().is(".gridstoryshowoption ") && !$target.is(".gridstoryOpt ") && !$target.parents().is(".gridstoryOpt ")){
            $(".gridstoryshowoption").hide();
        }
        
    });    
    
  
})(jQuery)

var cansort=""; var confirmStoryId=""; var confirmStageId=""; var confirmType="";var stageidglb="";
var DragNdrop=false;
function initdraganddrop(){
    var Startids="";
    
    $(".SBcontentCls").sortable({
        connectWith: '.SBcontentCls',
        cancel:".backlogstage, .SBstoryHistory",
        handle: '.SBdragdiv',
        opacity: 0.5,
        placeholder:'.SBcontentCls',
        start: function( event, ui ) {
            //var status=$(this).attr('status');
            var cls=$(this).attr('class');
            Startids=$(this).parent().attr('id');
            stageidglb=$(this).attr('id').split("_")[1];
            console.log(cls+"<---->"+Startids+"<--sssss-->"+stageidglb);
        },
        receive: function(ev, ui) {
               cansort = ui;
               DragNdrop = true;
               var cls1 = ui.item.parent().attr('class');
               var Stopids = ui.item.parents().attr('id');
               var nextstageid = ui.item.parents().attr('stageid');
               var epicid = ui.item.attr('id').split("_")[1];
               $('div.SBstory').removeClass('SBpopupDivclk');//---added for highlight
               ui.item.addClass('SBpopupDivclk');//---added for highlight
      
               
                confirmStageId = ui.item.parent().attr('id').split('_')[1];
                confirmType = ui.item.attr('type');
                 
                if($('.backlogstage1').hasClass(cls1)){ 
                    confirmAction="Backlog";
                    stagename="Backlog"; 
                    alertFun(getValues(companyAlerts,"Alert_StageBacklogMsg"),'warning');
                    revertDrag();
                }else if($('.completestage1').hasClass(cls1)){ 
                    confirmAction  = "Completed";
                    stagename="Completed"; 
                    confirmSBStoryMove(confirmStageId,epicid,stagename,stageidglb,"");
                }else{
                    confirmAction="Other";
                    stagename="Other";  
                    confirmSBStoryMove(confirmStageId,epicid,stagename,stageidglb,"");
                }
                
               console.log(stageidglb+"<---->"+cansort+"<---->"+cls1+"<---->"+Stopids+"<---->"+confirmStoryId+"<---->"+Startids+"<---->"+confirmStageId+"<---->"+confirmType);
               
        }




    });  
  } 

function listSprint(viewtype){
    if(sprintviewtype!=viewtype){
        sprintviewtype=viewtype;
        let jsonbody={
            "groupLoadType":"active",
            "project_id":prjid
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/fetchJsonSprint",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                sprintListJsonGlb=result;
                $('#sprintContentGridListDiv,#sprintContentListDiv,#SprintScrumboardDiv,#SBsettingsDiv').html('');
                $('.calendar-1').html('').removeClass('elem-CalenStyle').hide();
                $("#agileSview").hide();
                closeSprintSB('');
                if(sprintviewtype=="grid"){
                    $('#sprintContentListDiv, #sprintHeaderDiv').hide();
                    $('#sprintContentGridListDiv').addClass('d-flex').removeClass('d-none');
                    $('#sprintHeaderDiv').addClass('d-none').removeClass('d-flex');
                    $('#sprintContentGridListDiv').append(listGridSprintUI(result));
                    $('#agielGview').attr('src','/images/task/list_view.svg').attr('onclick','listSprint("list")');
                    
                }else if(sprintviewtype=="list"){
                    $('#sprintContentListDiv').append(listSprintUI(result));
                    $('#sprintContentListDiv, #sprintHeaderDiv').show();
                    $('#sprintContentGridListDiv').addClass('d-none').removeClass('d-flex');
                    $('#agielGview').attr('src','/images/task/grid_view.svg').attr('onclick','listSprint("grid")');
                    $('#sprintHeaderDiv').addClass('d-flex').removeClass('d-none');
                    
                }else{
                    $('.calendar-1').html('');
                    listCalendarSprint();
                }
                if($('.calendar-1').children().length == 0){
                    $('#SprintDivContentMain').show();
                }    
                
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            
            }
        });
    }else{
        if($("#SprintHolderDiv").length!=0){
            $('#SprintDivContentMain').toggle();
        }
    }
    hideSprintChart();
}

function listGridSprintUI(result){
    var ui="";
    var sprintid="";
    var bgcolor="";var sdate=""; var edate="";var status="";var activeinactivestat="";var stattitle="";var statusimg="";
    var hideunhide="";var hideunhidetitle="";var hideunhideval="";var hideunhideimg="";
    var editclick="";
    var editopac="";var createdbyimg="";var scount="";var simage="";
    var sprintsdate="";var sprintenddate="";
    if(result!=""){
        for(var i=0;i<result.length;i++){
            bgcolor=result[i].color_code;
            bgcolor=bgcolor==""?"#f2788f":bgcolor;
            //sdate=result[i].start_date;edate=result[i].end_date;
            sdate=result[i].start_date.split('-');edate=result[i].end_date.split('-');
            sprintsdate=sdate[1]+"-"+sdate[2]+"-"+sdate[0];sprintenddate=edate[1]+"-"+edate[2]+"-"+edate[0];
            sprintid=result[i].sprint_id;
            status=result[i].status;
            statusimg=status=="Y"?"/images/agile/sprint_active.svg":"/images/agile/sprint_inactive.svg";
            hideunhide=result[i].sprint_hidden_status;
            hideunhidetitle=hideunhide=="D"?"Unhide":"Hide";
            hideunhideval=hideunhide=="D"?"Y":"D";
            hideunhideimg=hideunhide=="Y"?"/images/workspace/eye1.svg":"/images/workspace/eye2.svg";
            activeinactivestat=status=="Y"?"A":"Y";
            editclick=status=="Y"?"editSprint("+sprintid+",'edit','grid');":"";
            editopac=status=="A"?"opacity:0.3;":"";
            stattitle=status=="Y"?"Active":"Inactive";
            createdbyimg = lighttpdpath+"/userimages/"+result[i].created_by+"."+result[i].user_image_type;
            scount = result[i].storyCount;
            simage = scount!=0?"images/agile/storyiconfilled.svg":"images/agile/story.svg";
                  
            ui+="<div id='sprintGrid_"+sprintid+"' class='gridBox' style='background-color:"+bgcolor+";'>"
                +"<div id='' class='mt-2 px-2 d-flex justify-content-between w-100'>"
                    +"<div class='gridstoryOpt position-relative' style=''><img src='"+simage+"' id='' title='Story' onclick=\"showSToptions("+sprintid+");event.stopPropagation();\" style='width:18px;height:18px;'>"
                        +"<div id='gridstoryshowoption_"+sprintid+"' class='gridstoryshowoption position-absolute rounded p-1 border' style='display:none;cursor:pointer;'>"
                            +"<div id='newCreateStory' class='d-flex p-1 justify-content-start' style='height:24px;' onclick=\"createSprintStory('sprintgrid',"+sprintid+");event.stopPropagation();\">"//
                                +"<span class='pl-2'>Create New</span>"
                            +"</div>"
                            +"<div id='existingStory' class='d-flex p-1 justify-content-start' style='height:24px;' onclick=\"showStorieslist('edit',"+sprintid+",'editgrid');event.stopPropagation();\">"//showStorieslist(\"sprintgrid\","+sprintid+");
                                +"<span class='pl-2'>Existing</span>"
                            +"</div>"
                        +"</div>"
                    +"</div>"
                    +"<div class='' style=''><img src='/images/task/task_comments.svg' id='' title='Comments' onclick=\"sprintComments("+sprintid+",'"+result[i].sprint_title+"');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style=''><img src='/images/agile/scrumboard.svg' id='' title='Scrumboard' onclick=\"showSprintScrumboard("+sprintid+");event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style=''><img src='"+statusimg+"' id='gridaInactStatus_"+sprintid+"' title='"+stattitle+"' onclick=\"deleteSprintalert("+sprintid+",'sprint_active','"+activeinactivestat+"');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style=''><img src='"+hideunhideimg+"' id='' title='"+hideunhidetitle+"' onclick=\"deleteSprintalert("+sprintid+",'sprint_hide','"+hideunhideval+"');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style=''><img src='/images/agile/chart.svg' id='' title='Chart' onclick=\"showSprintChart("+sprintid+",'chart1');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style=''><img src='/images/agile/delete.svg' title='Delete' onclick=\"deleteSprintalert("+sprintid+",'sprint_delete','N');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                +"</div>"
                +"<div id='gridsprintlistcontent_"+sprintid+"' ondblclick=\""+editclick+"event.stopPropagation();\" class='mt-2 px-2 w-100 defaultExceedMultilineCls'><span class='sprinttitle' title='"+result[i].sprint_title+"'>"+result[i].sprint_title+"</span></div>"
                +"<div id='sprintCreateStoryGridDiv_"+sprintid+"'></div>"

                +"<input id='gridsprintname_"+sprintid+"' type='hidden' value='"+result[i].sprint_title+"'>"
                +"<input id='gridinstruction_"+sprintid+"' type='hidden' value='"+result[i].sprint_desc+"'>"
                +"<input id='gridsprintgroupid_"+sprintid+"' type='hidden' value='"+result[i].sprint_group_id+"'>"
                +"<input id='gridsprintstartdate_"+sprintid+"' type='hidden' value='"+sprintsdate+"'>"
                +"<input id='gridsprintenddate_"+sprintid+"' type='hidden' value='"+sprintenddate+"'>"

            +"</div>"
            
    
        }
    }else{
        ui="<div style='text-align:center;font-size:12px;'>No Stories Found</div>"
    }
    

    return ui;

}

function showSToptions(sprintid){
    $('.gridstoryshowoption').hide();
    $('#gridstoryshowoption_'+sprintid).show();

}

function listSprintUI(result){
    var ui="";
    var sprintid="";
    var bgcolor="";var sdate=""; var edate="";var status="";var activeinactivestat="";var stattitle="";var statusimg="";
    var hideunhide="";var hideunhidetitle="";var hideunhideval="";var hideunhideimg="";
    var editclick="";
    var editopac="";var createdbyimg="";
    if(result!=""){
        for(var i=0;i<result.length;i++){
            bgcolor=result[i].color_code;
            bgcolor=bgcolor==""?"#f2788f":bgcolor;
            sdate=result[i].sprint_start_date.replaceAll('-',' ');edate=result[i].sprint_end_date.replaceAll('-',' ');
            sprintid=result[i].sprint_id;
            status=result[i].status;
            statusimg=status=="Y"?"/images/agile/sprint_active.svg":"/images/agile/sprint_inactive.svg";
            hideunhide=result[i].sprint_hidden_status;
            hideunhidetitle=hideunhide=="D"?"Unhide":"Hide";
            hideunhideval=hideunhide=="D"?"Y":"D";
            hideunhideimg=hideunhide=="Y"?"/images/workspace/eye1.svg":"/images/workspace/eye2.svg";
            activeinactivestat=status=="Y"?"A":"Y";
            editclick=status=="Y"?"editSprint("+sprintid+",'edit');":"";
            editopac=status=="A"?"opacity:0.3;":"";
            stattitle=status=="Y"?"Active":"Inactive";
            createdbyimg = lighttpdpath+"/userimages/"+result[i].created_by+"."+result[i].user_image_type;
                  
            ui+="<div id='sprint_"+sprintid+"' class='sprintLists sprintLists1 position-relative' onmouseover='showSprintmoreoptions(this);event.stopPropagation();' onmouseout='hideSprintmoreoptions(this);event.stopPropagation();' style=''>"
                +"<div id='sprintlistcontent_"+sprintid+"' class='d-flex py-1 px-1 align-items-center position-relative' ondblclick=\""+editclick+"event.stopPropagation();\">"
                    +"<div class='defaultExceedCls pr-3' id='' style='width: 12%;text-align: center;'><span title='"+result[i].sprint_group_name+"' class='rounded-circle position-absolute sprintGroupdiv' style='background-color:"+bgcolor+";'></span></div>"
                    +"<div class='defaultExceedCls' id='' style='width: 50%;'><span class='sprinttitle' title='"+result[i].sprint_title+"'>"+result[i].sprint_title+"</span></div>"
                    +"<div class='defaultExceedCls' id='' style='width: 15%;'><span>"+sdate+"</span></div>"
                    +"<div class='defaultExceedCls' id='' style='width: 15%;'><span>"+edate+"</span></div>"
                    if(result[i].created_by == userIdglb){
                        ui+="<div class='defaultExceedCls pl-3' id='' style='width: 8%;'><img onclick='event.stopPropagation();getNewConvId("+result[i].created_by+", this);'  src='"+createdbyimg+"' title='"+result[i].name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle' style='width:25px;height:25px;'></div>"
                    }else{
                        ui+="<div class='defaultExceedCls pl-3' id='' style='width: 8%;'><img onclick='event.stopPropagation();getNewConvId("+result[i].created_by+", this);'  src='"+createdbyimg+"' title='"+result[i].name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle cursor' style='width:25px;height:25px;'></div>"
                    }
                ui+="</div>"
                +"<div id='sprintfloatoptionsID' class='sprintfloatoptions d-none align-items-center position-absolute'>"
                    //+"<div><img src='' style='width:15px;height:15px;'></div>"
                    +"<div class='' style='margin:0px 6px'><img src='/images/task/expand.svg' id='' title='Details' onclick=\"event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style='margin:0px 6px'><img src='/images/task/task_comments.svg' id='' title='Comments' onclick=\"sprintComments("+sprintid+",'"+result[i].sprint_title+"');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style='margin:0px 6px'><img src='/images/agile/scrumboard.svg' id='' title='Scrumboard' onclick=\"showSprintScrumboard("+sprintid+");event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style='margin:0px 6px'><img src='/images/conversation/edit.svg' id='editSprintimg_"+sprintid+"' title='Edit' onclick=\""+editclick+"event.stopPropagation();\" style='width:18px;height:18px;"+editopac+"'></div>"
                    +"<div class='' style='margin:0px 6px'><img src='"+statusimg+"' id='aInactStatus_"+sprintid+"' title='"+stattitle+"' onclick=\"deleteSprintalert("+sprintid+",'sprint_active','"+activeinactivestat+"');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style='margin:0px 6px'><img src='"+hideunhideimg+"' id='' title='"+hideunhidetitle+"' onclick=\"deleteSprintalert("+sprintid+",'sprint_hide','"+hideunhideval+"');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style='margin:0px 6px'><img src='/images/agile/chart.svg' id='' title='Chart' onclick=\"showSprintChart("+sprintid+",'chart1');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                    +"<div class='' style='margin:0px 6px'><img src='/images/agile/delete.svg' title='Delete' onclick=\"deleteSprintalert("+sprintid+",'sprint_delete','N');event.stopPropagation();\" style='width:18px;height:18px;'></div>"
                +"</div>"
            +"</div>"
    
    
        }
    }else{
        ui="<div style='text-align:center;font-size:12px;'>No Stories Found</div>"
    }
    

    return ui;
}

function sprintComments(sprintid,title,latestid,feedid,type){
    type = typeof(type)=="undefined"?"":type;
    let jsonbody = {
        "menuTypeId" : sprintid,
        "project_id" : prjid,
        "localOffsetTime" : localOffsetTime,		
        "feedType" : "agile",		
        "subMenuType" : "Sprint",
        "type" : type,
        "task_comment_id" : latestid
      }
      //$('#loadingBar').addClass('d-flex').removeClass('d-none');
      $.ajax({
        url: apiPath + "/" + myk + "/v1/getFeedDetails",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');
          
        },
        success: function (result) { 
          
          var UI=prepareCommentsUI(result, menuutype, prjid,'');
          feedid=parseInt(feedid);
          
          if(typeof(latestid)=="undefined" || latestid==""){
              popupCommentsUI(sprintid,title,'Sprint');
              AddSubComments(0,'',sprintid);
              $('body').find('#popupCommentsDivbody').append(UI);
              $('#replyBlock_0').focus();
          }else{
              $("#replyBlock_"+feedid).val('');
              if(feedid==0){
                  $(UI).insertAfter("#replyDivContainer_"+feedid).show();
              }else{
                  $("#replyDivContainer_"+feedid).remove();
                  $(UI).insertAfter('#actFeedDiv_' +feedid).show('slow');
              }
            
          }
          
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
      });


}

function showSprintScrumboard(sprintid,view){
    view = typeof(view)=="undefined"||view=="undefined"?"":view;
    $('#SprintScrumboardDiv').html('');
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    let jsonbody = {
        "sprint_id": sprintid,
        "project_id": prjid,
        "stage_id": "",
        "user_id": userIdglb
    }  
    $.ajax({
        url: apiPath+"/"+myk+"/v1/fetchSBdataForSprint",
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){
            if(result!=""){
                SBmenu="SBstories";
                $('#SprintDivContentMain').hide();
                $('.calendar-1').hide();
                $('#SprintHolderDiv').remove();
                $('#sprintSBback').addClass('d-flex').removeClass('d-none');
                $('#SprintScrumboardDiv').html(showSprintScrumboardUI(result,sprintid,view)).show();
                var stagedata = result[0].stageData;var stageid="";var value="";var hi="";var hival=""
                for(var i=0;i<stagedata.length;i++){
                    stageid=stagedata[i].stage_id;
                    value=$('#SBcontent_'+stageid).children().length;
                    $('#stageStoryCount_'+stageid).text(value);
                    //$("#storyCountTooltipid_"+stageid).text("Number of stories: "+value);
                    $('#contentstage_'+stageid+', #stagenameDiv_'+stageid).attr("title","Number of stories: "+value);
                }    
                
                var elmnt = document.getElementById("sprintSBtable");
                var y = elmnt.scrollHeight - 75;
                $('.SBcontentCls').css('height',y);

                setHgtforSbtable(); 
                initdraganddropStages("SBlist");
                initdraganddrop();
                if(view=="calendarview"){
                    $('.calendar-1').hide();
                    $('#sprintSBback').children('img').attr('onclick','closeSprintSB("'+view+'")');
                    
                }else if(view=="glbOption"){
                    
                }else{
                    $('.calendar-1').html('').hide();
                    $('#sprintSBback').children('img').attr('onclick','closeSprintSB("'+view+'")');
                }
                selectStSB();
            }
            
            
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });    


}

function setHgtforSbtable(){
    var h1=$('#content').height();
    var h2=$('#scrumboardMainDivUI').height();
    var h3=h1-h2-5;
    $('#sprintSBtable').css('height',h3); 
}

function selectStSB(){
    var ui="";
    for(var i=0;i<sprintListJsonGlb.length;i++){
        ui+="<div id='glbSprintList_"+sprintListJsonGlb[i].sprint_id+"' class='py-1 defaultExceedCls' title='"+sprintListJsonGlb[i].sprint_title+"' onclick=\"showSprintScrumboard("+sprintListJsonGlb[i].sprint_id+",'glbOption');\" style='cursor:pointer;'>"+sprintListJsonGlb[i].sprint_title+"</div>"
    }
    $("#sprintGlbListPopupDiv").append(ui);
}

function closeSprintSB(place){
    if(place == "SBsettings"){
        //glbSbsettingsSgroupid="";
        if($(".elem-CalenStyle").length!=0){
            $(".elem-CalenStyle").show();
        }
        $('#SBsettingsMaindiv').remove();
        $('#sprintSBback').children('img').attr('onclick','closeSprintSB()');
    }else if(place == "calendarview"){
        $('.calendar-1').show();
        SBmenu="";
        $('#SprintScrumboardDiv').html('');
    }else if(place == "chart"){
        $("#sprintDivMainbody").show();
        $("#sprintChartDiv").html("").hide().removeClass("jqplot-target");
        $("#sprintChart1, #sprintChart2").removeClass('d-block').addClass('d-none');
        $('#sprintSBback').children('img').attr('onclick','closeSprintSB()');
    }else{
        SBmenu="";
        $('#SprintScrumboardDiv').html('');
        if($(".elem-CalenStyle").length!=0){
            $('.calendar-1').show();
        }
    }
    $('#SprintDivContentMain').show();
    $('#sprintSBback').addClass('d-none').removeClass('d-flex').removeClass('d-block');
    
}


function showSprintScrumboardUI(result,sprintid,view){
    var ui="";
    var stagedata = result[0].stageData;
    var storydata = result[0].sprintData[0].storiesData;
    var stageclass="";var stagenamecls="";
    var color = result[0].color_code;
    color = typeof(color)=="undefined"||color==""?"#f2788f":color

    ui="<div id='scrumboardMainDivUI' style='border-bottom: 1px solid #aaaaaa;'>"     //1
        
        +"<div id='sprintSBDetailsDiv' class='' style='font-size:12px;'>"
            +"<div id='' class='d-flex justify-content-around' style='padding:5px 5px;'>"
                +"<div class='d-flex align-items-center w-25'>"
                    +"<div class='paddingSB'><img src='images/agile/sprint_blue.svg' style='width:20px;height:20px;'>&nbsp;&nbsp;</div>"   //"+getValues(companyLabels,"SPRINT")+"
                    //if(view=="glbOption"){
                        +"<div id='sprintGlbOptionIdDiv' class='position-relative px-1' onclick='showGlbSprintList();event.stopPropagation();'><img src='images/task/dowarrow.svg' style='width:12px;height:12px;cursor:pointer;'>"
                            +"<div id='sprintGlbListPopupDiv' class='wsScrollBar' style='display:none;'>"
                            +"</div>"
                        +"</div>"
                    //}
                    +"<div id='sprintname2' sprintid="+sprintid+" class='defaultExceedCls' style='width:75%;'>"+result[0].sprint_title+"</div>"
                +"</div>"
                +"<div class='d-flex align-items-center w-25'>"
                    +"<div id='sprintGroupSB1' sgid="+result[0].sprint_group_id+" class='paddingSB'>Group&nbsp;&nbsp;</div>"
                    +"<div  class='d-flex align-items-center defaultExceedCls' id='' style=''><span title='"+result[0].sprint_group_name+"' class='rounded-circle position-absolute' style='background-color:"+color+";box-shadow:0 6px 10px rgb(0 0 0 / 18%);width:15px;height:15px;margin-top:-1px;'></span><span class='ml-4'>"+result[0].sprint_group_name+"</span></div>"
                +"</div>"
                +"<div class='d-flex align-items-center w-25'>"
                    +"<div class='paddingSB'>"+getValues(companyLabels,"Start_date")+"&nbsp;:&nbsp;</div>"
                    +"<div class='defaultExceedCls' style=''>"+result[0].sprint_start_date+"</div>"
                +"</div>"
                +"<div class='d-flex align-items-center w-25'>"
                    +"<div class='paddingSB'>"+getValues(companyLabels,"End_date")+"&nbsp;:&nbsp;</div>"
                    +"<div class='' style=''>"+result[0].sprint_end_date+"</div>"   //border-bottom:1px solid darkgray;
                    +"<div class='defaultExceedCls ml-3'><img src='/images/task/taskInstructions_cir.svg' onclick='showSBSprintDesc(this);event.stopPropagation();' style='width:20px;height:20px;cursor:pointer;'></div>"
                    +"<div id='reassignButton' class='createBtn defaultExceedCls ml-3 d-none' onclick=\"reassigntoSprintpopup("+sprintid+");event.stopPropagation();\">Reassign</div>"
                +"</div>"
                
            +"</div>"
            +"<div id='sprintInstruction' class='d-none w-100' style='padding:5px 15px;'>"
                +"<div>"+getValues(companyLabels,"Description")+"&nbsp;:&nbsp;</div>"
                +"<div class='defaultExceedCls' title='"+result[0].sprint_desc+"' style='width:90%;'>"+result[0].sprint_desc+"</div>"
            +"</div>"

        +"</div>"

    +"</div>"       //1

    /* +"<table id='sprintSBtable'>"        
        +"<tbody class=''>"
            
            +"<tr class='d-flex px-2 headerbgcolor' style='border-bottom: 1px solid #a7a6a6;border-top: 1px solid #a7a6a6;'>"
            for(var i=0;i<stagedata.length;i++){
                ui+="<th id='stageheader_"+stagedata[i].stage_id+"' class='sprintSBheader headerbgcolor d-flex align-items-center px-0' stagetype='"+stagedata[i].stage_type+"'>"
                    +"<input class='d-none' type='checkbox' id='SBstagecheckbox_"+stagedata[i].stage_id+"' onclick='checkStories("+stagedata[i].stage_id+");event.stopPropagation();' style='cursor:pointer;'>"
                    +"<img id='SBstagecheck_"+stagedata[i].stage_id+"' onclick='checkAllstageStories("+stagedata[i].stage_id+");event.stopPropagation();' src='/images/agile/SBuncheck.svg' style='width:20px;height:20px;'>"
                    +"<span style='font-weight: 100;' class='pl-2'>"+stagedata[i].stage_name+"</span>"
                    +"<span id='stagecount_"+stagedata[i].stage_id+"' class='pl-2'></span>"
                +"</th>"
            }
            ui+="</tr>"

            +"<tr id='sprintSBname_"+result[0].sprint_id+"' class='d-none'><td class='sprintSBname px-2'>"+result[0].sprint_title+"</td></tr>"

            +"<tr id='sprintSB_"+result[0].sprint_id+"' class='d-flex SBsprintstories'>"
            for(var j=0;j<stagedata.length;j++){
                stagenamecls=stagedata[j].stage_name=="Sprint Backlog"?"stopdrag":"sprintSBcontent";
                stageclass= stagedata[j].stage_name=="Sprint Backlog"?"backlogstage":stagedata[j].stage_name=="Completed"?"completestage":"";
                ui+="<td id='stageBody_"+stagedata[j].stage_id+"' class='sprintSBcontent draghandle"+j+" "+stageclass+"' stagecount="+j+" stageid="+stagedata[j].stage_id+" stagetype='"+stagedata[j].stage_type+"'>"
                    ui+=scrumboardListUI(storydata,stagedata[j].stage_id,stageclass,sprintid)
                +"</td>"
            }
            

        +"</tbody>"
    +"</table>"    */    

    +"<div id='sprintSBtable' class='d-flex p-2 wsScrollBar' style='overflow-x: auto;overflow-y: auto;'>"
    ui+=SBsettingsStageslistloop(stagedata,"SBlistingstories",storydata)
    ui+="<div>"

    return ui;

}

function reassigntoSprintpopup(sprintid){
    var storyids=""; var stid1=""; var stid2="";
    $('#sprintSBtable > .SBsettingStageCls > .SBcontentCls > div').each(function () {
        stid1 = $(this).attr('id').split("_")[1];
        if($(this).find('#checkStoryid_'+stid1).hasClass('check')){
            stid2 = $(this).attr('sprintstoryid');
            storyids=stid2+","+storyids;
        }
    });
    ///storyids=storyids.substring(0,storyids.length-1);
    var jsonbody = {
        "project_id" : prjid,
        "sprint_id" : sprintid,
        "story_id" : storyids
    }
    console.log("storyids-->"+storyids);
    $.ajax({
        url: apiPath + "/" + myk + "/v1/fetchAssignSprints",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
             
        },
        success: function (result) {
            
            $('#sprintpopups').html('');
            $("#transparentDiv").show();
            $('#sprintpopups').html(activeSprintlistUI(result,sprintid)).show();
            $('#SBactiveSprintlistId').attr('selectedstories',storyids);
        }
        
    });     

    
}

function activeSprintlistUI(result,currentsprintid){
    var ui="";
    var color="";
    ui='<div class="SBactiveSprintlistCls" style="top:130px !important;" id="SBactiveSprintlistId" selectedstories="">'
      +'<div class="mx-auto w-75" style="">'
        +'<div class="modal-content container px-0" style="max-height: 500px !important;">'
          +'<div class="modal-header p-2 pl-3 d-flex align-items-center" style="border-bottom: 1px solid #5e5b5b;background-color: #003A5D;color: white;">'
            +'<span class="modal-title" style="width:40%;font-size:14px;">Sprint</span>'  
            +'<span class="modal-title" style="width:20%;font-size:14px;">Sprint Group</span>'
            +'<span class="modal-title" style="width:20%;font-size:14px;">Start Date</span>'
            +'<span class="modal-title" style="width:20%;font-size:14px;">End Date</span>'
          +'</div>'
          +'<div class="" style="">'
            +'<button type="button" onclick="closeactiveSprintlistUI();event.stopPropagation();" class="close p-0 AgileCloseButton" style="top: 5px;right: 10px;color: black;outline: none;color: white;">&times;</button>'
          +'</div>'
          +'<div id="activeSprintlistDivbody" class="modal-body p-0 wsScrollBar" style="font-size:12px;max-height: 400px;height: 400px;overflow: auto;">'
          if(result!=""){
            for(var i=0;i<result.length;i++){
                color = typeof(result[i].color_code)=="undefined" || result[i].color_code=="" ? "#f2788f" : result[i].color_code;  
                ui+="<div id='reassignSprintlist_"+result[i].sprint_id+"' onclick=\"reassignTosprint("+currentsprintid+","+result[i].sprint_id+");event.stopPropagation();\" class='reassignSprintlistCls media p-2 pl-3 d-flex align-items-center w-100 position-relative' style='border-bottom: 1px solid #cccccc;cursor:pointer;'>"
                    +"<div class='defaultExceedCls' title='"+result[i].sprint_title+"' style='width:40%;'>"+result[i].sprint_title+"</div>"
                    +"<div class='position-relative' style='width:20%;left:25px;'><span class='rounded-circle position-absolute' title='"+result[i].sprint_group_name+"' style='background-color:"+color+";box-shadow:0 6px 10px rgb(0 0 0 / 18%);width:15px;height:15px;margin-top:-6px;'></span></div>"
                    +"<div class='defaultExceedCls' style='width:20%;'>"+result[i].start_date+"</div>"
                    +"<div class='defaultExceedCls' style='width:20%;'>"+result[i].end_date+"</div>"
                +"</div>"
              }
          }else{
            ui+="<div class='d-flex justify-content-center mt-3'>No Data Found</div>"
          }
         
          ui+='</div>'
        +'</div>'
      +'</div>'
    +'</div>'
  

    return ui;
}

function closeactiveSprintlistUI(){
    $('#sprintpopups').html('').hide();
    $("#transparentDiv").hide();
}

function reassignTosprint(curentsprintid,newsprintid){
    confirmFunNew(getValues(companyAlerts,"Alert_assign"),"clear","reassignConfirm",curentsprintid,newsprintid);
}
async function reassignConfirm(curentsprintid,newsprintid,storyids){

    var storyids=$('#SBactiveSprintlistId').attr('selectedstories');
    
    let jsonbody = {
        "sprint_id" : newsprintid,
        "sprintId" : curentsprintid,
        "story_id" : storyids,
        "user_id" : userIdglb
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    await $.ajax({
        url: apiPath + "/" + myk + "/v1/updateAssignSprints",
        type: "PUT",
        //dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        
        },
        success: function (result) {

            if(result=="success"){
                showSprintScrumboard(curentsprintid);
                closeactiveSprintlistUI();
            }

            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });        
}

function scrumboardListUI(storydata,tableStageid,stageclass,sprintid){
    var ui="";
    var userlist="";var status="";var statimage="";var stageid="";var sprinttype="";var SBclass="";var priority="";var priorityimage="";
    var epicid="";var color="";var movecursor="";
    var name="";var userImg="";var user_id="";var statusclick="";
     
    for(var i=0;i<storydata.length;i++){
        userlist=storydata[i].loadSBStoryUsers;
        status=storydata[i].epic_status;
        statimage = status=="Backlog"?"/images/idea_old/Backlog.png":status=="Blocked"?"/images/idea_old/Blocked.png":status=="Hold"?"/images/idea_old/Hold.png":status=="Done"?"/images/idea_old/Done.png":status=="Cancel"?"/images/idea_old/cancel.png":status=="In Progress"?"/images/idea_old/Assigned.png":"";
        stageid=storydata[i].stage_id;  
        sprinttype=storydata[i].type;  
        stagename=storydata[i].stage_name;
        priority=storydata[i].epic_priority;
        priorityimage = (priority=="")? "images/task/p-six.svg" : priority=="1" ? "images/task/p-one.svg" : priority=="2" ? "images/task/p-two.svg":priority=="3" ? "images/task/p-three.svg" :priority=="4" ? "images/task/p-four.svg": "images/task/p-five.svg";
        epicid=storydata[i].epic_id;
        color=storydata[i].color_code;
        SBclass = sprinttype=="History"?"SBstoryHistory":"SBstory"; 
        color = sprinttype=="History"?"#ad9f47":typeof(color)=="undefined"||color==""?"#f6b847":color;
        movecursor = stageclass=="backlogstage" || sprinttype=="History"?"cursor:default":"cursor:move";
        statusclick = sprinttype=="History"?"":"showStatuspopupSB(this);"
        if(tableStageid == stageid){
            ui+="<div id='SBstory_"+epicid+"' sprintstoryid="+storydata[i].sprint_story_id+" sprintid="+sprintid+" class='"+SBclass+" "+stageclass+"cls rounded position-relative' style='background-color:"+color+";border: 2px solid "+color+";'>"
                if(sprinttype=="Present"){
                    ui+="<div class='position-absolute' style='top:-10px;left:-5px;'><img id='checkStoryid_"+epicid+"' class='SBcheckbox' onclick='checkitSBstory("+epicid+",this,"+tableStageid+");event.stopPropagation();' src='/images/task/inactivestep.svg' style='width:10px;height:10px;cursor:pointer;z-index:1;'></img></div>"
                }
                ui+="<div id='' class='SBDiv1 mx-2 d-flex'>"
                    +"<div class='d-none' style='width:10%'><input id='oneSbstorychekbox_"+epicid+"' class='mt-2 ' onclick='checkSBstory("+epicid+");event.stopPropagation();' type='checkbox' style='cursor:pointer;'></div>"
                    +"<div class='SBdragdiv d-flex align-items-center' style='width:90%;"+movecursor+"'>"
                        +"<div><img src='images/agile/story.svg' style='width:20px;height:20px;'></div>"
                        +"<div class='pl-2' style='color:black;'><span>ID&nbsp;:&nbsp;</span><span>"+storydata[i].unique_cust_id+"</span></div>"
                    +"</div>"
                    +"<div onclick=\"agileComments("+epicid+",'"+storydata[i].epic_title+"','','','','Story');event.stopPropagation();\" style=''><img class='mt-1' src='/images/task/task_comments.svg' style='cursor:pointer;width:20px;height:20px;'></div>"
                +"</div>"

                +"<div id='SBsprintcontentdiv' class='mx-2 pt-1' style=''>"
                    +"<div id='SBsprinttitle' style=''>"
                        +"<span class='defaultExceedMultilineCls' onclick=\"showStorydetails("+epicid+","+storydata[i].parent_epic_id+",'"+stagename+"','"+sprinttype+"',"+tableStageid+",'"+storydata[i].assigned_sprint+"');event.stopPropagation();\" title='"+storydata[i].epic_title+"' style='-webkit-line-clamp: 2 !important;cursor:pointer;font-size: 11px;color: black;'>"+storydata[i].epic_title+"</span>"
                    +"</div>"
                    if(stagename=="Sprint Backlog" && storydata[i].storySprintStatus=="Progress"){
                        ui+="<div class='createBtn mx-auto d-none' style='float:none;'>"+getValues(companyLabels,"Story_Assign")+"</div>"
                    }else{
                        ui+="<div id='assignUsersListSBsprint' class='d-none' style='height:30px;overflow:hidden;'>"
                        for(var j=0;j<userlist.length;j++){
                            name = userlist[j].name;
                            userImg = userlist[j].userImg;
                            user_id = userlist[j].user_id;

                            ui+="<div id='user_"+user_id+"' class='ml-2'>"
                                +"<img src='"+userImg+"' title='"+name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle' style='width: 30px;height: 30px;border-radius: 50%;cursor: pointer;'>"
                            +"</div>"
                        }
                        ui+="</div>"
                    }
                    
                ui+="</div>"

                +"<div class='d-flex align-items-center mx-2 pt-2'>"
                    +"<div class='mr-auto' style='width:15%;'><img id='SBsprintPriority_"+storydata[i].epic_id+"' title='"+priority+"' src='"+priorityimage+"' style='height:18px;width:18px;cursor:pointer;'></div>"
                    //+"<div class='defaultExceedCls' title='"+storydata[i].unique_cust_id+"' style='width:50%;text-align:center;color:black;'>"+storydata[i].unique_cust_id+"</div>"
                    if(sprinttype=="History"){
                        ui+="<div class='d-flex align-items-center' style='width:20%;'>"
                            +"<img id='SBreassignedimg_"+storydata[i].epic_id+"' title='Assigned to "+storydata[i].assigned_sprint+"' src='/images/idea_old/re-assigned.png' style='height:20px;width:20px;cursor:pointer;'>"
                        +"</div>"
                    }else{
                        ui+="<div class='d-flex align-items-center mx-1' style='width:20%;'>"
                        if(stagename=="Completed"){
                            ui+="<img id='SBsprintMoveleft_"+storydata[i].epic_id+"' class='sb_prevStage' title='"+getValues(companyLabels,"Previous_Stage")+"' onclick=\"moveSBstory(this,'left',"+epicid+");event.stopPropagation();\" src='/images/task/arrow_left.svg' style='height:18px;width:18px;cursor:pointer;'>"
                        }else if(stagename!="Sprint Backlog"){
                            ui+="<img id='SBsprintMoveleft_"+storydata[i].epic_id+"' class='sb_prevStage' title='"+getValues(companyLabels,"Previous_Stage")+"' onclick=\"moveSBstory(this,'left',"+epicid+");event.stopPropagation();\" src='/images/task/arrow_left.svg ' style='height:18px;width:18px;cursor:pointer;'>"
                            +"<img id='SBsprintMoveright_"+storydata[i].epic_id+"' class='sb_nextStage' title='"+getValues(companyLabels,"Next_Stage")+"' onclick=\"moveSBstory(this,'right',"+epicid+");event.stopPropagation();\" src='/images/task/arrow_right.svg' style='height:18px;width:18px;cursor:pointer;'>"    
                        }
                        ui+="</div>"
                    }
                    ui+="<div class='position-relative' onclick='"+statusclick+"event.stopPropagation();' style=''><img id='SBsprintStatus_"+storydata[i].epic_id+"' src='"+statimage+"' title='"+status+"' style='height:17px;width:17px;cursor:pointer;'>"
                        ui+=statusUpdate(storydata[i].epic_id,'SBlist')
                    ui+="</div>"
                +"</div>"

            +"</div>"
        }
    }

    return ui;
}

function showStatuspopupSB(obj){
    if($(obj).find('.agileMoreOption3').is(':hidden') == true){
        $(obj).find('.agileMoreOption3').addClass('d-block').removeClass('d-none');
    }else{
        $(obj).find('.agileMoreOption3').addClass('d-none').removeClass('d-block');
    }
}

function checkitSBstory(epicid,obj,stageid){
    
    if($(obj).attr('src').indexOf('/images/task/inactivestep.svg') == -1){
        $(obj).attr('src','/images/task/inactivestep.svg').removeClass('check');
        $('#SBstorypopupcheck_'+epicid).attr('src','/images/task/inactivestep.svg');
    }else{
        $(obj).attr('src','/images/task/activestep.svg').addClass('check');
        $('#SBstorypopupcheck_'+epicid).attr('src','/images/task/activestep.svg');
    }
    showReassign(stageid);
}

function checkAllstageStories(stageid){
    if($('#SBcontent_'+stageid).find('.check').length == 0){
        $('#SBcontent_'+stageid).find('.SBcheckbox').attr('src','/images/task/activestep.svg').addClass('check'); 
    }else{
        $('#SBcontent_'+stageid).find('.SBcheckbox').attr('src','/images/task/inactivestep.svg').removeClass('check');
    }
    if($('#SBstagecheck_'+stageid).attr('src').indexOf('/images/task/activestep.svg') == -1){
        $('#SBstagecheck_'+stageid).attr('src','/images/task/activestep.svg');
    }else{
        $('#SBstagecheck_'+stageid).attr('src','/images/task/inactivestep.svg');
    }
    showReassign(stageid);
}

function showReassign(stageid){
    if($('#sprintSBtable').find('.check').length == 0){
        $('#reassignButton').addClass('d-none').removeClass('d-block');
    }else{
        $('#reassignButton').addClass('d-block').removeClass('d-none');
    }
}

function showStorydetails(epicid,parentid,stagename,stagetype,stageid,assignedsprint){

    $('div.SBstory').removeClass('SBpopupDivclk');
    $('#SBstory_'+epicid).addClass('SBpopupDivclk');

    if($('#SBstorypopupDiv_'+epicid).length == 0){
        var sprintid=$('#sprintname2').attr('sprintid');
        $('#loadingBar').addClass('d-flex').removeClass('d-none');
        $.ajax({
            url: apiPath+"/"+myk+"/v1/loadSBStoryMoreUsers?sprintId="+sprintid+"&storyId="+epicid+"&type="+stagetype+"&action=",
            type:"GET",
            error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
            },
            success:function(result){
                $('.SBstorypopupDivcls').remove();
                $('#SBstory_'+epicid).append(showStorydetailsPopup(result,stagename,stagetype,stageid,sprintid,assignedsprint));

                $('#SBstorypopupcheck_'+epicid).attr('src',$('#checkStoryid_'+epicid).attr('src'));

                var priority = result[0].epic_priority;
                var priorityimage = (priority=="")? "images/task/p-six.svg" : priority=="1" ? "images/task/p-one.svg" : priority=="2" ? "images/task/p-two.svg":priority=="3" ? "images/task/p-three.svg" :priority=="4" ? "images/task/p-four.svg": "images/task/p-five.svg";
                var prioritytitle = (priority=="")? "None" : priority=="1" ? "Very Important" : priority=="2" ? "Important":priority=="3" ? "Medium" :priority=="4" ? "Low": "Very Low";

                if(typeof(assignedsprint)!="undefined" && assignedsprint!=""){
                    $('#assigned_'+epicid).addClass('d-flex').removeClass('d-none');
                }else{
                    $('#assigned_'+epicid).remove();
                }

                if(typeof(result[0].commentcount)!="undefined" && result[0].commentcount!="0" && result[0].commentcount!=""){
                    $('#SBiconcomments_'+epicid).addClass('d-flex').removeClass('d-none');
                    $('#SBiconcomments_'+epicid).children('img').attr('onclick','agileComments('+epicid+',"'+result[0].epic_title+'","","","","Story");event.stopPropagation();');
                }else{
                    $('#SBiconcomments_'+epicid).remove();
                }

                if(typeof(result[0].epic_point)!="undefined" && result[0].epic_point!="0" && result[0].epic_point!=""){
                    $('#SBiconpoints_'+epicid).addClass('d-flex').removeClass('d-none').attr('src','');
                    $('#SBpointsspan_'+epicid).text(result[0].epic_point);
                }else{
                    $('#SBiconpoints_'+epicid).remove();
                }

                if(typeof(result[0].epic_priority)!="undefined" && result[0].epic_priority!="0" && result[0].epic_priority!=""){
                    $('#SBiconpriority_'+epicid).addClass('d-flex').removeClass('d-none');
                    $('#SBiconpriority_'+epicid).children('img').attr('src',priorityimage).attr('title',prioritytitle);
                }else{
                    $('#SBiconpriority_'+epicid).remove();
                }

                if(typeof(result[0].taskcount)!="undefined" && result[0].taskcount!="0" && result[0].taskcount!=""){
                    $('#SBicontask_'+epicid).addClass('d-flex').removeClass('d-none').attr('src','');
                }else{
                    $('#SBicontask_'+epicid).remove();
                }

                if($('#SBiconpoints_'+epicid).length == 0){
                    $('#custIddiv').addClass('mr-auto');
                }
    
                var elmnt = document.getElementById("SBstorypopupDiv_"+epicid);
                elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
    
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
            }
        });   
    }else{
        $('#SBstorypopupDiv_'+epicid).remove();
    }
         
}


function showStorydetailsPopup(result,stageplace,stagetype,stageid,sprintid,assignedsprint){
    var ui="";
    var status = result[0].epic_status;
    var statusimage = status=="Backlog"?"/images/idea_old/Backlog.png":status=="Blocked"?"/images/idea_old/Blocked.png":status=="Hold"?"/images/idea_old/Hold.png":status=="Done"?"/images/idea_old/Done.png":status=="Cancel"?"/images/idea_old/cancel.png":status=="In Progress"?"/images/idea_old/Assigned.png":"";
    var bgcolor = result[0].color_code;
    var userlist = result[0].loadSBStoryUsers;
    var name = "";var userImg = "";var user_id = "";
    bgcolor = stagetype=="History" ? "#ad9f47" : typeof(bgcolor)=="undefined"||bgcolor==""?"#f6b847": bgcolor;
    
    ui="<div id='SBstorypopupDiv_"+result[0].epic_id+"' sprintid="+sprintid+" class='SBstorypopupDivcls' style=''>"  //1
        
        +"<div class='SBpopupDiv p-1 position-relative SBpopupDivclk' style='background-color:"+bgcolor+";'>" //2
            if(stagetype=="Present"){
                ui+="<div class='position-absolute' style='top:-10px;left:-5px;'><img src='/images/task/inactivestep.svg' id='SBstorypopupcheck_"+result[0].epic_id+"' onclick='selectSbstory("+result[0].epic_id+",this,"+stageid+");event.stopPropagation();' style='width:10px;height:10px;cursor:pointer;z-index:1;'></img></div>"
            }
            ui+="<div class='d-flex align-items-center mx-1 my-0'>"   
                +"<div class='p-0 ml-auto' onclick='closeSBstorypopup();event.stopPropagation();'><img src='images/menus/close3.svg' title='Close' style='cursor:pointer;width:13px;height:13px;'></div>"
            +"</div>"
            +"<div class='d-flex align-items-center pl-2 pr-3 mx-1 mt-0 mb-1'>"   //3
                +"<div class='p-1'><img src='images/agile/sprint_blue.svg' style='width:20px;height:20px;'></div>"
                +"<div class='p-1 defaultExceedCls mr-auto' style='max-width: 250px;'><span id='SBstorysprintname' title='"+result[0].sprint_title+"' class=''>"+result[0].sprint_title+"</span></div>"
                if(stagetype=="Present"){
                    ui+="<div id='assigned_"+result[0].epic_id+"' class='d-none p-1 ml-1'><img src='/images/idea_old/re-assigned.png' title='Assigned from "+assignedsprint+"' style='width:18px;height:18px;'></div>"
                }else{
                    ui+="<div id='assigned_"+result[0].epic_id+"' class='d-none p-1 ml-1'><img src='/images/idea_old/re-assigned.png' title='Assigned to "+assignedsprint+"' style='width:18px;height:18px;'></div>"
                }
                
            ui+="</div>"   //3
            
            +"<div class='d-flex align-items-center pl-2 pr-3 mx-1 my-1'>"   //4
                +"<div id='custIddiv' class='d-flex align-items-center p-1'>"
                    +"<span class='py-0 px-0 m-0' style=''>ID :</span>"
                    +"<input id='SBstoryCustomid' type='text' class='py-0 mx-1 mr-auto defaultExceedCls' id='' value='"+result[0].unique_cust_id+"' title='"+result[0].unique_cust_id+"' onblur=\"event.stopPropagation();\" onclick='event.stopPropagation();' style='cursor:pointer;border:none;padding-top: 5px;outline:none;width: 100px;background-color:"+bgcolor+";' autocomplete='off' readonly>"
                +"</div>"
                +"<div id='SBiconpoints_"+result[0].epic_id+"' class='d-none align-items-center p-1 mr-auto'><img class='' style='width:16px;height:16px;' src='images/idea_old/points.png'><span id='SBpointsspan_"+result[0].epic_id+"' title='' class='defaultExceedCls pl-2' style='width:100px;'></span></div>"
                +"<div id='SBiconpriority_"+result[0].epic_id+"' class='d-none p-1'><img class='SBpopOptions' src=''></div>"
                +"<div id='SBiconcomments_"+result[0].epic_id+"' class='d-none p-1'><img class='SBpopOptions' src='/images/task/task_comments.svg'></div>"
                +"<div id='SBicontask_"+result[0].epic_id+"' class='d-none p-1' onclick=\"openAgileTaskPopup("+result[0].epic_id+",'','list','"+stagetype+"',"+result[0].sprint_story_id+");event.stopPropagation();\"><img class='SBpopOptions' src='images/idea/task_nofill.svg'></div>"
                
            
            +"</div>"   //4

            +"<div class='d-flex align-items-center pl-2 pr-3 mx-1 my-1' style=''>"
                +"<textarea id='' onclick='event.stopPropagation();' onkeyup='event.stopPropagation();' placeholder='Enter your story...' class='wsScrollBar p-1' title='"+result[0].epic_title+"' style='width:99%;height: 135px;background-color:"+bgcolor+";outline: none;border: none;resize: none;' readonly>"+result[0].epic_title+"</textarea>"
            +"</div>" 

            if(stageplace=="Sprint Backlog" && stagetype=="Present"){
                if(result[0].storySprintStatus=="Progress"){
                    ui+="<div class='createBtn mx-auto pl-2 pr-3' onclick=\"openAgileTaskPopup("+result[0].epic_id+",'"+result[0].epic_title+"','SB',"+result[0].sprint_story_id+");event.stopPropagation();\" style='float:none;'>"+getValues(companyLabels,"Story_Assign")+"</div>"
                }
            }else{
                ui+="<div class='d-flex align-items-center pl-2 pr-3 mx-1 my-2' style='cursor: pointer;'>"//px-2
                    +"<div class='d-flex align-items-center' style='overflow: hidden;width:90%;'>"
                    for(var j=0;j<userlist.length;j++){
                        name = userlist[j].name;
                        if(name==""){
                            userImg = "images/temp/incomplete.png";
                            name="Assign Participants";
                        }else{
                            userImg = userlist[j].userImg;
                        }
                        
                        user_id = userlist[j].user_id;

                        ui+="<div id='user_"+user_id+"' class='mr-2'>"
                            +"<img src='"+userImg+"' title='"+name+"' onclick=\"openAgileTaskPopup("+result[0].epic_id+",'','list','"+stagetype+"',"+result[0].sprint_story_id+");event.stopPropagation();\" onerror='userImageOnErrorReplace(this);' class='rounded-circle' style='width: 30px;height: 30px;border-radius: 50%;cursor: pointer;'>"
                        +"</div>"
                    }
                    ui+="</div>"
                    +"<div class='' style='width:10%;text-align: right;'>"
                        +"<img id='statusimg' src='"+statusimage+"' title='"+status+"' style='width:22px;height:22px;' >"
                    +"</div>"    
                +"</div>"
            }
            


        ui+="</div>"   //2

    +"</div>"   //1
    
    return ui;
}

function selectSbstory(epicid,obj,stageid){
    if($(obj).attr('src').indexOf('/images/task/inactivestep.svg') == -1){
        $(obj).attr('src','/images/task/inactivestep.svg');
        $('#checkStoryid_'+epicid).attr('src','/images/task/inactivestep.svg').removeClass('check')
    }else{
        $(obj).attr('src','/images/task/activestep.svg');
        $('#checkStoryid_'+epicid).attr('src','/images/task/activestep.svg').addClass('check');
    }
    
    showReassign(stageid);
}

function closeSBstorypopup(){
    $('.SBstorypopupDivcls').remove();
    glbsprintplace="";
}

function moveSBstory(obj,move,epicid){
    var stageidnew="";
    var stageid="";
    var cloneTo="";
    var place="";
    var stagename="";
    stageid=$(obj).parents('li.SBsettingStageCls').attr('stageid');
    if(move=='left'){
        stageidnew=$('li#SBsettingStage_'+stageid).prev('li').attr('stageid');
        cloneTo=$('li#SBsettingStage_'+stageidnew);
    }else{
        stageidnew=$('li#SBsettingStage_'+stageid).next('li').attr('stageid');
        cloneTo=$('li#SBsettingStage_'+stageidnew);
    }
    
    console.log("stageidnew-->"+stageidnew);

    if($('li#SBsettingStage_'+stageid).hasClass('backlogstage')){
        stagename="Backlog";
    }else if($('li#SBsettingStage_'+stageid).hasClass('completestage')){
        stagename="Completed";
    }else{
        stagename="Other";
    }

    if($(cloneTo).hasClass('backlogstage')){
        confirmAction="Backlog";
        alertFunNew(getValues(companyAlerts,"Alert_StageBacklogMsg"),'error');
    }else if($(cloneTo).hasClass('completestage')){
        confirmAction="Completed";
        confirmSBStoryMove(stageidnew,epicid,stagename,stageid,move);
    }else{
        confirmAction="Other";
        confirmSBStoryMove(stageidnew,epicid,stagename,stageid,move);
    } 

}

function confirmSBStoryMove(stageidnew,epicid,stagename,stageid,move){
    var cloneval="";
    let jsonbody = {
        "user_id": userIdglb,
        "story_id": epicid,
        "stage_id": stageidnew,
        "stage_name": confirmAction,
        "place": "sprint",
        "type": "S"
    }  
    console.log("JSON.stringify(jsonbody)-->"+JSON.stringify(jsonbody));
    console.log(stageidnew+"<---->"+epicid+"<---->"+stagename+"<---->"+stageid);
    
    $.ajax({
        url: apiPath+"/"+myk+"/v1/moveStoryToStage",
        type:"POST",
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){
             
            if(result=="NotCompleted"){
               alertFunNew(getValues(companyAlerts,"Alert_StageCompleteError"),'warning');
               if(DragNdrop==true){
                revertDrag();
               }
            }else if(result=="Completed"){
                if(DragNdrop == true){
                    //confirmFunNew(getValues(companyAlerts,"Alert_StageCompleteUpdate"),"clear","SbsprintCompleteupdate","revertDrag");
                    confirmResetNew(getValues(companyAlerts,"Alert_StageCompleteUpdate"),'clear',"SbsprintCompleteupdate","revertDrag",'Ok','Cancel',epicid,stageidnew,confirmAction);
                }else{
                    confirmFunNew(getValues(companyAlerts,"Alert_StageCompleteUpdate"),"clear","SbsprintCompleteupdate",epicid);
                }
                
            }else{

                

                if(stagename=="Other" && move == "right"){
                    cloneval = $('#SBstory_'+epicid).clone();
                    $('#SBcontent_'+stageidnew).append(cloneval);
                    $('#SBcontent_'+stageid).find('#SBstory_'+epicid).remove();
                    $('#stageStoryCount_'+stageid).text($('#SBcontent_'+stageid).children().length);
                    $('#stageStoryCount_'+stageidnew).text($('#SBcontent_'+stageidnew).children().length);
                    //$("#storyCountTooltipid_"+stageid).text("Number of stories: "+$('#SBcontent_'+stageid).children().length);
                    //$("#storyCountTooltipid_"+stageidnew).text("Number of stories: "+$('#SBcontent_'+stageidnew).children().length);
                    $('#contentstage_'+stageid+', #stagenameDiv_'+stageid).attr("title","Number of stories: "+$('#SBcontent_'+stageid).children().length);
                    $('#contentstage_'+stageidnew+', #stagenameDiv_'+stageidnew).attr("title","Number of stories: "+$('#SBcontent_'+stageidnew).children().length);
                }else if(stagename=="Other" && move == "left"){
                    cloneval = $('#SBstory_'+epicid).clone();
                    $('#SBcontent_'+stageidnew).append(cloneval);
                    $('#SBcontent_'+stageid).find('#SBstory_'+epicid).remove();
                    $('#stageStoryCount_'+stageid).text($('#SBcontent_'+stageid).children().length);
                    $('#stageStoryCount_'+stageidnew).text($('#SBcontent_'+stageidnew).children().length); 
                    //$("#storyCountTooltipid_"+stageid).text("Number of stories: "+$('#SBcontent_'+stageid).children().length);
                    //$("#storyCountTooltipid_"+stageidnew).text("Number of stories: "+$('#SBcontent_'+stageidnew).children().length);
                    $('#contentstage_'+stageid+', #stagenameDiv_'+stageid).attr("title","Number of stories: "+$('#SBcontent_'+stageid).children().length);
                    $('#contentstage_'+stageidnew+', #stagenameDiv_'+stageidnew).attr("title","Number of stories: "+$('#SBcontent_'+stageidnew).children().length);
                }else{
                    if(stagename=="Other" && DragNdrop==true){
                        $('#stageStoryCount_'+stageid).text($('#SBcontent_'+stageid).children().length);
                        $('#stageStoryCount_'+stageidnew).text($('#SBcontent_'+stageidnew).children().length);
                        //$("#storyCountTooltipid_"+stageid).text("Number of stories: "+$('#SBcontent_'+stageid).children().length);
                        //$("#storyCountTooltipid_"+stageidnew).text("Number of stories: "+$('#SBcontent_'+stageidnew).children().length);
                        $('#contentstage_'+stageid+', #stagenameDiv_'+stageid).attr("title","Number of stories: "+$('#SBcontent_'+stageid).children().length);
                        $('#contentstage_'+stageidnew+', #stagenameDiv_'+stageidnew).attr("title","Number of stories: "+$('#SBcontent_'+stageidnew).children().length);
                    }
                } 

                
            
            }

            confirmAction="";
            stagename="";
            stageidglb="";
            DragNdrop==false;
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });     

}

function revertDrag(){
    $(cansort.sender).sortable("cancel");
    //$('#SB_story_'+confirmStoryId).find('img.sb_nextStage , img.sb_prevStage').show();
	removePrevNext();
}

function removePrevNext(){
    $('#sprintSBtable').each(function(){
       $(this).find('.backlogstage').find('.sb_nextStage,.sb_prevStage').hide();
       $(this).find('.completestage').find('.sb_nextStage').hide();
    });
    //var projUserStatus = projectUsersStatus;
    //if(projUserStatus!="PO"){
    //  $('#SB_table tr.SB_sprintStories').find('.sb_userListDiv img').removeAttr('onclick');
    //}
    $('.SBcontentCls').find('.SBdragdiv').css('cursor','move');
    $('td.backlogstage').find('.SBdragdiv').css('cursor','default');
}

function SbsprintCompleteupdate(epicid,stageidnew,confirmAction){
    console.log(epicid+"----"+stageidnew+"----"+confirmAction);
    $('#SBcompletepopup').html(updateCommmentSbcompleteUI(epicid,stageidnew,confirmAction));
    $('#completeCmttextbox').focus();
    $("#transparentDiv").show();
}

function updateCommmentSbcompleteUI(epicid,stageidnew,confirmAction){
    var ui="";

    $('#SBcompletepopup').html('');
    $("#transparentDiv").show();

    ui+='<div class="agilepopupcls" id="">'
      +'<div class="modal-dialog">'
          +'<div class="modal-content container">'
          
              
              +'<div class="modal-header py-2 pl-0" style="border-bottom: none !important;">'
                  +'<p class="modal-title" style="font-size:14px;color: black;font-weight: normal;text-transform: uppercase;">'+getValues(companyLabels,"Agile_comment")+'</p>'
                  +'<button type="button" onclick="closeCmtbox();" class="close p-0" data-dismiss="agilepopupcls" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
              +'</div>'
              
              
              +'<div class="" style="">'
                  +'<div class="p-0 " style="font-size:11px;">'
                      +'<div class="py-2 material-textfield">'
                          +'<textarea id="statuscommentcomplete_'+epicid+'" class="px-2 py-2 material-textfield-input1 newinput w-100" placeholder=" "  style="height:100px !important;outline:none;resize:none;"></textarea>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;top:25px">Enter your comments</label>'
                      +'</div>'
              +'</div>'
              
              +'<div class="modal-footer py-2 px-0" style="border-top: none !important;">'
                  +'<img src="/images/task/remove.svg" title="Cancel" onclick="closeCmtbox();event.stopPropagation();" style="width:25px;height:25px;cursor:pointer;">'
                  +"<img src=\"/images/task/tick.svg\" title=\"Update\" onclick=\"updateCompletestory("+epicid+","+stageidnew+",'"+confirmAction+"');event.stopPropagation();\" style=\"width:25px;height:25px;cursor:pointer;\">"
              +'</div>'
              
          +'</div>'
      +'</div>'
    +'</div>'
    return ui;

}

function updateCompletestory(epicid,stageidnew,confirmAction){
    let jsonbody = {
        "user_id": userIdglb,
        "story_id": epicid,
        "stage_id": stageidnew,
        "stage_name": confirmAction,
        "place": "sprint",
        "type": "S"
    }  
    console.log("JSON.stringify(jsonbody)-->"+JSON.stringify(jsonbody));
    
    $.ajax({
        url: apiPath+"/"+myk+"/v1/moveStoryToStage",
        type:"POST",
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){

            $('#SBsprintMoveright_'+epicid).addClass('d-none');
            updateStatus(epicid,'Done','SBlist');
            $('#SBcompletepopup').html('');
            $("#transparentDiv").hide();

        }
    });        

}

function closeCmtbox(epicid,stageidnew,confirmAction){
    console.log(epicid+"----"+stageidnew+"----"+confirmAction);
    $('#SBcompletepopup').html('');
    $("#transparentDiv").hide();
    revertDrag();
}

function cancelSBSprintcomplete(){
    $('#SBcompletepopup').html('');
    $("#transparentDiv").hide();
}

function checkSBstory(epicid){
    if($('#oneSbstorychekbox_'+epicid).prop('checked')==true){
        $('#reassignButton').addClass('d-block').removeClass('d-none');
    }else{
        $('#reassignButton').addClass('d-none').removeClass('d-block'); 
    }
}

function checkStories(stageid){
    if($('#SBstagecheckbox_'+stageid).prop('checked') == true){
        $('#stagecount_'+stageid).find('.SBcheckbox').prop('checked',true);
        $('#reassignButton').addClass('d-block').removeClass('d-none');
    }else{
        $('#stagecount_'+stageid).find('.SBcheckbox').prop('checked',false);
        $('#reassignButton').addClass('d-none').removeClass('d-block');
    }
}

function showSBSprintDesc(obj){
    $(obj).attr('onclick','hideSBSprintDesc(this)');    //.attr('src','/images/task/dependencyup.svg')
    $('#sprintInstruction').addClass('d-flex').removeClass('d-none');
    setHgtforSbtable();
}

function hideSBSprintDesc(obj){
    $(obj).attr('onclick','showSBSprintDesc(this)');        //.attr('src','/images/task/dependencydown.svg')
    $('#sprintInstruction').addClass('d-none').removeClass('d-flex');
    setHgtforSbtable();
}

function aInactivecheck(sprintid){

}

function showSprintmoreoptions(obj){
    $(obj).find('#sprintfloatoptionsID').removeClass('d-none').addClass('d-flex');
}

function hideSprintmoreoptions(obj){
    $(obj).find('#sprintfloatoptionsID').removeClass('d-flex').addClass('d-none');
}

function deleteSprintalert(sprintid,type,status){
    sprintDelStatus=status;
    if(type=="sprint_delete"){
        conFunNew(getValues(companyAlerts,"Alert_deletesprint"),"warning","deleteSprint","",sprintid,type);
    }else if(type=="sprint_active" && status=='A'){
        conFunNew(getValues(companyAlerts,"Alert_archivesprint"),"warning","deleteSprint","",sprintid,type);
    }else if(type=="sprint_active" && status=='Y'){
        conFunNew(getValues(companyAlerts,"Alert_activesprint"),"warning","deleteSprint","",sprintid,type);
    }else if(type=="sprint_hide" && status=='D'){
        conFunNew(getValues(companyAlerts,"Alert_disablesprint"),"warning","deleteSprint","",sprintid,type);
    }else if(type=="sprint_hide" && status=='Y'){
        conFunNew(getValues(companyAlerts,"Alert_enablesprint"),"warning","deleteSprint","",sprintid,type);
    }
    
}

function deleteSprint(sprintid,type){
    
    let jsonbody = {
      "sprint_id" : sprintid,
      "status" : sprintDelStatus,
      "type" : type,
      "user_id" :userIdglb
    }
    //$('#loadingBar').addClass('d-none').removeClass('d-flex');
    $.ajax({ 
        url: apiPath+"/"+myk+"/v1/deleteSprint",
        type:"DELETE",
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');
          }, 
        success:function(result){
          
            if(type=="sprint_active"){
                if(sprintDelStatus=="Y"){
                    $('#editSprintimg_'+sprintid).css('opacity','');
                    $('#editSprintimg_'+sprintid).attr('onclick','editSprint('+sprintid+',"edit");');
                    $('#sprintlistcontent_'+sprintid).attr('ondblclick','editSprint('+sprintid+',"edit");');
                    $('#aInactStatus_'+sprintid).attr('src','images/agile/sprint_active.svg').attr('title','Active');

                    $('#grideditSprintimg_'+sprintid).css('opacity','');
                    $('#grideditSprintimg_'+sprintid).attr('onclick','editSprint('+sprintid+',"edit");');
                    $('#gridsprintlistcontent_'+sprintid).attr('ondblclick','editSprint('+sprintid+',"edit");');
                    $('#gridaInactStatus_'+sprintid).attr('src','images/agile/sprint_active.svg').attr('title','Active');
                }else{
                    $('#editSprintimg_'+sprintid).css('opacity','0.3');
                    $('#editSprintimg_'+sprintid).removeAttr('onclick');
                    $('#sprintlistcontent_'+sprintid).removeAttr('ondblclick');
                    $('#aInactStatus_'+sprintid).attr('src','images/agile/sprint_inactive.svg').attr('title','Inactive');

                    $('#grideditSprintimg_'+sprintid).css('opacity','0.3');
                    $('#grideditSprintimg_'+sprintid).removeAttr('onclick');
                    $('#gridsprintlistcontent_'+sprintid).removeAttr('ondblclick');
                    $('#gridaInactStatus_'+sprintid).attr('src','images/agile/sprint_inactive.svg').attr('title','Inactive');
                }
                
                sprintDelStatus=sprintDelStatus=="Y"?"A":"Y";
                $('#aInactStatus_'+sprintid).attr('onclick','deleteSprintalert('+sprintid+',"'+type+'","'+sprintDelStatus+'");event.stopPropagation();');
                $('#gridaInactStatus_'+sprintid).attr('onclick','deleteSprintalert('+sprintid+',"'+type+'","'+sprintDelStatus+'");event.stopPropagation();');

            }else if(type=="sprint_delete"){
                $('#sprint_'+sprintid).remove();
                $('#sprintGrid_'+sprintid).remove();
            }else if(type=="sprint_hide"){
                $('#sprint_'+sprintid).remove();
                $('#sprintGrid_'+sprintid).remove();
            }
            
            sprintDelStatus="";
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        }  
    });    
}

function editSprint(sprintid,place){
    if($('#SprintHolderDiv').length == 0){
        $('[id^=sprintGrid_]').removeClass("taskNodeSelect");
        let jsonbody={
            "groupLoadType":"active",
            "project_id":prjid,
            "sprint_id":sprintid
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/fetchJsonSprint",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                if(result != ""){    
                    $('#SprintDivMain').append(createSprintUi(place,sprintid));
                    calenderForsprint();
                    
                    $('textarea#sprintname1').val(result[0].sprint_title);
                    if(result[0].sprint_desc.trim() != ""){
                        $('.instruction').show();
                        $('textarea#sprintInsTextarea').val(result[0].sprint_desc);
                        $('img#sprintinstruction').attr('src','/images/task/intr.svg');
                    }
                    var startdate = returnFormatedDatesprint(result[0].start_date);
                    var enddate = returnFormatedDatesprint(result[0].end_date);
                    $('#sprintstartDateCalNewUI').text(startdate).attr('parmanentvalue',startdate);
                    $('#sprintendDateCalNewUI').text(enddate).attr('parmanentvalue',startdate);
                    var sdate = result[0].start_date.split('-');
                    sdate = sdate[1]+"-"+sdate[2]+"-"+sdate[0];
                    var edate = result[0].end_date.split('-');
                    edate = edate[1]+"-"+edate[2]+"-"+edate[0];
                    $('#sprintstartDateCalNewUI').attr('formatdate',sdate);
                    $('#sprintendDateCalNewUI').attr('formatdate',edate);

                    var color = result[0].color_code==""?"#f2788f":result[0].color_code;
                    $('#sprintgroupchange').attr('groupid',result[0].sprint_group_id).attr('groupcode',color).attr('title',result[0].sprint_group_name).css('background',color);
                    $('#sprintgroupnamechange').text(result[0].sprint_group_name);

                    $('#savesprint').attr('onclick','updatesprint();');
                    $('#sprintIdHidden').val(result[0].sprint_id);
                    $('#sprintPlaceHidden').val("edit");

                    if(result[0].storyCount != "0"){
                        $('#sprintAddstory').attr('src','images/agile/storyiconfilled.svg');  //change immg
                        
                    }
                    $('#existingStory').trigger('click');

                    $('.time_calendersprint ').hide();

                    var elmnt = document.getElementById("sprintDiv");
                    elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});

                    $('#SprintDivContentMain, .calendar-1').hide();

                    $("#sprintGrid_"+sprintid).addClass("taskNodeSelect");

                }    
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            
            }
        });


        
    }else{
        $('#SprintHolderDiv').remove();
        $('#SprintDivContentMain,.calendar-1').show();
    }
}

function updatesprint(){
    var sprintid=$('#sprintIdHidden').val();
    var sprintname=$('#sprintname1').val().trim();
    var instruction=$('#sprintInsTextarea').val().trim();
    var sprintgroupid=$('#sprintgroupchange').attr('groupid');
    var sprintstartdate=$('#sprintstartDateCalNewUI').attr('formatdate');
    var sprintenddate=$('#sprintendDateCalNewUI').attr('formatdate');

    if(typeof(sprintname)=="undefined" || sprintname == ""){
        alertFunNew("Sprint name cannot be Empty",'error');
        return false;
    }else{
        var storyids = "";
        $('#storysprintcontentdiv > div').each(function () { 
            if($(this).hasClass('newadded')){
                var stid1 = $(this).attr('id').split("_")[1]; 
                storyids=stid1+","+storyids;
            }
        });
        storyids=storyids.substring(0,storyids.length-1);
        var removestoryids = "";
        $('#storysprintcontentdiv > div').each(function () { 
            if($(this).find('.check').attr("uncheckvalue")=='y'){
                var removestid1 = $(this).attr('id').split("_")[1]; 
                removestoryids=removestid1+","+removestoryids;
            }
        });
        removestoryids=removestoryids.substring(0,removestoryids.length-1);
        $('#loadingBar').addClass('d-flex').removeClass('d-none');
        let jsonbody = {
            "storyTasks": sprintname,
            "comments": instruction,
            "project_id": prjid,
            "sprint_group_id": sprintgroupid,
            "sprintStartDate": sprintstartdate,
            "sprintEndDate": sprintenddate,
            "user_id": userIdglb,
            "groupLoadType": "active",
            "selectedStories": storyids,
            "deletedStoryId": removestoryids,
            "sprintId": sprintid,
            "dateList": JSON.parse(calSprintDateSelectedJSON),
            "prevStartDate":sprintstartdate,
            "prevEndDate":sprintenddate
        }
        console.log("JSON.stringify(jsonbody)-->"+JSON.stringify(jsonbody));
        $.ajax({
            url: apiPath + "/" + myk + "/v1/createSprintTask",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            beforeSend: function (jqXHR, settings) {
            xhrPool.push(jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            
            },
            success: function (result) { 
                if(sprintviewtype=="grid"){
                    $('#sprintGrid_'+sprintid).replaceWith(listGridSprintUI(result));
                    
                }else if(sprintviewtype=="list"){
                    $('#sprint_'+sprintid).replaceWith(listSprintUI(result));
                    
                }else{
                    $('.calendar-1').html('');
                    listCalendarSprint();
                }
                $('#SprintDivContentMain').show();
                $('#SprintHolderDiv').remove();
                
                sprintcheckflag=false;
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
            } 
        });       
    }
    

}

function createSprint(place,sGroupid,ccode,sgname){
//alert("1-->"+$('#SprintHolderDiv').length)
    if($('#SprintHolderDiv').length == 0){
        $('#SprintDivMain').append(createSprintUi(place,0,sGroupid,ccode,sgname));
        calenderForsprint();
        $('#sprintstartDateCalNewUI').attr('formatdate',myCalendar.getFormatedDate("%m-%d-%Y", new Date()));
        $('#sprintendDateCalNewUI').attr('formatdate',myCalendar.getFormatedDate("%m-%d-%Y", new Date()));
        $('.time_calendersprint ').hide();
        $('#SprintDivContentMain').hide();
        $('#existingStory').trigger('click');
        
        if($('#sprintSBtable').is(':visible')){
            $('#SprintScrumboardDiv').html('');
            $('#sprintSBback').addClass('d-none').removeClass('d-flex').removeClass('d-block');
        }
        if($('#SBsettingsMaindiv').is(':visible')){
            $('#SBsettingsMaindiv').remove();
            $('#sprintSBback').children('img').attr('onclick','closeSprintSB()');
            $('#sprintSBback').addClass('d-none').removeClass('d-block').removeClass('d-flex');
        }
        if($('.calendar-1').is(':visible')){
            $('.calendar-1').hide();
        }
    }else{
        if(sprintcheckflag==true){
            var plac = $('#sprintPlaceHidden').val();
            var clkfunc = plac=="create"?"insertCreateSprint":"updatesprint";
            confirmResetNew(getValues(companyAlerts,"Alert_Save"),'clear',clkfunc,"cancelsprintcreatedone",'OK','CANCEL');
        }else{
            $('#SprintHolderDiv').remove();
            $('#SprintDivContentMain').show();
            if($('.calendar-1').children().length!=0){
                $('.calendar-1').show();
            }
            
        }
        
    }
    calSprintDateSelectedJSON="[]";

}

function createSprintUi(place,sprintid,sGroupid,ccode,sgname){
    var ui="";

    const d = new Date();
    var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let sprintnowDate=(monthNames[parseInt(d.getMonth())+1])	+" "+(parseInt(d.getDate())<10?"0"+d.getDate():d.getDate())+", "+d.getFullYear()	;	
    let amPm=parseInt(d.getHours())>11?"PM":"AM";	
    let sprintnowTime=(parseInt(d.getHours())>12?((parseInt(d.getHours())-12)<10?("0"+(parseInt(d.getHours())-12)):(parseInt(d.getHours())-12)):(parseInt(d.getHours())<10?"0"+d.getHours():d.getHours()))+":"+(parseInt(d.getMinutes())<10?"0"+d.getMinutes():d.getMinutes())+" "+amPm;
    ccode = typeof(ccode)=="undefined"||ccode==""?"#f2788f":ccode;
    sGroupid= typeof(sGroupid)=="undefined"?"":sGroupid;
    sgname= typeof(sgname)=="undefined"?"":sgname;
    ui="<div id='SprintHolderDiv'>"   //1
    
        +"<div id='sprintDiv'>" //2
            
        
            +'<div class="px-3 pt-3">' 
                +'<div style="">'
                    +'<div class="material-textfield" >'
                        +'<textarea id="sprintname1" type="text" class="theightdemo material-textfield-input sprinttextbox"  onkeyup="checkvalid(this,\''+place+'\');event.stopPropagation();"  placeholder=" " ></textarea>'
                        +'<label class="material-textfield-label" style="    font-size: 12px;top:18px">Sprint Name</label>'
                    +'</div>'
                +'</div>'
                + '<div class="instruction" style="display:none;margin-top:6px;">'
                    +'<div class="material-textfield" >'
                        +'<textarea id="sprintInsTextarea" type="text" onkeyup="changeImgSrc(\'instruction\');checkvalid(this,\''+place+'\');event.stopPropagation();" class="theightdemo material-textfield-input sprinttextbox"  onkeyup=""  placeholder=" " ></textarea>'
                        +'<label class="material-textfield-label" style="    font-size: 12px;top:18px">Instructions</label>'
                    +'</div>'
                + '</div>'
            +'</div>'

            /* +"<div id='sprintCreateStoryMainDiv' class='d-block px-3 py-2 position-relative'>"
                +"<img src='images/agile/story.svg' id='sprintCreateStoryImg' title='Create Story' onclick=\"createSprintStory('"+place+"');\" class='ml-2' style='cursor:pointer;width:20px;height:20px;'>"
                +"<div id='sprintCreateStoryDiv'></div>"
            +"</div>" */


            +"<div class='d-flex align-items-start px-3 py-2 position-relative'>"
                +"<div id='sprintOptionsDiv' class='d-flex' style='width:70%;'>"
                    +'<div class="p-1 convoSubIcons position-relative"><img id="sprintAddstory" title="Add Story" onclick="showstoryoptions(this,\''+place+'\');event.stopPropagation();" class="img-responsive sprinticon" src="images/agile/story.svg">'
                        +"<div class='storyshowoption position-absolute rounded p-1 border' style='display:none;cursor:pointer;'>"
                            +"<div id='newCreateStory' class='d-flex p-1 justify-content-start' style='height:24px;' onclick=\"createSprintStory('"+place+"');event.stopPropagation();\">"
                                //+"<img src='images/agile/story.svg' id='sprintCreateStoryImg' title='Create Story' class='' style='cursor:pointer;width:18px;height:18px;'>"
                                +"<span class='pl-2'>Create New</span>"
                            +"</div>"
                            +"<div id='existingStory' class='d-flex p-1 justify-content-start' style='height:24px;' onclick='showStorieslist(\""+place+"\","+sprintid+");event.stopPropagation();'>"
                                //+'<img id="sprintAddstory" title="Existing Story"  class="img-responsive" style="width:18px;height:18px;" src="images/agile/story.svg" >'
                                +"<span class='pl-2'>Existing</span>"
                            +"</div>"
                        +"</div>"
                        +"<div id='sprintCreateStoryDiv'></div>"
                    +'</div>'
                    +'<div class="p-1 convoSubIcons"><img id="sprintinstruction" title="Instruction" class="img-responsive sprinticon" src="/images/task/taskInstructions_cir.svg" onclick="showSprintInstruction();event.stopPropagation();"></div>'
                +"</div>"
                +"<div id='' class='' style='width:30%;font-size: 12px;'>"
                    +"<div class='firstDiv d-flex align-items-center justify-content-start'>"
                        
                        +"<div class='d-flex align-items-center mr-3'>"
                            +"<div>Start&nbsp;:&nbsp;</div>"
                            +"<div class='pl-1 d-flex align-items-center' style='color: black;border-bottom: 1px solid darkgray;white-space: nowrap;width:80px;'>"
                                +"<div id='sprintstartDateCalNewUI' formatdate='' prevdate='' parmanentvalue='"+sprintnowDate+"'>"+sprintnowDate+"&nbsp;</div>"
                                //+"<div id='sprintstartTimeCalNewUI' parmanentvalue='"+sprintnowTime+"'>"+sprintnowTime+"</div>"
                            +"</div>"
                            +"<div id='calenderForsprintstart' class='pl-2' onclick='showCalendersprint(this);event.stopPropagation();' style='cursor:pointer;'>"
                                +'<img src="images/landingpage/cal.svg" style="width:15px">'
                            +"</div>"
                        +"</div>"
                        
                        +"<div class='d-flex align-items-center mr-3'>"
                            +"<div class='pl-1'>Due&nbsp;:&nbsp;</div>"
                            +"<div class='pl-1 d-flex align-items-center' style='color: black;border-bottom: 1px solid darkgray;white-space: nowrap;width:80px;'>"
                                +"<div id='sprintendDateCalNewUI' formatdate='' prevdate='' parmanentvalue='"+sprintnowDate+"'>"+sprintnowDate+"&nbsp;</div>"
                                //+"<div id='sprintendTimeCalNewUI' parmanentvalue='"+sprintnowTime+"'>"+sprintnowTime+"</div>"
                            +"</div>"
                            +"<div id='calenderForsprintend' class='pl-2' onclick='showCalendersprint(this);event.stopPropagation();' style='cursor:pointer;'>"
                                +'<img src="images/landingpage/cal.svg" style="width:15px">'
                            +"</div>"
                        +"</div>"
                        
                        +"<div id='calendarcheckdiv' class='position-relative' style='cursor:pointer;'>"
                            +"<img src='images/agile/more.svg' onclick=\"openSprintselectDatepopup('"+place+"');event.stopPropagation();\" style='width:16px;'>"
                        +"</div>" 
                        

                    +"</div>"
                    +"<div class='secondDiv d-flex align-items-center justify-content-start pt-2'>"
                        
                        +"<div id='sprintgroupdiv' class='position-relative sprintgroupdivcls' style='cursor:pointer;'>"
                            +"<div class='d-flex align-items-center' onclick='listSprintgroups();event.stopPropagation();'>"//
                                +"<div>Sprint Group &nbsp; <img src='images/task/dowarrow.svg' style='width:12px;height:12px;'> </div>"
                                +"<div id='sprintgroupchange' groupid="+sGroupid+" groupcode='"+ccode+"' title='' class='sprintgroup rounded-circle ml-2' style='background: "+ccode+";'></div>"
                                +"<div id='sprintgroupnamechange' class='ml-2'>"+sgname+"</div>"
                            +"</div>"
                        +"</div>"
                        
                        

                    +"</div>"

                    /////////////

                    +'<div class="position-absolute p-1 time_calendersprint border border-secondary" style=" border:1px solid black;z-index:2 ;background:white;font-size:14px;color:#64696F;display:none;margin-top: 5px;max-width:412px;width:268px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;" >'
                    +'<div class="mt-1" >'
                    /* +'<div class="d-flex justify-content-around ml-1" style="max-width: 281px;">'      //Time Div Starts
                        +'<div class="mt-2"> Time </div>'
                        +'<div>'
                            +' <div><img class="ml-1" src="/images/arrowUpNew.png" class="removeonClickBtnIcon" onclick="increaseValuesprint(\'hour\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                            +' <div style="margin-left:2px" id="hour">10</div>'
                            +' <div><img class="ml-1" src="/images/arrowDownNew.png" class="removeonClickBtnIcon" onclick="decreaseValuesprint(\'hour\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                            
                        +' </div>'
                        +'<div>'
                            +' <div><img class="ml-1" src="/images/arrowUpNew.png" class="removeonClickBtnIcon"  onclick="increaseValuesprint(\'minute\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                            +' <div style="margin-left:2px" id="minute">10</div>'
                            +' <div><img class="ml-1" src="/images/arrowDownNew.png" class="removeonClickBtnIcon" onclick="decreaseValuesprint(\'minute\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                            
                        +' </div>'
                        +'<div>'
                            +' <div><img class="ml-1" src="/images/arrowUpNew.png" class="removeonClickBtnIcon"  onclick="increaseValuesprint(\'second\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                            +' <div style="margin-left:2px" id="second">10</div>'
                            +' <div><img class="ml-1" src="/images/arrowDownNew.png" class="removeonClickBtnIcon" onclick="decreaseValuesprint(\'second\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                            
                        +' </div>'
                            +'<div class="d-flex mt-2 justify-content-around mainampm" style="border:1px solid rgb(111, 158, 176);height:22px;width:50px;cursor:pointer" onclick="changeAmPmsprint(this);">'
                                +'<div class="rounded-circle colorDiv " style="height:18px;width:18px;background-color:rgb(111, 158, 176);margin-top:1px"></div>'
                                +'<div class="ampm" >AM</div>'
                            +' </div>'       
                        +'</div>' */ //Time div ends

                        +'<div id="calender_forsprint" class="mt-2 mx-1" style="height:265px;"> </div>'
                        //Calender Div 
                    +'</div>'

                    +'</div>'

                    /////////////


                +"</div>"

                
            +"</div>"

            +"<div id='sprintStorylistDiv' style='display:none;'></div>"

            +"<div id='' class='sprintSaveCancelDiv p-1'>"
                +"<div class='d-flex align-items-center justify-content-end'>"
                    +"<img title='Cancel' id='cancelsprint' class='img-responsive' src='/images/task/remove.svg' onclick='cancelsprintcreate();event.stopPropagation();' style='float:left;height:25px;width:25px;margin-right: 10px;cursor:pointer;'>"
                    +"<img title='Save' id='savesprint' class='img-responsive' src='/images/task/tick.svg' onclick='insertCreateSprint();event.stopPropagation();' style='float:left;height:25px;width:25px;margin-right: 8px;cursor:pointer;'>"
                +"<div>"
            +"</div>"
            +"<input type='hidden' id='sprintIdHidden' value='0'>"
            +"<input type='hidden' id='sprintPlaceHidden' value='create'>"

        +"</div>"   //2


    +"</div>"   //1



    return ui;

}

function showstoryoptions(obj,place){
    
    if(place == "edit" && $('#sprintStorylistDiv').is(':visible')==false){
        //$('#existingStory').trigger('click');
        $('.storyshowoption').show();
    }else{
        $('.storyshowoption').toggle();
    }
}

function createSprintStory(place,sprintid){
    if($('.agileDivCls').length == 0){
        if(place=="sprintgrid"){
            $('#sprintCreateStoryGridDiv_'+sprintid).html(prepareCreateAgileUi(0,"sub","create","S","fromSprintgrid"));
            var elmnt = document.getElementById("AgileDiv_0");
            elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
            ////$('#saveepic_0').attr('onclick','adStorytoSprint('+sprintid+',"'+place+'");event.stopPropagation();');
        }else{
            $('#sprintCreateStoryDiv').html(prepareCreateAgileUi(0,"sub","create","S","fromSprint"));
        }
        $('.agileDivCls').css({"left": "30px", "top": "-30px"});
        $('.storyshowoption').hide();
    }else{
        $(".agileDivCls").remove();
    }
    
}

function adStorytoSprint(sprintid,storyid){
    var sprintname=$('#gridsprintname_'+sprintid).val();
    var instruction=$('#gridinstruction_'+sprintid).val();
    var sprintgroupid=$('#gridsprintgroupid_'+sprintid).val();
    var sprintstartdate=$('#gridsprintstartdate_'+sprintid).val();
    var sprintenddate=$('#gridsprintenddate_'+sprintid).val();
    var removestoryids="";
    if(storyid==''){
        $('#storysprintcontentdiv > div').each(function () { 
            if($(this).hasClass('newadded')){
                var stid1 = $(this).attr('id').split("_")[1]; 
                storyid=stid1+","+storyid;
            }
        });
        storyid=storyid.substring(0,storyid.length-1);
        
        $('#storysprintcontentdiv > div').each(function () { 
            if($(this).find('.check').attr("uncheckvalue")=='y'){
                var removestid1 = $(this).attr('id').split("_")[1]; 
                removestoryids=removestid1+","+removestoryids;
            }
        });
        removestoryids=removestoryids.substring(0,removestoryids.length-1);
    }
    let jsonbody = {
        "storyTasks": sprintname,
        "comments": instruction,
        "project_id": prjid,
        "sprint_group_id": sprintgroupid,
        "sprintStartDate": sprintstartdate,
        "sprintEndDate": sprintenddate,
        "user_id": userIdglb,
        "groupLoadType": "active",
        "selectedStories": storyid,
        "deletedStoryId": removestoryids,
        "sprintId": sprintid,
        "dateList": JSON.parse(calSprintDateSelectedJSON),
        "prevStartDate":sprintstartdate,
        "prevEndDate":sprintenddate
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    console.log("JSON.stringify(jsonbody)-->"+JSON.stringify(jsonbody));
    $.ajax({
        url: apiPath + "/" + myk + "/v1/createSprintTask",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        beforeSend: function (jqXHR, settings) {
        xhrPool.push(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        
        },
        success: function (result) {
            $(".agileDivCls").remove();
            if(sprintviewtype=="grid"){
                $('#sprintGrid_'+sprintid).replaceWith(listGridSprintUI(result));
                
            }else if(sprintviewtype=="list"){
                $('#sprint_'+sprintid).replaceWith(listSprintUI(result));
                
            }else{
                $('.calendar-1').html('');
                listCalendarSprint();
            }
            closeExistStory();
            sprintcheckflag=false;
            $('#loadingBar').addClass('d-none').removeClass('d-flex');

        }
    });        

}

function openSprintselectDatepopup(place){
    var sprintId=$('#sprintIdHidden').val();
    var startDate=$('#sprintstartDateCalNewUI').attr('formatdate');
    var endDate=$('#sprintendDateCalNewUI').attr('formatdate');
    var startprevdate=$('#sprintstartDateCalNewUI').attr('prevdate');
    var endprevdate=$('#sprintendDateCalNewUI').attr('prevdate');
    var sprintplace = $('#sprintPlaceHidden').val();
    //$('.openSprintselectDatepopupcls').remove();
        
    if(typeof(startDate) == "undefined" || startDate == "undefined" || startDate.trim() == "" || startDate == null){
        alertFun(getValues(companyAlerts,"Alert_taskStartDate"),'warning');
		return false;	
    }else if(typeof(endDate) == "undefined" || endDate == "undefined" || endDate.trim() == "" || endDate == null){
        alertFun(getValues(companyAlerts,"Alert_taskEndDate"),'warning');
	    return false;		
    }else if(startprevdate != "" && endprevdate != ""){
        if((startprevdate != startDate) || (endprevdate != endDate)){
            $('.openSprintselectDatepopupcls').remove();
            conFunNew(getValues(companyAlerts,"Alert_Reset_Data"),"warning","openSprintselectDatepopupconfirm","openSprintCancelselectDatepopupconfirm",place);
        }else{
            if($('.openSprintselectDatepopupcls').length == 0){
                openSprintselectDatepopupconfirm(sprintplace);
            }else{
                $('.openSprintselectDatepopupcls').toggle();
            }      
        }

    }else{
        if($('.openSprintselectDatepopupcls').length == 0){
            openSprintselectDatepopupconfirm(sprintplace);
        }else{
            $('.openSprintselectDatepopupcls').toggle();
        } 
    }
        
      
}

function openSprintselectDatepopupconfirm(place){
    var sprintId=$('#sprintIdHidden').val();
    var startDate=$('#sprintstartDateCalNewUI').attr('formatdate');
    var endDate=$('#sprintendDateCalNewUI').attr('formatdate');
    var startprevdate=$('#sprintstartDateCalNewUI').attr('prevdate');
    var endprevdate=$('#sprintendDateCalNewUI').attr('prevdate');
    var sprintplace = $('#sprintPlaceHidden').val();
    if(sprintplace == "create"){
        let jsonbody={
            "sprint_id" : sprintId,
            "task_start_date" : startDate,
            "task_end_date" : endDate
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/getSprintMoreData",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                
                $('#calendarcheckdiv').append(openSprintselectDatepopupUI(result));

                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            
            }
        });
    }else{
        $.ajax({
            url: apiPath + "/" + myk + "/v1/getSprintMoreData1?sprintId="+sprintId+"&str_date="+startDate+"&end_date="+endDate+"",
            type: "POST",
            contentType: "application/json",
            error: function (jqXHR, textStatus, errorThrown) {
              checkError(jqXHR, textStatus, errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');
              
            },
            success: function (result) { 
                
                $('#calendarcheckdiv').append(openSprintselectDatepopupUI(result));
                $('#sprintdatecheckcontent div.inactivesprintdate').addClass('d-none').removeClass('d-flex');
                $('#loadingBar').removeClass('d-flex').addClass('d-none');
            }
          });
    }
}

function openSprintCancelselectDatepopupconfirm(place){
    var sprintId=$('#sprintIdHidden').val();
    var startDate=$('#sprintstartDateCalNewUI').attr('formatdate');
    var endDate=$('#sprintendDateCalNewUI').attr('formatdate');
    var startprevdate=$('#sprintstartDateCalNewUI').attr('prevdate');
    var endprevdate=$('#sprintendDateCalNewUI').attr('prevdate');
    var sprintplace = $('#sprintPlaceHidden').val();
    if(sprintplace == "create"){
        let jsonbody={
            "sprint_id" : sprintId,
            "task_start_date" : startprevdate,
            "task_end_date" : endprevdate
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/getSprintMoreData",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                
                $('#calendarcheckdiv').append(openSprintselectDatepopupUI(result));

                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            
            }
        });
    }else{
        $.ajax({
            url: apiPath + "/" + myk + "/v1/getSprintMoreData1?sprintId="+sprintId+"&str_date="+startDate+"&end_date="+endDate+"",
            type: "POST",
            contentType: "application/json",
            error: function (jqXHR, textStatus, errorThrown) {
              checkError(jqXHR, textStatus, errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');
              
            },
            success: function (result) { 
                
                $('#calendarcheckdiv').append(openSprintselectDatepopupUI(result));

                $('#loadingBar').removeClass('d-flex').addClass('d-none');
            }
          });
    }
}

function openSprintselectDatepopupUI(result){
    var ui = "";
    var checkcls="";var tdate="";

    ui="<div class='position-absolute rounded openSprintselectDatepopupcls' id='' style='cursor: pointer; display: block;'>"
        +"<div id='' class='p-2 d-flex align-items-center' style='border-bottom: 1px solid #5e5b5b;background-color: #003A5D;color: white;'>"
            +"<span class='' style='width:10%;'></span>"
            +"<span class='defaultExceedCls' style='width:30%;'>Date</span>"
            +"<span class='defaultExceedCls' style='width:30%;'>Day</span>"
            +"<span class='defaultExceedCls' style='width:30%;'>Estimated</span>"
            +'<button type="button" onclick="closeSprintdatepopup();event.stopPropagation();" class="close p-0" data-dismiss="" style="color: white;outline: none;top:6px;right:9px;">&times;</button>'
        +"</div>"
        +"<div id='sprintdatecheckcontent' class='wsScrollBar' style='height:200px;overflow:auto;'>"
        for(var i=0;i<result.length;i++){
            checkcls = result[i].checked == "checked" ? "activesprintdate" : "inactivesprintdate";
            tdate=result[i].taskDate.replaceAll('/','-');

            ui+="<div id='sprintdate_"+result[i].checkId+"' checkid="+result[i].checkId+" class='"+checkcls+" p-2 d-flex align-items-center sprintdatecheckcontentEach' style='border-bottom: 1px solid #cccccc;'>"
                +"<div style='width:10%;' class='pt-1'><input onclick='changeSelectSprintdatecheckbox(this);event.stopPropagation();' id='inputSprintdatecheckbox_"+result[i].checkId+"' type='checkbox' "+result[i].checked+"></div>"
                +"<div id='sprintcalDates_"+result[i].checkId+"' class='defaultExceedCls' style='width:30%;'>"+tdate+"</div>"
                +"<div id='sprintcalDays_"+result[i].checkId+"' class='defaultExceedCls' style='width:30%;'>"+result[i].finalDay+"</div>"
                +"<div class='' style='width:30%;'><input id='sprintcalEsttimes_"+result[i].checkId+"' type=''class='w-100 border-0' style='border-bottom: 1px solid #c1c5c8 !important;outline:none;' value='"+result[i].estimated_hour+"' readonly></div>"
            +"</div>"
        }    
        ui+="</div>"
        +"<div class='p-2 d-flex align-items-center'>"
            +"<div class='d-flex align-items-center'>"
                +"<input type='radio' id='removeUnchecksprintdate' onclick='sprintDateRadioclick(this);event.stopPropagation();' class='' checked><span class='pl-1 pr-2'>"+getValues(companyLabels,"Remove_Unchecked_Dates")+"</span>"
                +"<input type='radio' id='showAllsprintdate' onclick='sprintDateRadioclick(this);event.stopPropagation();' class=''><span class='pl-1'>"+getValues(companyLabels,"Show_All_Dates")+"</span>"
            +"</div>"
            +'<div id="" onclick="generateSprintDatesjson(\'calendar\');event.stopPropagation();" style="" class="sprintCalSave ml-auto">OK</div>'
        +"</div>"

    +"</div>"
    
    return ui;

}

function changeSelectSprintdatecheckbox(obj){
    //var a = daysdifference();
    
    if($(obj).prop("checked")==false){
        $(obj).parent().parent().removeClass("activesprintdate").addClass("inactivesprintdate");
    }else{
        $(obj).parent().parent().removeClass("inactivesprintdate").addClass("activesprintdate");
    }

    if($('#removeUnchecksprintdate').prop("checked")==true){
        $(obj).parent().parent().addClass('d-none').removeClass('d-flex');
    }else{
        $(obj).parent().parent().addClass('d-flex').removeClass('d-none');
    }
}

function sprintDateRadioclick(obj){
	var objId = $(obj).attr('id');
	if(objId == 'showAllsprintdate'){
		$('#sprintdatecheckcontent div.inactivesprintdate').addClass('d-flex').removeClass('d-none');
        $('#removeUnchecksprintdate').prop("checked",false);
	}
    if(objId == 'removeUnchecksprintdate'){
		$('#sprintdatecheckcontent div.inactivesprintdate').addClass('d-none').removeClass('d-flex');
        $('#showAllsprintdate').prop("checked",false);
	}
}




function daysdifference(){
    var startDay = new Date($('#sprintstartDateCalNewUI').attr('formatdate').replaceAll("-","/"));
    var endDay = new Date($('#sprintendDateCalNewUI').attr('formatdate').replaceAll("-","/"));
   
    var millisBetween = startDay.getTime() - endDay.getTime();
    var days = millisBetween / (1000 * 3600 * 24);
   
    return Math.round(Math.abs(days));
}

function closeSprintdatepopup(){
    $('.openSprintselectDatepopupcls').toggle();
}

function changeImgSrc(place){
    if(place=='instruction'){
        if($("#sprintInsTextarea").val().trim()!=""){
          $("#sprintinstruction").attr('src','/images/task/intr.svg')
        }else{
          $("#sprintinstruction").attr('src','/images/task/taskInstructions_cir.svg')
        }
    }else if(place=='story'){
    
        let count=0;
        $('#storysprintcontentdiv > div').each(function () { 
            if($(this).find('.check').prop("checked")==true){
                count++;
            }
        });
        if(count>0){
            $("#sprintAddstory").attr('src','images/agile/storyiconfilled.svg')   //change img here
        }else{
            $("#sprintAddstory").attr('src','images/agile/story.svg')
        }
      }
}

function insertCreateSprint(){
    var sprintname=$('#sprintname1').val().trim();
    var instruction=$('#sprintInsTextarea').val().trim();
    var sprintgroupid=$('#sprintgroupchange').attr('groupid');
    var sprintstartdate=$('#sprintstartDateCalNewUI').attr('formatdate');
    var sprintenddate=$('#sprintendDateCalNewUI').attr('formatdate');

    if(typeof(sprintname)=="undefined" || sprintname == ""){
        alertFunNew("Sprint name cannot be Empty",'error');
        return false;
    }else{
        var storyids = "";
        $('#storysprintcontentdiv > div').each(function () { 
            if($(this).find('.check').prop("checked")==true){
                var stid1 = $(this).attr('id').split("_")[1]; 
                storyids=stid1+","+storyids;
            }
        });
        storyids=storyids.substring(0,storyids.length-1);
        console.log("storyids--->"+storyids);
        $('#loadingBar').addClass('d-flex').removeClass('d-none');
        let jsonbody = {
            "storyTasks": sprintname,
            "comments": instruction,
            "project_id": prjid,
            "sprint_group_id": sprintgroupid,
            "sprintStartDate": sprintstartdate,
            "sprintEndDate": sprintenddate,
            "user_id": userIdglb,
            "groupLoadType": "active",
            "selectedStories": storyids,
            "sprintId" : "",
            "dateList" : JSON.parse(calSprintDateSelectedJSON)
        }
        $.ajax({
            url: apiPath + "/" + myk + "/v1/createSprintTask",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            beforeSend: function (jqXHR, settings) {
            xhrPool.push(jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            
            },
            success: function (result) { 
                if(sprintviewtype=="grid"){
                    $('#sprintContentGridListDiv').prepend(listGridSprintUI(result));
                    $('#sprintContentGridListDiv').show();
                }else if(sprintviewtype=="list"){
                    $('#sprintContentListDiv').prepend(listSprintUI(result));
                    $('#sprintContentListDiv').show();
                }else{
                    $('.calendar-1').html('');
                    listCalendarSprint();
                }
                if($('.calendar-1').children().length != 0){
                    $('.calendar-1').show();
                }
                
                $('#SprintHolderDiv').remove();
                
                calSprintDateSelectedJSON="[]";
                sprintcheckflag=false;
                $('#SprintDivContentMain').show();
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
            }
        });       
    }
     
}

function cancelsprintcreate(){
    //alert("2-->"+sprintcheckflag)
    if(sprintcheckflag==true){
        var plac = $('#sprintPlaceHidden').val();
        var clkfunc = plac=="create"?"insertCreateSprint":"updatesprint";
        confirmResetNew(getValues(companyAlerts,"Alert_Save"),'clear',clkfunc,"cancelsprintcreatedone",'OK','CANCEL');
    }else{
        cancelsprintcreatedone();
    }
    
}
function cancelsprintcreatedone(){
    if($('.calendar-1').children().length != 0){
        $('.calendar-1').show();
    }else{
        $('#SprintDivContentMain').show();
    }
    $('#SprintHolderDiv').remove();
    calSprintDateSelectedJSON="[]";
    sprintcheckflag=false;
}

function listSprintgroups(fromplace){
    
    fromplace == typeof(fromplace)=="undefined"||fromplace=="undefined"?"":fromplace;  
    $.ajax({
      url: apiPath + "/" + myk + "/v1/fetchSprintGroupData?projId="+prjid+"&sortVal&groupLoadType=active&groupid",
      type: "POST",
      contentType: "application/json",
      beforeSend: function (jqXHR, settings) {
        xhrPool.push(jqXHR);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        
      },
      success: function (result) { 
          //alert("6->"+fromplace);
        if(fromplace=="SBsettings"){
            $('#SBsettingGrouplist').append(showSprintgrouppopup(result,fromplace));
            $('#SBsettingGrouplist').find('#sprintGroupPopupDiv').css('left','1px');
        }else if(fromplace=="SBsettingsOpen"){
            //alert("3-->"+sprintcheckflag)
            if(sprintcheckflag==true){
                var plac = $('#sprintPlaceHidden').val();
                var clkfunc = plac=="create"?"insertCreateSprint":"updatesprint";
                confirmResetNew(getValues(companyAlerts,"Alert_Save"),'clear',clkfunc,"cancelsprintcreatedone",'OK','CANCEL');
                var sGroupid=result[0].sprint_group_id;
                openSBsettings(sGroupid);
            }else{
                var sGroupid=result[0].sprint_group_id;
                openSBsettings(sGroupid);
            }
            
        }else if(fromplace=="sprintcreate"){
            var sGroupid=result[0].sprint_group_id;
            var ccode=result[0].color_code;
            var sgname=result[0].sprint_group_name;
            createSprint('create',sGroupid,ccode,sgname); 
            
        }else{
            //alert("4->"+sprintcheckflag)
            /* if(sprintcheckflag==true){
                var plac = $('#sprintPlaceHidden').val();
                var clkfunc = plac=="create"?"insertCreateSprint":"updatesprint";
                confirmResetNew(getValues(companyAlerts,"Alert_Save"),'clear',clkfunc,"cancelsprintcreatedone",'OK','CANCEL');
            }else{ */
                $('#sprintgroupdiv').append(showSprintgrouppopup(result,fromplace));
            //}
            
        }
        
      
      }
    });
    hideSprintChart();

}

function showStorieslist(place,sprintid,placetype,clickid){

    if($('#sprintStorylistUIMainDiv').length == 0){
        $('.storyshowoption').hide();
        let jsonbody="";
        if(place=="create"){
            jsonbody={
                "project_id":prjid,
                "filterStoryIds":"",
                "deletedStoryId":"",
                "sprintId":"",
                "filterType":"Story"
            }
        }else{
            jsonbody={
                "project_id":prjid,
                "filterStoryIds":"",
                "sprintId":sprintid,
                "deletedStoryId":"",
                "filterType":"Story"
            }
        }
        
        $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/loadStoryForProject",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) { 
                
                if(placetype=="editgrid"){
                    $("#transparentDiv").show();
                    $('#sprintpopups').append(preparesprintStorylistUI(result,place,placetype,sprintid)).show();
                }else{
                    $('#sprintStorylistUIMainDiv').remove();
                    $('#sprintStorylistDiv').append(preparesprintStorylistUI(result,place,"",sprintid)).show();
    
                    if(placetype === "fromSprintCreate"){
                        var elmnt = document.getElementById("storyList_"+clickid);
                        elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
                        $('#storyList_'+clickid).find('input#storylistckeckbox_'+clickid).trigger('click');
                    }
                }
                
                
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            
            }
        }); 
    }else{
        $('#sprintStorylistDiv').toggle();
    }
    
    


}

function preparesprintStorylistUI(result,place,placetype,sprintid){
    var ui="";
    var cls1="";var cls2="";var cls3="";var h1="";

    cls1 = placetype=="editgrid"?"gridExistingCls mx-auto w-75":"";
    cls2 = placetype=="editgrid"?"modal-content":"";
    cls3 = placetype!="editgrid"?"mt-1":"";
    h1 = placetype=="editgrid"?"400px":"200px";
    ui="<div id='sprintStorylistUIMainDiv' class='px-3 py-2 "+cls1+"' style='top:130px !important;'>"  //1

        +"<div id='sprintStorylistInnerDiv_"+sprintid+"' class='"+cls2+"'>"   //2
            if(placetype!="editgrid"){
                ui+="<div id='sprintStorylistTitleDiv' class='d-flex align-items-center px-1' style='border-bottom: 1px solid darkgray;font-size:12px;'>"
                    +"<div>Story</div>"
                    +"<img src='images/task/dowarrow.svg' onclick='hidesprintStorylistUIMainDiv();event.stopPropagation();' class='ml-1' style='width:15px;height:15px;cursor:pointer;'>"
                +"</div>" 
            }
            

            ui+="<div class='d-flex py-1 pr-1 storysprintheaderdiv w-100 "+cls3+"'>"
                +"<div class='defaultExceedCls' style='width:6%;'></div>"
                +"<div class='defaultExceedCls' style='width:6%;'></div>"
                +"<div class='defaultExceedCls' style='width:6%;'>Id</div>"
                +"<div class='defaultExceedCls' style='width:58%;'>Story</div>"
                +"<div class='defaultExceedCls' style='width:8%;'>Points</div>"
                +"<div class='defaultExceedCls' style='width:8%;'>Priority</div>"
                +"<div class='defaultExceedCls' style='width:8%;'>Status</div>"
                if(placetype=="editgrid"){
                    ui+='<button type="button" onclick="closeExistStory();event.stopPropagation();" class="close p-0" style="top: 0px;right: 6px;outline: none;">&times;</button>'  
                }
            ui+="</div>"

            +"<div id='storysprintcontentdiv' class='w-100 wsScrollBar' style='height:"+h1+";overflow:auto;'>"
                ui+=preparesprintStorylistUILoop(result,place)
            ui+="</div>"

            if(placetype=="editgrid"){
                ui+="<div class='modal-footer p-1 border-0'>"
                    +"<div id='' onclick=\"adStorytoSprint("+sprintid+",'');event.stopPropagation();\" class='createBtn' style='width: 70px;float: right;'>SAVE</div>"
                +"</div>"
            }    

        +"</div>"   //2

    +"</div>"   //1

    return ui;
}

function closeExistStory(){
    $("#transparentDiv").hide();
    $('#sprintpopups').html('').hide();
}

function hidesprintStorylistUIMainDiv(){
    $('#sprintStorylistDiv').toggle();
}

function preparesprintStorylistUILoop(result,place){
    var ui="";
    var priority="";var pimage ="";var status="";var statimage="";var check="";var orgImgSrc="";var orgImgSrcTitle="";var newaddcls="";var checkinput="";
    if(result!=""){
        for(var i=0;i<result.length;i++){
            priority=result[i].epic_priority;
            pimage = (priority == "")? "images/task/p-six.svg" : priority == "1" ? "images/task/p-one.svg" : priority == "2" ? "images/task/p-two.svg":priority == "3" ? "images/task/p-three.svg" :priority == "4" ? "images/task/p-four.svg":priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
            status = result[i].epic_status;
            statimage = status=="Backlog"?"/images/idea_old/Backlog.png":status=="Blocked"?"/images/idea_old/Blocked.png":status=="Hold"?"/images/idea_old/Hold.png":status=="Done"?"/images/idea_old/Done.png":status=="Cancel"?"/images/idea_old/cancel.png":status=="In Progress"?"/images/idea_old/Assigned.png":"";
            check=result[i].story_id;
            console.log("result[i].parent_epic_id:"+result[i].parent_epic_id);
            orgImgSrc = (typeof(result[i].parent_epic_id) =="undefined" || result[i].parent_epic_id =="0" || result[i].parent_epic_id =="") ? "/images/agile/unorg_blue.svg" : "/images/agile/org_blue.svg";
            orgImgSrcTitle = (typeof(result[i].parent_epic_id) =="undefined" || result[i].parent_epic_id =="0" || result[i].parent_epic_id =="") ? "Unorganized" : "Organized";
            //checkinput = place=="edit"?"checkUpdateStorysel();":"";

            check = typeof(check)=="undefined"||check==""?"":"checked";
            ui+="<div id='storyList_"+result[i].epic_id+"' class='d-flex align-items-center py-2' style='border-bottom: 1px solid darkgray;font-size:12px;z-index: 1;'>"
                +"<div class='defaultExceedCls' style='width:6%;text-align:center;'><input type='checkbox' id='storylistckeckbox_"+result[i].epic_id+"' class='check' place=\""+place+"\" uncheckvalue='' onclick=\"changeImgSrc('story');changevalueinEdit(this,'"+place+"',"+result[i].epic_id+");checkUpdateStorysel('"+place+"');event.stopPropagation();\" style='cursor:pointer;' "+check+"></div>"
                +"<div class='defaultExceedCls' style='width:6%;text-align:center;'><img src='"+orgImgSrc+"' title='"+orgImgSrcTitle+"' class='image-fluid' style='height: 16px;'></div>"
                +"<div class='defaultExceedCls' style='width:6%;'>"+result[i].unique_cust_id+"</div>"
                +"<div class='defaultExceedCls' style='width:58%;'>"+result[i].epic_title+"</div>"
                +"<div class='defaultExceedCls pl-3' style='width:8%;'>"+result[i].epic_point+"</div>"
                +"<div class='defaultExceedCls pl-3' style='width:8%;' title='"+result[i].epic_priority+"'><img src='"+pimage+"' style='width:20px;height:20px;'></div>"
                +"<div class='defaultExceedCls pl-3' style='width:8%;' title='"+status+"'><img id='storystatimg_"+result[i].epic_id+"' src='"+statimage+"' style='width:20px;height:20px;'></div>"
            +"</div>"
    
        }  
    }else{
        ui="<div style='text-align:center;font-size:12px;'>No Stories Found</div>"
    }
      

    return ui;
}

function changevalueinEdit(obj,place,epicid){

    if(place=="edit" && $(obj).prop("checked")==false){
        $(obj).attr('uncheckvalue','y');
        $('#storyList_'+epicid).removeClass('newadded');
    }else if(place=="edit" && $(obj).prop("checked")==true){
        $(obj).attr('uncheckvalue','');
        $('#storyList_'+epicid).addClass('newadded');
    }

    if($(obj).prop("checked")==true){
        $('#storystatimg_'+epicid).attr('src','/images/idea_old/Assigned.png').attr('title','In Progress');
    }else{
        $('#storystatimg_'+epicid).attr('src','/images/idea_old/Backlog.png').attr('title','Backlog');
    }
    
}

function showSprintInstruction(){
    $(".instruction").toggle();
}

function showSprintgrouppopup(result,fromplace){
    var ui="";
    $('#sprintGroupPopupDiv').remove();

    ui="<div id='sprintGroupPopupDiv' class='position-absolute sprintGroupPopupDivcls'>"
        +"<div id='divForSprintgroup'>"
            +"<div id='addsprintGroupDiv' class='d-flex align-items-center pt-1' onclick=\"prepareAddsprintGroupUI('"+fromplace+"');event.stopPropagation();\">"
                +"<img src='images/task/plus.svg' style='width:15px;height:15px;'>"
                +"<span class='pl-2' style='font-size: 12px;margin-top: 1px;font-weight: 700;color: #5e5b5b;margin-bottom: 2px;'>Add new sprint group</span>"
            +"</div>"
            +"<div id='sprintgroupListingDiv'>"
               ui+=showSprintgrouppopupLoop(result,fromplace) 
            ui+="</div>"
        +"</div>"
    
    +"</div>"


    return ui;
}

function showSprintgrouppopupLoop(result,fromplace){
    var ui="";var color="";
    for(var i=0;i<result.length;i++){
        color = result[i].color_code;
        color = color==""?"#f2788f":color;
        ui+="<div id='sprintgroupID_"+result[i].sprint_group_id+"' class='d-flex align-items-center py-1 position-relative w-100' onmouseover='showsprintgroupeditoption(this);' onmouseout='hidesprintgroupeditoption(this);'>"
            +"<div class='sprintgroup rounded-circle' style='width:15px;height:15px;background:"+color+";'></div>"
            +"<div class='defaultExceedCls pl-2' title='' onclick=\"changeSprintgroup(this,'"+fromplace+"');event.stopPropagation();\" groupid=\""+result[i].sprint_group_id+"\" groupcode=\""+color+"\" style='width:94%;'>"+result[i].sprint_group_name+"</div>"
            +"<div id='sprintgroupeditoption' class='position-absolute actFeedOptionsDiv d-none' style='border-radius: 8px;z-index:10;'>"
                +"<img src='images/conversation/edit.svg' onclick='editSprintgroups("+result[i].sprint_group_id+");event.stopPropagation();' class='mx-1' title='Edit' style='width:18px;height:18px;'>"
                +"<img src='images/task/minus.svg' class='mx-1' onclick='deleteSprintgroups();event.stopPropagation();' title='Delete' style='width:18px;height:18px;'>"
            +"</div>"
        +"</div>"

    } 

    return ui;
}

function editSprintgroups(groupid){
    $.ajax({
        url: apiPath + "/" + myk + "/v1/fetchSprintGroupData?projId="+prjid+"&sortVal&groupLoadType=active&groupid="+groupid+"",
        type: "POST",
        contentType: "application/json",
        beforeSend: function (jqXHR, settings) {
          xhrPool.push(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
          
        },
        success: function (result) { 
            
            prepareAddsprintGroupUI();
            $('textarea#sprintGrouptextarea').val(result[0].sprint_group_name);
            $('input#sprintgroupcolor1').val(result[0].color_code);
            $('span.minicolors-swatch-color').css('background-color',result[0].color_code);
            $('#sprintgroupsave').attr('onclick','updateSprintgroup('+groupid+');event.stopPropagation();')
        }
    });
}

function updateSprintgroup(groupid){
    var groupname = $('#sprintGrouptextarea').val().trim();
    var color = $('#sprintgroupcolor1').val();
    if(typeof(groupname) == "undefined" || groupname == ""){
        alertFunNew("Group name cannot be Empty",'error');
        return false;
    }else{
        let jsonbody={
            "groupName": groupname,
            "project_id": prjid,
            "sprint_group_id": groupid,
            "groupLoadType":"active",
            "user_id": userIdglb,
            "company_id": companyIdglb,
            "color_code": color
        }
    
        //$('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/createSprintGroup",
            type: "PUT",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                var color = result[0].color_code==""?"#f2788f":result[0].color_code;
                $('#sprintgroupID_'+groupid).replaceWith(showSprintgrouppopupLoop(result));
                $('#addSprintgroupuiDiv').remove();
                $("#sprintGroupPopupDiv").css('height','143px'); 
                $('#divForSprintgroup').show();
                $("#sprintgroupchange").css('background',color).attr('groupcode',color).attr('title',result[0].sprint_group_name);
                $("#sprintgroupchange").attr('groupid',result[0].sprint_group_id);
                $('#sprintgroupnamechange').text(result[0].sprint_group_name);
                $("#changeGroup").attr('title',result[0].sprint_group_name);
                $(".sprintGroupPopupDivcls").remove();
                //$('#loadingBar').addClass('d-none').removeClass('d-flex');
            }
            
          });
    }
}

function changeSprintgroup(obj,fromplace){
    var group_color=$(obj).attr('groupcode');
    var gname = $(obj).text();
    var groupid=$(obj).attr('groupid');
    if(fromplace == "SBsettings"){
        //glbSbsettingsSgroupid=groupid;
        openSBsettings(groupid);
    }else{
        $("#sprintgroupchange").css('background',group_color).attr('groupcode',group_color);
        $("#sprintgroupchange").attr('groupid',$(obj).attr('groupid')).attr('title',gname);
        $('#sprintgroupnamechange').text(gname);
        $("#changeGroup").attr('title',$(obj).attr('groupname'));
        $('#sprintGroupPopupDiv').remove();
        sprintcheckflag=true;
    }
    
}

function showsprintgroupeditoption(obj){
    $(obj).find('#sprintgroupeditoption').addClass('d-block').removeClass('d-none');
}
function hidesprintgroupeditoption(obj){
    $(obj).find('#sprintgroupeditoption').addClass('d-none').removeClass('d-block');
    $('#sprintgroupeditoption').addClass('d-none').removeClass('d-block');
}

function showSprintgrouppopupui(){

}

function prepareAddsprintGroupUI(fromplace){
    var ui="";

    ui="<div id='addSprintgroupuiDiv' class='addSprintgroupuiDivcls'>"
        +"<div class='material-textfield mt-1'>"
            +"<textarea id='sprintGrouptextarea' type='text' placeholder=' ' onclick='event.stopPropagation();' class='theightdemo material-textfield-input'></textarea>"
            +"<label class='material-textfield-label' Style='font-size: 12px;top:18px;margin-left:8px'>Sprint Group Name</label>"
        +"</div>"

        +'<div class="d-flex justify-content-between ">'
            +'<input type="text" id="sprintgroupcolor1"  data-inline="true" value="" onclick="" style="border-radius: 6px;resize:none;outline:none;border:1px solid #C1C5C8;text-transform:uppercase;width:85px "/>'
            +'<div class="d-flex justify-content-end mt-1">'
                +'<img id="sprintgroupcancel" title="Cancel" class="img-responsive " src="/images/task/remove.svg" style="float:left;height:25px;width:25px;margin-right: 2px;cursor:pointer;" onclick="cancelSprintgroup();event.stopPropagation();">'
                +'<img id="sprintgroupsave" title="Save" class="img-responsive " src="/images/task/tick.svg" style="float:left;height:25px;width:25px;margin-right: 2px;cursor:pointer;" onclick="createSprintgroup();event.stopPropagation();" >'
            +'</div>'
        +'</div>'

        +'<div id="colorBox" class="colorGrid pt-2"></div>'
        +'<div id="wantMoresprintcolor" class="wantMoreColor d-flex justify-content-center pt-2">'
            +'<button type="button" id="showMoresprintcolor" onclick="showMoresprintColor();event.stopPropagation();" class="btn btn-sm rounded">Show Colors</button>'
        +'</div>'

    +"</div>"

    $('#sprintGroupPopupDiv').prepend(ui);
    $('#divForSprintgroup').hide();

    $("#sprintgroupcolor1").minicolors({
     
     
        control: 'hue',
      
        format: 'hex',
        hide: function(){
          $(".minicolors-panel").show();
        },
        change:function(){
        },
        inline: false,
       
        theme: 'default',
        swatches: []
        
      });
      $("#sprintgroupcolor1").removeClass("minicolors-input").addClass("minicolors-inputnew");
      colorGridView('','sprintplace');
  
      $(".minicolors-swatch-color").css("background-color", "#f39a4e");
      $("#sprintgroupcolor1").val("#f39a4e");
     
      $("#sprintGroupPopupDiv").css('height','258px'); 
      setTimeout(function(){ 
        $(".minicolors-panel").hide();
      },300);
        
    
}

function createSprintgroup(){
    
    var groupname = $('#sprintGrouptextarea').val().trim();
    var color = $('#sprintgroupcolor1').val();
    if(typeof(groupname) == "undefined" || groupname == ""){
        alertFunNew("Group name cannot be Empty",'error');
        return false;
    }else{
        let jsonbody={
            "groupName": groupname,
            "project_id": prjid,
            "sprint_group_id": "",
            "groupLoadType":"active",
            "user_id": userIdglb,
            "company_id": companyIdglb,
            "color_code": color
        }
        //$(".sprintGroupPopupDivcls").remove();
        //$('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/createSprintGroup",
            type: "PUT",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
             
                $('#sprintgroupListingDiv').prepend(showSprintgrouppopupLoop(result));
                $('#addSprintgroupuiDiv').remove();
                $("#sprintGroupPopupDiv").css('height','143px'); 
                $('#divForSprintgroup').show();
                //$('#loadingBar').addClass('d-none').removeClass('d-flex');
            }
            
          });
    }
    
}

function editGroup(group_id,place){
    var group_name=$("#group_"+group_id).attr('groupname');
    var group_code=$("#group_"+group_id).attr('groupcode');
    prepareAddGroupUI(group_code,place);
    
    $("#group_create_name").val(group_name);
    $("#group_save").attr('onclick','updateGroup('+group_id+',\''+place+'\')');
  
  }

function cancelSprintgroup(){
    $('#addSprintgroupuiDiv').remove();
    $("#sprintGroupPopupDiv").css('height','143px'); 
    $('#divForSprintgroup').show();
    
}

function changesprintgroupcolor(obj){
    var color=$(obj).attr('nodecolor');
    $('#sprintgroupcolor1').val(color);
    $(".minicolors-swatch-color").css("background-color", "none");
    $(".minicolors-swatch-color").css("background-color", color);
}

function showMoresprintColor(){
    $("#colorBox").hide();
    $("#wantMoresprintcolor").hide();
    $("#showMoresprintcolor").hide();
    $("#sprintgroupcolor1").addClass("minicolors-input");
    $("#sprintgroupcolor1").removeClass("minicolors-inputnew");
    $(".minicolors-panel").show();
}


function calenderForsprint() {
 
    myCalendar = new dhtmlXCalendarObject("calender_forsprint");
    var today = new Date();
    myCalendar.setSensitiveRange(today, null);
    myCalendar.setDateFormat("%Y-%m-%d");
    myCalendar.hideTime();
    $('.dhtmlxcalendar_material').show();
    $('.dhtmlxcalendar_material').css({'z-index':'9000','border-radius':'5px','background-color':'#FFFFFF'});
    myCalendar.attachEvent("onClick",function(date){
        sprintchangeDatainReal('calender');
    });
   
}

function increaseValuesprint(type){
    let hour="";
    let minute="";
    let second="";
    if(type=='hour'){
         hour=(parseInt($("#hour").text())+1)>12?"1":(parseInt($("#hour").text())+1);
         $("#hour").text(parseInt(hour)<10?"0"+hour:hour);
         minute=parseInt($("#minute").text())
         second=parseInt($("#second").text())
    }else if(type=="minute"){
         hour=parseInt($("#hour").text());
         minute=parseInt($("#minute").text())+1>59?"1":parseInt($("#minute").text())+1;
         $("#minute").text(parseInt(minute)<10?"0"+minute:minute);
         second=parseInt($("#second").text());
    }else{
         hour=parseInt($("#hour").text());
         minute=parseInt($("#minute").text())
         second=parseInt($("#second").text())+1>59?"1":parseInt($("#second").text())+1;
         $("#second").text(parseInt(second)<10?"0"+second:second);
    }
    let selectedOne=$(".activestartend").attr('id');
    
    let fullTime=(parseInt(hour)<10?"0"+hour:hour)+" :" +(parseInt(minute)<10?"0"+minute:minute)+" "+($(".ampm").text());
    if(selectedOne=='calenderForTask'){
      $("#sprintstartTimeCalNewUI").text(fullTime);
    }else{
      $("#sprintendTimeCalNewUI").text(fullTime);
    }

}
function decreaseValuesprint(type){
    if(type=='hour'){
      hour=(parseInt($("#hour").text())-1)<1?"12":(parseInt($("#hour").text())-1);
      $("#hour").text(parseInt(hour)<10?"0"+hour:hour);
      minute=parseInt($("#minute").text())
      second=parseInt($("#second").text())
    }else if(type=="minute"){
        hour=parseInt($("#hour").text());
        minute=parseInt($("#minute").text())-1<0?"59":parseInt($("#minute").text())-1;
        $("#minute").text(parseInt(minute)<10?"0"+minute:minute);
        second=parseInt($("#second").text());
    }else{
        hour=parseInt($("#hour").text());
        minute=parseInt($("#minute").text())
        second=parseInt($("#second").text())-1<0?"59":parseInt($("#second").text())-1;
        $("#second").text(parseInt(second)<10?"0"+second:second);
    }
    let selectedOne=$(".activestartend").attr('id');
    
    let fullTime=(parseInt(hour)<10?"0"+hour:hour)+" :" +(parseInt(minute)<10?"0"+minute:minute)+" "+($(".ampm").text());
    if(selectedOne=='calenderForTask'){
      $("#sprintstartTimeCalNewUI").text(fullTime);
    }else{
      $("#sprintendTimeCalNewUI").text(fullTime);
    }
}
function changeAmPmsprint(obj){
  let ampm="";
  
  if($(obj).attr('class').includes('amactive')){
    $(obj).removeClass("amactive");
    $(obj).addClass("pmactive");
    $(obj).find(".ampm").text("PM");
    ampm="PM"
    $('.colorDiv').css('background-color','#FFF');
  }else{
    $(obj).removeClass("pmactive");
    $(obj).addClass("amactive");
    $(obj).find(".ampm").text("AM");
    ampm="AM"
    $('.colorDiv').css('background-color','rgb(111, 158, 176)');
  }
  let selectedOne=$(".selected-deadline").attr('id');
  let hour=$("#hour").text();
  let minute=$("#minute").text();
  let second=$("#second").text();
  let fullTime=(parseInt(hour)<10?"0"+parseInt(hour):hour)+" :" +(parseInt(minute)<10?"0"+parseInt(hour):minute)+" : "+(parseInt(second)<10?"0"+parseInt(hour):second)+" "+(ampm);
  if(selectedOne.includes('start')==true){
    $("#sprintstartTimeCalNewUI").text(fullTime);
    
  }else{
    $("#sprintendTimeCalNewUI").text(fullTime);
  }

}

function showCalendersprint(obj){
  
    let id=$(obj).attr('id');
    $(".time_calendersprint").toggle();
  
    $(".activestartend").removeClass("activestartend");
    $(obj).addClass('activestartend');
    
    /* if(id=="calenderForsprintstart"){
      let fullhour=$("#sprintstartTimeCalNewUI").text().substring(0,$("#sprintstartTimeCalNewUI").text().length-2);
      $("#hour").text(fullhour.split(":")[0].trim());
      $("#minute").text(fullhour.split(":")[1].trim());
      $("#second").text("00");
      $(".mainampm").addClass($("#sprintstartTimeCalNewUI").text().substring($("#sprintstartTimeCalNewUI").text().length-2,$("#sprintstartTimeCalNewUI").text().length).toLowerCase()+"active");
      if($("#sprintstartTimeCalNewUI").text().substring($("#sprintstartTimeCalNewUI").text().length-2,$("#sprintstartTimeCalNewUI").text().length).toLowerCase()=="pm"){
        $(".ampm").text("PM");
        $('.colorDiv').css('background-color','#FFF');
      }else{
        $(".ampm").text("AM");
        $('.colorDiv').css('background-color','rgb(111, 158, 176)');
      }
    }else{
      let fullhour=$("#sprintendTimeCalNewUI").text().substring(0,$("#sprintendTimeCalNewUI").text().length-2);
      $("#hour").text(fullhour.split(":")[0].trim());
      $("#minute").text(fullhour.split(":")[1].trim());
      $("#second").text("00");
      $(".mainampm").addClass($("#sprintendTimeCalNewUI").text().substring($("#sprintendTimeCalNewUI").text().length-2,$("#sprintendTimeCalNewUI").text().length).toLowerCase()+"active");
      if($("#sprintendTimeCalNewUI").text().substring($("#sprintendTimeCalNewUI").text().length-2,$("#sprintendTimeCalNewUI").text().length).toLowerCase()=="pm"){
        $(".ampm").text("PM");
        $('.colorDiv').css('background-color','#FFF');
      }else{
        $(".ampm").text("AM");
        $('.colorDiv').css('background-color','rgb(111, 158, 176)');
      }
    } */
   
  
  }

  function sprintchangeDatainReal(type){
    let selectedOne=$(".activestartend").attr('id');
    var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let changeDate=myCalendar.getFormatedDate();
    //alert(changeDate);
    changeDate=monthNames[parseInt(changeDate.split("-")[1])]	+" "+changeDate.split("-")[2]+", "+changeDate.split("-")[0]	;	       
    if(type=='calender'){
      if(selectedOne=='calenderForsprintstart'){
        $("#sprintstartDateCalNewUI").text(changeDate);
        $("#sprintstartDateCalNewUI").attr('parmanentvalue',changeDate)
        let startDate=$("#sprintstartDateCalNewUI").text();
        var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        let startdateMonth=monthNames.indexOf(startDate.substring(0,3))<10?"0"+monthNames.indexOf(startDate.substring(0,3)):monthNames.indexOf(startDate.substring(0,3));
        let startdateDate=startDate.substring(4,6)
        let start=startdateMonth.toString().trim()+"-"+startdateDate.trim()+"-"+startDate.split(",")[1].trim();
  
        let endDate=$("#sprintendDateCalNewUI").text();
        let enddateMonth=monthNames.indexOf(endDate.substring(0,3))<10?"0"+monthNames.indexOf(endDate.substring(0,3)):monthNames.indexOf(endDate.substring(0,3));
        let enddateDate=endDate.substring(4,6)
        let end=enddateMonth.toString().trim()+"-"+enddateDate.trim()+"-"+endDate.split(",")[1].trim();
  
        let formatdate = myCalendar.getFormatedDate("%m-%d-%Y");
        $("#sprintstartDateCalNewUI").attr('formatdate',formatdate);

        /* let startfullTime=$("#startTimeCalNewUI").text();
        let hour=startfullTime.substring(startfullTime.length-2,startfullTime.length)=="PM"?(parseInt(startfullTime.split(":")[0].trim())+12):startfullTime.split(":")[0].trim();
        if(parseInt(hour)>23){
          hour="12";
        }
        let startTime=hour+":"+startfullTime.split(":")[1].trim().substring(0,2);
        
  
        let endfullTime=$("#endTimeCalNewUI").text();
        let endhour=endfullTime.substring(endfullTime.length-2,endfullTime.length)=="PM"?(parseInt(endfullTime.split(":")[0].trim())+12):endfullTime.split(":")[0].trim();
        if(parseInt(endhour)>23){
          endhour="12";
        }
  
        let endTime=endhour+":"+endfullTime.split(":")[1].trim().substring(0,2); */
  
        let startDateto=start;
        let startDatecompare=new Date(startDateto);
        let endDatecompare=new Date(end);
        
        if(startDatecompare>endDatecompare){
          $("#sprintendDateCalNewUI").text(changeDate);
          $("#sprintendDateCalNewUI").attr('parmanentvalue',changeDate)
          $("#sprintendDateCalNewUI").attr('formatdate',myCalendar.getFormatedDate("%m-%d-%Y", changeDate));
  
        }
  
      }else{
        $("#sprintendDateCalNewUI").text(changeDate);
        saveAsTemporaryDatasprint();
      }
    }
   
  
  }

  function saveAsTemporaryDatasprint(){
 
    $(".activestartend").removeClass("activestartend");
  
    let startDate=$("#sprintstartDateCalNewUI").text();
  
    var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let startdateMonth=monthNames.indexOf(startDate.substring(0,3))<10?"0"+monthNames.indexOf(startDate.substring(0,3)):monthNames.indexOf(startDate.substring(0,3));
    let startdateDate=startDate.substring(4,6)
    let start=startdateMonth.toString().trim()+"-"+startdateDate.trim()+"-"+startDate.split(",")[1].trim();
  
    let endDate=$("#sprintendDateCalNewUI").text();
    let enddateMonth=monthNames.indexOf(endDate.substring(0,3))<10?"0"+monthNames.indexOf(endDate.substring(0,3)):monthNames.indexOf(endDate.substring(0,3));
    let enddateDate=endDate.substring(4,6)
    let end=enddateMonth.toString().trim()+"-"+enddateDate.trim()+"-"+endDate.split(",")[1].trim();
  
  
    let formatdate = myCalendar.getFormatedDate("%m-%d-%Y");
    $("#sprintendDateCalNewUI").attr('formatdate',formatdate);

    /* let startfullTime=$("#startTimeCalNewUI").text();
    let hour=startfullTime.substring(startfullTime.length-2,startfullTime.length)=="PM"?(parseInt(startfullTime.split(":")[0].trim())+12):startfullTime.split(":")[0].trim();
    if(parseInt(hour)>23){
      hour="12";
    }
    let startTime=hour+":"+startfullTime.split(":")[1].trim().substring(0,2);
    
  
    let endfullTime=$("#endTimeCalNewUI").text();
    let endhour=endfullTime.substring(endfullTime.length-2,endfullTime.length)=="PM"?(parseInt(endfullTime.split(":")[0].trim())+12):endfullTime.split(":")[0].trim();
    if(parseInt(endhour)>23){
      endhour="12";
    }
    let endTime=endhour+":"+endfullTime.split(":")[1].trim().substring(0,2); */
  
    let startDatecompare=new Date(start);
    let endDatecompare=new Date(end);
    //alert(start+"----"+startTime);
    if(startDatecompare>endDatecompare){
      alertFunNew("Due Date cannot be earlier than Start Date",'error');
      //$("#startTimeCalNewUI").text($("#startTimeCalNewUI").attr('parmanentvalue'));
      $("#sprintstartDateCalNewUI").text($("#sprintstartDateCalNewUI").attr('parmanentvalue'));
  
      //$("#endTimeCalNewUI").text($("#endTimeCalNewUI").attr('parmanentvalue'));
      $("#sprintendDateCalNewUI").text($("#sprintendDateCalNewUI").attr('parmanentvalue'));

      $("#sprintstartDateCalNewUI").attr('formatdate',myCalendar.getFormatedDate("%m-%d-%Y", $("#sprintstartDateCalNewUI").attr('parmanentvalue')));
      $("#sprintendDateCalNewUI").attr('formatdate',myCalendar.getFormatedDate("%m-%d-%Y", $("#sprintendDateCalNewUI").attr('parmanentvalue')));
  
    }else{
      
      //$("#startTimeCalNewUI").attr('parmanentvalue', $("#startTimeCalNewUI").text());
      $("#sprintendDateCalNewUI").attr('parmanentvalue', $("#sprintendDateCalNewUI").text())
      
    }
  
  
  
  }

  function returnFormatedDatesprint(date){
    var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      let changeDate=date;
    changeDate=monthNames[parseInt(changeDate.split("-")[1])]	+" "+changeDate.split("-")[2]+", "+changeDate.split("-")[0]	;		  
    return changeDate;
  }

var glbSbsettingsSgroupid="";
function openSBsettings(sGroupId){
    
    let jsonbody = {
        "sprint_group_id" : sGroupId,
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    $.ajax({
        url: apiPath + "/" + myk + "/v1/fetchGroupStagesNew",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          
        },
        success: function (result) {
            if($(".elem-CalenStyle").length!=0){
                $(".elem-CalenStyle").hide();
            }
            $('#SBsettingsMaindiv').remove();
            $('#SprintHolderDiv').remove();
            $('#SBsettingsDiv').html(openSBsettingsUI(result,"SBsettingsresult"));
            
            var h1=$('#content').height();
            var h2=$('#SBsettingsContent1').height();
            var h3=h1-h2-18;
            $('#SBsettingsStageslist').css('height',h3); 
            initdraganddropStages("stagesettings");
            hideGlboption();
            $('#loadingBar').addClass('d-none').removeClass('d-flex');

        }
    });
}

function openSBsettingsUI(result,SBplace){
    var ui="";
    $('#SprintDivContentMain').hide();
    $('#SprintScrumboardDiv').hide();
    $('#sprintSBback').addClass('d-block').removeClass('d-none');
    $('#sprintSBback').children('img').attr('onclick','closeSprintSB("SBsettings");');
    var sprintgroupdata = result[0].sprintGroupData;
    var color = sprintgroupdata[0].color_code;
    color = typeof(color)=="undefined"||color==""?"#f2788f":color;
    ui="<div id='SBsettingsMaindiv' style='font-size: 12px;'>"
        +"<div id='SBsettingsContent1' onclick='' groupid="+sprintgroupdata[0].sprint_group_id+" class='py-2 px-3 d-flex align-items-center position-relative' style='border-bottom: 1px solid #aaaaaa;'>"
            +"<div>"+getValues(companyLabels,"Sprint_Group")+"</div>"
            +"<div id='SBsettingGrouplist' class='position-relative ml-2' style='left:1px;cursor:pointer;' onclick=\"listSprintgroups('SBsettings');event.stopPropagation();\"><img src='images/task/dowarrow.svg' style='width:12px;height:12px;'></div>"
            +"<div class='position-relative rounded-circle ml-2' onclick=\"listSprintgroups('SBsettings');event.stopPropagation();\" style='cursor:pointer;width:15px;height:15px;background-color:"+color+"'></div>"
            +"<div id='SBsettingsGroupname' onclick=\"listSprintgroups('SBsettings');event.stopPropagation();\" class='ml-2' style='cursor:pointer;'>"+sprintgroupdata[0].sprint_group_name+"</div>"
           
        +"</div>"
        +"<ul id='SBsettingsStageslist' onclick='' class='d-flex p-2' style='overflow-x: auto;'>"
            ui+=SBsettingsStageslistloop(result,SBplace,"")
        ui+="</ul>"



    +"</div>"


    return ui;
}

function SBsettingsStageslistloop(result,SBplace,storydata){
    var ui="";
    var j = result.length-1;
    var color3="";var color4="";
    color3=["#cce6ff","#ffe6cc","#e0ebeb","#ccebff","#d9d9f2","#d9f2e6","#ffcccc","#ffffcc","#ffccff","#ccffcc","#ddccff","#ebccff","#f5d6eb","#f2ffcc","#f2d9e6","#ffd6cc","#ddccff","#ccfff5","#ccffcc","#ddccff","#ccfff2","#ffebcc","#ccffcc","#ffccff","#ffcce6"];
    color4=["#99ccff","#ffcc99","#c2d6d6","#99d6ff","#b3b3e6","#b3e6cc","#ff9999","#ffff99","#ff99ff","#99ff99","#bb99ff","#d699ff","#ebadd6","#e6ff99","#e6b3cc","#ffad99","#bb99ff","#99ffeb","#99ff99","#bb99ff","#99ffe6","#ffd699","#99ff99","#ff99ff","#ff99cc"];
    for(var i=0;i<result.length;i++){
        ui+=stagesCommonUI(result[i].stage_id,result[i].stage_order,result[i].stage_name,result[i].stage_type,color3[i],color4[i],SBplace,storydata)
    }
    /* if(SBplace!="SBlistingstories"){
        ui+=SBsettingCreatenewStage("SBsettingsresult")
    } */
    //ui+=stagesCommonUI(result[j].stage_id,result[j].stage_order,result[j].stage_name,result[i].stage_type,"","",SBplace,storydata)
    
    return ui;
}

function stagesCommonUI(stageid,stageorder,stagename,stagetype,color3,color4,SBplace,storydata){
    var ui="";
    var color="";var color2="";
    color = stagename=="Sprint Backlog"?"#f1e4e6":stagename=="Assigned"?"#f7efe1":stagename=="Completed"?"#dff9e5":typeof(color)=="undefined"?"#efe2e1":color3;
    color2 = stagename=="Sprint Backlog"?"#eeced4":stagename=="Assigned"?"#f2d8a9":stagename=="Completed"?"#baecc5":typeof(color2)=="undefined"?"#ddc8c7":color4;
    var classhandle = stagetype=="Custom"?"dragndrophandle":"stopdragndrop";var classhandle1 = stagetype=="Custom"?"dragndrophandle1":"stopdragndrop1";
    var cursor1 = stagetype=="Custom"?"cursor: move":"cursor: default";
    var class1 = SBplace=="SBlistingstories"?"d-block":"";
    var sprintid = SBplace=="SBlistingstories" ? $('#sprintname2').attr('sprintid') : "";
    var stageclass= stagename=="Sprint Backlog"?"backlogstage":stagename=="Completed"?"completestage":"";
    var stageclass1= stagename=="Sprint Backlog"?"backlogstage1":stagename=="Completed"?"completestage1":"";
     

    ui="<li id='SBsettingStage_"+stageid+"' stageid="+stageid+" stagetype='"+stagetype+"' onclick='' onmouseover=\"showSBsettingFloatOption(this,"+stageid+")\" onmouseout=\"hideSBsettingFloatOption(this,"+stageid+")\" class='card-group SBsettingStageCls rounded px-1 py-2 position-relative "+class1+" "+stageclass+" loopstage' style=''>" 
        +"<div id='idStagename_"+stageid+"' class='stageheader card border-0 w-100 "+classhandle+" position-relative rounded'  style='background-color:"+color+";'>"   
            if(SBplace=="SBlistingstories"){
                ui+="<img id='SBstagecheck_"+stageid+"' onclick='checkAllstageStories("+stageid+");event.stopPropagation();' src='/images/task/inactivestep.svg' class='position-absolute' style='cursor:pointer;width:10px;height:10px;top:-4px;left:-4px;z-index:1;'>"
            }
            ui+="<div class='position-relative p-2 "+classhandle1+"' style='"+cursor1+";'>"
                +"<div id='contentstage_"+stageid+"' onmouseover='showScountTooltip(this);event.stopPropagation();' onmouseout='hideScountTooltip(this);event.stopPropagation();' class='contentstageCls rounded d-flex align-items-center justify-content-between position-relative' class='w-100' style='background-color:"+color2+"'>"
                    //+"<div id='storyCountTooltipid_"+stageid+"' class='storyCountTooltiptext' style='display:none;'></div>"
                    +"<div id='stagenameDiv_"+stageid+"' class='defaultExceedCls rounded px-2 py-1' style='width:fit-content;max-width:90%;' title='"+stagename+"'>"+stagename+"</div>"
                    if(SBplace=="SBlistingstories"){
                        ui+="<div id='stageStoryCount_"+stageid+"' class='p-1 ml-auto rounded' style='margin-right:4px;'></div>"
                    }
                    if(stagename!="Sprint Backlog" && stagename!="Completed" && SBplace=="SBsettingsresult"){
                        ui+="<div class='ml-auto createStageCls' style='margin-right:5px;' onclick=\"createNewstage("+stageid+");event.stopPropagation();\"><img src='images/task/plus.svg' style='width:15px;height:15px;cursor:pointer;'></div>"
                    }
                ui+="</div>"    
                if(stagetype=="Custom" && SBplace=="SBsettingsresult"){
                    ui+="<div id='editStageinput_"+stageid+"' onclick='' class='p-1 d-none editStageCls' style='z-index:2;'><input id='editStageNameinput_"+stageid+"' value='"+stagename+"' class='w-100 border-0' onkeydown=\"updateStagename(event,"+stageid+");event.stopPropagation();\" style='border-bottom: 1px solid grey !important;font-size:12px;outline:none;resize:none;background-color:"+color+";'></div>"
                    ui+=SBsettingFloatOption(stageid)
                }
            ui+="</div>"
        +"</div>"
        +"<div id='SbstagereorderDiv_"+stageid+"' class='position-absolute d-none SBstagereorderCls' style='border-radius: 50px;border: 1px solid rgb(161, 161, 161);background-color: rgb(255, 255, 255);width: 35px;height: 35px;top:5px;left: 100px ;z-index: 100;'>"
          +"<div class='my-0 mx-auto' style='height:10px;width:10px;'>"
            +"<img class='' src='/images/idea_old/arrow-top.png' onclick='' style='opacity:0.2;cursor:pointer;height:10px;width:10px;float:left;'>"
          +"</div>"
          +"<div class='' style='height:12px;'>"
            +"<img src='/images/idea_old/arrow-left.png' onclick=\"reorderStage("+stageid+",'left');event.stopPropagation();\" style='cursor:pointer;float:left;height:10px;width:10px;margin-top:2px;'>"
            +"<img src='/images/idea_old/arrow-right.png' onclick=\"reorderStage("+stageid+",'right');event.stopPropagation();\" style='cursor:pointer;float:right;height:10px;width:10px;margin-top:2px;'>"
          +"</div>"
          +"<div class='my-0 mx-auto' style='height:10px;width:10px;'>"
            +"<img class='' src='/images/idea_old/arrow-down.png' onclick='' style='opacity:0.2;cursor:pointer;height:10px;width:10px;float:left;'>"
          +"</div>"
        +"</div>"
        +"<div id='SBcontent_"+stageid+"' class='py-2 SBcontentCls "+stageclass1+"' onclick='' style='min-height:465px;height:auto;background-color:"+color+";margin-top:-7px;'>"
        if(SBplace=="SBlistingstories"){
            ui+=scrumboardListUI(storydata,stageid,"",sprintid)
        }
        ui+="</div>"
    +"</li>"
    
    return ui;
}

function SBsettingCreatenewStage(stageid){
    var ui="";

    ui="<li id='SBsettingStageCreate' stageorder='' stagetype='Custom' class='card-group SBsettingStageCls rounded p-3 stopdragndrop d-block' style='background-color:#fff;'>" 
        +"<div class='card border-0 w-100 rounded' style='background-color:#efe2e1;'>"   
            /* +"<div id='stageCreatenewLabeldiv' class='p-2 d-flex'>"
                +"<div class='' onclick=\"createNewstage();event.stopPropagation();\"><img src='images/task/plus.svg' style='width:15px;height:15px;cursor:pointer;'></div>"
                +"<div class='ml-2'>New</div>"
            +"</div>" */
            +"<div id='newstageCreateDiv' class='d-block mx-2'>"
                +"<div class='p-1'><input id='newStagenameinput' class='w-100 border-0' onblur=\"insertNewStage1(event,"+stageid+");event.stopPropagation();\" onkeydown=\"insertNewStage(event,this,"+stageid+");event.stopPropagation();\" style='border-bottom: 1px solid grey !important;font-size:12px;outline:none;resize:none;background-color:#efe2e1;'></div>"
                /* +"<div class='d-flex float-right'>"
                    +"<img src='/images/task/remove.svg' onclick=\"insertNewstage();event.stopPropagation();\" class='mx-1' style='width:18px;height:18px;cursor:pointer;' title='Cancel'>"
                    +"<img src='/images/task/tick.svg' onclick=\"createNewstage();event.stopPropagation();\" class='mx-1' style='width:18px;height:18px;cursor:pointer;' title='Create'>"
                +"</div>" */
            +"</div>"
        +"</div>" 
        +"<div id='SBcontent_' class='py-2 rounded' onclick='' style='min-height:450px;height:auto;background-color:#efe2e1;margin-top: -9px;'>"
        
        +"</div>"  
    +"</li>"   

    return ui;
}

function reorderStage(stageid,moveside){
    var stagetype="";
    var nextstageid="";
    var clData="";
    if(moveside == "left"){
        stagetype = $('#SBsettingStage_'+stageid).prev('li').attr('stagetype');
        nextstageid = $('#SBsettingStage_'+stageid).prev('li').attr('id').split('_')[1];
        if(stagetype=="Default"){
            alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
        }else{ 
            clData=$('#SBsettingStage_'+stageid).clone();
            $('#SBsettingStage_'+stageid).remove();
            $(clData).insertBefore('#SBsettingStage_'+nextstageid);
            updateSBStageorder(stageid,nextstageid);
        }   
    }else{
        stagetype = $('#SBsettingStage_'+stageid).next('li').attr('stagetype');
        nextstageid = $('#SBsettingStage_'+stageid).next('li').attr('id').split('_')[1];
        if(typeof(nextstageid)!="undefined"){
            if(typeof(stagetype)=="undefined" || stagetype=="Default" || stagetype==""){
                alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
            }else{ 
                clData=$('#SBsettingStage_'+stageid).clone();
                $('#SBsettingStage_'+stageid).remove();
                $(clData).insertAfter('#SBsettingStage_'+nextstageid);
                updateSBStageorder(stageid,nextstageid);
            }  
        }else{
            alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
        }
        
    }

}

function updateSBStageorder(stageid,nextstageid){

    let jsonbody = {
        "currentId" : stageid,
        "targetId" : nextstageid,
        "type" : "SG"
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    $.ajax({
        url: apiPath + "/" + myk + "/v1/updateStageOrder",
        type: "PUT",
        //dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success: function (result) {


            $(".SBstagereorderCls").addClass('d-none').removeClass('d-block');
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });        
}

function editStagename(event,stageid){
    if($('#editStageinput_'+stageid).is(':visible') == true){
        $('#contentstage_'+stageid).addClass('d-flex').removeClass('d-none');
        $('#editStageinput_'+stageid).addClass('d-none').removeClass('d-block');
    }else{
        $('#editStageinput_'+stageid).addClass('d-block').removeClass('d-none');
        $('#contentstage_'+stageid).addClass('d-none').removeClass('d-flex');
        $('#editStageNameinput_'+stageid).focus();
        $('#editStageNameinput_'+stageid).attr('onblur','updateStagename1(event,'+stageid+');event.stopPropagation();');
    }
}

function updateStagename(event,stageid){
    var code = null;
	code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if(code == 13){
        updateStagename1(event,stageid);
    }    
}

function updateStagename1(event,stageid){
    var stagename = $('#editStageNameinput_'+stageid).val().trim();
    var groupid = $('#SBsettingsContent1').attr('groupid');
    if(stagename==""){
        alertFunNew(getValues(companyAlerts,"Alert_StageNoEmt"),'error');
    }else{
        let jsonbody = {
            "user_id" : userIdglb,
            "stage_id" : stageid,
            "stage_name" : stagename,
            "type" : "SG",
            "sprint_group_id" : groupid
        }
        $.ajax({
            url: apiPath + "/" + myk + "/v1/updateStage",
            type: "PUT",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
            },
            success: function (result) { 
                $('#editStageNameinput_'+stageid).removeAttr('onblur');
                $('#stagenameDiv_'+stageid).text(stagename).attr('title',stagename);
                $('#editStageNameinput_'+stageid).attr('value',stagename);
                $('#contentstage_'+stageid).addClass('d-flex').removeClass('d-none');
                $('#editStageinput_'+stageid).addClass('d-none').removeClass('d-block');
            }
        });       

    }
}

function dontcancelstagecreate(){
    $('#newStagenameinput').focus();
}


function insertNewStage(event,obj,stageid){
    var code = null;
	code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if(code == 13){
        insertNewStage1(event,stageid);
    }
}
function insertNewStage1(event,prevstageid){
       
    var stagename = $('#newStagenameinput').val().trim();
    var groupid = $('#SBsettingsContent1').attr('groupid');
    if(stagename==""){
        confirmResetNew("Do you want to cancel?",'clear',"createNewstage","dontcancelstagecreate",'Yes','No');
    }else{
    $.ajax({
        url: apiPath+"/"+myk+"/v1/insertStage?stageName="+stagename+"&groupId="+groupid+"&projId="+prjid+"&type=SG&userId="+userIdglb+"&companyId="+companyIdglb+"",
        type:"GET",
        error: function(jqXHR, textStatus, errorThrown) {
            checkError(jqXHR,textStatus,errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){
            $('#newStagenameinput').removeAttr('onblur');
            var stageid=result[0].stage_id;
            var stageorder="";
            var stagename=result[0].stage_name;
            var stagetype=result[0].stage_type
            var color1=["#cce6ff","#ffe6cc","#e0ebeb","#ccebff","#d9d9f2","#d9f2e6","#ffcccc","#ffffcc","#ffccff","#ccffcc","#ddccff","#ebccff","#f5d6eb","#f2ffcc","#f2d9e6","#ffd6cc","#ddccff","#ccfff5","#ccffcc","#ddccff","#ccfff2","#ffebcc","#ccffcc","#ffccff","#ffcce6"];
            var color2=["#99ccff","#ffcc99","#c2d6d6","#99d6ff","#b3b3e6","#b3e6cc","#ff9999","#ffff99","#ff99ff","#99ff99","#bb99ff","#d699ff","#ebadd6","#e6ff99","#e6b3cc","#ffad99","#bb99ff","#99ffeb","#99ff99","#bb99ff","#99ffe6","#ffd699","#99ff99","#ff99ff","#ff99cc"];
            $(stagesCommonUI(stageid,stageorder,stagename,stagetype,"","","SBsettingsresult","")).insertAfter('#SBsettingStage_'+prevstageid);
            var i = $('#SBsettingStage_'+stageid).index()-2;
            createNewstage();
            $('#newStagenameinput').val('');
            $('#idStagename_'+stageid+', #SBcontent_'+stageid+', #editStageNameinput_'+stageid).css('background-color',color1[i]);
            $('#stagenameDiv_'+stageid).css('background-color',color2[i]);
            updatedragReorder("stagesettings");
        }
                
    });    
    }
  
    
}

function createNewstage(stageid){
    if($('#SBsettingStageCreate').length==0){
        $(SBsettingCreatenewStage(stageid)).insertAfter($('#SBsettingStage_'+stageid));
        $('#newstageCreateDiv').find('input').focus();
        $('#newStagenameinput').attr('onblur','insertNewStage1(event,'+stageid+');event.stopPropagation();');
        var elmnt = document.getElementById("SBsettingStageCreate");
        elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
    }else{
        $('#SBsettingStageCreate').remove();
    }
    /* if($('#newstageCreateDiv').is(':hidden')){
        $('#SBsettingStageCreate').addClass('d-block').removeClass('d-none');
        $('#newstageCreateDiv').addClass('d-block').removeClass('d-none');
        $('#stageCreatenewLabeldiv').addClass('d-none').removeClass('d-flex');
        $('#newstageCreateDiv').find('input').focus();
        $('#newStagenameinput').attr('onblur','insertNewStage1(event);event.stopPropagation();');
        var elmnt = document.getElementById("SBsettingStageCreate");
        elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
    }else{
        $('#SBsettingStageCreate').addClass('d-none').removeClass('d-block');
        $('#newstageCreateDiv').addClass('d-none').removeClass('d-block');
        $('#stageCreatenewLabeldiv').addClass('d-flex').removeClass('d-none');
    } */
}
function closestage(){
    $('#SBsettingStageCreate').addClass('d-none').removeClass('d-block');
    $('#newstageCreateDiv').addClass('d-none').removeClass('d-block');
    $('#stageCreatenewLabeldiv').addClass('d-flex').removeClass('d-none');
    $('#BoxOverlay').css('display','none');
	$('#confirmDiv').css('display','none');
}

function SBsettingFloatOption(stageid){
    var ui="";

    ui="<div id='SBsettingFloatOptions_"+stageid+"' onclick='event.stopPropagation();' class='sprintfloatoptions position-absolute d-none' style='top:35px;right:0px !important;width:100px !important;'>"
        +"<div class='' style='margin:0px 6px' title='Reorder' onclick=\"showSBreorder("+stageid+");event.stopPropagation();\"><img src='/images/task/expand.svg' style='width:18px;height:18px;'></div>"
        +"<div class='editStagenameOptionCls' style='margin:0px 6px' title='Edit' onclick=\"editStagename(event,"+stageid+");event.stopPropagation();\"><img src='/images/conversation/edit.svg' style='width:18px;height:18px;'></div>"
        +"<div class='' style='margin:0px 6px' title='Delete'><img src='/images/agile/delete.svg' onclick=\"deleteSBStage("+stageid+");event.stopPropagation();\" style='width:18px;height:18px;'></div>"
    +"</div>"

    return ui;
}

function deleteSBStage(stageid){
    let jsonbody = {
        "stage_id" : stageid,
        "user_id": userIdglb,
        "type" : "SG"
    }
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
    $.ajax({ 
        url: apiPath+"/"+myk+"/v1/deleteStage",
        type:"DELETE",
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
            //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        }, 
        success:function(result){
            $('#SBsettingStage_'+stageid).remove();
        }
    });    
}

function showSBreorder(stageid){
    if($('#SbstagereorderDiv_'+stageid).is(':hidden')){
        $('#SbstagereorderDiv_'+stageid).addClass('d-block').removeClass('d-none');
    }else{
        $('#SbstagereorderDiv_'+stageid).addClass('d-none').removeClass('d-block');
    }


}

function showSBsettingFloatOption(obj){
    $(obj).find('.sprintfloatoptions').addClass('d-flex').removeClass('d-none');
}
function hideSBsettingFloatOption(obj){
    $(obj).find('.sprintfloatoptions').addClass('d-none').removeClass('d-flex');
    $('.sprintfloatoptions').addClass('d-none').removeClass('d-flex');
}

var cansortstage="";
//placeholder:'.SBsettingStageCls',
        //forcePlaceholderSize: true,
        
function initdraganddropStages(place){
    var targetid="";var currentstageid="";var cloneval ="";
    cloneval = $('#SBsettingsStageslist').clone();
    $("#SBsettingsStageslist, #sprintSBtable").sortable({
        connectWith: '#SBsettingsStageslist, #sprintSBtable',
        cancel:".stopdragndrop",
        handle: '.dragndrophandle1',
        opacity: 0.5,
        placeholder:'.SBsettingStageCls',
        start: function( event, ui ) {
            currentstageid=ui.item.attr('id').split("_")[1];
            console.log("currentstageid--->"+currentstageid);
        },
        stop: function(event, ui) {
            //cansortstage = ui;
            console.log("1->"+$('#SBsettingStage_'+currentstageid).prev().attr('stagetype'));
            console.log("2->"+$('#SBsettingStage_'+currentstageid).next().attr('stagetype'));
            if($('#SBsettingStage_'+currentstageid).prev().attr('stagetype')=="Default" && $('#SBsettingStage_'+currentstageid).next().attr('stagetype')=="Custom"){
                targetid=$('#SBsettingStage_'+currentstageid).next().attr('id').split("_")[1];
                updatedragReorder(place);
            }else if($('#SBsettingStage_'+currentstageid).next().attr('stagetype')=="Default" && $('#SBsettingStage_'+currentstageid).prev().attr('stagetype')=="Custom"){
                targetid=$('#SBsettingStage_'+currentstageid).prev().attr('id').split("_")[1];
                updatedragReorder(place);
            }else if($('#SBsettingStage_'+currentstageid).prev().attr('stagetype')=="Default" && $('#SBsettingStage_'+currentstageid).next().attr('stagetype')=="Default"){
                revertStageDrag();
            }else if($('#SBsettingStage_'+currentstageid).prev().attr('stagetype')=="Default" && typeof($('#SBsettingStage_'+currentstageid).next().attr('stagetype'))=="undefined"){
                revertStageDrag();
            }else if(typeof($('#SBsettingStage_'+currentstageid).prev().attr('stagetype')=="undefined") && $('#SBsettingStage_'+currentstageid).next().attr('stagetype')=="Default"){
                revertStageDrag();
            }else{
                targetid=$('#SBsettingStage_'+currentstageid).prev().attr('id').split("_")[1];
                updatedragReorder(place);
            }


            console.log(currentstageid+"<--targetid-->"+targetid);
            console.log($('#SBsettingStage_'+currentstageid).next().attr('stagetype')+"<--------->"+$('#SBsettingStage_'+currentstageid).prev().attr('stagetype'));
            
        }




    });  
}  

function stopStageDrag(cloneval){
    $('#SBsettingsStageslist').remove();
    $('#SBsettingsMaindiv').append(cloneval);
    initdraganddropStages();
    alertFun(getValues(companyAlerts,"Alert_StageBacklogMsg"),'warning');
}


function updatedragReorder(place){
    var grpid = place=="stagesettings"?$('#SBsettingsContent1').attr('groupid'):$('#sprintGroupSB1').attr('sgid'); 
    var stageData = getStageData(place);
    let jsonbody = {
        "sprint_group_id" : grpid,
        "stageData"  :JSON.parse(stageData)
    }
    console.log("jsonbody-->"+jsonbody);
     
    $.ajax({
        url: apiPath + "/" + myk + "/v1/dragDropStage",
        type: "PUT",
        //dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
         
        },
        success: function (result) {


           
        }
    });   

}

function getStageData(place){

    var stage_id="";
    var order="";var i=0;
    var jsonvalue = '[';
    place = place=="stagesettings"?"#SBsettingsStageslist":"#sprintSBtable";
    $(''+place+' .loopstage').each(function(){
      stage_id = $(this).attr('stageid');
      order = $(this).index();
      console.log(i+"<--order-->"+order);
      jsonvalue+="{";
      jsonvalue+="\"stage_order\":\""+i+"\",";
      jsonvalue+="\"stage_id\":\""+stage_id+"\"";
      jsonvalue+="},";
      i++;
      
    });
    var jsonresult="";
    if(jsonvalue.length>1){
      jsonresult = jsonvalue.substring(0, jsonvalue.length-1);
      jsonresult=jsonresult+"]";
    }else{
      jsonresult ="[]";
    }
    console.log("jsonresult--->"+jsonresult);
    return jsonresult;
     
}

function revertStageDrag(){
    alertFun(getValues(companyAlerts,"Alert_StageBacklogMsg"),'warning');
    $('#SBsettingsStageslist, #sprintSBtable').sortable("cancel");
}

function showSprintlist(){
    if($('#SprintHolderDiv').length!=0){
        if($(".elem-CalenStyle").length==0){
            $('#SprintDivContentMain').toggle();
        }else{
            $('.elem-CalenStyle').toggle();
        }
    }
    if($('#SBsettingsMaindiv').length!=0){
        if($(".elem-CalenStyle").length==0){
            $('#SprintDivContentMain').toggle();
        }else{
            $('.elem-CalenStyle').toggle();
        }
    }
    
}
function checkvalid(obj){
    if($(obj).val().trim()!=""){
        sprintcheckflag=true;
    }else{
        sprintcheckflag=false;
    }
}

function sprintCheckalert(){

}

function dontCancelSprint(){
    $('#sprintname1').focus();
    ///sprintcheckflag=false;
}
function checkUpdateStorysel(place){
    /* if(place=="edit"){
        if($(this).parents().hasClass('newadded')){
            sprintcheckflag=true;
        }else{
            sprintcheckflag=false;
        }
     
        if($(this).attr("uncheckvalue")=='y'){
            sprintcheckflag=true;
        }else{
            sprintcheckflag=false;
        }
    }else{ */
        sprintcheckflag=true;
    //}
        
     
} 
var jsonCalView="";
async function listCalendarSprint(viewtype){
    if(sprintviewtype!=viewtype){
        sprintviewtype=viewtype;
        let jsonbody={
            "groupLoadType":"active",
            "project_id":prjid,
            "filterType" : "calender"
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        await $.ajax({
            url: apiPath + "/" + myk + "/v1/fetchJsonSprint",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                jsonCalView=result;
                $('#sprintContentGridListDiv,#sprintContentListDiv,#SprintScrumboardDiv,#SBsettingsDiv').html('');
                $("#agileSview").hide();
                initlistCalendarSprint(result);
                
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            
            }
        });
    }else{
        if($("#SprintHolderDiv").length!=0){
            $('.elem-CalenStyle').toggle();
        }    
    }
    hideSprintChart();
}


function initlistCalendarSprint(result){

    //plugin refer https://www.jqueryscript.net/time-clock/Mobile-friendly-Drag-Drop-Event-Calendar-Plugin-CalenStyle.html

    $('#sprintContentListDiv, #sprintContentGridListDiv').hide();
    $('#sprintHeaderDiv').addClass('d-none').removeClass('d-flex');
    $('#sprintContentGridListDiv,#sprintContentListDiv').html('');
    closeSprintSB("calendarview");
    $(".calendar-1").show();
    
    
    $(".calendar-1").CalenStyle({
    
        visibleView:"WeekView",
        fullMonthNames: ["January","February","March","April","May","June","July","August","September","October","November","December"],
        shortMonthNames: ["January","February","March","April","May","June","July","August","September","October","November","December"],
        viewsToDisplay: [
                {
                    viewName:"DayView",
                    viewDisplayName:"Day"
                },
                {
                    viewName:"WeekView",
                    viewDisplayName:"Week"
                },
                {
                    viewName:"DetailedMonthView",
                    viewDisplayName:"Month"
                }
                
        ], 
        headerComponents:{
            DatePickerIcon: "<span class='cContHeaderDatePickerIcon clickableLink icon-Calendar'><img src='images/landingpage/cal.svg' style='height:18px;width:18px;vertical-align: unset !important;'></span>",
            FullscreenButton: function(bIsFullscreen)
            {
                var sIconClass = (bIsFullscreen) ? "icon-Contract" : "icon-Expand";
                return "<span class='cContHeaderFullscreen clickableLink "+ sIconClass +"'></span>";
            },
            PreviousButton: "<span id='nextprev1' class='cContHeaderButton cContHeaderNavButton cContHeaderPrevButton clickableLink icon-Prev'><img src='/images/task/arrow_left.svg' style='height:18px;width:18px;vertical-align: unset !important;'></span>",
            NextButton: "<span id='nextprev2' class='cContHeaderButton cContHeaderNavButton cContHeaderNextButton clickableLink icon-Next'><img src='/images/task/arrow_right.svg' style='height:18px;width:18px;vertical-align: unset !important;'></span>",
            TodayButton: "<span class='cContHeaderButton cContHeaderToday clickableLink'></span>",
            HeaderLabel: "<span class='cContHeaderLabelOuter'><span class='cContHeaderLabel'></span></span>",
            HeaderLabelWithDropdownMenuArrow: "<span class='cContHeaderLabelOuter clickableLink'><span class='cContHeaderLabel'></span><span class='cContHeaderDropdownMenuArrow'></span></span>",
            MenuSegmentedTab: "<span class='cContHeaderMenuSegmentedTab'></span>",
            MenuDropdownIcon: "<span class='cContHeaderMenuButton clickableLink'>☰</span>"
        },
        calDataSource:[
					    {
					        sourceFetchType: "ALL",
					        sourceType: "JSON",						
					        source: {
						        eventSource: result
                                
						    },
                            config:{
                                changeColorBasedOn: "Event"
                            }   
					        
					    }
        ],
        isDragNDropInMonthView:false,
        isDragNDropInDetailView:false,
        isDragNDropInTaskPlannerView:false,
        hideEventTime:{
            Default: true,
            DetailedMonthView: true,
            MonthView: true,
            WeekView: true,
            DayView: true,
            CustomView: true,
            QuickAgendaView: true,
            TaskPlannerView: true,
            DayEventDetailView: true,
            AgendaView: true,
            WeekPlannerView: true
        },
        eventIcon:"Dot",
        
        
        eventClicked: function(visibleView, sElemSelector, oEvent){
            $('[id^=Event-]').removeClass("taskNodeSelect");
		    //editSprint(oEvent.sprint_id,'edit');
            showSprintScrumboard(oEvent.sprint_id,'calendarview');
            $(sElemSelector).addClass("taskNodeSelect");

        }                

        
    
    });

    afterload();

    $("#cContHeaderMenuDetailedMonthView").trigger("click");
    
    $("#nextprev1 , #nextprev2, #cContHeaderMenuWeekView, #cContHeaderMenuDetailedMonthView, #cContHeaderMenuDayView").on("click", function(){
        afterload();
    });

}

$("#nextprev1 , #nextprev2, #cContHeaderMenuWeekView, #cContHeaderMenuDetailedMonthView, #cContHeaderMenuDayView").on("click", function(){
    afterload();
});

function changedata(result){
    var idd=""; var idd2="";var j="";var k="";
    
    for(var i=0;i<result.length;i++){

        j = i+1;
        k = $('#Event-'+j).attr('data-id');
        if($('#Event-'+j).hasClass('calendarSprint_'+result[i].sprint_id)==false && (typeof(k)!="undefined"||k!="undefined")){
            $('#Event-'+j).addClass('calendarSprint_'+result[i].sprint_id);
            $('#Event-'+j).find('.cEventLink').prepend('<span class="ml-1 float-left storycountCalendar" style="font-size: 12px;">'+result[i].storyCount+'</span>');
            $('#Event-'+j).find('.cEventLink').append('<span class="mx-1 float-right sprintIconsCalendar"><img src="/images/agile/scrumboard.svg" onclick=showSprintScrumboard('+result[i].sprint_id+');event.stopPropagation(); style="width:15px;height:15px;margin: 0px 3px 3px 3px !important;"></span>');
        }
    
    }  
     
}

function loadGlbScrumboard(){
    hideSprintChart();
    if($("#scrumboardMainDivUI").length == 0){
        showSprintScrumboard(sprintListJsonGlb[0].sprint_id,"glbOption");
    }
    
}

function showGlbSprintList(){
    $("#sprintGlbListPopupDiv").toggle();
}

function showScountTooltip(obj){
    $(obj).find(".storyCountTooltiptext").show();
}
function hideScountTooltip(obj){
    $(".storyCountTooltiptext").hide();
}

function showSprintChart(sprintid,charttype){

    $("#sprintChart1").children("img").attr("onclick","showSprintChart("+sprintid+",'chart1')");
    $("#sprintChart2").children("img").attr("onclick","showSprintChart("+sprintid+",'chart2')");
    $("#sprintChart1, #sprintChart2, #sprintSBback").removeClass("d-none").addClass("d-block");
    $("#sprintChartDiv").html("").removeClass("jqplot-target");
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
    $.ajax({
        url: apiPath+"/"+myk+"/v1/fetchSprintChartData?sprintId="+sprintid+"&chartType="+charttype,
        type:"GET",
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
                }, 
        success:function(result){

            $("#sprintDivMainbody").hide();
            $("#sprintChartDiv").show();
            $('#sprintSBback').children('img').attr("onclick","closeSprintSB('chart')");

            initChart(result);

            $('#loadingBar').addClass('d-none').removeClass('d-flex');  
        }
    });      



} 

//http://www.jqplot.com/  chart plugin
function initChart(data){

    var ticks=[];
    var sprint1 = [];
    var sprint2 = [];
    var maxVal1=0;
	var maxVal2=0;
	var finalMaxVal=0;
	var minVal1=0;
	var minVal2=0;
	var finalMinVal=0;

    for (var i=0; i<data.length; i++){ 
        if(parseInt(maxVal1)<parseInt(data[i].estimated_hour)){
           maxVal1=parseInt(data[i].estimated_hour);
        }
        if(parseInt(minVal1)> parseInt(data[i].estimated_hour)){
           minVal1=parseInt(data[i].estimated_hour);
        }
        sprint1.push([data[i].displayDate, data[i].estimated_hour]); 
        ticks.push([data[i].displayDate]);
    }
    for (var j=0; j<data.length; j++){ 
        if(parseInt(maxVal2)<parseInt(data[j].actual_hour)){
           maxVal2=parseInt(data[j].actual_hour);
        }
        if(parseInt(minVal2)>parseInt(data[j].actual_hour)){
           minVal2=parseInt(data[j].actual_hour);
        }
        sprint2.push([data[j].displayDate, data[j].actual_hour]); 
    }
    if(parseInt(maxVal1)>parseInt(maxVal2)){
        finalMaxVal=maxVal1;
    }else{
        finalMaxVal=maxVal2;
    }
    if(parseInt(minVal1)< parseInt(minVal2)){
        finalMinVal=minVal1;
    }else{
        finalMinVal=minVal2;
    }  
    if(parseInt(finalMinVal) >= 0){
        finalMinVal = 0 ;
    }else{
        finalMinVal = parseInt(finalMinVal)-50;
    }
    console.log(sprint1);
	console.log(sprint2);
    console.log(ticks);
    console.log(finalMaxVal);
	console.log(finalMinVal);
    console.log(minVal1);
    console.log(minVal2);

    var h1 = $("#content").height()-50;
    var w1 = $("#content").width()-50;
    
    var labels=["Estimated Hours","Actual Hours"];
	$.jqplot('sprintChartDiv', [sprint1,sprint2], {
        title:'chart',
        height: h1,
        width: w1,
        animate : true,
        cursor: {
            show: true,
            showTooltip: true
                            
        },
        axes:{
            xaxis:{
                renderer:$.jqplot.DateAxisRenderer,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                ticks:ticks,
                tickOptions: {
                formatString: '%d-%b-%y', 
                    angle: -30
                },
                drawMajorGridlines: true	
            },yaxis: {
                autoscale:true,
                label: "Hours",
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                min: parseInt(finalMinVal),
				max: parseInt(finalMaxVal)+5,
                tickOptions:{
                    formatString:'%.0f'
                }
            }
        },
        highlighter: {
            show: true,
            sizeAdjust: 1,
            tooltipOffset: 9,
            style: 'crosshair',
        },
                        
        legend: {
            show: true,
            placement: 'inside',
            labels:labels
        }
    });
   

}

function hideSprintChart(){
    if($("#sprintDivMainbody").is(":hidden")){
        $("#sprintDivMainbody").show();
        $("#sprintChartDiv").html("").hide().removeClass("jqplot-target");
        $("#sprintChart1, #sprintChart2").removeClass('d-block').addClass('d-none');
        $('#sprintSBback').children('img').attr('onclick','closeSprintSB()');
        $('#sprintSBback').addClass("d-none").removeClass("d-block");
    }
}