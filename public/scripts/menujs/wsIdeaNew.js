
var menuutype="Idea";

(function ($) {
  function init() {
      $('.easy-tree').EasyTree({
          addable: true,
          editable: true,
          deletable: true
      });
  }

  window.onload = init();
  
  listIdeas();
})(jQuery)

function colorpicker(){
  $(".colorplace").minicolors({
     
     
    control: 'hue',
  
    format: 'hex',
    hide: function(){
      $(".minicolors-panel").show();
    },
    change:function(){
    //  console.log($("#group_color").val().toUpperCase())
    //   $("#group_color").val($("#group_color").val().toUpperCase());
    //   $("#group_color").attr('value',$("#group_color").val().toUpperCase());
    },
    inline: false,
   
    theme: 'default',
    swatches: []
    
  });
  colorGridView();
 
  $(".colorplace").css('height','258px'); 
     setTimeout(function(){ 

     
       $(".minicolors-panel").hide();
  

    },300);
}


function getOrderData(ideaId,parentId){
  ////var parentId=$('#mainIdea_'+ideaId).parent('ul').attr('id').split('_')[1];
  var id="";
  var order="";
  var jsonvalue = '[';
  $('#idealistUL_'+parentId+' li').each(function(){
    id = $(this).attr('id').split('_')[1];
    order = $(this).index();
    jsonvalue+="{";
    jsonvalue+="\"id\":\""+id+"\",";
    jsonvalue+="\"order\":\""+order+"\"";
    jsonvalue+="},";
    
  });
  var jsonresult="";
  if(jsonvalue.length>1){
    jsonresult = jsonvalue.substring(0, jsonvalue.length-1);
    jsonresult=jsonresult+"]";
  }else{
    jsonresult ="[]";
  }
  console.log("jsonresult--->"+jsonresult);
  return jsonresult;
}


function removeicon(id){
  var parentId=$('#mainIdea_'+id).parent('ul').attr('id').split('_')[1];
  var childid=$('#mainIdea_'+id).attr('childid');
  var prevPId=$('#mainIdea_'+id).attr('parent_idea_id');
  
  var previdealiLength=$('#mainIdea_'+prevPId).find('> li').length;
  
}

function updateIdeaDragnDrop(ideaId){
  var parentId=$('#mainIdea_'+ideaId).parent('ul').attr('id').split('_')[1];
  var prevPId=$('#mainIdea_'+ideaId).attr('parent_idea_id');
  var place=$('#mainIdea_'+ideaId).attr('place');
  var previdealiLength=$('#idealistUL_'+prevPId).find('>li').length;
  var childid=$('#mainIdea_'+ideaId).attr('childid');
  var parentChildId=$('#mainIdea_'+parentId).attr('childid');
  var rootid = $('#idealistUL_'+parentId).attr('rootid');
  var level="";
  if(parentId == 0 || parentId == "0"){
    $('#mainIdea_'+ideaId).removeClass('pt-2 p-0 rounded parent_li ideadrop').addClass('pb-3 pt-4 ideadrag');
    if((parentId == 0 || parentId == "0") && place == "sub"){
      level="sub";
    }else{
      level="main";
    }
    $('#mainIdea_'+ideaId).attr('place','main');
  }else if(parentId != 0 || parentId != "0"){
    $('#mainIdea_'+ideaId).removeClass('pb-3 pt-4 ideadrag').addClass('drag-handle pt-2 p-0 rounded ideadrop');
    $('#mainIdea_'+ideaId).attr('place','sub');
    level="sub";
    $('#mainIdeaExpColl_'+parentId).show().attr('src','images/task/minus.svg');
  }
  
  $('#mainIdea_'+ideaId).attr('parent_idea_id',parentId);
  
  var orderdata = getOrderData(ideaId,parentId);
  let jsonbody = {
		"idea_id" : ideaId,
    "user_id" : userIdglb,
    "parent_idea_id" : parentId,
    "topIdeaId" : rootid,
    "project_id" : prjid,
    "type" : "Project",
    "topicType" : projType,
    "subAction" : "IdeaRefresh",
    "companyId" : companyIdglb,
    "levelType" : level,
    "ideaDetails":JSON.parse(orderdata)
	}
  
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
	console.log("jsonbody-->"+JSON.stringify(jsonbody));
  $.ajax({
		url: apiPath + "/" + myk + "/v1/updateHorizontalOrder",
    type: "POST",
    //dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
      console.log("parentId-->"+parentId);
        var childidfromresponse="";
        if(parentId == 0 || parentId == "0"){
          //$('#mainIdea_'+ideaId).find('span').css('background-color','#f6b847');
          $('#mainIdea_'+ideaId).removeAttr('style');
          $('#mainIdea_'+ideaId).children('span').attr('onclick','openIdea('+ideaId+')');
          $('#mainIdea_'+ideaId).attr('rootid',ideaId);
          $('#mainIdea_'+ideaId).css({"width": "auto", "height": "auto"});  
          var $elem = $('#mainIdea_'+ideaId);
          $elem[0].style.setProperty('padding-top', '30px', 'important');
          $elem[0].style.setProperty('padding-bottom', '25px', 'important');
          var cid = $('#mainIdea_'+ideaId).attr('childid');
          if(cid!=""){
            $('#mainIdea_'+ideaId).children('span').append("<img src=\"images/idea/expand_all2.svg\" class=\"expandCollapseImgIdea\" onclick=\"ideaExpandAll('singleIdea',"+ideaId+","+cid+");event.stopPropagation();\" style=\"width:10px;height:10px;position: absolute;right: 5px;top: 5px;cursor:pointer;\">");
          }
        }else{
          childidfromresponse=result.split("@#@")[1];
          parentChildId=childidfromresponse;
          $('#mainIdea_'+parentId).attr('childid',childidfromresponse);
          $('#mainIdea_'+ideaId).css('width','min-content');
          $('#mainIdea_'+parentId).children('span').append("<img src=\"images/idea/2.svg\" onclick=\"ideaExpandAll('singleIdea',"+parentId+","+parentChildId+");event.stopPropagation();\" style=\"width:10px;height:10px;position: absolute;right: 5px;top: 5px;cursor:pointer;\">");
        } 
        if(typeof(parentChildId)!="undefined" && parentChildId!=""){
          var disabledrag = $('#mainIdea_'+parentId).attr('disableclass');
          $('#mainIdeaExpColl_'+parentId).attr('src','images/task/minus.svg');
          subList(parentId,'',disabledrag,childidfromresponse);
          $('#mainIdea_'+ideaId).first().remove();
          $('#mainIdea_'+ideaId).children('span').attr('onclick','openSubIdea('+ideaId+','+parentId+',"")');
          
          
        }
        if(previdealiLength == 0){
          $('#mainIdeaExpColl_'+prevPId).hide();
          $('#mainIdea_'+prevPId).children('span').children('img').remove();
        }
        
        
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
    }  
	});   
}



function nodeDragnDrop(){
  var nestedSortables = [].slice.call(document.querySelectorAll('.mytree'));
  var sortables = [];
  for(var i=0; i< nestedSortables.length; i++){
    sortables[i] = new Sortable(nestedSortables[i], {
      group: 'nested',
      animation: 150,
      fallbackOnBody: true,
      filter: ".disabledragndrop",
      swapThreshold: 0.65,
    });
  }
} 



function listIdeas() {
  
  var localOffsetTime = getTimeOffset(new Date()).toString();
  $('#loadingBar').addClass('d-flex');
  /* let jsonbody=	{
      "project_id": prjid,
      "type": "Project",
      "sortVal": "",
      "localOffsetTime": localOffsetTime,
      "subAction": "IdeaLoad",
      "projectUsersStatus": projectUsersStatus,
      "topicType" : projType,
      "user_id" : userIdglb
  } */
  //console.log(index + "---" + limit);

  $.ajax({
    //url: apiPath + "/" + myk + "/v1/fetchIdea",
    url: apiPath + "/" + myk + "/v1/fetchIdeaNew?parentIdeaId=0&projectId="+prjid+"&userId="+userIdglb+"&type=Project&ideaId=&localoffsetTimeZone="+localOffsetTime+"&projUserStatus="+projectUsersStatus+"&searchTxt",
    type: "POST",
    //dataType: 'json',
    contentType: "application/json",
    //data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
     
     $('.mytree1').append(prapareIdeaUI(result));
     nodeDragnDrop();
     $('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });
}

function prapareIdeaUI(result){
  var ui="";
  var bgcolor="";
  var disableclass="";var disableclass1="";
  for(var i=0;i<result.length;i++){
    //onmouseout='rearrange(this,"+result[i].idea_id+",event);event.stopPropagation();' ondrop='updateIdeaDragnDrop("+result[i].idea_id+");event.stopPropagation();'
    bgcolor = result[i].color_code;
    if(bgcolor==""){
      bgcolor="#f6b847";
    }
    if(result[i].idea_locked=="N"){
      disableclass="";
      disableclass1="mytree";
    }else{
      disableclass="disabledragndrop";
      disableclass1="disabledragndrop";
    }
    ui+="<li id='mainIdea_"+result[i].idea_id+"' place='main' rootid='"+result[i].idea_id+"' parent_idea_id='0' childid=\""+result[i].childrenIdea+"\" iComplete='"+result[i].idea_completion+"'  ondragend='updateIdeaDragnDrop("+result[i].idea_id+");event.stopPropagation();' onmouseover='hideideapriorityoptions(event);event.stopPropagation();' class='drag-handle ideadrag parent_li position-relative "+disableclass+" idealistclass' style='height:auto;width: auto;padding-top: 30px !important;padding-bottom: 25px !important;' disableclass='"+disableclass+"'>"
      +"<span onclick='openIdea("+result[i].idea_id+",\""+disableclass+"\");closeReorder("+result[i].idea_id+");event.stopPropagation();' class='border-0 defaultExceedCls ' style='position:relative;font-size: 12px;width: 200px;color: black;text-align: left;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);border-radius: 8px;background-color:"+bgcolor+";' title='"+result[i].idea_title+"'>"+result[i].idea_title+""
      if(typeof(result[i].childrenIdea) != "undefined" && result[i].childrenIdea != ""){
        ui+="<img class='expandCollapseImgIdea' src='images/idea/expand_all2.svg' title='Expand All' onclick='ideaExpandAll(\"singleIdea\","+result[i].idea_id+","+result[i].childrenIdea+");event.stopPropagation();' >"
      }  
      ui+="</span>"
      +"<div id='ideaReorderdiv_"+result[i].idea_id+"' class='position-absolute d-none ideareorder' style='border-radius: 50px;border: 1px solid rgb(161, 161, 161);background-color: rgb(255, 255, 255);width: 35px;height: 35px;top:32px;left: 115px ;z-index: 100;'>"
          +"<div class='my-0 mx-auto' style='height:10px;width:10px;'>"
            +"<img src='/images/idea_old/arrow-top.png' onclick='reorderIdea(\"up\","+result[i].idea_id+");event.stopPropagation();' style='cursor:pointer;height:10px;width:10px;float:left;'>"
          +"</div>"
          +"<div class='' style='height:12px;'>"
            +"<img src='/images/idea_old/arrow-left.png' onclick='reorderIdea(\"left\","+result[i].idea_id+");event.stopPropagation();' style='cursor:pointer;float:left;height:10px;width:10px;margin-top:2px;'>"
            +"<img src='/images/idea_old/arrow-right.png' onclick='reorderIdea(\"right\","+result[i].idea_id+");event.stopPropagation();' style='cursor:pointer;float:right;height:10px;width:10px;margin-top:2px;'>"
          +"</div>"
          +"<div class='my-0 mx-auto' style='height:10px;width:10px;'>"
            +"<img src='/images/idea_old/arrow-down.png' onclick='reorderIdea(\"down\","+result[i].idea_id+");event.stopPropagation();' style='cursor:pointer;height:10px;width:10px;float:left;'>"
          +"</div>"
        +"</div>"
    if(typeof(result[i].childrenIdea) != "undefined" && result[i].childrenIdea != ""){
      ui+="<img src='images/task/plus.svg' id='mainIdeaExpColl_"+result[i].idea_id+"' class='expandIdea position-absolute' onclick=\"loadIdeaSublist("+result[i].idea_id+",'','"+disableclass+"',"+result[i].childrenIdea+");event.stopPropagation();\" style='width: 20px;height: 20px;margin-top: 9px;cursor:pointer;'>"
    }else{
      ui+="<img src='images/task/minus.svg' id='mainIdeaExpColl_"+result[i].idea_id+"' class='expandIdea position-absolute' onclick=\"loadIdeaSublist("+result[i].idea_id+",'','"+disableclass+"');event.stopPropagation();\" style='display:none;width: 20px;height: 20px;margin-top: 9px;cursor:pointer;'>"
    }
    ui+="<div id='openideadiv_"+result[i].idea_id+"' onclick='event.stopPropagation();' class='d-none ideaPopupDiv position-absolute p-1 border' style='border-radius: 8px;z-index:100;width: 250px;background-color: rgb(255, 255, 255);color:black;font-size: 11px;top: 3px;left: 200px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;border-color: rgb(193, 197, 200)!important;height: 90px;display: none;'></div>"
    +"<ul id='idealistUL_"+result[i].idea_id+"' rootid='"+result[i].idea_id+"' class='mytree1  sub-ul "+disableclass1+"' level=2 style='padding-left: 245px;'>"
    if(typeof(result[i].subIdea)!="undefined"){
      ui+=prepareSubIdeaUi(result[i].subIdea,"pid",disableclass,result[i].childrenIdea);
    }else{
     
    }
    ui+="</ul>"
    +"</li>"
  }  
  return ui;
}

function loadIdeaSublist(parentIdeaId,colorCode,disableclass,parentchildid) {
  //if($('#idealistUL_'+parentIdeaId).find(' > li').is(':visible')){
  var a=$('#mainIdeaExpColl_'+parentIdeaId).attr('src');
  if(a.indexOf("images/task/minus.svg") > -1 ){
    $('#mainIdeaExpColl_'+parentIdeaId).attr('src','images/task/plus.svg');
    $('#idealistUL_'+parentIdeaId).find(' > li').remove();
  }else{
    subList(parentIdeaId,colorCode,disableclass,parentchildid);
  }
}

function subList(parentIdeaId,colorCode,disableclass,parentchildid){
  $('#idealistUL_'+parentIdeaId).find(' > li').remove();
  var localOffsetTime = getTimeOffset(new Date()).toString();
  $('#loadingBar').addClass('d-flex');
  var level=$('#idealistUL_'+parentIdeaId).attr('level');
  level++;
  $.ajax({
    //url: apiPath + "/" + myk + "/v1/loadSubIdeas?ideaId=&parentIdeaId="+parentIdeaId+"&topIdeaId="+parentIdeaId+"&projId="+prjid+"&type=Project&topicType="+projType+"&subAction=IdeaSubLoad&localoffsetTimeZone="+localOffsetTime+"&userId="+userIdglb+"",
    url: apiPath + "/" + myk + "/v1/fetchIdeaNew?parentIdeaId="+parentIdeaId+"&projectId="+prjid+"&userId="+userIdglb+"&type=Project&ideaId=&localoffsetTimeZone="+localOffsetTime+"&projUserStatus="+projectUsersStatus+"&searchTxt",
    type: "POST",
    //dataType: 'json',
    contentType: "application/json",
    //data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
        
    },
    success: function (result) { 
      $('#idealistUL_'+parentIdeaId).append(prepareSubIdeaUi(result,parentIdeaId,disableclass,parentchildid));
      $('#mainIdeaExpColl_'+parentIdeaId).attr('src','images/task/minus.svg');
      nodeDragnDrop();
      $('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });
}

function prepareSubIdeaUi(result,parentIdeaId,disableclass,parentchildid){
  var ui="";var class1="";var bgcolor="";var disableclass1="";
  for(var i=0;i<result.length;i++){
      bgcolor = result[i].color_code;
      if(bgcolor==""||bgcolor=="0"){
        bgcolor="#f6b847";
      }
      if(parentIdeaId=="pid"){
        parentIdeaId=result[i].parent_idea_id;
      }
      disableclass1 = disableclass==""?"mytree":disableclass;
      
      ui+="<li id='mainIdea_"+result[i].idea_id+"' place='sub' rootid='"+result[i].rootIdeaId+"' parent_idea_id='"+parentIdeaId+"'  childid=\""+result[i].childrenIdea+"\" iComplete='"+result[i].idea_completion+"' ondragend='updateIdeaDragnDrop("+result[i].idea_id+");event.stopPropagation();' class='childrenid_"+parentchildid+" subListLi drag-handle pt-2 p-0 rounded ideadrop parent_li position-relative "+disableclass+" idealistclass' disableclass='"+disableclass+"' style='width: min-content;'>"
        +"<span onclick='openSubIdea("+result[i].idea_id+","+parentIdeaId+",\""+disableclass+"\");closeReorder("+result[i].idea_id+");event.stopPropagation();'  class='border-0 defaultExceedCls ' style='position:relative;font-size: 12px;width: 200px;color: black;text-align: left;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);border-radius: 8px;background-color:"+bgcolor+";' title='"+result[i].idea_title+"'>"+result[i].idea_title+""
        
        +"</span>"
        +"<div id='ideaReorderdiv_"+result[i].idea_id+"' class='position-absolute d-none' style='border-radius: 50px;border: 1px solid rgb(161, 161, 161);background-color: rgb(255, 255, 255);width: 35px;height: 35px;top:10px;left: 90px ;z-index: 100;'>"
          +"<div class='my-0 mx-auto' style='height:10px;width:10px;'>"
            +"<img src='/images/idea_old/arrow-top.png' onclick='reorderIdea(\"up\","+result[i].idea_id+");event.stopPropagation();' style='cursor:pointer;height:10px;width:10px;float:left;'>"
          +"</div>"
          +"<div class='' style='height:12px;'>"
            +"<img src='/images/idea_old/arrow-left.png' onclick='reorderIdea(\"left\","+result[i].idea_id+");event.stopPropagation();' style='cursor:pointer;float:left;height:10px;width:10px;margin-top:2px;'>"
            +"<img src='/images/idea_old/arrow-right.png' onclick='reorderIdea(\"right\","+result[i].idea_id+");event.stopPropagation();' style='cursor:pointer;float:right;height:10px;width:10px;margin-top:2px;'>"
          +"</div>"
          +"<div class='my-0 mx-auto' style='height:10px;width:10px;'>"
            +"<img src='/images/idea_old/arrow-down.png' onclick='reorderIdea(\"down\","+result[i].idea_id+");event.stopPropagation();' style='cursor:pointer;height:10px;width:10px;float:left;'>"
          +"</div>"
        +"</div>"
      if(typeof(result[i].childrenIdea) != "undefined" && result[i].childrenIdea != ""){
        ui+="<img src='images/task/plus.svg' id='mainIdeaExpColl_"+result[i].idea_id+"' class='expandIdea position-absolute' onclick=\"loadIdeaSublist("+result[i].idea_id+",'"+bgcolor+"','"+disableclass+"',"+parentchildid+");event.stopPropagation();\" style='width: 20px;height: 20px;margin-top: 9px;cursor:pointer;'>"
      }else{
        ui+="<img src='images/task/minus.svg' id='mainIdeaExpColl_"+result[i].idea_id+"' class='expandIdea position-absolute' onclick=\"loadIdeaSublist("+result[i].idea_id+");event.stopPropagation();\" style='display:none;width: 20px;height: 20px;margin-top: 9px;cursor:pointer;'>"
      }
      ui+="<div id='openideadiv_"+result[i].idea_id+"' onclick='event.stopPropagation();' class='d-none ideaPopupDiv position-absolute p-1 border' style='border-radius: 8px;z-index:100;width: 250px;background-color: rgb(255, 255, 255);color:black;font-size: 11px;top: -12px;left: 150px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;border-color: rgb(193, 197, 200)!important;height: 90px;display: none;'></div>"
      +"<ul id='idealistUL_"+result[i].idea_id+"' rootid='"+result[i].rootIdeaId+"' level="+level+" class='mytree1  sub-ul "+disableclass1+"' style='padding-left: 245px;'>"
      if(typeof(result[i].subIdea)!="undefined"){
        ui+=prepareSubIdeaUi(result[i].subIdea,"pid",disableclass,result[i].childrenIdea);
      }else{
        
      }
      ui+="</ul>"
      +"</li>"
  }
  return ui;
}



function openIdea(ideaid,disableclass){
  $("[id^='mainIdea_']").children('span').removeClass('nodeselect');
  $("[class^='createIdeaDiv_']").remove();
  if($('#editIdeaMainDiv_'+ideaid).is(':visible')){
    $('#editIdeaMainDiv_'+ideaid).css('display','none').remove();
    //$('#mainIdea_'+ideaid).children('span').removeClass('nodeselect');
    ideaidglb="";
  }else{
    $('#editIdeaMainDiv_'+ideaidglb).css('display','none').remove();
    //$('#mainIdea_'+ideaidglb).children('span').removeClass('nodeselect');
    $('#mainIdea_'+ideaid).children('span').addClass('nodeselect');
    
    var localOffsetTime = getTimeOffset(new Date()).toString();
    /* let jsonbody=	{
      "project_id": prjid,
      "type": "Project",
      "sortVal": "",
      "localOffsetTime": localOffsetTime,
      "subAction": "IdeaLoad",
      "projectUsersStatus": projectUsersStatus,
      "topicType" : projType,
      "user_id" : userIdglb,
      "typeOfFetch" : "singleIdea",
      "idea_id" : ideaid
    } */
    //$('#loadingBar').removeClass('d-none').addClass('d-flex');
    $.ajax({
      //url: apiPath + "/" + myk + "/v1/fetchIdea",
      url: apiPath + "/" + myk + "/v1/fetchIdeaNew?parentIdeaId=0&projectId="+prjid+"&userId="+userIdglb+"&type=Project&ideaId="+ideaid+"&localoffsetTimeZone="+localOffsetTime+"&projUserStatus="+projectUsersStatus+"&searchTxt",
      type: "POST",
      //dataType: 'json',
      contentType: "application/json",
      //data: JSON.stringify(jsonbody),
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        
      },
      success: function (result) { 

        openIdeaUIpopup(ideaid,result,"MainIdea",disableclass);
        ideaidglb=ideaid;
        //$('#loadingBar').removeClass('d-flex').addClass('d-none');
      }
     
    });  

  }
}

function openSubIdea(ideaid,parentIdeaId,disableclass){
  $("[id^='mainIdea_']").children('span').removeClass('nodeselect');
  if($('#editIdeaMainDiv_'+ideaid).is(':visible')){
    $('#editIdeaMainDiv_'+ideaid).css('display','none').remove();
    //$('#mainIdea_'+ideaid).children('span').removeClass('nodeselect');
    ideaidglb="";
  }else{
    $('#editIdeaMainDiv_'+ideaidglb).css('display','none').remove();
    //$('#mainIdea_'+ideaidglb).children('span').removeClass('nodeselect');
    $('#mainIdea_'+ideaid).children('span').addClass('nodeselect');
    var localOffsetTime = getTimeOffset(new Date()).toString();
    var level=$('#idealistUL_'+parentIdeaId).attr('level');
    level++;
    //$('#loadingBar').removeClass('d-none').addClass('d-flex');
    $.ajax({
      //url: apiPath + "/" + myk + "/v1/loadSubIdeas?ideaId=&parentIdeaId="+parentIdeaId+"&topIdeaId="+parentIdeaId+"&projId="+prjid+"&type=Project&topicType="+projType+"&subAction=IdeaSubLoad&localoffsetTimeZone="+localOffsetTime+"&userId="+userIdglb+"&typeOffecth=singleIdea&ideaIId="+ideaid+"",
      url: apiPath + "/" + myk + "/v1/fetchIdeaNew?parentIdeaId="+parentIdeaId+"&projectId="+prjid+"&userId="+userIdglb+"&type=Project&ideaId="+ideaid+"&localoffsetTimeZone="+localOffsetTime+"&projUserStatus="+projectUsersStatus+"&searchTxt",
      type: "POST",
      //dataType: 'json',
      contentType: "application/json",
      //data: JSON.stringify(jsonbody),
      beforeSend: function (jqXHR, settings) {
        xhrPool.push(jqXHR);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        //$('#loadingBar').addClass('d-none').removeClass('d-flex');
          
      },
      success: function (result) { 
        if(result!=""){
          openIdeaUIpopup(ideaid,result,"SubIdea",disableclass);
          ideaidglb=ideaid;
          
        }
        //$('#loadingBar').removeClass('d-flex').addClass('d-none');
      }
    });    
  }
}

function numFormatter(num) {
  if(num > 999 && num < 1000000){
      return (num/1000).toFixed(1) + 'K';
  }else if(num > 1000000){
      return (num/1000000).toFixed(1) + 'M';
  }else if(num < 900){
      return num;
  }
}

function openIdeaUIpopup(ideaid,result,place,disableclass){
  var ui="";
  $("[id^='ideaReorderdiv_']").removeClass('d-block').addClass('d-none');
  var bgColor=result[0].color_code;
  if(bgColor==""||bgColor=="0"){
    bgColor="#f6b847";
  }
  var priority = result[0].idea_priority;
  var priorityimage="";
  priorityimage = (priority=="")? "images/task/p-six.svg" : priority=="1" ? "images/task/p-one.svg" : priority=="2" ? "images/task/p-two.svg":priority=="3" ? "images/task/p-three.svg" :priority=="4" ? "images/task/p-four.svg": "images/task/p-five.svg";
  var ideaitle = result[0].idea_title;
  var ideaname = result[0].idea_title.replace(/(\r\n|\n|\r)/gm, "");
  var ideaname1 = result[0].idea_title.replace(/(\r\n|\n|\r)/gm, " ");
  //ideanameglb=ideaname;
  var voted_type=result[0].voted_type;
  var likeimg="";var like="";var unlikeimg="";var unlike="";
  var parentIdeaId=result[0].parent_idea_id;
  var childrenid = result[0].childrenIdea;
  var idealock = result[0].idea_locked;
  var customIdeaid = result[0].unique_cust_id;
  var createdByid = result[0].created_by;
  var customIdeaidborder="";
  var likecount = result[0].likeCnt;
  var dislikecount = result[0].dislikeCnt;
  likecount = numFormatter(likecount);
  dislikecount = numFormatter(dislikecount);
  
  if(voted_type == "like"){
    likeimg="images/idea/thumbs_up2.svg";
    like="Like";
    unlikeimg="images/idea/thumbs_down.svg";
    unlike="Unlike";
  }else if(voted_type == "unlike"){
    likeimg="images/idea/thumbs_up.svg";
    like="Like";
    unlikeimg="images/idea/thumbs_down2.svg";
    unlike="Unlike";
  }else{
    likeimg="images/idea/thumbs_up.svg";
    like="Like";
    unlikeimg="images/idea/thumbs_down.svg";
    unlike="Unlike";
  }
  var lockunlockstatus = "";
  if(typeof(disableclass) == "undefined" || disableclass == "" || disableclass=="undefined"){
    lockunlockstatus="N";
  }else{
    lockunlockstatus="Y";
  }
  /* if(parentIdeaId == 0 || parentIdeaId == "0"){
    place="MainIdea";
  }else{
    place="SubIdea";
  } */
  var uShareAcc = result[0].userSharedAccess;
  if(uShareAcc == ""){
    uShareAcc="W";
  } 

  var addIOnclick="";var addIOptionOpac="";
  if((uShareAcc=="W") || (result[0].projectUsersStatus=="PO")){
    addIOnclick="createSubIdea("+ideaid+",\"SubIdea\",\""+bgColor+"\");";
  }else{
    addIOptionOpac="opacity:0.5;";
  }
  if(uShareAcc=="R"){
    if(place != "MainIdea"){
      addIOnclick="createSubIdea("+ideaid+",\"SubIdea\",\""+bgColor+"\");";
    }else{
      addIOptionOpac="opacity:0.5;";
    }
  }
  
  if(result[0].projectUsersStatus!="PO" && uShareAcc=="R"){
    if(place != "MainIdea"){
      addIOnclick="createSubIdea("+ideaid+",\"SubIdea\",\""+bgColor+"\");";
    }else{
      addIOptionOpac="opacity:0.5;";
    }
  }

  var taskOnclick="";var taskOptionOpac="";var docOnclick="";var docOptionOpac="";
  if((uShareAcc=="W") || (result[0].projectUsersStatus=="PO" && place != "MainIdea")){
    taskOnclick="openTaskPopup("+ideaid+",\""+ideaname+"\",\"options\",\""+result[0].projectUsersStatus+"\");";
    docOnclick="trigIdeaDoc(this);";
  }else{
    taskOptionOpac="opacity:0.5;";
    docOptionOpac="opacity:0.5;";
  } 

  var reorderonclick="";var reorderOptionOpac="";
  if(uShareAcc=="W" && result[0].projectUsersStatus=="PO"){
    reorderonclick="showReorderOption("+ideaid+");";
  }else{
    reorderOptionOpac="opacity:0.5;";
  }
  
  var priorityonclick="";var priorityOptionOpac="";var projectonclick="";var projectOptionOpac="";
  if(uShareAcc=="W" && result[0].projectUsersStatus=="PO" && place == "MainIdea"){
    priorityonclick="trigIdeaPriority(this);";
    projectonclick="openProjects("+ideaid+");";
  }else{
    priorityOptionOpac="opacity:0.5;";
    projectOptionOpac="opacity:0.5;";
  }

  var delOnclick="";var delOptionOpac="";
  if(uShareAcc=="W" && result[0].idea_locked=="N"){
    delOnclick ="deleteIdea("+ideaid+");";
  }else{
    delOptionOpac = "opacity:0.5;";
  }


  if(place != "MainIdea"){
    if(result[0].idea_completion=="N" && place != "MainIdea"){
      if(result[0].idea_locked=="N"){
        addIOnclick="createSubIdea("+ideaid+",\"SubIdea\",\""+bgColor+"\");";
        taskOnclick="openTaskPopup("+ideaid+",\""+ideaname+"\",\"options\",\""+result[0].projectUsersStatus+"\");";
        docOnclick="trigIdeaDoc(this);";
      }
    }else{
      if(result[0].idea_locked=="N"){
        addIOptionOpac="opacity:0.5;";
        taskOptionOpac="opacity:0.5;";
        docOptionOpac="opacity:0.5;";
      }  
    }
  }

  ui="<div id='editIdeaMainDiv_"+ideaid+"' onmouseover='hideideapriorityoptions(event);event.stopPropagation();' iLock='"+idealock+"' userShAcc='"+uShareAcc+"' iPlace='"+place+"' createdByid="+createdByid+" userstat='"+result[0].projectUsersStatus+"' style='position:absolute;z-index:100;border-radius: 8px;top: 3px;left: 190px;'>"
    
    +'<div class="editIdeaDiv " style="">'
      
      +"<div class='editideadivcontent p-1' style='position:relative;border-radius: 8px;width: 500px; height:auto;color: black;font-size: 12px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;border-color: rgb(193, 197, 200) !important;display: block;background-color:"+bgColor+";'>"
        +"<div class='d-flex align-items-center' style='height: 20px;'>"
          +"<img src='images/menus/close3.svg' onclick=\"closeEditIdea("+ideaid+",'"+ideaname+"');\" style='width: 10px;position: absolute;cursor:pointer;right:18px;top:15px;'>"
        +"</div>"
        +"<div class='d-flex align-items-center ml-3 mr-1 mt-2' style='height: 30px;'>"
          +"<div class='d-flex mr-auto' style=''>"
            +"<p class='py-2 px-0 m-0' style='font-size: 12px;'>ID :</p>"
            +"<input type='text' id='customid_"+ideaid+"' onclick='editCustomidinput(this,"+ideaid+");' cIdeaid='"+customIdeaid+"' onblur='blurCustomidinput(this,\"update\",\""+customIdeaid+"\");' onkeydown='updateCid("+ideaid+",event);' value='"+customIdeaid+"' class='mt-1 ml-2 mr-auto' style='cursor:pointer;border:none;height: 20px;outline:none;width: 50%;padding-top: 5px;background-color:"+bgColor+";' autocomplete='off'>"
          +"</div>" 
          if(place == "MainIdea"){
            ui+="<div class='px-1' id='ideapriority_"+ideaid+"' priority='"+priority+"' onclick='showideapriorityoptions(\"edit\","+ideaid+")' style='display:none;'><img src='"+priorityimage+"' id='ideapriorityimg_"+ideaid+"' class='image-fluid ideaOptionIcons'  title='' style='position:relative;'>"
              +"<div id='iPriorityDiv_"+ideaid+"' class='p-1' style='position: absolute;display:none;color: black;background: rgb(255, 255, 255);border: 1px solid rgb(166, 166, 166);padding: 7px;height: 140px;overflow: auto;border-radius: 2px;font-size: 12px;width: 150px;z-index: 3;text-align: left;'></div>"
            +"</div>"
          }
          ui+="<div class='px-1' id='ideacomment_"+ideaid+"' onclick='openComments("+ideaid+",\"\",\"\");' style='display:none;'><img src='/images/task/task_comments.svg' title='View Comments'  class='image-fluid ideaOptionIcons'  ></div>"
          +"<div class='px-1' id='ideatask_"+ideaid+"' onclick=\"openTaskPopup("+ideaid+",'"+ideaname+"','icons','"+result[0].projectUsersStatus+"');\" style='display:none;'><img src='images/idea/task_nofill.svg' title='View Tasks' class='image-fluid ideaOptionIcons' ></div>"
          if(place == "MainIdea"){
            ui+="<div class='px-1' id='ideashare_"+ideaid+"'  style='' onclick='ideaShare("+ideaid+");'><img src='images/idea/share_nofill.svg' title='Share' class='image-fluid ideaOptionIcons' ></div>"
          }
          ui+="<div class='px-1' id='ideadocument_"+ideaid+"'  onclick='listIdeaLinksDocs("+ideaid+");' style='display:none;'><img src='images/idea/document.svg' tite='View Documents' class='image-fluid ideaOptionIcons' ></div>"
          if(place == "MainIdea"){
            if(idealock == "N"){
              ui+="<div class='px-1' id='idealock_"+ideaid+"' style='display:none;'><img src='images/idea/unlock.svg' onclick='lockunlockIdea("+ideaid+",\"Y\","+childrenid+")' class='image-fluid ideaOptionIcons' title='Lock' ></div>"
            }else{
              ui+="<div class='px-1' id='idealock_"+ideaid+"' style='display:none;'><img src='images/idea/lock.svg' onclick='lockunlockIdea("+ideaid+",\"N\","+childrenid+")' class='image-fluid ideaOptionIcons' title='Unlock' ></div>"
            }
          }
          ui+="<div class='px-1' id='completeidea_"+ideaid+"' comletestat='N' style='display:none;'><img src='images/task/completed.svg' class='image-fluid ideaOptionIcons' title='Completed' ></div>"
          +"<div class='px-1 ml-2 position-relative'>"
            +"<img id='ideamoreoptdivimg_"+ideaid+"' src='images/conversation/three_dots.svg' onclick='showideaoption("+ideaid+");' class='' style='width:22px;cursor:pointer;'>"
            +"<div class='position-absolute rounded' id='ideamoreoptdiv_"+ideaid+"' onmouseover='hideideapriorityoptions(event,"+ideaid+");event.stopPropagation();' style='width: 105px;border: 1px solid #c1c5c8;background-color: #fff;color: black;font-size:12px;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);display:none;left:40px;top: -16px;'>"
              +"<div id='addidea_"+ideaid+"' class='pl-3 py-1 pt-2 ideaoption' onclick='"+addIOnclick+"' style='"+addIOptionOpac+"'><span  style='cursor:pointer;'>Add Idea</span></div>"
              +"<div id='reorder_"+ideaid+"' class='pl-3 py-1 ideaoption' onclick='"+reorderonclick+"event.stopPropagation();' style='"+reorderOptionOpac+"'><span  style='cursor:pointer;'>Reorder</span></div>"
              +"<div id='complete_"+ideaid+"' class='pl-3 py-1 ideaoption' onclick='completeIdea("+ideaid+",\"edit\");'><span  style='cursor:pointer;'>Complete</span></div>"
              +"<div class='pl-3 py-1 ideaoption d-flex align-items-center position-relative' id='ideEditPriorityDiv' onmouseover='"+priorityonclick+"event.stopPropagation();' style='"+priorityOptionOpac+"' ><span  style='cursor:pointer;'>Priority</span>"    //<img src='images/arrow.png' class='ml-auto mr-1' style='width: 8px;float: right;height: 15px;'>
                +"<div id='iEditPriorityDiv' class='p-1' style='position: absolute;display:none;color: black;background: rgb(255, 255, 255);border: 1px solid rgb(166, 166, 166);padding: 7px;overflow: auto;border-radius: 2px;font-size: 12px;width: 150px;z-index: 3;text-align: left;left:103px;top:0px;'></div>"
              +"</div>"
              +"<div id='project_"+ideaid+"' class='pl-3 py-1 ideaoption' onclick='"+projectonclick+"' style='"+projectOptionOpac+"'><span style='cursor:pointer;'>Copy</span></div>"
              +"<div id='comment_"+ideaid+"' class='pl-3 py-1 ideaoption' onclick='openComments("+ideaid+",\"\",\"\");'><span style='cursor:pointer;'>Comment</span></div>"
              +"<div id='taskeditIdea_"+ideaid+"' class='pl-3 py-1 ideaoption' onclick='"+taskOnclick+"' style='"+taskOptionOpac+"'><span style='cursor:pointer;'>Task</span></div>"
              +"<div class='pl-3 py-1 ideaoption d-flex align-items-center position-relative' id='ideaEditDocumentoptiondiv' onmouseover='"+docOnclick+"event.stopPropagation();' style='"+docOptionOpac+"'><span onclick='' style='cursor:pointer;'>Documents</span>"    //<img src='images/arrow.png' class='ml-auto mr-1' style='width: 8px;float: right;height: 15px;'>
                +"<div id='iDocumentDiv' class='p-1' style='position: absolute;display:none;font-size: 11px;color: black;background: rgb(255, 255, 255);border: 1px solid rgb(166, 166, 166);padding: 7px;overflow: auto;border-radius: 2px;font-size: 12px;width: 145px;z-index: 3;text-align: left;left:103px;top:0px;'></div>"
              +"</div>"
              +"<div id='color_"+ideaid+"' class='colormaindiv pl-3 py-1 ideaoption position-relative d-flex align-items-center' onmouseover='showEditColor("+ideaid+");'><span  style='cursor:pointer;'>Color</span>"  //<img src='images/arrow.png' class='ml-auto mr-1' style='width: 8px;float: right;height: 15px;'>
                +"<div class='position-absolute rounded colorOptiondiv p-2 border' id='colorOptiondiv_"+ideaid+"' style='background-color: #fff;color: black;font-size:11px;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);left:103px;top: -50px;width: 182px;display:none;'>"
                  +"<div class='rounded  align-items-center w-50 d-none' style='border: 1px solid #C1C5C8;'>"
                    +"<div id='colorbg_"+ideaid+"' class='m-1 border colorplace' style='height:15px;width:15px;background-color:"+bgColor+"'></div>"
                    +"<span id='colorname_"+ideaid+"' class='m-1' style='font-size:12px;'>"+bgColor+"</span>"
                  +"</div>"
                  +"<div id='colorblock_"+ideaid+"' class='pt-1'>"
                  +"</div>"
                  /* +'<div id="wantMore" class="wantMoreColor d-flex justify-content-center pt-2">'
                    +'<button type="button" id="showMore" onclick="showMoreColor();" class="btn btn-sm rounded">Show Colors</button>'
                  +'</div>' */
                +"</div>"
              +"</div>"
              +"<div id='share_"+ideaid+"' class='pl-3 py-1 ideaoption' onclick='ideaShare("+ideaid+");'><span style='cursor:pointer;'>Share</span></div>"
              +"<div id='story_"+ideaid+"' class='pl-3 py-1 ideaoption' ><span onclick='' style='cursor:pointer;'>Story</span></div>"
              +"<div id='delete_"+ideaid+"' class='pl-3 py- pb-2 ideaoption' onclick='"+delOnclick+"' style='"+delOptionOpac+"'><span  style='cursor:pointer;'>Delete</span></div>"
              
            +"</div>"
          +"</div>"
        +"</div>"
        +"<div class='mx-auto d-flex align-items-start pt-2' style='height: 175px;'>"
          +"<textarea id='ideaInput_"+ideaid+"' value='"+ideaitle+"' onclick='clickinput("+ideaid+");' ondblclick='showideaedittextarea("+ideaid+",event);' class='ml-3 mr-3 m-0 p-0 w-100 wsScrollBar' title='' onkeyup='ideaTextareGrow(event,this);' onkeydown='updateonevent(event,"+ideaid+");' placeholder='Enter your idea...' style='user-select: none;height: 170px;font-size: 12px;background-color: #f6b847;outline: none;border:none;resize:none;cursor:pointer;background-color:"+bgColor+";' autocomplete='off' readonly>"+ideaitle+"</textarea>"
        +"</div>"
        +"<div class='d-flex align-items-center w-100 mt-1' style='height: 30px;'>"
          +"<div class='ml-3 w-25 d-flex align-items-center'>"
            +"<div id='idealike_"+ideaid+"' class='d-flex align-items-center' style=''><img id='idealikeimg_"+ideaid+"' src='"+likeimg+"' title='"+like+"' onclick='updatelikeimage(this,\"edit\","+ideaid+");'  class='image-fluid ideaOptionIcons' title='Like' ><span id='likespan_"+ideaid+"' class='pl-1'>"+likecount+"</span></div>"
            +"<div id='ideaunlike_"+ideaid+"' class='ml-1 pl-3 d-flex align-items-center' style=''><img id='ideaunlikeimg_"+ideaid+"' src='"+unlikeimg+"' title='"+unlike+"' onclick='updateunlikeimage(this,\"edit\","+ideaid+");'  class='image-fluid ideaOptionIcons' title='Unlike' ><span id='unlikespan_"+ideaid+"' class='pl-1'>"+dislikecount+"</span></div>"
          +"</div>"
          +"<div class='mr-3 w-75' id='saveEditDiv_"+ideaid+"' style='padding: 5px 0px;display:none;'>"
              +"<img id=\"\" title=\"Save\" class=\"ml-2\" src=\"/images/task/tick.svg\" onclick=\"updateIdea("+ideaid+");\" style=\"float:right;height:25px;width:25px;cursor:pointer;\">"
              +"<img id=\"\" title=\"Cancel\" class=\"\" src=\"/images/task/remove.svg\" onclick=\"closeEditfield("+ideaid+",'"+bgColor+"','"+ideaname1+"');\" style=\"float:right;height:25px;width:25px;cursor:pointer;\">"
          +"</div>"
      
      +"</div>"
      
    +"</div>" 
    
    +"<input id='nodecolorold_"+ideaid+"' type='hidden' value='"+bgColor+"'>"
    +"<input id='nodecolornew_"+ideaid+"' type='hidden' value='"+bgColor+"'>"
    
  +'</div>'
  
  +"</div>"

  $('#mainIdea_'+ideaid).append(ui);

  if(priority!=0 || priority!="0"){
    $('#ideapriority_'+ideaid).css('display','block');
  }
  if(result[0].likeCnt!=0 || result[0].likeCnt!="0"){
    
  }
  if(result[0].ideaDocs!=0 || result[0].ideaDocs!="0" || result[0].ideaLink!=0 || result[0].ideaLink!="0"){
    $('#ideadocument_'+ideaid).css('display','block');
  }
  if(result[0].ideaStory!=0 || result[0].ideaStory!="0"){
    
  }
  if(result[0].ideaCmts!=0 || result[0].ideaCmts!="0"){
    $('#ideacomment_'+ideaid).css('display','block');
  }
  if(result[0].ideaTasks!=0 || result[0].ideaTasks!="0"){
    $('#ideatask_'+ideaid).css('display','block');
  }
  if(result[0].idea_completion=="Y"){
    $('#completeidea_'+ideaid+'').css('display','block');
    $('#complete_'+ideaid+'').children('span').text('Incomplete');
    $('#addidea_'+ideaid+',#reorder_'+ideaid+', #project_'+ideaid+' ,#taskeditIdea_'+ideaid+', #share_'+ideaid+', #story_'+ideaid+'').removeAttr('onclick').css('opacity','0.3');
    $('#ideaEditDocumentoptiondiv, #color_'+ideaid).removeAttr('onmouseover').css('opacity','0.3'); 

    //$('#customid_'+ideaid+',#ideapriority_'+ideaid+', #ideadocument_'+ideaid+', #ideatask_'+ideaid+', #ideashare_'+ideaid+', #idealikeimg_'+ideaid+', #ideaunlikeimg_'+ideaid).removeAttr('onclick').css('opacity','0.3');
    $('#ideaInput_'+ideaid).removeAttr('ondblclick').css('opacity','0.3');
    //$('#customid_'+ideaid).removeAttr('onkeydown');

  }
  if(childrenid != ""){
    $('#idealock_'+ideaid).css('display','block');
  } 
  if(idealock == "Y"){
    $('#addidea_'+ideaid+',#reorder_'+ideaid+', #complete_'+ideaid+', #project_'+ideaid+' ,#taskeditIdea_'+ideaid+', #share_'+ideaid+', #story_'+ideaid+', #delete_'+ideaid+'').removeAttr('onclick').css('opacity','0.3');
    $('#ideEditPriorityDiv , #ideaEditDocumentoptiondiv, #color_'+ideaid).removeAttr('onmouseover').css('opacity','0.3');
    /* $('#customid_'+ideaid+',#ideapriority_'+ideaid+', #ideadocument_'+ideaid+', #ideatask_'+ideaid+', #ideashare_'+ideaid+', #idealikeimg_'+ideaid+', #ideaunlikeimg_'+ideaid).removeAttr('onclick').css('opacity','0.3');
    $('#ideaInput_'+ideaid).removeAttr('ondblclick').css('opacity','0.3');
    $('#customid_'+ideaid).removeAttr('onkeydown'); */

  }
  if(place == "SubIdea"){
    if($('#mainIdea_'+ideaid).attr('disableclass') == "disabledragndrop"){
      $('#addidea_'+ideaid+',#reorder_'+ideaid+', #complete_'+ideaid+', #project_'+ideaid+' ,#taskeditIdea_'+ideaid+', #share_'+ideaid+', #story_'+ideaid+', #delete_'+ideaid+'').removeAttr('onclick').css('opacity','0.3');
      $('#ideEditPriorityDiv , #ideaEditDocumentoptiondiv, #color_'+ideaid).removeAttr('onmouseover').css('opacity','0.3');
      /* $('#customid_'+ideaid+',#ideapriority_'+ideaid+', #ideadocument_'+ideaid+', #ideatask_'+ideaid+', #ideashare_'+ideaid+', #idealikeimg_'+ideaid+', #ideaunlikeimg_'+ideaid).removeAttr('onclick').css('opacity','0.3');
      $('#ideaInput_'+ideaid).removeAttr('ondblclick').css('opacity','0.3');
      $('#customid_'+ideaid).removeAttr('onkeydown'); */
    }
  }
  
  
  ideaPriorityUi('editoption',ideaid);
  iShowDocuploadOptions(ideaid);
  //showIdeaColorBox(ideaid,bgColor);
  colorGridView(ideaid,'ideaEdit');
  //lockUnlockIdeaoptions(ideaid,lockunlockstatus,result[0].parent_idea_id);
  hideSubideaOptions(parentIdeaId,ideaid);
  //colorpicker();
  var elmnt = document.getElementById("editIdeaMainDiv_"+ideaid);
  elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
}

function updateonevent(event,ideaid){
  if(event.ctrlKey && event.keyCode == 13){
    updateIdea(ideaid);
  }
}


function closeEditfield(ideaid,bgColor,ideaname){
  $('#ideaInput_'+ideaid).attr('readonly',true);
  $('#ideaInput_'+ideaid).val(ideaname);
  $('#editIdeaMainDiv_'+ideaid).find('.editideadivcontent, #customid_'+ideaid+', #ideaInput_'+ideaid).css('background-color',bgColor);
  $('#mainIdea_'+ideaid).children('span').css('background-color',bgColor);
  $('.colortickimage').hide();
  $('#saveEditDiv_'+ideaid).hide();
  $('.colorbox').css('border','none');
}

function editCustomidinput(obj,ideaid){
  $(obj).css('border-bottom', '1px solid #AAAAAA');
  if($(obj).val() === "-"){
    $(obj).val('');
  }
}

function blurCustomidinput(obj,place,customIdeaid){
  if(place=="create"){
    //if($(obj).val() == ""){ 
      /* if($(obj).val() == ""){
        customIdeaid="-";
      }else{ */
        customIdeaid=$(obj).val();
      //}
      $(obj).val(customIdeaid).css('border-bottom','none');
    /* }else{
      $(obj).css('border-bottom','1px solid black');
    }   */
  }else{
    
    /* if($(obj).val().trim() == "" || customIdeaid == "-"){
      $(obj).css('border-bottom','none');
    }else{
      $(obj).css('border-bottom','1px solid black');
    }   */
    $(obj).val(customIdeaid).css('border-bottom','none');;
  } 
  
  
}

function openComments(ideaid,latestid,feedid){
  var localOffsetTime = getTimeOffset(new Date()).toString();
  let jsonbody = {
    "feedType" : "wsIdea",
    "localOffsetTime" : localOffsetTime,
    "idea_comment_id" : latestid,		
    "menuTypeId" : ideaid,		
    "project_id" : prjid
  }
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  $.ajax({
    url: apiPath + "/" + myk + "/v1/getFeedDetails",
    type: "POST",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
      var UI=prepareCommentsUI(result, menuutype, prjid,'');
      feedid=parseInt(feedid);
      var ideatitle = $('#mainIdea_'+ideaid).children('span').text();
      if(latestid == ""){
        //ideaCommentsUI(ideaid,ideatitle);
        popupCommentsUI(ideaid,ideatitle,'Idea');
        AddSubComments(0,'',ideaid);
        $('body').find('#popupCommentsDivbody').append(UI);
        $('#replyBlock_0').focus();
      }else{
        $("#replyBlock_"+feedid).val('');
				if(feedid==0){
          $(UI).insertAfter("#replyDivContainer_"+feedid).show();
				}else{
          $("#replyDivContainer_"+feedid).remove();
					$(UI).insertAfter('#actFeedDiv_' +feedid).show('slow');
				}
        
      }
      
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
    }
  });
}

function ideaCommentsUI(ideaid,ideatitle){
	var ui="";
	$('#glbCommentsDiv').html('');
  $("#transparentDiv").show();
  
  ui='<div class="ideaComments" id="" ideaid='+ideaid+'>'
    +'<div class="" style="">'
      +'<div class="modal-content pl-2 pr-1 wsScrollBar" style="overflow: auto;z-index:1000 !important;max-height: 550px;height: 550px;width:88%;margin-left: 120px;font-size:12px;">'
        +'<div class="modal-header pt-2 pb-0 px-0" style="border-bottom: 1px solid #b4adad;min-height:30px;">'
          +'<div class="d-none  align-items-center pb-1" style="">'
            +'<div class="pl-1 mr-1" style="">Idea Comments</div>'//<img class="" onclick="closeIdeaComents();event.stopPropagation();" src="images/task/dowarrow.svg" style="height:15px;width:15px; cursor:pointer;margin-top:2px">
          +'</div>'
          //+'<button type="button" onclick="closeIdeaComents();" class="close p-0 ideaCloseButton" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
          +'<img src="images/menus/close3.svg" onclick="closeIdeaComents();" class="ml-auto" style="cursor:pointer;width:10px;height:10px;margin-top:1px;margin-right:0px;">'
        +'</div>'
        +'<div class="d-flex  align-items-center py-1 w-100 task_type_divison" style="color:white;font-size: 14px;">'
          +'<div class="defaultExceedCls" style="width:99%;">'
            +'<img src="/images/menus/ideas.svg" class="ml-1" style="width:17px;height:17px;">'
            +'<span class="pl-2" style="">Idea Comments - </span>'//<img class="" onclick="closeIdeaComents();event.stopPropagation();" src="images/task/dowarrow.svg" style="height:15px;width:15px; cursor:pointer;margin-top:2px">
            +'<span class="pl-1" style="width:88%;">'+ideatitle+'</span>'
          +'</div>'
        +'</div>'
        +'<div id="ideacommentsDivbody" class="modal-body py-0 px-1 justify-content-center" style="height: auto;">'
        +'</div>'
      +'</div>'
    +'</div>'
  +'</div>'

  $('#glbCommentsDiv').append(ui).show();
}

function listIdeaLinksDocs(ideaid){
  let jsonbody = {
    "idea_id" : ideaid,
    "user_id" : userIdglb
  }
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  $.ajax({
    url: apiPath + "/" + myk + "/v1/listIdeaDocs",
    type: "POST",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 

      if(result != ""){
        prepareIdeaDocsLinksUi(result);
      }
      
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
    }
  });    
}



function closeIdeaComents(){
  $('#glbCommentsDiv').html('').hide();
  $("#transparentDiv").hide();
}
function hideSubideaOptions(parentIdeaId,ideaid){
  if(parentIdeaId != "0"){
    $('#ideEditPriorityDiv').removeAttr('onmouseover').css('opacity','0.3');
    $('#project_'+ideaid+', #story_'+ideaid+'').removeAttr('onclick').css('opacity','0.3');
  }
}

function showReorderOption(ideaid){
  $('#ideaReorderdiv_'+ideaid).removeClass('d-none').addClass('d-block');
  $('#editIdeaMainDiv_'+ideaid).css('display','none').remove();
  $('#mainIdea_'+ideaid).children('span').removeClass('nodeselect');
  ideaidglb1=ideaid;
}
function closeReorder(ideaid){
  $('#ideaReorderdiv_'+ideaid).removeClass('d-block').addClass('d-none');
  ideaidglb1="";
}

function reorderIdea(movetype,ideaId){
  var nextList = "";
  var level="";
  $('#ideaReorderdiv_'+ideaId).addClass('d-none').removeClass('d-block');
		if(movetype == "up"){
			if($('#mainIdea_'+ideaId).prevAll('.idealistclass:visible').length){
				nextList = $('#mainIdea_'+ideaId).prev('.idealistclass:visible').attr('id').trim().split('_')[1];
				if(typeof(nextList)!='undefined'){
					var listData = $('#mainIdea_'+ideaId).clone();
					$('#mainIdea_'+ideaId).remove();
					$(listData).insertBefore('#mainIdea_'+nextList);
					updateOrderIdea(ideaId,nextList);
				}
			}else if($('#mainIdea_'+ideaId).prev('.sub-ul:visible').length){
				nextList = $('#mainIdea_'+ideaId).prev('.sub-ul:visible').attr('id').trim().split('_')[1];
				if(typeof(nextList)!='undefined'){
					var listData = $('#mainIdea_'+ideaId).clone();
					$('#mainIdea_'+ideaId).remove();
					$(listData).insertBefore('#mainIdea_'+nextList);
          updateOrderIdea(ideaId,nextList);
				}
			}else{
				alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
			}
		}else if(movetype == "down"){
			if($('#mainIdea_'+ideaId).nextAll('.idealistclass:visible').length){
				nextList = $('#mainIdea_'+ideaId).nextAll('.idealistclass:visible').attr('id').trim().split('_')[1];
				if(typeof(nextList)!='undefined'){
					var listData = $('#mainIdea_'+ideaId).clone();
					$('#mainIdea_'+ideaId).remove();
					$(listData).insertAfter('#mainIdea_'+nextList);
					updateOrderIdea(ideaId,nextList);
				}
			}else if($('#mainIdea_'+ideaId).next('.sub-ul:visible').length){
				nextList = $('#mainIdea_'+ideaId).next('.sub-ul:visible').attr('id').trim().split('_')[1];
				if(typeof(nextList)!='undefined'){
					var listData = $('#mainIdea_'+ideaId).clone();
					$('#mainIdea_'+ideaId).remove();
					$(listData).insertAfter('#mainIdea_'+nextList);
          updateOrderIdea(ideaId,nextList);
				}
			}else{
				alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
			}
		}else if(movetype == "left"){
			if($('#mainIdea_'+ideaId).parents('ul[id^=idealistUL_]').parents('li.idealistclass').length){
				var locked = $('#mainIdea_'+ideaId).attr('disableclass');
        var place=$('#mainIdea_'+ideaId).attr('place');
				nextList = $('#mainIdea_'+ideaId).parents('ul[id^=idealistUL_]').attr('id').trim().split('_')[1];
				var parentIdeaId = $('#mainIdea_'+nextList).attr('parent_idea_id');
				var topIdeaId = $('#mainIdea_'+ideaId).parents('.idealistclass').attr('id').trim().split('_')[1];
				var liData = $('#mainIdea_'+ideaId).clone();
				$('#mainIdea_'+ideaId).remove();
        console.log("nextLi"+nextList);
        console.log("parentIdeaId"+parentIdeaId);
        console.log("topIdeaId"+topIdeaId);
				if(parentIdeaId == 0){
					$(liData).insertAfter('#mainIdea_'+topIdeaId);
					$('#mainIdea_'+ideaId).removeClass('sub-ul').addClass('row idealistclass');
				}else{
				  //$(liData).insertAfter('#mainIdea_'+nextList);
				}
        
				if(parentIdeaId == 0 || parentIdeaId == "0"){
          //$('#mainIdea_'+ideaId).removeClass('pt-2 p-0 rounded parent_li ideadrop').addClass('pb-3 pt-4 ideadrag');
          if((parentIdeaId == 0 || parentIdeaId == "0") && place == "sub"){
            level="sub";
          }else{
            level="main";
          }
          $('#mainIdea_'+ideaId).attr('place','main');
          $('#mainIdea_'+ideaId).addClass('mainideaclass');
          $('#ideaReorderdiv_'+ideaId).css({"top": "32px", "left": "115px"});  
          $('#mainIdea_'+ideaId).children('span').attr('onclick','openIdea('+ideaId+')');
          $('#mainIdea_'+ideaId).removeClass().addClass('drag-handle ideadrag parent_li position-relative  idealistclass mainideaclass');
          
        }else if(parentIdeaId != 0 || parentIdeaId != "0"){
          $('#mainIdea_'+ideaId).removeClass('pb-3 pt-4 ideadrag').addClass('drag-handle pt-2 p-0 rounded ideadrop');
          $('#mainIdea_'+ideaId).attr('place','sub');
          level="sub";
          $('#mainIdeaExpColl_'+parentIdeaId).show().attr('src','images/task/minus.svg');
          $('#mainIdea_'+ideaId).removeClass('mainideaclass');
          $('#mainIdea_'+ideaId).children('span').attr('onclick','openSubIdea('+ideaId+','+parentIdeaId+',"")');
        }	
        $('#mainIdea_'+ideaId).attr('parent_idea_id',parentIdeaId);
				$('#mainIdea_'+ideaId).removeAttr('style');
        if($('#idealistUL_'+topIdeaId).children('li').length == 0 && $('#mainIdeaExpColl_'+topIdeaId).attr('src') == "images/task/minus.svg"){
          $('#mainIdeaExpColl_'+topIdeaId).attr("src","images/task/minus.svg").hide();
          $('#mainIdea_'+topIdeaId).children('span').children('img').hide();
        }
        
        updateReorderHorizondal(ideaId,parentIdeaId,level);
			}else{
				alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
			} 
		}else if(movetype == "right"){
			if($('#mainIdea_'+ideaId).parents('.idealistclass:visible').length || $("#mainIdea_"+ideaId).hasClass("idealistclass")){
				
					if($("#mainIdea_"+ideaId+":first-child").index() == 0){
						alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
					}else{
						if($("#mainIdea_"+ideaId).hasClass("idealistclass")){
							nextList = $('#mainIdea_'+ideaId).prev('.idealistclass').attr('id').trim().split('_')[1];/*previous idea id*/
							var parentIdeaId = nextList;
							var topIdeaId = ideaId;
						}else{
							nextList = $('#mainIdea_'+ideaId).prev('li[id^=mainIdea_]').attr('id').trim().split('_')[1];/*previous idea id*/
							var pId = $('#mainIdea_'+nextList).parent('ul').attr('id').split('_')[1];
							var parentIdeaId = nextList;
							var topIdeaId = $('#mainIdea_'+ideaId).parents('.idealistclass').attr('id').trim().split('_')[1];
						}
            
            if($('#mainIdea_'+nextList).attr('disableclass') == "disabledragndrop"){
              alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
            }else{
              var liData = $('#mainIdea_'+ideaId).clone();
              $('#mainIdea_'+ideaId).remove();
              console.log("nextLi"+nextList);
              console.log("parentIdeaId"+parentIdeaId);
              console.log("topIdeaId"+topIdeaId);
              
              if($('#mainIdeaExpColl_'+parentIdeaId).attr('src').indexOf('plus.png')){
                $(liData).removeAttr('style');
                //$(liData).appendTo('#idealistUL_'+nextList);
                $('#mainIdea_'+ideaId).removeClass('pb-3 pt-4 ideadrag').addClass('drag-handle pt-2 p-0 rounded ideadrop');
                $('#mainIdea_'+ideaId).attr('place','sub');
                level="sub";
                $('#mainIdeaExpColl_'+parentIdeaId).show().attr('src','images/task/minus.svg');
              }else{
                $(liData).removeAttr('style');
                //$(liData).appendTo('#idealistUL_'+nextList);
                $('#mainIdea_'+ideaId).removeClass('pb-3 pt-4 ideadrag').addClass('drag-handle pt-2 p-0 rounded ideadrop');
                $('#mainIdea_'+ideaId).attr('place','sub');
                level="sub";
              }
              $('#mainIdea_'+ideaId).attr('parent_idea_id',nextList);
              $('#ideaReorderdiv_'+ideaId).css({"top": "10px", "left": "90px"});
              $('#mainIdea_'+ideaId).removeClass('mainideaclass'); 
              $('#mainIdea_'+ideaId).children('span').attr('onclick','openSubIdea('+ideaId+','+parentIdeaId+',"")'); 
              if($('#idealistUL_'+topIdeaId).children('li').length == 0 && $('#mainIdeaExpColl_'+topIdeaId).attr('src') == "images/task/minus.svg"){
                $('#mainIdeaExpColl_'+topIdeaId).attr("src","images/task/minus.svg").hide();
                $('#mainIdea_'+topIdeaId).children('span').children('img').hide();
              }
              updateReorderHorizondal(ideaId,nextList,level);
            }
						
            
						
					}
				
			}else{
				alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
			}
		} 
}

function updateOrderIdea(currentid,targetid){
  let jsonbody = {
    "currentId" : currentid,
    "targetId" : targetid
  }
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  $.ajax({
    url: apiPath + "/" + myk + "/v1/updateTopBottomOrder",
    type: "PUT",
    //dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 


      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
    }
  });    
}

function updateReorderHorizondal(ideaId,parentId,level){
  
  var orderdata = getOrderData(ideaId,parentId);
  let jsonbody = {
		"idea_id" : ideaId,
    "user_id" : userIdglb,
    "parent_idea_id" : parentId,
    "topIdeaId" : parentId,
    "project_id" : prjid,
    "type" : "Project",
    "topicType" : projType,
    "subAction" : "IdeaRefresh",
    "companyId" : companyIdglb,
    "levelType" : level,
    "ideaDetails":JSON.parse(orderdata)
	}
  
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
	console.log("jsonbody-->"+JSON.stringify(jsonbody));
  $.ajax({
		url: apiPath + "/" + myk + "/v1/updateHorizontalOrder",
    type: "POST",
    //dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
      console.log("parentId-->"+parentId);
        var childidfromresponse="";
        if(parentId != 0 || parentId != "0"){
          childidfromresponse=result.split("@#@")[1];
          parentChildId=childidfromresponse;
          $('#mainIdea_'+parentId).attr('childid',childidfromresponse);
          $('#mainIdea_'+ideaId).css('width','min-content');
          $('#mainIdea_'+parentId).children('span').append("<img src=\"images/idea/expand_all2.svg\" class=\"expandCollapseImgIdea\" onclick=\"ideaExpandAll('singleIdea',"+parentId+","+parentChildId+");event.stopPropagation();\" style=\"width:10px;height:10px;position: absolute;right: 5px;top: 5px;cursor:pointer;\">")
          var disabledrag = $('#mainIdea_'+parentId).attr('disableclass');
          $('#mainIdeaExpColl_'+parentId).attr('src','images/task/minus.svg');
          subList(parentId,'',disabledrag,childidfromresponse);
          $('#mainIdea_'+ideaId).first().remove();
        }else{
          $('#mainIdea_'+ideaId).removeAttr('style');
          var cid = $('#mainIdea_'+ideaId).attr('childid');
          if(cid!=""){
            $('#mainIdea_'+ideaId).children('span').append("<img src=\"images/idea/expand_all2.svg\" class=\"expandCollapseImgIdea\" onclick=\"ideaExpandAll('singleIdea',"+ideaId+","+cid+");event.stopPropagation();\" style=\"width:10px;height:10px;position: absolute;right: 5px;top: 5px;cursor:pointer;\">");
          }
          
        } 
        
        
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
    }  
	});   
}


function lockunlockIdea(ideaid,status,childrenid){
  let jsonbody = {
    "idea_id" : ideaid,
    "lockStatus" : status
  }
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  $.ajax({
    url: apiPath + "/" + myk + "/v1/updateIdeaLock",
    type: "PUT",
    //dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
      if(status == "Y"){
        $('#mainIdea_'+ideaid).addClass('disabledragndrop');
        $('#idealistUL_'+ideaid).addClass('disabledragndrop');
        $('#idealistUL_'+ideaid).removeClass('mytree');
        $('#mainIdea_'+ideaid).find("[id^='idealistUL_']").addClass('disabledragndrop');
        $('#mainIdea_'+ideaid).find("[id^='idealistUL_']").removeClass('mytree');
        $('#mainIdeaExpColl_'+ideaid).removeAttr('onclick');
        $('#mainIdeaExpColl_'+ideaid).attr('onclick','loadIdeaSublist('+ideaid+',"",\"disabledragndrop\")');
        
        $('#addidea_'+ideaid+',#reorder_'+ideaid+', #complete_'+ideaid+', #project_'+ideaid+' ,#taskeditIdea_'+ideaid+', #color_'+ideaid+', #share_'+ideaid+', #story_'+ideaid+', #delete_'+ideaid+'').removeAttr('onclick').css('opacity','0.3');
        $('#ideEditPriorityDiv , #ideaEditDocumentoptiondiv').removeAttr('onmouseover').css('opacity','0.3');
        $('#mainIdea_'+ideaid).attr('disableclass','disabledragndrop');
        
      }else{
        
        $('#mainIdea_'+ideaid).removeClass('disabledragndrop');
        $('#idealistUL_'+ideaid).addClass('mytree');
        $('#idealistUL_'+ideaid).removeClass('disabledragndrop');
        //$('#mainIdea_'+ideaid).find("[id^='idealistUL_']").removeClass('disabledragndrop');
        //$('#mainIdea_'+ideaid).find("[id^='idealistUL_']").addClass('mytree');
        $('#mainIdeaExpColl_'+ideaid).removeAttr('onclick');
        $('#mainIdeaExpColl_'+ideaid).attr('onclick','loadIdeaSublist('+ideaid+',"","")');
        var color = $('.editideadivcontent').css('background-color');
        
        $('#addidea_'+ideaid+',#reorder_'+ideaid+', #complete_'+ideaid+', #project_'+ideaid+' ,#taskeditIdea_'+ideaid+', #color_'+ideaid+', #share_'+ideaid+', #story_'+ideaid+', #delete_'+ideaid+'').css('opacity','');
        $('#ideEditPriorityDiv , #ideaEditDocumentoptiondiv').removeAttr('onmouseover').css('opacity','');

        $('#addidea_'+ideaid).attr('onclick','createSubIdea('+ideaid+',"SubIdea","'+color+'");');
        $('#complete_'+ideaid).attr('onclick','completeIdea('+ideaid+',"edit");');
        $('#delete_'+ideaid).attr('onclick','deleteIdea('+ideaid+')');
        $('#color_'+ideaid).attr('onclick','showEditColor('+ideaid+')');
        $('#ideEditPriorityDiv').attr('onmouseover','trigIdeaPriority(this)');
        $('#ideaEditDocumentoptiondiv').attr('onmouseover','trigIdeaDoc(this)');
        $('#mainIdea_'+ideaid).attr('disableclass','');
        
      } 
      
      $('#mainIdea_'+ideaid).children('span').removeClass('nodeselect');
      if($('#mainIdeaExpColl_'+ideaid).attr('src') == "images/task/minus.svg"){
        $('#mainIdeaExpColl_'+ideaid).trigger('click');
      }
      closeEditpopup(ideaid);
      nodeDragnDrop();
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
    }
    
  }); 

}

function lockUnlockIdeaoptions(ideaid,status,parentid){
  if(typeof(parentid)!="undefined" && (parentid!=0 || parentid!="0")){
    if(status == "Y"){
      $('#addidea_'+ideaid+',#reorder_'+ideaid+', #complete_'+ideaid+', #project_'+ideaid+' ,#taskeditIdea_'+ideaid+', #color_'+ideaid+', #share_'+ideaid+', #story_'+ideaid+', #delete_'+ideaid+'').removeAttr('onclick').css('opacity','0.3');
      $('#ideEditPriorityDiv , #ideaEditDocumentoptiondiv').removeAttr('onmouseover').css('opacity','0.3');
      
    }else{
      var color = $('.editideadivcontent').css('background-color');
          
      $('#addidea_'+ideaid+',#reorder_'+ideaid+', #complete_'+ideaid+', #project_'+ideaid+' ,#taskeditIdea_'+ideaid+', #color_'+ideaid+', #share_'+ideaid+', #story_'+ideaid+', #delete_'+ideaid+'').css('opacity','');
      $('#ideEditPriorityDiv , #ideaEditDocumentoptiondiv').removeAttr('onmouseover').css('opacity','');

      $('#addidea_'+ideaid).attr('onclick','createSubIdea('+ideaid+',"SubIdea","'+color+'");');
      $('#complete_'+ideaid).attr('onclick','completeIdea('+ideaid+',"edit");');
      $('#delete_'+ideaid).attr('onclick','deleteIdea('+ideaid+')');
      $('#color_'+ideaid).attr('onclick','showEditColor('+ideaid+')');
      $('#ideEditPriorityDiv').attr('onmouseover','trigIdeaPriority(this)');
      $('#ideaEditDocumentoptiondiv').attr('onmouseover','trigIdeaDoc(this)');
    }
  }

}


function trigIdeaPriority(obj){
  $(obj).find('#iEditPriorityDiv').show();
  var elmnt = document.getElementById("iEditPriorityDiv");
  elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
}
function trigIdeaPrioritycreate(obj){
  $(obj).find('#iCreatePriorityDiv').show();
  $('#colorOptiondiv').hide();
  var elmnt1 = document.getElementById("iCreatePriorityDiv");
  elmnt1.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
}

function trigIdeaDoc(obj){
  $('#iDocumentDiv').show();
  $('.colorOptiondiv').hide();
  var elmnt = document.getElementById("iDocumentDiv");
  elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
}

function deleteIdea(ideaid){
  var parentid = $('#mainIdea_'+ideaid).attr('parent_idea_id');
  let jsonbody = {
    "user_id" : userIdglb,
    "idea_id" : ideaid
  }
  //$('#loadingBar').addClass('d-none').removeClass('d-flex');
  $.ajax({ 
      url: apiPath+"/"+myk+"/v1/deleteIdea",
      type:"DELETE",
      contentType:"application/json",
      data: JSON.stringify(jsonbody),
      error: function(jqXHR, textStatus, errorThrown) {
        checkError(jqXHR,textStatus,errorThrown);
        //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        }, 
      success:function(result){
        if(result == "Successfully"){
          $('#mainIdea_'+ideaid).remove();
        }else{
          alertFunNew("Failed to delete.",'error');
        }
        
        if($('#idealistUL_'+parentid).children().length == 0){
          $("#mainIdeaExpColl_"+parentid).css('display','none');
        }
        //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      }  
  });    
}

function updateIdea(ideaid){
  var localOffsetTime = getTimeOffset(new Date()).toString();
  var editIdeaTitle = $('#ideaInput_'+ideaid).val().trim();
  if(editIdeaTitle == ""){
    alertFunNew("Idea Name cannot be empty.",'error');
    $('#ideaInput_'+ideaid).focus();
	  return false;
  }else{
    var ncolor = $('#nodecolornew_'+ideaid).val();
    let jsonbody=	{
      "idea_title" : editIdeaTitle,
      "idea_id" : ideaid,
      "user_id" : userIdglb,
      "localOffsetTime" : localOffsetTime
    }
    $("[id^='mainIdea_']").children('span').removeClass('nodeselect');
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    $.ajax({
      url: apiPath + "/" + myk + "/v1/updateIdea",
      type: "PUT",
      dataType: 'json',
      contentType: "application/json",
      data: JSON.stringify(jsonbody),
      beforeSend: function (jqXHR, settings) {
        xhrPool.push(jqXHR);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        
      },
      success: function (result) { 
        updateNodeColor(ideaid,ncolor);
        $('#mainIdea_'+ideaid).children('span').text(editIdeaTitle);
        $('#mainIdea_'+ideaid).children('span').attr('title',editIdeaTitle).css('background-color',ncolor);
        $('#mainIdea_'+ideaid).children('span').css('background-color',ncolor);
        $('#nodecolorold_'+ideaid).val(ncolor);
        closeEditpopup(ideaid);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
      }
      
    }); 
  }
   
  
}

function createSubIdea(parentideaid,ideaPlace,bgColor){
  insertCreateIdeaUI(parentideaid,ideaPlace,bgColor);
  ideaidglb=parentideaid;
}

function iShowDocuploadOptions(ideaid){
  let ui = "";
    ui="<div class='iPriorityclass' style='font-size:11px;'>"
        +'<div class="d-flex p-1 justify-content-start" style="height: 24px; cursor:pointer;">'
				+'  <form enctype="multipart/form-data" style="position:absolute;top:-100px;" method="post" name="ideaFileUpload" id="ideaFileUpload">'
				+'  <input type="file" class="Upload_cLabelTitle" title="Upload" value="" onchange="readFileUrlinIdea(this,'+ideaid+');" name="FileUpload" id="ideaFileUpload_'+ideaid+'" hidden></form>'
				+' 	<img class="img-fluid" src="images/conversation/computer.svg" style="width:20px;" onclick="openSystemFolder('+ideaid+')" id="mainUpload">'
				+'	<span class="pl-2" style="white-space: nowrap;" onclick="openSystemFolder('+ideaid+')">From Computer</span>'
				+'</div>'
				+'<div class="d-flex p-1 justify-content-start" style="height: 24px;">'
				+' 	<img class="img-fluid" style="width:20px;" src="images/conversation/folder.svg">'
				+'	<span class="pl-2" style="white-space: nowrap;" >From Repository</span>'
				+'</div>'
        +'<div class="d-flex p-1 justify-content-start" onclick="attlinkPopup('+ideaid+');" style="height: 24px;cursor:pointer;">'
				+' 	<img class="img-fluid" style="width:20px;" src="images/task/attach.svg">'
				+'	<span class="pl-2" style="white-space: nowrap;" >Attach Link</span>'
				+'</div>'
    +"</div>"

    $('#iDocumentDiv').append(ui);
}

function closeEditIdea(ideaid,ideaname){
  var editedIdea = $('#ideaInput_'+ideaid).val().replace(/(\r\n|\n|\r)/gm, "");
  if(ideaname == editedIdea){
    closeEditpopup(ideaid);
  }else{
    conFunNew(getValues(companyAlerts,"Alert_Save"),'warning','updateIdea','cancelidea',ideaid);
  }
  
}
function closeEditpopup(ideaid){
  var oldcolor = $('#nodecolorold_'+ideaid).val();
  $('#mainIdea_'+ideaid).children('span').css('background-color',oldcolor);
  $('#editIdeaMainDiv_'+ideaid).css('display','none').remove();
  $("#mainIdea_"+ideaid).children('span').removeClass('nodeselect');
  //nodecolor="";ideaidglb="";
}
function cancelidea(){
  $("[id^='editIdeaMainDiv_']").remove();
  $("[id^='mainIdea_']").children('span').removeClass('nodeselect');
}

function showideaedittextarea(ideaid,event){
  $('#ideaInput_'+ideaid).removeAttr('readonly');
  event.preventDefault();
  //$('#ideaInput_'+ideaid).css({"user-select": "none", "-webkit-user-select": "none", "-khtml-user-select":"none", "-moz-user-select":"none", "-ms-user-select":"none"});
  $('#saveEditDiv_'+ideaid).show();
  $('#ideaInput_'+ideaid).trigger('click');
}
function clickinput(ideaid){
  $('#ideaInput_'+ideaid).focus();
}

var nodecolor="";
function insertCreateIdeaUI(parentideaid,ideaPlace,bgColor) {
  let UI = "";
  if ($(".createIdeaDiv_"+parentideaid).length == 0) {
    let UI=prepareAddIdeaUiNew(parentideaid,ideaPlace,bgColor);
    
    if(parentideaid == 0){
      $("#idealist").prepend(UI).show('slow');
      $('#ideaInput_'+parentideaid).focus();
    }else{
      $('#editIdeaMainDiv_'+parentideaid).css('display','none').remove();
      $("#mainIdea_"+parentideaid).append(UI).show('slow');
      $('#ideaInput_'+parentideaid).focus();
      //$('#ideamoreoptdiv_'+parentideaid).css({"left": "335px", "top": "35px"});
      //$('#colorOptiondiv').css({"left": "78px", "top": "20px"});
    } 
    colorGridView(parentideaid,'ideaCreate'); 
    //colorpicker();
    //$('#colorOptiondiv').html(showIdeaColorBox());
    ideaPriorityUi("createplace",parentideaid);
  }else{
    $(".createIdeaDiv_"+parentideaid).fadeOut(300, function(){ $(this).remove();});
    nodecolor="";
    ideaidglb="";
  }
  if(typeof(parentideaid)!="undefined" && parentideaid!=""){
    nodecolor=bgColor;
    closeEditpopup(parentideaid);
  } 


}

function prepareAddIdeaUiNew(parentideaid,ideaPlace,bgColor){
  let ui="";
  if(parentideaid == 0 && ideaPlace=="MainIdea"){
    bgColor="#f6b847";
  }

  if(parentideaid == 0){
    ui+="<div class='createIdeaDiv_"+parentideaid+"' style='border-bottom: 1px solid black;' onmouseover='hidecreatecoloroption(event);'>"
          +"<div class='idea_type_divison'>"
            +"<div style='padding: 6px 8px 8px 0px;'>"
              +"<span class='py-1 px-2 rounded' style='font-size:12px;background-color: #FFF !important;color: #595B94;'>Create Idea</span>"
            +"</div>"  
          +"</div>"
  }else{
    ui+="<div class='createIdeaDiv_"+parentideaid+"' style='position: absolute;z-index: 100;border-radius: 8px;top: -10px;left: 210px;'>"
  }
    ui+='<div class="addIdeaDiv" onmouseover="hideideapriorityoptions(event,'+parentideaid+');event.stopPropagation();" style="padding: 15px 5px 15px 30px;">'
      
      +"<div class='createideamaindiv p-1' style='border-radius: 8px;width: 500px;height:auto;background-color: "+bgColor+";color: black;font-size: 12px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;border-color: rgb(193, 197, 200) !important;display: block;'>"
        +"<div class='d-flex align-items-center ml-3 mr-2 mt-2' style='height: 30px;'>"
          +"<div class='d-flex mr-auto' style=''>"
            +"<p class='py-2 px-0 m-0' style='font-size: 12px;'>ID :</p>"
            +"<input type='text' id='customid_"+parentideaid+"' value='' onclick='editCustomidinput(this,"+parentideaid+");' onblur='blurCustomidinput(this,\"create\");' class='mt-1 ml-2 mr-auto' style='cursor:pointer;height: 20px;border:none;padding-top: 5px;background-color: "+bgColor+";outline:none;width: 50%;background-color:' autocomplete='off'>"
          +"</div>" 
          +"<div class='px-1' id='ideapriority_"+parentideaid+"' priority='6' style='display:none;'><img src='images/task/p-six.svg' id='ideapriorityimg_0' class='image-fluid ideaOptionIcons' onclick='showideapriorityoptions(\"\",0);event.stopPropagation();'  title='' style='position:relative;'>"
            +"<div id='iPriorityDiv_0' class='p-1' style='position: absolute;display:none;color: black;background: rgb(255, 255, 255);border: 1px solid rgb(166, 166, 166);padding: 7px;height: 140px;overflow: auto;border-radius: 2px;font-size: 12px;width: 150px;z-index: 3;text-align: left;'></div>"
          +"</div>"
          +"<div class='px-1' id='ideacomment_"+parentideaid+"' style='display:none;'><img src='/images/task/task_comments.svg'  class='image-fluid ideaOptionIcons' title='' style='cursor:pointer;'></div>"
          +"<div class='px-1' id='ideatask_"+parentideaid+"' style='display:none;'><img src='images/task/task.svg'  class='image-fluid ideaOptionIcons' title='' style='cursor:pointer;'></div>"
          +"<div class='px-1' id='ideadocument_"+parentideaid+"' style='display:none;'><img src='images/idea/document.svg' class='image-fluid ideaOptionIcons' title='' style='cursor:pointer;'></div>"
          +"<div class='px-1' id='ideashare_"+parentideaid+"'style='display:none;'><img src='images/idea/share_nofill.svg'  class='image-fluid ideaOptionIcons' title='' style='cursor:pointer;'></div>"
          +"<div class='px-1' id='completeidea_"+parentideaid+"' comletestat='N' style='display:none;'><img src='images/task/completed.svg' class='image-fluid ideaOptionIcons' title='Completed' style='cursor:pointer;'></div>"
          +'<div class="px-1 ml-1 position-relative">'
            +"<img id='ideamoreoptdivimg_"+parentideaid+"' src='images/conversation/three_dots.svg' onclick='showideaoption("+parentideaid+");event.stopPropagation();' class='' style='width:22px;cursor:pointer;'>"
            +"<div class='position-absolute rounded' id='ideamoreoptdiv_"+parentideaid+"' style='width: 75px;border: 1px solid #c1c5c8;background-color: #fff;color: black;font-size:11px;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);display:none;left:45px;top: -10px;'>"
              +"<span class='pl-2 py-1 ideaoption completeCreate' onclick='completeIdea("+parentideaid+");event.stopPropagation();' style='cursor:pointer;'>Complete</span>"
              +"<span class='pl-2 py-1 ideaoption position-relative' id='ideCreatePriorityDiv' onmouseover='trigIdeaPrioritycreate(this);event.stopPropagation();' style='cursor:pointer;'>Priority" //onclick='showIdeaPriority("+parentideaid+");event.stopPropagation();'
                +"<div id='iCreatePriorityDiv' class='p-1' style='position: absolute;display:none;color: black;background: rgb(255, 255, 255);border: 1px solid rgb(166, 166, 166);padding: 7px;height: 140px;overflow: auto;border-radius: 2px;font-size: 12px;width: 150px;z-index: 3;text-align: left;left:73px;top:-5px;'></div>"
              +"</span>"
              +"<span class='pl-2 py-1 ideaoption d-flex align-items-center position-relative' onmouseover='showColor("+parentideaid+");event.stopPropagation();' style='cursor:pointer;'>Color"     //<img src='images/arrow.png' class='ml-auto mr-1' style='width: 8px;float: right;height: 15px;'
                +"<div class='position-absolute rounded colorOptiondiv p-2 border' id='colorOptiondiv' style='background-color: #fff;color: black;font-size:11px;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);display:none;left:73px;top: -25px;width:182px;z-index: 10;'>"
                  +"<div class='rounded d-none ailgn-items-center w-50' style='border: 1px solid #C1C5C8;'>"
                    +"<div id='colorbg_"+parentideaid+"' class='m-1 border colorplace' style='height:15px;width:15px;background-color:"+bgColor+"'></div>"
                    +"<span id='colorname_"+parentideaid+"' class='m-1' style='font-size:12px;'>"+bgColor+"</span>"
                  +"</div>"
                  +"<div id='colorblock_"+parentideaid+"' class='pt-1'>"
                  +"</div>"
                +"</div>"
              +"</span>"
            +"</div>"
          +"</div>"
        
        +"</div>"
        +"<div class='mx-auto d-flex align-items-start pt-2' style='height: 175px;'>"
          +"<textarea id='ideaInput_"+parentideaid+"' class='ml-3 mr-3 m-0 p-0 w-100 wsScrollBar' title='' onkeyup='ideaTextareGrow(event,this);' onkeydown='' placeholder='Enter your idea...' style='height: 170px;font-size: 12px;background-color: "+bgColor+";outline: none;border:none;resize:none;' autocomplete='off'></textarea>"
        +"</div>"
        +"<div class='d-flex align-items-center w-100 mt-1' style='height: 30px;'>"
          +"<div class='ml-3 w-25 d-flex align-items-center'>"
            +"<div id='idealike_"+parentideaid+"' class='d-flex align-items-center' style=''><img id='idealikeimg_"+parentideaid+"' src='images/idea/thumbs_up.svg' title='Like' onclick='updatelikeimage(this,\"\","+parentideaid+");' like='' class='image-fluid ideaOptionIcons' ><span id='likespan_"+parentideaid+"' class='pl-1'>0</span></div>"
            +"<div id='ideaunlike_"+parentideaid+"' class='ml-1 pl-3 d-flex align-items-center' style=''><img id='ideaunlikeimg_"+parentideaid+"' src='images/idea/thumbs_down.svg' title='Unlike' onclick='updateunlikeimage(this,\"\","+parentideaid+");' like='' class='image-fluid ideaOptionIcons' ><span id='unlikespan_"+parentideaid+"' class='pl-1'>0</span></div>"
          +"</div>"
          +"<div class='w-75 mr-3' style='padding: 5px 0px;'>"
              +"<img id=\"\" title=\"Save\" class=\"ml-2\" src=\"/images/task/tick.svg\" onclick=\"createIdea("+parentideaid+",'"+ideaPlace+"','"+bgColor+"');event.stopPropagation();\" style=\"float:right;height:25px;width:25px;cursor:pointer;\">"
              +"<img id=\"\" title=\"Cancel\" class=\"\" src=\"/images/task/remove.svg\" onclick=\"insertCreateIdeaUI("+parentideaid+");event.stopPropagation();\" style=\"float:right;height:25px;width:25px;cursor:pointer;\">"
          +"</div>"
      +"</div>"
      
    +"</div>" 
    
    
    
  +'</div>'
  
  +"</div>"

  return ui;
}


function hidecreatecoloroption(event){
  var $target = $(event.target);
  if(!$target.is("#colorOptiondiv") &&  !$target.parents().is("#colorOptiondiv")){
    $('#colorOptiondiv').hide();
  }
}

function changenodecolor(obj,ideaid,place){
  nodecolor = $(obj).attr('nodecolor');
  if(ideaid==0 || place=="ideaCreate"){
    $('.createideamaindiv ,#ideaInput_'+ideaid+' , #customid_'+ideaid+'').css('background-color',nodecolor);
    $("[id^='ideaInput_']").css('background-color',nodecolor);
    $("[id^='customid_']").css('background-color',nodecolor);
    ///$('#colorOptiondiv').css('display','none');
  }else{
    $('.editideadivcontent ,#ideaInput_'+ideaid+' , #customid_'+ideaid+'').css('background-color',nodecolor);
    $('#mainIdea_'+ideaid).children('span').css('background-color',nodecolor);
    $('#nodecolornew_'+ideaid).val(nodecolor);
    //$('#colorOptiondiv_'+ideaid).css('display','none');
    $('#saveEditDiv_'+ideaid).show();
  }
  $('#colorbg_'+ideaid).css('background-color',nodecolor);
  $('#colorname_'+ideaid).text(nodecolor);
  $('.colorbox').css('border','');
  $(obj).css('border','2px solid grey');
}

function showColor(id){
  /* if($('#colorOptiondiv').is(':visible')){
    $('#colorOptiondiv').css('display','none');
  }else{ */
    $('#iCreatePriorityDiv').hide();
    $('#colorOptiondiv').css('display','block');
    //$('#ideamoreoptdiv_'+id).css('display','none');
  //}
}

function showEditColor(ideaid){
  $('#colorOptiondiv_'+ideaid).css('display','block');
  var elmnt = document.getElementById("colorOptiondiv_"+ideaid);
  elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  /* if($('#colorOptiondiv_'+ideaid).is(':visible')){
    $('#colorOptiondiv_'+ideaid).css('display','none');
  }else{
    $('#colorOptiondiv_'+ideaid).html('').append(showIdeaColorBox(ideaid)).css('display','block');
    $('#ideamoreoptdiv_'+ideaid).css('display','none');
  } */
}

function showIdeaColorBox(ideaid,bgColor){
  let ui = "";
  if(typeof(ideaid)=="undefined" || ideaid==""){
    ideaid=0;
  } 
  ui="<div class='px-2 py-1 rounded' style='display:block;cursor:pointer;border: 1px solid #8e8e8e;'>"  
    +"<div class='p-1 d-flex' style=''>"
      +"<div nodecolor='#f6c2d9' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#f6c2d9;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#fff69b' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#fff69b;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#bcdfca' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#bcdfca;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#a1c8ea' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#a1c8ea;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#e4dae2' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#e4dae2;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    +"</div>"
    +"<div class='p-1 d-flex' style=''>"
      +"<div nodecolor='#ff7eb9' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#ff7eb9;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#ff65a3' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#ff65a3;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#7afcff' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#7afcff;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#feff9c' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#feff9c;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#fff740' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#fff740;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    +"</div>"
    +"<div class='p-1 d-flex' style=''>"
      +"<div nodecolor='#f39a4e' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#f39a4e;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#eb6092' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#eb6092;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#4ab6d9' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#4ab6d9;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#abcc51' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#abcc51;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#f9c847' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#f9c847;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    +"</div>"
    +"<div class='p-1 d-flex' style=''>"
      +"<div nodecolor='#ffd938' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#ffd938;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#90909a' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#90909a;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#d6d4df' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#d6d4df;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#b3cce2' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#b3cce2;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#1dace6' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#1dace6;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    +"</div>"
    +"<div class='p-1 d-flex' style=''>"
      +"<div nodecolor='#73cac5' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#73cac5;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#e3e546' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#e3e546;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#f2788f' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#f2788f;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#f69dbb' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#f69dbb;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div nodecolor='#fbad4b' onclick='changenodecolor(this,"+ideaid+");' style='width: 15px;height: 25px;background-color:#fbad4b;position: relative;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    +"</div>"
    /* +"<div class='p-1 d-flex justify-content-end' style=''>"
      +"<div onclick='' style=''><img src='/images/task/remove.svg' style='width: 15px;height: 15px;' /></div>"
      +"<div onclick='' style=''><img src='/images/task/tick.svg' style='width: 15px;height: 15px;' /></div>"
    +"</div>" */
    
    
  +"</div>"

  if(typeof(ideaid)=="undefined" || ideaid==""){
    return ui;
  }else{
    $('#colorOptiondiv_'+ideaid).append(ui);
  }
  

}






function completeIdea(idea_id,place){
  var completeideavalue="";
  if($('#completeidea_'+idea_id).css('display') == "none"){
    $('#completeidea_'+idea_id).css('display','block').attr('comletestat','Y');
    completeideavalue='Y';
    $('.completeCreate').text('Incomplete');
  }else{
    $('#completeidea_'+idea_id).css('display','none').attr('comletestat','N');
    completeideavalue='N';
    $('.completeCreate').text('Complete');
  }
  if(place == "edit"){
    updateComplete(idea_id,completeideavalue);
    
  }
}

function showideapriorityoptions(place,ideaid){
  let ui = "";
  if($('#iPriorityDiv_'+ideaid).is(':visible')){
    $('#iPriorityDiv_'+ideaid).html('').hide();
    ideaidglb2="";
  }else{
    ideaPriorityUi(place,ideaid);
    ideaidglb2=ideaid;
  }
  
}

function hideideapriorityoptions(event,ideaid){
  var $target = $(event.target);
  if(!$target.is("#ideEditPriorityDiv") &&  !$target.parents().is("#ideEditPriorityDiv") && !$target.is("#iEditPriorityDiv") &&  !$target.parents().is("#iEditPriorityDiv")){
    $('#iEditPriorityDiv').hide();
  }
  if(!$target.is("#ideaEditDocumentoptiondiv") &&  !$target.is("#iDocumentDiv") &&  !$target.parents().is("#iDocumentDiv")){
    $('#iDocumentDiv').hide();
  }
  if(!$target.is("#iCreatePriorityDiv") &&  !$target.parents().is("#iCreatePriorityDiv") && !$target.is("#ideCreatePriorityDiv") &&  !$target.parents().is("#ideCreatePriorityDiv")){
    $('#iCreatePriorityDiv').hide();
  }
  if(!$target.is(".colormaindiv") && !$target.parents().is(".colormaindiv") && !$target.is("#colorOptiondiv_"+ideaid) &&  !$target.parents().is("#colorOptiondiv_"+ideaid)){
    $('#colorOptiondiv_'+ideaid).hide();
  }
  if(!$target.is(".colormaindiv") && !$target.parents().is(".colormaindiv") && !$target.is("#colorOptiondiv") &&  !$target.parents().is("#colorOptiondiv")){
    $('#colorOptiondiv').hide();
  } 
  
}

function ideaPriorityUi(place,ideaid){
  let ui ="";
  
  ui="<div class='iPriorityclass'>"
    +"<div class='d-flex align-items-center p-1' style='cursor:pointer;'>"
      +"<span onclick='changeideaPriority(this,\""+place+"\","+ideaid+");' priority='1'>"
        +"<img src='images/task/p-one.svg' style='width:15px;height:15px;'><span class='pl-2'>Very Important</span>"
      +"<span>"
    +"</div>"
    +"<div class='d-flex align-items-center p-1' style='cursor:pointer;' >"
      +"<span  onclick='changeideaPriority(this,\""+place+"\","+ideaid+");event.stopPropagation();' priority='2'>"
        +"<img src='images/task/p-two.svg' style='width:15px;height:15px;'><span class='pl-2'>Important</span>"
      +"<span>"
    +"</div>"
    +"<div class='d-flex align-items-center p-1' style='cursor:pointer;'>"
      +"<span  onclick='changeideaPriority(this,\""+place+"\","+ideaid+");event.stopPropagation();' priority='3'> "
        +"<img src='images/task/p-three.svg' style='width:15px;height:15px;'><span class='pl-2'>Medium</span>"
      +"<span>"
    +"</div>"
    +"<div class='d-flex align-items-center p-1' style='cursor:pointer;'>"
      +"<span  onclick='changeideaPriority(this,\""+place+"\","+ideaid+");event.stopPropagation();' priority='4'>"
        +"<img src='images/task/p-four.svg' style='width:15px;height:15px;'><span class='pl-2'>Low</span>"
      +"<span>"
    +"</div>"
    +"<div class='d-flex align-items-center p-1' style='cursor:pointer;'>"
      +"<span  onclick='changeideaPriority(this,\""+place+"\","+ideaid+");event.stopPropagation();' priority='5'>"
        +"<img src='images/task/p-five.svg' style='width:15px;height:15px;'><span class='pl-2'>Very Low</span>"
      +"<span>"
    +"</div>"

    +"</div>"

    if(place == "editoption"){
      $('#iEditPriorityDiv').append(ui);
    }else if(place == "createplace"){
      $('#iCreatePriorityDiv').append(ui);
    }else{
      $('#iPriorityDiv_'+ideaid).append(ui).show();
    }
    
}

function updatelikeimage(obj,place,idea_id){
  var idealike="";
  if($('#idealikeimg_'+idea_id).attr('src').indexOf('thumbs_up2') == -1){
    $('#idealikeimg_'+idea_id).attr('src','/images/idea/thumbs_up2.svg');
    $('#ideaunlikeimg_'+idea_id).attr('src','images/idea/thumbs_down.svg');
    idealike='like';
  }else{
    $('#idealikeimg_'+idea_id).attr('src','/images/idea/thumbs_up.svg');
    idealike='';
  }
  if(place == "edit"){
    updateLike(idea_id,idealike);
  }else{
    if(idealike=='like'){
      $('#likespan_'+idea_id).text('1');
    }else{
      $('#likespan_'+idea_id).text('0');
    }  
    $('#unlikespan_'+idea_id).text('0');
  }
}

function updateunlikeimage(obj,place,idea_id){
  var idealike="";
  if($('#ideaunlikeimg_'+idea_id).attr('src').indexOf('thumbs_down2') == -1){
    $('#ideaunlikeimg_'+idea_id).attr('src','images/idea/thumbs_down2.svg');
    $('#idealikeimg_'+idea_id).attr('src','/images/idea/thumbs_up.svg');
    idealike='unlike';
  }else{
    $('#ideaunlikeimg_'+idea_id).attr('src','images/idea/thumbs_down.svg');
    idealike='';
  }
  
  if(place == "edit"){
    updateLike(idea_id,idealike);
  }else{
    if(idealike=='unlike'){
      $('#unlikespan_'+idea_id).text('1');
    }else{
      $('#unlikespan_'+idea_id).text('0');
    }  
    $('#likespan_'+idea_id).text('0');
  }
}

function changeideaPriority(obj,place,ideaid){
  var ideaPriorityImage = $(obj).children('img').attr('src');
  var priorityNumber = $(obj).attr('priority');
  $('#ideapriorityimg_'+ideaid).attr('src',ideaPriorityImage);
  $('#ideapriority_'+ideaid).attr('priority',priorityNumber);
  $('#iPriorityDiv_'+ideaid).html('').hide();
  
  if(place == "edit" || place == "editoption"){
    updateIdeaPriority(ideaid,priorityNumber);
  }else if(place == "createplace"){
    showIdeaPriority(ideaid);
    $('#ideapriorityimg_0').attr('src',ideaPriorityImage);
    $('#iPriorityDiv_0').html('').hide();
  }
  ideaidglb2="";
}

function showIdeaPriority(ideaid){
  $('#ideapriority_'+ideaid).show();
  /* if($('#ideapriority_'+ideaid).is(':visible')){
    $('#ideapriority_'+ideaid).hide();
  }else{
    $('#ideapriority_'+ideaid).show();
  } */
}

function showideaoption(ideaid){
  if($('.colorOptiondiv').is(':visible')){
    $('.colorOptiondiv').css('display','none');
  }
  if($('#ideamoreoptdiv_'+ideaid).is(':visible')){
    $('#ideamoreoptdiv_'+ideaid).css('display','none');
    ideaidglb4="";
  }else{
    $('#ideamoreoptdiv_'+ideaid).css('display','grid');
    ideaidglb4=ideaid;
    var elmnt = document.getElementById("ideamoreoptdiv_"+ideaid);
    elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  }
}

function createIdea(parentideaid,ideaPlace,bgColor){
  
  var ideatitle = $('#ideaInput_'+parentideaid).val();
  var priorityNumber = $('#ideapriority_'+parentideaid).attr('priority');
  var customid = $('#customid_'+parentideaid).val();
  var completeidea = $('#completeidea_'+parentideaid).attr('comletestat');
  var idealike = $('#idealikeimg_'+parentideaid).attr('src') == "/images/idea/thumbs_up2.svg" ? "like" : $('#ideaunlikeimg_'+parentideaid).attr('src') == "images/idea/thumbs_down2.svg" ? "unlike" : "";
  var rootid = $('#mainIdea_'+parentideaid).attr('rootid');
  if(typeof(rootid) == "undefined"){
    rootid=parentideaid;
  }

  if(nodecolor==""){
    nodecolor=bgColor;
  }
  if(ideatitle == ""){
    alertFunNew("Idea Name cannot be empty.",'error');
	  return false;
  }
  $("[id^='mainIdea_']").children('span').removeClass('nodeselect');
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody=	{
    "idea_title"  : ideatitle,
    "parent_idea_id" : parentideaid,
    "createType" : ideaPlace,
    "rootIdeaId" : rootid,
    "type" : "Project",
    "project_id" : prjid,
    "user_id" : userIdglb,
    "companyId" : companyIdglb,
    "color_code" : nodecolor
  }
  $.ajax({
    url: apiPath + "/" + myk + "/v1/insertMainIdea",
    type: "POST",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
     if(result != ""){
      if($('#ideapriority_'+parentideaid).is(':visible')){
        updateIdeaPriority(result[0].idea_id,priorityNumber);
      }
      if(typeof(customid)!="undefined" && customid!=""){
        updateCustomId(result[0].idea_id,customid);
      }
      if(typeof(completeidea)!="undefined" && completeidea!=""){
        if(completeidea!='N'){
          updateComplete(result[0].idea_id,completeidea);
        }
      }
      if(typeof(idealike)!="undefined" && idealike!=""){
        updateLike(result[0].idea_id,idealike);
      }  
      $('.createIdeaDiv_'+parentideaid).hide().remove();
      if(ideaPlace == "MainIdea"){
        $('#idealistUL_0').prepend(prapareIdeaUI(result));
        $('#mainIdea_'+result[0].idea_id).children('span').addClass('nodeselect');
      }else if(ideaPlace == "SubIdea"){
        $('#mainIdeaExpColl_'+parentideaid).attr('src','images/task/minus.svg').show();
        $('#idealistUL_'+parentideaid).prepend(prepareSubIdeaUi(result,parentideaid));
        $('#idealistUL_0 > #mainIdea_'+parentideaid).children('span').append("<img src=\"images/idea/expand_all2.svg\" class=\"expandCollapseImgIdea\" onclick=\"ideaExpandAll('singleIdea',"+parentideaid+");event.stopPropagation();\" style=\"width:10px;height:10px;position: absolute;right: 5px;top: 5px;cursor:pointer;\">")
        subList(parentideaid);
        var elmnt = document.getElementById("mainIdea_"+parentideaid);
        elmnt.scrollIntoView();
        setTimeout( function(){
          $('#idealistUL_'+parentideaid).find('#mainIdea_'+result[0].idea_id).children('span').addClass('nodeselect');
        },2000);
        
      }
      
      $('#mainIdea_'+result[0].idea_id).children('span').css('background-color',nodecolor);
      nodecolor="";
      

     }

     $('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });
}

function updateIdeaPriority(idea_id,priorityNumber){
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody=	{
    "idea_id" : idea_id,
    "priority" : priorityNumber,
    "user_id" : userIdglb
  }
  $.ajax({
    url: apiPath + "/" + myk + "/v1/insertIdeaPriority",
    type: "PUT",
    //dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
     if(result == "success"){
      $('#ideamoreoptdiv_'+idea_id+' , #iEditPriorityDiv').hide();

     }

     //$('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });

}

function updateCid(idea_id,event){
  var code = null;
	code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
  if(code == 13){
    var customid = $('#customid_'+idea_id).val();
    updateCustomId(idea_id,customid);
  }
  
}

function updateCustomId(idea_id,customid){
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody=	{
    "idea_id" : idea_id,
    "customId" : customid,
    "user_id" : userIdglb
  }
  $.ajax({
    url: apiPath + "/" + myk + "/v1/updateCustomIdIdea",
    type: "PUT",
    //dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
     if(result == "success"){
      $('#customid_'+idea_id).blur();
      $('#customid_'+idea_id).val(customid);
      $('#customid_'+idea_id).css('border-bottom','none');
      $('#customid_'+idea_id).attr('onblur','');
      $('#customid_'+idea_id).attr('onblur','blurCustomidinput(this,\"update\",\"'+customid+'\")');
     }

     //$('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });
}

function updateComplete(idea_id,completeidea){
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody=	{
    "idea_id" : idea_id,
    "ideaStatus" : completeidea,
    "user_id" : userIdglb
  }
  $.ajax({
    url: apiPath + "/" + myk + "/v1/updateCompletionIdea",
    type: "PUT",
    //dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
     if(result == "success"){
      $('#ideamoreoptdiv_'+idea_id).css('display','none');
      $('#editIdeaMainDiv_'+idea_id).parent('#mainIdea_'+idea_id).attr('icomplete',completeidea);
      if(completeidea == "Y"){
        $('#complete_'+idea_id+'').children('span').text('Incomplete');
        $('#addidea_'+idea_id+',#reorder_'+idea_id+', #project_'+idea_id+' ,#taskeditIdea_'+idea_id+', #share_'+idea_id+', #story_'+idea_id+'').removeAttr('onclick').css('opacity','0.3');
        $('#ideaEditDocumentoptiondiv ,#color_'+idea_id+'').removeAttr('onmouseover').css('opacity','0.3'); 
        //$('#customid_'+idea_id+',#ideapriority_'+idea_id+', #ideadocument_'+idea_id+', #ideatask_'+idea_id+', #ideashare_'+idea_id+', #idealikeimg_'+idea_id+', #ideaunlikeimg_'+idea_id).removeAttr('onclick').css('opacity','0.3');
        $('#ideaInput_'+idea_id).removeAttr('ondblclick').css('opacity','0.3');
        //$('#customid_'+idea_id).removeAttr('onkeydown');
      }else{
        
        /* $('#mainIdea_'+idea_id).trigger('click');
        $('#mainIdea_'+idea_id).children('span').trigger('click'); */

        $('#complete_'+idea_id+'').children('span').text('Complete');
        var color = $('.editideadivcontent').css('background-color');
        
        $('#complete_'+idea_id+', #color_'+idea_id+', #share_'+idea_id+', #story_'+idea_id+', #ideaInput_'+idea_id).css('opacity','');

        $('#ideaInput_'+idea_id).attr('ondblclick','showideaedittextarea('+idea_id+',event);');

        /* $('#customid_'+idea_id+',#ideapriority_'+idea_id+', #ideadocument_'+idea_id+', #ideatask_'+idea_id+', #ideashare_'+idea_id+', #idealikeimg_'+idea_id+', #ideaunlikeimg_'+idea_id+', #ideaInput_'+idea_id).css('opacity','');
        $('#ideapriority_'+idea_id).attr('onclick','showideapriorityoptions("edit",'+idea_id+')');
        var iName=$('#ideaInput_'+idea_id).val().replace(/(\r\n|\n|\r)/gm, " ");
        var ustat=$('#editIdeaMainDiv_'+idea_id).attr('userstat');
        $('#ideatask_'+idea_id).attr('onclick','openTaskPopup('+idea_id+',"'+iName+'","icons","'+ustat+'");');
        $('#ideashare_'+idea_id).attr('onclick','ideaShare('+idea_id+');');
        $('#ideadocument_'+idea_id).attr('onclick','listIdeaLinksDocs('+idea_id+');');
        $('#customid_'+idea_id).attr('onclick','editCustomidinput(this,'+idea_id+');');
        $('#idealikeimg_'+idea_id).attr('onclick','updatelikeimage(this,"edit",'+idea_id+');');
        $('#ideaunlikeimg_'+idea_id).attr('onclick','updateunlikeimage(this,"edit",'+idea_id+');');
        $('#customid_'+idea_id).attr('onkeydown','updateCid('+idea_id+',event);'); */
        

        $('#complete_'+idea_id).attr('onclick','completeIdea('+idea_id+',"edit");');
        $('#color_'+idea_id).attr('onmouseover','showEditColor('+idea_id+')');
        $('#share_'+idea_id).attr('onclick','ideaShare('+idea_id+');');

        var uShareAcc = $('#editIdeaMainDiv_'+idea_id).attr('userShAcc');
        var pUsersStatus = $('#editIdeaMainDiv_'+idea_id).attr('userstat');
        var place = $('#editIdeaMainDiv_'+idea_id).attr('iPlace');
        var lockStat = $('#editIdeaMainDiv_'+idea_id).attr('iLock');

        if((uShareAcc=="W") || (pUsersStatus=="PO")){
          $('#addidea_'+idea_id).attr('onclick','createSubIdea('+idea_id+',"SubIdea","'+color+'");');
          $('#addidea_'+idea_id).css('opacity','');
        }else{
          $('#addidea_'+idea_id).css('opacity','0.3');
        }
        if(uShareAcc=="R"){
          if(place != "MainIdea"){
            $('#addidea_'+idea_id).attr('onclick','createSubIdea('+idea_id+',"SubIdea","'+color+'");');
            $('#addidea_'+idea_id).css('opacity','');
          }else{
            $('#addidea_'+idea_id).css('opacity','0.3');
          }
        }

        if(pUsersStatus!="PO" && uShareAcc!="W"){
          if(place != "MainIdea"){
            $('#addidea_'+idea_id).attr('onclick','createSubIdea('+idea_id+',"SubIdea","'+color+'");');
            $('#addidea_'+idea_id).css('opacity','');
          }else{
            $('#addidea_'+idea_id).css('opacity','0.3');
          }
        }

        
        if((uShareAcc=="W") || (pUsersStatus=="PO" && place != "MainIdea")){
          var iInput = $('#ideaInput_'+idea_id).val();
          $('#taskeditIdea_'+idea_id).attr('onclick','openTaskPopup('+idea_id+',"'+iInput+'","options","'+pUsersStatus+'");');
          $('#ideaEditDocumentoptiondiv').attr('onmouseover','trigIdeaDoc(this)');
          $('#taskeditIdea_'+idea_id).css('opacity','');
          $('#ideaEditDocumentoptiondiv').removeAttr('onmouseover').css('opacity','');
        }else{
          $('#taskeditIdea_'+idea_id).css('opacity','0.3');
          $('#ideaEditDocumentoptiondiv').removeAttr('onmouseover').css('opacity','0.3');
        } 

        if(uShareAcc=="W" && pUsersStatus=="PO"){
          $('#reorder_'+idea_id).attr('onclick','showReorderOption('+idea_id+');event.stopPropagation();');
          $('#reorder_'+idea_id).css('opacity','');
        }else{
          $('#reorder_'+idea_id).css('opacity','0.3');
        }

        if(uShareAcc=="W" && pUsersStatus=="PO" && place == "MainIdea"){
          $('#ideEditPriorityDiv').attr('onmouseover','trigIdeaPriority(this)');
          $('#project_'+idea_id).attr('onclick','openProjects('+idea_id+');event.stopPropagation();');
          $('#ideEditPriorityDiv').removeAttr('onmouseover').css('opacity','');
          $('#project_'+idea_id).css('opacity','');
        }else{
          $('#ideEditPriorityDiv').removeAttr('onmouseover').css('opacity','0.3');
          $('#project_'+idea_id).css('opacity','0.3');
        }

        if(uShareAcc=="W" && lockStat=="N"){
          $('#delete_'+idea_id).attr('onclick','openProjects('+idea_id+');;event.stopPropagation();');
          $('#delete_'+idea_id).css('opacity','');
        }else{
          $('#delete_'+idea_id).css('opacity','0.3');
        }


        if(place != "MainIdea"){
          if(lockStat=="N" && place != "MainIdea"){
          
              $('#addidea_'+idea_id).attr('onclick','createSubIdea('+idea_id+',"SubIdea","'+color+'");');
              var iInput = $('#ideaInput_'+idea_id).val();
              $('#taskeditIdea_'+idea_id).attr('onclick','openTaskPopup('+idea_id+',"'+iInput+'","options","'+pUsersStatus+'");');
              $('#ideaEditDocumentoptiondiv').attr('onmouseover','trigIdeaDoc(this)');
              $('#addidea_'+idea_id).css('opacity','');
              $('#taskeditIdea_'+idea_id).css('opacity','');
              $('#ideaEditDocumentoptiondiv').removeAttr('onmouseover').css('opacity','');
            
          }else{
            
              $('#addidea_'+idea_id).css('opacity','0.3');
              $('#taskeditIdea_'+idea_id).css('opacity','0.3');
              $('#ideaEditDocumentoptiondiv').removeAttr('onmouseover').css('opacity','0.3');
              
          }
        }
      }
    }

     //$('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });
}

function updateLike(idea_id,idealike){
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody=	{
    "idea_id" : idea_id,
    "voteAction" : idealike,
    "user_id" : userIdglb
  }
  $.ajax({
    url: apiPath + "/" + myk + "/v1/insertIdeaVote",
    type: "POST",
    //dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 

      var like = result.split('@#@')[0];
      var unlike = result.split('@#@')[1];
      like = numFormatter(like);
      unlike = numFormatter(unlike);
      $('#likespan_'+idea_id).text(like);
      $('#unlikespan_'+idea_id).text(unlike);

     //$('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });

}

function updateNodeColor(ideaid,nodecolor){
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody=	{
    "idea_id" : ideaid,
    "color_code" : nodecolor
  }
  $.ajax({
    url: apiPath + "/" + myk + "/v1/updateColorCode",
    type: "PUT",
    //dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
     if(result == "success"){
      //$('#mainIdea_'+ideaid).children('span').css('background-color',nodecolor);
      //$('#editIdeaMainDiv_'+ideaid).css('display','none').remove();

     }

     //$('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });

}


function ideaTextareGrow(e, obj){
	if (e.keyCode == 13 || e.keyCode == 8 || e.keyCode == 91 ){
		  let scrollHeight = $(obj).get(0).scrollHeight-20;
      console.log("scrollHeight-->"+scrollHeight)
      if(scrollHeight > 160 && scrollHeight < 220){
			  $(obj).css('height', scrollHeight+10+'px');
        $(obj).parent().css('height', scrollHeight+10+'px');
      }
		  if(scrollHeight >= 180){
        $(obj).css('overflow-y', 'auto');
      }else{
			  $(obj).css('overflow-y', 'hidden');
		  }
  }
}  

function openSystemFolder(ideaId) {
	document.getElementById('ideaFileUpload_'+ideaId+'').click();
}

function readFileUrlinIdea(input,ideaId){
  let inputFiles = input.files[0];
  if (input.files && input.files[0]) {
    if(input.files[0].size >= 25000000){
      confirmReset(getValues(companyAlerts,"Alert_ResetMoreSize"),'delete','docUploadInComments','cancelTheDragAndDropUpload');
      $('.alertMainCss').css({'width': '28.5%'});
      $('.alertMainCss').css({'left': '45%'});
      $('#BoxConfirmBtnOk').css({'width': '21%'});
      $('#BoxConfirmBtnOk').val('Continue');
    }else{
      uploadIdeaDocument(inputFiles,ideaId);
    }
  }
}

async function uploadIdeaDocument(inputFiles,ideaId){
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  
  var formData = new FormData();
  formData.append('file', inputFiles);
  formData.append('taskType', 'Project');
  formData.append('user_id', userIdglb);
  formData.append('company_id', companyIdglb);
  formData.append('place', 'AgileRepositoryFileUpload');
  formData.append('resourceId', ideaId);
  formData.append('viewPlace', 'Idea');
  
  $.ajax({
      url: apiPath+"/"+myk+"/v1/upload", 
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      dataType:'text',
      data: formData,
      error: function(jqXHR, textStatus, errorThrown) {
              checkError(jqXHR,textStatus,errorThrown);
              //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      },
      success: function(url) {
        let documentId=url.split('@@##')[1];
         
        $('#ideamoreoptdiv_'+ideaId).hide();
        $('#ideadocument_'+ideaId).show();

        //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      }
  });
  
}

function attlinkPopup(ideaid){
	var ui="";
	$('#attachlinkDivData').html('');
  $("#transparentDiv").show();
  ui+='<div class="ideaAttacklink" id="">'
    +'<div class="modal-dialog">'
      +'<div class="modal-content container">'
      
        
        +'<div class="modal-header py-2 pl-0" style="border-bottom: none !important;">'
          +'<p class="modal-title" style="font-size:14px;color: black;font-weight: normal;">Attach Link</p>'
          +'<button type="button" onclick="closeAttlink();" class="close p-0" data-dismiss="ideaAttacklink" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
        +'</div>'
        
        
        +'<div class="pt-2 pl-0" style="">'
          +'<div class="p-0 " style="font-size:11px;">'
            +'<div class="py-2 material-textfield">'
              +'<input id="ideaAttlinkTitle" class="px-2 py-0 material-textfield-input1 newinput w-100" placeholder=" "  style="outline:none;"/>'
              +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;top:25px">Title</label>'
            +'</div>'
            +'<div class="py-2 material-textfield">'  
              +'<input id="ideaAttlinkaddress" class="px-2 py-0 material-textfield-input1 newinput w-100" placeholder=" " style="outline:none;"/>'
              +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;top:25px">Link</label>'
            +'</div>'  
		      +'</div>'
        +'</div>'
        
        +'<div class="modal-footer py-2 px-0" style="border-top: none !important;">'
          +'<img src="/images/task/remove.svg" title="Cancel" onclick="closeAttlink();" style="width:25px;height:25px;cursor:pointer;">'
          +'<img src="/images/task/tick.svg" title="Attach" onclick="attachIdealink('+ideaid+');" style="width:25px;height:25px;cursor:pointer;">'
        +'</div>'
        
      +'</div>'
    +'</div>'
  +'</div>'

  $('#attachlinkDivData').append(ui).show();
}

function attachIdealink(ideaid){
  var key = $("#ideaAttlinkaddress").val().trim();
  var title = $("#ideaAttlinkTitle").val().trim();
  var validlink = isValidURL(key);
  if( key == '' ){
    alertFunNew(getValues(companyAlerts,"Alert_enterLink"),'error');
    $("#ideaAttlinkaddress").focus();
    return false;
  }else if(validlink == false){
    alertFunNew(getValues(companyAlerts,"Alert_invalidUrl"),'error');
    $("#ideaAttlinkaddress").focus();
    return false;
  }else{

    //$('#loadingBar').addClass('d-flex').removeClass('d-none');
    let jsonbody=	{
      "idea_id" : ideaid,
      "user_id" : userIdglb,
      "title" : title,
      "link"  : key,
      "type" : "Project",
      "project_id" : prjid,
      "companyId" : companyIdglb
    }
    $.ajax({
      url: apiPath + "/" + myk + "/v1/insertIdeaLink",
      type: "PUT",
      dataType: 'json',
      contentType: "application/json",
      data: JSON.stringify(jsonbody),
      beforeSend: function (jqXHR, settings) {
        xhrPool.push(jqXHR);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        
      },
      success: function (result) { 
      
      $('#ideamoreoptdiv_'+ideaid).hide();
      $('#ideadocument_'+ideaid).show();
      $('#attachlinkDivData').html('').hide();
      $("#transparentDiv").hide();
      
      //$('#loadingBar').removeClass('d-flex').addClass('d-none');
      }
    });

  }
  
}

function closeAttlink(){
  $('#attachlinkDivData').html('').hide();
  $('#ideaProjectListDiv').html('').hide();
  $("#transparentDiv").hide();
}

function openProjects(ideaid){
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody=	{
    "user_id" : userIdglb
  }
  $.ajax({
    url: apiPath + "/" + myk + "/v1/loadProjects",
    type: "POST",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
     
     ideaProjectListing(result,ideaid);
     
     $('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });
}

function searchIdeaProjects(){
  if($('#ideaPrjlistsearch').is(':visible') == true){
    $('#ideaPrjlistsearch').hide();
    $('#ideaPrjlist').show();
  }else{
    $('#ideaPrjlistsearch').show().focus();
    $('#ideaPrjlist').hide();
  }
}

function filterIdeaproject(obj){
  var value = $(obj).val().toLowerCase();
  $(".ideaProjects *").filter(function() {
    $(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
}

function ideaProjectListing(result,ideaid){
	var ui="";
	$('#ideaProjectListDiv').html('');
  $("#transparentDiv").show();
  
  ui='<div class="ideaAttacklink" id="">'
    +'<div class="mx-auto" style="max-width: 400px;max-height: 400px;">'
      +'<div class="modal-content container pl-2" style="">'
        +'<div class="modal-header pt-2 pb-1 pl-0 pr-1" style="border-bottom: 1px solid #AAAAAA;">'
          +'<div class="w-100">'
            +'<p class="modal-title" id="ideaPrjlist" style="font-size:16px;">Projects</p>'
            +'<input id="ideaPrjlistsearch" type="text" onkeyup="filterIdeaproject(this);" class="serachBoxInputBoxCommonCls" placeholder="Search here..." style="display:none;width: 93%;outline: none;border: none;font-size: 12px;" autocomplete="off">'
          +'</div>'
          +'<img src="images/menus/search2.svg" onclick="searchIdeaProjects();" class="mr-2" style="width: 22px; height:22px; cursor: pointer;">'
          +'<button type="button" onclick="closeAttlink();" class="close p-0 ideaCloseButton" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
        +'</div>'
        +'<div class="modal-body pt-0 px-0 justify-content-center wsScrollBar" style="max-height: 400px;height: 400px;overflow: auto;">'
        if(result != ""){
          for(var i=0;i<result.length;i++){
            ui+='<div class="ideaProjects p-1 ideaCloseButton" onclick="addIdeaToProject('+ideaid+','+result[i].project_id+');" style="font-size:11px;cursor:pointer;border-bottom: 1px solid #cccccc;height: 40px;">'
              +'<div class="d-flex align-items-center ">'
                +'<img class="border" src="'+result[i].imageUrl+'" style="width:27px;height:27px;">'
                +'<span class="defaultExceedCls ml-2" style="font-size: 12px;width: 225px;">'+result[i].project_name+'</span>'
              +'</div>'
              
            +'</div>'
          } 
        }else{
          ui+='<span>No Projects Found.</span>'
        } 
        ui+='</div>'
      +'</div>'
    +'</div>'
  +'</div>'

  $('#ideaProjectListDiv').append(ui).show();
}

var ideaidforCopyMove="";
var prjidforIdeaCopyMove="";
function addIdeaToProject(ideaId,selectedPrjid){
  ideaidforCopyMove=ideaId;
  prjidforIdeaCopyMove=selectedPrjid;
  //confirmCopyMove(getValues(companyAlerts,"Alert_CopyMove"),"RESET",'confirmCopyIdea','confirmMoveIdea');
  conFunNew("Would you like to copy?",'warning','confirmCopyIdea');
}

function confirmMoveIdea(){
  if(prjid == prjidforIdeaCopyMove){
    alertFunNew(getValues(companyAlerts,"Alert_moveExist"),'error');
    ideaidforCopyMove="";
    prjidforIdeaCopyMove="";
    return false;          	 
  }else{
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
  
    let jsonbody=	{
      "idea_id" : ideaidforCopyMove,
      "selectId" : prjidforIdeaCopyMove,
      "selectType" : "Project",
      "actType" : "move",
      "project_id" : prjid,
      "type" : "Project",
      "user_id" : userIdglb,
      "companyId"  : companyIdglb
    }
    $.ajax({
      url: apiPath + "/" + myk + "/v1/addToProject",
      type: "POST",
		  contentType: "application/json",
		  data: JSON.stringify(jsonbody),
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        console.log("------------------------>");
      },
      success: function (result) { 
      if(result == "success"){
        $('#mainIdea_'+ideaidforCopyMove).remove();
        $('#ideaProjectListDiv').html('').hide();
        $("#transparentDiv").hide();
        ideaidforCopyMove="";
        prjidforIdeaCopyMove="";
        $('#loadingBar').removeClass('d-flex').addClass('d-none');
        alertFunNew(getValues(companyAlerts,"Alert_moved"),'warning');
      }
        
      }
    });
    
  }
  
}

function confirmCopyIdea(){
  if(prjid == prjidforIdeaCopyMove){
    alertFunNew(getValues(companyAlerts,"Alert_copyExist"),'error');
    ideaidforCopyMove="";
    prjidforIdeaCopyMove="";
    return false;          	 
  }else{
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
  
    let jsonbody=	{
      "idea_id" : ideaidforCopyMove,
      "selectId" : prjidforIdeaCopyMove,
      "selectType" : "Project",
      "actType" : "copy",
      "project_id" : prjid,
      "type" : "Project",
      "user_id" : userIdglb,
      "companyId"  : companyIdglb
    }
    $.ajax({
      url: apiPath + "/" + myk + "/v1/addToProject",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(jsonbody),
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        console.log("------------------------------->");
        
      },
      success: function (result) { 
      if(result == "success"){
        $('#ideaProjectListDiv').html('').hide();
        $("#transparentDiv").hide();
        ideaidforCopyMove="";
        prjidforIdeaCopyMove="";
        $('#loadingBar').removeClass('d-flex').addClass('d-none');
        alertFunNew(getValues(companyAlerts,"Alert_copied"),'warning');
      }
      }
    });
    
  }
  
}

function prepareIdeaDocsLinksUi(result){
  var ui="";
  var extension="";
  var type="";
	$('#DocsLinkListDiv').html('');
  $("#transparentDiv").show();
  
  ui='<div class="ideaAttacklink" id="">'
    +'<div class="mx-auto w-75" style="">'
      +'<div class="modal-content container px-0" style="max-height: 500px !important;">'
        +'<div class="modal-header pt-2 pb-2 pl-3 pr-2 d-flex align-items-center" style="border-bottom: 1px solid #5e5b5b;background-color: #003A5D;color: white;">'
          +'<span class="modal-title" style="width:35%;font-size:14px;">Name</span>'  
          +'<span class="modal-title" style="width:35%;font-size:14px;">Title</span>'
          +'<span class="modal-title" style="width:15%;font-size:14px;">Created By</span>'
          +'<span class="modal-title" style="width:15%;font-size:14px;">Created On</span>'
        +'</div>'
        +'<div class="" style="">'
          +'<button type="button" onclick="closeIdeaDocLinkPopup();" class="close p-0 ideaCloseButton" style="top: 5px;right: 10px;color: black;outline: none;color: white;">&times;</button>'
        +'</div>'
        +'<div class="modal-body p-0 justify-content-center wsScrollBar" style="max-height: 400px;height: 400px;overflow: auto;">'
        
        if(result != ""){
          for(var i=0; i<result.length ;i++){
            if(result[i].document_type == "Link Document"){
              type="Link";
            }else{
              type="Document";
            } 

            ui+='<div class="ideadochover ">'
            +'<div id="ideaDoc_'+result[i].idea_documents_id+'" class="media pl-3 pb-1 d-flex align-items-center w-100 position-relative" onmouseover="showIdeaDocOption('+result[i].idea_documents_id+');" onmouseout="hideideaDocOption('+result[i].idea_documents_id+');" style="font-size:12px;cursor:pointer;border-bottom: 1px solid #cccccc;height: 45px;">'
            +'<div class="d-flex align-items-center w-100" style="padding-top: 5px;">'
              if(result[i].document_type == "Link Document"){
                ui+="<div class=\"defaultExceedCls material-textfield\" onclick='' style='width:35%;'><span id='ideaspanurl_"+result[i].idea_documents_id+"' title='"+result[i].document_name+"' onclick=\"openIdeaUrl('"+result[i].document_name+"');\" style='width:35%;cursor:pointer;color: blue;'>"+result[i].document_name+"</span>"
                  +"<input id='ideainputurl_"+result[i].idea_documents_id+"' placeholder=' ' class='inputideaclass_"+result[i].idea_documents_id+" editideaclass material-textfield-input1 newinput1 py-1' placeholder=' ' value='"+result[i].document_name+"' title='"+result[i].document_name+"'  style='display:none;cursor:pointer;outline:none;border:1px solid #C1C5C8;' >"
                  +'<label class="material-textfield-label m-0 ml-1 inputideaclass_'+result[i].idea_documents_id+'" style="display:none;font-size: 12px;top: 1px !important">Link</label>'
                +"</div>"
              }else{
                extension=result[i].document_name;
                extension = extension.substr(extension.lastIndexOf(".")+1).toLowerCase();
                if(extension.toLowerCase() == "jpeg" || extension.toLowerCase() == "jpg" || extension.toLowerCase() == "png" || extension.toLowerCase() == "gif" || extension.toLowerCase() == "bmp" ){
                  ui+="<div id='ideaidthumbnailOpenUi' ondblclick=\"expandImage("+result[i].document_id+",'"+extension+"');\" class='defaultWordBreak position-relative d-flex align-items-center' style='width:35%;'>"
                    +"<img src=\"images/document/"+extension+".svg\" onclick=\"ideathumbnailOpenUi(this,"+result[i].document_id+",'"+extension+"',"+result[i].idea_id+");\" style=\"width: 20px;\">"
                }else if(extension.toLowerCase() == "mp4" || extension.toLowerCase() == "webm" || extension.toLowerCase() == "mov" || extension.toLowerCase() == "ogg" || extension.toLowerCase() == "avi" || extension.toLowerCase() == "mpg"){
                  ui+="<div id='ideavideoOpenUi' onclick=\"openVideo("+result[i].document_id+",'"+extension+"',"+prjid+");\" class='defaultWordBreak position-relative d-flex align-items-center vDoc_"+result[i].idea_id+"' style='width:35%;'>"
                    +"<video id='ideauploadedVideo' class='float-left' style='display:none;cursor: pointer; height:80px; width:130px;background-color: #000;'  id='view_"+result[i].idea_documents_id+"'><source src='"+lighttpdpath+"//projectDocuments//"+result[i].idea_documents_id+"."+extension+"'></video>"
                    +'<img src="images/conversation/videoplay.svg" style="width: 20px;">'
                }else{
                  ui+="<div id='ideaidviewordownloadUi' class='defaultWordBreak position-relative d-flex align-items-center' style='width:35%;'>"
                  +"<img onclick=\"ideaviewordownloadUi("+result[i].document_id+",this,'"+extension+"',"+result[i].idea_id+")\" class='vDoc_"+result[i].idea_id+"' style='cursor: pointer;width:20px;' src='images/document/"+extension+".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_"+result[i].idea_documents_id+"' class='downCalIcon '>"
                  
                }
                ui+='<span class="pl-2 defaultExceedCls" title="'+result[i].document_name+'">'+result[i].document_name+'</span>'
                +"</div>"
              }
              ui+='<div class="defaultExceedCls material-textfield" onclick="" style="width:35%;"><span id="ideaspanDocname_'+result[i].idea_documents_id+'" title="'+result[i].document_title+'">'+result[i].document_title+'</span>'
                +'<input id="ideainputDocname_'+result[i].idea_documents_id+'" class="inputideaclass_'+result[i].idea_documents_id+' editideaclass material-textfield-input1 newinput1 py-1" value="'+result[i].document_title+'" title="'+result[i].document_title+'" style="display:none;cursor:pointer;outline:none;border:1px solid #C1C5C8;" >'
                +'<label class="material-textfield-label m-0 ml-1 inputideaclass_'+result[i].idea_documents_id+'" style="display:none;font-size: 12px;top: 1px !important">Title</label>'
              +'</div>'
              
              +'<div class="defaultExceedCls" style="width:15%;"><span title="'+result[i].createdBy+'">'+result[i].createdBy+'</span></div>'
              +'<div class="defaultExceedCls" style="width:15%;"><span>'+result[i].created_date+'</span></div>'
            +'</div>'
            +"<div id='ideadocOptions_"+result[i].idea_documents_id+"' class='position-absolute px-2 py-1' style='display:none;right: 5px;top: 7px;z-index: 1;border: 1px solid rgb(193, 197, 200);right: 5px;background-color: rgb(255, 255, 255);border-radius: 8px;'>"
              +"<img id=\"cancelIdeadoc_"+result[i].idea_documents_id+"\" title=\"Cancel\" class=\"img-responsive\" src=\"/images/task/remove.svg\" onclick=\"cancelIdeadocs("+result[i].idea_documents_id+",'"+type+"','"+result[i].document_name+"','"+result[i].document_title+"');\" style=\"display:none;height:20px;width:20px;cursor:pointer;margin-right: 5px;\">"
              +"<img id=\"updateIdeadoc_"+result[i].idea_documents_id+"\" title=\"Update\" class=\"img-responsive\" src=\"/images/task/tick.svg\" onclick=\"updateEditedDocs("+result[i].idea_documents_id+",'"+type+"');\" style=\"display:none;height:20px;width:20px;cursor:pointer;\">"
              +"<img id=\"editIdeadoc_"+result[i].idea_documents_id+"\" title=\"Edit\" class=\"img-responsive\" src=\"/images/conversation/edit.svg\" onclick=\"showeditplace("+result[i].idea_documents_id+",'"+type+"');\" style=\"height:18px;width:18px;margin-right: 5px;cursor:pointer;\">"
              +"<img id=\"deleteIdeadoc_"+result[i].idea_documents_id+"\" title=\"Delete\" class=\"img-responsive\" src=\"/images/conversation/delete.svg\" onclick=\"deleteIdeadocsConfirm("+result[i].idea_documents_id+",'"+type+"');\" style=\"height:15px;width:15px;cursor:pointer;\">"
            +"</div>"
             
            +'</div>'
            +'</div>'
          }
        }else{
          ui+='<span>No Files Found.</span>'
        }
        
        ui+='</div>'
      +'</div>'
    +'</div>'
  +'</div>'

  $('#DocsLinkListDiv').append(ui).show();
}

function showeditplace(docid,type){
  if(type=="Link"){
    $('#ideaspanurl_'+docid).hide();
  }
  $('#ideaspanDocname_'+docid).hide();
  $('.inputideaclass_'+docid).show();
  $('.inputideaclass_'+docid).parent().removeClass('defaultExceedCls');
  $('#editIdeadoc_'+docid).hide();
  $('#deleteIdeadoc_'+docid).hide();
  $('#updateIdeadoc_'+docid).show();
  $('#cancelIdeadoc_'+docid).show();
  $('#ideadocOptions_'+docid).addClass('d-block');
  
}

function cancelIdeadocs(docid,type,dname,dtitle){
  if(type=="Link"){
    $('#ideaspanurl_'+docid).show();
  }
  $('#ideaspanDocname_'+docid).show();
  $('.inputideaclass_'+docid).hide();
  $('.inputideaclass_'+docid).parent().addClass('defaultExceedCls');
  $('#editIdeadoc_'+docid).show();
  $('#deleteIdeadoc_'+docid).show();
  $('#updateIdeadoc_'+docid).hide();
  $('#cancelIdeadoc_'+docid).hide();
  $('#ideadocOptions_'+docid).removeClass('d-block');
  $('#ideainputDocname_'+docid).val(dtitle);
  $('#ideainputurl_'+docid).val(dname);
  
}

function editIdeDocName(obj){
  
}

function updateEditedDocs(docid,type){
  var editeddocname = $('#ideainputDocname_'+docid).val();
  var editedlink = $('#ideainputurl_'+docid).val();
  var validlink = isValidURL(editedlink);
  if( typeof(editedlink) != "undefined" && editedlink == '' ){
    alertFunNew(getValues(companyAlerts,"Alert_enterLink"),'error');
    $('#ideainputurl_'+docid).focus();
    return false;
  }else if(typeof(editedlink) != "undefined" && validlink == false){
    alertFunNew(getValues(companyAlerts,"Alert_invalidUrl"),'error');
    $('#ideainputurl_'+docid).focus();
    return false;
  }else{
    if(typeof(editedlink) == "undefined"){
      editedlink="";
    }
    let jsonbody = {
      "idea_documents_id" : docid,
      "user_id" : userIdglb,
      "title" : editeddocname,
      "document_type" : type,
      "link" : editedlink
      
    }
    //$('#loadingBar').addClass('d-flex').removeClass('d-none');  
    $.ajax({
      url: apiPath + "/" + myk + "/v1/updateIdeaDocLink",
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(jsonbody),
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
      },
      success: function (data) {
        
        $('#ideainputDocname_'+docid).val(editeddocname);
        $('#ideainputurl_'+docid).val(editedlink);
        $('#ideaspanDocname_'+docid).text(editeddocname);
        $('#ideaspanDocname_'+docid).attr('title',editeddocname);
        $('#ideaspanurl_'+docid).text(editedlink);
        $('#ideaspanurl_'+docid).attr('title',editedlink);
        if(type=="Link"){
          $('#ideaspanurl_'+docid).show();
        }
        $('#ideaspanDocname_'+docid).show();
        $('.inputideaclass_'+docid).hide();
        $('.inputideaclass_'+docid).parent().addClass('defaultExceedCls');
        $('#editIdeadoc_'+docid).show();
        $('#deleteIdeadoc_'+docid).show();
        $('#updateIdeadoc_'+docid).hide();
        $('#cancelIdeadoc_'+docid).hide();
        $('#ideadocOptions_'+docid).removeClass('d-block');
  
        //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
      }
    });
  } 
  
}

function deleteIdeadocsConfirm(docid,type){
  conFunNew(getValues(companyAlerts,"Alert_delete"),"warning","deleteIdeadocs","",docid,type);
}

function deleteIdeadocs(docid,type){
  let jsonbody = {
		"user_id" : userIdglb,
    "document_type" : type,
    "idea_documents_id" : docid
	}
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');  
	$.ajax({
		url: apiPath + "/" + myk + "/v1/deleteIdeaDocLink",
		type: "DELETE",
    contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			//$('#loadingBar').addClass('d-none').removeClass('d-flex');  
		},
		success: function (data) {
	    
      $('#ideaDoc_'+docid).remove();
		  //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
    }
	});
}

function showIdeaDocOption(document_id){
  $('#ideadocOptions_'+document_id).show();
}

function hideideaDocOption(document_id){
  $('#ideadocOptions_'+document_id).hide();
}

function openIdeaUrl(url){
  window.open(url);
}

function closeIdeaDocLinkPopup(){
  placefordocview="";
  $('#DocsLinkListDiv').html('').hide();
  $("#transparentDiv").hide();
  //$("[id^='editIdeaMainDiv_']").css('display','none').remove();
  $("[id^='mainIdea_']").children('span').removeClass('nodeselect');
  ideaidglb="";
}

function ideathumbnailOpenUi(obj,docid,ext,ideaid){
	var ui="";
  placefordocview="idea";
	$('#ideaviewthumbnail').remove();
	
	
	ui='<div id="ideaviewthumbnail" class="actFeedOptionsDiv d-block" style="left: 25px;top: -3px;display: none;right: unset;border-radius: 8px;">' 
		//+'<div class="arrow-left"></div>
		+'<div class="d-flex align-items-center">'
		+"<img src=\"images/conversation/expand.svg\" title=\"View\" id='expandOptionImg_"+ideaid+"' onclick=\"expandImage("+docid+",'"+ext+"');\" class=\"image-fluid mx-2\" style=\"height: 16px; width:16px; cursor:pointer;\">"
		+"<img src=\"images/conversation/download2.svg\" title=\"Download\" id='downloadOptionImg_"+ideaid+"' onclick=\"downloadActFile("+docid+","+prjid+",'"+ext+"',"+ideaid+")\" class=\"image-fluid mr-2 ml-1\" style=\"width: 18px; height: 18px; cursor:pointer;\">"
		+'</div>'
	+'</div>'

	$(obj).parent().append(ui);
	
 }

function ideaviewordownloadUi(docid,obj,ext,ideaid){
	var ui="";
	placefordocview="idea";
	$('#ideaviewdownloadfileinconversation').remove();

	ui='<div id="ideaviewdownloadfileinconversation" class="actFeedOptionsDiv d-block" style="left: 25px;top: -3px;right: unset;border-radius: 8px;">' 
		//+'<div class="arrow-left"></div>'
		+'<div class="d-flex align-items-center">'
		+"<img src=\"images/conversation/expand.svg\" title=\"View\" id='viewOptionImg_"+ideaid+"' onclick=\"viewdocument("+docid+",'"+ext+"');\" class=\"image-fluid\" style=\"height: 16px; width:16px; cursor:pointer;margin:0px 6px;\">"
		+"<img src=\"images/conversation/download2.svg\" title=\"Download\" id='downloadOptionImg_"+ideaid+"'  onclick=\"downloadActFile("+docid+","+prjid+",'"+ext+"',"+ideaid+")\" class=\"image-fluid\" style=\"margin:0px 6px;height: 18px; width:18px; cursor:pointer;\">"
		
		+'</div>'
	+'</div>'
	$(obj).parent().append(ui);

}

function ideaShare(ideaid){
  let jsonbody = {
		"idea_id" : ideaid,
    "project_id" : prjid,
    "type" : "Project"
	}
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');  
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchShareIdeaUI",
		type: "POST",
    contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			//$('#loadingBar').addClass('d-none').removeClass('d-flex');  
		},
		success: function (result) {
	    
      shareUiList(result,ideaid);
      var disable = $('#editIdeaMainDiv_'+ideaid).parent('#mainIdea_'+ideaid).attr('disableclass');
      if(disable == "disabledragndrop"){
        $('#ShareListDiv').find('.createBtn').removeAttr('onclick').addClass('d-none');
      }
      var disableshare = $('#editIdeaMainDiv_'+ideaid).parent('#mainIdea_'+ideaid).attr('icomplete');
      if(disableshare == "Y"){
        $('#ShareListDiv').find('.createBtn').removeAttr('onclick').addClass('d-none');
      }
		  //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
    }
	});
}

function shareUiList(result,ideaid){
  var ui="";
  var uRole="";
  var userRole="";
  var sharetype="";
  var sharetypeimgAccessed="";
  var onclickshare="";
  var accessdisable="";
  $('#ShareListDiv').html('');
  $("#transparentDiv").show();

  ui='<div class="ideaShareUI" style="" id="">'
    +'<div class="mx-auto w-75" style="">'
      +'<div class="modal-content container px-0" style="max-height: 500px !important;z-index:1000;top: -450px;">'
        +'<div class="modal-header py-2 px-3 d-flex align-items-center" style="border-bottom: 1px solid #5e5b5b;background-color: #003A5D;color: white;">'
          +'<span class="modal-title " style="font-size:14px;width:5%;"></span>'
          +'<span class="modal-title " style="font-size:14px;width:20%;">Name</span>'
          +'<span class="modal-title " style="font-size:14px;width:15%;">User Role</span>'
          +'<span class="modal-title " style="font-size:14px;width:15%;">Mobile</span>'
          +'<span class="modal-title " style="font-size:14px;width:15%;">Work</span>'
          +'<span class="modal-title " style="font-size:14px;width:20%;">Email</span>'
          +'<span class="modal-title " style="font-size:14px;width:10%;text-align:center;">Access</span>'
          //+'<img src="images/menus/close3.svg" onclick="closeIdeashareUIPopup();" class="" style="cursor:pointer;width:10px;height:10px;margin-top:1px;margin-right:0px;">'
        +'</div>'
        +'<div class="" style="">'
          +'<button type="button" onclick="closeIdeashareUIPopup();" class="close p-0 ideaCloseButton" style="top: 5px;right: 10px;color: black;outline: none;color: white;">&times;</button>'
        +'</div>'
        +'<div class="modal-body p-0 justify-content-center wsScrollBar" style="max-height: 400px;height: 400px;overflow: auto;">'
        if(result != ""){
          for(var i=0; i<result.length ;i++){
            
            uRole = result[i].userRole;
            userRole = uRole=="TM"?"Team Member":uRole=="PO"?"Project Manager":"Observer";
            sharetype = result[i].shared_type;
            sharetypeimgAccessed = sharetype=="R"?"images/idea/R.svg":"images/idea/W.svg";
            onclickshare = projectUsersStatus=="PO"?"changeAccessType(this);":"";
            accessdisable = projectUsersStatus=="PO"?"":"opacity: 0.5;";

            ui+="<div id='shareList_"+result[i].idea_shared_id+"' personuserid="+result[i].user_id+" class='shareidealist d-flex align-items-center ideadochover py-2 px-3 ' style='font-size:12px;border-bottom: 1px solid #aaaa;'>"
              +"<div style='width:5%;'><img class='rounded-circle' src='"+result[i].imageUrl+"?"+result[i].imgTimeStamp+"' style='width:25px;height:25px;'></div>"
              +"<div class='defaultExceedCls' style='width:20%;'><span>"+result[i].name+"</span></div>"
              +"<div class='defaultExceedCls' style='width:15%;'><span>"+userRole+"</span></div>"
              +"<div class='defaultExceedCls' style='width:15%;'><span>"+result[i].user_phone+"</span></div>"
              +"<div class='defaultExceedCls' style='width:15%;'><span>"+result[i].work_number+"</span></div>"
              +"<div class='defaultExceedCls' style='width:20%;'><span>"+result[i].user_email1+"</span></div>"
              +"<div id='' style='width:10%;text-align:center;"+accessdisable+"' class='accesstypediv_"+result[i].idea_shared_id+"' shareType='"+sharetype+"' onclick='"+onclickshare+"'><img class='' src='"+sharetypeimgAccessed+"' style='width:25px;cursor:pointer;'></div>"
            +"</div>"
          
          }
        }else{
          ui+="<span>No Users Found.</span>"
        }    

        ui+='</div>'
        if(projectUsersStatus=="PO"){
          ui+='<div class="modal-footer p-2 border-0">'
            +'<div id="" onclick="saveShareIdea('+ideaid+');" style="width: 70px;float: right;" class="createBtn">Save</div>'
          +'</div>'
        }
      
      ui+='</div>'
    +'</div>'
    +'</div>'  

    $('#ShareListDiv').append(ui).show();
}

function changeAccessType(obj){
  var type = $(obj).attr('shareType');
  if(type=="R"){
    $(obj).children('img').attr('src','images/idea/W.svg');
    $(obj).attr('shareType','W');
  }else{
    $(obj).children('img').attr('src','images/idea/R.svg');
    $(obj).attr('shareType','R');
  }
}

function saveShareIdea(ideaid){
  var userData="[";
  var personuserid="";
  var access="";
  var id="";
  $('.shareidealist').each(function (){
    id=$(this).attr('id').split('_')[1];
    personuserid=$(this).attr('personuserid');
    access=$(this).find('.accesstypediv_'+id).attr('shareType');
    userData+="{";
    userData+="\"user_id\":\""+personuserid+"\",";
    userData+="\"shared_type\":\""+access+"\"";
    userData+="},";
  });
  userData=userData.length>1?userData.substring(0,userData.length-1)+"]":"[]";

  let jsonbody = {
		"idea_id" : ideaid,
    "user_id" : userIdglb,
    "userShareType" : JSON.parse(userData)
	}
  $('#loadingBar').addClass('d-flex').removeClass('d-none');  
	$.ajax({
		url: apiPath + "/" + myk + "/v1/shareIdea",
		type: "PUT",
    //dataType: 'json',
    contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');  
		},
		success: function (result) {
	    closeIdeashareUIPopup();
      $('#loadingBar').addClass('d-none').removeClass('d-flex');  
    }
	}); 
}

function closeIdeashareUIPopup(){
  $('#ShareListDiv').html('');
  $("#transparentDiv").hide();
}

function openTaskPopup(ideaid,ideatitle,place,prjUserStat,disableclass){
  let jsonbody = {
		"user_id":userIdglb,
    "company_id":companyIdglb,
    "limit":"",
    "index":"",
    "sortVal":"",
    "text":"",
    "project_id":prjid,
    "sortTaskType":"idea",
    "source_id" : ideaid
	}
  $('#loadingBar').addClass('d-flex').removeClass('d-none');  
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getTaskDetails/TeamZone",
		type: "POST",
    contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');  
		},
		success: function (result) {
	    
      ideaTaskUI(ideaid,ideatitle);
      $('#ideaTasksListHeaderDiv').append(wsTaskUi("Idea"));

      $("#ideaTasksListDivbody").html(createTaskUI(result));
      if(place=="options"){
        $('.createIdeaTaskbutton').trigger('click');
        $("#headerTask, #taskList, .popupTaskDiv").hide();
      }
      
      if($("#ideaTasksListDivbody").html().length==0){
        $("#ideaTasksListDivbody").append("<div class='d-flex justify-content-center mt-3 notaskdiv'>No Task Found</div>");
      }
      var disable = $('#editIdeaMainDiv_'+ideaid).parent('#mainIdea_'+ideaid).attr('disableclass');
      if(disable == "disabledragndrop"){
        $('.createIdeaTaskbutton').removeAttr('onclick').addClass('d-none');
      }
      var disableshare = $('#editIdeaMainDiv_'+ideaid).parent('#mainIdea_'+ideaid).attr('icomplete');
      if(disableshare == "Y"){
        $('.createIdeaTaskbutton').removeAttr('onclick').addClass('d-none');
      }
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    }
	});
  
}

function ideaTaskUI(ideaid,ideatitle,place){
	var ui="";
	$('#otherModuleTaskListDiv').html('');
  $("#transparentDiv").show();
  
  ui='<div class="ideaTask" id="" ideaid='+ideaid+'>'
    +'<div class="" style="">'
      +'<div class="modal-content px-2" style="z-index:1000 !important;max-height: 550px;height: 550px;width: 88%;margin-left: 120px;">'
        +'<div class="modal-header pt-2 pb-0 px-0" style="border-bottom: 1px solid #b4adad;height:30px;">'
          +'<div class=" d-flex  align-items-center w-100" style="">'
            +'<div class="pl-2 d-none" style="font-size:12px;"><img src="images/task/idea-task.svg" style="width:17px;height:17px;"><span class="pl-2">Idea Task</span></div>'
            +"<div class=\"ml-auto pr-3 pb-1 d-none\" title=\"Create Task\"><img class=\"createIdeaTaskbutton\" onclick=\"createIdeaTask('"+ideatitle+"');event.stopPropagation();\" src=\"images/task/plus.svg\" style=\"height:18px;width:18px; cursor:pointer;\"></div>"
          +'</div>'
          //+'<button type="button" onclick="closeIdeaTask();" class="close p-0" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
          +'<img src="images/menus/close3.svg" onclick="closeIdeaTask();" class="" style="cursor:pointer;width:10px;height:10px;margin-top:1px;margin-right:0px;">'
        +'</div>'
        +'<div class="p-0 wsScrollBar" style="overflow: auto;height: inherit;">'
          +'<div class="py-1 w-100 task_type_divison" style="color:#fff;font-size:12px;">'
            +'<div class="pl-2 d-flex align-items-center" style="font-size:14px;">'
              +'<img src="/images/menus/ideas.svg" style="width:17px;height:17px;">'
              +'<span class="pl-2" style="">Idea Task</span>'
            +'</div>'
          +'</div>'
          +'<div id="ideacreateTaskDivbody" class="p-0" style=""></div>'
          +'<div class="popupTaskDiv">'
            +'<div id="ideaTasksListHeaderDiv" class="p-0" style=""></div>'
            +'<div id="ideaTasksListDivbody" class="p-0" style=""></div>'
          +'</div>'
        +'</div>'
        
      +'</div>'
    +'</div>'
  +'</div>'

  $('#otherModuleTaskListDiv').append(ui).show();
}

function closeIdeaTask(){
  $('#otherModuleTaskListDiv').html('').hide();
  $("#transparentDiv").hide();
}

function createIdeaTask(ideatitle){
  insertCreateTaskUI();
  $('#taskname').val(ideatitle).focus();
}

function ideaExpandAll(place,ideaid) {
  
  var localOffsetTime = getTimeOffset(new Date()).toString();
  $('#loadingBar').addClass('d-flex');
  
  $.ajax({
    url: apiPath + "/" + myk + "/v1/loadAllIdeas?parentIdeaId=0&projectId="+prjid+"&userId="+userIdglb+"&type=Project&ideaId="+ideaid+"&localoffsetTimeZone="+localOffsetTime+"&projUserStatus="+projectUsersStatus+"&searchTxt",
    type: "POST",
    //dataType: 'json',
    contentType: "application/json",
    //data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 

      if(place=="all"){
        ideauilist = $('#idealistUL_0').html();

        $('#idealistUL_0').html(prapareIdeaUI(result));

        if($("[id^='mainIdeaExpColl_']").is(':visible')){
          $("[id^='mainIdeaExpColl_']").attr('src','images/task/minus.svg');
        }

        $('#ideaExpandCollapse').children('img').attr('src','images/idea/collapse_all2.svg');
        $('#ideaExpandCollapse').children('span').text('Collapse All');
        $('#ideaExpandCollapse').attr('onclick','closeIdeaExpand("'+place+'");event.stopPropagation();');
        $('#optionsId').show();
      }else{
        ideauilist = $("#mainIdea_"+ideaid).clone();
        $('#mainIdea_'+ideaid).replaceWith(prapareIdeaUI(result));
        //$('#mainIdeaExpColl_'+ideaid).attr('src','images/task/minus.svg');
        if($('#mainIdea_'+ideaid).find("[id^='mainIdeaExpColl_']").is(':visible')){
          $('#mainIdea_'+ideaid).find("[id^='mainIdeaExpColl_']").attr('src','images/task/minus.svg');
        }
        $('#mainIdea_'+ideaid).children('span').children('img').attr('src','images/idea/collapse_all2.svg');
        $('#mainIdea_'+ideaid).children('span').children('img').attr('title','Collapse All');
        $('#mainIdea_'+ideaid).children('span').children('img').attr('onclick','closeIdeaExpand("'+place+'",'+ideaid+');event.stopPropagation();');
      }
      
      nodeDragnDrop();
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
    }
  });
}

function closeIdeaExpand(place,ideaid){
  if(place=="all"){
    $('#idealistUL_0').html(ideauilist);
    if($("[id^='mainIdeaExpColl_']").is(':visible')){
      $("[id^='mainIdeaExpColl_']").attr('src','images/task/plus.svg');
    }
    $('#ideaExpandCollapse').children('img').attr('src','images/idea/expand_all2.svg');
    $('#ideaExpandCollapse').children('span').text('Expand All');
    $('#ideaExpandCollapse').attr('onclick','ideaExpandAll("all","");event.stopPropagation();');
  }else{
    $('#mainIdea_'+ideaid).replaceWith(ideauilist);
    //$('#mainIdeaExpColl_'+ideaid).attr('src','images/task/plus.svg');
    if($('#mainIdea_'+ideaid).find("[id^='mainIdeaExpColl_']").is(':visible')){
      $('#mainIdea_'+ideaid).find("[id^='mainIdeaExpColl_']").attr('src','images/task/plus.svg');
    }
    $('#mainIdea_'+ideaid).children('span').children('img').attr('src','images/idea/expand_all2.svg');
    $('#mainIdea_'+ideaid).children('span').children('img').attr('title','Expand All');
    $('#mainIdea_'+ideaid).children('span').children('img').attr('onclick','ideaExpandAll("'+place+'",'+ideaid+');event.stopPropagation();');
  }
  nodeDragnDrop();
  ideauilist="";
  
}


function ideaSearch() {
  
  var localOffsetTime = getTimeOffset(new Date()).toString();
  $('#loadingBar').addClass('d-flex');
  
  $.ajax({
    url: apiPath + "/" + myk + "/v1/loadAllIdeas?parentIdeaId=0&projectId="+prjid+"&userId="+userIdglb+"&type=Project&ideaId=&localoffsetTimeZone="+localOffsetTime+"&projUserStatus="+projectUsersStatus+"&searchTxt="+ideasearchvalue+"",
    type: "POST",
    contentType: "application/json",
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    },
    success: function (result) { 
      
      ideauilist = $('#idealistUL_0').html();
      $('#idealistUL_0').html(prapareIdeaUI(result));
      if($("[id^='mainIdeaExpColl_']").is(':visible')){
        $("[id^='mainIdeaExpColl_']").attr('src','images/task/minus.svg');
      }

      $('.idealistclass > span').each(function () {
        var ideaval = $(this).text();
        if(ideaval.indexOf(ideasearchvalue) != -1){
          $(this).addClass('nodeselect');
        }
      });  

      $('#loadingBar').addClass('d-none').removeClass('d-flex');
    }
    
  });
}    

/* function rearrange(obj,id,event){
  
  var place=$('#mainIdea_'+id).attr('place');
  var parentId=$('#mainIdea_'+id).parent('ul').attr('id').split('_')[1];
  if(parentId == 0 || parentId == "0"){
    console.log("11111");
    $('#mainIdea_'+id).removeClass('pt-2 p-0 rounded parent_li ideadrop').addClass('pb-3 pt-4 ideadrag');
    $('#mainIdea_'+id).attr('place','main');
  }else if(parentId != 0 || parentId != "0"){
    console.log("22222");
    $('#mainIdea_'+id).removeClass('pb-3 pt-4 ideadrag').addClass('drag-handle pt-2 p-0 rounded ideadrop');
    $('#mainIdea_'+id).attr('place','sub');
  }
  
  
} */

/* function nodeDragnDrop(){
  var parentId="";
  var dragid="";
  var pId="";
  $( ".mytree" ).sortable({
    connectWith: ".mytree",
    placeholder: "ui-state-highlight",
    items: "> li",
    //helper: "clone",
    start: function( event, ui ){
      console.log("start...............");
      dragid=ui.item.attr('id').split('_')[1];
      console.log("dragid--->"+dragid); 
      
    },
    receive: function( event, ui ) {
      console.log("receive...............");
    }, 
    update: function( event, ui ) {
      console.log("update...............");
      parentId=ui.item.parent('ul').attr('id').split('_')[1];
      console.log("parentId--->"+parentId);
      if(parentId == 0 || parentId == "0"){
        $('#mainIdea_'+dragid).removeClass('pt-2 p-0 rounded  ideadrop').addClass('pb-3 pt-4 ideadrag');
        $('#mainIdea_'+dragid).attr('place','main');
      }else if(parentId != 0 || parentId != "0"){
        $('#mainIdea_'+dragid).removeClass('pb-3 pt-4 ideadrag').addClass('drag-handle pt-2 p-0 rounded ideadrop');
        $('#mainIdea_'+dragid).attr('place','sub');
      }
    },stop: function( event, ui ) {
      console.log("stop...............");
    }
  });
} */


/* function nodeDragnDrop(){
  
  

  $("#ideadrag ul").sortable({
    connectWith: "#ideadrag ul",
		appendTo: ".mytree1",					
		items: "> li",
		forceHelperSize: true,
		forcePlaceholderSize: true,
		placeholder: "ui-state-highlight",			
		handle:".drag-handle",						
		delay: "500",
		cursor:"move",
		scroll: true, 
		scrollSensitivity: 100,
		scrollSpeed: 20,
		tolerance: 'pointer', 
		toleranceMove: 'tolerance', 
    
    start: function( event, ui ) {
      console.log("start...............");
     
      $('#idealist').find('ul.ui-sortable').css('min-height','30px');
    },
    receive: function( event, ui ) {
      console.log("receive...............");
      
    },
    stop: function( event, ui ){
      console.log("stop...............");
      
    }
  }); 
}  */

/* function nodeDragnDrop(){
  var nestedSortables = [].slice.call(document.querySelectorAll('.mytree'));
  var sortables = [];
  for(var i=0; i< nestedSortables.length; i++){
    sortables[i] = new Sortable(nestedSortables[i], {
      group: 'nested',
      animation: 150,
      fallbackOnBody: true,
      swapThreshold: 0.65,
    });
  }
}  */

/* function nodeDragnDrop(){
  var action = "";
	var drag_id = "";
	var parentId = "";
	var dragClass  = "";
  
  var nestedSortables = [].slice.call(document.querySelectorAll('.easy-tree'));
	
    
    var sortables = [];
    for(var i=0; i< nestedSortables.length; i++){

      sortables[i] = new Sortable(nestedSortables[i], {
        group: 'nested',
        animation: 150,
        fallbackOnBody: true,
        swapThreshold: 0.65,
        
    });
    var group = $("ul.serialization").sortable({
      group: 'serialization',
      delay: 500,
      onDrop: function ($item, container, _super) {
        var data = group.sortable("serialize").get();
    
        var jsonString = JSON.stringify(data, null, ' ');
    
        console.log("l-l-l->"+jsonString);
        _super($item, container);
      }
    });
    
    } 
}  */
/* var oldContainer;
function nodeDragnDrop(){
  var nestedSortables = [].slice.call(document.querySelectorAll('.easy-tree'));
  var sortables = [];
  for(var i=0; i< nestedSortables.length; i++){
    alert("rr");
    sortables[i] = new Sortable(nestedSortables[i], {
      group: 'nested',
      animation: 150,
      fallbackOnBody: true,
      swapThreshold: 0.65, 
      afterMove: function (placeholder, container) {
        alert("sss");
      },
      onDrop: function ($item, container, _super) {
        container.el.removeClass("active");
        _super($item, container);
        alert("ddd");
      }
    

    });
  }
  
  

} */ 

/* function nodeDragnDrop(){
var group = $("ul.easy-tree").sortable({
  group: 'serialization',
  delay: 500,
  onDrop: function ($item, container, _super) {
    var data = group.sortable("serialize").get();

    var jsonString = JSON.stringify(data, null, ' ');

    alert("-->"+jsonString);
    _super($item, container);
  }
});
} */

/* function nodeDragnDrop(){
  var oldContainer;
  alert("ttv");
  $("ul.easy-tree").sortable({
    group: 'nested',
    afterMove: function (placeholder, container) {
      
    },
    onDrop: function ($item, container, _super) {
      alert("tt");
    }
  });

} */

