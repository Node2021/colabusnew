var limitForNotif=0;
var indexForNotif=0;
var notificationScrollFlag=true;
var socket =null;
var contactsortval = "";
var contactsearchval = "";
var contactsearchtype="";

try{
  socket =  io();
  socket.on('newfeed', data => {
    var moduleType = $('.breadCrumMenuName').text();
      if (data[0].project_id == prjid && moduleType=="Conversations") {
          prepareUIForLatestFeedNew(data);    
      }
      // else{
        //$('#notificationRemind').addClass('d-block').removeClass('d-none');
      $('#notificationBell').attr('src','/images/menus/bell2.svg');
      usersForWorkspace(data[0].project_id,data[0].activityfeed_id,data[0].activityfeed,data[0].name,data[0].user_image,data[0].parent_feed_id,data[0].created_time);
      // }
      console.log("data : "+JSON.stringify(data));        	
  });
  socket.on('delete', data => {
              $('#deleteFeed_' + data).remove();
              $("#actFeedMainDiv_" + data).remove(); 	
  });
}catch(e){
  console.log("Socket exception:"+e);
}

function customDeskNotification(message,imageUrl,fId,pFeedId,name,createTime,ProjectId,pImgType,notifId,notifiStatus,notType,actid,prName,projUserStatus,project_status,status){
  var ui="";
  $('#customDesktopNotification').show();
  var header="";
  if(pFeedId == 0 ){
    header="Team Zone | New Conversation";
  }else{
    header="Team Zone | Reply Conversation";
  }
  var projectimage="";
  if(typeof(pImgType)=="undefined" || pImgType == "-" || pImgType == ""){
    projectimage = lighttpdpath+"/projectimages/dummyLogo.png";
  }else{
    projectimage = lighttpdpath+"/projectimages/"+ProjectId+"."+pImgType;
  }
  var projectType="";
  if(projUserStatus == "PO"){
    projectType="MyProjects";
  }else{
    projectType="SharedProjects";
  }

  ui='<div id="dNotification_'+notifId+'" class="alert alert-dismissible m-0 border px-2 py-1 my-1 w-100 rounded position-relative dNotf" style="background-color: #ffffff;box-shadow: 0px 0px 3px 1px;z-index: 100;" >'    //onmouseover=\"showMarkAsReadButton(this,'+notifId+');\" onmouseout=\"hideMarkAsReadButton(this,'+notifId+');\"
  +'<button type="button" class="close px-2 py-1" data-dismiss="alert" style="color: black !important;outline:none;">&times;</button>'
  +'<div class="d-flex" style="width: 95%;">'
    +'<img src="images/notificationbell.png" class="rounded-circle" style="width:20px;height:20px;">'
    +'<p class="m-0 pl-2" style="font-size: 14px;font-family: opensanscondbold;color: #636565;">'+header+'</p>'
  +'</div>'
  +'<div class="media mt-0 align-items-center w-100" onclick="notificationListing();">'
    +'<img src="'+projectimage+'" class="rounded mt-2 border" style="width:50px;height: 50px;">'
    +'<div class="media-body pl-3" style="width: 90%;">'
      +"<p class='mb-0 notfContentHover' title=\""+message+"\" onclick=\"notificationRedirection('"+notType+"',"+actid+","+ProjectId+",'"+prName+"','"+projectimage+"','"+projUserStatus+"','"+project_status+"','"+projectType+"',"+notifId+",'"+status+"');\" style=\"cursor:pointer;font-size: 13px;color: black;line-height: 1.5em;height: 3em;overflow: hidden;\">"+message+"</p>"  //defaultExceedCls
    +'</div>'
  +'</div>'
  +'<div class="media pt-2 pb-1 pl-2 align-items-center w-100">'
    +'<img class="rounded-circle" style="width: 30px;border: 1px solid #c6bcbc;margin-left: 3px;" src="'+imageUrl+'" title="" onerror="userImageOnErrorReplace(this);">'
    +'<div class="media-body pl-1 d-flex" style="width: 90%;">'
      +'<p class="my-1 defaultExceedCls w-50" style="color:black;font-size: 12px;">'+name+'</p>'
      +'<p class="defaultExceedCls mb-0 mt-1 w-50" style="color:#858181;font-size: 11px;text-align: end;">'+createTime+'</p>'
    +'</div>'
  +'</div>'
  +"<div id=\"\" class=\"markAsReadButton\" onclick=\"changeNotificationCheck(this,'desktopNotification','"+notifiStatus+"');\" notifid=\""+notifId+"\" style=\"position: absolute;color: #000;font-size: 12px;padding: 4px 6px;background-color: #fff;border-radius: 5px;border: 1px solid #bfbfbf;right: 25px;top:2px;cursor: pointer;\">Mark as read</div>"
  +'</div>'

  

  $('#customDesktopNotification').prepend(ui);
  
  setTimeout(function(){
    $('#dNotification_'+notifId).remove();
  }, 8000);
}



function usersForWorkspace(projectID,feedId,message,name,image,pFeedId,created_time) {
  
  let jsonbody1={
    "mainId":projectID,
		"notification_id":feedId,
    "company_id":companyIdglb
	}
  $.ajax({
		url: apiPath + "/" + myk + "/v1/notificationUsers",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody1),
		error: function (jqXHR, textStatus, errorThrown) {
		checkError(jqXHR, textStatus, errorThrown);
		
		},
		success: function (result) { 
			
			for(var i=0;i<result.length;i++){
				userIDDD = result[i].userId;
        if(userIdglb == userIDDD){
          fetchDataforDeskNotification(feedId,"workspace_act_feed",userIDDD,image,pFeedId);
        }  
			}
		}
	});
}
//default notification method showNotification(message,name,imageUrl);


function fetchDataforDeskNotification(notId,notType,otherUid,image,pFeedId){
	var imageUrl=lighttpdpath+"/userimages/"+image+"?"+d.getTime();
	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"company_id": companyIdglb,
		"index": 0,
		"limit": 50,
		"localoffsetTimeZone": localOffsetTime,
		"text": "",
		"type": "unread",
		"userId": otherUid,
		"notId" : notId,
		"notType" : notType
	}
	
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchNotification",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  
		},
		success: function (result) {
			
      if (Notification.permission === "granted") {
        customDeskNotification(result[0].name,imageUrl,notId,pFeedId,result[0].createdName,result[0].created_timestamp,result[0].notMainId,result[0].prjImgType,result[0].notification_id,result[0].status,result[0].notType,result[0].id,result[0].project_name,result[0].projUserStatus,result[0].project_status,result[0].status);
      }else if(Notification.permission !== "denied") {
        Notification.requestPermission().then(permission =>{
          if(permission === "granted"){
            customDeskNotification(result[0].name,imageUrl,notId,pFeedId,result[0].createdName,result[0].created_timestamp,result[0].notMainId,result[0].prjImgType,result[0].notification_id,result[0].status,result[0].notType,result[0].id,result[0].project_name,result[0].projUserStatus,result[0].project_status,result[0].status);
          }
        })
      }
    }
	});
}

function showNotification(message,name,imageUrl){
  const notification = new Notification('Notifications', {
    body: message,
    icon: imageUrl
  }); /// New conversation

  // setTimeout(() => {
  //   notification.close();
  // }, 10 * 1000);notificationListing()

  // notification.addEventListener('click', () => {

  //   window.open('https://www.javascripttutorial.net/web-apis/javascript-notification/', '_blank');
  // });

  notification.onclick = function() { notificationListing(); };
}

function redirect(){
	checksession();
	var pact = pAct.split("\"")[1];
    var rid = roleid.split("\"")[1];
	//var cnme = cname.split("\"")[1];
	console.log(hostname+"----"+pact+"----"+rid);
	window.location.href = hostname+"/postlogin?pAct=home&roleid="+rid;
	
  }

  function custlabel(){
	   
	  $.ajax({
			url:apiPath+"/"+myk+"/v1/loadCustomlebels/"+companyIdglb+"",
			type:"GET",
			dataType:"json",
			error: function(jqXHR, textStatus, errorThrown) {
	         		checkError(jqXHR,textStatus,errorThrown);
					///alert("jqXHR-->"+jqXHR.status);
	        //$("#loadingBar").hide();
					//timerControl("");
					},
			success:function(result){
				///alert("result-->"+result);
				companyLabels = result;
				loadNewAccountLabel('postloginonload');
        loadNewAccountLabel('conversationonload');
        loadNewAccountLabel('landingpageonload');
				//loadCustomLabel('onload');
				
			} 
			
		});
		
				
	} 	
 	
  function custalert(){
	   
	  $.ajax({
			url:apiPath+"/"+myk+"/v1/loadCompanyAlerts/"+companyIdglb+"",
			type:"GET",
			dataType:"json",
			error: function(jqXHR, textStatus, errorThrown) {
	         		checkError(jqXHR,textStatus,errorThrown);
					///alert("jqXHR-->"+jqXHR.status);
	        // 		$("#loadingBar").hide();
					//timerControl("");
					},
			success:function(result){
				///console.log("companyAlerts-->"+JSON.stringify(result));
				companyAlerts = result;
				loadNewAccountLabel('postloginonload'); 
        loadNewAccountLabel('conversationonload');
        loadNewAccountLabel('landingpageonload');
				///loadCustomLabel('onload');
				
			} 
			
		});
		
				
	} 	

  function searchOpen(){
      $("#demo-b").html('<input class="searchIconDiv" type="search" id="globalSearchBox" onblur="searchClose()" placeholder="Search" title="Search" style="padding:0px 4px 2px 19px;"/>');
      $("#globalSearchBox").focus();
  }
  
  //@ to reset the chat session value
  //----- these variables and methods are there in chat.js, used when chat window is closed.
  var globalPeerConnection =null;
  var globalChatConnection =null;
  var globalChatJID =null;
  function releaseChatSession(){
      sessionStorage.name = "chatBoxClosed";
      //console.log("globalChatConnection:"+globalChatConnection);
      //console.log("globalPeerConnection:"+globalPeerConnection);
      //console.log("globalChatJID:"+globalChatJID);
      //if(globalChatConnection!=null){
         //if(globalPeerConnection!=null){
         //   disconnectReqVideoCallWenChatWindowClosed();
         //}else{
         //   disconnectFromMain();
         //}
      //}
  }
  
    function showSearchInputDiv(){
	          $("#glSearchDivId").show();
	          $("#glSearchDivId").css('margin-top','4px');
	          $("#SearchMainLower").hide();
	          $("#SearchMainLowerImgId").hide();
	          $("#comLogoDiv").css('visibility','hidden');
	          $("#home").css('visibility','hidden');
	          $(".glSearchCls").css('margin-right','15px !important');
	}
    
    
    
	function hideSearchBoxLower(){
	          $("#glSearchDivId").hide();
	          $("#glSearchDivId").css('margin-top','0px');
	          $("#SearchMainLower").show();
	          $("#SearchMainLowerImgId").show();
	          $("#comLogoDiv").css('visibility','visible');
	          $("#home").css('visibility','visible');
	          $(".glSearchCls").css('margin-right','15px !important');
	}
  /*
  / @ to open chat Box on click of chat icon
  */
 var width1;
 var height1;
  function openChatWindow(type){ 
	  
	  //alert("place--"+companyAlerts);
  		var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  		width1= width - ((75/100)*width);
  		var height = screen.availHeight;
  		var top = height - ((79/100)*height);
  		if(type=="load"){
	  		
	 		 if ($.browser.mozilla) {
	 		 	height1= height - ((85/100)*height);
	 		 }else{
	 			height1= height - ((84/100)*height);
	 		 }
 		 }else{
 		 	 /* if ($.browser.mozilla) {
	 		 	height1= height - ((30/100)*height);
	 		 	
	 		 }else{ */
 		 	 	height1= height - ((30/100)*height);
 		 	 	
 		 	 //}
 		 }
 		
  		var left = (screen.width/2)-(320/2);
        
  		if(sessionStorage.name != "colabusChat" ){   
			//  alert(sessionStorage.name+"<--sessionStoragename--type-->"+type);
	      // w = window.open('${path}/jsp/chat.jsp', 'Colabus Chat', 'toolbar=no,location=no,screenX=950,screenY=280, status=no, menubar=no,scrollbars=yes, resizable=yes ,width='+width1+',height='+height1+'');       	  	      
	       if(type=="load"){
	       			w = window.open('/chat', 'Colabus Chat', 'toolbar=no,location=no, status=no, menubar=no,scrollbars=yes, resizable=yes ,width=320,height='+height1+',top='+top+', left='+left+'');
	       }else{
	      	       	w = window.open('/chat', 'Colabus Chat', 'toolbar=no,location=no,status=no, menubar=no,scrollbars=yes, resizable=yes ,width=320,height='+height1+', top='+top+', left=1080');
	      			
	       }
	       if(w ==null  || typeof(w)=='undefined'){
	          alertFun(getValues(companyAlerts,"Alert_NotifMsg"),'warning');
	          return false;
	       }else{
	         if(type=="load"){
	         
	         }else{
	           //w.moveBy(1080,240);
	         }
	         w.focus(); 
	         sessionStorage.name = "colabusChat";
	       }
	       sessionStorage.chatCount ="open";
	   }else{
	   		
		    winRef = window.open('', 'Colabus Chat');
	        winRef.focus();
	       
	  }
 }
 
 /*  function openChatWindowsecond(){
   		if(sessionStorage.click!="once"){
	 		// w.resizeTo(370, 630);
	  		 //w.moveBy(1050,220); 
	  		  w.resizeTo(width1, height1);
	  		  w.moveBy(1080,240);
	  		  sessionStorage.click="once"; 
	  		 }       
  }  */
  
  /*----sound notification for chat-----*/
   function chatNotification(){
	   $('#sound').html('<audio autoplay="autoplay"><source src="' +path+ '/sound/glass.mp3" type="audio/mpeg" /><source src="' +path+ '/sound/glass.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="' +path+ '/sound/glass.mp3" /></audio>');
    }
  
  	function searchClose(){
  		searchVal = globalSearchTxt;
  		if(searchVal != "" && searchVal != null){
      		$("#demo-b").html('<img class="topIcon" onclick="searchOpen()" id="searchIcon" title="Search" src="images/searchOne.png"/>');
      	}else{
      		$("#demo-b").html("<input class=\"searchIconDiv\" type=\"search\" id=\"globalSearchBox\" onblur=\"searchClose()\" placeholder=\"Search\" title=\"Search\" value = \""+searchVal+"\" style=\"padding:0px 4px 2px 19px;\"/>");
      		$("#globalSearchBox").show().focus();
      	}
  	}
  
 function submitProjImageUploadForm(e) {
	  var formname;
	    
	    if(e){
	    	formname = e.form.name;
	    }else{
	    	formname = 'workspaceImgUploadForm';
	    }
		var uploadForm = document.getElementById(formname);
		uploadForm.target = 'upload_proj_image';
		uploadForm.submit();
	
	}
  var fileForUpload="";
  function readURL(input) {
      fileForUpload=input.files[0];
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#hideDiv').attr('src', e.target.result);
          };
         projectImgUploadFromSysFlag = true;
         reader.readAsDataURL(input.files[0]);
      }
  }
  
  /* Code for Redirection From email Notification */
  
  function enotifyId(enotid){
	  var localOffsetTime=getTimeOffset(new Date());
	  $.ajax({
			url:"${path}/landPageNotAction.do",
			type:"POST",
			data:{act:"fetchNotificationRedirectionUrl",localOffsetTime:localOffsetTime,enotid:enotid },
			error: function(jqXHR, textStatus, errorThrown) {
	         		checkError(jqXHR,textStatus,errorThrown);
	         		//$("#loadingBar").hide();
					//timerControl("");
					},
			success:function(result){
				checkSessionTimeOut(result);
				window.location.href = result;  
			}
		});
				
	} 	
  

  function totalMsge(){
  var total=0;
  var toatlMsg;
  var localTZ=getTimeOffset(new Date());
   /* $.ajax({
		url: path+"/connectAction.do",
	    data:{act:"fetchPersonalMessagesChat",searchKey:"",searchStr:"",localTZ:localTZ},
	    beforeSend: function (jqXHR, settings) {
	        xhrPool.push(jqXHR);
	    },	
	    mimeType: "textPlain",
	    success:function(result){
			//console.log(result);
	    	checkSessionTimeOut(result);
			//alert(result);
	    	var data=result.split("##@@##");	    
	    	try{
	    		jsonDataConversation=jQuery.parseJSON(data[0]);
		    	for(var i=0; i<jsonDataConversation.length; i++){
	   				total += parseInt(jsonDataConversation[i].unreadMsgCount, 10);   	
	   			}
		    	toatlMsg= total;   	
		    	document.getElementById("count1").innerHTML=toatlMsg; 
		    	if(toatlMsg==0){
		    		$("#count1").hide();
		    	} else{
		    		$("#count1").show();
			    	if(toatlMsg>99){
			    	 	document.getElementById("count1").innerHTML="99+";
			    	}else{
			    		document.getElementById("count1").innerHTML=toatlMsg; 
			    	}
		    	}
	    	}catch(e){
	    		
	    	}
	    		
		}
	}); */
}

  
  function showHelp(){
	    var ph = $("#helpContainerDivNew").height();
	    var bh = $("#divDataNew").innerHeight();
	   	$("#modalBodyDiv").css("height",ph-bh-10); 
		$("#transparentDiv").show();
		$("#helpContainerDivNew").show();
		$("body").css("overflow", "hidden");
		demoFrameNew.location.href ="${path}/jsp/ColabusHelpDoc.htm";
  }
	
	function loadHelpFile(){
		var ww = $(window).width();
		var wh = $(window).height();
		var aw = 1152;
		var ah = 412;
		var al = 0;
		var at = 0;
		
		al = (ww - aw) / 2;
		at = (wh - ah) / 2;
		
		
		//$("#transparentDiv").show();
		$('div#helpContainerDivNew').show();
		$("#divDataNew").show();
		$("iFrame#demoFrameNew").show();
		 $("#helpContainerDivNew").css("height", wh-20);
		$("iFrame#demoFrameNew").css("height", wh-40); 
		$("body").css("overflow", "hidden");
		
		if (ww > 1024){
			$("#helpContainerDivNew").css("margin-left", "40px");
			$("#helpContainerDivNew").css("width", ww-80);
			$("#helpContainerDivNew").css("overflow", "hidden");
			$("iFrame#demoFrameNew").css("overflow", "hidden");
			$("iFrame#demoFrameNew").css("width", ww-80);
			$("iFrame#demoFrameNew").css("height", wh-30); 
		}else{
			$("#helpContainerDivNew").css("margin-left", "20px");
			$("#helpContainerDivNew").css("width", ww-120);
			$("iFrame#demoFrameNew").css("width", ww-130);
			$("div.helpContentCls").css("width", "75%");
		}
		
		if(wh < 570){
			$('#helpContainerDivNew').css('margin-top','10px');
			$('#helpContainerDivNew').css('margin-left','20px');
			$('#helpContainerDivNew').css('margin-right','20px');
			$('#helpContainerDivNew').css('width',ww-120);
		}else{
			//$('#helpContainerDivNew').css('top','50%');
			$('#helpContainerDivNew').css('margin-top','12px');
			$('#helpContainerDiv').css('margin-left','75px');
			$('#helpContainerDiv').css('margin-right','75px');
		}
		
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
   		if(isiPad){
	   		$("#helpContainerDivNew").css("margin-left", "50px");
			$("#helpContainerDivNew").css("width", ww-100);
			$("iFrame#demoFrameNew").css("width", ww-100);
			$('#helpContainerDivNew').css('margin-top','5px');
			$('#helpContainerDivNew').css('left','0%');
   		}
		if(jQuery.browser.msie){
			$('#helpContainerDivNew').css('margin-top','10px');
			$("iFrame#demoFrameNew").css("height", wh);
			$("#helpContainerDivNew").css("height", wh-30);
		}
		
}

	function closeDemo(){
		$("#helpContainerDivNew").hide();
		$("#transparentDiv").hide();
		$("body").css("overflow-y", "auto");	
 		$("body").css("overflow-x", "hidden !important;");	
	}	
	
    function gLocation() {
    	if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(sPosition);
	      } 
	  	else {
	  	  alert("Geolocation is not supported by this browser.");
	  	  }
    } 
    
	function sPosition(position) {
		latitude= position.coords.latitude;
   	    longitude= position.coords.longitude;
   	 	checkMykronusStatus('start');
	}
	
	function checkMykronusStatus(status){
		var localOffsetTime=getTimeOffset(new Date());
	    var date = new Date();
		var date1=date.getDate();
		var month=date.getMonth();
		var year=date.getYear();
		var domain =getDomain(); 	
		var Month = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
		var monthNum = month+1;
		monthNum = monthNum < 10 ? "0"+monthNum : monthNum;
		date1 = date1 < 10 ? "0"+date1 : date1;
		var forDate= Month[month]+" "+date1+", "+(1900+year);
		var globPresentDate = (1900+year)+"-"+monthNum+"-"+date1;
		var presentDate = globPresentDate;
		var statusComment = "AutoStart from Colabus";
		/* if(userId==2){
			$.ajax({
				url: "${path}/connectAction.do",
				type:"POST",
				beforeSend: function (jqXHR, settings) {
				        xhrPool.push(jqXHR);
				    },	
				mimeType: "textPlain",
			    data:{act:"insertAutostartRec",status:status,comapnyId:companyId,lat:latitude,lng:longitude,localoffset:localOffsetTime,comment:statusComment,gpresentData:presentDate},
				success:function(result){
					
				  }
				});
		} */
		
		//var myKronusURL = "https://mykronusdev.colabus.com/AppAuth?act=WorkStatusCheckThroughColabus&user_id="+userId+"&company_id="+companyId+"&status="+status+"&latp="+latitude+"&longp="+longitude+"&localOffsetTime="+localOffsetTime+"&presentDate="+presentDate+"&device=web&statusComment="+statusComment+"&latitude="+latitude+"&longitude="+longitude+"&globPresentDate="+globPresentDate+"";
		//var myKronusURL = "https://mykronusdev.colabus.com/AppAuth?act=WorkStatusCheckThroughColabus&user_id="+userId+"&company_id="+companyId+"&status="+status+"&latp="+latitude+"&longp="+longitude+"&localOffsetTime="+localOffsetTime+"&presentDate="+presentDate+"&device=web&statusComment="+statusComment+"&latitude="+latitude+"&longitude="+longitude+"&globPresentDate="+globPresentDate+"";
		///alert("domain->"+domain.indexOf("localhost:8080"));
		// var myKronusURL = apiPath+"/"+myk+"/v1/WorkStatusCheckThroughColabus";
    // var myKronusURL = "https://dev.mykronus.com/mykronusdev/v1/WorkStatusCheckThroughColabus";
    var myKronusURL = "https://dev.mykronus.com/mykronusdev/v1/WorkStatusCheckThroughColabusnew";
		/* if(domain.indexOf("localhost:8080") == -1){
			myKronusURL = "https://newmykronus.colabus.com/mykronus/v1/WorkStatusCheckThroughColabus";
		}else{
			myKronusURL = "https://newmykronus.colabus.com/mykronus/v1/WorkStatusCheckThroughColabus";
		} */
		
		var value = {
				"company_id": parseInt(companyIdglb),
			    "user_id": parseInt(userIdglb),
			    "presentdate": presentDate,
			    "localOffsetTime": localOffsetTime,
			    "lat_position": latitude,
			    "long_position": longitude
		}
		
		$.ajax({
			url: myKronusURL,
			type:"POST",
			contentType:"application/json",
			data: JSON.stringify(value),
			success:function(result){
          var currentDate="";
          var currentTime="";
          var status="";
          var timeZoneName = "";
				 console.log("result:"+result);
         if(result.length === 0){
          console.log("if");
          currentDate="";
          currentTime="";
          status="";
          timeZoneName = "";
         }else{
          console.log("else");
          currentDate=result[0].userUsageDate;
          currentTime=result[0].userUsageTime;
          status=result[0].prevStatus;
          timeZoneName = result[0].user_timezone;
         }
         
         console.log(status);
         if(status == ""){
           //$("#clck").css("border","2px solid #595B94");
           //$('.hours-hand div, .minutes-hand div').css('background-color','#595B94');
           $("#clck").css("background-image","url(../images/menus/clockface_default.svg)");
           $("#clockStatusSpan").text("Offline");
           $("#clockStatusTimeSpan").text("");
           if(typeof(sessionStorage.Mykronus) == "undefined" || sessionStorage.Mykronus == "" || sessionStorage.Mykronus == "N"){
				    	$("#transparentDiv").show();
				    	// confirmReset("Would you like to track your work hours in MyKronus?",'delete','gLocationone','gLocationhide',"Yes","No");
              // conFunNew("Would you like to track your work hours in MyKronus?",'warning','gLocationone','gLocationhide');
              confirmResetNew("Would you like to track your work hours in MyKronus?",'delete','gLocationone','gLocationhide',"Yes","No");
				    	sessionStorage.Mykronus = 'Y';
				    }
         }
				else if(status == "stop"){
          //$("#clck").css("border","2px solid red");
          //$('.hours-hand div, .minutes-hand div').css('background-color','red');
          $("#clck").css("background-image","url(../images/menus/clockface_red.svg)");
          $("#clockStatusSpan").text("Off");
          $("#clockStatusTimeSpan").text("Since "+currentTime);
          //$('div[id^=mark]').css("border-top","4px solid red");
					if(typeof(sessionStorage.Mykronus) == "undefined" || sessionStorage.Mykronus == "" || sessionStorage.Mykronus == "N"){
              // console.log("stop and start again");
				    	$("#transparentDiv").show();
				    	// confirmReset("Would you like to track your work hours in MyKronus?",'delete','gLocationone','gLocationhide',"Yes","No");
              confirmResetNew("Would you like to track your work hours in MyKronus?",'delete','gLocationone','gLocationhide',"Yes","No");
				    	sessionStorage.Mykronus = 'Y';
				    } 
				}else if(status == "start" || status == "break_close"){
          //$("#clck").css("border","2px solid #60cf35");
          //$('.hours-hand div, .minutes-hand div').css('background-color','#60cf35');
          $("#clck").css("background-image","url(../images/menus/clockface_green.svg)");
          $("#clockStatusSpan").text("Work");
          $("#clockStatusTimeSpan").text("Since "+currentTime+" "+timeZoneName);
          //$('div[id^=mark]').css("border-top","4px solid #60cf35");
        }else if(status == "break_start"){
          //$("#clck").css("border","2px solid #DA9900");
          //$('.hours-hand div, .minutes-hand div').css('background-color','#DA9900');
          $("#clck").css("background-image","url(../images/menus/clockface_orange.svg)");
          $("#clockStatusSpan").text("Break");
          $("#clockStatusTimeSpan").text("Since "+currentTime+" "+timeZoneName);
          //$('div[id^=mark]').css("border-top","4px solid #DA9900");
        }   
			  }
			});
			
	}

	function gLocationone() {
		 $("#transparentDiv").hide();
	    	if (navigator.geolocation) {
		        navigator.geolocation.getCurrentPosition(sPositionone);
		      } 
		  	else {
		  	  alert("Geolocation is not supported by this browser.");
		  	  }
	    } 
	  
	function sPositionone(position) {
		latitude= position.coords.latitude;
	   	    longitude= position.coords.longitude;
	   	    updateMykronusWork('start');
	}
	
	function gLocationhide(){
		$("#transparentDiv").hide();
	}
	  
			
    function updateMykronusWork(status){
    	 
	    var localOffsetTime=getTimeOffset(new Date());
	    var date = new Date();
		var date1=date.getDate();
		var month=date.getMonth();
		var year=date.getYear();
			
		var Month = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
		var monthNum = month+1;
		monthNum = monthNum < 10 ? "0"+monthNum : monthNum;
		date1 = date1 < 10 ? "0"+date1 : date1;
		var forDate= Month[month]+" "+date1+", "+(1900+year);
		var globPresentDate = (1900+year)+"-"+monthNum+"-"+date1;
		var presentDate = globPresentDate;
		var statusComment = "AutoStart from Colabus";
		if(userId==2){
			$.ajax({
				url: "${path}/connectAction.do",
				type:"POST",
				beforeSend: function (jqXHR, settings) {
				        xhrPool.push(jqXHR);
				    },	
				mimeType: "textPlain",
			    data:{act:"insertAutostartRec",status:status,comapnyId:companyId,lat:latitude,lng:longitude,localoffset:localOffsetTime,comment:statusComment,gpresentData:presentDate},
				success:function(result){
					
				  }
				});
		}
		
		//var myKronusURL = "https://mykronusdev.colabus.com/AppAuth?act=WorkStatusUpdateThroughColabus&user_id="+userId+"&company_id="+companyId+"&status="+status+"&latp="+latitude+"&longp="+longitude+"&localOffsetTime="+localOffsetTime+"&presentDate="+presentDate+"&device=web&statusComment="+statusComment+"&latitude="+latitude+"&longitude="+longitude+"&globPresentDate="+globPresentDate+"";
		//var myKronusURL = "https://mykronusdev.colabus.com/AppAuth?act=WorkStatusUpdateThroughColabus&user_id="+userId+"&company_id="+companyId+"&status="+status+"&latp="+latitude+"&longp="+longitude+"&localOffsetTime="+localOffsetTime+"&presentDate="+presentDate+"&device=web&statusComment="+statusComment+"&latitude="+latitude+"&longitude="+longitude+"&globPresentDate="+globPresentDate+"";
		// var myKronusURL = apiPath+"/"+myk+"/v1/updateuserstatus";
    var myKronusURL = "https://dev.mykronus.com/mykronusdev/v1/updateuserstatus";
		/* if(domain.indexOf("localhost:8080") == -1){
			myKronusURL = "https://newmykronus.colabus.com/mykronusdev/v1/updateuserstatus";
		}else{
			myKronusURL = "https://newmykronus.colabus.com/mykronus/v1/updateuserstatus";
		} */
		
		var value = {
	   			"company_id":parseInt(companyIdglb),
	  			"user_id":parseInt(userIdglb),
	  	 		"presentdate":presentDate,
	  			"status":status,
	   			"localoffsetTimeZone":localOffsetTime,
	   			"lat_position":latitude,
	   			"long_position":longitude,
	   			"statusComment":statusComment,
	  	}
		$.ajax({
			url: myKronusURL,
			type:"POST",
			contentType:"application/json",
			data: JSON.stringify(value),
			success:function(result){
				console.log("result::::"+result);
				if(result=="success"){
          checkMykronusStatus('start');
          //$("#clck").css("border","3px solid #60cf35");
					// alertFun("MyKronus Started",'warning');
          alertFunNew("MyKronus Started",'warning');
				}
			  }
			});
			
		/* var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange=function() {
        console.log("xmlhttp.readyState:"+xmlhttp.readyState);console.log("xmlhttp.status:"+xmlhttp.status);
		if (xmlhttp.readyState==4) {
		    var response = xmlhttp.responseText; 
		    console.log("response:"+response);
		    if(response == "success"){
		    	alertFun("MyKronus Started",'warning');
		    }
		 }
		}
		xmlhttp.open("GET",myKronusURL,true);
		xmlhttp.send(); */
		
    }	

function showClockStatusDiv(){
  $('#clockStatusDiv').addClass('d-flex').removeClass('d-none');
}
function hideClockStatusDiv(){
  $('#clockStatusDiv').addClass('d-none').removeClass('d-flex');
}


    /*about function */
   function showAboutProj(){
    $("#transparentDiv").show();
    $("div#aboutProjContent").css('display','block');
}

function listMenus(){
        
  //if(companyType == "Business"){
  //    $('.accessOnlyEntp').remove();
  //}
if($("#menuListDiv").is(':hidden')){
  $("#menuListDiv").slideDown("slow");
}else{
  $("#menuListDiv").slideUp("slow");
}
}



 function createNewWorkspaceProject(wsPlace,wsProjId){
     //if (userRegType == "web_standard") {
     //	createNewStdProject();
     //} else {
         saveWrkSpaceProject(wsPlace,wsProjId);
     //}
 }
 
 function enterprojectTagName(obj){
     var projTagNameVal=$(obj).val();
     var tagres;
     if (!projTagNameVal) {
        return false;
     } else {
         tagres = projTagNameVal.toLowerCase();
         tagres = tagres.replace(/[^a-z0-9]|\s+|\r?\n|\r/gmi, " ");
         tagres = tagres.replace(/[\W_]+/g,"");
     }
     $("input#projTagName").val(tagres);
 }
 
 function autofillProjectTagName(obj){
     var projNameVal = $(obj).val();
     var res;
     if (!projNameVal) {
        return false;
     } else {
         res = projNameVal.toLowerCase();
         res = res.replace(/[^a-z0-9]|\s+|\r?\n|\r/gmi, " ");
         res = res.replace(/[\W_]+/g,"");
     }
     $("input#projTagName").val(res);
 }
  var invateUser= [];
  function saveWrkSpaceProject(wsPlace,wsProjId) {
     $('#loadingBar').show();
     timerControl("start");
     var projectName = $("input#projName").val();
     var projTagName = $("input#projTagName").val();
     var projLen=projectName.length;
     var upload = $('#hideDiv').data('imgName'); 
     var desc = myEditorData.getData();; ///$('textarea#comment1').val()
     var flag = $('#publicProject').prop('checked'); 
     var privacy= $('#privacyImg').attr('value');
     var startdate = $('#startDate').val();
     var enddate = $('#endDate').val();
     var hiddenStatus = $('#unhide').attr('value');
     var active= $('#privacyImg1').attr('value');

     var emailAttDwnload = "checked";
		 if($('#emailAttachmentCheckVal').prop('checked') == true) {
        emailAttDwnload = "checked";
     } else {
				emailAttDwnload = "unChecked";
		 } 
     
    
     $('#userList li').each(function(){
         var id = $(this).attr('id').split('_')[1];
            ID += id+" ";
            invateUser.push(id);
     });
     console.log(invateUser);
  
 
     if (projLen>100){
         alertFun(getValues(companyAlerts,"Alert_ProjectNameExceed"),'warning');
         $('#loadingBar').hide();
         timerControl("");
         return false;
     }
     if (projectName.trim() == '') {
          alertFun(getValues(companyAlerts,"Alert_ProjNameEmpty"),'warning');
         $('#projName').focus();
         $('#loadingBar').hide();
         timerControl("");
         return false;
     }
     if (projectName == "" || projectName == null) {
         alertFun(getValues(companyAlerts,"Alert_EnterProjName"),'warning');
         
         $('#projName').focus();
         $('#loadingBar').hide();
         timerControl("");
     } if(projTagName == "" || projTagName == null){
          alertFun(getValues(companyAlerts,"Alert_WS_TagName"),'warning');
          $('#projTagName').focus();
          $('#loadingBar').hide();
          timerControl("");
     }else {
      glbprojName=projectName;
      glbprojTagName=projTagName;
      glbdesc=desc;
      glbprivacy=privacy;
      glbupload=upload;
      glbSDate=startdate;
      glbEDate=enddate;
      eAttDownload=emailAttDwnload;
      projHiddenStatus=hiddenStatus;
      globActive=active;
      if(wsPlace=="updateWSProject"){
        updateProjectData(wsPlace,wsProjId);
      }else{
        checkProjectNameExistsNew1(projectName,desc,privacy,upload,projTagName,startdate,enddate,emailAttDwnload,hiddenStatus,active,wsPlace,wsProjId);
      }
     }
 } 
 
 var glbprojName="";
 var glbprojTagName="";
 var glbdesc="";
 var glbprivacy="";
 var glbupload="";
 var glbSDate="";
 var glbEDate="";
 var eAttDownload="";
 var projHiddenStatus = "";
 var globActive="";
 function checkProjectNameExistsNew1(projName,desc,privacy,upload,projTagName,startdate,enddate,emailAttDwnload,hiddenStatus,active,wsPlace,wsProjId){
   $.ajax({
        url : apiPath+"/"+myk+"/v1/checkProjectTagNameExists?projTagName="+projTagName+"&companyId="+companyIdglb+"",
        type:"GET",
        error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          $("#loadingBar").hide();
          timerControl("");
          },
        success : function(tagdata) {
          checkSessionTimeOut(tagdata);
              if (tagdata == 'Yes') {
                alertFun(getValues(companyAlerts,"Alert_HashTagName"),'warning');
                $('#projTagName').focus();
                $('#loadingBar').hide();
                timerControl("");
                return false;
                }else{
                  saveProjectDataNew();
                  // confirmReset("Project Code cannot be altered in future.","delete","saveProjectData","saveProjectCancel");
                    
                }
        }
   });
 }

 /// file upload for workspace profile image

 function uploadImageWorkspace(obj,prId,update){
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    var formdata = new FormData();
    formdata.append("file",obj);
    formdata.append("place","workspaceCreateUpload");
    formdata.append("projId",prId);
    formdata.append("resourceId",0);
    // let jsonBody= {
    //   "file": obj,
    //   "place": "workspaceCreateUpload",
    //   "projid": prId,
    //   "resourceid":0
    // }

    $.ajax({
        url:apiPath+"/"+myk+"/v1/upload",
        type:"POST",
        processData: false,
				contentType: false,
        data: formdata,
        cache: false,
				mimeType: "multipart/form-data",
        error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          timerControl("");
          },
        success : function(result){
          // console.log("Result for projects::::"+JSON.stringify(result));
          if(fileForUpload != ""){
            //loadMyprojectJsonNew();
            fileForUpload="";
          }
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          
        }
    });
 }

 function checkProjectNameExistsNew(projName,desc,privacy,upload,projTagName) {
     var splChar = /[!#@$`~:?<>\\\^&*(){}[\]<>?/|\-\"]/;
     if (!splChar.test(projName)) {
         $.ajax({
             url : apiPath+"/"+myk+"/v1/checkProjectNameExists?projName="+projName+"&companyId="+companyIdglb+"&projectId=8564",
             type : "POST",
             data : {
                     act : "checkProjectNameExists",
                     projName : projName
                     },
             error: function(jqXHR, textStatus, errorThrown) {
                     checkError(jqXHR,textStatus,errorThrown);
                     $("#loadingBar").hide();
                     timerControl("");
                     },
             success : function(data) {
                 checkSessionTimeOut(data);
                 if (data == 'Yes') {
                      alertFun(getValues(companyAlerts,"Alert_ProjNameExist"),'warning');
                      $('#projName').focus();
                      $('#loadingBar').hide();
                      timerControl("");
                      return false;
                 } else {
                     $.ajax({
                       url : path + "/workspaceAction.do",
                       type : "POST",
                       data : {
                                 act : "checkProjectTagNameExists",
                                 projTagName : projTagName
                                 },
                          error: function(jqXHR, textStatus, errorThrown) {
                                 checkError(jqXHR,textStatus,errorThrown);
                                 $("#loadingBar").hide();
                                 timerControl("");
                                 },
                       success : function(tagdata) {
                         checkSessionTimeOut(tagdata);
                         if (tagdata == 'Yes') {
                              alertFun(getValues(companyAlerts,"Alert_HashTagName"),'warning');
                              $('#projTagName').focus();
                              $('#loadingBar').hide();
                              timerControl("");
                              return false;
                         }else{
                            glbprojName=projName;
                            glbprojTagName=projTagName;
                            glbdesc=desc;
                            glbprivacy=privacy;
                            glbupload=upload;
                            saveProjectData();
                           // confirmReset("Project Code cannot be altered in future.","delete","saveProjectData","saveProjectCancel");
                            
                         }
                       }
                    });
                 }
             }
         });
     } else {
         alertFun(getValues(companyAlerts,"Alert_ContSplChar"),'warning');
         $('#loadingBar').hide();
         timerControl("");
         return false;
     }
 }
 
 function saveProjectCancel(){
    $('#projTagName').focus();
    $('#loadingBar').hide();
    timerControl("");
 }


 function updateProjectData(wsPlace,wsProjId){
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody = "";
  var action="";var actType="";
  var update = "update";
  jsonbody = { 
      "project_tag_name" : glbprojTagName,
      "projName" : glbprojName,
      "projDesc" : glbdesc,
      "projAccessType" : glbprivacy,
      "user_id" : userIdglb,
      "project_id" : wsProjId,
      "projArchiveType" : globActive,
      "project_start_date" : glbSDate,
      "project_end_date" : glbEDate,
      "project_download_status" : eAttDownload,
      "project_hiden_status" : projHiddenStatus
  }
  $.ajax({
    url: apiPath+"/"+myk+"/v1/updateProjectName",
    type:"PUT",
    //dataType:'json',
    contentType:"application/json",
    data: JSON.stringify(jsonbody),
    error: function(jqXHR, textStatus, errorThrown) {
      checkError(jqXHR,textStatus,errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      timerControl("");
      },
    success : function(result) {
      // console.log("Result for projects::::"+JSON.stringify(result));
      
      addUsers(wsProjId,'');
      uploadImageWorkspace(fileForUpload,wsProjId,update);
      closeCreateNewWorkspace();
      if(fileForUpload == ""){
        loadMyprojectJsonNew();
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
      }    
        
      
    }
  });

 }

 function saveProjectDataNew(){
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody = "";
  var action="";var actType="";
  
 
    action="createOrUpdateNewWorkSpace";
    
    jsonbody = { 
      "project_tag_name" : glbprojTagName,
      "projName" : glbprojName,
      "projDesc" : glbdesc,
      "projEmailHost" : "", 
      "projEmailAct" : "", 
      "projEmailId" : "", 
      "projEmailPwd" : "",   
      "projAccessType" : glbprivacy,   
      "inviteUser" : [],
      "userRegType":"",
      "projEmailFetchType":"",
      "upload":glbupload,
      "user_id":userIdglb,
      "companyId":companyIdglb,
      "projArchiveType":globActive,
      "project_hiden_status":projHiddenStatus,
      "project_start_date":glbSDate,
      "project_end_date":glbEDate,
      "project_download_status":eAttDownload
      
    }
  
   $.ajax({
        url: apiPath+"/"+myk+"/v1/createOrUpdateNewWorkSpace",
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          timerControl("");
          },
        success : function(result) {
          // console.log("Result for projects::::"+JSON.stringify(result));
          
          
            newPrjId = result[0].project_id;
            addUsers(newPrjId,'');
            uploadImageWorkspace(fileForUpload,newPrjId);
            $('input#projUploadId').val(result);
            $('#newProjContent,#transparentDiv').hide();
            var obj = $('#workspaceImgUploadForm').data('obj');
            
            // if(projectImgUploadFromSysFlag){
            //    submitProjImageUploadForm(obj);
            // }
            var projectType = "";
            if(result[0].proj_user_status=="PO"){
              projectType="MyProjects";
            }else{
              projectType="SharedProjects";
            }
            var imgExtension="";
            
            if(typeof(fileForUpload)!="undefined" && fileForUpload != ""){
              imgExtension =fileForUpload.name.split('.').pop().toLowerCase();
            }else{
              imgExtension="";
            }
            var imgPath="";
            if(typeof(imgExtension)!="undefined" && imgExtension != ""){
              imgPath = lighttpdpath+"/projectimages/"+newPrjId+"."+imgExtension+"?"+d.getTime();
            }else{
              imgPath = lighttpdpath + "/projectimages/dummyLogo.png";
            }
                  
            setTimeout(function(){
                $('#transparentDiv').hide();
                $('#newWSUiPopup').hide();
                // switchZone('exsitingWorkSpace');
                projectRedirection(newPrjId,result[0].project_name,result[0].proj_user_status,result[0].status,projectType,imgPath);
                switchZone('landingpageTOtz');				
                // loadProjectData(result);	
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
              }, 6000);
            
            projectImgUploadFromSysFlag = false;
          
        }
   });
 }
 
 function addUsers(projId,page,place){
    
     var email= "";
     var name = "";
     var role = "";
     var id = "";
     var updStatus = "";

     //$('#teamContentDiv').find('[id^=user_]').attr('userrole')
    
     var jsonvalue = '[';
     if(page == "advancePage"){
      $('#teamContentDiv li').each(function(){

        id = $(this).attr('id').split('_')[1];
        email = $(this).attr('email');
        name = $(this).attr('name');
        role = $(this).find('#role_'+id).val();
        updStatus = $(this).attr('updateStatus');
        jsonvalue+="{";
        jsonvalue+="\"user_id\":\""+id+"\",";
        jsonvalue+="\"useremailid\":\""+email+"\",";
        jsonvalue+="\"userName\":\""+name+"\",";
        jsonvalue+="\"proj_user_status\":\""+role+"\",";
        jsonvalue+="\"updateStatus\":\""+updStatus+"\"";
        jsonvalue+="},";
      });
     }else{
      $('#userList li').each(function(){

        id = $(this).attr('id').split('_')[1];
        email = $(this).attr('email');
        name = $(this).attr('name');
        role = $(this).find('#role_'+id).val();
        updStatus = $(this).attr('updateStatus');
        jsonvalue+="{";
        jsonvalue+="\"user_id\":\""+id+"\",";
        jsonvalue+="\"useremailid\":\""+email+"\",";
        jsonvalue+="\"userName\":\""+name+"\",";
        jsonvalue+="\"proj_user_status\":\""+role+"\",";
        jsonvalue+="\"updateStatus\":\""+updStatus+"\"";
        jsonvalue+="},";
      });
     }

      var jsonresult="";
      if(jsonvalue.length>1){
        jsonresult = jsonvalue.substring(0, jsonvalue.length-1);
        jsonresult=jsonresult+"]";
      }else{
        jsonresult ="[]";
      }
      console.log("jsonresult--->"+jsonresult);
    let jsonbody= {
      "user_id":userIdglb,	
      "companyId":companyIdglb,	
      "project_id":projId,
      "inviteUser":JSON.parse(jsonresult)
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
     $.ajax({
         url : apiPath+"/"+myk+"/v1/saveInivtedEntUsersToProj",
         type : "POST",
         contentType:"application/json",
         data:JSON.stringify(jsonbody),
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $('#loadingBar').addClass('d-none').removeClass('d-flex');
                 
                 },
         success : function(data) {

          if(page == "advancePage"){
            $('#advOptionContainer').hide();
            $("#transparentDiv").hide();
            $("#newWSUiPopup").html('');
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            loadMyprojectJsonNew();
          }

          if(place == "teams"){
            console.log("in side team");
            $('#advOptionContainer').hide();
            $("#transparentDiv").hide();
            $("#newWSUiPopup").html('');
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            listContacts('teams');
          }

         }
                  
     });                             
 }

 /* function addUsersAdv(projId){
  
   var email= "";
   var name = "";
   var role = "";
   var id = "";
 

   var jsonvalue = '[';
   $('#teamContentDiv li').each(function(){     

      id = $(this).attr('id').split('_')[1];
      email = $(this).attr('email');
      name = $(this).attr('name');
      role = $(this).find('#role_'+id).val();
      console.log(role);
      jsonvalue+="{";
      jsonvalue+="\"user_id\":\""+id+"\",";
      jsonvalue+="\"useremailid\":\""+email+"\",";
      jsonvalue+="\"userName\":\""+name+"\",";
      jsonvalue+="\"proj_user_status\":\""+role+"\"";
      jsonvalue+="},";
    });
    var jsonresult="";
    if(jsonvalue.length>1){
      jsonresult = jsonvalue.substring(0, jsonvalue.length-1);
      jsonresult=jsonresult+"]";
    }else{
      jsonresult ="[]";
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody= {
    "user_id":userIdglb,	
    "companyId":companyIdglb,	
    "project_id":projId,
    "inviteUser":JSON.parse(jsonresult)
  }
   $.ajax({
       url : apiPath+"/"+myk+"/v1/saveInivtedEntUsersToProj",
       type : "POST",
       contentType:"application/json",
       data:JSON.stringify(jsonbody),
       error: function(jqXHR, textStatus, errorThrown) {
               checkError(jqXHR,textStatus,errorThrown);
               $('#loadingBar').addClass('d-none').removeClass('d-flex');
               },
       success : function(data) {

          $('#advOptionContainer').hide();
          $("#transparentDiv").hide();
          $("#newWSUiPopup").html('');
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          loadMyprojectJsonNew();

       }

   });                             
} */
 
 function openProfile(){
      $("#loadingBar").show();	
      timerControl("start");
                 $.ajax({
                     url:"${path}/postLoginAction.do",
                     type:"POST",
                     data:{act:"loadUserDetails"},
                     error: function(jqXHR, textStatus, errorThrown) {
                             checkError(jqXHR,textStatus,errorThrown);
                             $("#loadingBar").hide();
                             timerControl("");
                             },
                     success:function(result){
                         checkSessionTimeOut(result);
                         jsonDataProfile = $.parseJSON(result);
                         userProfile();
                         $("#loadingBar").hide();	
                         timerControl("");
                     }
                 });
         
                 
      
  }
 
 
 function uploadpopUp(){
        
      return  "<div style=\"width: 170px; min-width: 100px; background-color: rgb(255, 255, 255); height: auto; z-index: 5010; border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); border: 1px solid rgb(161, 161, 161); float: left; padding-left: 5px; margin-left: 126%; margin-top: -45%; display:none;position:absolute;\" id=\"Profile_Upload\">"
                  +"<div style=\"margin-left:-20px;margin-top:10px;\">"
                    +"<img src=\"images/arrowLeft.png\" >"
                  +"</div>"
                  +"<div style=\"height:28px;margin-top:-32px;\" class=\"optionList\">"
                   +"<span style=\"margin-left:18px; margin-top: 5px;width:75%\" class=\"OptionsFont Upload Upload_from_system_cLabelHtml\" id=\"uploadCompanyImage\"> </span>"
                 +"</div>"
                  +"<div onclick=\"getUserAlbums()\" style=\"width:100%;height:23px;margin-bottom:3px;cursor:pointer;float:left\">"
                    +"<span style=\"margin-left: 18px; margin-top: 1px;width:75%\" class=\"OptionsFont Attach_Link Upload_from_gallery_cLabelHtml\"> </span>"
                  +"</div>"
               +"</div>"
 }
 
 
 function userProfile(){
     $('#transparentDiv').show();
    
    var UI ="<div id='userDetailpopUp'  class='MyPflpopup' style=\"border:1px solid #a6a6a6; position: fixed; background: #fff;border-radius: 7px;\">"
        
              +"<div style=\"width:100%; height: 18%;\">"
                   +"<div style=\"height: 65%;width: 95%;margin-left: 2%;min-height: 68px;\">"
                         +"<div style=\"height: 100%;float: left;width: 78px;position: relative;\">"
                            +"<div style=\"padding-top: 12%;height: 90%;float: left;width: 95%;position: relative;\">"
                              +"<img id='profileImgProfile'  src='"+jsonDataProfile[0].imgName+"' title='"+replaceSpecialCharacter(jsonDataProfile[0].userFirstname)+" "+replaceSpecialCharacter(jsonDataProfile[0].userLastname)+"' style=\"border-radius:50%;height: 55px;width: 55px;position: absolute;bottom: 0px;\" onerror=\"userImageOnErrorReplace(this)\" >"
                              +"<img id='upload_profile' class='' src='images/upload.png' style=\"cursor: pointer;float: right;position: absolute;width: 14px;right: 0px;bottom: 0px;\" >"
                              //+uploadpopUp() //-----------This is to show the toggle options like upload from the system and Upload from the gallery
                            +"</div>"
                         +"</div>"
                         
                         +"<div style=\"width: 70%;height: 90%;float:left;padding-left: 2%;position: relative;\">"
                               +"<div style=\"width: 90%;float: left;position: absolute;bottom: 0px;\">" 
                                   +"<div class='proHeaderCss' style=\"width: 99%;height: auto;font-size: 15px;\">"+replaceSpecialCharacter(jsonDataProfile[0].userFirstname)+" "+replaceSpecialCharacter(jsonDataProfile[0].userLastname)+"</div>"
                                   +"<div style=\"width: 99%;font-family: OpenSansRegular;font-size: 10px;height: auto;float: left;\">User Name: "+jsonDataProfile[0].login_name+"</div>"
                               +"</div>" 
                             
                         +"</div>"
                         +"<div style=\"width:2%;float:right;\">"
                             +"<img onclick='closeUserPopUp()' src=\"images/close.png\" style=\"padding-top: 15px;cursor:pointer;\">"
                         +"</div>"
                  + "</div>"
                  
                  +"<div class='MyPflpopupOpt' style='width: 95%; border-bottom: 1px solid #A6A6A6;margin-left: 2%;float:left;margin-bottom:3%;padding-left: 5%;border-top: 1px solid #a6a6a6;'>"
                      +"<div style='float:left; height: 100%; padding-top: 3%; width: 16%;text-align: center;' id='Profile' class='tabActive defaultTabsPro Profile_cLabelHtml'></div>"
                      +"<div style='float:left; margin-left: 3%; height: 100%; padding-top: 3%;' id='options' class='tabInactive defaultTabsPro Options_cLabelHtml'></div>"
                  +"</div>"
              
           +"</div>"
       
           +"<div id='usersDetailsDiv1' style='width:100%; height:68%; overflow:auto;'>"
           
               +"<form action='${path}/UserProfileImageUpload' id='uploadUserProfileForm' name='uploadUserProfileForm' method='post' enctype='multipart/form-data'>"
                 +"<label style='width: 160px;display:none;' class='imageUpload'>"
                     +"<input style='left:0px;width:150px;top:0px;' type='file' multiple='multiple' id='profileFileName' accept='image/*' name='profileFileName' class='file' onchange='readURLProfile(this);'>"
                 +"</label>"
             +"</form>"
                   +"<iframe id='upload_target' name='upload_target' style='display: none;'></iframe>"
                 +"<input id='x1' type='hidden'  name='x1'>"
                 +"<input id='y1' type='hidden'  name='y1'>"
                 +"<input id='x2' type='hidden'  name='x2'>"
                 +"<input id='y2' type='hidden'  name='y2'>"
           
               +"<form style=\"width:100%; height:100%;\" >"
                 +"<div class='tlabelDemo' ><span class='tlabelSpan first_name_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' id='userProFirstName' value='"+replaceSpecialCharacter(jsonDataProfile[0].userFirstname)+"' >"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpan last_name_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' id='userProlastName' value='"+jsonDataProfile[0].userLastname+"'>"
                 +"</div>" 
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpan user_role_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' id='' value='"+jsonDataProfile[0].userRoleVal+"' style='background-color: #ececec;' readonly='readonly'>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpan Company_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' id='userProCompany' value='"+replaceSpecialCharacter(jsonDataProfile[0].userCompany)+"'>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpan Job_Title_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' id='userProJobTitle' value='"+replaceSpecialCharacter(jsonDataProfile[0].userJob)+"'>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpan Department_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' id='userProDepartment' value='"+replaceSpecialCharacter(jsonDataProfile[0].department)+"'>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpan Mobile_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' id='userProMobile' value='"+jsonDataProfile[0].phone+"'>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpan Work_Phone_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' placeholder='Work Phone 1' id='userProWork1' value='"+jsonDataProfile[0].work_number1+"'>"
                 +"<input type='text' style='margin-top: 1%; margin-left: 25%;' class='theightdemo' placeholder='Work Phone 2' id='userProWork2' value='"+jsonDataProfile[0].work_number2+"'>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpan Home_Phone_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' placeholder='Home Phone 1' id='userProHome' value='"+jsonDataProfile[0].user_home_number1+"'>"
                 +"<input type='text' style='margin-top: 1%; margin-left: 25%;' class='theightdemo' placeholder='Home Phone 2' id='userProHome2' value='"+jsonDataProfile[0].user_home_number2+"'>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpan Email_cLabelText'></span>"
                 +"<input type='text' class='theightdemo' placeholder='Email 1' id='userProEmailId' value='"+jsonDataProfile[0].user_email1+"'>"
                 +"<input type='text' style='margin-top: 1%; margin-left: 25%;' class='theightdemo' placeholder='Email 2' id='userProEmailId2' value='"+jsonDataProfile[0].user_email2+"'>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpanText Office_Address_cLabelText'></span>"
                 +"<textarea class='tareademo' id='userProOfficeAddress'>"+replaceSpecialCharacter(jsonDataProfile[0].work_address)+"</textarea>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpanText Home_Address_cLabelText'></span>"
                 +"<textarea class='tareademo' id='userProHomeAddress'>"+replaceSpecialCharacter(jsonDataProfile[0].home_address)+"</textarea>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpanText Expertise_cLabelText'></span>"
                 +"<textarea class='tareademo' id='userProExpertise'>"+replaceSpecialCharacter(jsonDataProfile[0].expertise)+"</textarea>"
                 +"</div>"
                 
                 +"<div class='tlabelDemo' ><span class='tlabelSpanText Biography_cLabelText'></span>"
                 +"<textarea class='tareademo' id='userProBiography'>"+replaceSpecialCharacter(jsonDataProfile[0].biography)+"</textarea>"
                 +"</div>"
                 
               +"</form>"
           +"</div>"
           
        +"<div id='usersDetailsDiv2' style=\"width:100%; height:72%;display:none;\">"
           +"<form style=\"width:100%; height:100%;\" >"
             +"<div class='tlabelHeader' >Change password</div>"
             
             +"<div class='tlabelDemo' ><span class='tlabelSpan'>Old Password: </span>"
             +"<input type='text' class='theightdemo' >"
             +"</div>"
             
             +"<div class='tlabelDemo' ><span class='tlabelSpan'>New Password: </span>"
             +"<input type='text' class='theightdemo' >"
             +"</div>"
             
             +"<div class='tlabelDemo' ><span class='tlabelSpan'>Re-Type Password: </span>"
             +"<input type='text' class='theightdemo' >"
             +"</div>"
             
             +"<div class='tlabelHeader' >Notification</div>"
             +"<ul class='tRadio'>"
                 +"<li class='tlabel2'><img id='notiType_7' class='rmessage' width='18px' height='18px' src='images/repository/radio_uncheck.png'> <span>Get each message</span></li>"
                 +"<li class='tlabel2'><img id='notiType_8' class='rmessage' width='18px' height='18px' src='images/repository/radio_check.png'> <span>No Email Notifications</span></li>"
                 +"<li class='tlabel2'><img id='notiType_9' value='ImpN' class='rmessage' width='18px' height='18px' src='images/repository/radio_uncheck.png'> <span>Important Notifications</span></li>"
                 +"<li class='tlabel2'><img id='notiType_6' class='rmessage' width='18px' height='18px' src='images/repository/radio_uncheck.png'> <span>Daily digest</span></li>"
             +"</ul>"  
             
             +"<div class='tlabelHeader' >Drives</div>"
             +"<ul class='tRadio'>"
                 +"<li class='tlabel2'><img width='18px' height='18px' src='images/repository/CheckDisable.png'> <span>Colabus</span></li>"
                 +"<li class='tlabel2'><img width='18px' height='18px' src='images/repository/Check.png'> <span>test drive</span></li>"
                 +"<li class='tlabel2'><img width='18px' height='18px' src='images/repository/uncheck.png'> <span>customRepository</span></li>"
                 +"<li class='tlabel2'><img width='18px' height='18px' src='images/repository/uncheck.png'> <span>Google Drive</span></li>"
                 +"<li class='tlabel2'><img width='18px' height='18px' src='images/repository/uncheck.png'> <span>OneDrive</span></li>"
                 +"<li class='tlabel2'><img width='18px' height='18px' src='images/repository/uncheck.png'> <span>Box</span></li>"
                 +"<li class='tlabel2'><img width='18px' height='18px' src='images/repository/uncheck.png'> <span>Drop Box</span></li>"
             +"</ul>" 
             
           +"</form>"
       +"</div>"
           
       +"<div id='usersDetailsDiv3' style=\"width:100%; height:72%; overflow:auto;display:none;\">"
           +"<form style=\"width:100%; height:100%;\" >"
                +"<div class='tlabelHeader' >WORKSPACE</div>"
                 
                +"<ul class='tRadio' style='float: left; width: 100%;'>"
                        +"<li class='tlabel2'>"
                        +"<div class='notSubContent'  style='width: 100%; float: left;'>"
                          +"<div style='float:left;'>"
                            +"<input type='checkbox' class='checkheight notiInputClass' name ='wrkSpaceActFeed' id='Notif_Workspace_ActivityFeed_NotiCheckVal' value='' />"
                            +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Workspace_ActivityFeed_NotiCheckId' />"
                          +"</div>"
                             +"<div class ='notContent' style='float:left;' value='' id='Notif_Workspace_ActivityFeed_NotiSubLabel'></div>"
                           +"</div>"     
                     +"</li>"
                     
                     +"<li class='tlabel2'>"
                        +"<div class='notSubContent' style='width: 100%; float: left;'>"
                          +"<div style='float:left;'>"
                            +"<input type='checkbox' class='checkheight notiInputClass' name ='wrkSpaceFolderShare' id='Notif_Workspace_FolderShare_NotiCheckVal' value='' />"
                            +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Workspace_FolderShare_NotiCheckId' />"
                          +"</div>"
                             +"<div class ='notContent' style='float:left;' value='' id='Notif_Workspace_FolderShare_NotiSubLabel'></div>"
                           +"</div>"     
                     +"</li>"
                     
                       +"<li class='tlabel2'>"
                        +"<div class='notSubContent' style='width: 100%; float: left;'>"
                          +"<div style='float:left;'>"
                            +"<input type='checkbox' class='checkheight notiInputClass' name ='wrkSpaceFileShare' id='Notif_Workspace_FileShare_NotiCheckVal' value='' />"
                            +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Workspace_FileShare_NotiCheckId' />"
                          +"</div>"
                             +"<div class ='notContent' style='float:left;' value='' id='Notif_Workspace_FileShare_NotiSubLabel'></div>"
                           +"</div>"     
                     +"</li>"
                     
                +"</ul>" 
                
                +"<div class='tlabelHeader' >IDEAS</div>"
                +"<ul class='tRadio' style='float: left; width: 100%;'>"
                
                 +"<li class='tlabel2'>"
                    +"<div class='notSubContent' style='width: 100%; float: left;'>"
                      +"<div style='float:left;'>"
                        +"<input type='checkbox' class='checkheight notiInputClass' name ='ideasComments' id='Notif_Idea_Comments_NotiCheckVal' value='' />"
                        +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Idea_Comments_NotiCheckId' />"
                      +"</div>"
                         +"<div class ='notContent' style='float:left;' value='' id='Notif_Idea_Comments_NotiSubLabel'></div>"
                       +"</div>"     
                 +"</li>"
                 
                 +"<li class='tlabel2'>"
                    +"<div class='notSubContent' style='width: 100%; float: left;'>"
                      +"<div style='float:left;'>"
                        +"<input type='checkbox' class='checkheight notiInputClass' name ='ideasShare' id='Notif_Idea_Share_NotiCheckVal' value='' />"
                        +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Idea_Share_NotiCheckId' />"
                      +"</div>"
                         +"<div class ='notContent' style='float:left;' value='' id='Notif_Idea_Share_NotiSubLabel'></div>"
                       +"</div>"     
                 +"</li>"
                 
                 +"<li class='tlabel2'>"
                    +"<div class='notSubContent' style='width: 100%; float: left;'>"
                      +"<div style='float:left;'>"
                        +"<input type='checkbox' class='checkheight notiInputClass' name ='ideasDocuments' id='Notif_Idea_Documents_NotiCheckVal' value='' />"
                        +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Idea_Documents_NotiCheckId' />"
                      +"</div>"
                         +"<div class ='notContent' style='float:left;' value='' id='Notif_Idea_Documents_NotiSubLabel'></div>"
                       +"</div>"     
                 +"</li>"
                        
                        
                +"</ul>" 
            
                +"<div class='tlabelHeader' >CALENDAR</div>"
                +"<ul class='tRadio' style='float: left; width: 100%;'>"
                
                +"<li class='tlabel2'>"
                    +"<div class='notSubContent' style='width: 100%; float: left;'>"
                     +"<div style='float:left;'>"
                       +"<input type='checkbox' class='checkheight notiInputClass' name ='calendarTask' id='Notif_Calendar_Tasks_NotiCheckVal' value='' />"
                       +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Calendar_Tasks_NotiCheckId' />"
                     +"</div>"
                       +"<div class ='notContent' style='float:left;' value='' id='Notif_Calendar_Tasks_NotiSubLabel'></div>"
                       +"</div>"     
                +"</li>"
                        
                +"<li class='tlabel2'>"
                   +"<div class='notSubContent' style='width: 100%; float: left;'>"
                     +"<div style='float:left;'>"
                       +"<input type='checkbox' class='checkheight notiInputClass' name ='calendarWFTask' id='Notif_Calendar_WorkflowTasks_NotiCheckVal' value='' />"
                       +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Calendar_WorkflowTasks_NotiCheckId' />"
                   +"</div>"
                    +"<div class ='notContent' style='float:left;' value='' id='Notif_Calendar_WorkflowTasks_NotiSubLabel'></div>"
                     +"</div>"     
                +"</li>"
                        
                +"<li class='tlabel2'>"
                  +"<div class='notSubContent' style='width: 100%; float: left;'>"
                    +"<div style='float:left;'>"
                       +"<input type='checkbox' class='checkheight notiInputClass' name ='calendarDocTask' id='Notif_Calendar_DocumentTasks_NotiCheckVal' value='' />"
                       +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Calendar_DocumentTasks_NotiCheckId' />"
                    +"</div>"
                      +"<div class ='notContent' style='float:left;' value='' id='Notif_Calendar_DocumentTasks_NotiSubLabel'></div>"
                 +"</div>"     
                +"</li>"
                        
                +"<li class='tlabel2'>"
                  +"<div class='notSubContent' style='width: 100%; float: left;'>"
                   +"<div style='float:left;'>"
                     +"<input type='checkbox' class='checkheight notiInputClass' name ='calendarWDocTask' id='Notif_Calendar_WorkspaceDocTasks_NotiCheckVal' value='' />"
                     +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Calendar_WorkspaceDocTasks_NotiCheckId' />"
                   +"</div>"
                     +"<div class ='notContent' style='float:left;' value='' id='Notif_Calendar_WorkspaceDocTasks_NotiSubLabel'></div>"
                   +"</div>"     
                +"</li>"
                        
               +"<li class='tlabel2'>"
                 +"<div class='notSubContent' style='width: 100%; float: left;'>"
                  +"<div style='float:left;'>"
                   +"<input type='checkbox' class='checkheight notiInputClass' name ='calendarEvents' id='Notif_Calendar_Events_NotiCheckVal' value='' />"
                   +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Calendar_Events_NotiCheckId' />"
                  +"</div>"
                     +"<div class ='notContent' style='float:left;' value='' id='Notif_Calendar_Events_NotiSubLabel'></div>"
                 +"</div>"     
              +"</li>"
                        
              +"<li class='tlabel2'>"
               +"<div class='notSubContent' style='width: 100%; float: left;'>"
                 +"<div style='float:left;'>"
                   +"<input type='checkbox' class='checkheight notiInputClass' name ='calendarSprintTask' id='Notif_Calendar_SprintTasks_NotiCheckVal' value='' />"
                   +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Calendar_SprintTasks_NotiCheckId' />"
                 +"</div>"
                   +"<div class ='notContent' style='float:left;' value='' id='Notif_Calendar_SprintTasks_NotiSubLabel'></div>"
                +"</div>"     
              +"</li>"
                        
              +"<li class='tlabel2'>"
               +"<div class='notSubContent' style='width: 100%; float: left;'>"
                 +"<div style='float:left;'>"
                  +"<input type='checkbox' class='checkheight notiInputClass' name ='calendarIdeaTask' id='Notif_Calendar_IdeaTasks_NotiCheckVal' value='' />"
                  +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Calendar_IdeaTasks_NotiCheckId' />"
                 +"</div>"
                   +"<div class ='notContent' style='float:left;' value='' id='Notif_Calendar_IdeaTasks_NotiSubLabel'></div>"
               +"</div>"     
              +"</li>"
                        
                +"</ul>" 
               
                +"<div class='tlabelHeader' >AGILE</div>"
                +"<ul class='tRadio' style='float: left; width: 100%;'>"
                
                +"<li class='tlabel2'>"
                   +"<div class='notSubContent' style='width: 100%; float: left;'>"
                     +"<div style='float:left;'>"
                      +"<input type='checkbox' class='checkheight notiInputClass' name ='agileComments' id='Notif_Agile_Comments_NotiCheckVal' value='' />"
                      +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Agile_Comments_NotiCheckId' />"
                     +"</div>"
                       +"<div class ='notContent' style='float:left;' value='' id='Notif_Agile_Comments_NotiSubLabel'></div>"
                   +"</div>"     
                  +"</li>"
                        
                  +"<li class='tlabel2'>"
                   +"<div class='notSubContent' style='width: 100%;float: left;'>"
                     +"<div style='float:left;'>"
                      +"<input type='checkbox' class='checkheight notiInputClass' name ='agileDocument' id='Notif_Agile_Documents_NotiCheckVal' value='' />"
                      +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Agile_Documents_NotiCheckId' />"
                     +"</div>"
                       +"<div class ='notContent' style='float:left;' value='' id='Notif_Agile_Documents_NotiSubLabel'></div>"
                   +"</div>"     
                  +"</li>"
                        
                +"</ul>"
                
                +"<div class='tlabelHeader' >REPOSITORY</div>"
                +"<ul class='tRadio' style='float: left; width: 100%;'>"
                
                    +"<li class='tlabel2'>"
                       +"<div class='notSubContent' style='width: 100%; float: left;'>"
                         +"<div style='float:left;'>"
                          +"<input type='checkbox' class='checkheight notiInputClass' name ='repoComments' id='Notif_Repository_Comments_NotiCheckVal' value='' />"
                          +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Repository_Comments_NotiCheckId' />"
                         +"</div>"
                           +"<div class ='notContent' style='float:left;' value='' id='Notif_Repository_Comments_NotiSubLabel'></div>"
                       +"</div>"     
                      +"</li>"
                        
                      +"<li class='tlabel2'>"
                       +"<div class='notSubContent' style='width: 100%; float: left;'>"
                         +"<div style='float:left;'>"
                          +"<input type='checkbox' class='checkheight notiInputClass' name ='repFolShare' id='Notif_Repository_FolderShare_NotiCheckVal' value='' />"
                          +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Repository_FolderShare_NotiCheckId' />"
                         +"</div>"
                           +"<div class ='notContent' style='float:left;' value='' id='Notif_Repository_FolderShare_NotiSubLabel'></div>"
                       +"</div>"     
                      +"</li>"
                        
                      +"<li class='tlabel2'>"
                       +"<div class='notSubContent' style='width: 100%; float: left;'>"
                         +"<div style='float:left;'>"
                          +"<input type='checkbox' class='checkheight notiInputClass' name ='repoDocShare' id='Notif_Repository_DocumentShare_NotiCheckVal' value='' />"
                          +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Repository_DocumentShare_NotiCheckId' />"
                         +"</div>"
                           +"<div class ='notContent' style='float:left;' value='' id='Notif_Repository_DocumentShare_NotiSubLabel'></div>"
                       +"</div>"     
                      +"</li>"
                        
                +"</ul>"
                
                +"<div class='tlabelHeader' >CONNECT</div>"
                +"<ul class='tRadio' style='float: left; width: 100%;'>"
                
                    +"<li class='tlabel2'>"
                       +"<div class='notSubContent' style='width: 100%; float: left;'>"
                         +"<div style='float:left;'>"
                          +"<input type='checkbox' class='checkheight notiInputClass' name ='connUsers' id='Notif_Connect_NewUsers_NotiCheckVal' value='' />"
                          +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Connect_NewUsers_NotiCheckId' />"
                         +"</div>"
                           +"<div class ='notContent' style='float:left;' value='' id='Notif_Connect_NewUsers_NotiSubLabel'></div>"
                       +"</div>"     
                    +"</li>"
                        
                    +"<li class='tlabel2'>"
                       +"<div class='notSubContent' style='width: 100%; float: left;'>"
                         +"<div style='float:left;'>"
                          +"<input type='checkbox' class='checkheight notiInputClass' name ='connMess' id='Notif_Connect_Messages_NotiCheckVal' value='' />"
                          +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Connect_Messages_NotiCheckId' />"
                         +"</div>"
                           +"<div class ='notContent' style='float:left;' value='' id='Notif_Connect_Messages_NotiSubLabel'></div>"
                       +"</div>"     
                    +"</li>"
                    
                +"</ul>"
                
                +"<div class='tlabelHeader' >MY ZONE</div>"
                +"<ul class='tRadio' style='float: left; width: 100%;'>"
                
                    +"<li class='tlabel2'>"
                       +"<div class='notSubContent' style='width: 100%; float: left;'>"
                         +"<div style='float:left;'>"
                          +"<input type='checkbox' class='checkheight notiInputClass' name ='notesShare' id='Notif_Myzone_NotesShare_NotiCheckVal' value='' />"
                          +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Myzone_NotesShare_NotiCheckId' />"
                         +"</div>"
                           +"<div class ='notContent' style='float:left;' value='' id='Notif_Myzone_NotesShare_NotiSubLabel'></div>"
                       +"</div>"     
                    +"</li>"
                        
                    +"<li class='tlabel2'>"
                       +"<div class='notSubContent' style='width: 100%; float: left;'>"
                         +"<div style='float:left;'>"
                          +"<input type='checkbox' class='checkheight notiInputClass' name ='blogShare' id='Notif_Myzone_BlogShare_NotiCheckVal' value='' />"
                          +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Myzone_BlogShare_NotiCheckId' />"
                         +"</div>"
                           +"<div class ='notContent' style='float:left;' value='' id='Notif_Myzone_BlogShare_NotiSubLabel'></div>"
                       +"</div>"     
                    +"</li>"
                        
                    +"<li class='tlabel2'>"
                       +"<div class='notSubContent' style='width: 100%; float: left;'>"
                         +"<div style='float:left;'>"
                          +"<input type='checkbox' class='checkheight notiInputClass' name ='galShare' id='Notif_Myzone_GalleryShare_NotiCheckVal' value='' />"
                          +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Myzone_GalleryShare_NotiCheckId' />"
                         +"</div>"
                           +"<div class ='notContent' style='float:left;' value='' id='Notif_Myzone_GalleryShare_NotiSubLabel'></div>"
                       +"</div>"     
                    +"</li>"
                        
                    +"<li class='tlabel2'>"
                       +"<div class='notSubContent' style='width: 100%; float: left;'>"
                         +"<div style='float:left;'>"
                          +"<input type='checkbox' class='checkheight notiInputClass' name ='galComments' id='Notif_Myzone_GalleryComments_NotiCheckVal' value='' />"
                          +"<input class='notHiddenVal' type='hidden' value='' id='Notif_Myzone_GalleryComments_NotiCheckId' />"
                         +"</div>"
                           +"<div class ='notContent' style='float:left;' value='' id='Notif_Myzone_GalleryComments_NotiSubLabel'></div>"
                       +"</div>"     
                    +"</li>"
                        
                +"</ul>"
     
                +"<div class='tlabelHeader' >PROJECT ADMIN</div>"
                +"<ul class='tRadio' style='float: left; width: 100%;'>"
                
                +"<li class='tlabel2'>"
                   +"<div class='notSubContent' style='width: 100%; float: left;'>"
                     +"<div style='float:left;'>"
                      +"<input type='checkbox' class='checkheight notiInputClass' name ='projApproval' id='Notif_ProjectAdmin_Approval_NotiCheckVal' value='' />"
                      +"<input class='notHiddenVal' type='hidden' value='' id='Notif_ProjectAdmin_Approval_NotiCheckId' />"
                     +"</div>"
                       +"<div class ='notContent' style='float:left;' value='' id='Notif_ProjectAdmin_Approval_NotiSubLabel'></div>"
                   +"</div>"     
                +"</li>"
                        
                +"</ul>"
                
           +"</form>"
   +"</div>"
           
           +"<div style='width: 100%; height: 8%; border-top: 1px solid #a6a6a6;'>"
                +"<div id='changeDynamic2' onclick='saveUserProData();' style=' margin-right: 4%;' class='createBtn SAVE_cLabelHtml'></div>"
           +"</div>"
             
        +"</div>";

      $('#connectData').show().html(UI);
      
      profileImgProfileFlag = false;
      
     popUpScrollBar('usersDetailsDiv1'); 
     $('#usersDetailsDiv1').find('.mCustomScrollBox').css('width','100%');
     
      /* these code snippits are two change the active inactive tabs in user profile   */  
        $('#userDetailpopUp').find('#Profile').click(function(){
            $(this).toggleClass('tabInactive tabActive');
            $('#userDetailpopUp').find('#options').toggleClass('tabInactive tabActive');
            $('#userDetailpopUp').find('#usersDetailsDiv2, #usersDetailsDiv3').hide();
            $('#userDetailpopUp').find('#usersDetailsDiv1').show();
            $('#userDetailpopUp').find('#changeDynamic2').attr('onclick','').attr('onclick','saveUserProData()').html('save');
        });
        
        $('#userDetailpopUp').find('#options').click(function(){
              
            
            $(this).toggleClass('tabInactive tabActive');
            $('#userDetailpopUp').find('#Profile').toggleClass('tabInactive tabActive');
            $('#userDetailpopUp').find('#usersDetailsDiv1, #usersDetailsDiv3').hide();
            $('#userDetailpopUp').find('#usersDetailsDiv2').show();
            
               loadOptionsDiv();
                                                   
        });
        
        $("#upload_profile").on('click', function(e){
                e.preventDefault();
                $("#profileFileName:hidden").trigger('click');
                 /* $("#Profile_Upload").slideToggle(function(){
                   $(this).css('overflow','');
                   });*/
            });
        
        $("#uploadCompanyImage").on('click', function(e){
                e.preventDefault();
                $("#profileFileName:hidden").trigger('click');
                
            });
        
        /*   $('#userDetailpopUp').find('#changeDynamic2').click(function(){
             var formname = 'uploadUserProfileForm';

                     var uploadForm = document.getElementById(formname);
                     uploadForm.target = 'upload_target';
                     uploadForm.submit(); 
        });*/
        loadNewAccountLabel('postloginonload');
          
      
 }
 /* function readURLProfile(input) {
     alert("Inside the read URL FILE");  
     if (input.files && input.files[0]) {
           var reader = new FileReader();

           reader.onload = function (e) {
               $('#profileImgProfile').attr('src', e.target.result);
               alert("source attrb::"+e.target.result);
               fileExtension = input.files[0].name.split('.').pop().toLowerCase(); // this code is to get the image extension
               fileName = input.files[0].name.split('.')[0]; // this code is to get the image Name
               alert("FileName and FileExtension>>>"+fileName+"."+fileExtension);
               $('#Profile_Upload').hide();
               profileImgProfileFlag = true;
           };
          reader.readAsDataURL(input.files[0]);
        
       }
   }  */
   
   
   function readURLProfile(input){ //-------------this is for profile pic
       
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           //fileExtension = input.files[0].name.split('.').pop().toLowerCase(); // this code is to get the image extension
           //fileName = input.files[0].name.split('.')[0]; // this code is to get the image Name
           //console.log("name--->"+ input.files[0].name.split('.')[0]);
           
           $('#albumFileExtn').val(input.files[0].name.split('.').pop().toLowerCase());
           $('#albumFileName').val(input.files[0].name.split('.')[0]);
           
           $('#Profile_Upload').hide();
           reader.onload = function (e) {
           
              //--- below code is used for image cropper
               initCrop(e.target.result);
               var uploadCrop = imageCrop.data('cropper');
               $("#upload-result").on("click",function(){
                      $('#profilepictransparentDiv').hide();
                      uploadCrop.getCroppedCanvas().toBlob((blob) => {
                             $('#profileImgProfile').attr('src', URL.createObjectURL(blob));
                             profileBlobData= blob; 
                             fileExtension ="jpeg";
                             fileName = userId+".jpeg";
                             closeCropper();
                             profileImgProfileFlag = true;
                              
                      },'image/jpeg');
               });
                 
                //--- above code is used for image cropper
                
           }	 
           reader.readAsDataURL(input.files[0]);
       }  
  }
  // var profileBlobData;
  
  function readURLNewProfile(input) { //---------this is for viola screen
      // console.log(input.files[0].toString());
        if (input.files && input.files[0]) {
           var reader = new FileReader();
           

           $('#albumFileExtn').val(input.files[0].name.split('.').pop().toLowerCase());
           $('#albumFileName').val(input.files[0].name.split('.')[0]);
           
           reader.onload = function (e) {
              //--- below code is used for image cropper
               initCrop(e.target.result);
               var uploadCropvoila = imageCrop.data('cropper');
               $("#upload-result").on("click",function(){
                   $('#profilepictransparentDiv').hide();
                   uploadCropvoila.getCroppedCanvas().toBlob((blob) => {
                             $('#upload_Newprofile').attr('src', URL.createObjectURL(blob));
                             profileBlobData= input.files[0]; 
                             fileExtension ="jpeg";
                             fileName = userId+".jpeg";
                             closeCropper();
                             profileImgProfileFlag = true;
                    },'image/jpeg');
               });
           }	 
           reader.readAsDataURL(input.files[0]);
       }  
       
   }
  
  
  
   function dataURItoBlob(dataURI) {
         var byteString;
         if (dataURI.split(',')[0].indexOf('base64') >= 0)
         byteString = atob(dataURI.split(',')[1]);
         else
         byteString = unescape(dataURI.split(',')[1]);
         var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
         var ia = new Uint8Array(byteString.length);
         for (var i = 0; i < byteString.length; i++) {
         ia[i] = byteString.charCodeAt(i);
         }
      return new Blob([ia], {type:mimeString});
     }	 
      
   /* Upload profile Image data using Blob */
   
   function uploadProfileImgBlob(fileName){
         
          var formData = new FormData();    
         formData.append('file',  profileBlobData, fileName);
         var uFileName = fileName;
         $.ajax({		         			     
                 url: path+'/ProfilePicCropFile?userId='+userId,			      
                 type:"POST",
                 data:formData,
                 processData: false,
                 contentType: false, 
                 cache: false,
                 mimeType: "textPlain",
               error: function(jqXHR, textStatus, errorThrown) {
                  checkError(jqXHR,textStatus,errorThrown);
                  $("#loadingBar").hide();
                  timerControl("");
               },
               success: function(result) {
                   if(result=='success'){
                       $("img#profileImg").attr('src','').attr("src", "lighttpdpath/userimages/"+uFileName+"?timestamp="+new Date().getTime());
                       $('img.userIcon_'+userId).attr("src", "lighttpdpath/userimages/"+uFileName+"?timestamp="+new Date().getTime());
                   }
                   $("#loadingBar").hide();
                   timerControl("");
               }
         });
       
       
   }
   
  function uploadToRepo(fileExtension){
         
         var aExtn = $('#albumFileExtn').val();
         var aName = $('#albumFileName').val();
           
         console.log('Selected file: ' + aName);
         console.log('Selected aExtn: ' + aExtn);
         
          var formData = new FormData();    
         formData.append('file',  profileBlobData, aName+"."+fileExtension);
         
         $.ajax({		         			     
                 url: path+'/UserProfileImageUpload',			      
                 type:"POST",
                 data:formData,
                 processData: false,
                 contentType: false, 
                 cache: false,
                 mimeType: "textPlain",
               error: function(jqXHR, textStatus, errorThrown) {
                  checkError(jqXHR,textStatus,errorThrown);
                  $("#loadingBar").hide();
                  timerControl("");
               },
               success: function(result) {
                   //console.log("album upload-->"+result)
                   // if(result=='success'){
                   //}
                   
               }
         });
       
         
  }
  

 
  $("#addPhoto").on('click', function(e){
                e.preventDefault();
                $("#profileFileName:hidden").trigger('click');
                
            }); 
  
 /* function uploadFromViola(){
         var formname = 'uploadUserProfileFormNew';
         var uploadForm = document.getElementById(formname);
         uploadForm.target = 'upload_targetNew';
         uploadForm.submit();
 } */
 
 function closeUserPopUp(){
        $('#transparentDiv').hide();
        $('#connectData').html('');
 }	
 
 function getChangePwdDiv(){
     sessionStorage.test =='Y';
        if(dirCheckValue == 'Y'){
           $.ajax({
         url : "${path}/postLoginAction.do",
         type : "POST",
         data : {
         act : "insertVoilaUpdateAD",
             
             fileName  :  ""
         },
         error: function(jqXHR, textStatus, errorThrown) {
                     checkError(jqXHR,textStatus,errorThrown);
                     $("#loadingBar").hide();
                     timerControl("");
                     },
         success : function(result) {
            sessionStorage.test = 'N'
            closeUserPopUp();	
            $('.popUpContent').hide();
               $('#transparentDiv').hide();
         }
     });
         
     }else{
           $('#viola-div').hide();
         $('#ch-pwd-div').show();
         $("input#confirmEmail").val(userEmail);
       }
     
 }
 
 function inviteUsersPopup(){
     userRoleId = '${userRoleId}';
         if(userRoleId == "2"){
         $("div#welcome-Data").show();
         $("div#viola-div").hide();
         $("div#ch-pwd-div").hide();
         $("div#transparentDiv").show();
         $("div#inviteNewusers").show();
         }
         else {
         $("div#welcome-Data").hide();
         $("div#transparentDiv").css('display','none');
         }
         
 }
 
 
 function saveUserProData() {
      
          $("#loadingBar").show();
          timerControl("start");
     
          if(profileImgProfileFlag){
              uploadToRepo(fileExtension); // to upload image in repository
              uploadProfileImgBlob(fileName);
          }else{
              fileName ="";
              fileExtension="0";
          }	 	   
             var userFirstName  =  $("input#userProFirstName").val();
             var userLastName   =  $("input#userProlastName").val().trim();
             var userCompany    =  $("input#userProCompany").val();
             var userJobTitle   =  $("input#userProJobTitle").val();
             var userDepartment =  $("input#userProDepartment").val();
             var userExpertise  =  $("textarea#userProExpertise").val();
             var userBiography  =  $("textarea#userProBiography").val();
             var userUserEmailId = $("input#userProEmailId").val().trim();
             var userUserEmailId1= $("input#userProEmailId2").val().trim();
             var userUserPhone   = $("input#userProMobile").val();
             var userWorkNumber1 = $("input#userProWork1").val();
             var userWorkNumber2 = $("input#userProWork2").val();
             var userHomeNumber1 = $("input#userProHome").val();
             var userHomeNumber2 = $("input#userProHome2").val();
             var userOfficeAddress=$("textarea#userProOfficeAddress").val();
             var userHomeAddress = $("textarea#userProHomeAddress").val();
             var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
             var phoneValidation = /^\s*[0-9, -+.()\s]+\s*$/
                     

     if(userUserPhone != "" && userUserPhone != null){
         if(!phoneValidation.test(userUserPhone) ) {
             alertFun(getValues(companyAlerts,"Alert_mobnum"),'warning');
              $("#loadingBar").hide();
              timerControl("");
             return false;
         }
     }
     if(userWorkNumber1 != "" && userWorkNumber1 != null){
         if(!phoneValidation.test(userWorkNumber1) ) {
             alertFun(getValues(companyAlerts,"Alert_worknum"),'warning');
              $("#loadingBar").hide();
              timerControl("");
             return false;
         }
     }
     if(userWorkNumber2 != "" && userWorkNumber2 != null){
         if(!phoneValidation.test(userWorkNumber2) ) {
             alertFun(getValues(companyAlerts,"Alert_worknum"),'warning');
              $("#loadingBar").hide();
              timerControl("");
             return false;
         }
     }
     if(userHomeNumber1 != "" && userHomeNumber1 != null){
         if(!phoneValidation.test(userHomeNumber1) ) {
             alertFun(getValues(companyAlerts,"Alert_homenum"),'warning');
              $("#loadingBar").hide();
              timerControl("");
             return false;
         }
     }
     if(userHomeNumber2 != "" && userHomeNumber2 != null){
         if(!phoneValidation.test(userHomeNumber2) ) {
             alertFun(getValues(companyAlerts,"Alert_homenum"),'warning');
              $("#loadingBar").hide();
              timerControl("");
             return false;
         }
     }
     
     
     if(userFirstName==""){
           //alertFun("Enter First Name.",'warning');
           alertFun(getValues(companyAlerts,"Alert_EnterFname"),'warning');
           $("#loadingBar").hide();
           timerControl("");
          return false;
       }
      
      
      if( userUserEmailId == "" &&   userUserEmailId1 == ""){
           alertFun(getValues(companyAlerts,"Alert_EnterEmail"),'warning');
           $("#loadingBar").hide();
           timerControl("");
          return false;
      }else{
       if(userUserEmailId == "" || userUserEmailId == null){
           alertFun(getValues(companyAlerts,"Alert_EnterPriMail"),'warning');
           $("#loadingBar").hide();
           timerControl("");
          return false;
          }
      
      }
      
       var characterReg = /^\s*[a-zA-Z0-9,\s]+\s*$/;
             if(!characterReg.test(userFirstName)) {
             //alertFun("Special characters are not allowed.",'warning');
             alertFun(getValues(companyAlerts,"Alert_specialCharacter"),'warning');
             $("#loadingBar").hide();
              timerControl("");
             return false;
           }
           
           if(userUserEmailId == "" || userUserEmailId == null){
               
           }else{
               if(!emailReg.test(userUserEmailId.trim()) ) {
                  //parent.alertFun("Please enter valid Email",'warning');
                  alertFun(getValues(companyAlerts,"Alert_EmailInvalid"),'warning');
                  $("#loadingBar").hide();
                  timerControl("");
                 return false;
             }
         }	
          if(userUserEmailId1 == null || userUserEmailId1 == ""){   
          }else{
              if(!emailReg.test(userUserEmailId1) ) {
                  //parent.alertFun("Please enter valid Email",'warning');
                  alertFun(getValues(companyAlerts,"Alert_EmailInvalid"),'warning');
                  $("#loadingBar").hide();
                  timerControl("");
                 return false;
             }	
           }
          /* if(fileExtension == '0'){
              var path=$("#profileImg").attr('src');
              var lIndx = path.substring(path.lastIndexOf("/") + 1, path.length);
              var ext=lIndx.split(".");
              var extn=ext[1].split("?");
              fileExtension=extn[0];
           } */
           //alert(fileName+"--"+fileExtension);   
            
         $.ajax({
                 url:"${path}/postLoginAction.do",
                 type:"POST",
                 data:{act:"saveUserProfileData", userFirstName:userFirstName,
                              userLastName   :userLastName,  
                              userCompany    :userCompany ,
                              userJobTitle   :userJobTitle,
                              userDepartment :userDepartment,
                              userExpertise  :userExpertise,
                              userBiography  :userBiography,
                              userUserEmailId :userUserEmailId,
                              userUserEmailId1:userUserEmailId1,
                              userUserPhone   :userUserPhone,
                              userWorkNumber1 :userWorkNumber1,
                              userWorkNumber2 :userWorkNumber2,
                              userHomeNumber1 : userHomeNumber1,
                              userHomeNumber2 : userHomeNumber2,
                              userOfficeAddress:userOfficeAddress,
                              userHomeNumber2:userHomeNumber2,
                              uPhotoId:uPhotoId,
                              uDefaultType:uDefaultType,   
                              fileExtension:fileExtension,
                              fileName:fileName,
                              userHomeAddress :userHomeAddress },
                 error: function(jqXHR, textStatus, errorThrown) {
                             checkError(jqXHR,textStatus,errorThrown);
                             $("#loadingBar").hide();
                             timerControl("");
                             },
                 success:function(result){
                      checkSessionTimeOut(result);
                      
                     if(result != 'success'){
                          //alertFun("Could not update User Profile",'warning');
                          alertFun(getValues(companyAlerts,"Alert_UpdateUserProfile"),'warning');
                          $('#transparentDiv').hide();
                          $("#loadingBar").hide();
                           timerControl("");
                           return false;
                     }else{					
                         
                          /* this code snippet is to reload the page if update happened in company contacts, to refresh the page */
                          //var url = window.location.href; 
                          //var pAct = url.split('=')[1]; 
                          //  if(pAct == 'comContact')
                         //	   location.reload();
                         
                          alertFun(getValues(companyAlerts,"Alert_ProfileUpdated"),'warning');
                          var userId = '${userId}';
                 
                         // if(fileExtension != "0" && profileImgProfileFlag!=true){
                         //    setUserNewPic(fileExtension);
                         //}  
                         $('img#profileImg').attr('title',replaceSpecialCharacter(userFirstName+' '+userLastName));
                         $('#transparentDiv ,#userDetailpopUp').hide();
                         $("#loadingBar").hide();	
                         timerControl("");
                     }
                     uPhotoId="";
                     fileName="";
                 } 
             });
             
      }	
 
 
 function setUserNewPic(imgExtn){
     var userId = '${userId}';
     $("img#profileImg").attr('src','').attr("src", "lighttpdpath/userimages/"+userId+"."+imgExtn+"?timestamp="+new Date().getTime());
     $('img.userIcon_'+userId).attr("src", "lighttpdpath/userimages/"+userId+"."+imgExtn+"?timestamp="+new Date().getTime());
 }
 
 
  function loadOptionsDiv(){
     $("#loadingBar").show();	
     timerControl("start");
     
     $.ajax({
             url:"${path}/postLoginAction.do",
             type:"POST",
             data:{act:"loadOptionsDetails"},
             error: function(jqXHR, textStatus, errorThrown) {
                     checkError(jqXHR,textStatus,errorThrown);
                     $("#loadingBar").hide();
                     timerControl("");
                     },
             success:function(result){
                 checkSessionTimeOut(result);
                 //console.log("result======================"+result);
                 var res = result.split("#@@#");
                                        
                     $('#usersDetailsDiv2').html('').html(res[0]);
                     loadNewAccountLabel('postloginOptions');    
                  $('#usersDetailsDiv2').find('.mCustomScrollBox').css('width','100%');
                 
                    $('#userDetailpopUp').find('.rmessage').click(function(){
                        $('#userDetailpopUp').find('.rmessage').attr('src','images/repository/radio_uncheck.png');
                        $(this).attr('src','images/repository/radio_check.png');
                        
                        if($(this).attr('id') == 'notiType_9'){
                            $('#userDetailpopUp').find('#usersDetailsDiv2').hide();
                            $('#userDetailpopUp').find('#usersDetailsDiv3').show();
                         
                            $('#usersDetailsDiv3').mCustomScrollbar("destroy");
                            popUpScrollBar('usersDetailsDiv3');
                            $('#usersDetailsDiv3').find('.mCustomScrollBox').css('width','100%');
                 
                            loadUserNotification();
                     
                            
                        }
                    });
                 
                    $('#userDetailpopUp').find('#changeDynamic2').attr('onclick','').attr('onclick','saveUserPwdData()').html("save");
                    //$('#usersDetailsDiv2').mCustomScrollbar("destroy");
                    $('#usersDetailsDiv2').css('overflow','auto');
                    popUpScrollBar('usersDetailsDiv2');
                    $('#usersDetailsDiv2').mCustomScrollbar("update");
                    $("#loadingBar").hide();	
                     timerControl("");
                     loadNewAccountLabel('postloginOptions');
                 if(dirCheckValue == 'Y'){
                           $('.Change_password_cLabelHtml').css({'display':'none'});
                         $('div#usersDetailsDiv2').find('.tlabelDemo').css({'display':'none'});
                 }else{
                         $('.Change_password_cLabelHtml').css({'display':'block'});
                         $('div#usersDetailsDiv2').find('.tlabelDemo').css({'display':'block'});
                 }
                 
             }
         });
         
     } 
  
  function cassandraDisable(obj){
         if($(obj).attr('src').indexOf('images/selcettwo.png') != -1){
            $('#disableCassandraNotify').attr('src','images/selcet.png').attr('status','N');
         }else{
             $('#disableCassandraNotify').attr('src','images/selcettwo.png').attr('status','Y');	
         }
      }
  function saveUserPwdData(){
          
          $("div#notificationType").hide();
             var oldPassword  =  $("input#userOldPassword").val();
             var newPassword  =  $("input#userNewPassword").val();
             var userReNewPwd =  $("input#userNewRePassword").val();
             
             if(oldPassword!=''){
                 if( newPassword == '' ){
                 //alertFun("Please enter your New Password.",'warning');
                 alertFun(getValues(companyAlerts,"Alert_newPassword"),'warning');
                 $("input#userNewPassword").focus();
                 $("#loadingBar").hide();		
                 return false;
                 }
             }
             if(newPassword!=''){
                 if( oldPassword == '' ){
                     //alertFun("Please enter your Old Password.",'warning');
                     alertFun(getValues(companyAlerts,"Alert_oldPassword"),'warning');
                     $("input#userOldPassword").focus();
                     $("#loadingBar").hide();		
                     return false;
                     }
             }
             if(newPassword!=''){
                 if( userReNewPwd == '' ){
                     // alert('Please re-type your New Password!');
                     //alertFun("Please re-type your New Password.",'warning');
                     alertFun(getValues(companyAlerts,"Alert_RetypePassword"),'warning');
                     $("input#userNewRePassword").focus();
                     $("#loadingBar").hide();		
                     return false;
                 }
             }if( userReNewPwd != newPassword ){
                 // alert('Re-type Password does not match New Password!');
                 //alertFun("Re-type Password does not match New Password.",'warning');
                 alertFun(getValues(companyAlerts,"Alert_PasswordError"),'warning');
                 $("#userNewRePassword").focus();
                 $("#loadingBar").hide();		
                 return false;
             }
             var confdomain = $("input#confdomain").val();
             var confuser = $("input#confuser").val();
             var confpass = $("input#confpass").val();
             var confStatus = $("#confimage").attr('value');
              
             if(confStatus == 'Y'){
                  if(confdomain == ''){
                     alertFun(getValues(companyAlerts,"Alert_Confluencedomain"),'warning');
                     return false;
                 }else if(confuser == ''){
                     alertFun(getValues(companyAlerts,"Alert_enterUsername"),'warning');
                     return false;
                 }else if(confuser != '' && confpass ==''){
                     alertFun(getValues(companyAlerts,"Alert_Confluencepassword"),'warning');
                     return false;
                 }
                  
             }
                 
             $("#loadingBar").show();
             timerControl("start");
             loaduserDriveData();
                 if(radioCheck==''){
                     radioCheck ='8';
                 }
                 
             
             
             var glbCassandraStatus = $('#disableCassandraNotify').attr('status');
             //console.log("flaged value here===> "+glbCassandraStatus);
             
         
             $.ajax({
                 url:"${path}/postLoginAction.do",
                 type:"POST",
                 data:{act:"profileUserChangePwd",oldPassword:oldPassword,
                                                  newPassword:newPassword,
                                                  userReNewPwd:userReNewPwd,
                                                  confluenceDomain:confdomain,
                                                  confluenceAvail:confStatus,
                                                  confluenceUn:confuser,
                                                  confluencePass:confpass,
                                                  selectedvalue:radioCheck,
                                                  driveUserData:uData,
                                                  CheckedVal:CheckedVal,
                                                  xmlData:xmlData,
                                                  flagVal:flagVal,
                                                  glbCassandraStatus:glbCassandraStatus},
                         error: function(jqXHR, textStatus, errorThrown) {
                         checkError(jqXHR,textStatus,errorThrown);
                         $("#loadingBar").hide();
                         timerControl("");
                         },
                 success:function(result){
                  checkSessionTimeOut(result);
                   CheckedVal='';
                   if(result=='passwordUpdate'){
                      //alertFun("password has been changed.",'warning');
                      alertFun(getValues(companyAlerts,"Alert_PasswordChanged"),'warning');
                      $("#loadingBar").hide();	
                      timerControl("");
                   }	
                   if(result=='false'){
                         //alertFun("Please enter the currect oldpassword.",'warning');
                         alertFun(getValues(companyAlerts,"Alert_oldPasswordError"),'warning');
                         $("#loadingBar").hide();	
                         timerControl("");
                         return false;
                      }else{
                     //   closeUserProfile();
                      }			 
                      if(result=='notificationUpdate'){
                         //alertFun("notification type has been changed .",'warning');
                         alertFun(getValues(companyAlerts,"Alert_ProfileUpdated"),'warning');
                         $("#loadingBar").hide();	
                         timerControl("");
                      
                      }
                      if(result == 'drivesUpdate'){
                       //alertFun("Drive type has been changed.",'warning');
                       alertFun(getValues(companyAlerts,"Alert_DriveTypeChanged"),'warning');
                       
                       $("#loadingBar").hide();	
                       timerControl("");
                     } 
                     
                      if(result == ''){
                       //alertFun("Drive type has been changed.",'warning');
                       alertFun(getValues(companyAlerts,"Alert_ProfileUpdated"),'warning');
                       $("#loadingBar").hide();	
                       timerControl("");
                     } 
                 $('#transparentDiv ,#userDetailpopUp').hide();
             //	 closeUserProfile();
                 $("#loadingBar").hide();	
                 timerControl("");
              }
            });
          }
  
             
  
  
     var uData ="";   
     function loaduserDriveData(){
         var userId = "${sessionScope['com.user.userId']}"; 
         uData = "<userData>";
         $('div.userDriveClass').each(function() {
         var id  = $(this).attr('id').split('_')[1];
         var userName = $('div#displayDiv_'+id).find("input#userDriveLoginName_"+id).val();
         var userPwd = $('div#displayDiv_'+id).find("input#userDriveLoginpwd_"+id).val();
         
         //alert("id------"+id);
         var checkStatus = $('img#userDrive_'+id).attr('class');
         var defaultType = $('img#userDrive_'+id).attr('defaultType');
         var userCheckValue = $("img#userDrive_"+id).attr('value');
            uData +="<user id=\""+userId+"\">";
            uData +="<userID>"+userId+"</userID>";
         uData +="<id>"+id+"</id>";
         uData +="<userCheckStatus>"+checkStatus+"</userCheckStatus>";
         uData +="<userLoginName>"+userName+"</userLoginName>";
         uData +="<userLoginPwd>"+userPwd+"</userLoginPwd>";
         uData +="<defaultType>"+defaultType+"</defaultType>";
         uData +="</user>";
      });
         uData +="</userData>";
         return uData;
    }
 
     var radioCheck ="";
     function radioChecked(Obj){
         var idObj = $(Obj).attr('id');
         var id = idObj.split('_');
         var d = $('#userDetailpopUp').find('.rmessage').attr('id');
     
         radioCheck = id[1];
     
         if(id[1] == "9"){
             //	$("div#userProfileOptionsData").hide();
             //	$("input#userProfilePwdCancel").attr("onclick","closeNots();");
                 //$("input#userProfilePwdSave").attr("onclick","saveUserNotifications();");
                 //$("input#userProfilePwdSave").val("Add");
                 
             
             }
     }	   
 
      function loadUserNotification(){
          var label_name ="";
          var label_id = "";
          var label_value = "";
          $("#loadingBar").show();
          var userRegType = "${userRegType}";
          timerControl("start");
              $.ajax({
                 url:"${path}/postLoginAction.do",
                 type:"POST",
                 data:{act:"loadUserNotification"},
                 error: function(jqXHR, textStatus, errorThrown) {
                         checkError(jqXHR,textStatus,errorThrown);
                         $("#loadingBar").hide();
                         timerControl("");
                         },
                 success:function(result){
                     checkSessionTimeOut(result);
                 // alert(result);	
                  var res = result.split("##@##");
                  $("div#notificationType").show();
                  var custLabels = '${companyLabels}';
                  var notiTypeKeys = "Notif_Workspace_ActivityFeed,Notif_Workspace_FolderShare,Notif_Workspace_FileShare,Notif_Workspace_Task,Notif_Workspace_DocumentTask,Notif_Idea_Comments,Notif_Idea_Share,Notif_Idea_Documents,"+
                                     "Notif_Idea_Tasks,Notif_Calendar_Tasks,Notif_Calendar_WorkflowTasks,Notif_Calendar_DocumentTasks,Notif_Calendar_WorkspaceDocTasks,Notif_Calendar_WorkspaceDocTasks,"+
                                     "Notif_Calendar_Events,Notif_Calendar_SprintTasks,Notif_Calendar_IdeaTasks,Notif_Agile_Tasks,Notif_Agile_Comments,Notif_Agile_Documents,Notif_Repository_Comments,"+
                                     "Notif_Repository_FolderShare,Notif_Repository_DocumentTask,Notif_Repository_DocumentShare,Notif_Connect_NewUsers,Notif_Connect_Messages,Notif_Myzone_NotesShare,Notif_Myzone_BlogShare,"+
                                     "Notif_Myzone_GalleryShare,Notif_Myzone_GalleryComments,Notif_ProjectAdmin_Approval";	
                  
              $(res[0]).find("labelData").each(function(){
                       label_id=$(this).find("notifi_id").text();
                       label_name=$(this).find("notifi_name").text();
                       $('#'+label_name+'_NotiCheckVal').val(label_id);
                       $('#'+label_name+'_NotiCheckId').val(label_id);
                  });
                 $(res[1]).find("notLabelData").each(function(){
                       var checkedId=$(this).find("notification_id").text();
                       $("input[value='" + checkedId + "']").prop('checked', true);
                  });
                  
                   if(userRegType.split("_")[1] == 'standard' ){
                      $("div#wrkSpaceProjAdmin").hide();
                      $("div#workFlowtemp").hide();
                  }else{
                      }
                  
                 var custLabelsData = '${companyLabels}';
                  var notiKeyList = notiTypeKeys.split(',');
                 var notiValue = "";
                 var notiKey = "";
                 for(var i=0;i<=notiKeyList.length-1;i++){
                       notiKey=notiKeyList[i];
                       notiValue=getValues(custLabelsData,notiKey);
                         $('#'+notiKey+'_NotiSubLabel').text(notiValue);
                  }
                  
                 var notHeaderKey="PROJECTS,IDEAS,CALENDAR,AGILE,REPOSITORY,MY_ZONE,CONNECT,PROJECT_ADMIN";
                  var notiHeaderKeyList = notHeaderKey.split(',');
                 var notiHeaderValue = "";
                 var notiHeaderKey = "";
                 for(var i=0;i<=notiHeaderKeyList.length-1;i++){
                       notiHeaderKey=notiHeaderKeyList[i];
                       notiHeaderValue=getValues(custLabelsData,notiHeaderKey);
                         $('#'+notiHeaderKey+'_NotiHeaderLabel').text(notiHeaderValue);
                  }

                 $('#userDetailpopUp').find('#changeDynamic2').attr('onclick','').attr('onclick','saveUserNotifications();').text('Add');

                  $("#loadingBar").hide();
                   timerControl("");
                   }
             });
          }	
 
           var xmlData= "";
         var flagVal = "false";
  function saveUserNotifications(){
         var value ="";
         var id  = "";
         var status = "unChecked";
         xmlData='<xmlData>';
         $('div.notSubContent').each(function() {
             $(this).find('input[type=checkbox]:checked').each(function(i){
                id  = $(this).val();
                status = "checked";
                value = $(this).parent().next("div.notContent").text();
                xmlData+='<labelData>';
                xmlData+='<notifi_id>'+id+'</notifi_id>';
                xmlData+='<notifi_value>'+value+'</notifi_value>';
                xmlData+='<notifi_status>'+status+'</notifi_status>';
                xmlData+='</labelData>';
                flagVal = "true";
                return true;
              });
             });
         xmlData+='</xmlData>';
         if(xmlData == '<xmlData></xmlData>'){
             flagVal ="true";
         }
         $("input#userProfilePwdSave").val(getValues(companyAlerts,"Save"));
          $("div#userProfileOptionsData").show();
          $("div#notificationType").hide();
          
           $('#userDetailpopUp').find('#usersDetailsDiv1, #usersDetailsDiv3').hide();
          $('#userDetailpopUp').find('#usersDetailsDiv2').show();
          
          $('#userDetailpopUp').find('#changeDynamic2').attr('onclick','').attr('onclick','saveUserPwdData()').html('save');
          
      //	$("input#userProfilePwdCancel").attr("onclick","closeUserProfile();");
         //$("input#userProfilePwdSave").attr("onclick","saveUserPwdData()");
      } 	 
 
     var CheckedVal = "";
     var checkedId ="";
     var chkId="";
     var val="";
     var chkcustom="";
 function uncheckSelIds(obj,id){
     var defaultDrive = $(obj).attr('defaultType');
     if(defaultDrive=='N'){
        var defaultChechked = $('img[id^=customRepository_]').hasClass('defaultCheck');
        if(defaultChechked){
           alertFun(getValues(companyAlerts,"Alert_ContactSysAdmin"),'warning');
           return false;
        }
      }
     
        if($(obj).attr('class') == 'Check'){
                  $(obj).removeClass("Check").addClass("UnCheck");
                  $(obj).attr("value", " ");
                     $(obj).attr("value", "N");
                  $(obj).attr('src','images/selcet.png')
                  var newIds="";
                  var idArray=checkedId.split(',');
                  for(var i=0;i<idArray.length-1;i++){
                     if(idArray[i]!=id){
                              newIds=newIds+idArray[i]+',';
                         }
                        }
                        checkedId=newIds;
                        
         }else{  
             $(obj).removeClass("UnCheck").addClass("Check");
             $(obj).attr("value", "Y");
             $(obj).attr('src','images/selcettwo.png');
             checkedId += id+",";
         }
         CheckedVal =checkedId;
 
     }

     function showUserWelcomepopup(){
          //put the condition
          if(traillogin == 'Y'){
               $('#transparentDiv').show();
               $("div#loginUserName").html("Welcome "+userFirstName);
               $(".hopscotch-bubble animated hopscotch-callout no-number").show();
              $('#welcome-Data').show();
              getUserLogInName(userId);
              xmppRegister(userId);
          }else{
           $("div#transparentDiv").css('display','block');
           $('#transparentDiv').hide();
           $('#welcome-Data').hide();
          }
     }
     
     function changePwd(){
         var prefered_userId = $("input#userId").val().trim();
         var pwd = $("input#newPwd").val().trim();
         var confirmPwd = $("input#confirmPwd").val().trim();
         var characterReg = /^\s*[a-zA-Z0-9,_,.,@,\s]+\s*$/;
         var fnlen= $.trim(prefered_userId).length;
         var emailId = $("input#confirmEmail").val().trim();
         $("input#updatedUserLogInName").val(prefered_userId);
         
         if(profileImgProfileFlag){
             uploadToRepo(fileExtension); // to upload image in repository
              uploadProfileImgBlob(fileName);
         }else{
              fileName ="";
              fileExtension="0";
          }	   
              
         if(prefered_userId == ''){
             alertFun(getValues(companyAlerts,"Alert_NmeNotEmpty"),'warning');
             return false;
         }
         
         if(fnlen==0){
             alertFun(getValues(companyAlerts,"Alert_NoSpace"),'warning');
             $("input#userId").focus();
             return false;
         } 
         
         /*if(!characterReg.test(prefered_userId)) {
                 alertFun("Special characters are not allowed.",'warning');
                 $("input#userId").focus();
                 return false;
         }*/
         
         if(pwd == '' || confirmPwd == ''){
             alertFun(getValues(companyAlerts,"Alert_EntPass"),'warning');
             return false;
         }
         
         if( pwd != confirmPwd ){
             alertFun(getValues(companyAlerts,"Alert_PassMismatch"),'warning');
             return false;
         }
         
          if(emailId=="" || emailId==null){
                 alert(getValues(companyAlerts,"Alert_EmailEmpty!"));
                 return false;
          }
            
          if(!validateEmailJs(emailId,true,true) ) {
                 return false;
          } 
         
         $('#loadingBar').show();
         timerControl("start");
         confirmPwd=confirmPwd.replaceAll("#","CH(21)").replaceAll("~","CH(22)").replaceAll("%","CH(23)").replaceAll("&","CH(24)").replaceAll("+","CH(25)");
         $.ajax({
                url: "${path}/register.do?act=updateUserDetails&emailId="+emailId+"&prefered_userId="+prefered_userId+"&confirmPwd="+confirmPwd+"&userId="+userId+"&fileName="+fileName+"&fileExtension="+fileExtension,
             type:"POST",
             error: function(jqXHR, textStatus, errorThrown) {
                     checkError(jqXHR,textStatus,errorThrown);
                     $("#loadingBar").hide();
                     timerControl("");
                     },
             success:function(result){
                 checkSessionTimeOut(result);
                 if(result == "success"){
                     $("#loadingBar").hide();
                      timerControl("");
                     alertFun(getValues(companyAlerts,"Alert_DetailUpdateSuccess"),'warning');
                     $("input#userId").val("");
                     $("div#welcome-Data").hide();
                     activationLinkUpdate();
                     //inviteUsersPopup();
                     $("div#transparentDiv").css('display','block');
                 }
                 if(result == "failure"){
                     $("#loadingBar").hide();
                      timerControl("");
                     alertFun(getValues(companyAlerts,"Alert_UsrSameId"),'warning');
                     ///$("input#userId").val("");
                     ///$("input#newPwd").val("");
                     ///$("input#confirmPwd").val("");
                 }
             }
         });
     }
    
     function addMoreEmail(){
         /*var emailUI = " <div style=\"padding-left: 0px ! important; float: left; width: 100%; margin-bottom: 5px;\" class=\"form-group\"> "
                     + " <div style=\"float: left;\"> <img style=\"margin-top: 10px;\" src=\"images/website/userinvite.png\"></div> "
                     + " <div style=\"float: left; margin: 6px; width: 90%;\"><input type=\"text\" placeholder=\"Teammate's email\" id=\"user_1\" class=\"form-control\"></div> "  
                     + " </div> " ;
         $("div#emailDiv").append(emailUI);*/
     }
     
    function inviteNewUsers(){
            $("div#transparentDiv").show();
             var emailDetails="";
             var userIds="";
             var characterReg = /^\s*[a-zA-Z0-9,_,.,@,\s]+\s*$/;
             $(".getEmail").each(function(){
                     if($(this).val().trim()!=""){
                         if(!validateEmailJs($(this).val(),true,true)) {
                              alertFun(getValues(companyAlerts,"Alert_InvalidEmail"),'warning');
                              return false;
                              }
                              else {
                               emailDetails = emailDetails + $(this).val().trim()+",";
                              } 
                     }
             });
                 
             $(".getUserId").each(function(){
                     if($(this).val().trim()!=""){
                         if(!characterReg.test($(this).val().trim())) {
                              alertFun(getValues(companyAlerts,"Alert_NoSplChar"),'warning');
                         }
                         else{ userIds = userIds + $(this).val().trim()+","; }
                     }
             });
             
             emailDetails = emailDetails.substring(0, emailDetails.length - 1);
             userIds = userIds.substring(0, userIds.length - 1);
         
             if(emailDetails == ""){
                  alertFun(getValues(companyAlerts,"Alert_EmailFldEmpty"),'warning');
                 return false;
             }
             
             if(userIds == ""){
                 alertFun(getValues(companyAlerts,"Alert_IdFldNoEmpty"),'warning');
                 return false;
             }
             
             $('#loadingBar').show();
             timerControl("start");
             
                 $.ajax({
                     url: "${path}/register.do?act=inviteNewUsers&emailDetails="+emailDetails+"&userIds="+userIds,
                                     type:"POST",
                                     error: function(jqXHR, textStatus, errorThrown) {
                                             checkError(jqXHR,textStatus,errorThrown);
                                             $("#loadingBar").hide();
                                             timerControl("");
                                             },
                                     success:function(result){
                                         checkSessionTimeOut(result);
                                        result = result.split("!@#$$#@!@#$");
                                         if(result[0]== "success" && result[1] == ""){
                                             $("div#welcome-Data").hide();
                                             $("div#transparentDiv").hide();
                                             alertFun(getValues(companyAlerts,"Alert_AllInvited"),'warning');
                                             $("#loadingBar").hide();
                                             timerControl("");
                                         }
                                         if(result[0]!="success" && result[1]!="") {
                                         alertFun(getValues(companyAlerts,"Alert_AlreadyReg"),'warning');
                                           var temp2 = result[1].split(",");
                                           for(var j=0;j<temp2.length-1;j++){
                                               $(".getUserId").each(function(){
                                                 if($(this).val()==temp2[j])
                                                   $(this).css('border','1px solid red');
                                               });
                                            }
                                         $("#loadingBar").hide();
                                         timerControl("");
                                         }
                                 }
                 })
                 emailDetails=""; //cleaer the contents of global variables.
                 userIds ="";
   }
   
   function closeInitialPopUp(){
       $("div#welcome-Data").hide();
       $("div#transparentDiv").hide();
   }
   
   function getUserLogInName(userId){
       $.ajax({
                 url: "${path}/register.do?act=getUserLogInName&userId="+userId,
                 type:"POST",
                 error: function(jqXHR, textStatus, errorThrown) {
                         checkError(jqXHR,textStatus,errorThrown);
                         $("#loadingBar").hide();
                         timerControl("");
                         },
                 success:function(result){
                     checkSessionTimeOut(result);
                     var res = result.split("*&*&*");
                     $("input#userId").val(res[1]);
                     var data = "Welcome " +res[0];
                     $("div#loginUserName").html(data)
                 }
     })
   }
   
     
   function activationLinkUpdate(){
       $("div#welcome-Data").show();
     $("div#viola-div").hide();
     $("div#ch-pwd-div").hide();
     $("div#transparentDiv").show();
     $("div#cong-user-div").show();
      var welcome = "Your log in User Id is " +$("input#updatedUserLogInName").val();
      $("span#introText").html(welcome);
      if(userRoleId == "2"){
          $("span#inviteNewUsersagain").show();
      }
      else{
          var temp = "Welcome user, Start using colabus and realize how easy it is to work together using Colabus."
          $("span#inviteNewUsersagain").html(temp);
      }
   }
   
   function goBackToPrevScreen(){
       $("div#viola-div").hide();
       $("div#cong-user-div").hide();
       $("div#welcome-Data").show();
       $("div#ch-pwd-div").show();
       $("input#userId").val($("input#updatedUserLogInName").val());
       
   }
   
   function inviteTeam(){
       $("div#welcome-Data").hide();
       addCompanyContact('company','create','welcome-screen');
   }
   
   function closeCongratsDiv(){
       $('#transparentDiv').hide();
       $("div#welcome-Data").hide();
   }
   

   function checkForEnterKey(event){
       if(event.keyCode == '13' ){
         changePwd();
     }
   }
   
  
  
 

function loadNewAccountLabel(place){
 //console.log("plc---"+place);
   var keyList="";
   if(place=='postloginonload'){
    keyList="My Profile,New inZone,Notification History,Log Out,Help,landingPage Search,Chat,Profile,Options,first name,last name,user role,Company,Job Title,Department,Mobile,Work Phone,Home Phone,Email,Office Address,Home Address,Expertise,Biography,Clear,Home,"+
             "Type Old Password,Main Menu,Name,Hash Code,Description,PRIVACY,Invite members,Hash create,WS P Icon,WS add,Email,Department,Work Phone,Mobile,Home Phone,Office Address,Home Address,Expertise,Biography,Say Cassandra,Name or Email,SAVE,Close,Workspace,Subscribe,"+
             "Invite collegue,Back,Invite team,Congratulations,Account activate,ADD PHOTO,Unique Userid,User Email,user email,Confirm Password,New Password,Prefer Userid,user id,WF Next,Change password,Kickstart,Welcome,name,size,owner,created,Shared by,modifier,modified,Search and sort,myzone,Switch drive,Create folder,Delete,Add Participants,Upload From System,Upload from gallery,Save,write,read,Cover,Layout,Sort,Created by,Search,Clear,Read,Write,Thank,"+
            "db Weeks Progress,Agile activites This week,Story Status This week,Task Status This week,Sprint Groups This week,Upload CSV file,Download sample CSV file,Type,Team Zone,Upload from system,Upload from gallery,My Zone,Enterprise Zone,Start New inZone,Existing inZone,Contacts,Tasks,DOCUMENTS,Messages,Notes,Blogs,Gallery,Analytics,System Admin,Integration,All Scrums,scrumboards,documents,messages,notes,blog,gallery,tasks,mytask,assignedtask,history,myzone,c_off,c_on,Repo_Rename,"+
            "Feature Added,Document Added,Epic Added,Story Added,db Document Added,Task Added,Enter Sprint Group,Search,Conversations,Tasks,Documents,Team,Email,Ideas,Agile Definition,Agile Sprint";	
    }else if(place=='postloginOptions'){
        keyList="Type Old Password,Change password,Type New Password,Re Type New Password,Notification,Drives,Disable Cassandra Notifications,Get each message,No Email Notifications,Important Notifications,Daily digest,Confluence Domain Name,User name,password";
    }else if (place == 'conversationonload') {
        keyList = "Upload from repository,Upload from system,Attach Link,Team Zone,My Zone,Enterprise Zone,Start New inZone,Existing inZone,Contacts,Tasks,DOCUMENTS,Messages,Notes,Blogs,Gallery,Analytics,System Admin,Integration,All Scrums,Workspace,Email,Documents,Conversations,Team,Ideas,Agile,Agile Definition,Agile Sprint,Team Zone,Latest Update,Oldest Update,Latest Conversation Thread,Oldest Conversation Thread,Search and sort";
    } else if (place == "conversationactFeed") {
        keyList = "Trend,Reply,Edit,Task,Delete,Posted by,on,Post,Close,Clear,Options,Task Id,Type,Priority,Assigned to,Created by,Start_date,End_date,Status,Created On,Modified On,Rec audio,Camera,Upload,Download,Upload from repository,Upload from system,Attach Link,";
    }else if(place=='landingpageonload'){
        keyList="Team Zone,My Zone,Enterprise Zone,Start New inZone,Existing inZone,Get Team Work Done,Get My Work Done,Contacts,Recent,Tasks,DOCUMENTS,Messages,Notes,Blogs,Gallery,Analytics,System Admin,Get My,Integration,Notifications,All Scrums,Howdy";	
    }
   //console.log("companyLabels------------>"+companyLabels);
     var labelKeyList=keyList.split(',');
     var labelValue="";
     var labelKey="";
     for(var i=0;i<=labelKeyList.length-1;i++){
         labelKey=labelKeyList[i].replaceAll(' ','_');
         labelKey=labelKey.replaceAll('/','_');
         labelKey=labelKey.replaceAll('-','_');
         labelValue=getValues(companyLabels,labelKey);
         
         $('.'+labelKey+'_cLabelTitle').attr('title',labelValue);
         $('.'+labelKey+'_cLabelText').text(labelValue);
         $('.'+labelKey+'_cLabelLink').text(labelValue);
         $('.'+labelKey+'_cLabelPlaceholder').attr('placeholder',labelValue);
         $('.'+labelKey+'_cLabelHtml').html(labelValue);
         $('.'+labelKey+'_cLabelValue').val(labelValue);
 
 }

}


//<-----------chat notifications--------->

function chatOffLineNotification(){
var localTZ=getTimeOffset(new Date());
setInterval(function(){
   if(sessionStorage.name != "colabusChat"){ 
       /* $.ajax({
          url:"${path}/ChatAuth",
          type:"post",
          data:{act:"getGroupMessagesForDeskNotification",userId:"${userId}",searchKey:"",searchStr:"",localTZ:localTZ},
          dataType:"text",
          error: function(jqXHR, textStatus, errorThrown) {
                     checkError(jqXHR,textStatus,errorThrown);
                     },
          success:function(result){
               var jsonChat = result.split("##")[0];
              var jsonGroup = result.split("##")[1];
              var jsonCount = result.split("##")[2];
              var total=0;
              var toatlMsg;
                 var data=jsonCount.split("##@@##");
                 var jsonDataConversation=jQuery.parseJSON(data[0]);
              for(var j=0; j<jsonDataConversation.length; j++){
                 total += parseInt(jsonDataConversation[j].unreadMsgCount, 10);   	
              }
              toatlMsg= total;   	
              document.getElementById("count1").innerHTML=toatlMsg; 
              if(toatlMsg==0){
                 $("#count1").hide();
              } else{
                 $("#count1").show();
                 if(toatlMsg>99){
                      document.getElementById("count1").innerHTML="99+";
                 }else{
                     document.getElementById("count1").innerHTML=toatlMsg; 
                 }
              }
              
              if(jsonChat !== "[]"){
              var jsonDataChat=eval(jQuery.parseJSON(jsonChat));
                  if(jsonDataChat.length > 0){
                          var senderId;
                          var senderMsg;
                          var imgType;
                          var imgUrl ='';
                          var test;
                         for(var i =0; i<=jsonDataChat.length; i++){ 
                              senderId = jsonDataChat[i].senderId;
                              senderMsg = jsonDataChat[i].senderMsg;
                              senderMsg = replaceSpecialCharacterForTitle(senderMsg);
                              imgType=jsonDataChat[i].imgType;
                              if(senderId != userId && sessionStorage.name != "colabusChat"){
                                   imgUrl = lighttpdPath+"/userimages/"+senderId+"."+imgType ;
                                   test = ChangeImage(imgUrl,senderMsg);				     
                                  playSound('glass');
                              }
                         }   
                   }
                }
                 
                 if(jsonGroup !== "[]"){
                  var jsonDataGroup=eval(jQuery.parseJSON(jsonGroup));
                     if(jsonDataGroup.length > 0){
                        var senderId;
                        var senderMsg;
                        var imgType;
                        var imgUrl ='';
                        var test;
                        for(var k =0;k<=jsonDataGroup.length; k++){
                                   senderId = jsonDataGroup[k].senderId;
                                 senderMsg = jsonDataGroup[k].senderMsg;
                                 senderMsg = replaceSpecialCharacterForTitle(senderMsg);
                                 imgType=jsonDataGroup[k].imgType;
                                 if(senderId != userId &&  sessionStorage.name != "colabusChat"){
                                      imgUrl = lighttpdPath+"/userimages/"+senderId+"."+imgType ;
                                      test = ChangeImage(imgUrl,senderMsg);			 	
                                      playSound('glass');
                                 }
                        }
                      }
               }
         }
     }); */
    }   
    if(sessionStorage.name != "colabusChat"){
            getVideoOffmsg();
    }
 },30000);
}
       
function chatunreadMessages(){
   $.ajax({
          url:"${path}/ChatAuth",
          type:"post",
          data:{act:"getUnReadMsgCount",userId:"${userId}"},
          dataType:"text",
          error: function(jqXHR, textStatus, errorThrown) {
                     checkError(jqXHR,textStatus,errorThrown);
                     $("#loadingBar").hide();
                     timerControl("");
                     },
          success:function(result){
              checkSessionTimeOut(result);
              if(result >0){
                 if(result > 99)
                     $('#chatNot').show().text('99+');
                 else
                     $('#chatNot').show().text(result);	
             }else{
                 $('#chatNot').hide();
             }
          }
        }); 
}

 //Enter an API key from the Google API Console:
   //   https://console.developers.google.com/apis/credentials?project=_
   var apiKey = '565436098961-3e7ap83f3lsgh5qjk4jsfcol0s66r647.apps.googleusercontent.com';
   // Enter a client ID for a web application from the Google API Console:
   //   https://console.developers.google.com/apis/credentials?project=_
   // In your API Console project, add a JavaScript origin that corresponds
   //   to the domain where you will be running the script.
   var clientId = '565436098961-3e7ap83f3lsgh5qjk4jsfcol0s66r647.apps.googleusercontent.com';
   // Enter one or more authorization scopes. Refer to the documentation for
   // the API or https://developers.google.com/identity/protocols/googlescopes
   // for details.
   var scopes = 'profile';
   var auth2; // The Sign-In object.
   var authorizeButton = document.getElementById('authorize-button');
   var signoutButton = document.getElementById('signout-button');
   
   function handleClientLoad() {
     // Load the API client and auth library
     gapi.load('client:auth2', initAuth);
   }
   
   function initAuth() {
     gapi.client.setApiKey(apiKey);
     gapi.auth2.init({
         client_id: clientId,
         scope: scopes
     }).then(function () {
       auth2 = gapi.auth2.getAuthInstance();
       // Listen for sign-in state changes.
       auth2.isSignedIn.listen(updateSigninStatus);
       // Handle the initial sign-in state.
       updateSigninStatus(auth2.isSignedIn.get());
       authorizeButton.onclick = handleAuthClick;
       signoutButton.onclick = handleSignoutClick;
     });
   }
   
   function updateSigninStatus(isSignedIn) {
     if (isSignedIn) {
       /*authorizeButton.style.display = 'none';
       signoutButton.style.display = 'block';*/
       makeApiCall();
     } else {
       /*authorizeButton.style.display = 'block';
       signoutButton.style.display = 'none';*/
     }
   }
   
   function handleAuthClick(event) {
     auth2.signIn();
   }
   
   function handleSignoutClick(event) {
      $("#logoutframe").show();
      if(typeof auth2 !=='undefined')
        auth2.signOut(); 	 
   }
   
   // Load the API and make an API call.  Display the results on the screen.
   function makeApiCall() {
     console.log(auth2.currentUser.get().getBasicProfile().getGivenName());
 }
 
 $(document).ready(function(){ 
 // connect();
 if((sessionStorage.name == undefined || sessionStorage.name == "") && companyPlan != 'web_Social_92' && sessionStorage.name != "colabusChat"){
 //openChatWindow(); 
 //connect();
 }
 });
  function ChangeImage(imgUrl,senderMsg) {
                         
                         var image= new Image();
                         image.onload = function() {
                           notifyMe(imgUrl, senderMsg); 
                         };
                         image.onerror = function() {
                           notifyMe(lighttpdPath+"/userimages/userImage.png", senderMsg); 
                         };
                         image.src =imgUrl;
                     }
 
         
 function getVideoOffmsg(){
     var callerId;
     var callerName;
     var callerImgType;
     var imgUrl ='';
     var msg;
     /* $.ajax({
                  url: path+"/ChatAuth",
                  type:"post",
                  data:{act:"getVideoOffmsg",toUserId:userId},
                  mimeType: "textPlain",
                  success:function(result){
                  var data = jQuery.parseJSON(result);
                         if(data.length >0){
                             for(var i =0; i<data.length; i++){
                                 callerId = data[i].callerId;
                                 callerName= data[i].callerName;
                                 callerImgType = data[i].callerImgType;
                                 imgUrl = lighttpdPath+"/userimages/"+callerId+"."+callerImgType ;
                                 //imgUrl = lighttpdPath+"/userimages/16.jpg" ;
                                 msg = "Missed Call From "+callerName;
                                 var test = ChangeImage(imgUrl,msg);
                                 playSound('glass');
                             }
                             localStorage.setItem("chatFlashStatusFlag","true");
                             setChatFlash();
                         }
                  } 
             
             }); */
         
}
//$(window).resize(resizeChatWindow);
var chatFlashStatus=null;			
function setChatFlash(){
     if(chatFlashStatus!=null){
        resetChatFlash();
     }   
    chatFlashStatus = setInterval(function(){
                          if($('#chatIcon').attr('src').indexOf('chat.png')!= -1){
                             $('#chatIcon').attr('src','images/chatFlash.png');
                             document.title="Missed call"
                             document.getElementById('favicon').href = "images/chatFlash.png";
                          }else{
                             $('#chatIcon').attr('src','images/chat.png');
                             document.title="Colabus"
                             document.getElementById('favicon').href = "images/favicon.ico";
                          }
                     },2000);
}	

function resetChatFlash(){
       clearInterval(chatFlashStatus);
       chatFlashStatus = null;
       $('#chatIcon').attr('src',path+'/images/chat.png');
       localStorage.setItem("chatFlashStatusFlag","false");
       document.title="Colabus"
       document.getElementById('favicon').href = path+"/images/favicon.ico";
}

var callFlashStatus=null;			
function setCallFlash(){
      if(callFlashStatus!=null){
         clearCallFlash();
      }
      callFlashStatus = setInterval(function(){
                      if($('#chatIcon').attr('src').indexOf('chat.png')!= -1 ){
                         $('#chatIcon').attr('src',path+'/images/chatFlash2.png');
                         document.title="Incoming call"
                         document.getElementById('favicon').href = path+"/images/chatFlash2.png";
                       }else{
                         $('#chatIcon').attr('src',path+'/images/chat.png');
                         document.title="Colabus"
                           document.getElementById('favicon').href = path+"/images/favicon.ico";
                      }
                 },2000);
}
function clearCallFlash(){
   $('#chatIcon').attr('src',path+'/images/chat.png');
   clearInterval(callFlashStatus);
   callFlashStatus = null;
   document.title="Colabus"
   document.getElementById('favicon').href = path+"/images/favicon.ico";
}	

                         
function closePreviewDiv(){
  document.getElementById('recAudio').pause();
  document.getElementById('recVideo').pause();
  $("#previewDiv").hide().children('#recVideo').hide().parent().children('#recAudio').hide();
  if(globalmenu=='Idea'){
		$("#transparentDiv").show();
	}
}

function getCount(type){
   var totalMsg = $("#chatNotMain").find("#count1").html();
   console.log("totalMsg befor in---"+totalMsg);
   if(type !="visible"){
       totalMsg++;  
   }
  
   console.log("totalMsg after in---"+totalMsg);
   $("#chatNotMain").find("#count1").html(totalMsg).show();
   console.log("---->"+$("#count1").length)
   
   return totalMsg;
}
function changeImg(type){
   if(type=="online"){
       $("#chatIm").attr('src',path+'/images/video_old/callgreen.svg');
   }
   else{
       $("#chatIm").attr('src',path+'/images/video_old/callred.svg');
   }
}
setInterval(function() {
 

   if(sessionStorage.name != "colabusChat"){
       changeImg('offline')
    }
   else{
       changeImg('online')
   }
  }, 500);

  function landingpageui(){
    var ui="";
    $('#content').html('');
    ///alert("ooo");
    ui="<section  class=\" w-100 h-100\">"
      +"<div class=\"container-fluid  p-0\">"
        +"<div class=\"row m-0\">"
        /// Menu icon div Starts
          +"<div class=\"col-12\" style=\"display:contents;\">"
            +"<div class=\"col-lg-8 col-md-8 col-sm-12 col-xs-12 px-2\" style=\"height:561px;background-color: #2B2C4B;\">"


                +"<h1 class=\"mt-4\" style=\"color:#fff;\">What you like to do?</h1>"
                +"<div class=\"card-deck mt-4\">"
                  +"<div class=\"card \" style='min-height:112px; margin-bottom:30px !important;'>"
                  +"  <div onclick=\"createNewWorkspace();\" style=\"cursor:pointer;\" class=\"card-body text-left d-flex justify-content-start align-items-center p-2\">"
                  +"    <div style='width:90px; height:90px;' class=\"d-flex align-items-center justify-content-center ml-2\">"
                  +"        <img class=\"img-fluid\" src=\"images/landingpage/start.svg\" style=\"height:80px; width:80px;\">"
                  +"    </div>"
                  +"    <p class=\"card-text ml-4 mb-0\">Start New Workspace</p>"
                  +"  </div>"
                  +"</div>"
                  +"<div class=\"card\" style='min-height:112px;margin-bottom:30px !important;''>"
                  +"   <div class=\"card-body text-left d-flex justify-content-start align-items-center p-2\" onclick=\"switchZone('exsitingWorkSpace');\" style='cursor: pointer;'>"
                  +"    <div style='width:90px; height:90px;' class=\"d-flex align-items-center justify-content-center ml-2\">"
                  +"        <img class=\"img-fluid\" src=\"images/landingpage/work.svg\" style=\"height:80px; width:80px;\">"
                  +"    </div>"
                  +"    <p class=\"card-text ml-4 mb-0\">Work on Existing Workspace</p>"
                  +"   </div>"  
                  +"</div>"
                +"</div>"
                +"<div class=\"card-deck\">"
                  +"<div class=\"card \" style='min-height:112px;margin-bottom:30px !important;''>"
                  +"  <div class=\"card-body text-left d-flex justify-content-start align-items-center p-2\">"
                  +"    <div style='width:90px; height:90px;' class=\"d-flex align-items-center justify-content-center ml-2\">"
                  +"        <img class=\"img-fluid\" src=\"images/landingpage/recent.svg\" style=\"height:80px; width:80px;\">"
                  +"    </div>"
                  +"    <div class=\"ml-3 \">"
                  +"        <p class=\"card-text mb-1 mx-1 mt-2\">Recently visited Workspace</p>"
                  +"        <div id=\"recentWorkspaceListingDiv\" class=\"d-flex my-1\">"
                  +"        </div>"
                  +"    </div>"
                  +"  </div>"
                  +"</div>"
                  +" <div class=\"card \" style='min-height:112px;margin-bottom:30px !important;''>"
                  +"   <div class=\"card-body text-left d-flex justify-content-start align-items-center p-2\">"
                  +"    <div style='width:90px; height:90px;' class=\"d-flex align-items-center justify-content-center ml-2\">"
                  +"        <img class=\"img-fluid\" src=\"images/landingpage/my.svg\" style=\"height: 75px;width: 75px;\">"
                  +"    </div>"
                  +"    <p class=\"card-text ml-4 mb-0\">My stuff</p>"
                  +"   </div>"
                  +" </div>"
                +"</div>"
                +"<div class=\"card-deck\">"
                  +"<div class=\"card \" style='min-height:112px;margin-bottom:30px !important;''>"
                  +"  <div class=\"card-body text-left d-flex justify-content-start align-items-center p-2\">"
                  +"    <div style='width:90px; height:90px;' class=\"d-flex align-items-center justify-content-center ml-2\">"
                  +"        <img class=\"img-fluid\" src=\"images/landingpage/analytics.svg\" style=\"height: 75px;width: 75px;\">"
                  +"    </div>"
                  +"    <p class=\"card-text ml-4 mb-0\">Analytics</p>"
                  +"  </div>"
                  +"</div>"
                  +"<div class=\"card \" style='min-height:112px;margin-bottom:30px !important;''>"
                  +"   <div class=\"card-body text-left d-flex justify-content-start align-items-center p-2\" onclick=\"switchZone('contacts');\" style='cursor: pointer;'>"
                  +"    <div style='width:90px; height:90px;' class=\"d-flex align-items-center justify-content-center ml-2\">"
                  +"        <img class=\"img-fluid\" src=\"images/landingpage/phone.svg\" style=\"height: 75px;width: 75px;\">"
                  +"    </div>"
                  +"    <p class=\"card-text ml-4 mb-0\">Contacts</p>"
                  +"   </div>"
                  +"</div>"
                +"</div>"

            
            +"</div>"

          +"</div>"	///Notification content div

        +"</div>"	///row Ends 
      +"</div>"	/// container-fluid Ends
    +"</section>"
    
    
    $('#content').append(ui);
  }

  function exsitingWorkSpaceUi(){
    var ui="";
    $('#content').html('');

    ui="<section  class=\" w-100 h-100\">"
      +"<div class=\"container-fluid  p-0\">"
        +"<div class=\"row m-0\">"
          +"<div id=\"work\" style='margin-top: 20px;padding: 34px;'></div>"
          +"<div id=\"subscribe\" style='margin-top: 13px;padding-top: 40px;padding-left: 35px;'></div>"
        +"</div>"
      +"</div>"	
    +"</section>"

    $('#content').append(ui);

  }

function wsConversationPageUi(){
	var ui="";
	$('#content').html('');
	/*
	ui="<div style=\"font-family:OpenSansRegular !important;\">"
	+"<div class=\"eventPopup wsTaskMainDivCss createTaskPopUpMainCls\" id=\"createTaskPopup\" style = \"z-index: 6500;\"></div>"
	+"<div class=\"eventPopup wsTaskMainDivCss createTaskPopUpMainCls\" style = \"display: none;\" id=\"viewTaskUI\"></div>"
	// Task level Folder div 
	+"<div id=\"ideaTaskLevelDocPopUp\" class=\"taskLevelUpldPopup eventPopup wsTaskMainDivCss createTaskPopUpMainCls\" style=\"z-index: 9000;\">"
  		+"<div style=\"height: 520px; padding: 0px 15px;\">"
	 		+"<img src=\""+path+"/images/close.png\" class=\"Close_cLabelTitle\" style=\"float:right; cursor: pointer; margin-top: 5px;margin-right: -5px;\" onclick=\"cancelTaskLevelAttachDoc();\" />"
	 		+"<div id=\"docRepoDiv\" style=\" width: 100%; height: 48px;border-bottom:1px solid #BFBFBF;\">"
	   			+"<span style=\"cursor: pointer;margin-left:12px;margin-top:10px;float:left;\"	class=\"All_folders_cLabelText\" onclick=\"loadTaskLevelFolder();\"></span>"
	   			+"<span id=\"TaskLevelfolderNames\" style=\"float:left;margin-top:10px;\">  </span>"
	   			+"<img id=\"upImag\" class=\"Up_cLabelTitle\" src=\""+path+"/images/back.png\"   value=\"dsc\"  style=\" padding-left:0px !important; float:right;  cursor: pointer; margin-top:10px;margin-right:25px; \" onclick=\"loadTaskLevelFolder();\" />"
	 		+"</div>"
	  		+"<div class=\"tabContentHeader\" style=\"width: 98.4%;\">"
				+"<div style=\"float:left;width:8%;margin-left:10px;height:100%;float:left;\"></div>"
				+"<div class=\"hName\" style=\"overflow: hidden; width: 22%;float:left;\"></div>"
				+"<div class=\"hSize\" style=\"overflow: hidden; width: 9%;float:left;\"></div>"
				+"<div class=\"hOwner\" style=\"overflow: hidden; width: 16%;float:left;\"></div>"
				+"<div class=\"hCreated\" style=\"overflow: hidden; width: 12%;float:left;\"></div>"
				+"<div class=\"hLModified\" style=\"overflow: hidden; width: 17%;float:left;\"></div>"
				+"<div class=\"hModified\" style=\"overflow: hidden; width: 10%;float:left;\"></div>"
	  		+"</div>"		
	  		+"<div id=\"TaskLevelfolderDiv\" style=\"background-color:white; width: 100%;height:390px;overflow-y:auto;\"></div>"
	  		+"<div id=\"attachDocBtn\" style=\"width: 100%; height:30px;float: right;padding-top:10px;border-top:1px solid #BFBFBF;\">"
	    		//<!--  +"<input id=\"attachCancelBtnDiv"  class=\"text-uppercase createBtn" type=\"button" name=\"cancel" value=\"" onclick=\"cancelTaskLevelAttachDoc();\"  />"
	          	//+"<input id=\"TaskLevelattachSaveBtnDiv"  class=\"text-uppercase createBtn"  type=\"button" name=\"ok" value=\"" onclick=\"attachDocumentforTaskLevelIdea();\" style=\"display:none;\" />"    -->
	   			+"<div id=\"attachCancelBtnDiv\" style=\"display:none;\"  onclick=\"cancelTaskLevelAttachDoc();\" class=\"text-uppercase createBtn\" style=\"margin-right:3%;\">Cancel</div>"
	   			+"<div id=\"TaskLevelattachSaveBtnDiv\" style=\"display:none;\"  onclick=\"attachDocumentforTaskLevelIdea();\" class=\"text-uppercase createBtn\" style=\"margin-right:3%;\">ok</div>"
	    		+"<input type=\"hidden\" name=\"docIdeaId\" id=\"TaskLeveldocIdeaId\" value=\"\"/>"
	  		+"</div>"
  		+"</div>"	  
	+"</div>"
    +"<input type = \"hidden\" id = \"projIdTaskHidden\" value = \"\" />"   
 	+"<input type = \"hidden\" id = \"projNameTaskHidden\" value = \"\" />" 
 	
 	+"<div id=\"ideaDocPopUp\" style=\"padding:1%;border: 1px solid #B0B0B0;border-radius:0.4em !important; height:85%;display:none;position: absolute;  z-index: 7000;width:71%; top: 10%;left:50%;margin-left:-450px; background-color: #FFFFFF;box-shadow:0 0 5px 2px rgba(0, 0, 0, 0.35);overflow-y:auto;\">"
  		+"<div style=\"float: left;height: 90%;width: 100%;\">"
	 		+"<img src=\""+path+"/images/close.png\"  class=\"Close_cLabelTitle\" style=\"float: right; cursor: pointer; \" onclick=\"cancelAttachDoc();\" />"
	 		+"<div id=\"docRepoDiv\" style=\" width: 100%; height: 48px;border-bottom:1px solid #BFBFBF;\">"
	   			+"<span style=\"cursor: pointer;margin-left:12px;margin-top:10px;float:left;\"	class=\"All_folders_cLabelText\" onclick=\"loadFolder();\"></span>"
	   			+"<span id=\"folderNames\" style=\"float:left;margin-top:10px;\">  </span>"
	   			+"<img id=\"upImag\" class=\"Up_cLabelTitle\" src=\""+path+"/images/back.png\"  value=\"dsc\"  style=\" padding-left:0px !important; float:right;  cursor: pointer; margin-right:25px; \" onclick=\"loadFolder();\" />"
	 		+"</div>"
			+"<div class=\"tabContentHeader\" style=\"width: 98.4%;\">"
				+"<div style=\"float:left;width:8%;margin-left:10px;height:100%;float:left;\"></div>"
				+"<div class=\"hName\" style=\"overflow: hidden; width: 22%;float:left;\"></div>"
				+"<div class=\"hSize\" style=\"overflow: hidden; width: 9%;float:left;\"></div>"
				+"<div class=\"hOwner\" style=\"overflow: hidden; width: 16%;float:left;\"></div>"
				+"<div class=\"hCreated\" style=\"overflow: hidden; width: 12%;float:left;\"></div>"
				+"<div class=\"hLModified\" style=\"overflow: hidden; width: 17%;float:left;\"></div>"
				+"<div class=\"hModified\" style=\"overflow: hidden; width: 10%;float:left;\"></div>"
	  		+"</div>"	
	  		+"<div id=\"folderDiv\" style=\"background-color:white; width: 100%;height:390px;overflow-y:auto;\"></div>"
	  		+"<div id=\"attachDocBtn\" style=\"width: 100%; height:30px;float: right;padding-top:10px;border-top:1px solid #BFBFBF;\">"
   				// +"<input id=\"attachCancelBtnDiv"  class=\"ideaCancelBtn Cancel_cLabelValue" type=\"button" name=\"cancel" value=\"" onclick=\"cancelAttachDoc();\"  />"			
	    		//+"<input  class=\"ideaSaveBtn OK_cLabelValue" type=\"button" name=\"ok" value=\""  style=\"display:none;\" />"
	   			+"<div id=\"attachSaveBtnDiv\" style=\"display:none;\"  onclick=\"attachDocumentforIdea();\" class=\"text-uppercase createBtn\" style=\"margin-right:3%;\">ok</div>"
	    		+"<input type=\"hidden\" name=\"docIdeaId\" id=\"docIdeaId\" value=\"\"/>"
	  		+"</div>"
  		+"</div>"	  
	+"</div>"
	
+"<nav class=\"navbar contentClsTabDiv\" >"
	   +"<div class=\"container-fluid defaultContainerClsTab\" id=\"tabMainDiv\">"
	      +"<div class=\"navbar-header col-sm-12 col-xs-12 defaultCls\" style=\"margin-left: 0px;\">"
		      +"<div class=\"breadcrumMain\" >"
		         +"<div class=\"breadcrumContent\"><span style=\"float: left;margin-right: 8px;\"  loadType=\"wMenuProj\" class=\"Team_Zone_cLabelHtml text-uppercase\" onclick=\"loadModule(this);\"></span>"
		         +"<img id=\"projListDDImg\" src=\""+path+"/images/idea/sprintDetailExpand.png\" style=\"float: left;margin: 2px 8px 0 0px;width: 17px;\" class=\"img-responsive\" onclick=\"showProjectDDList();\">"
		         +"<img class=\"breadcrumImg img-responsive\" onerror=\"imageOnProjNotErrorReplace(this);\" src=\"\" onclick=\"loadProjectDescription();\"/>"
		         +"<span class=\"breadCrumProjName\"></span></div>"
		         +"<div id=\"projListDDContainer\">"
		         	+"<div id=\"projListDDContainerSub\" style=\"padding-bottom:4px;margin-top: 6px;margin-right: 5px;margin-bottom: 5px;margin-left: 5px;width: 98%;\"></div>"
                     +"<hr style=\"margin: -10px 0px 0px 6px;border-bottom: 3px solid #cccccc;\">"
                     +"<div id='projListDDContainerSub1' style=\"padding-bottom:4px;margin-top: 6px;margin-right: 5px;margin-bottom: 5px;margin-left: 5px;width: 98%;\"></div>"
                 +"</div>"
		      +"</div>"
			  +"<ul class=\"nav nav-tabs\">"
			    +"<li class=\"tabActive defaultTabs\" style = \"width: 11%;\" >"
			      +"<div onclick=\"loadActivityFeed('', 'firstClick')\" class=\"tabNameCls Conversations_cLabelHtml text-uppercase\" style=\"text-align: left; width: 85%\"></div>"
			      +"<img id=\"convoInfo\" class= \"infoCss\"  style = \"float: left;margin-top: 4px;display: none;\" src=\""+path+"/images/info.png\"/>"
			   +" </li>"
			    +"<li class=\"tabInactive defaultTabs\"   onclick=\"loadProjectTabData('loadTaskjsp')\">"
			        +"<div class=\"tabNameCls Tasks_cLabelHtml text-uppercase\"></div>"
			    +"</li>"
			    +"<li class=\"tabInactive defaultTabs accToRegType\" style = \"visibility: hidden\"  onclick=\"loadProjectTabData('loadDocumentjsp')\">"
			       +"<div class=\"tabNameCls Documents_cLabelHtml text-uppercase\"></div>"
                +"</li>"
                +"<li class=\"tabInactive defaultTabs\"   onclick=\"loadProjectTabData('loadTeamjsp'')\">"
			       +"<div class=\"tabNameCls Team_cLabelHtml text-uppercase\"></div>"
			    +"</li>"
			    +"<li class=\"tabInactive defaultTabs accToRegType\" style = \"visibility: hidden\"   onclick=\"loadProjectTabData('loadEmailjsp')\">"
			      +"<div class=\"tabNameCls Email_cLabelHtml text-uppercase\"></div>"
			    +"</li>"
			    +"<li class=\"tabInactive defaultTabs accToRegType colPlanBC\" style = \"visibility: hidden\"   onclick=\"loadProjectTabData('iMenu')\">"
			      +"<div class=\"tabNameCls Ideas_cLabelHtml text-uppercase\"></div>"
			    +"</li>"
			    +"<li class=\"tabInactive defaultTabs dropdown accToRegTypeAgile colPlanBC\" style = \"visibility: hidden\"  >"
			      +"<div class=\"tabNameCls  text-uppercase\" data-toggle=\"dropdown\" style=\"margin-right: 6px;\">"
			        +"<div class=\"Agile_cLabelHtml\" style=\"width: 60%;float: left;text-align: right;margin-right: 4px;\"></div>"
			      +"<img  class=\"img-responsive\" src=\""+path+"/images/downarrow.png\" style=\"width: 10px;margin-top: 8px;\"/>"
			      +"</div>"
			      
			      +"<ul class=\"dropdown-menu dropdownULDiv\">"
			         +"<div id=\"agileDropdownDiv\" ><img src=\""+path+"/images/topArrow.png\" class=\"dropdownArrowImgDivCls\"></div>"
			        +" <li style=\"text-align: left;\" class=\"subtabInactive tabNameCls  subTabNameCls\" onclick=\"loadProjectTabData(\'storyMenu')\"><div class=\"Agile_Definition_cLabelHtml\" style=\"margin-bottom: 3px;margin-left: 8px;\">Definition</div></li>"
				     +" <li style=\"text-align: left;border-bottom: 0px solid #ccc;\" class=\"subtabInactive tabNameCls \" onclick=\"loadProjectTabData('sprintMenu')\"><div class=\"Agile_Sprint_cLabelHtml\" style=\"margin-top: 3px;margin-left: 8px;\">Sprint</div></li>"
   				 +"</ul>"
			    +"</li>"
			    +"<li class=\"pull-right wrkRightIcon\" id=\"SearchActivityFeed\" style=\"margin-top: 5px;\">"
                  +"<img src=\""+path+"/images/sort.png\"  id=\"SearchActivityFeed1\" onclick=\"showSearchNsort();\" class=\"wrkRightImgCls img-responsive Search_and_sort_cLabelTitle\"/>"
                 
                  +"<img src=\""+path+"/images/back.png\"  id=\"SearchActivityFeed2\" onclick=\"loadActivityFeed('', 'firstClick');\" style=\"display:none\" title =\"back\" class=\"wrkRightImgCls img-responsive\"/>"
        
                    +"<div class = \"searchSortPopUp\" style=\"margin-top:-42px;height: 67px;\" id = \"wrkSpaceSearchActivityFeedSearch\">"
		         	  +"<div id = \"WsarrowDiv\" class=\"workSpace_arrow_right\" style=\"margin-top:14px;margin-right:-16px;\"><img src=\""+path+"/images/arrow.png\"></img></div>"
		 	          +"<div class=\"sortoptcls\">"
					 		+"<select  id = \"sortInWs\" style = \"\" class = \"drpDwnSelected wrkSpaceSearchActivityFeedSearchNew\" onchange = \"sortActFeedDetails(this)\"  type=\"company\">"
				      		 	+"<option class=\"drpDwnOptions Sort_cLabelText Latest_Update_cLabelText\" value=\"lu\" id=\"sort\"></option>"
				       			+"<option class=\"drpDwnOptions Sort_Name_cLabelText Oldest_Update_cLabelText\" value=\"ou\" id=\"name\"></option>"
				       			+"<option class=\"drpDwnOptions Sort_cLabelText Latest_Conversation_Thread_cLabelText\" value=\"lc\" id=\"sort\"></option>"
				       			+"<option class=\"drpDwnOptions Sort_Name_cLabelText Oldest_Conversation_Thread_cLabelText\" value=\"oc\" id=\"name\"></option>"
					   			// +"<option class=\"drpDwnOptions Email_cLabelText" value=\"createdby" id=\"createdby">Created By</option>"
					   			+"</select>"
					   			+"<div class=\"imgsrchfltr\" align=\"center\" style=\"\">"
									+"<img src=\""+path+"/images/sort2.png\" style=\"height: 16px;\">"
								+"</div>"
		 	         +"</div>"
					+"<div class=\"serachMainDivCls\">"
				        +"<input onkeyup=\"filterConversationData()\" class = \"serachBoxInputBoxCommonCls wrkSpaceSearchActivityFeedSearchNew\"  id = \"searchBox\" name = \"searchBox\" placeholder = \"Search\" type=\"text\">"
				        
					    +"<div class=\"searchCloseIconMainDiv\">"  
		  					+"<img class=\"serachBoxClearIcon\"  id=\"ClearComSearch\" src=\""+path+"/images/xforsearch.png\">"
		  				+"</div>"
		  				+"<div class=\"searchIconMainDiv\">"
		  					+"<img src=\""+path+"/images/searchTwo.png\" onclick=\"searchWrkspaceActFeed();\" id=\"wsSearch\" class=\"notesSearchbox\">"
			           +" </div>"
		            +"</div>"
                +"</div>"
                +"</li>"
		      +"</ul>"
		  +"</div>"
	  +"</div>"
  +"</nav>"
  +"<div class=\"container-fluid contentCls\">"
     +"<div id=\"tabContentDiv\">"
	     +"<div class=\"row\" id = \"rowTab\" style=\"margin-left:0px;margin-right:0px;background:white;padding:30px 24px 0px 25px\">"
	         
		     +"<div class=\"col-sm-12 col-xs-12\" style=\"height:45px;padding-left:0px; padding-right:0px;\">"
		       ///+"<div  id=\"main_commentTextarea" contenteditable=\"true" class=\"main_commentTxtArea" ></div>"
		       +"<textarea  id=\"main_commentTextarea\" class=\"main_commentTxtArea\" onkeyup=\"enterKeyValidCommentActFeed(event,"+prjid+",'activityFeed','keyup');\" onkeypress=\"enterKeyValidCommentActFeed(event,"+prjid+",'activityFeed','keypress');\"></textarea>"//---------------
		      ///+"<textarea  id=\"main_commentTextarea" class=\"main_commentTxtArea" style= "overflow:auto;\" onkeypress=\"enterKeyValidCommentActFeed(event,${projId},'activityFeed');\"></textarea> '
				+"<input type=\"hidden\" id=\"menuTypeForComment\" value=\"activityfeed\">"
				+"<input type=\"hidden\" id=\"ProjIdForComment\" value=\""+prjid+"\">"//-------------------------------

              +"<div class=\"main_commentTxtArea\" id=\"audioContainer\" style=\"display:none;\">"
			     +"<div id=\"audioArea\" style=\"display:none;\">"
			        +"<div style=\"position: absolute; padding: 0.7%; margin-left: 28%;\">"
			            +"<img src=\"\" onclick=\"\" id=\"customPlayer\" style=\"cursor:pointer;\">"
			            	+"<div id=\"timeline\">"
	                    		+"<div id=\"playhead\"></div>"
		                    +"</div>"
			        +"</div>"
			        +"<audio  src=\"\" id=\"audio\" ontimeupdate=\"updateTime()\" onended=\"onVoiceEnded()\" style=\"width: 100%; height: 100%; padding: 0.7%;\"></audio>"
			         
			         +"<div style=\"padding: 1%; float: right; margin-right: 22%;\"><img src=\""+path+"/images/circle.png\" onclick=\"\" style=\"width: 20px;cursor:pointer;\" id=\"cancelVoice\"></div>"
			      +"</div>"
			      
			      +"<div class=\"timer\" style=\"float: right; display: block; height: 60%; margin-top: 1%; width: 18%;\">" 
			         +"<label style=\"color:red; float: left;\">Recording</label>"
			         +"<div style=\"font-size: 14px; float: left; padding-left: 11%;\" class=\"timerSpan\"></div>"
			      +"</div>" 
		     +"</div>"

		      +"<div  class=\"projUserListMain proTagNameId\">"
		        +"<ul class=\"projuserList\"></ul>"
		      +"</div>"
		      +"<div  class=\"activityfieldUserListMain activityfieldMainuserslist\">"
		        +"<ul class=\"activityfielduserList\"></ul>"
		      +"</div>"

		     +"<div id = \"convoPost\" align=\"center\" class=\"main_commentTxtArea_btnDiv\" >"
		        +"<div style=\"width: 42%;float: left;display:none;\" onclick=\"clearMainFeedData()\"><img class=\"img-responsive\" src=\""+path+"/images/workspace/remove.png\" style=\"padding-left:8px;margin-top: 10px;margin-bottom: 5px;cursor:pointer;\" alt=\"Image\"></div>"
		        +"<div style=\"width: 55%;\" onclick=\"CommentActFeed("+prjid+",'activityFeed')\" ><img class=\"img-responsive Post_cLabelTitle\" src=\""+path+"/images/workspace/post.png\" title=\" \" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\"></div>"
		        +"<input type=\"hidden\" id=\"latestFeed\" value=\"\" />"
		      +"</div>"
		      +"<div align=\"center\" class=\"main_commentTxtArea_optDiv \" style=\"height: 59px;\">"
		        +"<div id = \"actFeedMoreOpt\" align=\"right\" style=\"float: right; width: 35%;\" onclick=\"showActFeedMore(0)\">"
		           +"<img id=\"moreOption\" title=\" \" class=\"img-responsive Options_cLabelTitle\" src=\""+path+"/images/more.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
		        +"</div>"
		        ///+"<div id=\"actFeedOptPopDiv_0" class = "actOpt" style = "display:none;float: right;padding: 5px;background-color: rgb(255, 255, 255);position: absolute; height: auto;border-radius: 2px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); border: 1px solid rgb(161, 161, 161); z-index: 1; width: 50px; margin-right: -5px; right: -56px; margin-top: 2px;\">"            
		        
		        +"<div id=\"actFeedOptPopDiv_0\" class = \"actOpt\" style=\"right: 50px;\" >"          
		          +"<div class=\"workSpace_arrow_right\" style=\"position: absolute; float: left; margin-top: 2px; right: -10px;\">"
		              +"<img style=\"transform: rotate(0deg); width: 10px; height: 20px;\" src=\""+path+"/images/arrow.png\">"
		          +"</div>"
		          +"<div data-toggle=\"modal\" data-target=\"#myModal\" class=\"Camera_cLabelTitle\" title=\" \" onclick=\"initWebcam('','',"+prjid+",'activityFeed')\" style=\"width: 99%; height: auto; cursor: pointer; float: left; border-bottom: 1px solid rgb(204, 204, 204); padding: 2px 0px 8px;\">" 
		              +"<img class=\"imgCss\" src=\""+path+"/images/takepicture.png\" style=\"border: medium none;\">"
		          +"</div>"          
		          +"<div onclick=\"\" style=\"width: 99%; height: auto; cursor: pointer; float: left; padding: 8px 0px 4px;\">" 
		           
		               +"<form action=\""+path+"/WorkSpaceProjImgUpload\" enctype=\"multipart/form-data\" style=\"position:absolute;top:-100px;\" method=\"post\" name=\"convFileUpload\" id=\"convFileUpload\" >"				
				          +"<input type=\"file\" class=\"Upload_cLabelTitle\" title=\"\" value=\"\" onchange=\"readFileUrl(this,0);\" class=\"topic_file\" value=\"\" name=\"FileUpload\" id=\"FileUpload\">"
				      +"</form>"
		          
		             +"<img class=\"imgCss\" src=\""+path+"/images/upload.png\" title=\"Upload\" id=\"mainUpload\" style=\"border: medium none;\">"
		          +"</div>"         
		        +"</div>"
		        +"<div id=\"recAudioMsg\" style=\"float: right; width: 40%;\">"
		           +"<img class=\"img-responsive Rec_audio_cLabelTitle\" title=\" \" src=\""+path+"/images/record.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
		        +"</div>"
		       
		      +"</div>"
		       ///+"<img id = "testItalic" src=\"'+path+"/images/text_italic.png" onclick = "changeFontToItalic(this);\" ></img>"  
		      
		     +"</div>"
		     
		     +"<div class=\"col-sm-12 col-xs-12\" style=\"height:1px;background:#C1C5C8;margin-top: 15px;\"></div>"
		 +"</div>"    
	     +"<div class=\"row\" id=\"activityFeedList\" style=\"margin-left:0px;margin-right:0px;background:white;padding: 0 20px 0 20px\"></div>"
	 +"</div>"
  +"</div>"
  +"<input type=\"hidden\" name=\"projectidhidden\" id=\"projectidhidden\" value=\""+prjid+"\"/>"
  
	// <!-- Task related html -->
  		//<!-- Dependency task html -->	
      /*
		+"<div id=\"dependencyCreateTask\" class=\"createTaskPopUpMainCls eventPopup wsTaskMainDivCss \" style = \"z-index: 7000;\">"	
			+"<div id=\"depTaskContainerBase\" class=\"wsTaskBaseDivCss\">"
				+"<div id=\"dependencyTaskContainer\" style = \"width :57%;\" class=\"wsTaskMainContainerCss\">"
					+"<div  class=\"wsTaskMainContainerHeaderCss\" id=\"dependencyHeader\" style=\"height:6%;\">"
						+"<div style=\"font-size: 14px; font-family: OpenSansSemibold;\" class=\"wsTaskHeaderSelCss\">Task</div>"
					+"</div>"
					+"<div id=\"deptaskDetails\" class=\"wsTaskDetailsContainerCss\" style=\"height:17%;border:none !important;\">"
						+"<div style=\"width:100%;float: left;margin-top: 4px;\" class=\"taskTitle\">"
							+"<div style=\"width:12%;font-size: 13px; font-family: OpenSansSemibold;\" class=\"wsTaskLabels\" id=\"dependencyTitleText\">Task</div>"
			    			+"<input type=\"text\" class=\"defaultText\" style=\"border: 1px solid #c0c0c0; border-radius: 3px;height: 25px;width: 88%;\" id=\"dependencyEventName\" size=\"52\" placeholder=\"\" disabled=\"disabled\">"
						+"</div>"
						+"<div style=\"float:left;width:100%;height:25%;margin-top: 10px;\" id=\"dependencyDate\">"
							+"<div style=\"width:12%;font-size: 13px; font-family: OpenSansSemibold;\" class=\"wsTaskLabels\" id=\"dependencyTitleText\">From</div>"
							+"<input type=\"text\" class=\"taskStartEndDate\" readonly=\"true\" value=\"\" size=\"12\" placeholder=\"starts at\" style=\"border: 1px solid #c0c0c0; border-radius: 3px;height: 25px;width: 38%;float:left;\" id=\"depTaskStartDate\"  disabled=\"disabled\">"
							+"<div style=\"width:12%;font-size: 13px; font-family: OpenSansSemibold;\" class=\"wsTaskLabels\" id=\"dependencyTitleText\">To</div>"
							+"<input type=\"text\"  id=\"depTaskEndDate\" class=\"taskStartEndDate\" onclick=\"setSens('taskStartDate', 'min');\" style=\"border: 1px solid #c0c0c0; border-radius: 3px;height: 25px;width: 38%;\"  value=\"\" size=\"12\" placeholder=\"ends at\" readonly=\"true\" disabled=\"disabled\">"
						+"</div>"
					+"</div>"
					+"<div style=\"width:100%;float:left;padding-bottom: 5px;\" class=\"tabContentHeader\" >"
						+"<div align=\"left\" style=\"margin-left:5px;margin-top: 4px;\" class=\"\">Dependent Task</div>"
					+"</div>"
					+"<div id=\"selectedDependencyTaskDiv\" style=\"width:100%;float:left;\">"
						+"<div style=\"width:100%;height:313px;overflow-y:auto;margin-top:2%;\" id=\"selectedDependencyTaskContent\"></div>"
					+"</div>"
					+"<div id=\"depTaskSaveContainer\" style=\"height:10%;width:100%;margin-top:1%;border-top:1px solid #CCCCCC;float :left;\">"
						+"<div class=\"createDependencyTask text-uppercase createBtn\" id=\"createDepTask\" onclick=\"saveDependencyTasks()\">Save</div>"
					+"</div>"
				+"</div>"
				+"<div id=\"dependencyTaskListContainer\" style = \"width :43%;border: medium none !important;\" class=\"wsTaskMainContainerCss\">"
					+"<div style=\"height:6%;\" id=\"dependencyHeader\" class=\"wsTaskMainContainerHeaderCss\">"
						+"<div style=\"float: left;width: 100%;padding-left: 5px;\" id=\"dependencySearchTask\">"
							+"<div style=\"float: left;width: 100%;\">"
								+"<input type=\"text\" value=\"\" onkeyup=\"searchDepPage(this);\" style=\"float:left;margin-left:-5px;width: 100%;height: 30px;font-family:tahoma;padding-right:20px;border: 1px solid #C0C0C0; color: #848484; border-radius:3px;margin-top:-2%;\" class=\"userSearch defaultText Search\" placeholder=\"Search\" id=\"depSearch\" size=\"15\">"
								+"<input type=\"image\" value=\"\" src=\"images/searchTwo.png\" style=\"float: left; overflow: hidden; position: absolute; margin-left: -18px; margin-top: 2px;\"  class=\"depSearchUserImg\" id=\"depTaskWordSearch\">"
							+"</div>"
						+"</div>"
						///+"<img id=\"imgDepSearch"style=\"cursor: pointer;float: right;margin-top:-6%;margin-right:8%;\" onclick=\"showDepSearchNsort();\" src=\"images/calender/SortNSearchBlack.png" alt=\"">"
						
						+"<img src=\"images/close.png\" class=\"closePopup\" id=\"closePopup2\" style=\"float: right; cursor: pointer; margin-top: -3%; margin-right: -3%;\" onclick=\"closeDependencyPopUp();\">"
					+"</div>"
					+"<div style=\"width:100%;border-bottom: 1px solid #BFBFBF;float:left;padding-bottom: 5px;padding-top: 5px;\" class=\"tabContentHeader\">"
				 		+"<div align=\"left\" style=\"font-weight:normal;float:left;margin-top:3px;overflow:hidden;height:18px;\" class=\"\">&nbsp;<span class=\"eventNameLabel Task\" style=\"font-size:14px;\">Task</span></div>"
		    		+"</div>"
		    		+"<div id=\"depTaskList\"  style=\"float:left;width:100%;height:402px;margin-top:5px;font-size:13px;\"></div>"
		    		+"<div style=\"float:left;width:97%;height:40px;margin-top:2%;border-top:1px solid #BFBFBF;\">"
			 		 	+"<img style=\"float: right; margin-top: 12px; cursor: pointer;display:none;\" src=\"images/add.png\" onclick=\"addTaskAsDependency(this);\" class=\"addParticipantIcon Add_Stories_cLabelTitle\" id=\"addTasksdependency\">"
					+"</div>"
				+"</div>"
			+"</div>"
		+"</div>"
		//<!-- Dependency task html ends-->	
	//<!--  Conversation Thread Popup -->	
		+"<div class=\"taskLevelUpldPopup eventPopup wsTaskMainDivCss createTaskPopUpMainCls\" id=\"taskActivityFeed\" style=\"display: none;z-index: 8000;\">"
			+"<div id=\"convTaskContainer\" class=\"wsTaskBaseDivCss\">"
				+"<div id = \"toDisplayConvTaskName\" class= \"defaultExceedCls\" style=\"float: left; padding-left: 5px; width: 93%; font-size: 14px;font-family: opensansregular;\"></div>"
					+"<img id=\"docTaskclose\" class=\"closePopup Close\" src=\""+path+"/images/close.png\"  style=\"float: right;cursor: pointer;margin-left: 5px;\" onclick=\"convTaskclosePopup();\" title=\"Close\"></img>"
					+"<div id=\"convTaskContentMainDiv\" style=\"width: 100%;float: left;height: 93%\">"
						+"<div style=\"width:99%;padding-top: 1%;border-bottom: 1px solid #c1c5c8; \">"
	     			+"</div>"
	     			+"<div id=\"convTaskContentSubDiv\" style=\"overflow: auto;width: 100%;height: 94%;\"></div>"
				+"</div>"
			+"</div>"
	 	+"</div>" 	
	 	
	 //<!--  Conversation Thread Popup Ends here -->		
	//<!-- For upload document for task level --> 
	+"<iframe id=\"upload_task_target1\" name=\"upload_task_target1\" src=\"\" style=\"display:none;\"></iframe>" 
		+"<div class=\"taskLevelUpldPopup eventPopup wsTaskMainDivCss createTaskPopUpMainCls\" id=\"uploadTaskLevelDoc\" style=\"display: none;z-index: 8000; position: absolute;\">"
			+"<div id=\"taskContainerBaseDocUpload\" class=\"wsTaskBaseDivCss\">"
  				+"<div id = \"toDisplayTaskName\" style=\"float: left; padding-left: 5px; width: 93%; font-size: 14px;font-family: opensansregular;\"></div>"
     			+"<img id=\"docTaskclose\" class=\"closePopup Close\" src=\""+path+"/images/close.png\"  style=\"float: right;cursor: pointer;margin-left: 5px;\" onclick=\"docTaskclosePopup();\" title=\"Close\"></img>"
   				+"<div id=\"docTaskUpload\" class=\"cPointer\" style=\"float: left; display: block; margin-left: 5px; margin-top: 10px;\" title=\"Upload document for task\" onclick=\"TaskDocUpload();\">"
        			+"<img id = \"uploadTaskDocumentsImg\" style = \"width: 80%;\" src=\""+path+"/images/upload.png\">"
      			+"</div>"
    			+"<div id=\"WsUploadOptnsTask\" style=\"display:none;width: 170px; min-width: 100px; background-color: rgb(255, 255, 255); position: absolute; height: auto; z-index: 5010; margin-right: 105px; padding-left: 5px; border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); border: 1px solid rgb(161, 161, 161);left: 49px;\">"
					+"<div id=\"WsarrowDiv_left\" class=\"iOption_arrow-right\" style=\"float: left; margin-left: -18px; margin-top: 6px;\">"
						+"<img style=\"height: 23px;\" src=\""+path+"/images/arrow-left_new.png\" />"
					+"</div>"
     
	  				+"<div class=\"optionList\" style=\"height: 28px;\" onclick=\"\" onmouseover=\"onMouseOverDiv(this)\" onmouseout=\"onMouseOutDiv(this)\">"
						+"<div id=\"uploadDiv\" style=\"float:left;height:21px;\" class=\"cPointer\">"
							+"<form id=\"taskDocUploadForm\" name=\"taskDocUploadForm\" method=\"post\" enctype=\"multipart/form-data\" accept-charset = utf-8 action=\""+path+"/CalendarDocTaskUpload\">"
								+"<label class=\"repoUpload\" style=\"margin-top: 3px;width: 160px;background-image:none;\">"
				 			+"<input type=\"hidden\" id=\"uploadDocTaskId\" name=\"uploadDocTaskId\" />"
									+"<input type=\"hidden\" name=\"detectBrowser\" id=\"detectBrowser\" value=\"\"/>"
									+"<input type=\"hidden\" name=\"size\" id=\"size\" value=\"\"/>"
									+"<input type=\"hidden\" id=\"taskLevelprojectId\" name=\"taskLevelprojectId\" />"
									+"<input type=\"hidden\" id=\"doctaskType\" name=\"doctaskType\" value=\"\"/>" 
									+"<input type=\"hidden\" id=\"mValueHidddenVal\" name=\"mValueHidddenVal\" value=\"1\"/>"
									+"<input type=\"file\" id=\"\" name=\"filename\" value=\"\" class=\"file\" onchange=\"submitRepositoryFileForm(this);\"/>"
								+"</label>"
							+"</form>"
						+"</div>"
	  					+"<span class=\"OptionsFont Upload Upload_from_system_cLabelHtml\" style=\"margin-left:12px;margin-top: 3px;\">Upload from system</span>"
	  				+"</div>"
	  				+"<div class=\"optionList\" style=\"width:100%;height:25px;margin-bottom:3px;cursor:pointer;\" onmouseover=\"onMouseOverDiv(this)\" onmouseout=\"onMouseOutDiv(this)\" onclick=\"attachTaskRepo()\">" 
	        			+"<span class=\"OptionsFont Upload_from_repository_cLabelHtml\" style=\"margin-left: 12px;\">Upload from repository</span>"
	  				+"</div>"
	  				+"<div style=\"width:100%;height:25px;margin-bottom:3px;cursor:pointer;\" onclick=\"attachTaskLevelLink();\" onmouseover=\"onMouseOverDiv(this)\" onmouseout=\"onMouseOutDiv(this)\" onclick=\"attachTaskRepo()\">"
	           			+"<span class=\"OptionsFont Attach_Link_cLabelHtml\" style=\"margin-left: 12px;\">Attach Link</span>"
	  				+"</div>"
				+"</div>"
      
      			+"<div id=\"TaskLevellinkSection\" class=\"linkSection\" style=\"float: left; color: rgb(132, 132, 132); width:98%; font-weight: normal; padding-left:5px;display:none;margin-top:10px;margin-bottom:5px;\">"
					+"<div id=\"TaskLevellinkHeader\" class=\"Attach_Link_cLabelHtml\" style=\"float: left; font-weight: bold; width: 100%;\">Attach Link</div>"
					+"<div id=\"TaskLevellinkDiv\" style=\"float: left; width: 100%;\">"
		 			+"<table width=\"100%\" cellspacing=\"4\" cellpadding=\"2\" align=\"center\" style=\"margin-top: 10px;\">"
		   				+"<tbody>"
		      				+"<tr style=\"font-family:Tahoma;color:#848484;\">"
								+"<td width=\"8%\" align=\"left\"><span style=\"float: left;height: 18px;overflow: hidden;max-width: 100px;\" class=\"Link_Title_cLabelText\">Link_Title</span></td>"
								+"<td width=\"40%\" align=\"left\"><input type=\"text\" style=\"width:97%;border: 1px solid #BFBFBF;border-radius: 2px 2px 2px 2px;padding: 2px;font-family:Tahoma;color:#848484;\" value=\"\" id=\"TaskLevellinkTitle\" name=\"TaskLevellinkTitle\"></td>"
								+"<td width=\"5%\" align=\"left\"><span style=\"float: left;height: 18px;overflow: hidden;max-width: 100px;\" class=\"Link_cLabelText\">Link</span></td>"
								+"<td width=\"41%\" align=\"left\"><input type=\"text\" style=\"width:97%;border: 1px solid #BFBFBF;border-radius: 2px 2px 2px 2px;padding: 2px;font-family:Tahoma;color:#848484;\" value=\"\" id=\"TaskLevellinkLink\" name=\"TaskLevellinkLink\"></td>"
								+"<td width=\"15%\" align=\"left\"><img width=\"20\" height=\"18\" onclick=\"funAttachTaskLevelLinkForTask();\" style=\"cursor:pointer;margin-left:5px;\" title=\"Attach\" src=\""+path+"/images/attache.png\" class=\"Attach_cLabelTitle\" id=\"TaskLevelattachLinkImg\"><img width=\"20\" height=\"18\" onclick=\"cancelAttachTaskLevelLink();\" style=\"cursor:pointer;margin-left:5px;\" title=\"Close\" src=\""+path+"/images/remove.png\" class=\"Close_cLabelTitle\" id=\"TaskLeveldeleteLinkImg\"></td>"
							+"</tr>"
		  	 			+"</tbody>"
         			+"</table>"
        		+"</div>"
	 		+"</div>"
			+"<input type=\"hidden\" name=\"TaskLevellinkTaskId\" id=\"TaskLevellinkTaskId\" />"
      		+"<div id=\"docTaskContentMainDiv\" style=\"width: 100%;float: left;height: 93%\">"
          		+"<div class=\"tabContentHeader\" style=\"width:99% \">"
		  			+"<div class=\"hName\" style=\"overflow:hidden;height: 100%;width: 57.7%;float:left; padding-left: 1.5%;\">Name</div>"
		    		+"<div class=\"hOwner\" style=\"overflow:hidden;height: 100%;width:23%;float:left;\">Owner</div>"
		    		+"<div class=\"hCreated\" style=\"overflow:hidden;height: 100%;width: 15%;float:left;\">Created</div>"
		  		+"</div>"
			+"<div id=\"docTaskContentSubDiv\" style=\"overflow: auto;width: 100%;height: 94%;\"></div>"
    	+"</div>"
	+"</div>"
	+"</div>"
	
	//<!--  modal for snapsof-->
	+"<div  id=\"myModal\" class=\"modal fade\" role=\"dialog\">"
	+"<div class=\"modal-dialog\">"
		+"<div class=\"modal-content\" id =\"camBody\" style=\"display:block;border-radius:0px\">"
			+"<div class=\"modal-header\" style=\"border-bottom:0px\">"
			  +"<button onclick=\"stopWebcam()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>"
			  +"<h5 class=\"modal-title\" style=\"font-weight:bold\">Photo Capture</h5>"
			+"</div>"
			
			+"<div class=\"modal-body row\" >"
				+"<div  class=\"col-md-12 col-lg-12\" style=\"margin-top:-18px\">"
					+"<div id=\"recordImage\" class=\"row\" style=\" padding-left: 15px;padding-right: 15px;\">"
						+"<video onclick=\"snapshot(this);\" class=\"mysnapShot\" id=\"video\" autoplay muted></video>"
					+"</div>"
					+"<div id=\"camImage\"class=\"row\" style=\"display:none;padding-left:15px;padding-right:15px\">"
						+"<canvas  id=\"myCanvas\" class=\"canvasForSnap\" style=\"\"></canvas>"  
					+"</div>"
			   +"</div>"
			
			+"</div>"
			
			+"<div class=\"modal-footer row\" style=\"border-top:0px;padding-top: 0px; padding-bottom: 25px;height: 65px;\">"
				+"<div class=\"col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-xs-offset-2 col-lg-5 col-md-5 col-sm-5 col-xs-5\">"
					+"<img id = \"cam\"  onclick=\"snapshot(this);\" style = \"cursor:pointer;height:40px;width:40px\" src=\"images/camara.svg\">"
					+"<img id = \"videoCam\"  onclick=\"videoType('video');\" style = \"cursor:pointer;height:40px;width:40px\" src=\"images/videoIcon.svg\">"
					+"<img id = \"modalclose\"  class=\"modalclose\" onclick=\"\" style = \"display:none;cursor:pointer;height:40px;width:40px\" src=\"\">"
					//+"<img id = "videoCam"  class=\"modalclose" onclick=\"test('video');\" style = "cursor:pointer;height:40px;width:40px" src=\"'+path+"/images/videoIcon.svg">" 
				+"</div>"
				
				
			+"</div>"
			
			
		+"</div>"
		
		+"<div class=\"modal-content\" id=\"videoBody\" style=\"display:none\" >"
			+"<div class=\"modal-header\">"
			  //<button onclick=\"so()" type=\"button" class=\"close" data-dismiss=\"modal">&times;</button>" 
			  +"<button onclick=\"stopWebcam()\" type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>"
			  +"<h5 class=\"modal-title\" style=\"font-weight:bold\">Video Capture</h5>"
			+"</div>"
			
			+"<div class=\"modal-body row\" >"
				+"<div class=\"col-lg-6 col-md-6\">"
				+"<video width =\"210%\" id=\"videoCapture\" autoplay muted></video>"
				+"</div>"
				
				+"<div class=\"col-lg-6 col-md-6\">"
			 		+"<video width=\"210%\" id=\"recorded\" style=\"display:none;float:right\" controls></video>"
				+"</div>"
			+"</div>"
			
			    +"<div class=\"modal-footer \" style=\"display: flex;place-content: center;\">"
					+"<img id =\"videocancel\"  onclick=\"uploadCancel();\" style=\"cursor: pointer; height: 30px; width: 30px;\" src=\""+path+"/images/cancel.svg\">"
					+"<img id=\"download\" class=\"modalclose\" onclick=\"uploadVideo();\" style=\"margin-left: 4px;display: inline-block; cursor: pointer; height: 30px; width: 30px;\" src=\""+path+"/images/submit.svg\" data-dismiss=\"modal\">"
					+"<img src=\""+path+"/images/recordvideo.png\" title=\"Start Recording\" style=\"float:right;cursor:pointer\" id=\"record\" >"
				    +"<img src=\""+path+"/images/stopvideo.png\" title=\"Stop Recording\" style=\"float:right;cursor:pointer;display:none\" id=\"stop\" >"
				+"</div>"
		  +"</div>"
		
	+"</div>"
	+"</div>"
  
  +"</div>"
  
  +"</div>";  
	*/
	ui ="<div class=\"container-fluid contentCls p-0\" id=\"newId\">"
        +"<div id=\"tabContentDiv111\" style=\"display: none;\"><canvas id=\"upload_Newprofile555\" width=\"150\" height=\"150\"></canvas></div>"
       +"<div id=\"tabContentDiv\">"
	        
          +"<div class=\"row ml-0 mr-0 px-3\" id = \"rowTab\" style=\"background:white;padding:20px 0px 0px 0px\">"

              
	            +"<div class=\"col-12 p-0\" >"
                  +"<textarea  id=\"main_commentTextarea\" class=\"main_commentTxtArea rounded\" onpaste ='onPasteScreenShot(event,this);' onkeyup ='checkTag(event,\"main\",this,"+prjid+",\"activityFeed\"); commentAutoGrow(event,this,\"main\",0);'  onblur ='commentAutoGrow(event,this,\"main\",0);' oninput ='commentAutoGrow(event,this,\"main\",0);' onkeypress ='checkTag(event,\"main\",this,"+prjid+",\"activityFeed\")'); commentAutoGrow(event,this,\"main\",0);'></textarea>"//---------------
                  +"<input type=\"hidden\" id=\"menuTypeForComment\" value=\"activityfeed\">"
                  + "<input type=\"hidden\" id=\"ProjIdForComment\" value=\"" + prjid + "\">"
                  + "<div class=\"atHashContainerMain\" style=\"display: table; width: 100%;height: 2px;\"></div>"
                  //-------------------------------
                	// +"<div  class=\"activityfieldUserListMain activityfieldMainuserslist  \"          style='font-size:14px; margin-top: 90px !important; width:23vw !important'>"
		                  // +"<ul class=\"activityfielduserList\">"
                      // +"<li class=\"first\" style='font-size: 12px;color: black;font-weight: 700;cursor:pointer;'>Kalpan Chatterjee</li>"
                      // +"<li class=\"second\" style='font-size: 12px;color: black;font-weight: 700;cursor:pointer;margin-left: 4px !important;'>Tanmoy Hazra</li>"
                      // +"<li class=\"third\" style='font-size: 12px;color: black;font-weight: 700;cursor:pointer;margin-left: 4px !important;'>Kalpan Chatterjee</li>"
                      // +"<li class=\"fourth\" style='font-size: 12px;color: black;font-weight: 700;cursor:pointer;margin-left: 4px !important;'>Tanmoykanti Hazra</li>"
                      // +"<li class=\"fifth\" style='font-size: 12px;color: black;font-weight: 700;cursor:pointer;margin-left: 4px !important;'>Kalpan Chatterjee</li>"
                      // +"</ul>"
		              // +"</div>"
                  // +"<div  class=\"projUserListMain proTagNameId   \"      style='font-size:14px; margin-top: 90px !important; width:23vw !important'>"
                    // +"<ul class=\"projuserList\">"
                    //   +"<li class=\"first\" style='font-size: 12px;font-weight: 700;overflow: hidden;color: black;margin: 4% !important;cursor:pointer;'>devv4</li>"
                    //   +"<li class=\"second\" style='font-size: 12px;font-weight: 700;overflow: hidden;color: black;margin: 4% !important;cursor:pointer;'>mykronus</li>"
                    //   +"<li class=\"third\" style='font-size: 12px;font-weight: 700;overflow: hidden;color: black;margin: 4% !important;cursor:pointer;'>teamcity</li>"
                    //   +"<li class=\"fourth\" style='font-size: 12px;font-weight: 700;overflow: hidden;color: black;margin: 4% !important;cursor:pointer;'>mobiletesting</li>"
                    //   +"<li class=\"fifth\" style='font-size: 12px;font-weight: 700;overflow: hidden;color: black;margin: 4% !important;cursor:pointer;'>1testingworkspace</li>"
                    //   +"<li class=\"sixth\" style='font-size: 12px;font-weight: 700;overflow: hidden;color: black;margin: 4% !important;cursor:pointer;'>colabusv5development</li>"
                    //   +"<li class=\"seventh\" style='font-size: 12px;font-weight: 700;overflow: hidden;color: black;margin: 4% !important;cursor:pointer;'>code1</li>"
                    //   +"<li class=\"eigth\" style='font-size: 12px;font-weight: 700;overflow: hidden;color: black;margin: 4% !important;cursor:pointer;'>product</li>"
                    // +"</ul>"

		              //  +"</div>"

              +"</div>"
              +"<div class=\"col-12  p-0 d-flex justify-content-between mt-1\"  >"
                +'<div class="d-flex justify-content-between ">'
                  +'<div class="py-1 px-2 voiceFeed convoSubIcons" onclick="VoiceRecordingConversationNew(true,\'main\')"><img src="/images/conversation/mic.svg" class="image-fluid" title="Record audio" style="height:22px;cursor:pointer;"></div>'
                  +'<div class="py-1 px-2 position-relative convoSubIcons"><img src="/images/conversation/attach.svg" class="image-fluid fileAttachIcon" style="height:20px;cursor:pointer;" title="Upload" onclick="openAttachOptions(this,\'main\',0)"></div>'
                  +'<div class="py-1 px-2 convoSubIcons"><img src="/images/conversation/image.svg" class="image-fluid" title="From Gallery" style="height:20px;cursor:pointer;"></div>'
                  +'<div class="py-1 px-2 convoSubIcons"><img src="/images/conversation/cam.svg" onclick="camUiPopup();" class="image-fluid" title="Camera" style="height:20px;cursor:pointer;"></div>'
                  +"<div class=\"py-1 px-2 convoSubIcons\" onmouseover=\"changeSmileyellow('main');\" onmouseout=\"changeSmilewhite('main');\"><img src=\"/images/conversation/smile.svg\" id=\"smilePop\" title=\"\" class=\"image-fluid\" style=\"height:22px;cursor:pointer;\"></div>"
                  +'<div class="py-1 px-2 convoSubIcons"><img src="/images/conversation/at.svg" id="atTagImg" title="" class="image-fluid atImgExt" style="height:20px;cursor:pointer;" onclick="externalAtHashClick(\'main\', 0, \'@\')"></div>'
                  +'<div class="py-1 px-2 convoSubIcons"><img src="/images/conversation/hash.svg" id="hashTagImg" title="" class="image-fluid hashImgExt" style="height:19px;cursor:pointer;" onclick="externalAtHashClick(\'main\', 0, \'#\')"></div>'
                +'</div>'
                +'<div class="d-flex justify-content-between">'
                    +"<div class='p-1 mx-1'><img  class='convopost' src=\"/images/conversation/post1.svg\" title='Post' onclick=\"CommentActFeed("+prjid+",'activityFeed')\" class=\"image-fluid\" style=\"height:26px;cursor:pointer;\"></div>"
                +'</div>'
              +"</div>"
		      +"</div>"    

	        +"<div class=\"row m-0\" id=\"activityFeedList\" style=\"background:white;padding: 12px 0px 0px\">"
          
              /* +'<div class="media border border-bottom-1 border-top-0 border-left-0 border-right-0 p-0 pt-2 w-100" style="color:#787077;">'
                  +'<img src="https://newtest.colabus.com:1933///userimages/690.jpeg?1622210863872" alt="Saravanan Balakrishnan" class="mr-3 mt-1 rounded-circle" style="width:45px;">'
                  +'<div class="media-body">'
                      +'<p class="mb-2 font-weight-bold" style="font-size: 13px;">Saravanan Balakrishnan <span class="ml-2" style="color:#999999;font-size:12px;"><span>Posted : 1 d ago</span>&nbsp;|&nbsp;<span>Modified : 30 m ago</span></span></p>'
                      +'<p style="font-size: 14px;">@Dhananjaya Kumar - You can park or take this as medium priority - Introduce "Recommendations" option inside the CME- Call options (just above the highlights). We will discuss this feature - I know, we have to change the query to list the details accordingly.</p>'
                      
                      +'<div class="media pl-0 pt-1 pr-0 pb-1">'
                          +'<img src="https://newtest.colabus.com:1933///userimages/16.jpeg?1622210864817" alt="Dhananjay Kumar" class="mr-3 mt-1 rounded-circle" style="width:45px;">'
                          +'<div class="media-body">'
                          +'<p class="mb-2 font-weight-bold" style="font-size: 13px;">Dhananjay Kumar <span class="ml-2" style="color:#999999;font-size:12px;"><span>Posted : 1 h ago</span>&nbsp;|&nbsp;<span >Modified : 30 s ago</span></span></p>'
                              +'<p style="font-size: 14px;">@Saravanan Balakrishnan It is already there in highlights. We will discuss sir.</p>'
                          +'</div>'
                      +'</div>' 

                  +'</div>'
              +'</div>'
          
          +"</div>" */
	 
      +"</div>"
    +"</div>"
  +"<input type=\"hidden\" name=\"projectidhidden\" id=\"projectidhidden\" value=\""+prjid+"\"/>"
	
	$('#content').append(ui);
  // memberfind("main","");
//  wrkSpaceHashCodeFind("","main");
}

function changeSmileyellow(place,id){
  if(place == "main"){
    $('#smilePop').attr('src','/images/conversation/smile2.svg');
  }else{
    $('#smilePop_'+id).attr('src','/images/conversation/smile2.svg');
  }
  
}
function changeSmilewhite(place,id){
  if(place == "main"){
    $('#smilePop').attr('src','/images/conversation/smile.svg');
  }else{
    $('#smilePop_'+id).attr('src','/images/conversation/smile.svg');
  }
  
}








function comingsoon(){
  $('#content').html('');
  var ui = "<div class=\"container-fluid contentCls\"><h1 class='d-flex justify-content-center' style='margin-top: 15%;'>COMING SOON</h1></div>"
  $('#content').append(ui);
}

function showZonesOptions(){
  if($('#zoneListDiv').is(':hidden')){
    $('#zoneListDiv').show();
  }else{
    $('#zoneListDiv').hide();
  }
}

function switchZone(zonetype){
  
    pageAct=zonetype;
    if(zonetype == 'home'){
          $('#tzNavMenuDiv').removeClass('d-block').addClass('d-none');
          $('#content').css('padding-left','100px').addClass('homecontainersection');
          $('#tzBreadCrumDiv').show();
          $('#tzBreadCrumDiv').removeClass('visible').addClass('invisible');
          $('#addWorkspace').removeClass('d-block').addClass('d-none');
          $('#mzBreadCrumDiv').hide();
          $('.teamzone').remove();
          $('.gap2').remove();
          $('.workspaceOp').remove();
          $('.subscribeOp').remove();
          // $('#tzBreadCrumDiv').removeClass('invisible').addClass('visible');
          // $('#tzNavMenuDiv').css('position','fixed');
          //$('#homecontent').removeClass('d-none').addClass('d-block');
          loadSubPageContent(zonetype);
        
    }else if(zonetype == 'contacts'){
      /* $('#content').html("");
      $('#content').removeClass('homecontainersection');
      $('#tzNavMenuDiv').removeClass('d-block').addClass('d-none');
      $('#content').css('padding-left','0px');
      $('.horznavbar').css('padding-left','46px');
      $('#tzBreadCrumDiv').show();
      $('#tzBreadCrumDiv').removeClass('visible').addClass('invisible');
      $('#mzBreadCrumDiv').hide(); */
      $('#content').removeClass('homecontainersection');
      $('#tzNavMenuDiv').removeClass('d-none').addClass('d-block');
      $('#zoneOptionImg').attr('src','images/menus/entzone_white.svg');
      $('#zoneOptionSpan').text('Team Zone').text(getValues(companyLabels,"Enterprise_Zone"));
      $('.allZoneMenus').addClass('d-none').removeClass('d-flex');
      $('#ezMenuList').addClass('d-flex').removeClass('d-none');
      $('#tzBreadCrumDiv').show();
      $('#tzBreadCrumDiv').removeClass('visible').addClass('invisible');
      $('#addWorkspace').removeClass('d-block').addClass('d-none');
      $('#mzBreadCrumDiv').hide();
      
      loadSubPageContent(zonetype);
      menuLinkActive($('#contactsEZ'));

    }else if(zonetype == 'tz'){
      $('#tzNavMenuDiv').removeClass('d-none').addClass('d-block');
      
      $('#zoneOptionImg').attr('src','images/menus/teamzone_white.svg');
      $('#zoneOptionSpan').text('Team Zone').text(getValues(companyLabels,"Team_Zone"));
      
      $('.allZoneMenus').addClass('d-none').removeClass('d-flex');
      $('#tzMenuList').addClass('d-flex').removeClass('d-none');
      $('#mzBreadCrumDiv').hide();
      $('#tzBreadCrumDiv').show();
      $('#tzBreadCrumDiv').removeClass('invisible').addClass('visible');
      $('#addWorkspace').removeClass('d-block').addClass('d-none');
      $('#content').html("");
      showProjectDDList("1").then( res => {
				
        let result = res[0].result;
        if(result == "success"){
          let latestProj = res[0].publicProjectList;
         console.log("latestProj:"+latestProj);
				let projId = latestProj[0].project_id;
				let projName = latestProj[0].project_name;
				projName=projName.replaceAll("ch(20)","'");
				let projUserStatus = latestProj[0].project_user_role;
				let projArchStatus = latestProj[0].project_status;
				let projectType = "";
          if(projUserStatus == "PO"){
            projectType="MyProjects";
          }else{
            projectType="SharedProjects";
          } 
          let prjImagePath = latestProj[0].imageUrl;
  
          //---update breadcrum proj icon and name
          $('img.breadcrumImg').attr('src',prjImagePath).attr('title', projName);
          $('span.breadCrumProjName').text(projName);
          menuLinkActive($('#convMenu'));//---activating conversation menu default
          projectRedirection(projId,projName,projUserStatus,projArchStatus,projectType,prjImagePath);
        }else{
          switchZone('home');
        }
				
			 });
    }else if(zonetype == 'mz' || zonetype == 'myTask'){
      $('#tzNavMenuDiv').removeClass('d-none').addClass('d-block');
      $('#zoneOptionImg').attr('src','images/menus/myzone_white.svg');
      $('#zoneOptionSpan').text('Team Zone').text(getValues(companyLabels,"My_Zone"));
      $('.allZoneMenus').addClass('d-none').removeClass('d-flex');
      $('#mzMenuList').addClass('d-flex').removeClass('d-none');
      $('#tzBreadCrumDiv').removeClass('visible').addClass('invisible');
      $('#content').html("").css('padding-left','100px').removeClass('homecontainersection');
      $('#addWorkspace').removeClass('d-block').addClass('d-none');
      $('#tzBreadCrumDiv').hide();
      $('#mzBreadCrumDiv').show();
      $('.horznavbar').css('padding-left','');
      // $('span.breadCrum1').text(getValues(companyLabels,"mytask")).attr('onclick','fetchmzTasks("MyZone","firstClick");mzOptionActive(this);').addClass('mzoptionactive');
      // $('span.breadCrum2').text(getValues(companyLabels,"assignedtask")).attr('onclick','fetchmzTasks("assignedTask","firstClick");mzOptionActive(this);');
      $('span.breadCrum3').text("Tasks").attr('onclick','fetchmzTasks("historyTask","firstClick");mzOptionActive(this);').addClass('mzoptionactive');
      loadSubPageContent(zonetype);
      menuLinkActive($('#mzTaskMenu'));
    }else if(zonetype == 'ez' || zonetype == 'contacts'){
      $('#tzNavMenuDiv').removeClass('d-none').addClass('d-block');
      $('#zoneOptionImg').attr('src','images/menus/entzone_white.svg');
      $('#zoneOptionSpan').text('Team Zone').text(getValues(companyLabels,"Enterprise_Zone"));
      $('.allZoneMenus').addClass('d-none').removeClass('d-flex');
      $('#ezMenuList').addClass('d-flex').removeClass('d-none');
      $('#tzBreadCrumDiv').show();
      $('#tzBreadCrumDiv').removeClass('visible').addClass('invisible');
      $('#addWorkspace').removeClass('d-block').addClass('d-none');
      $('#mzBreadCrumDiv').hide();
      $('#content').html("");
      loadSubPageContent('contacts');
      menuLinkActive($('#contactsEZ'));
    }else if(zonetype == 'landingpageTOtz' || zonetype == 'loadTaskPage' || zonetype == 'ideasMenu' || zonetype == 'agile' || zonetype == 'sprint' || zonetype == 'wsdocument'|| zonetype == 'emailMenu'){
      if(zonetype == 'landingpageTOtz'){
        menuLinkActive($('#convMenu'));
      }else if(zonetype == 'loadTaskPage'){
        console.log("task module");
        menuLinkActive($('#taskMenu'));
      }else if(zonetype == 'ideasMenu'){
        menuLinkActive($('#ideaMenu'));
      }else if(zonetype == 'agile'){
        menuLinkActive($('#agileMenu'));
      }else if(zonetype == 'sprint'){
        menuLinkActive($('#sprintMenu'));
      }else if(zonetype == 'wsdocument'){
        menuLinkActive($('#wsdocumentMenu'));
      }else if(zonetype == 'wsTeam'){
        menuLinkActive($('#wsTeam'));
      }
      else if(zonetype == 'emailMenu'){
        menuLinkActive($('#emailMenu'));
        $(".breadCrumMenuName").text("Email");
      }
      $('#tzNavMenuDiv').removeClass('d-none').addClass('d-block');
      $('#zoneOptionImg').attr('src','images/menus/teamzone_white.svg');
      $('#zoneOptionSpan').text('Team Zone').text(getValues(companyLabels,"Team_Zone"));
      
      $('.allZoneMenus').addClass('d-none').removeClass('d-flex');
      $('#tzMenuList').addClass('d-flex').removeClass('d-none');

      $('#content').css('padding-left','100px').html('').removeClass('homecontainersection');
      $('.horznavbar').css('padding-left','100px');
      $('#tzBreadCrumDiv').show();
      $('#tzBreadCrumDiv').removeClass('invisible').addClass('visible');
      $('#addWorkspace').removeClass('d-block').addClass('d-none');
      $('#mzBreadCrumDiv').hide();
      $('.teamzone').remove();
      $('.gap2').remove();
      $('.workspaceOp').remove();
      $('.subscribeOp').remove();
      $('.breadCrumProjName').show();
      $('#gap1').show();
      $('.breadCrumMenuName').show();
      // $('#tzNavMenuDiv').css('position','fixed');
      

    }else if(zonetype == 'exsitingWorkSpace'){
      $('#content').removeClass('homecontainersection');
      $('#tzNavMenuDiv').removeClass('d-block').addClass('d-none');
      // $('#tzBreadCrumDiv').html("");
      // $('#tzNavMenuDiv').css('position','relative');
      // // $('.sidebar-sticky').css('display','none');
      $('#content').css('padding-left','0px');
      $('.horznavbar').css('padding-left','46px');
      $('#tzBreadCrumDiv').show();
      $('#tzBreadCrumDiv').removeClass('visible').addClass('invisible');
      $('#mzBreadCrumDiv').hide();
      $('#addWorkspace').removeClass('d-none').addClass('d-block');
      workSpaceOptionUi();
      loadSubPageContent(zonetype);
    }else if(zonetype == 'colabusIntegration'){
      $('#content').removeClass('homecontainersection');
      $('#tzNavMenuDiv').removeClass('d-none').addClass('d-block');
      $('#zoneOptionImg').attr('src','images/menus/entzone_white.svg');
      $('#zoneOptionSpan').text('Team Zone').text(getValues(companyLabels,"Enterprise_Zone"));
      $('.allZoneMenus').addClass('d-none').removeClass('d-flex');
      $('#ezMenuList').addClass('d-flex').removeClass('d-none');
      $('#tzBreadCrumDiv').show();
      $('#tzBreadCrumDiv').removeClass('visible').addClass('invisible');
      $('#addWorkspace').removeClass('d-block').addClass('d-none');
      $('#mzBreadCrumDiv').hide();
      
      loadSubPageContent(zonetype);
      menuLinkActive($('#integrationEZ'));

    }
    
}

function mzOptionActive(obj){
  $('span.mzoption').removeClass('mzoptionactive');
	$(obj).addClass('mzoptionactive');
}
var typevalueForWork="";
function workSpaceOptionUi(){
  $('#tzBreadCrumDiv').removeClass('invisible').addClass('visible');
  $('.breadCrumProjName').hide();
  $('#gap1').hide();
  $('.breadCrumMenuName').hide();
  var Ui="";
  Ui="<span class=\"teamzone\">Teamzone</span>"
  +"<span class=\"gap2\" style='vertical-align: text-bottom;'>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</span>"
  +"<span class=\"workspaceOp\" onclick=\"workspaceO()\" style='cursor: pointer;text-decoration: underline;'>Workspace</span>"
  +"<span class=\"gap2\" style='vertical-align: text-bottom;'>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</span>"
  +"<span class=\"subscribeOp\" onclick=\"subcribeO()\" style='cursor: pointer;color: #a29dbf;'>Subscribe</span>"

  $('#tzBreadCrumDiv').append(Ui);
}

function workspaceO(){
  $('.workspaceOp').css('color','white');
  $('.workspaceOp').css('text-decoration','underline');
  $('.subscribeOp').css('text-decoration','none');
  $('.subscribeOp').css('color','#a29dbf');
  $('#subscribe').hide();
  $('#work').show();
  $('#addWorkspace').removeClass('d-none').addClass('d-block');
  typevalueForWork="exsit";
}

function subcribeO(){
  $('.subscribeOp').css('color','white');
  $('.subscribeOp').css('text-decoration','underline');
  $('.workspaceOp').css('text-decoration','none');
  $('.workspaceOp').css('color','#a29dbf');
  $('#work').hide();
  $('#subscribe').show();
  $('#addWorkspace').removeClass('d-block').addClass('d-none');
  typevalueForWork="subscribe";
}




function opensrhsort(){
  var ui="";
  console.log(pageAct);
  

  if(pageAct == "home"){
      $('#tzNavMenuDiv').removeClass('sidebar');
      $('#searchoptiondivPopup').html('');
      
      ui='<div id="globalsearchdivision" style="height: calc(100vh - 90px);" class="">'
          
          +'<div class="container-fluid">'
            +'<span class="d-flex justify-content-end mr-4" onclick="closeSearch()" style="cursor:pointer;" title="Close">x</span>'
          +'</div>'
          
          +'<div class="container d-flex w-100 my-4" style="height: 40px;">'
            +'<input class="srh w-100" style="height:40px;" type="text" placeholder="Search all" name="search">'
            +'<img src="/images/menus/search.svg" id="" onclick="" class="" style="cursor:pointer;background-color:rgb(50, 13, 111);" title="">'
          +'</div>'

          +'<div class="container d-flex">'
            +'<ul class="nav nav-pills" style="width:80%">'
            +'<li><a class="paddingclass mr-sm-1 p-sm-1 p-md-1 p-lg-2 p-xl-2" style="padding-left:0px;" href="#">All</a></li>'
            +'<li><a class="paddingclass mr-sm-1 p-sm-1 p-md-1 p-lg-2 p-xl-2" href="#">Projects</a></li>'
            +'<li><a class="paddingclass mr-sm-1 p-sm-1 p-md-1 p-lg-2 p-xl-2" href="#">Notifications</a></li>'
            +'<li><a class="paddingclass mr-sm-1 p-sm-1 p-md-1 p-lg-2 p-xl-2" href="#">Files</a></li>'
            +'<li><a class="paddingclass mr-sm-1 p-sm-1 p-md-1 p-lg-2 p-xl-2" href="#">People</a></li>'
            +'</ul>'
            +'<ul class="nav navbar-nav" style="width:20%">'
            +'<li><a class="float-right rounded defaultExceedCls" style="color:black;background-color: #80b3ff;text-decoration: none;" href="#" align="center">Filter by date</li>'
            +'</ul>'
          +'</div>'
        +'</div>' 

      $('#searchoptiondivPopup').append(ui).show();
   
  
  }else if(pageAct == 'tz' || pageAct == 'landingpageTOtz'){

    $('#searchoptiondiv').html('');
    
      ui='<div class = "searchSortPopUp d-block" style="height: 71px;" id = "wrkSpaceSearchActivityFeedSearch">'
      //+'<div id = "WsarrowDiv" class="workSpace_arrow_right" style="margin-top:14px;margin-right:-16px;"><img src="/images/arrow.png"></img></div>'
      +'<div class="serachMainDivCls d-flex">'
        +'<select  id = "sortInWs" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected wrkSpaceSearchActivityFeedSearchNew" onchange = "sortActFeedDetails(this)"  type="company">'
        +'<option class="drpDwnOptions Sort_cLabelText Latest_Update_cLabelText" value="lu" id="sort">'+getValues(companyLabels,"Latest_Update")+'</option>'
        +'<option class="drpDwnOptions Sort_Name_cLabelText Oldest_Update_cLabelText" value="ou" id="name">'+getValues(companyLabels,"Oldest_Update")+'</option>'
        +'<option class="drpDwnOptions Sort_cLabelText Latest_Conversation_Thread_cLabelText" value="lc" id="sort">'+getValues(companyLabels,"Latest_Conversation_Thread")+'</option>'
        +'<option class="drpDwnOptions Sort_Name_cLabelText Oldest_Conversation_Thread_cLabelText" value="oc" id="name">'+getValues(companyLabels,"Oldest_Conversation_Thread")+'</option>'
        +'</select>'
        +'<div class="searchIconMainDiv" align="center" style="">'
        +'<img src="/images/sort2.png" style="height: 16px;">'
        +'</div>'
      +'</div>'
      +'<div class="serachMainDivCls d-flex">'
      +'<input onkeyup="filterConversationData()" onkeypress="searchconversation(event)" style = "width:90%;border: none;outline: none;" class = "serachBoxInputBoxCommonCls wrkSpaceSearchActivityFeedSearchNew"  id = "searchBox" name = "searchBox" placeholder = "Search" type="text">'
        +'<div class="searchCloseIconMainDiv"> ' 
        +'<img class="serachBoxClearIcon" onclick="clearsearch();" id="ClearComSearch" src="/images/xforsearch.png">'
        +'</div>'
        +'<div class="searchIconMainDiv" align="center">'
        +'<img src="/images/searchTwo.png" onclick="searchWrkspaceActFeed();" id="wsSearch" class="notesSearchbox">'
        +'</div>'
      +'</div>'
    +'</div>	'

    $('#searchoptiondiv').append(ui).show();
    $('#cmecontent').addClass('d-none');
  }else if(pageAct == "loadTaskPage"){
    $('#searchoptiondiv').html('');
    
      ui='<div class = "searchSortPopUp d-block" style="height: 100px;" id = "">'
      //+'<div id = "WsarrowDiv" class="workSpace_arrow_right" style="margin-top:14px;margin-right:-16px;"><img src="/images/arrow.png"></img></div>'
      +'<div id="selectTaskType" class="serachMainDivCls d-flex">'
        +'<select  id = "taskFilter" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected" onchange = "taskFilterSort();"  type="company">'
          +'<option id="defaultTaskSort" value="All" class="drpDwnOptions All_cLabelText">'+getValues(companyLabels,"All")+'</option>'
          +'<option value="task" class="drpDwnOptions Notif_Calendar_Tasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_Tasks")+'</option>'
          +'<option value="ConversationTasks" class="drpDwnOptions Conversation_Tasks_cLabelText">'+getValues(companyLabels,"Conversation_Tasks")+'</option>'
          +'<option value="workflow" class="drpDwnOptions Notif_Calendar_WorkflowTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_WorkflowTasks")+'</option>'
          +'<option value="document" class="drpDwnOptions Notif_Calendar_DocumentTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_DocumentTasks")+'</option>'
          +'<option value="EmailTasks" class="drpDwnOptions Notif_Calendar_EmailTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_EmailTasks")+'</option>'
          +'<option value="idea" class="drpDwnOptions Notif_Calendar_IdeaTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_IdeaTasks")+'</option>'
          +'<option value="sprint" class="drpDwnOptions Notif_Calendar_SprintTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_SprintTasks")+'</option>'
          +'<option value="HighlightTasks" class="drpDwnOptions Highlight_Task_cLabelText">'+getValues(companyLabels,"Highlight_Task")+'</option>'
          +'<option value="Event" class="drpDwnOptions Event_cLabelText">'+getValues(companyLabels,"Event")+'</option>'
          +'<option value="Completed" class="drpDwnOptions Completed_cLabelText">'+getValues(companyLabels,"Completed")+'</option>'
          +'<option value="NotCompleted" class="drpDwnOptions Tasks_exceeded_cLabelText">'+getValues(companyLabels,"Tasks")+'</option>'
          +'<option value="InProgress" class="drpDwnOptions Active_tasks_cLabelText">'+getValues(companyLabels,"Active_tasks")+'</option>'
          +'<option value="InComplete" class="drpDwnOptions InComplete_cLabelText">'+getValues(companyLabels,"InComplete")+'</option>'
        +'</select>'
        +'<div class="searchIconMainDiv mt-1" align="center" style="">'
        +'<img src="/images/filter1.png" style="height: 14px;">'
        +'</div>'
      +'</div>'
      +'<div id="sortDiv" class="serachMainDivCls d-flex">'
        +'<select  id = "taskSort" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected" onchange = "taskFilterSort();"  type="company">'
          +'<option value="sort" class="drpDwnOptions Sort_cLabelText">'+getValues(companyLabels,"Sort")+'</option>'
          +'<option value="name" class="drpDwnOptions Task_Name_cLabelText">'+getValues(companyLabels,"Task_Name")+'</option>'
          +'<option value="assignedto" class="drpDwnOptions Assigned_to_cLabelText">'+getValues(companyLabels,"Assigned_to")+'</option>'
          +'<option value="createdby" class="drpDwnOptions Created_by_cLabelText">'+getValues(companyLabels,"Created_by")+'</option>'
          +'<option value="priority_hl" class="drpDwnOptions Priorityhl_cLabelText">'+getValues(companyLabels,"Priorityhl")+'</option>'
          +'<option value="priority_lh" class="drpDwnOptions Prioritylh_cLabelText">'+getValues(companyLabels,"Prioritylh")+'</option>'
          +'<option value="created_timestamp" class="drpDwnOptions Most_recently_created_cLabelText">'+getValues(companyLabels,"Most_recently_created")+'</option>'
          +'<option value="group_name" class="drpDwnOptions Group_Name_cLabelText">'+getValues(companyLabels,"Group_Name")+'</option>'
        +'</select>'
        +'<div class="searchIconMainDiv mt-1" align="center" style="">'
        +'<img src="/images/sort2.png" style="height: 14px;">'
        +'</div>'
      +'</div>'
      +'<div class="serachMainDivCls d-flex">'
      +'<input onkeyup="searchOnTaskEnter(event,this);" style = "width:90%;border: none;outline: none;" class = "serachBoxInputBoxCommonCls wrkSpaceSearchActivityFeedSearchNew"  id = "searchBox" name = "searchBox" placeholder = "Search" type="text">'
        +'<div class="searchCloseIconMainDiv"> ' 
        +'<img class="serachBoxClearIcon" onclick="clearWrkspaceTaskSearch(this);" id="ClearComSearch" src="/images/xforsearch.png">'
        +'</div>'
        +'<div class="searchIconMainDiv mt-1" align="center">'
        +'<img src="/images/searchTwo.png" onclick="taskFilterSort();" id="wsSearch" style="height: 14px;" class="notesSearchbox">'
        +'</div>'
      +'</div>'
    +'</div>	'

    $('#searchoptiondiv').append(ui).show();

    if(taskFilterVal != "")
    $("#taskFilter option[value="+taskFilterVal+"]").attr('selected', 'selected');

    if(taskSortType != "")
    $("#taskSort option[value="+taskSortType+"]").attr('selected', 'selected'); 

    if(taskSearchWord != "")
    $("#searchBox").val(taskSearchWord);

  }else if( pageAct == "mz"){

    $('#searchoptiondiv').html('');
    
      ui='<div class = "searchSortPopUp d-block" style="height: 100px;" id = "">'
      //+'<div id = "WsarrowDiv" class="workSpace_arrow_right" style="margin-top:14px;margin-right:-16px;"><img src="/images/arrow.png"></img></div>'
      +'<div id="selectTaskType" class="serachMainDivCls d-flex">'
        +'<select  id = "taskFilter" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected" onchange = "taskFilterSort();"  type="company">'
          +'<option id="defaultTaskSort" value="All" class="drpDwnOptions All_cLabelText">'+getValues(companyLabels,"All")+'</option>'
          +'<option value="task" class="drpDwnOptions Notif_Calendar_Tasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_Tasks")+'</option>'
          +'<option value="ConversationTasks" class="drpDwnOptions Conversation_Tasks_cLabelText">'+getValues(companyLabels,"Conversation_Tasks")+'</option>'
          +'<option value="workflow" class="drpDwnOptions Notif_Calendar_WorkflowTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_WorkflowTasks")+'</option>'
          +'<option value="document" class="drpDwnOptions Notif_Calendar_DocumentTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_DocumentTasks")+'</option>'
          +'<option value="EmailTasks" class="drpDwnOptions Notif_Calendar_EmailTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_EmailTasks")+'</option>'
          +'<option value="idea" class="drpDwnOptions Notif_Calendar_IdeaTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_IdeaTasks")+'</option>'
          +'<option value="sprint" class="drpDwnOptions Notif_Calendar_SprintTasks_cLabelText">'+getValues(companyLabels,"Notif_Calendar_SprintTasks")+'</option>'
          +'<option value="HighlightTasks" class="drpDwnOptions Highlight_Task_cLabelText">'+getValues(companyLabels,"Highlight_Task")+'</option>'
          +'<option value="Event" class="drpDwnOptions Event_cLabelText">'+getValues(companyLabels,"Event")+'</option>'
          +'<option value="Completed" class="drpDwnOptions Completed_cLabelText">'+getValues(companyLabels,"Completed")+'</option>'
          +'<option value="NotCompleted" class="drpDwnOptions Tasks_exceeded_cLabelText">'+getValues(companyLabels,"Tasks")+'</option>'
          +'<option value="InProgress" class="drpDwnOptions Active_tasks_cLabelText">'+getValues(companyLabels,"Active_tasks")+'</option>'
          +'<option value="InComplete" class="drpDwnOptions InComplete_cLabelText">'+getValues(companyLabels,"InComplete")+'</option>'
          +'<option value="MyZone" class="drpDwnOptions My_Tasks_cLabelText">'+getValues(companyLabels,"MY_TASKS")+'</option>'
          +'<option value="assignedTask" class="drpDwnOptions Assigned_Tasks_cLabelText">'+getValues(companyLabels,"ASSIGNED_TASKS")+'</option>'
        +'</select>'
        +'<div class="searchIconMainDiv mt-1" align="center" style="">'
        +'<img src="/images/filter1.png" style="height: 14px;">'
        +'</div>'
      +'</div>'
      +'<div id="sortDiv" class="serachMainDivCls d-flex">'
        +'<select  id = "taskSort" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected" onchange = "taskFilterSort();"  type="company">'
          +'<option value="sort" class="drpDwnOptions Sort_cLabelText">'+getValues(companyLabels,"Sort")+'</option>'
          +'<option value="name" class="drpDwnOptions Task_Name_cLabelText">'+getValues(companyLabels,"Task_Name")+'</option>'
          +'<option value="assignedto" class="drpDwnOptions Assigned_to_cLabelText">'+getValues(companyLabels,"Assigned_to")+'</option>'
          +'<option value="createdby" class="drpDwnOptions Created_by_cLabelText">'+getValues(companyLabels,"Created_by")+'</option>'
          +'<option value="priority_hl" class="drpDwnOptions Priorityhl_cLabelText">'+getValues(companyLabels,"Priorityhl")+'</option>'
          +'<option value="priority_lh" class="drpDwnOptions Prioritylh_cLabelText">'+getValues(companyLabels,"Prioritylh")+'</option>'
          +'<option value="created_timestamp" class="drpDwnOptions Most_recently_created_cLabelText">'+getValues(companyLabels,"Most_recently_created")+'</option>'
          +'<option value="group_name" class="drpDwnOptions Group_Name_cLabelText">'+getValues(companyLabels,"Group_Name")+'</option>'
        +'</select>'
        +'<div class="searchIconMainDiv mt-1" align="center" style="">'
        +'<img src="/images/sort2.png" style="height: 14px;">'
        +'</div>'
      +'</div>'
      +'<div class="serachMainDivCls d-flex">'
      +'<input onkeyup="searchOnTaskEnter(event,this);" style = "width:90%;border: none;outline: none;" class = "serachBoxInputBoxCommonCls wrkSpaceSearchActivityFeedSearchNew"  id = "searchBox" name = "searchBox" placeholder = "Search" type="text">'
        +'<div class="searchCloseIconMainDiv"> ' 
        +'<img class="serachBoxClearIcon" onclick="clearWrkspaceTaskSearch(this);" id="ClearComSearch" src="/images/xforsearch.png">'
        +'</div>'
        +'<div class="searchIconMainDiv mt-1" align="center">'
        +'<img src="/images/searchTwo.png" onclick="taskFilterSort();" id="wsSearch" style="height: 14px;" class="notesSearchbox">'
        +'</div>'
      +'</div>'
    +'</div>	'

    $('#searchoptiondiv').append(ui).show();

    if(taskFilterVal != "")
    $("#taskFilter option[value="+taskFilterVal+"]").attr('selected', 'selected');

    if(taskSortType != "")
    $("#taskSort option[value="+taskSortType+"]").attr('selected', 'selected'); 

    if(taskSearchWord != "")
    $("#searchBox").val(taskSearchWord);

  }else if(pageAct == "exsitingWorkSpace"){
    // alert("typevalueForWork :"+typevalueForWork);
    $('#searchoptiondiv').html('');
    if(typevalueForWork == "exsit" || typevalueForWork == ""){
      ui='<div class = "searchSortPopUp d-block" style="height: 100px;" id = "wrkSpaceSearchActivityFeedSearch">'
      //+'<div id = "WsarrowDiv" class="workSpace_arrow_right" style="margin-top:14px;margin-right:-16px;"><img src="/images/arrow.png"></img></div>'
      +'<div class="serachMainDivCls d-flex">'
        +'<select  id = "exsitSort" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected wrkSpaceSearchActivityFeedSearchNew" onchange = "filterWorkspace()"  type="company">'
        +'<option class="drpDwnOptions Sort_cLabelText Latest_Update_cLabelText" value="sort" id="sort">Sort</option>'
        +'<option class="drpDwnOptions Sort_Name_cLabelText Oldest_Update_cLabelText" value="myWrkVisible" id="name" selected>My Workspaces (Visible)</option>'
        +'<option class="drpDwnOptions Sort_cLabelText Latest_Conversation_Thread_cLabelText" value="myWrkHidden" id="sort">My Workspaces (Hidden)</option>'
        +'<option class="drpDwnOptions Sort_Name_cLabelText Oldest_Conversation_Thread_cLabelText" value="allMyWrk" id="name">All My Workspaces</option>'
        +'</select>'
        +'<div class="searchIconMainDiv" align="center" style="">'
        +'<img src="/images/filter1.png" style="height: 16px;">'
        +'</div>'
      +'</div>'
      +'<div class="serachMainDivCls d-flex">'
        +'<select  id = "exsitFilter" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected wrkSpaceSearchActivityFeedSearchNew" onchange = "filterWorkspace()"  type="company">'
        +'<option class="drpDwnOptions Sort_cLabelText Latest_Update_cLabelText" value="sort" id="sort">Sort</option>'
        +'<option class="drpDwnOptions Sort_Name_cLabelText Oldest_Update_cLabelText" value="name" id="name">Name</option>'
        +'<option class="drpDwnOptions Sort_cLabelText Latest_Conversation_Thread_cLabelText" value="recentlyused" id="sort">Recently Used</option>'
        +'</select>'
        +'<div class="searchIconMainDiv" align="center" style="">'
        +'<img src="/images/sort2.png" style="height: 16px;">'
        +'</div>'
      +'</div>'
      +'<div class="serachMainDivCls d-flex">'
      +'<input  style = "width:90%;border: none;outline: none;" class = "serachBoxInputBoxCommonCls wrkSpaceSearchActivityFeedSearchNew"  id = "exsitsSearch" name = "searchBox" placeholder = "Search" type="text">'
        +'<div class="searchCloseIconMainDiv"> ' 
        +'<img class="serachBoxClearIcon1" onclick="clearsearchWork();" id="ClearComSearch" src="/images/xforsearch.png">'
        +'</div>'
        +'<div class="searchIconMainDiv" align="center">'
        +'<img src="/images/searchTwo.png" onclick="filterWorkspace();" id="wsSearch" class="notesSearchbox">'
        +'</div>'
      +'</div>'
    +'</div>	'

  }else if(typevalueForWork == "subscribe"){
    ui='<div class = "searchSortPopUp d-block" style="height: 70px;" id = "wrkSpaceSearchActivityFeedSearch">'
      //+'<div id = "WsarrowDiv" class="workSpace_arrow_right" style="margin-top:14px;margin-right:-16px;"><img src="/images/arrow.png"></img></div>'
      +'<div class="serachMainDivCls d-flex">'
        +'<select  id = "subscribeFilter" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected wrkSpaceSearchActivityFeedSearchNew" onchange = "subscribeFilterWork()"  type="company">'
        +'<option class="drpDwnOptions Sort_cLabelText Latest_Update_cLabelText" value="sort" id="sort">Sort</option>'
        +'<option class="drpDwnOptions Sort_Name_cLabelText Oldest_Update_cLabelText" value="name" id="name">Name</option>'
        +'</select>'
        +'<div class="searchIconMainDiv" align="center" style="">'
        +'<img src="/images/sort2.png" style="height: 16px;">'
        +'</div>'
      +'</div>'
      +'<div class="serachMainDivCls d-flex">'
      +'<input  style = "width:90%;border: none;outline: none;" class = "serachBoxInputBoxCommonCls wrkSpaceSearchActivityFeedSearchNew"  id = "subscribeSearch" name = "searchBox" placeholder = "Search" type="text">'
        +'<div class="searchCloseIconMainDiv"> ' 
        +'<img class="serachBoxClearIcon1" onclick="clearsearchSubscribeWork();" id="ClearComSearch" src="/images/xforsearch.png">'
        +'</div>'
        +'<div class="searchIconMainDiv" align="center">'
        +'<img src="/images/searchTwo.png" onclick="subscribeFilterWork();" id="wsSearch" class="notesSearchbox">'
        +'</div>'
      +'</div>'
    +'</div>	'
  }

    $('#searchoptiondiv').append(ui).show();
  }else if(pageAct == "ideasMenu"){
    $('#searchoptiondiv').html('');
    ideasearchvalue = ideasearchvalue != "" ? ideasearchvalue : "";
    ui='<div class = "searchSortPopUp d-block" style="height: 40px;" id = "wrkSpaceSearchActivityFeedSearch">'
      +'<div class="serachMainDivCls d-flex">'
      +'<input  style = "width:90%;border: none;outline: none;" class = "" onkeyup="searchIdea(event);" value="'+ideasearchvalue+'" id = "wsideaSearch" name = "searchBox" placeholder = "Search" type="text" autocomplete="off">'
        +'<div class="searchCloseIconMainDiv"> ' 
        +'<img class="" onclick="clearIdeaSearch();" id="ClearComSearch" src="/images/conversation/delete.svg" style="width:10px;margin-top:6px;display: block;cursor: pointer;">'
        +'</div>'
        +'<div class="searchIconMainDiv" align="center">'
        +'<img src="/images/searchTwo.png" onclick="searchIdea1();" id="ideaSearch" class="mt-1" style="cursor: pointer;">'
        +'</div>'
      +'</div>'
    +'</div>	'


    $('#searchoptiondiv').append(ui).show();
  }else if(pageAct == "sprint"){
   if($('.sprintsearchSortPopUp').length==0){
      $('#searchoptiondiv').html('');
      
      ui='<div class = "sprintsearchSortPopUp searchSortPopUp" style="height: 100px;display:block;" id = "">'
      //+'<div id = "WsarrowDiv" class="workSpace_arrow_right" style="margin-top:14px;margin-right:-16px;"><img src="/images/arrow.png"></img></div>'
      +'<div id="" class="serachMainDivCls d-flex">'
        +'<select  id = "sprintFilterid" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected" onchange = "sprintFilter();"  type="company">'
          +'<option value="active" class="drpDwnOptions">Show Active Sprints</option>'
          +'<option value="notactive" class="drpDwnOptions">Show Hidden Sprints</option>'
          +'<option id="defaultTaskSort" value="all" class="drpDwnOptions">'+getValues(companyLabels,"All")+'</option>'
        +'</select>'
        +'<div class="searchIconMainDiv mt-1" align="center" style="">'
        +'<img src="/images/filter1.png" style="height: 14px;">'
        +'</div>'
      +'</div>'
      +'<div id="sortDiv" class="serachMainDivCls d-flex">'
        +'<select  id = "sprintSortid" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected" onchange = "sprintSort();"  type="company">'
          +'<option value="sort" class="drpDwnOptions">'+getValues(companyLabels,"Sort")+'</option>'
          +'<option value="Created Date" class="drpDwnOptions">'+getValues(companyLabels,"Created_Date")+'</option>'
          +'<option value="Created by" class="drpDwnOptions">'+getValues(companyLabels,"Created_by")+'</option>'
          +'<option value="Created by" class="drpDwnOptions ">'+getValues(companyLabels,"Updated_By")+'</option>'
        +'</select>'
        +'<div class="searchIconMainDiv mt-1" align="center" style="">'
        +'<img src="/images/sort2.png" style="height: 14px;">'
        +'</div>'
      +'</div>'
      +'<div class="serachMainDivCls d-flex">'
      +'<input onkeyup="searchOnSprintEnter(event,this);searchSprint();" style = "width:90%;border: none;outline: none;" class = "serachBoxInputBoxCommonCls wrkSpaceSearchActivityFeedSearchNew"  id = "sprintinputsearch" name = "searchBox" placeholder = "Search" type="text">'
        +'<div class="searchCloseIconMainDiv"> ' 
        +'<img class="serachBoxClearIcon" onclick="clearSprintSrh(this);" style="margin-top:10px !important;" id="ClearComSearch" src="/images/xforsearch.png">'
        +'</div>'
        +'<div class="searchIconMainDiv mt-1" align="center">'
        +'<img src="/images/searchTwo.png" onclick="searchSprint();" id="sprintSearch" style="height: 14px;cursor:pointer;" class="">'
        +'</div>'
      +'</div>'
    +'</div>	'

    $('#searchoptiondiv').append(ui).show();


   }else{
     $('#searchoptiondiv').toggle();
   }
    

  }else if(pageAct == "agile"){
    $('#searchoptiondiv').html('');
      
    ui='<div class = "agilesearchSortPopUp searchSortPopUp" style="display:block;height: 71px;" id = "">'
    //+'<div id = "WsarrowDiv" class="workSpace_arrow_right" style="margin-top:14px;margin-right:-16px;"><img src="/images/arrow.png"></img></div>'
    +'<div id="sortDiv" class="serachMainDivCls d-flex">'
      +'<select  id = "agileSortid" style = "width:97%;border: none;outline: none;" class = "drpDwnSelected" onchange = "agileSort();">'
        +'<option value="" class="drpDwnOptions">'+getValues(companyLabels,"Sort")+'</option>'
        +'<option value="Backlog" class="drpDwnOptions">'+getValues(companyLabels,"Sort_Backlog")+'</option>'
        +'<option value="In Progress" class="drpDwnOptions">'+getValues(companyLabels,"db_Assigned")+'</option>'
        +'<option value="Blocked" class="drpDwnOptions ">'+getValues(companyLabels,"db_Blocked")+'</option>'
        +'<option value="Hold" class="drpDwnOptions">'+getValues(companyLabels,"db_Hold")+'</option>'
        +'<option value="Done" class="drpDwnOptions">'+getValues(companyLabels,"db_Done")+'</option>'
        +'<option value="Cancel" class="drpDwnOptions">'+getValues(companyLabels,"db_Cancel")+'</option>'
      +'</select>'
      +'<div class="searchIconMainDiv" align="center" style="">'
      +'<img src="/images/sort2.png" style="height: 14px;margin-top: 5px;">'
      +'</div>'
    +'</div>'
    +'<div class="serachMainDivCls d-flex">'
    +'<input onkeyup="searchOnAgileEnter(event,this);searchAgile(event,this);" style = "width:90%;border: none;outline: none;" class = "serachBoxInputBoxCommonCls wrkSpaceSearchActivityFeedSearchNew"  id = "agileinputsearch" name = "searchBox" placeholder = "Search" type="text">'
      +'<div class="searchCloseIconMainDiv"> ' 
      +'<img class="serachBoxClearIcon" onclick="clearAgileSrh(this);" style="margin-top:10px !important;" id="ClearComSearch" src="/images/xforsearch.png">'
      +'</div>'
      +'<div class="searchIconMainDiv" align="center">'
      +'<img src="/images/searchTwo.png" onclick="searchAgileIcon(event,this);" id="agileSearch" style="height: 14px;cursor:pointer;margin-top: 5px;" class="">'
      +'</div>'
    +'</div>'
    +'</div>	'

    $('#searchoptiondiv').append(ui).show();


   }else if(globalmenu=="contacts"){
    if($('#contactssearchnsort').length==0){ 
      $('#searchoptiondiv').html('');
      ui='<div class = "searchSortPopUp d-block" style="height: 70px;" id = "contactssearchnsort">'
        +'<div class="serachMainDivCls d-flex">'
          +'<select  id = "contactsSort" style = "width:97%;border: none;outline: none;" class = "" onchange = "sortContacts();event.stopPropagation();"  type="company">'
            +'<option class="drpDwnOptions" value="" id="">'+getValues(companyLabels,"Sort")+'</option>'
            +'<option class="drpDwnOptions" value="name" id="">'+getValues(companyLabels,"name")+'</option>'
            +'<option class="drpDwnOptions" value="email" id="">'+getValues(companyLabels,"email")+'</option>'
            +'<option class="drpDwnOptions" value="ActiveUsers" id="">'+getValues(companyLabels,"Active_User")+'</option>'
            +'<option class="drpDwnOptions" value="InActiveUsers" id="">'+getValues(companyLabels,"Inactive_User")+'</option>'
          +'</select>'
          +'<div class="searchIconMainDiv" align="center" style="">'
          +'<img src="/images/sort2.png" style="height: 16px;">'
          +'</div>'
        +'</div>'
        +'<div class="serachMainDivCls d-flex">'
        +'<input  onkeyup="searchOnContactsEnter(event,this);searchContactslist(event);" style = "width:90%;border: none;outline: none;" class = ""  id = "contactSearchInput" name = "searchBox" placeholder = "Search" type="text">'
          +'<div class="searchCloseIconMainDiv"> ' 
          +'<img class="serachBoxClearIcon" onclick="clearContactSearch();" id="" src="/images/xforsearch.png">'
          +'</div>'
          +'<div class="searchIconMainDiv" align="center">'
          +'<img src="/images/searchTwo.png" onclick="searchContactslistClick(event);" id="" class="notesSearchbox">'
          +'</div>'
        +'</div>'
      +'</div>	'
      $('#searchoptiondiv').append(ui).show();
    }else{
      $('#searchoptiondiv').toggle();
    }  

   }else{
    $('#searchoptiondiv').html('');
   }


}

function searchSprint(){
  hideSprintChart();
  var value = $('#sprintinputsearch').val().toLowerCase();
  $(".sprintLists  .sprinttitle").filter(function() {
      $(this).parents('.sprintLists').toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
}


function searchOnSprintEnter(event,obj){
  var sprintSearchword = $("#sprintinputsearch").val();
  if(sprintSearchword != ''){
      $('.serachBoxClearIcon').show();
  }else{
      $('.serachBoxClearIcon').hide();
  }
}

var sprintFilterVal="";
function sprintFilter(){
  sprintFilterVal = $('#sprintFilterid').find("option:selected").attr('value');
  sprintFilterSort();
}
var sprintSortVal="";
function sprintSort(){
  sprintSortVal = $('#sprintSortid').find("option:selected").attr('value');
  sprintFilterSort();
}

function sprintFilterSort(){
  let jsonbody={
    "groupLoadType":sprintFilterVal,
    "project_id":prjid,
    "sortVal":sprintSortVal
  }
  $('#loadingBar').addClass('d-flex').removeClass('d-none');  
  $.ajax({
      url: apiPath + "/" + myk + "/v1/fetchJsonSprint",
      type: "POST",
      dataType: 'json',
      contentType: "application/json",
      data: JSON.stringify(jsonbody),
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');  
      },
      success: function (result) {
          if(sprintviewtype=="list"){
            $('#sprintContentListDiv').html('');
            $('#sprintContentListDiv').html(listSprintUI(result));
          }else{
            $('#sprintContentGridListDiv').html('');
            $('#sprintContentGridListDiv').append(listGridSprintUI(result));
          }
          

          $('#loadingBar').addClass('d-none').removeClass('d-flex');  
      
      }
  });
  hideSprintChart();
}

function clearSprintSrh(){
  $('#sprintContentListDiv').html('');
  listSprint();
  $('#sprintinputsearch').val('');
  $('.serachBoxClearIcon').hide();
}

function clearIdeaSearch(){
  $('#content').html('');
  wsIdesUi();
  listIdeas();
  $('#wsideaSearch').val('');
  ideasearchvalue="";
  ideauilist="";
}
function searchIdea1(){
  $('#content').html('');
  wsIdesUi();
  ideasearchvalue = $('#wsideaSearch').val();
  ideaSearch();
  //listIdeas(searchvalue);
  //ideaExpandAll('all','');

}
var ideasearchvalue="";
function searchIdea(event){
  ideasearchvalue = $('#wsideaSearch').val();
  if(ideasearchvalue != ''){
    $('#ClearComSearch').show();
  }else{
      $('#ClearComSearch').hide();
  }
  var code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
  if(code==13){
    $('#content').html('');
    wsIdesUi();
    ideaSearch();
    //ideaExpandAll('all','');
    //listIdeas(ideasearchvalue);
  }
}

var taskFilterVal = "";
var taskSortType = "";
var taskSearchWord = "";
function taskFilterSort(obj){
  
  taskFilterVal = $('#taskFilter').find("option:selected").attr('value');
  taskSortType = $('#taskSort').find("option:selected").attr('value');
  taskSearchWord = $("#searchBox").val();
  if(typeof(prjid)!="undefined" && prjid != ""){
    fetchTasks('','firstClick');
  }else{
    fetchmzTasks(mzTasktype,"firstClick");
  }
  
}

function searchOnTaskEnter(event,obj){
  taskSearchWord = $("#searchBox").val();
  if(taskSearchWord != ''){
      $('.serachBoxClearIcon').show();
  }else{
      $('.serachBoxClearIcon').hide();
  }
  taskSearchWord = taskSearchWord.toLowerCase();
  var code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
  if(code==13){
    if(typeof(prjid)!="undefined" && prjid != ""){
      fetchTasks('','firstClick');
    }else{
      fetchmzTasks(mzTasktype,"firstClick");
    }
  }
}
function clearWrkspaceTaskSearch(obj){
  taskSearchWord="";
  $("#searchBox").val('');
  $('#serachBoxClearIcon').hide();
  if(typeof(prjid)!="undefined" && prjid != ""){
    fetchTasks('','firstClick');
  }else{
    fetchmzTasks(mzTasktype,"firstClick");
  }
}

function closeSearch(){
  $('#searchoptiondivPopup').html('')
  $('#tzNavMenuDiv').addClass('sidebar');
}

function openNotifSearch(){
  if($('#notSearchDiv').is(':visible')){
    $('#notLabel').show();
    $('#notSearchDiv').hide();
  }else{
    $('#notLabel').hide();
    $('#notSearchDiv').show();
    $('input#searchBox1').focus();
    if(notifSearchText != ""){
      $('input#searchBox1').val(notifSearchText);
      $('#serachBoxClearIcon').show();
    }
  }
}  

function prepareNotificationUi(jsondata){
	var ui="";
  
  ui+="<div class=\"pl-3 pt-2 pb-1 notificationtitle d-flex\" style=\"color:black;border-bottom: 1px solid #e3dcdc;\">" //font-family: opensanscondbold;
        +" <p id='notLabel' class=\"mb-0\" style='font-size: 20px;'>Notifications</p>"
        +'<div id="notSearchDiv" class="border-0 pt-1" style="width:90%;padding-bottom: 2px;display:none;">'
          +'<input type="text" name="searchBox" placeholder="Search here..." onkeyup="filterNotification();event.stopPropagation();" onkeypress="searchNotification(event);event.stopPropagation();" style="width:93%;outline: none;border: none;font-size: 14px;" id="searchBox1" class="serachBoxInputBoxCommonCls Search_cLabelPlaceholder " autocomplete="off"/>'
          +'<input id="serachBoxClearIcon"  type="image" style="display:none;width: 10px;height: 10px;" class="" src="/images/xforsearch.png" title="Clear" onclick="clearsearchbox();event.stopPropagation();" >'
        +'</div>'
        +" <img class=\"ml-auto mr-3 mt-1\" src=\"images/menus/search2.svg\" id=\"notifSearchNSort\" onclick=\"openNotifSearch();\" style=\"width: 22px; height:22px; cursor: pointer;\" title=\"\">"
        +" <img class=\"ml-1 mr-2 mt-1 d-none\" src=\"images/info.png\" id=\"\" onclick=\"\" style=\"width: 15px;height:15px;cursor: pointer;\" title=\"\">"
        +"<div class=\"srhPopup w-50 p-1\" id=\"ssDiv\" style=\"display:none;position:absolute;\">"
          
        +"</div>"
      +"</div>"
      
      +"<div class=\"pl-3 pt-2 pb-1 d-flex\" style=\"color:black;font-family: opensanscondbold;\">"
        +"<ul id='notifTabs' class=\"d-flex pl-0 m-0\" style=\"width:97%;list-style: none;border-bottom: 1px solid #e3dcdc;\">"
          +"<li class=\"mr-3 pr-1 tabClass1\" id='unreadTab' onclick=\"WrkspaceTeamSort('unread');\" style=''><a data-toggle=\"tab\"></a>Unread</li>"
          +"<li class=\"mx-3 px-1 tabClass1\" id='allTab' onclick=\"WrkspaceTeamSort('all');\" style=''><a data-toggle=\"tab\"></a>All</li>"
          +"<li class=\"mx-3 px-1 tabClass1\" id='mentionedTab' onclick=\"WrkspaceTeamSort('Important');\" style=''><a data-toggle=\"tab\"></a>Mentioned</li>"
          +"<li class=\"mx-3 px-1 tabClass1\" id='senderTab' onclick=\"WrkspaceTeamSort('Sender');\" style=''><a data-toggle=\"tab\"></a>Sender</li>"
          /* +"<select>"

          +"</select>" */
        +"</ul>"
      +"</div>"
      +"<div id=\"notificationMainDiv\" class=\"pl-3 wsScrollBar\" style=\"height: calc(100vh - 90px - 70px);overflow-y:auto;overflow-x: hidden;\">"//calc(100vh - 100px - 70px)
      +"</div>"

    
  //ui+="<p class=\"w-100 mt-2 mb-0 pr-2 notfContentHover\" style=\"color: #337ab7;font-size: 14px;font-weight: bold;cursor: pointer;\" align=\"right\">More</p>"
   
  return ui;
	
 }

function notificationListDatafromJson(jsondata){
  var ui="";
  for(i = 0; i < jsondata.length ; i++){
    var notification_id = jsondata[i].notification_id;
    var notType = jsondata[i].notType;
    var createdBy = jsondata[i].createdBy;
    var createdName = jsondata[i].createdName;
    var created_timestamp = jsondata[i].created_timestamp;
    var user_image_type = jsondata[i].user_image_type;
    var mainFeed = jsondata[i].mainFeed;
    var name = jsondata[i].name;
    var prjImgType = jsondata[i].prjImgType;
    var projectid = jsondata[i].notMainId;
    var status = jsondata[i].status;
    var project_name = jsondata[i].project_name;
    var projUserStatus = jsondata[i].projUserStatus;
    var id = jsondata[i].id;
    var projArhStat = jsondata[i].project_status;

    var projectType="";
    if(projUserStatus == "PO"){
      projectType="MyProjects";
    }else{
      projectType="SharedProjects";
    } 


    var projectimage="";
    if(prjImgType == "-" || prjImgType == ""){
      projectimage = lighttpdpath+"/projectimages/dummyLogo.png";
    }else{
      projectimage = lighttpdpath+"/projectimages/"+projectid+"."+prjImgType;
    }
    
    var timestamp = new Date().getTime();
    var userimage = lighttpdpath+"/userimages/"+createdBy+"."+user_image_type+"?"+timestamp;
    
    var header="";
    if(notType == "workspace_act_feed"){
      header = mainFeed == "-" ? "Team Zone | New Conversation" : "Team Zone | Reply Conversation";
    }else if(notType == "task_user_comments"){
      if(projectid != 0){
        header = "Myzone | Task Participant Comment";
      }else{
        header = "Teamzone | Conversations | Task Participant Comment";
      }
    }else if(notType == "Wsfolder"){
      header = "Teamzone | Document | Folder share";
    }else if(notType == "cal_task"){
      if(projectid != 0){
        header = "Teamzone | Conversations | Task";
      }else{
        header = "Myzone | Task";
      }
    }else if(notType == "cal_workflow_task"){
      header = "Myzone | Task | Workflow task";
    }else if(notType == "task_comment"){
      if(projectid != 0){
        header = "Myzone | Task Participant Comment";
      }else{
        header = "Teamzone | Conversations | Task Participant Comment";
      }
    }
    
    
    
    ui+="<div id=\"notifListMain_"+notification_id+"\" class=\"pt-2 position-relative\" onmouseover=\"showMarkAsReadButton(this,"+notification_id+");\" onmouseout=\"hideMarkAsReadButton(this,"+notification_id+");\" style=\"border-bottom: 1px solid #CED2D5;float:left;width: 98%;\"> "		//"+notification_id+"		class=\""+impNotfCls+"\"
    +"	<h6 class='' style=\"font-size: 14px;color:#636565;font-family: opensanscondbold;width: 95%;\">"+header+"</h6>"
    +"	<div class=\"media mt-2 align-items-center\" style='width: 100%;'>"
    +"		<img id=\"notificationUnread_"+notification_id+"\" notifid=\""+notification_id+"\" title=\"Mark as read\" onclick=\"changeNotificationCheck(this,'landingPage','"+status+"');event.stopPropagation();\" src=\"images/selcet.png\" class=\"align-self-start mr-2 mt-2 d-none\" style=\"width:15px;cursor:pointer;\">"
    +"		<img src=\""+projectimage+"\" title=\""+project_name+"\" class=\"align-self-start mr-2 border border-dark rounded mt-1\" style=\"width:50px;height: 50px;\">"
    +"		<div class=\"media-body pl-2 w-75\">"
    +"			<p class=\"defaultExceedCls mb-0 notfContentHover\" onclick=\"notificationRedirection('"+notType+"',"+id+","+projectid+",'"+project_name+"','"+projectimage+"','"+projUserStatus+"','"+projArhStat+"','"+projectType+"',"+notification_id+",'"+status+"');\" style=\"color:black;font-size: 12px;line-height: 17px;cursor:pointer;\" title=\""+name+"\">"+name+"</p>"
    
    if(notType == "workspace_act_feed" && mainFeed != "-"){
      ui+="			<p class=\"defaultExceedCls mb-0 mt-1\" style=\"color:black;font-size: 11px;font-style: italic;\" title=\"Main:"+mainFeed+"\">Main:"+mainFeed+"</p>"
    }
    
    ui+="	  </div>"
    +"  </div>"
    +"	<div class=\"media py-3 pl-2 align-items-center\" style='width: 100%;'>"
    +"  	<img class=\"rounded-circle\" style=\"width: 30px;border: 1px solid #c6bcbc;\" src=\""+userimage+"\" title=\""+createdName+"\" onerror=\"userImageOnErrorReplace(this);\" alt=\"\">"
    +"		<div class=\"media-body pl-1 d-flex\">"
    +"    	<p class=\"my-1 defaultExceedCls w-50\" style=\"color:black;font-size: 12px;\">"+createdName+"</p>"
    +"			<p class=\"defaultExceedCls mb-0 mt-1 w-50\" align=\"right\" style=\"color:#858181;font-size: 11px;\" title=\""+created_timestamp+"\">"+created_timestamp+"</p>"
    +"		</div>"
    +"	</div>"
    if(jsondata[i].notification_history_status == "N"){
      ui+="<div id=\"markAsReadButton_"+notification_id+"\" class=\"markAsReadButton\" onclick=\"changeNotificationCheck(notificationUnread_"+notification_id+",'landingPage','"+status+"');\" style=\"position: absolute;color: #000;font-size: 12px;padding: 4px 6px;background-color: #fff;border-radius: 5px;border: 1px solid #bfbfbf;right: 2%;top:2%;cursor: pointer;display: none;\">Mark as read</div>"
    }
    ui+="</div>"
    

  }


  $('#notificationMainDiv').append(ui);
  
}

 function showMarkAsReadButton(obj,nId){
    $(obj).find('.markAsReadButton').show();
    $('#notifListMain_'+nId).css('background-color','#e6e9ec');
    $('#dNotification_'+nId).css('background-color','#e6e9ec');
 }
 function hideMarkAsReadButton(obj,nId){
  $(obj).find('.markAsReadButton').hide();
  $('#notifListMain_'+nId).css('background-color','');
  $('#dNotification_'+nId).css('background-color','#ffffff');
 }
 /* function openNotifSearchSort(){
   var ui="";
   $('#ssDiv').html('');
      
   ui="<div id=\"WsarrowDiv_\" class=\"triangle-right\" style=\"display:none;margin-top:-2px;margin-right:10px;\"></div>"
      +'<div id="searchDiv" class="serachMainDivCls d-flex" style="border: none;">'
        +'<div class="drpDwnSelected mb-1" style="width:90%;border-bottom: 1px solid #c5c5c5;border-radius: 0px;margin-left: 2px;">'
          +'<input type="text" name="searchBox" placeholder="Search" onkeyup="filterNotification();event.stopPropagation();" onkeypress="searchNotification(event);event.stopPropagation();" style="width:93%;outline: none;border: none;" id="searchBox1" class="serachBoxInputBoxCommonCls Search_cLabelPlaceholder "/>'
          +'<input id="serachBoxClearIcon"  type="image" style="" class="serachBoxClearIcon clearIcon float-right" src="/images/xforsearch.png" title="Clear" onclick="clearsearchbox();event.stopPropagation();" >'
        +'</div>'
        +'<div class="my-1 ml-1">'
          +'<input type="image" id="SearchImg" style=\"width: 13px;\" src="/images/searchTwo.png" class="wsSearchCss"  onclick="searchNotif();event.stopPropagation();" />'
        +'</div>'
			+'</div>' 

     

    $('#ssDiv').append(ui).show();
    
    if(notifSearchText != ""){
      $('input#searchBox1').val(notifSearchText);
      $('#serachBoxClearIcon').show();
    } 

 } */

function searchNotification(event){
  if($('#searchBox1').val() == '' )
      $('#serachBoxClearIcon').hide();
  else
      $('#serachBoxClearIcon').show();
  
  if(event.keyCode == 13)  // the enter key code
   {
    searchNotif();
   }
}
function filterNotification(){
  if($('#searchBox1').val() == '' ){
      $('#serachBoxClearIcon').hide();
      searchNotif();
      $('#notifSearchNSort').attr('onclick','openNotifSearch()');
  }
  else{
      $('#serachBoxClearIcon').show();
      $('#notifSearchNSort').attr('onclick','searchNotif()');

  }
}
function clearsearchbox(){
  $('#searchBox1').val("");
  $('#serachBoxClearIcon').hide();
  notifSearchText="";
  notidata('firstClick');
}


function WrkspaceTeamSort(value){
  notifFilterTab = value;
  addvalue = 6611;
  indexForNotif=0;
  notifSearchText="";
  notidata('firstClick');
}

function searchNotif(){
  notifSearchText = $('#searchBox1').val();
  if(notifSearchText == ""){
      return false;
  }
  $("#notificationMainDiv").html('');
  notidata('');
  $('input#searchBox1').val(notifSearchText);
}

 var conversationFeedId="";   //global feed id for notif redirection
 function notificationRedirection(notType,notifSourceId,projectid,prjName,prjimage,prjUserStat,prjArhStat,PrjType,notificationId,historyStatus){
    checksession();
    
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    $.ajax({
      url: apiPath+"/"+myk+"/v1/updateNotificationCheckedStatus?checkednotificationId="+notificationId+"&historyStatus="+historyStatus,
      type:"PUT",
      error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          }, 
      success:function(result){
        $('img.breadcrumImg').attr('src', '').attr('src', prjimage).attr('title', prjName);
        $('span.breadCrumProjName').text(prjName);
        from ="gSearch";
        if(notType == "workspace_act_feed"){
          index = 0;
          limitAct = 0;
          conversationFeedId=notifSourceId;
          switchZone('landingpageTOtz');
          loadSubPageContent("landingpageTOtz",projectid,prjName,prjUserStat,prjArhStat,PrjType,prjimage);
        }
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
      }
    });
      



 }

 function notidata(firstclick){
  if(typeof(notifFilterTab) == "undefined" || notifFilterTab == ""){
    notifFilterTab="unread";
  } 
  if((firstclick=='firstClick') && ($('#notificationContainer:visible').length == 0)){
    $("#notificationContainer").html('');
    indexForNotif=0;
  }
  limitForNotif=50;
  checksession();
	$('#loadingBar').addClass('d-flex');
  var localOffsetTime=getTimeOffset(new Date());
  let jsonbody = {
    "company_id":companyIdglb,
    "userId":userIdglb,
    "localoffsetTimeZone":localOffsetTime,
    "text":notifSearchText,
    "type":notifFilterTab,
    "limit":limitForNotif,
    "index":indexForNotif
  }

  $.ajax({
    url: apiPath+"/"+myk+"/v1/fetchNotification",
    type:"POST",
    dataType:'json',
    contentType:"application/json",
    data: JSON.stringify(jsonbody),
    error: function(jqXHR, textStatus, errorThrown) {
        checkError(jqXHR,textStatus,errorThrown);
        $("#loadingBar").hide();
        timerControl("");
        }, 
    success : function(result){
      
      if(firstclick=='firstClick'){
        $("#notificationContainer").html(prepareNotificationUi(result));
        notificationListDatafromJson(result);
      }else{
        notificationListDatafromJson(result);
      }

      if(notifFilterTab == "all"){
        $('#unreadTab, #mentionedTab, #senderTab, #assigntomeTab').removeClass('tabClass1active');
        $('#allTab').addClass('tabClass1active');
      }else if(notifFilterTab == "unread"){
        $('#allTab, #mentionedTab, #senderTab, #assigntomeTab').removeClass('tabClass1active');
        $('#unreadTab').addClass('tabClass1active');
      }else if(notifFilterTab == "Important"){
        $('#allTab, #unreadTab, #senderTab, #assigntomeTab').removeClass('tabClass1active');
        $('#mentionedTab').addClass('tabClass1active');
      }else if(notifFilterTab == "Sender"){
        $('#allTab, #mentionedTab, #unreadTab, #assigntomeTab').removeClass('tabClass1active');
        $('#senderTab').addClass('tabClass1active');
      }else{
        $('#allTab, #mentionedTab, #senderTab, #assigntomeTab').removeClass('tabClass1active');
        $('#unreadTab').addClass('tabClass1active');
      }  
      
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      
    
    }
  }); 

	
 }

 var addvalue = 6611; // --var with values to control onscroll in notification 
 var addvalue1 = 6611;
 function notifScroll(event){
  var a = $("#notificationContainer").height() + addvalue;
  var b =  $('#notificationMainDiv').scrollTop() + $("#notificationMainDiv").height();
  if( notificationScrollFlag && b > a){ 
    indexForNotif=limitForNotif+indexForNotif;
    notidata('');
    addvalue = addvalue+addvalue1;
    notificationScrollFlag=false;
    setTimeout( function(){
      notificationScrollFlag=true;
    },1000);
  }
}

 function notificationListing() {    
 	if($('#notificationContainer:visible').length == 0){
    addvalue = 6611;
    $('#notificationBell').removeAttr('src').attr('src','/images/menus/bellminus.svg');
    notidata('firstClick');
    $('#notificationContainer').show();     //"slide", { direction: "right" }, 1000
    $("#cmecontent").addClass("d-none");
    setTimeout(function(){
      //$('#notificationRemind').addClass('d-none').removeClass('d-block');
      //$('#notificationBell').attr('src','/images/menus/bell2.svg');
    }, 6000);
  }else{
    $('#notificationBell').removeAttr('src').attr('src','/images/menus/bellplus.svg');
    $("#notificationContainer").hide().html('');    //"slide", { direction: "right" }, 1000
  }  
 }

function showHelp(){
  var ui="";
  
  if($('#helpPopupContainer:visible').length == 0){

    ui+="<div id=\"helpPopupContainer\" class=\"m-3\" style=\"height: calc(100vh - 90px - 35px);overflow-y:auto;overflow-x: hidden;\">"
        
        +"<div class=\"d-flex border-bottom pb-2 justify-content-start align-items-center\">"
        +"<img src=\"/images/menus/helpyellow.svg\" class='ml-1' style=\"width: 28px;\">"
        +"<p style=\"color:black;font-weight: bold;\" class=\"mb-0 ml-2\">Learn, Collaborate & Get Inspired</p>"
        +"</div>"
        
        +"<div class=\"media  pb-2 pt-3 border-bottom\">"
        +"<img src=\"images/menus/help1.svg\" class=\"align-self-start mt-1 mx-2\" style=\"width:45px\">"
        +"<div class=\"media-body ml-2\">"
        +"<p class=\"mb-1\" style=\"color:black;font-size: 13px;\">From video tutorials and guides, to live demos or customer support, we’re here for you.</p>"
        +"<p class=\"mb-1\" style=\"text-decoration-line: underline;font-size: 13px;font-weight: bold;\"><a href=\"\">Get Started</a></p>"
        +"</div>"
        +"</div>"

        +"<div class=\"media pb-2 pt-3 border-bottom\">"
        +"<img src=\"images/menus/help2.svg\" class=\"align-self-start mt-1 mx-2\" style=\"width:45px\">"
        +"<div class=\"media-body ml-2\">"
        +"<p class=\"mb-0\"style=\"color:black;font-size: 14px;font-weight: bold;\">Getting Started</p>"
        +"<p class=\"mb-1\" style=\"color:black;font-size: 13px;\">Dive right in and get started with the top must-have features of colabus</p>"
        +"<p class=\"mb-1\" style=\"text-decoration-line: underline;font-size: 13px;font-weight: bold;\"><a href=\"\">Get Started</a></p>"
        +"</div>"
        +"</div>"

        +"<div class=\"media pb-2 pt-3 border-bottom\">"
        +"<img src=\"images/menus/help3.svg\" class=\"align-self-start mt-1 mx-2\" style=\"width:45px\">"
        +"<div class=\"media-body ml-2\">"
        +"<p class=\"mb-0\" style=\"color:black;font-size: 14px;font-weight: bold;\">What's New</p>"
        +"<p class=\"mb-1\" style=\"color:black;font-size: 13px;\">See our latest features and improvements added by the colabus team with our users’ feedback in mind.</p>"
        +"<p class=\"mb-1\" style=\"text-decoration-line: underline;font-size: 13px;font-weight: bold;\"><a href=\"\">Get Started</a></p>"
        +"</div>"
        +"</div>"

        +"<div class=\"media pb-2 pt-3 \">"
        +"<img src=\"\" class=\"align-self-start mt-1 mx-2 border\" style=\"width:45px;visibility:hidden;\">"
        +"<div class=\"media-body\">"
        +"<h4 style=\"color:black;\">Spread the word of <br><b>awesomeness</b></h4>"
        +"<div class=\"media mt-3\">"
        +"<img src=\"images/menus/facebook.svg\" class=\"align-self-start mr-3 mt-1\" style=\"width:48px\">"
        +"<img src=\"images/menus/linkdin.svg\" class=\"align-self-start mr-3 mt-1\" style=\"width:48px\">"
        +"<img src=\"images/menus/twitter.svg\" class=\"align-self-start mt-1\" style=\"width:48px\">"
        +"</div>"
        +"</div>"
        +"</div>"

       

      +"</div>"

      $("#helpPopup").html(ui);
      $("#cmecontent").addClass("d-none");

  }else{
      $('#helpPopup').html('');
  } 
}


function createNewWorkspace(type){
  var ui="";

  var d = new Date();
  var date = d.getDate(); var month = d.getMonth()+1; var year = d.getFullYear();
  var dayDate = (date < 10 ? '0' : '')+date; var dayMonth = (month < 10 ? '0' : '')+month;
  var todayDate = dayMonth+"-"+dayDate+"-"+year;

  $('#transparentDiv').show();

  ui='<div class="modal11 d-flex justify-content-center" id="wsOptionContainer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' 
      +'<div class="modal-dialog11" style="max-height:90%;  margin-top: 50px; margin-bottom:50px;" >'
          +'<div class="modal-content11 container px-4">' 
              +'<div class="modal-header11 pt-3" style="border-bottom: 1px solid #5e5b5b !important;">' 
                  +'<h5 id="wsPopupHeader" class="modal-title defaultExceedCls" style="color:black;font-size: 16px !important;">Create Workspace</h5>' 
                  +"<button type=\"button\" class=\"close\" onclick=\"closeCreateNewWorkspace();\" id=\"\" style=\"top: 5px;right: 10px;color: black;opacity: unset;padding: 0px;outline:none;\" data-dismiss=\"modal11\">×</button>"
              +'</div>' 
              +'<div class="modal-body11 wsScrollBar pr-2">'
              +"<p class=\"pt-2 m-0\"style=\"font-weight:bold;font-size:12px;color:#5e5b5b;\">A Workspace is where a team colloborates on a project/initiative or topic. For e.g. #Marketing, #NexGen Product #Support</p>"
              +"<div align=\"center\" class=\"row defaultScrollDiv py-3\" style=\"border-bottom: 1px solid #fff;margin: 0px;height:94%\">"
                +"<div id=\"userPicContainer\" class=\"col-xs-12 col-sm-4 px-0 py-2\" style=\"\">"
                    +"<div class=\"\">"
                        +"<div class=\"d-flex \" style=\"flex-direction: column;position:relative;\">"
                            +"<img id=\"uploadCompanyImage\" class=\"img img-responsive\" onclick=\"projImgUpload(this);\" src=\"images/workspace/edit.svg\" style=\"cursor: pointer;position: absolute;max-width: 28px;top: -9px;border-radius: 50%;left:85px;\">"
                            +"<img  id=\"hideDiv\" src=\""+lighttpdpath+"/projectimages/dummyLogo.png\" class=\"border\" style=\"width: 100px;height: 100px;\" />"
                            +'<div class="statusDiv border" style=\"width: 100px;height:10px;border-top: none !important;display:none;\" ></div>'
                            +"<img class=\"mb-4 mt-2\" src=\"images/menus/upload.svg\" onclick=\"projImgUpload(this);\" style=\"width:20px;height: 20px;cursor:pointer;align-self: flex-end;display: none;\"/>"
                        +"</div>"
                        +"<div class='d-flex mt-1' style='justify-content: left;' >"
                          +"<img id=\"unhide\" title='Visible' value='unhide' class=\"img img-responsive\" onclick=\"privacyClick2()\" src=\"images/workspace/eye1.svg\" style=\"cursor: pointer;height: 24px;width: 24px;\">"
                          // +"<img id=\"hide\" title='Hidden' class=\"img img-responsive mr-4\" onclick=\"\" src=\"images/hide.png\" style=\"cursor: pointer;height: 19px;width: 19px;\">"
                        +"</div>"
                        +"<div class=\"mt-2 d-flex\" style=\"flex-direction: column;\">"
                          +"<div class='position-relative d-flex'>"
                            +"<img id='privacyImg' src=\"/images/workspace/pvt.svg\" onclick=\"privacyClick();\" onmouseover='showPopUp()' onmouseout='hidePopUp()' value=\"private\" style=\"cursor:pointer;height: 19px;width:82px;margin-bottom: 3px;\">"
                              +"<div id='pripubPopUp' class=\"rounded d-none flex-column text-align-center position-absolute font-weight-bold\" style=\"border: 1px solid #c1c5c8;background-color: #fff;top: -13px;padding: 6px;margin: 0px auto;font-size: 12px;text-align: left;left: 75px;min-width: 110px;width: 150px;z-index: 1;\">"
                                +"<span id=\"priv\" style=\"color:#999999;font-size:9px;\">Private - By invitation only <br> Public - Subscribe to be invited</span>"
                                //+"<span id=\"pub\" style=\"color:#999999;font-size:9px;\">Public- Subscribe to be invited</span>"
                              +"</div>"
                          +"</div>"
                          //+"<p class=\"\" style=\"font-size:10px;font-weight: bold;color: #5e5b5b;text-align:left;margin-bottom: 0px;\">Private is only visible to members. Public will require users to subscribe to be granted access</p>"
                            
                        +"</div>"
                        +"<div class=\"mt-2 d-flex\" style=\"flex-direction: column;\">"
                          +"<div class='position-relative d-flex'>"
                            +"<img id='privacyImg1' src=\"/images/workspace/act.svg\" onclick=\"privacyClick1();\" onmouseover='showPopUp1()' onmouseout='hidePopUp1()' value=\"Y\" style=\"cursor:pointer;height: 19px;width:82px;margin-bottom: 3px;\">"
                            +"<div id='pripubPopUp1' class=\"rounded d-none flex-column text-align-center position-absolute font-weight-bold\" style=\"border: 1px solid #c1c5c8;background-color: #fff;top: -13px;padding: 6px;margin: 0px auto;font-size: 12px;text-align: left;left: 75px;min-width: 110px;width: 150px;z-index: 1;\">"
                              +"<span id=\"acti\" style=\"color:#999999;font-size:9px;\">Active - Modifications allowed <br> Archive - No modifications allowed. (Read only mode) </span>"
                              /* +"<span id=\"arch\" style=\"color:#999999;font-size:9px;\">Archive- No modifications</span>"
                              +"<span id=\"allo\" style=\"color:#999999;font-size:9px;\">allowed. (Read only mode)</span>" */
                            +"</div>"
                          +"</div>"
                        +"</div>" 
                        +"<div class=\"d-flex mt-3\" style=\"flex-direction: column;\">"
                          +"<div class=\"d-flex align-items-start justify-content-start mb-2\">"
                            +"<label style=\"float: left;font-size: 12px;font-weight: bold;color: #5e5b5b;\"  class=\"mt-1 mb-0\" title=\"\">Start</label>"
                            +"<input type=\"text\" class=\"mx-2 hasDatePicker startsAt border-0\" placeholder=\"\" id=\"startDate\"  onclick=\"setSens('endDate', 'max');\" style=\"height: 24px;outline:none;font-size: 12px;border-bottom: 1px solid #b4adad !important;float: none;overflow: hidden;text-align: center;\" value='' autocomplete=\"off\">"
                            +"<img style=\"width:21px;\" id='startClend' class=\"mt-1 ml-1\" src='images/landingpage/cal.svg' />"
                          +"</div>"
                          +"<div class=\"d-flex align-items-start justify-content-start mb-2\">"
                            +"<label style=\"float: left;font-size: 12px;font-weight: bold;color: #5e5b5b;padding-right: 2px;\"  class=\"mt-1 mb-0\" title=\"\">End</label>"
                            +"<input type=\"text\" class=\"hasDatePicker endsAt border-0 mx-2\" placeholder=\"\" id=\"endDate\" onclick=\"setSens('startDate', 'min');\" style=\"height: 24px;outline:none;font-size: 12px;border-bottom: 1px solid #b4adad !important;float: none;overflow: hidden;text-align: center;\" value='' autocomplete=\"off\">"
                            +"<img style=\"width:21px;\" id='endClend' class=\"mt-1 ml-1\" src='images/landingpage/cal.svg' />"
                          +"</div>"
                        +"</div>"
                        +"<div class=\"mt-2 d-flex\">"
                          +"<input type=\"checkbox\" class=\"\" id=\"emailAttachmentCheckVal\" style=\"height:17px;margin-right: 8px;margin-top: 2px;\">"
                          +"<p style=\"font-size:12px;font-weight: bold;color: #5e5b5b;text-align:left;\">Save email attachments to the folder</p>"
                        +"</div>"
                      +"</div>"
                    +"</div>"
                    +"<div id=\"usersDetailsDiv1\" class=\"col-xs-12 col-sm-8 profScreen pr-0 pl-4 pb-2 pt-1\" style=\"\">"
                      +"<div style='width: 100%;height: 100%;'>"
                        +"<div class=\"\"><label class=\"m-0\" style=\"float: left;font-size:12px;color:#b4adad;\">Project Name</label><input type=\"text\" id=\"projName\" placeholder=\"\" onkeyup=\"autofillProjectTagName(this);\" style=\"font-size:12px;outline:none;border-bottom:1px solid #b4adad !important;border-radius:0 !important;box-shadow: none !important;\" class=\"form-control validate border-0 py-0\"></div>"
                        +"<div class=\"mt-3 position-relative\"><label class=\"m-0\" style=\"float: left;font-size:12px;color:#b4adad;\">Project Code</label><label class=\"pt-1\" align=\"center\" style=\"position: absolute;font-size: 16px;left: 0%;top:10px;width:10px;\">#</label><input type=\"text\" id=\"projTagName\" placeholder=\"\" onkeyup=\"enterprojectTagName(this)\" class=\"form-control validate  border-0 py-0\" style=\"padding-left: 15px;font-size:12px;outline:none;border-bottom:1px solid #b4adad !important;border-radius:0 !important;box-shadow: none !important;\"></div>"
                        +"<div class=\"mt-3\" style='height:100px;'><textarea type=\"text\" placeholder=\"Description\" id=\"comment1\" class=\"md-textarea form-control\" style=\"font-size:14px;height: 110px;\" rows=\"3\"></textarea></div>"
                        
                        
                        
                        +"<div class=\"d-flex mt-4 slectMembers\" style=\"\">"
                          +"<p style=\"font-size: 12px;font-weight: bold;color: #5e5b5b;margin-bottom: 2px;\">Team Members</p>"
                          +"<img class=\"ml-1\" id=\"plusPop\" src=\"images/task/plus.svg\" onclick=\"inviteUserListUi();\" style=\"width:20px;height: 20px;cursor:pointer;\"/>"
                        +"</div>"
                        +"<div class=\"inviteProjUserDiv w-100 mt-1\" placeholder=\"\">"
                            +"<div id=\"userList\" class='h-100'>"
                              +"<ul class=\"m-0 p-0 h-100 wsScrollBar\" style=\" list-style-type : none;overflow-y: auto;\"></ul>"
                            +"</div>"
                        +"</div>"
                      +"</div>"
                    +"</div>"
              +"</div>"  
              
              
              
              
              
              +'</div>' //body 
              +'<div id="wsSaveUpdateDiv" class="modal-footer11 pb-3 pr-2">'
                +"<div id=\"changeDynamic\" onclick=\"createNewWorkspaceProject('','');\" style=\" width: 70px;float: right;\" class=\"ml-2 mt-0 createBtn SAVE_cLabelHtml\">Save</div>"
                +"<div id=\"cancelOption\" onclick=\"closeCreateNewWorkspace();\" style=\" width: 70px;float: right;\" class=\"createBtn mt-0 mx-2\">Cancel</div>"
                if(type == "updateWSProject"){
                  ui+="<div id=\"advanceSettingsOptionid\" onclick=\"showAdv();wstabs('#tab01',1);\" style=\"float: right;margin-top: 15px;cursor: pointer;text-decoration: underline;font-size: 14px;color: #015782;text-decoration-color: #0091da;\" class=\"mr-4 mt-1\"><img src=\"images/workspace/advance.svg\" style=\"width: 18px;height: 18px;margin-right: 2px;\" class=\"\">Advanced Settings</div>"   
                }
              ui+="</div>" //footer
          +'</div>' //content
      +'</div>' //dialog
    +'</div>' //modal
  

  /* ui="<div class=\"newWorkSpaceCreate\" id=\"wsOptionContainer\" style=\"display:block;\">"
  +"<div class=\"row h-100 mainBox\" id=\"\">"
    +"<div class=\"modal-content hModalContent h-100\" style=''>"
      +"<div class=\"modal-header closebtttn\" style=' padding: 10px 5px;'>"
        +"<button type=\"button\" class=\"close\" onclick=\"closeCreateNewWorkspace();\" id=\"\" style=\"color: black;opacity: unset;padding: 8px 0px 0px 0px;outline:none;\" data-dismiss=\"modal\">×</button>"
      +"</div>"
      +"<div class=\"modal-body pt-0\" style='overflow-y:auto;'>"
        +"<div class=\"row\" style='margin: 0px;'>"
          +"<div class=\"col-sm-12\">"
            +"<div class=\"row\">"
              +"<div class=\"col-sm-12 h-100\" id=\"hListDiv\" style=''>"
                +"<div  class=\"header1\" style=\"margin-left: 16px;\">"
                    +"<h6 id=\"wsPopupHeader\" class=\"\" style=\"font-weight:bold;font-size:20px;color: #5e5b5b;border-bottom: 1px solid black;line-height: 30px;\">Create New Workspace</h6>"
                    +"<p style=\"font-weight:bold;font-size:12px;color:#5e5b5b;\">A Workspace is where a team colloborates on a project/ initiative or topic. For e.g. #Marketing, #NexGen Product #Support</p>"
                +"</div>"
                +"<div align=\"center\" class=\"row defaultScrollDiv\" style=\"border-bottom: 1px solid #fff;margin: 0px;padding: 4px 0px;\">"
                  +"<div id=\"userPicContainer\" class=\"col-xs-12 col-sm-4\" style=\"padding-top: 10px;\">"
                  +"<div class=\"\">"
                      +"<div class=\"d-flex \" style=\"flex-direction: column;position:relative;\">"
                          +"<img id=\"uploadCompanyImage\" class=\"img img-responsive\" onclick=\"projImgUpload(this);\" src=\"images/edit.png\" style=\"cursor: pointer;position: absolute;max-width: 28px;top: -9px;border-radius: 50%;right: 79px;\">"
                          +"<img  id=\"hideDiv\" src=\""+lighttpdpath+"/projectimages/dummyLogo.png\" class=\"border rounded\" style=\"width: 60%;height: 60%;\" />"
                          +"<img class=\"mb-4 mt-2\" src=\"images/menus/upload.svg\" onclick=\"projImgUpload(this);\" style=\"width:20px;height: 20px;cursor:pointer;align-self: flex-end;display: none;\"/>"
                      +"</div>"
                      +"<div class='d-flex mt-2' style='justify-content: center;' >"
                        +"<img id=\"unhide\" title='Visible' value='unhide' class=\"img img-responsive\" onclick=\"privacyClick2()\" src=\"images/view.png\" style=\"cursor: pointer;height: 20px;width: 20px;\">"
                        // +"<img id=\"hide\" title='Hidden' class=\"img img-responsive mr-4\" onclick=\"\" src=\"images/hide.png\" style=\"cursor: pointer;height: 19px;width: 19px;\">"
                      +"</div>"
                      +"<div class=\"mt-2 d-flex\" style=\"flex-direction: column;\">"
                        +"<div class='position-relative d-flex'>"
                          +"<img id='privacyImg' src=\"/images/workspace/pvt.svg\" onclick=\"privacyClick();\" onmouseover='showPopUp()' onmouseout='hidePopUp()' value=\"Private\" style=\"cursor:pointer;height: 18px;margin-bottom: 3px;\">"
                            +"<div id='pripubPopUp' class=\"rounded d-none flex-column text-align-center position-absolute font-weight-bold\" style=\"border: 1px solid #c1c5c8;background-color: #fff;top: -13px;padding: 6px;margin: 0px auto;font-size: 12px;text-align: left;left: 53px;min-width: 110px;z-index: 1;\">"
                              +"<span id=\"priv\" style=\"color:#999999;font-size:9px;\">Private- By invitation only</span>"
                              +"<span id=\"pub\" style=\"color:#999999;font-size:9px;\">Public- Subscribe to be invited</span>"
                            +"</div>"
                        +"</div>"
                        //+"<p class=\"\" style=\"font-size:10px;font-weight: bold;color: #5e5b5b;text-align:left;margin-bottom: 0px;\">Private is only visible to members. Public will require users to subscribe to be granted access</p>"
                          
                      +"</div>"
                      +"<div class=\"mt-2 d-flex\" style=\"flex-direction: column;\">"
                        +"<div class='position-relative d-flex'>"
                          +"<img id='privacyImg1' src=\"/images/workspace/act.svg\" onclick=\"privacyClick1();\" onmouseover='showPopUp1()' onmouseout='hidePopUp1()' value=\"Y\" style=\"cursor:pointer;height: 18px;margin-bottom: 3px;\">"
                          +"<div id='pripubPopUp1' class=\"rounded d-none flex-column text-align-center position-absolute font-weight-bold\" style=\"border: 1px solid #c1c5c8;background-color: #fff;top: -13px;padding: 6px;margin: 0px auto;font-size: 12px;text-align: left;left: 53px;min-width: 110px;z-index: 1;\">"
                            +"<span id=\"acti\" style=\"color:#999999;font-size:9px;\">Active- Modifications allowed</span>"
                            +"<span id=\"arch\" style=\"color:#999999;font-size:9px;\">Archive- No modifications</span>"
                            +"<span id=\"allo\" style=\"color:#999999;font-size:9px;\">allowed. (Read only mode)</span>"
                          +"</div>"
                        +"</div>"
                      +"</div>" 
                      +"<div class=\"d-flex mt-3\" style=\"flex-direction: column;\">"
                        +"<div class=\"d-flex align-items-start justify-content-between mb-2\">"
                          +"<label style=\"float: left;font-size: 14px;font-weight: bold;color: #5e5b5b;\"  class=\"mt-1 mb-0\" title=\"\">Start</label>"
                          +"<input type=\"text\" class=\"ml-2 hasDatePicker startsAt border-0\" placeholder=\"\" id=\"startDate\"  onclick=\"setSens('endDate', 'max');\" style=\"height: 24px;outline:none;font-size: 12px;border-bottom: 1px solid #b4adad !important;float: none;overflow: hidden;\" value='' autocomplete=\"off\">"
                          +"<img style=\"width:18px;\" id='startClend' class=\"mt-1\" src='images/landingpage/cal.svg' />"
                        +"</div>"
                        +"<div class=\"d-flex align-items-start justify-content-between mb-2\">"
                          +"<label style=\"float: left;font-size: 14px;font-weight: bold;color: #5e5b5b;\"  class=\"mt-1 mb-0\" title=\"\">End</label>"
                          +"<input type=\"text\" class=\"hasDatePicker endsAt border-0\" placeholder=\"\" id=\"endDate\" onclick=\"setSens('startDate', 'min');\" style=\"height: 24px;outline:none;font-size: 12px;border-bottom: 1px solid #b4adad !important;margin-left: 14px;float: none;overflow: hidden;\" value='' autocomplete=\"off\">"
                          +"<img style=\"width:18px;\" id='endClend' class=\"mt-1\" src='images/landingpage/cal.svg' />"
                        +"</div>"
                      +"</div>"
                      +"<div class=\"mt-2 d-flex\">"
                        +"<input type=\"checkbox\" class=\"\" id=\"emailAttachmentCheckVal\" style=\"height:17px;margin-right: 8px;margin-top: 2px;\">"
                        +"<p style=\"font-size:12px;font-weight: bold;color: #5e5b5b;text-align:left;\">Save email attachments to the folder</p>"
                      +"</div>"
                    +"</div>"
                  +"</div>"
                  +"<div id=\"usersDetailsDiv1\" class=\"col-xs-12 col-sm-8 profScreen\" style=\"padding: 6px 0px 0px 15px; \">"
                    +"<div style='width: 100%;height: 100%;'>"
                      +"<div class=\"mt-1\"><label class=\"m-0\" style=\"float: left;font-size:14px;color:#b4adad;\">Project Name</label><input type=\"text\" id=\"projName\" placeholder=\"\" onkeyup=\"autofillProjectTagName(this);\" style=\"font-size:14px;outline:none;border-bottom:1px solid #b4adad !important;border-radius:0 !important;box-shadow: none !important;\" class=\"form-control validate border-0 py-0\"></div>"
                      +"<div class=\"mt-3 position-relative\"><label class=\"m-0\" style=\"float: left;font-size:14px;color:#b4adad;\">Project Code</label><label class=\"pt-1\" align=\"center\" style=\"position: absolute;font-size: 16px;left: 0%;top:15px;width:10px;\">#</label><input type=\"text\" id=\"projTagName\" placeholder=\"\" onkeyup=\"enterprojectTagName(this)\" class=\"form-control validate  border-0 py-0\" style=\"padding-left: 15px;font-size:14px;outline:none;border-bottom:1px solid #b4adad !important;border-radius:0 !important;box-shadow: none !important;\"></div>"
                      +"<div class=\"mt-3\"><textarea type=\"text\" placeholder=\"Description\" id=\"comment1\" class=\"md-textarea form-control\" style=\"font-size:14px;height: 110px;\" rows=\"3\"></textarea></div>"
                      
                      
                      
                      +"<div class=\"d-flex mt-4 slectMembers\" style=\"\">"
                        +"<p style=\"font-size: 13px;font-weight: bold;color: #5e5b5b;margin-bottom: 2px;\">Team Members</p>"
                        +"<img class=\"ml-1\" id=\"plusPop\" src=\"images/menus/invite-user.svg\" onclick=\"inviteUserListUi();\" style=\"width:20px;height: 20px;cursor:pointer;\"/>"
                      +"</div>"
                      +"<div class=\"inviteProjUserDiv w-100 mt-1\" placeholder=\"\">"
                          +"<div id=\"userList\" class='h-100'>"
                            +"<ul class=\"m-0 p-0 h-100\" style=\" list-style-type : none;overflow-y: auto;\"></ul>"
                          +"</div>"
                      +"</div>"
                    +"</div>"
                  +"</div>"
                +"</div>"
                +"<div align=\"center\" class=\"row\" style=\"margin: 0px;display: flex;justify-content: flex-end;\">"
                  +"<div class=\"col-xs-12 saveUpdateDiv\" style=\"\">"
                  +"<div id=\"changeDynamic\" onclick=\"createNewWorkspaceProject('','');\" style=\" width: 100px;float: right;\" class=\"ml-2 createBtn SAVE_cLabelHtml\">Save</div>"
                  +"<div id=\"\" onclick=\"closeCreateNewWorkspace();\" style=\" width: 100px;float: right;\" class=\"createBtn \">Cancel</div>"
                  if(type == "updateWSProject"){
                    ui+="<div id=\"\" onclick=\"showAdv();wstabs('#tab01',1);\" style=\"width: 100px;float: right;margin-top: 15px;cursor: pointer;text-decoration: underline;\" class=\"mr-2\"><img src=\"images/workspace/advance.svg\" style=\"width: 18px;height: 18px;margin-right: 2px;\" class=\"\">Advanced</div>"
                  }
                  ui+="</div>"
                +"</div>"
              +"</div>" */

  $('#newWSUiPopup').append(ui).show();
  
  ckeditor();
  $('.ck').css("font-size", "14px");
  $('.ck-placeholder').css("height", "75px").addClass('m-0');
  
  if(type == "updateWSProject"){
    showAdvancedOptions();
  }
  initProjectCalender();
  
}



function showAdv(){
  $('#wsOptionContainer').addClass('d-none').removeClass('d-flex');
  $('#advOptionContainer').addClass('d-flex').removeClass('d-none');
}

function showAdvancedOptions(){
  var ui="";

  ui='<div class="modal12 d-none justify-content-center" id="advOptionContainer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' 
      +'<div class="modal-dialog11" style="max-height:90%;  margin-top: 50px; margin-bottom:50px;" >'
        +'<div class="modal-content11 container px-4">' 
          +'<div class="modal-header11 pt-3">' 
          +"<button type=\"button\" class=\"close\" onclick=\"closeAdvOptions('workspace');\" id=\"cancel1\" style=\"color: black;opacity: unset;top:5px;right:10px;outline:none;\" data-dismiss=\"modal\">×</button>"
          +'<div class="d-flex justify-content-between align-items-center border-0" style="width: 100%;height: 32px;border-bottom: 1px solid #5e5b5b !important;">'
            +'<div class="d-flex" id="tabAdvHeader">'
              +'<img src="images/arrowLeft.png" class="m-1" style="width: 10px;height: 20px;display:none;">'
              +'<img src="images/arrow.png" class="m-1" style="width: 10px;height: 20px;display:none;">'
              +'<img src="images/workspace/dots.svg" id="cancel2" class="m-1" onclick="closeAdvOptions();" style="width: 15px;height: 20px;cursor:pointer;">'
              +'<h6 class="defaultExceedCls p-1 mb-0" style="color: black;font-size: 16px !important;margin-bottom:0px !important;">Advanced settings</h6>'
            +'</div>'
            +'<div class="tabs1 px-1" onclick="wstabs(this,1)" id="tab01">'
              +'<h6 class="defaultExceedCls p-1 mb-0" style="font-size: 16px !important;">Users</h6>'
            +'</div>'
            +'<div class="tabs1 px-1 " onclick="wstabs(this,2)" id="tab02">'
              +'<h6 class="defaultExceedCls p-1 mb-0" style="font-size: 16px !important;">Status</h6>'
            +'</div>'
            +'<div class="tabs1 px-1" id="tab03" onclick="wstabs(this,3)">'
              +'<h6 class="defaultExceedCls p-1 mb-0" style="font-size: 16px !important;">Templates</h6>'
            +'</div>'
            +'<div class="tabs1 px-1" id="tab04" onclick="wstabs(this,4)">'
              +'<h6 class="defaultExceedCls p-1 mb-0" style="font-size: 16px !important;">Task Codes</h6>'
            +'</div>'
            +'<div class="tabs1 px-1" id="tab05" onclick="wstabs(this,5)">'
              +'<h6 class="defaultExceedCls p-1 mb-0" style="font-size: 16px !important;">Integration</h6>'
            +'</div>'
          +'</div>'
        +"</div>"
        +"<div class=\"modal-body11 wsScrollBar p-0 mt-1\" style='overflow-y:auto;'>"
            
        +'<fieldset id="tab011" class="pt-2">'
          +'<div class="pl-4">'
              +'<h6 class="m-0" style="color: black;font-weight: bold;">Users</h6>'
              +'<div class="mt-2" style="width: 97%;">'
                +'<p class="my-1" style="color: #464242;font-weight: bold;font-size: 12px;">Pending Users</p>'
                +'<div id="pendingContentDiv" class="wsScrollBar" style="height:110px;overflow-y: auto;border: 1px solid #bcb5b5;border-radius: 10px;">'


                +'</div>'
              +'</div>'
              +'<div class="mt-3" style="width: 97%;">'
                +'<div class="d-flex">'
                  +'<p class="my-1" style="color: #464242;;font-weight: bold;font-size: 12px;">Team Members</p>'
                  +'<img class="ml-1 my-1" id="plusPop" src="images/task/plus.svg" onclick="inviteUserListUiAdv();" style="position:relative;width:20px;height: 20px;cursor:pointer;">'
                  +'<div class=\"slectMembersAdv\" style=\"position:absolute;left:190px;\"></div>'
                  +'<p class="my-1 ml-auto mr-2" style="color: #464242;;font-weight: bold;font-size: 12px;display:none;">Default/Alphabetical</p>'
                  +'<img src="images/workspace/defualt.svg" style="width: 15px;display:none;">'
                +'</div>'  
                +'<div id="teamContentDiv" class="" style="height:250px;overflow-y: auto;border: 1px solid #bcb5b5;border-radius: 10px;">'
                  +"<ul class=\"m-0 p-0 h-100 wsScrollBar\" style=\" list-style-type : none;overflow-y: auto;\"></ul>"
                    

                +'</div>'
              +'</div>'
              
          +'</div>'
          +'<div class="px-3">'
              


          +'</div>'

          +"<div align=\"center\" class=\"mt-3 mb-2 mr-4\" style=\"margin: 0px;display: flex;justify-content: flex-end;\">"
            +"<div class=\"col-xs-12 saveUpdateDiv\" style=\"/*padding: 10px 4px 15px;*/\">"
              +"<div id=\"advUpdate1\" onclick=\"\" style=\" width: 100px;float: right;\" class=\"ml-2 createBtn SAVE_cLabelHtml\">Update</div>"
              +"<div id=\"cancel3\" onclick=\"closeAdvOptions('workspace');\" style=\" width: 100px;float: right;\" class=\"createBtn mx-2\">Cancel</div>"
            +"</div>"
          +"</div>"
        +'</fieldset>'
        +'<fieldset class="show pt-2 pr-3" id="tab021">'
            +'<div class="pl-4">'
                +'<h6 class="m-0" style="color: black;font-weight: bold;">Status</h6>'
                +'<div class="row mt-2">'
                  +'<div class="col-xs-12 col-sm-2" style="display:none;">'
                    +'<img id="statusPagePrjimg" src="https://testbeta.colabus.com:1933///projectimages/dummyLogo.png" class="border" style="width:90px;height:80px;border-bottom:none !important;">'
                    +'<div class="statusDiv border" style=\"width: 90px;height:10px;border-top:none !important;\" ></div>'
                  +'</div>'
                  +'<div class="col-xs-12 col-sm-10">'
                    +'<div id="statusDaterange" class="align-items-center" style="display:flex;">'
                      //+'<p class=\"defaultExceedCls m-0 \" style=\"float: left;font-size: 12px;font-weight: bold;color: #232222;\">Date Range</p>'
                      +"<div class=\"mb-2 mr-2\" style=\"\">"
                          +"<p class=\"defaultExceedCls m-0 mt-2\" style=\"float: left;font-size: 12px;font-weight: bold;color: #7f7c7c;\"  class=\"\" title=\"\">Start Date</p>"
                          +"<input type=\"text\" class=\"ml-2 hasDatePicker startsAt border-0\" placeholder=\"\" id=\"startDateWSStatus\"  onclick=\"setSens('endDateWSStatus', 'max');\" style=\"height: 20px;outline:none;font-size: 12px;border-bottom: 1px solid #b4adad !important;float: none;overflow: hidden;width: 75px;\" value='' autocomplete=\"off\">"
                          +"<img style=\"width:18px;\" id='startClendWSStatus' class=\"ml-2\" src='images/landingpage/cal.svg' />"
                      +"</div>"
                      +"<div class=\"mb-2 mx-2\" style=\"\">"
                          +"<p class=\"defaultExceedCls m-0 mt-2\" style=\"float: left;font-size: 12px;font-weight: bold;color: #7f7c7c;\"  class=\"\" title=\"\">End Date</p>"
                          +"<input type=\"text\" class=\"hasDatePicker endsAt border-0\" placeholder=\"\" id=\"endDateWSStatus\" onclick=\"setSens('startDateWSStatus', 'min');\" style=\"height: 20px;outline:none;font-size: 12px;border-bottom: 1px solid #b4adad !important;margin-left: 14px;float: none;overflow: hidden;width: 75px;\" value='' autocomplete=\"off\">"
                          +"<img style=\"width:18px;\" id='endClendWSStatus' class=\"ml-2\" src='images/landingpage/cal.svg' />"
                      +"</div>"
                    +"</div>" 
                    
                  +'</div>'
                +'</div>'
            +'</div>'
            +'<div class="pt-2 pl-4">'
              +'<div id="graphDiv" class="rounded" style="border: 1px solid black;height:150px;width: 100%;">'
                +'<canvas class="mt-2 pr-1" id="chartCanvas" style=\"width:100%;height:100%;overflow-x:auto;\"></canvas>'
                //+"<img style=\"width:100%;height:100%\" id='' class=\"\" src='images/temp/statusgraph.png' />"
              +'</div>'
            +'</div>'  
            +'<div class="pt-3 pl-4">'
              +'<div class="d-flex" id="statusTextareadiv" style="border:none;height:75px;width:100%;">'
                +'<div id="statusColorcodediv" title="Status"  style="width:2%;">'

                +'</div>'
                
                +'<div style="width:98%;height: 100%;">'
                  +'<textarea id="statCmtText" class="rounded-right" placeholder="Enter project status text here......" style="outline:none;font-size: 12px;height: 100%;width:100%;border: 1px solid #ccc2c2;resize:none;"></textarea>'
                +'</div>'
                +'<input id="commentStatusValue" value="" type="hidden"/>'

              +'</div>'
              +'<div class="d-flex justify-content-between mt-1" style="height: 25px;">'
                +'<div class="convoSubIcons p-1">'
                  +"<img id='colorCodeButton' class=\"\" title='Select status' onclick='openColorcodes(this);' src=\"/images/workspace/colorcodeworkspace_status.svg\" title=\"Post\" onclick=\"postStatusComment();\" style=\"width:30px;height: 25px;cursor:pointer;position:relative;margin-left: -10px;\">"
                +'</div>'
                +'<div class="convoSubIcons p-1">'  
                  +"<img class=\"\" src=\"/images/conversation/post.svg\" title=\"Post\" onclick=\"postStatusComment();\" style=\"width:20px;cursor:pointer;float: right;\">"
                +'</div>'
              +'</div>'
              +'<div id="statusColorcode" onclick="" style="position:absolute;display:none;left:50px !important;top: 220px;z-index:100;">'

              +'</div>'
              +'<div id="statusCommentDiv" class="py-2 mt-1" style="">' 
                      

                      
                      
              
              
              +'</div>'
                
               
            +'</div>'
        +'</fieldset>' 
        
        +'<fieldset class="show" id="tab031">'
            +'<div class="pl-5">'
            +'</div>'
            +'<div class="px-3">'
              +"<img style=\"width: 100%;height: 70vh;\" id='' class=\"ml-2 mb-3\" src='images/temp/Screenshot 2021-07-16 at 6.48.52 PM.png' />"
            +'</div>'
        +'</fieldset>' 

        +'<fieldset class="show pl-4 pr-3 pt-3" id="tab041">'
          +'<div id="tCodeMainDiv">'
            +'<div class="" style="height:110px;border-bottom:1px solid #bebebe;">'
                +'<h6 class="m-0" style="color: black;font-weight: bold;font-size: 14px;">Task Codes</h6>'

                +"<div class='my-2' >"
                  + "<textarea id='taskQuote' class=\"pb-0 pt-2 px-2 rounded wsScrollBar\" style='resize: none;height: 50px;width: 100%;float: left;border: 1px solid #bebebe;font-size: 12px;outline:none;'></textarea>"
                +"</div>"
                +"<div class='my-2' >"
                  + "<div id='codeLib' align=\"center\" class=\"p-1 convoSubIcons\"  style='height: 35px;float: left;'>"
                    +	"<img class=\"img-responsive mt-0\" title='Library' src= \"/images/library.png\" onclick=\"openTcodeLibrary();\" style=\"cursor: pointer;width: 25px;height: 25px;\" >"
                  +"</div>"
                  + "<div id='postTcode' align='center' class='p-1 convoSubIcons' style='height: 35px;float: right;'>"
                    +	"<img class=\"img-responsive mt-0\" onclick='postTaskCode();' title='Post' src=\"images/conversation/post.svg\" style=\"width: 20px;cursor:pointer;height: 25px;\">"
                  + "</div>"
                +"</div>"
            +'</div>'
            
            +'<div id="taskCompletionCodeDiv" class="mb-2 wsScrollBar" style="height:340px;overflow-y: scroll;">'
            
            +'</div>'
          +'</div>'
          
          +'<div id="tCodeLibraryDiv" class="mb-2" style="display:none;">'
            +'<div class="" style="height:25px;">'
              +'<span class="m-0" style="color: black;font-weight: bold;font-size: 14px;border-bottom:3px solid #569aaf;">TASK COMPLETION CODES</span>'
            +'</div>'
            +'<div id="tCodeLibraryListDiv" class="mt-2 wsScrollBar" style="height: 400px;overflow: auto;">'
            
            +'</div>'
            +"<div align=\"center\" class=\"mt-3 mx-2\" style=\"margin: 0px;display: flex;justify-content: flex-end;\">"
              +"<div class=\"col-xs-12 saveUpdateDiv\" style=\"/*padding: 10px 4px 15px;*/\">"
                +"<div id=\"\" onclick=\"checkTCodes();\" style=\"\" class=\"ml-2 createBtn\">Add</div>"
                +"<div id=\"\" onclick=\"backToTaskCodes();\" style=\"\" class=\"createBtn mx-2\">Back</div>"
              +"</div>"
            +"</div>"
          +'</div>'
        +'</fieldset>'


        +'<fieldset class="show" id="tab051">'
            +'<div class="pl-5">'
                //+'<h6 class="m-0" style="color: black;font-weight: bold;">Integration</h6>'
                
            +'</div>'
            +'<div class="px-3">'
                +"<img style=\"width: 100%;height: 70vh;\" id='' class=\"ml-2 mb-3\" src='images/temp/Screenshot 2021-07-16 at 6.49.18 PM.png' />"
            +'</div>'
        +'</fieldset>'
        +'<input type="hidden" id="settingProjectId">'
        +"</div>"

        

    +"</div>"
  +"</div>"
  



  $('#newWSUiPopup').append(ui).show();

}

function wstabs(obj,tabnumber){

  $(obj).addClass("active1");
  
  
  current_fs = $(".active1");
  
  next_fs = $(obj).attr('id');
  next_fs = "#" + next_fs + "1";
  
  $("fieldset").removeClass("show");
  $(next_fs).addClass("show");
  
  current_fs.animate({}, {
    step: function() {
      current_fs.css({
        'display': 'none',
        'position': 'relative'
      });
      next_fs.css({
        'display': 'block'
      });
    }
  });

  if(tabnumber==1){
    $('#tab02, #tab03, #tab04, #tab05').removeClass('active1');
    $('#tab01').addClass('active1');
  }else if(tabnumber==2){
    $('#tab01, #tab03, #tab04, #tab05').removeClass('active1');
    $('#tab02').addClass('active1');
  }else if(tabnumber==3){
    $('#tab01, #tab02, #tab04, #tab05').removeClass('active1');
    $('#tab03').addClass('active1');
  }else if(tabnumber==4){
    $('#tab01, #tab02, #tab03, #tab05').removeClass('active1');
    $('#tab04').addClass('active1');
  }else if(tabnumber==5){
    $('#tab01, #tab02, #tab03, #tab04').removeClass('active1');
    $('#tab05').addClass('active1');
  }



}

function openColorcodes(obj){
  if($('#statusColorcode').is(':hidden')){
    $('#statusColorcode').show();
  }else{
    $('#statusColorcode').hide();
  }
  

}

function closeAdvOptions(place){
  if(place == "workspace" || place=="undefined"){
    
    $('#wsOptionContainer').addClass('d-flex').removeClass('d-none');
    $('#advOptionContainer').addClass('d-none').removeClass('d-flex');
  }else{
    $('#advOptionContainer').hide();
    $("#transparentDiv").hide();
    $("#newWSUiPopup").html('');
    $('#loadingBar').addClass('d-none').removeClass('d-flex');
  }
  
}


function hidePopUp() {
  $('#pripubPopUp').addClass('d-none').removeClass('d-flex');
}

function showPopUp() {
  $('#pripubPopUp').addClass('d-flex').removeClass('d-none');
}
function hidePopUp1() {
  $('#pripubPopUp1').addClass('d-none').removeClass('d-flex');
}

function showPopUp1() {
  $('#pripubPopUp1').addClass('d-flex').removeClass('d-none');
}

var myEditorData = "";
function ckeditor() {
  ClassicEditor.create( document.querySelector( '#comment1' ) )
                .then( editor => {
                    console.log( editor );
                    myEditorData = editor;
                } )
                .catch( error => {
                    console.error( error );
                } );
}

function openInvUserSearch(){
  $('.invUsers').hide();
  $('#inviteUsers').show().focus();
  $('#inviteUsersAdv').show().focus();
}

function inviteUserListUi(){
  $('#inviteUserpopU').remove();
  var Ui="";

  Ui="<div class=\"position-absolute border border-secondary p-3 inviteUserpopUp ml-2 rounded\" id=\"inviteUserpopU\" style=\"z-index: 100;width: 290px;height: 235px;left: 130px;bottom: 5px;background-color: white;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;\">" //<input type='text' style='border-radius: 0px;border-bottom:0px;' id=\"inviteUsers\"  placeholder=\"Invite Members\" class=\"projInviteUser form-control validate\">
    +'<div class="d-flex justify-content-between" style="">'
      +"<div class='pb-1 invUsers'  style='font-size: 14px;font-weight: normal;'>Invite Users</div>"
      +'<div class="serachimage d-flex" onclick="openInvUserSearch();">'
        +'<input type="text" id="inviteUsers" placeholder=\"Invite Members\" class="projInviteUser border-0" style="display:none;width: 240px;border-bottom:1px solid #6c757d !important;">'
        +'<img class="searchImage" src="/images/menus/search2.svg" style="height:20px;width:20px; cursor:pointer;"/>'
      +'</div>'
    +'</div>'
    +"<div id=\"userforProj\" class=\"projUserListMainnewWS border-0 wsScrollBar\" style=\"display: block;overflow-y: auto;overflow-x: hidden;\">"
    +"<ul class=\"projuserList\"></ul>"
    +"</div></div>"
    fetchUsers();
    $('.slectMembers').append(Ui).show();
    
}

function inviteUserListUiAdv(){
  $('.slectMembersAdv').html('');
  var Ui="";

  Ui="<div class=\"position-absolute border border-secondary p-3 inviteUserpopUp ml-2 rounded\" id=\"inviteUserpopUAdv\" style=\"z-index: 100;width: 290px;height: 235px;left: -50px;background-color: white;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;\">" //<input type='text' style='border-radius: 0px;border-bottom:0px;' id=\"inviteUsers\"  placeholder=\"Invite Members\" class=\"projInviteUser form-control validate\">
    +'<div class="d-flex justify-content-between" style="">'
      +"<div class='pb-1 invUsers' style='font-size: 14px;font-weight: normal;'>Invite Users</div>"
      +'<div class="serachimage d-flex" onclick="openInvUserSearch();">'
        +'<input type="text" id="inviteUsersAdv" placeholder=\"Invite Members\" class="projInviteUser border-0" style="display:none;width: 240px;border-bottom:1px solid #6c757d !important;">'
        +'<img class="searchImage" src="/images/menus/search2.svg" style="height:20px;width:20px; cursor:pointer;"/>'
      +'</div>'
    +'</div>'
    +"<div id=\"userforProj\" class=\"projUserListMainnewWS border-0 wsScrollBar\" style=\"display: block;overflow-y: auto;overflow-x: hidden;\">"
        +"<ul class=\"projuserList\"></ul>"
    +"</div>"
    +'</div>'  
    fetchUsers();
    $('.slectMembersAdv').append(Ui).show();
    
}

var myCalendar;var myCalendarWSStatus;
function initProjectCalender() {
     myCalendar = new dhtmlXCalendarObject([{input:"startDate",button:"startClend"},{input:"endDate",button:"endClend"}]);
     myCalendarWSStatus = new dhtmlXCalendarObject([{input:"startDateWSStatus",button:"startClendWSStatus"},{input:"endDateWSStatus",button:"endClendWSStatus"}]);
     var today = new Date();
     myCalendar.setSensitiveRange(today, null);
     myCalendar.setDateFormat("%Y-%m-%d");
     myCalendarWSStatus.setSensitiveRange(today, null);
     myCalendarWSStatus.setDateFormat("%Y-%m-%d");
     $('.dhtmlxcalendar_material').removeAttr("style");
     $('.dhtmlxcalendar_material').css({'border-radius':'5px','border':'2px solid #BFBFBF','background-color':'#FFFFFF','top':'163px','left': '379px','z-index':'3'});
     myCalendar.hideTime();
     myCalendarWSStatus.hideTime();
 }
function setSens(id, k) {
       if (k == "min"){ 
          myCalendar.setSensitiveRange($("input#"+id).val(),null);
          myCalendarWSStatus.setSensitiveRange($("input#"+id).val(),null);
       }else {
         if(byId(id).value){
           myCalendar.setSensitiveRange(new Date(), null);
           myCalendarWSStatus.setSensitiveRange(new Date(), null);
         }else{
         myCalendar.setSensitiveRange(new Date(), null);
         myCalendarWSStatus.setSensitiveRange(new Date(), null);
         }
      }
}
function byId(id) {
  return document.getElementById(id);
}

function changeType(obj){
    $(obj).attr('type','date');
}

function projImgUpload(obj){
	if($(obj).parent().find('div.attachListOptions').length == 0 ){
		
		var ui='<div id="wsprofileUploadOption" class="attachListOptions position-absolute rounded p-1 border d-block" style="width: 140px;background-color: #fff;font-size: 11px;border-color: #a2a5a7 !important;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);left: 125px;top: -13px;z-index: 100;">'
            +'<div style="float:right;margin-left:-19px;margin-top:9px;z-index: 100;position: absolute;display:none;">'
              +'<img src="images/arrowLeft.png" style="float:left;">'
            +'</div>'
            +'<div class="d-flex p-1 justify-content-start" style="height: 24px; cursor:pointer;">'
            +'  <form enctype="multipart/form-data" style="top:-100px;" method="post" name="projectImageUpload" id="projectImageUpload">'
            +'  <input type="file" class="Upload_cLabelTitle" title="Upload" value="" onchange="readURL(this);" name="prjImgUpload" id="prjImgUpload" hidden></form>'
            +' 	<img class="img-fluid" src="images/conversation/computer.svg" style="width:20px;" onclick="openFiles()" id="mainWSUpload">'
				    +'	<span class="pl-2" style="white-space: nowrap;" onclick="openFiles()">From Computer</span>'
            //+'	<span class="pl-2" onclick="openFiles()">Upload From System</span>'
            +'</div>'
            +'<div class="d-flex p-1 justify-content-start" style="height: 24px;">'
            +' 	<img class="img-fluid" style="width:20px;" src="images/conversation/folder.svg">'
				    +'	<span class="pl-2" style="white-space: nowrap;" >From Repository</span>'
            //+'	<span class="pl-2">Upload From Gallery</span>'
            +'</div>'
          +'</div>'
		$(obj).parent().append(ui);
			
	}else{
		$('div.attachListOptions').remove();
	}
}

function openFiles(){
  document.getElementById('prjImgUpload').click();
}

function closeCreateNewWorkspace(){
    $("#transparentDiv").hide();
    $("#newWSUiPopup").html('');
}

function privacyClick(){
  if($('#privacyImg').attr('value') == 'private'){
    $('#privacyImg').attr('src','images/workspace/pub.svg').attr('value','public'); 
  }else{
    $('#privacyImg').attr('src','images/workspace/pvt.svg').attr('value','private');
  }
}
function privacyClick1(){
  if($('#privacyImg1').attr('value') == 'Y'){
    $('#privacyImg1').attr('src','images/workspace/arc.svg').attr('value',"A"); 
  }else{
    $('#privacyImg1').attr('src','images/workspace/act.svg').attr('value',"Y");
  }
}

function privacyClick2(){
  if($('#unhide').attr('value') == 'unhide'){
    $('#unhide').attr('src','images/workspace/eye2.svg').attr('value',"hide").attr('title','Hidden'); 
  }else{
    $('#unhide').attr('src','images/workspace/eye1.svg').attr('value',"unhide").attr('title','Visible');
  }
}

function fetchUsers(){
	  
  $.ajax({
    
      url: apiPath+"/"+myk+"/v1/fetchParticipants?user_id="+userIdglb+"&company_id="+companyIdglb+"&contact_permission=1",
      type:"GET",
      error: function(jqXHR, textStatus, errorThrown) {
                  checkError(jqXHR,textStatus,errorThrown);
                  $("#loadingBar").hide();
          
      }, 
      success:function(result){
      
         userName = result;

         var name = '';
         var UI ='';
         var id = '';
         var email = '';
         for(var i= 0; i< userName.length;i++){
           
          name = userName[i].user_full_name;
          id = userName[i].user_id;
          email = userName[i].user_email1;
          let imgName = lighttpdpath+"/userimages/"+userName[i].user_image+"?"+d.getTime();
      
        if(userIdglb != id)  
        UI+="<li class='listOfUser my-1' style ='color: black; display:block;cursor:pointer;border-bottom: 0px solid #cccccc;display: flex;align-items: center;' id='"+id+"' email='"+email+"' onclick=\"addusers(this,'"+userName[i].user_image+"','"+email+"');removeUserFromList(this,"+id+");\"><img id=\"personimagedisplay\"  src=\""+imgName+"\" title='"+name+"' onerror='userImageOnErrorReplace(this);' class=\"mr-3 rounded-circle listImgProf\" style=\"\">"+name+"</li>";
         }
         
         $('#userforProj ul').html(UI);
         selectUsers();
         selectUsersAdv();
       }
  }); 
}

function removeUserFromList(obj,selectedId){
 $('#'+selectedId).hide();
}

function selectUsers(){
  var val = '';
  var val1 = '';
  $('#inviteUsers').on('keyup',function(){
      var txt = $('#inviteUsers').val().toLowerCase();
  //   if(txt.length>0){
    //     $('#userforProj').show();
    //  }else{
    //     $('#userforProj').show();
    //  } 
      $('#userforProj li').each(function(){   // This code will show only names which are relevant or match will the search key word
           val = $(this).text().toLowerCase();
           val1 = $(this).text().toLowerCase();
         if(val1.indexOf(txt)> -1 || val.indexOf(txt)!= -1)
             {
           $(this).show();
         }else{
           $(this).hide();
         }
     });			
     
      $('#userList li').each(function(){      
       var id =  $(this).attr('id').split('_')[1];
       $('#userforProj ul').find('#'+id).hide();
      }); 
   });	

}

function selectUsersAdv(){
  var val = '';
  var val1 = '';
  $('#inviteUsersAdv').on('keyup',function(){
      var txt = $('#inviteUsersAdv').val().toLowerCase();
  //   if(txt.length>0){
    //     $('#userforProj').show();
    //  }else{
    //     $('#userforProj').show();
    //  } 
      $('#userforProj li').each(function(){   // This code will show only names which are relevant or match will the search key word
           val = $(this).text().toLowerCase();
           val1 = $(this).text().toLowerCase();
         if(val1.indexOf(txt)> -1 || val.indexOf(txt)!= -1)
             {
           $(this).show();
         }else{
           $(this).hide();
         }
     });			
     
      $('#teamContentDiv li').each(function(){      
       var id =  $(this).attr('id').split('_')[1];
       $('#userforProj ul').find('#'+id).hide();
      }); 
   });	

}

function addusers(obj,imgName,email){
	
  var id = $(obj).attr('id');
  var name = $(obj).text();
  let img = lighttpdpath+"/userimages/"+imgName+"?"+d.getTime();

  var UI  = "<li id='user_"+id+"' updateStatus='insert' class='prjUserClsNew userStat_"+id+"' onmouseover='addBgcolor("+id+")' onmouseout='removeBgcolor("+id+")' userrole='' email='"+email+"' name='"+name+"' style='display: flex;justify-content: space-between;height:43px !important;width: 100%;border-bottom: 1px solid #cdc4c4;background-color: #fff;border-top-left-radius: 10px;border-top-right-radius: 10px;color: #464242;' >"
         +"<div class=\"d-flex pt-1\" style='text-overflow: ellipsis;overflow: hidden;white-space: nowrap;'><img id=\"personimagedisplay\"  src=\""+img+"\" title='"+name+"' onerror='userImageOnErrorReplace(this);' class=\"mr-3 rounded-circle profImg\" style=\"\"><span class='profNam ml-1 pt-1' style='font-size: 12px;'>"+name+"</span></div>"
         +"<div class=\"pt-1\" id=\"roleSection\" style='display: flex;justify-content: space-between;'>"
                  +"<div class='dropDow' style=''><select name=\"role\" class='selectOp' id='role_"+id+"' onchange='' style='font-size: 12px;margin-top: 3px;color: #5e5b5b;'>"
                      // +"<option value=\"role\">role</option>"
                    +"<option id='ow_' value=\"PO\">Owner</option>"
                    +"<option id='tm_' value=\"TM\" selected>Team Member</option>"
                    +"<option id='ob_' value=\"OB\">Observer</option>"
                  +"</select></div>"
                  +"<div class='closeOp' style='width: auto;'><img onclick='removeUser("+id+")' class='closeAp' src='/images/task/minus.svg' style='width: 15px; height: 15px; cursor: pointer;'></div>"
            +"</div>"
         +"</li>";
  
  // $('#userforProj').hide();
  $('#userList ul').append(UI);
  $('#teamContentDiv ul').append(UI);
  $('#inviteUsers').val('');
}

function addBgcolor(id,place){
  if(place == "taskcode"){
    $('#wsCode_'+id).css('background-color','#cceeff');
    $('#taskCodeOptionsDiv_'+id).addClass('d-flex').removeClass('d-none');
  }else{
    $('.userStat_'+id).css('background-color','#cceeff');
    $('#Teamuser_'+id).css('background-color','#cceeff');
  }
  
 }
 function removeBgcolor(id,place){
  if(place == "taskcode"){
    $('#wsCode_'+id).css('background-color','');
    $('#taskCodeOptionsDiv_'+id).addClass('d-none').removeClass('d-flex');
  }else{
    $('.userStat_'+id).css('background-color','');
    $('#Teamuser_'+id).css('background-color','');
  }
}

function removeUser(id){
  $('#userList ul').children('#user_'+id).remove();
  $('#userforProj ul').children('#'+id).show();
}

function removeUserUpdate(id,wsprojectID,role,advancePage){
  
  var count=0;
  var uId="";
  var value="";
  if(advancePage == "advancePage"){
    $('#teamContentDiv li').each(function(){

      uId = $(this).attr('id').split('_')[1];
      if(uId != id){
        value = $(this).find('.dropDow').find('.selectOp').val();
        if(value == "PO"){
          count++;
        }
      }
      
    });
  }else{
    $('#userList li').each(function(){

      uId = $(this).attr('id').split('_')[1];
      if(uId != id){
        value = $(this).find('.dropDow').find('.selectOp').val();
        if(value == "PO"){
          count++;
        }
      }
      
    });
  }
  
  if(count == 0){
    alertFun("There must be one project admin for a project", 'warning');
  }else{
    
    $('#userList ul').children('#user_'+id).remove();
    $('#teamContentDiv ul').children('#user_'+id).remove();
    $('#userforProj ul').children('#'+id).show();

   let jsonbody = {
      "user_id" : userIdglb,
      "project_id" : wsprojectID,
      "proj_user_status" : role,
      "delUserId" : id
    }


    $.ajax({
      url: apiPath+"/"+myk+"/v1/deleteUsersFromProject",
      type: "PUT", 
      contentType:"application/json",
      data: JSON.stringify(jsonbody),
      error: function(jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR,textStatus,errorThrown);
            
      },  
      success:function(data) {



      }
    });   
  }

   

}

/* function wsTaskUi(type){
  let ui = "";
  type=typeof(type)=='undefined'?'':type;
  
  var paddingleft0 = "";var projectpl="";
  var taskNameWidth = "";var typepl="";
  var taskTypeWidth = "";let pt="pl-1";let st="";let tpt="pl-1";
  if(typeof(prjid) == "undefined" || prjid == ""){
    paddingleft0="";
    taskNameWidth="width:41%";
    taskTypeWidth="width:10%";
    projectpl="pl-4";typepl="pl-3";
    pt="pl-2";
    tpt="ml-1 pl-2"
   
  }else{
    paddingleft0="pl-0";
    taskNameWidth="width:51%";
    taskTypeWidth="width:8%";
    projectpl="";typepl="";
    st="margin-left: 3px;margin-right: -6px;"
  }
  
  ui +=
    "<div class='TaskDiv' style=' overflow-x:hidden'>" +
    "<div class='d-flex px-2 py-1 min-vw-100 vw-100 headerBar'> " +
    "<div id='headerProjectTask' class=' headerTaskPrj' style='width:8%;display:none;'><span class=\""+projectpl+"\">Project</span></div>" +
    "<div id='headertaskType' class='"+paddingleft0+" headerTaskPrj pl-3' style='"+taskTypeWidth+";'><span class=\""+typepl+"\">Type</span></div>" +
    "<div class='headerTaskPrj "+pt+"' style='width:8%;"+st+"'> ID</div>" +
    "<div id='headerTaskname' class='types "+tpt+"' style='"+taskNameWidth+"' > Task</div>" +
    "<div class='pl-1 types' style='width:13%'> Start</div>" +
    "<div class='types' style='width:13%'> Due</div>" +
    "<div class='types'> Estimate</div>" +
    "<div class='types'> Actual</div>" +
    "<div class='types' style='width:8%'> Priority</div>" +
    "<div class='types' style='width:8%'> Status</div>" +
    "<div class='types' style='width:8%'> Trend</div>" +
    "</div>"+
    "<div class='pt-2 ' id='taskList' style='font-size: 14px'>  </div>" +
    "</div>";

  
  if(type!=''){
    return ui;
  }
  $("#content").html(ui);

  if(typeof(prjid) == "undefined" || prjid == ""){
    $('#headerProjectTask').show();
  }

}
 */

/* function wsTaskUi(type){
  let ui = "";
  type=typeof(type)=='undefined'?'':type;

  var paddingleft0 = "";var projectpl="";
  var taskNameWidth = "";var typepl="";
  var taskTypeWidth = "";let pt="pl-1";let st="";let tpt="pl-1";
  if(typeof(prjid) == "undefined" || prjid == ""){
    paddingleft0="";
    taskNameWidth="width:41%";
    taskTypeWidth="width:10%";
    projectpl="pl-4";typepl="pl-3";
    pt="pl-2";
    tpt="ml-1 pl-2"
   
  }else{
    paddingleft0="pl-0";
    taskNameWidth="width:51%";
    taskTypeWidth="width:8%";
    projectpl="";typepl="";
    st="margin-left: 3px;margin-right: -6px;"
  }

  ui +=
  "<div class='TaskDiv' style=' overflow-x:hidden'>" +
  "<div class='d-flex px-2 py-1 headerBar w-100'> " +
  
  "<div class='defaultExceedCls' id='headerProjectTask' style='display:none;'>Project</div>"+
  "<div class='defaultExceedCls' id='headertaskType' style='width: 6%;'>Type</div>"+
  "<div class='defaultExceedCls' id='headertaskgroup' style='width: 5%;'>Group</div>"+
  "<div class='defaultExceedCls' style='width: 7%;'>Members</div>"+
  "<div class='defaultExceedCls' style='width: 6%;'>ID</div>"+
  "<div class='defaultExceedCls' id='headerTaskname' style='width: 24%;'>Task</div>"+
  "<div class='defaultExceedCls' style='width: 9%;'>Start</div>"+
  "<div class='defaultExceedCls' style='width: 9%;'>Due</div>"+
  "<div class='defaultExceedCls' style='width: 8%;'>Estimate</div>"+
  "<div class='defaultExceedCls' style='width: 8%;'>Actual</div>"+
  "<div class='defaultExceedCls' style='width: 6%;'>Priority</div>"+
  "<div class='defaultExceedCls' style='width: 6%;'>Status</div>"+
  "<div class='defaultExceedCls' style='width: 6%;'>Trend</div>"+


  "</div>"+
  "<div class='' id='taskList' style='font-size: 14px'>  </div>" +
  "</div>";



  if(type!=''){
    return ui;
  }
  $("#content").html(ui);

  if(typeof(prjid) == "undefined" || prjid == ""){
    $('#headerProjectTask').show().css('width','5%');
    $('#headerTaskname').show().css('width','19% !important');
  }

} */

function wsTaskUi(type){
  let ui = "";
  type=typeof(type)=='undefined'?'':type;

  var paddingleft0 = "";var projectpl="";
  var taskNameWidth = "";var typepl="";
  var taskTypeWidth = "";let pt="pl-1";let st="";let tpt="pl-1";
  if(typeof(prjid) == "undefined" || prjid == ""){
    paddingleft0="";
    taskNameWidth="width:41%";
    taskTypeWidth="width:10%";
    projectpl="pl-4";typepl="pl-3";
    pt="pl-2";
    tpt="ml-1 pl-2"
   
  }else{
    paddingleft0="pl-0";
    taskNameWidth="width:51%";
    taskTypeWidth="width:8%";
    projectpl="";typepl="";
    st="margin-left: 3px;margin-right: -6px;"
  }

  ui +=
  "<div class='TaskDiv' style=' '>" 
    +'<div class="" id="addTaskHolderDiv" style="font-size: 12px"></div>'
    +"<div class='d-flex py-1 headerBar w-100' id='headerTask' style='z-index: 1;'> " +
    
    "<div class='defaultExceedCls ' id='headerProjectTask' style='display:none;text-align: center;'>Project</div>"+
    "<div class='defaultExceedCls pl-2' id='headertaskType' style='width: 5%;'>Type</div>"+
    "<div class='defaultExceedCls' id='headertaskgroup' style='width: 5%;'>Group</div>"+
    "<div class='defaultExceedCls' id='headertaskgroup' style='width: 7%;'>Assignee</div>"+
    "<div class='defaultExceedCls' style='width: 5%;'>ID</div>"+
    "<div class='defaultExceedCls' id='headerTaskname' style='width: 34%;'>Task</div>"+
    //"<div class='defaultExceedCls' style='width: 9%;'>Start</div>"+
    "<div class='defaultExceedCls' style='width: 9%;'>Due</div>"+
    "<div class='defaultExceedCls' style='width: 8%;'>Estimate</div>"+
    "<div class='defaultExceedCls pl-1' style='width: 6%;'>Actual</div>"+
    "<div class='defaultExceedCls' style='width: 6%;text-align: center;'>Priority</div>"+
    "<div class='defaultExceedCls' style='width: 6%;text-align: center;'>Status</div>"+
    "<div class='defaultExceedCls' style='width: 6%;text-align: center;'>Trend</div>"+
    "<div class='defaultExceedCls' style='width: 3%;text-align: center;'></div>"+


    "</div>"
  
    + "<div class='' id='taskList' style='font-size: 14px'>  </div>" +
  "</div>"
  +'<div class="calendar-2" style="display:none;"></div>'



  if(type!=''){
    return ui;
  }
  $("#content").html(ui);

  if(typeof(prjid) == "undefined" || prjid == ""){
    $('#headerProjectTask').show().css('width','5%');
    ///$('#headerTaskname').show().css('width','26% !important');
  }

}

function calculateElapseTime(elapsedTime){
  //console.log("elapsedTime:"+elapsedTime);
 
  var elapTotalTime="";
  var elapHour = "";
  var elapMin = "";
  var elapSec = "";
  if(elapsedTime !="" || elapsedTime !="-" || elapsedTime !="00:00:00"){
    elapHour = parseInt(elapsedTime.split(":")[0]);
    elapMin = parseInt(elapsedTime.split(":")[1]);
    elapSec = parseInt(elapsedTime.split(":")[2]);
    
    //console.log("elapHour:"+elapHour+" elapMin:"+elapMin+" elapSec:"+elapSec);
    if(elapHour == 0){
      if(elapMin ==0){
        elapTotalTime = (elapSec ==0 ? "1 s ago" : parseInt(elapSec) +" s"+" ago");
      }else{
        elapTotalTime = parseInt(elapMin) +" m"+" ago";
      }
    }else if(elapHour < 24){
      if(elapMin==0){
        elapTotalTime = (elapHour==0 ? parseInt(elapHour)+1 +" h"+" ago" : (parseInt(elapHour)) +" h"+" ago");
      }else if(elapMin>=30){
        //var newMin=(elapHour/60)+elapMin;
        //newMin=newMin%60;
        //if(newMin>30){
          //elapTotalTime=parseInt(elapHour)+1 +" h"+" ago"
        //}
        elapTotalTime=parseInt(elapHour)+1 +" h"+" ago"
      }
      else if(elapMin<30){
        elapTotalTime = (elapHour==0 ? parseInt(elapHour)+1 +" h"+" ago" : (parseInt(elapHour)) +" h"+" ago");
      }
    }else if(parseInt(elapHour) >=24){
      var  elapDay= parseInt(elapHour)/24;
      elapDay = Math.floor(elapDay);
      if(elapDay==1)
        elapTotalTime = elapDay +" d"+" ago";
      else
        elapTotalTime = elapDay +" d"+" ago";
    }
  }else{
    elapTotalTime = "1 s ago";
  }
  //console.log("ElapTotalTime:"+elapTotalTime);
  return elapTotalTime;
}

function wsIdesUi(){
  var ui="";
  ui="<div id='ideaDivHeader' class=''>"
  //+"<p class='m-0 pl-3'>Ideas Module</p>"
  +"</div>"
  +"<div id='idealist'>"
    +"<div id='ideadrag' class='easy-tree'>"
    +"<ul id='idealistUL_0' class='mytree1 mytree p-0 sub-ul' level=1 style=''>"
    +"</ul>"
    +"</div>"
  +"</div>"
  $("#content").html(ui);

}

function isValidURL(url){
  var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
  if(RegExp.test(url)){
      return true;
  }else{
      return false;
  }
}


function colorGridView(id,place){
  var ui="";

  ui="<div class='px-2 py-1 rounded' style='display:flex;cursor:pointer;border: 1px solid #C1C5C8;flex-wrap: wrap;justify-content: center;'>"  
    +"<div class='d-flex colorboxdiv' style='flex-wrap: wrap;width: 97%;'>"
      +"<div class='rounded colorbox' nodecolor='#f6c2d9' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#f6c2d9;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#fff69b' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#fff69b;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#bcdfca' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#bcdfca;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#a1c8ea' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#a1c8ea;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#e4dae2' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#e4dae2;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    // +"</div>"
    // +"<div class='p-1 d-flex' style=''>"
      +"<div class='rounded colorbox' nodecolor='#ff7eb9' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#ff7eb9;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#ff65a3' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#ff65a3;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#7afcff' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#7afcff;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#feff9c' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#feff9c;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#fff740' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#fff740;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    // +"</div>"
    // +"<div class='p-1 d-flex' style=''>"
      +"<div class='rounded colorbox' nodecolor='#f39a4e' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#f39a4e;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#eb6092' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#eb6092;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#4ab6d9' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#4ab6d9;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#abcc51' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#abcc51;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#f9c847' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#f9c847;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    // +"</div>"
    // +"<div class='p-1 d-flex' style=''>"
      +"<div class='rounded colorbox' nodecolor='#ffd938' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#ffd938;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#90909a' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#90909a;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#d6d4df' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#d6d4df;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#b3cce2' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#b3cce2;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#1dace6' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#1dace6;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    // +"</div>"
    // +"<div class='p-1 d-flex' style=''>"
      +"<div class='rounded colorbox' nodecolor='#73cac5' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#73cac5;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#e3e546' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#e3e546;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#f2788f' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#f2788f;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#f69dbb' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#f69dbb;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
      +"<div class='rounded colorbox' nodecolor='#fbad4b' onmouseover='hilightColor(this);' onmouseout='normalImg(this)' onclick='changenodecolorNew(this);event.stopPropagation();' style='width: 25px;height: 25px;background-color:#fbad4b;position: relative;margin: 2px;'><img class='colortickimage' src='images/temp/tick.png' style='display:none;position: absolute;width: 15px;height: 20px;'></div>"
    // +"</div>"
    /* +"<div class='p-1 d-flex justify-content-end' style=''>"
      +"<div onclick='' style=''><img src='/images/task/remove.svg' style='width: 15px;height: 15px;' /></div>"
      +"<div onclick='' style=''><img src='/images/task/tick.svg' style='width: 15px;height: 15px;' /></div>"
    +"</div>" */
    
    +"</div>"
  +"</div>"

  if(place == "ideaEdit" || place == "ideaCreate"){
    if(typeof(id)=="undefined" || id==""){
      id=0;
    }
    
    $('#colorblock_'+id).append(ui);
    
    $('.colorboxdiv').css('width','');
    $('.colorbox').removeAttr('onmouseover');
    $('.colorbox').removeAttr('onmouseout');
    $('.colorbox').attr('onclick','changenodecolor(this,'+id+',"'+place+'");');
  }else if(place == "sprintplace"){
    $('#colorBox').append(ui);
    $('.colorboxdiv').css('width','');
    $('.colorbox').removeAttr('onmouseover');
    $('.colorbox').removeAttr('onmouseout');
    $('.colorbox').attr('onclick','changesprintgroupcolor(this);event.stopPropagation();');
  }else{
    $('#colorBoxNew').append(ui);
  }  

}

// $(document).on("mouseover", ".abcd", function() {
//   console.log( "Hi!"); // jQuery 1.4.3+

//   $(this).css("border", "2px solid grey");
// });
// var mouseOver = true;
function changenodecolorNew(ab){
  var colorCode = $(ab).attr("nodecolor");
  // $(ab).removeAttr('onmouseout');
  $(ab).css("border", "2px solid grey");
  $(".minicolors-swatch-color").css("background-color", "none");
  $(".minicolors-swatch-color").css("background-color", colorCode);
  $("#group_color").val(colorCode);
  $(".colorbox").css("border", "");
  $(ab).css("border", "2px solid grey");
  $("#group_color").removeClass("minicolors-input");
  $(".minicolors-panel").hide();
  // $('.colorboxdiv').children('div').each(function () {
  //   $(this).css("border", "none");
  // });
  // $(ab).css("border", "2px solid grey");
  // mouseOver = false;
}

function hilightColor(ab){
  // if(mouseOver == true){
    // $(ab).css("border", "2px solid grey");
  // }

}
function normalImg(ab){
  // if(mouseOver == true){
    // $(ab).css("border", "none");
  // }
}

function wsAgileUi(){
  var ui="";
  
  ui="<div id='agileDiv'>"
  /* +"<div class='agileHeader d-flex'>"
    +"<div id='orgAgileHeader' class='pl-3 py-1' style='width:25%;'>"
      +"<img src='/images/agile/org_white.svg' class='image-fluid mr-2' title='Organized' style='height: 16px;'>"
      +"<span class='d-none'>Organized</span>"
    +"</div>"
    +"<div class='d-none' style='border-left: 1px solid #AAAAAA;width:1%;box-shadow:rgb(0 0 0 / 18%) 0px 2px 6px;'></div>"
    +"<div id='unorgAgileHeader' class='pl-3 py-1' style='width:74%;'>"
      +"<img src='/images/agile/unorg_white.svg' class='image-fluid mr-2' title='Unorganized' style='height: 16px;'>"
      +"<span class='d-none'>Unorganized</span>"
    +"</div>"
  +"</div>" */
  +"<div id='agile' class='d-flex'>"
    +"<div id='organizedAgile' class='m-2 easy-tree1 rounded' style='width:25%;background-color: #f1e4e6;box-shadow:0 6px 12px rgb(0 0 0 / 18%);'>"
      
      +"<ul id='agilelistUL_0' class='mytree pl-3 pt-2 position-relative' style='z-index:1;'>"
        +"<div class='d-flex align-items-center mr-3 rounded py-1 orgheader' style='background-color: #eeced4;'><img src='/images/agile/org_blue.svg' class='image-fluid mx-2' title='Organized' style='width:20px;height: 20px;'><span class='defaultExceedCls' style='font-size:12px;color:black;'>Organized</span></div>"
      +"</ul>"
    +"</div>"
    +"<div id='vline' class='position-relative' style='width:2px;margin-top: 15px;'>"   //border-left: 1px solid #DEE2E6;
      +"<img class='' id='agileArrowright' title='Expand' src='images/agile/splitter.svg' onclick='expandEpics();' style='position:fixed;width:25px;height:25px;margin-left:-13px;z-index:10;cursor:pointer;'>"
      +"<img class='' id='agileArrowleft' title='Collapse' src='images/agile/splitter.svg' onclick='collapseEpics();' style='display:none;position:fixed;width:25px;height:25px;margin-left:-13px;z-index:10;cursor:pointer;'>"
    +"</div>"
    +"<div id='unorganizedAgile' class='m-2 rounded' style='background-color: #f7efe1;position:relative;width:74%;box-shadow:0 6px 12px rgb(0 0 0 / 18%);'>"
    +"<div class='d-flex align-items-center mx-3 mt-2 rounded py-1 unorgheader' style='background-color: #f2d8a9;'><img src='/images/agile/unorg_blue.svg' class='image-fluid mx-2' title='Unorganized' style='width:20px;height: 20px;'><span class='defaultExceedCls' style='font-size:12px;color:black;'>Unorganized</span></div>"
    +"</div>"
  +"</div>"
  +"</div>"
  


  $("#content").html(ui);

  

}

function showOption(){
  var ui="";
  var menu = globalmenu;
  if(globalmenu=="Idea"){
    if($('#optionsId').length == 0){
      ui='<div class="position-absolute rounded border px-2 py-1" id="optionsId" style="cursor:pointer; width: 125px;background-color: white;color: black; font-size: 12px; box-shadow: rgba(0, 0, 0, 0.18) 0px 6px 12px; display: block; right: 15px; top: 3px;">'
        +"<div class=\"d-flex justify-content-start\" id=\"ideaExpandCollapse\" onclick=\"ideaExpandAll('all','');event.stopPropagation();\">"
          +'<img class="class="img-fluid" src="images/idea/expand_all2.svg" style="width:20px;height:20px;">'
          +'<span class="pl-2">Expand All</span>'
        +'</div>'
      +'</div>'

      $('#mainOptions').append(ui);
    }else{
      hideGlboption(menu);
    }
  }else if(globalmenu=="sprint"){
    if($('#optionsId').length == 0){
      ui='<div class="position-absolute rounded border px-0 py-0" id="optionsId" style="cursor:pointer; width: 170px;background-color: white;color: black; font-size: 12px; box-shadow: rgba(0, 0, 0, 0.18) 0px 6px 12px; display: block; right: 15px; top: 3px;">'
        /* +"<div class=\"d-flex align-items-start py-1 px-2\" id=\"SBsettingsoption\" onclick=\"showSprintlist('SBsettingsOpen');event.stopPropagation();\" style=\"border-bottom: 1px solid #aaaaaa;\">"
          +'<img class="class="img-fluid" src="images/agile/sprint_blue.svg" style="width:18px;height:18px;">'
          +'<span class="pl-2">Sprint List</span>'
        +'</div>' */
        +"<div class=\"d-flex align-items-start py-1 px-2\" id=\"SBsettingsoption\" onclick=\"listSprintgroups('SBsettingsOpen');event.stopPropagation();\">" //openSBsettings();
          +'<img class="class="img-fluid" src="/images/agile/scrumboard.svg" style="width:20px;height:20px;">'
          +'<span class="pl-2">Sprint Group Settings</span>'
        +'</div>'
      +'</div>'

      $('#mainOptions').append(ui);
    }else{
      hideGlboption(menu);
    }
  }else if(globalmenu=="agile"){
    if($('#optionsId').length == 0){
      ui='<div class="position-absolute rounded border px-2 py-1" id="optionsId" style="cursor:pointer; width: 125px;background-color: white;color: black; font-size: 12px; box-shadow: rgba(0, 0, 0, 0.18) 0px 6px 12px; display: block; right: 15px; top: 3px;">'
        +"<div class=\"d-flex align-items-center\" id=\"agileExpandCollapse\" onclick=\"agileExpandAll();event.stopPropagation();\">"
          +'<img class="class="img-fluid" src="images/idea/expand_all2.svg" style="width:20px;height:20px;">'
          +'<span class="pl-2">Expand All</span>'
        +'</div>'
      +'</div>'
      $('#mainOptions').append(ui);
    }else{
      hideGlboption(menu);
    }
  }
}

function showTaskList(){
  if($(".elem-CalenStyle").is(":hidden")==true){
    $(".elem-CalenStyle").show();
  }else if($("#addTaskDiv").length!=0){
    $("#headerTask, #taskList, .popupTaskDiv").toggle();
    if(taskview=="listtask"){
      if($("#headerTask").hasClass('d-flex')){
        $("#headerTask").addClass('d-none').removeClass('d-flex');
      }else{
        $("#headerTask").addClass('d-flex').removeClass('d-none');
      }
    }
    
  }
  

  
}
  


function hideGlboption(menu){
  if(menu==globalmenu){
    $('#optionsId').toggle();
  }else{
    $('#optionsId').remove();
  }
}
  

function wsSprintpageUi(){
  var ui="";

  ui="<div id='sprintDivMainbody' class='sprintDiv' style=' '>" 
    +"<div id='SBcompletepopup'></div>"
    +"<div id='SprintDivMain' place=''></div>"
    +"<div id='SprintDivContentMain' class='sprintPageCls'>"
      +"<div id='sprintHeaderDiv' class='d-flex p-1'>"
        +"<div class='defaultExceedCls' id='' style='width: 12%;text-align: center;'>"+getValues(companyLabels,"Sprint_Group")+"</div>"
        +"<div class='defaultExceedCls' id='' style='width: 50%;'>"+getValues(companyLabels,"SPRINT")+"</div>"
        +"<div class='defaultExceedCls' id='' style='width: 15%;'>"+getValues(companyLabels,"Start_date")+"</div>"
        +"<div class='defaultExceedCls' id='' style='width: 15%;'>"+getValues(companyLabels,"End_date")+"</div>"
        +"<div class='defaultExceedCls' id='' style='width: 8%;'>"+getValues(companyLabels,"Created")+"</div>"
      +"</div>"
      +"<div id='sprintContentListDiv'></div>"
      +"<div id='sprintContentGridListDiv' class='d-flex flex-wrap justify-content-start' style='font-size: 14px;'></div>"
    +"</div>"
    +'<div class="calendar-1" style="display:none;"></div>'
    +"<div id='SprintScrumboardDiv' class='sprintPageCls'></div>"
    +"<div id='SBsettingsDiv' class='sprintPageCls'></div>"
    

  +"</div>"
  +"<div id='sprintChartDiv' style='display:none;'></div>"

  $("#content").html(ui);


}

function generateSprintDatesjson(place){
  var jsonvalue="[";
  var tDate=""; var dateStatus=""; var estHour=""; var id="";
  $('#sprintdatecheckcontent').find('.sprintdatecheckcontentEach').each(function(){
      id = $(this).attr('checkid');
      tDate = $(this).find('#sprintcalDates_'+id).text();
      dateStatus = $(this).find('#inputSprintdatecheckbox_'+id).prop("checked");
      estHour = $(this).find('#sprintcalEsttimes_'+id).val();
      dateStatus = dateStatus==true?"Checked":"Unchecked";
      jsonvalue+="{";
      jsonvalue+="\"taskDate\":\""+tDate+"\",";
      jsonvalue+="\"dateStatus\":\""+dateStatus+"\",";
      jsonvalue+="\"estimated_hour\":\""+estHour+"\"";
      jsonvalue+="},";
      
  });
  
  if(jsonvalue.length>1){
      calSprintDateSelectedJSON = jsonvalue.substring(0, jsonvalue.length-1);
      calSprintDateSelectedJSON=calSprintDateSelectedJSON+"]";
  }else{
      calSprintDateSelectedJSON ="[]";
  }
  console.log("calSprintDateSelectedJSON-->"+calSprintDateSelectedJSON);

  if(place=="calendar"){
    $('#sprintstartDateCalNewUI').attr('prevdate',$('#sprintstartDateCalNewUI').attr('formatdate'));
    $('#sprintendDateCalNewUI').attr('prevdate',$('#sprintendDateCalNewUI').attr('formatdate'));
    sprintcheckflag=true;
  }
  
  $('.openSprintselectDatepopupcls').toggle();

}

function showSprintViewoptions(){
  $("#agileSview").toggle();
}


function popupTaskUI(srcid,srctitle,place){
    var ui="";
    $('#otherModuleTaskListDiv').html('');
    $("#transparentDiv").show();
    var header1 = globalmenu=="activityFeed"?"Conversation Task":globalmenu=="wsdocument"?"Document Task":"";
    var header1img = globalmenu=="activityFeed"?"/images/menus/conversation.svg":globalmenu=="wsdocument"?"/images/menus/doc.svg":"";
    
    ui='<div class="popupTask" id="" srcid='+srcid+'>'
        +'<div class="" style="">'
        +'<div class="modal-content px-2" onclick="" style="z-index:1000 !important;max-height: 550px;height: 550px;width: 88%;margin-left: 120px;">'
            +'<div class="modal-header pt-2 pb-0 px-0" style="border-bottom: 1px solid #b4adad;height:30px;">'
            +'<div class=" d-flex  align-items-center w-100" style="">'
                +'<div class="pl-2 d-none" style="font-size:12px;"><img src="'+header1img+'" style="width:17px;height:17px;"><span class="pl-2">'+header1+'</span></div>'
                +"<div class=\"ml-auto pr-3 pb-1 d-none\" title=\"Create Task\"><img class=\"creatPopupTaskbutton\" onclick=\"createPopupTask();event.stopPropagation();\" src=\"images/task/plus.svg\" style=\"height:18px;width:18px; cursor:pointer;\"></div>"
            +'</div>'
            //+'<button type="button" onclick="closeagileTask();" class="close p-0" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
            +'<img src="images/menus/close3.svg" onclick="closePopupTask();event.stopPropagation();" class="" style="cursor:pointer;width:10px;height:10px;margin-top:1px;margin-right:0px;">'
            +'</div>'
            +'<div class="p-0 wsScrollBar" style="overflow: auto;height: inherit;">'
            +'<div class="py-1 w-100 task_type_divison" style="color:#fff;font-size:12px;">'
                +'<div class="pl-2 d-flex align-items-center" style="font-size:14px;">'
                +'<img src="'+header1img+'" style="width:17px;height:17px;">'
                +'<span class="pl-2" style="">'+header1+'</span>'
                +'</div>'
            +'</div>'
            +'<div id="popupcreateTaskDivbody" class="p-0" style=""></div>'
            +'<div class="popupTaskDiv">'
                +'<div id="popupTasksListHeaderDiv" class="p-0" style=""></div>'
                +'<div id="popupTasksListDivbody" class="p-0" style=""></div>'
            +'</div>'    
            +'</div>'
            
        +'</div>'
        +'</div>'
    +'</div>'

    return ui;
  }

  function closePopupTask(){
    $('#otherModuleTaskListDiv').html('').hide();
    $("#transparentDiv").hide();
    convoTask="";
    if(globalmenu=="wsdocument"){
      $("#viewdownloadfileinconversation").addClass("viewdownloadfileinconversationCls");
    }
  }

  function createPopupTask(){
    insertCreateTaskUI();
  }

  function showTaskViewoptions(){
    $("#taskOptionview").toggle();
  }

  function searchOnAgileEnter(event,obj){
    var agileSearchword = $("#agileinputsearch").val();
    if(agileSearchword.trim() != ''){
        $('.serachBoxClearIcon').show();
    }else{
        $('.serachBoxClearIcon').hide();
    }
  }

  function clearAgileSrh(){

    $('#agilelistUL_0, #unorganizedAgile').html("");
    listAgileEpics();
    listUnorgStories();
    $('#agilelistUL_0').prepend("<div class='d-flex align-items-center mr-3 rounded py-1 orgheader' style='background-color: #eeced4;'><img src='/images/agile/org_blue.svg' class='image-fluid mx-2' title='Organized' style='width:20px;height: 20px;'><span class='defaultExceedCls' style='font-size:12px;color:black;'>Organized</span></div>");
    $('#unorganizedAgile').prepend("<div class='d-flex align-items-center mx-3 mt-2 rounded py-1 unorgheader' style='background-color: #f2d8a9;'><img src='/images/agile/unorg_blue.svg' class='image-fluid mx-2' title='Unorganized' style='width:20px;height: 20px;'><span class='defaultExceedCls' style='font-size:12px;color:black;'>Unorganized</span></div>");
    $('#agileinputsearch').val('');
    $('.serachBoxClearIcon').hide();

  }


function searchAgile(event,obj){
  var code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
  if(code==13){
    var agileSearchval = $("#agileinputsearch").val();
    var agileSortval = "";
    searchagiletextOrg(agileSearchval,agileSortval);
    searchagiletextUnorg(agileSearchval,agileSortval);
  }
}

function searchAgileIcon(event,obj){
  var agileSearchval = $("#agileinputsearch").val();
  var agileSortval = "";
  searchagiletextOrg(agileSearchval,agileSortval);
  searchagiletextUnorg(agileSearchval,agileSortval);
}

function searchagiletextOrg(agileSearchval,agileSortval){
    
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    let jsonbody = {
        "user_id":userIdglb,
        "project_id":prjid,
        "company_id":companyIdglb,
        "sortVal":agileSortval,
        "searchTxt":agileSearchval,
        "localOffsetTime":localOffsetTime,
        "action":"loadEpicStories"
    }  
    $.ajax({
        url: apiPath+"/"+myk+"/v1/getEpics",
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){
            $('#agilelistUL_0').html(prepareAgileUI(result,"sort"));
            $('#agilelistUL_0').prepend("<div class='d-flex align-items-center mr-3 rounded py-1 orgheader' style='background-color: #eeced4;'><img src='/images/agile/org_blue.svg' class='image-fluid mx-2' title='Organized' style='width:20px;height: 20px;'><span class='defaultExceedCls' style='font-size:12px;color:black;'>Organized</span></div>");

            $('#organizedAgile').removeClass('w-25').addClass('w-75');
            $('#unorganizedAgile').removeClass('w-75').addClass('w-25');
            
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });
}

function searchagiletextUnorg(agileSearchval,agileSortval){
  //$('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody = {
      "user_id":userIdglb,
      "project_id":prjid,
      "company_id":companyIdglb,
      "sortVal":agileSortval,
      "searchTxt":agileSearchval,
      "localOffsetTime":localOffsetTime
  }  
  $.ajax({
      url: apiPath+"/"+myk+"/v1/unorganizedStory",
      type:"POST",
      dataType:'json',
      contentType:"application/json",
      data: JSON.stringify(jsonbody),
      error: function(jqXHR, textStatus, errorThrown) {
              checkError(jqXHR,textStatus,errorThrown);
              //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      },
      success:function(result){
          $('#unorganizedAgile').html(storiesUi(result,'unorganized',0));
          $('#unorganizedAgile').prepend("<div class='d-flex align-items-center mx-3 mt-2 rounded py-1 unorgheader' style='background-color: #f2d8a9;'><img src='/images/agile/unorg_blue.svg' class='image-fluid mx-2' title='Unorganized' style='width:20px;height: 20px;'><span class='defaultExceedCls' style='font-size:12px;color:black;'>Unorganized</span></div>");

          $('#organizedAgile').removeClass('w-25').addClass('w-75');
          $('#unorganizedAgile').removeClass('w-75').addClass('w-25');
          
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      }
  });
}

function agileSort(){
  var agileSearchval = "";
  var agileSortval = $('#agileSortid').find("option:selected").attr('value');
  searchagiletextOrg(agileSearchval,agileSortval);
  searchagiletextUnorg(agileSearchval,agileSortval);
}


function csvupload(){
	  var ui="";
	  $('#agilepopups').html('');
    $("#transparentDiv").show();
    var fileurl="";
    if(globalmenu=="Task"){
      fileurl="https://newtest.colabus.com:1933///csvFileTaskExampleForProject.csv";
    }else if(globalmenu=="agile"){
      fileurl="https://newtest.colabus.com:1933//EpicCsv/csv_sample_agile.csv";
    }else if(globalmenu=="wsTeam"){
      fileurl="https://newtest.colabus.com:1933///csvFileExampleForTeam.csv";
    }

    
    ui='<div class="csvuploadcls pophover" id="">'
        +'<div class="modal-dialog">'
        +'<div class="modal-content container">'
        
            
            +'<div class="modal-header py-2 pl-0" style="border-bottom: none !important;">'
              +'<p class="modal-title" style="font-size:14px;color: black;font-weight: normal;">'+getValues(companyLabels,"Upload_CSV_file")+'</p>'
              +'<button type="button" onclick="closeCsvUpload();" class="close p-0" data-dismiss="csvuploadcls" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
            +'</div>'
            
            
            +'<div class="pt-2 pl-0" style="height:100px;">'
              +'<div id="cvsImg" style="float:left;height:25px;width:14px;padding: 8px;">'
                +'<form action="" name="uploadCsvData" id="uploadCsvData" method="post" enctype="multipart/form-data">'
                  +'<label class="imageUpload" style="position: absolute;"> '
                    +'<input type="file" onchange="uploadCsv();" class="file" name="CsvFileName" id="CsvFileName" multiple="multiple" style="left:0px;width:150px;top:0px;">'
                  +'</label>'
                +'</form>'
              +'</div>'
              +'<div style="color: #787d81;float: left;font-family: opensansregular;margin-left: 4%;margin-top: 5px;width: 90%;" class="defaultExceedCls" id="csvUploadFileName"></div>'
            +'</div>'
            
            +'<div class="modal-footer d-flex justify-content-between align-items-center py-2 px-0" style="border-top: none !important;">'
              +"<div>"
                +"<a href='"+fileurl+"' style='font-size: 14px;color: #6b6b6b;text-decoration: underline;' target='_blank'>"+getValues(companyLabels,"Download_sample_CSV_file")+"</a>"
              +"</div>"
              +"<div>"
                +'<img src="/images/task/remove.svg" title="Cancel" onclick="closeCsvUpload();event.stopPropagation();" class="mx-1" style="width:25px;height:25px;cursor:pointer;">'
                +'<img src="/images/task/tick.svg" title="Upload" onclick="readDataFromCsv();event.stopPropagation();" style="width:25px;height:25px;cursor:pointer;">'
              +'</div>'//uploadCsv();
            +'</div>'
            
        +'</div>'
        +'</div>'
    +'</div>'

    $('#agilepopups').append(ui).show();
  }
  // var blob1 = "";
  function getCanvas(){
              var hash = $("#upload_Newprofile").attr('src').toString(); 
              var canvas = $("#upload_Newprofile11")[0];
              var context = canvas.getContext('2d');
              var imgData = "";
              var base_image = new Image();
              base_image.src = hash;
              base_image.onload = function(event){
                  context.drawImage(base_image,0,0,150,150);
                  imgData = context.getImageData(0, 0, canvas.width, canvas.height);

              }
  }


  var fileUploadType =  "uploadTheMainFile";
  function uploadProfilePic(){
      if(profileBlobData==""){
        // var canvas = $("#upload_Newprofile11")[0];
        // var context = canvas.getContext('2d');
        // var dataURL = context.canvas.toDataURL("image/jpeg", 1);
        // var data = atob(dataURL.substring("data:image/jpeg;base64,".length)),
			  // asArray = new Uint8Array(data.length);

        // for (var i = 0, len = data.length; i < len; ++i) {
        //   asArray[i] = data.charCodeAt(i);
        // }
        // profileBlobData = new Blob([asArray.buffer], { type: "image/jpeg" });
        // // profileBlobData=blob;
        // fileUploadType = "";
      }

      console.log(fileUploadType);
      var data = new FormData();
          data.append('file', profileBlobData);
				  data.append("place", "profile");
				  data.append("resourceId",userIdglb);
				  data.append("user_id",userIdglb);
          data.append("company_id",companyIdglb);
          if(fileUploadType == ""){
            data.append("taskType",fileUploadType);
          }else{
            data.append("taskType",fileUploadType);
          }
          
          
          $.ajax({
            url: apiPath + "/" + myk + "/v1/upload",			      
            type:"POST",
            processData: false,
            contentType: false,
            data: data,
            cache: false,
            mimeType: "multipart/form-data",
            error: function(jqXHR, textStatus, errorThrown) {
              checkError(jqXHR,textStatus,errorThrown);
              
            },
            success: function(result) {		         
                  
              // globUrl = result;
              // console.log(globUrl);
            }
            
         });
  }


  function uploadProfileCropPic(){
    if(profileBlobData==""){
      var canvas = $("#upload_Newprofile11")[0];
      var context = canvas.getContext('2d');
      var dataURL = context.canvas.toDataURL("image/jpeg", 1);
      var data = atob(dataURL.substring("data:image/jpeg;base64,".length)),
      asArray = new Uint8Array(data.length);

      for (var i = 0, len = data.length; i < len; ++i) {
        asArray[i] = data.charCodeAt(i);
      }
      profileBlobData = new Blob([asArray.buffer], { type: "image/jpeg" });
      // profileBlobData=blob;

    }


    // console.log(blob+fileName);
          var data = new FormData();

          // if(profileBlobData==""){
          //   data.append('file', blob1);
          // }else{
            data.append('file', profileBlobData);
          // }

          
				  data.append("place", "ProfilePicCropFile");
				  data.append("resourceId",userIdglb);
				  data.append("user_id",userIdglb);
          
          $.ajax({
            url: "http://localhost:8080/v1/upload",			      
            type:"POST",
            processData: false,
            contentType: false,
            data: data,
            cache: false,
            mimeType: "multipart/form-data",
            error: function(jqXHR, textStatus, errorThrown) {
              checkError(jqXHR,textStatus,errorThrown);
              
            },
            success: function(result) {		         
                  
              // globUrl = result;
              // console.log(globUrl);
            }
            
         });
  }

  function readURLNewProfile2(input) { //---------this is for viola screen
		 
    if (input.files && input.files[0]) {
         var reader = new FileReader();
         

       $('#albumFileExtn').val(input.files[0].name.split('.').pop().toLowerCase());
       $('#albumFileName').val(input.files[0].name.split('.')[0]);
       
         reader.onload = function (e) {
            //--- below code is used for image cropper
           initCrop(e.target.result);
           var uploadCropvoila = imageCrop.data('cropper');
           $("#upload-result").on("click",function(){
             $('#profilepictransparentDiv').hide();
             uploadCropvoila.getCroppedCanvas().toBlob((blob) => {
                 $('#newcreateprofile').attr('src', URL.createObjectURL(blob));
                 console.log(blob);
                 console.log(URL.createObjectURL(blob));
                 profileBlobData= blob; 
                 fileExtension ="jpeg";
                 fileName = userId+".jpeg";
                 closeCropper();
                 profileImgProfileFlag = true;
            },'image/jpeg');
           });
         }	 
     reader.readAsDataURL(input.files[0]);
   }  
     
 }

  function readURLNewProfile1(input) { //---------this is for viola screen
		 
    if (input.files && input.files[0]) {
         var reader = new FileReader();
         

       $('#albumFileExtn').val(input.files[0].name.split('.').pop().toLowerCase());
       $('#albumFileName').val(input.files[0].name.split('.')[0]);
       
         reader.onload = function (e) {
            //--- below code is used for image cropper
           initCrop(e.target.result);
           var uploadCropvoila = imageCrop.data('cropper');
           $("#upload-result").on("click",function(){
             $('#profilepictransparentDiv').hide();
             uploadCropvoila.getCroppedCanvas().toBlob((blob) => {
                 $('#upload_Newprofile').attr('src', URL.createObjectURL(blob));
                 console.log(blob);
                 console.log(URL.createObjectURL(blob));
                 profileBlobData= blob; 
                 fileExtension ="jpeg";
                 fileName = userId+".jpeg";
                 closeCropper();
                 profileImgProfileFlag = true;
            },'image/jpeg');
           });
         }	 
     reader.readAsDataURL(input.files[0]);
   }  
     
 }

  function closeCsvUpload(){
    $('#agilepopups').html('').hide();
    $("#transparentDiv").hide();
    $('#welcome-data').html('').hide();
  }
  var globUrl ="";
  function uploadCsv(){
    var fileex=$("#CsvFileName").val();
		console.log(fileex);
		var extension = fileex.split(".");
		console.log(extension);
		if(extension[1] == "csv" || extension[1] == "CSV"){
			$("#csvUploadFileName").text(fileex);
			fileex = fileex.replace(/^.*[\\\/]/, '');
    	
	  	var data = new FormData();
      if(globalmenu=="Task"){
        jQuery.each(jQuery('#CsvFileName')[0].files, function(i, file) {
				  //data.append('file-'+i, file);
				  data.append('file', file);
				  data.append("place", uploadplace);
				  data.append("resourceId",parseInt(userId));
				  data.append("user_id",parseInt(userId));
				  data.append("company_id",parseInt(companyId));	
	  	});
      }else if(globalmenu=="agile"){
        jQuery.each(jQuery('#CsvFileName')[0].files, function(i, file) {
				  //data.append('file-'+i, file);
				  data.append('file', file);
				  data.append("place", uploadplace);
				  data.append("resourceId",parseInt(userId));
				  data.append("user_id",parseInt(userId));
				  data.append("company_id",parseInt(companyId));	
	  	});
      }else if(globalmenu=="wsTeam"){
        jQuery.each(jQuery('#CsvFileName')[0].files, function(i, file) {
				  //data.append('file-'+i, file);
				  data.append('file', file);
				  data.append("place", "UploadCsvFile4users");
				  data.append("resourceId",parseInt(userIdglb));
	
	  	});
      }
	  	
	  	$.ajax({
				url: apiPath+"/"+myk+"/v1/upload",			      
			  type:"POST",
				processData: false,
				contentType: false,
        data: data,
				cache: false,
				mimeType: "multipart/form-data",
				error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					
				},
		    success: function(result) {		         
		        	
					globUrl = result;
          console.log(globUrl);
        }
				
		 });
			
	  		
		}else{
			alert("Upload CSV File Only");
			return false;
		}
  }

  function readDataFromCsv(){
    let jsonbody= {
      "user_id":userIdglb,	
      "companyId":companyIdglb,	
      "project_id":prjid,
      "fileName":globUrl
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
     $.ajax({
         url : apiPath+"/"+myk+"/v1/saveCsvUserFileDetailsTeam",
         type : "POST",
         contentType:"application/json",
         data:JSON.stringify(jsonbody),
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $('#loadingBar').addClass('d-none').removeClass('d-flex');
                 
                 },
         success : function(data) {

          // if(page == "advancePage"){
          //   $('#advOptionContainer').hide();
          //   $("#transparentDiv").hide();
          //   $("#newWSUiPopup").html('');
          //   $('#loadingBar').addClass('d-none').removeClass('d-flex');
          //   loadMyprojectJsonNew();
          // }

          if(data=="failure"){
            alertFun(getValues(companyAlerts,"Alert_UsrAddFail"),"warning");
          }
          if(data=="success"){
            if(globalmenu=="wsTeam"){
              // console.log("in side team");
              // $('#advOptionContainer').hide();
              // $("#transparentDiv").hide();
              // $("#newWSUiPopup").html('');
              $('#agilepopups').html('').hide();
              $("#transparentDiv").hide();
              $('#loadingBar').addClass('d-none').removeClass('d-flex');
              alertFun(getValues(companyAlerts,"Alert_UsrAdded"),"warning");
              listContacts('teams');
            }
          }
          

         }
                  
     });
  }

function contactsDiv(){
  var ui = "";

  ui="<div id='contactsMainDiv'>"
    +"<div id='contactsHeaderDiv' class='headerBar px-2 py-1' style='display:flex;z-index: 1;'>"
      +"<div class='defaultExceedCls' id='' style='width: 6%;text-align: center;'></div>"
      +"<div class='defaultExceedCls' id='' style='width: 20%;'>"+getValues(companyLabels,"Connect_Name")+"</div>"
      +"<div class='defaultExceedCls' id='' style='width: 17%;'>"+getValues(companyLabels,"Connect_Department")+"</div>"
      +"<div class='defaultExceedCls' id='' style='width: 15%;'>"+getValues(companyLabels,"Connect_Mobile")+"</div>"
      +"<div class='defaultExceedCls' id='' style='width: 13%;'>"+getValues(companyLabels,"Connect_Work")+"</div>"
      +"<div class='defaultExceedCls' id='' style='width: 17%;'>"+getValues(companyLabels,"Connect_Email")+"</div>"
      +"<div class='defaultExceedCls' id='' style='width: 6%;text-align: center;'>Status</div>"
      +"<div class='defaultExceedCls' id='' style='width: 6%;'></div>"
    +"</div>"  
    +"<div id='contactsListDiv' class=''></div>"
  +"</div>"  
  +"<div id='contactsTilesDiv' class='pt-2' style='display:none;'></div>"   
  
  $("#content").html(ui);
}

function integrationDiv(){
  var ui = "";

  ui="<div id='integrationMainDiv'>"
  
    +"<div id='integationListDiv' class=''style='margin:23px' title='Email' >"
    +"<img id='emailIntegration1' onclick= 'emailPopupDiv()' src='/images/integration/emailsetup.png' style='height:80px;cursor:pointer;'>"
    +"<div style='margin-top:5px;color:grey;text-align:center;width:6.5%;overflow: hidden;font-family: Tahoma;font-size: 13px;'>"
		+"<span title='Email Integration' style='color:grey;' id='readtext'>Email</span>"
		  +"</div>"
    //+"<div><input type='submit' class='btn btn-info' onclick='getEmail()' value='authenticate' style='margin:10px'></div>"
    +"</div>"
  +"</div>"  
    
  
  $("#content").html(ui);
}

function teamDiv(){
  var ui = "";

  ui="<div id='contactsMainDiv'>"
    +"<div id='contactsHeaderDiv' class='headerBar px-2 py-1' style='display:flex;z-index: 1;'>"
      +"<div class='defaultExceedCls' id='' style='width: 6%;text-align: center;'></div>"
      +"<div class='defaultExceedCls' id='' style='width: 24%;'>"+getValues(companyLabels,"Connect_Name")+"</div>"
      +"<div class='defaultExceedCls' id='' style='width: 20%;'>"+getValues(companyLabels,"Connect_Department")+"</div>"
      +"<div class='defaultExceedCls' id='' style='width: 15%;'>"+getValues(companyLabels,"Connect_Mobile")+"</div>"
      +"<div class='defaultExceedCls' id='' style='width: 15%;'>"+getValues(companyLabels,"Connect_Work")+"</div>"
      +"<div class='defaultExceedCls' id='' style='width: 20%;'>"+getValues(companyLabels,"Connect_Email")+"</div>"
    +"</div>"  
    +"<div id='teamListDiv' class=''></div>"
  +"</div>"  
  +"<div id='teamTilesDiv' class='pt-2' style='display:none;'></div>"   
  
  $("#content").html(ui);
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(regex.test(email)){
      return true;
  }else{
      return false;
  }
}

function showContactViewoptions(place){
  if(place == "teams"){
    $("#teamViewdiv").toggle();
  }else{
    $("#contactViewdiv").toggle();
  }
  
}

function sortContacts(){
  limitcontact = 0;
  indexcontact = 0;
  contactsortval = $("#contactsSort :selected").val();
  sortContactslist();
}  

function wsDocsUi(){
  var ui="";
  ui="<div id='documentMainDiv'>"
    +"<div id='folderCreateDiv' class=''></div>"
    +"<div id='documentListDiv' class=''>"
      +"<div id='documentListHeaders' class='d-flex align-items-center headerBar px-2 py-1' style='z-index:1;'>"
        +"<div class='defaultExceedCls documentListHeaders1' id='' style='padding-left: 55px;'>"+getValues(companyLabels,"Name")+"</div>"
        +"<div class='defaultExceedCls documentListHeaders2 ' id='' style=''>"+getValues(companyLabels,"size")+"</div>"
        +"<div class='defaultExceedCls documentListHeaders2 ' id='' style=''>"+getValues(companyLabels,"owner")+"</div>"
        +"<div class='defaultExceedCls documentListHeaders2 ' id='' style=''>"+getValues(companyLabels,"created")+"</div>"
        +"<div class='defaultExceedCls documentListHeaders2 ' id='' style=''>"+getValues(companyLabels,"modifier")+"</div>"
        +"<div class='defaultExceedCls documentListHeaders2 ' id='' style=''>"+getValues(companyLabels,"modified")+"</div>"
        +"<div class='defaultExceedCls documentListHeaders3 ' id='' style=''></div>"
        +"<div class='defaultExceedCls documentListHeaders3 ' id='' style=''></div>"
      +"</div>"
      +"<ul id='documentListUL_0' class='documentTree ' style=''></ul>"
      +"<ul id='documentDrivesList' class='documentTree ' style=''></ul>"
    +"</div>"
    +"<div id='documentGridDiv' class=''></div>"
  +"</div>"
  $("#content").html(ui);

}

function checkSplChar(word){
  var splChar = /^\s*[a-zA-Z0-9,_\-\+&.:()\s]+\s*$/;
   if (!splChar.test(word)) {
       return false;
   } else {
       return true;
   }
}

function showemailpopup(){
  
  emailPopupDiv();
  //newemailbox();
 
 }

 function emailPopupDiv(){
  var ui ="";
  ui='<div class="SBactiveSprintlistCls" style="top:85px !important;" id="SBactiveSprintlistId" selectedstories="">'
			+'<div class="mx-auto w-50 modal-dialog11" style="max-width: 550px; min-height: 150px;height: calc(100% - 132px - 4px);max-height: 391px;">'
				+'<div id="addgrp1" class="modal-content11 container px-3" style="">'
					+'<div class="modal-header11 pt-3" style="border-bottom: 1px solid #b4adad !important;">' 
						+'<h5 id="updategroup" class="modal-title defaultExceedCls" style="color:black;font-size: 16px !important;">Email</h5>' 
						+"<button type=\"button\" class=\"close\" onclick=\"closeEmailPopup();\" id=\"\" style=\"top: 5px;right: 10px;color: black;opacity: unset;padding: 0px;outline:none;\" data-dismiss=\"modal11\">×</button>"
          +'</div>' 
          

          +'<div class="modal-body11 wsScrollBar mb-2" style="height: calc(100% - 40px);">'
              +'<div  style="width:100%;float:left;height: 280px;padding-top: 2%;padding-right: 2%;border-bottom: 1px solid #9c9c9c;padding-bottom: 2%;">'
                    +'<div id="emailConfigContent" style="width: 100%;float: left;">'
	        
	       +'<div style="width: 99%;float: left;margin-left: 1%;border-bottom: 1px solid #9c9c9c;">'
		      +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	 +'<div style="float:left;width: 100%;">'
					+'<div style="float: left;margin-top: 1px;width: 5%;"><input checked="checked" type="radio" value="colabusConfig" name="fetchEmailConfig" onchange="emailConfigType(this);" id="colEmailConfig"></input> </div>'
					+'<div class="Use_Colabus_Email_cLabelHtml" style="color:#484848;float: left;font-size: 16px;width: 63%;font-family: opensanscondbold;border-right: 1px solid #ccc;margin-right: 3%;">Use Colabus Email </div>'
				    
				    +'<div style="float: left;width:14%;display: block;" id="configDiv">'
				       +'<div style="float:left;margin-top: 3px;margin-right: 10%; ">'
				          +'<div id="emailStatus" class="roundImgRedConfig"></div>'
				       +'</div>'
				       +'<div class="configemailLabel Status_cLabelHtml">Status </div>'
				    +'</div>'
				    
				    
				 +'</div>'
	          +'</div>'
	          
	          +'<div style="float: left;width: 100%;padding-left: 6%;">'
		          +'<div style="width: 85%;float: left;padding-right: 2%;">'
		            +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	    +'<div style="width: 100%;float: left;">'
	                 +'<input type="text" class="form-control" style="height: 30px;" id="colabusEmailId" placeholder="eg: abc@colabusmail.com">'
	                +'</div>'
	              +'</div>'
	            +'</div> '
	              +'<img id="configuredEmailImg" src="/images/offSwitch.png"  value=""  onclick="newemailbox()" style="width:27px;margin-top:1px; cursor:pointer; margin-left: 2%;">'
	             
                      +'<div class="form-group" style="float: left;width: 83%;padding-left: 0;margin-bottom: 10px;">'
                          +'<label style="width: 40%;float: left;font-weight: normal;margin-top: 1px;color: #000"  class="projPopLabel Email_data_fetch_type_cLabelHtml" >Fetch emails from </label>'
                          +'<div style="float:right;width: 50%;">'
                            +'<div style="float: left;margin-top: 1px;" ><input type="radio" value="ColspecifedDate" name="ColfetchEmail" checked="checked" onchange="colemailFetchType(this);" id="ColfetchEmailsFromDate"></input> Current date </div>'
                            +'<div style="float: right;margin-top: 1px;" ><input type="radio" value="ColspecifedDate" name="ColfetchEmail" checked="checked" onchange="colemailFetchType(this);" id="FromDate"></input> All </div>'
                            
                          +'</div>'
                      +'</div>'
          +'</div>' 

					
					
					// +'<div id="wsSaveUpdateDiv" class="modal-footer11 pb-3 d-flex justify-content-end">'
					// 	+"<div id=\"\" onclick=\"newemailbox();\" style=\" width: 100px !important;float: right;\" class=\"createBtn mt-0 mr-2\">Authenticate</div>"
					// 	+"<div id=\"editgrpbtn\"  onclick=\"GetEmailAccessToken();\" style=\" width: 70px !important;float: right;\" class=\" mt-0 createBtn SAVE_cLabelHtml\">Save</div>"
      		// 		+'</div>'
        +'</div>'
        +'</div>'
        +'</div>'
      +'</div>'
      +'<div style="float: left;width: 100%;margin-bottom: 10px">'
	          +'<div id="emailConfigSave" style=" width: 70px;float: right;" class="createBtn mt-0 mx-2" onclick="GetEmailAccessToken();"> Save </div>'
            +'<div id="emailConfigCancel" style=" width: 70px;float: right;" class="createBtn mt-0 mx-2" onclick="closeEmailPopup();"> Cancel </div>'
	        
            +'</div>'
		+'</div>'
 
  $("#emailIntegration").html(ui).show();
  $("#transparentDiv").show();
  getEmailconfig();
 }

 function closeEmailPopup(){
  $("#emailIntegration").hide();
  $("#transparentDiv").hide();
 }

 var deviceCode;
 function newemailbox(){
  var value = $("#emailStatus").attr('class');
  var emailid = $("#colabusEmailId").val();
   if(emailid != ""   ){
      if(value != "roundImgGreenConfig"){
      $.ajax({
        url: "https://accounts.zoho.in/oauth/v3/device/code",
        type:"POST",
        data: jQuery.param({ 
          client_id: "1004.7UJ4MURMBALXUUYF143OGE6W2GSERU", 
          scope : "ZohoMail.accounts.READ,ZohoMail.messages.READ,ZohoMail.messages.CREATE,ZohoMail.messages.DELETE,ZohoMail.accounts.CREATE,ZohoMail.folders.READ,ZohoMail.folders.CREATE,",
          grant_type:"device_request",
          access_type:"offline "
        }) ,
        error: function(jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR,textStatus,errorThrown);
                    $("#loadingBar").hide();
            timerControl("");
            }, 
        success:function(result){
            var w=  window.open(result.verification_uri_complete,"MsgWindow", "width=700,height=600");    
            deviceCode=result.device_code;  
          }
        });
        }else{
          savetEmailConfig("N");
        }
    }else{
      alertFun("Please enter email id.",'warning');

      
    }
 }
var accesstoken;
var refAccesstoken;
 function GetEmailAccessToken(){
  var value = $("#emailStatus").attr('class');
  if(value == "roundImgRedConfig"){
   
  $.ajax({
    url: "https://accounts.zoho.in/oauth/v3/device/token",
    type:"POST",
    data: jQuery.param({ 
      client_id: "1004.7UJ4MURMBALXUUYF143OGE6W2GSERU", 
      grant_type:"device_request",
      client_secret:"24956f725d333c5389f82077b284fe8d58109f4205",
      grant_type:"device_token",
      code:deviceCode
  }) ,
    error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
        timerControl("");
        }, 
    success:function(result){
       
      
      accesstoken= result.access_token;
      refAccesstoken=result.refresh_token;
      saveAccessToken(accesstoken,refAccesstoken,'Y');
     
      
          }
     });
    }else{
      alertFun("Zoho Mail is already configured.",'warning');
    }
 }

 function saveAccessToken(accessToken,refAccesstoken,status){
  
  var email = $("#colabusEmailId").val();
  
  let jsonbody={
    "companyId":companyIdglb,
    "accessToken":accessToken,
    "status":status,
    "configEmailId":email,
    "refToken":refAccesstoken
	};
  $.ajax({
    url: apiPath+"/"+myk+"/v1/saveAccessTokenZoho",
     //url : "http://localhost:8080/v1/saveAccessTokenZoho",
    type:"PUT",
    contentType:"application/json",
    data: JSON.stringify(jsonbody),
    error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
        timerControl("");
        }, 
    success:function(result){
        $("#configuredEmailImg").attr("src","/images/onSwitch.png");
        $("#emailStatus").attr("class","roundImgGreenConfig");
        alertFun("Zoho Mail configured Successfully.",'warning');
        
       }
     });

 }
var msgBox;
const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      
 function getEmail(){
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody={
    "companyId":companyIdglb,
    "projId":prjid
  };
  
  $.ajax({
    url: apiPath+"/"+myk+"/v1/getEmails",
    //url : "http://localhost:8080/v1/getEmails",

    type:"POST",
    dataType:'json',
    contentType:"application/json",
    data: JSON.stringify(jsonbody),
    error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        // timerControl("");
        }, 
    success:function(result){

      msgBox = result;
      if(result.length>0){
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      var ui="";
      var d;
      var time;
       var imgName = lighttpdpath+"/userimages/";
       const datac = [];
       var j=0;
      //  for(var i=0;i<result.length;i++){
      //   var threadstatus = result[i].threadStatus;

      //   if(threadstatus == "Y" && datac.includes(result[i].msgThread)){
      //     datac[j]=result[i].msgThread;

      //   }
      //  }
      const currd = new Date();
      let currmonth = months[currd.getMonth()];
      let currdate1 = currd.getDate();
      var currti=  currmonth+", "+currdate1;

        for(var i=0;i<result.length;i++){
          console.log(result[i].threadStatus+"--"+result[i].msgSubject +"--from---"+result[i].hasAttachment);
          d= new Date(parseInt(result[i].dat));
          var timestamp = d.getHours()+":"+d.getMinutes()+":"+d.getMilliseconds();
          
          var threadstatus = result[i].threadStatus;
          let month = months[d.getMonth()];
          
          let date1 = d.getDate();
          var ti=  month+", "+date1;
          if(currti == ti){
            ti= "Today, "+ d.getHours()+":"+d.getMinutes();
          }else{
            ti;
          }
        //var ti = timestamp;
        // +'<div style="width:100%">'
        
        //  +'<p class="mb-0 font-weight-bold" style="font-size: 13px;" >'+result[i].sender+'</p>'
        //  +'<p class="mb-2 text-dark" style="font-size: 15px;word-break:break-word;margin-top:-2px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;">'+result[i].msgSubject+'</p>'
        // +'</div>'

        // +'<div  style="width:10%;float:right;color: #999999;font-size: 12px;">'+ti+'</div>'
        // +'</div>'
        // +'</li>'

        
        if(datac.includes(result[i].msgThread) == false){
          //alert(threadstatus+"---"+result[i].msgThread+"=="+datac.includes(result[i].msgThread));
          datac[j]=result[i].msgThread;
           j++;
        ui+="<div id= 'msg_"+result[i].msgid+"'  msgid= '"+result[i].msgid+"' threadid= '"+result[i].msgThread+"' onclick='openMail(this)' class='emailactive d-flex py-1 border border-top-0 border-left-0 border-right-0' style='color: black;height: 53px; border-left: 1px solid grey;font-size: 13px;cursor:pointer'>" +
            // "<div class='d-flex' style='font-size: 13px;'>" +
              "<img id='imagedisplay' src='"+imgName+"' title='"+result[i].sender+"' alt='"+result[i].sender+"' onerror='groupImageOnErrorReplace(this)'; class=' mx-2 my-2 cursor rounded-circle ' style='width: 30px;height: 30px;'> " +
              "<div class='pl-1 pt-1' style='width: -webkit-fill-available;'> " +
                 "<div class='d-flex align-items-center' style=''> " +
                    "<div class=' font-weight-bold defaultExceedMultilineCls' style='-webkit-line-clamp: 1;'>"+result[i].sender+"</div>"+
                    '<div class="mr-1 ml-auto" style="color: #999999;font-size: 11px;white-space: nowrap;">'+ti+'</div>'+
                  "</div>" +
                  "<div class='d-flex align-items-center' style=''> " +
                      "<div id='' class='defaultExceedMultilineCls w-100' title='" +result[i].msgSubject+"' style='font-size: 13px;font-family: OpenSansRegular;-webkit-line-clamp: 1;margin-right: 2vh;'> " +result[i].msgSubject+"</div>" +
                      '<img id="attach_'+result[i].msgid+'" src="/images/conversation/attach.svg" class="mr-2 d-none" style="height: 16px; width: 16px;">'+
                  "</div>" +
                "</div> " +
              "</div> " +
            "</div>" +
            "</div>" +
          "</div>"

        }else if(threadstatus == "N"){
          ui+="<div id= 'msg_"+result[i].msgid+"'  msgid= '"+result[i].msgid+"' threadid= '"+result[i].msgThread+"' onclick='openMail(this)' class='emailactive d-flex py-1 border border-top-0 border-left-0 border-right-0' style='color: black;height: 53px; border-left: 1px solid grey;font-size: 13px;cursor:pointer'>" +
            // "<div class='d-flex' style='font-size: 13px;'>" +
              "<img id='imagedisplay' src='"+imgName+"' title='"+result[i].sender+"' alt='"+result[i].sender+"' onerror='groupImageOnErrorReplace(this)'; class=' mx-2 my-2 cursor rounded-circle ' style='width: 30px;height: 30px;'> " +
              "<div class='pl-1 pt-1' style='width: -webkit-fill-available;'> " +
                 "<div class='d-flex align-items-center' style=''> " +
                    "<div class=' font-weight-bold defaultExceedMultilineCls' style='-webkit-line-clamp: 1;'>"+result[i].sender+"</div>"+
                    '<div class="mr-1 ml-auto" style="color: #999999;font-size: 11px;white-space: nowrap;">'+ti+'</div>'+
                  "</div>" +
                  "<div class='d-flex align-items-center' style=''> " +
                      "<div id='' class='defaultExceedMultilineCls w-100' title='" +result[i].msgSubject+"' style='font-size: 13px;font-family: OpenSansRegular;-webkit-line-clamp: 1;margin-right: 2vh;'> " +result[i].msgSubject+"</div>" +
                      '<img id="attach_'+result[i].msgid+'" src="/images/conversation/attach.svg" class="mr-2 d-none" style="height: 16px; width: 16px;">'+
                  "</div>" +
              "</div> " +
            "</div>" +
            "</div>" +
          "</div>"
        }
        }
        //$("#mailCount").text(result.length);
        $("#zohoMail").html(ui);
        for(var i=0;i<result.length;i++){
          if(result[i].hasAttachment=="1"){
            $("#attach_"+result[i].msgid).addClass("d-flex");
          }
       }
      }else{
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
      }
      //openMail(this,result[0].msgid); 
       $("#zohoMail div:first-child").trigger('onclick');     
          }
     });

 }

 function emailDiv(){
  var ui = "";

  ui="<div id='emailMainDiv' style='height:100%'"
      
    
  +"</div>"  
     
  
  $("#content").html(ui);
}

function mailboxes(){
    if($('#emailbox2').is(':hidden')){
        $('#emailbox2').show();
        $('#emailbox').removeClass('col-lg-9 col-md-8').addClass('col-lg-7 col-md-5');
    }else{
        $('#emailbox2').hide();
        $('#emailbox').removeClass('col-lg-7 col-md-5').addClass('col-lg-9 col-md-8');
    }
  
}

function emailLayout(){
 var senttype = "new";
  var ui = "";
  ui='<div class="container-fluid" style="height:100%;">'
  +'<div class="row" style="height:100%;">'
      
      +'<div id="emailbox2" class="col-lg-2 col-md-3 p-0 h-100 border border-top-0 border-left-0 border-bottom-0" style="display: none;width: -webkit-fill-available;">'
          +'<div class="d-flex align-items-center font-weight-bold float-left ml-1  border border-top-0 border-left-0 border-right-0" style="font-size: 12px;color: gray;width: -webkit-fill-available;">Mailboxes</div>'
         
          +'<div class="d-flex flex-column align-items-center px-2 cursor" style="width: -webkit-fill-available;">'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Inbox</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Flagged</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Drafts</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Sent</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Junk</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">Bin'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">All Mail</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'

          +'</div>'

          +'<div class="mt-1 d-flex align-items-center font-weight-bold float-left ml-1  border border-left-0 border-right-0" style="font-size: 12px;color: gray;width: -webkit-fill-available;">Smart Mailboxes</div>'
          +'<div class="mt-1 d-flex align-items-center font-weight-bold float-left ml-1" style="font-size: 12px;color: gray;width: -webkit-fill-available;">'+userEmail+'</div>'

          +'<div class="d-flex flex-column align-items-center px-2 cursor" style="width: -webkit-fill-available;">'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Important</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Personal</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Receipts</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
              +'<div class="w-100 mt-1 d-flex align-items-center ml-2 mr-1 cursor" style="font-size: 12px;">'
                  +'<img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Work</span>'
                  +'<div class="ml-auto float-right badge mr-1" style="background-color: grey;height: 16px;">22</div>'
              +'</div>'
          +'</div>'

      +'</div>'
      +'<div class="col-lg-3 col-md-4 p-0 h-100 border border-top-0 border-left-0 border-bottom-0" style="">'
       
        // +'<ul class="list-group">'
          +'<div  style="height: 49px;" class="d-flex align-items-center p-2 border border-top-0 border-left-0 border-right-0">'
              +'<div class="mr-auto font-weight-bold" style="">'
                  +'<img class="mr-1 cursor" src="/images/menus/hamburger.svg" onclick="mailboxes();" title="" style="height: 20px; width:20px;">'
                  +'<span id="">Inbox</span>'
              +'</div>' 
              +'<div class="mr-2 cursor" style=""><img src="/images/conversation/edit.svg" onclick="replyToEmail(\'new\');" title="" style="height: 20px; width:20px;"></div>'  
              +'<div class="mr-2 cursor" style=""><img src="/images/menus/filter.svg" onclick="" title="" style="height: 20px; width:20px;"></div>'
              +'<div class="mr-2 cursor" style=""><img src="/images/menus/search2.svg" onclick="" title="" style="height: 20px; width:20px;"></div>'
          +'</div>'
        // +'</ul>'
        +'<div class="wsScrollBar " id="zohoMail" style="height: calc(100vh - 49px - 91px) !important;overflow-y: auto;">'
        
      +'</div>'
      +'</div>'
      +'<div id="emailbox" class="col-lg-9 col-md-8 p-0" style="height:100%;">'
          +'<div id ="convid" class="" style="">'
            
          +'</div>'
          +'<div id ="convid2" class="" style="display: none;height: 60px;">'
            
          +'</div>'
          +'<div id ="convMsg" class="wsScrollBar" style="height: calc(100vh - 91px) !important;overflow-y: auto;">'
            +'<div id ="convMsg3" class="wsScrollBar p-2" style="height: 100%; overflow-y: auto;background-color:#f0f0f0; ">'
            +'</div>'
            // var ui = "";
            +'<div class="mb-0 w-100 d-none align-items-center" style="height: 25px;">'
                +'<div class=" d-flex align-items-center" style="width: 85%;">'
                  +"<img src=\"images/cme/reply.svg\" title=\"Reply\" onclick='event.stopPropagation();' id='' class=\"image-fluid cursor float-left\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
                  +'<textarea placeholder="reply to: Nivin" class="rounded" style="height: 25px;font-size:13px;resize: none;width: 100%;background-image: none;"></textarea>'
                +'</div>'
                +'<div class="ml-auto">'
                  +"<img src=\"images/conversation/expand.svg\" title=\"Reply\" onclick='event.stopPropagation();' id='' class=\"image-fluid cursor mr-2\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
                  +"<img src=\"images/conversation/smile.svg\" title=\"Reply\" onclick='event.stopPropagation();' id='' class=\"image-fluid cursor mr-2\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
                  +"<img src=\"images/conversation/expand.svg\" title=\"Reply\" onclick='event.stopPropagation();' id='' class=\"image-fluid cursor mr-3\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
                +'</div>'
              +'</div>'


          +'</div>'
          +'<div id ="convMsg2" class="wsScrollBar" style="display: none;overflow-y: auto; background-color:#f0f0f0; height: calc(100vh - 91px) !important;">'
            
          


          +'</div>'
      
         
      +'</div>'
  +'</div>'
  +'</div>'
  $("#emailMainDiv").html(ui); 
  }

  function getEmailFromDB(){
    

    $.ajax({
     url: apiPath+"/"+myk+"/v1/getEmails",
     type: "POST",
     dataType:"json",
     beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },	
     error: function(jqXHR, textStatus, errorThrown) {
        checkError(jqXHR,textStatus,errorThrown);
        $("#loadingBar").hide();
        timerControl("");
        }, 
     success:function(result){

     }

    });

  }

  function getEmailconfig(){
    let jsonbody={
      "companyId":companyIdglb
  
    };
    
    $.ajax({
      url: apiPath+"/"+myk+"/v1/getEmailConfig",
      type:"POST",
      dataType:'json',
      contentType:"application/json",
      data: JSON.stringify(jsonbody),
      beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
     },	
     error: function(jqXHR, textStatus, errorThrown) {
        checkError(jqXHR,textStatus,errorThrown);
        $("#loadingBar").hide();
        timerControl("");
        }, 
     success:function(result){
      if(result.length>0){
        if(result[0].status =='Y'){
          $("#configuredEmailImg").attr("src","/images/onSwitch.png");
          $("#emailStatus").attr("class","roundImgGreenConfig");
          $("#colabusEmailId").val(result[0].configEmailId);
        }else{
          $("#configuredEmailImg").attr("src","/images/offSwitch.png");
          $("#emailStatus").attr("class","roundImgRedConfig");
          $("#colabusEmailId").val("");
        }
      }
     }

    });

  }

  function savetEmailConfig(status){
  
    var email = $("#colabusEmailId").val();
    
    let jsonbody={
      "companyId":companyIdglb,
      "status":status
      
    };
    $.ajax({
      url: apiPath+"/"+myk+"/v1/savetEmailConfig",
      type:"POST",
      contentType:"application/json",
      data: JSON.stringify(jsonbody),
      error: function(jqXHR, textStatus, errorThrown) {
                  checkError(jqXHR,textStatus,errorThrown);
                  $("#loadingBar").hide();
          timerControl("");
          }, 
      success:function(result){
          $("#configuredEmailImg").attr("src","/images/offSwitch.png");
          $("#emailStatus").attr("class","roundImgRedConfig");
          alertFun("Zoho Mail unconfigured Successfully.",'warning');
          
         }
       });
  
   }

function createNewUser(){
  $('#newWSUiPopup').append(userDetailPopup("","createcontact")).show();
}

function changeGrantaccess(){
  if($("#newUserGrantaccess").attr('src').indexOf('uncheck.svg') != -1){
      $("#newUserGrantaccess").attr('src','images/task/check.svg');
      $("#newUserGrantaccess").attr('gAccess','check');
      conFunNew(getValues(companyAlerts,"Alert_UsrGrantedToCrt"),'warning','','changeGrantaccess');
  }else{
      $("#newUserGrantaccess").attr('src','images/task/uncheck.svg');
      $("#newUserGrantaccess").attr('gAccess','');
  }
}

function createNewContact(){
  var cFname = $("#contactFirstname").val();
  var cLname = $("#contactLastname").val();
  var cEmail = $("#contactEmail").val();
  var cUserid = $("#contactUserid").val();
  var cRole = $('#contactRole :selected').val();
  var cSelpermission = $('#contactSelpermission :selected').val();
  var cGrantaccess = $("#newUserGrantaccess").attr('gaccess');
  var cComment = $("#contactComment").text();
  if(cComment.trim()==""){
      cComment = $("#contactComment").attr("placeholder");
  }
  console.log(cFname+"<--->"+cLname+"<--->"+cEmail+"<--->"+cUserid+"<--->"+cGrantaccess+"<--->"+cComment);
  if(cFname.trim()==""){
      alertFunNew(getValues(companyAlerts,"Alert_FirNmeNoEmt"),'error');
  }else if(cEmail.trim()==""){
      alertFunNew(getValues(companyAlerts,"Alert_EmailEmpty"),'error');
  }else if(isEmail(cEmail)==false){
      alertFunNew(getValues(companyAlerts,"Alert_invalidEmail"),'error');
  }else if(cUserid.trim()==""){
      alertFunNew(getValues(companyAlerts,"Alert_UserIdEmpty"),'error');
  }else if($('#contactRole :selected').val()=="0"){
      alertFunNew(getValues(companyAlerts,"Alert_SelectRole"),'error');
  }else if($('#contactSelpermission :selected').val()=="0"){
      alertFunNew(getValues(companyAlerts,"Alert_SelectPermission"),'error');
  }else{
    console.log("enter else block");
      let jsonbody = {
          "fname" : cFname,
          "lname" : cLname,
          "email" : cEmail,
          "welcomeText" : cComment,
          "role_id" : cRole,
          "contactPermission" : cSelpermission,
          "user_id" : cUserid,
          "userAddPermission" : cGrantaccess,
          "loginUser" : userIdglb,
          "companyId" : companyIdglb,
          "headers" : apiPath
      }
      console.log(jsonbody);
      console.log(apiPath+"<--->"+myk);
      // checksession();
      // console.log("menuuplace-->"+menuuplace);
      // console.log(JSON.stringify(jsonbody));
      $('#loadingBar').addClass('d-flex').removeClass('d-none');
      $.ajax({
          url:apiPath+"/"+myk+"/v1/createNewUser",
          type:"POST",
          ////dataType:"json",
          contentType: "application/json",
          data: JSON.stringify(jsonbody),
          error: function(jqXHR, textStatus, errorThrown) {
                   checkError(jqXHR,textStatus,errorThrown);
                   $('#loadingBar').addClass('d-none').removeClass('d-flex');
          },
          success:function(result){
              var newuserid = result.split("@@")[1];
              var status = result.split("@@")[0];
              var host = apiPath.split("//")[1];
              //////getUserDetails(newuserid,menuuplace,"newuser");
              if(status=="success"){
                  closeUserdet();
                  registerUserInXmpp(userIdglb,newuserid,host);
                  uploadProfilePic11(newuserid);
                  conFunNew(getValues(companyAlerts,"Alert_UsrCrtAddMore"),'warning','createNewUser');
                  $('#loadingBar').addClass('d-none').removeClass('d-flex');
                  statusCme();
              }else{
                  // closeUserdet();
                  // conFunNew(getValues(companyAlerts,"Alert_UsrSameId"),'warning');
                  alertFunNew(getValues(companyAlerts,"Alert_UsrSameId"),'error');
                  // alertFun(getValues(companyAlerts,"Alert_UsrSameId"),'warning');
                  $('#contactUserid').css('border','1px solid red');
                  $('#loadingBar').addClass('d-none').removeClass('d-flex');
              }
              
          } 
          
      }); 
  }

}

function uploadProfilePic11(userId){
  // if(profileBlobData==""){
  //   var canvas = $("#upload_Newprofile11")[0];
  //   var context = canvas.getContext('2d');
  //   var dataURL = context.canvas.toDataURL("image/jpeg", 1);
  //   var data = atob(dataURL.substring("data:image/jpeg;base64,".length)),
  //   asArray = new Uint8Array(data.length);

  //   for (var i = 0, len = data.length; i < len; ++i) {
  //     asArray[i] = data.charCodeAt(i);
  //   }
  //   profileBlobData = new Blob([asArray.buffer], { type: "image/jpeg" });
  //   // profileBlobData=blob;
  //   fileUploadType = "";
  // }

  console.log(fileUploadType);
  var data = new FormData();
      data.append('file', profileBlobData);
      data.append("place", "profile");
      data.append("resourceId",userId);
      data.append("user_id",userId);
      data.append("company_id",companyIdglb);
      // if(fileUploadType == ""){
      //   data.append("taskType",fileUploadType);
      // }else{
      //   data.append("taskType",fileUploadType);
      // }
      
      
      $.ajax({
        url: apiPath + "/" + myk + "/v1/upload",			      
        type:"POST",
        processData: false,
        contentType: false,
        data: data,
        cache: false,
        mimeType: "multipart/form-data",
        error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          
        },
        success: function(result) {		         
              
          // globUrl = result;
          // console.log(globUrl);
        }
        
     });
}


function registerUserInXmpp(createdBy,newuserid,host){

    $.ajax({
        url:apiPath + "/" + myk + "/v1/xmppNewCompanyUser?userId="+createdBy+"&newUserId="+newuserid+"&host="+host,
        type:"PUT",
        ////dataType:"json",
        // contentType: "application/json",
        // data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){
            // var newuserid = result.split("@@")[1];
            // var status = result.split("@@")[0]
            // //////getUserDetails(newuserid,menuuplace,"newuser");
            // if(status=="success"){
            //     closeUserdet();
            //     registerUserInXmpp(userIdglb,newuserid,"testbeta.colabus.com");
            //     conFunNew(getValues(companyAlerts,"Alert_UsrCrtAddMore"),'warning','createNewUser');
            //     $('#loadingBar').addClass('d-none').removeClass('d-flex');
            // }else{
            //     // closeUserdet();
            //     // conFunNew(getValues(companyAlerts,"Alert_UsrSameId"),'warning');
            //     alertFunNew(getValues(companyAlerts,"Alert_UsrSameId"),'error');
            //     // alertFun(getValues(companyAlerts,"Alert_UsrSameId"),'warning');
            //     $('#contactUserid').css('border','1px solid red');
            //     $('#loadingBar').addClass('d-none').removeClass('d-flex');
            // }
            
        } 
        
    }); 
}

function closeUserdet(){
  $('#newWSUiPopup').html("").hide();
  $("#transparentDiv").hide();
}

function uploadPicnew(){
  $("#profileFileNamenew:hidden").trigger('click');
}

function createusername(){
  var fname = $("#contactFirstname").val();
  var lname = $("#contactLastname").val();
  var name = fname+" "+lname;
  $('#newcreateprofile').attr('title', name).trigger('onerror');
}

function userDetailPopup(result,menuplace){

  var ui="";
  var name = "";var dept = "";var loginname = "";var mobile = "";var work = "";var email = "";
  var userimg = "";var workaddress = "";var homeaddress = "";var expertise = "";var bio = "";var homephone = "";
  if(menuplace=="contacts"){
      name = result[0].name;
      dept = result[0].user_department;
      loginname = result[0].login_name;
      mobile = result[0].user_phone;
      work = result[0].work_number;
      email = result[0].user_email;
      userimg = lighttpdpath+"/userimages/"+result[0].user_image_type;
      workaddress = result[0].user_work_address;
      homeaddress = result[0].user_home_address;
      expertise = result[0].user_expertise;
      bio = result[0].biography;
      homephone = result[0].user_phone;
  }
  
  var read = menuplace=="contacts"?"readonly":"";
  var header = menuplace=="contacts"?getValues(companyLabels,"User_details"):menuplace=="createcontact"?getValues(companyLabels,"CREATE_USER"):"";

  $("#transparentDiv").show();
  
  ui='<div class="modal d-flex justify-content-center" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' 
    +'<div class="modal-dialog" style="max-width: 80% !important;min-width: 45%;top: 5%;" >'
        +'<div class="modal-content container px-4" style="max-height: 89%;">' 
            +'<div class="modal-header py-2 px-0 mt-2" style="border-bottom: 1px solid #5e5b5b !important;">' 
                +'<span id="" class="modal-title defaultExceedCls" style="font-size: 14px;">'+header+'</span>' 
                if(menuplace == "createcontact"){
                  ui+="<img src='images/temp/scanqr.png' class='d-none' style='width:20px;height:20px;'>"
                }  
                ui+="<button type=\"button\" class=\"close\" onclick=\"closeUserdet();event.stopPropagation();\" id=\"\" style=\"right: 25px;top:20px;color: black;opacity: unset;padding: 0px;outline:none;\" data-dismiss=\"modal\">×</button>"
            +'</div>' 
            +'<div class="modal-body wsScrollBar px-0 py-0 d-xs-block d-sm-flex d-md-flex d-xl-flex" style="max-height: 85%;overflow: auto;">'
              if(menuplace == "createcontact"){
                ui+="<div class='my-1 col-xs-12 col-sm-4 col-md-4 col-xl-4 position-relative' style='height: 336px;text-align:center;background-color: #F2F2F2;'>"
                        +'<div class="mx-auto mt-2" style="width: 130px;height: 130px;background-color: #762165;border-radius: 12px;">'
                            +"<img id='newcreateprofile' class='rounded-circle cursor mt-3 mx-3' src='images/voiler_screen/userDummy.png' title='' onerror='userImageOnErrorReplace(this);' style='width: 74px;'>"
                            +'<div class="mt-3 cursor" onclick="uploadPicnew();" style="font-size: 9px;color: white;">Add Profile Picture</div>'

                            +"<form action='/UserProfileImageUpload' id='uploadUserProfileFormNew' name='uploadUserProfileFormNew' method='post' enctype='multipart/form-data'>"
                                +"<label style='width: 160px;display:none;' class='imageUpload'> <input style='left:0px;width:150px;top:0px;' type='file' multiple='multiple' id='profileFileNamenew' name='profileFileName' class='file' onchange='readURLNewProfile2(this);'>"
                                    +"<input style='left:0px;width:150px;top:0px;' type='hidden' id='albumFileName' name='albumFileName' />"
                                    +"<input style='left:0px;width:150px;top:0px;' type='hidden' id='albumFileExtn' name='albumFileExtn' />"
                                +"</label>"
                            +"</form> "
                            +"<iframe id='upload_targetNew' name='upload_targetNew' style='display: none;'></iframe>"
                                +"<input id='x1' type='hidden'  name='x1'>"
                                +"<input id='y1' type='hidden'  name='y1'>"
                                +"<input id='x2' type='hidden'  name='x2'>"
                                +"<input id='y2' type='hidden'  name='y2'>"

                        +'</div>'
                  // if(menuplace!="contacts"){
                  //     ui+="<img src='/images/task/upload_cir.svg' class='position-absolute' style='width:20px;height:20px;top: 0px;right: 50px;'>"
                  // }
                  ui+="</div>"
                  +"<div class='my-1 col-xs-12 col-sm-8 col-md-8 col-xl-8 wsScrollBar' style='height: 336px;text-align:center;overflow: auto;'>"
                      +'<div class="py-2 material-textfield">'
                          +'<input id="contactFirstname" onblur="createusername();" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;"/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Enter_First_Name")+'</label>'
                          +'<span class="mandatoryField" style="">*</span>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="contactLastname" onblur="createusername();" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;"/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Enter_Last_Name")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="contactEmail" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;"/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Enter_Email")+'</label>'
                          +'<span class="mandatoryField" style="">*</span>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="contactUserid" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;"/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Enter_User_Id")+'</label>'
                          +'<span class="mandatoryField" style="">*</span>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<select id="contactRole" class="px-2 py-0 material-textfield-input newinput w-100 userInput" onchange="" style="outline: none;">'
                              +"<option value='0'>"+getValues(companyLabels,"Select_Role")+"</option>"
                              +"<option value='4'>"+getValues(companyLabels,"Project_Administrator")+"</option>"
                              +"<option value='2'>"+getValues(companyLabels,"System_Administrator")+"</option>"
                              +"<option value='5'>"+getValues(companyLabels,"User")+"</option>"
                              +"<option value='8'>"+getValues(companyLabels,"Guest")+"</option>"
                          +'</select>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Select_Role")+'</label>'
                          +'<span class="mandatoryField" style="">*</span>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<select id="contactSelpermission" class="px-2 py-0 material-textfield-input newinput w-100 userInput" onchange="" style="outline: none;">'
                              +"<option value='0'>"+getValues(companyLabels,"Select_Permission")+"</option>"
                              +"<option value='4'>"+getValues(companyLabels,"Permit_Full_Access_to_Contacts")+"</option>"
                              +"<option value='2'>"+getValues(companyLabels,"Restrict_Contacts_to_Project_Teams")+"</option>"
                              +"<option value='5'>"+getValues(companyLabels,"No_Access_to_Contacts")+"</option>"
                          +'</select>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Select_Permission")+'</label>'
                          +'<span class="mandatoryField" style="">*</span>'
                      +'</div>'
                      +'<div class="py-2 d-flex align-items-center">'
                          +'<div class="" style="">'
                              +'<span id="" class="px-2 py-0 material-textfield-input newinput w-100 userInput border-0" style="">'+getValues(companyLabels,"System_grant")+'</span>'
                          +'</div>'
                          +'<div style="">'
                              +"<img id='newUserGrantaccess' gAccess='' src='images/task/uncheck.svg' onclick='changeGrantaccess();event.stopPropagation();' style='width:18px;height:18px;cursor:pointer;'>"
                          +'</div>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<textarea id="contactComment" class="px-2 py-2 material-textfield-input newinput w-100 userInpTextarea" placeholder="'+getValues(companyLabels,"Create_User_Text")+'"  style="outline:none;"></textarea>'
                          /* +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Enter_User_Id")+'</label>'
                          +'<span class="mandatoryField" style="">*</span>' */
                      +'</div>'


                  +"</div>"
              }else{
                ui+="<div class='my-1 col-xs-12 col-sm-4 col-md-4 col-xl-4 position-relative' style='height: 336px;text-align:center;background-color: #F2F2F2;'>"
                +'<div class="mx-auto mt-2" style="width: 130px;height: 130px;background-color: #762165;border-radius: 12px;">'
                    +"<img id='newcreateprofile' class='rounded-circle cursor mt-3 mx-3' src='"+userimg+"' title='"+name+"' onerror='userImageOnErrorReplace(this);' style='width: 74px;'>"
                    // +'<div class="mt-3 cursor" onclick="uploadPicnew();" style="font-size: 9px;color: white;">Add Profile Picture</div>'
                  if(menuplace!="contacts"){
                      ui+="<img src='/images/task/upload_cir.svg' class='position-absolute' style='width:20px;height:20px;top: 0px;right: 50px;'>"
                  }
                  ui+="</div>"
                  ui+="</div>"
                  +"<div class='my-1 col-xs-12 col-sm-8 col-md-8 col-xl-8 wsScrollBar' style='height: 336px;text-align:center;overflow: auto;'>"

                  // +"<div class='col-xs-12 col-sm-8 col-md-8 col-xl-8' style='text-align:center;'>"
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+name+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"User_name")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+loginname+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"User_login_name")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+email+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Email")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+dept+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Department")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+work+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Work_Phone")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+mobile+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Mobile")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+homephone+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Home_Phone")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+workaddress+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Office_Address")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+homeaddress+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Home_Address")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+expertise+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Expertise")+'</label>'
                      +'</div>'
                      +'<div class="py-2 material-textfield">'
                          +'<input id="" value="'+bio+'" class="px-2 py-0 material-textfield-input newinput w-100 userInput" placeholder=" "  style="outline:none;" '+read+'/>'
                          +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">'+getValues(companyLabels,"Biography")+'</label>'
                      +'</div>'

                  +"</div>"
              }
              

            ui+='</div>' //body 
            +'<div id="" class="modal-footer pb-3 pr-2">'
              if(menuplace == "createcontact"){
                  ui+="<div id=\"\" onclick=\"createNewContact('"+menuplace+"');event.stopPropagation();\" style=\"width: 70px;float: right;\" class=\"createBtn\">"+getValues(companyLabels,"Hash_create")+"</div>"
              }
            ui+="</div>" //footer
        +'</div>' //content
    +'</div>' //dialog
  +'</div>' //modal

  return ui;

} 

var loadProjectDataForDoc = "";
function copyMovePopup(id){
  // alert("inside");
  var ui ="";
  ui='<div class="SBactiveSprintlistCls" style="top:85px !important;" id="SBactiveSprintlistId" selectedstories="">'
			+'<div class="mx-auto w-50 modal-dialog11" style="max-width: 80%; min-height: 150px;height: calc(100% - 70px - 4px);">'
				+'<div id="addgrp1" class="modal-content11 container px-3" style="">'
					+'<div class="modal-header1122 pt-3" style="border-bottom: 1px solid #b4adad !important;">' 
						+'<h5 id="workspaceZone" onclick="showWorkspaces();" class="modal-title defaultExceedCls docactive" style="color:#a4a4a4;font-size: 14px !important;cursor:pointer;">Workspace</h5>'
            +'<h5 id="myZoneFold" onclick="showMyzoneFolder();" class="modal-title ml-2 defaultExceedCls" style="color:#a4a4a4;font-size: 14px !important;cursor:pointer;display:none;">My Zone</h5>'
            +'<img id="copyMoveBack" src="images/cme/leftarrow_blue.svg" title="Back" onclick="backToWorkspace();" class="image-fluid mt-0 mr-3 d-none" style="height:20px;cursor:pointer;">'
						+"<button type=\"button\" class=\"close\" onclick=\"closeCopyPopup();\" id=\"\" style=\"top: 1px;right: 5px;color: black;opacity: unset;padding: 0px;outline:none;\" data-dismiss=\"modal11\">×</button>"
          +'</div>' 
          

          +'<div class="modal-body11 wsScrollBar mb-2" id="folderListBlock" style="height: calc(100% - 40px);border-bottom: 1px solid #9c9c9c;">'
              // +'<div  style="width:100%;float:left;height: 280px;padding-top: 2%;padding-right: 2%;border-bottom: 1px solid #9c9c9c;padding-bottom: 2%;">'
                    +'<div id="projListInDoc" ></div>'
                    +'<hr id="hrLine" style="margin: -2px 0px;border-bottom: 3px solid #cccccc;">'
                    +'<div id="projListInDoc1" ></div>'
                    // +"<ul id='documentListUL_122' class='documentTree ' style=''></ul>"
                    // +'<div id="" style="width: 100%;float: left;">'
	        
	       //+'<div style="width: 99%;float: left;margin-left: 1%;border-bottom: 1px solid #9c9c9c;">'
		      
	          
	          

					
					
					// +'<div id="wsSaveUpdateDiv" class="modal-footer11 pb-3 d-flex justify-content-end">'
					// 	+"<div id=\"\" onclick=\"newemailbox();\" style=\" width: 100px !important;float: right;\" class=\"createBtn mt-0 mr-2\">Authenticate</div>"
					// 	+"<div id=\"editgrpbtn\"  onclick=\"GetEmailAccessToken();\" style=\" width: 70px !important;float: right;\" class=\" mt-0 createBtn SAVE_cLabelHtml\">Save</div>"
      		// 		+'</div>'
        //+'</div>'
        // +'</div>'
        // +'</div>'
      +'</div>'
      +'<div style="float: left;width: 100%;margin-bottom: 10px">'
            +'<div id="moveBtnFordoc" style=" width: 70px;float: right;" class="createBtn mt-0 mx-2" onclick="moveDocManualPro('+id+');"> Move </div>'
	          +'<div id="emailConfigSave" style=" width: 70px;float: right;" class="createBtn mt-0 mx-2" onclick="copyDocManualPro('+id+');"> Copy </div>'
            +'<div id="emailConfigCancel" style=" width: 70px;float: right;" class="createBtn mt-0 mx-2" onclick="closeCopyPopup();"> Cancel </div>'
	        
            +'</div>'
		+'</div>'
 
  $("#copyMovePopup").html(ui).show();
  $("#transparentDiv").show();
  loadProjectsInDoc();
 }

 function showWorkspaces(){
    $('#workspaceZone').addClass('docactive');
    $('#myZoneFold').removeClass('docactive');
    $('#hrLine').addClass('d-block').removeClass('d-none');
    $('#projListInDoc').addClass('d-block').removeClass('d-none');
    $('#projListInDoc1').addClass('d-block').removeClass('d-none');
    $('#copyMoveBack').addClass('d-block').removeClass('d-none');
    loadProjectsInDoc();
 }

 function showMyzoneFolder(){
    $('#workspaceZone').removeClass('docactive');
    $('#myZoneFold').addClass('docactive');
    $('#copyMoveBack').addClass('d-none').removeClass('d-block');
    $('#projListInDoc1').addClass('d-none').removeClass('d-block');
    $('#projListInDoc1').html('');
    $('#hrLine').addClass('d-none').removeClass('d-block');
    $('#projListInDoc').addClass('d-none').removeClass('d-block');
    $('#projListInDoc').html('');
    // loadMyZoneFoldersInCopyMove();
}

 function loadMyZoneFoldersInCopyMove(){
    // $("#documentListUL_111").html("");
    let jsonbody = { 
      "clientRepo": "N",
      "custom_drive_id": "0",
      "folder_id": "0",
      "user_id": userIdglb,
      "company_id": companyIdglb,
      "from": "CopyRMove",
      "localOffsetTime": localOffsetTime
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');

    $.ajax({
      url: apiPath+"/"+myk+"/v1/CopyMoveFolderList",
      type:"POST",
      dataType:'json',
      contentType:"application/json",
      data: JSON.stringify(jsonbody),
      error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          }, 
      success : function(result){
        // checkSessionTimeOut(result);
        console.log(result);
        // loadProjectDataForDoc = result;
        // if(loadProjectDataForDoc == ''){
        // $('#projListInDoc').html("<span>No projects found.</span>");
        // }else{
        //   $("#documentListUL_122").append();
        // }
        $('#loadingBar').addClass('d-none').removeClass('d-flex');

      
      }
    });

 }

 function loadProjectsInDoc(){
    let jsonbody = { 
      "user_id" : userIdglb,
      "sortType" : "",
      "type" : "", 
      "txt" : "", 
      "companyId" : companyIdglb, 
      "workspaceType" : "recent",      
      "filterVal" : "",
      "limit":""
    }
    checksession();
    $('#projListInDoc').html("");
    //timerControl("start"); 
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    $.ajax({
      url: apiPath+"/"+myk+"/v1/projectList",/// local : http://localhost:8080/v1/projectList server : apiPath+"/"+myk+"/v1/loadProjDDList"
      type:"POST",
      dataType:'json',
      contentType:"application/json",
      data: JSON.stringify(jsonbody),
      error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          }, 
      success : function(result){
        checkSessionTimeOut(result);
        console.log(result);
        loadProjectDataForDoc = result;
        if(loadProjectDataForDoc == ''){
        $('#projListInDoc').html("<span>No projects found.</span>");
        }else{
          prepareProjListForDocUI();
          prepareProjListForDocUI1();
        }
        $('#loadingBar').addClass('d-none').removeClass('d-flex');

      
      }
    });
 }

 function prepareProjListForDocUI() {
  var isiPad = navigator.userAgent.match(/iPad/i) != null;
	var UI = "";
	var recentFlag = false;
  $('#projListInDoc').html("");
	var projArray = loadProjectDataForDoc[0].publicProjectList;
	for (var i = 0; i < projArray.length ; i++) {
	    	var jsonData = projArray;
			// var projectID = jsonData[i].project_id;
		    // var ProjTitle = jsonData[i].project_name; ///projTitle
			// var funcFlag = "";//jsonData[i].funcFlag
			// var funClick = "";//jsonData[i].funClick
			// var imageUrl = jsonData[i].imageUrl; // url for images
			// var imageClass = jsonData[i].imageClass;
			// var projName = jsonData[i].project_name;
			// var projectArchiveStatus = jsonData[i].project_status;
			// var cancelL = jsonData[i].cancelL;
			// var ctxPath = jsonData[i].ctxPath;
		    // projSettingL = jsonData[i].projSettingL;
			// var projectUsersStatus = jsonData[i].proj_user_status;
			// projName=projName.replaceAll("ch(20)","'");

			var projectID = jsonData[i].project_id;
	    	var ProjTitle = jsonData[i].project_name;//jsonData[i].projTitle
			var funcFlag = "";//jsonData[i].funcFlag
			var funClick = "";//jsonData[i].funClick
			var imageUrl = jsonData[i].imageUrl; // url for images imageUrl
			var imageClass = "";///jsonData[i].imageClass
			var projName = jsonData[i].project_name; ///project_name ///projName
			var projectArchiveStatus = jsonData[i].project_user_status;///project_user_status///status
			var cancelL = "";///jsonData[i].cancelL
			var ctxPath = jsonData[i].ctxPath;
	    	// projSettingL = "";///jsonData[i].projSettingL
			var projectUsersStatus = jsonData[i].project_user_role; ///project_user_role//proj_user_status
			var projAccessStatus = jsonData[i].projAccessStatus;
			projName=projName.replaceAll("ch(20)","'");


			if(jsonData[i].project_status=="I"){
				ProjTitle = "Click to unlock"
			}


			// if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="PO" && jsonData[i].project_status=="A" )){
			// 	funcFlag = "psubscribed";
			// 	funClick = "readMoreProjects(this);";
			// }
	
			if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="I" )){
				// funcFlag = "pLocked";
				// funClick = "readMoreProjects(this);";	
				imageUrl = imageUrl+(""+ctxPath+"/images/Projects/locked_project.png");
				imageClass ="display:none";
				// funClick = "unlockProject(this);";
			}
	
			// if( ( projectUsersStatus=="TM" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="SU" && jsonData[i].project_status=="Y" )){
			// 	funcFlag = "psubscribed";
			// 	funClick = "readMoreProjects(this);";
			// }
				let projArchStatus =jsonData[i].project_status;
				let projectType = "";
				if(projectUsersStatus == "PO"){
					projectType="MyProjects";
				}else{
					projectType="SharedProjects";
				} 
				let prjImagePath =jsonData[i].imageUrl;

		    if((jsonData[i].project_status !="I" && typeof projectId == 'undefined') || (jsonData[i].projStatus !="I" && projectId != projectID) ){
		        recentFlag = true;
		        
		        UI += "	 <div id=\"proj_"+projectID+"\" class='d-flex justify-content-left align-items-center w-100 p-1 text-dark' style=\"border-bottom:1px solid #cccccc;height: 40px;cursor:pointer;\" onclick='openFolder( "+"\""+projectID+"\")'>"
		         if(projectUsersStatus=="PO"){
				  UI+=" 	<div id=\"projectName_"+ projectID+ "\"  style=\"display:none;\" >"+ projName+ "</div>"
					+ " 	<div id=\"projectType_"+ projectID+ "\"  style=\"display:none;\" >MyProjects</div>"
					+ " 	<div id=\"projectid"+ projectID+ "\" value=\""+projectID+"\"  style=\"display:none;\" ></div>"
					+ " 	<div id=\"projectArchiveStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectArchiveStatus+ "</div>"
					+ " 	<div id=\"projectUsersStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectUsersStatus+ "</div>";
	             }else{
	             	
	              UI+="		<div id=\"projectName_"+ projectID+ "\"  style=\"display:none;\" >"+ projName+ "</div>"
					+ " 	<div id=\"projectType_"+ projectID+ "\"  style=\"display:none;\" >SharedProjects</div>"
					+ " 	<div id=\"projectid"+ projectID+ "\" value=\""+projectID+"\"  style=\"display:none;\" ></div>"
					+ " 	<div id=\"projectArchiveStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectArchiveStatus+ "</div>"
					+ " 	<div id=\"projectUsersStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectUsersStatus+ "</div>";
	             }
		        UI += "	    <img id=\"projectImageId_"+ projectID+ "\" class='lozad' style=\"height: 27px;width: 27px;margin-top: 1%;border: 1px solid #cccccc;\" title=\""+ProjTitle+"\" onerror=\"userImageOnErrorReplace(this);\" src='/images/workspace/projectImage.svg' data-src=\""+ imageUrl+"?"+d.getTime()+ "\" >"
		        UI += "	    <span class='defaultExceedCls ml-2 mt-1' style=\"width:78%;font-size:13px;\" title=\""+ProjTitle+"\">"+projName+"</span>"
		        UI += "	 </div>"
		    
		    }
	}
    UI=UI.replaceAll("ch(20)","'").replaceAll("ch(30)","chr(dbl)").replaceAll("ch(50)","[").replaceAll("ch(51)","]").replaceAll("ch(curly)","{").replaceAll("ch(clcurly)","}").replaceAll("ch(backslash)","\\");	
	$('#projListInDoc').append(UI);
	observer.observe();//---- to lazy load user images
 }

 function prepareProjListForDocUI1(){
	var UI = "";
	var recentFlag = false;
  $('#projListInDoc1').html("");
	var projArray = loadProjectDataForDoc[0].myProjectList;
	for (var i = 0; i < projArray.length ; i++) {
	    	var jsonData = projArray;
			// var projectID = jsonData[i].project_id;
		    // var ProjTitle = jsonData[i].projTitle;
			// var funcFlag = jsonData[i].funcFlag;
			// var funClick = jsonData[i].funClick;
			// var imageUrl = jsonData[i].imageUrl; // url for images
			// var imageClass = jsonData[i].imageClass;
			// var projName = jsonData[i].project_name;
			// var projectArchiveStatus = jsonData[i].project_status;
			// var cancelL = jsonData[i].cancelL;
			// var ctxPath = jsonData[i].ctxPath;
		    // projSettingL = jsonData[i].projSettingL;
			// var projectUsersStatus = jsonData[i].proj_user_status;
			// projName=projName.replaceAll("ch(20)","'");



			var projectID = jsonData[i].project_id;
	    	var ProjTitle = jsonData[i].project_name;//jsonData[i].projTitle
			var funcFlag = "";//jsonData[i].funcFlag
			var funClick = "";//jsonData[i].funClick
			var imageUrl = jsonData[i].imageUrl; // url for images imageUrl
			var imageClass = "";///jsonData[i].imageClass
			var projName = jsonData[i].project_name; ///project_name ///projName
			var projectArchiveStatus = jsonData[i].project_user_status;///project_user_status///status
			var cancelL = "";///jsonData[i].cancelL
			var ctxPath = jsonData[i].ctxPath;
	    	projSettingL = "";///jsonData[i].projSettingL
			var projectUsersStatus = jsonData[i].project_user_role; ///project_user_role//proj_user_status
			var projAccessStatus = jsonData[i].projAccessStatus;
			projName=projName.replaceAll("ch(20)","'");
			let projArchStatus =jsonData[i].project_status;
			let projectType = "";
			if(projectUsersStatus == "PO"){
			projectType="MyProjects";
			}else{
				projectType="SharedProjects";
			} 
			let prjImagePath =jsonData[i].imageUrl;

			if(jsonData[i].project_status=="I"){
				ProjTitle = "Click to unlock"
			}


			// if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="PO" && jsonData[i].project_status=="A" )){
			// 	funcFlag = "psubscribed";
			// 	funClick = "readMoreProjects(this);";
			// }
	
			if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="I" )){
				// funcFlag = "pLocked";
				// funClick = "readMoreProjects(this);";	
				imageUrl = imageUrl+(""+ctxPath+"/images/Projects/locked_project.png");
				imageClass ="display:none";
				// funClick = "unlockProject(this);";
			}
	
			// if( ( projectUsersStatus=="TM" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="SU" && jsonData[i].project_status=="Y" )){
			// 	funcFlag = "psubscribed";
			// 	funClick = "readMoreProjects(this);";
			// }




		    if((jsonData[i].project_status !="I" && typeof projectId == 'undefined') || (jsonData[i].projStatus !="I" && projectId != projectID) ){
		        recentFlag = true;
		        
		        UI += "	 <div id=\"proj_"+projectID+"\" class='d-flex justify-content-left align-items-center w-100 p-1 text-dark' style=\"border-bottom:1px solid #cccccc;height: 40px;cursor:pointer;\"  onclick='openFolder( "+"\""+projectID+"\")'>"
		         if(projectUsersStatus=="PO"){
				  UI+=" 	<div id=\"projectName_"+ projectID+ "\"  style=\"display:none;\" >"+ projName+ "</div>"
					+ " 	<div id=\"projectType_"+ projectID+ "\"  style=\"display:none;\" >MyProjects</div>"
					+ " 	<div id=\"projectid"+ projectID+ "\" value=\""+projectID+"\"  style=\"display:none;\" ></div>"
					+ " 	<div id=\"projectArchiveStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectArchiveStatus+ "</div>"
					+ " 	<div id=\"projectUsersStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectUsersStatus+ "</div>";
	             }else{
	             	
	              UI+="		<div id=\"projectName_"+ projectID+ "\"  style=\"display:none;\" >"+ projName+ "</div>"
					+ " 	<div id=\"projectType_"+ projectID+ "\"  style=\"display:none;\" >SharedProjects</div>"
					+ " 	<div id=\"projectid"+ projectID+ "\" value=\""+projectID+"\"  style=\"display:none;\" ></div>"
					+ " 	<div id=\"projectArchiveStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectArchiveStatus+ "</div>"
					+ " 	<div id=\"projectUsersStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectUsersStatus+ "</div>";
	             }
		        UI += "	    <img id=\"projectImageId_"+ projectID+ "\" class='lozad' style=\"height: 27px;width: 27px;margin-top: 1%;border: 1px solid #cccccc;\" onerror=\"userImageOnErrorReplace(this);\" title=\""+ProjTitle+"\" src='/images/workspace/projectImage.svg' data-src=\""+ imageUrl+"?"+d.getTime()+ "\" >"
		        UI += "	    <span class='defaultExceedCls ml-2 mt-1' style=\"width:78%;font-size:13px;\"  title=\""+ProjTitle+"\">"+projName+"</span>"
		        UI += "	 </div>"
		    
		    }
	}
    UI=UI.replaceAll("ch(20)","'").replaceAll("ch(30)","chr(dbl)").replaceAll("ch(50)","[").replaceAll("ch(51)","]").replaceAll("ch(curly)","{").replaceAll("ch(clcurly)","}").replaceAll("ch(backslash)","\\");	
	$('#projListInDoc1').append(UI);
	observer.observe();//---- to lazy load user images
  }
  var projectIdForCopyMove = "";
  function openFolder(projId){
    $('#projListInDoc1').addClass('d-none').removeClass('d-block');
    $('#hrLine').addClass('d-none').removeClass('d-block');
    $('#projListInDoc').addClass('d-none').removeClass('d-block');
    $('#copyMoveBack').addClass('d-block').removeClass('d-none');
    if($("#documentListUL_111").length){
      $("#documentListUL_111").remove();
    }
    projectIdForCopyMove = projId;
    // $("#documentListUL_111").html("");
        let jsonbody={
          "clientRepo" : "N",
          "driveId" : "0",
          "filtervalue" : "",
          "txt" : "",
          "status" : "",
          "ipadVal" : "NO",
          "projec_id" : projId,
          "user_id" : userIdglb,
          "limitIndex" : 0,
          "limitLength" : 100,
          "localOffsetTime" : localOffsetTime
      }
      $('#loadingBar').addClass('d-flex').removeClass('d-none');  
      $.ajax({
          url: apiPath + "/" + myk + "/v1/getFolders",
          type: "POST",
          dataType: 'json',
          contentType: "application/json",
          data: JSON.stringify(jsonbody),
          error: function (jqXHR, textStatus, errorThrown) {
              checkError(jqXHR, textStatus, errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');  
          },
          success: function (result) {
              $('#folderListBlock').append("<ul id='documentListUL_111' class='documentTree ' style=''></ul>");
              if(result!=""){
                  console.log("In side If");
                  $("#documentListUL_111").append(documentListingUI(result,"","copyMove")).show();
              }else{
                  $("#documentListUL_111").html("<div class='' style='text-align:center;font-size:12px;margin-top: 25px;'>No Document Found</div>");
              }
              $('.documentTree').simpleTreeMenu();
              // listDocDrive();
              $('#loadingBar').addClass('d-none').removeClass('d-flex');  
              // nodeDragnDropDoc();
          }
      });
  }
  var folderIdddsss = "";

  function moveDocManualPro(docid) {
    console.log(docid);
    if(folderIdddsss==""){
      console.log("not select foleders for move");
      alertFunNew(getValues(companyAlerts,"Alert_selectFolder"),'warning');
      return false;
    }else{
      folderIdddsss=folderIdddsss.split('_')[1];
    }

    console.log(folderIdddsss+"<-->"+projectIdForCopyMove);
      
      let jsonbody={
        "folder_id" : folderIdddsss,
        "document_id" : docid,
        "company_id" : companyIdglb,
        "user_id" : userIdglb,
        "projec_id" : projectIdForCopyMove
    }
    console.log(JSON.stringify(jsonbody));
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
    $.ajax({
        url: apiPath + "/" + myk + "/v1/WsMoveTofolder",
        type: "POST",
        // dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            console.log("HEHEHE");
            $('#loadingBar').addClass('d-none').removeClass('d-flex');  
        },
        success: function (result) {
          console.log(result);
            if(result == 'failure'){
              alertFunNew(getValues(companyAlerts,"Alert_DocumentExists"),'warning');
            }else if(result == 'docNameexists'){
              alertFunNew(getValues(companyAlerts,"Alert_documentNameExists"),'warning');
            }else if(result == 'success'){
              alertFunNew(getValues(companyAlerts,"Alert_DocMoved"),'warning');
                $('#document_'+docid).remove();
                $("#copyMovePopup").hide();
                $("#transparentDiv").hide();
            }else{
              $("#copyMovePopup").hide();
              $("#transparentDiv").hide();				  		
            }
              $('#loadingBar').addClass('d-none').removeClass('d-flex'); 
        }
    });
  }

  function copyDocManualPro(docid) {
    console.log(docid);
      if(folderIdddsss==""){
        console.log("not select foleders for copy");
        alertFunNew(getValues(companyAlerts,"Alert_selectFolder"),'warning');
        return false;
      }else{
        folderIdddsss=folderIdddsss.split('_')[1];
      }

      console.log(folderIdddsss+"<-->"+projectIdForCopyMove);
      
      let jsonbody={
        "folder_id" : folderIdddsss,
        "document_id" : docid,
        "company_id" : companyIdglb,
        "user_id" : userIdglb,
        "projec_id" : projectIdForCopyMove
    }
    console.log(JSON.stringify(jsonbody));
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
    $.ajax({
        url: apiPath + "/" + myk + "/v1/WsCopyTofolder",
        type: "POST",
        // dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            console.log("HEHEHE");
            $('#loadingBar').addClass('d-none').removeClass('d-flex');  
        },
        success: function (result) {
              console.log(result);
              if(result=="success"){
                alertFunNew(getValues(companyAlerts,"Alert_DocCopied"),'warning');
                $("#copyMovePopup").hide();
                $("#transparentDiv").hide();
              }else if(result=="docNameexists"){
                alertFunNew(getValues(companyAlerts,"Alert_documentNameExists"),'warning');
              }else if(result=="failure"){
                alertFunNew(getValues(companyAlerts,"Alert_DocumentExists"),'warning');
              }else{
                $("#copyMovePopup").hide();
                $("#transparentDiv").hide();
              }
              $('#loadingBar').addClass('d-none').removeClass('d-flex'); 
        }
    });


  }

  function backToWorkspace(){
    $("#documentListUL_111").remove();
    $('#projListInDoc1').addClass('d-block').removeClass('d-none');
    $('#hrLine').addClass('d-block').removeClass('d-none');
    $('#projListInDoc').addClass('d-block').removeClass('d-none');
    $('#copyMoveBack').addClass('d-none').removeClass('d-block');
  }

 function closeCopyPopup(){
   $("#copyMovePopup").hide();
   $("#transparentDiv").hide();
 }

var mailmsgid;
function openMail(obj){
  $('.emailactive').removeClass('emailactive2');
  $(obj).addClass('emailactive2');
  backToEmail();
  $("#convid").html("");
  $("#convMsg3").html("");
  var sentmtype="reply";
   var result =msgBox;
    //var from = $("#msg_"+msgid).attr("from");
 
    var msgid = $(obj).attr("msgid");
    var msgThread = $(obj).attr("threadid");
    
    if(result.length>0){
      var ui="";
      var d;
      var time;
       var imgName = lighttpdpath+"/userimages/";
       var senderid;

       const currd = new Date();
      let currmonth = months[currd.getMonth()];
      let currdate1 = currd.getDate();
      var currti=  currmonth+", "+currdate1;
        for(var i=0;i<result.length;i++){
          d= new Date(parseInt(result[i].timeStamp));
          var timestamp = d.getHours()+":"+d.getMinutes()+":"+d.getMilliseconds();
          var ti=  calculateElapseTime(timestamp);
          //var ti=  timestamp;
          
          if(result[i].msgid == msgid){
             senderid = result[i].fromId;
            var ui ="";
            /*ui ='<div id="emailheader"  style="" class="d-flex align-items-center p-2 border border-top-0 border-right-0 border-left-0">'
                  +'<div class="d-flex align-items-center mr-auto" style="">'
                      +'<img id="imagedisplayhead"  src="'+imgName+'" title="'+result[i].sender+'" onerror="userImageOnErrorReplace(this);" alt="'+result[i].sender+'" class="mx-2 my-1 mt-0 rounded-circle" style="width:35px;">'
                      +'<div class="font-weight-bold pl-1" style="">'+result[i].sender+''
                      +'</div>'
                  +'</div>' 
                    +'<img class="mr-2 cursor d-none" src="/images/menus/search2.svg" onclick="" title="" style="height: 20px; width:20px;">'
                    +'<img class="mr-2 cursor d-none" src="/images/conversation/edit.svg" onclick="" title="" style="height: 20px; width:20px;">'  
                    +'<img class="mr-2 cursor d-none" src="/images/menus/more_blue.svg" onclick="" title="" style="height: 20px; width:20px;">'
             +'</div>'
            $("#convid").append(ui);
            */
          }

        }

        for(var i=0;i<result.length;i++){
          
          d= new Date(parseInt(result[i].dat));
          var timestamp = d.getHours()+":"+d.getMinutes()+":"+d.getMilliseconds();
          
          
          let month = months[d.getMonth()];
          
          let date1 = d.getDate();
          var ti=  month+", "+date1;
          if(currti == ti){
            ti= "Today, "+ d.getHours()+":"+d.getMinutes();
          }else{
            ti;
          }
          if(msgThread == "undefined" ){
            if(result[i].msgid == msgid){
              
              uim ='<div  class="row mx-2 my-2" style="box-shadow:0 6px 12px rgb(0 0 0 / 18%);position: relative;background-color:#fff;border:1px solid #d7d7d7;border-radius:2px;">'
                    +'<div class="d-none justify-content-between py-2 px-3 w-100" style="font-size: 13px;background: #E8E8E8;">'
                        +'<div style="color: #7d7d7d;">Found in Sent</div>'
                        +'<img src="/images/conversation/post1.svg" style="height: 20px;width: 20px;">'
                    +'</div>'

                    +'<div  class="col-12 px-3 py-2" onmouseover="showEmailmoreoptions(this);event.stopPropagation();" onmouseout="hideEmailmoreoptions(this);event.stopPropagation();" style="">'
                      +'<div class="row m-0 p-0 w-100" style="font-size: 12px;">'
                          +'<div class="col-lg-11 p-0 d-flex flex-column">'
                                +'<div class="d-flex justify-content-between align-items-center">'
                                    +'<div class=" font-weight-bold" style="font-size: 14px;" >'+result[i].sender+'</div>'
                                    +'<div id="mailAttachExist_'+result[i].msgid+'" class="ml-3 mr-auto" style="display:none;">'
                                        +'<img src="/images/conversation/attach.svg" style="height: 16px; width:16px;">'
                                    +'</div>'
                                    +'<div class="mx-1" style="white-space: nowrap;">'+ti+'</div>'
                                +'</div>'
                                +'<div class=" mt-1" style="line-height: 14px;">'+result[i].msgSubject+'</div>'
                                +'<div class="mt-1 d-flex">'
                                      +'<div class="mr-2" style="color:#818181;">To:</div>'
                                      +'<div class="">'+result[i].msgto+'</div>'
                                +'</div>'
                                +'<div id="mailCC_'+result[i].msgid+'" class="mt-1 d-flex">'
                                      +'<div class="mr-2" style="color:#818181;">CC:</div>'
                                      +'<div class="">'+result[i].emailcc+'</div>'
                                +'</div>'
                          +'</div>'
                          +'<div class="col-lg-1 p-0"><img id="" src="'+imgName+'" title="'+result[i].sender+'" alt="'+result[i].sender+'" onerror="userImageOnErrorReplace(this)" class=" ml-3 cursor rounded-circle " style="width: 35px;height: 35px;"></div>'
                    +'</div>'

                    +'<div class="d-flex w-100 position-relative justify-content-center" >'
                        +'<div id="emailfloatoptionsID" class="d-none actFeedOptionsDiv" style="border-radius: 8px;right: unset;position: absolute;z-index: 1;top: 0px;">' 
                                +'<div class="d-flex align-items-center">'
                                  +"<img src=\"images/conversation/reply.svg\" title=\"Reply\" onclick='replyToEmail(\""+sentmtype+"\",\""+result[i].msgid+"\",\""+result[i].fromId+"\",\""+result[i].msgSubject+"\",\""+result[i].sender+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
                                  +"<img src=\"images/conversation/replyAll.svg\" title=\"Reply All\" onclick='replyToEmail(\"replyall\",\""+result[i].msgid+"\",\""+result[i].fromId+"\",\""+result[i].msgSubject+"\",\""+result[i].sender+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
                                  // +"<img src=\"images/cme/forward.svg\" title=\"Forward\" onclick='replyToEmail(\"forward\",\""+result[i].msgid+"\",\""+result[i].fromId+"\",\""+result[i].msgSubject+"\",\""+result[i].sender+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
                                  +"<img src=\"images/task/expand.svg\" title=\"Detail\" onclick='msgDetails(\"details\",\""+result[i].msgid+"\",\""+result[i].fromId+"\",\""+result[i].msgSubject+"\",\""+result[i].msg+"\",\""+result[i].sender+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
                                  +"<img src=\"images/conversation/delete.svg\" title=\"Delete\" onclick='deleteMail(\""+result[i].msgid+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:15px;width:15px;\">"
                                  +'<img src="images/conversation/three_dots.svg" title="More" id="" class="d-none image-fluid cursor" style="margin: 0px 6px;height: 15px;width:18px;">'
                                +'</div>'
                        +'</div>'
                    +'</div>'

                    +'<div class="mt-3 pt-2 border border-left-0 border-right-0 border-bottom-0" style="font-size: 13px;word-break:break-word;">'+result[i].content+'</div>'

                    +'<div id="mailAttachmentListContainer" class="mt-2" style="display:none;">'
                        +'<div class="d-flex justify-content-center align-items-center w-100" style="">'
                            +'<div id="mailAttachmentListDiv" class="d-flex flex-column w-75">'
                                //Below is the static UI code of attached file
                                +'<div class="p-1 mb-1 mr-1" style="border: 1px solid #eaeaea"><img src="images/profile/userImage.svg" class="" style="" title="phonedis.svg"></div>'
                                +'<div class="p-1 mb-1 mr-1" style="border: 1px solid #eaeaea"><img src="images/profile/userImage.svg" class="" style="" title="phonedis.svg"></div>'
                            +'</div>'
                        +'</div>'
                    +'</div>'

                  +'</div>' 
                +'</div>'
            $("#convMsg3").append(uim);
            if(result[i].hasAttachment=="1"){
              $("#mailAttachExist_"+result[i].msgid).css("display","block");
              getAttachment(result[i].msgid,result[i].folderId);
            }
            if(result[i].emailcc == "Not Provided"){
              $("#mailCC_"+result[i].msgid+"").removeClass("d-flex").addClass("d-none");
            }else{
              $("#mailCC_"+result[i].msgid+"").removeClass("d-none").addClass("d-flex");
            }
            }
          }else if(result[i].msgThread == msgThread){
            
            var uim ="";
            uim ='<div  class="row mx-2 my-2" style="box-shadow:0 6px 12px rgb(0 0 0 / 18%);position: relative;background-color:#fff;border:1px solid #d7d7d7;border-radius:2px;">'
                  +'<div class="d-none justify-content-between py-2 px-3 w-100" style="font-size: 13px;background: #E8E8E8;">'
                      +'<div style="color: #7d7d7d;">Found in Sent</div>'
                      +'<img src="/images/conversation/post1.svg" style="height: 20px;width: 20px;">'
                  +'</div>'
                  +'<div  class="col-12 px-3 py-2" onmouseover="showEmailmoreoptions(this);event.stopPropagation();" onmouseout="hideEmailmoreoptions(this);event.stopPropagation();" style="">'
              
                    +'<div class="row m-0 p-0 w-100" style="font-size: 12px;">'
                        +'<div class="col-lg-11 p-0 d-flex flex-column">'
                              +'<div class="d-flex justify-content-between align-items-center">'
                                  +'<div class=" font-weight-bold" style="font-size: 14px;" >'+result[i].sender+'</div>'
                                  +'<div id="mailAttachExist_'+result[i].msgid+'" class="ml-3 mr-auto" style="display:none;">'
                                      +'<img src="/images/conversation/attach.svg" style="height: 16px; width:16px;">'
                                  +'</div>'
                                  +'<div class="mx-1" style="white-space: nowrap;">'+ti+'</div>'
                              +'</div>'
                              +'<div class=" mt-1" style="line-height: 14px;">'+result[i].msgSubject+'</div>'
                              +'<div class="mt-1 d-flex">'
                                    +'<div class="mr-2" style="color:#818181;">To:</div>'
                                    +'<div class="">'+result[i].msgto+'</div>'
                              +'</div>'
                              +'<div id="mailCC_'+result[i].msgid+'" class="mt-1 d-flex">'
                                    +'<div class="mr-2" style="color:#818181;">CC:</div>'
                                    +'<div class="">'+result[i].emailcc+'</div>'
                              +'</div>'
                        +'</div>'
                        +'<div class="col-lg-1 p-0"><img id="" src="'+imgName+'" title="'+result[i].sender+'" alt="'+result[i].sender+'" onerror="userImageOnErrorReplace(this)" class=" ml-3 cursor rounded-circle " style="width: 35px;height: 35px;"></div>'
                    +'</div>'

                    +'<div class="d-flex w-100 position-relative justify-content-center" >'
                      +'<div id="emailfloatoptionsID" class="d-none actFeedOptionsDiv" style="border-radius: 8px;right: unset;position: absolute;z-index: 1;top: 0px;">' 
                              +'<div class="d-flex align-items-center">'
                                +"<img src=\"images/conversation/reply.svg\" title=\"Reply\" onclick='replyToEmail(\""+sentmtype+"\",\""+result[i].msgid+"\",\""+result[i].fromId+"\",\""+result[i].msgSubject+"\",\""+result[i].sender+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
                                +"<img src=\"images/conversation/replyAll.svg\" title=\"Reply All\" onclick='replyToEmail(\"replyall\",\""+result[i].msgid+"\",\""+result[i].fromId+"\",\""+result[i].msgSubject+"\",\""+result[i].sender+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
                                // +"<img src=\"images/cme/forward.svg\" title=\"Forward\" onclick='replyToEmail(\"forward\",\""+result[i].msgid+"\",\""+result[i].fromId+"\",\""+result[i].msgSubject+"\",\""+result[i].sender+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
                                +"<img src=\"images/task/expand.svg\" title=\"Detail\" onclick='msgDetails(\"details\",\""+result[i].msgid+"\",\""+result[i].fromId+"\",\""+result[i].msgSubject+"\",\""+result[i].msg+"\",\""+result[i].sender+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
                                +"<img src=\"images/conversation/delete.svg\" title=\"Delete\" onclick='deleteMail(\""+result[i].msgid+"\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:15px;width:15px;\">"
                                +'<img src="images/conversation/three_dots.svg" title="More" id="" class="d-none image-fluid cursor" style="margin: 0px 6px;height: 15px;width:18px;">'
                              +'</div>'
                      +'</div>'
                    +'</div>'

                    +'<div class="mt-3 pt-2 border border-left-0 border-right-0 border-bottom-0" style="font-size: 13px;word-break:break-word;">'+result[i].content+'</div>'

                    +'<div id="mailAttachmentListContainer" class="mt-2" style="display:none;">'
                        +'<div class="d-flex justify-content-center align-items-center w-100" style="">'
                            +'<div id="mailAttachmentListDiv" class="d-flex flex-column w-75">'
                                //Below is the static UI code of attached file
                                +'<div class="p-1 mb-1 mr-1" style="border: 1px solid #eaeaea"><img src="images/profile/userImage.svg" class="" style="" title="phonedis.svg"></div>'
                                +'<div class="p-1 mb-1 mr-1" style="border: 1px solid #eaeaea"><img src="images/profile/userImage.svg" class="" style="" title="phonedis.svg"></div>'
                            +'</div>'
                        +'</div>'
                    +'</div>'

                  +'</div>' 
                +'</div>'
            $("#convMsg3").append(uim);
            if(result[i].hasAttachment=="1"){
              $("#mailAttachExist_"+result[i].msgid).css("display","block");
              getAttachment(result[i].msgid,result[i].folderId);
            }
            if(result[i].emailcc == "Not Provided"){
              $("#mailCC_"+result[i].msgid+"").removeClass("d-flex").addClass("d-none");
            }else{
              $("#mailCC_"+result[i].msgid+"").removeClass("d-none").addClass("d-flex");
            }

          }

         
        }
        // var ui = "";
        // ui ='<div class="mb-0 w-100 d-flex align-items-center" style="height: 25px;">'
        //       +'<div class=" d-flex align-items-center" style="width: 85%;">'
        //         +"<img src=\"images/conversation/expand.svg\" title=\"Reply\" onclick='event.stopPropagation();' id='' class=\"image-fluid cursor float-left\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
        //         +'<textarea placeholder="reply to: Nivin" class="rounded" style="height: 25px;font-size:13px;resize: none;width: 100%;background-image: none;"></textarea>'
        //       +'</div>'
        //       +'<div class="ml-auto">'
        //         +"<img src=\"images/conversation/expand.svg\" title=\"Reply\" onclick='event.stopPropagation();' id='' class=\"image-fluid cursor mr-2\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
        //         +"<img src=\"images/conversation/expand.svg\" title=\"Reply\" onclick='event.stopPropagation();' id='' class=\"image-fluid cursor mr-2\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
        //         +"<img src=\"images/conversation/expand.svg\" title=\"Reply\" onclick='event.stopPropagation();' id='' class=\"image-fluid cursor mr-2\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
        //       +'</div>'
        //     +'</div>'

        // $("#convMsg").append(ui);
      }

 }

 function showEmailmoreoptions(obj) {
	$(obj).find('#emailfloatoptionsID').removeClass('d-none').addClass('d-flex');
}

function hideEmailmoreoptions(obj) {
	$(obj).find('#emailfloatoptionsID').removeClass('d-flex').addClass('d-none');
}

var globalmsgid;
var globalfromid;
var globalmsgsub;
var globalsenttype;
function replyToEmail(sentmtype,msgmid,fromidmmsg,msgmsub,uimg){

  for(var i =0;i<msgBox.length;i++){
      if(msgBox[i].msgid==msgmid){
      
console.log(msgBox[i].msgto+""+msgBox[i].fromId);
      }
  }
  attachFile="";
  globalfromid = fromidmmsg;
  globalmsgid =msgmid;
  globalmsgsub = msgmsub;
  globalsenttype=sentmtype;

    $('#convid').hide();
    $('#convMsg').hide();
    //$('#convid2').html('').show();
    $('#convMsg2').html('').show();

    /*var ui ="";
    ui ='<div id="emailheader"  style="height: 60px;" class="d-flex justify-content-start align-items-center p-2 border border-top-0 border-right-0 border-left-0">'
            +'<img class="ml-2 cursor" src="/images/cme/leftarrow_blue.svg" onclick="backToEmail();" title="Back" style="height: 20px; width:20px;">'
            +'<div class="font-weight-bold ml-3" style="">New Message'
              +'</div>'
      +'</div>'
    $("#convid2").append(ui);
    */
    var msgto = '';
    var emailcc = '';
    var content = '';
    for(var i =0;i<msgBox.length;i++){
        if(msgBox[i].msgid==msgmid){
            msgto = msgBox[i].msgto;
            emailcc = msgBox[i].emailcc;
            content = msgBox[i].content;
        }
    }
    console.log("msgto--"+msgto);
   

    var ui2 ="";
    ui2 ='<div class="m-3 px-4 pt-3 pb-4" style="background-color:#fff;border:1px solid #e6e6e6;border-radius:2px;">'
            +'<div id="emailheader" style="height: 30px;" class="d-flex justify-content-start align-items-center border border-top-0 border-right-0 border-left-0">'
                +'<img class="ml-2 cursor" src="/images/cme/leftarrow_blue.svg" onclick="backToEmail();" title="Back" style="height: 20px; width:20px;display: none;">'
                +'<div id="mailheader" class="font-weight-normal " style="font-size: 14px;"></div>'
            +'</div>'
            
            +'<div class="d-flex align-items-center mt-1">'
                +'<div class="d-flex align-items-center w-100" style="border-color: #C1C5C8;border-width: 0px 0px 1px 0px;border-style: solid;">'
                    +'<div class="mr-2" style="font-size: 13px;text-align:right;color:#818181;">From:</div>'
                    +"<input disabled type='text' class='' placeholder='' id='mailfrom'  style='font-size: 12px;height: 35px;width:100%;overflow:auto;padding-left: 10px;resize: none;border: none;outline:none !important;' />"
                +'</div>'
            +'</div>'

            +'<div class="d-flex align-items-center">'
                +'<div class="d-flex align-items-center w-100" style="border-color: #C1C5C8;border-width: 0px 0px 1px 0px;border-style: solid;">'
                    +'<div class="mr-2" style="font-size: 13px;text-align:right;color:#818181;">To:</div>'
                    // +"<input type='text' class='' placeholder='' id='mailto'  style='font-size: 12px;height: 35px;width:100%;overflow:auto;padding-left: 10px;resize: none;border: none;outline:none !important;' />"
                    +'<div class="w-100" style="min-height: 40px;">'
                        +'<div id="selectedemail1"  onclick="fetchwsuser(\'1\');" class="my-1 defaultExceedCls w-100 d-flex flex-wrap align-items-center" style="font-size: 12px;">'
                                +'<div id="mailtomain" class="uemail d-flex align-items-center my-1 mx-1" style="border-radius: 15px;border: 1px solid #C1C5C8;">'
                                    +"<img id='mailtosrc' data-src='' src=''  title='"+uimg+"' onerror='userImageOnErrorReplace(this);' class='lozad userimage rounded-circle mx-1 float-left' style='width:20px;'>"
                                    +'<span id="mailto" class="mx-1"></span>' 
                                    +'<button onclick="removeEmailUser(this);" type="button" class="mx-1 cursor" style="background: none;font-size: 16px;color: black;outline: none;border: none;">&times;</button>'
                                +'</div>'

                                +'<div id="emailto_1" contentEditable="true" tabindex="-1" onblur="addNewEmail(this);" onclick="event.stopPropagation();fetchwsuser(\'1\');" onkeyup="searchContactusers()" class="red-input my-1 mx-1  px-2" style="width: 60px;">'
                                +'</div>'
                        +'</div>'
                        +'<div id="userspopup1" class="userspopup epop1" style="display: none;">'
                        +'</div>'
                        
                    +'</div>'
                    +'<div class="mr-2" style="font-size: 13px;text-align:right;color:#818181;"><span onclick="showCcBcc(\'cc\');fetchwsuser(\'2\');" class="mr-1 cursor">Cc</span><span onclick="showCcBcc(\'bcc\');fetchwsuser(\'3\');" class="cursor">Bcc</span></div>'
                    
                +'</div>'
            +'</div>'
            +'<div id="ccDiv" class="d-none align-items-center">'
                +'<div class="d-flex align-items-center w-100" style="border-color: #C1C5C8;border-width: 0px 0px 1px 0px;border-style: solid;">'
                    +'<div class="mr-2" style="font-size: 13px;text-align:right;color:#818181;">Cc:</div>'
                    // +"<input type='text' class='' placeholder='' id='cc'  style='font-size: 12px;height: 35px;width:100%;overflow:auto;padding-left: 10px;resize: none;border: none;outline:none !important;' />"
                    +'<div class="w-100" style="min-height: 40px;">'
                        +'<div id="selectedemail2" onclick="fetchwsuser(\'2\');" class="my-1 defaultExceedCls w-100 d-flex flex-wrap align-items-center" style="font-size: 12px;">'
                           
                                +'<div id="emailto_2" contentEditable="true" tabindex="-1" onblur="addNewEmail(this);" onclick="event.stopPropagation();fetchwsuser(\'2\');" onkeyup="searchContactusers()" class="red-input my-1 mx-1  px-2" style="width: 60px;">'
                                +'</div>'
                        +'</div>'
                        +'<div id="userspopup2" class="userspopup epop1" style="display: none;">'
                        +'</div>'
                
                    +'</div>'
                +'</div>'
            +'</div>'
            +'<div id="bccDiv" class="d-none align-items-center">'
                +'<div class="d-flex align-items-center w-100" style="border-color: #C1C5C8;border-width: 0px 0px 1px 0px;border-style: solid;">'
                    +'<div class="mr-2" style="font-size: 13px;text-align:right;color:#818181;">Bcc:</div>'
                    // +"<input type='text' class='' placeholder='' id='bcc'  style='font-size: 12px;height: 35px;width:100%;overflow:auto;padding-left: 10px;resize: none;border: none;outline:none !important;' />"
                    +'<div class="w-100" style="min-height: 40px;">'
                        +'<div id="selectedemail3" onclick="fetchwsuser(\'3\');" class="my-1 defaultExceedCls w-100 d-flex flex-wrap align-items-center" style="font-size: 12px;">'

                                +'<div id="emailto_3" contentEditable="true" tabindex="-1" onblur="addNewEmail(this);" onclick="event.stopPropagation();fetchwsuser(\'3\');" onkeyup="searchContactusers()" class="red-input my-1 mx-1  px-2" style="width: 60px;">'
                                +'</div>'
                        +'</div>'
                        +'<div id="userspopup3" class="userspopup epop1" style="display: none;">'
                        +'</div>'
                
                    +'</div>'
                +'</div>'
            +'</div>'
            +'<div class="d-flex align-items-center mt-1">'
                +'<div class="d-flex align-items-center w-100" style="border-color: #C1C5C8;border-width: 0px 0px 1px 0px;border-style: solid;">'
                    +'<div class="mr-2" style="font-size: 13px;text-align:right;color:#818181;">Subject:</div>'
                    +"<input type='text' class='' placeholder='' id='mailSub'  style='font-size: 12px;height: 35px;width:100%;overflow:auto;padding-left: 10px;resize: none;border: none;outline:none !important;' />"
                +'</div>'
            +'</div>'
            +'<div class="d-flex align-items-start mt-2" style="height: 230px;">'
                +'<div class="d-flex align-items-start h-100 w-100">'
                    + "<textarea class='wsScrollBar mt-2' id='emailBody' placeholder='Your message...' style='height: 100%;width:100%; overflow:auto; padding:0px 5px;resize: none;'></textarea>"
                +'</div>'
            +'</div>'
            +'<div class="d-flex align-items-center mt-1">'
                +'<div class="d-flex align-items-center w-100" style="">'
                    +'<form style="display:none;">'
                      +" <input type='file' id='mailUploadInput' multiple='multiple' name='files'  />"
                    +"</form>"
                    +'<div id="mailUploadPreview" class="d-flex"></div>'
                +'</div>'
            +'</div>'
            +'<div id="mailbutton" class="d-flex align-items-center pt-2" style="">'
              +'<div id="" onclick="sendMail()" style=" width: 70px;float: left;" class="emailButton createBtn mt-0 ml-0 mr-2">Send</div>'
              +'<div id="" onclick="" style=" width: 70px;float: left;" class="emailButton createBtn mt-0 mx-2">Save</div>'
              +'<div id="cnEmailBtn" onclick="backToEmail()" style=" width: 70px;float: left;" class="emailButton createBtn mt-0 mx-2">Cancel</div>'
              +'<img src="/images/conversation/attach.svg" onclick="mailAttach()" class="emailButton" title="Upload" style="height: 20px; width:20px;cursor:pointer;">'
            +'</div>'
        +'</div>'
        

    $('#convMsg2').html(ui2);
   
    ckeditor3();
    $("#mailfrom").val('mailadmin@colabusmailbox.com');
    // $("#mailto").attr("disabled",true);
   
    if(globalsenttype == "new"){
      
      //  $("#mailto").val('');
       $('#emailto_1').focus();
       $("#mailtomain").remove();
      //  $("#mailto").attr("disabled",false);
       $("#mailheader").text("New Message");
       fetchwsuser('1');
        initUpload();
    }else if(globalsenttype == "reply"){
        // var tomsg = msgto.split('&lt;mailadmin@colabusmailbox.com&gt;');
        // $("#mailto").val(globalfromid);
        // if(globalfromid == 'mailadmin@colabusmailbox.com'){
        //   $("#mailto").prepend(userEmail);
        // }else{
          $("#mailto").prepend(globalfromid);
        // }
        $("#mailtomain").attr('mail', globalfromid);
        $('#emailto_1').focus();
        $("#mailSub").val(globalmsgsub);
        $("#mailheader").text("Reply Message");
        fetchwsuser('1');
        initUpload();
        replyEmails(msgto);
        if(globalfromid == 'mailadmin@colabusmailbox.com'){
          if($('#selectedemail1').children().length > 2){
            $("#mailtomain").remove();
          }
        }
    }else if(globalsenttype == "replyall"){
      // var tomsg = msgto.split('&lt;mailadmin@colabusmailbox.com&gt;');
      // $("#mailto").val(globalfromid);
      // if(globalfromid == 'mailadmin@colabusmailbox.com'){
      //   $("#mailto").prepend(userEmail);
      // }else{
        $("#mailto").prepend(globalfromid);
      // }
      $("#mailtomain").attr('mail', globalfromid);
      $('#emailto_1').focus();
      $("#mailSub").val(globalmsgsub);
      $("#mailheader").text("ReplyAll Message");
      fetchwsuser('1');
      initUpload();
      replyEmails(msgto);
      if(emailcc != 'Not Provided'){
        replyEmails(emailcc);
      }
      if(globalfromid == 'mailadmin@colabusmailbox.com'){
        if($('#selectedemail1').children().length > 2){
          $("#mailtomain").remove();
        }
      }
    }else if(globalsenttype == "details"){
        // $("#mailto").val(globalfromid);
        $("#mailto").prepend(globalfromid);
        $("#mailSub").val(globalmsgsub);
        $("#mailheader").text(globalmsgsub);
        $('#emailBody').val(content);
        $(".emailButton").hide();
        $("#cnEmailBtn").show();
    }else if(globalsenttype == "forward"){
      
      //  $("#mailto").val('');
      $("#mailSub").val(globalmsgsub);
      $('#emailBody').val(content);

       $('#emailto_1').focus();
       $("#mailtomain").remove();
      //  $("#mailto").attr("disabled",false);
       $("#mailheader").text("Forward Message");
       fetchwsuser('1');
        initUpload();
    }
}

function replyEmails(mail){
  var st = mail;
  var name='';
  if(st.indexOf(",") > 0){
    var stArray = st.split(",");
    //alert(stArray[0]);
    stArray = stArray.toString().split("&gt;");
    //alert(stArray[0]);
  }else{
    stArray = st.split("&gt;");
  }
  for(var i=0; i< stArray.length-1; i++){
    // alert(stArray[i]);
      console.log(stArray[i].indexOf('&quot;'));
      if(stArray[i].indexOf("&quot;") >= 0){
            name=stArray[i].substring(stArray[i].indexOf('&quot;')+6,stArray[i].lastIndexOf('&quot;') );
      }
      var id = stArray[i].substring(stArray[i].indexOf('&lt;')+4);
      // alert(id);
      if(id == 'mailadmin@colabusmailbox.com'){

      }else{
        if(name == ''){
          name = id;
        }
        var ui = reuseEmailId('1', id,'0', name);
        $('#emailto_1').text('').focus().before(ui);
      }
  }
}

function showCcBcc(type){
  if(type=='cc'){
    $('#ccDiv').removeClass('d-none').addClass('d-flex');
  }else{
    $('#bccDiv').removeClass('d-none').addClass('d-flex');
  }
}
function mailAttach(){
  $('#mailUploadInput').trigger('click');

}
var attachFile="";

function readURLNewProfile1222(newUploadEmailDoc) { //---------this is for email upload
  
  var formdata = new FormData();
  formdata.append("file",newUploadEmailDoc);
  let jsonbody={
    "fileName":"test",
    "filepath":newUploadEmailDoc,
    "companyId":companyIdglb
   
  }
  $.ajax({
    url: apiPath+"/"+myk+"/v1/UploadData",
    //url : "http://localhost:8080/v1/UploadData",
    type:"POST",
        processData: false,
				contentType: false,
        data: formdata,
        cache: false,
				mimeType: "multipart/form-data",
    error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
        timerControl("");
        },  
    success:function(result){
      if(attachFile ==""){
        attachFile=result;
      }else{
        attachFile=attachFile+","+result;
      }
      
      
          }
     });


//   if (input.files && input.files[0]) {
//        var reader = new FileReader();
       

//      $('#albumFileExtn').val(input.files[0].name.split('.').pop().toLowerCase());
//      $('#albumFileName').val(input.files[0].name.split('.')[0]);
     
//        reader.onload = function (e) {
//         var blob = new Blob([fileReader.result]);
//         // const blob = new Blob([new Uint8Array(e.target.result)], {type: input.files[0].type });
//         console.log(blob);
//        }	 
//    reader.readAsDataURL(input.files[0]);
//  }  
   
}
function initUpload(){ 
  $("#mailUploadInput").on('change', function(e) {
    /*var image_holder = $("#mailUploadPreview");
    var files = e.target.files,
    filesLength = files.length;
    for (var i = 0; i < filesLength; i++) {
      var f = files[i]
      var fileReader = new FileReader();
      fileReader.onload = (function(e) {
          var file = e.target;
          console.log(e.target.type);
          $("<div class=\"pip mr-2 mb-1\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" style='border: 1px solid #eaeaea; width:50px;' title=\"" + file.name + "\"/>" +
            //"<br/><span class=\"remove\">Remove image</span>" +
            "</div>").appendTo(image_holder);
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
        });
      fileReader.readAsDataURL(f);
    }
    */
    //Get count of selected files
    var countFiles = $(this)[0].files.length;
    var imgPath = $(this)[0].value;
    var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    console.log(imgPath);
    //var image_holder = $("#mailUploadPreview");
    //image_holder.empty();
    //if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
      if (typeof(FileReader) != "undefined") {
        //loop for each file selected for uploaded.
        for (var i = 0; i < countFiles; i++) 
        {
          var reader = new FileReader();
          reader.fileName = $(this)[0].files[i].name;
          reader.onload = function(e) {
            
              var filename = e.target.fileName;
              ext = filename.substring(filename.lastIndexOf('.')+1).toLowerCase();
              console.log("ext:"+ext);
              var src = "";
              if(ext.toLowerCase() == "png" || ext.toLowerCase() == "jpg" || ext.toLowerCase() == "jpeg" || ext.toLowerCase() == "gif" || ext.toLowerCase() == "svg"){
                src = e.target.result;
              }else{
                src = 'images/document/'+ext+'.svg';
              }

              $("#mailUploadPreview").append("<div class='p-1 mb-1 mr-1' style='border: 1px solid #eaeaea' ><img src='"+src+"' class='' style='width:50px;' title='"+e.target.fileName+"' /></div>")
          }
          reader.readAsDataURL($(this)[0].files[i]);

          readURLNewProfile1222($(this)[0].files[i]);

        }
      } else {
        alert("This browser does not support FileReader.");
      }
    //} else {
    //  alert("Pls select only images");
    //}
    
  });
}

function reuseEmailId(type, email, uid, uname, image){
  var ui = '';
    if(type == '2'){
        ui = '<div id="'+email+'" mail="'+email+'" class="uemail d-flex align-items-center my-1 mx-1" style="border-radius: 15px;border: 1px solid #C1C5C8;;">'
                    +"<img data-src='' src='/images/profile/userImage.svg'  title='' onerror='userImageOnErrorReplace(this);' class='lozad userimage rounded-circle mx-1 float-left' style='width:20px;'>" 
                    +'<span class="mx-1">'+email+'</span>'
                    +'<button  onclick="removeEmailUser(this);" type="button" class="mx-1 cursor" style="background: none;font-size: 16px;color: black;outline: none;border: none;">&times;</button>'
                  +'</div>'
    }else{
        ui = '<div id="Email_'+uid+'" mail="'+email+'" class="uemail d-flex align-items-center my-1 mx-1" style="border-radius: 15px;border: 1px solid #C1C5C8;">'
                    +"<img data-src='"+image+"' src='"+image+"'  title='"+uname+"' onerror='userImageOnErrorReplace(this);' class='lozad userimage rounded-circle mx-1 float-left' style='width:20px;'>" 
                    +'<span class="mx-1">'+email+'</span>'
                    +'<button  onclick="removeEmailUser(this);" type="button" class="mx-1 cursor" style="background: none;font-size: 16px;color: black;outline: none;border: none;">&times;</button>'
                  +'</div>'
    }
    return ui;
}

function addNewEmail(obj){
  console.log('onblur');
  if($(".userspopup").is(':visible')){
    // $("#userspopup").hide();
  }else{
    var email = $(obj).text();
    var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if(email == ''){

    }else{

        if (email.match(validRegex)) {

          console.log("Valid email address!");

          // document.form1.text1.focus();
            var ui = reuseEmailId('2', email);
              // var ui = '<div id="'+email+'" class="uemail my-1 mx-1" style="border-radius: 15px;border: 1px solid #C1C5C8;;">'+email+''
              //             +"<img data-src='' src='/images/profile/userImage.svg'  title='' onerror='userImageOnErrorReplace(this);' class='lozad userimage rounded-circle mx-1 mt-1 float-left' style='width:20px;'>" 
              //             +'<button  onclick="removeEmailUser(this);" type="button" class="mx-1" style="background: none;font-size: 16px;color: black;outline: none;border: none;">&times;</button>'
              //         +'</div>'

              $('#emailto_'+globalemail).text('').focus().before(ui);
              $("#userspopup").hide();
              // fetchwsuser();

          // return true;

        } else {
	        alertFunNew("Invalid email address!",'error');

          // $('#emailto').focus();

          console.log("Invalid email address!");

          // document.form1.text1.focus();

          // return false;

        }
        
      }
  }
}

jQuery(".red-input").attr("tabindex",-1).focus();

function selectemailuser(email, uid, uname, image, obj){
  var ui = reuseEmailId('1', email, uid, uname, image);
  // var ui = '<div id="Email_'+uid+'" class="uemail my-1 mx-1" style="border-radius: 15px;border: 1px solid #C1C5C8;">'+email+''
  //             +"<img data-src='"+image+"' src='"+image+"'  title='"+uname+"' onerror='userImageOnErrorReplace(this);' class='lozad userimage rounded-circle mx-1 mt-1 float-left' style='width:20px;'>" 
  //             +'<button  onclick="removeEmailUser(this);" type="button" class="mx-1" style="background: none;font-size: 16px;color: black;outline: none;border: none;">&times;</button>'
  //           +'</div>'

  $('#emailto_'+globalemail).text('').focus().before(ui);
  $(".userspopup").hide();
  // fetchwsuser();
}

function removeEmailUser(obj){
    $(obj).parents('.uemail').remove();
  // fetchwsuser();
  $('#emailto_'+globalemail).focus();
}

function handlingEmailUsers() {
  var id = '';
  $('#selectedemail > div').each(function () {
    id = $(this).attr('id').split('_')[1];
    $('#Mail_' + id).remove();
  });
}

function fetchwsuser(type){
  var jsonData="";
  $.ajax({ 
    url: apiPath+"/"+myk+"/v1/fetchParticipants?user_id="+userIdglb+"&company_id="+companyIdglb+"&project_id="+prjid,
    type:"GET",
    ///data:{act:'fetchAllUsersListForSpecChar',projectId:projectId,userId: userId,menuType:menuType,docFolId:docFolId,taskId:taskId},
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function(jqXHR, textStatus, errorThrown) {
        checkError(jqXHR,textStatus,errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        timerControl("");
    }, 
    success:function(result){
     
      jsonData = result;
      var ui = prepareUIforUsers(jsonData,prjid);
      $('#loadingBar').addClass('d-none').removeClass('d-flex'); 
 
      if(type == '1'){
          var eval = globalemail;
          $("#userspopup3").html('');
          $("#userspopup2").html('');
          $("#userspopup1").html(ui).hide();
          globalemail = '1';
          $('#emailto_'+globalemail).focus();
          if(globalemail != eval){
              $('#emailto_'+eval).text('');
          }
      }else if(type == '2'){
          var eval = globalemail;
          $('#emailto_'+globalemail).text('');
          $("#userspopup1").html('');
          $("#userspopup3").html('');
          $("#userspopup2").html(ui).hide();
          globalemail = '2';
          $('#emailto_'+globalemail).focus();
          if(globalemail != eval){
            $('#emailto_'+eval).text('');
          }
      }else{
          var eval = globalemail;
          $('#emailto_'+globalemail).text('');
          $("#userspopup1").html('');
          $("#userspopup2").html('');
          $("#userspopup3").html(ui).hide();
          globalemail = '3';
          $('#emailto_'+globalemail).focus();
          if(globalemail != eval){
            $('#emailto_'+eval).text('');
          }
      }
      // handlingEmailUsers()
    }
  });
}

function searchContactusers() {
	var txt = $('#emailto_'+globalemail).text().toLowerCase();

	$('#filteruser').children().each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding
		var val = $(this).find('.names').text().toLowerCase();
		if (val.indexOf(txt) != -1) {
			  $(this).removeClass('d-none').addClass('d-flex');
		} else {
      $(this).removeClass('d-flex').addClass('d-none');
		}
	});
  var cnt = $('#filteruser').find('.email99').length;
  if($('#filteruser').find('.d-flex').length > 0 && $('#filteruser').find('.d-flex').length < cnt){
      $(".userspopup").show();
  }
  else{
      $(".userspopup").hide();
  }
}

function prepareUIforUsers(jsonData, prjid){
  var sessionuserId = userIdglb;

  //console.log(jsonData)
  var UI="";
  UI = "<div class=\" col-12 d-flex justify-content-center position-relative\" id='popupemail' style='z-index:2'>"
    +'<div class="border border-secondary py-3 pl-3 pr-2 touchablePop atListPop position-absolute rounded" style="background-color:white;margin-top: -2px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;">'
  +"<div class=\"header pb-1\" style='font-weight: normal;'>Select participants</div>"
  +"<div id='filteruser' class=\"wsScrollBar\" style='margin-top:5px;height:190px;overflow-y:auto;overflow-x: hidden;width: 290px;'>"
  for(i=0;i<jsonData.length;i++){
    // var selecteduserId = jsonData[i].user_id
    var d= new Date();
      let imgName = lighttpdpath+"/userimages/"+jsonData[i].user_image+"?"+d.getTime();
    // if(jsonData[i].user_id != sessionuserId){
        var username=jsonData[i].user_full_name;
        var email=jsonData[i].user_email1;
        UI+="<div id='Mail_"+jsonData[i].user_id+"' class=\"email99 cursor d-flex align-items-center\" style =' padding:10px 5px;' onclick=\"selectemailuser('"+email+"','"+jsonData[i].user_id+"','"+username+"','"+imgName+"',this)\">"
	  if(jsonData[i].user_id == userIdglb){
		    UI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);"  src="'+imgName+'" title="'+username+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle" style="width:30px;height:30px;">'
	  }else{
		    UI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);"  src="'+imgName+'" title="'+username+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle cursor" style="width:30px;height:30px;">'
	  }
	  UI+='<div class="names defaultExceedCls" style="font-size: 14px;font-weight: normal;" title="'+ jsonData[i].user_full_name+'">'
        + jsonData[i].user_full_name
        +'</div>'
              + "</div > ";
    // }
  }
   UI+="</div>" 
  UI+="</div>"
  +"</div>"
  return UI;
}

function backToEmail(){
     $('#convid').show();
    $('#convMsg').show();
    $('#convid2').hide();
    $('#convMsg2').hide();
    $('#ckeditor-height').remove();
}

var myEditorEmailData = "";
function ckeditor3() {
	var $ref=$('#emailBody');
        ClassicEditor
  		// .create( document.querySelector( '#transBody' ) )
		  .create( $ref[0] ,{
        // config.toolbarLocation = 'bottom';
			// your options
		} )
                .then( editor => {
					$('<style id="ckeditor-height" type="text/css" scoped>.ck-editor .ck-editor__editable_inline {min-height: 100% !important;} .ck-reset, .ck-editor, .ck-rounded-corners{height:100% !important;width:100% !important;} .ck .ck-editor__main{height:80%;} .ck .ck-toolbar{background: none;border-width: 0px 0px 1px 0px !important;border-color: #C1C5C8; border-style: solid; border-radius: unset !important;} .ck.ck-editor__main>.ck-editor__editable.ck-rounded-corners{ border-width: 0px 0px 1px 0px !important;border-color: #C1C5C8; border-style: solid; box-shadow: none !important; border-radius: unset !important;}</style>').insertAfter($ref);

                    // console.log( editor );
                    myEditorEmailData = editor;
                   
                } )
                .catch( error => {
                    console.error( error );
                } );
}


function sendMail(){
  $('.red-input').remove();
  var emails = '';
  var emailcc='';
  var emailbcc='';
  var id = '';
  var id2="";
  var id3="";
	$('#selectedemail1 > div').each(function () {
		id = $(this).attr('mail');
		emails += id + ",";
  });
  
  var count = emails.length;
  var allemailid = emails.substring(0, count - 1);
  

  $('#selectedemail2 > div').each(function () {
		id2 = $(this).attr('mail');
		emailcc += id2 + ",";
  });
  var emailscc;
  var countcc='';
  if(emailcc != ""){
     countcc = emailcc.length;
     emailscc = emailcc.substring(0, countcc - 1);
  }
 

  $('#selectedemail3 > div').each(function () {
		id3 = $(this).attr('mail');
		emailbcc += id3 + ",";
  });
  var countbcc='';
  var emailsbcc="";
  if(emailbcc != ""){
   countbcc = emailbcc.length;
   emailsbcc = emailbcc.substring(0, countbcc - 1);
  }


  //var mailTo = $("#mailto").val();
  var mailSub = $("#mailSub").val();
  var mailcc;
  var mailBcc;
  var emailBody =  myEditorEmailData.getData();
  var msgFrom;
  var type ;
  
  if(globalsenttype == "new"){
    type = "new";

  }else{
    type == "reply";
  }
  
  if(type == "reply"){
    msgFrom ="mailadmin@colabusmailbox.com";
  }else{
    msgFrom ="mailadmin@colabusmailbox.com";
  }
  var attachJson;
  if(attachFile ==""){

  }else{
    attachFile =attachFile;
    
    
  }
 
  let jsonbody={
    "msgto":allemailid,
		"msgSubject":mailSub,
    "msg":emailBody,
    "msgid":globalmsgid,
    "msgfrom":msgFrom,
    "companyId":companyIdglb,
    "sentType":globalsenttype,
    "emailcc":emailscc,
    "emailbcc":emailsbcc,
    "attach":attachFile,
	}
  // alert(type);
  $.ajax({
    url: apiPath+"/"+myk+"/v1/sentMail",
    //url : "http://localhost:8080/v1/sentMail",
    type:"POST",
    dataType:'json',
    contentType:"application/json",
    data: JSON.stringify(jsonbody),
    error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
        timerControl("");
        },  
    success:function(result){
      attachFile="";
      
        backToEmail();
        if(type =="new"){
          alertFun("Email Sent.", 'warning');
        }else{
          alertFun("Email Replied.", 'warning');
        }
          }
     });  
 }

 function msgDetails(sentmtype,msgmid,fromidmmsg,msgmsub,msg,uimg){
  
  replyToEmail(sentmtype,msgmid,fromidmmsg,msgmsub,uimg);
 
  $("#emailBody").text(msg);
 // $("#mailbutton").removeClass("d-flex").addClass("d-none");

 }

 function deleteMail(msgid){
   alert(msgid);
  let jsonbody={
    
    "msgid":msgid,
    "companyId":companyIdglb
   
	}
   
  $.ajax({
    url: apiPath+"/"+myk+"/v1/deleteMail",
    //url : "http://localhost:8080/v1/deleteMail",
    type:"DELETE",
    dataType:'json',
    contentType:"application/json",
    data: JSON.stringify(jsonbody),
    error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
        timerControl("");
        },  
    success:function(result){
       
      
      alertFun("Email Deleted.", 'warning');
     
      
          }
     });  

 }

 function getAttachment(msgid,folderid){
 // alert(msgid+"-"+folderid);
 let jsonbody={
   
   "msgid":msgid,
   "folderId":folderid,
   "companyId":companyIdglb
  
 }
  
 $.ajax({
   url: apiPath+"/"+myk+"/v1/getAttachment",
  // url : "http://localhost:8080/v1/getAttachment",
   type:"GET",
   dataType:'json',
   contentType:"*/*",
   data: JSON.stringify(jsonbody),
   error: function(jqXHR, textStatus, errorThrown) {
               checkError(jqXHR,textStatus,errorThrown);
               $("#loadingBar").hide();
       timerControl("");
       },  
   success:function(result){
      
     
     
    
     
         }
    });  

}


