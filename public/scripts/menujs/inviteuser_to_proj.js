	var adminContDiv = "";
	var teamContDiv = "";
	var subContDiv = "";
	var projectId = "";
	var adminDivs = "";
	var addedUserIds = "";



 	function inviteUsersToProject(position,check){
	 	//hide and show of containers 
	 	    $("div#taskCompletionCodesContainer").hide();
	 		$("div#fullProjectDetailsContainer").hide();
	 		$("div#apporvalRequestContainer").hide();
			$("div#templateContainer").hide();
			$("div#optionalDrivesContainer").hide();
			$("div#projectStatusContainer").hide();
			$("div#inviteUsersToProjectWorkspace").show();
	 	
 		$('#loadingBar').show();
		timerControl("start");
		$("#transparentDiv").show();
		var windowWidth = $(window).width();
		
		if(position=="workspace"){
	    	var projectId = $("input#globalProjectId").val();
			$.ajax({
				url: path+"/workspaceAction.do",
				type:"POST",
				data:{act:"getTheProjectUsers",projectId:projectId},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
					checkSessionTimeOut(result);
					
					var height = getHeightDynamically('inviteUsers')
					if(check == 'change'){
						height = height - 30;
					}
					else {
					  height = $('#rightProjectContainer').height();
					}
					$("div#displayUsersContainer").css("height",height+"px");
					prepareCommonUsersUI(result);
					var projType = $('#hiddenProjSettingType').val();
    				if(projType !='myProject'){
    				  $('img.userRemoveClass').hide();
    				}
					$("#transparentDiv").hide();
	       		 	$("div#inviteUsersToProjectWorkspace").show();
					$("div#displayContentContainer").show();
					
					checkForNoUsers();
					$('#loadingBar').hide();
					timerControl("");
			   }
			 });
 		}
	}
 	
 		function projectAdminInviteUserScroll(id){
		 		$("div#"+id).mCustomScrollbar({
						scrollButtons:{
							enable:true
						},
					});
				$("div#"+id).mCustomScrollbar("update");
	 			$('div.mCSB_container').css('margin-right','0px');	
				$('div#'+id+' .mCSB_container').css('margin-right','15px');
			    $('div#'+id+' .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
				$('div#'+id+' .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
				$('div#'+id+' .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
				$('div#'+id+' .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
				$('div#'+id+' .mCSB_scrollTools .mCSB_draggerRail').css('background','#B1B1B1');
				$('div#'+id+' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','#6C6C6C');
			    $('div#'+id+' .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','#B1B1B1');
			    $('div#'+id+'.mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','#B1B1B1');	
	 			$('div#'+id+' .mCSB_container').css('margin-right','15px');
	    		$('div#'+id+' div.mCSB_scrollTools').css('margin-left','0.5%');	
	    		$("div#"+id+" div.mCS_no_scrollbar").css('height','100%');
 	}
 	
 	function imageOnErrorUserImgReplace(obj ){
		/*$(obj).attr('src',lighttpdPath + "/userimages/userImage.png");*/
 		Initial(obj);
    }
    
    function Initial(obj){
    	$(obj).initial({
    		name:$(obj).attr("title"),
    		height:64,
    		width:64,
    		charCount:2,
    		textColor:"#fff",
    		fontSize:30,
    		fontWeight:700
    	}); 
    }
 	
	
	function addTeamMembersList(type,popDataDiv,popUpContClas,checkPartImg,popContId,popSearchId,popSearchIcon){
		$("div#projAdminTeamUsers").html("");
		$('#loadingBar').show();
		timerControl("start");
		$("."+popUpContClas).hide();
		$('#'+checkPartImg).hide();
		$("#"+popContId).css("display","block");
		$("input#"+popSearchId).val('');
		$("img#"+popSearchIcon).attr('src','images/Search.png');
		var projectId = $("input#globalProjectId").val();
			    
			$.ajax({
		      	url: path+"/paLoad.do",
				type:"POST",
				data:{act:"addTeamMembersToList",searchUser:"",projectId:projectId,type:type},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
				checkSessionTimeOut(result);
				prepareTeamMemberUI(result,popDataDiv);
				var plusIconOff=$('#addTeamMembers').offset().top;
				var userListContainerOff=$('#teamUserlistContainer').offset().top;
			    var containerHeight=$("#teamUserlistContainer").height();
			    var remainHeight = $(window).height()-plusIconOff;
			    var setMargin = plusIconOff-userListContainerOff;
			    
			   if(containerHeight>remainHeight){
			   		var temp=containerHeight-remainHeight;
			   		$('#wsTeamArrow').css("margin-top",setMargin+"px");
			   }
			   else{
			   	   $('#teamUserlistContainer').css("margin-top",setMargin-30+"px");
			   }
				hideParticipants();
			    hideAddedParticipants("div#projAdminTeamUsers_");
 					$(".projAdminTeamUsers").each(function(){
						if ($('div.projAdminTeamUsersDiv').is(':visible')) {
						
						}else{
						   $("#projAdminTeamUsers").html("No Users to display");
						}
					});
					$('#loadingBar').hide();
					timerControl("");
				}
			  });
	}
	
	
	
	
	function addSubscribeMembersList(type,popDataDiv,popUpContClas,checkPartImg,popContId,popSearchId,popSearchIcon){
		$("div#sysAdminSubscribeUsers").html("");
		$('#loadingBar').show();
	    timerControl("start");
		$("."+popUpContClas).hide();
		$("div#"+popUpContClas).css("display","block");
		$('#'+checkPartImg).hide();
		$("div#teamUserlistContainer").css("margin-top","");
		$('#teamMemberUserArrow').css('margin-top','');
		$("input#"+popSearchId).val('');
		$("img#"+popSearchIcon).attr('src','images/Search.png');
		var projectId = $("input#globalProjectId").val();
			$.ajax({
		      	url: path+"/paLoad.do",
				type:"POST",
				data:{act:"addSubscribeUserToList",searchUser:"",projectId:projectId,type:type},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
				parent.checkSessionTimeOut(result);
				$("#"+checkPartImg).show();
				prepareObserversUI(result,popDataDiv);
				
				var plusIconOff = $("img#addObserversNew").offset().top;
				var userListContainerOff=$('#SubscribeUserlistContainer').offset().top;
			    var containerHeight=$("#SubscribeUserlistContainer").height();
			    var remainHeight = $(window).height()-plusIconOff;
			    var setMargin = plusIconOff-userListContainerOff;
				
				if(containerHeight>remainHeight){
			   		var temp=containerHeight-remainHeight;
			   		$('#wsObserverArrow').css("margin-top",setMargin+"px");
			   }
			   else{
			   	   $('#SubscribeUserlistContainer').css("margin-top",setMargin-30+"px");
			   }
			   hideParticipants();
			   hideAddedParticipants("div#projAdminSubscribeUsers_");
 					$(".projAdminObvUsers").each(function(){
						if ($('div.projAdminSubscribeUsersDiv').is(':visible')) {
						
						}else{
						   $("#sysAdminSubscribeUsers").html("No Users to display");
						}
					});			
					$('#loadingBar').hide();
					timerControl("");
				}
			  });
		}
	
			function inviteUsersListScroll(id){
		 		$("div#"+id).mCustomScrollbar({
						scrollButtons:{
							enable:true
						},
					});
				$("div#"+id).mCustomScrollbar("update");
	 			$('div.mCSB_container').css('margin-right','0px');	
				$('div#'+id+' .mCSB_container').css('margin-right','15px');
			    $('div#'+id+' .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
				$('div#'+id+' .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
				$('div#'+id+' .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
				$('div#'+id+' .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
				$('div#'+id+' .mCSB_scrollTools .mCSB_draggerRail').css('background','#B1B1B1');
				$('div#'+id+' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','#6C6C6C');
			    $('div#'+id+' .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','#B1B1B1');
			    $('div#'+id+' .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','#B1B1B1');	
				$('div#'+id+' .mCSB_container').css('margin-right','15px');
	    		$('div#'+id+' div.mCSB_scrollTools').css('margin-left','0.5%');	
 	}
 	
 	
 	
 	
 	function hideParticipants(){
 			var usrId1 ="";
			 var usrId2 = "";
			 var usrId3 = "";
				$("#"+adminContDiv+" > div").each(function(i) {
					 usrId1 = this.id.split('_')[1];
					$('div#projAdminUsers_'+usrId1).hide();
					$('div#projAdminTeamUsers_'+usrId1).hide();
					$('div#projAdminUsers_'+usrId1).find('.userChecked').removeClass();
					$('div#projAdminSubscribeUsers_'+usrId1).hide();
					$("div#sysAdminUsers").mCustomScrollbar("update");
			   });
			   $("#"+teamContDiv+" > div").each(function(i) {
					var usrId2 = this.id.split('_')[1];
					$('div#projAdminUsers_'+usrId2).hide();
					$('div#projAdminTeamUsers_'+usrId2).hide();
					$('div#projAdminTeamUsers_'+usrId2).find('.userChecked').removeClass();
					$('div#projAdminSubscribeUsers_'+usrId2).hide();
					$("div#projAdminTeamUsers").mCustomScrollbar("update");
			   });
			   $("#"+subContDiv+" > div").each(function(i) {
					var usrId3 = this.id.split('_')[1];
					$('div#projAdminUsers_'+usrId3).hide();
					$('div#projAdminTeamUsers_'+usrId3).hide();
					$('div#projAdminSubscribeUsers_'+usrId3).find('.userChecked').removeClass();
					$('div#projAdminSubscribeUsers_'+usrId3).hide();
					$("div#sysAdminSubscribeUsers").mCustomScrollbar("update");
			   });
		}
 	
 	/* filter admin users*/
 	function searchProjAdminUsers(obj,hideDiv,searchIcon,scrollDiv,filterDiv,searchInputDiv,spanDivClass){
			var searchElement = $(obj).val().toLowerCase();
			$('.'+hideDiv).hide();
			if(searchElement!=''){
				filterProjAdminUsers(searchElement,hideDiv,filterDiv,spanDivClass);
			    $("img#"+searchIcon).attr('title','');
				$("img#"+searchIcon).attr('src','images/workspace/remove.png');
			    $("img#"+searchIcon).attr('onclick','clearAdminProjSearch(this,"'+hideDiv+'","'+searchIcon+'","'+scrollDiv+'","'+filterDiv+'","'+searchInputDiv+'","'+spanDivClass+'");');
			    $("img#"+searchIcon).attr('title','clear');
		  }else{
				$('.'+hideDiv).show();
				$("img#"+searchIcon).attr('src','images/Search.png');
				$("img#"+searchIcon).attr('onclick','');
				$("img#"+searchIcon).attr('title','search');
				// hideParticipants();
				hideAddedParticipants("div#projAdminUsers_");
		}
			if(!isiPad){
				$("div#"+scrollDiv).mCustomScrollbar("update");
			}
			 hideParticipants();
		}
			
	 function filterProjAdminUsers(searchEle,hideDiv,filterDiv,spanDivClass){
			$('div.'+filterDiv).find('span.'+spanDivClass+':contains("'+searchEle+'")').parents('div.'+hideDiv+'').show();
			// hideParticipants();
			hideAddedParticipants("div#projAdminUsers_");
	}
	
			function clearAdminProjSearch(obj,hideDiv,searchIcon,scrollDiv,filterDiv,searchInputDiv,spanDivClass){
				$("input#"+searchInputDiv).val('');
				searchProjAdminUsers(obj,hideDiv,searchIcon,scrollDiv,filterDiv,searchInputDiv,spanDivClass);
				
		}
		
		
		/* filter team member users*/
		function searchTeamMemberUsers(obj,hideDiv,searchIcon,scrollDiv,filterDiv,searchInputDiv,spanDivClass){
			var searchElement = $(obj).val().toLowerCase();
			$('.'+hideDiv).hide();
			if(searchElement!=''){
				filterProjTeamUsers(searchElement,hideDiv,filterDiv,spanDivClass);
				$("img#"+searchIcon).attr('title','');
				$("img#"+searchIcon).attr('src','images/workspace/remove.png');
				//$("img#sysAdminTeamSearchIcon").attr('onclick','clearTeamInput()');
			    $("img#"+searchIcon).attr('onclick','clearProjTeamSearch(this,"'+hideDiv+'","'+searchIcon+'","'+scrollDiv+'","'+filterDiv+'","'+searchInputDiv+'","'+spanDivClass+'");');
		 		$("img#"+searchIcon).attr('title','clear');
		  }else{
				$('.'+hideDiv).show();
				$("img#"+searchIcon).attr('src','images/search.png');
				$("img#"+searchIcon).attr('onclick','');
				$("img#"+searchIcon).attr('title','search');
				// hideParticipants();
				hideAddedParticipants("div#projAdminTeamUsers_");
				addTeamMembersList('workspace','projAdminTeamUsers','popAdminUserListContainer','addTeamUsersParticipants','teamUserlistContainer','searchAdminTeamUsersList','sysAdminTeamSearchIcon');
		}
			if(!isiPad){
				$("div#"+scrollDiv).mCustomScrollbar("update");
			}
			hideParticipants();
		}
		
		function filterProjTeamUsers(searchEle,hideDiv,filterDiv,spanDivClass){
				// $('div.projAdminTeamUserName').find('span.projAdmimTeamUserName:contains("'+searchEle+'")').parents('div.projAdminTeamUsersDiv').show();
				$('div.'+filterDiv).find('span.'+spanDivClass+':contains("'+searchEle+'")').parents('div.'+hideDiv+'').show();
				// hideParticipants();
			hideAddedParticipants("div#projAdminTeamUsers_");
		}
		
	function hideAddedParticipants(userDivIdFrom){
		if(addedUserIds != "" || addedUserIds != 'undefined'){
			var idres = addedUserIds.split(',');
			for(var i=0; i<idres.length-1; i++){
				$(userDivIdFrom+idres[i]).css("display","none");
			}
			$("div#projAdminTeamUsers_"+rInvUserId).show();
			$("div#projAdminSubscribeUsers_"+rInvUserId).show();
		}
	}
	
		function clearProjTeamSearch(obj,hideDiv,searchIcon,scrollDiv,filterDiv,searchInputDiv,spanDivClass){
			$("input#"+searchInputDiv).val('');
			searchTeamMemberUsers(obj,hideDiv,searchIcon,scrollDiv,filterDiv,searchInputDiv,spanDivClass);
		    addTeamMembersList('workspace','projAdminTeamUsers','popAdminUserListContainer','addTeamUsersParticipants','teamUserlistContainer','searchAdminTeamUsersList','sysAdminTeamSearchIcon');
			
		}
		
		
		
		/* filter observer users */
		function searchProjSubscribeUsers(obj,hideDiv,searchIcon,scrollDiv,filterDiv,searchInputDiv,spanDivClass){
			var searchElement = $(obj).val().toLowerCase();
			$('.'+hideDiv).hide();
			if(searchElement!=''){
				filterProjSubscribeUsers(searchElement,hideDiv,filterDiv,spanDivClass);
				$("img#"+searchIcon).attr('title','');
				$("img#"+searchIcon).attr('src','images/workspace/remove.png');
				//$("img#"+searchIcon).attr('onclick','clearSubs();');
			    $("img#"+searchIcon).attr('onclick','clearProjTeamSearch(this,"'+hideDiv+'","'+searchIcon+'","'+scrollDiv+'","'+filterDiv+'","'+searchInputDiv+'","'+spanDivClass+'");');
				$("img#"+searchIcon).attr('title','clear');
		  }else{
				$('.'+hideDiv).show();
				$("img#"+searchIcon).attr('src','images/Projects/search.png');
				$("img#"+searchIcon).attr('onclick','');
				$("img#"+searchIcon).attr('title','search');
				// hideParticipants();
				hideAddedParticipants("div#projAdminSubscribeUsers_");
		}
			if(!isiPad){
				$("div#"+scrollDiv).mCustomScrollbar("update");
			}
			hideParticipants();
		}
		
	function filterProjSubscribeUsers(searchEle,hideDiv,filterDiv,spanDivClass){
			//$('div.projAdminSubscribeUser').find('span.projAdmimSubscribeUserName:contains("'+searchEle+'")').parents('div.projAdminSubscribeUsersDiv').show();
			$('div.'+filterDiv).find('span.'+spanDivClass+':contains("'+searchEle+'")').parents('div.'+hideDiv+'').show();
			// hideParticipants();
			hideAddedParticipants("div#projAdminSubscribeUsers_");
	}
	
		function clearProjSubsSearch(obj,hideDiv,searchIcon,scrollDiv,filterDiv,searchInputDiv,spanDivClass){
			$("input#"+searchInputDiv).val('');
			searchProjSubscribeUsers(obj,hideDiv,searchIcon,scrollDiv,filterDiv,searchInputDiv,spanDivClass);
		}
		
	 function cancelTeamUserList(id){
			$("div#"+id).css("display","none");
		}
		
		function cancelSubscribeUserList(id){
			$("div#"+id).css("display","none");	
		}
		
		function cancelAdminUserList(id){
			$("div#"+id).css("display","none");
			$("#wsInviteTransparent").hide();
		
		}
	
	 function showInviteUserPropUp(obj,obserListDiv, obserArrow,teamListDiv,teamArrow){
	 	$("button#chgSaveBtnClick").attr("onclick", "").attr("onclick", "callUsersForProjcts();");
	 	$("div#showInviteUserPropUp").html("");
	 	$('#teamUserlistContainer').show();
		$('#teamMemberUserArrow').show();
		$('#wsInviteTransparent').show();
		var buttonOffsetTop = $('#addTeamMembers').offset().top;
		var inviteUserOffset = $('#inviteUserMainDiv').offset().top;
	   	var popUpHeight = $('#inviteUserMainDiv').height();
	   	var totalHeight = inviteUserOffset + popUpHeight;
	   	var offHeight = totalHeight - buttonOffsetTop;
	   	var halfoffHeight = (offHeight / 2) + 20;
	   	
 	} 
 	
 	/*check participants*/
 	function checkProjAdminUsers(obj,adminUserClass,adminPartClass,teamUserClass,teamPartClass,sharedUserClass,sharePartClass) {
		var classObj = $(obj).attr('class');
		//$(obj).removeClass();
		//alert('test');
		if(classObj == 'userChecked') {
		    $(obj).addClass('userUnChecked');
			$(obj).removeClass('userChecked');
			if($('div.'+adminUserClass+' .userChecked').length < 1) {
				$('#'+adminPartClass).css('display','none');
			}	
			
			if($('div.'+teamUserClass+' .userChecked').length < 1) {
				$('#'+teamPartClass).hide();
			}
			
			if($('div#'+sharedUserClass+' .userChecked').length < 1) {
				$('#'+sharePartClass).hide();
			}	
			else{
				$('#'+adminPartClass).show();
				$('#'+teamPartClass).show();
				$('#'+sharePartClass).show();
				}
		}	
		else {
			$(obj).addClass('userChecked');
			$(obj).removeClass('userUnChecked');
			$('#'+adminPartClass).show();
			$('#'+teamPartClass).show();
			$('#'+sharePartClass).show();
			
		}		
 	}
 	
 	/*add admin users on double click*/
 	
		 var uData = "";
		
  function addAdminUseronDbClick(obj,adminUsersDiv,adminScroll,type){
			var addParticipantId = $(obj).attr('id');
			var sysAdminUserId = addParticipantId.split('_')[1];
			var userEmail = addParticipantId.split('_')[2];
			addedUserIds = addedUserIds + sysAdminUserId +",";
			var ids="";
	    	var projadminUserName = $('#projAdminUserName_'+sysAdminUserId).text();
	    	var sysAdminProjectId = $('#projAdminUserProject_'+sysAdminUserId).text();
	    	var sysAdminUserEmail = $('#projAdminUserName_'+sysAdminUserId).attr('title');
	    	var sysAdminUserImage = $('img#projAdminUserImage_'+sysAdminUserId).attr('src');
	    	var sysAdminProjectImage = $('div#projAdminProjImage_'+sysAdminUserId).text();
	    	var userMember = "";
	    	var userProjUserId = $('input#projAdminProjUser_'+sysAdminUserId).val();
		    var userAction = "";
		     var deleteFun = "";
		       $('.findUsersList').each(function() {
				    userMember = $(this).attr('id').split('_')[1];
				    if(sysAdminUserId == userMember){
					    $("div#TeamMembersContainer_"+userMember).remove();
					    $("div#SubscribeUsersContainer_"+userMember).remove();
			    	}
			    });
			    
			     if(userProjUserId != '0' ){
			    		userAction = "update";
			    	}else{
			    		userAction = "insert";
			   	  }
			   	  
			   	 if(type == 'sysAdmin'){
			   	 	deleteFun = "deleteSysAdmin("+sysAdminUserId+","+sysAdminProjectId+",'PO');"
			   	 }else{
			   	 	deleteFun = "removeInvRestUsersFrmUI("+sysAdminUserId+",'PO','"+type+"');"
			   	 }
			   	 
			        var userPreviousStatus=$("div#projAdminUsers_"+sysAdminUserId+" input#projAdminProjCurrStatus_"+sysAdminUserId).val();
			      	/*var emailDiv = "<div class=\"poNewUser findUsersList actFeedHover\" style=\"background:#cccccc;margin-bottom:5px;float:left; padding:1%; width:100%;border-bottom:1px solid #CCCCCC;\" id=\"AdminUsersContainer_"+sysAdminUserId+"\">"
			                       +" <div style=\"float:left; margin:5px;width: 8% \" id=\"userImg\"> <img style=\"height:30px; width:30px;\" src="+sysAdminUserImage+"></div>  "
			                       +" <div style=\"float:left; margin:10px; font-family:Tahoma;font-size:13px;width: 79% \" id=\"userName\"><span>"+projadminUserName+"</span></div> "
			                       +" <div title=\"Delete\" onclick=\""+deleteFun+"\" style=\"float:left; margin-top:2%;cursor:pointer;width: 4%\" title=\"removeAdmin\"id=\"userMinimiseImg\" class = \"actFeedHoverImgDiv\"><img style = \"width: 15px; height: 15px;\" src=\"images/minus.png\"></div>"
			                       +" <input type=\"hidden\" class=\"po_prevStatus\" value='"+userAction+"' >"
			                       +" <input type=\"hidden\" class=\"po_userId\" value='"+sysAdminUserId+"' >"
			                       +" <input type=\"hidden\" class=\"po_user_list\" value=\"\">"
			                       +" <input type=\"hidden\" class=\"po_proj_user_id\" value="+userProjUserId+">"
			                       +" <input type=\"hidden\" class=\"po_user_list\" value=\"listUser\">"
			                       +" <input type=\"hidden\" class=\"po_user_Fname\" value=\"\" >"
			                       +" <input type=\"hidden\" class=\"po_user_Lname\" value=\"\" >"
			                       +" <input type=\"hidden\" class=\"po_user_Email\" value=\"\" >"
			                       +" <input type=\"hidden\" class=\"po_user_LoginName\" value=\"\" >"
			                       +" <input type=\"hidden\" class=\"po_user_Password\" value=\"\" >"
			                     +"</div>"*/
			          var data = "<div class=\"addAdminUsers findUsersList actFeedHover\" userDetails = \"adminUsersContainer_"+sysAdminUserId+"_"+projadminUserName+"_"+userEmail+"_po\"  id=\"AdminUsersContainer_"+sysAdminUserId+"\" style=\"background:#cccccc;float:left;width:99%;border-bottom: 1px solid rgb(193, 197, 200);margin-left:8px;\">"
									+ "<div id=\"userImage\" style=\"float:left;width:5%;\"><img style=\"height:30px;width:30px;margin:10px;\" src="+sysAdminUserImage+"></div>"
									+ "<div id=\"userName\" style=\"float:left;width:40%;margin-top:15px;\">"+projadminUserName+"</div>"
									+ "<div id=\"userEmail\" style=\"float:left;width:40%;margin-top:15px;\">"+userEmail+"</div>"
									+ "<div class = \"actFeedHoverImgDiv\" title=\"Delete\" onclick=\""+deleteFun+"\" style=\"float:right;width:5%;margin-top:15px;margin-right:-3px;\"><img style=\"height:15px;width:15px;float: left;\" src=\""+path+"/images/minus.png\" /></div>"
								+"</div>";
			                     
			       
			      if(userMember == sysAdminUserId){
			         userMember = "";
		     			   }else{
				 			 $("div#adminResultContainer").prepend(data);
				 			 //$("div#adminResultContainer").mCustomScrollbar("update");
			 			}
			      $('#loadingBar').hide();
				  timerControl("");
			 hideEntAddedUser("admin", sysAdminUserId);
			
			if(type == 'sysAdmin'){
				///AddinivitedUsers("PO", sysAdminUserId);
				///uncomment the above lines to save the user on dbclick.							
			}		
			
			$(".projAdminUsers").each(function(){
						if ($('div.projAdminUsersDiv').is(':visible')) {
						
						}else{
						   $("#sysAdminUsers").html("No Users to display");
						}
					})	
		popUpScrollBar('displayUsersContainer');
	}
	
	/*add team members on double click */
	function addTeamUseronDbClick(obj,adminUsersDiv,teamScroll,type){
				 var addParticipantId = $(obj).attr('id');
				 var sysAdminTeamUserId = addParticipantId.split('_')[1];
				 var email = addParticipantId.split('_')[2];
				 addedUserIds = addedUserIds + sysAdminTeamUserId +",";
				 var projAdminTeamUserName = $('#projAdminTeamUserName_'+sysAdminTeamUserId).text();
		    	 var sysAdminTeamProjectId = $('div#projAdminTeamProject_'+sysAdminTeamUserId).text();
		    	 var sysAdminTeamUserEmail = $('#projAdminTeamUserName_'+sysAdminTeamUserId).attr('title');
		    	 var  sysAdminTeamUserImage = $('img#projAdminTeamUserImage_'+sysAdminTeamUserId).attr('src');
		    	 var sysAdminTeamProjectImage = $('div#projAdminTeamProjImage_'+sysAdminTeamUserId).text();
		    	 var prevStatus = "";
		    	 var userPreviousStatus =$("div#projAdminTeamUsers_"+sysAdminTeamUserId+" div#projAdminTeamUserCurrStatus_"+sysAdminTeamUserId).text();
		    	 var userProjUserId = $('input#projAdminTeamProjUserId_'+sysAdminTeamUserId).val();
		    	 var userAction = "";
		    	 var deleteFun = "";
		  		  $('.findUsersList').each(function() {
							    userMember = $(this).attr('id').split('_')[1];
							    if(sysAdminTeamUserId == userMember){
								    $("div#AdminUsersContainer_"+userMember).remove();
								    $("div#SubscribeUsersContainer_"+userMember).remove();
						    	}
						    });
	 				prevStatus="BU";
	 				if(userProjUserId != '0' ){
						    		userAction = "update";
						    	}else{
						    		userAction = "insert";
						   	  }
					       
					       
					if(type == 'sysAdmin'){
				   	 	deleteFun = "deleteSysAdmin("+sysAdminTeamUserId+","+sysAdminTeamProjectId+",'TM');"
				   	 }else{
				   	 	deleteFun = "removeInvRestUsersFrmUI("+sysAdminTeamUserId+",'TM','"+type+"');"
				   	 	
				   	 }
	 				
	 				/*var emailDiv = "<div  class=\"tmNewUser findUsersList actFeedHover\" style=\"background:#cccccc;margin-bottom:5px;float:left; padding:1%; width:100%;border-bottom:1px solid #CCCCCC;\" id=\"TeamMembersContainer_"+sysAdminTeamUserId+"\">"
				                     +" <div style=\"float:left; margin:5px;width: 8% \" id=\"userImg\"> <img style=\"height:30px; width:30px;\" src="+sysAdminTeamUserImage+"></div> "
				                     +" <div style=\"float:left; margin:10px; font-family:Tahoma;font-size:13px;width: 79% \" id=\"userName\"><span>"+projAdminTeamUserName+"</span></div>"
				                     +" <div title=\"Delete\" onclick=\""+deleteFun+"\" style=\"float:left; margin-top:2%;cursor:pointer;width: 4%\" title=\"Delete user\" id=\"userMinimiseImg\" class = \"actFeedHoverImgDiv\" ><img style = \"width: 15px; height: 15px;\" src=\"images/minus.png\"></div>"
				                     +" <input type=\"hidden\" class=\"tm_prevStatus\" value='"+userAction+"' >"
				                     +" <input type=\"hidden\" class=\"tm_user_list\" value=\"\" >"
			                         +" <input type=\"hidden\" class=\"tm_userId\" value='"+sysAdminTeamUserId+"' >"
			                         +" <input type=\"hidden\" class=\"tm_proj_user_id\" value="+userProjUserId+" >"
			                         +" <input type=\"hidden\" class=\"tm_user_list\" value=\"listUser\" >"
			                         +" <input type=\"hidden\" class=\"tm_user_Fname\" value=\"\" >"
			                         +" <input type=\"hidden\" class=\"tm_user_Lname\" value=\"\" >"
			                         +" <input type=\"hidden\" class=\"tm_user_Email\" value=\"\" >"
			                         +" <input type=\"hidden\" class=\"tm_user_LoginName\" value=\"\" >"
			                         +" <input type=\"hidden\" class=\"tm_user_Password\" value=\"\" >"
				                     +" </div>"*/
				    var data = "<div class=\"addTeamMembers findUsersList actFeedHover\" userDetails = \"adminUsersContainer_"+sysAdminTeamUserId+"_"+projAdminTeamUserName+"_"+email+"_tm\" id=\"AdminUsersContainer_"+sysAdminTeamUserId+"\" style=\"background:#cccccc;float:left;width:99%;border-bottom: 1px solid rgb(193, 197, 200);margin-left:8px;\">"
									+ "<div id=\"userImage\" style=\"float:left;width:5%;\"><img style=\"height:30px;width:30px;margin:10px;\" src="+sysAdminTeamUserImage+"></div>"
									+ "<div id=\"userName\" style=\"float:left;width:40%;margin-top:15px;\">"+projAdminTeamUserName+"</div>"
									+ "<div id=\"userEmail\" style=\"float:left;width:40%;margin-top:15px;\">"+email+"</div>"
									+ "<div class = \"actFeedHoverImgDiv\" title=\"Delete\" onclick=\""+deleteFun+"\" style=\"float:right;width:5%;margin-top:15px;margin-right:-3px;\"><img style=\"height:15px;width:15px;float: left;\" src=\""+path+"/images/minus.png\" /></div>"
								+"</div>";
				   
				    if($('div#teamMemberResultContainer').html() == "No users to display"){
				    	 $('div#teamMemberResultContainer').html("");
				    	 $("div#teamMemberResultContainer").css("height","");
				    	 $("div#teamMemberResultContainer").css("margin","");
				    	 
				    }
				     $('div#teamMemberResultContainer').prepend(data);
					 $('div#inviteUserTeamCont').remove(); 
					 // $("div#"+teamScroll).mCustomScrollbar("update");
					 $("div#"+teamScroll).mCustomScrollbar("destroy");
					 projectAdminInviteUserScroll(teamScroll);
				     $('#loadingBar').hide();
					 timerControl("");
					 
					 hideEntAddedUser("teammember", sysAdminTeamUserId);
					 hideParticipants();
					if(type == 'sysAdmin'){
						////AddinivitedUsers("TM", sysAdminTeamUserId);
						///uncomment the above lines to save user on dbclick							
					}	
					
					$(".projAdminTeamUsers").each(function(){
						if ($('div.projAdminTeamUsersDiv').is(':visible')) {
						
						}else{
						   $("#projAdminTeamUsers").html("No Users to display");
						}
					});
			popUpScrollBar('displayUsersContainer');
	}
	
	function addSubscribeUseronDbClick(obj,adminUsersDiv,obserScroll,type){
				var addParticipantId = $(obj).attr('id');
				var sysAdminSubscribeUserId = addParticipantId.split('_')[1];
				var email = addParticipantId.split('_')[2];
				addedUserIds = addedUserIds + sysAdminSubscribeUserId +",";
				var sysAdminSubscribeUserName = $('#projAdminSubscribeUserName_'+sysAdminSubscribeUserId).text();
		    	var sysAdminSubscribeProjectId = $('#projAdminSubscribeUserProject_'+sysAdminSubscribeUserId).text();
		    	var sysAdminSubscribeUserEmail = $('#projAdminSubscribeUserName_'+sysAdminSubscribeUserId).attr('title');
		    	var sysAdminSubscribeUserImage = $('img#projAdminSubscribeUserImage_'+sysAdminSubscribeUserId).attr('src');
		    	var sysAdminSubscribeProjectImage = $('input#projAdminSubscribeProjImage_'+sysAdminSubscribeUserId).val();
		    	var userMember = "";
		    	var deleteFun = "";
		    	 var userProjUserId = $('input#projAdminSubProjUserId_'+sysAdminSubscribeUserId).val();
		    	 var userAction = "";
		    	 $('.findUsersList').each(function() {
							    userMember = $(this).attr('id').split('_')[1];
							    if(sysAdminSubscribeUserId == userMember){
								    $("div#AdminUsersContainer_"+userMember).remove();
								    $("div#TeamMembersContainer_"+userMember).remove();
						    	}
						    });
		    	
		    	if(userProjUserId != '0' ){
						    		userAction = "update";
						    	}else{
						    		userAction = "insert";
						   	  }
					       
			   if(type == 'sysAdmin'){
				   	 	deleteFun = "deleteSysAdmin("+sysAdminSubscribeUserId+","+sysAdminSubscribeProjectId+",'SU');"
				   	 }else{
				   	 	deleteFun = "removeInvRestUsersFrmUI("+sysAdminSubscribeUserId+",'SU','"+type+"');"
				   	 	
				   	 }
				   	 
		    	
		    	var userPreviousStatus=$("div#projAdminSubscribeUsers_"+sysAdminSubscribeUserId+" input#SubscribeProjCurrStatus_"+sysAdminSubscribeUserId).val();
	 			/*var emailDiv = "<div class=\"suNewUser findUsersList actFeedHover\" style=\"background:#cccccc;margin-bottom:5px;float:left; padding:1%; width:100%;border-bottom:1px solid #CCCCCC;\" id=\"SubscribeUsersContainer_"+sysAdminSubscribeUserId+"\">"
					     				+" <div style=\"float:left; margin:5px;width: 8% \" id=\"userImg\"> <img style=\"height:30px; width:30px;\" src="+sysAdminSubscribeUserImage+"></div>"
					     				+" <div style=\"float:left; margin:10px; font-family:Tahoma;font-size:13px;width: 79% \" id=\"userName\"><span>"+sysAdminSubscribeUserName+"</span></div> "
					     				+" <div title=\"Delete\" onclick=\""+deleteFun+"\" style=\"float:left; margin-top:2%;cursor:pointer;width: 4%\" title=\"Delete user\" id=\"userMinimiseImg\" class = \"actFeedHoverImgDiv\"><img style = \"width: 15px; height: 15px;\" src=\"images/minus.png\"></div>"
					     				+" <input type=\"hidden\" class=\"su_prevStatus\" value='insert' >"
					     				+" <input type=\"hidden\" class=\"su_user_list\" value=\"\" >"
						                +" <input type=\"hidden\" class=\"su_userId\" value='"+sysAdminSubscribeUserId+"' >"
						                +" <input type=\"hidden\" class=\"su_proj_user_id\" value="+userProjUserId+" >"
						                +" <input type=\"hidden\" class=\"su_user_list\" value=\"listUser\" >"
				                        +" <input type=\"hidden\" class=\"su_user_Fname\" value=\"\" >"
				                        +" <input type=\"hidden\" class=\"su_user_Lname\" value=\"\" >"
				                        +" <input type=\"hidden\" class=\"su_user_Email\" value=\"\" >"
				                        +" <input type=\"hidden\" class=\"su_user_LoginName\" value=\"\" >"
				                        +" <input type=\"hidden\" class=\"su_user_Password\" value=\"\" >"
					     			   +"</div>"*/
					     			   var data = "<div class=\"addObservers findUsersList actFeedHover\" userDetails = \"adminUsersContainer_"+sysAdminSubscribeUserId+"_"+sysAdminSubscribeUserName+"_"+email+"_ob\" id=\"AdminUsersContainer_"+sysAdminSubscribeUserId+"\" style=\"background:#cccccc;float:left;width:100%;border-bottom: 1px solid rgb(193, 197, 200);margin-left:8px;\">"
												+ "<div id=\"userImage\" style=\"float:left;width:5%;\"><img style=\"height:30px;width:30px;margin:10px;\" src="+sysAdminSubscribeUserImage+"></div>"
												+ "<div id=\"userName\" style=\"float:left;width:40%;margin-top:15px;\">"+sysAdminSubscribeUserName+"</div>"
												+ "<div id=\"userEmail\" style=\"float:left;width:40%;margin-top:15px;\">"+email+"</div>"
												+ "<div class = \"actFeedHoverImgDiv\" title=\"Delete\" onclick=\""+deleteFun+"\" style=\"float:right;width:5%;margin-top:15px;margin-right:-3px;\"><img style=\"height:15px;width:15px;float: left;\" src=\""+path+"/images/minus.png\" /></div>"
											+"</div>";
					     			   if(userMember == sysAdminSubscribeUserId){
					     			   userMember = "";
					     			   }else{
								 			$('div#observseResultContainer').prepend(data);
						 			}
				// $("div#"+obserScroll).mCustomScrollbar("update");
				//$("div#"+obserScroll).mCustomScrollbar("destroy");
				projectAdminInviteUserScroll(obserScroll);
				$('div#inviteUserObvCont').remove(); 
				$('#loadingBar').hide();
				timerControl("");
				
				hideEntAddedUser("observer", sysAdminSubscribeUserId);
        		// hideParticipants();
				/*if(type == 'sysAdmin'){
					///AddinivitedUsers("SU", sysAdminSubscribeUserId);	
					///uncomment the above line to add the user on db click.						
				}*/     
				$(".projAdminObvUsers").each(function(){
					if ($('div.projAdminSubscribeUsersDiv').is(':visible')) {
					
					}else{
					   $("#sysAdminSubscribeUsers").html("No Users to display");
					}
				});		
			popUpScrollBar('displayUsersContainer');		
		}
	
	
		function addProjAdminParticipants(obj,status,partTypeClass,checkUserConId,appendDiv,scrollDiv,type){
				$('#loadingBar').show();
				timerControl("start");
				var addParticipantId = $(obj).attr('id');
				var projectUserCount = $('.userChecked').length;
				var sysAdminTeamUserId ="";
				var projAdminTeamUserName ="";
				var sysAdminTeamUserEmail = "";
				var sysAdminTeamUserImage ="";
				var sysAdminTeamProjectId ="";
				var sysAdminTeamProjectImage ="";
				var userCurrentStatus='';
				var userPreviousStatus = "";
				var userProjUserId = "";
				var userAction = "";
				var deleteFun ="";
				 if(addParticipantId != null && addParticipantId != 0 && addParticipantId == "addAdminUserParticipants") {
		   			 var ids=",";
		   			 var emailDiv = "";
					$('div#'+checkUserConId+' .userChecked').each(function() {
				    	 sysAdminUserId = $(this).prev().attr('id').split('_')[1];
				    	 addedUserIds = addedUserIds + sysAdminUserId +",";
				    	 ids=ids+sysAdminUserId+",";
				    	 projadminUserName = $('#projAdminUserName_'+sysAdminUserId).text();
				    	 sysAdminProjectId = $('#projAdminUserProject_'+sysAdminUserId).text();
				    	 sysAdminUserEmail = $('#projAdminUserName_'+sysAdminUserId).attr('title');
				    	 sysAdminUserImage = $('img#projAdminUserImage_'+sysAdminUserId).attr('src');
				    	 sysAdminProjectImage = $('input#projAdminProjImage_'+sysAdminUserId).text();
				    	 sysAdminProjectname  = $('div#sysAdminProjectName_'+sysAdminUserId).text();
				    	 userProjUserId = $('input#projAdminProjUser_'+sysAdminUserId).val();
				    	 var userMember = "";
				    	 
						    $('.findUsersList').each(function() {
							    userMember = $(this).attr('id').split('_')[1];
							    if(sysAdminUserId == userMember){
							    $("div#TeamMembersContainer_"+userMember).remove();
							    $("div#SubscribeUsersContainer_"+userMember).remove();
							    
						    	}
						    });
						    
						    if(userProjUserId != '0' ){
						    	userAction = "update";
						    }else{
						    	userAction = "insert";
						    }
						    
						    if(type == 'sysAdmin'){
						   	 	deleteFun = "deleteSysAdmin("+sysAdminUserId+","+sysAdminProjectId+",'PO');"
						   	 }else{
						   	 	deleteFun = "removeInvRestUsersFrmUI("+sysAdminUserId+",'PO','"+type+"');"
				   	 		}
					       	 var userPreviousStatus=$("div#projAdminUsers_"+sysAdminUserId+" input#projAdminProjCurrStatus_"+sysAdminUserId).val();	
						        /*emailDiv = "<div class=\"poNewUser findUsersList actFeedHover\" style=\"background:#cccccc;margin-bottom:5px;float:left; padding:1%; width:100%;border-bottom:1px solid #CCCCCC;\" id=\"AdminUsersContainer_"+sysAdminUserId+"\">"
						                       +" <div style=\"float:left; margin:5px;width: 8% \" id=\"userImg\"> <img style=\"height:30px; width:30px;\" src="+sysAdminUserImage+"></div>  "
						                       +" <div style=\"float:left; margin:10px; font-family:Tahoma;font-size:13px;width: 79% \" id=\"userName\"><span>"+projadminUserName+"</span></div> "
						                       +" <div title=\"Delete\"  onclick=\""+deleteFun+"\" style=\"float:left; margin-top:2%;cursor:pointer;width: 4%\" id=\"userMinimiseImg\" class = \"actFeedHoverImgDiv\"><img style = \"width: 15px; height: 15px;\" src=\"images/minus.png\"></div>"
						                       +" <input type=\"hidden\" class=\"po_prevStatus\" value='"+userAction+"' >"
						                       +" <input type=\"hidden\" class=\"po_user_list\" value=\"\" >"
						                       +" <input type=\"hidden\" class=\"po_userId\" value='"+sysAdminUserId+"' >"
						                       +" <input type=\"hidden\" class=\"po_proj_user_id\" value="+userProjUserId+" >"
						                       +" <input type=\"hidden\" class=\"po_user_list\" value=\"listUser\" >"
						                       +" <input type=\"hidden\" class=\"po_user_Fname\" value=\"\">"
						                       +" <input type=\"hidden\" class=\"po_user_Lname\" value=\"\" >"
						                       +" <input type=\"hidden\" class=\"po_user_Email\" value=\"\" >"
						                       +" <input type=\"hidden\" class=\"po_user_LoginName\" value=\"\" >"
						                       +" <input type=\"hidden\" class=\"po_user_Password\" value=\"\" >"
						                       +" </div>"*/
						       emailDiv = "<div class=\"addAdminUsers findUsersList actFeedHover\" userDetails = \"adminUsersContainer_"+sysAdminUserId+"_"+projadminUserName+"_"+userEmail+"_po\"  id=\"AdminUsersContainer_"+sysAdminUserId+"\" style=\"background:#cccccc;float:left;width:100%;border-bottom: 1px solid rgb(193, 197, 200);margin-left:8px;\">"
									+ "<div id=\"userImage\" style=\"float:left;width:5%;\"><img style=\"height:30px;width:30px;margin:10px;\" src="+sysAdminUserImage+"></div>"
									+ "<div id=\"userName\" style=\"float:left;width:40%;margin-top:15px;\">"+projadminUserName+"</div>"
									+ "<div id=\"userEmail\" style=\"float:left;width:40%;margin-top:15px;\">"+userEmail+"</div>"
									+ "<div class = \"actFeedHoverImgDiv\" title=\"Delete\" onclick=\""+deleteFun+"\" style=\"float:right;width:5%;margin-top:15px;margin-right:-3px;\"><img style=\"height:15px;width:15px;float: left;\" src=\""+path+"/images/minus.png\" /></div>"
								+"</div>";
						       if(userMember == sysAdminUserId){
						           userMember = "";
				     	  			}else{
				     	  			$('div#adminResultContainer').prepend(emailDiv);
							 	    //$("div#"+scrollDiv).mCustomScrollbar("update");
							 	    //$("div#"+scrollDiv).mCustomScrollbar("destroy");
									//projectAdminInviteUserScroll(scrollDiv);
							 	   
					 			}
						     $('#loadingBar').hide();
							 timerControl("");
							  hideEntAddedUser("admin", sysAdminUserId);
						});
						// hideParticipants();
						
						/*if(type == 'sysAdmin'){
							////AddinivitedUsers("PO", "CheckBox");							
						}*/
						 emailDiv = "";
						 
						 $(".projAdminUsers").each(function(){
							if ($('div.projAdminUsersDiv').is(':visible')) {
							
							}else{
							   $("#sysAdminUsers").html("No Users to display");
							}
						});			
				}
				
				 else if(addParticipantId != null && addParticipantId != 0 && addParticipantId == "addTeamUsersParticipants") {
					 	 var ids=",";
						 var prevStatus = ",";
						 var emailDiv = "";
						 $('div#'+checkUserConId+' .userChecked').each(function() {
					    	 sysAdminTeamUserId = $(this).prev().attr('id').split('_')[1];
					    	 addedUserIds = addedUserIds + sysAdminTeamUserId +",";
					    	 ids=ids+sysAdminTeamUserId+",";
					    	 projAdminTeamUserName = $('#projAdminTeamUserName_'+sysAdminTeamUserId).text();
					    	 sysAdminTeamProjectId = $('div#projAdminTeamProject_'+sysAdminTeamUserId).text();
					    	 sysAdminTeamUserEmail = $('#projAdminTeamUserName_'+sysAdminTeamUserId).attr('title');
					    	 sysAdminTeamUserImage = $('img#projAdminTeamUserImage_'+sysAdminTeamUserId).attr('src');
					    	 sysAdminTeamProjectImage = $('input#projAdminTeamUserImage_'+sysAdminTeamUserId).text();
					    	 var teamUserPreviousStatus =$("input#projAdminTeamUserCurrStatus_"+sysAdminTeamUserId).val();
					    	 userProjUserId = $('input#projAdminTeamProjUserId_'+sysAdminTeamUserId).val();
					    	 var userMember = "";
					    	 var teamUserCurrentStatus = 'TM';
					    	 userAction = "";
					    	
					   				$('.findUsersList').each(function() {
								    userMember = $(this).attr('id').split('_')[1];
								    if(sysAdminTeamUserId == userMember){
									    $("div#AdminUsersContainer_"+userMember).remove();
									    $("div#SubscribeUsersContainer_"+userMember).remove();
							    	}
							    });
							     if(userProjUserId != '0' ){
							    	userAction = "update";
							    }else{
							    	userAction = "insert";
							    }
							   
							    if(type == 'sysAdmin'){
							   	 	deleteFun = "deleteSysAdmin("+sysAdminTeamUserId+","+sysAdminTeamProjectId+",'TM');"
							   	 }else{
							   	 	deleteFun = "removeInvRestUsersFrmUI("+sysAdminTeamUserId+",'TM','"+type+"');"
					   	 		}
					       emailDiv = "<div  class=\"tmNewUser findUsersList actFeedHover\" style=\"background:#cccccc;margin-bottom:5px; float:left; padding:1%; width:100%;border-bottom:1px solid #CCCCCC;\" id=\"TeamMembersContainer_"+sysAdminTeamUserId+"\">"
					                     +" <div style=\"float:left; margin:5px;width: 8% \" id=\"userImg\"> <img style=\"height:30px; width:30px;\" src="+sysAdminTeamUserImage+"></div> "
					                     +" <div style=\"float:left; margin:10px; font-family:Tahoma;font-size:13px;width: 79%; \" id=\"userName\"><span>"+projAdminTeamUserName+"</span></div>"
					                     +" <div onclick=\""+deleteFun+"\" title=\"Delete\" style=\"float:left; margin-top:2%;cursor:pointer;width: 4%\" id=\"userMinimiseImg\" class = \"actFeedHoverImgDiv\"><img style = \"width: 15px; height: 15px;\" src=\"images/minus.png\"></div>"
					                     +" <input type=\"hidden\" class=\"tm_prevStatus\" value='"+userAction+"' >"
					                     +" <input type=\"hidden\" class=\"tm_user_list\" value=\"\" >"
				                         +" <input type=\"hidden\" class=\"tm_userId\" value='"+sysAdminTeamUserId+"' >"
				                         +" <input type=\"hidden\" class=\"tm_proj_user_id\" value="+userProjUserId+" >"
				                         +" <input type=\"hidden\" class=\"tm_user_list\" value=\"listUser\" >"
				                         +" <input type=\"hidden\" class=\"tm_user_Fname\" value=\"\" >"
				                         +" <input type=\"hidden\" class=\"tm_user_Lname\" value=\"\" >"
				                         +" <input type=\"hidden\" class=\"tm_user_Email\" value=\"\" >"
				                         +" <input type=\"hidden\" class=\"tm_user_LoginName\" value=\"\" >"
				                         +" <input type=\"hidden\" class=\"tm_user_Password\" value=\"\" >"
				                         +"</div>"
				             if(userMember == sysAdminTeamUserId){
				              	userMember = "";
					     	 }else{
					 	 		$('div#'+appendDiv).prepend(emailDiv);
					 	 	   // $("div#"+scrollDiv).mCustomScrollbar("update");
					 	 	   		 //$("div#"+scrollDiv).mCustomScrollbar("destroy");
									 //projectAdminInviteUserScroll(scrollDiv);
						 		}
						      $('div#inviteUserTeamCont').remove();       
							 
						      parent.$('#loadingBar').hide();
							  parent.timerControl("");
							  hideEntAddedUser("teammember", sysAdminTeamUserId);
							  
							  $(".projAdminTeamUsers").each(function(){
								if ($('div.projAdminTeamUsersDiv').is(':visible')) {
								
								}else{
								   $("#projAdminTeamUsers").html("No Users to display");
								}
							});
					});
					// hideParticipants();
					
					/*if(type == 'sysAdmin'){
						////AddinivitedUsers("TM", "CheckBox");
						uncomment the block to add users on click 								
					}*/	
					 emailDiv = " ";				
				}
				
			 else if(addParticipantId != null && addParticipantId != 0 && addParticipantId == "addSubscribeUsersParticipants") {
					 var ids=",";
					  var emailDiv= "";
					 $('div#'+checkUserConId+' .userChecked').each(function() {
			    	 sysAdminSubscribeUserId = $(this).prev().attr('id').split('_')[1];
			    	 addedUserIds = addedUserIds + sysAdminSubscribeUserId +",";
			    	 ids=ids+sysAdminSubscribeUserId+",";
			    	 sysAdminSubscribeUserName = $('#projAdminSubscribeUserName_'+sysAdminSubscribeUserId).text();
			    	 sysAdminSubscribeProjectId = $('#projAdminSubscribeUserProject_'+sysAdminSubscribeUserId).text();
			    	 sysAdminSubscribeUserEmail = $('#projAdminSubscribeUserName_'+sysAdminSubscribeUserId).attr('title');
			    	 sysAdminSubscribeUserImage = $('img#projAdminSubscribeUserImage_'+sysAdminSubscribeUserId).attr('src');
			    	 sysAdminSubscribeProjectImage = $('input#projAdminSubscribeProjImage_'+sysAdminSubscribeUserId).val();
			    	 userProjUserId = $('input#projAdminSubProjUserId_'+sysAdminSubscribeUserId).val();
			    	 var userMember = "";
			    	 userAction = "";
			    	 
			    	 			$('.findUsersList').each(function() {
							    userMember = $(this).attr('id').split('_')[1];
							     if(sysAdminSubscribeUserId == userMember){
								    $("div#AdminUsersContainer_"+userMember).remove();
								    $("div#TeamMembersContainer_"+userMember).remove();
						    	}
						    });
						    
						    if(userProjUserId != '0' ){
						    	userAction = "update";
						    }else{
						    	userAction = "insert";
						    }
			    	
			    	
			    			if(type == 'sysAdmin'){
						   	 	deleteFun = "deleteSysAdmin("+sysAdminSubscribeUserId+","+sysAdminSubscribeProjectId+",'SU');"
						   	 }else{
						   	 	deleteFun = "removeInvRestUsersFrmUI("+sysAdminSubscribeUserId+",'SU','"+type+"');"
				   	 		}
				         var subUserPreviousStatus=$("div#projAdminSubscribeUsers_"+sysAdminSubscribeUserId+" input#SubscribeProjCurrStatus_"+sysAdminSubscribeUserId).val();
					      emailDiv = "<div class=\"suNewUser findUsersList actFeedHover\" style=\"background:#cccccc;margin-bottom:5px;float:left; padding:1%; width:100%;border-bottom:1px solid #CCCCCC;\" id=\"SubscribeUsersContainer_"+sysAdminSubscribeUserId+"\">"
					     				+" <div style=\"float:left; margin:5px;width: 8% \" id=\"userImg\"> <img style=\"height:30px; width:30px;\" src="+sysAdminSubscribeUserImage+"></div>"
					     				+" <div style=\"float:left; margin:10px; font-family:Tahoma;font-size:13px;width: 79% \" id=\"userName\"><span>"+sysAdminSubscribeUserName+"</span></div> "
					     				+" <div title=\"Delete\"  onclick=\""+deleteFun+"\" style=\"float:left; margin-top:2%;cursor:pointer;width: 4%\" id=\"userMinimiseImg\" class = \"actFeedHoverImgDiv\"><img style = \"width: 15px; height: 15px;\" src=\"images/minus.png\"></div>"
					     				+" <input type=\"hidden\" class=\"su_prevStatus\" value='"+userAction+"' >"
					     				+" <input type=\"hidden\" class=\"su_user_list\" value=\"\" >"
						                +" <input type=\"hidden\" class=\"su_userId\" value='"+sysAdminSubscribeUserId+"' >"
						                +" <input type=\"hidden\" class=\"su_proj_user_id\" value="+userProjUserId+" >"
						                +" <input type=\"hidden\" class=\"su_user_list\" value=\"listUser\" >"
				                        +" <input type=\"hidden\" class=\"su_user_Fname\" value=\"\" >"
				                        +" <input type=\"hidden\" class=\"su_user_Lname\" value=\"\" >"
				                        +" <input type=\"hidden\" class=\"su_user_Email\" value=\"\" >"
				                        +" <input type=\"hidden\" class=\"su_user_LoginName\" value=\"\" >"
				                        +" <input type=\"hidden\" class=\"su_user_Password\" value=\"\" >"
					     			    +" </div>"
					     			   if(userMember == sysAdminSubscribeUserId){
					     			    userMember = "";
					     			   }else{
								 			 $('div#'+appendDiv).prepend(emailDiv);
							 	  			//  $("div#"+scrollDiv).mCustomScrollbar("update");
							 	  			
							 	  			//$("div#"+scrollDiv).mCustomScrollbar("destroy");
									 		//projectAdminInviteUserScroll(scrollDiv);
						 			}
						 $('div#inviteUserObvCont').remove(); 
						 parent.$('#loadingBar').hide();
						 parent.timerControl("");
						 hideEntAddedUser("observer", sysAdminSubscribeUserId);
						
					});
					// hideParticipants();
					
					/*if(type == 'sysAdmin'){
						AddinivitedUsers("SU", "CheckBox");		
						uncomment the block to add users on click 					
					}*/	
					 emailDiv = " ";	
					 
					 
					 $(".projAdminObvUsers").each(function(){
						if ($('div.projAdminSubscribeUsersDiv').is(':visible')) {
						
						}else{
						   $("#sysAdminSubscribeUsers").html("No Users to display");
						}
					});								
				}
				if(type=='sysAdmin'){
				  $(".findUsersList").css("background","none");
				}
		$('#loadingBar').hide();
		timerControl("");
	}
	
	/* remove admin users */
	  var rAdminPUId = "";
	  var rAdminUId = "";
 	function removeInvitedAdminUser(status,projUserId,userId){
	 	 rAdminPUId = projUserId;
	 	 rAdminUId = userId;
	 	  parent.confirmFun(getValues(companyAlerts,"Alert_remove"),"delete","removeAdminUsers");
	 	  $('#transparentDiv').show();
	 	 //un-comment the below lines if the constraint have to be imposed saying that any 1 random user must be there as project administrator. 
	 	 /*var d = $("#sysAdminUserContainer > div ").length;
 		 if(d == 1){
 		 parent.alertFun("Project must have atleast 1 administrator",'warning');
 		 return false;
 		 }
	 	 else {
	 	 parent.confirmFun("Do you want to delete ?","delete","removeAdminUsers");
	 	 }*/
 	}
 	
 	function removeAdminUsers(){
	  	parent.$('#loadingBar').show();
		parent.timerControl("start");
			$.ajax({
			      	url: path+"/paLoad.do",
					type:"POST",
					data:{act:"deleteProjAdminUsers",userId:rAdminUId,projId:wrkFlowProjId},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
					parent.checkSessionTimeOut(result);
					    userDeleted = new Boolean(true);
						$("div#AdminUsersContainer_"+rAdminUId).remove();
						$("div#AdminUsersContainer_"+rAdminUId).html(" ");
						$('#inviteUserMainDiv').show();
						parent.$('#loadingBar').hide();
						parent.timerControl("");
					}
				  });
	 }
 	
 	/* remove rest users */
 	
	  var rPUid = "";
	  var rInivteStatus ="";
	  var rInvUserId = "";
 	function removeInvitedUser(status,projUserId,userId){
	 	 rPUid = projUserId;
	 	 rInivteStatus = status;
	 	 rInvUserId = userId;
	 	 parent.confirmFun(getValues(companyAlerts,"Alert_delete"),"delete","removeInvitedUsers");
	 	 $('#transparentDiv').show();
 	}
 	
  var userDeleted = new Boolean(false);
 	function removeInvitedUsers(){
	  $('#loadingBar').show();
	  timerControl("start");
			$.ajax({
			      	url: path+"/paLoad.do",
					type:"POST",
					data:{act:"deleteInivtedUsers",userId:rInvUserId,projId:wrkFlowProjId},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
					checkSessionTimeOut(result);
					userDeleted = new Boolean(true);
					if(rInivteStatus == 'TM'){
						$("div#TeamMembersContainer_"+rInvUserId).remove();
						$("div#TeamMembersContainer_"+rInvUserId).html(" ");
					}
					else{
						$("div#SubscribeUsersContainer_"+rInvUserId).remove();
						$("div#SubscribeUsersContainer_"+rInvUserId).html(" ");
					}
					///$("div#sysAdminUserDetails").mCustomScrollbar("update");
						$(".checkObsDivExist").each(function() {
							if($(".subscribeUsers").size() == "0"){
									$("#sysAdminSubscribeMainDiv").html("<div style=\"float:right; margin:10px;float:left;font-family:tahoma;font-size:13px;\" id=\"inviteUserObvCont\">No Users to display</div>");
								}
					   });	
					   
					   $(".checkTeamDivExist").each(function(){
						   if($(".teamMembers").size() == "0"){
						   		$("#sysAdminTeamMainDiv").html("<div style=\"float:right; margin:10px;float:left;font-family:tahoma;font-size:13px;\" id=\"inviteUserTeamCont\">No Users to display</div>");
						   	}
					   });
					    $("#projAdminUsers_"+rInvUserId).css("display","block");
						$("#projAdminTeamUsers_"+rInvUserId).css("display","block");
						$("#projAdminSubscribeUsers_"+rInvUserId).css("display","block");
					$('#inviteUserMainDiv').show();	
					$('#loadingBar').hide();
					timerControl("");
					}
				  });
	 }
 	
 
 	/*delete from UI*/
 	var delFrmUIuId = "";
 	var delFrmUIstatus = "";
 	var delFrmUIType = ""; 
 	function removeInvUsersFromUI(userId,status,type){
	 	delFrmUIuId = userId;
	 	delFrmUIstatus = status;
	 	delFrmUIType = type; 
 	 	var d = $("#sysAdminUserContainer > div ").length;
	 	 if( d == 1){
	 	 	parent.alertFun(getValues(companyAlerts,"Alert_UsrNoDel"),'warning');
	 	 }else{
	 	 	parent.confirmFun(getValues(companyAlerts,"Alert_delete"),"delete","removeInUsersFrmUI");
	 	 }
	 	
 	}
 	
 	function removeInUsersFrmUI(){
 		 if(delFrmUIstatus == 'PO'){
	 			$("div#AdminUsersContainer_"+delFrmUIuId).remove();
	 		}
 		
 		
 		else if(dRFrmUIStatus == 'TM'){
 			$("div#TeamMembersContainer_"+dRFrmUIuId).remove();
 		}
 		else{
 			$("div#SubscribeUsersContainer_"+dRFrmUIuId).remove();
 		}
 		 if(delFrmUIType=='sysAdmin'){
 			deleteUsers();
 		}
 	}
 	
 	
 	var dRFrmUIuId = "";
 	var dRFrmUIStatus = "";
 	var dRFrmUIType = "";
 	function removeInvRestUsersFrmUI(userId,status,type ){
	 	 dRFrmUIuId = userId;
	 	 dRFrmUIStatus = status;
	 	 dRFrmUIType = type;
 		parent.confirmFun(getValues(companyAlerts,"Alert_remove"),"clear","removeRestInFrmUI");
 	
 	}
 	
 	function removeRestInFrmUI(){
 			
 	if(dRFrmUIStatus == 'PO'){
	    $("div#AdminUsersContainer_"+dRFrmUIuId).remove();
	 }
 	 else if(dRFrmUIStatus == 'TM'){
 	    $("div#AdminUsersContainer_"+dRFrmUIuId).remove();
 	  }
 		else{
 			$("div#AdminUsersContainer_"+dRFrmUIuId).remove();
 		}
 		 if(dRFrmUIType=='sysAdmin'){
 			deleteUsers();
 		}
 	}
 	/*delete system admin participant users*/
 	var delSysAdminUId = "";
 	var delSysAdminPId = "";
 	var delSysAdminPUstat = "";
 	function deleteSysAdmin(userId,projectId, projUserStatus){
 		delSysAdminUId = userId;
 		delSysAdminPId = projectId;
 		delSysAdminPUstat = projUserStatus;
 		 var d = $("#sysAdminUserContainer > div ").length;
 		 if( projUserStatus == 'PO' && d == 1){
	 	 	parent.alertFun(getValues(companyAlerts,"Alert_UsrNoDel"),'warning');
	   }else{
 			parent.confirmFun(getValues(companyAlerts,"Alert_delete"),"delete","deleteSysAdminPart");
 		}
 	}
 	
 	function deleteSysAdminPart(){
 		parent.$('#loadingBar').show();
		parent.timerControl("start");
	 		$.ajax({
			      	url: path+"/paLoad.do",
					type:"POST",
					data:{act:"deleteInivtedSysAdminUsers",userId:delSysAdminUId,projId:delSysAdminPId,projUserStatus:delSysAdminPUstat},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
					parent.checkSessionTimeOut(result);
					if(delSysAdminPUstat == 'PO'){
					    $("div#AdminUsersContainer_"+delSysAdminUId).remove();
						$("div#AdminUsersContainer_"+delSysAdminUId).html(" ");
					} 
					
					else if(delSysAdminPUstat == 'TM'){
						$("div#TeamMembersContainer_"+delSysAdminUId).remove();
						$("div#TeamMembersContainer_"+delSysAdminUId).html(" ");
					}
					else{
						$("div#SubscribeUsersContainer_"+delSysAdminUId).remove();
						$("div#SubscribeUsersContainerr_"+delSysAdminUId).html(" ");
					}
					
			$("div#sysAdminUserDetails").mCustomScrollbar("update");
			parent.$('#loadingBar').hide();
			parent.timerControl("");
			}
		  });
 		}
 		
 		function prepareAdminUI(jsonResult,divName){
 		var jsonData = jQuery.parseJSON(jsonResult);
        var sysAdminUI="";
        if(jsonData){
        	var json = eval(jsonData);
        	 for(var i=0;i<json.length;i++){
        	 sysAdminUI  +="<div class=\"projAdminUsersDiv\" id=\"projAdminUsers_"+json[i].user_id+"\" style=\"height:35px;width: 100%;float:left; padding-bottom: 5px;padding-top: 3px; border-bottom: 1px solid #BFBFBF;\">"
						 + "<div id=\"projAdminUserName_"+json[i].user_id+"\" title=\""+json[i].userName+"\"  style=\"width: 70%;float:left;color: #848484;overflow: hidden;padding-left: 5px;margin-top: 3px;display:none; overflow: hidden;text-overflow: ellipsis;white-space: nowrap;\">"+json[i].userName+"</div>"
						 + "<div id=\"projAdminUserProject_"+json[i].user_id+"\" title=\""+json[i].userName+"\"  style=\"width: 70%;float:left;color: #848484;overflow: hidden;padding-left: 5px;margin-top: 3px;display:none;\">"+json[i].projId+"</div>"
						 + "<input type=\"hidden\" id=\"projAdminProjImage_"+json[i].user_id+"\" value=\"'"+json[i].projImageUrl+"'\" />"
					     + "<input type=\"hidden\" id=\"projAdminProjUser_"+json[i].user_id+"\" value=\""+json[i].user_id+"\" />"
						 + "<img id=\"projAdminUserImage_"+json[i].user_id+"\" title=\""+json[i].userName+"\" style=\"height:25px; width: 25px;float: left;\" onerror=\"javascript:imageOnErrorUserImgReplace(this);\">"
						 + "<div class=\"projAdminProjUserName\"  ondblclick=\"addAdminUseronDbClick(this,'sysAdminUserContainer','sysAdminUserDetails','"+json[i].addUserType+"');\"  id=\"projAdminUserName_"+json[i].user_id+"_"+json[i].userEmail+"\"   style=\"cursor:pointer;width: 70%;float:left;color: #848484;padding-left: 5px;margin-top: 3px;font-size:13px;font-family:Tahoma;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;\">"+json[i].userName+"<span style=\"display:none;\" class=\"projadminUserName\">"+json[i].userName.toLowerCase()+"</span></div>"
						 + "<div class=\"userUnChecked\" style=\"float: right;width:25px;height: 25px;cursor:pointer;margin:5px 5px 0 0;\" onclick=\"checkProjAdminUsers(this,'projAdminUsersDiv','addAdminUserParticipants','projAdminTeamUsersDiv','addTeamUsersParticipants','userListSharedDiv','addSubscribeUsersParticipants');\"></div>"
						 + " </div>"; 
        	 }
        }
        else {
        sysAdminUI = "<p style='font-family:tahoma;font-size:13px;'>No users available.</p>" ;
        }
        $("div#adminUserListContainer").show();
        $('#'+divName).html(sysAdminUI);
 	}
 	
 	function prepareTeamMemberUI(jsonResult,divName){
 		var jsonData = jQuery.parseJSON(jsonResult);
        var teamMemberUI="";
        if(jsonData){
        	var json = eval(jsonData);
        	 for(var i=0;i<json.length;i++){
        	 teamMemberUI  += " <div class=\"projAdminTeamUsersDiv\" id=\"projAdminTeamUsers_"+json[i].user_id+"\" style=\"height:35px;width: 100%;float:left; padding-bottom: 5px;padding-top: 3px; border-bottom: 1px solid #BFBFBF;\">"
							+ "<div id=\"projAdminTeamUserName_"+json[i].user_id+"\" title=\""+json[i].userName+"\"  style=\"width: 70%;float:left;color: #848484;overflow: hidden;padding-left: 5px;margin-top: 3px;display:none; overflow: hidden;text-overflow: ellipsis;white-space: nowrap;\">"+json[i].userName+"</div>"
							+ "<div id=\"projAdminTeamProject_"+json[i].user_id+"\" title=\""+json[i].userName+"\"  style=\"width: 70%;float:left;color: #848484;overflow: hidden;padding-left: 5px;margin-top: 3px;display:none;\">"+json[i].projId+"</div>"
							+ "<input type=\"hidden\" id=\"projAdminTeamProjImage_"+json[i].user_id+"\" value=\"'"+json[i].projImageUrl+"'\" />"
							+ "<input type=\"hidden\" id=\"projAdminTeamProjUserId_"+json[i].user_id+"\" value=\""+json[i].user_id+"\" />"
							+ "<img id=\"projAdminTeamUserImage_"+json[i].user_id+"\" title=\""+json[i].userName+"\" style=\"height:25px; width: 25px;float: left;\" src=\""+json[i].imageUrl+"\" onerror=\"javascript:imageOnErrorUserImgReplace(this);\">"
							+ "<div class=\"projAdminTeamUserName\" ondblclick=\"addTeamUseronDbClick(this,'sysAdminTeamMainDiv','sysAdminUserDetails','"+json[i].type+"');\"   id=\"projAdminTeamUserName_"+json[i].user_id+"_"+json[i].userEmail+"\"  title=\""+json[i].userName+"\" style=\"cursor:pointer;width: 70%;float:left;color: #848484;padding-left: 5px;margin-top: 3px;font-size:13px;font-family:Tahoma;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;\">"+json[i].userName+"<span class=\"projAdmimTeamUserName\" style=\"display:none;\">"+json[i].userName.toLowerCase()+"</span></div>"
							+"<div class=\"userUnChecked\" style=\"float: right;width:25px;height: 25px;cursor:pointer;margin:5px 5px 0 0;\" onclick=\"checkProjAdminUsers(this,'projAdminUsersDiv','addAdminUserParticipants','projAdminTeamUsersDiv','addTeamUsersParticipants','userListSharedDiv','addSubscribeUsersParticipants');\"></div>"
							+"</div>";
        	 }
        }
        else {
        teamMemberUI = "<p style='font-family:tahoma;font-size:13px;'>No users available.</p>" ;
        }
        $("div#teamUserlistContainer").show();
        $('#'+divName).html(teamMemberUI);
 	}
 	
 	function prepareObserversUI(jsonResult,divName){
 		var jsonData = jQuery.parseJSON(jsonResult);
        var observerUI="";
        if(jsonData){
        	var json = eval(jsonData);
        	 for(var i=0;i<json.length;i++){
        	 observerUI  +="<div class=\"projAdminSubscribeUsersDiv\" id=\"projAdminSubscribeUsers_"+json[i].user_id+"\" style=\"height:35px;width: 100%;float:left; padding-bottom: 5px;padding-top: 3px; border-bottom: 1px solid #BFBFBF;\">"
						 +"<div id=\"projAdminSubscribeUserName_"+json[i].user_id+"\" title=\""+json[i].userName+"\" style=\"width: 70%;float:left;color: #848484;overflow: hidden;padding-left: 5px;margin-top: 3px;display:none; overflow: hidden;text-overflow: ellipsis;white-space: nowrap;\">"+json[i].userName+"</div>"
						 +"<div id=\"projAdminSubscribeUserProject_"+json[i].user_id+"\" title=\""+json[i].userName+"\"  style=\"width: 70%;float:left;color: #848484;overflow: hidden;padding-left: 5px;margin-top: 3px;display:none;\">"+json[i].projId+"</div>"
						 +"<input type=\"hidden\" id=\"projAdminSubscribeProjImage_"+json[i].user_id+"\" value=\"'"+json[i].projImageUrl+"'\" />"
					     +"<input type=\"hidden\" id=\"projAdminSubProjUserId_"+json[i].user_id+"\" value=\""+json[i].user_id+"\" />"
						 +"<img id=\"projAdminSubscribeUserImage_"+json[i].user_id+"\" title=\""+json[i].userName+"\"style=\"height:25px; width: 25px;float: left;\" src=\""+json[i].imageUrl+"\" onerror=\"javascript:imageOnErrorUserImgReplace(this);\">"
						 +"<div class=\"projAdminSubscribeUser\" ondblclick=\"addSubscribeUseronDbClick(this,'sysAdminSubscribeMainDiv','sysAdminUserDetails','"+json[i].type+"');\" id=\"projAdminSubscribeUserName_"+json[i].user_id+"_"+json[i].userEmail+"\"  title=\""+json[i].userName+"\"  style=\"cursor:pointer;width: 70%;float:left;color: #848484;padding-left: 5px;margin-top: 3px;font-size:13px;font-family:Tahoma;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;\">"+json[i].userName+"<span class=\"projAdmimSubscribeUserName\" style=\"display:none;\">"+json[i].userName.toLowerCase()+"</span></div>"
						 +"<div class=\"userUnChecked\" style=\"float: right;width:25px;height: 25px;cursor:pointer;margin:5px 5px 0 0;\" onclick=\"checkProjAdminUsers(this,'projAdminUsersDiv','addAdminUserParticipants','projAdminTeamUsersDiv','addTeamUsersParticipants','userListSharedDiv','addSubscribeUsersParticipants');\"></div>"
						 +"</div>";
        	 }
        }
        else {
        observerUI = "<p style='font-family:tahoma;font-size:13px;'>No users available.</p>" ;
        }
        $("div#SubscribeUserlistContainer").show();
        $('#'+divName).html(observerUI);
 	}
 	
 	var saveInvitedData = "";
 	function saveInvitedUsers() {
 		var check = $('select#sysAdminProjStatusSort option:selected').val();
 		if(check == 'D'){
 		parent.confirmFun(getValues(companyAlerts,"Alert_InactContAdd"),"cancel","inviteUsers");
 		}else {
 		inviteUsers();
 		}
   }
 	
	function saveInvitedUserDetails(){
 	var userId = "";
	var inviteUserXmlData = "";
	var proj_user_id = "";
	var userList = "";
	var userFname = "";
	var userLName = "";
	var userPassword = "";
	var userLoginName = "";
	var userEmailId = "";
	$('#adminResultContainer .poNewUser')
			.each(function() {
						userId = $(this).find('input.po_userId').val();
						actionType = $(this).find('input.po_prevStatus').val();
						proj_user_id = $(this).find('input.po_proj_user_id').val();
						userList = $(this).find('input.po_user_list').val();
						userFname = $(this).find('input.po_user_Fname').val();
						userLName = $(this).find('input.po_user_Lname').val();
						userPassword = $(this).find('input.po_user_Password')
								.val();
						userLoginName = $(this).find('input.po_user_LoginName')
								.val();
						userEmailId = $(this).find('input.po_user_Email').val();

						inviteUserXmlData += "<user userid=\"" + userId + "\">";
						inviteUserXmlData += "<action>PO</action>";
						inviteUserXmlData += "<projUserId>" + proj_user_id
								+ "</projUserId>";
						inviteUserXmlData += "<userList>" + userList
								+ "</userList>";
						inviteUserXmlData += "<userFname>" + userFname
								+ "</userFname>";
						inviteUserXmlData += "<userLName>" + userLName
								+ "</userLName>";
						inviteUserXmlData += "<userPassword>" + userPassword
								+ "</userPassword>";
						inviteUserXmlData += "<userLoginName>" + userLoginName
								+ "</userLoginName>";
						inviteUserXmlData += "<userEmailId>" + userEmailId
								+ "</userEmailId>";
						inviteUserXmlData += "</user>";
					});
 
	$('#teamMemberResultContainer .tmNewUser')
			.each(function() {
						userId = $(this).find('input.tm_userId').val();
						actionType = $(this).find('input.tm_prevStatus').val();
						proj_user_id = $(this).find('input.tm_proj_user_id')
								.val();
						user_list = $(this).find('input.tm_user_list').val();

						userFname = $(this).find('input.tm_user_Fname').val();
						userLName = $(this).find('input.tm_user_Lname').val();
						userPassword = $(this).find('input.tm_user_Password')
								.val();
						userLoginName = $(this).find('input.tm_user_LoginName')
								.val();
						userEmailId = $(this).find('input.tm_user_Email').val();

						inviteUserXmlData += "<user userid=\"" + userId + "\">";
						inviteUserXmlData += "<action>TM</action>";
						inviteUserXmlData += "<projUserId>" + proj_user_id
								+ "</projUserId>";
						inviteUserXmlData += "<userList>" + userList
								+ "</userList>";
						inviteUserXmlData += "<userFname>" + userFname
								+ "</userFname>";
						inviteUserXmlData += "<userLName>" + userLName
								+ "</userLName>";
						inviteUserXmlData += "<userPassword>" + userPassword
								+ "</userPassword>";
						inviteUserXmlData += "<userLoginName>" + userLoginName
								+ "</userLoginName>";
						inviteUserXmlData += "<userEmailId>" + userEmailId
								+ "</userEmailId>";
						inviteUserXmlData += "</user>";
					});

	$('#observseResultContainer .suNewUser')
			.each(
					function() {
						userId = $(this).find('input.su_userId').val();
						actionType = $(this).find('input.su_prevStatus').val();
						proj_user_id = $(this).find('input.su_proj_user_id')
								.val();
						userList = $(this).find('input.su_user_list').val();

						userFname = $(this).find('input.su_user_Fname').val();
						userLName = $(this).find('input.su_user_Lname').val();
						userPassword = $(this).find('input.su_user_Password')
								.val();
						userLoginName = $(this).find('input.su_user_LoginName')
								.val();
						userEmailId = $(this).find('input.su_user_Email').val();

						inviteUserXmlData += "<user userid=\"" + userId + "\">"
						inviteUserXmlData += "<action>SU</action>";
						inviteUserXmlData += "<projUserId>" + proj_user_id
								+ "</projUserId>";
						inviteUserXmlData += "<userList>" + userList
								+ "</userList>";
						inviteUserXmlData += "<userFname>" + userFname
								+ "</userFname>";
						inviteUserXmlData += "<userLName>" + userLName
								+ "</userLName>";
						inviteUserXmlData += "<userPassword>" + userPassword
								+ "</userPassword>";
						inviteUserXmlData += "<userLoginName>" + userLoginName
								+ "</userLoginName>";
						inviteUserXmlData += "<userEmailId>" + userEmailId
								+ "</userEmailId>";
						inviteUserXmlData += "</user>";

					});
	
	$('#sysAdminUserContainer').children('[id^=AdminUsersContainer_]').removeClass("poNewUser");
	$('#sysAdminTeamMainDiv').children('[id^=TeamMembersContainer_]').removeClass("tmNewUser");
	$('#sysAdminSubscribeMainDiv').children('[id^=SubscribeUsersContainer_]').removeClass("suNewUser");
	return inviteUserXmlData;
 	}
 	
 	function inviteUsers(){
 	saveInvitedData = saveInvitedUserDetails();
    if (saveInvitedData == "" && userDeleted == false) {
		parent.alertFun(getValues(companyAlerts,"Alert_AddUsr"), "warning");
	} 
	
	else if (saveInvitedData != "") {
	var r='';
	var projectId = $("input#globalProjectId").val()
		$('#loadingBar').show();
		timerControl("start");
		saveInvitedData = "<userData>" + saveInvitedData + "</userData>";
		$.ajax({
			url : path+"/paLoad.do",
			type : "POST",
			data : {
						act : "saveInivtedEntUsersToProj",
						saveInviteData : saveInvitedData,
						projId : projectId
					},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success : function(data) {
				parent.checkSessionTimeOut(data);
				saveInvitedData = "";
				inviteUserXmlData = "";
				parent.$('#loadingBar').hide();
				parent.timerControl("");
				$(".findUsersList").css("background","");
				addedUserIds = "";
			}
		});
	}
	userDeleted = new Boolean(false);
 	}
 	
 	
 	function loadMyProjectsScrollBarJson() {
		$("div#inviteUserContainer").mCustomScrollbar("destroy");
		$("div#inviteUserContainer").mCustomScrollbar({
			scrollButtons : {
				enable : true
			},
			callbacks : {
				onTotalScroll : function() {
				},
				onTotalScrollOffset : 100
			}
		});
	}
	
	function getHeightDynamically(type){
	 	    var winHeight = $(window).height();
		 	var ckeHeight = "";
		 	if(type == 'projectDesc'){
		 	ckeHeight = eval(winHeight) - ( $("div#myNavbar").height() +  $("div#tabMainDiv").height() + $("div#subContainer").height() + 145 );
		 	}
		 	else {
		 	ckeHeight = eval(winHeight) - ( $("div#myNavbar").height() +  $("div#tabMainDiv").height() + $("div#subContainer").height() + 80 );
		 	}
			return ckeHeight;
	    }
	    
	function prepareCommonUsersUI(jsonResult){
	    $("div#adminResultContainer").html("");
	    $("div#teamMemberResultContainer").html("");
	    $("div#observseResultContainer").html("");
	    $("div#displayUsersContainer").mCustomScrollbar("destroy");
	    	var UI="";
	    	var jsonData = jQuery.parseJSON(jsonResult);
	    	 if(jsonData){
	    	   var json = eval(jsonData);
    	         if(jsonData.length>0){
    	 	        for(var i=0;i<json.length;i++){
    	 	        UI="";
    	 	 		UI += "<div id=\"AdminUsersContainer_"+json[i].userId+"\" class=\"userAvailable_"+json[i].userId+"\" style=\"float:left;width:99%;border-bottom: 1px solid rgb(193, 197, 200);margin-left:8px;\">"
							+ "<div id=\"\" style=\"float:left;width:5%;\"><img onerror=\"imageOnErrorReplaceUser(this);\" title=\""+json[i].userName+"\" style=\"height:30px;width:30px;margin:10px;border-radius: 50%;\" src=\" "+json[i].userImage+"\"></div>"
							+ "<div class=\"defaultExceedCls\" style=\"float:left;width:40%;margin-top:15px;\">"+json[i].userName+"</div>"
							+ "<div id=\"\" style=\"float:left;width:40%;margin-top:15px;\">"+json[i].email+"</div>";
							if( companyType == 'Social'){
								if(json[i].projectOwner == userId && json[i].userId == userId){
								 UI = UI + "<div style=\"float:right;width:5%;margin-top:15px;margin-right:-3px;\"><img class=\"userRemoveClass\" style=\"height:15px;width:15px;float: left;\" /></div>";
								}
								else {
								 UI = UI + "<div onclick=\"removeUserFromProject("+json[i].userId+",'"+json[i].userType+"')\" style=\"float:right;width:5%;margin-top:15px;margin-right:-3px;\"><img class=\"userRemoveClass\" style=\"height:15px;width:15px;float: left;\" src=\""+path+"/images/minus.png\" /></div>";
								}
							}
							else {
							UI = UI + "<div onclick=\"removeUserFromProject("+json[i].userId+",'"+json[i].userType+"')\" style=\"float:right;width:5%;margin-top:15px;margin-right:-3px;\"><img class=\"userRemoveClass\" style=\"height:15px;width:15px;float: left;\" src=\""+path+"/images/minus.png\" /></div>";
							}
						+"</div>";
						if(json[i].userType == 'SU'){
						$("div#observseResultContainer").append(UI);
						}
						else if(json[i].userType == 'TM'){
							$("div#teamMemberResultContainer").append(UI);
						}
						else {
						$("div#adminResultContainer").append(UI);
						}
						
						if(i== (json.length-1)){
						   setTimeout(function(){
						      popUpScrollBar('displayUsersContainer');
						   },100);
						}
						
    	 	        }
    	 	  }
	       }
	       //$("input#hiddenCountInviteUsers").val($("div#displayUsersContainer").find("div[id^=AdminUsersContainer_]").length)
	    }
	    
	    var delUserId="";
	    var delUserType="";
	    function removeUserFromProject(userId,type){
	    	 delUserId="";
	    	 delUserType="";
	    	 delUserId=userId;
	    	 delUserType=type;
	    	 confirmFun(getValues(companyAlerts,"Alert_remove"),"delete","removeUsersFromProject");
	    }
	    
	function removeUsersFromProject(){
	    if(delUserType == 'PO'){
	    	if($("div#adminResultContainer").find("div[id^=AdminUsersContainer_]").length == 1){
	    		alertFun(getValues(companyAlerts,"Alert_OnePrjAdmin"), "warning");
	    		return false;
	    	}
	    }
	    $('#loadingBar').show();
		timerControl("start");
	    
	    var projectId = $("input#globalProjectId").val();
		    $.ajax({
		    	url : path + "/workspaceAction.do",
		    	type : "POST",
			    data : {
						act : "deleteUsersFromProject",
						projectId : projectId,
						delUserId : delUserId,
						delUserType:delUserType
					  },
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
			  success : function(data) {
					checkSessionTimeOut(data);
					if(data == "success"){
						  inviteUserflagChanges = true;
						  if(delUserType == 'PO'){
						     $("div#adminResultContainer").find("div.userAvailable_"+delUserId+"").remove();
						  }else if(delUserType == 'TM'){
						     $("div#teamMemberResultContainer").find("div.userAvailable_"+delUserId+"").remove();
						  }else{
						     $("div#observseResultContainer").find("div.userAvailable_"+delUserId+"").remove();
						  }  
						    alertFun(getValues(companyAlerts,"Alert_UsrRemove"),'warning');
					}else {
						 alertFun(getValues(companyAlerts,"Alert_DelFail"),'warning');
					}
					
					timerControl("");	
					$('#loadingBar').hide();
					popUpScrollBar('displayUsersContainer');
					
			  }
		    });
		    
	    }
	    
	    function checkForNoUsers(){
	            if($('div#adminResultContainer').children().length == '0'){
	           // setTimeout({
	              $("div#adminResultContainer").html("No users to display").css({"height":"100px","margin":"10px"});
	           /// },2000)
				}
				
				if($('div#teamMemberResultContainer').children().length== '0'){
				    $("div#teamMemberResultContainer").html("No users to display").css({"height":"100px","margin":"10px"});
				}
				
				if($('div#observseResultContainer').children().length == '0'){
					$("div#observseResultContainer").html("No users to display").css({"height":"100px","margin":"10px"});
				}
				
				 if($('div#adminResultContainer').children().length != '0'){
				    $("div#adminResultContainer").css({"height":"","margin":""});
				}
				
				if($('div#teamMemberResultContainer').children().length != '0'){
				    $("div#teamMemberResultContainer").css({"height":"","margin":""});
				}
				
				if($('div#observseResultContainer').children().length != '0'){
				    $("div#observseResultContainer").css({"height":"","margin":""});
				}
	    }
	    
	    function clearSubs(){
	    $("input#searchAdminSubscribeUsersList").val("");
	    }
	    
	    function clearTeamInput(){
	    $("input#searchAdminTeamUsersList").val("");
	    }
	
	
 	