
var localOffsetTime=getTimeOffset(new Date());
$("body").mouseover(function(event){
    var $target = $(event.target);
    if(!$target.is(".folderFloatIcons") && !$target.parents().is(".folderFloatIcons")){
        $('.uploadFilesinFolders').removeClass('d-block').addClass('d-none');
    }
}); 
$("body").click(function(event){
    var $target = $(event.target);
    if(!$target.is(".folderFloatIcons") && !$target.parents().is(".folderFloatIcons")){
        $('.uploadFilesinFolders').removeClass('d-block').addClass('d-none');
    }
});   
var menuutype="wsDocument";
listDocument();

async function listDocument(){

    let jsonbody={
        "clientRepo" : "N",
        "driveId" : "0",
        "filtervalue" : "",
        "txt" : "",
        "status" : "",
        "ipadVal" : "NO",
        "projec_id" : prjid,
        "user_id" : userIdglb,
        "limitIndex" : 0,
        "limitLength" : 100,
        "localOffsetTime" : localOffsetTime
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
    await $.ajax({
        url: apiPath + "/" + myk + "/v1/getFolders",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');  
        },
        success: function (result) {
            
            if(result!=""){
                $("#documentListUL_0").append(documentListingUI(result,"","")).show();
            }else{
                $("#documentListUL_0").html("<div class='' style='text-align:center;font-size:12px;margin-top: 25px;'>No Document Found</div>");
            }
            $('.documentTree').simpleTreeMenu();
            listDocDrive();
            $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            nodeDragnDropDoc();
        }
    });        

}

function nodeDragnDropDoc(){
    var nestedSortables = [].slice.call(document.querySelectorAll('.mytreeDoc'));
    var sortables = [];
    for(var i=0; i< nestedSortables.length; i++){
      sortables[i] = new Sortable(nestedSortables[i], {
        group: 'nested',
        put: false,
        animation: 150,
        fallbackOnBody: true,
        swapThreshold: 0.65,
        onStart: function (/**Event*/evt) {
            evt.oldIndex;  // element index within parent
            
            var fromFolderType = $(evt.from).attr('foldertype');
            var folderAccessType= $(evt.from).attr('accessType');
            
            if(fromFolderType =="Attachments" || folderAccessType =="R"){
                alertFunNew(getValues(companyAlerts,"Alert_movePermission"),'error');
                return;
            }

        },
        onEnd: function (evt) {
            var itemEl = $(evt.item).find('li').attr('id').split("_")[1];  // dragged HTMLElement
            var indexTo = $(evt.to).attr('id').split("_")[1];    // target list
            var indexFrom =  $(evt.from).attr('id').split("_")[1];  // previous list
            var fromfoldertype = $(evt.from).attr('foldertype');
            var foldertype = $(evt.to).attr('foldertype');
            var level = $(evt.to).attr('level');
            evt.oldIndex;  // element's old index within old parent
            evt.newIndex;  // element's new index within new parent
            //alert("docid="+itemEl+"-indexTo="+indexTo+"-indexFrom="+indexFrom+"-foldertype="+foldertype+"-level="+level);
            evt.oldDraggableIndex; // element's old index within old parent, only counting draggable elements
            evt.newDraggableIndex; // element's new index within new parent, only counting draggable elements
           
            
            if(fromfoldertype !="Attachments"){
                //listSubFolder(indexTo,foldertype,level);
                dragAndDropDoc(indexTo,itemEl);
                evt.clone // the clone element
                evt.pullMode;  // when item is in another sortable: `"clone"` if cloning, `true` if moving
            }else{
                //alert("inside");
               //alertFunNew(getValues(companyAlerts,"Alert_movePermission"),'error');
                //listDocument();
            }
            
        }
      });
    }
}

function documentListingUI(result,parentid,place){
    var ui="";
    var folderid = "";var folderimage = "";var foldername = "";var sysfolder = "";var has_sub_docs = "";
    var user_share_type = "";var has_sub_folders = "";var foldertype = "";var owner="";var ownerimg="";
    var lastupdatedby="";var lastupdatedbyimg="";var level=1;var shareUserAccessType="";
    var idForSublist = "";
    var size="";var createDate = "";var createTime = "";
    var lastUpdateDate = "";var lastUpdateTime = "";
    for(var i=0;i<result.length;i++){

        folderid=result[i].folder_id;
        foldername=result[i].folder_name;
        sysfolder=result[i].sys_folder;
        has_sub_docs=result[i].has_sub_docs;
        user_share_type=result[i].user_share_type;
        has_sub_folders=result[i].has_sub_folders;
        bbottom="";//result[i].parent_folder_id==0||result[i].parent_folder_id=="0"?"border-bottom: 1px solid #AAAAAA;":"";
        lipadding=result[i].parent_folder_id==0||result[i].parent_folder_id=="0"?"px-2 py-2":"p-2";
        if(place==""){
            idForSublist = "documentListUL_"+folderid;
            size="-";
            createDate = result[i].created_date;
            createTime = result[i].created_time;
            lastUpdateDate = result[i].last_updated_date;
            lastUpdateTime = result[i].last_updated_time;
        }else{
            idForSublist = "documentListUL1_"+folderid;
            size="";
            createDate = "";
            createTime = "";
            lastUpdateDate = "";
            lastUpdateTime = "";
        }
        if(sysfolder=="Y"){
            if(foldername=="Attachments"){
                folderimage = "folder_email_closed.svg";
                if(has_sub_docs!="N"){
                    folderimage = "folder_email_open.svg";
                }
            }else if(foldername=="Idea"){
                folderimage = "folder_idea_closed.svg";
                if(has_sub_docs!="N"){
                    folderimage = "folder_idea_open.svg";
                }
            }else if(foldername=="Agile"){
                folderimage = "folder_agile_closed.svg";
                if(has_sub_docs!="N"){
                    folderimage = "folder_agile_open.svg";
                }
            }else if(foldername=="Task" || foldername=="Tasks"){
                folderimage = "folder_task_closed.svg";
                if(has_sub_docs!="N"){
                    folderimage = "folder_task_open.svg";
                }
            }else if(foldername=="Chat Archive"){
                folderimage = "EntireTeam_Y.svg";
            }
            else if(foldername=="Conversations"){
                folderimage = "folder_conversation_closed.svg";
                if(has_sub_docs!="N"){
                    folderimage = "folder_conversation_open.svg";
                }
            }
        }else{
            folderimage = "EntireTeam_N.svg";
            if(has_sub_docs!="N" || has_sub_folders!="N"){
                folderimage = "EntireTeam_Y.svg";
            }
            if(user_share_type=="user_share_type" ){
                folderimage = "user_share_type_closed_Y.svg";
                if(has_sub_docs!="N"){
                    folderimage = "user_share_type_open_Y.svg";
                }
            }
        }

        if(foldername=="Attachments" || foldername=="Idea" || foldername=="Agile" || foldername=="Task" || foldername=="Chat Archive" || foldername=="Conversations"){
            foldertype="Attachments";
        }else if(result[i].fftype_of_file=="default"){
            foldertype="default";
        }else{
            foldertype="parshared_to";
        }
        owner = result[i].owner;
        shareUserAccessType=result[i].user_share_access;
        ownerimg = lighttpdpath+"/userimages/"+result[i].created_by+"."+result[i].user_image_type;
        lastupdatedby = result[i].last_updated_by;
        if(lastupdatedby!=""){
            lastupdatedbyimg = lighttpdpath+"/userimages/"+result[i].last_updated_by+"."+result[i].lastUpdateByuser_image_type;
        }
       
        
        ui+="<li ondragleave='leaveDrag(event,"+folderid+")' ondragover='allowDrag(event,"+folderid+")' id='document_"+folderid+"' accessType= "+shareUserAccessType+" class='mytreeDoc' level="+level+" parentid=0 foldertype='"+foldertype+"' style='cursor:pointer;font-size:12px;'>"
            +"<div class='d-flex align-items-center hov position-relative' onclick='event.stopPropagation();' onmouseover='showFoldermoreoptions(this);event.stopPropagation();' onmouseout='hideFoldermoreoptions(this);event.stopPropagation();'>"
                +"<div id='documentList1_"+folderid+"' class='d-flex align-items-center w-100' onclick=\"listSubFolder("+folderid+",'"+foldertype+"',"+level+",'"+place+"');slectFolderForCopyMove(this,'"+place+"');event.stopPropagation();\">"
                    +"<div class='d-flex align-items-center "+lipadding+" documentListHeaders1' style=''>"
                        +"<span class=''><img id='documentToggleIcon_"+folderid+"' class='docList' src='css/plugins/simpleTreeMenu/menu-arrow-right.png' style=''></span>"//
                        +"<img id='docFolderImg_"+folderid+"' class='ml-2' src='images/document/"+folderimage+"' style='width: 45px;height: 45px;' >" 
                        +"<span id='docFolderSpan_"+folderid+"' style='' class='pl-2 defaultExceedCls docFolderSpanCls'>"+foldername+"</span>"    // stm-content
                        //+"<input id='docFolderInput_"+folderid+"' type='text' value='"+foldername+"' style='display:none;'>"
                        +'<div id="docFolderInputDiv_'+folderid+'" class="p-2 material-textfield w-50 docFolderInputDivCls" style="display:none;">'
                            +'<input id="docFolderInput_'+folderid+'" value="'+foldername+'" name="'+foldername+'" nameext="'+foldername+'"  type="text" onclick="event.stopPropagation();"  onkeydown="renameFolderOnclick(event,'+folderid+');event.stopPropagation();" onblur="renameFolder('+folderid+');event.stopPropagation();" class="px-2 py-0 material-textfield-input docFolderInputCls" placeholder=" "  style="font-size: 12px;padding: 2px 5px !important;outline:none"/>'
                            +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">Folder Name</label>'
                        +'</div>'
                    +"</div>"  
                    +"<div class='documentListHeaders2 defaultExceedCls '>"+size+"</div>" 
                    +"<div class='documentListHeaders2 defaultExceedCls '>" 
                    if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations" && place==""){
                        ui+="<img src='"+ownerimg+"' class='rounded-circle' title='"+owner+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'>"
                    }
                    ui+="</div>"
                    +"<div class='documentListHeaders2 defaultExceedCls '>"
                        +"<span>"+createDate+"</span><br>"
                        +"<span>"+createTime+"</span>"
                    +"</div>" 
                    +"<div class='documentListHeaders2 defaultExceedCls '>"
                    if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations" && lastupdatedby!="" && place==""){
                        ui+="<img src='"+lastupdatedbyimg+"' class='rounded-circle' title='"+lastupdatedby+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'>"
                    }    
                    ui+="</div>"   
                    +"<div class='documentListHeaders2 defaultExceedCls '>"
                        +"<span>"+lastUpdateDate+"</span><br>"
                        +"<span>"+lastUpdateTime+"</span>"
                    +"</div>"
                    
                +"</div>"   
                if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations" && place==""){ 
                    ui+=floatOption(folderid)
                }
            ui+="</div>"
            
            +"<ul   ondrop='drop(event,"+folderid+")' ondragover='allowDrop(event)' id='"+idForSublist+"' class=' documentTree p-0' level="+level+">"
                
        
            +"</ul>"
        +"</li>"

    }    

    return ui;

}

function listSubFolder(parentFolderId,foldertype,level,place){
    if(place==""){
        if($("#documentListUL_"+parentFolderId).children().length==0){
            let jsonbody={
                "fId" : parentFolderId,
                "foldertype" : foldertype,
                "projec_id" : prjid,
                "user_id" : userIdglb,
                "company_id" : companyIdglb,
                "sharetype" : "",
                "userShareType" : "",
                "filtervalue" : "",
                "limitIndex" : "",
                "limitLength" :"",
                "txt" : "",
                "status" : "",
                "localOffsetTime" : localOffsetTime,
                "sortvalue" : ""
            }
            $('#loadingBar').addClass('d-flex').removeClass('d-none');  
            $.ajax({
                url: apiPath + "/" + myk + "/v1/fetchFolderDocs",
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(jsonbody),
                error: function (jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR, textStatus, errorThrown);
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');  
                },
                success: function (result) {
                    level=level+1;
                    if(result!=""){
                        $("#documentListUL_"+parentFolderId).append(subDocumentListingUI(result,level,place)).show();
                    }else{
                        $("#documentListUL_"+parentFolderId).html("<div class='' style='font-size:12px;padding-left: 50px;margin: 10px 0;'>No Document Found</div>");
                    }
                    /* if(level > 1){
                        console.log("level->"+level);
                        var wid = $(".documentListHeaders1").innerWidth();
                        $(".documentListHeaders1").css('width',wid+30);
                        console.log("wid->"+wid);
                        
                    } */
                    $("#documentToggleIcon_"+parentFolderId).attr("src","css/plugins/simpleTreeMenu/menu-arrow-down.png");
                    $('.documentTree').simpleTreeMenu();
    
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');  
                }
            }); 
        }else{
            /* console.log("level->"+level);
            if(level > 1){
               
                var wid = $(".documentListHeaders1").width();
                $(".documentListHeaders1").css('width',wid+10);
                console.log("wid->"+wid);
                
            } */
            $("#documentListUL_"+parentFolderId).html("");
            $("#documentToggleIcon_"+parentFolderId).attr("src","css/plugins/simpleTreeMenu/menu-arrow-right.png");
        }   
        nodeDragnDropDoc();
    }else{
        if($("#documentListUL1_"+parentFolderId).children().length==0){
            let jsonbody={
                "fId" : parentFolderId,
                "foldertype" : foldertype,
                "projec_id" : prjid,
                "user_id" : userIdglb,
                "company_id" : companyIdglb,
                "sharetype" : "",
                "userShareType" : "",
                "filtervalue" : "",
                "limitIndex" : "",
                "limitLength" :"",
                "txt" : "",
                "status" : "",
                "localOffsetTime" : localOffsetTime,
                "sortvalue" : ""
            }
            $('#loadingBar').addClass('d-flex').removeClass('d-none');  
            $.ajax({
                url: apiPath + "/" + myk + "/v1/fetchFolderDocs",
                type: "POST",
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(jsonbody),
                error: function (jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR, textStatus, errorThrown);
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');  
                },
                success: function (result) {
                    level=level+1;
                    if(result!=""){
                        $("#documentListUL1_"+parentFolderId).append(subDocumentListingUI(result,level,place)).show(); 
                    }else{
                        $("#documentListUL1_"+parentFolderId).html("<div class='' style='font-size:12px;padding-left: 50px;margin: 10px 0;'>No Document Found</div>");  
                    }
                    /* if(level > 1){
                        console.log("level->"+level);
                        var wid = $(".documentListHeaders1").innerWidth();
                        $(".documentListHeaders1").css('width',wid+30);
                        console.log("wid->"+wid);
                        
                    } */
                    $("#documentToggleIcon_"+parentFolderId).attr("src","css/plugins/simpleTreeMenu/menu-arrow-down.png");
                    $('.documentTree').simpleTreeMenu();
    
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');  
                }
            }); 
        }else{
            /* console.log("level->"+level);
            if(level > 1){
               
                var wid = $(".documentListHeaders1").width();
                $(".documentListHeaders1").css('width',wid+10);
                console.log("wid->"+wid);
                
            } */
            $("#documentListUL1_"+parentFolderId).html("");
            $("#documentToggleIcon_"+parentFolderId).attr("src","css/plugins/simpleTreeMenu/menu-arrow-right.png");
        }   
    }
     
}

function subDocumentListingUI(result,level,place){
    var ui="";
    var folderid = "";var folderimage = "";var foldername = "";var sysfolder = "";var has_sub_docs = "";
    var user_share_type = "";var has_sub_folders = "";var foldertype = "";var owner="";var ownerimg="";
    var lastupdatedby="";var lastupdatedbyimg="";
    var documenttype = "";var docid="";var mar1="";var fType ="";var val1="";
    var val2="";var val3="";
    var doccmt="";var doctask="";var mll1="";
    var idForCopyMoveSubList = "";
    var hideAndShowDoc = "";
    var size1="";var createDate1 = "";var createTime1 = "";
    var lastUpdateDate1 = "";var lastUpdateTime1 = "";
    for(var i=0;i<result.length;i++){
        lipadding=result[i].parent_folder_id==0||result[i].parent_folder_id=="0"?"p-4":"p-2";
        folderid=result[i].folder_id;
        foldername=result[i].name;
        sysfolder=result[i].sys_folder;
        has_sub_docs=result[i].has_sub_docs;
        user_share_type=result[i].userShareType;
        has_sub_folders=result[i].has_sub_folders;
        
        if(result[i].dataType=="Folder"){
            if(place==""){
                idForCopyMoveSubList = "documentListUL_"+folderid;
                size1="-";
                createDate1 = result[i].created_date;
                createTime1 = result[i].created_time;
                lastUpdateDate1 = result[i].last_updated_date;
                lastUpdateTime1 = result[i].last_updated_time;
            }else{
                idForCopyMoveSubList = "documentListUL1_"+folderid;
                size1="-";
                createDate1 = "";
                createTime1 = "";
                lastUpdateDate1 = "";
                lastUpdateTime1 = "";
            }
            

            if(sysfolder=="Y"){
                if(foldername=="Attachments"){
                    folderimage = "folder_email_closed.svg";
                    if(has_sub_docs!="N"){
                        folderimage = "folder_email_open.svg";
                    }
                }else if(foldername=="Idea"){
                    folderimage = "folder_idea_open.svg";
                }else if(foldername=="Agile"){
                    folderimage = "folder_agile_open.svg";
                }else if(foldername=="Task"){
                    folderimage = "folder_task_open.svg";
                }else if(foldername=="Chat Archive"){
                    folderimage = "EntireTeam_Y.svg";
                }
                else if(foldername=="Conversations"){
                    folderimage = "folder_conversation_open.svg";
                }
            }else{
                
                if(has_sub_folders!="N" || has_sub_docs!="N"){
                    folderimage = "EntireTeam_Y.svg";
                }else{
                    folderimage = "EntireTeam_N.svg";
                }
            }

            if(foldername=="Attachments" || foldername=="Idea" || foldername=="Agile" || foldername=="Task" || foldername=="Chat Archive" || foldername=="Conversations"){
                foldertype="Attachments";
            }else if(result[i].fftype_of_file=="default"){
                foldertype="default";
            }else{
                foldertype="parshared_to";
            }

            owner = result[i].owner;
            ownerimg = lighttpdpath+"/userimages/"+result[i].created_by+"."+result[i].user_image_type;
            lastupdatedby = result[i].last_updated_by;
            lastupdatedbyimg = lighttpdpath+"/userimages/"+result[i].last_updated_by+"."+result[i].lastUpdateByuser_image_type;

            mar1 = has_sub_folders!="N" || has_sub_docs!="N"?"ml-2":"";
            mp1 = has_sub_folders!="N" || has_sub_docs!="N"?" pl-4 ml-2":"pl-5";

            if((has_sub_folders!="N" || has_sub_docs!="N") && level==2){
                mll1="margin-left:-8px;"
            }else if(level>2){
                mll1="margin-left:-8px;"
            }else{
                mll1="";
            }
            
            ui+="<li draggable='true' id='document_"+folderid+"' class='mytreeDoc' level="+level+" parentid="+result[i].parent_folder_id+" foldertype='"+foldertype+"'  onclick='event.stopPropagation();' style='cursor:pointer;list-style: none;'>"
                +"<div class='d-flex align-items-center hov position-relative'  onclick='event.stopPropagation();' onmouseover='showFoldermoreoptions(this);event.stopPropagation();' onmouseout='hideFoldermoreoptions(this);event.stopPropagation();'>"
                    +"<div id='documentList1_"+folderid+"' class='d-flex align-items-center w-100' onclick=\"listSubFolder("+folderid+",'"+foldertype+"',"+level+",'"+place+"');slectFolderForCopyMove(this,'"+place+"');event.stopPropagation();\">"    
                        +"<div class='d-flex align-items-center "+lipadding+" documentListHeaders1 "+mp1+"'>"
                            if(has_sub_folders!="N" || has_sub_docs!="N"){
                                ui+="<span class=''><img id='documentToggleIcon_"+folderid+"' class='docList' src='css/plugins/simpleTreeMenu/menu-arrow-right.png' style=''></span>"//+"<span class='stm-icon'></span>"
                            }
                            ui+="<img id='docFolderImg_"+folderid+"' class='"+mar1+"' src='images/document/"+folderimage+"' style='width: 45px;height: 45px;' >"
                            +"<span id='docFolderSpan_"+folderid+"' style='' class='pl-2 defaultExceedCls docFolderSpanCls'>"+foldername+"</span>"    // stm-content
                            +'<div id="docFolderInputDiv_'+folderid+'" class="p-2 material-textfield w-50 docFolderInputDivCls" style="display:none;">'
                                +'<input id="docFolderInput_'+folderid+'" value="'+foldername+'" name="'+foldername+'" nameext="'+foldername+'"  type="text" onclick="event.stopPropagation();"  onkeydown="renameFolderOnclick(event,'+folderid+');event.stopPropagation();" onblur="renameFolder('+folderid+');event.stopPropagation();" class="px-2 py-0 material-textfield-input docFolderInputCls" placeholder=" "  style="font-size: 12px;padding: 2px 5px !important;outline:none"/>'
                                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">Folder Name</label>'
                            +'</div>'
                        +"</div>"  
                        +"<div class='documentListHeaders2 defaultExceedCls ' style='"+mll1+"'>"+size1+"</div>" 
                        +"<div class='documentListHeaders2 defaultExceedCls '>" 
                        if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations" && place==""){
                            ui+="<img src='"+ownerimg+"'  class='rounded-circle' title='"+owner+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'>"
                        }
                        ui+="</div>"
                        +"<div class='documentListHeaders2 defaultExceedCls '>"
                            +"<span>"+createDate1+"</span><br>"
                            +"<span>"+createTime1+"</span>"
                        +"</div>" 
                        +"<div class='documentListHeaders2 defaultExceedCls '>"
                        if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations"  && lastupdatedby!="" && place==""){
                            ui+="<img src='"+lastupdatedbyimg+"' class='rounded-circle' title='"+lastupdatedby+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'>"
                        }    
                        ui+="</div>"  
                        +"<div class='documentListHeaders2 defaultExceedCls '>"
                            +"<span>"+lastUpdateDate1+"</span><br>"
                            +"<span>"+lastUpdateTime1+"</span>"
                        +"</div>"
                    +"</div>"  
                    if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations" && place==""){  
                        ui+=floatOption(folderid)
                    }
                ui+="</div>"
                +"<ul id='"+idForCopyMoveSubList+"' class='documentTree' level="+level+"></ul>"
            +"</li>"

        }else if(result[i].dataType=="Document" && result[i].document_category=="link"){

            documenttype = result[i].document_type;
            docid = result[i].document_id;
            owner = result[i].owner;
            ownerimg = lighttpdpath+"/userimages/"+result[i].created_by+"."+result[i].user_image_type;
            lastupdatedby = result[i].last_updated_by;
            lastupdatedbyimg = lighttpdpath+"/userimages/"+result[i].last_updated_by+"."+result[i].lastUpdateByuser_image_type;

            doccmt = result[i].doc_comments;
            doctask = result[i].doc_tasks;

            if(doccmt==""){
                val2="d-none";
            }else{
                val2="d-block";
            }

            if(place==""){
                console.log("hello");
                hideAndShowDoc = "d-block";
            }else{
                hideAndShowDoc = "d-none";
            }
            
            if(doctask==""){
                val3="d-none";
            }else{
                val3="d-block";
            }

            if(level>2){
                mll1="margin-left:-6px;"
            }else{
                mll1="";
            }

            ui+="<li draggable='true' id='document_"+docid+"' class='mytreeDoc py-2 hov "+hideAndShowDoc+"' level="+level+" parentid="+result[i].parent_folder_id+" foldertype='"+foldertype+"'  onclick='event.stopPropagation();' style='cursor:pointer;list-style: none;'>"
                +"<div class='d-flex align-items-center position-relative'  onclick='event.stopPropagation();' onmouseover='showFoldermoreoptions(this);event.stopPropagation();' onmouseout='hideFoldermoreoptions(this);event.stopPropagation();'>"
                    +"<div id='documentList1_"+docid+"' class='d-flex align-items-center w-100' onclick=\"event.stopPropagation();\">"    
                        +"<div class='d-flex align-items-center documentListHeaders1 pl-5'>"
                            +"<img id='docFolderImg_"+docid+"' class='"+mar1+"' src='images/temp/link.png' style='width: 30px;height: 25px;' >"
                            +"<span id='docFolderSpan_"+docid+"' class='pl-2 py-1 defaultExceedCls docFolderSpanCls' linkname='"+result[i].document_physical_name+"' link='"+foldername+"' title='"+result[i].document_physical_name+"' onclick='renameLink();event.stopPropagation();'><a href='"+foldername+"' target='_blank' style='cursor:pointer;color:black;' >"+foldername+"</a></span>"    // stm-content
                            /* +'<div id="docFolderInputDiv_'+docid+'" class="p-2 material-textfield w-50 docFolderInputDivCls" style="display:none;">'
                                +'<input id="docFolderInput_'+docid+'" value="'+result[i].document_physical_name+'" linkname="'+result[i].document_physical_name+'"  type="text" onclick="event.stopPropagation();"  onkeydown="renameFolderOnclick(event,'+folderid+');event.stopPropagation();" onblur="renameFolder('+folderid+');event.stopPropagation();" class="px-2 py-0 material-textfield-input docFolderInputCls" placeholder=" "  style="font-size: 12px;padding: 2px 5px !important;outline:none"/>'
                                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">Title</label>'
                            +'</div>'
                            +'<div id="docFolderInputLinkDiv_'+docid+'" class="p-2 material-textfield w-50 docFolderInputDivCls" style="display:none;">'
                                +'<input id="docFolderLinkInput_'+docid+'" value="'+foldername+'" link="'+foldername+'"  type="text" onclick="event.stopPropagation();"  onkeydown="renameFolderOnclick(event,'+folderid+');event.stopPropagation();" onblur="renameFolder('+folderid+');event.stopPropagation();" class="px-2 py-0 material-textfield-input docFolderInputCls" placeholder=" "  style="font-size: 12px;padding: 2px 5px !important;outline:none"/>'
                                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">Link</label>'
                            +'</div>' */
                        +"</div>"  
                        +"<div class='documentListHeaders2 defaultExceedCls ' style='"+mll1+"'>-</div>" 
                        +"<div class='documentListHeaders2 defaultExceedCls '>" 
                        if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations" && place==""){
                            ui+="<img src='"+ownerimg+"'  class='rounded-circle' title='"+owner+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'>"
                        }
                        ui+="</div>"
                        +"<div class='documentListHeaders2 defaultExceedCls '>"
                            +"<span>"+result[i].created_date+"</span><br>"
                            +"<span>"+result[i].created_time+"</span>"
                        +"</div>" 
                        +"<div class='documentListHeaders2 defaultExceedCls '>"
                        if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations"  && lastupdatedby!="" && place==""){
                            ui+="<img src='"+lastupdatedbyimg+"' class='rounded-circle' title='"+lastupdatedby+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'>"
                        }    
                        ui+="</div>"  
                        +"<div class='documentListHeaders2 defaultExceedCls '>"
                            +"<span>"+result[i].last_updated_date+"</span><br>"
                            +"<span>"+result[i].last_updated_time+"</span>"
                        +"</div>"
                        
                        +"<div class='documentListHeaders3 "+val2+"' onclick='wsDocComments("+docid+",\""+result[i].name+"\",\"\",\"\",\"\");event.stopPropagation();'>"
                            +"<img src='/images/task/task_comments.svg'  class='img-responsive  task_icon'>"
                        +"</div>"
                        +"<div class='documentListHeaders3 "+val3+"' onclick='wsdocumentTask("+docid+",\""+result[i].name+"\","+val1+");event.stopPropagation();'>"
                            +"<img src='images/idea/task_nofill.svg' class='img-responsive  task_icon'>"
                        +"</div>"

                    +"</div>"  
                    if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations" && place==""){  
                        ui+=floatOptionForFiles(folderid,result[i].document_type,docid,result[i].name,owner,"")
                    }
                ui+="</div>"
            +"</li>"


        }else{

            if(place==""){
                // console.log("hello");
                hideAndShowDoc = "d-block";
            }else{
                hideAndShowDoc = "d-none";
            }


            documenttype = result[i].document_type;
            docid = result[i].document_id;
            owner = result[i].owner;
            ownerimg = lighttpdpath+"/userimages/"+result[i].created_by+"."+result[i].user_image_type;
            lastupdatedby = result[i].last_updated_by;
            lastupdatedbyimg = lighttpdpath+"/userimages/"+result[i].last_updated_by+"."+result[i].lastUpdateByuser_image_type;
            fType = $("#document_"+folderid).attr("foldertype");
            if(documenttype=="png" || documenttype=="jpg" || documenttype=="jpeg" || documenttype=="gif" || documenttype=="svg"){
                val1 = "['"+docid+"','"+documenttype+"','"+feedId+"']";//"thumbnailOpenUi(this,"+docid+",'"+documenttype+"',"+feedId+")";
            }else if(documenttype=="mp3" || documenttype=="wav" || documenttype=="wma" || documenttype=="mp4" || documenttype=="m4v" || documenttype=="mov" || documenttype=="flv" || documenttype=="f4v" || documenttype=="ogg" || documenttype=="ogv" || documenttype=="wmv" || documenttype=="vp6" || documenttype=="vp5" || documenttype=="mpg" || documenttype=="avi" || documenttype=="mpeg" || documenttype=="webm"){
                val1 = "['"+docid+"','"+documenttype+"','"+prjid+"']";//"openVideo("+docid+",'"+documenttype+"',"+prjid+")";
            }else{
                val1 = "['"+docid+"','"+documenttype+"','"+docid+"']";//"viewordownloadUi("+docid+",this,'"+documenttype+"',"+docid+")";
            }
            
            doccmt = result[i].doc_comments;
            doctask = result[i].doc_tasks;

            if(doccmt==""){
                val2="d-none";
            }else{
                val2="d-block";
            }
            
            if(doctask==""){
                val3="d-none";
            }else{
                val3="d-block";
            }

            if(level>2){
                mll1="margin-left:-6px;"
            }else{
                mll1="";
            }

            ui+="<li draggable='true' id='document_"+docid+"' class='mytreeDoc py-3 hov "+hideAndShowDoc+"' level="+level+" parentid="+result[i].parent_folder_id+"  onclick='event.stopPropagation();' style='font-size:12px;cursor:pointer;list-style: none;'>"
                +"<div class='d-flex align-items-center' style='position:relative;'  onclick='event.stopPropagation();' onmouseover='showFoldermoreoptions(this);event.stopPropagation();' onmouseout='hideFoldermoreoptions(this);event.stopPropagation();'>"
                    
                    if(documenttype=="png" || documenttype=="jpg" || documenttype=="jpeg" || documenttype=="gif" || documenttype=="svg"){
                        ui+="<div class='mytreeDoc d-flex align-items-center documentListHeaders1 pl-5'  onclick='event.stopPropagation();'>"
                            +"<img src='"+lighttpdpath+"//projectDocuments//"+docid+"."+documenttype+"' onclick=\"thumbnailOpenUi(this,"+docid+",'"+documenttype+"',"+feedId+");event.stopPropagation();\" ondblclick=\"expandImage("+docid+",'"+documenttype+"');event.stopPropagation();\" class='rounded' style='width:60px;height:40px;border: 1px solid #d9d9d9;'>"
                            +"<span id='docFolderSpan_"+docid+"' class='pl-2 docFolderSpanCls'>"+result[i].name+"</span>"
                            +'<div id="docFolderInputDiv_'+docid+'" class="p-2 material-textfield w-50 docFolderInputDivCls" style="display:none;">'
                                +'<input id="docFolderInput_'+docid+'" value="'+result[i].name.split(".")[0]+'" name="'+result[i].name.split(".")[0]+'" nameext="'+result[i].name+'"  type="text" onclick="event.stopPropagation();" onkeydown="updateFilenameOnenter(event,'+docid+',\''+documenttype+'\');event.stopPropagation();" onblur="updateFilename('+docid+',\''+documenttype+'\');event.stopPropagation();" class="px-2 py-0 material-textfield-input docFolderInputCls" placeholder=" "  style="font-size: 12px;padding: 2px 5px !important;outline:none"/>'
                                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">Folder Name</label>'
                            +'</div>'
                        +"</div>"  
                    }else if(documenttype=="mp3" || documenttype=="wav" || documenttype=="wma" || documenttype=="mp4" || documenttype=="m4v" || documenttype=="mov" || documenttype=="flv" || documenttype=="f4v" || documenttype=="ogg" || documenttype=="ogv" || documenttype=="wmv" || documenttype=="vp6" || documenttype=="vp5" || documenttype=="mpg" || documenttype=="avi" || documenttype=="mpeg" || documenttype=="webm"){
                        ui+="<div class='d-flex align-items-center documentListHeaders1 pl-5'  onclick='event.stopPropagation();' >"
                            +"<video class='float-left rounded' style='cursor: pointer; width:60px;height:40px;background-color: #000;'><source src='"+lighttpdpath+"//projectDocuments//"+docid+"."+documenttype+"'></video>"
                            +"<img src='images/conversation/videoplay.svg' class='rounded' onclick=\"openVideo("+docid+",'"+documenttype+"',"+prjid+");event.stopPropagation();\" style='width:20px;height:20px;position:absolute;left: 67px;'>"
                            +"<span id='docFolderSpan_"+docid+"' class='pl-2 docFolderSpanCls'>"+result[i].name+"</span>"
                            +'<div id="docFolderInputDiv_'+docid+'" class="p-2 material-textfield w-50 docFolderInputDivCls" style="display:none;">'
                                +'<input id="docFolderInput_'+docid+'" value="'+result[i].name.split(".")[0]+'" name="'+result[i].name.split(".")[0]+'" nameext="'+result[i].name+'" type="text" onclick="event.stopPropagation();" onkeydown="updateFilenameOnenter(event,'+docid+',\''+documenttype+'\');event.stopPropagation();" onblur="updateFilename('+docid+',\''+documenttype+'\');event.stopPropagation();" class="px-2 py-0 material-textfield-input docFolderInputCls" placeholder=" "  style="font-size: 12px;padding: 2px 5px !important;outline:none"/>'
                                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">Folder Name</label>'
                            +'</div>'
                        +"</div>"
                    }else{
                        ui+="<div class='d-flex align-items-center documentListHeaders1 pl-5'  onclick='event.stopPropagation();'>"
                            +"<img src='images/document/"+documenttype+".svg' onerror='imageOnFileNotErrorReplace(this)' onclick=\"viewordownloadUi("+docid+",this,'"+documenttype+"',"+docid+");event.stopPropagation();\" style='width:30px;height:30px;'>"
                            +"<span id='docFolderSpan_"+docid+"' class='pl-2 docFolderSpanCls'>"+result[i].name+"</span>"
                            +'<div id="docFolderInputDiv_'+docid+'" class="p-2 material-textfield w-50 docFolderInputDivCls" style="display:none;">'
                                +'<input id="docFolderInput_'+docid+'" value="'+result[i].name.split(".")[0]+'" name="'+result[i].name.split(".")[0]+'" nameext="'+result[i].name+'" type="text" onclick="event.stopPropagation();" onkeydown="updateFilenameOnenter(event,'+docid+',\''+documenttype+'\');event.stopPropagation();" onblur="updateFilename('+docid+',\''+documenttype+'\');event.stopPropagation();" class="px-2 py-0 material-textfield-input docFolderInputCls" placeholder=" "  style="font-size: 12px;padding: 2px 5px !important;outline:none"/>'
                                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">Folder Name</label>'
                            +'</div>'
                        +"</div>"
                    }
                    
                    ui+="<div class='documentListHeaders2 defaultExceedCls ' style='"+mll1+"'>"+result[i].document_size+"</div>" 
                    +"<div class='documentListHeaders2 defaultExceedCls '>" 
                    if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations"  && place==""){
                        ui+="<img src='"+ownerimg+"'  class='rounded-circle' title='"+owner+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'>"
                    }
                    ui+="</div>"
                    +"<div class='documentListHeaders2 defaultExceedCls '>"
                        +"<span>"+result[i].created_date+"</span><br>"
                        +"<span>"+result[i].created_time+"</span>"
                    +"</div>" 
                    +"<div class='documentListHeaders2 defaultExceedCls '>"
                    if(foldername!="Attachments" && foldername!="Idea" && foldername!="Agile" && foldername!="Task" && foldername!="Chat Archive" && foldername!="Conversations"  && lastupdatedby!=""  && place==""){
                        ui+="<img src='"+lastupdatedbyimg+"' class='rounded-circle' title='"+lastupdatedby+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'>"
                    }    
                    ui+="</div>"   
                    +"<div class='documentListHeaders2 defaultExceedCls '>"
                        +"<span>"+result[i].last_updated_date+"</span><br>"
                        +"<span>"+result[i].last_updated_time+"</span>"
                    +"</div>"  

                    +"<div class='documentListHeaders3 "+val2+"' onclick='wsDocComments("+docid+",\""+result[i].name+"\",\"\",\"\",\"\");event.stopPropagation();' >"
                        +"<img src='/images/task/task_comments.svg' class='img-responsive  task_icon'>"
                    +"</div>"
                    +"<div class='documentListHeaders3 "+val3+"' onclick=\"wsdocumentTask("+docid+",'"+result[i].name+"',"+val1+");event.stopPropagation();\" >"
                        +"<img src='images/idea/task_nofill.svg' class='img-responsive  task_icon'>"
                    +"</div>"

                    if(fType!="Attachments" && fType!="Idea" && fType!="Agile" && fType!="Task" && fType!="Chat Archive" && fType!="Conversations"  && place==""){
                        ui+=floatOptionForFiles(folderid,result[i].document_type,docid,result[i].name,owner,val1)
                    }
                ui+="</div>"
            +"</li>"

        }

    }    

    return ui;

}

function slectFolderForCopyMove(obj,place){
    // "[id^='userPopup_']"
    $('[id^="documentList1_"]').css('background', 'none');
    if(place==""){

    }else{
        
        $(obj).css('background', '#eef5f7');
        folderIdddsss = $(obj).attr('id');
        console.log(folderIdddsss);
    }
    
}

function renameFolderOnclick(event,folderid){
    var code = null;
	code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if(code == 13){
        $("#docFolderInput_"+folderid).blur();
        //renameFolder(folderid);
    }
}

function renameFolder(folderid){

    var filename = $("#docFolderInput_"+folderid).val();
    var parentid = $("#document_"+folderid).attr('parentid');
    var oldname = $("#docFolderInput_"+folderid).attr('name');
    var oldnameext = $("#docFolderInput_"+folderid).attr('nameext');
    var fType = parentid==0?"":"default";
    
    if(filename.trim()==""){
        conFunNew(getValues(companyAlerts,"Alert_Folder"),"warning","continueRename","cancelRename",folderid);
    }else if(filename.trim()=="Attachments"){
        alertFunNew(getValues(companyAlerts,"Alert_folderAttachName"),'error');
    }else if(filename.trim()=="Agile"){
        alertFunNew(getValues(companyAlerts,"Alert_NoFolAgile"),'error');
        $("#docFolderInput_"+docid).focus();
    }else if(filename.trim()=="Idea"){
        alertFunNew(getValues(companyAlerts,"Alert_NoIdeaTopicFolName"),'error');
        $("#docFolderInput_"+docid).focus();
    }else if(filename.trim()=="Task"){
        alertFunNew(getValues(companyAlerts,"Alert_NoTaskFolName"),'error');
        $("#docFolderInput_"+docid).focus();
    }else if(!checkSplChar(filename)){
        alertFunNew(getValues(companyAlerts,"Alert_specialCharacter"),'error');	   	        
    }else if(filename==oldname){
        $("#docFolderInput_"+folderid).val(filename);
        $("#docFolderInput_"+folderid).attr('name',filename);
        $("#docFolderSpan_"+folderid).text(filename).show();
        $("#docFolderInputDiv_"+folderid).hide();
    }else{
        let jsonbody = { 
            "clientRepo": "N",
            "driveId": "0",
            "newFolderName": filename,
            "actionType": "update",
            "projec_id": prjid,
            "user_id": userIdglb,
            "localOffsetTime": localOffsetTime,
            "fId": parentid,
            "company_id": companyIdglb,
            "foldertype": fType,
            "rfId" : folderid
        }
        $.ajax({
        url: apiPath+"/"+myk+"/v1/validateFolder",
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
            checkError(jqXHR,textStatus,errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            
            },
        success : function(result) {
            if(result[0].result=="Same folder name is exist"){
                alertFunNew(getValues(companyAlerts,"Alert_folderNameExist"),'error');
                $("#docFolderInput_"+folderid).val(oldname);
                $("#docFolderInput_"+folderid).attr('name',oldname);
                $("#docFolderSpan_"+folderid).text(oldname).show();
                $("#docFolderInputDiv_"+folderid).hide();
            }else if(result!=""){
                $("#docFolderInput_"+folderid).val(filename);
                $("#docFolderInput_"+folderid).attr('name',filename);
                $("#docFolderSpan_"+folderid).text(filename).show();
                $("#docFolderInputDiv_"+folderid).hide();
            }    
            
        }
        });
    }    



}

function updateFilenameOnenter(event,docid,doctype){
    var code = null;
	code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if(code == 13){
        $("#docFolderInput_"+docid).blur();
        //updateFilename(docid,doctype);
    }
}

function updateFilename(docid,doctype){
    var filename = $("#docFolderInput_"+docid).val();
    var filePhyname = filename+"."+doctype;
    var parentid = $("#document_"+docid).attr('parentid');
    var oldname = $("#docFolderInput_"+docid).attr('name');
    var oldnameext = $("#docFolderInput_"+docid).attr('nameext');
    
    if(filename.trim()==""){
        conFunNew(getValues(companyAlerts,"Alert_Folder"),"warning","continueRename","cancelRename",docid);
        //confirmFunNew(getValues(companyAlerts,"Alert_Folder"),"clear","continueRename",docid,oldnameext);
        //$("#docFolderInput_"+docid).focus();
    }else if(filename.trim()=="Attachments"){
        alertFunNew(getValues(companyAlerts,"Alert_folderAttachName"),'error');
    }else if(filename.trim()=="Agile"){
        alertFunNew(getValues(companyAlerts,"Alert_NoFolAgile"),'error');
        $("#docFolderInput_"+docid).focus();
    }else if(filename.trim()=="Idea"){
        alertFunNew(getValues(companyAlerts,"Alert_NoIdeaTopicFolName"),'error');
        $("#docFolderInput_"+docid).focus();
    }else if(filename.trim()=="Task"){
        alertFunNew(getValues(companyAlerts,"Alert_NoTaskFolName"),'error');
        $("#docFolderInput_"+docid).focus();
    }else if(!checkSplChar(filename)){
        alertFunNew(getValues(companyAlerts,"Alert_specialCharacter"),'error');	   	        
    }else if(filename==oldname){
        $("#docFolderInput_"+docid).val(filename);
        $("#docFolderInput_"+docid).attr('name',filename).attr('nameext',filePhyname);
        $("#docFolderSpan_"+docid).text(filePhyname).show();
        $("#docFolderInputDiv_"+docid).hide();
    }else{
        let jsonbody = { 
            "document_physical_name" : filePhyname,
            "document_id" : docid,
            "newname" : filename,
            "folder_id" : parentid,
            "projec_id" : prjid,
            "user_id" : userIdglb,
            "company_id" : companyIdglb
        }
        $.ajax({
        url: apiPath+"/"+myk+"/v1/renameFile",
        type:"PUT",
        //dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
            checkError(jqXHR,textStatus,errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            
            },
        success : function(result) {
            
            $("#docFolderInput_"+docid).val(filename);
            $("#docFolderInput_"+docid).attr('name',filename).attr('nameext',filePhyname);
            $("#docFolderSpan_"+docid).text(filePhyname).show();
            $("#docFolderInputDiv_"+docid).hide();
            
        }
        });
    }    


}

function continueRename(docid){
    
    var oldname = $("#docFolderInput_"+docid).attr('name');
    var oldnameext = $("#docFolderInput_"+docid).attr('nameext');
    
    $("#docFolderInput_"+docid).val(oldname).focus();
}

function cancelRename(){
    $(".docFolderSpanCls").show();
    $(".docFolderInputDivCls").hide();
}

function addNewFolder(parentid){
    let ui = addNewFolderUI(parentid);
    if($("#folderCreateDivId_"+parentid).length==0){
        $(".folderCreateDivCls").remove();
        if(parentid==0){
            $("#documentListUL_0").prepend(ui);
        }else{
            $("#document_"+parentid).append(ui);
        }
        $("#folderName_"+parentid).focus();
    }else{
        $("#folderCreateDivId_"+parentid).remove();
    }
    


}

function addNewFolderUI(parentid){
    var ui="";
    var mp1 = parentid==0?"p-2":"p-2 pl-4 ml-2";

    ui="<div id='folderCreateDivId_"+parentid+"' class='folderCreateDivCls'>"
        +"<div class='d-flex align-items-center "+mp1+"'>"
            +"<img src='images/document/EntireTeam_N.svg' class='mx-3' style='width: 45px;height: 45px;'>"
            +'<div class="py-2 material-textfield w-25">'
                +'<input id="folderName_'+parentid+'" type="text" onkeydown="insertOnEnter(event,'+parentid+',\'insert\');event.stopPropagation();" onblur="insertNewFolder('+parentid+',\'insert\');event.stopPropagation();" class="px-2 py-0 material-textfield-input" placeholder=" "  style="font-size: 12px;padding: 2px 5px !important;outline:none"/>'
                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;">Folder Name</label>'
            +'</div>'
        +"</div>"
        
    +"</div>"

    return ui;
}

function insertOnEnter(event,parentid,type1){

    var code = null;
	code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if(code == 13){
        //insertNewFolder(parentid,type1);
        $("#folderName_"+parentid).blur();
    }

}

function insertNewFolder(parentid,type1){

    var foldername = $("#folderName_"+parentid).val();
    var foldertype = parentid!=0?"default":"";
    
    if(foldername.trim()==""){
        conFunNew(getValues(companyAlerts,"Alert_Folder"),"warning","continueCreate","cancelCreate");
        $('#newFileName').focus();
    }else if(foldername.trim()=="Attachments"){
        alertFunNew(getValues(companyAlerts,"Alert_folderAttachName"),'error');
        $('#newFileName').focus();
    }else if(foldername.trim()=="Agile"){
        alertFunNew(getValues(companyAlerts,"Alert_NoFolAgile"),'error');
        $('#newFileName').focus();
    }else if(foldername.trim()=="Idea"){
        alertFunNew(getValues(companyAlerts,"Alert_NoIdeaTopicFolName"),'error');
        $('#newFileName').focus();
    }else if(foldername.trim()=="Task"){
        alertFunNew(getValues(companyAlerts,"Alert_NoTaskFolName"),'error');
        $('#newFileName').focus();
    }else if(!checkSplChar(foldername)){
        alertFunNew(getValues(companyAlerts,"Alert_specialCharacter"),'error');	   	        
    }else{
        let jsonbody={
            "clientRepo": "N",
            "driveId": "0",
            "newFolderName": foldername,
            "actionType": type1,
            "projec_id": prjid,
            "user_id": userIdglb,
            "localOffsetTime": localOffsetTime,
            "fId" : parentid,
            "company_id" : companyIdglb,
            "foldertype" : foldertype
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/validateFolder",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                if(result[0].result=="Same folder name is exist"){
                    alertFunNew(getValues(companyAlerts,"Alert_folderNameExist"),'error');
                }else if(result!=""){
                    $(".folderCreateDivCls").remove();
                    if(parentid==0){
                        $("#documentListUL_"+parentid).prepend(documentListingUI(result,parentid,"")).show();
                    }else{
                        if($("#documentListUL_"+parentid).children().length==0){
                            $("#documentList1_"+parentid).trigger('click');
                        }else{
                            var lev = $("#documentListUL_"+parentid+":first-child").attr('level');
                            $("#document_"+parentid).find("#documentListUL_"+parentid).prepend(subDocumentListingUI(result,lev)).show();
                        }
                        if($("#docFolderImg_"+parentid).attr('src').indexOf('EntireTeam_N') >= 0){
                            $("#docFolderImg_"+parentid).attr('src','images/document/EntireTeam_Y.svg').addClass('ml-2');
                            $("#docFolderImg_"+parentid).parent().removeClass("pl-5").addClass("pl-4 ml-2");
                            if($("#document_"+parentid).attr('level')!=1){
                                $('<span class=""><img id="documentToggleIcon_'+parentid+'" class="docList" src="css/plugins/simpleTreeMenu/menu-arrow-right.png" style=""></span>').insertBefore("#docFolderImg_"+parentid);
                            }
                        }
                    }
                    
                        
                }
                $('.documentTree').simpleTreeMenu();
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  

            }
        }); 
    }
    
}

function continueCreate(){
    $(".folderCreateDivCls").find('input').focus();
}
function cancelCreate(){
    $(".folderCreateDivCls").remove();
}

function floatOption(folderid){
    var ui="";

    ui="<div id='folderOption_"+folderid+"' class='d-none align-items-center position-absolute folderFloatIcons' style=''>"
        +"<div><img src='/images/conversation/edit.svg' onclick='renameDocFolderUI("+folderid+");event.stopPropagation();' title='Rename' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div class='position-relative' >"
            +"<img src='/images/conversation/attach.svg' title='Upload' onclick='showFolderUpload(this);event.stopPropagation();' style='height: 18px; width:18px;margin: 0px 6px;'>"
            +"<div class='position-absolute uploadFilesinFolders' style='display:none;'>"
                
                +'<div class="d-flex p-1 justify-content-start" style="height: 24px; cursor:pointer;">'
                    +'  <form enctype="multipart/form-data" style="position:absolute;top:-100px;" method="post" name="folderFileUpload" id="folderFileUpload">'
                    +'  <input type="file" class="Upload_cLabelTitle" title="Upload" value="" onchange="readFileUrlinfolder(this,'+folderid+',\'\',\'uploadFromSystem\');event.stopPropagation();" name="FileUpload" id="folderFileUpload_'+folderid+'" hidden></form>'
                    +' 	<img class="img-fluid" src="images/conversation/computer.svg" style="width:20px;" onclick="openFolderDoc('+folderid+');event.stopPropagation();" id="mainUpload">'
                    +'	<span class="pl-2" style="white-space: nowrap;" onclick="openFolderDoc('+folderid+');event.stopPropagation();">From Computer</span>'
                +'</div>'
                +'<div class="d-flex p-1 justify-content-start" onclick="openFolderDoc('+folderid+');event.stopPropagation();" style="height: 24px;">'
                    +' 	<img class="img-fluid" style="width:20px;" src="images/conversation/folder.svg">'
                    +'	<span class="pl-2" style="white-space: nowrap;" >From Repository</span>'
                +'</div>'
                +'<div class="d-flex p-1 justify-content-start" onclick="attDoclinkPopup('+folderid+',\'new\',\'\');event.stopPropagation();" style="height: 24px;cursor:pointer;">'
                    +' 	<img class="img-fluid" style="width:20px;" src="images/task/attach.svg">'
                    +'	<span class="pl-2" style="white-space: nowrap;" >Attach Link</span>'
                +'</div>'
            
            +"</div>"
        +"</div>"
        +"<div><img src='images/task/plus.svg' title='Add Folder' onclick='addNewFolder("+folderid+");event.stopPropagation();' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='images/idea/share_nofill.svg' onclick='listCollaborators("+folderid+");event.stopPropagation();' title='Share' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/conversation/delete.svg' onclick='deleteDocFolder("+folderid+");event.stopPropagation();' title='Delete' style='height: 15px; width:15px;margin: 0px 6px;'></div>"
    +"</div>"

    return ui;
}


function floatOptionLink(docid,folderid){
    var ui="";

    ui="<div id='folderOption_"+docid+"' class='d-none align-items-center position-absolute folderFloatIcons' style=''>"
        +"<div><img src='/images/conversation/edit.svg' onclick='attDoclinkPopup("+folderid+",\"rename\","+docid+");event.stopPropagation();' title='Rename' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/task/task_comments.svg' title='Comments' onclick='wsDocComments("+folderid+")' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='images/idea/task_nofill.svg' title='Task' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/cme/copy.svg' title='Copy' onclick='copyMoveDiv()' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/conversation/delete.svg' onclick='deleteDocFile("+docid+");event.stopPropagation();' title='Delete' style='height: 15px; width:15px;margin: 0px 6px;'></div>"
    +"</div>"

    return ui;
}

function listCollaborators(folderid){

    let jsonbody={
        "fId" : folderid,
        "projec_id" : prjid
    }
    $.ajax({
        url: apiPath + "/" + myk + "/v1/loadFolderCollabrators",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
            
        },
        success: function (result) { 
    
            $('#ShareListDiv').html('');
            $("#transparentDiv").show();
            $("#ShareListDiv").append(prepareListCollaboratorsUI(folderid,result[0].user_share_type,result[0].entireteam_share_access)).show();
            for(var i=0;i<result.length;i++){
                if(result[i].userRole=="PO"){
                    $("#docShareOwnerList").append(shareUserUI(result[i]));
                }else if(result[i].userRole=="TM"){
                    $("#docShareMemberList").append(shareUserUI(result[i]));
                }else if(result[i].userRole=="SU"){
                    $("#docShareMemberList").append(shareUserUI(result[i]));
                }
            }
            if(result[0].user_share_type=="user_share_type"){
                $("#addMemberIcon").show();
                $(".removeMemberDocShareCls").show();
            }else{
                $("#addMemberIcon").hide();
            }
            listDocMembers();
            
        }
    });

}

function prepareListCollaboratorsUI(folderid,usersharetype,entireTeamAccess){
    var ui="";
    var folderimg = $("#docFolderImg_"+folderid).attr("src");
    var foldername = $("#docFolderSpan_"+folderid).text();
    var usersharetypeimg="";var usersharetypeimg2="";var entireTeamAccessimg="";var shareDocType=usersharetype;
    if(usersharetype=="EntireTeam"){
        usersharetypeimg1="images/task/check.svg";
        usersharetypeimg2="images/task/uncheck.svg";
    }else{
        usersharetypeimg1="images/task/uncheck.svg";
        usersharetypeimg2="images/task/check.svg";
    }
    if(entireTeamAccess=="R"){
        entireTeamAccessimg="images/idea/R.svg";
    }else{
        entireTeamAccessimg="images/idea/W.svg";
    }

    ui='<div class="modal d-flex justify-content-center popupDocTrail" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' 
      +'<div class="modal-dialog" style="max-width: 80% !important;min-width: 50%;top: 2%;" >'
          +'<div class="modal-content container px-2 py-1" style="max-height: 95%;min-height:95%;font-size: 12px;">' 
              +'<div class="d-flex align-items-center justify-content-start modal-header p-2" style="border-bottom: 1px solid #5e5b5b !important;">' 
                +"<img id='docShareHeaderFolderImg' src='"+folderimg+"' style='width: 30px;height: 25px;'>"
                +"<span class='pl-2 defaultExceedCls' style='font-size:14px;'>"+foldername+"</span>"
                +'<button type="button" onclick="closeDocShare();event.stopPropagation();" class="close p-0" data-dismiss="docAttachlink" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
              +'</div>' 
              +'<div class="modal-body wsScrollBar p-2" style="max-height: 85%;overflow: auto;">'
                +'<div id="docShareOwner" class="d-block py-1" style="">'
                    +"<div ><span class='px-1 py-0' style='border-bottom: 2px solid #3fa1ee;'>"+getValues(companyLabels,"owner")+"</span></div>"
                    +"<div id='docShareOwnerList' class='d-block py-2'></div>"
                +'</div>'
                +'<div id="docShareMember" class="d-block py-1" style="">'
                    +"<div class='position-relative'><span class='px-1 py-0' style='border-bottom: 2px solid #3fa1ee;'>"+getValues(companyLabels,"Team_Members")+"</span>"
                        +"<img id='addMemberIcon' onclick='listDocMembers();event.stopPropagation();' src='images/task/plus.svg' class='ml-2' style='width:20px;height: 20px;cursor:pointer;display:none;'>"
                        +"<div id='docAddMemberList' class='position-absolute p-2' style='display:none;'></div>"
                    +"</div>"
                    +"<div id='docShareMemberList' class='d-block py-2'></div>"
                +'</div>'
              +'</div>' //body 
              +'<div id="" class="modal-footer p-1 d-block">'
                +"<div id='shareDocTypeId' class='d-flex align-items-center justify-content-between w-100 p-2' shareDocType='"+shareDocType+"' >"
                    +"<div class='d-flex align-items-center'>"
                        +"<img id='shEntireTeam' onclick='shareDocTypeOption(\"EntireTeam\");event.stopPropagation();' src='"+usersharetypeimg1+"' style='width:18px;height:18px;cursor:pointer;'>"
                        +"<span class='px-2'>Share with Entire Team</span>"
                        +"<img id='shEntireTeamimg' onclick='changeEntireTeamAccess(this);event.stopPropagation();' entireTeamAccess='"+entireTeamAccess+"' src='"+entireTeamAccessimg+"' style='width:25px;cursor:pointer;'>"
                    +"</div>"
                    +"<div class='d-flex align-items-center'>"
                        +"<img id='selUsersShare' onclick='shareDocTypeOption(\"user_share_type\");event.stopPropagation();' src='"+usersharetypeimg2+"' style='width:18px;height:18px;cursor:pointer;'>"
                        +"<span class='pl-2'>Selective Users</span>"
                    +"</div>"
                +"</div>"
                +"<div class='p-2'>"
                    +"<span class='createBtn' onclick='saveDocSharePopup("+folderid+");event.stopPropagation();'>"+getValues(companyLabels,"SAVE")+"</span>"
                +"</div>"
              
              +"</div>" //footer
          +'</div>' //content
      +'</div>' //dialog
    +'</div>' //modal

    return ui;   

}

function saveDocSharePopup(folderid){
    var radioValue=$("#shEntireTeamimg").attr("entireteamaccess");
    var newShareType=$("#shareDocTypeId").attr("sharedoctype");
    var updateAccess="[";
    var uid="";var stype="";
    $('.shareUserCls').each(function (){
        uid=$(this).attr('id').split('_')[1];
        stype=$(this).find('#shareUserAccessType_'+uid).attr('sharetype');
        if($("#shareUserId_"+uid).attr("add")=="add"){
            updateAccess+="{";
            updateAccess+="\"user_id\":\""+uid+"\",";
            updateAccess+="\"share_type\":\""+stype+"\"";
            updateAccess+="},";
        }
        
    });
    updateAccess=updateAccess.length>1?updateAccess.substring(0,updateAccess.length-1)+"]":"[]";

    let jsonbody = {
		"user_id" : userIdglb,
        "company_id" : companyIdglb,
        "projec_id" : prjid,
        "fId" : folderid,
        "radioValue" : radioValue,
        "newShareType" : newShareType,
        "updateAccess": JSON.parse(updateAccess)
	}
    
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
	$.ajax({
		url: apiPath + "/" + myk + "/v1/shareToAllUsers",
		type: "PUT",
        //dataType: 'json',
        contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');  
		},
		success: function (result) {

            closeDocShare();
            if(newShareType=="user_share_type"){
                $("#docFolderImg_"+folderid).attr("src","images/document/user_share_type_open_Y.svg");
            }else{
                $("#docFolderImg_"+folderid).attr("src","images/document/EntireTeam_Y.svg");
            }
            
            $('#loadingBar').addClass('d-none').removeClass('d-flex');  
        }
	});  
}

function changeEntireTeamAccess(obj){
    if($(obj).attr("entireTeamAccess")=="R"){
        $(obj).attr("src","images/idea/W.svg");
        $(obj).attr("entireTeamAccess","W");
        $(".teamMemberAccessCls").attr("src","images/idea/W.svg").attr("entireteamaccess","W").attr("sharetype","W");
    }else{
        $(obj).attr("src","images/idea/R.svg");
        $(obj).attr("entireTeamAccess","R");
        $(".teamMemberAccessCls").attr("src","images/idea/R.svg").attr("entireteamaccess","R").attr("sharetype","R");
    }
}

function shareDocTypeOption(type){

    if(type=="user_share_type"){
        conFunNew(getValues(companyAlerts,"Alert_ShareTeamOff"),"warning","confirmShareDocTypeOption","");
    }else if(type=="EntireTeam"){
        $("#shEntireTeam").attr("src","images/task/check.svg");
        $("#selUsersShare").attr("src","images/task/uncheck.svg");
        $("#addMemberIcon").hide();
        $("#shareDocTypeId").attr("shareDocType","EntireTeam");
        var uid="";        
        $('.docMemberListCls').each(function (){
            uid=$(this).children().attr("id").split("_")[1];
            
            if($("#shareUserId_"+uid).hasClass("d-flex")==false){
                $(this).children().trigger("click");
            }
        });
        $(".removeMemberDocShareCls").hide();
    }

}

function confirmShareDocTypeOption(){
    $("#shEntireTeam").attr("src","images/task/uncheck.svg");
    $("#selUsersShare").attr("src","images/task/check.svg");
    $("#addMemberIcon").show();
    $("#shareDocTypeId").attr("shareDocType","user_share_type");
    $(".removeMemberDocShareCls").show();
}

function shareUserUI(result){
    var ui="";
    
    var userimg = lighttpdpath+"/userimages/"+result.user_id+"."+result.user_image_type;
    var shareTypeimg="";var onclick="";var tmclass="";
    if(result.sharetype=="R"){
        shareTypeimg="images/idea/R.svg";
    }else{
        shareTypeimg="images/idea/W.svg";
    }
    if(result.userRole!="PO"){
        tmclass="teamMemberAccessCls";
        onclick="docToggleRWaccess(this);";
    }
    

    ui="<div id='shareUserId_"+result.user_id+"' add='add' class='shareUserCls d-flex align-items-center p-1'>"
        +"<div style=''><img src='"+userimg+"' class='rounded-circle' title='"+result.name+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'></div>"
        +"<div style='width:90%;' class='defaultExceedCls'><span class='pl-2'>"+result.name+"</span></div>"
        +"<div style='width:10%;' class='d-flex align-items-center justify-content-end'>"
            +"<img id='shareUserAccessType_"+result.user_id+"' onclick='"+onclick+"event.stopPropagation();' shareType='"+result.sharetype+"' class='"+tmclass+"' src='"+shareTypeimg+"' style='width:25px;cursor:pointer;'>"
            if(result.created_by!=result.user_id && result.userRole!="PO"){
                ui+="<img id='' src='/images/task/minus.svg' onclick='removeMemberDocShare("+result.user_id+");event.stopPropagation();' class='ml-2 removeMemberDocShareCls' style='width:18px;height:18px;cursor:pointer;display:none;'>"
            }
        ui+="</div>"
    +"</div>"

    return ui;
}

function removeMemberDocShare(uid){
    $("#shareUserId_"+uid).addClass("d-none").removeClass("d-flex");
    $("#memberList_"+uid).addClass("d-flex").removeClass("d-none");
    $("#shareUserId_"+uid).attr("add","remove");
    if($("#memberList_"+uid).parent().is(":hidden")){
        $("#memberList_"+uid).parent().show();
    }    
}

function docToggleRWaccess(obj){
    var type = $(obj).attr('shareType');
    if(type=="R"){
        $(obj).attr('src','images/idea/W.svg');
        $(obj).attr('shareType','W');
    }else{
        $(obj).attr('src','images/idea/R.svg');
        $(obj).attr('shareType','R');
    }
}

function closeDocShare(){
    $('#ShareListDiv').html('').hide();
    $("#transparentDiv").hide();
}

function listDocMembers(){
    if($("#docAddMemberList").children().length==0){
        let jsonbody={
            "projec_id" : prjid,
            "user_id" : userIdglb,
            "company_id" : companyIdglb
        }
        $.ajax({
            url: apiPath + "/" + myk + "/v1/getAllUsers",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
                
            },
            success: function (result) { 
                
                $("#docAddMemberList").append(docAddMemberListUI(result));

                $('.shareUserCls').each(function (){
                    uid=$(this).attr('id').split('_')[1];
                    $("#memberList_"+uid).addClass("d-none").removeClass("d-flex");
                });

                
                
                
            }
        });
    }else{
        $("#docAddMemberList").toggle();
    }    

}

function searchDocMemberAdd(){
    if($("#docAddMemberListUIInput").is(":visible")){
        $("#docAddMemberListUIInput").hide();
        $("#docAddMemberListUISpan").show();
    }else{
        $("#docAddMemberListUIInput").show().focus();
        $("#docAddMemberListUISpan").hide();
    }
}

function docAddMemberListUI(result){
    var ui="";

    ui="<div class='d-flex align-items-center justify-content-between py-2'>"
        +"<span id='docAddMemberListUISpan'>Select Members</span>"
        +"<input id='docAddMemberListUIInput' onkeyup='docAddMemberListSearch();' type='text' placeholder='search here' class='border-0 w-100' style='display:none;outline:none;border-bottom:1px solid #6c757d !important;'>"
        +"<img src='/images/menus/search2.svg' onclick='searchDocMemberAdd();event.stopPropagation();' style='height:18px;width:18px; cursor:pointer;'>"
    +"</div>"
    ui+=docAddMemberListUILoop(result)

    return ui;

}

function docAddMemberListUILoop(result){
    var ui="";
    for(var i=0;i<result.length;i++){

        ui+="<div class='docMemberListCls'>"
            +"<div id='memberList_"+result[i].user_id+"' class='memberListCls d-flex align-items-center py-1' onclick='addMembertoDocShare("+result[i].user_id+",\""+result[i].name+"\",\""+result[i].userImage+"\");event.stopPropagation();' style='cursor:pointer;'>"
                +"<img src='"+result[i].userImage+"' class='rounded-circle' title='"+result[i].name+"' onerror='imageOnProjNotErrorReplace(this);' style='width:30px;height:30px;'>"
                +"<div class='pl-2 docMemberListNameDiv'>"+result[i].name+"</div>"
            +"</div>"
        +"</div>"
    }
    return ui;
}

function addMembertoDocShare(uid,uname,uimage){
    if($("#shareUserId_"+uid).length==0){
        var ui="";

        var stype = $("#shEntireTeamimg").attr("entireteamaccess");
        var stypeimg = $("#shEntireTeamimg").attr("src");
        
        ui="<div id='shareUserId_"+uid+"' add='add' class='shareUserCls d-flex align-items-center p-1'>"
            +"<div style=''><img src='"+uimage+"' class='rounded-circle' title='"+uname+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'></div>"
            +"<div style='width:90%;' class='defaultExceedCls'><span class='pl-2'>"+uname+"</span></div>"
            +"<div style='width:10%;' class='d-flex align-items-center justify-content-end'>"
                +"<img id='shareUserAccessType_"+uid+"' onclick='docToggleRWaccess(this);event.stopPropagation();' shareType='"+stype+"' class='teamMemberAccessCls' src='"+stypeimg+"' style='width:25px;cursor:pointer;'>"
                +"<img id='' src='/images/task/minus.svg' onclick='removeMemberDocShare("+uid+");event.stopPropagation();' class='ml-2 removeMemberDocShareCls' style='width:18px;height:18px;cursor:pointer;'>"
            +"</div>"
        +"</div>"

        $("#docShareMemberList").append(ui);
        
    }else{
        $("#shareUserId_"+uid).addClass("d-flex").removeClass("d-none");
        $("#shareUserId_"+uid).attr("add","add");
    }
    $("#memberList_"+uid).parent().hide();

}

function docAddMemberListSearch(){

    var docAddMemberVal = $('#docAddMemberListUIInput').val().toLowerCase();
    
    $(".docMemberListCls  .docMemberListNameDiv").filter(function() {
      $(this).parents('.docMemberListCls').toggle($(this).text().toLowerCase().indexOf(docAddMemberVal) > -1);
    });

}

function renameDocLinkUI(docid){
    $("#docFolderSpan_"+docid).hide();
    $("#docFolderInputDiv_"+docid+" , #docFolderInputLinkDiv_"+docid+"").show();
}

function deleteDocFolder(folderid){
    conFunNew(getValues(companyAlerts,"Alert_delete"),"warning","continueDocDelete","",folderid);
}

function continueDocDelete(folderid){
    var pid = $("#document_"+folderid).attr("parentid");
    $.ajax({
        url: apiPath + "/" + myk + "/v1/deleteFolder?folderId="+folderid,
        type: "DELETE",
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            
        },
        success: function (data) {
        
          $("#document_"+folderid).remove();

          if($("#documentListUL_"+pid).children().length==0){
          $("#docFolderImg_"+pid).attr("src","images/document/EntireTeam_N.svg");
          $("#documentToggleIcon_"+pid).css("visibility","hidden");
        }
          
        }
    });
}

function renameDocFolderUI(folderid){
    $("#docFolderSpan_"+folderid).hide();
    $("#docFolderInputDiv_"+folderid).show();
    $("#docFolderInput_"+folderid).focus();
}

function showFolderUpload(obj){
    $(obj).next('.uploadFilesinFolders').addClass('d-block').removeClass('d-none');
}

function showFoldermoreoptions(obj){
    $(obj).find('.folderFloatIcons').removeClass('d-none').addClass('d-flex');
}

function hideFoldermoreoptions(obj){
    $(obj).find('.folderFloatIcons').removeClass('d-flex').addClass('d-none');
}


function floatOptionForFiles(folderid,doctype,docid,docname,docowner,storedvalues){
    
    var ui="";
    
    ui="<div id='folderOption_"+folderid+"' class='d-none align-items-center position-absolute folderFloatIcons' style=''>"
    if(doctype=="link"){
        ui+="<div><img src='/images/conversation/edit.svg' onclick='attDoclinkPopup("+folderid+",\"rename\","+docid+");event.stopPropagation();' title='Rename' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/task/task_comments.svg' title='Comments' onclick='wsDocComments("+docid+",\""+docname+"\",\"\",\"\",\"\");event.stopPropagation();' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='images/idea/task_nofill.svg' title='Task' onclick='wsdocumentTask("+docid+",\""+docname+"\",\""+storedvalues+"\",\"options\",\"link\");event.stopPropagation();' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/cme/copy.svg' title='Copy' onclick='copyMovePopup("+docid+")' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/conversation/delete.svg' onclick='deleteDocFile("+docid+");event.stopPropagation();' title='Delete' style='height: 15px; width:15px;margin: 0px 6px;'></div>"
    }else{
        ui+="<div><img src='/images/conversation/edit.svg'onclick='renameDocFileUI("+docid+");event.stopPropagation();' title='Rename' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='images/idea/share_nofill.svg' title='Share' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div>"
            +'  <form enctype="multipart/form-data" style="position:absolute;top:-100px;" method="post" name="folderFileUpload" id="folderFileUpload">'
            +'  <input type="file" id="folderFileUpload1_'+folderid+'" class="Upload_cLabelTitle" title="Upload" value="" onchange="readFileUrlinfolder(this,'+folderid+','+docid+',\'updateDocUploadFromSystem\');event.stopPropagation();" name="FileUpload" id="folderFileUpload_'+folderid+'" hidden></form>'
            +' 	<img class="img-fluid" src="/images/conversation/reply.svg" title="Update" style="height: 18px; width:18px;margin: 0px 6px;" onclick="openFolderDoc1('+folderid+');event.stopPropagation();" id="mainUpload">'
        +'</div>'
        +"<div><img src='images/idea/task_nofill.svg' title='Task' onclick=\"wsdocumentTask("+docid+",'"+docname+"',"+storedvalues+",'options','docs');event.stopPropagation();\" style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/task/task_comments.svg' onclick='wsDocComments("+docid+",\""+docname+"\",\"\",\"\",\"\");event.stopPropagation();' title='Comments' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/cme/copy.svg' title='Copy' onclick='copyMovePopup("+docid+")' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='images/idea/document.svg' onclick='docTrailUI("+docid+",\""+doctype+"\",\""+docname+"\",\""+docowner+"\");event.stopPropagation();' title='Trail' style='height: 18px; width:18px;margin: 0px 6px;'></div>"
        +"<div><img src='/images/conversation/delete.svg' onclick='deleteDocFile("+docid+");event.stopPropagation();' title='Delete' style='height: 15px; width:15px;margin: 0px 6px;'></div>"
    }    
    ui+="</div>"

    return ui;
}

function deleteDocFile(docid){
    conFunNew(getValues(companyAlerts,"Alert_delete"),"warning","continueDocFileDelete","",docid);
}

async function continueDocFileDelete(docid){
      let jsonbody = {
        "document_id" : docid,
        "projec_id" : prjid
      }
      var pid = $("#document_"+docid).attr("parentid");
      
      //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      await $.ajax({ 
          url: apiPath+"/"+myk+"/v1/deleteFile",
          type:"DELETE",
          contentType:"application/json",
          data: JSON.stringify(jsonbody),
          error: function(jqXHR, textStatus, errorThrown) {
            checkError(jqXHR,textStatus,errorThrown);
            //$('#loadingBar').addClass('d-none').removeClass('d-flex');
          }, 
          success:function(result){
            
            $("#document_"+docid).remove();

            if($("#documentListUL_"+pid).children().length==0){
                $("#docFolderImg_"+pid).attr("src","images/document/EntireTeam_N.svg");
                $("#documentToggleIcon_"+pid).css("visibility","hidden");
            }
            //$('#loadingBar').addClass('d-none').removeClass('d-flex');
          }  
      });  
}

function renameDocFileUI(id){
    $("#docFolderSpan_"+id).hide();
    $("#docFolderInputDiv_"+id).show();
    $("#docFolderInput_"+id).focus();
}

function openFolderDoc(folderId) {
	document.getElementById('folderFileUpload_'+folderId+'').click();
}
function openFolderDoc1(folderId) {
	document.getElementById('folderFileUpload1_'+folderId+'').click();
}

function readFileUrlinfolder(input,folderId,docid,place){
  let inputFiles = input.files[0];
  if (input.files && input.files[0]) {
    if(input.files[0].size >= 25000000){
      //conFunNew(getValues(companyAlerts,"Alert_ResetMoreSize"),"warning","uploadFolderDocument","",inputFiles,folderId);
      confirmFunNew(getValues(companyAlerts,"Alert_ResetMoreSize"),"clear","uploadFolderDocument",inputFiles,folderId,docid,place);
      $('.alertMainCss').css({'width': '28.5%'});
      $('.alertMainCss').css({'left': '45%'});
      $('#BoxConfirmBtnOk').css({'width': '21%'});
      $('#BoxConfirmBtnOk').val('Continue');
    }else{
      uploadFolderDocument(inputFiles,folderId,docid,place);
    }
  }
}

async function uploadFolderDocument(inputFiles,folderId,docid,place){
    //$('#loadingBar').addClass('d-flex').removeClass('d-none');
    docid = typeof(docid)=="undefined"||docid=="undefined"?"":docid;
    
    var formData = new FormData();
    formData.append('file', inputFiles);
    formData.append('user_id', userIdglb);
    formData.append('company_id', companyIdglb);
    formData.append('place', place);
    formData.append('resourceId', folderId);
    formData.append('viewPlace', 'N');
    formData.append('projId', prjid);
    formData.append('menuTypeId', docid);

    
    $.ajax({
        url: apiPath+"/"+myk+"/v1/upload", 
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        dataType:'text',
        data: formData,
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success: function(result) {
          if(place=="uploadFromSystem"){
            if($("#documentListUL_"+folderId).children().length==0){
                $("#documentList1_"+folderId).trigger("click");
            }else{
                addFiletoUI(folderId,$("#document_"+folderId).attr('foldertype'),$("#document_"+folderId).attr('level'),result,place);
            }
    
            if($("#docFolderImg_"+folderId).attr('src').indexOf('EntireTeam_N') >= 0){
                $("#docFolderImg_"+folderId).attr('src','images/document/EntireTeam_Y.svg').addClass('ml-2');
                $("#docFolderImg_"+folderId).parent().removeClass("pl-5").addClass("pl-4 ml-2");
                if($("#document_"+folderId).attr('level')!=1){
                    $('<span class=""><img id="documentToggleIcon_'+folderId+'" class="docList" src="css/plugins/simpleTreeMenu/menu-arrow-right.png" style=""></span>').insertBefore("#docFolderImg_"+folderId);
                }
            }
          }else if(place=="updateDocUploadFromSystem"){
            var lev = $("#document_"+folderId).attr('level');
            addFiletoUI(folderId,$("#document_"+folderId).attr('foldertype'),lev,result,place);
            
          }
          
  
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });
    
  }

  function attDoclinkPopup(folderid,type,docid){
	var ui="";
	$('#attachlinkDivData').html('');
    $("#transparentDiv").show();
    ui+='<div class="docAttachlink" id="">'
        +'<div class="modal-dialog">'
        +'<div class="modal-content container">'
        
            
            +'<div class="modal-header py-2 pl-0" style="border-bottom: none !important;">'
            +'<p class="modal-title" style="font-size:14px;color: black;font-weight: normal;">Attach Link</p>'
            +'<button type="button" onclick="closeDocAttlink();" class="close p-0" data-dismiss="docAttachlink" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
            +'</div>'
            
            
            +'<div class="pt-2 pl-0" style="">'
            +'<div class="p-0 " style="font-size:11px;">'
                +'<div class="py-2 material-textfield">'
                +'<input id="docAttlinkTitle" class="px-2 py-0 material-textfield-input1 newinput w-100" placeholder=" "  style="outline:none;"/>'
                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;top:25px">Title</label>'
                +'</div>'
                +'<div class="py-2 material-textfield">'  
                +'<input id="docAttlinkaddress" class="px-2 py-0 material-textfield-input1 newinput w-100" placeholder=" " style="outline:none;"/>'
                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;top:25px">Link</label>'
                +'</div>'  
                +'</div>'
            +'</div>'
            
            +'<div class="modal-footer py-2 px-0" style="border-top: none !important;">'
                +'<img src="/images/task/remove.svg" title="Cancel" onclick="closeDocAttlink();event.stopPropagation();" style="width:25px;height:25px;cursor:pointer;">'
                +'<img id="docLinkEditSave" src="/images/task/tick.svg" title="Attach" onclick="attachDoclink('+folderid+',\''+type+'\','+docid+');event.stopPropagation();" style="width:25px;height:25px;cursor:pointer;">'
            +'</div>'
            
        +'</div>'
        +'</div>'
    +'</div>'

    $('#attachlinkDivData').append(ui).show();
    
    if(type=="rename"){
        $("#docAttlinkTitle").val($("#docFolderSpan_"+docid).attr("linkname"));
        $("#docAttlinkaddress").val($("#docFolderSpan_"+docid).attr("link"));
        $("#attachlinkDivData").find(".modal-title").text("Update Link");
    }

}

function closeDocAttlink(){
    $('#attachlinkDivData').html('').hide();
    $("#transparentDiv").hide();
}

function attachDoclink(folderid,type,docid){
    
    var name = $("#docAttlinkTitle").val();
    var link = $("#docAttlinkaddress").val();

    var validlink = isValidURL(link);
    if(link == ''){
        alertFunNew(getValues(companyAlerts,"Alert_enterLink"),'error');
        $("#docAttlinkaddress").focus();
        return false;
    }else if(validlink == false){
        alertFunNew(getValues(companyAlerts,"Alert_invalidUrl"),'error');
        $("#docAttlinkaddress").focus();
        return false;
    }else{

        let jsonbody={
            "folder_id" : folderid,
            "linkName" : name,
            "link" : link,
            "projec_id" : prjid,
            "type" : type,
            "document_id" : docid,
            "user_id" : userIdglb,
            "company_id" : companyIdglb,
            "foldertype" : "default",
            "localOffsetTime" : localOffsetTime
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/saveLink",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                
                if(type=="new"){
                    closeDocAttlink();
                    if($("#documentListUL_"+folderid).children().length==0){
                        $("#documentList1_"+folderid).trigger('click');
                    }else{
                        var lev = $("#documentListUL_"+folderid+":first-child").attr('level');
                        $("#document_"+folderid).find("#documentListUL_"+folderid).prepend(subDocumentListingUI(result,lev)).show();
                    }
                    if($("#docFolderImg_"+folderid).attr('src').indexOf('EntireTeam_N') >= 0){
                        $("#docFolderImg_"+folderid).attr('src','images/document/EntireTeam_Y.svg').addClass('ml-2');
                        $("#docFolderImg_"+folderid).parent().removeClass("pl-5").addClass("pl-4 ml-2");
                        if($("#document_"+folderid).attr('level')!=1){
                            $('<span class=""><img id="documentToggleIcon_'+folderid+'" class="docList" src="css/plugins/simpleTreeMenu/menu-arrow-right.png" style=""></span>').insertBefore("#docFolderImg_"+parentid);
                        }
                    }

                }else{
                    closeDocAttlink();
                    $("#docFolderSpan_"+docid).attr('linkname',name).attr('link',link).attr('title',name);
                    $("#docFolderSpan_"+docid).children().text(link).attr('href',link);
                }

                $('.documentTree').simpleTreeMenu();
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            }
        }); 

    }    

}

function addFiletoUI(parentFolderId,foldertype,level,documentid,place){
    let jsonbody={
        "fId" : parentFolderId,
        "foldertype" : foldertype,
        "projec_id" : prjid,
        "user_id" : userIdglb,
        "company_id" : companyIdglb,
        "sharetype" : "",
        "userShareType" : "",
        "filtervalue" : "",
        "limitIndex" : "",
        "limitLength" :"",
        "txt" : "",
        "status" : "",
        "localOffsetTime" : localOffsetTime,
        "sortvalue" : "",
        "document_id" : documentid,
        "folder_id" : "0"
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
    $.ajax({
        url: apiPath + "/" + myk + "/v1/fetchFolderDocs",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');  
        },
        success: function (result) {
            if(result!=""){
                if(place=="uploadFromSystem"){
                    $("#documentListUL_"+parentFolderId).prepend(subDocumentListingUI(result,level)).show();
                }else if(place=="updateDocUploadFromSystem"){
                    $("#document_"+documentid).replaceWith(subDocumentListingUI(result,level));
                }    
            }
            $('.documentTree').simpleTreeMenu();
            $('#loadingBar').addClass('d-none').removeClass('d-flex');  
        }
    }); 
}

function listDocDrive(){
    let result = [
        {
            "id":"1",
            "folder":"googledrive",
            "name":"Google Drive"
        },
        {
            "id":"2",
            "folder":"dropbox",
            "name":"Drop Box"
        },
        {
            "id":"3",
            "folder":"onedrive",
            "name":"One Drive"
        },
        {
            "id":"4",
            "folder":"box",
            "name":"Box"
        }
    ]
    $("#documentDrivesList").append(listDocDriveUI(result));
}

function listDocDriveUI(result){
    var ui="";

    for(var i=0;i<result.length;i++){
        ui+="<li id='document_"+result[i].id+"' class='' level='' parentid=0 foldertype='' style='cursor:pointer;font-size:12px;'>"
            +"<div class='d-flex align-items-center w-100 p-2' onclick='listDriveFolders("+result[i].id+");event.stopPropagation();'>"
                +"<span class=''><img id='documentToggleIcon_"+result[i].id+"' class='docList' src='css/plugins/simpleTreeMenu/menu-arrow-right.png' style=''></span>"//
                +"<img id='docFolderImg_' class='ml-2' src='images/document/"+result[i].folder+".svg' style='width: 45x;height: 45px;' >" 
                +"<span id='docFolderSpan_' style='' class='pl-2 defaultExceedCls docFolderSpanCls'>"+result[i].name+"</span>"
            +"</div>"
            +"<ul id='documentListUL_"+result[i].id+"' class='p-0'></ul>"
        +"</li>"

    }  
    
    return ui;
}

function listDriveFolders(id){
    if($("#documentListUL_"+id).children().length==0){
        $("#documentListUL_"+id).html("<div class='' style='font-size:12px;padding-left: 50px;margin: 10px 0;'>No Document Found</div>");
        $("#documentToggleIcon_"+id).attr("src","css/plugins/simpleTreeMenu/menu-arrow-down.png");
        $('.documentTree').simpleTreeMenu();
    }else{
        $("#documentListUL_"+id).html("");
        $("#documentToggleIcon_"+id).attr("src","css/plugins/simpleTreeMenu/menu-arrow-right.png");
    }
}


function docTrailUI(docid,doctype,docname,docowner){
	var ui="";
	$('#DocsLinkListDiv').html('');
    $("#transparentDiv").show();

    ui='<div class="modal d-flex justify-content-center popupDocTrail" id="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' 
      +'<div class="modal-dialog" style="max-width: 70% !important;min-width: 70%;top: 5%;" >'
          +'<div class="modal-content container px-2 py-1" style="max-height: 95%;min-height:85%;font-size: 12px;">' 
              +'<div class="d-block modal-header p-0" style="border-bottom: 1px solid #5e5b5b !important;">' 
                +"<div class='d-flex align-items-center'>"
                    +"<div class='px-0 py-2'>"
                        +'<img src="images/document/'+doctype+'.svg" onerror="imageOnFileNotErrorReplace(this)" class="" style="width:30px;height:30px;">'
                    +"</div>"
                    +"<div class='px-1 w-50 px-0 py-2' style='display:grid;'>"
                        +"<span class='defaultExceedCls' style='font-weight:bold;'>"+getValues(companyLabels,"Document_Trail")+" : "+docname+"</span>"
                        +"<span class='defaultExceedCls' style='font-style:italic;'>"+getValues(companyLabels,"owner")+" : "+docowner+"</span>"
                    +"</div>"
                    +"<div class='px-1 w-50 px-3 py-2'  style='text-align: end;'>"
                        +"<div class='material-textfield'>"
                            +"<select class='px-2 py-1 rounded docTrailView material-textfield-input w-50' onchange='changeDTrailView();event.stopPropagation();' style='outline:none;border: 1px solid #C1C5C8;'>"
                                +"<option>"+getValues(companyLabels,"Document_Summary_View")+"</option>"
                                +"<option>"+getValues(companyLabels,"Document_Detail_View")+"</option>"
                            +"</select>"
                            +"<label class='sellabel  '>"+getValues(companyLabels,"View")+"</label>"
                        +"</div>"    
                    +"</div>"
                    +"<div class='ml-auto mb-auto'>"
                        +'<img src="images/menus/close3.svg" onclick="closeDocTrail();" class="ml-auto" style="cursor:pointer;width:10px;height:10px;">'
                    +"</div>"
                +"</div>"    

                +'<div class="py-1 px-2 w-100 docsHeader" style="">'
                    +'<div id="docSummaryViewHeader" class="d-flex  align-items-center defaultExceedCls " style="">'
                        
                        +'<span class="defaultExceedCls" style="width:30%;">'+getValues(companyLabels,"Document_User")+'</span>'
                        +'<span class="defaultExceedCls" style="width:20%;text-align: center;">'+getValues(companyLabels,"Document_Last_Download")+'</span>'
                        +'<span class="defaultExceedCls" style="width:15%;text-align: center;">'+getValues(companyLabels,"View")+'/'+getValues(companyLabels,"Document_Downloads")+'</span>'
                        +'<span class="defaultExceedCls" style="width:20%;text-align: center;">'+getValues(companyLabels,"Document_Last_Modified")+'</span>'
                        +'<span class="defaultExceedCls" style="width:15%;text-align: center;">'+getValues(companyLabels,"Document_Modifications")+'</span>'
                        
                    +'</div>'
                    +'<div id="docDetailViewHeader" class="d-none align-items-center defaultExceedCls " style="">'
                        
                        +'<span class="defaultExceedCls" style="width:31%;">'+getValues(companyLabels,"Document_User")+'</span>'
                        +'<span class="defaultExceedCls" style="width:23%;">'+getValues(companyLabels,"File")+'</span>'
                        +'<span class="defaultExceedCls" style="width:23%;">'+getValues(companyLabels,"Action")+'/'+getValues(companyLabels,"Document_Downloads")+'</span>'
                        +'<span class="defaultExceedCls" style="width:23%;">'+getValues(companyLabels,"Date_and_Time")+'</span>'
                        
                    +'</div>'
                +'</div>'

              +'</div>' 
              +'<div class="modal-body wsScrollBar p-0" style="max-height: 85%;overflow: auto;">'
                +'<div id="docSummaryViewBody" class="" style=""></div>'
                +'<div id="docDetailViewBody" class="" style="display:none;"></div>'
              +'</div>' //body 
              +'<div id="" class="modal-footer pb-3 pr-2">'
              
              +"</div>" //footer
          +'</div>' //content
      +'</div>' //dialog
    +'</div>' //modal

    $("#DocsLinkListDiv").append(ui).show();
    docSummaryView(docid);
    
}

function changeDTrailView(){
    if($("#docDetailViewBody").is(":visible")){
        $("#docSummaryViewHeader").addClass("d-flex").removeClass("d-none");
        $("#docDetailViewHeader").addClass("d-none").removeClass("d-flex");
        $("#docSummaryViewBody").show();
        $("#docDetailViewBody").hide();
    }else{
        $("#docSummaryViewHeader").addClass("d-none").removeClass("d-flex");
        $("#docDetailViewHeader").addClass("d-flex").removeClass("d-none");
        $("#docSummaryViewBody").hide();
        $("#docDetailViewBody").show();
    }
}

function closeDocTrail(){
    $('#DocsLinkListDiv').html('').hide();
    $("#transparentDiv").hide();
}

function docSummaryView(docid){
    let jsonbody = {
        "document_id" : docid,
        "user_id"  : userIdglb
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    $.ajax({
        url: apiPath+"/"+myk+"/v1/showSummary",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          
        },
        success: function (result) { 

            if(result!=""){
                $("#docSummaryViewBody").append(docSummaryViewUI(result));
            }else{
                $("#docSummaryViewBody").html("<div class='' style='text-align:center;font-size:12px;margin-top: 25px;'>No Data Found</div>");
            }
            
            docDetailView(docid);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        
        }
    });       
}

function docDetailView(docid){
    let jsonbody = {
        "document_id" : docid
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    $.ajax({
        url: apiPath+"/"+myk+"/v1/showDetail",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          
        },
        success: function (result) { 
            
            if(result!=""){
                $("#docDetailViewBody").append(docDetailViewUI(result));
            }else{
                $("#docDetailViewBody").append("<div class='' style='text-align:center;font-size:12px;margin-top: 25px;'>No Data Found</div>");
            }
            
            
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        
        }
    });   
}

function docSummaryViewUI(result){
    var ui="";
    
    for(var i=0;i<result.length;i++){
    
        ui+="<div>"
            +"<div class='d-flex align-items-center py-1 px-2 w-100 hov' onclick='listUserTrailDoc("+result[i].document_id+","+result[i].user_id+");event.stopPropagation();' style='border-bottom: 1px solid #aaaaaa;cursor:pointer;'>"
                +"<div class='defaultExceedCls' style='width:30%;'><img src='"+result[i].userImage+"' class='rounded-circle' title='"+result[i].user_name+"' onerror='userImageOnErrorReplace(this);' style='height:30px;width:30px;'><span class='pl-2'>"+result[i].user_name+"</span></div>"
                +"<div class='defaultExceedCls' style='width:20%;text-align: center;'><span class=''>"+result[i].downloadDate+"</span></div>"
                +"<div class='defaultExceedCls' style='width:15%;text-align: center;'><span class=''>"+result[i].download_count+"</span></div>"
                +"<div class='defaultExceedCls' style='width:20%;text-align: center;'><span class=''>"+result[i].modifiedDate+"</span></div>"
                +"<div class='defaultExceedCls' style='width:15%;text-align: center;'><span class=''>"+result[i].modified_count+"</span></div>"
            +"</div>"
            +"<div id='listUserTrailDocDiv_"+result[i].user_id+"'>"

            +"</div>"
        +"</div>"
    
    }    

    return ui;

}

function docDetailViewUI(result){
    var ui="";
    
    for(var i=0;i<result.length;i++){
        
        ui+="<div class='d-flex align-items-center py-1 px-2 w-100 hov' onclick='downloadDetailDoc(\""+result[i].trail_document_name+"\","+result[i].document_id+","+result[i].trail_id+");event.stopPropagation();' style='border-bottom: 1px solid #aaaaaa;cursor:pointer;'>"
            +"<div class='defaultExceedCls' style='width:31%;'><img src='"+result[i].userImage+"' class='rounded-circle' title='"+result[i].name+"' onerror='userImageOnErrorReplace(this);' style='height:30px;width:30px;'><span class='pl-2'>"+result[i].name+"</span></div>"
            +"<div class='defaultExceedCls' style='width:23%;'><span >"+result[i].trail_document_name+"</span></div>"
            +"<div class='defaultExceedCls' style='width:23%;'><span class=''>"+result[i].trailaction+"</span></div>"
            +"<div class='defaultExceedCls' style='width:23%;'><span class=''>"+result[i].date+"</span></div>"
        +"</div>"
    
    } 

    return ui;

}

function listUserTrailDoc(docid,userid){

    if($("#listUserTrailDocDiv_"+userid).children().length==0){
        let jsonbody = {
            "document_id" : docid,
            "user_id" : userIdglb
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');
        $.ajax({
            url: apiPath+"/"+myk+"/v1/showTrailDetail",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            
            },
            success: function (result) { 
                
                if(result!=""){
                    $("#listUserTrailDocDiv_"+userid).append(listUserTrailDocUI(result));
                }else{
                    $("#listUserTrailDocDiv_"+userid).append("<div class='' style='text-align:center;font-size:12px;margin-top: 25px;'>No Data Found</div>");
                }
                
                
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
            
            }
        });
    }else{
        $("#listUserTrailDocDiv_"+userid).html("");
    }      

}

function listUserTrailDocUI(result){

    var ui="";
    ui+="<div class='pl-3 py-1'>"
        +'<div id="docDetailViewHeader" class="d-flex align-items-center defaultExceedCls docsHeader px-2 py-1" style="">'
            +'<span class="defaultExceedCls" style="width:25%;">'+getValues(companyLabels,"Date_and_Time")+'</span>'
            +'<span class="defaultExceedCls" style="width:25%;">'+getValues(companyLabels,"Action")+'</span>'
            +'<span class="defaultExceedCls" style="width:50%;">'+getValues(companyLabels,"File")+'</span>'
        +'</div>'
        for(var i=0;i<result.length;i++){
            
            ui+="<div class='d-flex align-items-center py-2 px-2 w-100 hov'  onclick='downloadDetailDoc(\""+result[i].trail_document_name+"\","+result[i].document_id+","+result[i].trail_id+");event.stopPropagation();' style='border-bottom: 1px solid #aaaaaa;cursor:pointer;'>"
                +"<div class='defaultExceedCls' style='width:25%;'><span >"+result[i].date+"</span></div>"
                +"<div class='defaultExceedCls' style='width:25%;'><span class=''>"+result[i].trail_action+"</span></div>"
                +"<div class='defaultExceedCls' style='width:50%;'><span class=''>"+result[i].trail_document_name+"</span></div>"
            +"</div>"
        
        } 
    ui+="</div>"    

    return ui;

}

function downloadDetailDoc(docName,docId,trailid){
    conFunNew11(getValues(companyAlerts,"Alert_Download_Doc"),"warning","downloadDetailDocConfirm","",docName,docId,trailid);
}

function downloadDetailDocConfirm(docName,docId,trailid){

    var URLForDownLoad = apiPath+"/"+myk+"/v1/download?fileName="+docName+"&Path=//projectDocuments//documnetTrailHistory//&place=document&docId="+docId+"&companyId="+companyIdglb+"&userId="+trailid+"&projectId="+prjid+"";
    
    window.open(URLForDownLoad);
}

function changeDateFormat(d){
    var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let newdate= d.split("-")[2]+" "+monthNames[d.split("-")[1] - 1]+", "+d.split("-")[0];
    return newdate;
}

function wsDocComments(docid,title,latestid,feedid,type){
    
    let jsonbody = {
        "feedType" : "wsDocument",
        "company_id" : companyIdglb,
        "project_id" : prjid,
        "user_id" :userIdglb,
        "doc_id" : docid,
        "localOffsetTime" : localOffsetTime,
        "doc_comment_id" : latestid,
        "type":type,
        "task_comment_id":feedid
      }
      //$('#loadingBar').addClass('d-flex').removeClass('d-none');
      $.ajax({
        url: apiPath + "/" + myk + "/v1/getFeedDetails",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');
          
        },
        success: function (result) { 
          
          var UI=prepareCommentsUI(result, menuutype, prjid,'');
          feedid=parseInt(feedid);
          if(typeof(latestid)=="undefined" || latestid==""){
              popupCommentsUI(docid,title,'Document');
              AddSubComments(0,'',docid);
              $('body').find('#popupCommentsDivbody').append(UI);
              $('#replyBlock_0').focus();
          }else{
              $("#replyBlock_"+feedid).val('');
              if(feedid==0){
                  $(UI).insertAfter("#replyDivContainer_"+feedid).show();
              }else{
                  $("#replyDivContainer_"+feedid).remove();
                  $(UI).insertAfter('#actFeedDiv_' +feedid).show('slow');
              }
            
          }
          
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
      });


}

async function wsdocumentTask(docid,feedname,val1,place,doctype){
	
	let jsonbody = {
		"user_id": userIdglb,
        "company_id": companyIdglb,
        "limit": "",
        "index": "",
        "sortVal": "",
        "text": "",
        "project_id": prjid,
        "sortTaskType": "document",
        "source_id": docid
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
    await $.ajax({
          url: apiPath + "/" + myk + "/v1/getTaskDetails/TeamZone",
          type: "POST",
          contentType: "application/json",
          data: JSON.stringify(jsonbody),
          error: function (jqXHR, textStatus, errorThrown) {
              checkError(jqXHR, textStatus, errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');  
          },
          success: function (result) {
			
			$('#otherModuleTaskListDiv').append(popupTaskUI(docid,feedname)).show();
            $('#popupTasksListHeaderDiv').append(wsTaskUi("wsdocument"));
      
            $("#popupTasksListDivbody").html(createTaskUI(result));
			if(place=="options"){
                insertCreateTaskUI(val1,doctype,feedname);
            }
			
            
			$("#taskname").val(feedname);
            
            if($("#popupTasksListDivbody").html().length==0){
                $("#popupTasksListDivbody").append("<div class='d-flex justify-content-center mt-3 notaskdiv'>No Task Found</div>");
            }

			$('#loadingBar').addClass('d-none').removeClass('d-flex');  
		  }
		});	  	
}

function documentCommentShare(feedid,parentfeedid){
    var docid = $(".popupComments").attr("sourceid");
    let jsonbody = {
		"doc_comment_id" : feedid,
        "document_id" : docid
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
    $.ajax({
        url: apiPath + "/" + myk + "/v1/fetchCommentSharedUsers",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');  
        },
        success: function (result) {
			
			$('#DocsLinkListDiv').html(docCommentShareUI(result,feedid,parentfeedid)).show();
			$("#transparentDiv2").show();
            listDocCmtShareMembers(feedid,parentfeedid)
			$('#loadingBar').addClass('d-none').removeClass('d-flex');  
		}
	});	  


}

function docCommentShareUI(result,feedid,parentfeedid){
	var ui="";
	
    ui='<div class="modal d-flex justify-content-center docCommentShareUICls" id="" feedid='+feedid+' parentfeedid='+parentfeedid+' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' 
      +'<div class="modal-dialog" style="max-width: 80% !important;min-width: 50%;top: 5%;" >'
          +'<div class="modal-content container px-2 py-1" style="max-height: 95%;min-height:95%;font-size: 12px;">' 
              +'<div class="d-flex align-items-center justify-content-start modal-header p-2" style="border-bottom: 1px solid #5e5b5b !important;">' 
                +"<span class='defaultExceedCls' style='font-size:14px;'>COMMENT SHARE</span>"
                +'<button type="button" onclick="docCommentShare();event.stopPropagation();" class="close p-0" data-dismiss="DocsLinkListDiv" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
              +'</div>' 
              +'<div class="modal-body wsScrollBar p-2" style="max-height: 85%;overflow: auto;">'
                +'<div id="docCommentSelected" class="d-block py-1" style="">'
                    +"<div class='position-relative'><span class='px-1 py-0' style='border-bottom: 2px solid #3fa1ee;'>"+getValues(companyLabels,"Participants")+"</span>"
                        +"<img id='addMemberDocCmtShareIcon' onclick='listDocCmtShareMembers("+feedid+","+parentfeedid+");event.stopPropagation();' src='images/task/plus.svg' class='ml-2' style='width:20px;height: 20px;cursor:pointer;'>"
                        +"<div id='docAddMemberDocCmtShareList' class='position-absolute p-2' style='display:none;'></div>"
                    +"</div>"
                    +"<div id='docCmtShareMemberList' class='d-block py-2'>"
                        ui+=docCmtShareUserUI(result)
                    ui+="</div>"
                +'</div>'
              +'</div>' //body 
              +'<div id="" class="modal-footer p-1">'
                +"<div class='p-2'>"
                    +"<span class='createBtn' onclick='saveShareCmtSelected();event.stopPropagation();'>"+getValues(companyLabels,"SAVE")+"</span>"
                +"</div>"
              
              +"</div>" //footer
          +'</div>' //content
      +'</div>' //dialog
    +'</div>' //modal

    return ui;
    
}

function docCommentShare(){
    $('#DocsLinkListDiv').html('').hide();
    $("#transparentDiv2").hide();
}

function listDocCmtShareMembers(feedid,parentfeedid){
    if($("#docAddMemberDocCmtShareList").children().length==0){
        var docid = $(".popupComments").attr("sourceid");
        let jsonbody = {
            "doc_comment_id" : feedid,
            "document_id" : docid,
            "parent_comment_id" : parentfeedid,
            "user_id" : userIdglb
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/fetchToshareUser",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                
                $('#docAddMemberDocCmtShareList').html(docCmtShareMemberListUI(result));
                

                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            }
        });	  
    }else{
        $("#docAddMemberDocCmtShareList").toggle();
    }    
}

function docCmtShareMemberListUI(result){
    var ui="";

    ui="<div class='d-flex align-items-center justify-content-between py-2'>"
        +"<span id='docAddMemberListUISpan'>Select Members</span>"
        +"<input id='docAddMemberListUIInput' onkeyup='docAddMemberListSearch();' type='text' placeholder='search here' class='border-0 w-100' style='display:none;outline:none;border-bottom:1px solid #6c757d !important;'>"
        +"<img src='/images/menus/search2.svg' onclick='searchDocMemberAdd();event.stopPropagation();' style='height:18px;width:18px; cursor:pointer;'>"
    +"</div>"
    ui+=docCmtShareMemberListUILoop(result)

    return ui;

}

function docCmtShareMemberListUILoop(result){
    var ui="";var cls1="";
    for(var i=0;i<result.length;i++){
        if($("#docCmtShareUserId_"+result[i].user_id).length==0){
            cls1="display:block";
        }else{
            cls1="display:none";
        }
        ui+="<div class='docMemberListCls' style='"+cls1+"'>"
            +"<div id='docCmtShareMemberList_"+result[i].user_id+"' class='memberListCls d-flex align-items-center py-1' onclick='addMembertoDocCmtShare("+result[i].user_id+",\""+result[i].name+"\",\""+result[i].userImage+"\");event.stopPropagation();' style='cursor:pointer;'>"
                +"<img src='"+result[i].userImage+"' class='rounded-circle' title='"+result[i].name+"' onerror='imageOnProjNotErrorReplace(this);' style='width:30px;height:30px;'>"
                +"<div class='pl-2 docMemberListNameDiv'>"+result[i].name+"</div>"
            +"</div>"
        +"</div>"
    }
    return ui;
}

function docCmtShareUserUI(result){
    var ui="";
    if(result!=""){
        
        for(var i=0;i<result.length;i++){ 
            
            ui+="<div id='docCmtShareUserId_"+result[i].user_id+"' add='existing' class='shareUserCls d-flex align-items-center p-1'>"
                +"<div style=''><img src='"+result[i].userImage+"' class='rounded-circle' title='"+result[i].name+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'></div>"
                +"<div style='width:90%;' class='defaultExceedCls'><span class='pl-2'>"+result[i].name+"</span></div>"
                +"<div style='width:10%;' class='d-flex align-items-center justify-content-end'>"
                    +"<img id='' src='/images/task/minus.svg' onclick='removeMemberDocCmtShare("+result[i].user_id+");event.stopPropagation();' class='ml-2 removeMemberDocShareCls' style='width:18px;height:18px;cursor:pointer;'>"
                +"</div>"
            +"</div>"
        }
    }else{
        ui="<div class='noUserDiv' style='text-align:center;font-size:12px;'>No Users Selected</div>"
    }
    

    return ui;
}

function addMembertoDocCmtShare(uid,uname,uimg){
    var ui="";
    if($("#docCmtShareUserId_"+uid).length==0){
        ui="<div id='docCmtShareUserId_"+uid+"' add='add' class='shareUserCls d-flex align-items-center p-1'>"
            +"<div style=''><img src='"+uimg+"' class='rounded-circle' title='"+uname+"' onerror='userImageOnErrorReplace(this);' style='width:30px;height:30px;'></div>"
            +"<div style='width:90%;' class='defaultExceedCls'><span class='pl-2'>"+uname+"</span></div>"
            +"<div style='width:10%;' class='d-flex align-items-center justify-content-end'>"
                +"<img id='' src='/images/task/minus.svg' onclick='removeMemberDocCmtShare("+uid+");event.stopPropagation();' class='ml-2 removeMemberDocShareCls' style='width:18px;height:18px;cursor:pointer;'>"
            +"</div>"
        +"</div>"

        $("#docCmtShareMemberList").append(ui);
        $(".noUserDiv").remove();
    }else{
        $("#docCmtShareUserId_"+uid).addClass("d-flex").removeClass("d-none");
        $("#docCmtShareUserId_"+uid).attr("add","add");
    }
    $("#docCmtShareMemberList_"+uid).parent().hide();


}

function removeMemberDocCmtShare(uid){
    
    var fid = $(".docCommentShareUICls").attr("parentfeedid")==0 || $(".docCommentShareUICls").attr("parentfeedid")=="0" ? $(".docCommentShareUICls").attr("feedid") : $(".docCommentShareUICls").attr("parentfeedid");
    let jsonbody = {
        "user_id" : uid,
        "doc_comment_id" : fid
    }
    //$('#loadingBar').addClass('d-flex').removeClass('d-none');
    $.ajax({ 
      url: apiPath+"/"+myk+"/v1/deleteSharedUser",
      type:"DELETE",
      contentType:"application/json",
      data: JSON.stringify(jsonbody),
      error: function(jqXHR, textStatus, errorThrown) {
              checkError(jqXHR,textStatus,errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');
              }, 
      success:function(result){
        $("#docCmtShareMemberList_"+uid).parent().show();
        $("#docCmtShareUserId_"+uid).addClass("d-none").removeClass("d-flex");
        $("#docCmtShareUserId_"+uid).remove();
        if($("#docCmtShareMemberList").children().is(":visible") == false){
            $("#docCmtShareMemberList").html("<div class='noUserDiv' style='text-align:center;font-size:12px;'>No Users Selected</div>");
        }
      }
    });    
}

function saveShareCmtSelected(){
    var fid = $(".docCommentShareUICls").attr("parentfeedid")==0 || $(".docCommentShareUICls").attr("parentfeedid")=="0" ? $(".docCommentShareUICls").attr("feedid") : $(".docCommentShareUICls").attr("parentfeedid");
    var updateAccess="[";
    var uid="";
    $('.shareUserCls').each(function (){
        uid=$(this).attr('id').split('_')[1];
        if($("#docCmtShareUserId_"+uid).attr("add")=="add"){
            updateAccess+="{";
            updateAccess+="\"user_id\":\""+uid+"\"";
            updateAccess+="},";
        }
    });
    updateAccess=updateAccess.length>1?updateAccess.substring(0,updateAccess.length-1)+"]":"[]";
    let jsonbody = { 
        "user_id" : userIdglb,
        "doc_comment_id" : $(".docCommentShareUICls").attr("feedid"),
        "updateAccess": JSON.parse(updateAccess),
        "document_id" : fid
    }
    
    $.ajax({
        url: apiPath+"/"+myk+"/v1/shareCommentfile",
        type:"PUT",
        //dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
            checkError(jqXHR,textStatus,errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            
            },
        success : function(result) {
            
            docCommentShare();
            
        }
    });
}

function dragAndDropDoc(folderId,docId){
    
  
   let jsonbody = { 
    "folder_id" : folderId,
    "document_id" :docId,
    "user_id" : userIdglb,
    "company_id" : companyIdglb,
    "projec_id" : prjid,
    }

 
  $.ajax({

    url : apiPath+"/"+myk+"/v1/WsMoveTofolder",
    //url:"http://localhost:8080/v1/WsMoveTofolder",
    type :"POST",
    contentType:"application/json",
    data:JSON.stringify(jsonbody),
    error : {
        
    },
    success :{

    }

  });

}

function copyMoveDiv(){

    

}

function allowDrop(ev) {
    ev.preventDefault();
  }
  
  function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
  }
  
  function drop(ev,id) {
    ev.preventDefault();
    var folderAccessType = $("#document_"+id).attr('accesstype');
    if(folderAccessType =="R"){
        alertFunNew(getValues(companyAlerts,"Alert_movePermission"),'error');
        
    }else{
        for (var i = 0; i < ev.dataTransfer.files.length; i++) {
            console.log("New file: ", ev.dataTransfer.files[i]);
            readFileUrlinfolder(ev.dataTransfer,id,"",'uploadFromSystem');
        }
    }
    $("#documentList1_"+id).css('background-color','#fff');
}

function allowDrag(ev,id){

   $("#documentList1_"+id).css('background-color','rgba(158,207,250,0.3)');

}

function leaveDrag(ev,id){

    $("#documentList1_"+id).css('background-color','#fff');

}