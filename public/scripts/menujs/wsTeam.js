var indexcontact = 0;
var limitcontact = 0;
var scrollFlag = true;
var menuuplace = "teams";

$(document).ready(function() {
    limitcontact = 0;
    indexcontact = 0;
    listContacts(menuuplace);

    $("#content").on('scroll',()=>{
        
        if(menuuplace=="teams"){
            /* console.log("=====================");
            console.log("=====================");
            console.log($('#content').scrollTop());
            console.log($('#content').height());
            console.log($('#content').scrollTop() +$('#content').height());
            console.log($('#contactsMainDiv').scrollTop());
            console.log($("#contactsMainDiv").height());
            console.log($('#contactsMainDiv').scrollTop() + $("#contactsMainDiv").height() - 25);
            console.log("=====================");
            console.log("====================="); */
            if( scrollFlag==true && $('#content').scrollTop() +$('#content').height() >  $('#teamListDiv').scrollTop() + $("#teamListDiv").height() - 25){ 
                scrollFlag=false;
                indexcontact=limitcontact+indexcontact;
                listContacts(menuuplace,"scrolldata"); 
                setTimeout( function(){
                   scrollFlag=true;
                },1000);
            }
        }
        else if(menuuplace=="teamsTiles"){
            /* console.log("=====================");
            console.log("=====================");
            console.log($('#content').scrollTop());
            console.log($('#content').height());
            console.log($('#content').scrollTop() +$('#content').height());
            console.log($('#contactsTilesDiv').scrollTop());
            console.log($("#contactsTilesDiv").height());
            console.log($('#contactsTilesDiv').scrollTop() + $("#contactsTilesDiv").height() - 25);
            console.log("=====================");
            console.log("====================="); */
            if( scrollFlag==true && $('#content').scrollTop() +$('#content').height() >  $('#contactsTilesDiv').scrollTop() + $("#contactsTilesDiv").height() - 25){ 
                scrollFlag=false;
                indexcontact=limitcontact+indexcontact;
                listContacts(menuuplace,"scrolldata"); 
                setTimeout( function(){
                   scrollFlag=true;
                },1000);
            }
        }
        
    })
});


function listContacts(menuplace,scrolldata){
    console.log("in side team list");
    menuuplace=menuplace;
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    if(menuuplace=="teams"){
        limitcontact = 25;
    }else{
        limitcontact = 24;
    }
    
    let jsonbody = {
        "user_id": userIdglb,
        "companyId": companyIdglb,
        "txt": "",
        "index": indexcontact,
        "limit": limitcontact,
        "sortValue": "",
        "project_id" : prjid
    }
    checksession();
    $.ajax({
        url:apiPath+"/"+myk+"/v1/loadTeamData",
        type:"POST",
        dataType:"json",
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $('#loadingBar').addClass('d-none').removeClass('d-flex');
                },
        success:function(result){
            if(scrolldata!="scrolldata"){
                $("#teamListDiv, #teamTilesDiv").html("");
            }
            
            if(menuuplace=="teams"){
                $("#teamTilesDiv").hide();
                $("#teamListDiv").show()
                $("#teamListDiv").append(prepareContactsUI(result,menuuplace));
                validationUserRoleForInserting(result);
            }else if(menuuplace=="teamsTiles"){
                $("#teamListDiv").hide();
                $("#teamTilesDiv").append(prepareTilesContactsUI(result,menuuplace)).show();
                validationUserRoleForInserting(result);
            }else{

            }
            
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        } 
        
    });
}

function prepareTilesContactsUI(result,menuuplace,newuser){
    var ui="";
    
    ui="<div class='d-flex flex-wrap justify-content-around px-3'>"
    ui+=prepareTilesContactsUIloop(result,menuuplace,newuser)    
    ui+"</div>"

    return ui;

}


function prepareTilesContactsUIloop(result,menuuplace,newuser){
    var ui="";
    var userid = "";var name="";var userimg="";var dept="";var mobile="";var work="";var email="";var status="";var rolename="";var workaddress="";
    var contactNumber = "";var job="";var loginname="";
    var color = ["#f6c2d9","#fff69b","#bcdfca","#a1c8ea","#e4dae2","#ff7eb9","#ff65a3","#7afcff","#feff9c","#fff740","#f39a4e","#eb6092","#4ab6d9","#abcc51","#f9c847","#ffd938","#90909a","#d6d4df","#b3cce2","#1dace6","#73cac5","#e3e546","#f2788f","#f69dbb","#fbad4b"];
    for(let i=0;i<result.length;i++){
        userid = result[i].user_id;
        name = result[i].name;
        dept = result[i].user_department;
        mobile = result[i].user_phone;
        work = result[i].work_number;
        email = result[i].user_email;
        userimg = result[i].userImage;
        rolename = result[i].role_name;
        // job = result[i].user_job;
        workaddress = result[i].user_work_address;
        loginname = result[i].user_login_name;

        // status = $("#userId_"+userid).find(":nth-child(2)").attr("class");
        // console.log(status);
        // if(typeof(status)=="undefined"||status=="undefined"){
        //     if($("#chatIcon").attr("src").indexOf('chatgreen.svg') != -1 && newuser!="newuser"){
        //         status="user_box_status_online";
        //     }else{
        //         status="user_box_status";
        //     }
        // }else{
        //    if(status=="user_box_status"){
        //         status="user_box_status";
        //    }else{
        //         status="user_box_status_online";
        //    } 
        // }
        
        status = $("#userId_"+userid).find(":nth-child(2)").attr("class");
        if(typeof(status)=="undefined"||status=="undefined"){
            if($("#chatIcon").attr("src").indexOf('chatgreen.svg') != -1 && newuser!="newuser"){
                status="user_box_status_online";
            }else{
                status="user_box_status";
            }
        }  
        
        job = job.trim()==""?"-":job;

        if(work.trim()=="-" && mobile.trim()=="-"){
            contactNumber = "-";
        }else if(work.trim()=="" && mobile.trim()==""){
            contactNumber = "-";
        }else if(work.trim()=="-" && (mobile.trim()!="-" || mobile.trim()!="")){
            contactNumber = mobile;
        }else if(work.trim()=="" && (mobile.trim()!="-" || mobile.trim()!="")){
            contactNumber = mobile;
        }else if((work.trim()!="-" || work.trim()!="") && (mobile.trim()=="-")){
            contactNumber = work;
        }else if((work.trim()!="-" || work.trim()!="") && (mobile.trim()=="")){
            contactNumber = work;
        }else if(work.trim()!="-" && work.trim()!="" && mobile.trim()!="" && mobile.trim()!="-"){
            contactNumber = mobile;
        }


        ui+="<div id='contactListUser_"+userid+"'  class='contactsListDivCls card my-3 mx-1 border-0' style='height: auto;background-color:"+color[i]+"'>"
            +'<div class="media border-0 py-1 px-2 w-100 d-block" onclick="showTilePopup('+userid+');event.stopPropagation();">'
                +"<div class='d-flex align-items-center pt-2 px-1'>"
                    +"<div class='w-25 position-relative'>"
                    if(userid == userIdglb){
                        ui+='<img id="" src="'+userimg+'" onclick="event.stopPropagation();getNewConvId('+userid+', this);" title="'+name+'" onerror="userImageOnErrorReplace(this);" class="tileContantImage rounded-circle" style="border: 0px solid #a3c2e1;width: 64px;height: 64px;">'
                    }else{
                        ui+='<img id="" src="'+userimg+'" onclick="event.stopPropagation();getNewConvId('+userid+', this);" title="'+name+'" onerror="userImageOnErrorReplace(this);" class="tileContantImage rounded-circle cursor" style="border: 0px solid #a3c2e1;width: 64px;height: 64px;">'
                    }
                    ui+="<span class='"+status+" user_box_status uid_"+userid+"' style='left: 48px !important;'></span>"
                    +"</div>"
                    +'<div class="media-body w-75 pl-3">'
                        +'<div class="my-2">'
                            +'<h5 class="defaultExceedCls" title="'+name+'" style="font-size:14px;font-weight: bold;">'+name+'</h5>'
                            +'<p class="defaultExceedCls" title="'+job+'" style="font-size:12px;">'+job+'</p>'
                        +'</div>'
                    +'</div>'
                +"</div>"    
                +'<div class="media-body w-100 pl-5 pt-1 pb-2">'
                    +'<div class="d-flex align-items-center pl-2"><img src="images/landingpage/call.svg" style="width:18px;height:18px;">'
                        +'<span class="defaultExceedCls pl-2" title="'+contactNumber+'" style="font-size:12px;">'+contactNumber+'</span>'
                    +'</div>'  
                    +'<div class="d-flex align-items-center my-2 pl-2"><img src="images/landingpage/email.svg" style="width:18px;height:18px;"><span class="defaultExceedCls pl-2" title="'+email+'" style="font-size:12px;">'+email+'</span></div>'  
                    +'<div class="d-flex align-items-center pl-2"><img src="images/landingpage/userlogin.svg" style="width:18px;height:18px;"><span class="defaultExceedCls pl-2" title="'+loginname+'" style="font-size:12px;">'+loginname+'</span></div>'  
                +"</div>"
            +'</div>'

            +"<div id='userPopup_"+userid+"' class='position-absolute userPopup' style='display:none;'>"
                ui+=openTileUserPopup(result[i],status)
            ui+="</div>"    
            
        +"</div>"
        

    }
    return ui;
}

function validationUserRoleForInserting(result){
    var userid = "";
    var userStatus = "";
    for(let i=0;i<result.length;i++){
        userid = result[i].user_id;
        userStatus = result[i].proj_user_status;
        if(userid.match(userIdglb)){
            if(userStatus.match("PO")){
                $("#insertUserInTeam").removeClass('d-none').addClass('d-block');
                $("#addButtonForTeam").attr("onclick", "fetchWsData('updateWSProject',"+prjid+",'team');event.stopPropagation();");
                $("#teamcsvimageoption").removeClass('d-none').addClass('d-block');
            }else{
                $("#insertUserInTeam").removeClass('d-block').addClass('d-none');
                $("#teamcsvimageoption").removeClass('d-block').addClass('d-none');
            }
        }
    }
}

function prepareContactsUI(result,menuplace,newuser){
    var ui = "";
    var userid = "";var name="";var userimg="";var dept="";var mobile="";var work="";var email="";var status="";
    for(let i=0;i<result.length;i++){
        userid = result[i].user_id;
        name = result[i].name;
        dept = result[i].user_department;
        mobile = result[i].user_phone;
        work = result[i].work_number;
        email = result[i].user_email;
        userimg = result[i].userImage;

        status = $("#userId_"+userid).find(":nth-child(2)").attr("class");
        console.log("status--"+status+"userid--"+userid);
        if(typeof(status)=="undefined"||status=="undefined"){
            if($("#chatIcon").attr("src").indexOf('chatgreen.svg') != -1 && newuser!="newuser"){
                status="user_box_status_online";
            }else{
                status="user_box_status";
            }
        }    

        ui+="<div id='contactListUser_"+userid+"' class='contactsListCls d-flex align-items-center px-2 py-1 hov' onclick=\"getUserDetails("+userid+",'"+menuplace+"');event.stopPropagation();\" style='border-bottom:1px solid #AAAAAA;font-size:12px;cursor:pointer;'>"
            +"<div style='width: 6%;text-align: center;position:relative;'>"
                +"<div class='position-relative'>"
                    // +"<img src='"+userimg+"' title='"+name+"' onerror='userImageOnErrorReplace(this);' class='uid_"+userid+" rounded-circle' style='width:30px;height:30px;'>"
                    if(userid == userIdglb){
                        ui+="<img onclick='event.stopPropagation();getNewConvId("+userid+", this);'  src='"+userimg+"' title='"+name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle' style='width:30px;height:30px;'>"
                    }else{
                        ui+="<img onclick='event.stopPropagation();getNewConvId("+userid+", this);'  src='"+userimg+"' title='"+name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle cursor' style='width:30px;height:30px;'>"
                    }
                    ui+="<span class='uid_"+userid+" user_box_status "+status+" position-absolute' style=''></span></div>"
                +"</div>"
            +"<div class='defaultExceedCls' title='"+name+"' style='width: 24%;'>"+name+"</div>"
            +"<div class='defaultExceedCls' title='"+dept+"' style='width: 20%;'>"+dept+"</div>"
            +"<div class='defaultExceedCls' title='"+mobile+"' style='width: 15%;'>"+mobile+"</div>"
            +"<div class='defaultExceedCls' title='"+work+"' style='width: 15%;'>"+work+"</div>"
            +"<div class='defaultExceedCls' title='"+email+"' style='width: 20%;'>"+email+"</div>"
        +"</div>"
    }    
    return ui;
}

function openTileUserPopup(result,status){
    
    var ui="";
    var contactNumber="";
    var userid = result.user_id;
    var name = result.name;
    var dept = result.user_department;
    var mobile = result.user_phone;
    var work = result.work_number;
    var email = result.user_email;
    var userimg = result.userImage;
    var rolename = result.role_name;
    var job = "-";
    var workaddress = result.user_work_address;
    var homeaddress = result.user_home_address;
    var loginname = result.user_login_name;
    job = job.trim()==""?"-":job;

    if(work.trim()=="-" && mobile.trim()=="-"){
        contactNumber = "-";
    }else if(work.trim()=="" && mobile.trim()==""){
        contactNumber = "-";
    }else if(work.trim()=="-" && (mobile.trim()!="-" || mobile.trim()!="")){
        contactNumber = mobile;
    }else if(work.trim()=="" && (mobile.trim()!="-" || mobile.trim()!="")){
        contactNumber = mobile;
    }else if((work.trim()!="-" || work.trim()!="") && (mobile.trim()=="-")){
        contactNumber = work;
    }else if((work.trim()!="-" || work.trim()!="") && (mobile.trim()=="")){
        contactNumber = work;
    }else if(work.trim()!="-" && work.trim()!="" && mobile.trim()!="" && mobile.trim()!="-"){
        contactNumber = mobile;
    }


    ui="<div class='d-flex px-2 py-3'>"
        +"<div class='col-3 position-relative p-0' style='text-align:center;'>"
            +"<img src='"+userimg+"' title='"+name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle' style='width: 75px;height: 75px;'>"
            +"<span class='"+status+" position-absolute' style=''></span>"
        +"</div>"
        +"<div class='col-9 pl-3 pr-0' style=''>"
            +'<h5 class="defaultExceedCls pt-3" title="'+name+'" style="font-size:14px;font-weight: bold;">'+name+'</h5>'
            +'<p class="defaultExceedCls" title="'+rolename+'" style="font-size:12px;">'+rolename+'</p>'
            +"<div class='d-flex py-2'>"
                +"<img src='images/landingpage/call.svg' class='iconClass'>"
                +"<input class='border-0 pl-2 ' title='"+contactNumber+"' value='"+contactNumber+"' onclick='event.stopPropagation();' style='outline:none;width: -webkit-fill-available;'>"
                +"<img src='/images/conversation/edit.svg' class='iconClass'>"
            +"</div>"
            +"<div class='d-flex py-2'>"
                +"<img src='images/landingpage/email.svg' class='iconClass'>"
                +"<input class='border-0 pl-2 ' title='"+email+"' value='"+email+"' onclick='event.stopPropagation();' style='outline:none;width: -webkit-fill-available;'>"
                +"<img src='/images/conversation/edit.svg' class='iconClass'>"
            +"</div>"
            +"<div class='d-flex py-2'>"
                +"<img src='images/landingpage/userlogin.svg' class='iconClass'>"
                +"<input class='border-0 pl-2 ' title='"+loginname+"' value='"+loginname+"' onclick='event.stopPropagation();' style='outline:none;width: -webkit-fill-available;'>"
            +"</div>"
            +"<div class='d-flex align-items-center py-2'>"
                +"<img src='images/temp/file.png' class='iconClass'>"
                +"<input class='border-0 pl-2 ' title='"+job+"' value='"+job+"' onclick='event.stopPropagation();' style='outline:none;resize:none;width: -webkit-fill-available;'>"
            +"</div>"
            +"<div class='d-flex align-items-center py-2'>"
                +"<img src='images/temp/address.png' class='iconClass'>"
                +"<span class='border-0 pl-2 mr-auto' style='-webkit-line-clamp: 3;   overflow: hidden;word-break: break-all;-webkit-box-orient: vertical;display: -webkit-box;'>"+workaddress+"</span>"
                //+"<textarea class='border-0 pl-2 mr-auto' onclick='event.stopPropagation();' style='outline:none;resize:none;width: -webkit-fill-available;'>"+workaddress+"</textarea>"
            +"</div>"
            +"<div class='d-flex align-items-center py-2'>"
                +"<img src='images/temp/address.png' class='iconClass'>"
                +"<span class='border-0 pl-2 mr-auto' style='-webkit-line-clamp: 3;   overflow: hidden;word-break: break-all;-webkit-box-orient: vertical;display: -webkit-box;'>"+homeaddress+"</span>"
                //+"<textarea class='border-0 pl-2 mr-auto' onclick='event.stopPropagation();' style='outline:none;resize:none;width: -webkit-fill-available;'>"+homeaddress+"</textarea>"
            +"</div>"

        +"</div>"
    +"</div>"
    +"<div clas='d-flex align-items-center'>"
        +"<div class='mr-3'><img src='/images/task/tick.svg' onclick='showTilePopup("+userid+");event.stopPropagation();' class='mx-1 my-2' style='width:25px;height:25px;cursor:pointer;float: right;'></div>"
        +"<div class=''><img src='/images/task/remove.svg' class='mx-1 my-2' style='width:25px;height:25px;cursor:pointer;float: right;'></div>"
    +"</div>"



    return ui;
}