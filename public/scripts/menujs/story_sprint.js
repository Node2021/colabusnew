
 
 /**
   *
   *
   *
   this file contains common scripts that are used in both story and sprint menus.
   *
   *
   *
   *
 **/
 function prepareTaskHeaderUI(){
    var taskUI='';
    taskUI+="<div id=\"assignedtaskheaders\" class=\"tabContentHeader\" style='margin-top:-5px;'>";
    taskUI+=" <div class=\"tabContentSubHeader\" style='margin: 0px;'>";
	taskUI+="	<div class=\"tabContentHeaderName \" style=\"width: 5%;font-size:13px;\"><span style=\"float: left; height: 20px; margin-left: 5%; width: 95%; overflow: hidden;\" class=\"Type_cLabelText\"></span></div>";
	taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 31%;font-size:13px;\"><span style=\"margin-left: 2%; float: left; width: 95%;\" class=\"Task_cLabelText\"></span></div>";
	taskUI+="	<div class=\"tabContentHeaderName Assigned_to_cLabelHtml\" style=\"width:20%;font-size:13px;height:20px;overflow:hidden;\"></div>";
	taskUI+="	<div class=\"tabContentHeaderName Created_by_cLabelHtml\" style=\"width: 13%;font-size:13px;height:20px;overflow:hidden;\"></div>";
	taskUI+="	<div class=\"tabContentHeaderName Start_date_cLabelHtml\" style=\"width: 10%;font-size:13px;height:20px;overflow:hidden;\"></div>";
	taskUI+="	<div class=\"tabContentHeaderName End_date_cLabelHtml\" style=\"width: 10%;font-size:13px;height:20px;overflow:hidden;\"></div>";
	taskUI+="	<div class=\"tabContentHeaderName Status_cLabelHtml\" style=\"width: 5%;font-size:13px;height:20px;overflow:hidden;\"></div>";
	taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 5%;font-size:13px;height:20px;overflow:hidden;text-align:center;\">Trend</div>";
	taskUI+=" </div>";
	taskUI+="</div>";
	
	return taskUI;
 }
 
 
 
 function prepareTaskListUI(data,viewPlace){	
    var taskUI='';
	/*if(viewPlace=="SB"){
		if(data){
	     	var json = eval(data);
			for(var i=0; i<json.length; i++){	
				taskUI+="<div id=\"createTaskListUiDiv_"+json[i].taskId+"\" ></div>"
			}
		}
	}*/
    taskUI+="<div id=\"assignedtaskheaders\" class=\"tabContentHeader\" >";
    taskUI+=" <div class=\"tabContentSubHeader\" style='margin: 0px;'>";
    if(viewPlace=="Epic"){
		taskUI+="	<div class=\"tabContentHeaderName \" style=\"width: 5%;font-size:13px;\"><span style=\"float: left; height: 20px; margin-left: 5%; width: 95%; overflow: hidden;\" class=\"Type_cLabelText\"></span></div>";
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls\" title=\"Action\" style=\"width: 5%;\">Action</div>";
		taskUI+="	<div class=\"tabContentHeaderName Task_Id_cLabelHtml\" style=\"width:5%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 34%;font-size:13px;\"><span style=\"float: left; width: 95%;\" class=\"Task_cLabelText\"></span></div>";
		///taskUI+="	<div class=\"tabContentHeaderName Assigned_to_cLabelHtml\" style=\"width:20%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		//taskUI+="	<div class=\"tabContentHeaderName Created_by_cLabelHtml\" style=\"width: 12%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 10%;font-size:13px;height:20px;overflow:hidden;\">Start</div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 10%;font-size:13px;height:20px;overflow:hidden;\">Due</div>";
		taskUI+="	<div class=\"hLModified tabContentHeaderName defaultExceedCls\" title=\"Estimated Time\" style=\"width: 8%;\">Estimated</div>";
		taskUI+="	<div class=\"hLModified tabContentHeaderName defaultExceedCls\" title=\"Actual Time\" style=\"width: 8%;\">Actual</div>";
		taskUI+="	<div class=\"tabContentHeaderName Status_cLabelHtml\" style=\"width: 5%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 5%;font-size:13px;height:20px;overflow:hidden;text-align:center;\">Trend</div>";
		taskUI+="	<div class=\"hSize tabContentHeaderName defaultExceedCls\" title=\""+getValues(companyLabels,"Priority")+"\" style=\"width: 5%;\">"+getValues(companyLabels,"Priority")+"</div>";
	}else if(viewPlace=="SB"){
		taskUI+="	<div class=\"tabContentHeaderName \" style=\"width: 6%;font-size:13px;\"><span style=\"float: left; height: 20px; margin-left: 5%; width: 95%; overflow: hidden;\" class=\"Type_cLabelText\"></span></div>";
		taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls\" title=\"Action\" style=\"width: 6%;\">Action</div>";
		taskUI+="	<div class=\"tabContentHeaderName Task_Id_cLabelHtml\" style=\"width:6%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 30%;font-size:13px;\"><span style=\"float: left; width: 95%;\" class=\"Task_cLabelText\"></span></div>";
		///taskUI+="	<div class=\"tabContentHeaderName Assigned_to_cLabelHtml\" style=\"width:20%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		//taskUI+="	<div class=\"tabContentHeaderName Created_by_cLabelHtml\" style=\"width: 12%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 10%;font-size:13px;height:20px;overflow:hidden;\">Start</div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 10%;font-size:13px;height:20px;overflow:hidden;\">Due</div>";
		taskUI+="	<div class=\"hLModified tabContentHeaderName defaultExceedCls\" title=\"Estimated Time\" style=\"width: 8%;\">Estimated</div>";
		taskUI+="	<div class=\"hLModified tabContentHeaderName defaultExceedCls\" title=\"Actual Time\" style=\"width: 8%;\">Actual</div>";
		taskUI+="	<div class=\"tabContentHeaderName Status_cLabelHtml\" style=\"width: 5%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 5%;font-size:13px;height:20px;overflow:hidden;text-align:center;\">Trend</div>";
		taskUI+="	<div class=\"hSize tabContentHeaderName defaultExceedCls\" title=\""+getValues(companyLabels,"Priority")+"\" style=\"width: 6%;\">"+getValues(companyLabels,"Priority")+"</div>";
	}else{
	    taskUI+="	<div class=\"tabContentHeaderName \" style=\"width: 5%;font-size:13px;\"><span style=\"float: left; height: 20px; margin-left: 5%; width: 95%; overflow: hidden;\" class=\"Type_cLabelText\"></span></div>";
	    taskUI+="	<div class=\"tabContentHeaderName defaultExceedCls\" title=\"Action\" style=\"width: 5%;\">Action</div>";
		taskUI+="	<div class=\"tabContentHeaderName Task_Id_cLabelHtml\" style=\"width:6%;font-size:13px;height:20px;overflow:hidden;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 32%;font-size:13px;\"><span style=\"margin-left: -3.5%; float: left; width: 95%;\" class=\"Task_cLabelText\"></span></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 10%;font-size:13px;height:20px;overflow:hidden;padding-left: 12px;\">Start</div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 10%;font-size:13px;height:20px;overflow:hidden;padding-left: 12px;\">Due</div>";
		taskUI+="	<div class=\"hLModified tabContentHeaderName defaultExceedCls\" title=\"Estimated Time\" style=\"width: 8%;\">Estimated</div>";
		taskUI+="	<div class=\"hLModified tabContentHeaderName defaultExceedCls\" title=\"Actual Time\" style=\"width: 8%;\">Actual</div>";
		taskUI+="	<div class=\"tabContentHeaderName Status_cLabelHtml\" style=\"width: 5%;font-size:13px;height:20px;overflow:hidden;text-align:center;\"></div>";
		taskUI+="	<div class=\"tabContentHeaderName\" style=\"width: 5%;font-size:13px;height:20px;overflow:hidden;text-align:center;\">Trend</div>";
		taskUI+="	<div class=\"hSize tabContentHeaderName defaultExceedCls\" title=\""+getValues(companyLabels,"Priority")+"\" style=\"width: 5%;\">"+getValues(companyLabels,"Priority")+"</div>";
	}
	taskUI+=" </div>";
	taskUI+="</div>";
	
	taskUI+="<div id=\"taskListDiv\" style='width:100%;'>";
	/*if(viewPlace == 'SB'){
		taskUI+="<div id=\"createTaskListViewUI\" style='width:100%;'>";
	}*/
    taskUI+= prepareSprintTasksList(data, viewPlace);
    taskUI+="</div>";
   return taskUI;
   
 }
 
 function prepareSprintTasksList(data, viewPlace){
	  var taskUI = '';
	   if(data){
	     var json = eval(data);
	     var assignedTo='';	
	     var sentimentBgCol='';
	     if(viewPlace=="Epic"){
		     for(var i=0; i<json.length; i++){	    	 
				 var titled = json[i].Priority_image == "" ? "": json[i].Priority_image.indexOf("images/VeryImportant.png") != -1 ? "Very important" : json[i].Priority_image.indexOf("images/Important.png") != -1 ? "Important": json[i].Priority_image.indexOf("images/medium.png") != -1 ? "Medium": json[i].Priority_image.indexOf("images/Low.png") != -1 ? "Low": json[i].Priority_image.indexOf("images/VeryLow.png") != -1 ? "Very low": json[i].Priority_image.indexOf("images/none.png") != -1 ? "None" :"";
		    	 json[i].taskSubject = replaceSpecialCharacter(json[i].taskSubject);	    	
		    	 json[i].taskSubject = unicodeTonotificationValue(json[i].taskSubject);	    	 
		       /*  if(userId==json[i].createdById || projectUsersStatus=="PO" ){
	                  taskUI+="<div id=\"task_"+json[i].taskId+"\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"viewTask("+json[i].taskId+","+json[i].taskListType+");\">";  
					//taskUI+="<div id=\"task_"+json[i].taskId+"\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"viewTask("+json[i].taskId+",'assignedSprint');\">";  
				 }else{
					taskUI+="<div id=\"task_"+json[i].taskId+"\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"myStroyTasks("+json[i].epicId+","+json[i].taskId+");\">"; 
				 }*/
			
				 var taskStartDate=json[i].startDate;
			     var taskEndDate=json[i].endDate;
				 var taskstarttime=json[i].startTime;
			     var taskendtime=json[i].endTime;
				
				 if(taskStartDate == "00-00-0000"){
			    	taskStartDate = "-";
				 }else{
					taskStartDate = taskStartDate.replaceAll("-", " ");
				 }
				 if(taskEndDate == "00-00-0000"){
				   	taskEndDate = "-";
				 }else{
				   	taskEndDate = taskEndDate.replaceAll("-", " ");
				 }	
			
				 var userTaskStatus = json[i].userTaskStatus;
				 var statusImg="";
				 var userStatus="";
				 if(userTaskStatus=="Created"){
					userStatus="Created";
					statusImg = path+"/images/task_created.svg";
				 }else if(userTaskStatus=="Inprogress"){
						userStatus="Inprogress";
						statusImg = path+"/images/task_inprogress.svg";
				 }else if(userTaskStatus=="Paused"){
						userStatus="Paused";
						statusImg = path+"/images/task_paused.svg";
				 }else if(userTaskStatus=="Completed"){
						userStatus="Completed";
						statusImg = path+"/images/task_completed.svg";
				 }else{
						statusImg = path+"/images/task_nostatus.svg";
				 }
				 taskUI+="<div id=\"task_"+json[i].taskId+"\" style=\"width:100%;float:left;\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"newUicreateTaskUI('update',"+json[i].taskId+",'"+json[i].taskListType+"',"+json[i].epicId+",'"+json[i].type+"','','','','sprintTask','','Epic');\">";  	//viewTask("+json[i].taskId+",'"+json[i].taskListType+"','"+json[i].epicId+"','"+json[i].type+"');
				 taskUI+="    <div class=\"docListViewBorderCls taskId_"+json[i].taskId+"\" style=\"padding-top: 8px;font-size:12px;margin:0px;height:80px;\">";
				 
				/********************************************************************  First Row columns ********************************************************************/
	 			 taskUI+="<div>";
				 
				 taskUI+=" <div class=\"tabContentHeaderName\" align=\"left\" style=\"width:5%;margin-top: 1px;float: left;padding-left: 10px;\"><img title = \"Sprint Task\" class=\"taskimgClss\" style = \"width: 80%;height: 80%\" src=\""+path+"/images/calender/newsprintimg.png\" /></div>";
				 
				 if(userStatus != ""){
					taskUI+=" <div  class=\"tabContentHeaderName\" align=\"center\" style=\"margin-top: 2px;width:5%;padding-left: 0px;\"> <img title=\"\" id=\"taskPlay_"+json[i].taskId+"\" onclick=\"changeStatusOfUserTask("+json[i].taskId+","+json[i].project_id+",'listView');event.stopPropagation();\" src=\""+statusImg+"\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 15px;margin-top: 1px;\" class=\"img-responsive\"></div>"; //+json[i].project_id+
				 }else{
					taskUI+=" <div  class=\"tabContentHeaderName\" align=\"center\" style=\"margin-top: 1px;width:5%;padding-left: 0px;\"> <img title=\"\" id=\"\"  src=\""+statusImg+"\" style = \"width: 20px;margin-top: 1px;\" class=\"img-responsive\"></div>";
				 }
			
				 taskUI+="  <div class=\"tabContentHeaderName defaultExceedCls\" title=\""+json[i].taskId+"\" style=\"width:5%;margin-top: 3px;float: left;padding-left: 1px;font-size: 14px;\">"+json[i].taskId+"</div>";
				 
				 taskUI+="  <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:70%;margin-top: 3px;float:left;font-size: 14px;\" title=\""+json[i].taskSubject+"\">"+TextLink(json[i].taskSubject)+"</div>";
		         
				 taskUI+="  <div class=\"tabContentHeaderName\" align=\"left\" valign=\"middle\" style=\"float:left;margin-top:6px;width:5%;padding-left: 10px;\"><img title=\""+json[i].userCompletedStatus+"\" style=\"width:20px;\" src=\""+json[i].taskStatusImage+"\" /></div>";
				 
				 taskUI+="  <div align=\"center\" style=\"width:5%;padding-top: 2px;padding-left:0px;padding-right:0px;\"  class=\"col-sm-3 col-xs-3 tabContentHeaderName defaultExceedCls\">";
						
			 			if(json[i].trending >=-1 && json[i].trending <=-0.49){
			        		sentimentBgCol='images/tasksNewUI/meterred.png';
			        		
			        	}else if(json[i].trending >=-0.50 && json[i].trending <0){
			        		sentimentBgCol='images/tasksNewUI/meteryellow.png';
			        		
			        	}else if(json[i].trending == 0 || json[i].trending == 0.0){
			        		sentimentBgCol='images/tasksNewUI/meteryellow_center.png';
			        		
					    }else if((json[i].trending >0 || json[i].trending > 0.0)&& json[i].trending <=0.49){
			        		sentimentBgCol='images/tasksNewUI/meteryellow_Lightgreen.png';
			        		
			        	}else if(json[i].trending >=0.5 && json[i].trending <=1){
			        		sentimentBgCol='images/tasksNewUI/meter_fullgreen.png';
			        		
			        	}else{
			        		json[i].trending = 'No Data';
			        		sentimentBgCol='images/tasksNewUI/meterDefault.png';  
			        		
			        	}
			 	 taskUI += "	<img title=\""+json[i].trending+"\"  src=\""+path+"/"+sentimentBgCol+"\" style=\"width:30px;height:30px;\"  >";
			        	//taskUI += ' <div  title="'+trending+'" style="width: 20px;height: 5px;background-color:'+sentimentBgCol+'"></div>'
			 	 taskUI += "</div>";
		
				 taskUI+="<div  class=\"rTasksPriority\" align=\"middle\" valign=\"middle\" style=\" width: 5%;float: left;margin-top: 6px;\"><img title=\""+json[i].priorityTitle+"\" src=\""+path+""+json[i].Priority_image+"\" ></div>";
			
				 
				 taskUI+="</div>";
				 
				 /********************************************************************  First Row columns end********************************************************************/
				/********************************************************************  second Row columns********************************************************************/
				 taskUI+="<div>";
			
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" title=\"\" style=\"width: 6%;color: darkgrey;font-size: 14px;padding-top: 6px;\">Created</div>";
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" style=\"float:left;width:4%;margin-left: -5px;padding-top: 2px;\">";
	         		var createdByImg = json[i].createdByDetail;
			 		if(createdByImg==""){
				    		taskUI+="<div style=\"float:left;margin-left: 12.5%; padding-top: 1.8%;\">-</div>";
				    }else{
					   	for(var j=0; j<createdByImg.length; j++){
						    userImg = lighttpdPath+"/userimages/"+createdByImg[j].userId+"."+createdByImg[j].userImgExtn+'?'+createdByImg[j].imgTimeStamp;
						    taskUI+='<img class="img-responsive" style="float:left;margin-right:5px;width:25px;height:25px;border-radius:50%;" src="'+userImg+'" title="'+createdByImg[j].userName+'" onerror="javascript:userImageOnErrorReplace(this);">';
						}
				    }
		 		 taskUI+="</div>";
			
				 taskUI+="<div style=\"width: 5%;float: left;color:darkgrey;font-size: 14px;text-align: right;padding-top: 6px;\"><span style=\"\">Assigned</span></div>";
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" style=\"float:left;width:34%;margin-top: 1px;padding-left: 11px;\">";
		         assignedTo = json[i].AssignedUsersName;
			         for(var j=0; j<assignedTo.length; j++){
			             userImg = lighttpdPath+"/userimages/"+assignedTo[j].userId+"."+assignedTo[j].userImgExtn+'?'+assignedTo[j].imgTimeStamp;
			             taskUI+="<img class=\"img-responsive\" style=\"float:left;margin-right:5px;width:25px;height:25px;border-radius:50%;\" src=\""+userImg+"\" title=\""+assignedTo[j].userName+"\" onerror=\"javascript:userImageOnErrorReplace(this);\">";
					 }
		         taskUI+="</div>";
			
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:10%;float:left;padding-top: 6px;color: darkgrey;font-size: 14px\" title=\""+taskStartDate+" "+taskstarttime+"\"><div>"+taskStartDate+"</div></div>";
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:10%;float:left;padding-top: 6px;color: darkgrey;font-size: 14px\" title=\""+taskEndDate+" "+taskendtime+"\"><div>"+taskEndDate+"</div></div>";
				
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:8%;float:left;padding-top: 6px;padding-left: 10px;color: darkgrey;font-size: 14px\"><div>"+json[i].estimatedHour+" h "+json[i].estimatedMinute+" m</div></div>";
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:8%;float:left;padding-top: 6px;padding-left: 7px;color: darkgrey;font-size: 14px\"><div>"+json[i].actualHour+" h "+json[i].actualMinute+" m</div></div>";
					
				 taskUI+="<div align=\"right\" class=\"cmntsIcon\" style=\"display:none;width: 15%;float: left;padding-right: 29px;padding-top: 6px;\"><img id=\"cIcon_"+json[i].id+"\" style=\"cursor:pointer;width: 20px;display:none;\" title=\""+getValues(companyLabels,"View_comments")+"\" src=\""+path+"/images/commentBlack2.png\" onclick=\"showTaskComments("+json[i].id+","+json[i].project_id+");event.stopPropagation();\"></div>";
				 
				 taskUI+="<input type=\"hidden\" id=\"taskListType_"+json[i].taskId+"\" value=\""+json[i].getViewType+"\" />";	
			
				 taskUI+="</div>";
			
			
				/********************************************************************  second Row columns end********************************************************************/
				 
				taskUI+="</div>";
				
				taskUI+="<div id=\"Taskcomment_"+json[i].taskId+"\" class=\"epicCommonListDivCss\" style=\'padding-top: 20px;padding-right: 20px;width: 100%;margin-left: 0px;padding-left: 20px;float: left;margin-bottom: 0px;\'>"
 	            +"		<div class=\"TaskCommentMainTextareaDiv\" style=\"float:left;width:100%;padding-right: 20px;\">"
 	            +"		</div>"
 	            +"		<div class=\"TaskCommentListDiv\" style=\"float:left;width:100%;border-top: 1px solid #c1c5c8;\">"
 	            +"		</div>"
     	        +"	 </div>"
						
				+"	 <div id=\"TaskStatus_"+json[i].taskId+"\" class=\"epicCommonListDivCss TaskCommentContainer\" onclick=\"event.stopPropagation();\" style=\"padding: 5px 0px 0px;float: left;width: 100%;margin-left: 0px;margin-right: 0px;margin-bottom: 0px;border-radius: 0px;display: none;overflow: hidden;\">"
				+"		<div class=\"TaskStatusMainTextareaDiv\" style=\"float:left;width:100%;border-bottom: 1px solid rgb(193, 197, 200);padding: 0px 5px 5px 5px;\">"
				+"		</div>"
			    +"	 </div>";
				
				taskUI+="</div>";	 
			
			
			
		     }
	     }else if(viewPlace=="SB"){
		     for(var i=0; i<json.length; i++){	    	 
				 var titled = json[i].Priority_image == "" ? "": json[i].Priority_image.indexOf("images/VeryImportant.png") != -1 ? "Very important" : json[i].Priority_image.indexOf("images/Important.png") != -1 ? "Important": json[i].Priority_image.indexOf("images/medium.png") != -1 ? "Medium": json[i].Priority_image.indexOf("images/Low.png") != -1 ? "Low": json[i].Priority_image.indexOf("images/VeryLow.png") != -1 ? "Very low": json[i].Priority_image.indexOf("images/none.png") != -1 ? "None" :"";
		    	 json[i].taskSubject = replaceSpecialCharacter(json[i].taskSubject);	    	
		    	 json[i].taskSubject = unicodeTonotificationValue(json[i].taskSubject);	    	 
		       /*  if(userId==json[i].createdById || projectUsersStatus=="PO" ){
	                  taskUI+="<div id=\"task_"+json[i].taskId+"\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"viewTask("+json[i].taskId+","+json[i].taskListType+");\">";  
					//taskUI+="<div id=\"task_"+json[i].taskId+"\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"viewTask("+json[i].taskId+",'assignedSprint');\">";  
				 }else{
					taskUI+="<div id=\"task_"+json[i].taskId+"\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"myStroyTasks("+json[i].epicId+","+json[i].taskId+");\">"; 
				 }*/
			
				 var taskStartDate=json[i].startDate;
			     var taskEndDate=json[i].endDate;
				 var taskstarttime=json[i].startTime;
			     var taskendtime=json[i].endTime;
				
				 if(taskStartDate == "00-00-0000"){
			    	taskStartDate = "-";
				 }else{
					taskStartDate = taskStartDate.replaceAll("-", " ");
				 }
				 if(taskEndDate == "00-00-0000"){
				   	taskEndDate = "-";
				 }else{
				   	taskEndDate = taskEndDate.replaceAll("-", " ");
				 }	
			
				 var userTaskStatus = json[i].userTaskStatus;
				 var statusImg="";
				 var userStatus="";
				 if(userTaskStatus=="Created"){
					userStatus="Created";
					statusImg = path+"/images/task_created.svg";
				 }else if(userTaskStatus=="Inprogress"){
						userStatus="Inprogress";
						statusImg = path+"/images/task_inprogress.svg";
				 }else if(userTaskStatus=="Paused"){
						userStatus="Paused";
						statusImg = path+"/images/task_paused.svg";
				 }else if(userTaskStatus=="Completed"){
						userStatus="Completed";
						statusImg = path+"/images/task_completed.svg";
				 }else{
						statusImg = path+"/images/task_nostatus.svg";
				 }
				 taskUI+="<div id=\"task_"+json[i].taskId+"\" style=\"width:100%;float:left;\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"newUicreateTaskUI('update',"+json[i].taskId+",'"+json[i].taskListType+"',"+json[i].epicId+",'"+json[i].type+"','','','','SBsprintTask','','SB');\">";  	//viewTask("+json[i].taskId+",'"+json[i].taskListType+"','"+json[i].epicId+"','"+json[i].type+"');	//newUicreateTaskUI('update',"+json[i].taskId+",'"+json[i].taskListType+"',"+json[i].epicId+",'"+json[i].type+"','','','','SBsprintTask','','SB');
				 taskUI+="    <div class=\"docListViewBorderCls taskId_"+json[i].taskId+"\" style=\"padding-top: 8px;font-size:12px;margin:0px;height:80px;\">";
				 
				/********************************************************************  First Row columns ********************************************************************/
	 			 taskUI+="<div>";
				 
				 taskUI+=" <div class=\"tabContentHeaderName\" align=\"left\" style=\"width:6%;margin-top: 1px;float: left;padding-left: 10px;\"><img title = \"Sprint Task\" class=\"taskimgClss\" style = \"width: 80%;height: 80%\" src=\""+path+"/images/calender/newsprintimg.png\" /></div>";
				if(userStatus != ""){
					if(json[i].type == "Present"){
						taskUI+=" <div  class=\"tabContentHeaderName\" align=\"left\" style=\"margin-top: 3px;width:6%;padding-left: 15px;\"> <img title=\"\" id=\"taskPlay_"+json[i].taskId+"\" onclick=\"changeStatusOfUserTask("+json[i].taskId+","+json[i].project_id+",'listView');event.stopPropagation();\" src=\""+statusImg+"\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 15px;margin-top: 1px;\" class=\"img-responsive\"></div>"; //+json[i].project_id+
					}else{
						if(userStatus != "Completed"){
							taskUI+=" <div  class=\"tabContentHeaderName\" align=\"left\" style=\"margin-top: 3px;width:6%;padding-left: 15px;\"> <img title=\"\" id=\"taskPlay_"+json[i].taskId+"\"  src=\""+path+"/images/task_created.svg\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 15px;margin-top: 1px;\" class=\"img-responsive\"></div>"; //+json[i].project_id+
						}else{
							taskUI+=" <div  class=\"tabContentHeaderName\" align=\"left\" style=\"margin-top: 3px;width:6%;padding-left: 15px;\"> <img title=\"\" id=\"taskPlay_"+json[i].taskId+"\"  src=\""+path+"/images/task_completed.svg\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 15px;margin-top: 1px;\" class=\"img-responsive\"></div>"; //+json[i].project_id+
						}
					}
				 }else{
					if(json[i].type == "Present"){
						taskUI+=" <div  class=\"tabContentHeaderName\" align=\"left\" style=\"margin-top: 3px;width:6%;padding-left: 15px;\"> <img title=\"\" id=\"\"  src=\""+statusImg+"\" style = \"width: 20px;margin-top: 1px;\" class=\"img-responsive\"></div>";
				 	}else{
						if(userStatus != "Completed"){
							taskUI+=" <div  class=\"tabContentHeaderName\" align=\"left\" style=\"margin-top: 3px;width:6%;padding-left: 15px;\"> <img title=\"\" id=\"taskPlay_"+json[i].taskId+"\"  src=\""+path+"/images/task_created.svg\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 15px;margin-top: 1px;\" class=\"img-responsive\"></div>"; //+json[i].project_id+
						}else{
							taskUI+=" <div  class=\"tabContentHeaderName\" align=\"left\" style=\"margin-top: 3px;width:6%;padding-left: 15px;\"> <img title=\"\" id=\"taskPlay_"+json[i].taskId+"\"  src=\""+path+"/images/task_completed.svg\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 20px;margin-top: 0px;\" class=\"img-responsive\"></div>"; //+json[i].project_id+
						}
					}
				 }
			
				 taskUI+="  <div class=\"tabContentHeaderName defaultExceedCls\" title=\""+json[i].taskId+"\" style=\"width:6%;margin-top: 4px;float: left;padding-left: 1px;font-size: 14px;\">"+json[i].taskId+"</div>";
				 
				 taskUI+="  <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:66%;margin-top: 3px;float:left;font-size: 14px;\" title=\""+json[i].taskSubject+"\">"+TextLink(json[i].taskSubject)+"</div>";
		         
				 taskUI+="  <div class=\"tabContentHeaderName\" align=\"left\" valign=\"middle\" style=\"float:left;margin-top:6px;width:5%;padding-left: 10px;\"><img title=\""+json[i].userCompletedStatus+"\" style=\"width:20px;\" src=\""+json[i].taskStatusImage+"\" /></div>";
				 
				 taskUI+="  <div align=\"center\" style=\"width:5%;padding-top: 2px;padding-left:0px;padding-right:0px;\"  class=\"col-sm-3 col-xs-3 tabContentHeaderName defaultExceedCls\">";
						
			 			if(json[i].trending >=-1 && json[i].trending <=-0.49){
			        		sentimentBgCol='images/tasksNewUI/meterred.png';
			        		
			        	}else if(json[i].trending >=-0.50 && json[i].trending <0){
			        		sentimentBgCol='images/tasksNewUI/meteryellow.png';
			        		
			        	}else if(json[i].trending == 0 || json[i].trending == 0.0){
			        		sentimentBgCol='images/tasksNewUI/meteryellow_center.png';
			        		
					    }else if((json[i].trending >0 || json[i].trending > 0.0)&& json[i].trending <=0.49){
			        		sentimentBgCol='images/tasksNewUI/meteryellow_Lightgreen.png';
			        		
			        	}else if(json[i].trending >=0.5 && json[i].trending <=1){
			        		sentimentBgCol='images/tasksNewUI/meter_fullgreen.png';
			        		
			        	}else{
			        		json[i].trending = 'No Data';
			        		sentimentBgCol='images/tasksNewUI/meterDefault.png';  
			        		
			        	}
			 	 taskUI += "	<img title=\""+json[i].trending+"\"  src=\""+path+"/"+sentimentBgCol+"\" style=\"width:30px;height:30px;\"  >";
			        	//taskUI += ' <div  title="'+trending+'" style="width: 20px;height: 5px;background-color:'+sentimentBgCol+'"></div>'
			 	 taskUI += "</div>";
		
				 taskUI+="<div  class=\"rTasksPriority\" align=\"middle\" valign=\"middle\" style=\" width: 6%;float: left;margin-top: 6px;\"><img title=\""+json[i].priorityTitle+"\" src=\""+path+""+json[i].Priority_image+"\" ></div>";
			
				 
				 taskUI+="</div>";
				 
				 /********************************************************************  First Row columns end********************************************************************/
				/********************************************************************  second Row columns********************************************************************/
				 taskUI+="<div>";
			
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" title=\"\" style=\"width: 7%;color: darkgrey;font-size: 14px;padding-top: 6px;\">Created</div>";
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" style=\"float:left;width:5%;margin-left: -3px;padding-top: 2px;\">";
	         		var createdByImg = json[i].createdByDetail;
			 		if(createdByImg==""){
				    		taskUI+="<div style=\"float:left;margin-left: 12.5%; padding-top: 1.8%;\">-</div>";
				    }else{
					   	for(var j=0; j<createdByImg.length; j++){
						    userImg = lighttpdPath+"/userimages/"+createdByImg[j].userId+"."+createdByImg[j].userImgExtn+'?'+createdByImg[j].imgTimeStamp;
						    taskUI+='<img class="img-responsive" style="float:left;margin-right:5px;width:25px;height:25px;border-radius:50%;" src="'+userImg+'" title="'+createdByImg[j].userName+'" onerror="javascript:userImageOnErrorReplace(this);">';
						}
				    }
		 		 taskUI+="</div>";
			
				 taskUI+="<div style=\"width: 6%;float: left;color:darkgrey;font-size: 14px;text-align: right;padding-top: 6px;\"><span style=\"\">Assigned</span></div>";
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" style=\"float:left;width:30%;margin-top: 1px;padding-left: 11px;\">";
		         assignedTo = json[i].AssignedUsersName;
			         for(var j=0; j<assignedTo.length; j++){
			             userImg = lighttpdPath+"/userimages/"+assignedTo[j].userId+"."+assignedTo[j].userImgExtn+'?'+assignedTo[j].imgTimeStamp;
			             taskUI+="<img class=\"img-responsive\" style=\"float:left;margin-right:5px;width:25px;height:25px;border-radius:50%;\" src=\""+userImg+"\" title=\""+assignedTo[j].userName+"\" onerror=\"javascript:userImageOnErrorReplace(this);\">";
					 }
		         taskUI+="</div>";
			
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:10%;float:left;padding-top: 6px;color: darkgrey;font-size: 14px\" title=\""+taskStartDate+" "+taskstarttime+"\"><div>"+taskStartDate+"</div></div>";
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:10%;float:left;padding-top: 6px;color: darkgrey;font-size: 14px\" title=\""+taskEndDate+" "+taskendtime+"\"><div>"+taskEndDate+"</div></div>";
				
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:8%;float:left;padding-top: 6px;padding-left: 9px;color: darkgrey;font-size: 14px\"><div>"+json[i].estimatedHour+" h "+json[i].estimatedMinute+" m</div></div>";
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:8%;float:left;padding-top: 6px;padding-left: 6px;color: darkgrey;font-size: 14px\"><div>"+json[i].actualHour+" h "+json[i].actualMinute+" m</div></div>";
					
				 taskUI+="<div align=\"right\" class=\"cmntsIcon\" style=\"display:none;width: 15%;float: left;padding-right: 29px;padding-top: 6px;\"><img id=\"cIcon_"+json[i].id+"\" style=\"cursor:pointer;width: 20px;display:none;\" title=\""+getValues(companyLabels,"View_comments")+"\" src=\""+path+"/images/commentBlack2.png\" onclick=\"showTaskComments("+json[i].id+","+json[i].project_id+");event.stopPropagation();\"></div>";

				 taskUI+="<input type=\"hidden\" id=\"taskListType_"+json[i].taskId+"\" value=\""+json[i].getViewType+"\" />";	

				 taskUI+="</div>";
			
			
				/********************************************************************  second Row columns end********************************************************************/
				 
				taskUI+="</div>";
				
				/*taskUI+="<div id=\"Taskcomment_"+json[i].taskId+"\" class=\"epicCommonListDivCss\" style=\'padding-top: 20px;padding-right: 20px;width: 100%;margin-left: 0px;padding-left: 20px;float: left;margin-bottom: 0px;\'>"
 	            +"		<div class=\"TaskCommentMainTextareaDiv\" style=\"float:left;width:100%;padding-right: 20px;\">"
 	            +"		</div>"
 	            +"		<div class=\"TaskCommentListDiv\" style=\"float:left;width:100%;border-top: 1px solid #c1c5c8;\">"
 	            +"		</div>"
     	        +"	 </div>"*/
						
				taskUI+="	 <div id=\"TaskStatus_"+json[i].taskId+"\" class=\"epicCommonListDivCss TaskCommentContainer\" onclick=\"event.stopPropagation();\" style=\"padding: 5px 0px 0px;float: left;width: 100%;margin-left: 0px;margin-right: 0px;margin-bottom: 0px;border-radius: 0px;display: none;overflow: hidden;\">"
				+"		<div class=\"TaskStatusMainTextareaDiv\" style=\"float:left;width:100%;border-bottom: 1px solid rgb(193, 197, 200);padding: 0px 5px 5px 5px;\">"
				+"		</div>"
			    +"	 </div>";
				
				taskUI+="</div>";	 
			
			
			
		     }
	     }else{
		
			for(var i=0; i<json.length; i++){
				
				var userTaskStatus = json[i].userTaskStatus;
				var statusImg="";
				var userStatus="";
				 if(userTaskStatus=="Created"){
					userStatus="Created";
					statusImg = path+"/images/task_created.svg";
				 }else if(userTaskStatus=="Inprogress"){
						userStatus="Inprogress";
						statusImg = path+"/images/task_inprogress.svg";
				 }else if(userTaskStatus=="Paused"){
						userStatus="Paused";
						statusImg = path+"/images/task_paused.svg";
				 }else if(userTaskStatus=="Completed"){
						userStatus="Completed";
						statusImg = path+"/images/task_completed.svg";
				 }else{
						statusImg = path+"/images/task_nostatus.svg";
				 }
				
				 var taskStartDate=json[i].startDate;
			     var taskEndDate=json[i].endDate;
				 var taskstarttime=json[i].startTime;
			     var taskendtime=json[i].endTime;
				
				 if(taskStartDate == "00-00-0000"){
			    	taskStartDate = "-";
				 }else{
					taskStartDate = taskStartDate.replaceAll("-", " ");
				 }
				 if(taskEndDate == "00-00-0000"){
				   	taskEndDate = "-";
				 }else{
				   	taskEndDate = taskEndDate.replaceAll("-", " ");
				 }	
				
				taskUI+="<div id=\"task_"+json[i].taskId+"\" style=\"width:100%;float:left;\" class=\"assignedTaskPageClass docListViewRowCls\" onclick=\"newUicreateTaskUI('update',"+json[i].taskId+",'"+json[i].taskListType+"',"+json[i].epicId+",'"+json[i].type+"','','','','detailsprintTask','','Sprint');\">";		//viewTask("+json[i].taskId+",'"+json[i].taskListType+"','"+json[i].epicId+"','"+json[i].type+"');  
				taskUI+="<div class=\"docListViewBorderCls taskId_"+json[i].taskId+"\" style=\"padding-top: 8px;font-size:12px;margin:0px;height:80px;\">";
				 
				
				
				/********************************************************************  First Row columns ********************************************************************/
	 
				 taskUI+="<div>";
				 
				 taskUI+="<div class=\"tabContentHeaderName\" align=\"center\" style=\"width:5%;margin-top: 5px;float: left;\"><div style=\"float:left;margin-left:8px;\"><img title = \"Sprint Task\" class=\"taskimgClss\" src=\""+path+"/images/calender/newsprintimg.png\" /></div></div>";
				 
				 if(userStatus != ""){
					taskUI+=" <div  class=\"tabContentHeaderName\" align=\"center\" style=\"margin-top: 5px;width:5%;padding-left: 0px;\"> <img title=\"\" id=\"taskPlay_"+json[i].taskId+"\" onclick=\"changeStatusOfUserTask("+json[i].taskId+","+json[i].project_id+",'listView');event.stopPropagation();\" src=\""+statusImg+"\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 15px;margin-top: 1px;\" class=\"img-responsive\"></div>"; //+json[i].project_id+
				 }else{
					taskUI+=" <div  class=\"tabContentHeaderName\" align=\"center\" style=\"margin-top: 4px;width:5%;padding-left: 0px;\"> <img title=\"\" id=\"\"  src=\""+statusImg+"\" style = \"width: 20px;margin-top: 1px;\" class=\"img-responsive\"></div>";
				 }
				 
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\"  title=\""+json[i].taskId+"\" style=\"width:5%;margin-top: 6px;float: left;font-size:14px;\">"+json[i].taskId+"</div>";
				 
				 taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:68%;margin-top: 5px;float:left;font-size:14px;\" title=\""+json[i].taskSubject+"\">"+TextLink(json[i].taskSubject)+"</div>";
		         
				 taskUI+="   <div class=\"tabContentHeaderName\" align=\"center\" valign=\"middle\" style=\"float:left;margin-top:6px;width:7%;\"><img title=\""+json[i].userCompletedStatus+"\" style=\"width: 20px;\" src=\""+json[i].taskStatusImage+"\" /></div>";
				 
				 taskUI+="  <div align=\"left\" style=\"width:5%;padding-top: 2px;padding-left:3px;padding-right:0px;\"  class=\"col-sm-3 col-xs-3 tabContentHeaderName defaultExceedCls\">";
						
			 			if(json[i].trending >=-1 && json[i].trending <=-0.49){
			        		sentimentBgCol='images/tasksNewUI/meterred.png';
			        		
			        	}else if(json[i].trending >=-0.50 && json[i].trending <0){
			        		sentimentBgCol='images/tasksNewUI/meteryellow.png';
			        		
			        	}else if(json[i].trending == 0 || json[i].trending == 0.0){
			        		sentimentBgCol='images/tasksNewUI/meteryellow_center.png';
			        		
					    }else if((json[i].trending >0 || json[i].trending > 0.0)&& json[i].trending <=0.49){
			        		sentimentBgCol='images/tasksNewUI/meteryellow_Lightgreen.png';
			        		
			        	}else if(json[i].trending >=0.5 && json[i].trending <=1){
			        		sentimentBgCol='images/tasksNewUI/meter_fullgreen.png';
			        		
			        	}else{
			        		json[i].trending = 'No Data';
			        		sentimentBgCol='images/tasksNewUI/meterDefault.png';  
			        		
			        	}
			 	 taskUI += "	<img title=\""+json[i].trending+"\"  src=\""+path+"/"+sentimentBgCol+"\" style=\"width:30px;height:30px;\"  >";
			        	//taskUI += ' <div  title="'+trending+'" style="width: 20px;height: 5px;background-color:'+sentimentBgCol+'"></div>'
			 	 taskUI += "</div>";
				 
				 taskUI+="<div  class=\"rTasksPriority\" align=\"left\" valign=\"middle\" style=\" width: 5%;float: left;margin-top: 6px;padding-left:5px;\"><img title=\""+json[i].priorityTitle+"\" src=\""+path+""+json[i].Priority_image+"\" ></div>";
				 
				 taskUI+="</div>";
				 
				 /********************************************************************  First Row columns end********************************************************************/
				/********************************************************************  second Row columns********************************************************************/
				taskUI+="<div>";
				
				taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" title=\"\" style=\"width: 6%;color: darkgrey;font-size: 14px;padding-top: 6px;\">Created</div>";
				taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" style=\"float:left;width:4%;margin-left: -5px;padding-top: 2px;\">";
	         		var createdByImg = json[i].createdByDetail;
			 		if(createdByImg==""){
				    		taskUI+="<div style=\"float:left;margin-left: 12.5%; padding-top: 1.8%;\">-</div>";
				    }else{
					   	for(var j=0; j<createdByImg.length; j++){
						    userImg = lighttpdPath+"/userimages/"+createdByImg[j].userId+"."+createdByImg[j].userImgExtn+'?'+createdByImg[j].imgTimeStamp;
						    taskUI+='<img class="img-responsive" style="float:left;margin-right:5px;width:25px;height:25px;border-radius:50%;" src="'+userImg+'" title="'+createdByImg[j].userName+'" onerror="javascript:userImageOnErrorReplace(this);">';
						}
				    }
		 		taskUI+="</div>";
			
				taskUI+="<div style=\"width: 5%;float: left;color:darkgrey;font-size: 14px;text-align: right;padding-top: 6px;\"><span style=\"\">Assigned</span></div>";
				taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" style=\"float:left;width:34%;margin-top: 1px;padding-left: 11px;\">";
		        assignedTo = json[i].AssignedUsersName;
			         for(var j=0; j<assignedTo.length; j++){
			             userImg = lighttpdPath+"/userimages/"+assignedTo[j].userId+"."+assignedTo[j].userImgExtn+'?'+assignedTo[j].imgTimeStamp;
			             taskUI+="<img class=\"img-responsive\" style=\"float:left;margin-right:5px;width:25px;height:25px;border-radius:50%;\" src=\""+userImg+"\" title=\""+assignedTo[j].userName+"\" onerror=\"javascript:userImageOnErrorReplace(this);\">";
					 }
		        taskUI+="</div>";

				taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:10%;float:left;padding-top: 6px;color: darkgrey;font-size: 14px\" title=\""+taskStartDate+" "+taskstarttime+"\"><div>"+taskStartDate+"</div></div>";//
				taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:10%;float:left;padding-top: 6px;color: darkgrey;font-size: 14px\" title=\""+taskEndDate+" "+taskendtime+"\"><div>"+taskEndDate+"</div></div>";//
				
				taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:8%;float:left;padding-top: 6px;padding-left: 0px;color: darkgrey;font-size: 14px\"><div>"+json[i].estimated_hour+" h "+json[i].estimated_minute+" m</div></div>";
				taskUI+="<div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\" style=\"width:8%;float:left;padding-top: 6px;padding-left: 0px;color: darkgrey;font-size: 14px\"><div>"+json[i].totalActualhour+" h "+json[i].totalActualminute+" m</div></div>";
					
				taskUI+="<div align=\"right\" class=\"cmntsIcon\" style=\"display:none;width: 15%;float: left;padding-right: 29px;padding-top: 6px;\"><img id=\"cIcon_"+json[i].id+"\" style=\"cursor:pointer;width: 20px;display:none;\" title=\""+getValues(companyLabels,"View_comments")+"\" src=\""+path+"/images/commentBlack2.png\" onclick=\"showTaskComments("+json[i].id+","+json[i].project_id+");event.stopPropagation();\"></div>";
 
				taskUI+="<input type=\"hidden\" id=\"taskListType_"+json[i].taskId+"\" value=\""+json[i].getViewType+"\" />";	
			
				taskUI+="</div>";
				/********************************************************************  second Row columns end********************************************************************/

				 
				 
				taskUI+="</div>";
				
				
				taskUI+="<div id=\"Taskcomment_"+json[i].taskId+"\" class=\"epicCommonListDivCss\" style=\'padding-top: 20px;padding-right: 20px;width: 100%;margin-left: 0px;padding-left: 20px;float: left;margin-bottom: 0px;\'>"
 	            +"		<div class=\"TaskCommentMainTextareaDiv\" style=\"float:left;width:100%;padding-right: 20px;\">"
 	            +"		</div>"
 	            +"		<div class=\"TaskCommentListDiv\" style=\"float:left;width:100%;border-top: 1px solid #c1c5c8;\">"
 	            +"		</div>"
     	        +"	 </div>"
						
				+"	 <div id=\"TaskStatus_"+json[i].taskId+"\" class=\"epicCommonListDivCss TaskCommentContainer\" onclick=\"event.stopPropagation();\" style=\"padding: 5px 0px 0px;float: left;width: 100%;margin-left: 0px;margin-right: 0px;margin-bottom: 0px;border-radius: 0px;display: none;overflow: hidden;\">"
				+"		<div class=\"TaskStatusMainTextareaDiv\" style=\"float:left;width:100%;border-bottom: 1px solid rgb(193, 197, 200);padding: 0px 5px 5px 5px;\">"
				+"		</div>"
			    +"	 </div>";
				
				
				taskUI+="</div>";




			}
	     
	     	
	      }
	    }else{
	    	taskUI = "<span>No tasks found.</span>"
	    }
	   
	   return taskUI;
 }

 //------------------------------------------------ stage related functions ----------------------------------->
 
 function prepareStageUI(result){
    var agileUI = '';
   if(result){
		var json = eval(result);
		for(var i = 0; i < json.length; i++){
		   agileUI+="<div id=\"stageMainDiv_"+json[i].stageId+"\" class=\"col-lg-12 defaultPaddingCls \" >"
		           +"  <div class='docListViewBorderCls actFeedHover'>" 
		           +"    <div class='col-sm-10 col-xs-10 tabContentHeaderName' id='stageName_"+json[i].stageId+"' style='padding-top:15px;' ondblclick=\"editStage("+json[i].stageId+");\">"+replaceSpecialCharacter(json[i].stageName)+"</div>" 
		           +"    <div class='col-sm-2 col-xs-2 tabContentHeaderName actFeedHoverImgDiv stageMoreImgDiv' style='padding-top:15px;' ><img align = \"right\" id=\"stageOptionDiv_"+json[i].stageId+"\" class = \"img-responsive\" style=\"cursor: pointer; margin-right: 10px;\" src = \""+path+"/images/more.png\" onclick=\"openStageOptionPopup(this);\"></div>" 
		           +"    <div class=\"stageOptionsDiv\" id=\"stageOptionPopDiv_"+json[i].stageId+"\" style=\"right: 40px; width: 130px;\">"
		           +"      <div style=\"margin-top: 10px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\"><img src=\""+path+"/images/arrow.png\"></div>"
	   			   +"      <div onclick=\"editStage("+json[i].stageId+");\" class=\"optionMenuCss \"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" >"
			       +"         <span  class=\"optionSpanHover agileSpanCss Edit_cLabelText\"></span></div>"
			       +"      <div onclick=\"showStageReorder("+json[i].stageId+");\" class=\"optionMenuCss iReorder\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" >"
			       +"         <span  class=\"optionSpanHover agileSpanCss Agile_Reorder_cLabelText\"></span></div>"
			       +"      <div  class=\"optionMenuCss \" onclick=\"deleteStage('"+json[i].stageAssigned+"','"+json[i].stageId+"');\"  style=\"cursor: pointer; border-bottom: 1px solid rgb(204, 204, 204); height: auto; padding: 4px 0 6px;\" >"
		           +"        <span  class=\"optionSpanHover agileSpanCss Delete_cLabelText\"></span></div>"
		           +"    </div> "
		           +"    <div id='stageEditDiv_"+json[i].stageId+"' class=\"col-sm-12 col-xs-12 defaultPaddingCls\" style=\"margin: 10px 0px;display:none;\">"
             	   +"      <textarea class=\"main_epicTxtArea stageUpdTxtArea\" style=\"border-width: 1px 0px 1px 1px; width: 96.5%; border-radius: 3px 0px 0px 3px;\"></textarea>"
             	   +"      <div align=\"center\" class=\"main_epicTxtArea_btnDiv\">"
		           +"         <div><img alt=\"Image\" src=\""+path+"/images/workspace/post.png\" onclick=\"updateStage("+json[i].stageId+");\" title=\"post\" class=\"img-responsive epicSaveImg stageUpdImg\"></div>"
		           +"      </div>"
                   +"    </div>"
		           +"  </div>" 
		           +"</div>";
		   
		}
   }
   return agileUI;
 } 
 
    
 function createNewStage(){	
        var type = $('#stageType').val();
        var stageName = $("#stageTextarea").val().trim(); 
		var groupId = $('#stageGroupId').val(); 
		var projId = $('#stageProjId').val(); 
		if(stageName==''|| stageName==null){
				if(type=='SG'){
				  alertFun(getValues(companyAlerts,"Alert_StageNoEmt"),'warning');
				}else if(type=='E'){
				  alertFun(getValues(companyAlerts,"Alert_ValNoEmt"),'warning');
				}else if(type=='F'){
				  alertFun(getValues(companyAlerts,"Alert_FuncNoEmt"),'warning');
				}
				$("#stageTextarea").focus();
				return false;		
		}
		$("#loadingBar").show();
		timerControl("start");
		 $.ajax({
			   	url: path+"/agileActionNew.do",
				type:"POST",
				data:{action:"insertStage",stageName:stageName, groupId:groupId, type:type, projId:projId },
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
		          	checkSessionTimeOut(result);
		            //if(!isiPad){
	                //  $('#stageContainer .mCSB_container').html(result);
					//  $("#stageContainer").mCustomScrollbar("update");
					//}else{
					  result = jQuery.parseJSON(result);
            		  $('#stageContainer').html(prepareStageUI(result));
					//}
					loadAgileCustomLabel('Stages_'+type);
					//confirmClearCreateStage();
					$('#stageTextarea').val('');
					$("#loadingBar").hide();
					timerControl("");
				}
			});   
 }
 
  
  function openStageOptionPopup(obj){
     var idd=$(obj).attr('id').split('_')[1];
     //var t=$(obj).offset().top;
	 //var oh=$(window).height()-t;
	 //var h=$('#stageOptionPopDiv_'+idd).height();
     $('#stageOptionPopDiv_'+idd).css({'margin-top':'0px','margin-right':'1%'});
     $('#stageOptionPopDiv_'+idd+' #arrowDiv_'+idd).css('margin-top','0px');
     //if(oh<h){
     //    var fh=h-oh+10;
	 //    var fh1=h-oh+8;
	 //    $('#stageOptionPopDiv_'+idd).css('margin-top','-'+fh+'px');
	 //    $('#stageOptionPopDiv_'+idd+' #arrowDiv_'+idd).css('margin-top',fh1+'px');
	 //} 
	 
	 if ($('#stageOptionPopDiv_'+idd).is(":hidden")) {
	    $('.stageOptionsDiv').hide();
	    $('#stageOptionPopDiv_'+idd).slideDown('slow',function(){
	       $(this).css('overflow','');
	       $(this).children('div:last').css('border-bottom','none');
	    });
	 }else{
	    $('#stageOptionPopDiv_'+idd).slideUp('slow');
	 }  
  }
  
  function editStage(stageId){
     $('#stageMainDiv_'+stageId).find('.actFeedHover').children('div').hide();
     $('#stageMainDiv_'+stageId).find('.actFeedHover').css('height','70px');
     $('#stageEditDiv_'+stageId).show();
     $('#stageEditDiv_'+stageId).find('textarea').val($('#stageName_'+stageId).html()).focus();
  }
 
   function updateStage(stageId){	
        var type = $('#stageType').val();
        var stageName = $('#stageEditDiv_'+stageId).find('textarea').val().trim(); 
		
		if(stageName==''|| stageName==null){
				if(type=='SG'){
				  alertFun(getValues(companyAlerts,"Alert_StageNoEmt"),'warning');
				}else if(type=='E'){
				  alertFun(getValues(companyAlerts,"Alert_ValNoEmt"),'warning');
				}else if(type=='F'){
				  alertFun(getValues(companyAlerts,"Alert_FuncNoEmt"),'warning');
				}
				$('#stageEditDiv_'+stageId).find('textarea').focus();
				return false;		
		}
		$("#loadingBar").show();
		timerControl("start");
		 $.ajax({
			   	url: path+"/agileActionNew.do",
				type:"POST",
				data:{action:"updateStage",stageName:stageName, stageId:stageId, type:type},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
		            checkSessionTimeOut(result);
		            $('#stageName_'+stageId).html(stageName);
	                closeUpdateStage(stageId);
	                $("#loadingBar").hide();
					timerControl("");
				}
			});   
  }
  
  function closeUpdateStage(stageId){
     $('#stageMainDiv_'+stageId).find('.actFeedHover').children('div').not('div.stageOptionsDiv,div.ideaReorderDivs').show();
     $('#stageMainDiv_'+stageId).find('.actFeedHover').css('height','');
     $('#stageEditDiv_'+stageId).hide();
     $('#stageEditDiv_'+stageId).find('textarea').val('');
 }
 
 function showStageReorder(stageId){
      $('#stageOptionPopDiv_'+stageId).slideUp('slow');
      
	  if($('#stageMainDiv_'+stageId).find('div.ideaReorderDivs').length < 1){
          var storyLi="  <div id=\"stageReorderDiv_"+stageId+"\" class=\"ideaReorderDivs\" style=\"width:45px;border-radius:50px;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.6);border:1px solid #A1A1A1;position:absolute;display:none;height:45px;background-color:#ffffff;top:2px;right:50%;padding:5px;z-index:1;\" >";
			  storyLi+="    <div style=\"height:10px;width:10px;margin:0px auto;\"><img src=\""+path+"/images/idea/arrow-top.png\" onclick=\"reorderStage('top','"+stageId+"');\" style=\"cursor:pointer;height:10px;width:10px;float:left;\"/></div>";
			  storyLi+="    <div style=\"height:12px;\"><img src=\""+path+"/images/idea/arrow-left.png\" onclick=\"\" style=\"opacity:0.3;cursor:pointer;float:left;height:10px;width:10px;margin-top:2px;\"/><img src=\""+path+"/images/idea/arrow-right.png\" onclick=\"\" style=\"opacity:0.3;cursor:pointer;float:right;height:10px;width:10px;margin-top:2px;\"/></div>";
			  storyLi+="    <div style=\"height:10px;width:10px;margin:0px auto;\"><img src=\""+path+"/images/idea/arrow-down.png\" onclick=\"reorderStage('down','"+stageId+"');\" style='cursor:pointer;height:10px;width:10px;float:left;'/></div>";
			  storyLi+=" </div>";
			 
			$('#stageMainDiv_'+stageId).find('.actFeedHover').append(storyLi);
      }
      
      if ($('#stageReorderDiv_'+stageId).is(":hidden")) {
         $(".ideaReorderDivs").hide();
         $('#stageReorderDiv_'+stageId).show();
      }else{
         $('#stageReorderDiv_'+stageId).hide();
      }
 } 
 
 function reorderStage(position,stageId){
      var nextStageId="";
      if(position=="top"){
	      if($('#stageMainDiv_'+stageId).prevAll('div:visible').length){
			    nextStageId=$('#stageMainDiv_'+stageId).prevAll('div:visible').attr('id').trim().split('_')[1];
			    if(nextStageId!='undefined'){
				      var liData=$('div#stageMainDiv_'+stageId).clone();
				      $('div#stageMainDiv_'+stageId).remove();
				      $(liData).insertBefore('div#stageMainDiv_'+nextStageId);
				      updateStageOrder(stageId,nextStageId);
				}
			}else{
		      alertFun(getValues(companyAlerts,"Alert_reorderNotPermissible"),'warning');
		    }
		    
	  }else if(position=="down"){
	      if($('#stageMainDiv_'+stageId).nextAll('div:visible').length){
		       nextStageId=$('#stageMainDiv_'+stageId).nextAll('div:visible').attr('id').trim().split('_')[1];
			   if(nextStageId!='undefined'){
				      var liData=$('div#stageMainDiv_'+stageId).clone();
				      $('div#stageMainDiv_'+stageId).remove();
				      $(liData).insertAfter('div#stageMainDiv_'+nextStageId);
				      updateStageOrder(stageId,nextStageId);
			   }
		   }else{
		      alertFun(getValues(companyAlerts,"Alert_reorderNotPermissible"),'warning');
		    }
	 }
  
 }
 function updateStageOrder(currentId,targetId){
     $("#loadingBar").show();
	 timerControl("start");
	 var type = $('#stageType').val();
	 $.ajax({
			url: path+"/agileActionNew.do",
			type:"POST",
			data:{action:"updateStageOrder", currentId:currentId, targetId:targetId, type:type  },
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			success:function(result){
			   checkSessionTimeOut(result);
			   $("#loadingBar").hide();
			   timerControl("");
			}
	});  
 }
 
    var confStageId="";
  function deleteStage(stageAssigned,stageId){
     if(stageAssigned!=''){
     	alertFun(getValues(companyAlerts,"Alert_StageDelete"),'warning');
     }else{
     	confStageId = stageId;
     	confirmFun(getValues(companyAlerts,"Alert_delete"),"delete","deleteStageConfirm");
     }
  }
   function deleteStageConfirm(){	
        var stageId=confStageId; 
		$("#loadingBar").show();
		timerControl("start");
		var type = $('#stageType').val();
		 $.ajax({
			   	url: path+"/agileActionNew.do",
				type:"POST",
				data:{action:"deleteStage", stageId:stageId, type:type},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
		            checkSessionTimeOut(result);
		            $('#stageMainDiv_'+stageId).remove();
		            $("#loadingBar").hide();
					timerControl("");
				}
			});   
  }
  
  //--------------------------------------------------------------- scrumboard related functions ----------------------------------->
  
 function removePrevNext(){
    $('#SB_table tr.SB_sprintStories').each(function(){
       $(this).find('.removePrevNext').find('.sb_nextStage,.sb_prevStage').hide();
       $(this).find('.removePrev').find('.sb_prevStage').hide();
       $(this).find('.removeNext,.removeCompleted').find('.sb_nextStage').hide();
    });
    //var projUserStatus = projectUsersStatus;
    //if(projUserStatus!="PO"){
    //  $('#SB_table tr.SB_sprintStories').find('.sb_userListDiv img').removeAttr('onclick');
    //}
    $('td.SB_data').find('.SB_dragDiv').css('cursor','move');
    $('div.SB_history_story , td.removePrevNext').find('.SB_dragDiv').css('cursor','default');
 }
  
 var cansort;
 var DragNdrop = false;
 function initDragAndDrop(){
    var Startids="";
	 $(".SB_sprintStories .SB_data").sortable({
		 connectWith: '.SB_sprintStories .SB_data',
	     cancel:".removePrevNext,.SB_history_story,.dragDecline",
	     handle: '.SB_dragDiv',
	     opacity: 0.5,
	     placeholder:'.SB_sprintStories .SB_data',
	     start: function( event, ui ) {
	            var status=$(this).attr('status');
	          	var cls=$(this).attr('class');
	            Startids=$(this).parent().attr('id');
	     },
	     receive: function(ev, ui) {
	            cansort = ui;
	            DragNdrop = true;
	            var cls = ui.item.parent().attr('class');
	            var Stopids = ui.item.parent().parent().attr('id');
	            confirmStoryId = ui.item.attr('id').split('_')[2];
	            $('div.SB_feature,div.SB_epic,div.SB_story').css('border','none');//---added for highlight
	   			ui.item.css('border','2px solid #000');//---added for highlight
	   
				
				if(Stopids!=Startids || $('.dragDecline').hasClass(cls)){
	                revertDrag();
	             }else{
	                 confirmStageId = ui.item.parent().attr('id').split('_')[1];
				     confirmType = ui.item.attr('type');
		             if($('.removePrevNext').hasClass(cls)){ 
						  //confirmAction="Backlog";
						  //confirmReset(getValues(companyAlerts,"Alert_StageBacklog"),"clear","confirmStoryMove","revertDrag");
						  alertFun(getValues(companyAlerts,"Alert_StageBacklogMsg"),'warning');
						  revertDrag();
					 } else if($('.removeCompleted').hasClass(cls)){ 
						  confirmAction  = "Completed";
						  confirmStoryMove();
					 }else if($('.SB_data').hasClass(cls)){
		               confirmAction="Other";
			           confirmStoryMove();
		             }
	            } 
	         //}   
       },
	   stop: function(event, ui){
	          $('#SB_story_'+confirmStoryId).find('img.sb_nextStage , img.sb_prevStage').show();
		      removePrevNext();
		      setHeightToTR();
	         //var status=ui.item.parent().attr('status');
	         //var cls=ui.item.parent().attr('class');
	   }
      
       
	 });
 }
 
 function revertDrag(){
    $(cansort.sender).sortable("cancel");
    $('#SB_story_'+confirmStoryId).find('img.sb_nextStage , img.sb_prevStage').show();
	removePrevNext();
 }  
  
  function findStoryCount(){
    $('#SB_table tr[class^="sprintHr_"]').each(function(){
        var sprintId = $(this).attr('class').split('_')[1] ;
        for(var i=0;i<$(this).children('th').length;i++){
           var storyCount = $('tr#sprintTr_'+sprintId+' td:eq('+i+')').children('div').length;
           $(this).children('th:eq('+i+')').find('span.sbCountSpan').text(storyCount);
        }
    });
  
 }
 
  function setHeightToTR(){
   $('#SB_table').find('tr.SB_sprintStories').css('height','');
   $('#SB_table').find('tr.SB_sprintStories').each(function(){
      var h = $(this).height();
      $(this).height(h);
   });
 }
 
    function showEpicFeaturesOnclick(){
        $('table#SB_table').find('div.SB_epic').find('p[id^=sbStory_]').click(function(){
		   var epicId = $(this).attr('id').split('_')[1];
		   $('div.SB_feature,div.SB_epic,div.SB_story').css('border','none');
		   $(this).parents('div.SB_epic').css('border','2px solid #000');
		   $('div.SB_story[eId='+epicId+']').css('border','2px solid #000');
		   $('div.SB_feature[pId='+epicId+']').each(function(){
		      $(this).css('border','2px solid #000');
		      var featureId = $(this).attr('id').split('_')[2];
		      $('div.SB_story[fId='+featureId+']').css('border','2px solid #000');
		   });
		});
	    
	    $('table#SB_table').find('div.SB_feature').find('p[id^=sbStory_]').click(function(){
		   var featureId = $(this).attr('id').split('_')[1];
		   $('div.SB_feature,div.SB_epic,div.SB_story').css('border','none');
		   $(this).parents('div.SB_feature').css('border','2px solid #000');
		   var epicId = $(this).parents('div.SB_feature').attr('pId');
		   $('div.SB_story[fId='+featureId+']').css('border','2px solid #000');
		   $('div#SB_story_'+epicId).css('border','2px solid #000');
	    });
	    
	    $('table#SB_table').find('div.SB_story').find('p[id^=sbStory_]').click(function(){
		   var storyId = $(this).attr('id').split('_')[1];
		   $('div.SB_feature,div.SB_epic,div.SB_story').css('border','none');
		   $(this).parents('div.SB_story').css('border','2px solid #000');
		   var epicId = $(this).parents('div.SB_story').attr('eId');
		   var featureId = $(this).parents('div.SB_story').attr('fId');
		   $('div#SB_story_'+epicId).css('border','2px solid #000');
		   $('div#SB_story_'+featureId).css('border','2px solid #000');
	    });
	    
	    $('table#SB_table').find('div.SB_epic').find('img.SB_cmtIcon, div.sb_userListDiv, img.sb_nextStage, img.sb_prevStage, img.storyStatusPopIcon ').on("click", function(){
	       $('div.SB_feature,div.SB_epic,div.SB_story').css('border','none');
		   $(this).parents('div.SB_epic').css('border','2px solid #000');
		});
	    
	    $('table#SB_table').find('div.SB_feature').find('img.SB_cmtIcon, div.sb_userListDiv, img.sb_nextStage, img.sb_prevStage, img.storyStatusPopIcon ').on("click", function(){
	       $('div.SB_feature,div.SB_epic,div.SB_story').css('border','none');
		   $(this).parents('div.SB_feature').css('border','2px solid #000');
		});
	    
	    $('table#SB_table').find('div.SB_story').find('img.SB_cmtIcon, div.sb_userListDiv, img.sb_nextStage, img.sb_prevStage, img.storyStatusPopIcon ').on("click", function(){
	       $('div.SB_feature,div.SB_epic,div.SB_story').css('border','none');
		   $(this).parents('div.SB_story').css('border','2px solid #000');
		});
	    
	    
   }
 
 
 function popXYScroll(scrollDivId){
			$("div#"+scrollDivId).mCustomScrollbar({
				axis:"yx",
				scrollButtons:{
					enable:true
				},
				scrollbarPosition:"outside"
			});
				
			$('div#'+scrollDivId+' >.mCS-light.mCSB_scrollTools_horizontal').css('bottom','-4px');
			$('div#'+scrollDivId+' >.mCS-light.mCSB_scrollTools_vertical').css('margin-right','12px');
	        $('div#'+scrollDivId+' >.mCS-light >.mCSB_container').css('margin-right','12px');
			//$('div#'+scrollDivId).find('.mCSB_scrollTools').css('margin-right','5px');
			$('div#'+scrollDivId).find('.mCSB_scrollTools').find('.mCSB_buttonUp').css({'background-position':'-80px 0px','opacity':'0.6'});
			$('div#'+scrollDivId).find('.mCSB_scrollTools').find('.mCSB_buttonDown').css({'background-position':'-80px -20px','opacity':'0.6'});
			$('div#'+scrollDivId).find('.mCSB_scrollTools').find('.mCSB_buttonLeft').css({'background-position':'-82px -40px','opacity':'0.6'});
			$('div#'+scrollDivId).find('.mCSB_scrollTools').find('.mCSB_buttonRight').css({'background-position':'-78px -56px','opacity':'0.6'});
			$('div#'+scrollDivId).find('.mCSB_scrollTools').find('.mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
			$('div#'+scrollDivId).find('.mCSB_scrollTools').find('.mCSB_dragger').find('.mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
		    $('div#'+scrollDivId).find('.mCSB_scrollTools').find('.mCSB_dragger:hover').find('.mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
            $('div#'+scrollDivId+' >.mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
            //----------Dont remove below lines added for Ravi's machine------------------>
            $('a.mCSB_buttonDown').removeClass("hoverZoomLink");
         	$('a.mCSB_buttonRight').removeClass("hoverZoomLink");
         	$('a.mCSB_buttonLeft').removeClass("hoverZoomLink");
         	$('a.mCSB_buttonUp').removeClass("hoverZoomLink");
			
 }
 
  
 function popScroll(scrollDivId){
            $("div#"+scrollDivId).mCustomScrollbar('destroy');
			$("div#"+scrollDivId).mCustomScrollbar({
					scrollButtons:{
						enable:true
					}
					
				});
				
		        $('div#'+scrollDivId+' >.mCS-light >.mCSB_container').css('margin-right','15px');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools').css('margin-right','5px');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_buttonUp').css({'background-position':'-80px 0px','opacity':'0.6'});
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_buttonDown').css({'background-position':'-80px -20px','opacity':'0.6'});
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
				$('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
			    $('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
	            $('div#'+scrollDivId+' >.mCS-light >.mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
				
	}
 
  function moveStory(obj,action,storyId){
	   var stageId="";
	   var cloneToObj;
	   DragNdrop = false;
	   $('div.SB_feature,div.SB_epic,div.SB_story').css('border','none');//---added for highlight
	   $(obj).parents('div.SB_story').css('border','2px solid #000');//---added for highlight
	   if(action=='prevStage'){
	     stageId=$(obj).parents('td.SB_data').prev('td').attr('stage_id');
	     cloneToObj=$(obj).parents('td.SB_data').prev('td');
	   }else{
	     stageId=$(obj).parents('td.SB_data').next('td').attr('stage_id');
	     cloneToObj=$(obj).parents('td.SB_data').next('td');
	   }
	   confirmStoryId=storyId;
	   confirmStageId=stageId;
	   confirmCloneObj=cloneToObj;
	   confirmType = $(obj).attr('type');  
	  if($(cloneToObj).hasClass('removePrevNext')){
	     //confirmAction="Backlog";
	     //confirmFun(getValues(companyAlerts,"Alert_StageBacklog"),"clear","confirmStoryMove");
	     alertFun(getValues(companyAlerts,"Alert_StageBacklogMsg"),'warning');
	  }else if($(cloneToObj).hasClass('removeCompleted')){
	     confirmAction="Completed";
	     confirmStoryMove();
	  }else{
	     confirmAction="Other";
		 confirmStoryMove();
 	  } 
 }
 var confirmType='';
 
 function confirmStoryMove(){
           if(DragNdrop == false && confirmAction != 'Completed'){
              $('#SB_story_'+confirmStoryId).find('img.sb_nextStage , img.sb_prevStage').show();
		      var storyDiv=$('#SB_story_'+confirmStoryId).clone();
		 	  $('#SB_story_'+confirmStoryId).remove();
		 	  $(storyDiv).appendTo($(confirmCloneObj));
		 	  removePrevNext();
		   }
		   if(confirmAction != 'Other'){
		     $("#loadingBar").show();
	 	     timerControl("start");
	 	   }  
	 	   var place = $('#sbPlace').val();
	 	  
		   $.ajax({
	 			url: path+"/agileActionNew.do",
	 			type:"POST",
	 			data:{action:"moveStoryToStage",storyId:confirmStoryId,stageId:confirmStageId,stageAction:confirmAction,place:place,type:confirmType},
	 			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
	 			success:function(result){
	 			   checkSessionTimeOut(result);
	 			  
	 			   if(result=='NotCompleted'){
	 			      alertFun(getValues(companyAlerts,"Alert_StageCompleteError"),'warning');
	 			      if(DragNdrop == true){
	 			        revertDrag();
	 			      }  
	 			   }else if(result=='Completed'){
	 			      if(DragNdrop == true){
	 			         confirmReset(getValues(companyAlerts,"Alert_StageCompleteUpdate"),"clear","confirmSBStoryStatus","revertDrag");
	 			      }else{
	 			         confirmFun(getValues(companyAlerts,"Alert_StageCompleteUpdate"),"clear","confirmSBStoryStatus");
	 			      }
	 			   }else{
	 			         
					  
					  var data=result.split("@@@ColabuS@@@");
	 			      if(confirmAction=="Backlog" ){
	 			        $('#SB_story_'+confirmStoryId).find('.sb_userListDiv').html("");
	 			        var sprintId =  $('#SB_story_'+confirmStoryId).find('.sb_userListDiv').attr('sprintId');
	 			        if(data[0]=="Progress"){
	 			            var btn="   <div class=\"text-uppercase createBtn sbAssignBtn Story_Assign_cLabelValue\" sprintId = \""+sprintId+"\" onclick=\"createTaskUI('','Sprint', "+confirmStoryId+", 'SB', "+sprintId+");\" style=\"padding: 5px 10px ! important; margin-top: 0px; visibility: visible; float: none; width: 60px;\">Assign</div>";
							//var btn=" <input type=\"button\" style=\"margin-top: 8px; float: none;\" onclick=\"createEpicTasks("+confirmStoryId+");\" value=\"Assign\" name=\"Assign\" class=\"ideaSaveBtn Story_Assign_cLabelValue\" id=\"storyAssignBtn\">";
	 			        	$('#SB_story_'+confirmStoryId).find('.sb_userListDiv').html(btn);
	 			        }
	 			      }
	 			      if(data[1]!=""){
	 			         var epicStatus=data[1].split('@#@')[0];
	 			         var imgSrc=(epicStatus=='Backlog')? path+"/images/idea/Backlog.png" : (epicStatus=='In Progress')? path+"/images/idea/Assigned.png" : (epicStatus=='Blocked')? path+"/images/idea/Blocked.png" : (epicStatus=='Hold')? path+"/images/idea/Hold.png" : (epicStatus=='Done')? path+"/images/idea/Done.png" : path+"/images/idea/cancel.png"
	 					 $('#SB_story_'+confirmStoryId).find('.sb_storyStatus').attr('src',imgSrc).attr('title',epicStatus);
	  				  }
	  				  findStoryCount();
	  				  setHeightToTR();
	  				  showEpicFeaturesOnclick();
	  				  //$("div#groupSprintContainer").mCustomScrollbar('update');
	 			   }
	 			  if(confirmAction != 'Other'){
	 			    $("#loadingBar").hide();
	 			    timerControl("");
	 			  } 
	 		  }
	 	});
 } 
 
 
 function showSBStoryComments(storyId){
 	commentPlace='ScrumBoard'; //--------used in commonComments.js to decide in which tab adding/updating/deleting the comments.
   addComment(storyId,'agile');	  
 }
 
 function showSprintComments(sprintId){
   commentPlace='Sprint'; //--------used in commonComments.js to decide in which tab adding/updating/deleting the comments.
   $('.sprintActive').hide();
   $('#sprintActive_'+sprintId).show();
   addComment(sprintId,'agile');	 
 }
 
 function fetchSBStoryComments(storyId){
 	 $("#loadingBar").show();
 	 timerControl("start");
 	 
     $.ajax({
			url:""+path+"/agileActionNew.do",
			type:"POST",
			data:{action:"fetchComments",epicID:storyId, subMenuType:commentPlace  },
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			success:function(result){
				checkSessionTimeOut(result);
				$("#commentListDiv").mCustomScrollbar("destroy");
				$('#commentListDiv').html(prepareCommentsUI(jQuery.parseJSON(result),"agile",storyId));
				
				//$('#commentListDiv').find('div.ideaCommentListDiv').css('padding-right','10px');
				//$('#commentListDiv').find('.ideaFeedExtraFont').css('color','#000');
				//$('.IdeaCreateDiv1').children('div').css('border-top','');
				//$('.replyCommentDiv').children('div').css('border-top','');
				
				//confirmClearCreateStage();
				$('#commentListDiv').on("mouseover","div.actFeedHover",function(){  //----- to hide option popup
					if(!$(this).find('div.actFeedOptionsDiv').is(':visible')){
						$('div.actFeedHover').find('div.actFeedOptionsDiv').hide();
					}
			     });
				$('input.commentEpicId').val(storyId);
				$('#sbCommentListPopUp').show();
				$('#transparentDiv').show();
                loadAgileCustomLabel("fetchComments");
				if(!isiPad){ 
				  popScroll('commentListDiv');
				  $('#commentListDiv').find('.mCSB_scrollTools').css('margin-right','0px');
                }  
                //var landH=$('#sbCommentListPopUp').height();
				//var mainH=parent.$('#menuFrame').height();
				//if(mainH>landH){
							   //alert('1st');
				//}else{
				//	$('#sbCommentListPopUp').css({'top':'0%','margin-top':'0px','height':mainH-10+'px'});
				//	popScroll('sbCommentListPopUp');
				//} 
                $("#loadingBar").hide();
				timerControl("");
			    
		  }
	  });
 			  
 	}
 
  
 function fetchSprintComments(sprintId){
 	 $("#loadingBar").show();
 	 timerControl("start");
 	 
     $.ajax({
			url:""+path+"/agileActionNew.do",
			type:"POST",
			data:{action:"fetchComments",epicID:sprintId,subMenuType:commentPlace },
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			success:function(result){
				checkSessionTimeOut(result);
				$("#commentListDiv").mCustomScrollbar("destroy");
				$('#commentListDiv').html(prepareCommentsUI(jQuery.parseJSON(result),"agile",sprintId));
				
				$('#commentListDiv').on("mouseover","div.actFeedHover",function(){  //----- to hide option popup
					if(!$(this).find('div.actFeedOptionsDiv').is(':visible')){
						$('div.actFeedHover').find('div.actFeedOptionsDiv').hide();
					}
			     });
				$('input.commentEpicId').val(sprintId);
				$('#sbCommentListPopUp').show();
				$('#transparentDiv').show();
                loadAgileCustomLabel("fetchComments");
				if(!isiPad){ 
				  popScroll('commentListDiv');
				  $('#commentListDiv').find('.mCSB_scrollTools').css('margin-right','0px');
                }  
                if(typeof(agileNotificationType) != 'undefined' && agileNotificationType != "" && agileNotificationType == "agile_sprint_comment"){
                    $('#actFeedTot_'+epicCmtDocId).addClass("notHightlightCls");
                    if($("#commentListDiv").find('div.mCSB_scrollTools').css('display') == 'block'){ 
                            $("#commentListDiv").mCustomScrollbar("scrollTo", "#actFeedTot_"+epicCmtDocId);
                    }
                }
                $("#loadingBar").hide();
				timerControl("");
			    
		  }
	  });
 			  
 	}
 
 
 function closeSBCommentsPopup(){
    $('#sbCommentListPopUp').hide();
    $('#transparentDiv').hide();
    var srcId = $('#sbCommentListPopUp').find('input.commentEpicId').val();
    //alert(srcId);
    if($('#commentListDiv').find('.mCSB_container').children().length > 0){
       $('#SB_story_'+srcId).find('img.SB_cmtIcon[type=comment]').attr('src',path+'/images/idea/comment2.png');
    }else{
       $('#SB_story_'+srcId).find('img.SB_cmtIcon[type=comment]').attr('src',path+'/images/idea/comment1.png');
    }
    agileNotificationType="";
 }
 
  function showSBStoryDocuments(storyId){
    $('#sbDocListPopUp').show();
    $('#transparentDiv').show();
    $('.epicCommonListDivCss').html('');//---->In  story list view document list div 
    $("#loadingBar").show();
	timerControl("start");
    $.ajax({
			url: path+"/agileActionNew.do",
			type:"POST",
			data:{action:"listIdeaDocs",ideaId:storyId},
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
			success:function(result){
				checkSessionTimeOut(result);
				$("#docListDiv").mCustomScrollbar("destroy");
				$('#docListDiv').html(prepareStoryDocUI(jQuery.parseJSON(result)));
				$('#docListDiv').find('.sbRemoveIcons').remove();
				loadAgileCustomLabel("AttachDoc");
				if(!isiPad){ 
				  popScroll('docListDiv');
				  $('#docListDiv').find('.mCSB_scrollTools').css('margin-right','0px');
                }
				$("#loadingBar").hide();
				timerControl("");
		  }
	  });
    
    
  }

 function prepareStoryDocUIHeader(){
       var headerUI = '';
        headerUI+='<div id="assignedtaskheaders" class="tabContentHeader" >'
		headerUI+='      <div class="tabContentSubHeader" style="margin: 0px;">'
		headerUI+='         <div class="tabContentHeaderName Title_cLabelHtml" style="width:30%;font-size:13px;height:20px;overflow:hidden;"></div>'
		headerUI+=' 		<div class="tabContentHeaderName Repo_Name_cLabelHtml" style="width: 35%;font-size:13px;height:20px;overflow:hidden;"></div>'
		headerUI+='  		<div class="tabContentHeaderName Created_On_cLabelHtml" style="width: 15%;font-size:13px;height:20px;overflow:hidden;"></div>'
		headerUI+='	        <div class="tabContentHeaderName Created_by_cLabelHtml" style="width: 20%;font-size:13px;height:20px;overflow:hidden;"></div>'
		headerUI+='      </div>'
		headerUI+='    </div> '
		
		return headerUI;
 }

 function prepareStoryDocUI(data){
    
    var taskUI='';
    var imgCss="";
   if(data){
      var json = eval(data);
         var j = 1;
	     for(var i=0; i<json.length; i++){
	        j++;
	        imgCss="";
			if(userId != json[i].createdById){
				imgCss="display:none;";
			}
	        
	        if(json[i].type=='Document'){
	           taskUI+="<div id=\"agileDoc_"+json[i].id+"\" class=\"assignedTaskPageClass docListViewRowCls\" >";  
			   taskUI+="  <div class=\"docListViewBorderCls\" style=\"padding-top: 8px;font-size:12px;margin:0px;\">";
			   taskUI+="      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:30%;margin-top: 6px;float:left;\" ><input type=\"text\" class='defaultExceedCls' title=\""+replaceSpecialCharacter(json[i].title)+"\" name=\"titleDoc_"+json[i].id+"\" id=\"titleDoc_"+json[i].id+"\" value=\""+replaceSpecialCharacter(json[i].title)+"\" style=\"border:0px;background:transparent;width:95%;\" readOnly=\"readOnly\" /></div>";
	           taskUI+="      <div class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:35%;margin-top: 6px;float:left;\" >";
	           taskUI+="         <img src=\""+path+"/images/repository/"+json[i].documentImg+"\" style=\"cursor: pointer;height:28px;width:28px;float:left;max-width:15%;\" onclick=\"openViewDownloadPopup("+json[i].id+");\" />";
	           taskUI+="         <span class=\"docName defaultExceedCls\" id=\"spanDoc_"+j+"\" onclick=\"openViewDownloadPopup("+json[i].id+");\"  style='float: left;height: 18px;max-width: 70%;overflow: hidden;cursor:pointer;margin-top:4px;' title=\""+json[i].Name+"\">"+json[i].Name+"</span>";
	           taskUI+="         <input type=\"hidden\" name=\"attachDocId_"+j+"\" id=\"attachDocId_"+j+"\" value=\""+json[i].id+"\" /><input type=\"hidden\" name=\"repoDocId_"+j+"\" id=\"repoDocId_"+j+"\" value=\""+json[i].docid+"\" />";
	           
	           taskUI+="         <div class=\"hideOptionDiv docViewRdownload\" id=\"epicDownloadOption_"+json[i].id+"\" style=\"padding-left: 1px; margin-top: -12px;display:none;\">";
			   taskUI+="            <div style=\"float: left; margin-top: 10px; margin-left: -19px;\"><img src=\""+path+"/images/repository/arrowleft.png\"></div>";
			   taskUI+="            <div class=\"optionList\" style=\"float:left;margin-left: 7px; width: 85%;height: 28px;border-bottom: 1px solid #C1C5C8;\">";
			   taskUI+="                <a class=\"\" onclick=\"viewAgileDoc('"+json[i].docid+"','"+json[i].documentLocation+"','"+json[i].documentType+"');\" onmouseover=\"onMouseOverDiv(this)\" onmouseout=\"onMouseOutDiv(this)\"><div class=\"OptionsFont View_cLabelHtml\" style=\"margin-top: 4px;\">View</div></a> </div>";
			   taskUI+="            <div style=\"float: left;border-bottom:0px;margin-left: 7px;\" onmouseout=\"onMouseOutDiv(this)\" onmouseover=\"onMouseOverDiv(this)\" onclick=\"downloadAgileFile("+json[i].docid+",'"+json[i].documentLocation+"')\" class=\"optionList\"><div class=\"OptionsFont Download_cLabelHtml\">Download</div></div>";
			   taskUI+="         </div> ";
	           
	           
	           taskUI+="      </div>";
	           taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 15%;margin-top:6px;\" vaign=\"middle\"><span class='textExceedCss' title=\""+json[i].createdDate+"\">"+json[i].createdDate+"</span></div>";
			   taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 15%;margin-top:6px;\" vaign=\"middle\"><span class='textExceedCss' title=\""+json[i].createdBy+"\">"+json[i].createdBy+"</span></div>";
			   taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 5%;margin-top:6px;\" vaign=\"middle\">";
			   taskUI+="          <img id=\"editDoc_"+json[i].id+"\" class='sbRemoveIcons Edit_cLabelTitle' src=\""+path+"/images/idea/edit.png\" width=\"11\" height=\"11\" style=\"cursor:pointer;"+imgCss+"\" onClick=\"funAttachDocUpdate('"+json[i].id+"');\"/>";
			   taskUI+="          <img class='sbRemoveIcons Delete_cLabelTitle'  src=\""+path+"/images/close.png\" width=\"11\" height=\"11\" style=\"cursor:pointer;margin-left:12px;"+imgCss+"\" onClick=\"delteDocLink('"+json[i].id+"',"+json[i].epicId+");\"/>";
			   taskUI+="      </div>";			
			   taskUI+="  </div>";			
			   taskUI+="</div>";
	        }else{
	           taskUI+="<div id=\"epicLink_"+json[i].id+"\" class=\"assignedTaskPageClass docListViewRowCls\" >";  
			   taskUI+="  <div class=\"docListViewBorderCls\" style=\"padding-top: 8px;font-size:12px;margin:0px;\">";
			   taskUI+="      <div id=\"titleTd_"+json[i].id+"\" class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:30%;margin-top: 6px;float:left;\" ><input type=\"text\" class='defaultExceedCls' title=\""+replaceSpecialCharacter(json[i].title)+"\" name=\"title_"+json[i].id+"\" id=\"title_"+json[i].id+"\" value=\""+replaceSpecialCharacter(json[i].title)+"\" style=\"border:0px;background:transparent;width:95%;\" readOnly=\"readOnly\" /></div>";
	           taskUI+="      <div id=\"linkTd_"+j+"\"  class=\"tabContentHeaderName defaultExceedCls\" align=\"left\" valign=\"middle\"  style=\"width:35%;margin-top: 6px;float:left;\" >";
	           taskUI+="         <span class=\"docLink defaultExceedCls\" id=\"spanLinkIdea_"+json[i].id+"\" style='float: left;height: 18px;max-width: 80%;overflow: hidden;' onclick=\"openLink4Project(this ); \" title='"+json[i].Name+"'>"+json[i].Name+"</span>";
	           taskUI+="         <input type=\"text\" name=\"spanLink_"+json[i].id+"\" id=\"spanLink_"+json[i].id+"\" value=\""+json[i].Name+"\" style=\"display:none;border:0px;background:transparent;width:83%;float:left;\"  readOnly=\"readOnly\"/>";
	           taskUI+="      </div>";
	           taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 15%;margin-top:6px;\" vaign=\"middle\"><span class='textExceedCss' title=\""+json[i].createdDate+"\">"+json[i].createdDate+"</span></div>";
			   taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 15%;margin-top:6px;\" vaign=\"middle\"><span class='textExceedCss' title=\""+json[i].createdBy+"\">"+json[i].createdBy+"</span></div>";
			   taskUI+="      <div class=\"tabContentHeaderName\" align=\"left\" style=\" float: left; width: 5%;margin-top:6px;\" vaign=\"middle\">";
			   taskUI+="          <img id=\"editLink_"+json[i].id+"\" class='sbRemoveIcons Edit_cLabelTitle'  src=\""+path+"/images/idea/edit.png\" width=\"11\" height=\"11\" style=\"cursor:pointer;"+imgCss+"\" onClick=\"funAttachLinkUpdate('"+json[i].id+"');\"/>";
			   taskUI+="          <img src=\""+path+"/images/close.png\" class='sbRemoveIcons Delete_cLabelTitle' width=\"11\" height=\"11\" style=\"cursor:pointer;margin-left:12px;"+imgCss+"\" onClick=\"funAttachLinkDelete('"+json[i].id+"',"+json[i].epicId+");\"/>";
			   taskUI+="      </div>";			
			   taskUI+="  </div>";			
			   taskUI+="</div>";
			
	        }
		}
     
   }else{
      taskUI+="<span>No data found.</span>";
   }
   
   return taskUI;
 }

 function closeSBDocPopup(){
    $('#sbDocListPopUp').hide();
    $('#transparentDiv').hide();
 }
 
 function viewAgileDoc(docId,docLocation,docExt){
	  
	     var url="";
       if(docLocation=='Workspace'){
          if(docExt.toLowerCase() =='png' || docExt.toLowerCase() =='jpg' || docExt.toLowerCase() ==' bmp' || docExt.toLowerCase() =='gif' || docExt.toLowerCase() =='jpeg' || docExt.toLowerCase() =='mp3' || docExt.toLowerCase() =='mp4')
			 url = lighttpdPath+"/projectDocuments//"+docId+"."+docExt+"" ;
		  else
			 url = "https://docs.google.com/gview?url="+lighttpdPath+"//projectDocuments//"+docId+"."+docExt+"" ;	
	   }else{
	      if(docExt.toLowerCase() =='png' || docExt.toLowerCase() =='jpg' || docExt.toLowerCase() ==' bmp'  || docExt.toLowerCase() =='gif' || docExt.toLowerCase() =='jpeg' || docExt.toLowerCase() =='mp3' || docExt.toLowerCase() =='mp4')
            url = lighttpdPath+"/uploadedDocuments//"+docId+"."+docExt+"" ;
          else
			 url = "https://docs.google.com/gview?url="+lighttpdPath+"//uploadedDocuments//"+docId+"."+docExt+"" ;	  
       } 
	  window.open(url);
   }
 
 
 
  function openLink4Project(obj) {
		    var urlLink = $(obj).attr('title');
		    var httpIndex = urlLink.indexOf("http://");
		    if(httpIndex!=0){
		      httpIndex = urlLink.indexOf("https://");
		      if(httpIndex!=0){
		            urlLink ='http://'+urlLink ;
		        }
		    }
		    popup(urlLink);
 } 
 
 function popup(url) {
		    params = 'width=' + screen.width-1;
		    params += ', height=' + screen.height-1;
		    params += ', top=0, left=0'
		    params += ', fullscreen=no';
		    newwin = window.open(url, 'windowname4', params);
		    if (window.focus) {
		        newwin.focus()
		    }
		    return false;
 }
 
 
 function openViewDownloadPopup(rowId){
   if(!$('#epicDownloadOption_'+rowId).is(':visible')){
     $('div.docViewRdownload').hide();
     $('#epicDownloadOption_'+rowId).slideToggle(function(){
       $(this).css('overflow','');
     });
     $('div#docListDiv').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
   }else{
     $('#epicDownloadOption_'+rowId).slideToggle();
   }
 }
 
  var confirmDocLoc="";
 function downloadAgileFile(docId,docLocation){
		confirmId=docId;
		confirmDocLoc=docLocation;
		confirmFun(getValues(companyAlerts,"Alert_Download_Doc"),"close","downloadAgileConfirm");
 }	

 function downloadTaskDoc(docId,docLocation){
		confirmId=docId;
		confirmDocLoc=docLocation;
		confirmFun(getValues(companyAlerts,"Alert_Download_Doc"),"close","downloadFileConfirm");
 }
  function downloadAgileConfirm(){
		     var url="";
           if(confirmDocLoc=='Repository'){
			   url = path+"/repositoryAction.do?action=downloadfile&docId="+confirmId ;
		   }else{
              var projId = $('#proj_ID').val();	
              url= path+"/workspaceAction.do?act=downloadfile&docId="+confirmId+"&projId="+projId;
           } 
			window.open(url);
  }
 
 
 
 function showSBmoreUsers(obj,sprintId,storyId,storyType){
   $("#loadingBar").show();
   timerControl("start");
   
   $('#sbUsersPopup').show();
   $('#transparentDiv').show();
   
   $('#sb_reAssignedTr').hide();
   $('#sbUsersDiv').css('height','230px');
   var re_sprint=$(obj).parents('div.SB_story').find('input.sbReAssined').val();
   if(re_sprint!=''){
       if(storyType=='History'){
	     $('#sb_reAssignedTr').find('td:eq(0) div').html('To');
	   }else{
	      $('#sb_reAssignedTr').find('td:eq(0)  div').html('From');
	   }
	   $('#sb_reAssigned').val(re_sprint).attr('title',re_sprint);
	   $('#sb_reAssignedTr').show();
	   $('#sbUsersDiv').css('height','200px');
   }
           
   	$.ajax({
	   	url: path+"/agileActionNew.do",
		type:"POST",
		data:{action:"fetchSBmoreUsers",sprintId:sprintId,storyId:storyId,storyType:storyType},
		error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
		success:function(result){
            checkSessionTimeOut(result);
            var data = result.split("@#####@");
            $('#sbUsersDiv').html(data[0]);
            var sprintStoryId = $(obj).parents('div.SB_story').find('input.sbSprintStoryId').val();
            $('#sbUsersDiv').find('img').attr('sprintStoryId',sprintStoryId).attr('onclick','showStoryTasks("'+storyType+'",'+storyId+',"'+sprintStoryId+'")');
            
            var storyDetails = eval(JSON.parse(data[1]));        
            var sprintName = replaceSpecialCharacterForInput(storyDetails[0].sprintName);
            sprintName = unicodeTonotificationValue(sprintName);  
            var storyName = replaceSpecialCharacterForInput(storyDetails[0].epicName);
            storyName = unicodeTonotificationValue(storyName);  
            $('#sb_sprintName').val(sprintName);          
            $('#sb_storyName').val("Story : "+storyName); 
            
            var customId = storyDetails[0].customIdValue ;
   			customId = (customId=='')? '-':customId;
            $('#sb_storyId').val(customId);
            
            var Points = storyDetails[0].point ;
   			Points = (Points=='0' || Points=='')? '-':Points;
   			$('#sb_storyPts').val(Points);
   			
   			var Prty = storyDetails[0].PriorityImg ;
   			Prty = (Prty=='-' || Prty=='' ) ? '-': '<img style="height:16px;width:16px;float:left;" src="'+Prty+'" >';
   			$('#sb_storyPrty').html(Prty);
   			
   			var statusImg = '<img class="storyStatusPopIcon sb_storyStatus"  style="height: 18px; width: 18px; float: right; margin-right: -5px; cursor: default;" title="'+storyDetails[0].epicStatus+'" src="'+storyDetails[0].epicStatusImage+'">';
            $('#sb_storyStatus').html(statusImg);
            
            loadAgileCustomLabel('Detail view');
            
            if(!isiPad){
            	popScroll('sbUsersDiv');
	           	 //$('div#sprintStoryContainer >.mCS-light >.mCSB_scrollTools').css('margin-right','0px');
            }else{
              $('#assignedSprintheaders').css('width','100%');
            }
            
   			$("#loadingBar").hide();
   			timerControl("");
		}
	});
 }
 
 function closeSBMoreUsers(){
     $('#transparentDiv').hide();
     $('#sbUsersPopup').hide();
     $("div#sbUsersDiv").mCustomScrollbar('destroy');
 }
 
 //---------------------------------------- scrumboard task functions ----------------------------------------->
  var SB_task_type='';
  var SB_task_sprintStoryId='';
  var SB_task_sprintId = '';
  var SB_task_sprintGrpId = '';
  /*
   * -- These variables are declared in taskUIcode.js file and assigned here.
   * 
  var SB_task_viewType = '';
  var storyIdForCal = '';
  */
  function showStoryTasks(type,storyId,sprintStoryId){
	  SB_task_viewType="SB";
      
	  storyIdForCal = storyId;
      SB_task_type=type;
      SB_task_sprintStoryId=sprintStoryId;
      
       if($('#sbUsersPopup').is(':visible')){
    	   $('#sbUsersPopup,#sprintTransparent').hide();
    	   $("div#sbUsersDiv").mCustomScrollbar('destroy');
       }
 		$("#loadingBar").show();
 		timerControl("start");
	     $.ajax({
				url: path+"/agileActionNew.do",
				type:"POST",
				data:{action:"listEpicTask",ideaID:storyId,viewPlace:"Epic",type:type,pId:sprintStoryId},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
				success:function(result){
					checkSessionTimeOut(result);
					//alert(result+"///");alert("SB_task_viewType//"+SB_task_viewType);
					
					$('#ideaTaskListPopUp #taskListContainer').html(prepareTaskListUI(jQuery.parseJSON(result),"SB"));
					$('#taskListDiv').css('height','450px');
					$('#ideaTaskListPopUp').show();
					$('#createTaskListUiDiv_').show();
					$('#transparentDiv').css('display','block');
					loadAgileCustomLabel("TaskList");
					if(!isiPad){ 
						 popScroll('taskListDiv');
						 $('#taskListDiv').find('.mCSB_container').css('margin-right','0px');
						 $('div#taskListDiv >.mCS-light >.mCSB_scrollTools').css('margin-right','-3px');
                    }
					$("#loadingBar").hide();
					timerControl("");
			  }
		  });
 			  
 }
 
    
  function createTaskCancel(idd){
       
        $("#loadingBar").show();
        timerControl("start");
       /* if (!$('#ideaCreateTask').is(':hidden')) {
             //$("#ideaCreateTask").mCustomScrollbar("destroy");
             $('#ideaCreateTask').html("").css({'width':'810px','margin-left':'-405px','top':'50%','height':'540px','margin-top':'-270px','display':'none'});
			 $('#taskUserListContainer').css('float','right');
			 
		   }
		
		if (!$('#ideaMyTask').is(':hidden')) {
		     //$("#ideaMyTask").mCustomScrollbar("destroy");
             $('#ideaMyTask').html("").css({'display':'none','width':'810px','margin-left':'-405px','top':'50%','height':'500px','margin-top':'-250px'});
		 }
		      
 		if (!$('#ideaSharePopUp').is(':hidden')) {
 		     //$("#ideaSharePopUp").mCustomScrollbar("destroy");
 		     $('#ideaSharePopUp').html("").css({'width':'800px','margin-left':'-400px','top':'50%','height':'400px','margin-top':'-200px','display':'none'});
			 $('#taskUserListContainer').css('float','right');
		}  
		if (!$('#ideaDocPopUp').is(':hidden')) {
		     $('#ideaDocPopUp').html("").css('display','none');
		 }  
		 
		 */
		if (!$('#sprintCreateTask').is(':hidden')) {
		     $("#sprintCreateTask").mCustomScrollbar("destroy");
		     $('#sprintCreateTask').css({'width':'870px','margin-left':'-450px','top':'50%','height':'500px','margin-top':'-250px','display':'none'});
			 $('#transparentDiv').css('display','none');
			 $('#taskEventName,#taskEventDescription,#dateXmlTxtArea').val("");
	         $('#sprintId,#taskStartDate,#taskEndDate,#prevStartDate,#prevEndDate,#prevStartDate1,#prevEndDate1').val('');
	         $('#taskEventName,#selSprintGp,#taskStartDate,#taskEndDate').css("border","1px solid #BFBFBF");
	         if(!isiPad){
                 $('#sprintStoryListDiv').mCustomScrollbar("destroy"); 
                 $('#sprintStoryListDiv').html('');
                 $('#storyForSprintDiv').mCustomScrollbar("destroy"); 
                 $('#sprintDateHourDiv').mCustomScrollbar("destroy"); 
             }        
		 }
		 
		 if (!$('#AssignSprintList').is(':hidden')) {
		     $("#sprintListDiv").mCustomScrollbar("destroy");
             $('#AssignSprintList').hide().find('#sprintListDiv').html("");
		     $('#transparentDiv').css('display','none');
		 }  
		 if (!$('#ideaTaskListPopUp').is(':hidden')) {
		     $("#ideaTaskListPopUp").mCustomScrollbar("destroy");
             $('#ideaTaskListPopUp').css({'display':'none','top':'50%','height':'520px','margin-top':'-260px'});
		 } 
		/* if (!$('#sbCommentListPopUp').is(':hidden')) {
		  alert('Hi 3');
		     $("#sbCommentListPopUp").mCustomScrollbar("destroy");
             $('#sbCommentListPopUp').css({'display':'none','top':'50%','height':'520px','margin-top':'-260px'});
		 } */
		selectedStoryId="";deletedStoryId="";deletedUserID=",";selectedTaskemailId=",";deletedShareIdeaTopicUserID=",";
		
		$('#transparentDiv').css('display','none');
 		$("#loadingBar").hide();
 		timerControl("");
 }
 
  function  refreshSprintTask(taskId,viewPlace){
	  $("#loadingBar").show();
	  timerControl("start");
	  $.ajax({
		  url: path+"/agileActionNew.do",
		  type:"POST",
		  data:{action : "listEpicTask", ideaID : storyIdForCal, viewPlace : viewPlace, type : SB_task_type, pId : SB_task_sprintStoryId, sprintId: SB_task_sprintId, sprintGrpId: SB_task_sprintGrpId, taskId : taskId},
		  error: function(jqXHR, textStatus, errorThrown) {
			  checkError(jqXHR,textStatus,errorThrown);
			  $("#loadingBar").hide();
			  timerControl("");
		  },
		  success:function(result){
			  checkSessionTimeOut(result);
			  //console.log("result:"+result);
			  //console.log("SB_task_viewType:"+SB_task_viewType);
			  //console.log("SB_task_type:"+SB_task_type);
			  //console.log("SB_task_sprintStoryId:"+SB_task_sprintStoryId);
			  if(SB_task_viewType  =="SB"){
				  $('#ideaTaskListPopUp #taskListContainer').find("#task_"+taskId).replaceWith(prepareSprintTasksList(jQuery.parseJSON(result), viewPlace));
			  }else if(SB_task_viewType  =="DetailSprint"){
				  $('#taskContainer_'+SB_task_type+'_'+SB_task_sprintStoryId).find("#task_"+taskId).replaceWith(prepareSprintTasksList(jQuery.parseJSON(result), viewPlace));
			  }else if(SB_task_viewType  =="Epic"){
				  $('#epicListCommonDiv_'+storyIdForCal).find("#task_"+taskId).replaceWith(prepareSprintTasksList(jQuery.parseJSON(result), viewPlace));
			  }
			  
			  $("#loadingBar").hide();
			  timerControl("");
		  }
	  });
  }
  
  function refreshSprintTaskAfterDelete(taskId){
	  taskId = taskId.substring(0,taskId.length-1);
	  //alert(taskId);
	  if(SB_task_viewType  =="SB"){
		  $('#ideaTaskListPopUp #taskListContainer').find("#task_"+taskId).remove();
	  }else if(SB_task_viewType  =="DetailSprint"){
		  $('#taskContainer_'+SB_task_type+'_'+SB_task_sprintStoryId).find("#task_"+taskId).remove();
	  }else if(SB_task_viewType  =="Epic"){
		  $('#epicListCommonDiv_'+storyIdForCal).find("#task_"+taskId).remove();
	  }
  }
  
 //----------------- dashboard functions ------------------------------------>
 
  function loadOverallTaskProgress(){
        
	    $.ajax({
			url: path+"/agileActionNew.do",
	    	type: "POST", 
	    	data: {action:"loadOverallTaskProgress", projectId:projectId}, 
	    	error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
	    	success:function(result) {
	    	   checkSessionTimeOut(result);
	    	   
	    	   if(parseInt(result)>=50){
		         circleColor ="#418DCF";
		       }else{
		         circleColor ="#F66165"
		       }
		    	
		    	$('#overallCount' ).progressCircle({
					nPercent        : result,
					showPercentText : true,
					thickness       : '8',
					circleSize      : '100',
					borderColor		: circleColor
				});
		    }
	    });
	 }
	 
 	 function  loadProjectDataList(){
	    $("div#projectDataDiv").show();
	    
	    var epicAdded = "";
	    var featureAdded = "";
    	var storyAdded = "";
    	var documentAdded = "";
    	var taskAdded="";
    	$.ajax({
			url: path+"/agileActionNew.do",
	    	type: "POST", 
	    	data: {action:"loadProjectDataList", projectId:projectId}, 
	    	error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
	    	success:function(result){
	    	  checkSessionTimeOut(result);
	    	  epicAdded=result.split('#@#')[0];
	    	  storyAdded=result.split('#@#')[1];
	    	  documentAdded=result.split('#@#')[2];
	    	  taskAdded=result.split('#@#')[3];
	    	  featureAdded = result.split('#@#')[4];
	    	  if(epicAdded > 0){
	    	     $("#epicLabel").html(getValues(companyLabels,'db_Epics_Added'));
	    	  }
	    	  if(storyAdded > 0){
	    	     $("#storyLabel").html(getValues(companyLabels,'db_Storys_Added'));
	    	  }
	    	  if(featureAdded > 0){
	    	     $("#featureLabel").html(getValues(companyLabels,'db_Features_Added'));
	    	  }
	    	  if(documentAdded > 0){
	    	     $("#documentLabel").html(getValues(companyLabels,'db_Documents_Added'));
	    	  }
	    	  if(taskAdded > 0){
	    	     $("#taskLabel").html(getValues(companyLabels,'db_Tasks_Added'));
	    	  }
	    	  $("#epicCount").html(epicAdded);
	    	  $("#storyCount").html(storyAdded);
	    	  $("#documentCount").html(documentAdded);
	    	  $("#taskCount").html(taskAdded);
	    	  $("#featureCount").html(featureAdded);
	    	}
	    });
	 }
	 
 function loadprojectStoryStatusData(){
	    
	    $.ajax({
			url: path+"/agileActionNew.do",
	    	type: "POST", 
	    	data: {action:"loadprojectStoryStatusData", projectId:projectId}, 
	    	error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
	    	success:function(result) {
	    		 checkSessionTimeOut(result);
	    	     var dataList=result.split('#@#');
                 var data = new google.visualization.DataTable();
		         data.addColumn('string', 'Topping');
		         data.addColumn('number', 'Slices');
		         data.addColumn('string', 'storyid');
		         if(result =="0#@#0#@#0#@#0#@#0#@#0#@#'0'#@#'0'#@#'0'#@#'0'#@#'0'#@#'0'"){
		            $("#prjStoryGraph").html("No stories found.");
		         }else{
			         data.addRows([
					  	['Backlog',parseInt(dataList[0]),(dataList[6])],				  
					    ['Blocked',parseInt(dataList[1]),(dataList[7])],
					    ['In Progress',parseInt(dataList[2]),(dataList[8])],
					    ['Hold',parseInt(dataList[3]),(dataList[9])],
					    ['Done',parseInt(dataList[4]),(dataList[10])],
					    ['Cancel',parseInt(dataList[5]),(dataList[11])]
				     ]);
				 }
                 var chart = new google.visualization.PieChart(document.getElementById('prjStoryGraph'));
                
                  //Chart onclick function starts
			     function prjStoryStatusDetail() {
			        $("#loadingBar").show();
                    timerControl("start");
          			var selectedItem = chart.getSelection()[0];
         			 if (selectedItem) {
         			 	var storyStatusHeader = data.getValue(selectedItem.row, 0);
         			 	var storyIds = data.getValue(selectedItem.row, 2);
         			 	$.ajax({
				          url: path+"/agileActionNew.do",
				          type:"POST",
				          data:{action:"loadChartStatusDetails",projectId:projectId,storyStatusHeader:storyStatusHeader,storyIds:storyIds},
				          error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
							},
				          success:function(result){
	          				 checkSessionTimeOut(result);
	          				$('#chartStatusDetailDiv').css('display','block');
	          				$("#statusHeader").html(storyStatusHeader+' Details');
	            			$("#statusDataDiv").html(result);
		                    $('#transparentDiv').css('display','block');
	            			var landH=$('#chartStatusDetailDiv').height();
	            			
	            			$('#statusDataDiv').css('height',landH-90+'px');
							if(!isiPad){
							  popScroll('statusDataDiv');
							 }
							$("#loadingBar").hide();
                      		timerControl("");
						    
						   }
						});
         			 }
         		}

       			 google.visualization.events.addListener(chart, 'select', prjStoryStatusDetail);   
       			 
       			 //Chart onclick function ends
			     chart.draw(data);
	  		}
	 	 });
	 }
	 
 function closeTaskDetailPopup(){
     $('#chartDetailDiv').css('display','none');
	 $('#transparentDiv').css('display','none');
	 if(!isiPad){
		$("#taskDataDiv").mCustomScrollbar("destroy");
	 }
 }
 
 function closeStatusDetailPopup(){
     $('#chartStatusDetailDiv').css('display','none');
	 $('#transparentDiv').css('display','none');
	 if(!isiPad){
		$("#statusDataDiv").mCustomScrollbar("destroy");
	 }
 } 
 
  function loadprojectTaskStatus(){
	      
	       $.ajax({
	          url: path+"/agileActionNew.do",
	          type:"POST",
	          data:{action:"loadprojectTaskStatus",projectId:projectId},
	          error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
	          success:function(result){
	          		 checkSessionTimeOut(result);
	          		 
	          		 var dataList=result.split('#@#');
	          		 var data = new google.visualization.DataTable();
			         data.addColumn('string', 'Topping');
			         data.addColumn('number', 'Slices');
			         data.addColumn('string', 'taskid');
			        
			        if(result == "0#@#0#@#0#@#0#@#0#@#0#@#0#@#0"){
			          $("#prjTaskGraph").html("No task found.");
			         }else{
			           
				         data.addRows([
						  	['Completed Task',parseInt(dataList[0]),dataList[4]],				  
						    ['Inprogress Task',parseInt(dataList[1]),dataList[5]],
						    ['Not completed Task',parseInt(dataList[2]),dataList[6]],
						    ['Incompleted Task',parseInt(dataList[3]),dataList[7]]
					     ]);
					 }
                // Set chart options
                 
                // Instantiate and draw our chart, passing in some options.
			     var chart = new google.visualization.PieChart(document.getElementById('prjTaskGraph'));
			     
			     //Chart onclick function starts
			     function prjTaskDetail() {
			        
          			var selectedItem = chart.getSelection()[0];
         			 if (selectedItem) {
         			    $("#loadingBar").show();
                    	timerControl("start");
         			 	var taskStatusHeader = data.getValue(selectedItem.row, 0);
         			 	var taskIds = data.getValue(selectedItem.row, 2);
         			 	//alert(taskIds);
            			$.ajax({
				          url: path+"/agileActionNew.do",
				          type:"POST",
				          data:{action:"loadChartTaskDetails",projectId:projectId,taskStatusHeader:taskStatusHeader,taskIds:taskIds},
				          error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
						},
				          success:function(result){
	          				checkSessionTimeOut(result);
	          				$('#chartDetailDiv').css('display','block');
	          				$("#taskDataDiv").html(result);
	            			$("#taskStatusHeader").html(taskStatusHeader+' Details');
		                    $('#transparentDiv').css('display','block');
	            			var landH=$('#chartDetailDiv').height();
							$('#taskDataDiv').css('height',landH-90+'px');
						    if(!isiPad){
						      popScroll('taskDataDiv');
						    }
						     $("#loadingBar").hide();
                    		 timerControl("");  
						   }
						});
         			 }
         		 }

       			 google.visualization.events.addListener(chart, 'select', prjTaskDetail);   
       			 
       			 //Chart onclick function ends
       			 var options = {'colors': ['#279230', '#FA7B14', '#DC3912','#FAC81A']};
			     chart.draw(data,options);
	          }
	      });
	 } 
	 
  function loadSprintGroupStatus(){
	   
	   var data;
		  $.ajax({
			url: path+"/ActivityFeedAction.do",
	    	type: "POST", 
	    	data: {act:"loadSprintGroupStatus", projectId:projectId}, 
	    	error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
	    	success:function(result) {
	    	   checkSessionTimeOut(result);
	    	   var data=result.split('#@#');
	    	   if(data[0] == '<xmlData></xmlData>'){
	    	       $("#sprintData").attr('align','center');
	    	       $("#sprintData").html("No sprint groups available.");
	    	   }else{
		    	   var count=data[1];
		    	   var sprintdivW=count*276;
		    	   var progressDivW=$('#AgileStoryDashBoardDiv').width();
		    	   
		    	  $("#sprintGpProgData").css('width',progressDivW);
		    	  // if(sprintdivW > progressDivW){
		    	     $("#sprintData").css('width',sprintdivW+7);
		    	  // }
		    	   $("#sprintData").html("");
		    	   $(data[0]).find("labelData").each(function(i){
				    	var groupName = $(this).find("groupName").text();
				    	var groupId = $(this).find("groupId").text();
				    	var taskStatusIcon = $(this).find("groupTaskStatusImg").text();
				    	var taskStatusName =$(this).find("groupTaskStatusName").text();
						var taskStatusPer =$(this).find("groupTaskPer").text();
						var grupName = groupName;
		    			if(groupName.length > 16){
		    				grupName = groupName.substring(0,15)+".."
		    			}
						data='<div id="pDiv" style="width:276px;border-right:1px solid #cccccc;height:120px;float:left">';
				                data=data +'<div title="'+groupName+'" style="font-size:15px;text-align:center;">'+grupName+'</div>';
				                 data=data+'<div align="center" style="height:70px;">';
				                 //if(taskStatusName == 'In progress' || taskStatusName == 'Not completed'){
				                   data=data+ '<div id='+groupId+' class="dashboardProgressDivs" style="height: 60px; margin-top: 10px;" ></div>';
				                 // }
				                 //else{
				                 // data=data+'<img id="planningStatus" style="height: 50px; margin-top: 10px;" src="'+path+'/'+taskStatusIcon+'"/></div>';
				                 //}
				                data=data +'<div style="font-size:13px;font-weight:normal;text-align:center;">'+taskStatusName+'</div>';
				                data=data +'</div>';
				        $("#sprintData").append(data);
				       
				        if(parseInt(taskStatusPer)>=50){
					         circleColor ="#418DCF";
					    }else{
					         circleColor ="#F66165"
					    }
					       
				    	$('#'+groupId ).progressCircle({
							nPercent        : taskStatusPer,
							showPercentText : true,
							thickness       : '8',
							circleSize      : '50',
							borderColor		: circleColor
						});
				        
					});
					$( "div#pDiv" ).last().css( 'border-right','0px solid #cccccc' );
					
				}
				
	    	   $('#loadingBar').hide();
	  		   timerControl("");
	    	}
	      });
	 }	

  function loadWorkLoad(){
	    
	    $('#loadingBar').show();
	  	timerControl("start");
		  $.ajax({
			url: path+"/agileActionNew.do",
	    	type: "POST", 
	    	data: {action:"loadWorkUserLoad", projectId:projectId}, 
	    	error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
	    	success:function(result) {
	    	    checkSessionTimeOut(result);
		    	var mainArray = [ ['','']];
				var subArray; 
				if(result=="<xmlData></xmlData>"){
		    	       subArray = new Array("",0);
						mainArray.push(subArray);
		    	}else{
			        
			    	$(result).find("labelData").each(function(i){
				    	var estimatedHours = $(this).find("estimatedHours").text();
				    	var userNames = $(this).find("taskUsers").text();
						subArray = new Array();
						subArray.push((userNames));
						subArray.push(parseInt(estimatedHours));
						mainArray.push(subArray);
					});
				}
		            var data = google.visualization.arrayToDataTable(mainArray);
		            var options = {
		             	  legend: 'none',
				          title: '',
				          vAxis: {title: TeamMembersLabel,  titleTextStyle: {color: 'black' ,bold: true}},
				          hAxis: {title: NumberOfHoursLabel,  titleTextStyle: {color: 'black' ,bold: true}}
				        };
			        var chart = new google.visualization.BarChart(document.getElementById('workloadGraph'));
			        chart.draw(data, options);
		        
	       
	            $('#loadingBar').hide();
	  			timerControl("");
	    	}
	  	});
	}	
	
	function loadTaskPerUser(){
	    
		$('#loadingBar').show();
	  	timerControl("start");
		  $.ajax({
			url: path+"/agileActionNew.do",
	    	type: "POST", 
	    	data: {action:"loadTaskPerUser", projectId:projectId}, 
	    	error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
	    	success:function(result) {
		    	checkSessionTimeOut(result);
		    	var mainArray= [['', sprintTaskLabel]];
		    	if(result=="<xmlData></xmlData>"){
			    		subArray = new Array("",0);
						mainArray.push(subArray);
			    }else{
			    	$(result).find("labelData").each(function(i){
				    	var estimatedHours = $(this).find("estimatedHours").text();
				    	var userNames = $(this).find("taskUsers").text();
				    	var sprintTask = $(this).find("sprintTaskCount").text();
						subArray = new Array();
						subArray.push((userNames));
						subArray.push(parseInt(sprintTask));
						mainArray.push(subArray);
					});
		    	}
		    	
		        
		        var data = google.visualization.arrayToDataTable(mainArray);
			    var options = {
					        legend: { position: 'right', maxLines: 2 },
							bar: { groupWidth: '75%' },
							vAxis: {title: numberOfTasksLabel,  titleTextStyle: {color: 'black',bold: true}},
				          	hAxis: {title: TeamMembersLabel,  titleTextStyle: {color: 'black',bold: true}},
					        isStacked: true,
			      };
				        
		         var chart = new google.visualization.ColumnChart(document.getElementById('taskUserGraph'));
		         options = {'colors': ['#279230', '#FF9900', '#DC3912']};
		         chart.draw(data, options);
		         $('#loadingBar').hide();
		  		 timerControl("");
	    	}
	  	});
	}
	
	function jiraStoryComment(projId,colStoryId,comment){
		var comment =  "Created by -  "+userFullName+" : "+comment
		//var comment = comment + " | Created by : "+userFullName;
		$.ajax({ 
			url: path+'/adminUserAction.do',
			type:"POST",
		 	data:{act:'addjirastorycomment',projId:projId,colStoryId:colStoryId,comment:comment,
		 	},
		error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
					},
		success:function(result){
		
		}
		});
	}
	
	 
	
	