

function emailPopupDiv(){
 var ui ="";

 ui='<div class="col-sm-12 col-xs-12 popUp popUpMain defaultCls" id="emailIntegrationPopup" style="display: none;margin-left: 28%">'
      +'<div class="col-sm-5 col-xs-10 popUp popUpContent " style="top:5%" id="emailIntegrationPopupContent">'
        +'<div class="popUpHeaderMainDiv col-sm-12 col-xs-12 defaultCls" style="padding-bottom: 1%;border-bottom: 1px solid #9c9c9c;">'
          +'<div style="width: 21%;float: left;padding-right: 3%;border-right: 1px solid #9c9c9c;">'
	            +'<img style="width: 100%;height: 40px; border-radius: 3px;" onerror="imageOnCompanyErrorReplace(this);"  id="companyImg"/ >'
	          +'</div>'  
          +'<div style="padding-left: 1.5%;padding-top: 9px;width: 68%;" id="textChange" class="col-sm-11 col-xs-11 newProjHeader defaultCls Email_Integration_cLabelText">'
          +'</div>' 
          +'<img id="changeClsImage" class="popUpCloseIcon" width="10px" height="10px" src="${path}/images/close.png"  onClick="closeEmailPopup();" title="Close"/>'
	      +'<img id="emailConfigInfo" class= "infoCss"  style = "float: left; cursor:pointer; margin: -2px 0 0 20px;" src="${path}/images/info.png"/>'
	    +'</div>'
	    +'<div  style="width:100%;float:left;height: 472px;padding-top: 2%;padding-right: 2%;border-bottom: 1px solid #9c9c9c;padding-bottom: 2%;">'
	       
	       +'<div id="emailConfigContent" style="width: 100%;float: left;">'
	        
	       +'<div style="width: 99%;float: left;margin-left: 1%;border-bottom: 1px solid #9c9c9c;">'
		      +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	 +'<div style="float:left;width: 100%;">'
					+'<div style="float: left;margin-top: 1px;width: 5%;"><input type="radio" value="colabusConfig" name="fetchEmailConfig" onchange="emailConfigType(this);" id="colEmailConfig"></input> </div>'
					+'<div class="Use_Colabus_Email_cLabelHtml" style="color:#484848;float: left;font-size: 16px;width: 63%;font-family: opensanscondbold;border-right: 1px solid #ccc;margin-right: 3%;"> </div>'
				    
				    +'<div style="float: left;width:14%;display: none;" id="configDiv">'
				       +'<div style="float:left;margin-top: 3px;margin-right: 10%; ">'
				          +'<div class="roundImgGreenConfig"></div>'
				       +'</div>'
				       +'<div class="configemailLabel Status_cLabelHtml"> </div>'
				    +'</div>'
				    
				     +'<div style="float: left;width:14%;display: none;" id="notconfigDiv">'
				       +'<div style="float:left;margin-top: 3px;margin-right: 10%; ">'
				          +'<div class="roundImgRedConfig"></div>'
				       +'</div>'
				       +'<div class="configemailLabel Status_cLabelHtml"> </div>'
				    +'</div>'
				 +'</div>'
	          +'</div>'
	          
	          +'<div style="float: left;width: 100%;padding-left: 6%;">'
		        +'<div style="width: 85%;float: left;padding-right: 2%;">'
		         +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	  +'<div style="width: 100%;float: left;">'
	                 +'<input type="text" class="form-control" style="height: 30px;" id="colabusEmailId" placeholder="eg: abc@colabusmail.com">'
	               +'</div>'
	             +'</div>'
	           +'</div> '
	           +'<img id="configuredEmailImg" src=""  value=""  style="width:27px;margin-top:1px; cursor:pointer; margin-left: 2%;">'
	             
	            +'<div class="form-group" style="float: left;width: 83%;padding-left: 0;margin-bottom: 10px;">'
          	       +'<label style="width: 40%;float: left;font-weight: normal;margin-top: 1px;color: #000"  class="projPopLabel Email_data_fetch_type_cLabelHtml" > </label>'
         	       +'<div style="float:left;width: 35%;">'
				      +'<div style="float: left;margin-top: 1px;" ><input type="radio" value="ColspecifedDate" name="ColfetchEmail" checked="checked" onchange="colemailFetchType(this);" id="ColfetchEmailsFromDate"></input> </div>'
				      +'<div class="Specify_date_cLabelHtml" id="colspecifyDateL" style="margin-top:2px;float: left;font-size: 13px; overflow: hidden; height: 16px; margin-left: 5px;\"> </div>'
			       +'</div>'
			    
			       +'<div style="float:left;">'
				      +'<div style="float: left;margin-top: 1px;"><input type="radio" value="ColcompleteEmails" name="ColfetchEmail" onchange="colemailFetchType(this);" id="ColfetchAllEmailDetails"></input> </div>'
				      +'<div id="colspecifyDateL" class="All_cLabelHtml" style="margin-top:2px;float: left;font-size: 13px;overflow: hidden; height: 16px; margin-left: 5px;\"> </div>'
			       +'</div>'
         	    +'</div>'
              +'</div>'
              
             
              
	        +'</div>'
            
            +'<div style="width: 83.1%;float: left;margin-left: 1%;"> '
		      
		      
		       +'<div class="form-group" style="float: left;width: 100%;margin-bottom: 10px;padding-left: 0;margin-top: 2%;">'
	          	 +'<div style="float:left;width: 100%;">'
					+'<div style="float: left;margin-top: 1px;width: 6%;"><input type="radio" value="customConfig" name="fetchEmailConfig" onchange="emailConfigType(this);" id="custEmailConfig"></input> </div>'
					+'<div class="Use_Other_Email_cLabelHtml" style="color:#484848;float: left;font-size: 16px;width: 63%;font-family: opensanscondbold; margin-right: 3%;">Use Other Email</div>'
				 +'</div>'
	          +'</div>'
	          
	          +'<div style="float: left;width: 100%;padding-left: 7%;">'
		         +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	   +'<div style="width: 100%;float: left;">'
	                 +'<input type="text" style="height: 30px;" class="form-control" id="customEmailId" placeholder="Email Host eg: abc@example.com">'
	               +'</div>'
	             +'</div>'
	            
	             +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	   +'<div style="width: 100%;float: left;">'
	                 +'<input class="password_cLabelPlaceholder" type="password" style="height: 30px;" class="form-control" id="customEmailPassword" placeholder=" ">'
	               +'</div>'
	             +'</div>'
	             
	             +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	    +'<div style="width: 100%;float: left;">'
	                  +'<input class="HOST_NAME_cLabelPlaceholder" type="text" style="height: 30px;" class="form-control" id="customEmailMailServer" placeholder=" ">'
	                +'</div>'
	             +'</div>'
	             
	             +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	    +'<div style="width: 100%;float: left;">'
	          	       +'<select class="form-control"  style="height:30px;padding: 0 12px;" id="customEmailActType">'
						  +'<option class="ACCOUNT_TYPE_cLabelHtml" value=""> </option>'
						  +'<option value="pop3">pop3</option>'
					      +'<option value="imaps">imaps</option>'
			           +'</select>'
	                +'</div>'
	             +'</div>'
	             
	             +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	    +'<div style="width: 100%;float: left;">'
	                  +'<input type="text" style="height: 30px;" class="form-control" id="customEmailPortNumber" placeholder="Port">'
	                +'</div>'
	             +'</div>'
	             
	              +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;">'
	          	    +'<div style="width: 100%;float: left;">'
	          	       +'<select class="form-control" style="height:30px;padding: 0 12px;" id="customEmailSecurityType">'
						  +'<option class="Security_type_cLabelHtml" value="none">Security Type</option>'
				          +'<option value="SSL/TLS">SSL/TLS</option>'
				          +'<option value="SSL/TLS (Accept all certificates)">SSL/TLS (Accept all certificates)</option>'
				          +'<option value="STARTTLS">STARTTLS</option>'
				          +'<option value="STARTTLS (Accept all certificates)">STARTTLS (Accept all certificates)</option>'
			           +'</select>'
	                +'</div>'
	             +'</div>'
	             
	            +'<div style="float: left;width: 100%;">'
	                 +'<div id="emailTest" style="background-color: #7CE20F !important;margin-top: 0px;float: left;padding: 3px 20px !important;" class="text-uppercase createBtn Test_cLabelHtml" onclick="testEmailConnection('');"> </div>'
	            +'</div>'
            
                +'<div class="form-group" style="float: left;width: 100%;padding-left: 0;margin-bottom: 10px;margin-top: 10px;">'
          	       +'<label class="Email_data_fetch_type_cLabelHtml" style="width: 40%;float: left;font-weight: normal;margin-top: 1px;color: #000"  class="projPopLabel" > </label>'
         	       +'<div style="float:left;width: 35%;">'
				      +'<div style="float: left;margin-top: 1px;" id="d12"><input type="radio" value="currentDate" name="fetchEmail" checked="checked" onchange="custemailFetchType(this);" id="fetchEmailsFromDate"></input> </div>'
				      +'<div id="specifyDateL" class="Specify_date_cLabelHtml" style="margin-top:2px;float: left;font-size: 13px; overflow: hidden; height: 16px; margin-left: 5px;\"> </div>'
			       +'</div>'
			    
			       +'<div style="float:left;">'
				      +'<div style="float: left;margin-top: 1px;" id="d11"><input type="radio" value="completeEmails" name="fetchEmail" onchange="custemailFetchType(this);" id="fetchAllEmailDetails"></input> </div>'
				      +'<div id="specifyDateL" class="All_cLabelHtml" style="margin-top:2px;float: left;font-size: 13px;overflow: hidden; height: 16px; margin-left: 5px;\"> </div>'
			       +'</div>'
         	    +'</div>'
         	    
         	    
	            
         	    
         	   +'</div>'
	         +'</div>'
	         
          +'</div>'
	    +'</div>'
	    +'<div style="float: left;width: 100%;">'
	      +'<div id="emailConfigSave" class="text-uppercase createBtn SAVE_cLabelHtml" onclick="saveEmailConfig();"> </div>'
	     +'</div>'
	  +'</div>'
	+'</div>'
$("#integationListDiv").html(ui);

}


function checkEmailConfig(){

	alert("yrsdy");
}