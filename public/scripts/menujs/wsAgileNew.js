var localOffsetTime="";
var menuutype="";
var glbepicid="";
(function ($) {
    menuutype="agile";
    localOffsetTime=getTimeOffset(new Date());
    
    $("body").click(function(event ){
        var $target = $(event.target);
        if(!$target.is("#agileviewdownloadfileinconversation") && !$target.parents().is("#agileviewdownloadfileinconversation") && !$target.is("#agileidviewordownloadUi") && !$target.parents().is("#agileidviewordownloadUi")){
            $("#agileviewdownloadfileinconversation").remove();
        }
        if(!$target.is("#agileviewthumbnail") && !$target.parents().is("#agileviewthumbnail") && !$target.is("#agileidthumbnailOpenUi") && !$target.parents().is("#agileidthumbnailOpenUi")){
            $("#agileviewthumbnail").remove();
        }
        
        if(!$target.is(".epicIconPrioritycls") && !$target.parents().is(".epicIconPrioritycls")){
            $('.epicIconPrioritycls').find(".agileMoreOption2").removeClass('d-block').addClass('d-none');
        }
        if(!$target.is(".statusDivcls") && !$target.parents().is(".statusDivcls")){
            $('.statusDivcls').find(".agileMoreOption2").removeClass('d-block').addClass('d-none');
        }
        if(!$target.is(".liststatus") && !$target.parents().is(".liststatus") && !$target.is(".agileMoreOption1") && !$target.parents().is(".agileMoreOption1")){
            $("body").find(".agileMoreOption1").removeClass('d-block').addClass('d-none');
        }
        
    }); 

    function init() {
        $('.easy-tree1').EasyTree({
            addable: true,
            editable: true,
            deletable: true
        });
    }
  
    window.onload = init();
    listAgileEpics();
    listUnorgStories();
  
})(jQuery)

function nodeDragnDrop(){
    var nestedSortables = [].slice.call(document.querySelectorAll('.mytree'));
    var sortables = [];
    for(var i=0; i< nestedSortables.length; i++){
      sortables[i] = new Sortable(nestedSortables[i], {
        //group: 'nested',
        group: {
            name: 'shared',
            put: false
        },
        put: false,
        animation: 150,
        fallbackOnBody: true,
        swapThreshold: 0.65,
      });
    }
}
function nodeDragnDrop2(){
    var nestedSortables = [].slice.call(document.querySelectorAll('.mytree2'));
    var sortables = [];
    for(var i=0; i< nestedSortables.length; i++){
      sortables[i] = new Sortable(nestedSortables[i], {
        group: 'nested',
        put: false,
        animation: 150,
        fallbackOnBody: true,
        swapThreshold: 0.65,
      });
    }
} 
function nodeDragnDrop3(){
    var nestedSortables = [].slice.call(document.querySelectorAll('.mytree3'));
    var sortables = [];
    for(var i=0; i< nestedSortables.length; i++){
      sortables[i] = new Sortable(nestedSortables[i], {
        group: 'nested',
        put: false,
        animation: 150,
        fallbackOnBody: true,
        //filter: ".stop",
        swapThreshold: 0.65,
      });
    }
}


function listAgileEpics(){
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    let jsonbody = {
        "user_id":userIdglb,
        "project_id":prjid,
        "company_id":companyIdglb,
        "sortVal":"",
        "searchTxt":"",
        "localOffsetTime":localOffsetTime
    }  
    $.ajax({
        url: apiPath+"/"+myk+"/v1/getEpics",
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){
            $('#agilelistUL_0').append(prepareAgileUI(result));
            var h1 = $('#content').height(); var h2 = $('.agileHeader').height();
            var high = h1-h2;
            if($('#agilelistUL_0').children('li').length < 5){
                $('#vline').css('height',high);
            }else{
                $('#vline').css('height','auto');
            }
            nodeDragnDrop();
            nodeDragnDrop2();
            nodeDragnDrop3();

            var epicvalues = "";
            var value1 = "";
            var inception = "";
            var Construction = "";
            var completed1 = "";

            for(var i=0;i<result.length;i++){
                epicvalues = $('#epicvalue_'+result[i].epic_id).val();
                value1 = epicvalues.split(',');
                inception = value1[0].split(':')[0];
                Construction = value1[1].split(':')[0];
                completed1 = value1[2].split(':')[0];
                $('#valueinception_'+result[i].epic_id).attr('epicval',inception);
                $('#valueconstruction_'+result[i].epic_id).attr('epicval',Construction);
                $('#valuecompleted_'+result[i].epic_id).attr('epicval',completed1);

                $('.EFbacklogspan_'+result[i].epic_id).text(result[i].backlogCount);
                $('.EFinprogressspan_'+result[i].epic_id).text(result[i].assignedCount);
                $('.EFblockedspan_'+result[i].epic_id).text(result[i].blockedCount);
                $('.EFholdspan_'+result[i].epic_id).text(result[i].holdCount);
                $('.EFdonespan_'+result[i].epic_id).text(result[i].doneCount);
                $('.EFcancelspan_'+result[i].epic_id).text(result[i].cancelCount);

            }
            
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });    
}

function listUnorgStories(){
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    let jsonbody = {
        "user_id":userIdglb,
        "project_id":prjid,
        "company_id":companyIdglb,
        "sortVal":"",
        "searchTxt":"",
        "localOffsetTime":localOffsetTime
    }  
    $.ajax({
        url: apiPath+"/"+myk+"/v1/unorganizedStory",
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){
            
            $('#unorganizedAgile').append(storiesUi(result,'unorganized',0));
            nodeDragnDrop3();
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });   
}

async function updateDraggedorder(epicid,place,storyplace){
    var parentid = "";var type="";var rootid="";var moveplace="";
    if(storyplace=="underepicandfeature"){
        var checkFtype1 = $('#orgAgile_'+epicid).attr('epictype');
        var checkFtype2 = $('#orgAgile_'+epicid).parents('li').attr('epictype');
        if(checkFtype1 == "F" && checkFtype2 == "F"){
            parentid=$('#orgAgile_'+epicid).attr('parentid');
            var copydiv = $('#orgAgile_'+epicid).clone();
            $('#orgAgile_'+epicid).remove();
            $('#agileStoriesUL_'+parentid).append(copydiv);
            alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
        }else if(($('#orgAgile_'+epicid).parents().attr('id')=="storyBoxes") || ($('#storyDiv_'+epicid).parents().attr('id')=="storyBoxes" && $('#storyDiv_'+epicid).parents().parents().attr('id')=="unorganizedAgile")){
            if($('#orgAgile_'+epicid).attr('epictype')=="F"){
                var copylist = $('#orgAgile_'+epicid).clone();
                var pid = $('#orgAgile_'+epicid).attr('parentid');
                $('#orgAgile_'+epicid).remove();
                $('#agileStoriesUL_'+pid).append(copylist);
                alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
            }else{
                var orderdata = getOrderUnorg();
                let jsonbody = {
                    "user_id": userIdglb,
                    "parent_epic_id" : 0,
                    "epic_id" : epicid,
                    "company_id" : companyIdglb,
                    "localOffsetTime" : localOffsetTime,
                    "order":JSON.parse(orderdata)
                }
                $('#loadingBar').addClass('d-flex').removeClass('d-none');
                $.ajax({
                    url: apiPath + "/" + myk + "/v1/updateEpicStoryOrder",
                    type: "PUT",
                    dataType: 'json',
                    contentType: "application/json",
                    data: JSON.stringify(jsonbody),
                    error: function (jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR, textStatus, errorThrown);
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');
                    },
                    success: function (result) { 
                        var oldparentid="";
                        if($('#orgAgile_'+epicid).parents().attr('id')=="storyBoxes"){
                            oldparentid = $('#orgAgile_'+epicid).attr('parentid');
                            if($('#agileStoriesUL_'+oldparentid).children('li').length==0){
                                $('#bgBoxE_'+oldparentid).remove();
                                $('#orgAgileDiv_'+oldparentid).removeAttr('onclick');
                            }
                            $('#orgAgile_'+epicid).replaceWith(multiplestories(result,'unorganized'));
                        }else{
                            oldparentid = $('#storyDiv_'+epicid).attr('parentid');
                            if($('#agileStoriesUL_'+oldparentid).children('div').length==0){
                                $('#bgBoxE_'+oldparentid).remove();
                                $('#orgAgileDiv_'+oldparentid).removeAttr('onclick');
                            }
                            $('#storyDiv_'+epicid).replaceWith(multiplestories(result,'unorganized'));
                        }
                        
                        $('#loadingBar').addClass('d-none').removeClass('d-flex');
                    }
                }); 
            }
                   

        }else{
            if($('#orgAgile_'+epicid).attr('epictype')=="E"||$('#orgAgile_'+epicid).attr('epictype')=="F"||$('#orgAgile_'+epicid).attr('epictype')=="S"){
                parentid=$('#orgAgile_'+epicid).parent('ul').attr('id').split('_')[1];//.attr('parentid');
                type=$('#orgAgile_'+epicid).attr('epictype');
                rootid=$('#orgAgile_'+epicid).parent('ul').attr('rootid');
                $('#orgAgile_'+epicid).attr('parentid',parentid);
                if(rootid==0){
                    $('#orgAgile_'+epicid).attr('rootid',parentid);
                }else{
                    $('#orgAgile_'+epicid).attr('rootid',rootid);
                }    
            }else{
                parentid=$('#storyDiv_'+epicid).parents('ul').attr('id').split("_")[1];
                rootid=$('#storyDiv_'+epicid).parents('ul').attr('rootid');
                type="S";
                $('#storyDiv_'+epicid).attr('parentid',parentid);
                if(rootid==0){
                    $('#storyDiv_'+epicid).attr('rootid',parentid);
                }else{
                    $('#storyDiv_'+epicid).attr('rootid',rootid);
                }
            }
            if(rootid==0){
                moveplace="underEpic";
            }else{
                moveplace="underFeature";
            }
            console.log(rootid+"<--rootid-111111-parentId-->"+parentid);
            var orderdata = getOrder(place,parentid,type);
            let jsonbody = {
                "user_id": userIdglb,
                "parent_epic_id" : parentid,
                "epic_id" : epicid,
                "company_id" : companyIdglb,
                "localOffsetTime" : localOffsetTime,
                "order":JSON.parse(orderdata)
            }
            $('#loadingBar').addClass('d-flex').removeClass('d-none');
            console.log("jsonbody-->"+JSON.stringify(jsonbody));
            await $.ajax({
                url: apiPath + "/" + myk + "/v1/updateEpicStoryOrder",
                type: "PUT",
                dataType: 'json',
                contentType: "application/json",
                data: JSON.stringify(jsonbody),
                error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
                
                },
                success: function (result) { 
                    console.log("result-->"+result);
                    if(type!="E"){
                        if(moveplace=="underEpic"){
                            if($('#agileStoriesUL_'+parentid).children().length < 2){
                                $('#orgAgileDiv_'+parentid).append("<div id='bgBoxE_"+parentid+"' class='backgroundBox p-0 rounded' style='background-color: #d89cd8;cursor:pointer;'></div>");
                                $('#orgAgileDiv_'+parentid).attr('onclick','showFeaturesNstories('+parentid+',\"E\");event.stopPropagation();');
                                //showFeaturesNstories(parentid,"");
                                $('#storyDiv_'+epicid).remove();
                                $('#orgAgileDiv_'+parentid).trigger('click');
                            }else{
                                $('#storyDiv_'+epicid).replaceWith(prepareAgileUI(result));
                            }
                            
                        }else if(moveplace=="underFeature"){
                            
                            if($('#agileStoriesUL_'+parentid).children().length < 2){
                                $('#orgAgileDiv_'+parentid).append("<div id='bgBoxE_"+parentid+"' class='backgroundBox p-0 rounded' style='background-color: #d89cd8;cursor:pointer;'></div>");
                                $('#orgAgileDiv_'+parentid).attr('onclick','showFeaturesNstories('+parentid+',\"F\");event.stopPropagation();');
                                showFeaturesNstories(parentid,"F");
                                $('#orgAgile_'+epicid).remove();
                                $('#orgAgileDiv_'+parentid).trigger('click');
                                //$("[id^='storyDiv_']").css('display','block');
                            }else{
                                $('#storyDiv_'+epicid).remove();
                                $('.epicStory_'+parentid).append(multiplestories(result,'organized'));
                                //$('#storyDiv_'+result[0].epic_id).show();
                                //$("[id^='storyDiv_']").css('display','block');
                                $('#orgAgile_'+epicid).remove();
                            }
                           
                        }
                    }
                    
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');
                }
                
            });
        }
        copyUnorgFeature="";
        
    }else{
        var checkFtype1 = $('#storyDiv_'+epicid).attr('epictype');
        var checkFtype2 = $('#storyDiv_'+epicid).parents('li').attr('epictype');
        console.log(checkFtype1+"<------->"+checkFtype2);
        var place2 = typeof(checkFtype2)=="undefined"?"unorganized":"organized";
        if(checkFtype1 == "F" && checkFtype2 == "F"){
            alertFunNew(getValues(companyAlerts,"Alert_reorderNotPermissible"),'error');
            $('#storyDiv_'+epicid).remove();
            $('#storyBoxes').prepend(copyUnorgFeature);
        }else{
            updateOrgStories(epicid,place2,storyplace);
        }
        copyUnorgFeature="";
    } 
        
    
}
function getOrder(place,parentid,type){
    var id="";
    var order="";var pid="";var rid="";
    var jsonvalue = '[';
    if(type=="E"){
        $('#agilelistUL_0').find('.agileMainDivCls').each(function(){
            id = $(this).attr('id').split('_')[1];
            order = $(this).index();
            pid = $(this).attr('parentid');
            rid = $(this).attr('rootid');
            jsonvalue+="{";
            jsonvalue+="\"epic_id\":\""+id+"\",";
            jsonvalue+="\"epic_order\":\""+order+"\",";
            jsonvalue+="\"parent_epic_id\":\""+pid+"\",";
            jsonvalue+="\"root_epic_id\":\""+rid+"\"";
            jsonvalue+="},";
            
        });
    }else{
        $('#agileStoriesUL_'+parentid).find('.agileMainDivCls').each(function(){
            id = $(this).attr('id').split('_')[1];
            order = $(this).index();
            pid = $(this).attr('parentid');
            rid = $(this).attr('rootid');
            jsonvalue+="{";
            jsonvalue+="\"epic_id\":\""+id+"\",";
            jsonvalue+="\"epic_order\":\""+order+"\",";
            jsonvalue+="\"parent_epic_id\":\""+pid+"\",";
            jsonvalue+="\"root_epic_id\":\""+rid+"\"";
            jsonvalue+="},";
            
        });
    }
    
    var jsonresult="";
    if(jsonvalue.length>1){
      jsonresult = jsonvalue.substring(0, jsonvalue.length-1);
      jsonresult=jsonresult+"]";
    }else{
      jsonresult ="[]";
    }
    console.log("jsonresult--->"+jsonresult);
    return jsonresult;
  }

async function updateOrgStories(epicid,place,storyplace){
    var parentId="";
    var rootid="";
    var moveplace="";var orderdata="";
    if(place=="organized"){
        parentId=$('#storyDiv_'+epicid).parents('ul').attr('id').split('_')[1];
        rootid=$('#orgAgile_'+parentId).attr('rootid');
    }else{
        parentId=$('#storyDiv_'+epicid).attr('parentid');
        rootid=$('#storyDiv_'+epicid).attr('rootid');
    }
    
    if(typeof(rootid)=="undefined"||rootid=="undefined"){
        moveplace="";
    }else if(rootid==0){
        moveplace="underEpic";
        rootid=parentId;
    }else{
        moveplace="underFeature";
    }
    $('#storyDiv_'+epicid).attr('parentid',parentId);
    $('#storyDiv_'+epicid).attr('rootid',rootid);
    console.log(moveplace+"<--moveplace-->"+rootid+"<--rootid--parentId-->"+parentId);
    if(place=="organized"){
        orderdata = getOrder("",parentId,"S",rootid);
    }else{
        orderdata = getOrderUnorg();
    }
    
    let jsonbody = {
        "user_id": userIdglb,
        "parent_epic_id" : parentId,
        "epic_id" : epicid,
        "company_id" : companyIdglb,
        "localOffsetTime" : localOffsetTime,
        "order":JSON.parse(orderdata)
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
	
    await $.ajax({
        url: apiPath + "/" + myk + "/v1/updateEpicStoryOrder",
        type: "PUT",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        
        },
        success: function (result) { 
            if(moveplace=="underEpic"){
                if($('#agileStoriesUL_'+parentId).children().length < 2){
                    $('#orgAgileDiv_'+parentId).append("<div id='bgBoxE_"+parentId+"' class='backgroundBox p-0 rounded' style='background-color: #d89cd8;cursor:pointer;'></div>");
                    $('#orgAgileDiv_'+parentId).attr('onclick','showFeaturesNstories('+parentId+',\"E\");event.stopPropagation();');
                    showFeaturesNstories(parentId,"E");
                    $('#storyDiv_'+epicid).remove();
                    //$('#orgAgileDiv_'+parentId).trigger('click');
                }else{
                    $('#storyDiv_'+epicid).replaceWith(prepareAgileUI(result));
                }
                
            }else if(moveplace=="underFeature"){
                if($('#agileStoriesUL_'+parentId).children().length < 2){
                    $('#orgAgileDiv_'+parentId).append("<div id='bgBoxE_"+parentId+"' class='backgroundBox p-0 rounded' style='background-color: #d89cd8;cursor:pointer;'></div>");
                    $('#orgAgileDiv_'+parentId).attr('onclick','showFeaturesNstories('+parentId+',\"F\");event.stopPropagation();');
                    showFeaturesNstories(parentId,"F");
                    $('#storyDiv_'+epicid).remove();
                    $('#orgAgileDiv_'+parentId).trigger('click');
                }else{
                    $('#storyDiv_'+epicid).remove();
                    $('.epicStory_'+parentId).append(multiplestories(result,'organized'));
                    //$('#storyDiv_'+result[0].epic_id).show();
                }
               
            }
            nodeDragnDrop();
            nodeDragnDrop2();
            nodeDragnDrop3();
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
        
    });
}  

function getOrderUnorg(){
    var id="";
    var order="";var pid=0;var rid=0;
    var jsonvalue = '[';
    $('#unorganizedAgile').find('#storyBoxes').find('.agileMainDivCls').each(function(){
        id = $(this).attr('id').split('_')[1];
        order = $(this).index();
        jsonvalue+="{";
        jsonvalue+="\"epic_id\":\""+id+"\",";
        jsonvalue+="\"epic_order\":\""+order+"\",";
        jsonvalue+="\"parent_epic_id\":\""+pid+"\",";
        jsonvalue+="\"root_epic_id\":\""+rid+"\"";
        jsonvalue+="},";
         
    });
    
    var jsonresult="";
    if(jsonvalue.length>1){
      jsonresult = jsonvalue.substring(0, jsonvalue.length-1);
      jsonresult=jsonresult+"]";
    }else{
      jsonresult ="[]";
    }
    console.log("jsonresultgetOrderUnorg--->"+jsonresult);
    return jsonresult;
}

var copyUnorgFeature="";
var dragtoUnorg="";
function checkFeature(epicid,epictype,place){
    if(place == "unorganized"){
        copyUnorgFeature = $('#storyDiv_'+epicid).clone();
    }else if(place == "organized"){
         
    }
}

function prepareAgileUI(result,viewtype){
    var ui="";
    var epicid = "";
    var epiconclick="";var clkpointer="";var bgcolor="";var parentid="";var rootid="";var typeimage="";var typeimgtitle="";
    var statusimage = "";
    var status="";
    var viewtypeH = "";var viewtypeScr = "";
    for(var i=0;i<result.length;i++){
        epicid=result[i].epic_id;
        status = result[i].epic_status;
        statusimage = status=="Backlog"?"/images/idea_old/Backlog.png":status=="Blocked"?"/images/idea_old/Blocked.png":status=="Hold"?"/images/idea_old/Hold.png":status=="Done"?"/images/idea_old/Done.png":status=="Cancel"?"/images/idea_old/cancel.png":status=="In Progress"?"/images/idea_old/Assigned.png":"";
    
        if(result[i].storyCount != "0"){
            epiconclick="showFeaturesNstories("+epicid+",'"+result[i].epic_type+"','"+result[i].epicFeatureStagesList+"');";
            clkpointer="cursor:pointer;";
        }else{
            epiconclick="";
            clkpointer="";
        }   
        bgcolor = result[i].epic_type=="E" ? "#eeabb6" : result[i].epic_type=="F" ? "#d89cd8" : "#f6b847"; 
        parentid = result[i].parent_epic_id;
        rootid = result[i].root_epic_id;
        typeimage = result[i].epic_type=="E" ? "images/agile/epic.svg" : result[i].epic_type=="F" ? "images/agile/feature.svg" : "images/agile/story.svg"; 
        typeimgtitle = result[i].epic_type=="E" ? "Epic" : result[i].epic_type=="F" ? "Feature" : "Story"; 
        viewtypeH = viewtype=="expandall"&&result[i].epic_type=="F"?"height: auto;max-height: 250px;overflow: auto;":"";
        viewtypeScr = viewtype=="expandall"&&result[i].epic_type=="F"?"wsScrollBar":"";

        ui+="<li id='orgAgile_"+epicid+"' epictype='"+result[i].epic_type+"' parentid="+parentid+" rootid="+rootid+" ondragstart=\"checkFeature("+epicid+",'"+result[i].epic_type+"','organized');\" ondragend=\"updateDraggedorder("+epicid+",'organized','underepicandfeature');event.stopPropagation();\" class='my-4 p-0 rounded agileMainDivCls' style='position:relative;'>"
            +"<div id='orgAgileDiv_"+epicid+"' class='d-flex flex-column align-items-around rounded' onclick=\""+epiconclick+"event.stopPropagation();\" style='background-color:"+bgcolor+";width: 175px;border: 1px solid black;height:103px;"+clkpointer+"'>"    //ondblclick=\"createAgile("+epicid+",'','edit','"+result[i].epic_type+"','');event.stopPropagation();\" 
                +"<div class='d-flex align-items-center pt-1'>"
                    +"<div class='' style='padding-left:5px;'><img src='"+typeimage+"' title='"+typeimgtitle+"' style='width:20px;height:20px;'></div>"
                    if(result[i].epic_type != "S"){
                        ui+="<div class='ml-auto position-relative' onmouseover=\"showStoriescounts(this);event.stopPropagation();\" onmouseout=\"hideStoriescounts(this);event.stopPropagation();\" title='Stories'><img src='images/agile/story.svg' style='width:20px;height:20px;'>"
                            ui+=storiesCountUI(epicid)
                        ui+="</div>"
                        +"<div class='position-relative agileoptionsdiv ml-1'><img src='images/conversation/three_dots.svg' onclick='showAgileOption("+epicid+");event.stopPropagation();' class='' style='width:20px;height:20px;cursor:pointer;'>"
                    }else{
                        ui+="<div class='position-relative agileoptionsdiv ml-auto'><img src='images/conversation/three_dots.svg' onclick='showAgileOption("+epicid+");event.stopPropagation();' class='' style='width:20px;height:20px;cursor:pointer;'>"
                    }
                    if(result[i].epic_type == "S"){
                        ui+=optionsUI(epicid,'organized',parentid,'',result[i].epic_title,result[i].epic_point)
                    }else{
                        ui+=optionsEFui(epicid,result[i].epic_type,parentid,'organized',result[i].epic_title)
                    }
                    ui+="</div>"
                +"</div>"
                +"<div class='pt-1' style='height:40px;'>"
                    +"<span class='border-0 defaultExceedMultilineCls' style='-webkit-line-clamp: 2 !important;padding: 0px 5px;font-size:12px;color:black;height: 95%;' title=\""+result[i].epic_title+"\">"+result[i].epic_title+"</span>"
                +"</div>"
                +"<div class='d-flex align-items-center p-1 ' >"
                    +"<div class='ml-auto position-relative liststatus' onclick=\"showStatusupdate(this,"+epicid+");event.stopPropagation();\"><img id='agileliststat_"+epicid+"' src='"+statusimage+"' style='width:20px;height:20px;'>"
                        ui+=statusUpdate(epicid,'agilelists')
                    ui+="</div>"
                +"</div>"
            +"</div>"    
            +"<input id='epicvalue_"+epicid+"' type='hidden' value='"+result[i].epicFeatureStagesList+"'>"
            if(result[i].storiesData!=""){
                ui+="<div id='bgBoxE_"+epicid+"' class='backgroundBox p-0 rounded' style='background-color: "+bgcolor+";cursor:pointer;'></div>"
            }else if(result[i].storyCount != "0" && typeof(result[i].storiesData)=="undefined"){
                ui+="<div id='bgBoxE_"+epicid+"' class='backgroundBox p-0 rounded' style='background-color: "+bgcolor+";cursor:pointer;'></div>"
            }
            if(typeof(result[i].storiesData)!="undefined"){
                ui+="<ul id='agileStoriesUL_"+epicid+"' parentid="+parentid+" rootid="+rootid+" organized='organized' class='mytree1 mytree2 p-0 "+viewtypeScr+"' level=2 style='padding: 0px 0px 0px 80px !important;"+viewtypeH+"'>"
                if(typeof(result[i].storiesData)!="undefined" && result[i].epic_type=="E"){
                    ui+=prepareAgileUI(result[i].storiesData,"expandall");
                }else if(typeof(result[i].storiesData)!="undefined" && result[i].epic_type=="F"){
                    ui+=storiesUi(result[i].storiesData,"organized",epicid);
                }
                ui+="</ul>"
            }else if(result[i].epic_type != "S"){
                ui+="<ul id='agileStoriesUL_"+epicid+"' parentid="+parentid+" rootid="+rootid+" organized='organized' class='mytree1 mytree2 p-0' level=2 style='padding: 0px 0px 0px 80px !important;'>"
                +"</ul>"
            }

        ui+="</li>"

    }    
    return ui;
}

function storiesCountUI(id){
    var ui="";

    ui+="<div id='' class='d-none align-items-center position-absolute storycountcls rounded p-0' style='font-size:11px;'>"
        +"<div id='' class='py-1 px-2 d-flex align-items-center'><img title='Backlog' class='headerStoryimages' src='/images/idea_old/Backlog.png' style=''><span id='' class='EFbacklogspan_"+id+" smallfonts pl-1' style=''></span></div>"
        +"<div id='' class='py-1 px-2 d-flex align-items-center'><img title='In Progress' class='headerStoryimages' src='/images/idea_old/Assigned.png' style=''><span id='' class='EFinprogressspan_"+id+" smallfonts pl-1' style=''></span></div>"
        +"<div id='' class='py-1 px-2 d-flex align-items-center'><img title='Blocked' class='headerStoryimages' src='/images/idea_old/Blocked.png' style=''><span id='' class='EFblockedspan_"+id+" smallfonts pl-1' style=''></span></div>"
        +"<div id='' class='py-1 px-2 d-flex align-items-center'><img title='Hold' class='headerStoryimages' src='/images/idea_old/Hold.png' style=''><span id='' class='EFholdspan_"+id+" smallfonts pl-1' style=''></span></div>"
        +"<div id='' class='py-1 px-2 d-flex align-items-center'><img title='Done' class='headerStoryimages' src='/images/idea_old/Done.png' style=''><span id='' class='EFdonespan_"+id+" smallfonts pl-1' style=''></span></div>"
        +"<div id='' class='py-1 px-2 d-flex align-items-center'><img title='Cancel' class='headerStoryimages' src='/images/idea_old/cancel.png' style=''><span id='' class='EFcancelspan_"+id+" smallfonts pl-1' style=''></span></div>"
    +"</div>"

    return ui;
}

function showStoriescounts(obj){
    $(obj).children('div').addClass('d-block').removeClass('d-none');
}

function hideStoriescounts(obj){
    $(obj).children('div').removeClass('d-block').addClass('d-none');
    $('.storycountcls').removeClass('d-block').addClass('d-none');
}

function showFeaturesNstories(epicId,epicType,stagelist){
    
    if(epicType == "F"){
        if($('#agileStoriesUL_'+epicId).children('div').length == 0){
            glbepicid=epicId;
            $('#orgAgileHeader , #organizedAgile').removeClass('w-25').addClass('w-75');
            $('#unorgAgileHeader , #unorganizedAgile').removeClass('w-75').addClass('w-25');
            $('#loadingBar').addClass('d-flex').removeClass('d-none');
            $.ajax({
                url: apiPath+"/"+myk+"/v1/getStories?epicId="+epicId+"&sortVal=&searchTxt=&localoffsetTimeZone="+localOffsetTime+"",
                type:"GET",
                error: function(jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR,textStatus,errorThrown);
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');
                },
                success:function(result){
                    
                    $('#agileStoriesUL_'+epicId).append(storiesUi(result,'organized',epicId));
                    $('#agileStoriesUL_'+epicId).css({"height": "auto", "display": "grid"});
                    /* if(result.length > 8){
                        $('#agileStoriesUL_'+epicId).append('<div style="text-align:right;display:block;"><span id="loadMore" style="color:black;font-size: 12px;padding-right: 25px;cursor:pointer;">More ...</span></div>');//<span id="showLess" style="color:black;font-size: 12px;padding-right: 25px;cursor:pointer;display:none;">Show Less ...</span>
                    } */
                    //showMoreStories(epicId);
                    $('#agileArrowright').hide();$('#agileArrowleft').show();
                    nodeDragnDrop2();
                    nodeDragnDrop3();
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');
                }
            });
        }else{
            $('#agileStoriesUL_'+epicId).html('');
            $('#agileStoriesUL_'+epicId).css({"height": "", "display": ""});
            if($("[id^='agileStoriesUL_']").find('#storyBoxes').length == 0){
                $('#orgAgileHeader , #organizedAgile').removeClass('w-75').addClass('w-25');
                $('#unorgAgileHeader , #unorganizedAgile').removeClass('w-25').addClass('w-75');
                $('#agileArrowright').show();
                $('#agileArrowleft').hide(); 
            }
            
            glbepicid="";
        }    


    }else{
        if($('#agileStoriesUL_'+epicId).children('li').length == 0){
            glbepicid=epicId;
            $('#loadingBar').addClass('d-flex').removeClass('d-none');
            $.ajax({
                url: apiPath+"/"+myk+"/v1/getStories?epicId="+epicId+"&sortVal=&searchTxt=&localoffsetTimeZone="+localOffsetTime+"",
                type:"GET",
                error: function(jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR,textStatus,errorThrown);
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');
                },
                success:function(result){
                    $('#agileStoriesUL_'+epicId).append(prepareAgileUI(result));
                    nodeDragnDrop2();
                    nodeDragnDrop3();

                    var value1 = "";
                    var build="";
                    var deploy="";
                    var completed="";
                    value1 = stagelist.split(',');
                    build = value1[3].split(':')[0];
                    deploy = value1[4].split(':')[0];
                    completed = value1[5].split(':')[0];
                    
                    for(var i=0;i<result.length;i++){
                        if(result[i].epic_type=="F"){
                            $('#funcBuild_'+result[i].epic_id).attr('epicval',build);
                            $('#funcDeploy_'+result[i].epic_id).attr('epicval',deploy);
                            $('#funcCompleted_'+result[i].epic_id).attr('epicval',completed);

                            $('.EFbacklogspan_'+result[i].epic_id).text(result[i].backlogCount);
                            $('.EFinprogressspan_'+result[i].epic_id).text(result[i].assignedCount);
                            $('.EFblockedspan_'+result[i].epic_id).text(result[i].blockedCount);
                            $('.EFholdspan_'+result[i].epic_id).text(result[i].holdCount);
                            $('.EFdonespan_'+result[i].epic_id).text(result[i].doneCount);
                            $('.EFcancelspan_'+result[i].epic_id).text(result[i].cancelCount);
                       
                        }
                    }	

                    $('#loadingBar').addClass('d-none').removeClass('d-flex');
                }
            });
        }else{
            $('#agileStoriesUL_'+epicId).html('');
            glbepicid="";
        }
    }    

    /* if($('#agileStoriesUL_'+epicId).children('li').length == 0 || $('#agileStoriesUL_'+epicId).children('div').length == 0){
        if(epicType == "F"){
            $('#orgAgileHeader , #organizedAgile').removeClass('w-25').addClass('w-75');
            $('#unorgAgileHeader , #unorganizedAgile').removeClass('w-75').addClass('w-25');
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');
        $.ajax({
            url: apiPath+"/"+myk+"/v1/getStories?epicId="+epicId+"&sortVal=&searchTxt=&localoffsetTimeZone="+localOffsetTime+"",
            type:"GET",
            error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
            },
            success:function(result){
                console.log("result--->"+JSON.stringify(result));
                if(epicType == "F"){
                    $('#agileStoriesUL_'+epicId).append(storiesUi(result,'organized'));
                }else{
                    $('#agileStoriesUL_'+epicId).append(prepareAgileUI(result));
                }
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
            }
        });
    }else{
        $('#agileStoriesUL_'+epicId).html('');
        
        $('#orgAgileHeader , #organizedAgile').removeClass('w-75').addClass('w-25');
        $('#unorgAgileHeader , #unorganizedAgile').removeClass('w-25').addClass('w-75');
        
    }
 */
            
}

function showMoreStories(epicId){
    var size_li = $("#storyBoxes").children('.storyBox').length;
    var x=8;
    var height=$('#agileStoriesUL_'+epicId).height();
    $('#storyBoxes .storyBox:lt('+x+')').css('display','flex');
    $('#loadMore').click(function () {
        $('#showLess').show();
        x= (x+size_li-8 <= size_li) ? x+size_li-8 : size_li;
        $('#storyBoxes .storyBox:lt('+x+')').css('display','flex');
        $('#agileStoriesUL_'+epicId).css('height',height+100);
    });
    /* $('#showLess').click(function () {
        x=(x-4<0) ? 4 : x-4;
        $('#storyBoxes .storyBox').not(':lt('+x+')').css('display','none');
    }); */
}

function prepareAgileStoryUI(result){

}

function listStories(){


}

function storiesUi(result,place,id,type){
    var ui="";
    var classbox1 = place=="organized"?"pt-3 pb-2":"";
    var styles = place=="organized"?"height:98%;":"";
    var class1 = place=="organized"?"epicStory_"+id:"";
    ui="<div id='storyBoxes' class='"+classbox1+" "+class1+" mytree3' style='"+styles+"'>"
    ui+=multiplestories(result,place,type)
    ui+="</div>"
    return ui;    
}
function multiplestories(result,place,type){
    var ui="";
    var epicid="";
    var colors = ["#f9c847","#7afcff","#abcc51","#f69dbb","#f9c847","#7afcff","#abcc51","#f69dbb","#f9c847","#7afcff","#abcc51","#f69dbb","#f9c847","#7afcff","#abcc51","#f69dbb","#f9c847","#7afcff","#abcc51","#f69dbb","#f9c847","#7afcff","#abcc51","#f69dbb"];
    var classbox = place=="organized"?"storyBox m-2":"storyBox";
    var storyWidth = place=="organized"?"width: 175px;":"width: 195px;";    //display:none;
    var storyplace = place=="organized"?"underepicandfeature":"";
    var bcolor = "";
    var parentid="";var rootid="";var epictype="";var dstart="";var typeimage="";var typetitle="";
    var status="";var statusimage="";
    for(var i=0;i<result.length;i++){
        colorvalue=colors[i];
        epicid=result[i].epic_id;
        parentid = result[i].parent_epic_id;
        rootid = result[i].root_epic_id;
        epictype = result[i].epic_type;
        bcolor = result[i].epic_type=="S"?"#f9c847":"#d89cd8";
        typeimage = result[i].epic_type=="S"?"images/agile/story.svg":"images/agile/feature.svg";
        typetitle = result[i].epic_type=="S"?"Story":"Feature";
        status = result[i].epic_status;
        statusimage = status=="Backlog"?"/images/idea_old/Backlog.png":status=="Blocked"?"/images/idea_old/Blocked.png":status=="Hold"?"/images/idea_old/Hold.png":status=="Done"?"/images/idea_old/Done.png":status=="Cancel"?"/images/idea_old/cancel.png":status=="In Progress"?"/images/idea_old/Assigned.png":"";

        ui+="<div id='storyDiv_"+epicid+"' storyplace='"+place+"' parentid="+parentid+" rootid="+rootid+" epicType='"+epictype+"' ondragstart=\"checkFeature("+epicid+",'"+epictype+"','"+place+"');\" ondragend=\"updateDraggedorder("+epicid+",'"+place+"','"+storyplace+"');event.stopPropagation();\" class='d-flex flex-column align-items-around agileMainDivCls "+classbox+"' style='background-color: "+bcolor+";"+storyWidth+"'>"   //updateOrgStories("+epicid+",'"+place+"','"+storyplace+"')      //ondblclick=\"createAgile("+epicid+",'','edit','"+epictype+"','');event.stopPropagation();\" 
            +"<div class='d-flex justify-content-between pt-1'>"
                +"<div class='' style='padding-left:5px;'><img src='"+typeimage+"' title='"+typetitle+"' style='width:20px;height:20px;'></div>"
                +"<div class='position-relative'><img src='images/conversation/three_dots.svg' onclick='showAgileOption("+epicid+");event.stopPropagation();' class='' style='width:20px;height:20px;cursor:pointer;'>"
                if(epictype=="S"){
                    ui+=optionsUI(epicid,place,parentid,'storyDiv_',result[i].epic_title,result[i].epic_point)
                }else{
                    ui+=optionsEFui(epicid,epictype,parentid,'unorganized',result[i].epic_title)
                }
                
                ui+="</div>"
            +"</div>"
            +"<div style='height:45px;'>"
                +"<span id='storytitle_"+epicid+"' class='border-0 defaultExceedMultilineCls' style='-webkit-line-clamp: 2 !important;padding: 3px 5px 0px 5px;width:90%;height: 80%;font-size:12px;color:black;' title=\""+result[i].epic_title+"\">"+result[i].epic_title+"</span>"
            +"</div>"
            +"<div class='d-flex align-items-center p-1 pt-2'>"
                +"<div class='ml-auto position-relative liststatus' onclick=\"showStatusupdate(this,"+epicid+");event.stopPropagation();\"><img id='agileliststat_"+epicid+"' src='"+statusimage+"' style='width:20px;height:20px;'>"
                    ui+=statusUpdate(epicid,'agilelists')
                ui+="</div>"
            +"</div>"

        +"</div>"
    }

    return ui;
}

function optionsUI(epicid,place,parentid,divid,epictitle,epicpoints){
    var ui="";

    ui="<div class='position-absolute rounded agileMoreOption' id='agileStorymoreoptdiv_"+epicid+"'  style='cursor:pointer;'>"
        +"<div class='pl-2 pt-2 pb-1 agileOpt' onclick=\"createAgile("+epicid+",'','edit','S','',"+parentid+",'"+divid+"');event.stopPropagation();\"><span>"+getValues(companyLabels,"Edit")+"</span></div>"
        +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Associate")+"</span>"
            +"<div class='position-absolute rounded agileMoreOption1 agileMoreOptionsub d-none' style='left:98px;'>"
                +"<div class='pl-2 pt-2 pb-1'><span>"+getValues(companyLabels,"Epic")+"</span></div>"
                +"<div class='pl-2 pt-1 pb-2'><span>"+getValues(companyLabels,"Feature")+"</span></div>"
            +"</div>"
        +"</div>"
        +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Story_Assign")+"</span>"
            +"<div class='position-absolute rounded agileMoreOption1 agileMoreOptionsub d-none' style='left:98px;'>"
                +"<div class='pl-2 pt-2 pb-1'><span>"+getValues(companyLabels,"Group")+"</span></div>"
                +"<div class='pl-2 pt-1 pb-2'><span>"+getValues(companyLabels,"SPRINT")+"</span></div>"
            +"</div>"
        +"</div>"
        +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Status")+"</span>"
            ui+=statusUpdate(epicid,'optionview')
        ui+="</div>"
        +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Priority")+"</span>"
            ui+=agilePrioritylist(epicid,'optionview')
        ui+="</div>"//onclick='addPoints("+epicid+");event.stopPropagation();'     //editPoints("+epicid+","+parentid+",'"+divid+"');
        +"<div class='pl-2 py-1 agileOpt position-relative' onclick=\"createAgile("+epicid+",'','edit','S','',"+parentid+",'"+divid+"','pointsupdate');event.stopPropagation();\"><span>"+getValues(companyLabels,"Points")+"</span></div>"
        +"<div class='pl-2 py-1 agileOpt' onclick=\"agileComments("+epicid+",'"+epictitle+"','','','','Agile');event.stopPropagation();\"><span>"+getValues(companyLabels,"Comments")+"</span></div>"
        +"<div class='pl-2 py-1 agileOpt' onclick=\"openAgileTaskPopup("+epicid+",'"+epictitle+"','options');event.stopPropagation();\"><span>"+getValues(companyLabels,"Task")+"</span></div>"  //
        +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Documents")+"</span>"
            +"<div class='position-absolute rounded moreOptDoc agileMoreOptionsub d-none' style='left:98px;'>"
                +'<div class="d-flex p-1 justify-content-start" style="height: 24px; cursor:pointer;">'
                +'  <form enctype="multipart/form-data" style="position:absolute;top:-100px;" method="post" name="agileFileUpload" id="agileFileUpload">'
                +'  <input type="file" class="" title="Upload" value="" onchange="readFileUrlinAgile(this,'+epicid+');event.stopPropagation();" name="FileUpload" id="agileFileUpload_'+epicid+'" hidden></form>'
                +' 	<img class="img-fluid" src="images/conversation/computer.svg" style="width:20px;" onclick="openSystemFolder('+epicid+');event.stopPropagation();" id="mainUpload">'
                +'	<span class="pl-2" style="white-space: nowrap;" onclick="openSystemFolder('+epicid+');event.stopPropagation();">From Computer</span>'
                +'</div>'
                +'<div class="d-flex p-1 justify-content-start" style="height: 24px;">'
                +' 	<img class="img-fluid" style="width:20px;" src="images/conversation/folder.svg">'
                +'	<span class="pl-2" style="white-space: nowrap;" >From Repository</span>'
                +'</div>'
                +'<div class="d-flex p-1 justify-content-start" onclick="attlinkPopup('+epicid+');event.stopPropagation();" style="height: 24px;cursor:pointer;">'
                +' 	<img class="img-fluid" style="width:20px;" src="images/task/attach.svg">'
                +'	<span class="pl-2" style="white-space: nowrap;" >Attach Link</span>'
                +"</div>"
            +"</div>"      
        +"</div>"
        +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Defect")+"</span>"
            +"<div class='position-absolute rounded agileMoreOption1 agileMoreOptionsub  d-none' style=''>"
                +"<div class='pl-2 pt-2 pb-1'><span>"+getValues(companyLabels,"Create")+"</span></div>"
                +"<div class='pl-2 pt-1 pb-2'><span>"+getValues(companyLabels,"Associate")+"</span></div>"
            +"</div>"
        +"</div>"
        +"<div class='pl-2 pt-1 pb-2 agileOpt' onclick=\"confirmDelete("+epicid+",'"+place+"');event.stopPropagation();\"><span>"+getValues(companyLabels,"Delete")+"</span></div>"
    +"</div>"

    return ui;
}

function agilePrioritylist(epicid,view){
    var ui="";
    var viewclass = view=="detailview"?"agileMoreOption2":"agileMoreOption1";
    var viewstyle = view=="detailview"?"right:1px;top:20px;":"";

    ui="<div id='agilepriority_"+epicid+"' class='position-absolute rounded "+viewclass+" agileMoreOptionsub d-none' style='width:150px;"+viewstyle+"'>"
        +"<div class='pl-2 pt-2 pb-1 d-flex align-items-center' onclick=\"updatepriority(this,"+epicid+",'"+view+"');event.stopPropagation();\" priority='1'><img src='images/task/p-one.svg' style='width:15px;height:15px;'><span class='pl-2'>"+getValues(companyLabels,"Very_Important")+"</span></div>"
        +"<div class='pl-2 py-1 d-flex align-items-center' onclick=\"updatepriority(this,"+epicid+",'"+view+"');event.stopPropagation();\" priority='2'><img src='images/task/p-two.svg' style='width:15px;height:15px;'><span class='pl-2'>"+getValues(companyLabels,"Important")+"</span></div>"
        +"<div class='pl-2 py-1 d-flex align-items-center' onclick=\"updatepriority(this,"+epicid+",'"+view+"');event.stopPropagation();\" priority='3'><img src='images/task/p-three.svg' style='width:15px;height:15px;'><span class='pl-2'>"+getValues(companyLabels,"Medium")+"</span></div>"
        +"<div class='pl-2 py-1 d-flex align-items-center' onclick=\"updatepriority(this,"+epicid+",'"+view+"');event.stopPropagation();\" priority='4'><img src='images/task/p-four.svg' style='width:15px;height:15px;'><span class='pl-2'>"+getValues(companyLabels,"Low")+"</span></div>"
        +"<div class='pl-2 pt-1 pb-2 d-flex align-items-center' onclick=\"updatepriority(this,"+epicid+",'"+view+"');event.stopPropagation();\" priority='5'><img src='images/task/p-five.svg' style='width:15px;height:15px;'><span class='pl-2'>"+getValues(companyLabels,"Very_Low")+"</span></div>"
    +"</div>"

    return ui;
}

function showAgileOption(epicid){
    $("[id^='agileStorymoreoptdiv_']").hide();
    $("[id^='agileEpicmoreoptdiv_']").hide();
    $("[id^='agileFeaturemoreoptdiv_']").hide();
    $('#agileStorymoreoptdiv_'+epicid).toggle();
    $('#agileEpicmoreoptdiv_'+epicid).toggle();
    $('#agileFeaturemoreoptdiv_'+epicid).toggle();
}
function showSubOption(obj){
    $(obj).children('div').addClass('d-block').removeClass('d-none');
}
function hideSubOption(obj){
    $(obj).children('div').addClass('d-none').removeClass('d-block');
    $('.agileMoreOptionsub').addClass('d-none').removeClass('d-block');
}

function optionsEFui(epicid,epictype,parentid,featureplace,epictitle){
    var ui="";
    var divplace="";
    if(epictype == "E"){
        ui="<div class='position-absolute rounded agileMoreOption ' id='agileEpicmoreoptdiv_"+epicid+"'  style='cursor:pointer;'>"
            +"<div class='pl-2 pt-2 pb-1 agileOpt' onclick=\"createAgile("+epicid+",'sub','create','F','');event.stopPropagation();\"><span>"+getValues(companyLabels,"Add_Feature")+"</span></div>"
            +"<div class='pl-2 py-1 agileOpt' onclick=\"createAgile("+epicid+",'sub','create','S','fromEpic');event.stopPropagation();\"><span>"+getValues(companyLabels,"Add_Story")+"</span></div>"
            +"<div class='pl-2 py-1 agileOpt' onclick=\"createAgile("+epicid+",'','edit','"+epictype+"','',"+parentid+",'orgAgileDiv_');event.stopPropagation();\"><span>"+getValues(companyLabels,"Edit")+"</span></div>"
            +"<div class='pl-2 py-1 agileOpt'><span>"+getValues(companyLabels,"Reorder")+"</span></div>"
            +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Status")+"</span>"
                ui+=statusUpdate(epicid,'optionview')
            ui+="</div>"
            +"<div class='pl-2 py-1 agileOpt' onclick=\"agileComments("+epicid+",'"+epictitle+"','','','','Agile');event.stopPropagation();\"><span>"+getValues(companyLabels,"Comments")+"</span></div>"
            +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Agile_Value")+"</span>"
                +"<div class='position-absolute rounded agileMoreOption1 agileMoreOptionsub d-none' style='left:98px;'>"
                    +"<div id='valuebacklog_"+epicid+"' epicval='0' onclick=\"updateValue(this,"+epicid+");event.stopPropagation();\" class='pl-2 pt-2 pb-1'><span>"+getValues(companyLabels,"Backlog")+"</span></div>"
                    +"<div id='valueinception_"+epicid+"' epicval='' onclick=\"updateValue(this,"+epicid+");event.stopPropagation();\" class='pl-2 py-1'><span>Inception</span></div>"
                    +"<div id='valueconstruction_"+epicid+"' epicval='' onclick=\"updateValue(this,"+epicid+");event.stopPropagation();\" class='pl-2 py-1'><span>Construction</span></div>"
                    +"<div id='valuecompleted_"+epicid+"' epicval='' onclick=\"updateValue(this,"+epicid+");event.stopPropagation();\" class='pl-2 pt-1 pb-2'><span>"+getValues(companyLabels,"Completed")+"</span></div>"
                +"</div>"
            +"</div>"
            +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Documents")+"</span>"
                +"<div class='position-absolute rounded moreOptDoc agileMoreOptionsub  d-none' style='left:98px;'>"
                    +'<div class="d-flex p-1 justify-content-start" style="height: 24px; cursor:pointer;">'
                    +'  <form enctype="multipart/form-data" style="position:absolute;top:-100px;" method="post" name="agileFileUpload" id="agileFileUpload">'
                    +'  <input type="file" class="" title="Upload" value="" onchange="readFileUrlinAgile(this,'+epicid+');event.stopPropagation();" name="FileUpload" id="agileFileUpload_'+epicid+'" hidden></form>'
                    +' 	<img class="img-fluid" src="images/conversation/computer.svg" style="width:20px;" onclick="openSystemFolder('+epicid+');event.stopPropagation();" id="mainUpload">'
                    +'	<span class="pl-2" style="white-space: nowrap;" onclick="openSystemFolder('+epicid+');event.stopPropagation();">From Computer</span>'
                    +'</div>'
                    +'<div class="d-flex p-1 justify-content-start" style="height: 24px;">'
                    +' 	<img class="img-fluid" style="width:20px;" src="images/conversation/folder.svg">'
                    +'	<span class="pl-2" style="white-space: nowrap;" >From Repository</span>'
                    +'</div>'
                    +'<div class="d-flex p-1 justify-content-start" onclick="attlinkPopup('+epicid+');event.stopPropagation();" style="height: 24px;cursor:pointer;">'
                    +' 	<img class="img-fluid" style="width:20px;" src="images/task/attach.svg">'
                    +'	<span class="pl-2" style="white-space: nowrap;" >Attach Link</span>'
                    +"</div>"
                +"</div>"    
            +"</div>"
            +"<div class='pl-2 py-1 agileOpt' onclick=\"agileCollaborators("+epicid+");event.stopPropagation();\"><span>"+getValues(companyLabels,"Collaborators")+"</span></div>"
            +"<div class='pl-2 pt-1 pb-2 agileOpt' onclick=\"confirmDelete("+epicid+",'');event.stopPropagation();\"><span>"+getValues(companyLabels,"Delete")+"</span></div>"
        +"</div>"
    }else if(epictype == "F"){
        divplace = featureplace=="unorganized"?"storyDiv_":"orgAgileDiv_";
        ui="<div class='position-absolute rounded agileMoreOption ' onclick='event.stopPropagation();' id='agileFeaturemoreoptdiv_"+epicid+"'  style='cursor:pointer;'>"
            +"<div class='pl-2 pt-2 pb-1 agileOpt' onclick=\"createAgile("+epicid+",'sub','create','S','');\"><span>"+getValues(companyLabels,"Add_Story")+"</span></div>"
            +"<div class='pl-2 py-1 agileOpt' onclick=\"createAgile("+epicid+",'','edit','"+epictype+"','',"+parentid+",'"+divplace+"');event.stopPropagation();\"><span>"+getValues(companyLabels,"Edit")+"</span></div>"
            +"<div class='pl-2 py-1 agileOpt'><span>"+getValues(companyLabels,"Reorder")+"</span></div>"
            +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Status")+"</span>"
                ui+=statusUpdate(epicid,'optionview')
            ui+="</div>"
            +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Priority")+"</span>"
                ui+=agilePrioritylist(epicid,'optionview')
            ui+="</div>"
            +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Functionality")+"</span>"
                +"<div class='position-absolute rounded agileMoreOption1 agileMoreOptionsub d-none' style='left:98px;'>"
                    +"<div id='funcBacklog_"+epicid+"' epicval='0' onclick=\"updateValue(this,"+epicid+");event.stopPropagation();\" class='pl-2 pt-2 pb-1'><span>"+getValues(companyLabels,"Backlog")+"</span></div>"
                    +"<div id='funcBuild_"+epicid+"' epicval='' onclick=\"updateValue(this,"+epicid+");event.stopPropagation();\" class='pl-2 py-1'><span>Build</span></div>"
                    +"<div id='funcDeploy_"+epicid+"' epicval='' onclick=\"updateValue(this,"+epicid+");event.stopPropagation();\" class='pl-2 py-1'><span>Deploy</span></div>"
                    +"<div id='funcCompleted_"+epicid+"' epicval='' onclick=\"updateValue(this,"+epicid+");event.stopPropagation();\" class='pl-2 pt-1 pb-2'><span>"+getValues(companyLabels,"Completed")+"</span></div>"
                +"</div>"
            +"</div>"
            +"<div class='pl-2 py-1 agileOpt' onclick=\"agileComments("+epicid+",'"+epictitle+"','','','','Agile');event.stopPropagation();\"><span>"+getValues(companyLabels,"Comments")+"</span></div>"
            +"<div class='pl-2 py-1 agileOpt position-relative' onmouseover='showSubOption(this);event.stopPropagation();' onmouseout='hideSubOption(this);event.stopPropagation();'><span>"+getValues(companyLabels,"Documents")+"</span>"
                +"<div class='position-absolute rounded moreOptDoc agileMoreOptionsub d-none' style='left:98px;'>"
                    +'<div class="d-flex p-1 justify-content-start" style="height: 24px; cursor:pointer;">'
                    +'  <form enctype="multipart/form-data" style="position:absolute;top:-100px;" method="post" name="agileFileUpload" id="agileFileUpload">'
                    +'  <input type="file" class="" title="Upload" value="" onchange="readFileUrlinAgile(this,'+epicid+');event.stopPropagation();" name="FileUpload" id="agileFileUpload_'+epicid+'" hidden></form>'
                    +' 	<img class="img-fluid" src="images/conversation/computer.svg" style="width:20px;" onclick="openSystemFolder('+epicid+');event.stopPropagation();" id="mainUpload">'
                    +'	<span class="pl-2" style="white-space: nowrap;" onclick="openSystemFolder('+epicid+');event.stopPropagation();">From Computer</span>'
                    +'</div>'
                    +'<div class="d-flex p-1 justify-content-start" style="height: 24px;">'
                    +' 	<img class="img-fluid" style="width:20px;" src="images/conversation/folder.svg">'
                    +'	<span class="pl-2" style="white-space: nowrap;" >From Repository</span>'
                    +'</div>'
                    +'<div class="d-flex p-1 justify-content-start" onclick="attlinkPopup('+epicid+');event.stopPropagation();" style="height: 24px;cursor:pointer;">'
                    +' 	<img class="img-fluid" style="width:20px;" src="images/task/attach.svg">'
                    +'	<span class="pl-2" style="white-space: nowrap;" >Attach Link</span>'
                    +"</div>"
                +"</div>"  
            +"</div>"
            +"<div class='pl-2 pt-1 pb-2 agileOpt' onclick=\"confirmDelete("+epicid+",'');event.stopPropagation();\"><span>"+getValues(companyLabels,"Delete")+"</span></div>"
        +"</div>"
    }

    return ui;
}

function statusUpdate(epicid,view){
    var ui="";
    var viewclass = view=="detailview"?"agileMoreOption2":view=="SBlist"?"agileMoreOption3":"agileMoreOption1";
    var viewstyle = view=="detailview"?"top:-125px;right:25px;": view=="agilelists"?"top:0px;left:15px;":"";

    ui="<div class='position-absolute rounded "+viewclass+" agileMoreOptionsub d-none' style='"+viewstyle+"cursor:pointer;'>"
        +"<div class='pl-2 pt-2 pb-1' onclick=\"updateStatusui("+epicid+",'In Progress','"+view+"');event.stopPropagation();\"><span>"+getValues(companyLabels,"In_Progress")+"</span></div>"
        +"<div class='pl-2 py-1' onclick=\"updateStatusui("+epicid+",'Blocked','"+view+"');event.stopPropagation();\"><span>"+getValues(companyLabels,"Blocked")+"</span></div>"
        +"<div class='pl-2 py-1' onclick=\"updateStatusui("+epicid+",'Hold','"+view+"');event.stopPropagation();\"><span>"+getValues(companyLabels,"Hold")+"</span></div>"
        +"<div class='pl-2 py-1' onclick=\"updateStatusui("+epicid+",'Done','"+view+"');event.stopPropagation();\"><span>"+getValues(companyLabels,"Done")+"</span></div>"
        +"<div class='pl-2 pt-1 pb-2' onclick=\"updateStatusui("+epicid+",'Cancel','"+view+"');event.stopPropagation();\"><span>"+getValues(companyLabels,"Cancel")+"</span></div>"
    +"</div>"

    return ui;

}

function confirmDelete(epicid,place){
    conFunNew(getValues(companyAlerts,"Alert_delete"),"warning","deleteAgile","",epicid,place);
}

function deleteAgile(epicid,place){
    var parentid = "";
    var epictype = "";
    if($('#orgAgile_'+epicid).attr('epictype') == "S" || $('#orgAgile_'+epicid).attr('epictype') == "F" || $('#orgAgile_'+epicid).attr('epictype') == "E"){
        epictype = $('#orgAgile_'+epicid).attr('epictype');
        parentid = $('#orgAgile_'+epicid).parent('ul').attr('id').split('_')[1];
        $('#orgAgile_'+epicid).remove();
        place="organized";
    }else{
        $('#storyDiv_'+epicid).remove();
        epictype = "S";
    }  
    console.log(epicid+"<--->"+place+"<--->"+epictype);  
    var orderdata = getOrder(place,parentid,epictype);
    let jsonbody = {
      "user_id" : userIdglb,
      "epic_id" : epicid,
      "order": JSON.parse(orderdata)
    }
    console.log("jsonbody-->"+JSON.stringify(jsonbody));
    $('#loadingBar').addClass('d-none').removeClass('d-flex');
    $.ajax({ 
        url: apiPath+"/"+myk+"/v1/deleteAgile",
        type:"DELETE",
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }, 
        success:function(result){
          
          if($('#agileStoriesUL_'+parentid).children('li').length == 0){
            $('#bgBoxE_'+parentid).remove();
          }
          
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }  
    });  
}

function openSystemFolder(epicid) {
	document.getElementById('agileFileUpload_'+epicid+'').click();
}

function expandEpics(){
    $('#orgAgileHeader , #organizedAgile').removeClass('w-25').addClass('w-75');
    $('#unorgAgileHeader , #unorganizedAgile').removeClass('w-75').addClass('w-25');
    $('#agileArrowright').hide();
    $('#agileArrowleft').show();
}

function collapseEpics(){
    $('#orgAgileHeader , #organizedAgile').removeClass('w-75').addClass('w-25');
    $('#unorgAgileHeader , #unorganizedAgile').removeClass('w-25').addClass('w-75');
    $('#agileArrowright').show();
    $('#agileArrowleft').hide();
    if($("[id^='agileStoriesUL_']").find('#storyBoxes').length != 0){
        $("[id^='storyBoxes']").parent("[id^='agileStoriesUL_']").html('');
    }    
    /* if(glbepicid!=""){
        showFeaturesNstories(glbepicid,'F');
    } */
    
}

function createAgile(id,place,type,agileType,fromEpic,parentid,divid,pointsupdate){
    var ui="";

    if($('#AgileDiv_'+id).length == 0){

        if($('.agileDivCls').length > 0){
            $('.agileDivCls').remove();
        }

        if(type=="create"){
            ui = prepareCreateAgileUi(id,place,type,agileType,fromEpic);
            if(id==0){
                $("#agileDiv").prepend(ui).show('slow');
            }else{
                $('#orgAgile_'+id).prepend(ui);
                if(agileType=="F"){
                    $('#agileNametext_'+id).attr('placeholder','Enter your Feature...');
                }
            }
        }else{
            $('#agileEpicmoreoptdiv_'+id+' , #agileStorymoreoptdiv_'+id).hide();
            $('#orgAgile_'+id).prepend(ui);
            if(agileType == "E"){
                getEpicdata(id,agileType,pointsupdate);
            }else{
                getFeaturesAndStories(id,agileType,parentid,divid,pointsupdate);
            }
            
        }
            
    }else{
        $("#AgileDiv_"+id).fadeOut(300, function(){ $(this).remove();});
    }


}

async function getFeaturesAndStories(epicid,agileType,parentid,divid,pointsupdate){
   if(typeof(divid)=="undefined"||divid=="undefined"||divid==""){
        divid="orgAgileDiv_";
   }
   var id1=divid+epicid;
   $('#loadingBar').addClass('d-flex').removeClass('d-none');
    await $.ajax({
        url: apiPath+"/"+myk+"/v1/getStories?epicId="+parentid+"&user_id="+userIdglb+"&company_id="+companyIdglb+"&sortVal=&searchTxt=&localoffsetTimeZone="+localOffsetTime+"&fillterEpicId="+epicid+"",
        type:"GET",
        error: function(jqXHR, textStatus, errorThrown) {
            checkError(jqXHR,textStatus,errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){

            $('#'+id1).append(prepareCreateAgileUi(epicid,'','edit',agileType,''));
            setvalues(result,agileType,parentid,pointsupdate);

            var elmnt = document.getElementById("AgileDiv_"+epicid);
            elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});

            if(agileType=="F"){
                if($('#functionalityDiv_'+epicid).is(':hidden')){
                    $('#functionalityDiv_'+epicid).parent().css('height','20px');
                }
            }
            if(agileType=="S"){
                if($('#sprintDiv_'+epicid).is(':hidden')){
                    $('#sprintDiv_'+epicid).parent().css('height','20px');
                }
            }
            
            $('#loadingBar').addClass('d-none').removeClass('d-flex');

        }
    });    
}

async function getEpicdata(epicid,agileType,pointsupdate){
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    let jsonbody = {
        "user_id":userIdglb,
        "project_id":prjid,
        "company_id":companyIdglb,
        "sortVal":"",
        "searchTxt":"",
        "localOffsetTime":localOffsetTime,
        "epic_id": epicid
    }  
    await $.ajax({
        url: apiPath+"/"+myk+"/v1/getEpics",
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){
            
            $('#orgAgileDiv_'+epicid).append(prepareCreateAgileUi(epicid,'','edit',agileType,''));
            setvalues(result,agileType,pointsupdate);

            var elmnt = document.getElementById("AgileDiv_"+epicid);
            elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});

            if(agileType=="E"){
                if($('#valueDiv_'+epicid).is(':hidden')){
                    $('#valueDiv_'+epicid).parent().css('height','20px');
                }
            }
            
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });    
}

function setvalues(result,agileType,parentid,pointsupdate){
    var epicid = result[0].epic_id;
    var status = result[0].epic_status;
    var stagename = result[0].stage_name;

    var doccount = "";
    var commentcount = "";
    var linkcount = "";
    var sharecount = "";
    var taskcount = "";
    var pointcount = "";
    var priority = "";
    var priorityimage="";
    var prioritytitle = "";
    var epicpoints="";

    var statusimage = status=="Backlog"?"/images/idea_old/Backlog.png":status=="Blocked"?"/images/idea_old/Blocked.png":status=="Hold"?"/images/idea_old/Hold.png":status=="Done"?"/images/idea_old/Done.png":status=="Cancel"?"/images/idea_old/cancel.png":status=="In Progress"?"/images/idea_old/Assigned.png":"";
    $('#statusDiv_'+epicid).children('img').attr('src',statusimage).attr('title',status);
    //$('#statusDiv_'+epicid).children('span').text(status);
    $('input#agileCustomid_'+epicid).val(result[0].unique_cust_id).attr('title',result[0].unique_cust_id).attr('onkeydown','updateAgileCustomid('+epicid+',event,"'+agileType+'");event.stopPropagation();');
    $('textarea#agileNametext_'+epicid).val(result[0].epic_title).attr('title',result[0].epic_title).attr('readonly',true).attr('ondblclick','editAgileTextarea('+epicid+',event);event.stopPropagation();');
    $('#savecanceldiv').addClass('d-none').removeClass('d-flex');
    $('#saveepic_'+epicid).attr('onclick','updateagile('+epicid+',"'+agileType+'",'+parentid+');event.stopPropagation();');
    
    

    if(agileType=="E"){
        $('#backlogspan_'+epicid).text(result[0].backlogCount);
        $('#inprogressspan_'+epicid).text(result[0].assignedCount);
        $('#blockedspan_'+epicid).text(result[0].blockedCount);
        $('#holdspan_'+epicid).text(result[0].holdCount);
        $('#donespan_'+epicid).text(result[0].doneCount);
        $('#cancelspan_'+epicid).text(result[0].cancelCount);
        if(stagename!=""){
            $('#valueSpan_'+epicid).text(stagename);
            $('#valueDiv_'+epicid).addClass('d-flex').removeClass('d-none');
        }
        $('#epicIconspan_'+epicid).text(result[0].fpoint);
        $('#epicIconPointsinput_'+epicid).val(result[0].fpoint);
        
    }else if(agileType=="F"){

        if(stagename!=""){
            $('#functionalitySpan_'+epicid).text(stagename);
            $('#functionalityDiv_'+epicid).addClass('d-flex').removeClass('d-none');
        }

    }else if(agileType=="S"){
        var sprintName = result[0].sprintName;
        if(sprintName!=""){
            $('#sprintSpan_'+epicid).text(sprintName);
            $('#sprintDiv_'+epicid).addClass('d-flex').removeClass('d-none');
        }
        $('#epicIconspan_'+epicid).text(result[0].epic_point);
        $('#epicIconPointsinput_'+epicid).val(result[0].epic_point);
    }

    priority = result[0].epic_priority;
    priorityimage = (priority=="")? "images/task/p-six.svg" : priority=="1" ? "images/task/p-one.svg" : priority=="2" ? "images/task/p-two.svg":priority=="3" ? "images/task/p-three.svg" :priority=="4" ? "images/task/p-four.svg": "images/task/p-five.svg";
    prioritytitle = (priority=="")? "None" : priority=="1" ? "Very Important" : priority=="2" ? "Important":priority=="3" ? "Medium" :priority=="4" ? "Low": "Very Low";

    if(agileType=="E"){
        doccount = result[0].epicDocs;
        commentcount = result[0].epicCmts;
        linkcount = result[0].epicLink;
        sharecount = result[0].epicShare;
        taskcount = result[0].storyTasks;
        
        
        if((typeof(doccount)!="undefined" && doccount!="0" && doccount!="") || (typeof(linkcount)!="undefined" && linkcount!="0" && linkcount!="")){
            $("#epicIconDoc_"+epicid).show().attr('onclick','showDocuments('+epicid+')');
        }else{
            $("#epicIconDoc_"+epicid).remove();
        }
        if(typeof(commentcount)!="undefined" && commentcount!="0" && commentcount!=""){
            $("#epicIconComments_"+epicid).show().attr('onclick','agileComments('+epicid+',"'+result[0].epic_title+'","","","","Agile");event.stopPropagation();');
        }else{
            $("#epicIconComments_"+epicid).remove();
        }
        if(typeof(sharecount)!="undefined" && sharecount!="0" && sharecount!=""){
            $("#epicIconShare_"+epicid).show();
        }else{
            $("#epicIconShare_"+epicid).remove();
        }
        if(typeof(result[0].fpoint)!="undefined" && result[0].fpoint!="0" && result[0].fpoint!=""){
            $("#epicIconPoints_"+epicid).addClass('d-flex').removeClass('d-none');
        }else{
            $("#epicIconPoints_"+epicid).addClass('d-none').removeClass('d-flex');
        }
        if(typeof(priority)!="undefined" && priority!="0" && priority!=""){
            $("#epicIconPriority_"+epicid).show().attr('onclick','showagilePrioritylist('+epicid+');event.stopPropagation();');
            $("#epicIconPriority_"+epicid).children('img').attr('src',priorityimage).attr('title',prioritytitle);
        }else{
            $("#epicIconPriority_"+epicid).remove();
        }
        if(typeof(taskcount)!="undefined" && taskcount!="0" && taskcount!=""){
            $("#epicIconTask_"+epicid).show();
        }else{
            $("#epicIconTask_"+epicid).remove();
        }
    }else{
        doccount = result[0].storyDocs;
        commentcount = result[0].storyLink;
        linkcount = result[0].epicLink;
        sharecount = result[0].epicShare;
        taskcount = result[0].storyTasks;
        pointcount = result[0].epic_point;
        
        
        if((typeof(doccount)!="undefined" && doccount!="0" && doccount!="") || (typeof(linkcount)!="undefined" && linkcount!="0" && linkcount!="")){
            $("#epicIconDoc_"+epicid).show().attr('onclick','showDocuments('+epicid+');event.stopPropagation();');
        }else{
            $("#epicIconDoc_"+epicid).remove();
        }
        if(typeof(commentcount)!="undefined" && commentcount!="0" && commentcount!=""){
            $("#epicIconComments_"+epicid).show().attr('onclick','agileComments('+epicid+',"'+result[0].epic_title+'","","","","Agile");event.stopPropagation();');
        }else{
            $("#epicIconComments_"+epicid).remove();
        }
        if(typeof(sharecount)!="undefined" && sharecount!="0" && sharecount!=""){
            $("#epicIconShare_"+epicid).show();
        }else{
            $("#epicIconShare_"+epicid).remove();
        }
        if(typeof(pointcount)!="undefined" && pointcount!="0" && pointcount!=""){
            $("#epicIconPoints_"+epicid).addClass('d-flex').removeClass('d-none');
            $('#epicIconspan_'+epicid).attr('onclick','editpoints('+epicid+');event.stopPropagation();');
        }else{
            $("#epicIconPoints_"+epicid).addClass('d-none').removeClass('d-flex');
        }
        if(typeof(priority)!="undefined" && priority!="0" && priority!=""){
            $("#epicIconPriority_"+epicid).show().attr('onclick','showagilePrioritylist('+epicid+');event.stopPropagation();');
            $("#epicIconPriority_"+epicid).children('img').attr('src',priorityimage).attr('title',prioritytitle);
        }else{
            $("#epicIconPriority_"+epicid).remove();
        }
        if(typeof(taskcount)!="undefined" && taskcount!="0" && taskcount!=""){
            $("#epicIconTask_"+epicid).show();
        }else{
            $("#epicIconTask_"+epicid).remove();
        }
    }
    
    if(pointsupdate == "pointsupdate"){
        if($('#epicIconPoints_'+epicid).hasClass('d-flex') == true){
            $('#epicIconspan_'+epicid).addClass('d-none').removeClass('d-block');
            $('#epicIconPointsinput_'+epicid).addClass('d-block').removeClass('d-none').focus();
        }else{
            $('#epicIconPoints_'+epicid).addClass('d-flex').removeClass('d-none');
            $('#epicIconspan_'+epicid).attr('onclick','editpoints('+epicid+');event.stopPropagation();').addClass('d-none').removeClass('d-block');
            $('#epicIconPointsinput_'+epicid).addClass('d-block').removeClass('d-none').focus();
        }
    }
    
    if($('#epicIconPoints_'+epicid).hasClass('d-none') == true){
        $('#agileCustomid_'+epicid).parent().addClass('mr-auto');
    }


}

async function showagilePrioritylist(epicid){
    
    if($('#epicIconPriority_'+epicid).find('#agilepriority_'+epicid).hasClass('d-none')){
        $('#epicIconPriority_'+epicid).find('#agilepriority_'+epicid).addClass('d-block').removeClass('d-none');
    }else{
        $('#epicIconPriority_'+epicid).find('#agilepriority_'+epicid).addClass('d-none').removeClass('d-block');
    }
    
}

function editAgileTextarea(epicid,event){
    $('#agileNametext_'+epicid).removeAttr('readonly');
    event.preventDefault();
    $('#savecanceldiv').addClass('d-flex').removeClass('d-none');
    
}

function prepareCreateAgileUi(id,place,type,agileType,fromEpic){
    var ui="";
    var bgcolor="";
    if(agileType=="E"){
        bgcolor = "#eeabb6";
    }else if(agileType=="F"){
        bgcolor = "#d89cd8";
    }else{
        bgcolor = "#f6b847";
    }
    var cusIdClass = type=="edit"?"mx-3":"ml-3";
    var cusIdClass2 = type!="edit"?"mr-auto":"";
    var saveplace = type=="edit"?"ml-2":"ml-auto";
    var class1=type=="edit"?"pt-1":"pt-2";;
    if(place == "main"){
    ui="<div id='AgileDiv_"+id+"' class='agileDivCls' agiletype='S' style='height:auto;font-size:12px;padding: 15px 5px 15px 30px;'>"
        +'<div class="material-textfield"  style="">'
            +'<select  id="agileSelType" class="material-textfield-input"  onchange="changeAgileType('+id+');" style="border: 1px solid #C1C5C8;border-radius: 6px;height:30px;font-size:12px;outline: none;width:15%;padding-left:5px;">'
                +'<option value="S">Story</option>'
                +'<option value="E">Epic</option>'
                +'<option value="F">Feature</option>'
            +'</select>'
            +'<label class="material-textfield-label" id="" style="margin-left: 5px; font-size:11px; background-color: #fff !important;top: 0px;">Type</label>'
        +'</div>'
    }else if(place == "sub"){
        ui="<div id='AgileDiv_"+id+"' class='agileDivCls' agiletype='"+agileType+"' style='position:absolute;height:auto;font-size:12px;left:100px;top:85px;z-index:1000;'>"
    }else{
        ui="<div id='AgileDiv_"+id+"' class='agileDivCls' agiletype='"+agileType+"' style='position:absolute;height:auto;font-size:12px;left:100px;top:85px;z-index:1000;'>"
    }
        ui+="<div id='agileMainDiv_"+id+"'  class='aMaindiv mt-3' style='background-color: "+bgcolor+";'>"
            +"<div clas='d-flex align-items-center ml-3 mr-2 mt-2' style=''>"
                if(type=="edit"){
                    ui+='<div class="agileStatDiv d-flex align-items-center ml-3 pt-2" style="">'
                        if(agileType=="E"){
                            ui+="<div id='valueDiv_"+id+"' class='d-none align-items-center' style=''>"
                                +"<span class='pt-0 smallfonts'>Value : </span><span id='valueSpan_"+id+"' class='pl-1 pt-0 smallfonts'></span>"
                            +"</div>"
                        }else if(agileType=="F"){
                            ui+="<div id='functionalityDiv_"+id+"' class='d-none align-items-center' style=''>"
                                +"<span class='pt-0 smallfonts'>Functionality : </span><span id='functionalitySpan_"+id+"' class='pl-1 pt-0 smallfonts'></span>"
                            +"</div>"
                        }else if(agileType=="S"){
                            ui+="<div id='sprintDiv_"+id+"' class='d-none align-items-center' style=''>"
                                +"<img src='images/agile/sprint_blue.svg' style='width:15px;height:15px;' ><span id='sprintSpan_"+id+"' class='pl-1 pt-1 smallfonts'></span>"
                            +"</div>"
                        }
                        
                        ui+="<div id='' class='d-flex align-items-center' style=''>"
                            +'<img src="images/menus/close3.svg" onclick="closeEdit('+id+');event.stopPropagation();" style="width: 10px;position: absolute;cursor:pointer;right:10px;top:25px;">'
                        +'</div>'
                    +'</div>'
                }    
                ui+="<div class='d-flex align-items-center "+cusIdClass+" "+class1+"' style=''>"
                    +"<div class='d-flex align-items-center "+cusIdClass2+"'>"
                        +"<span class='py-0 px-0 m-0' style=''>ID :</span>"
                        +"<input id='agileCustomid_"+id+"' type='text' title='' class='py-0 mx-1 defaultExceedCls' id='' value='' onblur=\"editAgileCustomidblur(this,"+id+");event.stopPropagation();\" onclick='editAgileCustomid(this,"+id+");event.stopPropagation();' style='cursor:pointer;border:none;padding-top: 5px;outline:none;width: 100px;background-color:"+bgcolor+";' autocomplete='off'>"
                    +"</div>"
                    if(type=="edit"){
                        ui+="<div id='epicIconPoints_"+id+"' title='Points' class='px-1 mr-auto d-flex align-items-center' style='display:none;'><img class='agileOptions' style='width:15px !important;height:15px !important;' src='images/idea_old/points.png'><span id='epicIconspan_"+id+"' class='pl-2'></span><input id='epicIconPointsinput_"+id+"' onblur=\"hidepoints("+id+");event.stopPropagation();\" onkeydown=\"updatePoints(event,"+id+");event.stopPropagation();\" value='' class='defaultExceedCls ml-2 d-none' style='background-color: "+bgcolor+";width:50px;outline:none;border:none;border-bottom:1px solid #AAAAAA;'></div>"
                        +"<div id='epicIconPriority_"+id+"' class='px-1 position-relative epicIconPrioritycls' style='display:none;'><img class='agileOptions epicIconPrioritycls' src='images/task/p-six.svg'>"
                            ui+=agilePrioritylist(id,'detailview');
                        ui+="</div>"
                        +"<div id='epicIconComments_"+id+"' title='Comments' class='px-1' style='display:none;'><img class='agileOptions' src='/images/task/task_comments.svg'></div>"
                        +"<div id='epicIconTask_"+id+"' title='Task' class='px-1' onclick=\"openAgileTaskPopup("+id+",'','list');event.stopPropagation();\" style='display:none;'><img class='agileOptions' src='images/idea/task_nofill.svg'></div>"
                        +"<div id='epicIconDoc_"+id+"' title='Documents' class='px-1' style='display:none;'><img class='agileOptions' src='images/idea/document.svg'></div>"
                        +"<div id='epicIconShare_"+id+"' title='Collaborators' onclick=\"agileCollaborators("+id+");event.stopPropagation();\" class='px-1' style='display:none;'><img class='agileOptions' src='images/idea/share_nofill.svg'></div>"
                        
                    }else{
                        ui+='<div class="px-1 ml-1 position-relative">'
                            +"<img id='' src='images/conversation/three_dots.svg' onclick='event.stopPropagation();' class='' style='width:22px;cursor:pointer;'>"
                        +'</div>'
                    }
                    
                ui+="</div>"
                +"<div class='d-flex align-items-center ml-3 pb-2' style=''>"
                    +"<textarea id='agileNametext_"+id+"' onclick='event.stopPropagation();' onkeyup='agileTextareGrow(event,this);event.stopPropagation();' placeholder='Enter your story...' class='wsScrollBar px-0 m-0' style='width:97%;height: 170px;background-color:"+bgcolor+";outline: none;border: none;resize: none;'></textarea>"
                +"</div>" 
                +"<div class='d-flex mx-3 py-2' style=''>"
                    if(type=="edit" && agileType=="E"){
                        ui+="<div id='headerStories_"+id+"' class='d-flex align-items-center' style='font-size:11px;'>"
                            +"<div><span style=''>"+getValues(companyLabels,"Stories")+" :</span></div>"
                            +"<div id='backlogImg_"+id+"' class='px-1 d-flex align-items-center'><img class='headerStoryimages' src='/images/idea_old/Backlog.png' style=''><span id='backlogspan_"+id+"' class='smallfonts pl-1' style=''></span><span class='pl-1'>|</span></div>"
                            +"<div id='inprogressimg_"+id+"' class='px-1 d-flex align-items-center'><img class='headerStoryimages' src='/images/idea_old/Assigned.png' style=''><span id='inprogressspan_"+id+"' class='smallfonts pl-1' style=''></span><span class='pl-1'>|</span></div>"
                            +"<div id='blocked_"+id+"' class='px-1 d-flex align-items-center'><img class='headerStoryimages' src='/images/idea_old/Blocked.png' style=''><span id='blockedspan_"+id+"' class='smallfonts pl-1' style=''></span><span class='pl-1'>|</span></div>"
                            +"<div id='hold_"+id+"' class='px-1 d-flex align-items-center'><img class='headerStoryimages' src='/images/idea_old/Hold.png' style=''><span id='holdspan_"+id+"' class='smallfonts pl-1' style=''></span><span class='pl-1'>|</span></div>"
                            +"<div id='done_"+id+"' class='px-1 d-flex align-items-center'><img class='headerStoryimages' src='/images/idea_old/Done.png' style=''><span id='donespan_"+id+"' class='smallfonts pl-1' style=''></span><span class='pl-1'>|</span></div>"
                            +"<div id='cancel_"+id+"' class='px-1 d-flex align-items-center'><img class='headerStoryimages' src='/images/idea_old/cancel.png' style=''><span id='cancelspan_"+id+"' class='smallfonts pl-1' style=''></span></div>"

                        +"</div>"
                    }
                    if(type=="edit"){
                        ui+="<div id='statusDiv_"+id+"' onclick=\"showStatusoption(this,"+id+");event.stopPropagation();\" class='d-flex align-items-center position-relative statusDivcls ml-auto' style=''>"
                            +"<img src='' class='statusDivcls' style='width:20px;height:20px;'><span class='pl-1 smallfonts'></span>"
                            ui+=statusUpdate(id,'detailview')
                        ui+="</div>"
                    }
                    
                    ui+="<div id='savecanceldiv' class='d-flex align-items-center "+saveplace+"'>"
                        +"<img id=\"\" title=\"Cancel\" class=\"ml-auto\" src=\"/images/task/remove.svg\" onclick=\"cancelCreate("+id+");event.stopPropagation();\" style=\"float:right;height:25px;width:25px;cursor:pointer;\">"
                        +"<img id=\"saveepic_"+id+"\" title=\"Save\" class=\"ml-1\" src=\"/images/task/tick.svg\" onclick=\"createNewAgile("+id+",'"+fromEpic+"',event,'"+type+"');event.stopPropagation();\" style=\"float:right;height:25px;width:25px;cursor:pointer;\">"
                    +"</div>"
                    
                +"</div>"
                
            
            +"</div>"
            
            +"<input id='colorcodehiddenval_"+id+"' type='hidden' value='"+bgcolor+"'>"
        +"</div>"

    +"</div>"

    return ui;
}

function showStatusoption(obj,epicid){

    if($('#statusDiv_'+epicid).children('div').hasClass('d-none') == true){
        $('#statusDiv_'+epicid).children('div').addClass('d-block').removeClass('d-none');
    }else{
        $('#statusDiv_'+epicid).children('div').addClass('d-none').removeClass('d-block');
    }

}

function showStatusupdate(obj,epicid){
    if($(obj).children('div').hasClass('d-none') == true){
        $('.agileMoreOption1').addClass('d-none').removeClass('d-block');
        $(obj).children('div').addClass('d-block').removeClass('d-none');
    }else{
        $(obj).children('div').addClass('d-none').removeClass('d-block');
    }
}

function updateAgileCustomid(epicid,event,type){
    var code = null;
	code = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if(code == 13){
        var customid = $('#agileCustomid_'+epicid).val();
        updateAgileCid(customid,epicid,type);
    }
}

function updateagile(epicid,agileType,parentid){
    var editAgileTitle = $('#agileNametext_'+epicid).val().trim();
    if(editAgileTitle == ""){
        if(agileType=="E"){
            alertFunNew("Epic Name cannot be empty!",'error');
        }else if(agileType=="F"){
            alertFunNew("Feature Name cannot be empty!",'error');
        }else{
            alertFunNew("Story Name cannot be empty!",'error');
        }
        
        $('#agileNametext_'+epicid).focus();
        
    }else{
          if(typeof(parentid)=="undefined"){
            parentid="";
          }
          let jsonbody=	{
            "user_id" : userIdglb,
            "company_id" : companyIdglb,
            "project_id" : prjid,
            "epic_title" : editAgileTitle,
            "epic_type" : agileType,
            "localOffsetTime" : localOffsetTime,
            'epic_id' : epicid
          }
          
          $('#loadingBar').addClass('d-flex').removeClass('d-none');
          $.ajax({
            url: apiPath + "/" + myk + "/v1/updateAgile",
            type: "PUT",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            beforeSend: function (jqXHR, settings) {
              xhrPool.push(jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
              checkError(jqXHR, textStatus, errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');
              
            },
            success: function (result) { 
              
              $('#orgAgileDiv_'+epicid).children('span').text(editAgileTitle);
              $('#orgAgileDiv_'+epicid).children('span').attr('title',editAgileTitle);
              $('#storytitle_'+epicid).text(editAgileTitle).attr('title',editAgileTitle);
              cancelCreate(epicid);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');
            }
            
          }); 
    }

}

function closeEdit(id){
    $("#AgileDiv_"+id).remove();
}

function editAgileCustomid(obj,id){
  $(obj).css('border-bottom', '1px solid #AAAAAA');
}
function editAgileCustomidblur(obj,id){
    //if($('#agileCustomid_'+id).val().trim()==""){
        $(obj).css('border-bottom', 'none');
    //}
    
}

async function createNewAgile(id,fromEpic,event,type){

  var type = $('#AgileDiv_'+id).attr('agiletype');
  var epictitle = $('#agileNametext_'+id).val();
  var agileCid = $('#agileCustomid_'+id).val().trim();
  var color = $('#colorcodehiddenval_'+id).val();
  if(epictitle == ""){
    if(type=="E"){
        alertFunNew("Epic Name cannot be empty!",'error');
    }else if(type=="F"){
        alertFunNew("Feature Name cannot be empty!",'error');
    }else{
        alertFunNew("Story Name cannot be empty!",'error');
    }
    
    $('#agileNametext_'+epicid).focus();
    
    return false;
  }else{
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    let jsonbody = {
        "user_id": userIdglb,
        "company_id": companyIdglb,
        "project_id": prjid,
        "epic_title": epictitle,
        "epic_type": type,
        "parent_epic_id": id,
        "localOffsetTime": localOffsetTime,
        "color_code": color
    }
    await $.ajax({
        url: apiPath + "/" + myk + "/v1/postAgile",
        type: "POST",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        beforeSend: function (jqXHR, settings) {
        xhrPool.push(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        
        },
        success: function (result) { 

        if(fromEpic==="fromSprint"){
            if($('#sprintStorylistUIMainDiv').length == 0){
                if($('#sprintContentGridListDiv').children().length){
                    
                }else{
                    showStorieslist(type,$('#sprintIdHidden').val(),"fromSprintCreate",result[0].epic_id);
                }
                
            }else{
                $('#storysprintcontentdiv').prepend(preparesprintStorylistUILoop(result,type));
                $('#storyList_'+result[0].epic_id).find('input#storylistckeckbox_'+result[0].epic_id).trigger('click');
                var elmnt = document.getElementById("storyList_"+result[0].epic_id);
                elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
            }
            $('#storyList_'+result[0].epic_id).addClass('newadded');
            $(".agileDivCls").remove();
        }else if(fromEpic=="fromSprintgrid"){
            var stid = result[0].epic_id+",";
            var spid = $('#AgileDiv_0').parent().attr('id').split('_')[1];
            adStorytoSprint(spid,stid);
        }else{   

            if(id==0){
                if(type == "S" || type == "F"){
                    if($('#storyBoxes').length == 0){
                        $('#unorganizedAgile').prepend(storiesUi(result,'unorganized',0,type));
                    }else{
                        $('#storyBoxes').prepend(multiplestories(result,'unorganized'));
                    }    
                    nodeDragnDrop3();
                }else{
                    //$('#agilelistUL_0').prepend(prepareAgileUI(result));
                    $(prepareAgileUI(result)).insertAfter(".orgheader");
                    nodeDragnDrop();
                    nodeDragnDrop2();
                    nodeDragnDrop3();
                }
            }else{
                if(type == "F"){
                    
                    if($('#agileStoriesUL_'+id).children().length == 0){
                        showFeaturesNstories(id,"E");
                        $('#orgAgileDiv_'+id).append("<div id='bgBoxE_"+id+"' class='backgroundBox p-0 rounded' style='background-color: #d89cd8;cursor:pointer;'></div>");
                        $('#orgAgileDiv_'+id).attr('onclick','showFeaturesNstories('+id+',\"E\");event.stopPropagation();');
                    }else{
                        $('#agileStoriesUL_'+id).prepend(prepareAgileUI(result));
                        nodeDragnDrop2();
                    }
                    
                }else if(type == "S"){
                    if(fromEpic == "fromEpic"){
                        if($('#agileStoriesUL_'+id).children().length == 0){
                            showFeaturesNstories(id,"E");
                            $('#orgAgileDiv_'+id).append("<div id='bgBoxE_"+id+"' class='backgroundBox p-0 rounded' style='background-color: #d89cd8;cursor:pointer;'></div>");
                            $('#orgAgileDiv_'+id).attr('onclick','showFeaturesNstories('+id+',\"E\");event.stopPropagation();');
                        
                        }else{
                            $('#agileStoriesUL_'+id).prepend(prepareAgileUI(result));
                            //nodeDragnDrop2();
                        }
                    }else{
                        if($('#agileStoriesUL_'+id).children().length == 0){
                            showFeaturesNstories(id,"F");
                            $('#orgAgileDiv_'+id).append("<div id='bgBoxE_"+id+"' class='backgroundBox p-0 rounded' style='background-color: #d89cd8;cursor:pointer;'></div>");
                            $('#orgAgileDiv_'+id).attr('onclick','showFeaturesNstories('+id+',\"F\");event.stopPropagation();');
                        }else{
                            $('.epicStory_'+id).prepend(multiplestories(result,'organized'));
                            $('#storyDiv_'+result[0].epic_id).show();
                            //nodeDragnDrop3();
                            /* if($('#storyBoxes').children('.storyBox').length){
        
                            } */
                        }
                    }
                    nodeDragnDrop2();
                    nodeDragnDrop3();
                }
            }

            if(agileCid!=""){
                updateAgileCid(agileCid,result[0].epic_id,type);
            }
            
            $("#AgileDiv_"+id).fadeOut(300, function(){ $(this).remove();});
            
        }
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }    
    });  
  }  
}

function updateAgileCid(text,epicid,type){
    let jsonbody=	{
        "user_id" : userIdglb,
        "company_id" : companyIdglb,
        "epic_id" : epicid,
        "project_id" : prjid,
        "epic_type" : type,
        "unique_cust_id" : text

      }
      $.ajax({
        url: apiPath + "/" + myk + "/v1/updateCustomId",
        type: "PUT",
        //dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        beforeSend: function (jqXHR, settings) {
          xhrPool.push(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');
          
        },
        success: function (result) { 
         if(result == "success"){
            $('#agileCustomid_'+epicid).blur();
         }
    
         //$('#loadingBar').removeClass('d-flex').addClass('d-none');
        }
      });
}

function cancelCreate(id){
    $("#AgileDiv_"+id).fadeOut(300, function(){ $(this).remove();});
}

function changeAgileType(id){
    var type = $('#agileSelType :selected').val();
    if(type == "S"){
        $('#agileMainDiv_'+id+' , #agileNametext_'+id+' , #agileCustomid_'+id).css('background-color','#f6b847');
        $('#agileNametext_'+id).attr('placeholder','Enter your story...');
    }else if(type == "E"){
        $('#agileMainDiv_'+id+' , #agileNametext_'+id+' , #agileCustomid_'+id).css('background-color','#eeabb6');
        $('#agileNametext_'+id).attr('placeholder','Enter your Epic...');
    }else if(type == "F"){
        $('#agileMainDiv_'+id+' , #agileNametext_'+id+' , #agileCustomid_'+id).css('background-color','#d89cd8');
        $('#agileNametext_'+id).attr('placeholder','Enter your Feature...');
    }
    $('#AgileDiv_'+id).attr('agiletype',type);
}

function agileTextareGrow(e, obj){
	if (e.keyCode == 13 || e.keyCode == 8 || e.keyCode == 91 ){
      let scrollHeight = $(obj).get(0).scrollHeight-20;
      console.log("scrollHeight-->"+scrollHeight)
      if(scrollHeight > 150 && scrollHeight < 220){
		$(obj).css('height', scrollHeight+10+'px');
        $(obj).parent().css('height', scrollHeight+10+'px');
      }
	  if(scrollHeight > 150){
        $(obj).css('overflow-y', 'auto');
      }else{
	    $(obj).css('overflow-y', 'hidden');
	  }
  }
}  

function getOrderDataAgile(place){
    ////var parentId=$('#mainIdea_'+ideaId).parent('ul').attr('id').split('_')[1];
    var id="";
    var order="";
    var jsonvalue = '[';
    if(place == "unorganized"){
        $('#storyBoxes').find('.agileMainDivCls').each(function(){
            id = $(this).attr('id').split('_')[1];
            order = $(this).index();
            jsonvalue+="{";
            jsonvalue+="\"epic_id\":\""+id+"\",";
            jsonvalue+="\"epic_order\":\""+order+"\"";
            jsonvalue+="},";
            
        });
    }else{
        $('#agilelistUL_0').find('.agileMainDivCls').each(function(){
            id = $(this).attr('id').split('_')[1];
            order = $(this).index();
            jsonvalue+="{";
            jsonvalue+="\"epic_id\":\""+id+"\",";
            jsonvalue+="\"epic_order\":\""+order+"\"";
            jsonvalue+="},";
            
        });
    }
    
    var jsonresult="";
    if(jsonvalue.length>1){
      jsonresult = jsonvalue.substring(0, jsonvalue.length-1);
      jsonresult=jsonresult+"]";
    }else{
      jsonresult ="[]";
    }
    console.log("jsonresult--->"+jsonresult);
    return jsonresult;
  }

  async function openAgileTaskPopup(agileid,agiletitle,place,stagetype,pid){
    
    let jsonbody = {
        "user_id":userIdglb,
        "company_id":companyIdglb,
        "ideaId":agileid,
        "project_id":prjid,
        "type":stagetype,
        "pId": pid
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
    await $.ajax({
          url: apiPath + "/" + myk + "/v1/getTaskDetails/agile",
          type: "POST",
          contentType: "application/json",
          data: JSON.stringify(jsonbody),
          error: function (jqXHR, textStatus, errorThrown) {
              checkError(jqXHR, textStatus, errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');  
          },
          success: function (result) {
            $('#otherModuleTaskListDiv').append(agileTaskUI(agileid,agiletitle)).show();
            $('#agileTasksListHeaderDiv').append(wsTaskUi("sprint"));
      
            $("#agileTasksListDivbody").html(createTaskUI(result,stagetype));

            if(place=="options"){
                $("#headerTask, #taskList, .popupTaskDiv").hide();
            }

            for(let i=0;i<result.length;i++){
                //  console.log(result[i].approval_id);
                 for(let j=0;j<result[i].approval_id.length;j++){
                  // console.log("status--->"+result[i].approval_id[j].user_task_status);
                  // console.log("user_id--->"+result[i].approval_id[j].user_id);
                  // $('#rolenew1_'+result[i].approval_id[j].user_id+result[i].task_id).val(result[i].approval_id[j].user_task_status);
          
                  if(result[i].approval_id[j].user_task_status=="Pending approval"){
                    // apprImgPath="/images/task/pending_approval.svg";
                    // appTitle="Pending approval";
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/pending_approval.svg');
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','Pending approval');
                    $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).hide();
                  }else if(result[i].approval_id[j].user_task_status=="Approved"){
                    // apprImgPath="/images/task/approved.svg";
                    // appTitle="Approved";
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/approved.svg');
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','Approved');
                    $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).show();
                  }else if(result[i].approval_id[j].user_task_status=="Rejected"){
                    // apprImgPath="/images/task/rejected.svg";
                    // appTitle="rejected";
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/rejected.svg');
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','Rejected');
                    $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).hide();
                  }else if(result[i].approval_id[j].user_task_status=="On hold"){
                    // apprImgPath="/images/task/on_hold.svg";
                    // appTitle="On hold";
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/on_hold.svg');
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','On hold');
                    $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).hide();
                  }else if(result[i].approval_id[j].user_task_status=="Cancel"){
                    // apprImgPath="/images/task/cancel.svg";
                    // appTitle="Cancel";
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/cancel.svg');
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','Cancel');
                    $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).hide();
                  }
          
          
                  if(result[i].approval_id[j].user_id==userIdglb){
          
                  }else{
                    $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).removeAttr("onclick");
                  }
                 }
                
            }



            if(place=="options" || place=="SB"){
              $('.createagileTaskbutton').trigger('click');
              $("#taskname").val(agiletitle);
            }

            $('.groupDiv').addClass('d-none').removeClass('d-flex');
            $('.actualDiv ').addClass('mr-auto');
            
            if($("#agileTasksListDivbody").html().length==0){
              $("#agileTasksListDivbody").append("<div class='d-flex justify-content-center mt-3 notaskdiv'>No Task Found</div>");
            } 

            getSprintlist(agileid);

            if(place=="SB"){
                glbsprintplace="SB";
                AddUserToTheTask();
                /* $('#participantsUIdivArea').trigger('click');
                $('#taskassignee_'+userIdglb).trigger('click'); */
            }
            
            $('.dhtmlxcalendar_material').hide();
           
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        
          }
      });
    
  }

 

  


  
  

  function agileTaskUI(agileid,agiletitle,place){
	var ui="";
	$('#otherModuleTaskListDiv').html('');
    $("#transparentDiv").show();
    
    ui='<div class="agileTask" id="" agileid='+agileid+'>'
        +'<div class="" style="">'
        +'<div class="modal-content px-2" onclick="" style="z-index:1000 !important;max-height: 550px;height: 550px;width: 88%;margin-left: 120px;">'
            +'<div class="modal-header pt-2 pb-0 px-0" style="border-bottom: 1px solid #b4adad;height:30px;">'
            +'<div class=" d-flex  align-items-center w-100" style="">'
                +'<div class="pl-2 d-none" style="font-size:12px;"><img src="/images/menus/sprint.svg" style="width:17px;height:17px;"><span class="pl-2">Agile Task</span></div>'
                +"<div class=\"ml-auto pr-3 pb-1 d-none\" title=\"Create Task\"><img class=\"createagileTaskbutton\" onclick=\"createagileTask('"+agiletitle+"');event.stopPropagation();\" src=\"images/task/plus.svg\" style=\"height:18px;width:18px; cursor:pointer;\"></div>"
            +'</div>'
            //+'<button type="button" onclick="closeagileTask();" class="close p-0" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
            +'<img src="images/menus/close3.svg" onclick="closeagileTask();" class="" style="cursor:pointer;width:10px;height:10px;margin-top:1px;margin-right:0px;">'
            +'</div>'
            +'<div class="p-0 wsScrollBar" style="overflow: auto;height: inherit;">'
            +'<div class="py-1 w-100 task_type_divison" style="color:#fff;font-size:12px;">'
                +'<div class="pl-2 d-flex align-items-center" style="font-size:14px;">'
                +'<img src="/images/agile/sprinttask1.svg" style="width:17px;height:17px;">'
                +'<span class="pl-2" style="">Sprint Task</span>'
                +'</div>'
            +'</div>'
            +'<div id="agilecreateTaskDivbody" class="p-0" style=""></div>'
            +'<div class="popupTaskDiv">'
                +'<div id="agileTasksListHeaderDiv" class="p-0" style=""></div>'
                +'<div id="agileTasksListDivbody" class="p-0" style=""></div>'
            +'</div>'    
            +'</div>'
            
        +'</div>'
        +'</div>'
    +'</div>'

    return ui;
  }

  function closeagileTask(){
    $('#otherModuleTaskListDiv').html('').hide();
    $("#transparentDiv").hide();
  }

  function createagileTask(){
    insertCreateTaskUI();
    //$('#taskname').val(ideatitle).focus();
    $(".thirdDiv").addClass('d-flex').removeClass('d-none');
  }


  function readFileUrlinAgile(input,epicid){
    let inputFiles = input.files[0];
    if (input.files && input.files[0]) {
      if(input.files[0].size >= 25000000){
        confirmReset(getValues(companyAlerts,"Alert_ResetMoreSize"),'delete','docUploadInComments','cancelTheDragAndDropUpload');
        $('.alertMainCss').css({'width': '28.5%'});
        $('.alertMainCss').css({'left': '45%'});
        $('#BoxConfirmBtnOk').css({'width': '21%'});
        $('#BoxConfirmBtnOk').val('Continue');
      }else{
        uploadAgileDocument(inputFiles,epicid);
      }
    }
  }

  async function uploadAgileDocument(inputFiles,epicid){
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    
    var formData = new FormData();
    formData.append('file', inputFiles);
    formData.append('place', 'agileDoc');
    formData.append('resourceId', epicid);
    formData.append('projId', prjid);
    formData.append('user_id', userIdglb);
    formData.append('company_id', companyIdglb);
    formData.append('taskType', 'Project');
    
    $.ajax({
        url: apiPath+"/"+myk+"/v1/upload", 
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        dataType:'text',
        data: formData,
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success: function(url) {
          let documentId=url.split('@@##')[1];
           
          $('#agileEpicmoreoptdiv_'+epicid).hide();
          $('#epicIconDoc_'+epicid).show().attr('onclick','showDocuments('+epicid+');event.stopPropagation();');
          
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });
    
  }

  function showDocuments(epicid){
    $.ajax({
        url : apiPath+"/"+myk+"/v1/fetchDocs?ideaId="+epicid+"",
        type:"GET",
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            
        },
        success: function (result) {

            $('#DocsLinkListDiv').append(prepareIdeaDocsLinksUi(result)).show();

        }
    });

  }

  function prepareIdeaDocsLinksUi(result){
    var ui="";
    var extension="";
    var type="";
    var epicdocid=""; var epicdoctitle="";var epicdoc="";
    $('#DocsLinkListDiv').html('');
    $("#transparentDiv").show();
    
    ui='<div class="agileDocLinkcls" style="top:130px !important;" id="">'
      +'<div class="mx-auto w-75" style="">'
        +'<div class="modal-content container px-0" style="max-height: 500px !important;">'
          +'<div class="modal-header pt-2 pb-2 pl-3 pr-2 d-flex align-items-center" style="border-bottom: 1px solid #5e5b5b;background-color: #003A5D;color: white;">'
            +'<span class="modal-title" style="width:35%;font-size:14px;">Name</span>'  
            +'<span class="modal-title" style="width:35%;font-size:14px;">Title</span>'
            +'<span class="modal-title" style="width:15%;font-size:14px;">Created By</span>'
            +'<span class="modal-title" style="width:15%;font-size:14px;">Created On</span>'
          +'</div>'
          +'<div class="" style="">'
            +'<button type="button" onclick="closeAgileDocLinkPopup();" class="close p-0 AgileCloseButton" style="top: 5px;right: 10px;color: black;outline: none;color: white;">&times;</button>'
          +'</div>'
          +'<div class="modal-body p-0 justify-content-center wsScrollBar" style="max-height: 400px;height: 400px;overflow: auto;">'
          
          if(result != ""){
            for(var i=0; i<result.length ;i++){
              if(result[i].type == "link"){
                type="Link";
                epicdocid=result[i].epic_link_id;
                epicdoctitle=result[i].link_desc;
                epicdoc=result[i].link_text;
              }else{
                type="Document";
                epicdocid=result[i].epic_documents_id;
                epicdoctitle=result[i].document_title;
                epicdoc=result[i].link_text;
              } 
  
              ui+='<div class="Agiledochover ">'
              +'<div id="AgileDoc_'+result[i].epic_documents_id+'" class="media pl-3 pb-1 pr-2 d-flex align-items-center w-100 position-relative" onmouseover="showagileDocOption('+epicdocid+');" onmouseout="hideagileDocOption('+epicdocid+');" style="font-size:12px;cursor:pointer;border-bottom: 1px solid #cccccc;height: 45px;">'
              +'<div class="d-flex align-items-center w-100" style="padding-top: 5px;">'
                if(result[i].type == "link"){
                  ui+="<div class=\"defaultExceedCls material-textfield\" onclick='' style='width:35%;'><span id='agilespanurl_"+result[i].epic_link_id+"' title='"+result[i].link_text+"' onclick=\"openIdeaUrl('"+result[i].link_text+"');\" style='width:35%;cursor:pointer;color: blue;'>"+result[i].link_text+"</span>"
                    +"<input id='agileinputurl_"+result[i].epic_link_id+"' placeholder=' ' class='inputagileclass_"+result[i].epic_link_id+" editagileclass material-textfield-input1 newinput1 py-1' placeholder=' ' value='"+result[i].link_text+"' title='"+result[i].link_text+"'  style='display:none;cursor:pointer;outline:none;border:1px solid #C1C5C8;' >"
                    +'<label class="material-textfield-label m-0 ml-1 inputagileclass_'+result[i].epic_link_id+'" style="display:none;font-size: 12px;top: 1px !important">Link</label>'
                  +"</div>"
                  +'<div class="defaultExceedCls material-textfield" onclick="" style="width:35%;"><span id="agilespanDocname_'+result[i].epic_link_id+'" title="'+result[i].link_desc+'">'+result[i].link_desc+'</span>'
                    +'<input id="agileinputDocname_'+result[i].epic_link_id+'" class="inputagileclass_'+result[i].epic_link_id+' editagileclass material-textfield-input1 newinput1 py-1" value="'+result[i].link_desc+'" title="'+result[i].link_desc+'" style="display:none;cursor:pointer;outline:none;border:1px solid #C1C5C8;" >'
                    +'<label class="material-textfield-label m-0 ml-1 inputagileclass_'+result[i].epic_link_id+'" style="display:none;font-size: 12px;top: 1px !important">Title</label>'
                  +'</div>'
                }else{
                  extension=result[i].document_name;
                  extension = extension.substr(extension.lastIndexOf(".")+1).toLowerCase();
                  if(extension.toLowerCase() == "jpeg" || extension.toLowerCase() == "jpg" || extension.toLowerCase() == "png" || extension.toLowerCase() == "gif" || extension.toLowerCase() == "bmp" ){
                    ui+="<div id='agileidthumbnailOpenUi' ondblclick=\"expandImage("+result[i].document_id+",'"+extension+"');\" class='defaultWordBreak position-relative d-flex align-items-center' style='width:35%;'>"
                      +"<img src=\"images/document/"+extension+".svg\" onclick=\"agilethumbnailOpenUi(this,"+result[i].document_id+",'"+extension+"',"+result[i].epic_id+");\" style=\"width: 20px;\">"
                  }else if(extension.toLowerCase() == "mp4" || extension.toLowerCase() == "webm" || extension.toLowerCase() == "mov" || extension.toLowerCase() == "ogg" || extension.toLowerCase() == "avi" || extension.toLowerCase() == "mpg"){
                    ui+="<div id='agilevideoOpenUi' onclick=\"openVideo("+result[i].document_id+",'"+extension+"',"+prjid+");\" class='defaultWordBreak position-relative d-flex align-items-center vDoc_"+result[i].epic_id+"' style='width:35%;'>"
                      +"<video id='agileuploadedVideo' class='float-left' style='display:none;cursor: pointer; height:80px; width:130px;background-color: #000;'  id='view_"+result[i].epic_documents_id+"'><source src='"+lighttpdpath+"//projectDocuments//"+result[i].epic_documents_id+"."+extension+"'></video>"
                      +'<img src="images/conversation/videoplay.svg" style="width: 20px;">'
                  }else{
                    ui+="<div id='agileidviewordownloadUi' class='defaultWordBreak position-relative d-flex align-items-center' style='width:35%;'>"
                    +"<img onclick=\"agileviewordownloadUi("+result[i].document_id+",this,'"+extension+"',"+result[i].epic_id+")\" class='vDoc_"+result[i].epic_id+"' style='cursor: pointer;width:20px;' src='images/document/"+extension+".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_"+result[i].epic_documents_id+"' class='downCalIcon '>"
                    
                  }
                  ui+='<span class="pl-2 defaultExceedCls" title="'+result[i].document_name+'">'+result[i].document_name+'</span>'
                  +"</div>"
                  +'<div class="defaultExceedCls material-textfield" onclick="" style="width:35%;"><span id="agilespanDocname_'+result[i].epic_documents_id+'" title="'+result[i].document_title+'">'+result[i].document_title+'</span>'
                    +'<input id="agileinputDocname_'+result[i].epic_documents_id+'" class="inputagileclass_'+result[i].epic_documents_id+' editagileclass material-textfield-input1 newinput1 py-1" value="'+result[i].document_title+'" title="'+result[i].document_title+'" style="display:none;cursor:pointer;outline:none;border:1px solid #C1C5C8;" >'
                    +'<label class="material-textfield-label m-0 ml-1 inputagileclass_'+result[i].epic_documents_id+'" style="display:none;font-size: 12px;top: 1px !important">Title</label>'
                  +'</div>'
                }
                
                
                ui+='<div class="defaultExceedCls" style="width:15%;"><span title="'+result[i].owner+'">'+result[i].owner+'</span></div>'
                +'<div class="defaultExceedCls" style="width:15%;"><span>'+result[i].created_date+'</span></div>'
              +'</div>'
              +"<div id='agiledocOptions_"+epicdocid+"' class='position-absolute px-2 py-1' style='display:none;right: 5px;top: 7px;z-index: 1;border: 1px solid rgb(193, 197, 200);right: 5px;background-color: rgb(255, 255, 255);border-radius: 8px;'>"
                +"<img id=\"cancelagiledoc_"+epicdocid+"\" title=\"Cancel\" class=\"img-responsive\" src=\"/images/task/remove.svg\" onclick=\"cancelagiledocs("+epicdocid+",'"+type+"','"+epicdoctitle+"','"+epicdoc+"');\" style=\"display:none;height:20px;width:20px;cursor:pointer;margin-right: 5px;\">"
                +"<img id=\"updateagiledoc_"+epicdocid+"\" title=\"Update\" class=\"img-responsive\" src=\"/images/task/tick.svg\" onclick=\"updateEditedagileDocs("+epicdocid+",'"+type+"');\" style=\"display:none;height:20px;width:20px;cursor:pointer;\">"
                +"<img id=\"editagiledoc_"+epicdocid+"\" title=\"Edit\" class=\"img-responsive\" src=\"/images/conversation/edit.svg\" onclick=\"showagileeditplace("+epicdocid+",'"+type+"');\" style=\"height:18px;width:18px;margin-right: 5px;cursor:pointer;\">"
                +"<img id=\"deleteagiledoc_"+epicdocid+"\" title=\"Delete\" class=\"img-responsive\" src=\"/images/conversation/delete.svg\" onclick=\"deleteagiledocsConfirm("+epicdocid+",'"+type+"');\" style=\"height:15px;width:15px;cursor:pointer;\">"
              +"</div>"
               
              +'</div>'
              +'</div>'
            }
          }else{
            ui+='<span>No Files Found.</span>'
          }
          
          ui+='</div>'
        +'</div>'
      +'</div>'
    +'</div>'
  
    return ui;
    
  }

  function deleteagiledocsConfirm(docid,type){
    conFunNew(getValues(companyAlerts,"Alert_delete"),"warning","deleteagiledocs","",docid,type);
  }
  
  function deleteagiledocs(docid,type){
    
    //$('#loadingBar').addClass('d-flex').removeClass('d-none'); 
      type = type=="Document"?"doc":type; 
      $.ajax({
          url: apiPath + "/" + myk + "/v1/deleteDoc?type="+type+"&doc_id="+docid+"&userId="+userIdglb+"",
          type: "DELETE",
          error: function (jqXHR, textStatus, errorThrown) {
              checkError(jqXHR, textStatus, errorThrown);
              //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
          },
          success: function (data) {
          
            $('#AgileDoc_'+docid).parent().remove();
            //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
          }
      });
  }

  function agilethumbnailOpenUi(obj,docid,ext,epicid){
	var ui="";
    placefordocview="agile";
	$('#agileviewthumbnail').remove();
	
	ui='<div id="agileviewthumbnail" class="actFeedOptionsDiv d-block" style="left: 25px;top: -3px;display: none;right: unset;border-radius: 8px;">' 
		//+'<div class="arrow-left"></div>
		+'<div class="d-flex align-items-center">'
		+"<img src=\"images/conversation/expand.svg\" title=\"View\" id='expandOptionImg_"+epicid+"' onclick=\"expandImage("+docid+",'"+ext+"');\" class=\"image-fluid mx-2\" style=\"height: 16px; width:16px; cursor:pointer;\">"
		+"<img src=\"images/conversation/download2.svg\" title=\"Download\" id='downloadOptionImg_"+epicid+"' onclick=\"downloadActFile("+docid+","+prjid+",'"+ext+"',"+epicid+")\" class=\"image-fluid mr-2 ml-1\" style=\"width: 18px; height: 18px; cursor:pointer;\">"
		+'</div>'
	+'</div>'

	$(obj).parent().append(ui);
	
 }

 function agileviewordownloadUi(docid,obj,ext,epicid){
	var ui="";
	placefordocview="agile";
	$('#agileviewdownloadfileinconversation').remove();

	ui='<div id="agileviewdownloadfileinconversation" class="actFeedOptionsDiv d-block" style="left: 25px;top: -3px;right: unset;border-radius: 8px;">' 
		//+'<div class="arrow-left"></div>'
		+'<div class="d-flex align-items-center">'
            +"<img src=\"images/conversation/expand.svg\" title=\"View\" id='viewOptionImg_"+epicid+"' onclick=\"viewdocument("+docid+",'"+ext+"');\" class=\"image-fluid\" style=\"height: 16px; width:16px; cursor:pointer;margin:0px 6px;\">"
            +"<img src=\"images/conversation/download2.svg\" title=\"Download\" id='downloadOptionImg_"+epicid+"'  onclick=\"downloadActFile("+docid+","+prjid+",'"+ext+"',"+epicid+")\" class=\"image-fluid\" style=\"margin:0px 6px;height: 18px; width:18px; cursor:pointer;\">"
		+'</div>'
	+'</div>'
	$(obj).parent().append(ui);

}

  function openIdeaUrl(url){
    window.open(url);
  }

  function updateEditedagileDocs(docid,type){
    var editeddocname = $('#agileinputDocname_'+docid).val();
    var editedlink = $('#agileinputurl_'+docid).val();
    var validlink = isValidURL(editedlink);
    if( typeof(editedlink) != "undefined" && editedlink == '' ){
      alertFunNew(getValues(companyAlerts,"Alert_enterLink"),'error');
      $('#agileinputurl_'+docid).focus();
      return false;
    }else if(typeof(editedlink) != "undefined" && validlink == false){
      alertFunNew(getValues(companyAlerts,"Alert_invalidUrl"),'error');
      $('#agileinputurl_'+docid).focus();
      return false;
    }else{
      if(typeof(editedlink) == "undefined"){
        editedlink="";
      }
      type = type=="Document"?"doc":type; 
      //$('#loadingBar').addClass('d-flex').removeClass('d-none');  
      $.ajax({
        url: apiPath+"/"+myk+"/v1/updateDoc?type="+type+"&doc_id="+docid+"&title="+editeddocname+"&userId="+userIdglb+"&link="+editedlink+"",
        type:"PUT",
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
        },
        success: function (data) {
            
          $('#agileinputDocname_'+docid).attr('value',editeddocname);
          $('#agileinputurl_'+docid).val(editedlink);
          $('#agilespanDocname_'+docid).text(editeddocname);
          $('#agilespanDocname_'+docid).attr('title',editeddocname);
          $('#agilespanurl_'+docid).text(editedlink);
          $('#agilespanurl_'+docid).attr('title',editedlink);
          if(type=="Link"){
            $('#agilespanurl_'+docid).show();
            $('#cancelagiledoc_'+docid).attr('onclick','cancelagiledocs('+docid+',\"'+type+'\",\"'+editeddocname+'\",\"'+editedlink+'\");event.stopPropagation();');
          }else{  
            $('#cancelagiledoc_'+docid).attr('onclick','cancelagiledocs('+docid+',\"'+type+'\",\"'+editeddocname+'\");event.stopPropagation();');
          }
          $('#agilespanDocname_'+docid).show();
          $('.inputagileclass_'+docid).hide();
          $('.inputagileclass_'+docid).parent().addClass('defaultExceedCls');
          $('#editagiledoc_'+docid).show();
          $('#deleteagiledoc_'+docid).show();
          $('#updateagiledoc_'+docid).hide();
          $('#cancelagiledoc_'+docid).hide();
          $('#agiledocOptions_'+docid).removeClass('d-block');
    
          //$('#loadingBar').addClass('d-none').removeClass('d-flex');  
        }
      });
    } 
    
  }

  function showagileeditplace(docid,type){
    if(type=="Link"){
      $('#agilespanurl_'+docid).hide();
    }
    $('#agilespanDocname_'+docid).hide();
    $('.inputagileclass_'+docid).show();
    $('.inputagileclass_'+docid).parent().removeClass('defaultExceedCls');
    $('#editagiledoc_'+docid).hide();
    $('#deleteagiledoc_'+docid).hide();
    $('#updateagiledoc_'+docid).show();
    $('#cancelagiledoc_'+docid).show();
    $('#agiledocOptions_'+docid).addClass('d-block');
    
  }

  function cancelagiledocs(docid,type,dname,dtitle){
    if(type=="Link"){
      $('#agilespanurl_'+docid).show();
      $('#agileinputDocname_'+docid).val(dname);
      $('#agileinputurl_'+docid).val(dtitle);
    }else{
      $('#agileinputDocname_'+docid).val(dname);
      $('#agileinputurl_'+docid).val(dname);
    }
    $('#agilespanDocname_'+docid).show();
    $('.inputagileclass_'+docid).hide();
    $('.inputagileclass_'+docid).parent().addClass('defaultExceedCls');
    $('#editagiledoc_'+docid).show();
    $('#deleteagiledoc_'+docid).show();
    $('#updateagiledoc_'+docid).hide();
    $('#cancelagiledoc_'+docid).hide();
    $('#agiledocOptions_'+docid).removeClass('d-block');
    /* $('#agileinputDocname_'+docid).val(dtitle);
    $('#agileinputurl_'+docid).val(dname); */
    
  }

  function showagileDocOption(document_id){
    $('#agiledocOptions_'+document_id).show();
  }
  
  function hideagileDocOption(document_id){
    $('#agiledocOptions_'+document_id).hide();
  }

  function closeAgileDocLinkPopup(){
    $('#DocsLinkListDiv').html('').hide();
    $("#transparentDiv").hide();
  }

  function attlinkPopup(epicid){
	var ui="";
	$('#attachlinkDivData').html('');
    $("#transparentDiv").show();
    ui+='<div class="agileAttacklink" id="">'
        +'<div class="modal-dialog">'
        +'<div class="modal-content container">'
        
            
            +'<div class="modal-header py-2 pl-0" style="border-bottom: none !important;">'
            +'<p class="modal-title" style="font-size:14px;color: black;font-weight: normal;">Attach Link</p>'
            +'<button type="button" onclick="closeAttlink();" class="close p-0" data-dismiss="agileAttacklink" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
            +'</div>'
            
            
            +'<div class="pt-2 pl-0" style="">'
            +'<div class="p-0 " style="font-size:11px;">'
                +'<div class="py-2 material-textfield">'
                +'<input id="agileAttlinkTitle" class="px-2 py-0 material-textfield-input1 newinput w-100" placeholder=" "  style="outline:none;"/>'
                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;top:25px">Title</label>'
                +'</div>'
                +'<div class="py-2 material-textfield">'  
                +'<input id="agileAttlinkaddress" class="px-2 py-0 material-textfield-input1 newinput w-100" placeholder=" " style="outline:none;"/>'
                +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;top:25px">Link</label>'
                +'</div>'  
                +'</div>'
            +'</div>'
            
            +'<div class="modal-footer py-2 px-0" style="border-top: none !important;">'
            +'<img src="/images/task/remove.svg" title="Cancel" onclick="closeAttlink();" style="width:25px;height:25px;cursor:pointer;">'
            +'<img src="/images/task/tick.svg" title="Attach" onclick="attachagilelink('+epicid+');" style="width:25px;height:25px;cursor:pointer;">'
            +'</div>'
            
        +'</div>'
        +'</div>'
    +'</div>'

    $('#attachlinkDivData').append(ui).show();
  }

  function closeAttlink(){
    $('#attachlinkDivData').html('').hide();
    $("#transparentDiv").hide();
  }

  function attachagilelink(epicid){
    var linktitle = $('#agileAttlinkTitle').val();
    var link = $('#agileAttlinkaddress').val().trim();
    var validlink = isValidURL(link);
    if( link == '' ){
        alertFunNew(getValues(companyAlerts,"Alert_enterLink"),'error');
        $("#agileAttlinkaddress").focus();
        return false;
    }else if(validlink == false){
        alertFunNew(getValues(companyAlerts,"Alert_invalidUrl"),'error');
        $("#agileAttlinkaddress").focus();
        return false;
    }else{
        $('#loadingBar').addClass('d-flex').removeClass('d-none');
        $.ajax({
            url : apiPath+"/"+myk+"/v1/uploadLink?ideaId="+epicid+"&title="+linktitle+"&link="+link+"&roleid="+userroleid+"&user_id="+userIdglb+"&company_id="+companyIdglb+"",
            type:"POST",
            error: function (jqXHR, textStatus, errorThrown) {
                checkError(jqXHR, textStatus, errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
                
            },
            success: function (result) {
                closeAttlink();
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
    
            }
        });
    }
    
  }

  function updatepriority(obj,epicid,view){
    
    var value = $(obj).attr('priority');
    
    $.ajax({
        url: apiPath + "/" + myk + "/v1/updatePriority?ideaId="+epicid+"&priority="+value+"&user_id="+userIdglb+"",
        type: "PUT",
        beforeSend: function (jqXHR, settings) {
          xhrPool.push(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
         
          
        },
        success: function (result) { 

            if(view == "detailview"){
                var priority = value;
                var priorityimage = (priority=="")? "images/task/p-six.svg" : priority=="1" ? "images/task/p-one.svg" : priority=="2" ? "images/task/p-two.svg":priority=="3" ? "images/task/p-three.svg" :priority=="4" ? "images/task/p-four.svg": "images/task/p-five.svg";
                var prioritytitle = (priority=="")? "None" : priority=="1" ? "Very Important" : priority=="2" ? "Important":priority=="3" ? "Medium" :priority=="4" ? "Low": "Very Low";
            
                $('#epicIconPriority_'+epicid).children('img').attr('src',priorityimage).attr('title',prioritytitle);
                
            }
            
            $('.agileMoreOptionsub').addClass('d-none').removeClass('d-block');
            
        }
    });   

  }

  function updatePoints(event,epicid){
    var code = event.keyCode;
	if(code == 13){
        var pointsvalue = $('#epicIconPointsinput_'+epicid).val();
        var pointsoldvalue = $('#epicIconspan_'+epicid).text();
        if(pointsvalue.trim() == ""){
            //confirmReset(getValues(companyAlerts,"Alert_EmyPtCont"),'reset','pointContinue','pointExit');
            confirmResetNew(getValues(companyAlerts,"Alert_EmyPtCont"),'warning','updatepointszero','cancelpoints','Yes','No',epicid);
		   	return false;
        }else{
            var intRegex = /^\d+$/;
		    var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
		
		    if(intRegex.test(pointsvalue) || floatRegex.test(pointsvalue)){
                $.ajax({
                    url: apiPath+"/"+myk+"/v1/updateEpicPoint?epicPoint="+pointsvalue+"&epic_id="+epicid+"&user_id="+userIdglb+"",
                    type: "PUT",
                    beforeSend: function (jqXHR, settings) {
                      xhrPool.push(jqXHR);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                      checkError(jqXHR, textStatus, errorThrown);
                    },
                    success: function (result) { 
                        $('#epicIconPointsinput_'+epicid).val(pointsvalue).addClass('d-none').removeClass('d-block');
                        $('#epicIconspan_'+epicid).text(pointsvalue).addClass('d-block').removeClass('d-none');
                    }
                });  
            }else{
                $('#epicIconPointsinput_'+epicid).addClass('d-none').removeClass('d-block');
                $('#epicIconspan_'+epicid).addClass('d-block').removeClass('d-none');
                $('#epicIconPointsinput_'+epicid).val(pointsoldvalue);
                alertFunNew(getValues(companyAlerts,"Alert_numericValues"),'error');
            }
            
        }
         
    }
  }

  function updatepointszero(epicid){
    $.ajax({
        url: apiPath+"/"+myk+"/v1/updateEpicPoint?epicPoint=0&epic_id="+epicid+"&user_id="+userIdglb+"",
        type: "PUT",
        beforeSend: function (jqXHR, settings) {
          xhrPool.push(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
        },
        success: function (result) { 
            $('.agileMoreOptionsub').addClass('d-none').removeClass('d-block');
        }
    });  
  }

  function editpoints(epicid){

    $('#epicIconPointsinput_'+epicid).addClass('d-block').removeClass('d-none');
    $('#epicIconspan_'+epicid).addClass('d-none').removeClass('d-block');

  }

  function hidepoints(epicid){

    $('#epicIconPointsinput_'+epicid).addClass('d-none').removeClass('d-block');
    $('#epicIconspan_'+epicid).addClass('d-block').removeClass('d-none');

  }

  function updateStatus(epicid,status,view){
    var comments = $('#statuscomment_'+epicid).val();
    comments = typeof(comments)=="undefined"?"":comments;
    let jsonbody={
        "user_id": userIdglb,
        "company_id": companyIdglb,
        "epic_id": epicid,
        "epic_status": status,
        "epicCmts": comments
    }

    $.ajax({
        url: apiPath + "/" + myk + "/v1/updateEpicStatus",
        type: "PUT",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
             
        },
        success: function (result) {
            var statusimage = status=="Backlog"?"/images/idea_old/Backlog.png":status=="Blocked"?"/images/idea_old/Blocked.png":status=="Hold"?"/images/idea_old/Hold.png":status=="Done"?"/images/idea_old/Done.png":status=="Cancel"?"/images/idea_old/cancel.png":status=="In Progress"?"/images/idea_old/Assigned.png":"";
            if(view=="detailview"){
                $('#statusDiv_'+epicid).children('img').attr('src',statusimage).attr('title',status);
                $('.agileMoreOption2').removeClass('d-block').addClass('d-none');
            }else if(view=="SBlist"){
                $('#SBsprintStatus_'+epicid).attr('src',statusimage).attr('title',status);
                $('.agileMoreOption3').removeClass('d-block').addClass('d-none');
            }else if(view=="agilelists" || view=="optionview"){
                $('#agileliststat_'+epicid).attr('src',statusimage).attr('title',status);
                $('.agileMoreOption1').removeClass('d-block').addClass('d-none');
            }
            closeStatuscomment();

        }
    });        

  }

  function updateStatusui(epicid,status,view){
      var ui="";

      $('#agilepopups').html('');
      $("#transparentDiv").show();

      ui+='<div class="agilepopupcls" id="">'
        +'<div class="modal-dialog">'
            +'<div class="modal-content container">'
            
                
                +'<div class="modal-header py-2 pl-0" style="border-bottom: none !important;">'
                    +'<p class="modal-title" style="font-size:14px;color: black;font-weight: normal;text-transform: uppercase;">'+getValues(companyLabels,"Agile_comment")+'</p>'
                    +'<button type="button" onclick="closeStatuscomment();" class="close p-0" data-dismiss="agilepopupcls" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
                +'</div>'
                
                
                +'<div class="" style="">'
                    +'<div class="p-0 " style="font-size:11px;">'
                        +'<div class="py-2 material-textfield">'
                            +'<textarea id="statuscomment_'+epicid+'" class="px-2 py-2 material-textfield-input1 newinput w-100" placeholder=" "  style="height:100px !important;outline:none;resize:none;"></textarea>'
                            +'<label class="material-textfield-label m-0 ml-1" style="font-size: 12px;top:25px">Enter your comments</label>'
                        +'</div>'
                +'</div>'
                
                +'<div class="modal-footer py-2 px-0" style="border-top: none !important;">'
                    +'<img src="/images/task/remove.svg" title="Cancel" onclick="closeStatuscomment();event.stopPropagation();" style="width:25px;height:25px;cursor:pointer;">'
                    +"<img src=\"/images/task/tick.svg\" title=\"Update\" onclick=\"updateStatus("+epicid+",'"+status+"','"+view+"');event.stopPropagation();\" style=\"width:25px;height:25px;cursor:pointer;\">"
                +'</div>'
                
            +'</div>'
        +'</div>'
      +'</div>'

      $('#agilepopups').append(ui).show();

  }

  function closeStatuscomment(){
    $('#agilepopups').html('').hide();
    $("#transparentDiv").hide();
  }

 function updateValue(obj,epicid){
    
    var stageid = $(obj).attr('epicval');

    $.ajax({
        url: apiPath+"/"+myk+"/v1/updateEpicStages?stage_id="+stageid+"&epic_id="+epicid+"&user_id="+userIdglb+"",
        type: "PUT",
        beforeSend: function (jqXHR, settings) {
          xhrPool.push(jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
        },
        success: function (result) { 
            $('.agileMoreOptionsub').addClass('d-none').removeClass('d-block');
            $('.agileMoreOption ').hide();
        }
    });  
 }

 function agileCollaborators(epicid){

    $.ajax({
        url: apiPath + "/" + myk + "/v1/fetchShareAgileUI?epicId="+epicid+"&projId="+prjid+"",
        type: "GET",
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
             
        },
        success: function (result) {

            $('#ShareListDiv').html(agileCollaboratorslistUI(result,epicid)).show();
        }
    });        
    

 }

 function agileCollaboratorslistUI(result,epicid){
    var ui="";
 
    $('#ShareListDiv').html('');
    $("#transparentDiv").show();

    ui="<div class='agileShareUI' style=''>"    //1
        +'<div class="mx-auto w-75" style="">'      //2
            +'<div class="modal-content container px-0" style="">'    //3
                +'<div class="modal-header py-2 px-3 d-flex align-items-center" style="border-bottom: 1px solid #5e5b5b;background-color: #003A5D;color: white;">'
                    +'<span class="modal-title" style="font-size:14px;width:5%;"></span>'
                    +'<span class="modal-title" style="font-size:14px;width:20%;">Name</span>'
                    +'<span class="modal-title" style="font-size:14px;width:15%;">User Role</span>'
                    +'<span class="modal-title" style="font-size:14px;width:15%;">Mobile</span>'
                    +'<span class="modal-title" style="font-size:14px;width:15%;">Work</span>'
                    +'<span class="modal-title" style="font-size:14px;width:20%;">Email</span>'
                    +'<span class="modal-title" style="font-size:14px;width:10%;text-align:center;">Access</span>'
                    //+'<img src="images/menus/close3.svg" onclick="" class="" style="cursor:pointer;width:10px;height:10px;margin-top:1px;margin-right:0px;">'
                +'</div>'
                +'<div class="" style="">'
                    +'<button type="button" onclick="closeAgileCollab();" class="close p-0 agileCloseButton" style="top: 5px;right: 10px;color: black;outline: none;color: white;">&times;</button>'
                +'</div>'
                +'<div class="modal-body p-0 justify-content-center wsScrollBar" style="max-height: 400px;height: 400px;overflow: auto;">'
                if(result != ""){
                    for(var i=0; i<result.length ;i++){
                      
                      uRole = result[i].proj_user_status;
                      userRole = uRole=="TM"?"Team Member":uRole=="PO"?"Project Manager":"Observer";
                      sharetype = result[i].shared_type;
                      sharetypeimgAccessed = sharetype=="R"?"images/idea/R.svg":"images/idea/W.svg";
                      onclickshare = projectUsersStatus=="PO"?"changeAccessTypeagile(this);":"";
                      accessdisable = projectUsersStatus=="PO"?"":"opacity: 0.5;";
          
                      ui+="<div id='shareList_"+result[i].user_id+"' personuserid="+result[i].user_id+" class='agileSharelist d-flex align-items-center Agiledochover py-2 px-3 ' style='font-size:12px;border-bottom: 1px solid #aaaa;'>"
                        +"<div style='width:5%;'><img class='rounded-circle' src='"+result[i].imageUrl+"' style='width:25px;height:25px;'></div>"
                        +"<div class='defaultExceedCls' style='width:20%;'><span>"+result[i].name+"</span></div>"
                        +"<div class='defaultExceedCls' style='width:15%;'><span>"+userRole+"</span></div>"
                        +"<div class='defaultExceedCls' style='width:15%;'><span>"+result[i].user_phone+"</span></div>"
                        +"<div class='defaultExceedCls' style='width:15%;'><span>"+result[i].work_number+"</span></div>"
                        +"<div class='defaultExceedCls' style='width:20%;'><span>"+result[i].user_email+"</span></div>"
                        +"<div id='' style='width:10%;text-align:center;"+accessdisable+"' class='accesstypediv_"+result[i].user_id+"' shareType='"+sharetype+"' onclick='"+onclickshare+"'><img class='' src='"+sharetypeimgAccessed+"' style='width:25px;cursor:pointer;'></div>"
                      +"</div>"
                    
                    }
                }else{
                    ui+="<span class='d-flex justify-content-center mt-3'>No Users Found.</span>"
                }  
                ui+="</div>"
                +'<div class="modal-footer p-2 border-0">'
                    +'<div id="" onclick="updateAgileCollaborators('+epicid+');event.stopPropagation();" style="width: 70px;float: right;" class="createBtn">Save</div>'
                +'</div>'


            +"</div>"   //3

        +"</div>"   //2
    +"</div>"   //1

    return ui;

 }

function closeAgileCollab(){
    $('#ShareListDiv').html('').hide();
    $("#transparentDiv").hide();
}

function changeAccessTypeagile(obj){
    var type = $(obj).attr('shareType');
    if(type=="R"){
        $(obj).children('img').attr('src','images/idea/W.svg');
        $(obj).attr('shareType','W');
    }else{
        $(obj).children('img').attr('src','images/idea/R.svg');
        $(obj).attr('shareType','R');
    }
}

function updateAgileCollaborators(epicid){
    var userData="[";
    var access="";
    var id="";
    $('.agileSharelist').each(function (){
      id=$(this).attr('id').split('_')[1];
      access=$(this).find('.accesstypediv_'+id).attr('shareType');
      userData+="{";
      userData+="\"user_id\":\""+id+"\",";
      userData+="\"userShareType\":\""+access+"\"";
      userData+="},";
    });
    userData=userData.length>1?userData.substring(0,userData.length-1)+"]":"[]";
  
    let jsonbody = {
        "user_id" : userIdglb,
        "company_id" : companyIdglb,
        "epic_id" : epicid,
        "project_id" : prjid,
        "collabData" : JSON.parse(userData)
    }
    $('#loadingBar').addClass('d-flex').removeClass('d-none');  
        $.ajax({
            url: apiPath + "/" + myk + "/v1/updateCollaborator",
            type: "PUT",
            //dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody),
            error: function (jqXHR, textStatus, errorThrown) {
              checkError(jqXHR, textStatus, errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            },
            success: function (result) {
                closeAgileCollab();
                $('#loadingBar').addClass('d-none').removeClass('d-flex');  
            }
      }); 


}

function agileComments(agileid,title,latestid,feedid,type,commentplace){
    type = typeof(type)=="undefined"?"":type;
    let jsonbody = {
      "menuTypeId" : agileid,
      "project_id" : prjid,
      "localOffsetTime" : localOffsetTime,		
      "feedType" : "agile",		
      "subMenuType" : "Story",
      "type" : type,
      "task_comment_id" : latestid
    }
    //$('#loadingBar').addClass('d-flex').removeClass('d-none');
    $.ajax({
      url: apiPath + "/" + myk + "/v1/getFeedDetails",
      type: "POST",
      dataType: 'json',
      contentType: "application/json",
      data: JSON.stringify(jsonbody),
      error: function (jqXHR, textStatus, errorThrown) {
        checkError(jqXHR, textStatus, errorThrown);
        //$('#loadingBar').addClass('d-none').removeClass('d-flex');
        
      },
      success: function (result) { 
        
        var UI=prepareCommentsUI(result, menuutype, prjid,'');
        feedid=parseInt(feedid);
        
        if(typeof(latestid)=="undefined" || latestid==""){
            //agileCommentsUI(agileid,title);
            popupCommentsUI(agileid,title,commentplace);
            AddSubComments(0,'',agileid);
            $('body').find('#popupCommentsDivbody').append(UI);
            $('#replyBlock_0').focus();
        }else{
            $("#replyBlock_"+feedid).val('');
            if(feedid==0){
                $(UI).insertAfter("#replyDivContainer_"+feedid).show();
            }else{
                $("#replyDivContainer_"+feedid).remove();
                $(UI).insertAfter('#actFeedDiv_' +feedid).show('slow');
            }
          
        }
        
        //$('#loadingBar').addClass('d-none').removeClass('d-flex');
      }
    });
}
  

function agileCommentsUI(agileid,title){
	var ui="";
	$('#glbCommentsDiv').html('');
    $("#transparentDiv").show();
    
    ui='<div class="agileComments" agileid='+agileid+'>'
        +'<div class="" style="">'
        +'<div class="modal-content pl-2 pr-1 wsScrollBar" style="overflow: auto;z-index:1000 !important;max-height: 550px;height: 550px;width:88%;margin-left: 120px;font-size:12px;">'
            +'<div class="modal-header pt-2 pb-0 px-0" style="border-bottom: 1px solid #b4adad;min-height:27px;background-color: #fff;position: sticky;top: 0px;letter-spacing: 2px;font-size: 14px;z-index: 20;">'
                //+'<button type="button" onclick="closeAgileComents();" class="close p-0 ideaCloseButton" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
                +'<img src="images/menus/close3.svg" onclick="closeAgileComents();" class="ml-auto" style="cursor:pointer;width:10px;height:10px;margin-top:1px;margin-right:3px;">'
            +'</div>'
            +'<div class="d-flex  align-items-center py-1 w-100" style="background-color: #003A5D;position: sticky;top: 27px;letter-spacing: 1px;font-size: 14px;z-index: 10;color:#fff;">'
            +'<div class="defaultExceedCls" style="width:99%;">'
                +'<img src="/images/menus/agile.svg" class="ml-1" style="width:17px;height:17px;">'
                +'<span class="pl-2" style="">Agile Comments - </span>'
                +'<span class="pl-1" style="width:88%;">'+title+'</span>'
            +'</div>'
            +'</div>'
            +'<div id="agilecommentsDivbody" class="modal-body py-0 px-1 justify-content-center" style="height: auto;">'
            +'</div>'
        +'</div>'
        +'</div>'
    +'</div>'

    $('#glbCommentsDiv').append(ui).show();
}

function closeAgileComents(){
    $('#glbCommentsDiv').html('').hide();
    $("#transparentDiv").hide();
}

var agileListui="";
function agileExpandAll(){
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    let jsonbody = {
        "user_id":userIdglb,
        "project_id":prjid,
        "company_id":companyIdglb,
        "sortVal":"",
        "searchTxt":"",
        "localOffsetTime":localOffsetTime,
        "action":"loadEpicStories"
    }  
    $.ajax({
        url: apiPath+"/"+myk+"/v1/getEpics",
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
        },
        success:function(result){
            agileListui = $('#agilelistUL_0').html();

            $('#agilelistUL_0').html(prepareAgileUI(result));
            $('#agilelistUL_0').prepend("<div class='d-flex align-items-center mr-3 rounded py-1 orgheader' style='background-color: #eeced4;'><img src='/images/agile/org_blue.svg' class='image-fluid mx-2' title='Organized' style='width:20px;height: 20px;'><span class='defaultExceedCls' style='font-size:12px;color:black;'>Organized</span></div>");

            $('#agileExpandCollapse').children('img').attr('src','images/idea/collapse_all2.svg');
            $('#agileExpandCollapse').children('span').text('Collapse All');
            $('#agileExpandCollapse').attr('onclick','closeAgileExpand();event.stopPropagation();');

            $('#organizedAgile').removeClass('w-25').addClass('w-75');
            $('#unorganizedAgile').removeClass('w-75').addClass('w-25');
            $("#optionsId").addClass("d-block");

            $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
    });        
}

function closeAgileExpand(){
    $('#agilelistUL_0').html(agileListui);
    $('#agileExpandCollapse').children('img').attr('src','images/idea/expand_all2.svg');
    $('#agileExpandCollapse').children('span').text('Expand All');
    $('#agileExpandCollapse').attr('onclick','agileExpandAll();event.stopPropagation();');
    $('#organizedAgile').removeClass('w-75').addClass('w-25');
    $('#unorganizedAgile').removeClass('w-25').addClass('w-75');
    $("#optionsId").removeClass("d-block");
    agileListui="";
}