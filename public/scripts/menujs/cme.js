// const res = require("express/lib/response");

function checkChatDomain() {
	var domain = getDomain();
	if (domain.indexOf("localhost:3000") != -1 || domain.indexOf("newtest.colabus.com") != -1 || domain.indexOf("testbeta.colabus.com") != -1 || domain.indexOf("dev.colabus.com") != -1) {
		chatDomain = "devchat";
	} else if (domain.indexOf("www.colabus.com") != -1 || domain.indexOf("colabus.com") != -1 || domain.indexOf("testcme.colabus.com") != -1 || domain.indexOf("cme.colabus.com") != -1) {
		chatDomain = "chat";
	} else {
		chatDomain = "devchat";
	}
	return chatDomain;
}

var roster_count = 0;
var cmeDomain = checkChatDomain();
var contactsCompany = "";
function statusCme() {
	$.ajax({
		//    url:  apiPath+"/"+myk+"/v1/loadCompanyContactsforChat?userId="+userIdglb+"&cmpId="+companyIdglb+"",
		url: apiPath + "/" + myk + "/v1/fetchParticipants?user_id=" + userIdglb + "&company_id=" + companyIdglb + "&contact_permission=1",
		type: "GET",
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
		},
		success: function (result) {
			contactsCompany = result;

			// if (result != "SESSION_TIMEOUT") {
			userName = result;
			
			if(result.length <= 1){
				$("#cmecontent1").append("<div style='text-align: center;font-size: 12px;'>No data Found</div>");
			}else{
				prepareCmeContacts(userName);
				cmeConnect();
				cmeMessages();
				cmeContacts();
			}
			// }
		}
	});
}




function prepareCmeContacts(data) {
	try {
		$("#cmecontent2").html('');
		var UI = "";
		var user_full_name = "";
		var user_image_type = "";
		var user_id = "";
		var user_image = "";
		var imguser_full_name = "";
		var email = "";
		for (i = 0; i < data.length; i++) {
			user_full_name = data[i].user_full_name;
			user_id = data[i].user_id;
			// user_image_type = data[i].user_image_type;
			user_image = data[i].user_image;
			email = data[i].user_email1;;
			//if (user_image_type == null || user_image_type == "" || user_image_type == "null") {
			//	imgName = '';
			//}
			//else {
				var d = new Date();
				imgName = lighttpdpath + "/userimages/" + user_image + "?" + d.getTime();
			//}
			UI += "<div id='userId_" + user_id + "' uid='" + user_id + "' email='" + email + "' pname='" + user_full_name + "' jid='' type='chat' imgType='" + user_image_type + "' class='media border border-left-0 border-right-0 p-0 roster-contact offline position-relative'  onmouseover='showContactmoreoptions(this);' onmouseout='hideContactmoreoptions(this);' onclick='getNewConvId(" + user_id + ", this);' style=''> " +
						"<img data-src='" + imgName + "' src='/images/profile/userImage.svg'  title='" + user_full_name + "' onerror='userImageOnErrorReplace(this);' class='lozad userimage mr-3 mt-2 ml-2 rounded-circle mb-2 cursor' style='width:45px;'>" +
						"<div class='uid_" + user_id + " user_box_status' style='top: 8px !important;'></div>" +
						"<div class='media-body'>" +
							"<h6 class='user_box_name pt-4 pb-2 pname defaultExceedCls cursor' value='" + user_full_name + "' style='font-family: OpenSansRegular;font-size: 15px;'>" + user_full_name + "</h6>" +
						"</div>" 
					// UI +='<div id="contactfloatoptionsID" class="actFeedOptionsDiv d-none mt-3 mr-2" style="border-radius:8px;">' 
					UI +='<div id="contactfloatoptionsID" class=" d-none mt-3 mr-2" style="">' 
							+'<div class="d-flex align-items-center">'
							+'<img src="/images/cme/callAudio_blue.svg" title="Audio Call" onclick="event.stopPropagation();startavCall('+user_id+', \'true\', \'audio\');" class="image-fluid cursor audiocall mr-2" style="height:26px;width:26px;">'
							+'<img src="/images/cme/callVideo_blue.svg" title="Video Call" onclick="event.stopPropagation();startavCall('+user_id+', \'true\', \'video\');" id="" class="image-fluid cursor videocall mr-2" style="height: 26px;width:26px;">'
							+'</div>'
						+'</div>'
					+"</div>"
		}
		$("#cmecontent2").append(UI);
		$("#userId_" + userIdglb).remove();
		
		observer.observe();//---- to lazy load user images
	} catch (e) {
		console.log("Chat exception:" + e);
	}
}

function showContactmoreoptions(obj) {
	$(obj).addClass('cmecontacthover');
	// if($(obj).hasClass('online')){
		$(obj).find('#contactfloatoptionsID').removeClass('d-none').addClass('d-flex');
	// }
}

function hideContactmoreoptions(obj) {
	$(obj).removeClass('cmecontacthover');
	$(obj).find('#contactfloatoptionsID').removeClass('d-flex').addClass('d-none');
}

//xmpp connection
function cmeConnect() {
	try {
		var conn = new Strophe.Connection("https://" + cmeDomain + ".colabus.com:7443/http-bind/");
		var c = userIdglb + "@" + cmeDomain + ".colabus.com";
		var status;
		conn.connect(c, "welcome", function (status) {
			if (status === Strophe.Status.CONNECTED) {
				console.log("connected....");
				$(document).trigger("connected");
				changeImgCme('online');
				sendDeliverStanza();
				// setTimeout(checkIdleState, 25000);

			} else if (status === Strophe.Status.DISCONNECTED) {  // If chat get disconnected It will agin call the cmeConnect method to Connect to server.
				console.log("Disconnected....");
				changeImgCme('offline');
				cmeConnect();
			} else if (status === Strophe.Status.AUTHFAIL) {
				alertFun('User is not registered with the chat server. ', 'warning');
			} else if (status === Strophe.Status.ERROR || status === Strophe.Status.CONNFAIL) {
				alertFun('Chat server connection failed. ', 'warning');
			}
		})
		Gab.connection = conn;
		serverConnection = conn;
	} catch (e) {
		console.log("Chat exception:" + e);
	}
}

function changeImgCme(type) {
	if (type == "online") {
		$("#chatIcon").removeAttr('src').attr('src', path + '/images/menus/chatgreen.svg');
	}
	else {
		$("#chatIcon").removeAttr('src').attr('src', path + '/images/menus/chatred.svg');
	}
}

var jsonDataConversation = '';
var message = "";
function cmeMessages() {
	var localOffsetTime = getTimeOffset(new Date());
	$("#loadingBar").show();
	timerControl("start");
	let jsonbody = {
		"user_id": userIdglb,
		"localTZ": localOffsetTime
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getChatDetails",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			jsonDataConversation = result;
			jsonData = result;
			if(result.length == 0){
				$("#cmecontent1").html("<div style='text-align: center;font-size: 12px;'>No Data Found</div>");
			}else{
				prepareUICmeConversation(jsonData);
			}
		}
	});
}

function prepareUICmeConversation(data) {
	
	try {
		let UI = "";
		var cid = '';
		var msgType = '';
		var gname = '';
		var sender_image_type = '';
		var msg = '';
		var sendTime1 = '';
		var sender = '';
		var imgName = '';
		var groupMessCount = '';
		var messageCount = '';
		var unreadMsg = '';
		var sendDate = '';
		$("div#cmecontent1").html('');
		for (i = 0; i < data.length; i++) {
			cid = data[i].cid;
			msgType = data[i].msgType;
			gname = data[i].gname;
			sender_image_type = data[i].sender_image_type;
			msg = data[i].msg;
			sendTime1 = data[i].sendTime1;
			unreadMsg = data[i].unreadMsg;
			sendDate = data[i].sendDate;
			sender = data[i].sender;
			if (msgType == "chat") {
				var d = new Date();
				imgName = lighttpdpath + "/userimages/" + sender + "." + sender_image_type + "?" + d.getTime();
			}
			else if (msgType == "groupchat") {
				var d = new Date();
				imgName = lighttpdpath + "/GroupChatImages/" + cid + "." + sender_image_type + "?" + d.getTime();
			}
			else {
				imgName = '';
			}
			groupMessCount = data[i].unreadMsg;
			if (groupMessCount > 99)
				messageCount = "99+";
			else
				messageCount = groupMessCount;

			if (msgType == "groupchat") {
				UI += "<div id='groupuser_" + cid + "' cid='" + cid + "' type='" + msgType + "' gname='" + gname + "' gchat='1' class='media border p-0 cursor position-relative' style='color: black;display: -webkit-box;' onclick='openNewCmeChatBox(" + cid + ", this, " + cid + "," + groupMessCount + ");'> "
			}
			else {
				UI += "<div id='user_" + sender + "' cid='" + cid + "' type='" + msgType + "' pname='" + gname + "' chat='1' class='media border p-0 cursor position-relative' style='color: black;display: -webkit-box;' onclick='getNewConvId(" + sender + ", this," + groupMessCount + ");'> "
			}
			UI += "<img data-src='" + imgName + "' src='/images/profile/userImage.svg' title='" + gname + "' onerror='userImageOnErrorReplace(this);' class='lozad mr-3 my-3 ml-2 rounded-circle' style='width:45px;height: 45px;'>"
			if (msgType != "groupchat") {
				UI += "<div id='changestatus' class='uid_" + sender + " user_box_status float-left' style='top: 16px !important;'></div>  ";
			}
			UI += "<div class='media-body ml-1'>" +
					"<div class='pt-1 user_box_name defaultExceedCls pname' style='font-family: OpenSansRegular;font-size: 13px;' value='" + gname + "'>" + gname + "</div>" +
					"<div class='font-italic pt-2' style='font-size: 11px;'>" + sendTime1 + ",  " + sendDate + "</div>" +
						"<div class='d-flex justify-content-between'>"
			if (msgType == "groupchat") {
				UI += "<div class='defaultExceedCls defaultWordBreak font-weight-normal py-1 align-top ' style='font-family: OpenSansRegular;font-size: 15px;'><span class='defaultExceedCls'>" + msg + "</span></div>"
			} else {
				UI += "<div id='conversationContactMsg_" + sender + "' class='defaultExceedCls defaultWordBreak font-weight-normal py-1 align-top ' style='font-family: OpenSansRegular;font-size: 15px;'><span class='defaultExceedCls'>" + msg + "</span></div>" +
						"<div id='conversationContactMsg1_" + sender + "' class='defaultExceedCls defaultWordBreak font-weight-normal py-1 align-top ' style='font-family: OpenSansRegular;font-size: 15px;display: none;'><span class='defaultExceedCls'>" + msg + "</span></div>"
			}
			if (groupMessCount == 0) {
				UI += "<div id='conversationMsgCount_" + sender + "' class='mr-2 ml-0 float-right badge chatnotification' style=''></div>";
			}
			else {
				UI += "<div id='conversationMsgCount_" + sender + "' class='mr-2 ml-0 float-right badge chatnotification' style=''>" + messageCount + "</div>";
			}
			UI +=		"</div>" +
					"</div>" +
				"</div>"
		}
		$("div#cmecontent1").append(UI);
		observer.observe();//---- to lazy load user images

		for (i = 0; i < data.length; i++) {
			sender = data[i].sender;
			if ($('.uid_' + sender).hasClass("user_box_status_online")) {
				$('.uid_' + sender).addClass("user_box_status_online");
			}
			else {
				$('.uid_' + sender).removeClass("user_box_status_online");
			}
		}
		// var element = document.getElementById("uid_"+sender);
		// element.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
	} catch (e) {
		console.log("Chat exception:" + e);
	}
	// sendDeliverStanza();
}

function cmeContacts() {
	let UI = "";
	UI = "<div class='d-flex align-items-center text-white pl-3 py-2' style='height: 49px'>" +
			"<div id='cont' class='mr-auto py-2' style='font-size: 13px;'>"+
				'<img src="/images/cme/cme_contacts.svg" class="image-fluid" title="Contacts" style="height:24px;cursor:pointer;">'+
				'<span class="ml-1">Contacts</span>'+
			"</div>" +
			'<input  id="searchText2" class="border rounded d-none" style="width:86%;font-weight: normal;font-size: 13px;padding: 3px 8px;"  placeholder="Search" onkeyup="searchContact()">'+
			"<img id='cont1' src='images/cme/plus.svg' title='Add Guest User' class=' mx-1 cursor' style='width: 22px;'> " +
			"<img id='cont2' src='images/cme/filter.svg' title='Search' class=' mx-1 cursor' onclick='hideContacts()'  style='width: 22px;'> " +
			"<img id='cont3' onclick='backMessages()' src='images/cme/leftarrow.svg' title='Messages' class='backChatMain mr-3 cursor' style='width: 22px;'> " +
			'<img id="backContacts" src="images/cme/leftarrow.svg" class=" cursor d-none ml-2" onclick="backContacts()" title="Back"  style="width: 22px;">'+
		"</div>";
		

	$("#cmecontact").html(UI);
	cmeGroupHeader();
}

function cmeGroupHeader() {
	let UI = "";
	UI = "<div class='d-flex align-items-center text-white pl-3' style='height: 49px;'>" +
			'<input  id="searchText3" class="border rounded d-none" style="width:86%;font-weight: normal;font-size: 13px;padding: 3px 8px;"  placeholder="Search" onkeyup="searchGroup()">'+
			"<div id='grp1' class='mr-auto' style='font-size: 13px;'>"+
				'<img src="/images/cme/cme_groups.svg" class="image-fluid" title="Groups" style="height:24px;cursor:pointer;">'+
				'<span class="ml-1">Groups</span>'+
			"</div>" +
			// "<div class='cursor' style='font-size: 13px;' title='Add Group'>ADD GROUP</div>" +
			"<div>"+
				"<img id='grp2' src='images/cme/plus.svg' onclick='addGrpUI()' title='Add Group' class=' mx-1 cursor' style='width: 22px;'> " +
				"<img id='grp3' onclick='searchingGroups()' src='images/cme/filter.svg' title='Search' class=' mx-1 cursor' style='width: 22px;'> " +
				"<img id='grp4' onclick='backMessages()' src='images/cme/leftarrow.svg' title='Messages' class='backChatMain mr-3 cursor' style='width: 22px;'> " +
				'<img id="backgroup" onclick="backGroup()" src="images/cme/leftarrow.svg" class=" cursor d-none ml-2" title="Back"  style="width: 22px;">'+
			"</div>" +
		"</div>"

	$("#cmegroup").html(UI);
	cmeCallHeader();
}

function cmeCallHeader() {
	let UI = "";
	UI = "<div class='d-flex align-items-center text-white pl-3' style='height: 49px;'>" +
			"<div id='' class='mr-auto' style='font-size: 13px;'>"+
				'<img src="/images/cme/cme_calls.svg" class="image-fluid" title="Calls" style="height:24px;cursor:pointer;">'+
				'<span class="ml-1">Calls</span>'+
			"</div>" +
			"<div>"+
				"<img id='' onclick='backMessages()' src='images/cme/leftarrow.svg' title='Messages' class=' mr-3 cursor' style='width: 22px;'> " +
			"</div>" +
		"</div>"

	$("#cmecall").html(UI);
	cmeCallScreen();
	
}

function openGrpFiles() {
	document.getElementById('uploadchatImage').click();
}

var Groupie = {
	connection : null,
	room :  	'mucr.devchat.colabus.com',
	nickname : '',
	//NS_MUC : "http://jabber.org/protocol/muc",
	joined : null,
	participants : null,
	
	/*
	 * @ method is called when group message comes
	 */
/*	   on_public_message : function(message){
	  var from = $(message).attr('from'); 
	  var room = Strophe.getBareJidFromJid(from);
	  var nick = Strophe.getResourceFromJid(from);
	  var Id = room.split('@')[0];
	  var id= Id+"m";
	  var d = new Date();	
	  var body = $(message).children('body').text();
	  console.log("Hi U got new group Message...first line of message");
	  console.log("Group message"+body);
	  var name = nick.split('_')[2];
	  var senderImgType = nick.split('_')[3];
	  var userid= nick.split('_')[1]; 
   
	  
	  var Name = $('#groupChat-box ul').find('li#'+Id+' .user_box_name').text();
		 
	  /*	if(!$('#chat-content').children('#'+Id+'m').is(':visible')){
			 var i = $('#'+Id).attr('count');
				 i = parseInt(i) + 1;   
					 $('#'+Id).attr('count',i);
					 $('#'+Id).find('.chatnotification').css('visibility','visible').html(i);
					 playSound('glass');
		 }*/
		 
		 
 /*		
		 if(!$('#'+id+' #chat-dialog').is(':visible')){
			showPersonalConnectionMessages();
					 playSound('glass');
		 }else{
		  resizeChatWindow();
		  //  getPreviousGroupMessages(Id);
		  //  updateSeenMessages(Id,userId);
		  
			 var t=getTimeIn12Format(d);
			 if(("user_"+userId+"_"+userFullName+"_"+ userImgType ) != nick){
					  
			 $('#'+id+' #chat-dialog').append(	
				 "<div class='chat-income-overall row' style='outline:none' tabindex='0'>"+
				 "<div style='width:100%;float:left'>"+ 
				 "<div class='chat-income-imageframe col-xs-1' style='margin-top: 23px;'>"+
				 "<img  src='"+lighttpdPath+"/userimages/"+userid+"."+senderImgType+"'  onerror='userImageOnErrorReplace(this);' class='incomeframeImage' style='width: 40px; height: 38px; border-radius: 20px;margin-left:-7vh'/></div>"+ 
				 "<div class='col-xs-10 chatIncomeBubble' style = 'padding-left: 0vh;margin-left:-1em' >"+
				 "<div class='row' align='left' style='font-family: helvetica; font-size: 13px; width: 105%; padding-left: 34px;'>"+name+"</div>"+
				 "<div class='chat-income-message bubble row'>"+body+
				 "</div></div>"+
				 "<div class='chat-income-time row' style='padding-left: 34px;'>"+t+"</div></div></div>");
					 
				 playSound('glass');
		  }
	 
		  var h = $('#'+id).find('#chat-dialog')[0].scrollHeight;
		  $('#'+id).find('#chat-dialog').scrollTop(h);
		 }
		  
		  $("#chat-dialog").mCustomScrollbar("update");
		  
		   // This method is from a plug-in ifvisible.min.js which is imported in chat.jsp for more details check chat_desktop_notification.js
	/*    	
		 if((!$('#'+id+' #chat-dialog').is(':visible'))||(ifvisible.now('hidden'))){
			   window.opener.notifyMe(lighttpdPath+"/userimages/"+userid+"."+senderImgType , body);
			}  */
	   // showPersonalConnectionMessages();             
		  
	/*       return true;
	   
	} */

	 // Not in use
 /*    on_presence : function(presence){
	  
	   var from = $(presence).attr('from');
	   var room = Strophe.getBareJidFromJid(from); 
	
 }*/
	
  };

function createGrpId() {
	var gid = '';
	let jsonbody = {
		"user_id": userIdglb,
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/createMucRoom",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			gid = result;
			createGrp(gid);
			// console.log(gid);
		}
	});
}

function addGrpUI(gid, data, type) {
	$("#transparentDiv").show();
	let ui = '';
	ui='<div class="SBactiveSprintlistCls" style="top:85px !important;" id="SBactiveSprintlistId" selectedstories="">'
			+'<div class="mx-auto w-50 modal-dialog11" style="max-width: 500px; min-height: 150px;height: calc(100% - 132px - 4px);max-height: 391px;">'
				+'<div id="addgrp1" class="modal-content11 container px-3" style="">'
					+'<div class="modal-header11 pt-3" style="border-bottom: 1px solid #b4adad !important;">' 
						+'<h5 id="updategroup" class="modal-title defaultExceedCls" style="color:black;font-size: 16px !important;">Create Group</h5>' 
						+"<button type=\"button\" class=\"close\" onclick=\"closeaddGroup();\" id=\"\" style=\"top: 5px;right: 10px;color: black;opacity: unset;padding: 0px;outline:none;\" data-dismiss=\"modal11\">×</button>"
					+'</div>' 
					+'<div class="modal-body11 wsScrollBar mb-2" style="height: calc(100% - 40px);">'
						+'<div class="material-textfield" >'
							+'<textarea id="newgrpname"  type="text" class=" mt-3 theightdemo material-textfield-input grpnametextbox"  onkeyup="checkvalid(this);event.stopPropagation();"  placeholder=" " style="width: 85%;"></textarea>'
							+'<label class=" mt-3 material-textfield-label" style="font-size: 12px;top:18px;">Group Name</label>'
							+'<img id="uploadGrpImg2" src="images/cme/groupimage.svg" class="mr-2 mt-2 cursor float-right rounded-circle" onclick="openGrpFiles()" onchange="readImgURL(this)" title="Group"  onerror="groupImageOnErrorReplace(this);" title="Group" style="width: 45px;height: 45px;">'
						+'</div>'
						// +'<div id="" class="pb-2" style="height: 77px;">'
						// 	+'<span style="font-size: 12px;font-weight: bold;color: #5e5b5b;">Group Image :</span>'
							+"<form action='' enctype='multipart/form-data' method='post' name='chatImgUploadForm' id='chatImgUploadForm'>"+					
								"<label class='topicImageUpload' style='margin-top:-3px;width:160px;height:20px;position:absolute;display:none;'>" +
								"<a style='color:#FFFFFF;margin-left:-5px;' href='#'></a>"	+	
								"<input type='file' title='upload image'  onchange='readImgURL(this);' class='topic_file' value='' name='uploadchatImage' id='uploadchatImage' hidden>" +			
								"<input type='hidden' value='' name='chatUploadId' id='chatUploadId'>"+
								"</label>"+
							"</form>"
							// +'<div class="mt-1">'
							// 	+'<img id="uploadGrpImg2" src="images/temp/chat_groupiconRoom.png" class="cursor border float-left rounded-circle" onchange="readImgURL(this)" title="Group"  onerror="groupImageOnErrorReplace(this);" title="Group" style="width: 45px;height: 45px;">'
							// 	+'<img id="uploadGrpImg" src="images/workspace/edit.svg" class="cursor" onclick="openGrpFiles()" title="Upload Group Image" style="width: 20px; margin-left: -10px;margin-top: -10px;">'
							// +'</div>' 

						// +'</div>'
						+'<div id="" class=" d-flex" style="">'
							+'<div id="" class="mt-1" style="">'
								+'<span style="font-size: 12px;font-weight: bold;color: #5e5b5b;">Group Participants:</span>'
							+'</div>'
							+'<div id="" class="mt-1 ml-2" style="">'
								+'<img id="addparticipantpopup" src="images/task/plus.svg" class="cursor mr-2" onclick="addParticipants(this)" title="Add Participants" style="width: 22px;">'
							+'</div>'
							+'<div id="inviteGrpUsers"></div>'
						+'</div>'
						+'<div id="groupUsers" class="mt-2 defaultScrollDiv " style="overflow-x: hidden;overflow-y: auto;height: 174px;border-top: 1px solid #b4adad;border-bottom: 1px solid #b4adad;">'
						+'</div>'
					+'</div>'
					+'<div id="wsSaveUpdateDiv" class="modal-footer11 pb-3 d-flex justify-content-end">'
						+"<div id=\"\" onclick=\"closeaddGroup();\" style=\" width: 70px !important;float: right;\" class=\"createBtn mt-0 mr-2\">Cancel</div>"
						+"<div id=\"editgrpbtn\" onclick=\"createGrpId();\" style=\" width: 70px !important;float: right;\" class=\" mt-0 createBtn SAVE_cLabelHtml\">Save</div>"
      				+'</div>'
				+'</div>'
	
			+'</div>'
		+'</div>'


	$('#addcmeGroup').html(ui).show();
	if (type == "edit") {
		let UI = '';
		var group_name = data[0].group_name;
		var group_owner = data[0].group_owner;
		var grpimage = data[0].imageUrl;
		var user_id = "";
		var user_name = "";
		var user_img = "";
		var groupRole = "";
		var gusers = data[0].groupUsers;
		for (i = 0; i < gusers.length; i++) {
			user_id = gusers[i].user_id;
			user_name = gusers[i].user_name;
			user_img = gusers[i].imageUrl;
			groupRole = gusers[i].groupRole;

			UI += "<div id='userr_"+user_id+"' updateStatus='insert' class='prjUserClsNew userStat_"+user_id+"' onmouseover='addBgcolor("+user_id+")' onmouseout='removeBgcolor("+user_id+")' userrole='' name='"+user_name+"' style='display: flex;justify-content: space-between;height:43px !important;width: 100%;border-bottom: 1px solid #cdc4c4;background-color: #fff;border-top-left-radius: 10px;border-top-right-radius: 10px;color: #464242;' >"
					+"<div class=\"d-flex pt-1\" style='text-overflow: ellipsis;overflow: hidden;white-space: nowrap;'><img id=\"personimagedisplay\"  src=\""+user_img+"\" title='"+user_name+"'  onerror='userImageOnErrorReplace(this);' class=\"mr-3 rounded-circle profImg\" style=\"\"><span class='profNam ml-1 pt-1' style='font-size: 12px;'>"+user_name+"</span></div>"
					+"<div class=\"pt-1\" id=\"roleSection\" style='display: flex;justify-content: space-between;'>"
						// +"<div class='dropDow' style=''><select name=\"role\" class='selectOp' id='role_"+user_id+"' onchange='' style='font-size: 12px;margin-top: 3px;color: #5e5b5b;'>"
						// 		// +"<option value=\"role\">role</option>"
						// 	+"<option id='ow_' value=\"PO\">Admin</option>"
						// 	+"<option id='tm_' value=\"TM\" selected>Team Member</option>"
						// 	// +"<option id='ob_' value=\"OB\">Observer</option>"
						// 	+"</select>"
						// +"</div>"
						+"<div class='closeOp mr-2' style='width: auto;'>"
						if(user_id == group_owner){
							UI +="<img onclick='' class='mr-4' src='/images/cme/admin.svg' title='Admin' style='width: 15px; height: 15px; cursor: pointer;'>"
						}
						else{
							if(groupRole == "admin"){
							UI +="<img onclick='markAsUser(this)' class='mr-2 adminoruser' src='/images/cme/admin.svg' title='Admin' style='width: 15px; height: 15px; cursor: pointer;'>"

							}
							else{
								UI +="<img onclick='markAsAdmin(this)' class='mr-2 adminoruser' src='/images/cme/user.svg' title='Mark as Admin' style='width: 15px; height: 15px; cursor: pointer;'>"
							}
							UI +="<img onclick='deleteGrpUser("+user_id+")' class='' src='/images/task/minus.svg' title='remove' style='width: 15px; height: 15px; cursor: pointer;'>"
						}
							
					UI +="</div>"
					+"</div>"
				+"</div>";
		}

		// $('#userforProj').hide();
		$('#groupUsers').append(UI);


		$('#newgrpname').val(group_name);
		$('#uploadGrpImg2').attr('src', grpimage);
		$('#editgrpbtn').text("UPDATE");
		$('#updategroup').text("Update Group");
		$('#editgrpbtn').attr('onclick', 'updateGrp(' + gid + ')');


	}
	clearGrpAllValues();
}

function markAsAdmin(obj) {
	$(obj).attr('src', '/images/cme/admin.svg');
	$(obj).attr('title', 'Admin');
	$(obj).attr('onclick', 'markAsUser(this)');
}

function markAsUser(obj) {
	$(obj).attr('src', '/images/cme/user.svg');
	$(obj).attr('title', 'Mark as Admin');
	$(obj).attr('onclick', 'markAsAdmin(this)');
}

var delgrpusers = '';
function deleteGrpUser(id) {
	$('#groupUsers').children('#userr_' + id).remove();
	$('#userforGrp').children('#' + id).show();
	delgrpusers += id + ",";
}

function updateGrp(gid) {
	var data = uniquegrp;
	// console.log(data);
	var gname = $('#newgrpname').val();
	var grpImg = $('#uploadGrpImg2').attr('src');
	if (data[0].group_name != gname) {
		let jsonbody = {
			"ownerId": userIdglb,
			"mucId": gid,
			"mucName": gname
		}
		// console.log("group");
		$.ajax({
			url: apiPath + "/" + myk + "/v1/insertMUCRoomName",
			type: "POST",
			dataType: 'text',
			contentType: "application/json",
			// contentType: "text/plain",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				timerControl("");
			},
			success: function (result) {
				var group = result;
				// console.log(result);


				// if(result == "updated"){

				// }
			}


		});
	}
	if (grpimgchange == '1') {
		var filename = $('#uploadGrpImg2').attr('src');
		console.log("filename--" + filename);
		var formData = new FormData();
		formData.append('file', filegrp);
		formData.append('place', 'groupUploadProf');
		formData.append('resourceId', gid);
		formData.append('subMenuType', 'system');

		$.ajax({
			url: apiPath + "/" + myk + "/v1/upload",
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			// data: formData,
			dataType: 'text',
			data: formData,
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				// $('#loadingBar').addClass('d-none').removeClass('d-flex');
				// console.log(errorThrown);
			},
			success: function (result) {
				// console.log(result);
				// listOfConnectedRoom();
			}
		});

	}
	var roles = '';
	var users = '';
	var deluser = '';
	var id = '';
	var src = '';
	var userid = '';
	$('#groupUsers > div').each(function () {
		id = $(this).attr('id').split('_')[1];
		src = $('#userr_' + id).find('.adminoruser').attr('src');
		if (src == "/images/cme/admin.svg") {
			roles += "admin,";
		}
		else {
			roles += "user,";
		}
		userid += id + ",";
	});
	var count = userid.length;
	var count1 = roles.length;
	var users = userid.substring(0, count - 1);
	var role = roles.substring(0, count1 - 1);
	// console.log("users--" + users + "role--" + role);

	// var count = newgrpuser.length ;
	// var role1 = count.split(',')[0];
	// var role2 = count.split(',')[1];
	// // var count1 = newgrpuserrole.length ;

	// users = newgrpuser.substring(0, count - 1);
	// // role = newgrpuserrole.substring(0, count1 - 1);


	var count2 = delgrpusers.length;
	deluser = delgrpusers.substring(0, count2 - 1);


	// console.log("users--" + users + "role--" + role);

	let jsonbody = {
		"mucId": gid,
		"mucName": gname,
		"user_id": userIdglb,
		"selMembers": users,
		"delMembers": deluser,
		"roleForGroup": role
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/updateMUCRoomUsers",
		type: "PUT",
		dataType: 'text',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			// console.log(result);
			listOfConnectedRoom();
			clearGrpAllValues();
		}

	});

	// }




	$("#transparentDiv").show();
	closeaddGroup();
	// listOfConnectedRoom();
}

function clearGrpAllValues() {
	newgrpuser = '';
	newgrpuserrole = '';
	delgrpusers = '';
	grpimgchange = '';
}

function createGrp(gid) {
	var gname = $('#newgrpname').val();
	// console.log("gid--" + gid + "gname--" + gname);
	let jsonbody = {
		"ownerId": userIdglb,
		"mucId": gid,
		"mucName": gname
	}
	// console.log("group");
	$.ajax({
		url: apiPath + "/" + myk + "/v1/insertMUCRoomName",
		type: "POST",
		dataType: 'text',
		contentType: "application/json",
		// contentType: "text/plain",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			var group = result;
			// console.log(result);
			var userid = '';
			var roles = '';
			var id = '';
			var src = '';
			var src1 = '';
			var src2 = '';

			if (result == "updated") {
					//Groupie.connection.send($pres().c('priority').t('-1'));
					Gab.connection.send(
						$pres({
							to: gid + '@mucr.' + cmeDomain + '.colabus.com' + "/" + "user_" + userIdglb + "_" + userFullname + "_" + userImgType
						}).c('x', { xmlns: Gab.NS_MUC }));
	
					iq = $iq({
						to: gid + '@mucr.' + cmeDomain + '.colabus.com',
						type: 'set'
					}).c("query", {
						xmlns: Gab.NS_MUC + "#owner"
					});
					iq.c("x", {
						xmlns: "jabber:x:data",
						type: "submit"
					});
					var ownereId = userIdglb + '@mucr.' + cmeDomain + '.colabus.com'
					//send configuration you want
					iq.c('field', { 'var': 'FORM_TYPE' }).c('value').t('http://jabber.org/protocol/muc#roomconfig').up().up();
					iq.c('field', { 'var': 'muc#roomconfig_roomname' }).c('value').t(gname).up().up();
					iq.c('field', { 'var': 'muc#roomconfig_roomdesc' }).c('value').t(gname).up().up();
					iq.c('field', { 'var': 'muc#roomconfig_enablelogging' }).c('value').t('0').up().up();
					iq.c('field', { 'var': 'muc#roomconfig_changesubject' }).c('value').t('0').up().up();
					iq.c('field', { 'var': 'muc#roomconfig_maxusers' }).c('value').t(0).up().up();
					iq.c('field', { 'var': 'muc#roomconfig_presencebroadcast' }).c('value').t('moderator').c('value').t('participant').up().up();
					iq.c('field', { 'var': 'muc#roomconfig_publicroom' }).c('value').t('1').up().up();
					iq.c('field', { 'var': 'muc#roomconfig_persistentroom' }).c('value').t('1').up().up();
					iq.c('field', { 'var': 'muc#roomconfig_whois' }).c('value').t('anyone').up().up();
					iq.c('field', { 'var': 'muc#roomconfig_roomowners' }).c('value').t(ownereId).up().up();
	
					Gab.connection.sendIQ(iq.tree(), function () { console.log('success'); }, function (err) { console.log('error', err); });


				if (grpimgchange == '1') {
					var filename = $('#uploadGrpImg2').attr('src');
					// console.log("filename--" + filename);
					var formData = new FormData();
					formData.append('file', filegrp);
					formData.append('place', 'groupUploadProf');
					formData.append('resourceId', gid);
					formData.append('subMenuType', 'system');

					$.ajax({
						url: apiPath + "/" + myk + "/v1/upload",
						type: 'POST',
						processData: false,
						contentType: false,
						cache: false,
						// data: formData,
						dataType: 'text',
						data: formData,
						error: function (jqXHR, textStatus, errorThrown) {
							checkError(jqXHR, textStatus, errorThrown);
							// $('#loadingBar').addClass('d-none').removeClass('d-flex');
							console.log(errorThrown);
						},
						success: function (result) {

						}
					});
				}
				// console.log("in updated");
				$('#groupUsers > div').each(function () {
					// if($(this).attr('id')!='ignorediv'){
					id = $(this).attr('id').split('_')[1];
					// var role = $(this).find('#commentApprove_'+id).val();
					// console.log("role-->>"+role);
					// if(userArray1.includes(id)==false){
					// userArray1.push(id);
					src = $('#userr_' + id).find('.adminoruser').attr('src');
					if (src == "/images/cme/admin.svg") {
						roles += "admin,";
					}
					else {
						roles += "user,";
					}
					userid += id + ",";



					// }
					// }

				});
				console.log(id);
				var count = userid.length;
				var count1 = roles.length;
				var users = userid.substring(0, count - 1);
				var role = roles.substring(0, count1 - 1);
				// console.log("users--" + users + "role--" + role);

				let jsonbody = {
					"mucId": gid,
					"mucName": gname,
					"user_id": userIdglb,
					"selMembers": users,
					"delMembers": "",
					"roleForGroup": role
				}
				$.ajax({
					url: apiPath + "/" + myk + "/v1/updateMUCRoomUsers",
					type: "PUT",
					dataType: 'text',
					contentType: "application/json",
					data: JSON.stringify(jsonbody),
					error: function (jqXHR, textStatus, errorThrown) {
						checkError(jqXHR, textStatus, errorThrown);
						timerControl("");
					},
					success: function (result) {
						// console.log(result);
						listOfConnectedRoom();
						clearGrpAllValues();
					}

				});
			}
			// console.log("group8");
		}
	});


	$("#transparentDiv").show();
	closeaddGroup();

}

function openImgFiles() {
	document.getElementById('uploadGrpImg2').click();
}

var grpimgchange = '';
var filegrp = '';
function readImgURL(input) {
	filegrp = input.files[0];
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#uploadGrpImg2').attr('src', e.target.result);
			grpimgchange = '1';
		};
		projectImgUploadFromSysFlag = true;
		reader.readAsDataURL(input.files[0]);
	}

}

function addParticipants() {
	$('#inviteCmeGrppopU').remove();
	var Ui = "";

	Ui="<div class=\"position-absolute border border-secondary p-3 ml-2 rounded\" id=\"inviteCmeGrppopU\" style=\"z-index: 100;width: 290px;height: 235px;left: 136px;bottom: 50px;background-color: white;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;\">" //<input type='text' style='border-radius: 0px;border-bottom:0px;' id=\"inviteUsers\"  placeholder=\"Invite Members\" class=\"projInviteUser form-control validate\">
		+'<div class="d-flex justify-content-between" style="">'
			+"<div id='invcmeUsers' class='pb-1 invcmeUsers'  style='font-size: 14px;font-weight: normal;'>Users</div>"
				+'<div class="serachimage d-flex" onclick="openGrpUserSearch()">'
					+'<input type="text" id="inputGrpUser" onkeyup="searchAllGrpUsers()" placeholder=\"Invite Members\" class="projInviteUser border-0" style="display:none;width: 240px;border-bottom:1px solid #6c757d !important;">'
					+'<img id="gusersearch" onclick="openGrpUserSearch();event.stopPropagation();" class="searchImages" src="images/menus/search2.svg" style="height:20px;width:20px; cursor:pointer;"/>'
				+'</div>'
		+'</div>'
		+"<div id=\"userforGrp\" class=\"projUserListMainnewWS border-0 wsScrollBar\" style=\"display: block;overflow-y: auto;overflow-x: hidden;\">"
    	+"</div>"
	+"</div>"

	$('#inviteGrpUsers').append(Ui);
	fetchAllParticipants();
}

function openGrpUserSearch() {
	$('#invcmeUsers').hide();
	$('#inputGrpUser').show().focus();
}
function searchAllGrpUsers() {
	var txt = $('#inputGrpUser').val().toLowerCase();

	$('#userforGrp').children().each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding

		var val = $(this).find('.user_box_name').attr('value').toLowerCase();

		if (val.indexOf(txt) != -1) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}

function fetchAllParticipants() {
	userName = contactsCompany;
	var name = '';
	var UI = '';
	var id = '';
	var email = '';
	var d = new Date();

	for (var i = 0; i < userName.length; i++) {
		name = userName[i].user_full_name;
		id = userName[i].user_id;
		email = userName[i].user_email1;
		let imgName = lighttpdpath + "/userimages/" + userName[i].user_image + "?" + d.getTime();

		if (userIdglb != id) {
			UI += "<div id='fetch_" + id + "' uid='" + id + "' pname='" + name + "' jid='' type='chat' chat='2' class='media listofparticipants border border-left-0 border-right-0 p-0 roster-contact offline position-relative' onclick=\"addGrpusers(this,'" + userName[i].user_image + "','" + email + "');removeGrpUserFromList(this," + id + ");\"> " +
					"<img src='" + imgName + "' title='" + name + "' onerror='userImageOnErrorReplace(this);' class='userimage mr-3 mt-2 ml-2 rounded-circle mb-2 cursor' style='width:45px;'>" +
					"<div class='media-body'>" +
						"<h6 class='user_box_name pt-4 pb-2 pname defaultExceedCls cursor' value='" + name + "' style='font-family: OpenSansRegular;font-size: 15px;'>" + name + "</h6>" +
					"</div>" +
				"</div>"
		}
		// console.log("contacts");
	}
	$('#userforGrp').html(UI);
	handlingUsers();
}

function handlingUsers() {
	var id = '';
	$('#groupUsers > div').each(function () {
		id = $(this).attr('id').split('_')[1];
		$('#fetch_' + id).remove();
	});
}

function removeGrpUserFromList(obj, selectedId) {
	$('#fetch_' + selectedId).remove();
}

var newgrpuser = '';
var newgrpuserrole = '';
function addGrpusers(obj, imgName, email) {

	var id = $(obj).attr('uid');
	var name = $(obj).attr('pname');
	var d = new Date();
	let img = lighttpdpath + "/userimages/" + imgName + "?" + d.getTime();

	var UI  = "<div id='userr_"+id+"' updateStatus='insert' class='prjUserClsNew userStat_"+id+"' onmouseover='addBgcolor("+id+")' onmouseout='removeBgcolor("+id+")' userrole='' email='"+email+"' name='"+name+"' style='display: flex;justify-content: space-between;height:43px !important;width: 100%;border-bottom: 1px solid #cdc4c4;background-color: #fff;border-top-left-radius: 10px;border-top-right-radius: 10px;color: #464242;' >"
				+"<div class=\"d-flex pt-1\" style='text-overflow: ellipsis;overflow: hidden;white-space: nowrap;'><img id=\"personimagedisplay\"  src=\""+img+"\" title='"+name+"' onerror='userImageOnErrorReplace(this);' class=\"mr-3 rounded-circle profImg\" style=\"\"><span class='profNam ml-1 pt-1' style='font-size: 12px;'>"+name+"</span></div>"
				+"<div class=\"pt-1\" id=\"roleSection\" style='display: flex;justify-content: space-between;'>"
						// +"<div class='dropDow' style=''><select name=\"role\" class='selectOp' id='role_"+id+"' onchange='' style='font-size: 12px;margin-top: 3px;color: #5e5b5b;'>"
						// 		// +"<option value=\"role\">role</option>"
						// 	+"<option id='ow_' value=\"PO\">Admin</option>"
						// 	+"<option id='tm_' value=\"TM\" selected>Team Member</option>"
						// 	// +"<option id='ob_' value=\"OB\">Observer</option>"
						// 	+"</select>"
						// +"</div>"
						+"<div class='closeOp mr-2' style='width: auto;'>"
							+"<img onclick='markAsAdmin(this)' class='mr-2 adminoruser' src='/images/cme/user.svg' title='Mark as Admin' style='width: 15px; height: 15px; cursor: pointer;'>"
							+"<img onclick='removeGrpUser("+id+")' class='' src='/images/task/minus.svg' title='remove' style='width: 15px; height: 15px; cursor: pointer;'>"
						+"</div>"
				+"</div>"
			+"</div>";

	$('#groupUsers').append(UI);
	$('#inviteGrpUsers').val('');
	// newgrpuser += id + ",";
	// newgrpuserrole += "user,";
}

function removeGrpUser(id) {
	$('#groupUsers').children('#userr_' + id).remove();
	$('#userforGrp').children('#' + id).show();
}


function backAddParticipant() {
	$('#addgrp1').removeClass('d-none');
	$('#addgrp2').addClass('d-none');
}

function closeaddGroup() {
	groups();
	$("#addcmeGroup").hide();
	$("#transparentDiv").hide();
}

function loadMessages() {
	if ($("#cmecontent").is(':hidden')) {
		$("#cmecontent").removeClass("d-none");
		if($('#cmecontent1').children().length < 1){
			cmeMessages();
		}
	} else {
		$("#cmecontent").addClass("d-none");
	}
}

function contacts() {
	$("#cmemessage").addClass("d-none");
	$("#cmecontent1").addClass("d-none");
	$("#cmecontact").removeClass("d-none");
	$("#cmecontent2").removeClass("d-none");
	$("#cmegroup").addClass("d-none");
	$("#cmecontent3").addClass("d-none");
}

function groups() {
	$("#cmemessage").addClass("d-none");
	$("#cmecontent1").addClass("d-none");
	$("#cmecontact").addClass("d-none");
	$("#cmecontent2").addClass("d-none");
	$("#cmegroup").removeClass("d-none");
	$("#cmecontent3").removeClass("d-none");
	$("#cmecontactconv-mob").addClass("d-none");
	$("#cmecontentconv-mob").addClass("d-none");
	// $("#cmecontentconv").addClass("d-none");
	// $("#cmecontactconv").addClass("d-none");
}

function cmeAVScreen(){
	$("#cmemessage").addClass("d-none");
	$("#cmecontent1").addClass("d-none");
	$("#cmecontact").addClass("d-none");
	$("#cmecontent2").addClass("d-none");
	$("#cmegroup").addClass("d-none");
	$("#cmecontent3").addClass("d-none");
	$("#cmecontactconv-mob").addClass("d-none");
	$("#cmecontentconv-mob").addClass("d-none");
	// $("#cmecontentconv").addClass("d-none");
	// $("#cmecontactconv").addClass("d-none");
	$('#cmecallcontact').show();
}



function backMessages() {
	$("#cmecontact").addClass("d-none");
	$("#cmecontent2").addClass("d-none");
	$("#cmecontent1").removeClass("d-none");
	$("#cmemessage").removeClass("d-none");
	$("#cmegroup").addClass("d-none");
	$("#cmecontent3").addClass("d-none");
	$("#cmecall").addClass("d-none");
	$("#cmecontent4").addClass("d-none");
	// $("#cmecontactconv").addClass("d-none");
	// $("#cmecontentconv").addClass("d-none");
	$("#cmecontactconv-mob").addClass("d-none");
	$("#cmecontentconv-mob").addClass("d-none");
	$("#cmecallcontact").hide();
}

function backToCall(){
	$("#cmecontact").addClass("d-none");
	$("#cmecontent2").addClass("d-none");
	$("#cmecontent1").addClass("d-none");
	$("#cmemessage").addClass("d-none");
	$("#cmegroup").addClass("d-none");
	$("#cmecontent3").addClass("d-none");
	$("#cmecall").removeClass("d-none");
	$("#cmecontent4").removeClass("d-none");
	// $("#cmecontactconv").addClass("d-none");
	// $("#cmecontentconv").addClass("d-none");
	$("#cmecontactconv-mob").addClass("d-none");
	$("#cmecontentconv-mob").addClass("d-none");
	$("#cmecallcontact").hide();
}

function showGrpmoreoptions(obj) {
	$(obj).find('#groupfloatoptionsID').removeClass('d-none').addClass('d-flex');
}

function hideGrpmoreoptions(obj) {
	$(obj).find('#groupfloatoptionsID').removeClass('d-flex').addClass('d-none');
}

function cmeGroups(data) {
	$("#cmecontent3").html('');
	let UI = '';
	var group_name = "";
	var group_id = "";
	var imageUrl = "";
	var group_owner = "";
	for (i = 0; i < data.length; i++) {
		group_name = data[i].group_name;
		group_id = data[i].group_id;
		imageUrl = data[i].imageUrl;
		group_owner = data[i].group_owner;


		UI +="<div id='group_" + group_id + "' cid='"+group_id+"' gname='"+group_name+"' jid='' type='groupchat' onmouseover='showGrpmoreoptions(this);event.stopPropagation();' onmouseout='hideGrpmoreoptions(this);event.stopPropagation();' gchat='2' class='media border border-left-0 border-right-0 p-0 offline position-relative cursor' onclick='openNewCmeChatBox(" + group_id + ", this," + group_id + ");'> " +
				"<img data-src='" + imageUrl + "' src='/images/profile/userImage.svg' title='" + group_name + "'  onerror='userImageOnErrorReplace(this);' class='lozad userimage mr-3 mt-2 ml-2 rounded-circle mb-2 cursor' style='width:45px;height: 45px'>" +
				"<div class='media-body'>" +
					"<h6 class='user_box_name pt-4 pb-2 pname defaultExceedCls cursor' value='" + group_name + "' style='font-family: OpenSansRegular;font-size: 15px;'>" + group_name + "</h6>" +
				"</div>" 
				if(group_owner == userIdglb){
					UI +='<div id="groupfloatoptionsID" class="actFeedOptionsDiv d-none mt-3 mr-2" style="border-radius:8px;">' 
							+'<div class="d-flex align-items-center">'
							+"<img src=\"/images/conversation/edit.svg\" title=\"Edit\" onclick=\"editCmeGroup("+group_id+");event.stopPropagation();\" id='' class=\"image-fluid\" style=\"margin: 0px 6px;height:20px;width:20px;\">"
							+'<img src="/images/conversation/delete.svg" title="Delete" id="" class="image-fluid" style="margin: 0px 6px;height: 15px;width:15px;" onclick="deleteGroupconfirm('+group_id+');event.stopPropagation();">'
							+'</div>'
						+'</div>'
					
				}
			UI +="</div>"
	}
	$("#cmecontent3").append(UI);
	observer.observe();//---- to lazy load user images
}



var uniquegrp = '';
function editCmeGroup(gid) {
	// groups();
	let jsonbody = {
		"user_id": userIdglb,
		"g_id": gid
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getMUCRoomList",
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			// console.log(result);
			var data = result;
			uniquegrp = result;
			addGrpUI(gid, data, "edit");
		}
	});
}

function deleteGroupconfirm(gid) {
	// confirmFunNew("Do you want to delete?", "delete", "deleteCmeGroup", gid);conFunNew
	conFunNew11(getValues(companyAlerts,"Alert_delete"),'warning','deleteCmeGroup','', gid);
}

function deleteCmeGroup(gid) {
	let jsonbody = {
		"g_id": gid
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/deleteMucRoom",
		type: "DELETE",
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			// console.log(result);
			/* <iq type="set" id="4D0C27DD-D597-4D2B-BAA9-31738E5C55C9" from="497@devchat.colabus.com" to="162@mucr.devchat.colabus.com">
					 <query xmlns="http://jabber.org/protocol/muc#owner"><destroy><reason/></destroy></query>
					 </iq>*/

					 var deleteIq = $iq({
						to: gid + '@mucr.' + cmeDomain + '.colabus.com',
						type: 'set'
					}).c("query", {
						xmlns: Gab.NS_MUC + "#owner"
					}).c("destroy").c("reason");

					Gab.connection.sendIQ(deleteIq, function () { console.log('success--deleted'); }, function (err) { console.log('error--deletion', err); });

			listOfConnectedRoom();
		}
	});
}


function searchMessages() {
	var txt = $('#searchText1').val().toLowerCase();

	$('#cmecontent1').children().each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding
		var val = $(this).find('.user_box_name').attr('value').toLowerCase();
		if (val.indexOf(txt) != -1) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}

function searchContact() {
	var txt = $('#searchText2').val().toLowerCase();

	$('#cmecontent2').children().each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding
		var val = $(this).find('.user_box_name').attr('value').toLowerCase();
		if (val.indexOf(txt) != -1) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}

function searchGroup() {
	var txt = $('#searchText3').val().toLowerCase();

	$('#cmecontent3').children().each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding
		var val = $(this).find('.user_box_name').attr('value').toLowerCase();
		if (val.indexOf(txt) != -1) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}

function searchingMessages() {
	$('#cme1').addClass('d-none');
	$('#cme2').addClass('d-none');
	$('#cme3').addClass('d-none');
	$('#cme4').addClass('d-none');
	$('#searchMessage').addClass('d-none');
	$('#backMessage').removeClass('d-none');
	$('#searchText1').removeClass('d-none');
	$('#searchText1').focus();
	$('#searchMessage').parent().find('img.cmeExpandCollapse').hide();
}

function backMessage() {
	$("#searchText1").val("").trigger("keyup");
	$('#cme1').removeClass('d-none');
	$('#cme2').removeClass('d-none');
	$('#cme3').removeClass('d-none');
	$('#cme4').removeClass('d-none');
	$('#searchMessage').removeClass('d-none');
	$('#backMessage').addClass('d-none');
	$('#searchText1').addClass('d-none');
	if($('#cmecontent').hasClass('cmecontainer-mob')){
		$('#searchMessage').parent().find('img.cmeExpandCollapse').show();
	}
}

function hideContacts() {
	$('#cont').addClass('d-none');
	$('#cont1').addClass('d-none');
	$('#cont2').addClass('d-none');
	$('#cont3').addClass('d-none');
	$('#searchText2').removeClass('d-none');
	$('#backContacts').removeClass('d-none');
	$('#searchText2').focus();
}

function backContacts() {
	$("#searchText2").val("").trigger("keyup");
	$('#cont').removeClass('d-none');
	$('#cont1').removeClass('d-none');
	$('#cont2').removeClass('d-none');
	$('#cont3').removeClass('d-none');
	$('#searchText2').addClass('d-none');
	$('#backContacts').addClass('d-none');
}

function searchingGroups() {
	$('#grp1').addClass('d-none');
	$('#grp2').addClass('d-none');
	$('#grp3').addClass('d-none');
	$('#grp4').addClass('d-none');
	$('#searchText3').removeClass('d-none');
	$('#backgroup').removeClass('d-none');
	$('#searchText3').focus();
}

function backGroup() {
	$("#searchText3").val("").trigger("keyup");
	$('#grp1').removeClass('d-none');
	$('#grp2').removeClass('d-none');
	$('#grp3').removeClass('d-none');
	$('#grp4').removeClass('d-none');
	$('#searchText3').addClass('d-none');
	$('#backgroup').addClass('d-none');
}

function checkIdleState(){
	// console.log("check idle");
	// alert("check idle");
	var idlePresence = $pres().c('show').t('away');
	serverConnection.send(idlePresence);
	setTimeout(checkIdleState2, 15000);
}

function checkIdleState2(){
	// console.log("check idle2");
	var idlePresence = $pres().c('show').t('');
	serverConnection.send(idlePresence);
}

$(document).on('connected', function () {
	var iq = $iq({ type: 'get' }).c('query', { xmlns: 'jabber:iq:roster' }); // preparing the roster request Information Query to send to server
	Gab.connection.sendIQ(iq, Gab.on_roster); // Sending the roster request.
	Gab.connection.addHandler(Gab.on_roster_changed, "jabber:iq:roster", "iq", "set");  // This function will get called when a person will go online or offline.
	Gab.connection.addHandler(Gab.on_message, null, "message", "chat"); // This function Will get called when any message will come.	  
	Gab.connection.addHandler(Gab.on_public_message, null, "message", "groupchat");
	Gab.connection.addHandler(Gab.on_normal_message, null, "message", "normal");
});

function disconnect() {
	console.log("Disconnected...")
	try {
		var offlinePresence = $pres().c('show').t('offline');
		serverConnection.send(offlinePresence);
		// 	globalChatConnection.connection.disconnect();
		// globalChatConnection.connection = null;
		Gab.connection.disconnect();
		Gab.connection = null;
		w.close();
	} catch (e) {
		console.log('Null value handled!');
	}
}

function disconnectFromMain() {  //-------------- this function is called from postlogin.jsp when chat window is closed, to update user status to offline and to close the connection.
	console.log("Disconnected...")

	globalChatConnection.connection.disconnect();
	globalChatConnection.connection = null;
}

function getTimeIn12Format(d) {
	var hours = d.getHours();
	var m = d.getMinutes();
	var suffix = hours >= 12 ? "PM" : "AM";
	m = m < 10 ? '0' + m : m;

	hours = ((hours + 11) % 12 + 1);
	hours = hours < 10 ? '0' + hours : hours;
	return (hours + ':' + m + " " + suffix);
}

var connectedrooms = '';
function listOfConnectedRoom() {
	// console.log("userFullname--"+userFullname+"userFullName--"+ userFullName);
	let jsonbody = {
		"user_id": userIdglb,
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getMUCRoomList",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			jsonData = result;
			connectedrooms = result;
			if(result.length == 0){
				$("#cmecontent3").html("<div style='text-align: center;font-size: 12px;'>No data Found</div>");
			}else{
				sendGrpPresence(jsonData);
				cmeGroups(jsonData);
			}
		}
	});

}

function sendGrpPresence(jsonData) {
	var groupId = '';
	if (jsonData.length > 0) {
		for (i = 0; i < jsonData.length; i++) {
			groupId = jsonData[i].group_id;
			Gab.connection.send(
				$pres({
					to: groupId + '@mucr.' + cmeDomain + '.colabus.com' + "/" + "user_" + userIdglb + "_" + userFullname + "_" + userImgType
				}).c('x', { xmlns: "http://jabber.org/protocol/muc" }));
			Gab.connection.send($pres().c('status').t('available'));
		}
	}
}

function groupImageOnErrorReplace(obj) {
	Initial(obj);
}

function Initial(obj) {
	$(obj).initial({
		name: $(obj).attr("title"),
		height: 64,
		width: 64,
		charCount: 2,
		textColor: "#fff",
		fontSize: 30,
		fontWeight: 700
	});
}

Array.prototype.getUserOnlineStatus = function (el) {
	var arr = this;
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].userId == el) {
			if (arr[i].status == "online" || arr[i].status == "dnd") {
				return true;
			}
		}
	}
	return false;
}

Array.prototype.getIndexOfResourceId = function (el) {
	var arr = this;
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].resourceId == el) {
			return i;
		}
	}
	return -1;
}

Array.prototype.getUserStatus = function (el) {
	var arr = this;
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].userId == el) {
			if (arr[i].status == "dnd") {
				return true;
			}
		}
	}
	return false;
}

var Gab = {  // TO understand this Core part of chat Go through the book. 
	connection: null,
	room: 'conference.' + cmeDomain + '.colabus.com',
	room: 'mucr.' + cmeDomain + '.colabus.com',
	jid_to_id: function (jid) {
		return Strophe.getBareJidFromJid(jid).replace("@", "-").replace(".", "-").replace(".", "-");
	},

	on_roster: function (iq) {
		try {
			// console.log("inside roaster function");
			var userName = contactsCompany;
			if (roster_count == 0) {
				roster_count++;
				for (i = 0; i < userName.length; i++) {
					var name = "";
					var imgType = "";
					var useRegType = "";
					var upgradePlan = "";
					var useRegType1 = "";
					var flag = false;
					var duser = "";
					var user_status = "";
					var imgurl = "";
					var role_id = "";
					var created_by = "";
					var guestUserClass = "";
					var user_login_name = "";
					$(iq).find('item').each(function () {
						try {
							var jid = $(this).attr('jid');
							var id = jid.split('@')[0];
							if (id == userName[i].user_id) {
								name = userName[i].name;
								imgType = userName[i].imgType;
								useRegType = userName[i].useRegType;
								upgradePlan = userName[i].upgradePlan;
								role_id = userName[i].role_id;
								created_by = userName[i].created_by;
								user_login_name = userName[i].user_login_name;
								if (imgType == "" || imgType == "null" || imgType == null) {
									imgurl = "";
								} else {
									var d = new Date();
									imgurl = lighttpdpath + "/userimages/" + id + "." + imgType + "?" + d.getTime();
								}

								//name = name.replace("CHR(90)", " ").replace("CHR(90)", " ").replace("CHR(90)", " ").replace("CHR(90)", " ");
								var id = jid.split('@')[0];
								//  console.log('id---'+id+"----name-- "+name);

								// transform jid to id
								var jid_id = Gab.jid_to_id(jid);
								if (typeof useRegType != 'undefined' && useRegType != null && useRegType != "") {
									if (useRegType.indexOf('_') != -1) {
										useRegType1 = useRegType.split('_')[1];
									} else {
										useRegType1 == useRegType;
									}
									if ((useRegType1.toLowerCase() == "social" || useRegType1.toLowerCase() == "standard") && upgradePlan == 0) {
										duser = 'socialUser';
									}
								}
								// if(role_id == 8 && created_by == userId){
								// 	guestUserClass = "guest_user";
								// } 
								Gab.insert_contact($("#userId_" + id));

							}
						} catch (e) {
							console.log("Chat exception:" + e);
						}
					});
				}
			}

			Gab.connection.addHandler(Gab.on_presence, null, "presence");
			Gab.connection.send($pres());
			listOfConnectedRoom();
		} catch (e) {
			console.log("Chat exception:" + e);
		}
	},

	//When msg will come, this code will execute
	on_message: function (message) {
		// alert("inside on msg");
		try {
			// console.log("inside on message");
			var full_jid = $(message).attr('from');
			var jid = Strophe.getBareJidFromJid(full_jid);
			var jid_id = Gab.jid_to_id(jid);
			var d = new Date();
			var t = getTimeIn12Format(d);
			var time = d.getHours() + ":" + d.getMinutes();
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			var msgtime = tConvert(time) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();
			var uid = jid.split('@')[0];
			var cid = $('#user_' + uid).attr('cid');
			var flag = true;
			var imgType = $('#userId_' + uid).attr('imgType');
			// console.log("uid--"+uid);
			var type = 'none';
			var link = '';
			var filename = '';
			// var popup = $('#' + jid_id + 'm').attr('value');
			var atpos = jid.indexOf("@");
			var id = jid_id.split("-")[0];
			var composing = $(message).find("composing");
			var msgStatus = $(message).children('json').text();
			var msgId = $(message).attr('msgid');
			// console.log("full_jid--" + full_jid + "jid--" + jid + "jid_id--" + jid_id + "composing--" + composing + "msgStatus--" + msgStatus + "msgId--" + msgId);

			var compose = false;
			var body = $(message).find('html > body');
			var replyId = $(message).children('replyForMsgId').text();
			var msg_action = $(message).children('chatFortype').text();
			// console.log("replyId--" + replyId + "msg_action--" + msg_action);
			if (msgStatus != "") {
				compose = true;
				// msgStatus = JSON.parse($(message).children('json').text());
				// if(msgStatus.status == "seen"){
				// 	 $('#'+uid+'m div#'+msgId+' .chat-outgoing-status').html("<i> Read</i>").removeClass("chat-outgoing-status-sent").removeClass("chat-outgoing-status-deliver").addClass("chat-outgoing-status-seen");;
				// }else if(msgStatus.status =="delivered"){
				// 	 $('#'+uid+'m div#'+msgId+' .chat-outgoing-status').html("<i> Delivered</i>").removeClass("chat-outgoing-status-sent").addClass("chat-outgoing-status-deliver");
				// }else if(msgStatus.status =="allseen"){
				// 	 $('#'+uid+'m  .chat-outgoing-status').html("<i> Read</i>").removeClass("chat-outgoing-status-sent").removeClass("chat-outgoing-status-deliver").addClass("chat-outgoing-status-seen");
				// }else if(msgStatus.status =="alldelivered"){
				// 	$('#'+uid+'m  .chat-outgoing-status-sent').html("<i> Delivered</i>").removeClass("chat-outgoing-status-sent").addClass("chat-outgoing-status-deliver");
				// }
				msgStatus = JSON.parse($(message).children('json').text());
				// console.log("msgStatus--" + msgStatus + "msgStatus.status--" + msgStatus.status);
				if (msgStatus.status == "seen") {
					$('#' + uid + 'm div#' + msgId + ' .chat-outgoing-status').html("<i> Read</i>").removeClass("chat-outgoing-status-sent").removeClass("chat-outgoing-status-deliver").addClass("chat-outgoing-status-seen");;
				} else if (msgStatus.status == "delivered") {
					$('#' + uid + 'm div#' + msgId + ' .chat-outgoing-status').html("<i> Delivered</i>").removeClass("chat-outgoing-status-sent").addClass("chat-outgoing-status-deliver");
				} else if (msgStatus.status == "allseen") {
					$('#' + uid + 'm  .chat-outgoing-status').html("<i> Read</i>").removeClass("chat-outgoing-status-sent").removeClass("chat-outgoing-status-deliver").addClass("chat-outgoing-status-seen");
				} else if (msgStatus.status == "alldelivered") {
					$('#' + uid + 'm  .chat-outgoing-status-sent').html("<i> Delivered</i>").removeClass("chat-outgoing-status-sent").addClass("chat-outgoing-status-deliver");
				}
			}
			var imgUrl = lighttpdPath + "/userimages/" + uid + "." + imgType;
			if (composing.length > 0) {
				compose = true;
				$("#conversationContactMsg_" + Strophe.getNodeFromJid(jid)).hide();
				$("#conversationContactMsg1_" + Strophe.getNodeFromJid(jid)).show();
				$("#conversationContactMsg1_" + Strophe.getNodeFromJid(jid)).text("typing...").delay(3200).hide(100);
				$("#conversationContactMsg_" + Strophe.getNodeFromJid(jid)).delay(3200).show(100);
				$(".userPresStatus_" + Strophe.getNodeFromJid(jid)).hide();
				$(".userStatus_" + Strophe.getNodeFromJid(jid)).show();
				$(".userStatus_" + Strophe.getNodeFromJid(jid)).text("typing...").delay(3200).hide(100);
				$(".userPresStatus_" + Strophe.getNodeFromJid(jid)).delay(3200).show(100);
			}
			else {
				if (body.length === 0) {
					body = $(message).find('body');
					if (body.length > 0) {
						body = body.text();
					}
					else {
						body = null;
					}
				}
				else {
					body = body.contents();
					var span = $('<span></span>');
					body.each(function () {
						if (document.importNode) {
							$(document.importNode(this, true)).appendTo(span);
						}
						else {
						}
					});
					body = span;

				}
			}
			// alert(compose)	
			if (compose == false) {
				// console.log(body);

				if ((!$('#uid_' + uid).is(':visible'))) {

					playSound('glass');
					cmeMessages();
					seenMsg(jid_id, "delivered", msgId);
					if ($("#cmecontent1 #user_" + Strophe.getNodeFromJid(jid)).length == 0) {
						// console.log("not exist");
						cmeMessages();
					}
					else {
						//console.log("it exist");
						if (body) {
							// $("#conversationList ul #" + jid_id).find(".coversationContactTime").html(msgtime);
							$("#conversationContactMsg_" + id).html(body);
							$('#conversationContactMsg_' + id).css({ 'font-weight': ' bold' });
							$("#conversationContactMsg1_" + id).hide();
							$("#conversationContactMsg_" + id).show();
							// var chatUi = $("#conversationList ul").find("#" + jid_id).clone();
							// $("#conversationList ul").find("#" + jid_id).remove();
							// $("#conversationList ul").prepend(chatUi);

							var countValue = $("#conversationMsgCount_" + id).html();
							$("#conversationMsgCount_" + id).css({ 'visibility': ' visible' });

							/*** Below line of code---- comment for CME alone and Uncomment for Colabus CME. **/
							var totalMsg = window.opener.getCount("notvisible");

							//console.log("totalMsg befor---"+totalMsg);
							if (countValue == undefined) {
								countValue = 1;
							} else {
								countValue++;
							}
							$("#conversationMsgCount_" + id).html(countValue);
						}
						//console.log("countValue after---"+countValue);
					}
					//var response = showPersonalConnectionMessages();
					//if(response == "sucess"){
					// window.opener.totalMsge();
					//playSound('glass');

					// if (body) {
					// 	if ((body.indexOf('xml') > 0) && (body.indexOf('href') > 0) && (body.indexOf('filesharing') > 0)) {
					// 		var $xml = $.parseXML(body);
					// 		filename = $($xml).find('href').attr('fileName');
					// 		if (filename.indexOf('CH(38)') > 0) {
					// 			filename = filename.replace("CH(38)", "&");
					// 		}
					// 		var imgUrl = lighttpdPath + "/userimages/" + id + "." + imgType;
					// 		ChangeImage1(imgUrl, filename);
					// 		$("#conversationContactMsg_" + id).html(filename);
					// 		$('#conversationContactMsg_' + id).css({ 'font-weight': ' bold' });
					// 	} else {
					// 		ChangeImage1(imgUrl, body);
					// 	}
					// }
					// seenMsg(jid_id, "delivered", msgId);

					if (body) {
						seenMsg(jid_id, "delivered", msgId);
					}
				}
				else {
					playSound('glass');
					try {
						if (body && uid != userIdglb) {
							if ($("#" + msgId).length == 0) {
								// this code block is to handle file sharing 

								if ((body.indexOf('xml') > 0) && (body.indexOf('href') > 0) && (body.indexOf('filesharing') > 0)) {
									//console.log("if block");
									var $xml = $.parseXML(body);
									link = $($xml).find('href').attr('value');
									type = 'filesharing';
									filename = $($xml).find('href').attr('fileName');
									let UI = '';
									if (msg_action == "forward") {
										var splitt = link.split('uploadedDocuments/')[1];
										var splitting = splitt.split('?')[0];
										var file_id = splitting.split('.')[0];
										var file_ext = splitting.split('.')[1];
										// console.log("link--" + link + "splitting--" + splitting);

										if (filename.indexOf('CH(38)') > 0) {
											filename = filename.replace("CH(38)", "&");
										}

										//    $("#conversationContactMsg_"+uid).html(filename);
										//    $('#conversationContactMsg_'+uid).css({'font-weight':' bold'});
										//var imgUrl = lighttpdPath+"/userimages/"+id+"."+imgType ;
										// var test = ChangeImage1(imgUrl,filename);
										UI = "<div id=" + msgId + " class='w-50 ml-2 d-flex justify-content-start chat' style = '' >"
												+ "<div class='chatId' uid='" + uid + "'>"
													+ "<div onclick='replyForChat(this)' ondblclick='replyMsg()' from='one' ondblclick='replyMsg()' messageType='file' msg='" + filename + "' class='text-white px-3 py-2 mt-2 float-left chat-income-message singlebubble replyToReply '>"

										if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
											UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
													"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"

											UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;'>" + filename + "</a>" +
												"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
													+ "<div id='' class='defaultWordBreak float-left position-relative'>"
													+ "<img id='uploadedImage' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + link + "' class='downCalIcon '>"
												+ "</div>"
											+ "</div>"
										}
										else {
											file_ext = file_ext.toLowerCase();
											UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
												"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"

											UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + filename + "</a>" +
												"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
													+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"

													+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"

												+ "</div>"
											+ "</div>"
										}
										UI += "</div>"
											+ "<div class='py-1 float-left text-secondary chat-income-time' style=''>" + t + "</div></div></div>"

									}
									else {
										var splitting = link.split('//')[3];
										var file_id = splitting.split('.')[0];
										var file_ext = splitting.split('.')[1];
										// console.log("link--" + link + "splitting--" + splitting);

										if (filename.indexOf('CH(38)') > 0) {
											filename = filename.replace("CH(38)", "&");
										}

										//    $("#conversationContactMsg_"+uid).html(filename);
										//    $('#conversationContactMsg_'+uid).css({'font-weight':' bold'});
										//var imgUrl = lighttpdPath+"/userimages/"+id+"."+imgType ;
										// var test = ChangeImage1(imgUrl,filename);
										UI = "<div id=" + msgId + " class='ml-2 w-50 d-flex justify-content-start chat' style = '' >"
											+ "<div class='chatId' uid='" + uid + "'>"
											+ "<div onclick='replyForChat(this)' ondblclick='replyMsg()' from='one' ondblclick='replyMsg()' messageType='file' msg='" + filename + "' class='text-white px-3 py-2 mt-2 float-left chat-income-message singlebubble replyToReply '>"

										if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
											UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;'>" + filename + "</a>" +
												"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
													+ "<div id='' class='defaultWordBreak float-left position-relative'>"
													+ "<img id='uploadedImage' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + link + "' class='downCalIcon '>"
												+ "</div>"
											+ "</div>"
										}
										else {
											file_ext = file_ext.toLowerCase();
											UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + filename + "</a>" +
												"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
													+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
													+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
												+ "</div>"
											+ "</div>"
										}
										UI +="</div>"
											+ "<div class='py-1 float-left text-secondary chat-income-time' style=''>" + t + "</div></div></div>"
									}

									$('#uid_' + uid).append(UI);
									$("#" + uid + "m").animate({ scrollTop: 20000 }, 'normal');
									updateSeenMessages(cid, uid, "chat");
									seenMsg(jid_id, "seen", msgId);
									// cmeMessages();
								}
								else {
									// if (replyId != 0 && msg_action == "reply") {
									var replyUserId = $(message).children('replyForUserId').text();
									var replyName;
									var mainChatThread;
									var color = "";
									if (msg_action == "reply") {
										color = generateRandomColor();
										// if (replyId != undefined && replyId != 0 && msg_action != undefined && msg_action != "forward") {
										if (replyUserId == userIdglb) {
											replyName = "You";
											color = "#50baba;";
										} else {
											replyName = $("#userId_" + replyUserId).find(".user_box_name").text();
										}
										mainChatThread = $(message).children('replymsg').text();
									}
									let UI = "<div id='" + msgId + "' class='ml-2 w-50 d-flex justify-content-start chat' style = '' >" +
												"<div class='chatId' uid='" + uid + "'>"

									UI += "<div  onclick='replyForChat(this)' from='one' ondblclick='replyMsg()' messageType='text' msg='" + body + "'  class=' defaultWordBreak text-white px-3 py-2 mt-2 float-left chat-income-message singlebubble bubble replyToReply ChatText' >"
									if (msg_action == "reply") {
										UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
											"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
									}
									if (msg_action == "forward") {
										UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
											"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
									}
									UI += "" + body + "</div>"
									UI += "<div class='py-1 float-left text-secondary chat-income-time' style=''>" + t + "</div></div></div>"

									$('#uid_' + uid).append(UI);
									$("#" + uid + "m").animate({ scrollTop: 20000 }, 'normal');
									updateSeenMessages(cid, uid, "chat");
									seenMsg(jid_id, "seen", msgId);
									// cmeMessages();
								}
							}
						}
					} catch (eMsg) {
						console.log(eMsg);
					}
				}
				// playSound('glass');
			}
		} catch (e) {
			console.log("Chat exception:" + e);
		}
		return true;
	},

	on_public_message: function (message) {
		// alert("inside public gmsg");
		try {
			console.log("inside public gmsg");
			var from = $(message).attr('from');
			var body = $(message).children('body').text();
			var room = Strophe.getBareJidFromJid(from);
			var Id = room.split('@')[0];
			var nick = Strophe.getResourceFromJid(from);
			var roomType = $(message).attr('roomType');
			var FromName = from.split('/')[1];
			var origin = from.split('/')[0];
			var msgId = $(message).attr('msgid');
			// var msguid = msgId.split('@@')[1];
			// console.log("from--" + from + "body--" + body + "room--" + room + "Id--" + Id + "msgId--" + msgId + "roomType--" + roomType + "FromName--" + FromName + "origin--" + origin);
			var roomType;
			if (origin.indexOf("@mucr.devchat.colabus.com") > -1 || origin.indexOf("@mucr.chat.colabus.com") > -1) {
				roomType = "groupmsg";
			} else {
				roomType = "conference";
			}
			var userid;
			var senderName;
			var senderImgType;
			if (nick !== null) {
				senderName = nick.split('_')[2];
				senderImgType = nick.split('_')[3];
				userid = nick.split('_')[1];
			}
			var d = new Date();
			var time = d.getHours() + ":" + d.getMinutes();
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			var msgtime = tConvert(time) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();
			var id = Id + "m";
			var t = getTimeIn12Format(d);
			var msg = body;
			var imgName = lighttpdpath + "/userimages/" + userid + "." + senderImgType + "?" + d.getTime();
			var msg_action = $(message).children('chatFortype').text();
			var replyId = $(message).children('replyForMsgId').text();
			// console.log("replyId--"+replyId+"msg_action--"+msg_action);
			if (roomType != "conference") {
				var d = new Date();
				var time = d.getHours() + ":" + d.getMinutes();
				const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var msgtime = tConvert(time) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

				var id = Id + "m";
				var t = getTimeIn12Format(d);
				var msg = body;

				//if((!$('#'+id+' #chat-dialog').is(':visible'))||(ifvisible.now('hidden'))){
				if ((!$('#guid_' + Id).is(':visible'))) {
					playSound('glass');
					cmeMessages();
				}
				else {
					try {
						if (body && userid != userIdglb) {
							// this code block is to handle file sharing 
							// console.log(body);
							if ((body.indexOf('xml') > 0) && (body.indexOf('href') > 0) && (body.indexOf('filesharing') > 0)) {
								if ($("#" + msgId).length == 0) {
									var UI = '';
									var $xml = $.parseXML(body);
									link = $($xml).find('href').attr('value');
									type = 'filesharing';
									filename = $($xml).find('href').attr('fileName');
									if (msg_action == "forward") {
										var splitt = link.split('uploadedDocuments/')[1];
										var splitting = splitt.split('?')[0];
										var file_id = splitting.split('.')[0];
										var file_ext = splitting.split('.')[1];
										// console.log("link--" + link + "splitting--" + splitting);

										if (filename.indexOf('CH(38)') > 0) {
											filename = filename.replace("CH(38)", "&");
										}
										//    $("#conversationContactMsg_"+uid).html(filename);
										//    $('#conversationContactMsg_'+uid).css({'font-weight':' bold'});
										//var imgUrl = lighttpdPath+"/userimages/"+id+"."+imgType ;
										// var test = ChangeImage1(imgUrl,filename);
										UI = "<div id='" + msgId + "' class='media py-1 chat' style='display: -webkit-box;'>" +
												"<img src='" + imgName + "' class='align-self-start mt-4 mr-3 rounded-circle' title='" + senderName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
												"<div class='media-body chatId' uid='" + userid + "'>" +
													"<div style='font-size: 13px;color: gray;'>" + senderName + "</div>"

										UI += "<div onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='file' msg='" + filename + "' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;'>"
										if (msg_action == "forward") {
											UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
												"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
										}

										if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
											UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  style='color:#fff;'>" + filename + "</a>" +
												"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
													+ "<div id='' class='defaultWordBreak float-left position-relative'>"
														+ "<img id=''  ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + link + "' class='downCalIcon '>"
													+ "</div>"
												+ "</div>"
										}
										else {
											file_ext = file_ext.toLowerCase();
											UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + filename + "</a>" +
												"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
													+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
														+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
													+ "</div>"
												+ "</div>"
										}

										UI +=	"</div>" +
											"<div class='chat-income-time float-left p-0 pt-1'>" + t + "</div>" +
											"</div>" +
										"</div>"
									}
									else {
										var splitting = link.split('//')[3];
										var file_id = splitting.split('.')[0];
										var file_ext = splitting.split('.')[1];
										// console.log("filename--" + filename + "link--" + link);
										// console.log("$xml--" + $xml + "file_id--" + file_id);
										if (filename.indexOf('CH(38)') > 0) {
											filename = filename.replace("CH(38)", "&");
										}
										//    $("#conversationContactMsg_"+uid).html(filename);
										//    $('#conversationContactMsg_'+uid).css({'font-weight':' bold'});
										//var imgUrl = lighttpdPath+"/userimages/"+id+"."+imgType ;
										// var test = ChangeImage1(imgUrl,filename);
										UI = "<div id='" + msgId + "' class='media py-1 chat' style='display: -webkit-box;'>" +
												"<img src='" + imgName + "' class='align-self-start mt-4 mr-3 rounded-circle' title='" + senderName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
												"<div class='media-body chatId' uid='" + userid + "'>" +
													"<div style='font-size: 13px;color: gray;'>" + senderName + "</div>"

										UI += "<div onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='file' msg='" + filename + "' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;'>"


										if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
											UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  style='color:#fff;'>" + filename + "</a>" +
												"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
													+ "<div id='' class='defaultWordBreak float-left position-relative'>"
														+ "<img id=''  ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + link + "' class='downCalIcon '>"
													+ "</div>"
												+ "</div>"
										}
										else {
											file_ext = file_ext.toLowerCase();
											UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + filename + "</a>" +
												"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
													+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
														+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
													+ "</div>"
												+ "</div>"
										}

										UI +=	"</div>" +
											"<div class='chat-income-time float-left p-0 pt-1'>" + t + "</div>" +
											"</div>" +
										"</div>"
									}

									$('#guid_' + Id).append(UI);
									playSound('glass');
									$('#' + Id + 'm').animate({ scrollTop: 20000 }, 'normal');
									updateSeenMessages(Id, userid, "groupchat");
									// cmeMessages();
								}
							}
							else {
								// console.log("else");
								if ($("#" + msgId).length == 0) {
									// var msgId = $(message).attr('msgid');
									var replyId = $(message).children('replyForMsgId').text();
									// console.log("replyId--" + replyId);
									var mainChatThread;
									var replyName;
									if (replyId != undefined && replyId != 0) {
										var replyUserId = $(message).children('replyForUserId').text();
										if (replyUserId == userIdglb) {
											replyName = "You"
										} else {
											replyName = $("#userId_" + replyUserId).find(".user_box_name").text();
										}
										mainChatThread = $(message).children('replymsg').text();
									}
									let UI = "<div id='" + msgId + "' class='media chat'>" +
										"<img src='" + imgName + "' class='align-self-start mt-4 mr-3 rounded-circle' title='" + senderName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
										"<div class='media-body chatId' uid='" + userid + "'>" +
										"<div class=''>" + senderName + "</div>" +
										"<div onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='text' msg='" + body + "' class='float-left chat-income-message singlebubble bubble' style='max-width: 70%;word-break: break-word;'>"
									if (replyId != undefined && replyId != 0) {
										UI += "<div class='replyNameTop'>" + replyName + "</div>" +
											"<div class='replyMsgBottom'>" + mainChatThread + "</div>"
									}
									UI += "" + body + "</div>" +
										"<div class='chat-income-time float-left p-0 pt-1'>" + t + "</div>" +
										"</div>" +
										"</div>"

									$('#guid_' + Id).append(UI);
									playSound('glass');
									$('#' + Id + 'm').animate({ scrollTop: 20000 }, 'normal');
									updateSeenMessages(Id, userid, "groupchat");
									// cmeMessages();
								}
							}
						}
					}
					catch (eMsg) {
						console.log(eMsg);
					}



					// if (userid != userIdglb) {

					// 	playSound('glass');
					// 	if ($("#" + msguid).length == 0) {
					// 		let UI="<div id='"+msguid+"' class='media'>"+
					// 					"<img src='"+imgName+"' class='align-self-start mt-4 mr-3 rounded-circle' onerror='groupImageOnErrorReplace(this)'; style='width: 40px'>"+
					// 					"<div class='media-body'>"+
					// 						"<div>"+senderName+"</div>"+
					// 						"<div class='float-left chat-income-message bubble' style='max-width: 70%;word-break: break-word;'>"+body+"</div>"+
					// 						"<div class='chat-income-time float-left p-0 pt-1'>"+msgtime+"</div>"+
					// 					"</div>"+
					// 				"</div>"

					// 		$('#guid_' + Id).append(UI);
					// 		$('#'+Id+'m').animate({ scrollTop: 20000 }, 'normal');
					// 		updateSeenMessages(Id, userid, "groupchat" );
					// 		// cmeMessages();
					// 	}

					// }
					// if(userid != userId){
					// playSound('glass');
					//window.opener.totalMsge();
					// var imgUrl = lighttpdPath+"/userimages/"+userid+"."+senderImgType ;
					// ChangeImage1(imgUrl,body);
				}
				// $("#conversationList ul").find("#"+Id).find(".coversationGroupContactMsg").html(body);
				// $("#conversationList ul").find("#"+Id).find(".coversationGroupContactMsg").show();
				// $("#conversationList ul").find("#"+Id).find(".coversationGroupContactTime").html(msgtime);
				// if(!$('#'+id+' #chat-dialog').is(':visible')){
				// //showPersonalConnectionMessages();

				// var chatUi = $("#conversationList ul").find("#" + Id).clone();
				// $("#conversationList ul").find("#" + Id).remove();
				// $("#conversationList ul").prepend(chatUi);
				// var countValue = $("#conversationList ul").find("#"+Id).find(".chatnotification").html();
				// $("#conversationList ul").find("#"+Id).find(".chatnotification").css({'visibility':' visible'});
				// if(countValue == undefined){
				// 	countValue=1;
				// }else{
				// 	countValue++; 
				// }
				// $("#conversationList ul").find("#"+Id).find(".chatnotification").html(countValue);

				/*** Below line of code- comment for CME alone and Uncomment for Colabus CME. **/
				// 			var totalMsg =  window.opener.getCount("notvisible");

				// 		}else{
				// 			if(userid != userId){
				// 				var msgId = $(message).attr('msgid');
				// 				var replyId = $(message).children('replyForMsgId').text();

				// 				if(replyId != undefined && replyId != 0){
				// 					var replyUserId = $(message).children('replyForUserId').text();
				// 					var replyName;
				// 					if(replyUserId == userId){
				// 						replyName="You"
				// 					}else{
				// 						replyName=$("#"+replyUserId).find(".user_box_name").text(); 
				// 					}
				// 					var mainChatThread = $(message).children('replymsg').text();

				// 					$('#'+Id+'m' + ' #chat-dialog').append(
				// 								"<div id='"+msgId+"' class='chat-income-overall row chatId' style='outline:none' tabindex='0'>"+
				// 								"<div style='width:100%;float:left'>"+
				// 									"<div class='chat-income-imageframe col-xs-1' style='margin-top: 23px;'>"+
				// 										"<img  userId='"+userid+"' src='"+lighttpdPath+"/userimages/"+userid+"."+senderImgType+"?"+imgTime+"'  title='"+senderName+"' onerror='userImageOnErrorReplace(this);' class='incomeframeImage' style='width: 40px; height: 38px; border-radius: 50%;'/>"+
				// 									"</div>"+
				// 									"<div class='col-xs-10 chatIncomeBubble' style = 'padding-left: 0vh;margin-left:-1em' >"+
				// 										"<div class='row' align='left' style='font-family: helvetica; font-size: 13px; width: 105%; padding-left: 34px;'>"+senderName+"</div>"+
				// 										"<div onclick='replyForChat(this)' class='chat-income-message bubble row'>"+
				// 										"<div class='replyNameTop'>"+replyName+"</div>"+
				// 										"<div class='replyMsgBottom'>"+mainChatThread+"</div>"+
				// 										"<div messageType='text'  class='replyToReply ChatText'>"+
				// 										msg+
				// 										"</div>"+
				// 										"</div>"+
				// 										"<div class='chat-income-time row' style='padding-left: 34px;'>"+t+"</div>"+
				// 								"</div>"+
				// 							"</div>"+
				// 							"</div>");

				// 				}else{
				// 						$('#'+Id+'m' + ' #chat-dialog').append(
				// 						"<div id='"+msgId+"' class='chat-income-overall row chatId' style='outline:none' tabindex='0'>"+
				// 						"<div style='width:100%;float:left'>"+
				// 							"<div class='chat-income-imageframe col-xs-1' style='margin-top: 23px;'>"+
				// 								"<img  userId='"+userid+"' src='"+lighttpdPath+"/userimages/"+userid+"."+senderImgType+"?"+imgTime+"'  title='"+senderName+"' onerror='userImageOnErrorReplace(this);' class='incomeframeImage' style='width: 40px; height: 38px; border-radius: 50%;'/>"+
				// 							"</div>"+
				// 							"<div class='col-xs-10 chatIncomeBubble' style = 'padding-left: 0vh;margin-left:-1em' >"+
				// 								"<div class='row' align='left' style='font-family: helvetica; font-size: 13px; width: 105%; padding-left: 34px;'>"+senderName+"</div>"+
				// 								"<div onclick='replyForChat(this)' class='chat-income-message bubble row'>"+
				// 								"<div messageType='text'  class='ChatText'>"+
				// 								msg+
				// 								"</div>"+
				// 								"</div>"+
				// 								"<div class='chat-income-time row' style='padding-left: 34px;'>"+t+"</div>"+
				// 						"</div>"+
				// 					"</div>"+
				// 					"</div>");

				// 				}
				// 					var windowWidth1 = $(window).width();
				// 					if(windowWidth1<750){
				// 						$(".incomeframeImage").css({'margin-left':'-7vh'});
				// 					}else{
				// 						$(".incomeframeImage").css({'margin-left':'-4vh'});
				// 					}
				// 					playSound('glass');
				// 			}
				// 			var h = $('#'+id).find('#chat-dialog')[0].scrollHeight;
				// 			$('#'+id).find('#chat-dialog').scrollTop(h);
				// 		}

				// 		$("#chat-dialog").mCustomScrollbar("update");
				}
				else{
					console.log("insdie on group");
					var full_jid = $(message).attr('from');
					var jid = Strophe.getBareJidFromJid(full_jid);
					var jid_id = Gab.jid_to_id(jid);
					var type='none';
					var id=jid_id.split("-")[0];
					var jsonMsg = $(message).children('json').text();
					//console.log(userConnected);
					if(userid != userIdglb ){
						// console.log("------inside userif");
						var composing = $(message).find("composing");
						if(!(composing.length > 0)){
							// console.log("------inside compose.length");
							// console.log("1jid_id--"+jid_id+"jsonMsg--"+jsonMsg+"composing--"+composing);
							gotVideoRoomMsg(message,userid);
							if(userConnected.indexOf(parseInt(userid))<0){
								// console.log("------public gotmsgfromserver");
								// console.log("2jid_id--"+jid_id+"jsonMsg--"+jsonMsg+"composing--"+composing);
								gotMessageFromServer(parseInt(userid),message);
							}

						}else{
							$("#videochat_"+userid).show();
							$("#videochat_"+userid).html(" is typing...").delay(2200).hide(100);
						}
					}
				
			}
			return true;
		}
		catch (eMsg) {
			console.log(eMsg);
		}
	},

	on_presence: function (presence) {
		try {
			var ptype = $(presence).attr('type');
			var from = $(presence).attr('from');
			var jid_id = Gab.jid_to_id(from);
			var id = from.split('@')[0];
			// console.log("from--" + from + " jid_id--" + jid_id + "--ptype---" + ptype + "--id---" + id);
			var chatStatus = "";
			var contact = $("#userId_" + id);
			if (ptype !== 'unavailable' && ptype !== 'error') {
				var show = $(presence).find("show").text();
				// console.log('show--'+show);
				if (show === "" || show === "chat") {
					// console.log('online');
					contact.removeClass('offline');
					contact.addClass('online');
					// contact.removeClass('dnd');
					contact.removeClass('user_box_status_away');
					chatStatus = "online";
				}else if (show === 'online') {
					// console.log("inside online");
					contact.removeClass('offline');
					contact.addClass('online')
					contact.removeClass('user_box_status_away');
					// if($("#userId_" + id).hasClass('dnd')){
					// 	contact.addClass('dnd');
					// }else{
					// 	contact.removeClass('dnd');
					// }
					chatStatus = "online";
					// $('.uid_'+id).removeClass('away');
				}else if (show === "dnd") {
					// console.log('dnd');
					contact.addClass('dnd');
					chatStatus = "dnd";
					contact.removeClass('offline');
					contact.removeClass('pOnline');
					contact.addClass('online');
					contact.removeClass('user_box_status_away');
				}else if (show === "dndno") {
					// console.log('dnd');
					contact.removeClass('dnd');
					// chatStatus = "online";
					// contact.addClass('online');
				}else if (show === 'away') {
					// console.log('away');
					// contact.removeClass('online');
					// contact.addClass('away');
					if(callId != '' || callId != undefined){
						contact.addClass('online');
						contact.addClass('user_box_status_away');
						if($("#userId_" + id).hasClass('dnd')){
							contact.addClass('dnd');
						}else{
							contact.removeClass('dnd');
						}
						contact.removeClass('offline');
						chatStatus = "away";
						// $('.uid_'+id).addClass('away');
					}
				}
				
			}
			else if (ptype === 'unavailable') {
				// console.log('unavailable');
				contact.removeClass('online');
				// contact.removeClass('dnd');
				contact.addClass("offline");
				contact.removeClass('user_box_status_away');
				// $('.uid_'+id).removeClass('away');
				chatStatus = "offline";
			}  
			else if (ptype === 'error') {
				// console.log('error');
				contact.removeClass('online');
				contact.removeClass('dnd');
				contact.removeClass('user_box_status_away');
				chatStatus = "offline";
				// $('.uid_'+id).removeClass('away');
			}
			processUserList(presence, from, chatStatus);
			var li = contact.find(".roster-contact");
			// li.remove();
			Gab.insert_contact(li);
			presenseUpdate(jid_id, chatStatus, li);

			var jid_id = Gab.jid_to_id(from);
			// $('#chat-' + jid_id).data('jid', Strophe.getBareJidFromJid(from));
			var statusOnline = chatUserArray.getUserOnlineStatus(jid_id.split('-')[0]);
			// console.log("statusOnline:" + statusOnline);
		} catch (e) {
			console.log("Chat exception:" + e);
		}
		return true;
	},

	on_roster_changed: function (iq) {
		try {
			console.log("inside roster changed");
			// alert("roaster change function");
			$(iq).find('item').each(function () {
				try {
					var sub = $(this).attr('subscription');
					var jid = $(this).attr('jid');
					var name = $(this).attr('name') || jid;
					var jid_id = Gab.jid_to_id(jid);
					// console.log("Sub " + sub + "Jid " + jid + "name " + name + "jid_id " + jid_id);
					if (sub === 'remove') {
						//contact is being remove
						$('#' + jid_id).remove();
					} else {
						//contact is being added or modified
						var contact_html = "<li id='" + jid_id + "'>" +
							"<div class='" +
							($('#' + jid_id).attr('class') || "roster-contact offline") +
							"'>" +
								"<div class='roster-name'>" + name +
								"</div><input type='hidden' class='roster-jid'>" + jid +
							"></div></li>";

						if ($('#' + jid_id).length > 0) {
							$('#' + jid_id).replaceWith(contact_html);
							//namesFromColabusOffContacts();
						} else {
							Gab.insert_contact(contact_html);
						}
					}
				} catch (e) {
					console.log("Chat exception:" + e);
				}
			});
		} catch (e) {
			console.log("Chat exception:" + e);
		}
		return true;
	},

	presence_value: function (elem) {

		if (elem.hasClass('online')) {
			return 2;
		} else if (elem.hasClass('away')) {
			return 1;
		}
		return 0;
	},

	on_normal_message: function (message) {
		console.log("on normal message");
		var full_jid = $(message).attr('from');
		var jid = Strophe.getBareJidFromJid(full_jid);
		var jid_id = Gab.jid_to_id(jid);
		var type = 'none';
		var id = jid_id.split("-")[0];
		var imgType = $('#userId_' + id).attr('imgtype');
		var imgUrl = lighttpdPath + "/userimages/" + id + "." + imgType;
		//console.log("inside normal1");
		var jsonMsg = $(message).children('json').text();
		//console.log("ijsonMsg--"+jsonMsg);
		var composing = $(message).find("composing");
		// console.log("jid_id--"+jid_id+"jsonMsg--"+jsonMsg+"composing--"+composing);
		var compose = false;
		if (composing.length > 0) {
			compose = true;
			$(".userPresStatus_" + Strophe.getNodeFromJid(jid)).hide();
			$(".userStatus_" + Strophe.getNodeFromJid(jid)).show();
			$(".userStatus_" + Strophe.getNodeFromJid(jid)).text("typing...").delay(3200).hide(100);
			$(".userPresStatus_" + Strophe.getNodeFromJid(jid)).delay(3200).show(100);
		}
		if (compose == false) {
			gotMessageFromServer(parseInt(id), message);
		}

		return true;
	},

	//For insert contact to list
	insert_contact: function (elem) {
		try {
			// console.log("inside insert_contact");
			// var pname = elem.find('.pname').val();//.attr('value')
			var pname = elem.attr('pname');
			// console.log(pname);
			if (pname != undefined) {
				var jid = elem.attr('jid');
				var id = elem.attr('uid');
				var pres = Gab.presence_value(elem);
				//    console.log("jid--"+jid+" pname--"+pname +"--id---"+id); 
				var contacts = $('#userId_' + id);
				//scrollbarSinglechat(); 
				// console.log("inside contacts");
				/*
				  if (contacts.length > 0) {
					  console.log("inside contacts1");
					  var inserted = false;
	  
					  contacts.each(function () {
	  
						  var cmp_pres = Gab.presence_value($(this).find('.roster-contact'));
						  var cmp_jid = $(this).find('.rosterJid').val();
						  var cmp_name = $(this).find('.roster-contact').attr('value');
						  console.log(cmp_pres + "--" + pres);
						  if (pres > cmp_pres) {
							  $(this).before(elem);
							  inserted = true;
							  return false;
						  }
						  else{
	  
							  var n = pname.localeCompare(cmp_name);
							  if (n == -1) {
				  // console.log("inside insert_contact");
				  var pname = elem.find('.pname').val();//.attr('value')
				  if (pname != undefined) {
					  var jid = elem.attr('jid');
					  var id= elem.attr('uid');
					  var pres = Gab.presence_value(elem);
					  //    console.log("from--"+from+" jid_id--"+jid_id +"--ptype---"+ptype+"--id---"+id); 
					  var contacts = $('#userId_' + id);
					  //scrollbarSinglechat(); 
					  console.log("inside contacts");
				  /*
					  if (contacts.length > 0) {
						  console.log("inside contacts1");
						  var inserted = false;
	  
						  contacts.each(function () {
	  
							  var cmp_pres = Gab.presence_value($(this).find('.roster-contact'));
							  var cmp_jid = $(this).find('.rosterJid').val();
							  var cmp_name = $(this).find('.roster-contact').attr('value');
							  console.log(cmp_pres + "--" + pres);
							  if (pres > cmp_pres) {
								  $(this).before(elem);
								  inserted = true;
								  return false;
							  }
							  else{
	  
								  var n = pname.localeCompare(cmp_name);
								  if (n == -1) {
									  $(this).before(elem);
									  inserted = true;
									  return false;
								  }
							  }
						  });
	  
						  if (!inserted) {
							  console.log("inside not inserted");
							  $('#userId_' + id).append(elem);
						  }
	  
					  } else {
						  console.log("inside  inserted");
						  $('#userId_' + id).append(elem);
					  }
					  */
			}
		} catch (e) {
			console.log("Chat exception:" + e);
		}
	}
}

var chatUserArray = [];
function processUserList(presence, from, status) {
	try {
		var resourceId = from.split("/")[1];
		var resourceUserId = from.split("@")[0];
		if (!resourceId.includes("_")) {
			var tagMap = {};
			var i = null;
			for (i = 0; chatUserArray.length > i; i += 1) {
				tagMap[chatUserArray[i].resourceId] = chatUserArray[i];
			}
			var hasTag = function (tagName) {
				return tagMap[tagName];
			};
			if (typeof (hasTag(resourceId)) == 'undefined') {
				chatUserArray.push({
					resourceId: resourceId,
					userId: resourceUserId,
					status: status
				});
			} else {
				var aIndex = chatUserArray.getIndexOfResourceId(resourceId);
				chatUserArray[aIndex].status = status;
			}
		}
	} catch (e) {
		console.log("Chat exception:" + e);
	}
}

function presenseUpdate(jid_id, userPres, li) {
	try {
		// console.log("jid--"+jid_id);
		var id = jid_id.split("-")[0];
		var convObj = $(".uid_" + id);
		var contactObj = $("#userId_" + id).find(".user_box_status");
		// var chatcontentObj = $("#" + jid_id + "m").find(".user_box_status");
		if (userPres == "online" || userPres == "dnd") {
			// convObj.removeClass("user_box_status_offline");
			// contactObj.removeClass("user_box_status_offline");
			// chatcontentObj.removeClass("user_box_status_offline");
			convObj.addClass("user_box_status_online");
			contactObj.addClass("user_box_status_online");
			convObj.removeClass("away");
			
			// chatcontentObj.addClass("user_box_status_online");
			$('.userPresStatus_' + id).css({ "color": "#08B627" });
			$('.userPresStatus_' + id).show().text("online");
			var cloned = $("#userId_" + id).clone();
			$("#userId_" + id).remove();
			$("#cmecontent2").prepend(cloned);

		}else if (userPres == "away") {
			// console.log('away');
			convObj.addClass("away");
			contactObj.addClass("away");
			var uid = $("#cmecontent2").find('.offline:first').attr('uid');
			// console.log("Uid--"+uid);
			$('.userPresStatus_' + id).css({ "color": "orange" });
			$('.userPresStatus_' + id).show().text("away");
			var cloned = $("#userId_" + id).clone();
			$("#userId_" + id).remove();
			// $("#cmecontent2").prepend(cloned);
			if(uid == undefined || uid == ""){
				$("#cmecontent2").append(cloned);
			}else{
				$("#cmecontent2").find('#userId_'+uid).before(cloned);
			}
		}
		 else if (userPres == "offline") {
			var statusOnline = chatUserArray.getUserOnlineStatus(jid_id.split('-')[0]);
			if (statusOnline == true) {
				// console.log('true');
				// convObj.removeClass("user_box_status_offline");
				// contactObj.removeClass("user_box_status_offline");
				// chatcontentObj.removeClass("user_box_status_offline");
				convObj.addClass("user_box_status_online");
				contactObj.addClass("user_box_status_online");
				// chatcontentObj.addClass("user_box_status_online");
				// $('#chat-box li#' + jid_id + ' .roster-contact').removeClass("offline");
				// $('#chat-box li#' + jid_id + ' .roster-contact').addClass("online");
				$('.userPresStatus_' + id).css({ "color": "#08B627" });
				$('.userPresStatus_' + id).show().text("online");
				//   $("#user"+id).removeClass('offline');
				//   $("#user"+id).addClass('online');
				$("#userId_" + id).removeClass('offline');
				$("#userId_" + id).addClass('online');
				var cloned = $("#userId_" + id).clone();
				$("#userId_" + id).remove();
				$("#cmecontent2").prepend(cloned);
			} else {
				// console.log('false');
				convObj.removeClass("user_box_status_online");
				contactObj.removeClass("user_box_status_online");
				// chatcontentObj.removeClass("user_box_status_online");
				// convObj.addClass("user_box_status_offline");
				// contactObj.addClass("user_box_status_offline");
				// chatcontentObj.addClass("user_box_status_offline");
				$('.userPresStatus_' + id).css({ "color": "gray" });
				$('.userPresStatus_' + id).show().text("offline");
				// $('#chat-box #' + jid_id).remove();
				$("#userId_" + id).removeClass('online');
				$("#userId_" + id).addClass('offline');
				var contacts1 = $('.offline');

				var pname1 = $("#userId_" + id).find(".pname").text();


				contacts1.each(function () {

					var cmp_name1 = $(this).find('.pname').text();
					var n1 = pname1.localeCompare(cmp_name1);
					//    console.log("pname1--"+pname1+" cmp_name1--"+cmp_name1 +"--n1---"+n1+"--id---"+id); 
					if (n1 == -1) {
						let clone = $("#userId_" + id).clone();
						$("#userId_" + id).remove();
						$(this).before(clone);
						return false;
					}
				});
				// 	var contacts1 = $('#chat-box li div.offline');
				//   var pname1 = li.find('.user_box_name').attr('value');
				//   if(pname1 != undefined){
				// 	  contacts1.each(function(){
				// 		  var cmp_name1 = $(this).find('.user_box_name').attr('value');
				// 		  var n1 = pname1.localeCompare(cmp_name1);
				// 			  if(n1 == -1){
				// 				$(this).parent().before(li);
				// 				return false; 
				// 			  }
				// 	  });
				//   }
			}
		}
		/*C:
		$("#convoListContainer li").on("mouseover", function () {
			$(this).find('.hover-call-icons').show();
			if ($(this).find("div.roster-contact").hasClass("dnd")) {
				$(this).find('.hover-call-icons').find('.videocall').find('img').css({ "cursor": "not-allowed", "opacity": "0.5" }).attr("onclick", "");
				$(this).find('.hover-call-icons').find('.audiocall').find('img').css({ "cursor": "not-allowed", "opacity": "0.5" }).attr("onclick", "");
			} else {
				var vuserId = $(this).find("div.roster-contact").attr("id");
				$(this).find('.hover-call-icons').find('.videocall').find('img').css({ "cursor": "", "opacity": "" }).attr("onclick", "startavCall(" + vuserId + ",\"true\",\"video\")");
				$(this).find('.hover-call-icons').find('.audiocall').find('img').css({ "cursor": "", "opacity": "" }).attr("onclick", "startavCall(" + vuserId + ",\"true\",\"audio\")");
			}
		}).on("mouseout", function () {
			$(this).find('.hover-call-icons').hide();
		});*/
		observer.observe();//---- to lazy load user images
	} catch (e) {
		console.log("Chat exception:" + e);
	}
}

function cmeExpandCollapse(obj) {
	console.log("cmeinchat--"+cmeinchat+"cmeinchatid--"+cmeinchatid);
	if ($(obj).attr('src').indexOf('fullscreen_min.svg') != -1) {
		var drag = $('#chatVideo').is(":visible") || $('#chatAudio').is(":visible");
		// console.log("drag--"+drag);
		$('.cmeFullCompact').attr('src', 'images/cme/fullscreen_max.svg').attr('title', 'Expand');
		$('#cmecontent').removeClass('cmecontainer').removeClass('cmecontainer-min').addClass('cmecontainer-mob');
		$('#changescreen').removeClass('col-lg-4').addClass('col-lg-12');
		$('#cmecallcontainer').removeClass('col-lg-1').addClass('col-lg-3');
		$('#cmecallcontainer2').removeClass('col-lg-11').addClass('col-lg-9');
		$('#videocallcontainer').removeClass('col-lg-2').addClass('col-lg-4');
		$('#videocallcontainer2').removeClass('col-lg-10').addClass('col-lg-8');
		$('#cmecontent').css('padding-left', '');
		$('.compactcall').show();
		$('.expandcall').hide();
		$('.expandcmeclose').hide();
		$('.compactcmeclose').show();
		$('.full1').show();
		if(drag == true){
			chatBack('on');
		}
		
	} else {
		var drag = $('#chatVideo').is(":visible") || $('#chatAudio').is(":visible");
		// console.log("drag--"+drag);
		$('.cmeFullCompact').attr('src', 'images/cme/fullscreen_min.svg').attr('title', 'Collapse');
		$('#cmecontent').removeClass('cmecontainer-mob').removeClass('cmecontainer-min').addClass('cmecontainer');
		$('#changescreen').removeClass('col-lg-12').addClass('col-lg-4');
		$('#cmecallcontainer').removeClass('col-lg-3').addClass('col-lg-1');
		$('#cmecallcontainer2').removeClass('col-lg-9').addClass('col-lg-11');
		$('#videocallcontainer').removeClass('col-lg-4').addClass('col-lg-2');
		$('#videocallcontainer2').removeClass('col-lg-8').addClass('col-lg-10');
		$('.compactcall').hide();
		$('.expandcall').show();
		$('.expandcmeclose').show();
		$('.compactcmeclose').hide();
		$('.full1').hide();
		if(cmeinchat == '1'){
			getNewConvId(cmeinchatid);
		}else{
			if($('#cmecontent1').children().length < 1){
				$("#cmecontent2 div:first-child").trigger('onclick');
			}
			else{
				$("#cmecontent1 div:first-child").trigger('onclick');
				// $("#cmecontent1").children(":first").trigger('onclick');
			}
		}
		if(drag == true){
			chatBack('on');
		}
		/*if (pageAct == 'home' || pageAct == 'exsitingWorkSpace') { // this variable defined globally and it will get assigned with value based on the menu clicked during navigation
			$('#cmecontent').css('padding-left', '0px');
		} else {
			$('#cmecontent').css('padding-left', '');
		}*/
		//setCmeSwitchModeDiv();
	}
}
function setCmeSwitchModeDiv(){
	if(!$('#cmecallcontact').is(':hidden')){
		$('#cmeSwitchModeDiv').css('max-width','100%');
	}else{
		$('#cmeSwitchModeDiv').css('max-width','380px');
	}
}
function cmeMinMax(obj) {
	$('#cmecontent').css('padding-left', '');
	if ($(obj).attr('src').indexOf('cme_min.svg') != -1) {
		$('.cmeMinMax').attr('src', 'images/cme/cme_max.svg').attr('title', 'Maximize');
		$('#cmecontent').removeClass('cmecontainer').removeClass('cmecontainer-mob').addClass('cmecontainer-min');
		$('#cmeSwitchModeDiv').removeClass('d-none').addClass('d-flex');
		$('#changescreen').removeClass('col-lg-4').addClass('col-lg-12').hide();
		$('#cmecallcontainer').removeClass('col-lg-1').addClass('col-lg-3');
		$('#cmecallcontainer2').removeClass('col-lg-11').addClass('col-lg-9');
		$('.cmeFullCompact').hide();
	} else {
		$('.cmeMinMax').attr('src', 'images/cme/cme_min.svg').attr('title', 'Minimize');
		$('#cmeSwitchModeDiv').removeClass('d-flex').addClass('d-none');
		$('#changescreen').show();
		$('.cmeFullCompact').show().attr('src', 'images/cme/fullscreen_min.svg').attr('title', 'Collapse');;
		cmeExpandCollapse($('#cmeSwitchModeDiv').find('img.cmeFullCompact'));
	}
}

function getNewConvId(uid, obj, unreadCnt) {
	if(uid != userIdglb){
		// var conid = $('#user_'+uid).attr('cid');
		// if(conid == ' ' && conid == 'undefined' && conid == undefined){
		let jsonbody = {
			"fromUser": userIdglb,
			"toUser": uid
		}
		$.ajax({
			url: apiPath + "/" + myk + "/v1/checkConvId",
			type: "POST",
			dataType: 'json',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				timerControl("");
			},
			success: function (result) {
				// console.log(result);
				vConvId = result;
				openNewCmeChatBox(uid, obj, result, unreadCnt);
			}
		});
		if($('#cmecontent').hasClass('d-none')){
			$('#cmecontent').removeClass('d-none');
		}
	}
}

function chatMoreOptions() {
	if ($('#chatMoreOptionsDiv').is(':hidden')) {
		$('#chatMoreOptionsDiv').addClass('d-flex flex-column align-items-center');
	} else {
		$('#chatMoreOptionsDiv').removeClass('d-flex');
	}
}


function openNewCmeChatBox(uid, obj, cid, unreadCnt) {
	try {
		let UI = "";
		// $("#userId_" + uid).find(".clipboard").removeClass("clipboard");
		// $(obj).addClass("clipboard");

		// restoreValue();
		// if(addconfon == true){
		// 	return;
		// }
		// if(type !=="video"){	
		// 	var id= obj.id;
		// }else{
		// 	id= obj;
		// }
		// id=id.split("-")[0];
		// var name="";

		// if(!isNaN(id)){   // used from tasks but now not in use
		// 	name=$(obj).attr('name');
		// 	id=id+"-"+chatDomain+"-colabus-com";
		// }
		// var sid =id.split("-")[0];
		//     $('#'+id).attr('count','0');  // setting count for loading of previous messages
		// 	$('#'+id).find('.chatnotification').html('').css('visibility','visible');
		var conId = cid;
		var imgSrc = $('#userId_' + uid).find('img').attr('data-src');
		var userName = $('#userId_' + uid).find('.pname').text();
		var id = uid + "-" + cmeDomain + "-colabus-com";
		var type = $(obj).attr('type');
		console.log("type--"+type);
		if(type == undefined){
			type = "chat";
		}
		$("#cmecontactconv").children().remove();
		$("#cmecontactconv-mob").children().remove();
		$("#cmecontentconv").children().remove();
		$("#cmecontentconv-mob").children().remove();
		// console.log(type);
		if (type == "chat") {
			var chat = $(obj).attr('chat');
			UI ="<div id='"+id+"m' class=''>" +
					"<div id='chatNameHead'>" +
						"<div class='d-flex align-items-center position-relative' style='height: 49px; border-left: 1px solid grey;'>"+
							"<div class='mr-auto d-flex' style='font-size: 13px;color:#fff;'>" +
							    "<img src='"+imgSrc+"' title='"+userName+"' class=' mx-2 cursor rounded-circle float-left' style='width: 35px; height: 35px;' onerror='userImageOnErrorReplace(this);'> " +
								"<div class='uid_" + uid + " user_box_status' style='margin-left: -7px;top: 8px !important;height: 10px; width: 10px;'></div>" +
								"<div class='pl-1 pt-1' style=''> " +
									"<span class='mt-2'>"+userName+"</span>"+
									"<p class='userStatus_" + uid + " align_bottom' style='display:none;color:#08B627;font-size:11px;margin-bottom: 0px;margin-top: -2px;'>&nbsp;&nbsp;</p>"+
									"<p class='userPresStatus_"+uid+" align_bottom' style='font-size: 11px;margin-bottom: 0px;margin-top: -2px;'>&nbsp;&nbsp;</p> " +
									// "<p class='d-none' style='margin-left: 64px;font-size: 11px;'>online</p> "+
								"</div> " +
							"</div>" +
							// '<div class="dropdown">'+
							// '<img src="images/menus/more_blue.svg" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="More" class=" cursor" style="width: 22px;"> '
							'<img src="/images/menus/more.svg" class="mr-2 cursor" onclick="chatMoreOptions()" id="chatMoreOptions" title="More" class=" cursor" style="width: 22px;"> '
							+ '<div id="chatMoreOptionsDiv" class="flex-column rounded" align="center" style="display: none;margin-top: 126px;position: absolute;padding: 5px;z-index: 1;background-color: #fff;right: 30px;border: 1px solid #bfbfbf; box-shadow: 0 6px 12px rgb(0 0 0 / 18%);">'
								+ "<img onclick='startavCall(" + uid + ",\"true\",\"audio\")' class='video' title='Audio Call' style='cursor:pointer;width: 26px;height: 26px;' src='" + path + "/images/cme/callAudio_blue.svg'\>"
								+ "<img onclick='startavCall(" + uid + ",\"true\",\"video\")' class='video' title='Video Call'  style='cursor:pointer;width: 26px;height: 26px;' src='" + path + "/images/cme/callVideo_blue.svg'\>"
								// + "<img onclick='showChatFromRecomdView(\"" + id + "\")' class='video' title='Messages' style='cursor:pointer;margin-top: 3px;width: 40px;height: 40px;' src='" + path + "/images/temp/chat2.png'\>"
								+ "<img onclick='cmehighlightVideo();highlightVideo();showRecommendationsInChat(" + uid + ");' class='video' title='Recommendations' style='cursor:pointer;width: 26px;height: 26px;' src='" + path + "/images/cme/recomd_blue.svg'\>"
								// + "<img onclick='startavCall(" + id + ",\"true\",\"screenshare\")' class='video' title='Screen Share'  style='display:none;cursor:pointer;width: 38px;height: 26px;margin-top: 12px;margin-bottom: 4px;' src='" + path + "/images/temp/screenshare.png'\>"
							+ '</div>'
							+ '<div id="" class="chatMoreOptionsDivFullScreen align-items-center justify-content-between mr-2" style="display: none;">'
								+ "<img onclick='startavCall(" + uid + ",\"true\",\"audio\")' class='video mx-2' title='Audio Call' style='cursor:pointer;width: 26px;height: 26px;' src='" + path + "/images/cme/callAudio_white.svg'\>"
								+ "<img onclick='startavCall(" + uid + ",\"true\",\"video\")' class='video mx-2' title='Video Call'  style='cursor:pointer;width: 26px;height: 26px;' src='" + path + "/images/cme/callVideo_white.svg'\>"
								// + "<img onclick='showChatFromRecomdView(\"" + id + "\")' class='video' title='Messages' style='cursor:pointer;margin-top: 3px;width: 40px;height: 40px;' src='" + path + "/images/temp/chat2.png'\>"
								+ "<img onclick='cmehighlightVideo();highlightVideo();showRecommendationsInChat(" + uid + ");' class='video mx-2' title='Recommendations' style='cursor:pointer;width: 26px;height: 26px;' src='" + path + "/images/cme/recomd_white.svg'\>"
								+ '<img id="" class="cmeMinMax cmeExpandCollapse mx-2" src="images/cme/cme_min.svg" title="Minimize" onclick="cmeMinMax(this);event.stopPropagation();" style="display: none;width: 20px;height:20px;">'
								+ '<img id="" class="cmeFullCompact cmeExpandCollapse mx-2 mr-2" src="images/cme/fullscreen_min.svg" title="Collapse" onclick="cmeExpandCollapse(this);event.stopPropagation();" style="width: 20px;height:20px;">'
							
							+ '</div>'
							// 	'<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">'+
							// 		// "<img id='' class='dropdown-item cursor' onclick='backChatMessages(this)' src='images/temp/Phonegreen.png' title='Audio call' style='width: 22px;'> " +
							// 		// "<img id='' class='dropdown-item cursor' onclick='backChatMessages(this)' src='images/temp/vedioGreen.png' title='Video call' style='width: 22px;'> " +
							// 		// "<img id='' class='dropdown-item cursor' onclick='highlightVideo();showRecommendations();' src='images/temp/recomd.png' title='Recommendations' style='width: 22px;'> " +
							// 		'<a class="dropdown-item" href="#">Action</a>'+
							// 		'<a class="dropdown-item" href="#">Another action</a>'+
							// 		'<a class="dropdown-item" href="#">Something else here</a>'+
							// 	'</div>'+
							// '</div>'+
							+"<img id='backmsg' onclick='backChatMessages(this)' chat='"+chat+"' src='images/cme/leftarrow.svg' title='Messages' class='backmsg d-none mr-2 cursor' style='width: 22px;'> " +
						"</div>"+
					"</div>"+
					"<div id='chatOptionHead' class='d-none'>"+
						"<div class='d-flex justify-content-end align-items-centre' style='height: 49px;'>"
							+ "<img onclick='replyMsg()' class='mr-2'  title= 'Reply' style=\"cursor:pointer;width:22px;\"  src='images/cme/reply_white.svg'\>"
							+ "<img onclick='copyMsg()' class='mr-2' title= 'Copy' style=\"cursor:pointer;width: 22px;\" src='images/cme/copy_white.svg'\>"
							+ "<img data-toggle='modal' class='mr-4' data-target='#contactPopup' onclick='forwardMsg()' title= 'Forward' style=\"cursor:pointer;width:22px;\" src='images/cme/forward_white.svg'\>"
						+ "</div>"
					+ "</div>"
				+"</div>"
		}
		else {
			var cid = uid;
			var imgName = $(obj).find('img').attr('src');
			var gname = $(obj).attr('gname');
			var data = '';
			$.ajax({
				url: apiPath + "/" + myk + "/v1/getMUCRoomDetails?roomId=" + cid + "",
				type: "GET",
				dataType: 'json',
				contentType: "application/json",
				error: function (jqXHR, textStatus, errorThrown) {
					checkError(jqXHR, textStatus, errorThrown);
					timerControl("");
				},
				success: function (result) {
					data = result;
					var name = '';
					var uid = '';
					var imgType = '';
					var groupUser = '';
					var groupUserId = '';
					var users = '';
					var tiles = '';
					var id = data[0].g_id;

					groupUser = data[0].groupUsers;
					groupUserId = data[0].g_owner;
					for (i = 0; i < groupUser.length; i++) {
						name = groupUser[i].user_name;
						uid = groupUser[i].user_id;
						imgType = groupUser[i].sender_image_type
						// UI+="<li class='groupList' style='list-style-type:none;border-bottom: 1px solid #cccccc;padding:2% 2% 2% 7%;'>"+
						// 	"<div class='row' >" + 
						// 	"<div class='col-xs-3' style='width: 10%; height: 37px; padding-left: 8px;'> <img title='" +name+"' src='"+lighttpdPath+"/userimages/"+userid+"."+imgType+"' onerror='userImageOnErrorReplace(this);' style='height: 34px; min-height: 34px; width:34px;min-width: 34px;border-radius: 50%;'/></div>" +
						// 	"<div class='col-xs-6' style='font-family: OpenSansRegular; font-size: 14px; width: 70%; margin: 6px 6px 6px -17px;margin-left:1px' value='" +name +"'>" + name +"</div>"+
						// 	"<div>"+
						// 	"</li>";

						if (groupUserId == groupUser[i].user_id) {
							users += '<span style="color: blue;">' + name + '[GroupAdmin]</span> | ';
							tiles += name + '| ';
							// UI+="<li style='height:10%; border-bottom: 1px solid #cccccc;padding:5%;'>"+name+"<span style='float:right;'><img style='height: 15px; width: 15px; margin-top: 2px;' src='"+path+"/images/idea/myprojects.png'><span></li>";	
						} else {
							users += name + '| ';
							tiles += name + '| ';
							//  UI+="<li style='height:10%;border-bottom: 1px solid #cccccc;padding:5%;'>"+name+"</li>";
						}
					}
					$('#' + id + 'mm').find('div#gusers').html(users).attr('title', tiles);
					// $('#'+id+'mm').find('div#groupUser').html(UI);
				}
			});
			var gchat = $(obj).attr('gchat');
			UI ="<div id=\""+cid+"mm\" >" +
					"<div id='chatNameHead' class=''>" +
						"<div class='d-flex text-dark' style='height: 49px; border-left: 1px solid grey;'>" +
							"<div class='mr-auto d-flex' style='font-size: 13px;color:#fff;'>" +
								"<img src='"+imgName+"' title='"+gname+"'  onerror='groupImageOnErrorReplace(this)'; class=' mx-2 mt-1 cursor rounded-circle ' style='width: 40px;height: 40px;'> " +
								"<div class='pl-1 pt-1' style=''> " +
									"<div>"+gname+"</div>"+
									"<div id='gusers' class='defaultExceedMultilineCls' style='font-size: 13px;font-family: OpenSansRegular;-webkit-line-clamp: 1;margin-right: 2vh;'> " +
									// "<span  class='defaultExceedCls'></span>"+
									"</div> " +
								"</div> " +
							"</div>" +
							 '<div id="" class="chatMoreOptionsDivFullScreen align-items-center justify-content-between mr-2" style="display: none;">'
								+ '<img id="" class="cmeMinMax cmeExpandCollapse mx-2" src="images/cme/cme_min.svg" title="Minimize" onclick="cmeMinMax(this);event.stopPropagation();" style="display: none;width: 20px;height:20px;">'
								+ '<img id="" class="cmeFullCompact cmeExpandCollapse mx-2 mr-2" src="images/cme/fullscreen_min.svg" title="Collapse" onclick="cmeExpandCollapse(this);event.stopPropagation();" style="width: 20px;height:20px;">'
							+ '</div>'
							+"<img onclick='backGroupMessages(this)' gchat='"+gchat+"' src='images/cme/leftarrow.svg' title='Messages' class='backmsg mr-2 cursor ml-auto' style='width: 20px;'> " +
						"</div>"+
					"</div>"+
					"<div id='chatOptionHead' class='d-none'>"+
						"<div class='d-flex justify-content-end align-items-centre' style='height: 49px;'>"
							+ "<img onclick='replyMsg()' class='mr-2'  title= 'Reply' style=\"cursor:pointer;width:22px;\"  src='images/cme/reply_white.svg'\>"
							+ "<img onclick='copyMsg()' class='mr-2' title= 'Copy' style=\"cursor:pointer;width: 22px;\" src='images/cme/copy_white.svg'\>"
							+ "<img data-toggle='modal' class='mr-4' data-target='#contactPopup' onclick='forwardMsg()' title= 'Forward' style=\"cursor:pointer;width:22px;\" src='images/cme/forward_white.svg'\>"
						+ "</div>"
					+ "</div>"
				+"</div>"


		}
		if ($("#cmecontent").hasClass("cmecontainer-mob")) {
			// if(type == "chat"){
			var chat = $(obj).attr('chat');
			// if(chat == "1"){
			$('#cmegroup').addClass('d-none');
			$('#cmecontent3').addClass('d-none');
			$("#cmemessage").addClass("d-none");
			$("#cmecontent1").addClass("d-none");
			$("#cmecontact").addClass("d-none");
			$("#cmecontent2").addClass("d-none");
			$('#cmecall').addClass('d-none');
			$('#cmecontent4').addClass('d-none');
			// }
			// else{
			// $('#cmegroup').addClass('d-none');
			// $('#cmecontent3').addClass('d-none');
			// $("#cmemessage").addClass("d-none");
			// $("#cmecontent1").addClass("d-none");
			// $("#cmecontact").addClass("d-none");
			// $("#cmecontent2").addClass("d-none");
			// $('#cmecall').addClass('d-none');
			// $('#cmecontent4').addClass('d-none');
			// }
			$("#cmecontactconv-mob").removeClass("d-none");
			$("#cmecontentconv-mob").removeClass("d-none");
			$("#cmecontactconv").addClass("d-none");
			$("#cmecontentconv").addClass("d-none");
			// }
			// var gchat = $(obj).attr('gchat');
			// if(gchat == "2"){
			// 	$('#cmegroup').addClass('d-none');
			// 	$('#cmecontent3').addClass('d-none');
			// 	$("#cmemessage").addClass("d-none");
			// 	$("#cmecontent1").addClass("d-none");
			// 	$("#cmecontact").addClass("d-none");
			// 	$("#cmecontent2").addClass("d-none");

			// 	$("#cmecontactconv-mob").removeClass("d-none");
			// 	$("#cmecontentconv-mob").removeClass("d-none");
			// 	$("#cmecontactconv").addClass("d-none");
			// 	$("#cmecontentconv").addClass("d-none");
			// }


			$("#cmecontactconv-mob").append(UI);
			$('.backmsg').removeClass('d-none');
			$('#chatMoreOptions').removeClass('mr-4').addClass('mr-2');
			
			// $('#chatMoreOptionsDiv').addClass('d-flex');
			//$("#text-box").addClass("width-mob");
			//$("#gtext-box").addClass("width-mob");
			cmeinchat = '1';
			cmeinchatid = uid;
		}
		else {
			if (type == "chat") {
				// cmeinchat = '1';
				// cmeinchatid = uid;
				var chat = $(obj).attr('chat');
				if (chat == "1") {
					$("#cmemessage").removeClass("d-none");
					$("#cmecontent1").removeClass("d-none");
					$("#cmecontact").addClass("d-none");
					$("#cmecontent2").addClass("d-none");
					$('#cmegroup').addClass('d-none');
					$('#cmecontent3').addClass('d-none');
					$('#cmecall').addClass('d-none');
					$('#cmecontent4').addClass('d-none');
				}
				else {
					$("#cmemessage").addClass("d-none");
					$("#cmecontent1").addClass("d-none");
					$("#cmecontact").removeClass("d-none");
					$("#cmecontent2").removeClass("d-none");
					$('#cmegroup').addClass('d-none');
					$('#cmecontent3').addClass('d-none');
					$('#cmecall').addClass('d-none');
					$('#cmecontent4').addClass('d-none');
				}
			}
			else {
				var gchat = $(obj).attr('gchat');
				if (gchat == "1") {
					$("#cmemessage").removeClass("d-none");
					$("#cmecontent1").removeClass("d-none");
					$("#cmecontact").addClass("d-none");
					$("#cmecontent2").addClass("d-none");
					$('#cmegroup').addClass('d-none');
					$('#cmecontent3').addClass('d-none');
					$('#cmecall').addClass('d-none');
					$('#cmecontent4').addClass('d-none');
				}
				else {
					$("#cmemessage").addClass("d-none");
					$("#cmecontent1").addClass("d-none");
					$("#cmecontact").addClass("d-none");
					$("#cmecontent2").addClass("d-none");
					$('#cmegroup').removeClass('d-none');
					$('#cmecontent3').removeClass('d-none');
					$('#cmecall').addClass('d-none');
					$('#cmecontent4').addClass('d-none');
				}

			}

			$("#cmecontactconv-mob").addClass("d-none");
			$("#cmecontentconv-mob").addClass("d-none");
			$("#cmecontactconv").removeClass("d-none");
			$("#cmecontentconv").removeClass("d-none");

			$("#cmecontactconv").append(UI);
			$('.backmsg').addClass('d-none');
			$('#chatMoreOptions').removeClass('mr-2').addClass('mr-4');
			$('.chatMoreOptionsDivFullScreen').addClass('d-flex');
			$('#chatMoreOptions').hide();
			//$("#text-box").addClass("width");
			//$("#gtext-box").addClass("width");
		}

		if (type == "chat") {
			var userpres;
			if ($('#userId_' + uid).find('.user_box_status').hasClass("away")) {
				userpres = "away";
				$('.userPresStatus_' + uid).css({ "color": "orange" });
				$(".uid_" + uid).addClass("away");
			}
			else if ($('#userId_' + uid).find('.user_box_status').hasClass("user_box_status_online")) {
				userpres = "online";
				$('.userPresStatus_' + uid).css({ "color": "#08B627" });
				$(".uid_" + uid).addClass("user_box_status_online");
			} else {
				userpres = "offline";
				$('.userPresStatus_' + uid).css({ "color": "gray" });
				$(".uid_" + uid).removeClass("user_box_status_online");
			}
			$('.userPresStatus_' + uid).show().text(userpres);

			// var cid = $("#user_" + uid).attr('cid');
			// console.log("cid--"+cid);
			// var conId = getConvId(uid);
			// if(cid == '' && cid == "undefined"){
			// 	conId = getConvId(uid);
			// }
			// else{
			// 	conId = cid;
			// }
			//chat content body
			var localOffsetTime = getTimeOffset(new Date());
			let jsonbody = {
				"localTZ": localOffsetTime,
				"cid": conId,
				"user_id": uid,
				"company_id": companyIdglb,
				"index": "0",
				"limit": "20"
			}
			$.ajax({
				url: apiPath + "/" + myk + "/v1/getMsg",
				type: "POST",
				dataType: 'json',
				contentType: "application/json",
				data: JSON.stringify(jsonbody),
				error: function (jqXHR, textStatus, errorThrown) {
					checkError(jqXHR, textStatus, errorThrown);
					timerControl("");
				},
				success: function (result) {
					var jsonData = result;
					// console.log(jsonData);
					seenMsg(uid, "allseen", conId);

					restoreValue();
					prepareUICmePrevMsg(jsonData, uid, cid, unreadCnt);

				}
			});
		}
		else {
			//groupchat content body

			var cid = uid;
			// console.log(cid);
			var localOffsetTime = getTimeOffset(new Date());
			let jsonbody = {
				"group_id": cid,
				"index": "0",
				"limit": "20",
				"localTZ": localOffsetTime,
			}
			$.ajax({
				url: apiPath + "/" + myk + "/v1/fetchGroupLastMsgSet",
				type: "POST",
				dataType: 'json',
				contentType: "application/json",
				data: JSON.stringify(jsonbody),
				error: function (jqXHR, textStatus, errorThrown) {
					checkError(jqXHR, textStatus, errorThrown);
					timerControl("");
				},
				success: function (result) {
					var jsonData = result;
					// console.log(jsonData);
					prepareUICmePrevGroupMsg(jsonData, uid, cid, unreadCnt);
				}
			});


		}
	} catch (e) {
		console.log("Chat exception:" + e);
	}
}

function backChatMessages(obj) {
	var chat = $(obj).attr('chat');
	cmeinchatid = '';
	cmeinchat = '';
	if (chat == '1') {
		backMessages();
	}
	else {
		$("#cmecontact").removeClass("d-none");
		$("#cmecontent2").removeClass("d-none");
		$("#cmecontent1").addClass("d-none");
		$("#cmemessage").addClass("d-none");
		$("#cmegroup").addClass("d-none");
		$("#cmecontent3").addClass("d-none");
		$("#cmecall").addClass("d-none");
		$("#cmecontent4").addClass("d-none");
		// $("#cmecontactconv").addClass("d-none");
		// $("#cmecontentconv").addClass("d-none");
		$("#cmecontactconv-mob").addClass("d-none");
		$("#cmecontentconv-mob").addClass("d-none");
	}
}

function backGroupMessages(obj) {
	var gchat = $(obj).attr('gchat');
	if (gchat == '2') {
		$("#cmecontact").addClass("d-none");
		$("#cmecontent2").addClass("d-none");
		$("#cmecontent1").addClass("d-none");
		$("#cmemessage").addClass("d-none");
		$("#cmegroup").removeClass("d-none");
		$("#cmecontent3").removeClass("d-none");
		$("#cmecall").addClass("d-none");
		$("#cmecontent4").addClass("d-none");
		// $("#cmecontactconv").addClass("d-none");
		// $("#cmecontentconv").addClass("d-none");
		$("#cmecontactconv-mob").addClass("d-none");
		$("#cmecontentconv-mob").addClass("d-none");
	}
	else {
		backMessages();
	}
}

function prepareUICmePrevMsg(data, uid, cid, unreadCnt) {
	try{
	let UI = '';
	var d = new Date();
	var msg = '';
	var type = '';
	var created_time = '';
	var created_date = '';
	var message_status = '';
	var cm_id = '';
	var msg_action = '';
	var msg_type = '';
	var msg_fr_uid = '';
	var file_id = '';
	var file_ext = '';
	var replyName = '';
	var mainChatThread = '';
	var color = "";
	// console.log(data.length);

	UI += "<div id=\"" + uid + "m\" index='20' count='2' class='compactcme4 py-0 wsScrollBar' style='overflow-x: hidden;overflow-y: auto;'>"
	if (data.length > 19) {
		UI += "<div id='lem' class='d-flex align-items-center justify-content-center' style='background-color: #fff;text-align:center;height: 30px;margin-bottom: 10px !important;margin: 0 20px;' onclick='loadEarlierMessages(" + uid + ", this);'><a href='#' style='color: blue; font-family: helvetica;border-bottom: 1px solid blue;'>Load earlier messages</a></div>"
	}
	UI += "<div id='prepend_" + uid + "'></div>"

	for (i = data.length - 1; i >= 0; i--) {
		msg = data[i].msg;
		type = data[i].type;
		created_time = data[i].created_time;
		created_date = data[i].created_date;
		message_status = data[i].message_status;
		msg_type = data[i].msg_type;
		msg_fr_uid = data[i].msg_fr_uid;
		cm_id = data[i].cm_id;
		msg_action = data[i].msg_action;
		file_id = data[i].file_id;
		file_ext = data[i].file_ext;

		// console.log(data[i].replyThread[0].msg_fr_uid);
		if (msg_action == "reply") {
			color = generateRandomColor();
			if (data[i].replyThread == [] && data[i].replyThread[0].msg_reply_id == 0) {

			}
			else {
				if (data[i].replyThread[0].msg_fr_uid == userIdglb) {
					replyName = "You";
					color = "#50baba;";
				}
				else {
					replyName = data[i].replyThread[0].userFullName;
				}
				mainChatThread = data[i].replyThread[0].msg;
			}
		}
		imgPath = lighttpdpath + "/uploadedDocuments/" + file_id + "." + file_ext + "?" + d.getTime();
		if (uid == data[i].msg_fr_uid) {
			UI += "<div id='" + cm_id + "' class='w-50 d-flex justify-content-start ml-2 chat' style = '' >" +
				"<div class='chatId' uid='" + uid + "'>"
			if (msg_type == "file") {
				UI += "<div  onclick='replyForChat(this)' ondblclick='replyMsg()' messageType='file' from='one' msg='" + msg + "'  class='text-white px-3 py-2 mt-2 float-left chat-income-message singlebubble replyToReply '>"
				if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
					if (msg_action == "forward") {
						UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
							"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
					}
					UI += "<a path='" + imgPath + "'  ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;' class='ChatText'>" + msg + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
							+ "<div id='' class='defaultWordBreak float-left position-relative'>"
							+ "<img id=''  ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + imgPath + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				else {
					file_ext = file_ext.toLowerCase();
					if (msg_action == "forward") {
						UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
							"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
					}
					UI += "<a path='" + imgPath + "'  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;' class='ChatText'>" + msg + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
						+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
						+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
						+ "</div>"
						+ "</div>"
				}
				UI += "</div>"
			}
			else {
				UI += "<div  onclick='replyForChat(this)' from='one' ondblclick='replyMsg()' messageType='text' msg='" + msg + "'  class=' defaultWordBreak text-white px-3 py-2 mt-2 float-left chat-income-message singlebubble bubble replyToReply ' >"
				if (msg_action == "reply") {
					UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
						"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
				}
				if (msg_action == "forward") {
					UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
						"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
				}
				UI += "" + msg + "</div>"
			}
			UI += "<div class='py-1 float-left text-secondary chat-income-time' style=''><span>" + created_time + ", " + created_date + "</span></div></div></div>";
		}
		else {
			UI += "<div id='" + cm_id + "' class='mr-2 d-flex justify-content-end chat' style = 'margin-left: 50%;' >" +
				"<div class='chatId' uid='" + uid + "'>"
			if (msg_type == "file") {
				UI += "<div  onclick='replyForChat(this)' ondblclick='replyMsg()' from='one' messageType='file' msg='" + msg + "'  class='text-white px-3 py-2 float-right chat-outgoing-message singlebubble2 bubble2'>"

				if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
					if (msg_action == "forward") {
						UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
							"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
					}
					UI += "<a  path='" + imgPath + "' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  style='color:#fff;'  class='ChatText'>" + msg + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
							+ "<div id='' class='defaultWordBreak float-right position-relative'>"
							+ "<img id='' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;margin-right: 30px;' src='" + imgPath + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				else {
					file_ext = file_ext.toLowerCase();
					if (msg_action == "forward") {
						UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
							"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
					}
					UI += "<a  path='" + imgPath + "' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\"  style='color:#fff;'  class='ChatText'>" + msg + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "'>"
							+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
							+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				UI += "</div>"
			}
			else {
				UI += "<div  onclick='replyForChat(this)' from='one' ondblclick='replyMsg()' messageType='text' msg='" + msg + "'  class=' defaultWordBreak text-white px-3 py-2 float-right chat-outgoing-message singlebubble2 bubble2 '>"
				if (msg_action == "reply") {
					//var color = generateRandomColor();
					UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
						"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
				}
				if (msg_action == "forward") {
					UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
						"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
				}
				UI += "" + msg + "</div>"
			}
			UI += "<div class='py-1 float-right text-secondary chat-outgoing-time' style=''><span>" + created_time + ", " + created_date + "</span>";
			if (message_status == "sent") {
				UI += "<span class='mr-1 chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div></div>";
			} else if (message_status == "seen") {
				UI += "<span class='mr-1 chat-outgoing-status chat-outgoing-status-seen'><i> Read</i></span></div></div></div>";
			} else if (message_status == "delivered") {
				UI += "<span class='mr-1 chat-outgoing-status chat-outgoing-status-deliver'><i> Delivered</i></span></div></div></div>";
			}
		}
	}
	UI+="<div id='uid_"+uid+"'></div> "+
		"</div>"+
		"<div  id='text-box' style='' class='form-group mb-0'>"+
			"<div id ='' class='px-2 pt-1 pb-1 m-0 border border-left-0 border-right-0 border-bottom-0' style='width: 99%;'>"+
				"<textarea id='msgtext' class='cmetextarea rounded' uid='"+uid+"' cid='"+cid+"' type='text' style='font-size:14px;resize: none;width: 100%;background-image: none;' onpaste ='onPaste(event,this);' onkeypress='sendingMsg(event,this);commentAutoGrow(event,this,\"chat\");' onkeyup='commentAutoGrow(event,this,\"chat\");'   onblur ='commentAutoGrow(event,this,\"chat\");' oninput ='commentAutoGrow(event,this,\"chat\");' placeholder='Type message here...' ></textarea>"+
			"</div>"+
			'<div class="col-12 pt-1  px-1 d-flex justify-content-between mt-0">'+
				'<div class="d-flex justify-content-between ">'+
					'<div class="py-1 px-2 voiceFeed convoSubIcons " onclick="">'+
						'<img src="/images/conversation/mic.svg" class="image-fluid" title="Record audio" style="height:22px;cursor:pointer;">'+
					'</div>'+
					'<div class="py-1 px-2 position-relative convoSubIcons ">'+
						'<img src="/images/conversation/attach.svg" class="image-fluid " style="height:20px;cursor:pointer;" title="Upload" onclick="">'+
					'</div>'+
					'<div class="py-1 px-2 convoSubIcons ">'+
						'<img src="/images/conversation/image.svg" class="image-fluid" title="From Gallery" style="height:20px;cursor:pointer;">'+
					'</div>'+
					'<div class="py-1 px-2 convoSubIcons ">'+
						'<img src="/images/conversation/cam.svg" onclick="" class="image-fluid" title="Camera" style="height:20px;cursor:pointer;">'+
					'</div>'+
					'<div class="py-1 px-2 convoSubIcons" onmouseover="changeSmileyellow(\'main\');" onmouseout="changeSmilewhite(\'main\');">'+
						'<img src="/images/conversation/smile.svg" id="smilePop" title="" class="image-fluid" style="height:22px;cursor:pointer;">'+
					'</div>'+
					'<div class="py-1 px-2 convoSubIcons">'+
						'<img src="/images/conversation/at.svg" id="" title="" class="image-fluid atImgExt" style="height:20px;cursor:pointer;" onclick="">'+
					'</div>'+
					'<div class="py-1 px-2 convoSubIcons">'+
						'<img src="/images/conversation/hash.svg" id="" title="" class="image-fluid hashImgExt" style="height:19px;cursor:pointer;" onclick="">'+
					'</div>'+
				'</div>'+
				'<div class="d-flex justify-content-between">'+
					'<div class="p-1 mx-2">'+
						'<img class="chatpost" src="/images/conversation/post1.svg" title="Post" onclick="sendingMsg(event,this)" style="height:26px;cursor:pointer;">'+
						'<input type="hidden" id="chatType" value="chat" />'+
					'</div>'+
				'</div>'+
			'</div>'+
		"</div>"

	if ($("#changescreen").hasClass("col-lg-12")) {
		$("#cmecontentconv-mob").append(UI);
		cmeinchat = '1';
		cmeinchatid = uid;
	}
	else {
		$("#cmecontentconv").append(UI);
	}
	fileSharing(uid + 'm');  // Function call to initiate file sharing.
	$("#" + uid + "m").animate({ scrollTop: 20000 }, 'normal');
	$('#msgtext').focus();
	if (unreadCnt > 0) {
		updateSeenMessages(cid, uid, "chat");
	}
} catch (e) {
	console.log("Chat exception:" + e);
}
}

function prepareUICmePrevGroupMsg(data, uid, cid, unreadCnt) {
	let UI = '';
	// console.log("uid--"+uid+"cid--"+cid);
	var d = new Date();
	var sender_image_type = '';
	var sender = '';
	var msg = '';
	var g_id = '';
	var sender_name = '';
	var msg_reply_id = '';
	var created_time = '';
	var created_date = '';
	var msg_type = '';
	var filePathNew = '';
	var file_id = '';
	var file_ext = '';
	var msg_action = '';
	var replyName = '';
	var mainChatThread = '';
	var color = "";
	// console.log(data.length);

	UI += "<div id='" + cid + "m' index='20' count='2' class='compactcme4 wsScrollBar bg-white' style='overflow-x: hidden;overflow-y: auto;'>"
	if (data.length > 19) {
		UI += "<div id='lem'  class='d-flex align-items-center justify-content-center' style='background-color: #fff;text-align:center;height: 30px;margin-bottom: 10px !important;margin: 0 20px;' onclick='loadEarlierGrpMessages(" + cid + ", this);'><a href='#' style='color: blue; font-family: helvetica;border-bottom: 1px solid blue;'>Load earlier messages</a></div>"
	}
	UI += "<div class='container'>" +
			"<div id='prepend_" + cid + "'></div>"
	for (i = data.length - 1; i >= 0; i--) {
		sender_image_type = data[i].sender_image_type;
		sender = data[i].sender;
		msg = data[i].msg;
		g_id = data[i].g_id;
		sender_name = data[i].sender_name;
		msg_reply_id = data[i].msg_reply_id;
		created_time = data[i].created_time;
		created_date = data[i].created_date;
		msg_type = data[i].msg_type;
		filePathNew = data[i].filePathNew;
		file_id = data[i].file_id;
		file_ext = data[i].file_ext;
		g_msg_id = data[i].g_msg_id;
		msg_action = data[i].msg_action;
		if (msg_action == "reply") {
			color = generateRandomColor();
			if (data[i].replyThread == [] && data[i].replyThread[0].msg_reply_id == 0) {
			}
			else {
				if (data[i].replyThread[0].sender == userIdglb) {
					replyName = "You";
					color = "#50baba;";
				}
				else {
					replyName = data[i].replyThread[0].userFullName;
				}
				mainChatThread = data[i].replyThread[0].msg;
			}
		}
		if (userIdglb != sender) {
			imgSrc = $('#userId_' + sender).find('img').attr('data-src');
			UI += "<div id='" + g_msg_id + "' class='media py-1 chat' style='display: -webkit-box;'>" +
					"<img src='" + imgSrc + "' class='align-self-start mt-4 mr-3 rounded-circle' title='" + sender_name + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
					"<div class='media-body chatId' uid='" + sender + "''>" +
						"<div style='font-size: 13px;color: gray;'>" + sender_name + "</div>"
			if (msg_type == "file") {
				UI += "<div onclick='replyForChat(this)' ondblclick='replyMsg()' from='group' messageType='file' msg='" + msg + "' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;'>"
				if (msg_action == "forward") {
					UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
							"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
				}
				if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
					UI += "<a target='_blank' path='" + filePathNew + "' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" class='ChatText' style='color:#fff;'>" + msg + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
							+ "<div id='' class='defaultWordBreak float-left position-relative'>"
								+ "<img id=''  ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + filePathNew + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				else {
					file_ext = file_ext.toLowerCase();
					UI += "<a target='_blank' path='" + filePathNew + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + msg + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
							+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
								+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				UI += "</div>"
			}
			else {
				UI += "<div onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='text' msg='" + msg + "' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;word-break: break-word;'>"
				if (msg_action == "forward") {
					UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
						"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
				}
				if (msg_action == "reply") {
					UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
						"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
				}
				UI += "" + msg + "</div>"
			}
			UI += "<div class='chat-income-time float-left p-0 pt-1'>" + created_time + ", " + created_date + "</div>" +
				"</div>" +
			"</div>"
		}
		else {
			var imgName = lighttpdpath + "/userimages/" + userIdglb + "." + sender_image_type + "?" + d.getTime();
			UI += "<div id='" + g_msg_id + "' class='media py-1 chat' style='display: -webkit-box;'>" +
					"<div class='media-body mt-2 chatId' uid='" + userIdglb + "'>"
			if (msg_type == "file") {
				UI += "<div  onclick='replyForChat(this)' ondblclick='replyMsg()' from='group' messageType='file' msg='" + msg + "' class=' defaultWordBreak chat-income-message singlebubble bubble float-right' style='margin-left: 30%;background-color: #007b97;'>"
				if (msg_action == "forward") {
					UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
						"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
				}
				if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
					UI += "<a target='_blank' path='" + filePathNew + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;'>" + msg + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
							+ "<div id='' class='defaultWordBreak float-right position-relative'>"
								+ "<img id='' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1 mr-0' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + filePathNew + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"

				}
				else {
					file_ext = file_ext.toLowerCase();
					UI += "<a target='_blank' path='" + filePathNew + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + msg + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "'>"
							+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
								+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				UI += "</div>"
			}
			else {
				UI += "<div onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='text' msg='" + msg + "' class='px-3 py-2 defaultWordBreak bubble2 float-right chat-outgoing-message singlebubble2 bubble2' style='margin-left: 30%;background-color: #007b97;word-break: break-word;'>"
				if (msg_action == "forward") {
					UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
							"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
				}
				if (msg_action == "reply") {

					UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
							"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
				}
				UI += "" + msg + "</div>"
			}

			UI += "<div class='float-right mt-1' style='text-align: right;width: 100%;font-size: 11px;color: gray;'>" + created_time + ", " + created_date + "</div>" +
				"</div>" +
				"<img src='" + imgName + "' class='align-self-start ml-3 mt-2 rounded-circle' title='" + sender_name + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px;'>" +
			"</div>"
		}
	}
	UI+=	"<div id='guid_"+cid+"'></div>"+
				"</div>"+
			"</div>"+
			"<div  id='gtext-box' style='' class='form-group bg-white mb-0'>"+
				"<div id ='' class='px-2 pt-2 pb-1 m-0  border border-left-0 border-right-0 border-bottom-0' style='width: 99%;'>"+
					"<textarea id='groupmsgtext'  class='cmetextarea rounded' gid='"+cid+"' onpaste ='onPaste(event,this);' onkeypress='sendingGroupMsg(event,this,\"groupChat\");commentAutoGrow(event,this,\"chat\");' onkeyup='commentAutoGrow(event,this,\"chat\");'   onblur ='commentAutoGrow(event,this,\"chat\");' oninput ='commentAutoGrow(event,this,\"chat\");' type='text' style='font-size:14px;resize: none;'  placeholder='Type message here...'></textarea>"+
				"</div>"+
				'<div class="col-12  px-1 d-flex justify-content-between mt-0">'+
					'<div class="d-flex justify-content-between mt-1">'+
						'<div class="py-1 px-2  convoSubIcons " onclick="">'+
							'<img src="/images/conversation/mic.svg" class="image-fluid" title="Record audio" style="height:22px;cursor:pointer;">'+
						'</div>'+
						'<div class="py-1 px-2 position-relative convoSubIcons ">'+
							'<img src="/images/conversation/attach.svg" class="image-fluid " style="height:20px;cursor:pointer;" title="Upload" onclick="">'+
						'</div>'+
						'<div class="py-1 px-2 convoSubIcons ">'+
							'<img src="/images/conversation/image.svg" class="image-fluid" title="From Gallery" style="height:20px;cursor:pointer;">'+
						'</div>'+
						'<div class="py-1 px-2 convoSubIcons ">'+
							'<img src="/images/conversation/cam.svg" onclick="" class="image-fluid" title="Camera" style="height:20px;cursor:pointer;">'+
						'</div>'+
						'<div class="py-1 px-2 convoSubIcons" onmouseover="changeSmileyellow(\'main\');" onmouseout="changeSmilewhite(\'main\');">'+
							'<img src="/images/conversation/smile.svg" id="" title="" class="image-fluid" style="height:22px;cursor:pointer;">'+
						'</div>'+
						'<div class="py-1 px-2 convoSubIcons">'+
							'<img src="/images/conversation/at.svg" id="" title="" class="image-fluid atImgExt" style="height:20px;cursor:pointer;" onclick="">'+
						'</div>'+
						'<div class="py-1 px-2 convoSubIcons">'+
							'<img src="/images/conversation/hash.svg" id="" title="" class="image-fluid hashImgExt" style="height:19px;cursor:pointer;" onclick="">'+
						'</div>'+
					'</div>'+
					'<div class="d-flex justify-content-between">'+
						'<div class="p-1 mx-2">'+
							'<img class="chatpost" src="/images/conversation/post1.svg" title="Post" onclick="sendingGroupMsg(event,this)" style="height:26px;cursor:pointer;">'+
							'<input type="hidden" id="chatType" value="groupchat" />'+
						'</div>'+
					'</div>'+
				'</div>'+
			"</div>"

	if ($("#changescreen").hasClass("col-lg-12")) {
		$("#cmecontentconv-mob").append(UI);
	}
	else {
		$("#cmecontentconv").append(UI);
	}
	fileSharing(g_id + 'm');  // Function call to initiate file sharing.
	$('#' + g_id + 'm').animate({ scrollTop: 20000 }, 'normal');
	$('#groupmsgtext').focus();
	if (unreadCnt > 0) {
		updateSeenMessages(g_id, uid, "groupchat");
	}
}

/*Codes for file sharing Drag & drop */
function fileSharing(id) {
	var obj = $('#' + id);
	obj.on('dragenter', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
	obj.on('dragover', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
	obj.on('drop', function (e) {
		e.preventDefault();
		var files = e.originalEvent.dataTransfer.files;
		handleFileUpload(files, obj, id);
	});
	$(document).on('dragenter', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
	$(document).on('dragover', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
	$(document).on('drop', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
}

function handleFileUpload(files, obj, id) {
	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();
	for (var i = 0; i < files.length; i++) {
		if (window.File && window.FileReader && window.FileList && window.Blob) {
			sendFileToServer(files[i], obj, id);
		}
		else {
			alertFun('Browser does not support File APIs ', 'warning');
		}
	}
}

function sendFileToServer(obj, Id, id) {
	var name = obj.name;
	var size = obj.size;
	//var extn = name.split(".")[1];
	//var file_name = name.split(".")[0];
	var extn = name.substring(name.lastIndexOf(".") + 1, name.length);
	var file_name = name.substring(0, name.lastIndexOf("."));
	var uid = id.split('m')[0];
	//alert(Name+"---------"+obj.name+"-----"+repoPath);
	if (size > 10000000) {
		parent.alertFun('Upload failed. File size exceeded', 'warning');
		return false;
	} else {
		var chatType = $('#chatType').val();
		if($('#cmecallcontainer').is(':visible') && callId != "" && callId != undefined){
			// console.log('call dd');
			// var conid = $('#msgtext').attr('cid');
			chatType = 'call';
			var fd = new FormData();
			fd.append('file', obj);
			fd.append('place', "ChatFileSharing");
			fd.append('user_id', userIdglb);
			fd.append('company_id', companyIdglb);
			fd.append('resourceId', "0");
			fd.append('convotype', "N");
			fd.append('menuTypeId', callId);
			fd.append('subMenuType', "chat");
			sumbitDragDrop(fd, Id, extn, file_name, id, chatType);
		}
		else if (chatType == "chat") {
			// console.log('chat dd');
			var conid = $('#msgtext').attr('cid');
			var fd = new FormData();
			fd.append('file', obj);
			fd.append('place', "ChatFileSharing");
			fd.append('user_id', userIdglb);
			fd.append('company_id', companyIdglb);
			fd.append('resourceId', conid);
			fd.append('convotype', "N");
			fd.append('menuTypeId', "");
			fd.append('subMenuType', "chat");
			sumbitDragDrop(fd, Id, extn, file_name, id, chatType);
		} else {
			var conid = uid;
			var d = new Date();
			var m = d.getMonth();
			var time = d.getHours() + ":" + d.getMinutes() + "###" + d.getDate() + "-" + (Number(m) + 1) + "-" + d.getFullYear();
			var timeZone = getTimeOffset(d);
			var fd = new FormData();
			fd.append('file', obj);
			fd.append('place', "ChatFileSharing");
			fd.append('user_id', userIdglb);
			fd.append('company_id', companyIdglb);
			fd.append('resourceId', conid);
			fd.append('convotype', "N");
			fd.append('menuTypeId', "");
			fd.append('subMenuType', "groupChat");
			fd.append('time', time);
			fd.append('timeZone', timeZone);
			sumbitDragDrop(fd, Id, extn, file_name, id, chatType);
		}
	}
}

function getImageType(extn) {
	var img = 'chat_doc.png';
	extn = extn.toLowerCase();
	if (extn.toLowerCase() == 'png' || extn.toLowerCase() == 'jpeg' || extn.toLowerCase() == 'jpg') {
		img = "chat_img.png";
	} else if (extn.toLowerCase() == 'se') {
		img = "chat_mp3.png";
	} else if (extn.toLowerCase() == 'mp4' || extn.toLowerCase() == 'flv' || extn.toLowerCase() == 'avi') {
		img = "chat_vid.png";
	}
	return img;
}
/*
 * This function is to send the file to repository
 */
function sumbitDragDrop(formData, Id, extn, file_name, id, chatType) {
	if(chatType == "call"){
		// parent.$("#loadingBar").show();
		// parent.timerControl("start");
		var uid = id.split('m')[0];
		// var uid = $(Id).attr('id').split('-')[0];
		var c_jid = uid + "@" + cmeDomain + ".colabus.com";
		var img = getImageType(extn);
		var today = new Date();
		var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
		var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		var dateTime = date + '_' + time;
		// alert(userId);
		var status = 'N';
		// console.log("c_jid--" + c_jid + "uid--" + uid + "Id--" + Id + "extn--" + extn + "file_name--" + file_name + "id--" + id + "formData--" + formData);
		// if ($('#' + uid + '-' + cmeDomain + '-colabus-com').find('div.user_box_status').hasClass('user_box_status_online')) {
		// 	status = 'N';
		// }

		$.ajax({
			url: apiPath + "/" + myk + "/v1/upload",
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			dataType: 'text',
			data: formData,
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				// $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) {
				// console.log('result')
				var msgId = result.split("@@")[1];
				result = result.split("@@")[0];
				var splitting = result.split('//')[3];
				var file_id = splitting.split('.')[0];
				var file_ext = splitting.split('.')[1];
				// console.log('msgId--'+msgId+'result--'+result+'splitting--'+splitting+'file_id--'+file_id+'file_ext--'+file_ext);

				let UI = "<div id='" + msgId + "' class='mr-2 d-flex justify-content-end chat' style = 'margin-left: 50%;' >" +
							"<div class='chatId' uid='" + uid + "'>"
				UI += "<div onclick='' ondblclick='replyMsg()' from='one' messageType='file' msg='" + file_name + '.' + file_ext + "'   class='text-white px-3 py-2 float-right chat-outgoing-message singlebubble2 bubble2'>"
				if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;'>" + file_name + '.' + file_ext + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
							+ "<div id='' class='defaultWordBreak float-right position-relative'>"
								+ "<img id='uploadedImage' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;margin-right: 30px;' src='" + result + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				else {
					file_ext = file_ext.toLowerCase();
					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + file_name + '.' + file_ext + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "'>"
							+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
								+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				UI += 	"</div>"
					+ "<div class='py-1 float-right text-secondary chat-outgoing-time' style=''><span>" + getTimeIn12Format(new Date()) + "</span><span class='mr-1 chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div></div>"


				$("#updatecallmsg").append(UI);
				$('#updatecallmsg').animate({ scrollTop: 20000 }, 'normal');
				$("#videoChatText").val('');
				// $("#uid_" + uid).append(UI);
				// $("#" + uid + "m").animate({ scrollTop: 20000 }, 'normal');
				// cmeMessages();
				// $(Id).find('#chat-dialog').append(

				// 		        "<div id = "+msgId+" class='chat-outgoing-overall row'>"+
				// 			    "<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>"+ 
				//    				"<img src='"+lighttpdPath+"/userimages/"+userId+"."+userImgType+"?"+imgTime+"'  onerror='userImageOnErrorReplace(this);' style='float:right;width:40px; height:38px; border-radius: 50%;margin-left:-2.3vh; margin-top: 1vh;margin-bottom:2vh;'/></div>"+ 
				//    				"<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right;'>"+
				//    				"<div class='chat-outgoing-message singlebubble2 bubble2 row'>"+
				// 			    "<a target='_blank' href='"+result+"' style='color:#fff;'>"+file_name+'.'+extn+"</a>"+ 
				//    			    "</div>"+ 
				//    				"<div class='chat-outgoing-time row'><span>"+getTimeIn12Format(new Date())+"</span><span class='chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div>");
				if (file_name.indexOf('&') > 0) {

					file_name = file_name.replace("&", "CH(38)");
				}
				body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = '" + file_name + '.' + extn + "' value='" + result + "'><filesharing /></href>";
				// if($("#videoContent").is(":hidden")){    
				var jsonText = '{"calltype":"' + callType + '","type":"videochat","callid":"' + callId + '","msg":"' + body + '","msgid":"' + msgId + '"}';
				var message = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);; // preapring the chat stanza to send		 

				// var message = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "chat", "msgid": msgId }).c('body').t(body).up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
				Gab.connection.send(message);
				//  }
				//  else{
				//     var jsonText = '{"calltype":"'+callType+'","type":"videochat","msg":"'+body+'","callid":"'+callId+'"}';
				//     var message = $msg({to : jid, "type" : "normal" }).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
				//  	serverConnection.send(message);
				//  }  

				var d = new Date();
				var time1 = d.getHours() + ":" + d.getMinutes();
				const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

				// if($('#conversationList').is(':visible')){
				// 	$("#conversationList ul ").find("#coversationContactTime_"+uid).html(msgtime);
				/*if(type == "filesharing"){
					$("#conversationContactMsg_"+Id).html(filename);
					
				}else{
					$("#conversationContactMsg_"+Id).html(body);
				}*/
				// var fname = file_name+'.'+extn;
				// $("#conversationContactMsg_"+uid).html(fname);
				// $("#conversationContactMsg1_"+uid).hide();
				// $("#conversationContactMsg_"+uid).show();

			}
			// var newjid = uid+'-'+chatDomain+'-colabus-com';
			// var newchatUi = $("#conversationList ul").find("#" + newjid).clone();
			// $("#conversationList ul").find("#" + newjid).remove();
			// $("#conversationList ul").prepend(newchatUi);

			// $(Id).find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
			// }  
		});
	}
	else if (chatType == "chat") {
		// parent.$("#loadingBar").show();
		// parent.timerControl("start");
		var uid = id.split('m')[0];
		// var uid = $(Id).attr('id').split('-')[0];
		var c_jid = uid + "@" + cmeDomain + ".colabus.com";
		var img = getImageType(extn);
		var today = new Date();
		var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
		var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		var dateTime = date + '_' + time;
		// alert(userId);
		var status = 'N';
		// console.log("c_jid--" + c_jid + "uid--" + uid + "Id--" + Id + "extn--" + extn + "file_name--" + file_name + "id--" + id + "formData--" + formData);
		// if ($('#' + uid + '-' + cmeDomain + '-colabus-com').find('div.user_box_status').hasClass('user_box_status_online')) {
		// 	status = 'N';
		// }

		$.ajax({
			url: apiPath + "/" + myk + "/v1/upload",
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			dataType: 'text',
			data: formData,
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				// $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) {
				// console.log('result--'+result);
				var msgId = result.split("@@")[1];
				result = result.split("@@")[0];
				var splitting = result.split('//')[3];
				var file_id = splitting.split('.')[0];
				var file_ext = splitting.split('.')[1];
				// console.log('msgId--'+msgId+'result--'+result+'splitting--'+splitting+'file_id--'+file_id+'file_ext--'+file_ext);
				let UI = "<div id='" + msgId + "' class='mr-2 d-flex justify-content-end chat' style = 'margin-left: 50%;' >" +
							"<div class='chatId' uid='" + uid + "'>"
				UI += "<div onclick='replyForChat(this)' ondblclick='replyMsg()' from='one' messageType='file' msg='" + file_name + '.' + file_ext + "'   class='text-white px-3 py-2 float-right chat-outgoing-message singlebubble2 bubble2'>"
				if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;'>" + file_name + '.' + file_ext + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
							+ "<div id='' class='defaultWordBreak float-right position-relative'>"
								+ "<img id='uploadedImage' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;margin-right: 30px;' src='" + result + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				else {
					file_ext = file_ext.toLowerCase();
					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + file_name + '.' + file_ext + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "'>"
							+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
								+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				UI += 	"</div>"
					+ "<div class='py-1 float-right text-secondary chat-outgoing-time' style=''><span>" + getTimeIn12Format(new Date()) + "</span><span class='mr-1 chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div></div>"

				$("#uid_" + uid).append(UI);
				$("#" + uid + "m").animate({ scrollTop: 20000 }, 'normal');
				cmeMessages();
				// $(Id).find('#chat-dialog').append(

				// 		        "<div id = "+msgId+" class='chat-outgoing-overall row'>"+
				// 			    "<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>"+ 
				//    				"<img src='"+lighttpdPath+"/userimages/"+userId+"."+userImgType+"?"+imgTime+"'  onerror='userImageOnErrorReplace(this);' style='float:right;width:40px; height:38px; border-radius: 50%;margin-left:-2.3vh; margin-top: 1vh;margin-bottom:2vh;'/></div>"+ 
				//    				"<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right;'>"+
				//    				"<div class='chat-outgoing-message singlebubble2 bubble2 row'>"+
				// 			    "<a target='_blank' href='"+result+"' style='color:#fff;'>"+file_name+'.'+extn+"</a>"+ 
				//    			    "</div>"+ 
				//    				"<div class='chat-outgoing-time row'><span>"+getTimeIn12Format(new Date())+"</span><span class='chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div>");
				if (file_name.indexOf('&') > 0) {

					file_name = file_name.replace("&", "CH(38)");
				}
				body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = '" + file_name + '.' + extn + "' value='" + result + "'><filesharing /></href>";
				// if($("#videoContent").is(":hidden")){       
				var message = $msg({ to: c_jid, "type": "chat", "msgid": msgId }).c('body').t(body).up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
				Gab.connection.send(message);
				//  }
				//  else{
				//     var jsonText = '{"calltype":"'+callType+'","type":"videochat","msg":"'+body+'","callid":"'+callId+'"}';
				//     var message = $msg({to : jid, "type" : "normal" }).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
				//  	serverConnection.send(message);
				//  }  

				var d = new Date();
				var time1 = d.getHours() + ":" + d.getMinutes();
				const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

				// if($('#conversationList').is(':visible')){
				// 	$("#conversationList ul ").find("#coversationContactTime_"+uid).html(msgtime);
				/*if(type == "filesharing"){
					$("#conversationContactMsg_"+Id).html(filename);
					
				}else{
					$("#conversationContactMsg_"+Id).html(body);
				}*/
				// var fname = file_name+'.'+extn;
				// $("#conversationContactMsg_"+uid).html(fname);
				// $("#conversationContactMsg1_"+uid).hide();
				// $("#conversationContactMsg_"+uid).show();

			}
			// var newjid = uid+'-'+chatDomain+'-colabus-com';
			// var newchatUi = $("#conversationList ul").find("#" + newjid).clone();
			// $("#conversationList ul").find("#" + newjid).remove();
			// $("#conversationList ul").prepend(newchatUi);

			// $(Id).find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
			// }  
		});
		$("#loadingBar").hide();
		timerControl("");
	}
	else {
		var gid = id.split('m')[0];
		var c_jid = gid + "@mucr." + cmeDomain + ".colabus.com";
		var img = getImageType(extn);
		var status = 'N';
		var d = new Date();
		var time1 = d.getHours() + ":" + d.getMinutes();
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();
		var imgName = lighttpdpath + "/userimages/" + userIdglb + "." + userImgType + "?" + d.getTime();

		$.ajax({
			url: apiPath + "/" + myk + "/v1/upload",
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			dataType: 'text',
			data: formData,
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				// $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) {
				var msgId = result.split("@@")[1];
				result = result.split("@@")[0];
				var splitting = result.split('//')[3];
				var file_id = splitting.split('.')[0];
				var file_ext = splitting.split('.')[1];

				let UI = "<div id = " + msgId + " class='media py-1 chat' style='display: -webkit-box;'>" +
							"<div class='media-body mt-2 chatId' uid='" + userIdglb + "'>"

				UI += "<div  onclick='replyForChat(this)' ondblclick='replyMsg()' from='group' messageType='file' msg='" + file_name + '.' + file_ext + "' class=' defaultWordBreak chat-income-message singlebubble bubble float-right' style='margin-left: 30%;background-color: #007b97;'>"
				if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;'>" + file_name + '.' + file_ext + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
							+ "<div id='' class='defaultWordBreak float-right position-relative'>"
								+ "<img id='uploadedImage' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1 mr-0' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + result + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				else {
					file_ext = file_ext.toLowerCase();
					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + file_name + '.' + file_ext + "</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "'>"
							+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
								+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				UI += "</div>" +
					"<div class='float-right mt-1' style='text-align: right;width: 100%;font-size: 11px;color: gray;'>" + getTimeIn12Format(new Date()) + "<i>  Sent</i></div>" +
					"</div>" +
					"<img src='" + imgName + "' class='align-self-start ml-3 mt-2 rounded-circle' title='" + userFullname + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px;'>" +
					"</div>"
				
				$("#guid_" + gid).append(UI);
				$("#" + gid + "m").animate({ scrollTop: 20000 }, 'normal');
				cmeMessages();
				if (file_name.indexOf('&') > 0) {
					file_name = file_name.replace("&", "CH(38)");
				}
				body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = '" + file_name + '.' + file_ext + "' value='" + result + "'><filesharing /></href>";
				// if($("#videoContent").is(":hidden")){       
				var message = $msg({ to: c_jid, "type": "groupchat", "msgid": msgId }).c('body').t(body).up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
				Gab.connection.send(message);
				//  }
				//  else{
				//     var jsonText = '{"calltype":"'+callType+'","type":"videochat","msg":"'+body+'","callid":"'+callId+'"}';
				//     var message = $msg({to : jid, "type" : "normal" }).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
				//  	serverConnection.send(message);
				//  }  
			}
		});
	}
}
/*
 * @Method is Used to load Earlier messages
 */
function loadEarlierMessages(uid, obj) {
	// var Id = $(obj).parent().parent().attr('id');
	// Id = Id.substring(0,Id.length-1);
	// var conId =  $(obj).parent().parent().attr('conId');
	// var conId = $("#user_"+uid).attr("cid");
	getNextPreviousMessages(uid);
}

/*
 * This method Is to load the previous messages after loading of 20 messages beacuse
 * its difficult to append messages So I am using one more function.
 */
function getNextPreviousMessages(uid) {
	var data;
	var index = $("#" + uid + "m").attr('index');
	// var imgType = $("#"+id).attr('imgType');
	var count = $("#" + uid + "m").attr('count');
	// count = parseInt(count);
	// var $Id= $("#"+id+"m").find('#chat-dialog');
	// $Id.children('#lem').remove();
	var cid = $("#user_" + uid).attr('cid');
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var d = new Date();
	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"localTZ": localOffsetTime,
		"cid": cid,
		"user_id": uid,
		"company_id": companyIdglb,
		"index": index,
		"limit": "20"
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getMsg",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			if (result == 'No latest msg') {

			} else {
				data = result;
				let UI = '';
				var msg = '';
				var type = '';
				var created_time = '';
				var created_date = '';
				var message_status = '';
				var cm_id = '';
				var msg_action = '';
				var msg_type = '';
				var msg_fr_uid = '';
				var file_id = '';
				var file_ext = '';
				var replyName = '';
				var mainChatThread = '';
				var color = "";
				for (i = data.length - 1; i >= 0; i--) {
					msg = data[i].msg;
					type = data[i].type;
					created_time = data[i].created_time;
					created_date = data[i].created_date;
					message_status = data[i].message_status;
					msg_type = data[i].msg_type;
					msg_fr_uid = data[i].msg_fr_uid;
					cm_id = data[i].cm_id;
					msg_action = data[i].msg_action;
					file_id = data[i].file_id;
					file_ext = data[i].file_ext;
					if (msg_action == "reply") {
						color = generateRandomColor();
						if (data[i].replyThread == [] && data[i].replyThread[0].msg_reply_id == 0) {
						}
						else {
							if (data[i].replyThread[0].msg_fr_uid == userIdglb) {
								replyName = "You";
								color = "#50baba;";
							}
							else {
								replyName = data[i].replyThread[0].userFullName;
							}
							mainChatThread = data[i].replyThread[0].msg;
						}
					}
					imgPath = lighttpdpath + "/uploadedDocuments/" + file_id + "." + file_ext + "?" + d.getTime();
					if (uid == data[i].msg_fr_uid) {
						UI += "<div id='" + cm_id + "' class='w-50 ml-2 d-flex justify-content-start chat' style = '' >" +
								"<div class='chatId' uid='" + uid + "'>"
						if (msg_type == "file") {
							UI += "<div  onclick='replyForChat(this)' from='one' ondblclick='replyMsg()' messageType='file' msg='" + msg + "'  class='text-white px-3 py-2 mt-2float-left chat-income-message singlebubble replyToReply '>"
							if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
								UI += "<a  path='" + imgPath + "' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;' class='ChatText'>" + msg + "</a>" +
									"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
										+ "<div id='' class='defaultWordBreak float-left position-relative'>"
											+ "<img id=''  ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + imgPath + "' class='downCalIcon '>"
										+ "</div>"
									+ "</div>"
							}
							else {
								file_ext = file_ext.toLowerCase();
								UI += "<a  path='" + imgPath + "'  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;' class='ChatText'>" + msg + "</a>" +
									"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
										+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
											+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
										+ "</div>"
									+ "</div>"
							}
							UI += "</div>"
						}
						else {
							UI += "<div  onclick='replyForChat(this)' from='one' ondblclick='replyMsg()' messageType='text' msg='" + msg + "'  class=' defaultWordBreak text-white px-3 py-2 mt-2 ml-4 float-left chat-income-message singlebubble bubble replyToReply ' >"
							if (msg_action == "reply") {
								UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
									"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
							}
							if (msg_action == "forward") {
								UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
										"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
							}
							UI += "" + msg + "</div>"
						}
						UI += "<div class='py-1 float-left text-secondary chat-income-time' style=''><span>" + created_time + ", " + created_date + "</span></div></div></div>";



					}
					else {
						UI += "<div id='" + cm_id + "' class='mr-2 d-flex justify-content-end chat' style = 'margin-left: 50%;' >" +
								"<div class='chatId' uid='" + uid + "'>"
						if (msg_type == "file") {
							UI += "<div  onclick='replyForChat(this)' from='one' ondblclick='replyMsg()' messageType='file' msg='" + msg + "'  class='text-white px-3 py-2 float-right chat-outgoing-message singlebubble2 bubble2'>"
							if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
								UI += "<a path='" + imgPath + "' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  style='color:#fff;'  class='ChatText'>" + msg + "</a>" +
									"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
										+ "<div id='' class='defaultWordBreak float-right position-relative'>"
											+ "<img id='' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;margin-right: 30px;' src='" + imgPath + "' class='downCalIcon '>"
										+ "</div>"
									+ "</div>"
							}
							else {
								file_ext = file_ext.toLowerCase();
								UI += "<a path='" + imgPath + "' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\"  style='color:#fff;'  class='ChatText'>" + msg + "</a>" +
									"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "'>"
										+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
											+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
										+ "</div>"
									+ "</div>"
							}
							UI += "</div>"
						}
						else {
							UI += "<div  onclick='replyForChat(this)' from='one' ondblclick='replyMsg()' messageType='text' msg='" + msg + "'  class=' defaultWordBreak text-white px-3 py-2 float-right chat-outgoing-message singlebubble2 bubble2 '>"
							if (msg_action == "reply") {
								UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
									"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
							}
							if (msg_action == "forward") {
								UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
									"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
							}
							UI += "" + msg + "</div>"
						}
						UI += "<div class='py-1 float-right text-secondary chat-outgoing-time' style=''><span>" + created_time + ", " + created_date + "</span>";
						if (message_status == "sent") {
							UI += "<span class='mr-1 chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div></div>";
						} else if (message_status == "seen") {
							UI += "<span class='mr-1 chat-outgoing-status chat-outgoing-status-seen'><i> Read</i></span></div></div></div>";
						} else if (message_status == "delivered") {
							UI += "<span class='mr-1 chat-outgoing-status chat-outgoing-status-deliver'><i> Delivered</i></span></div></div></div>";
						}
					}
				}
				$("#prepend_" + uid).prepend(UI);
				index = parseInt(index) + 20;
				$("#" + uid + "m").attr('index', index);
				// for(i = jsonData.length-1; i >= 0; i--)
				// 	{
				// 	   var UI="";
				// 	   var t="";
				// 	   var d = new Date();
				// 	   var time = jsonData[i].time;
				// 	       time = time.replace("CHR(26)",":");

				//        var T =  time.split("#@#")[1];   
				// 	   var date=  time.split("#@#")[0];  

				// 	   var m = d.getMonth();
				// 	       m = months[m];
				// 	   var date1 = d.getDate();
				//     	   date1 = (date1>10) ? (date1) : ('0'+date1);           
				// 	   var D=date1+"-"+m+"-"+d.getFullYear();    

				// 	   if(date == D){
				// 		   t="Today  "+T;
				// 	   }else{
				// 		   t=T+" , "+ date;
				// 	   }

				// 	  	var ioType = jsonData[i].ioType;
				// 	  	var duration = jsonData[i].duration;
				// 	  	var type=jsonData[i].type;
				// 	  	var callType = jsonData[i].callType;
				// 	    var messageType  = jsonData[i].messageType;
				// 	   //var message=jsonData[i].message.replace("CHR(26)",":");
				// 	   //var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
				// 	    var message=jsonData[i].message;
				//         var message=replaceSpecialCharacter(message);
				//         var body = textTolink(message);
				//         var fileExt = jsonData[i].fileExt;
				//         var messageId=jsonData[i].messageId;
				//         var msgReplyId=jsonData[i].msg_reply_id;
				//         var msgAction=jsonData[i].msg_action;
				// 	   if(type=="chat"){
				//         	   if(jsonData[i].sender == userId){

				//         		   UI=	"<div id="+messageId+" class='chat-outgoing-overall row chatId'>"
				//         			   +"<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>" 
				// 		   				+"<img userId ='"+jsonData[i].sender+"' src='"+lighttpdPath+"/userimages/"+jsonData[i].sender+"."+userImgType+"?"+imgTime+"' onerror='userImageOnErrorReplace(this);' style='float:right;width: 40px; height: 38px; border-radius: 50%; margin-left:-2.3vh;margin-top: 1vh;margin-bottom:2vh;'/></div>" 
				// 		   				+"<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right'>"
				// 		   				+"<div onclick='replyForChat(this)' class='chat-outgoing-message singlebubble2 row'>";
				// 		        		   if(msgReplyId != "0" && msgAction != "forward"){
				// 		        			   var msgthread=jsonData[i].replyThread;
				// 	        		   			var mainChatThread;
				// 	        		   			var replyName;

				// 			        		   			if(msgthread[0].replyfrom == userId){
				// 			        		   				replyName="You";
				// 			        		   			}else{
				// 			        		   				replyName=msgthread[0].userName;
				// 			        		   			}

				// 			        		   			mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
				// 			        		   			mainChatThread = textTolink(mainChatThread,'reply');

				// 	        		   		 UI+="<div class='replyNameTop'>"+replyName+"</div>"+
				// 				   			 "<div class='replyMsgBottom'>"+mainChatThread+"</div>"+
				// 				   			 "<div messageType='"+messageType+"'  class='replyToReply ChatText'>";
				// 				   				if(messageType == "text"){
				// 				   				 	UI+= body;
				// 				   				}else if(messageType == "media"){
				// 				   				 	UI+= "<span src='"+jsonData[i].filePath+"' onclick='cmeopenVideo(this)' extenstion = '"+fileExt+"' style='cursor:pointer'>"+message+"."+fileExt+"</span>" ;
				// 				   				}else if(messageType == "highlight"){
				// 				   				 	UI+= "<a style='color:#fff;' href='#' onclick='viewHighlight("+jsonData[i].messageId+");'>"+message+"</a>" ;
				// 				   				}else{
				// 				   				 	UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"."+fileExt+"</a>" ;
				// 				   				}
				// 				   			    UI+="</div>";

				// 							}else if(msgAction == "forward"){
				// 								UI+="<div style='margin-left: -8px;margin-top: -5px;'>"+
				// 									"<img title='Forwarded' class='forwardIconCls' src='"+path+"/images/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"+
				// 								    "<div class='ChatText' style='margin-left: -8px;margin-top: 4px;'>";
				// 								if(messageType == "text"){
				// 				   				 	UI+= body;
				// 				   				}else if(messageType == "media"){
				// 				   				 	UI+= "<span src='"+jsonData[i].filePath+"' onclick='cmeopenVideo(this)' extenstion = '"+fileExt+"' style='cursor:pointer'>"+message+"."+fileExt+"</span>" ;
				// 				   				}else if(messageType == "highlight"){
				// 				   				 	UI+= "<a style='color:#fff;' href='#' onclick='viewHighlight("+jsonData[i].messageId+");'>"+message+"</a>" ;
				// 				   				}else{
				// 				   				 	UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"."+fileExt+"</a>" ;
				// 				   				}
				// 				   			    UI+="</div>";

				// 						}else{
				// 								 UI+= "<div messageType='"+messageType+"' class='ChatText'>";
				// 					   				if(messageType == "text"){
				// 					   				 	UI+= body;
				// 					   				}else if(messageType == "media"){
				// 					   				 	UI+= "<span src='"+jsonData[i].filePath+"' onclick='cmeopenVideo(this)' extenstion = '"+fileExt+"' style='cursor:pointer'>"+message+"."+fileExt+"</span>" ;
				// 					   				}else if(messageType == "highlight"){
				// 					   				 	UI+= "<a style='color:#fff;' href='#' onclick='viewHighlight("+jsonData[i].messageId+");'>"+message+"</a>" ;
				// 					   				}else{
				// 					   				 	UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"."+fileExt+"</a>" ;
				// 					   				}
				// 					   			    UI+="</div>";
				// 							}

				// 		        		   UI+="</div>"
				// 		        			 +"<div class='chat-outgoing-time row'><span>"+t+"</span></div></div></div>";

				//         	   }
				//         	   else{

				// 	        	 UI=  "<div id="+messageId+" class='single-chat-income-overall row chatId' style='outline:none' tabindex='0'>"

				// 						+"<div class='chat-income-imageframe col-xs-2' style='display:none' >"
				// 						+"<img  userId ='"+jsonData[i].sender+"' src='' class='incomeframeImage' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>" 
				// 						+"<div class='col-xs-10' style = 'margin-left: -2.2em;' >"
				// 						+"<div onclick='replyForChat(this)' class='chat-income-message singlebubble row'>"; 
				// 			        if(msgReplyId != 0 && msgAction != "forward"){
				// 			        		 var msgthread=jsonData[i].replyThread;

				// 	        		   			var mainChatThread;
				// 	        		   			var replyName;


				// 			        		   			if(msgthread[0].replyfrom == userId){
				// 			        		   				replyName="You";
				// 			        		   			}else{
				// 			        		   				replyName=msgthread[0].userName;
				// 			        		   			}

				// 			        		   			mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
				// 			        		   			mainChatThread = textTolink(mainChatThread,'reply');

				// 	        		   		 UI+="<div class='replyNameTop'>"+replyName+"</div>"+
				// 				   			 "<div class='replyMsgBottom'>"+mainChatThread+"</div>"+
				// 				   			 "<div messageType='"+messageType+"' class='replyToReply ChatText'>";
				// 							  if(messageType == "text"){
				// 				   				 	UI+= body;
				// 				   				}else if(messageType == "media"){
				// 				   				 	UI+= "<span src='"+jsonData[i].filePath+"' onclick='cmeopenVideo(this)' extenstion = '"+fileExt+"' style='cursor:pointer'>"+message+"."+fileExt+"</span>" ;
				// 				   				}else if(messageType == "highlight"){
				// 				   				 	UI+= "<a style='color:#fff;' href='#' onclick='viewHighlight("+jsonData[i].messageId+");'>"+message+"</a>" ;
				// 				   				}else{
				// 				   				 	UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"."+fileExt+"</a>" ;
				// 				   				}
				// 								UI+= "</div>";

				// 			        }else if(msgAction == "forward"){
				// 							UI+="<div style='margin-left: -8px;margin-top: -5px;'>"+
				// 								"<img title='Forwarded' class='forwardIconCls' src='"+path+"/images/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"+
				// 						    	"<div messageType='"+messageType+"'  class='ChatText' style='margin-left: -8px;margin-top: 4px;'>";
				// 							    if(messageType == "text"){
				// 				   				 	UI+= body;
				// 				   				}else if(messageType == "media"){
				// 				   				 	UI+= "<span src='"+jsonData[i].filePath+"' onclick='cmeopenVideo(this)' extenstion = '"+fileExt+"' style='cursor:pointer'>"+message+"."+fileExt+"</span>" ;
				// 				   				}else if(messageType == "highlight"){
				// 				   				 	UI+= "<a style='color:#fff;' href='#' onclick='viewHighlight("+jsonData[i].messageId+");'>"+message+"</a>" ;
				// 				   				}else{
				// 				   				 	UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"."+fileExt+"</a>" ;
				// 				   				}
				// 								UI+= "</div>";

				// 					}else{
				// 			        		 UI+= "<div messageType='"+messageType+"'  class='ChatText'>";
				// 							  if(messageType == "text"){
				// 				   				 	UI+= body;
				// 				   				}else if(messageType == "media"){
				// 				   				 	UI+= "<span src='"+jsonData[i].filePath+"' onclick='cmeopenVideo(this)' extenstion = '"+fileExt+"' style='cursor:pointer'>"+message+"."+fileExt+"</span>" ;
				// 				   				}else if(messageType == "highlight"){
				// 				   				 	UI+= "<a style='color:#fff;' href='#' onclick='viewHighlight("+jsonData[i].messageId+");'>"+message+"</a>" ;
				// 				   				}else{
				// 				   				 	UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"."+fileExt+"</a>" ;
				// 				   				}
				// 								UI+= "</div>";
				// 			        	 }

				// 			        	 UI+="</div>"
				// 			        	 +"<div class='chat-income-time row'>"+t+"</div></div></div>";
				//         	   }
				// 	   }else if (type=="call"){

				// 	    				UI=	"<div align='center' class='row' style='float:left;position:relative;width:100%;margin:0px'>"
				//         			         +"<div class='callmsg' style='font-size:12px; margin-top: 2vh;width: 50%; color:  #323232; padding: 9px; padding-left: 4px; background-color:#D8DDE1;border-radius: 9px;position:relative;'>";
				// 		   					 var src="";
				// 		   					 var msg="";
				// 		   					 if(ioType=="Incoming"){

				// 			   					 if(messageType =="calldisconnect"){
				// 			   					    src = callType=="AV"? path+'/images/video/incomingVC.png': path+'/images/video/incomingAC.png';
				// 			   					 	 msg = "Connected";
				// 			   					 }else if(messageType =="callnotresponded"){
				// 			   					 	src = callType=="AV"? path+'/images/video/incomingMVC.png': path+'/images/video/incomingMAC.png';
				// 			   					 	msg = "Missed Call";
				// 			   					 }else if(messageType =="callrequestcancelled"){
				// 			   					 	src = callType=="AV"? path+'/images/video/incomingMVC.png': path+'/images/video/incomingMAC.png';
				// 			   					 	msg = "Missed Call";
				// 			   					 }else if(messageType =="calldeclined"){
				// 			   					 	src = callType=="AV"? path+'/images/video/incomingMVC.png': path+'/images/video/incomingMAC.png';
				// 			   					 	msg = "Rejected";
				// 			   					 }else{
				// 			   					 	src = callType=="AV"? path+'/images/video/incomingVC.png': path+'/images/video/incomingAC.png';
				// 			   					 	msg = "Connected";
				// 			   					 }

				// 		   					 }else{

				// 		   					 	if(messageType =="calldisconnect"){
				// 			   					    src = callType=="AV"? path+'/images/video/outgoingVC.png': path+'/images/video/outgoingAC.png';
				// 			   					    msg = "Connected";
				// 			   					 }else if(messageType =="callnotresponded"){
				// 			   					 	src = callType=="AV"? path+'/images/video/outgoingMVC.png': path+'/images/video/outgoingMAC.png';
				// 			   					 	msg = "Didn't Connect" ;
				// 			   					 }else if(messageType =="callrequestcancelled"){
				// 			   					 	src = callType=="AV"? path+'/images/video/outgoingMVC.png': path+'/images/video/outgoingMAC.png';
				// 			   					 	msg = "Cancelled";
				// 			   					 }else if(messageType =="calldeclined"){
				// 			   					 	src = callType=="AV"? path+'/images/video/outgoingMVC.png': path+'/images/video/outgoingMAC.png';
				// 			   					 	msg = "Request Rejected";
				// 			   					 }else{
				// 			   					 	src = callType=="AV"? path+'/images/video/outgoingVC.png': path+'/images/video/outgoingAC.png';
				// 			   					 	 msg = "Connected";
				// 			   					 }
				// 			   				 }
				// 			   			UI+="<img style='height:22px;width:22px;margin-top: -10px;' src='"+src+"'>";
				// 						UI+="<span>&nbsp;&nbsp;"+msg+"<br></span>";
				// 						UI+="<span style='font-size:11px'> &nbsp;"+T+",&nbsp;"+date+"</span><br>";
				// 						if(duration!= ""){
				// 							if(duration.indexOf("-") != -1 ){
				// 								duration=duration.substring(1,duration.length);
				// 							}
				// 							UI+="<span>&nbsp;&nbsp;Duration: "+duration+"</span>";
				// 							UI+="<div style='position: absolute;right: 0;top: 5px;'>"
				// 							if(jsonData[i].hlightExist =='Y'){
				// 								UI+='<img title="Highlights" class="hrExist_'+jsonData[i].messageId+'" style="cursor:pointer;margin-right:10px;" action="expand" onclick=fetchCallRelatedData(this,'+jsonData[i].messageId+',"highlight") src="'+path+'/images/video/hlightExist.png">';
				// 							}
				// 							if(jsonData[i].recordExist =='Y'){
				// 								UI+='<img title="Records" class="hrExist_'+jsonData[i].messageId+'" style="cursor:pointer;margin-right:10px;" action="expand" onclick=fetchCallRelatedData(this,'+jsonData[i].messageId+',"media") src="'+path+'/images/video/recordExist.png">';
				// 						    }
				// 						    UI+="</div>"
				// 						}
				//    						UI+="</div></div></div>";
				//    						UI+="<div align='center' id='callData_"+jsonData[i].messageId+"' class='row' style='float:left;position:relative;width:100%;margin:0px'></div>";

				// 	   }
				// 	          $Id.prepend(UI);

				// 	}
				// if(windowWidth1 < 750){ // to resize the chat conversation UI for smaller size
				// 	$(".callmsg").css({'width':'94%'});
				//  }else{
				// 	$(".callmsg").css({'width':'50%'});
				//  }
				//  if(jsonData.length>=20 ){
				// 	   $Id.prepend("<div id='lem' style='background-color: #d9d4ce; text-align:center; height: 8%; margin-bottom:2%;padding-top: 1%;' onclick='loadEarlierMessages(this)'><a href='#' style='color: blue; font-family: helvetica;'>Load earlier messages</a></div>");
				//    }

				//gerneralScrollBarFunction("chat-dialog"); 

				// index = parseInt(index) + 20;
				// $("#"+uid+"m").attr('index',index);

				//  var height =  $Id[0].scrollHeight;
				//      height = height/count++;
				//      $Id.scrollTop(height);
				//  $("#"+id+"m").attr('cnt',count);

				//  var windowWidth1 = $(window).width();

				// if(windowWidth1 < 750){ // to resize the chat conversation UI for smaller size
				// // $(".chatOutbubble").css({'padding-right':'2.3em'});
				//  	$(".callmsg").css({'width':'94%'});
				//  }else{
				// 	// $(".chatOutbubble").css({'padding-right':'0.3em'});
				// 	 $(".callmsg").css({'width':'50%'});
				//  }
			}
		}
	});
	$("#loadingBar").hide();
	timerControl("");
}

function loadEarlierGrpMessages(gid, obj) {
	var index = $("#" + gid + "m").attr('index');
	var jsonData;
	//	var imgType = $("#"+id).attr('imgType');
	var count = $("#" + gid + "m").attr('count');
	// count = parseInt(count);
	// var $Id= $("#"+mucId+"m").find('#chat-dialog');
	//     $Id.children('#lem').remove();
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var d = new Date();

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"group_id": gid,
		"index": index,
		"limit": "20",
		"localTZ": localOffsetTime,
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchGroupLastMsgSet",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			if (result == 'No latest msg') {

			} else {
				var data = result;
				let UI = '';
				var d = new Date();
				var sender_image_type = '';
				var sender = '';
				var msg = '';
				var g_id = '';
				var sender_name = '';
				var msg_reply_id = '';
				var created_time = '';
				var created_date = '';
				var msg_type = '';
				var filePathNew = '';
				var file_id = '';
				var file_ext = '';
				var msg_action = '';
				var replyName = '';
				var mainChatThread = '';
				var color = "";
				for (i = data.length - 1; i >= 0; i--) {
					sender_image_type = data[i].sender_image_type;
					sender = data[i].sender;
					msg = data[i].msg;
					g_id = data[i].g_id;
					sender_name = data[i].sender_name;
					msg_reply_id = data[i].msg_reply_id;
					created_time = data[i].created_time;
					created_date = data[i].created_date;
					msg_type = data[i].msg_type;
					filePathNew = data[i].filePathNew;
					file_id = data[i].file_id;
					file_ext = data[i].file_ext;
					g_msg_id = data[i].g_msg_id;
					msg_action = data[i].msg_action;
					if (msg_action == "reply") {
						color = generateRandomColor();
						if (data[i].replyThread == [] && data[i].replyThread[0].msg_reply_id == 0) {
						}
						else {
							if (data[i].replyThread[0].sender == userIdglb) {
								replyName = "You";
								color = "#50baba;";
							}
							else {
								replyName = data[i].replyThread[0].userFullName;
							}
							mainChatThread = data[i].replyThread[0].msg;
						}
					}
					if (userIdglb != sender) {
						imgSrc = $('#userId_' + sender).find('img').attr('data-src');
						UI += "<div id='" + g_msg_id + "' class='media py-1 chat' style='display: -webkit-box;'>" +
								"<img src='" + imgSrc + "' class='align-self-start mt-4 mr-3 rounded-circle' title='" + sender_name + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
								"<div class='media-body chatId' uid='" + sender + "''>" +
									"<div style='font-size: 13px;color: gray;'>" + sender_name + "</div>"
						if (msg_type == "file") {
							UI += "<div onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='file' msg='" + msg + "' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;'>"
							if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
								UI += "<a target='_blank' path='" + filePathNew + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  style='color:#fff;'>" + msg + "</a>" +
									"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
										+ "<div id='' class='defaultWordBreak float-left position-relative'>"
											+ "<img id=''  ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + filePathNew + "' class='downCalIcon '>"
										+ "</div>"
									+ "</div>"
							}
							else {
								file_ext = file_ext.toLowerCase();
								UI += "<a target='_blank' path='" + filePathNew + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + msg + "</a>" +
									"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
										+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
											+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
										+ "</div>"
									+ "</div>"
							}
							UI += "</div>"
						}
						else {
							UI += "<div onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='text' msg='" + msg + "' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;word-break: break-word;'>"
							if (msg_action == "reply") {
								UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
									"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
							}
							UI += "" + msg + "</div>"
						}
						UI += "<div class='chat-income-time float-left p-0 pt-1'>" + created_time + ", " + created_date + "</div>" +
							"</div>" +
						"</div>"
					}
					else {
						var imgName = lighttpdpath + "/userimages/" + userIdglb + "." + sender_image_type + "?" + d.getTime();
						UI += "<div id='" + g_msg_id + "' class='media py-1 chat' style='display: -webkit-box;'>" +
								"<div class='media-body mt-2 chatId' uid='" + userIdglb + "'>"
						if (msg_type == "file") {
							UI += "<div  onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='file' msg='" + msg + "' class=' defaultWordBreak chat-income-message singlebubble bubble float-right' style='margin-left: 30%;background-color: #007b97;'>"

							if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {


								UI += "<a target='_blank' path='" + filePathNew + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;'>" + msg + "</a>" +
									"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
										+ "<div id='' class='defaultWordBreak float-right position-relative'>"
											+ "<img id='' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1 mr-0' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + filePathNew + "' class='downCalIcon '>"
										+ "</div>"
									+ "</div>"
							}
							else {
								file_ext = file_ext.toLowerCase();
								UI += "<a target='_blank' path='" + filePathNew + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + msg + "</a>" +
									"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "'>"
										+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
											+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
										+ "</div>"
									+ "</div>"
							}
							UI += "</div>"
						}
						else {
							UI += "<div  ondblclick='replyMsg()' messageType='text' msg='" + msg + "' class='px-3 py-2 defaultWordBreak bubble2 float-right chat-outgoing-message singlebubble2 bubble2' style='margin-left: 30%;background-color: #007b97;word-break: break-word;'>"
							if (msg_action == "reply") {
								UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
									"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
							}
							UI += "" + msg + "</div>"
						}
						UI += "<div class='float-right mt-1' style='text-align: right;width: 100%;font-size: 11px;color: gray;'>" + created_time + ", " + created_date + "</div>" +
							"</div>" +
							"<img src='" + imgName + "' class='align-self-start ml-3 mt-2 rounded-circle' title='" + sender_name + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px;'>" +
							"</div>"
					}
				}

				$("#prepend_" + gid).prepend(UI);
				index = parseInt(index) + 20;
				$("#" + gid + "m").attr('index', index);
				Id = "#prepend_" + gid;
				// for(i = jsonData.length-1; i >= 0; i--)
				// 	{
				// 	   var UI="";
				// 	   var t="";
				// 	   var d = new Date();
				// 	   var time = jsonData[i].time;
				// 	       time = time.replace("CHR(26)",":");
				// 	   var T =  time.split("#@#")[1];   
				// 	   var date=  time.split("#@#")[0];  
				// 	   var m = d.getMonth();
				// 	       m = months[m];
				//        var date1 = d.getDate();
				// 	       date1 = (date1>10) ? (date1) : ('0'+date1);

				// 	   var D=date1+"-"+ m +"-"+d.getFullYear();   

				// 	   if(date == D){
				// 		   t="Today  "+T;
				// 	   }else{
				// 		   t=T+" , "+ date;
				// 	   }
				// 	  // var message=jsonData[i].message.replace("CHR(26)",":");
				// 	   var message=jsonData[i].message;
				//        var message=replaceSpecialCharacter(message);
				// 	   var userFullName = jsonData[i].senderName;
				// 	   var Id = jsonData[i].sender;
				//        var senderImgType =jsonData[i].senderImgType;
				//        var msg = textTolink(message);
				//        var messageId=jsonData[i].messageId;
				//        var msgReplyId=jsonData[i].msg_reply_id;
				// 	   if(jsonData[i].sender == userId){
				//     		   UI=	"<div id="+messageId+" class='chat-outgoing-overall groupOutGoingChat row chatId'>"+
				// 			        	"<div class='chat-outgoing-imageframe col-xs-1'>"+ 
				// 			        		"<img userId='"+Id+"' src='"+lighttpdPath+"/userimages/"+Id+"."+userImgType+"'  title='"+userFullName+"' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%; margin-top: 1vh;margin-bottom:2vh;'/></div>"+
				// 			        	"<div class='col-xs-11 groupOutbubble' style='margin-top: -3vh;margin-left:-2vh;float: right; margin-right: -21px'>"+
				// 			        		"<div onclick='replyForChat(this)' class='chat-outgoing-message bubble2 row'>";
				// 			        		   if(msgReplyId != 0){
				// 		        		   		    var msgthread=jsonData[i].replyThread;
				// 		        		   			var mainChatThread;
				// 		        		   			var replyName;

				// 		        		   			if(msgthread[0].replyfrom == userId){
				// 		        		   				replyName="You";
				// 		        		   			}else{
				// 		        		   				replyName=msgthread[0].userName;
				// 		        		   			}

				// 		        		   			mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
				// 		        		   			mainChatThread = textTolink(mainChatThread,'reply');

				// 		        		   			UI+="<div class='replyNameTop'>"+replyName+"</div>"+
				// 					   			    	"<div class='replyMsgBottom'>"+mainChatThread+"</div>"+
				// 					   			    	"<div messageType='text'  class='replyToReply ChatText'>"+
				// 				        			   	msg + 
				// 				        			    "</div>";
				// 			        		   }else{
				// 			        			   UI+="<div messageType='text'  class='ChatText'>"+
				// 			        			   	msg + 
				// 			        			    "</div>";
				// 			        		   }

				// 			        		   UI+="</div>"+ 
				// 			        		   		"<div class='chat-outgoing-time row'><span>"+t+"</span></div></div></div>";

				// 	   }else{
				// 	   		UI=  "<div id="+messageId+" class='chat-income-overall row chatId' style='outline:none' tabindex='0'>"+
				// 			"<div class='chat-income-imageframe col-xs-1' style='margin-top: 23px;'>"+
				// 			"<img  userId='"+Id+"' src='"+lighttpdPath+"/userimages/"+Id+"."+senderImgType+"'  onerror='userImageOnErrorReplace(this);' title='"+userFullName+"' class='incomeframeImage' style='width: 40px; height: 38px; border-radius: 50%;margin-left:-7vh '/></div>"+ 
				// 			"<div class='col-xs-10 chatIncomeBubble' style = 'padding-left: 0vh;margin-left:-1.5em' >"+
				// 			"<div class='row'align='left' style='font-family: helvetica; font-size: 13px; width: 105%; padding-left: 34px;'>"+userFullName+"</div>"+
				// 			"<div onclick='replyForChat(this)' class='chat-income-message bubble row'>";
				// 	   		if(msgReplyId != 0){
				// 	   		    var msgthread=jsonData[i].replyThread;
				// 	   			var mainChatThread;
				// 	   			var replyName;

				// 	   			if(msgthread[0].replyfrom == userId){
				// 	   				replyName="You";
				// 	   			}else{
				// 	   				replyName=msgthread[0].userName;
				// 	   			}

				// 	   			mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
				// 	   			mainChatThread = textTolink(mainChatThread,'reply');

				// 	   			UI+="<div class='replyNameTop'>"+replyName+"</div>"+
				//    			    	"<div class='replyMsgBottom'>"+mainChatThread+"</div>"+
				//    			    	"<div messageType='text'  class='replyToReply ChatText'>"+
				//     			   	 msg + 
				//     			   	 "</div>";

				// 	   		}else{
				// 	   			UI+= "<div messageType='text' class='ChatText'>"+
				// 			   	 msg + 
				// 			   	 "</div>";
				// 	   		}

				// 	   		UI+= "</div>" +
				// 	   			 "<div class='chat-income-time row' style='padding-left: 34px;'>"+t+"</div>"+
				// 	   			 "</div>"+
				// 	   			 "</div>";
				// 	   }
				// 	          $Id.prepend(UI);
				// 	}

				//  if(jsonData.length>=20 ){
				// 	   $Id.prepend("<div id='lem' style='background-color: #d9d4ce; text-align:center; height: 8%;margin-bottom:2%;padding-top: 1%;' onclick='getGroupNextPreviousMessages("+mucId+")'><a href='#' style='color: blue; font-family: helvetica;'>Load earlier messages</a></div>");
				//    }

				//   index = parseInt(index) + 20;
				//	$("#"+id+"m").attr('index',index);
				//gerneralScrollBarFunction("chat-dialog"); 

				//  var height =  $Id[0].scrollHeight;
				//      height = height/count++;
				//      $Id.scrollTop(height);
				    //  $("#"+mucId+"m").attr('cnt',count);
				    //  $('#loader_'+mucId+'m').hide(); 
				//      var windowWidth1 = $(window).width();
				// 	 if(windowWidth1<750){

				// 		 $(".incomeframeImage").css({'margin-left':'-7vh'});
				// 		 $(".groupOutGoingChat").css({'margin-left':'-3vh'});
				// 		 $(".chatIncomeBubble").css({'margin-left':'-1em'});
				// 	 }else{

				// 		 $(".incomeframeImage").css({'margin-left':'-4vh'});
				// 		 $(".groupOutGoingChat").css({'margin-left':'0vh'});
				// 		 $(".chatIncomeBubble").css({'margin-left':'-1em'});
				// 	 }
			}
		}
	});
	$("#loadingBar").hide();
	timerControl("");
}
/* @This method is used to send a message to selected chat user
 * 
 */
var chatFortype = "normal";
function sendingMsg(ev, obj, type) {
	//console.log("sending msg--->");
	var uid = $('#msgtext').attr('uid');  // getting the id from chat box
	var cid = $('#msgtext').attr('cid');
	var textValue = '';
	if (ev.which == 8) {
		var textValue = $("#msgtext").val();
		return;
	}
	if (type !== "video") {
		var jid = uid.split(' ')[0] + "@" + cmeDomain + ".colabus.com";
		// var jid= "5331@devchat.colabus.com"; 
		var status = 'N';
		var d = new Date();
		if (ev.which == 13 || ev.which == 1) {//
			if (imageShre != undefined && imageShre != '') {
				postCanvasToURLImageForClipboardInCme(uid); // Function call to initiate clipboard sharing.
			}
			if (ev.which == 13) {
				ev.preventDefault();
				var body = $('#msgtext').val();
				textValue = body;

			}
			if (ev.which == 1) {
				ev.preventDefault();
				var body = $('#msgtext').val();
				textValue = body;

			}
			if ($.trim(body).length < 1)
				return;
			var t = getTimeIn12Format(d);
			var msg = body;
			// console.log(cid);
			var time1 = d.getHours() + ":" + d.getMinutes();
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

			var chatType = '';
			var chatId = '';
			var mainChatThread = replymsg;
			var replyName;
			var color = generateRandomColor();
			if (chatFortype == "reply") {
				chatType = "reply";
				chatId = replyForMsgId;
				if ($(".replythread").hasClass("chat-outgoing-message")) {
					color = "#50baba;";
					replyName = "You";
					$(".singlebubble2").css('background-color', '#007b97');
					$(".singlebubble2").removeClass("replythread");
					$("#msgtext").attr("placeholder", "Type message here...");
				} else {
					replyName = $("#userId_" + uid).find(".user_box_name").text();
					$(".singlebubble").css('background-color', '#0b4860');
					$(".singlebubble").removeClass("replythread");
					$("#msgtext").attr("placeholder", "Type message here...");
				}
			}
			let jsonbody = {
				"message": msg,
				"cid": cid,
				"msg_fr_uid": userIdglb,
				"message_status": "N",
				"messageType": "text",
				"docId": "0",
				"docPath": "",
				"chatFortype": chatType,
				"docExt": "",
				"msg_reply_id": chatId
			}
			$.ajax({
				url: apiPath + "/" + myk + "/v1/insertMsg",
				type: "POST",
				contentType: "application/json",
				data: JSON.stringify(jsonbody),
				error: function (jqXHR, textStatus, errorThrown) {
					checkError(jqXHR, textStatus, errorThrown);
					timerControl("");
				},
				success: function (result) {
					var jsonData = result;
					var message = $msg({ to: jid, "type": "chat", "msgid": result }).c('body').t(body) // preparing the one to one chat stanza to send	
						.up().c('replyForMsgId').t(chatId) //should take the value of msgid of the reply thread
						.up().c('replyForUserId').t(replyForUserId)  //should take the value of userid of the reply thread
						.up().c('replymsg').t(replymsg)  //should be the content of reply thread
						.up().c("chatFortype").t(chatType);  //should take the value 	of chatFortype value for the reply thread
					// console.log("msg send");
					Gab.connection.send(message);
					restoreValue();

					let UI = "<div id='" + result + "' class='mr-2 d-flex justify-content-end chat' style = 'margin-left: 50%;' >" +
								"<div class='chatId' uid='" + uid + "'>"
					UI += "<div  onclick='replyForChat(this)' from='one' ondblclick='replyMsg()' messageType='text' msg='" + body + "'  class=' defaultWordBreak text-white px-3 py-2 float-right chat-outgoing-message singlebubble2 bubble2 '>"
					if (chatType == "reply") {

						UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
							"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
					}
					UI += "" + body + "</div>"
					UI += "<div class='py-1 float-right text-secondary chat-outgoing-time' style=''><span>" + msgtime + "</span><span class='chat-outgoing-status mr-1 chat-outgoing-status-sent'><i> Sent</i></span></div></div></div>"

					$("#uid_" + uid).append(UI);
					$("#" + uid + "m").animate({ scrollTop: 20000 }, 'normal');
					$("#msgtext").val('');
					cmeMessages();
				}
			});
			// if ($("#conversationContactMsg_" + Id).length) {
			//     $("#conversationList ul ").find("#coversationContactTime_" + Id).html(msgtime);
			//     $("#conversationContactMsg_" + Id).html(body);
			//     $("#conversationContactMsg1_" + Id).hide();
			//     $("#conversationContactMsg_" + Id).show();
			//     var newjid = Id + '-' + chatDomain + '-colabus-com';
			//     var newchatUi = $("#conversationList ul").find("#" + newjid).clone();
			//     $("#conversationList ul").find("#" + newjid).remove();
			//     $("#conversationList ul").prepend(newchatUi);
			// } else {
			//     showPersonalConnectionMessages();
			// }

			// $(this).parent().data('composing', false);

		} else {
			// resizeChatBox();
			var notify = $msg({ to: jid, "type": "chat" }).c('composing', { xmlns: "http://jabber.org/protocol/chatstates" });
			Gab.connection.send(notify);
		}
	} else {
		var jid = obj + "@" + cmeDomain + ".colabus.com";
		var jsonText = '{"calltype":"AV","type":"callrequest"}';
		var notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		Gab.connection.send(notify);
	}
}

function sendingGroupMsg(ev, obj, type) {
	try {
		var gid = $('#groupmsgtext').attr('gid');// getting the group id from chat box
		var msgtype = "groupchat";
		// console.log(gid);
		var textValue = '';
		if (ev.which == 8) {
			var textValue = $('#groupmsgtext').val();
			return;
		}
		if (ev.which == 13 || ev.which == 1) {
			if (imageShre != undefined && imageShre != '') {
				postCanvasToURLImageForClipboardInCme(gid); // Function call to initiate clipboard sharing.
			}
			if (ev.which == 13) {
				// var jid = gid + '@mucr.' + cmeDomain + '.colabus.com';
				ev.preventDefault();
				var body = $('#groupmsgtext').val();
				textValue = body;
			}
			if (ev.which == 1) {
				// var jid = gid + '@mucr.' + cmeDomain + '.colabus.com';
				ev.preventDefault();
				var body = $('#groupmsgtext').val();
				textValue = body;
			}
			if ($.trim(body).length < 1)
				return;
			if (type !== "video") {
				// var gid = $("#groupmsgtext").attr('gid');  
				var jid = gid.split(' ')[0] + '@mucr.' + cmeDomain + '.colabus.com';
				// console.log(jid);
				var status = 'N';
				var d = new Date();
				var imgName = lighttpdpath + "/userimages/" + userIdglb + "." + userImgType + "?" + d.getTime();
				var body = textValue;
				var t = getTimeIn12Format(d);
				var msg = body;
				var m = d.getMonth();
				var time = d.getHours() + ":" + d.getMinutes() + "###" + d.getDate() + "-" + (Number(m) + 1) + "-" + d.getFullYear();
				var time1 = d.getHours() + ":" + d.getMinutes();
				const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();
				var chatType = '';
				var chatId = '';
				var mainChatThread = replymsg;
				var replyName;
				var color = generateRandomColor();
				if (chatFortype == "reply") {
					chatType = "reply";
					chatId = replyForMsgId;
					if ($(".replythread").hasClass("chat-outgoing-message")) {
						replyName = "You";
						color = "#50baba;";
						$(".bubble2").css('background-color', '#007b97');
						$(".bubble2").removeClass("replythread").removeClass('replyGroupBG2');
						$("#groupmsgtext").attr("placeholder", "Type message here...");
					}
					else {
						replyName = $("#userId_" + replyForUserId).find(".user_box_name").text();
						// replyName = $('.bubble').filter(".replythread").prev().text();
						$(".bubble").css('background-color', '#0b4860');
						$(".bubble").removeClass("replythread").removeClass('replyGroupBG');
						$("#groupmsgtext").attr("placeholder", "Type message here...");
					}
				}
				var localOffsetTime = getTimeOffset(new Date());
				let jsonbody = {
					"group_id": gid,
					"sender": userIdglb,
					"msg": msg,
					"sendTime": time,
					"timeZone": localOffsetTime,
					"msg_reply_id": chatId,
					"msg_action": ""
				}
				$.ajax({
					url: apiPath + "/" + myk + "/v1/insertGroupChateMsg",
					type: "POST",
					contentType: "application/json",
					data: JSON.stringify(jsonbody),
					error: function (jqXHR, textStatus, errorThrown) {
						checkError(jqXHR, textStatus, errorThrown);
						timerControl("");
					},
					success: function (result) {
						var jsonData = result;
						var msgId = result.split("@@")[1];
						// console.log(jsonData);
						var message = $msg({ to: jid, "type": "groupchat", "msgid": msgId }).c('body').t(body) //preparing the Group chat stanza to send	
							.up().c('replyForUserId').t(replyForUserId) //should take the value of userid of the reply thread
							.up().c('replymsg').t(replymsg) //should be the content of reply thread
							.up().c('replyForMsgId').t(chatId)
							.up().c("chatFortype").t(chatType); // should take the value of msgid of the reply thread 
						Gab.connection.send(message);
						restoreValue();

						let UI = "<div id='" + msgId + "' class='media chat'>" +
									"<div class='media-body mt-2 chatId' uid='" + userIdglb + "'>" +
										"<div onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='text' msg='" + body + "' class='px-3 py-2 chat-outgoing-message singlebubble2 bubble2 float-right' style='margin-left: 30%;background-color: #007b97;word-break: break-word;'>"
						if (chatType == "reply") {
							UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
								"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
						}
						UI += "" + body + "</div>" +
								"<div class='float-right mt-1' style='text-align: right;width: 100%;font-size: 11px;color: gray;'>" + msgtime + "</div>" +
							"</div>" +
							"<img src='" + imgName + "' class='align-self-start ml-3 mt-2 rounded-circle' title='" + userFullname + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px;'>" +
						"</div>"

						$("#guid_" + gid).append(UI);
						$('#' + gid + 'm').animate({ scrollTop: 20000 }, 'normal');
						$("#groupmsgtext").val('');
						cmeMessages();
					}
				});
			}
		}
	}
	catch (e) {
		console.log("Chat exception:" + e);
	}
}

function restoreValue() {
	replyForMsgId = 0;
	replyForUserId = "";
	replymsg = "";
	chatFortype = "";
}

var replyForMsgId = 0;
var replyForUserId = "";
var replymsg = "";
var globalObj = "";
function replyForChat(obj) {
	$(".singlebubble2").css('background-color', '#007b97');
	$(".singlebubble").css('background-color', '#0b4860');
	$(".bubble2").css('background-color', '#007b97');
	$(".bubble").css('background-color', '#0b4860');
	globalObj = obj;
	var uid = $(obj).parents(".chatId").attr('uid');
	if ($(obj).hasClass("replythread")) {
		$(".singlebubble").removeClass("replythread");
		$(".singlebubble2").removeClass("replythread");
		$(".bubble").removeClass("replythread").removeClass('replyGroupBG');
		$(".bubble2").removeClass("replythread").removeClass('replyGroupBG2');
		$("#chatOptionHead").addClass('d-none');
		$("#chatNameHead").removeClass('d-none');
		$("#msgtext").attr("placeholder", "Type message here...");
		$("#groupmsgtext").attr("placeholder", "Type message here...");
		chatFortype = "normal";
		replyForMsgId = 0;
		replyForUserId = "";
		replymsg = "";
	} else {
		$(".singlebubble").removeClass("replythread");
		$(".singlebubble2").removeClass("replythread");
		$(".bubble").removeClass("replythread").removeClass('replyGroupBG');
		$(".bubble2").removeClass("replythread").removeClass('replyGroupBG2');
		$("#chatOptionHead").removeClass('d-none');
		$("#chatNameHead").addClass('d-none');
		if ($(obj).hasClass('bubble2')) {
			$(obj).addClass("replythread").addClass('replyGroupBG2');
		} else {
			$(obj).addClass("replythread").addClass('replyGroupBG');
		}
		$(obj).css('background-color', '#339DEA');
	}
}

function replyMsg() {
	chatFortype = "reply";
	replyForMsgId = $(globalObj).parents(".chat").attr("id"); //should take the value of msgid of the reply thread
	if ($(".replythread").hasClass("chat-outgoing-message")) {
		//replyForUserId = should take the value of userid of the reply thread
		replyForUserId = userIdglb;
	} else {
		replyForUserId = $(globalObj).parents(".chatId").attr('uid');
	}
	replymsg = $(globalObj).attr('msg'); //should be the content of reply thread
	// console.log("replymsg--" + replymsg);
	$("#chatOptionHead").addClass('d-none');
	$("#chatNameHead").removeClass('d-none');
	$("#msgtext").attr("placeholder", "Reply...").focus();
	$("#groupmsgtext").attr("placeholder", "Reply...").focus();
}

function copyMsg() {
	chatFortype = "copy";
	replymsg = $(globalObj).attr('msg');
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val(replymsg).select();
	document.execCommand("copy");
	$temp.remove();
	$("#globalNotification").show().html("Copied!").delay(1000).hide(100);
	restoreValue();
	replyForChat(globalObj);
}

var forwardMsgType = "";
var forwardDocSrc = "";
var forwardFrom = "";
function forwardMsg() {
	chatFortype = "forward";
	replymsg = $(globalObj).attr('msg'); //should be the content of reply thread
	replyForMsgId = $(globalObj).parents(".chat").attr("id"); //should take the value of msgid of the reply thread
	// console.log("replyForMsgId--" + replyForMsgId + "replymsg--" + replymsg);
	if ($(".replythread").hasClass("chat-outgoing-message")) {
		//replyForUserId = should take the value of userid of the reply thread
		replyForUserId = userIdglb;
	} else {
		replyForUserId = $(globalObj).parents(".chatId").attr('uid');
	}
	var messagetype = $(globalObj).attr("messagetype");
	if (messagetype != "null" && messagetype == "file") {
		forwardMsgType = messagetype;
		forwardDocSrc = $(globalObj).find(".ChatText").attr('path');
		forwardFrom = $(globalObj).attr("from");
		// console.log("messagetype--" + messagetype + "forwardDocSrc--" + forwardDocSrc);
	} else {
		forwardMsgType = "";
	}
	$("#chatOptionHead").addClass('d-none');
	$("#chatNameHead").removeClass('d-none');
	// $("#contactList").html("");
	$("#transparentDiv").show();
	var popUp = '';
	let ui = '';
	ui='<div class="SBactiveSprintlistCls" style="top:90px !important;" id="SBactiveSprintlistId" selectedstories="">'
      +'<div class="mx-auto w-50" style="max-width: 500px;">'
        +'<div class="modal-content container px-0" style="max-height: 600px !important;">'
          +'<div class="modal-header pt-3 pr-2 pb-0 pl-3 d-flex align-items-center" style="border-bottom: 1px solid #e3dcdc;color: white;height: 39px;">'
            +'<span id="" class="menuhover contactss modal-title cmeactive" style="font-size:14px;color: #a4a4a4;" onclick="contactPopup()">CONTACTS</span>'  
            +'<span id="groups" class="menuhover modal-title cursor ml-4" style="font-size:14px;color: #a4a4a4;" onclick="groupPopup()">GROUPS</span>'
            // +'<span class="modal-title" style="width:20%;font-size:14px;">Start Date</span>'
            // +'<span class="modal-title" style="width:20%;font-size:14px;">End Date</span>'
			+'<input  id="searchText5" class="border border-0 d-none searchHeader" style="width:86%;font-weight: normal;font-size: 13px;height: auto;padding: 3px 8px;"  placeholder="Search" onkeyup="searchContacts()">'
			+'<img id="contsearch" src="images/menus/search2.svg" class=" ml-auto mr-4 mb-2 cursor" onclick="hideContact()" title="Search" style="width: 22px;">'
			+'<img id="backcontact" src="images/cme/leftarrow_blue.svg" class=" ml-auto mr-4 mb-2 cursor d-none" onclick="backContact()" title="Back"  style="width: 22px;">'
          +'</div>'
          +'<div class="" style="">'
            +'<button type="button" onclick="closeContact();event.stopPropagation();" class="close p-0 AgileCloseButton" style="top: 0px;right: 5px;color: black;outline: none;">&times;</button>'
          +'</div>'
          +'<div class="modal-body m-0 p-0" style="">'
				+'<div id="contactList" class="px-1 p-0 wsScrollBar" style="font-size:12px;max-height: 500px;height: 450px;overflow: auto;"></div>'
				+'<div id="groupList" class="px-1 p-0 wsScrollBar d-none" style="font-size:12px;max-height: 500px;height: 450px;overflow: auto;"></div>'
          +'</div>'
        +'</div>'
      +'</div>'
    +'</div>'

	$('#cmePopup').html(ui).show();
	$('#cmecontent2').children().clone().appendTo('#contactList');
	$('#contactList').children().each(function () {
		$(this).attr('onclick', 'forwardMsgTo(this)');
		$(this).removeAttr('id');
		// $(this).find('.audiocall .videocall').hide();
		$(this).attr('onmouseover', '');
		$(this).attr('onmouseout', '');
	});
	$('#cmecontent3').children().clone().appendTo('#groupList');
	$('#groupList').children().each(function () {
		$(this).attr('onclick', 'forwardMsgTo(this)');
		$(this).removeAttr('id');
		$(this).removeAttr('onmouseover');
		$(this).removeAttr('onmouseout');
		$(this).attr('onmouseover', '');
		$(this).attr('onmouseout', '');
	});
	backContact();
}

function groupPopup() {
	$('#contactList').addClass('d-none');
	$('#groupList').removeClass('d-none');
	$('.contactss').addClass('cursor');
	$('#groups').removeClass('cursor');
	$('#groups').addClass('cmeactive');
	$('.contactss').removeClass('cmeactive');
}

function contactPopup() {
	$('#groupList').addClass('d-none');
	$('#contactList').removeClass('d-none');
	$('.contactss').removeClass('cursor');
	$('#groups').addClass('cursor');
	$('#groups').removeClass('cmeactive');
	$('.contactss').addClass('cmeactive');
}

function hideContact() {
	
	$('#contsearch').addClass('d-none');
	$('#searchText5').removeClass('d-none');
	$('#searchText6').removeClass('d-none');
	$('#backcontact').removeClass('d-none');
	$('#searchText5').focus();
	$('#searchText6').focus();
	$('.contactss').addClass('d-none');
	$('#groups').addClass('d-none');
	
}

function backContact() {
	$("#searchText5").val("").trigger("keyup");
	$("#searchText6").val("").trigger("keyup");
	$('.contactss').removeClass('d-none');
	$('#contsearch').removeClass('d-none');
	$('#searchText5').addClass('d-none');
	$('#searchText6').addClass('d-none');
	$('#backcontact').addClass('d-none');
	$('#groups').removeClass('d-none');
}

function searchContacts() {        // for search of users in single chat
	var txt = $('#searchText5').val().toLowerCase();

	if ($('#groups').hasClass('cursor')) {
		$('#contactList').children().each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding
			var val = $(this).find('.user_box_name').attr('value').toLowerCase();
			if (val.indexOf(txt) != -1) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	}
	else {
		$('#groupList').children().each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding
			var val = $(this).find('.user_box_name').attr('value').toLowerCase();
			if (val.indexOf(txt) != -1) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	}
}

function closeContact() {
	$('#cmePopup').html('').hide();
	$("#cmePopupHL").hide();
	// $("#cmePopupTS").hide();
	$("#transparentDiv").hide();
	$(".singlebubble").removeClass("replythread");
	$(".singlebubble2").removeClass("replythread");
	$(".bubble").removeClass("replythread").removeClass('replyGroupBG');
	$(".bubble2").removeClass("replythread").removeClass('replyGroupBG2');
	$("#chatOptionHead").addClass('d-none');
	$("#chatNameHead").removeClass('d-none');
	$('.recomRECListDiv').html('');
	$(".recomWBListDiv").html('');
	$(".recomDOCListDiv").html('');
	$(".recomTASKListDiv").html('');
	$(".recomCONVListDiv").html('');
	$(".recomTSListDiv").html('');
	$(".recomHListDiv").html('');
	globalwb = "";
}

function GetSortedData(prop) {
	return function (a, b) {
		if (a[prop] > b[prop]) {
			return 1;
		} else if (a[prop] < b[prop]) {
			return -1;
		}
		return 0;
	}
}

function forwardMsgTo(obj) {
	var type = $(obj).attr('type');
	$("#contactList").html("");
	var status = 'N';
	var d = new Date();
	var Id = id;
	// var t = getTimeIn12Format(d);
	var msg = replymsg;
	if (forwardMsgType != "" && forwardMsgType == "file") {
		msg = "<a target='_blank' style='color:#fff;' href='" + forwardDocSrc + "' >" + msg + "</a>";
	}
	replymsg = replymsg.replace("'", "CHR(39)");
	var time1 = d.getHours() + ":" + d.getMinutes();
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();
	$('#cmePopup').hide();
	$("#transparentDiv").hide();
	// console.log("replyForMsgId--" + replyForMsgId + "replymsg--" + replymsg + "chatFortype--" + chatFortype);
	// console.log("messagetype--" + forwardMsgType + "forwardDocSrc--" + forwardDocSrc);
	if (forwardMsgType == "file") {
		if (type == "chat") {
			var id = $(obj).attr('uid');
			var cid = $('#user_' + id).attr('cid');
			var jid = id + "@" + cmeDomain + ".colabus.com";
			// console.log("id--" + id + "cid--" + cid);
			var localOffsetTime = getTimeOffset(new Date());
			var fromType = "";
			if (forwardFrom == "one") {
				fromType = "oneTone";
			}
			if (forwardFrom == "group") {
				fromType = "groupTone";
			}

			let jsonbody = {
				"cid": cid,
				"msgTime": msgtime,
				"timeZone": localOffsetTime,
				"message_status": "N",
				"chatFortype": "forward",
				"user_id": userIdglb,
				"msg_reply_id": replyForMsgId,
				"type": "chat",
				"type1": fromType
			}
			$.ajax({                        // This ajax call is to insert the one to one chat message into the database
				url: apiPath + "/" + myk + "/v1/insertFwdDocMsg",
				type: "POST",
				contentType: "application/json",
				data: JSON.stringify(jsonbody),
				error: function (jqXHR, textStatus, errorThrown) {
					checkError(jqXHR, textStatus, errorThrown);
					timerControl("");
				},
				success: function (result) {
					var jsonData = result;
					// console.log(jsonData);
					// if (result != "SESSION_TIMEOUT") {
					var msgId = result.split("@@")[1];
					// if (result != "SESSION_TIMEOUT") {
					// 	var msgId = result.toString().split("@@")[1];
					// $(divRef).attr("id", msgId);
					replymsg = replymsg.replace("CHR(39)", "'");

					var body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = '" + replymsg + "' value='" + forwardDocSrc + "'><filesharing /></href>";
					var message = $msg({ to: jid, "type": "chat", "msgid": msgId }).c('body').t(body)
						.up().c("replyForMsgId").t(replyForMsgId)//should take the value of msgid of the forward thread
						.up().c("chatFortype").t("forward") //should take the value of chatFortype value for the forward thread
						.up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
					Gab.connection.send(message);
					
					$('#user_' + id).trigger("onclick");
					restoreValue();
					closeContactList();
				}
			});
		}
		else {
			var gid = $(obj).attr('cid');
			var jid = gid + "@mucr." + cmeDomain + ".colabus.com";
			var localOffsetTime = getTimeOffset(new Date());
			// console.log("gid--" + gid + "type--" + type);
			var fromType = "";
			if (forwardFrom == "one") {
				fromType = "oneTone";
			}
			if (forwardFrom == "group") {
				fromType = "groupTone";
			}
			let jsonbody = {
				"cid": gid,
				"msgTime": msgtime,
				"timeZone": localOffsetTime,
				"message_status": "N",
				"chatFortype": "forward",
				"user_id": userIdglb,
				"msg_reply_id": replyForMsgId,
				"type": "groupchat",
				"type1": fromType
			}
			$.ajax({                        // This ajax call is to insert the one to one chat message into the database
				url: apiPath + "/" + myk + "/v1/insertFwdDocMsg",
				type: "POST",
				contentType: "application/json",
				data: JSON.stringify(jsonbody),
				error: function (jqXHR, textStatus, errorThrown) {
					checkError(jqXHR, textStatus, errorThrown);
					timerControl("");
				},
				success: function (result) {
					var jsonData = result;
					// console.log(jsonData);
					// if (result != "SESSION_TIMEOUT") {
					var msgId = result.split("@@")[1];
					// if (result != "SESSION_TIMEOUT") {
					// 	var msgId = result.toString().split("@@")[1];
					// $(divRef).attr("id", msgId);
					replymsg = replymsg.replace("CHR(39)", "'");

					var body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = '" + replymsg + "' value='" + forwardDocSrc + "'><filesharing /></href>";
					var message = $msg({ to: jid, "type": "groupchat", "msgid": msgId }).c('body').t(body)
						.up().c("replyForMsgId").t(replyForMsgId)//should take the value of msgid of the forward thread
						.up().c("chatFortype").t("forward") //should take the value of chatFortype value for the forward thread
						.up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
					Gab.connection.send(message);

					$('#groupuser_' + gid).trigger("onclick");
					restoreValue();
					closeContactList();
				}
			});
		}
	} else {
		if (type == "chat") {
			var id = $(obj).attr('uid');
			var cid = $('#user_' + id).attr('cid');
			var jid = id + "@" + cmeDomain + ".colabus.com";
			// console.log("id--" + id + "cid--" + cid);
			let jsonbody = {
				"message": msg,
				"cid": cid,
				"msg_fr_uid": userIdglb,
				"message_status": "N",
				"messageType": "text",
				"docId": "0",
				"docPath": "",
				"chatFortype": "forward",
				"docExt": "",
				"msg_reply_id": '',
				"type": "chat",
			}
			$.ajax({
				url: apiPath + "/" + myk + "/v1/insertMsg",
				type: "POST",
				contentType: "application/json",
				data: JSON.stringify(jsonbody),
				error: function (jqXHR, textStatus, errorThrown) {
					checkError(jqXHR, textStatus, errorThrown);
					timerControl("");
				},
				success: function (result) {
					var jsonData = result;
					// console.log(jsonData);
					// if (result != "SESSION_TIMEOUT") {
					var msgId = result.toString().split("@@")[1];
					// $(divRef).attr("id", msgId);
					replymsg = replymsg.replace("CHR(39)", "'");

					var message = $msg({ to: jid, "type": "chat", "msgid": result }).c('body').t(msg) // preparing the chat stanza to send
						.up().c("replyForMsgId").t(replyForMsgId)//should take the value of msgid of the forward thread
						.up().c("chatFortype").t(chatFortype); //should take the value of chatFortype value for the forward thread
					Gab.connection.send(message);
					
					$('#user_' + id).trigger("onclick");
					restoreValue();
					closeContactList();
				}
			});
		}
		else {
			var gid = $(obj).attr('cid');
			var jid = gid + "@mucr." + cmeDomain + ".colabus.com";
			replymsg = replymsg.replace("CHR(39)", "'");
			var m = d.getMonth();
			var time = d.getHours() + ":" + d.getMinutes() + "###" + d.getDate() + "-" + (Number(m) + 1) + "-" + d.getFullYear();
			// console.log("gid--" + gid + "type--" + type);
			var localOffsetTime = getTimeOffset(new Date());
			let jsonbody = {
				"group_id": gid,
				"sender": userIdglb,
				"msg": msg,
				"sendTime": time,
				"timeZone": localOffsetTime,
				"msg_reply_id": "0",
				"msg_action": "forward"
			}
			$.ajax({
				url: apiPath + "/" + myk + "/v1/insertGroupChateMsg",
				type: "POST",
				contentType: "application/json",
				data: JSON.stringify(jsonbody),
				error: function (jqXHR, textStatus, errorThrown) {
					checkError(jqXHR, textStatus, errorThrown);
					timerControl("");
				},
				success: function (result) {
					var jsonData = result;
					var msgId = result.split("@@")[1];
					$('#groupuser_' + gid).trigger("onclick");

					var message = $msg({ to: jid, "type": "groupchat", "msgid": result }).c('body').t(msg) //preparing the Group chat stanza to send	
						.up().c('replyForUserId').t(replyForUserId) //should take the value of userid of the reply thread
						.up().c('replymsg').t(replymsg) //should be the content of reply thread
						.up().c('replyForMsgId').t("")
						.up().c("chatFortype").t("forward"); // should take the value of msgid of the reply thread 

					Gab.connection.send(message);
					restoreValue();
					closeContactList();
				}
			});
		}
	}
}

function closeContactList() {
	if ($('.chat-income-message').hasClass("replythread") || $('.chat-outgoing-message').hasClass("replythread")) {
		$(".singlebubble").removeClass("replythread");
		$(".singlebubble2").removeClass("replythread");
		$(".bubble").removeClass("replythread").removeClass('replyGroupBG');
		$(".bubble2").removeClass("replythread").removeClass('replyGroupBG2');
		$(".singlebubble2").css('background-color', '#007b97');
		$(".singlebubble").css('background-color', '#0b4860');
		$(".bubble2").css('background-color', '#007b97');
		$(".bubble").css('background-color', '#0b4860');
		$("#chatOptionHead").hide();
		$("#chatNameHead").show();
		$("#text").attr("placeholder", "Type message here...");
		$("#contactList").html("");
		chatFortype = "normal";
		replyForMsgId = 0;
		replyForUserId = "";
		replymsg = "";
		forwardMsgType = "";
		forwardDocSrc = "";
	}
}

function playSound(filename) {
	$('#sound').html('<audio  autoplay="autoplay"><source src="/sound/' + filename + '.mp3" type="audio/mpeg" /><source src="/sound/' + filename + '.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="/sound/' + filename + '.mp3" /></audio>');
}

function seenMsg(id, type, msgid) {
	try {
		if (type == "alldelivered") {
			if (msgid.indexOf(',') > -1) {
				var msgIdArray = msgid.split(",");
				for (var i = 0; i < msgIdArray.length; i++) {
					var jid = id[i].split('-')[0] + "@" + cmeDomain + ".colabus.com";
					var msgId = msgIdArray[i];
					var jsonText = '{"status":"' + type + '"}';
					var message = $msg({ to: jid, "type": "chat", "msgid": msgId }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
					Gab.connection.send(message);
					//console.log("jid test----"+jid);
				}
			}
		} else {
			var jid = id + "@" + cmeDomain + ".colabus.com";
			// console.log("jid--"+jid);
			var jsonText = '{"status":"' + type + '"}';
			var message = $msg({ to: jid, "type": "chat", "msgid": msgid }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
			Gab.connection.send(message);
		}
	} catch (emsg) {
		console.log(emsg);
	}
	// if(type=="allseen"){
  	// 	type="seen";
	// }
	
	$.ajax({
		url: apiPath + "/" + myk + "/v1/updateMsgStatus?msgid=" + msgid + "&type=" + type + "&userId=" + userIdglb,
		type: "PUT",
		// contentType: "application/json",
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			var jsonData = result;
			// console.log(jsonData);
		}
	});
}

function sendDeliverStanza() {
	var sender = '';
	var groupMessCount = '';
	var cid;
	var cidArray = "";
	var jid_id;
	var jidArray = [];
	for (i = 0; i < jsonDataConversation.length; i++) {
		sender = jsonDataConversation[i].sender;
		groupMessCount = jsonDataConversation[i].unreadMsg;
		if (groupMessCount != 0) {
			cid = jsonDataConversation[i].cid;
			cidArray = cidArray + cid + ",";
			jid_id = sender + "-" + cmeDomain + "-colabus-com";
			jidArray.push(jid_id);
			// seenMsg(jid_id, "delivered", cid);
		}
	}
	cidArray = cidArray.substring(0, cidArray.length - 1);
	seenMsg(jidArray, "alldelivered", cidArray);
}

function updateSeenMessages(conId, uid, type) {
	$.ajax({
		url: apiPath + "/" + myk + "/v1/updateConvMsgStatus?userId=" + userIdglb + "&convId=" + conId + "&convTy=" + type,
		type: "PUT",
		// contentType: "application/json",
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			var jsonData = result;
			// console.log(jsonData);
			cmeMessages();
		}
	});
}
/* ***************	clipboard content into CME IM for one to one Chat ********************* */

var dataURL, filename, fileShare, imageShre;
function onPaste(event, obj) {
	// console.log('testing clipboard---->');
	const items = (event.clipboardData || event.originalEvent.clipboardData).items;
	let blob = null;
	for (const item of items) {
		if (item.type.indexOf('image') === 0) {
			blob = item.getAsFile();
		}
	}
	// load image if there is a pasted image
	if (blob !== null) {
		imageShre = blob;
		//   console.log("imageShre:"+imageShre); // data url!
		const reader = new FileReader();
		reader.onload = function (evt) {
			// console.log("data:"+evt.target.result); // data url!
			//this.imgRenderer.nativeElement.src = evt.target.result;
			$(obj).attr('placeholder', '').css({ backgroundImage: "url(" + evt.target.result + ")" });
		};
		reader.readAsDataURL(blob);
	}
}

var $width, $height;
$(function () {
	$width = $('#width');
	$height = $('#height');
	$('.target').on('click', function () {
		var $this = $(this);
		var bi = $this.css('background-image');
		$('.active').removeClass('active');
		$this.addClass('active');
	})
})

function postCanvasToURLImageForClipboardInCme(uid) {
	var file = imageShre;
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + '_' + time;
	var d = new Date();
	var time1 = d.getHours() + ":" + d.getMinutes();
	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();
	var chatType = $('#chatType').val();
	if (chatType == "chat") {
		var c_jid = uid + "@" + cmeDomain + ".colabus.com";
		var conid = $('#user_' + uid).attr('cid');
		var status = 'N';
		// console.log("id--" + uid + "c_jid--" + c_jid + "conid--" + conid);
		var formData = new FormData();
		formData.append('file', file, "image_" + dateTime + ".png");
		formData.append('place', "ChatFileSharing");
		formData.append('user_id', userIdglb);
		formData.append('company_id', companyIdglb);
		formData.append('resourceId', conid);
		formData.append('convotype', "N");
		formData.append('menuTypeId', "");
		formData.append('subMenuType', "chat");

		$.ajax({
			url: apiPath + "/" + myk + "/v1/upload",
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			data: formData,
			dataType: 'text',
			// data: formData,
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				// $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) {
				var msgId = result.split("@@")[1];
				result = result.split("@@")[0];
				var splitting = result.split('//')[3];
				var file_id = splitting.split('.')[0];
				var file_ext = splitting.split('.')[1];

				let UI = "<div id='" + msgId + "' class='mr-2 d-flex justify-content-end chat' style = 'margin-left: 50%;' >" +
							"<div class='chatId' uid='" + uid + "'>"
				UI += "<div onclick='replyForChat(this)' ondblclick='replyMsg()' from='one' messageType='file' msg='image_" + dateTime + ".png'  class='text-white px-3 py-2 float-right chat-outgoing-message  singlebubble2 bubble2'>"
				if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {

					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;'>image_" + dateTime + ".png</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
							+ "<div id='' class='defaultWordBreak float-right position-relative'>"
								+ "<img id='uploadedImage' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1 mr-0' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + result + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				else {
					file_ext = file_ext.toLowerCase();
					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>image_" + dateTime + ".png</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "'>"
							+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
								+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				UI += "</div>"
				UI += "<div class='py-1 float-right text-secondary chat-outgoing-time' style=''><span>" + getTimeIn12Format(new Date()) + "</span><span class='mr-1 chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div></div>"

				$("#uid_" + uid).append(UI);
				$("#" + uid + "m").animate({ scrollTop: 20000 }, 'normal');

				body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = 'image_" + dateTime + ".png' value='" + result + "'><filesharing /></href>";
				// if($("#videoContent").is(":hidden")){       
				var message = $msg({ to: c_jid, "type": "chat", "msgid": msgId }).c('body').t(body).up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
				Gab.connection.send(message);
				// }

				// if(file_name.indexOf('&') > 0){

				// 	file_name = file_name.replace("&","CH(38)");
				// }
				// body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = '"+file_name+'.'+extn+"' value='"+result+"'><filesharing /></href>";
				// // if($("#videoContent").is(":hidden")){       
				// 	var message = $msg({to : c_jid, "type" : "chat","msgid":msgId }).c('body').t(body).up().c('active', {xmlns: "http://jabber.org/protocol/chatstates"});		 
				// 	Gab.connection.send(message);
				//  }
				// else{
				// 	var jsonText = '{"calltype":"'+callType+'","type":"videochat","msg":"'+body+'","callid":"'+callId+'"}';
				// 	var message = $msg({to : jid, "type" : "normal" }).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
				// 	serverConnection.send(message);
				// }  

				// if($('#conversationList').is(':visible')){
				// 	$("#conversationList ul ").find("#coversationContactTime_"+id).html(msgtime);
				// 	$("#conversationContactMsg_"+id).html("image_"+dateTime+".png");
				// 	$("#conversationContactMsg1_"+id).hide();
				// 	$("#conversationContactMsg_"+id).show();
				// }
				// var newjid = id+'-'+cmeDomain+'-colabus-com';
				// var newchatUi = $("#conversationList ul").find("#" + newjid).clone();
				// 	$("#conversationList ul").find("#" + newjid).remove();
				// 	$("#conversationList ul").prepend(newchatUi);
				// $(obj).find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
				$('#msgtext').css('background-image', 'none');
				// $('#main_commentTextarea').css('background-image', 'none');
				$("#msgtext").val('');
				// $("#main_commentTextarea").val('');
				$('#msgtext').attr('placeholder', 'Type message here...');
				imageShre = "";
				//$('.chatpost').attr('onclick',"sendingMsg(event,this)");
				cmeMessages();
			}
		});
	}
	else {
		var gid = uid;
		var c_jid = gid + "@mucr." + cmeDomain + ".colabus.com";
		var d = new Date();
		var m = d.getMonth();
		var time = d.getHours() + ":" + d.getMinutes() + "###" + d.getDate() + "-" + (Number(m) + 1) + "-" + d.getFullYear();
		var timeZone = getTimeOffset(d);
		var imgName = lighttpdpath + "/userimages/" + userIdglb + "." + userImgType + "?" + d.getTime();
		var formData = new FormData();
		formData.append('file', file, "image_" + dateTime + ".png");
		formData.append('place', "ChatFileSharing");
		formData.append('user_id', userIdglb);
		formData.append('company_id', companyIdglb);
		formData.append('resourceId', gid);
		formData.append('convotype', "N");
		formData.append('menuTypeId', "");
		formData.append('subMenuType', "groupChat");
		formData.append('time', time);
		formData.append('timeZone', timeZone);

		$.ajax({
			url: apiPath + "/" + myk + "/v1/upload",
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			// data: formData,
			dataType: 'text',
			data: formData,
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				// $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) {
				var msgId = result.split("@@")[1];
				result = result.split("@@")[0];
				var splitting = result.split('//')[3];
				var file_id = splitting.split('.')[0];
				var file_ext = splitting.split('.')[1];

				let UI = "<div id = " + msgId + " class='media py-1 chat' style='display: -webkit-box;'>" +
							"<div class='media-body mt-2 chatId' uid='" + userIdglb + "'>"
				UI += "<div onclick='replyForChat(this)' ondblclick='replyMsg()' from='group' messageType='file' msg='image_" + dateTime + ".png' class=' defaultWordBreak chat-income-message bubble float-right' style='margin-left: 30%;background-color: #007b97;'>"
				if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" style='color:#fff;'>image_" + dateTime + ".png</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
							+ "<div id='' class='defaultWordBreak float-right position-relative'>"
								+ "<img id='uploadedImage' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1 mr-0' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + result + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				else {
					file_ext = file_ext.toLowerCase();
					UI += "<a target='_blank' path='" + result + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>image_" + dateTime + ".png</a>" +
						"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "'>"
							+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
								+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
							+ "</div>"
						+ "</div>"
				}
				UI += "</div>"
				UI += "<div class='float-right mt-1' style='text-align: right;width: 100%;font-size: 11px;color: gray;'>" + getTimeIn12Format(new Date()) + "<i>  Sent</i></div>" +
					"</div>" +
					"<img src='" + imgName + "' class='align-self-start ml-3 mt-2 rounded-circle' title='" + userFullname + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px;'>" +
					"</div>"

				$("#guid_" + gid).append(UI);
				$("#" + gid + "m").animate({ scrollTop: 20000 }, 'normal');

				body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = 'image_" + dateTime + ".png' value='" + result + "'><filesharing /></href>";
				// if($("#videoContent").is(":hidden")){       
				var message = $msg({ to: c_jid, "type": "groupchat", "msgid": msgId }).c('body').t(body).up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
				Gab.connection.send(message);
				// }

				// else{
				// 	var jsonText = '{"calltype":"'+callType+'","type":"videochat","msg":"'+body+'","callid":"'+callId+'"}';
				// 	var message = $msg({to : jid, "type" : "normal" }).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
				// 	serverConnection.send(message);
				// }  
				// if($('#conversationList').is(':visible')){
				// 	$("#conversationList ul ").find("#coversationContactTime_"+id).html(msgtime);
				// 	$("#conversationContactMsg_"+id).html("image_"+dateTime+".png");
				// 	$("#conversationContactMsg1_"+id).hide();
				// 	$("#conversationContactMsg_"+id).show();
				// }
				// var newjid = id+'-'+cmeDomain+'-colabus-com';
				// var newchatUi = $("#conversationList ul").find("#" + newjid).clone();
				// 	$("#conversationList ul").find("#" + newjid).remove();
				// 	$("#conversationList ul").prepend(newchatUi);
				// $(obj).find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
				$('#groupmsgtext').css('background-image', 'none');
				// $('#main_commentTextarea').css('background-image','none');	
				$("#groupmsgtext").val('');
				// $("#main_commentTextarea").val('');
				$('#groupmsgtext').attr('placeholder', 'Type message here...');
				imageShre = "";
				//$('.chatpost2').attr('onclick',"sendingMsg(event,this)");	
				cmeMessages();
			}
		});
	}
}

function dataURItoBlobForChat(dataURI) {
	var byteString;
	if (dataURI.split(',')[0].indexOf('base64') >= 0)
		byteString = atob(dataURI.split(',')[1]);
	else
		byteString = unescape(dataURI.split(',')[1]);
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
	var ia = new Uint8Array(byteString.length);
	for (var i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i);
	}
	return new Blob([ia], { type: mimeString });
}

/* *************** Code Ends For clipboard content into CME IM ********************* */

function textTolink(body, msgType) {
	if (body.indexOf("http") >= 0 || body.indexOf("https") >= 0 || body.indexOf("www") >= 0) {
		var temp = "";
		body = body.replace("\n", " \n");
		var dataArray = body.split("\n");
		var subdataArray = "";
		var textColor = "color:white;";
		if (typeof msgType != 'undefined' && msgType == "reply") {
			textColor = "color:inherit";
		}
		for (var j = 0; j < dataArray.length; j++) {
			subdataArray = dataArray[j].split(" ");
			for (var i = 0; i < subdataArray.length; i++) {
				if (subdataArray[i].indexOf("http") >= 0)
					temp += " <a style=\"text-decoration:underline; " + textColor + "\" target=\"_blank\" href=\"" + subdataArray[i] + "\">" + subdataArray[i] + "</a> ";
				else if (subdataArray[i].indexOf("www") >= 0)
					temp += " <a style=\"text-decoration:underline; " + textColor + "\" target=\"_blank\" href=\"http://" + subdataArray[i] + "\">" + subdataArray[i] + "</a> ";
				else if (subdataArray[i].indexOf("https") >= 0)
					temp += " <a style=\"text-decoration:underline; " + textColor + "\" target=\"_blank\" href=\"" + subdataArray[i] + "\">" + subdataArray[i] + "</a> ";
				else
					temp += " " + subdataArray[i];

			} temp += "</br>";
		}

	} else {
		temp = body
	}
	return temp;
}

function cmeCallScreen(){
	let UI ='';
	    
UI =
	'<div id="globalNotification" class="alert alert-success alert-dismissible" style="text-align:center;display:none;position: absolute; z-index: 10000;left:10%;right:10%;margin-top:2%;padding-top: 10px;"></div>'
	+'<div id="notification" style="display:none;position: absolute; z-index: 10000;right:10px;padding-top: 10px;"> </div>'
	+'<div id="ideaTransparent" class="ideaTransparent"></div>'
	
	

	+'<div id="incoming" class="incomingCall container-fluid" style="display: none;">'
		+'<div class="">'
			+'<div style="margin-top: 10vh;" class="" align="center">'
				+'<img id="callImage_1" src="<%=process.env.lighttpdpath%>/userimages/userImage.png" title="" style="height: 120px;cursor: pointer;border-radius: 50%;width: 120px;margin-right: -50px;visibility: hidden;" onerror="userImageOnErrorReplace(this);" class="callSubImages">' 
				+'<img id="callImage" src="<%=process.env.lighttpdpath%>/userimages/userImage.png" title="" style="height: 200px;cursor: pointer;border-radius: 50%;width: 200px;min-width: 100px;position:relative;" onerror="userImageOnErrorReplace(this);" class="">' 
				+'<img id="callImage_2" src="<%=process.env.lighttpdpath%>/userimages/userImage.png" title="" style="height: 120px;cursor: pointer;border-radius: 50%;width: 120px;margin-left: -50px;visibility: hidden;" onerror="userImageOnErrorReplace(this);" class="callSubImages">' 
				
				+'<div style="margin-top: 1vh;" align="center">'
				+'<span style="clear: both;font-size: 14px;color: #fff;">Incoming call</span>'
				+'</div>'
				+'<div style="margin-top: 1vh;" align="center">'
				+'<span id="callConnect" style="clear: both;font-size: 20px;color: #fff;"></span>'
				+'</div>'
			+'</div>'
		+'</div>'
		+'<div class="row" style="" align="center">'
			+'<div style="margin: 10vh auto 0;width: 60%;height: auto;max-width: 450px;">'
				+'<div style="width: 50px;height: auto;float: left;">'
					+'<img class="declineCallImgg" src="images/cme/audioRed.svg" title="Decline" onclick="diclineVideoCall()" style="cursor: pointer;margin-bottom: 8px;width:50px;height:50px;">'
					+'<span style="display: inline;color: #fff;">Decline</span>'
				+'</div>'
				+'<div style="width: 50px;height: auto;float: right;">'
					+'<img class="answerCallImg" src="images/cme/camGreen.svg" title="Accept" onclick="answerCall()" style="cursor: pointer;margin-bottom: 8px;width:50px;height:50px;">'
					+'<span style="display: inline;color: #fff;">Accept</span>'
				+'</div>'
				
			+'</div>'
		+'</div>'
	+'</div>'


	+'<div class="h-100" id="videoContent" style="display: none; margin-left: 15px;margin-right: 15px;">'
		+'<div class="row" style="height: 100%;">'
			+'<div id="chatFor" style="display: none; width: 30%; float: left;"></div>'
			+'<div id="highlightFor" style="display: none; width: 30%; height: 100%; float: right; position: relative;">'
				+'<div id="hlightContainer" style="display: block; width: 100%; height: 100%;">'
					+'<div id="hMainDiv" style="width: 100%; height: 92%;">'
						+'<div style="width: 100%; height: 8%; border-bottom: 1px solid rgb(233, 230, 230); padding: 2%; font-weight: bold; font-size: 13px; display: table;">'
							+'<div style="display: table-cell; vertical-align: middle;">'
								+'<span class="hListHeaderItems" style="margin-top: 0.5vh; float: left;">Meeting Highlights</span>'

							// 	+'<!--+'<span class="hProjHeader" style="margin-top: 0.5vh;float:left;display:none;">'Workspace+'</span>'
							//  +'<img class="hProjHeader" title="Back" onclick="backToHighlights();" style="display:none;cursor: pointer; float: right; height: 18px;    min-height: 18px; min-width: 16px; width: 16px;margin-right: 10px;" src="images/back1.png">'
							//  +'<img class="hListHeaderItems" src="images/video/associate.png" title="Associate to workspace" onclick="getAllProjectsForHighlights()" style="display:none;cursor:pointer;float: right;margin-right: 10px;">'
							//  -->'
								+'<img class="hListHeaderItems" title="Back" onclick="highlightVideo()" style="cursor: pointer; float: right; height: 18px; min-height: 18px; min-width: 16px; width: 16px; margin-right: 10px;" src="images/temp/vback.png">' 
								+'<img class="" src="images/temp/recomd.png" title="Recommendations" onclick="showRecommendations()" style="cursor: pointer; float: right; margin-right: 10px; height: 21px;">'
							+'</div>'
						+'</div>'
						// +'<div id="hListDiv" style="width: 100%; height: 92%; padding: 0px; overflow-y: auto;"></div>'
						// +'<div id="hProjListDiv" style="width: 100%; height: 92%; padding: 10px; overflow-y: auto; display: none;"></div>'
					+'</div>'

					// +'<!-- +'<img class="hListHeaderItems" src="images/video/hadd.png" title="Add" onclick="addNewHighlight()" style="cursor:pointer;float: right;/*! margin-right: 10px; */position: absolute;bottom: 10px;right: 0;z-index:1;">' -->'
					
					// +'<div id="hAddDiv" class="hEditBg" style="width: 100%; height: 164px; padding: 10px; display: none; z-index: 0; bottom: 0; position: absolute; border-top: 1px solid rgb(233, 230, 230);">'
					// 	+'<div style="width: 100%; height: 20px; float: left; margin-bottom: 4px; font-size: 12px;">'
					// 		+'<span id="hTime" style="height: 20px; float: left; width: 50px;"></span>'
					// 		+'<img id="hLightLockTime" style="width: 22px; cursor: pointer; float: left; margin-top: -3px;" title="" onclick="lockHighlightTime();" src="images/temp/timeLocker.png">'
					// 	+'</div>'
					// 	+'<input id="hMsg" highLightMsg="active" placeholder="Enter Highlight" style="width: 100%; border: 0px none; padding: 4px; margin-bottom: 3px; background-color: white; font-size: 12px;" onkeypress="validateAddHLEvent(event)">'
					// 	+'<textarea id="hLightDesc" placeholder="Enter Description" rows="" cols="" style="width: 100%; border: 0px none; height: 60px; background-color: white; font-size: 12px;" onkeypress="validateAddHLEvent(event)"></textarea>'

					// 	+'<input type="hidden" id="hLightHiddenFieldNew" value="hLightCreateNew">'
					// 	+'<div style="margin-top: 5px; display: block; float: right">'
					// 		+'<img id="hLightVoiceStartNew" src="images/temp/glbrecordDefault.png" style="width: 35px; cursor: pointer; float: left; margin-top: -6px;" onclick="recognizationToggleStart("","highLightSpeechType","highLightSpeechType");">'
					// 		+'<img id="hLightVoiceStopNew" src="images/temp/glbrecord.png" style="display: none; width: 35px; cursor: pointer; float: left; margin-top: -6px;" title="Voice" onclick="recognizationToggleStop();">' 
					// 		+'<img id="hLightCancel" style="width: 22px; cursor: pointer; margin-left: 6px;" title="Reset" onclick="cancelCreateHighlight();" src="images/temp/hcancel.png">' 
					// 		+'<img id="hLightSave" style="width: 22px; cursor: pointer; margin-left: 3px;" title="Save" onclick="sendingHighlight("save")" src="images/temp/hupdate.png">'
					// 	+'</div>'
					// +'</div>'
				+'</div>'
				+'<div id="recomdContainer" style="height: 100%; display: none;">'
					+'<div id="" style="width: 100%; height: 100%;">'
						+'<div style="width: 100%; height: 8%; border-bottom: 1px solid rgb(233, 230, 230); padding: 2%; font-weight: bold; font-size: 13px; display: table;">'
							+'<div style="display: table-cell; vertical-align: middle;">'
								+'<span class="" style="margin-top: 0.5vh; float: left;">Recommendations</span>'
								+'<img class="" title="Back" onclick="BackFromRecomd()" style="cursor: pointer; float: right; height: 18px; min-height: 18px; min-width: 16px; width: 16px; margin-right: 10px;" src="images/temp/vback.png">'
							+'</div>'
						+'</div>'
						+'<div id="recomdContainerListDiv" style="width: 100%; height: 92%; padding: 0px; overflow-y: auto;">'
							+'<div class="recomHLcontainer" style="width: 100%; float: left; padding: 5px;">'
								+'<div style="float: left; width: 100%;">'
									+'<img class="" id="" src="images/temp/recomdHL.png" style="cursor: pointer; opacity: 1; height: 20px; width: 20px; float: left;" onclick="ToggleRecomdType(\'HL\',this);" />' 
									+'<span style="float: left; margin-top: 1px; margin-left: 12px; font-size: 13px; font-weight: bold; cursor: pointer; color: #000;" onclick="ToggleRecomdType(\'HL\',this);">Highlights</span>'
								+'</div>'
								+'<div class="recomHListDiv" style="float: left; width: 100%; padding-left: 32px;"></div>'
							+'</div>'
							+'<div class="recomRECcontainer" style="width: 100%; float: left; padding: 5px;">'
								+'<div style="float: left; width: 100%;">'
									+'<img class="" id="" src="images/temp/recomdREC.png" style="cursor: pointer; opacity: 1; height: 20px; width: 20px; float: left;" onclick="ToggleRecomdType(\'REC\',this);" />' 
									+'<span style="float: left; margin-top: 1px; margin-left: 12px; font-size: 13px; font-weight: bold; cursor: pointer; color: #000;" onclick="ToggleRecomdType(\'REC\',this);">Recordings</span>'
								+'</div>'
								+'<div class="recomRECListDiv" style="float: left; width: 100%; padding-left: 32px;display: none;"></div>'
							+'</div>'
							+'<div class="recomWBcontainer" style="width: 100%; float: left; padding: 5px;">'
								+'<div style="float: left; width: 100%;">'
									+'<img class="" id="" src="images/temp/recomdWB.png" style="cursor: pointer; opacity: 1; height: 20px; width: 20px; float: left;" onclick="ToggleRecomdType(\'WB\',this);" />' 
									+'<span style="float: left; margin-top: 1px; margin-left: 12px; font-size: 13px; font-weight: bold; cursor: pointer; color: #000;" onclick="ToggleRecomdType(\'WB\',this);">White Boards</span>'
								+'</div>'
								+'<div class="recomWBListDiv" style="float: left; width: 100%; padding-left: 32px;display: none;"></div>'
							+'</div>'
							+'<div class="recomDOCcontainer" style="width: 100%; float: left; padding: 5px;">'
								+'<div style="float: left; width: 100%;">'
									+'<img class="" id="" src="images/temp/recomdDoc.png" style="cursor: pointer; opacity: 1; height: 20px; width: 20px; float: left;" onclick="ToggleRecomdType(\'DOC\',this);" />' 
									+'<span style="float: left; margin-top: 1px; margin-left: 12px; font-size: 13px; font-weight: bold; cursor: pointer; color: #000;" onclick="ToggleRecomdType(\'DOC\',this);">Documents</span>'
								+'</div>'
								+'<div class="recomDOCListDiv" style="float: left; width: 100%; padding-left: 32px;display: none;"></div>'
							+'</div>'
							+'<div class="recomTASKcontainer" style="width: 100%; float: left; padding: 5px;">'
								+'<div style="float: left; width: 100%;">'
									+'<img class="" id="" src="images/temp/recomdTask.png" style="cursor: pointer; opacity: 1; height: 20px; width: 20px; float: left;" onclick="ToggleRecomdType(\'TASK\',this);" />' 
									+'<span style="float: left; margin-top: 1px; margin-left: 12px; font-size: 13px; font-weight: bold; cursor: pointer; color: #000;" onclick="ToggleRecomdType(\'TASK\',this);">Tasks</span>'
								+'</div>'
								+'<div class="recomTASKListDiv" style="float: left; width: 100%; padding-left: 32px;display: none;"></div>'
							+'</div>'
							+'<div class="recomCONVcontainer" style="width: 100%; float: left; padding: 5px;">'
								+'<div style="float: left; width: 100%;">'
									+'<img class="" id="" src="images/temp/recomdConv.png" style="cursor: pointer; opacity: 1; height: 20px; width: 20px; float: left;" onclick="ToggleRecomdType(\'CONV\',this);" />' 
									+'<span style="float: left; margin-top: 1px; margin-left: 12px; font-size: 13px; font-weight: bold; cursor: pointer; color: #000;" onclick="ToggleRecomdType(\'CONV\',this);">Conversations</span>'
								+'</div>'
								+'<div class="recomCONVListDiv" style="float: left; width: 100%; padding-left: 32px;display: none;"></div>'
							+'</div>'
							+'<div class="recomTScontainer" style="width: 100%; float: left; padding: 5px;">'
								+'<div style="float: left; width: 100%;">'
									+'<img class="" id="" src="images/temp/recomdTrans.png" style="cursor: pointer; opacity: 1; height: 20px; width: 20px; float: left;" onclick="ToggleRecomdType(\'TS\',this);" />' 
									+'<span style="float: left; margin-top: 1px; margin-left: 12px; font-size: 13px; font-weight: bold; cursor: pointer; color: #000;" onclick="ToggleRecomdType(\'TS\',this);">Transcript</span>'
								+'</div>'
								+'<div class="recomTSListDiv" style="float: left; width: 100%; padding-left: 32px;display: none;"></div>'
							+'</div>'
						+'</div>'
					+'</div>'
				+'</div>'

			+'</div>'

			+'<div id="transcriptFor" style="display: none; width: 30%; height: 100%; float: right; position: relative;">'
				+'<div id="transcriptContainer" style="display: block; width: 100%; height: 100%;">'
					// +'<div id="tMainDiv" style="width: 100%; height: 92%;">'
					// 	+'<div style="width: 100%; height: 8%; border-bottom: 1px solid rgb(233, 230, 230); padding: 2%; font-weight: bold; font-size: 13px; display: table;">'
					// 		+'<div style="display: table-cell; vertical-align: middle;">'
					// 			+'<span class="hListHeaderItems" style="margin-top: 0.5vh; float: left;">Meeting Transcript</span>'

					// 			// +'<!--+'<span class="hProjHeader" style="margin-top: 0.5vh;float:left;display:none;">'Workspace+'</span>'
					// 			//  +'<img class="hProjHeader" title="Back" onclick="backToHighlights();" style="display:none;cursor: pointer; float: right; height: 18px;    min-height: 18px; min-width: 16px; width: 16px;margin-right: 10px;" src="images/back1.png">'
					// 			//  +'<img class="hListHeaderItems" src="images/video/associate.png" title="Associate to workspace" onclick="getAllProjectsForHighlights()" style="display:none;cursor:pointer;float: right;margin-right: 10px;">'
					// 			//  -->'

					// 			+'<img class="hListHeaderItems" title="Back" onclick="transcriptVideo()" style="cursor: pointer; float: right; height: 18px; min-height: 18px; min-width: 16px; width: 16px; margin-right: 10px;" src="images/temp/vback.png">' 
					// 			+'<img class="" src="images/temp/recomd.png" title="Recommendations" onclick="showTransRecommendations()" style="cursor: pointer; float: right; margin-right: 10px; height: 21px;">'
					// 		+'</div>'
					// 	+'</div>'
					// 	// +'<div id="tListDiv" style="width: 100%; height: 100%; padding: 0px; overflow-y: auto;"></div>'
					// +'</div>'

				// 	+'<!-- +'<img class="hListHeaderItems" src="images/video/hadd.png" title="Add" onclick="addNewHighlight()" style="cursor:pointer;float: right;/*! margin-right: 10px; */position: absolute;bottom: 10px;right: 0;z-index:1;">' -->'
				// 	+'<!-- +'<div id="tAddDiv" class="hEditBg" style="width: 100%;height: 164px;padding: 10px;display: none;z-index:0;bottom:0;position:absolute;border-top:1px solid rgb(233, 230, 230);">'
				// 	 +'<input id="tMsg" style="width: 100%;border: 0px none;padding: 4px;margin-bottom: 3px;background-color: white;font-size: 12px;" onkeypress="validateAddHLEvent(event)">'
				//   +'</div>' -->'

				+'</div>'
				+'<div id="recomdContainerForTrans" style="height: 100%; display: none;">'
					+'<div id="" style="width: 100%; height: 100%;">'
						+'<div style="width: 100%; height: 8%; border-bottom: 1px solid rgb(233, 230, 230); padding: 2%; font-weight: bold; font-size: 13px; display: table;">'
							+'<div style="display: table-cell; vertical-align: middle;">'
								+'<span class="" style="margin-top: 0.5vh; float: left;">Recommendations</span>'
								+'<img class="" title="Back" onclick="BackFromTransRecomd()" style="cursor: pointer; float: right; height: 18px; min-height: 18px; min-width: 16px; width: 16px; margin-right: 10px;" src="images/temp/vback.png">'
							+'</div>'
						+'</div>'
						// +'<div id="recomdContainerListDivForTrans" style="width: 100%; height: 92%; padding: 0px; overflow-y: auto;">'
						// +'</div>'//edited
					+'</div>'
				+'</div>'

			+'</div>'



			+'<div id="avCallContainer" style="width: 100%; height: 100%; float: left; position: relative;">'
				// +'<div id="recordStatusDiv" style="display: none; z-index: 102; width: 100%; position: absolute; color: #fff; top: 10px; font-weight: bold; text-align: center;">'
				// 	+'<img src="images/temp/rec.gif" /><span>REC</span>'
				// +'</div>'

				+'<div id="videoFor" style="display: none; width: 100%; height: 100%; float: right;">'

						// +'<!-- +'<div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel">'
						// 	 -->'

					+'<span class="arrow" onclick="cmeCallExpandChatFor();chatFor();"  style="display: none;cursor: pointer;margin-right: -34px;/* float: left; *//* margin-top: 17%; *//* display: flex; *//* justify-content: stretch; */float: left;position: absolute;top: 41%;left: -37px;z-index:1 "><img class="arr" src="images/temp/slideboxright.png" style="height:44px;width:41px"></span>'
					+'<div class="row m-0">'
						+'<div id="videocallcontainer" class="compactcme2 col-lg-4 col-md-3 m-0 p-0" style="height: calc(103vh - 82px - 70px);background-color: #082138;">'
							+'<div class="d-flex justify-content-center align-items-center">'
								+'<img class="" src="images/menus/teamzone_white.svg" style="height:20px;width:20px">'
								+'<span id="pcnt" class="pcnt ml-1 mt-1" style="font-size: 14px;color: white;"></span>'
							+'</div>' 
							+'<div id="local-video1" class="compactcme5"  style="display:none;max-width:100%;position:relative;">'
							+'</div>'
						+'</div>'
								// +'<a id="scroll1" onclick="customPrevScroll()" style="display:none;cursor:pointer;top: 60px;opacity: 0.7;left: 8px;color: white;position: absolute;" class="left slide" ><!-- +'<i class="glyphicon glyphicon-chevron-left">' --><img src="images/video/leftarrow.png"></a>'
								//   +'<a id="scroll2" onclick="customNextScroll()" class="right slide" style="display:none;cursor:pointer;top: 60px;opacity: 0.7;right: 8px;color: white;position: absolute;" ><!-- +'<i class="glyphicon glyphicon-chevron-right">' --><img src="images/video/rightarrow.png"></a>'
								
						// +'<!-- //+'</div>' mcs-horizontal-example -->'	
						+'<div id="videocallcontainer3" class="compactcme2 col-lg-4 col-md-6 m-0 p-0" style="display: none;background-color: white;">'
							+'<div class="" style="">'
								+'<button type="button" onclick="closeCallChat();event.stopPropagation();" class="close p-0 AgileCloseButton" style="top: 8px;right: 5px;color: black;outline: none;">&times;</button>'
							+'</div>'
							+'<div id="videocallchat"></div>'
						+'</div>'


						+'<div id="videocallcontainer2" class="compactcme2 col-lg-8 col-md-9 m-0 p-0" style="height: calc(101vh - 82px - 63px);background-color: #OA2844;">'
							// +'<div id="video-content" onmouseover="showvMainOptions()" onmouseleave="hidevMainOptions()"  style="height: 100%; width: 100%; top: 0;">'
							+'<div id="video-content" style="height: 100%; width: 100%; top: 0;">'
								// +'<div id="vtoggle" style="display:none;position: absolute;z-index:100;right:3px">'
								// 	+'<img  src="images/temp/remoteVup.png" style="margin-top:-42px;cursor: pointer; width: 24px;" onclick="toggleRemoteVideo();" title="">'
								// +'</div>'
								+'<div style="width: 100%; height: 100%; float: left; position: relative;">'
									+'<video id="remoteVideo" class="compactcme2" videoId="" muted autoplay style="width: 100%; background: transparent;"></video>'
									

									+'<div id="local-video" align="center" style="z-index: 100; right: 5px; position: absolute; bottom: 5px;">'
										+'<video id="localVideo" autoplay muted style="width: 90px; height: 70px; background: transparent; border: none;"><track kind="captions" src="mycaptions.srt" srclang="en" label="English" default></video>'
										+'<div id="screenShareDiv" onclick="ChangeStream();" style="display: none; cursor: pointer; text-align: center; border: 1px solid rgb(9, 159, 243); background-color: rgb(6, 205, 255); width: 100%; padding: 2px; border-radius: 5px; color: #fff; margin-top: 2px;">Stop sharing</div>'
									+'</div>'

										// +'<!-- +'<div id="videoUserLIst" align="center" style="z-index: 100; right: 5px; position: absolute; bottom: 100px;">'
										// +'<Ul>'list of he user+'</ul>'
										// +'</div>' -->'
								+'</div>'
								+'<div id="" class="transshow acall1 px-3 py-1 mx-auto w-75" style="display: none;height: 45px;color: white;position: absolute;left: 12%;background-color: rgb(0, 0, 0, 0.3) !important;outline: 0;"></div>'//background-color: #323639;

								// +'<div id="vMainOptionsDiv" class="avOptionOver"  align="center" style="width: 100%; text-align: center; bottom: 0px; position: absolute; z-index: 103; margin: 0px auto; background-color:#2B2C4B;padding-bottom: 12px;">'
								// 	+'<div style="height: 8%; width: 90%; background: none; margin: 10px auto; max-width: 700px;" align="center">'
								// 		+'<img class="videoOptionIcons avchatImg " id="vchatImg" defsrc="chat1.png" toglsrc="chat2.png" src="images/temp/chat1.png" style="display:none;cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="chatFor();">' 
								// 		+'<img class="videoOptionIcons vbchatImg " defsrc="chatBack1.png" toglsrc="chatBack1.png" src="images/temp/chatBack1.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="chatBack("on");">' 
								// 		+'<img class="videoOptionIcons " id="vaudioImg" defsrc="mic.png" toglsrc="mic2.png" src="images/temp/mic.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="muteAudio(this);">' 
								// 		+'<img class="videoOptionIcons callHangUp " defsrc="phonedis.svg" src="images/temp/phonedis.svg" style="cursor: pointer;margin-left: 2%;margin-top: 1%;margin-right: 2%;" onclick="hangup();" title="">'
								// 		+'<img class="videoOptionIcons " id="vMoreOptions" defsrc="more.png" toglsrc="more.png" src="images/temp/more.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 10px; opacity: 1;" onclick="showvMoreOptions()">'
								// 		+'<div style="display: inline; position: relative;">'
								// 			+'<div id="vMoreOptionsDiv" style="display: none; position: absolute; left: 0; padding: 8px; top: -265px; width: 50px; background-color: rgba(113, 118, 126, 0.5);">'
								// 				+'<img class="videoOptionIcons avTransImg" id="transcriptImg" defsrc="transcript_two.png" toglsrc="transcript_one.png" src="images/temp/transcript_one.png" style="cursor: pointer;" title="Transcripts" onclick="transcriptVideo();">'
								// 				+'<img class="videoOptionIcons avhglightImg" id="vhighlightImg" defsrc="avHL1.png" toglsrc="avHL2.png" src="images/temp/avHL1.png" style="cursor: pointer; margin-top: 10px;" title="Highlights" onclick="highlightVideo();">' 
								// 				+'<img class="videoOptionIcons" id="vscreenshareImg" defsrc="vscreenshare.png" toglsrc="vscreenshare2.png" src="images/temp/vscreenshare.png" style="cursor: pointer; margin-top: 10px;" title="Screen Share" onclick="ChangeStream(this);">' 
								// 				+'<img class="videoOptionIcons" id="vwhiteboardImg" defsrc="whiteboard.png" toglsrc="whiteboard.png" src="images/temp/whiteboard.png" style="cursor: pointer; margin-top: 10px;" title="Whiteboard" onclick="openWhiteboard();">' 
								// 				+'<img class="videoOptionIcons" id="vrecordImg" defsrc="record1.png" toglsrc="record2.png" src="images/temp/record1.png" style="cursor: pointer; margin-top: 10px;" title="Recording" onclick="recordVideo();">' 
								// 				+'<img class="videoOptionIcons" id="vstreamImg" defsrc="vpause.png" toglsrc="vplay.png" src="images/temp/vpause.png" style="cursor: pointer; margin-top: 10px;" title="Hold" onclick="pauseVideo(this);">' 
								// 				+'<img class="videoOptionIcons" id="vmuteImg" defsrc="vmute1.png" toglsrc="vmute2.png" src="images/temp/vmute1.png" style="cursor: pointer; margin-top: 10px;"  title="Video Pause" onclick="muteVideo(this);">' 
								// 				+'<img class="videoOptionIcons addnewuser" id="" defsrc="addchatuser.png" toglsrc="addchatuser.png" src="images/temp/addchatuser.png" style="display:none;cursor: pointer; margin-top: 10px;" title="Add User" onclick="addConfUsers();">'
								// 			+'</div>'
								// 		+'</div>'
								// 	+'</div>'
								// +'</div>'
								// +'<div class="d-flex justify-content-between align-items-start ml-2" style="position: absolute;z-index: 103;bottom: -12px;background: none;margin: 10px auto;" align="center">'
									
								// 	+'<div class="d-flex justify-content-between mx-1">'
								// 		+'<img src="images/temp/upload.png" style="width: 14px; height: 14px; float: left;margin-top: 7px;">'
								// 		+'<div id="rttspeedvideo" style="color: #fff;white-space: nowrap;font-weight: bold;margin-top: 0px;padding: 5px;"></div>'
								// 	+'</div>'
								
								// 	+'<div class="d-flex justify-content-between mx-1">'
								// 		+'<img src="images/temp/download.png" style=" width: 14px; height: 14px; float: left;margin-top: 7px;">'
								// 		+'<div id="ownspeedvideo" style="color: #fff;white-space: nowrap;font-weight: bold;margin-top: 0px;padding: 5px;"></div>'
								// 	+'</div>'

								// 	+'<div class="avTimer mx-1" id="vTimer" style="float: left;color: #fff;font-weight: bold;padding: 5px;"></div>'
								// 	+'<div class ="remotevideovoice" style="float: left;color: #fff;font-weight: bold;margin-top: 0px;padding: 5px; margin-top: -3px;">'
								// 		+'<div class="meterGraph" >'
								// 			+'<div class="meter" >'
								// 			+'</div>'
								// 		+'</div>'
								// 	+'</div>'
								// +'</div>'
							+'</div>'
						+'</div>'

						+'<div id="vMainOptionsDiv" class="avOptionOver d-flex justify-content-between align-items-center" align="center" style="width: 100%; text-align: center; bottom: 0px; position: absolute; z-index: 103; margin: 0px auto;background-color: #002432;">'
							+'<div class="p-2">'
								+'<img class=" videoOptionIcons mx-2 addnewuser"  defsrc="addchatuser.svg" toglsrc="addchatuser2.svg" src="images/cme/addchatuser.svg" style="display:none;cursor: pointer;" title="Add User" onclick="addConfUsers();">'
								+'<img class="expandcall videoOptionIcons mx-2"  defsrc="chatBack.svg" toglsrc="chatBack1.svg" src="images/cme/chatBack.svg" style="cursor: pointer;" title="Chat" onclick="cmeCallExpandChatFor();chatInCall();">'
								+'<img class="expandcall videoOptionIcons mx-2"  defsrc="risehand.svg" toglsrc="risehand.svg" src="images/cme/risehand.svg" style="cursor: pointer;" title="Raise Hand" onclick="">'
								+'<img class="expandcall videoOptionIcons mx-2" id="vrecordImg" defsrc="record1.svg" toglsrc="record2.svg" src="images/cme/record1.svg" style="cursor: pointer;" title="Recording" onclick="recordVideo();">' 
								+'<img class="expandcall videoOptionIcons mx-2 ascreenshareImg" id="ascreenshareImg" defsrc="vscreenshare.svg" toglsrc="screen2.svg" src="images/cme/vscreenshare.svg" style="cursor: pointer;" title="Screen Share" onclick="ChangeStream(this);">' 
								+'<img class="videoOptionIcons mx-2 " id="vMoreOptions" defsrc="more.svg" title="More" toglsrc="more.svg" src="images/cme/more.svg" style="cursor: pointer; margin-left: 2%; opacity: 1;" onclick="showvMoreOptions()">'
								+'<div style="display: inline; position: relative;">'
									+'<div id="vMoreOptionsDiv" class="rounded flex-column align-items-center " style="display: none; position: absolute; left: 0; bottom: 4px; width: 40px; background-color: rgba(113, 118, 126, 0.5);">'
										+'<img class="videoOptionIcons my-1 avTransImg" id="transcriptImg" defsrc="transscript.svg" toglsrc="transscript2.svg" src="images/cme/transscript.svg" style="cursor: pointer;" title="Transcripts" onclick="transcriptVideo();">' 
										+'<img class="videoOptionIcons my-1 avhglightImg" id="ahighlightImg" defsrc="avHL1.svg" toglsrc="avHL2.svg" src="images/cme/avHL1.svg" style="cursor: pointer;" title="Highlights" onclick="highlightVideo();">' 
										+'<img class="videoOptionIcons my-1" id="awhiteboardImg" defsrc="whiteboard.svg" toglsrc="whiteboard2.svg" src="images/cme/whiteboard.svg" style="cursor: pointer;" title="Whiteboard" onclick="cmeCallExpandChatFor();openWhiteboard();">' 
										// +'<img class="videoOptionIcons my-1" id="arecordImg" defsrc="record1.png" toglsrc="record2.png" src="images/cme/record1.png" style="cursor: pointer;" title="Recording" onclick="recordVideo();">' 
										// +'<img class="videoOptionIcons my-1" id="" defsrc="vpause.png" toglsrc="vplay.png" src="images/cme/vpause.png" style="cursor: pointer;" title="Hold" onclick="pauseAudio(this);">' 
										+'<img class="videoOptionIcons my-1 "  defsrc="recomd1.svg" toglsrc="recomd2.svg" src="images/cme/recomd1.svg" style="cursor: pointer;" title="Recommendations" onclick="cmehighlightVideo();highlightVideo();showRecommendationsInChat();">'

											+'<img class="compactcall videoOptionIcons my-1"  defsrc="chatBack.svg" toglsrc="chatBack1.svg" src="images/cme/chatBack.svg" style="cursor: pointer;" title="Chat" onclick="cmeCallExpandChatFor();chatInCall();">'
											+'<img class="compactcall videoOptionIcons my-1"  defsrc="risehand.svg" toglsrc="risehand.svg" src="images/cme/risehand.svg" style="cursor: pointer;" title="Raise Hand" onclick="">'
											+'<img class="compactcall videoOptionIcons my-1" id="" defsrc="record1.svg" toglsrc="record2.svg" src="images/cme/record1.svg" style="cursor: pointer;" title="Recording" onclick="recordVideo();">' 
											+'<img class="compactcall videoOptionIcons my-1 ascreenshareImg" id="ascreenshareImg" defsrc="vscreenshare.svg" toglsrc="screen2.svg" src="images/cme/vscreenshare.svg" style="cursor: pointer;" title="Screen Share" onclick="ChangeStream(this);">' 
										
									+'</div>'
								+'</div>'
							+'</div>'	
							+'<div class="p-2 rounded " style="background-color: #071821;">'
								+'<img class="videoOptionIcons mr-2 " id="aaudioImg" defsrc="mute1.svg" toglsrc="mute2.svg" src="images/cme/mute1.svg" style="cursor: pointer;" onclick="muteAudio(this);">' 
								+'<img class="videoOptionIcons mr-2" id="vmuteImg" defsrc="vmute1.svg" toglsrc="vmute2.svg" src="images/cme/vmute1.svg" style="cursor: pointer;"  title="Video Pause" onclick="muteVideo(this);">' 
								+'<img class="videoOptionIcons callHangUp " defsrc="phonedis.svg" src="images/cme/phonedis.svg" style="cursor: pointer;" onclick="hangup();">'
								+'<img class="videoOptionIcons ml-2" id="vstreamImg" defsrc="vpause.svg" toglsrc="vplay.svg" src="images/cme/vpause.svg" style="cursor: pointer;" title="Hold" onclick="pauseAudio(this);">' 
							+'</div>'
							+'<div id="" class="p-2">'
								+'<img class="videoOptionIcons mr-2" id="" onmouseover="showspeedud2()" onmouseleave="hidespeedud2()"  defsrc="volmeter.svg" toglsrc="volmeter.svg" src="images/cme/volmeter.svg" style="cursor: pointer;" title="" onclick="">' 
								+'<div id="udspeed2" class=" align-items-center rounded px-1" style="display: none;position: absolute;right: 30px;bottom: 50px;background-color: #316381">'
									+'<img class="mr-1" src="images/cme/bandw_up.svg" style="width: 15px;  height: 15px;">'
									+'<div id="rttspeedvideo" style="color: #fff;white-space: nowrap;font-weight: bold;"></div>'
									+'<img class="ml-2 mr-1" src="images/cme/bandw_down.svg" style=" width: 15px;  height: 15px;">'
									+'<div id="ownspeedvideo" style="color: #fff;white-space: nowrap;font-weight: bold;"></div>'
								+'</div>'
										
											
										
								+'<div class="avTimer mr-2 float-right" id="" style="color: #fff;font-weight: bold;padding: 5px;"></div>'

							+'</div>'
							// +'<div style="height: 8%; width: 90%; background: none; margin: 10px auto; max-width: 700px;" align="center">'
							// 	+'<img class="videoOptionIcons avchatImg " id="achatImg" defsrc="chat1.png" toglsrc="chat2.png" src="images/temp/chat1.png" style="display:none;cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="chatFor();">' 
							// 	+'<img class="videoOptionIcons vbchatImg " defsrc="chatBack1.png" toglsrc="chatBack1.png" src="images/temp/chatBack1.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="chatBack("on");">' 
							// 	+'<img class="videoOptionIcons " id="aaudioImg" defsrc="mic.png" toglsrc="mic2.png" src="images/temp/mic.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="muteAudio(this);">' 
							// 	+'<img class="videoOptionIcons callHangUp " defsrc="phonedis.svg" src="images/temp/phonedis.svg" style="cursor: pointer;margin-left: 2%;margin-top: 1%;margin-right: 2%;" onclick="hangup();">'
							// 	+'<img class="videoOptionIcons " id="aMoreOptions" defsrc="more.png" toglsrc="more.png" src="images/temp/more.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 10px; opacity: 1;" onclick="showvMoreOptions()">'
							// 	+'<div style="display: inline; position: relative;">'
							// 		+'<div id="aMoreOptionsDiv" style="display: none; position: absolute; left: 0; padding: 8px; top: -240px; width: 50px; background-color: rgba(113, 118, 126, 0.5);">'
							// 			+'<img class="videoOptionIcons avTransImg" id="transcriptImg" defsrc="transcript_two.png" toglsrc="transcript_one.png" src="images/temp/transcript_one.png" style="cursor: pointer;" title="Transcripts" onclick="transcriptVideo();">' 
							// 			+'<img class="videoOptionIcons avhglightImg" id="ahighlightImg" defsrc="avHL1.png" toglsrc="avHL2.png" src="images/temp/avHL1.png" style="cursor: pointer; margin-top: 10px;" title="Highlights" onclick="highlightVideo();">' 
							// 			+'<img class="videoOptionIcons" id="ascreenshareImg" defsrc="vscreenshare.png" toglsrc="vscreenshare2.png" src="images/temp/vscreenshare.png" style="cursor: pointer; margin-top: 10px;" title="Screen Share" onclick="ChangeStream(this);">' 
							// 			+'<img class="videoOptionIcons" id="awhiteboardImg" defsrc="whiteboard.png" toglsrc="whiteboard.png" src="images/temp/whiteboard.png" style="cursor: pointer; margin-top: 10px;" title="Whiteboard" onclick="openWhiteboard();">' 
							// 			+'<img class="videoOptionIcons" id="arecordImg" defsrc="record1.png" toglsrc="record2.png" src="images/temp/record1.png" style="cursor: pointer; margin-top: 10px;" title="Recording" onclick="recordVideo();">' 
							// 			+'<img class="videoOptionIcons" id="astreamImg" defsrc="vpause.png" toglsrc="vplay.png" src="images/temp/vpause.png" style="cursor: pointer; margin-top: 10px;" title="Hold" onclick="pauseAudio(this);">' 
							// 			+'<img class="videoOptionIcons addnewuser"  defsrc="addchatuser.png" toglsrc="addchatuser.png" src="images/temp/addchatuser.png" style="display:none;cursor: pointer; margin-top: 10px;" title="Add User" onclick="addConfUsers();">'
							// 		+'</div>'
							// 	+'</div>'
							// +'</div>'
						+'</div>'


					+'</div>'
				+'</div>'

				// +'<div id="audioFor" onmouseover="showvMainOptions()" onmouseleave="hidevMainOptions()" style="display: none; width: 100%; height: 100%; top: 0; float: right;">'
				+'<div id="audioFor" style="display: none; width: 100%; height: 100%; top: 0; float: right;">'
					+'<span class="arrow" onclick="cmeCallExpandChatFor();chatFor();"  style="display: none;cursor: pointer;margin-right: -34px;/* float: left; *//* margin-top: 17%; *//* display: flex; *//* justify-content: stretch; */float: left;position: absolute;top: 41%;left: -37px;z-index:1 ">'
					+'<img class="arr" src="images/temp/slideboxright.png" style="height:44px;width:41px"></span>'

					+'<div class="row m-0">'
						+'<div id="cmecallcontainer" class="compactcme2 col-lg-3 col-md-2 m-0 p-0" style="background-color: #082138;">'
							+'<div class="d-flex justify-content-center align-items-center">'
								+'<img class="" src="images/menus/teamzone_white.svg" style="height:20px;width:20px">'
								+'<span id="pcnt" class="pcnt ml-1 mt-1" style="font-size: 14px;color: white;"></span>'
							+'</div>' 	
							+'<div id="remote-audio" class="compactcme6 wsScrollBar" align="center" style="">'
								
								// +'<!-- +'<div id="local-video2" class="carousel-inner">'
								// +'</div>'
								// +'<a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>'
								//   +'<a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>'
								//  -->'

							+'</div>'
							// +'<a id="ascroll1" onclick="customPrevScroll()" style="display:none;cursor:pointer;top: 60px;opacity: 0.7;left: 8px;color: white;position: absolute;z-index:1;" class="left " >'//+'<!-- +'<i class="glyphicon glyphicon-chevron-left"></i>' -->'
							// +'<img src="images/temp/leftarrow.png"></a>'
							// +'<a id="ascroll2" onclick="customNextScroll()" class="right " style="display:none;cursor:pointer;top: 60px;opacity: 0.7;right: 8px;color: white;position: absolute;z-index:1;" >'//+'<!-- +'<i class="glyphicon glyphicon-chevron-right"></i>' -->'
							// +'<img src="images/temp/rightarrow.png"></a>'
										
							// +'<div id="atoggle" style="display:none;position: absolute;z-index:100;right:3px">'
							// 	+'<img  src="images/temp/remoteVup.png" style="margin-top:-42px;cursor: pointer; width: 24px;" onclick="toggleRemoteAudio();" title="">'
							// +'</div>'
						+'</div>'

						+'<div id="cmecallcontainer3" class="compactcme2 col-lg-4 col-md-6 m-0 p-0" style="height: calc(100vh - 50px - 84px);display: none;background-color: white;">'
							+'<div class="" style="">'
								+'<button type="button" onclick="closeCallChat();event.stopPropagation();" class="close p-0 AgileCloseButton" style="top: 8px;right: 5px;color: black;outline: none;">&times;</button>'
							+'</div>'
							+'<div id="audiocallchat"></div>'
							
						+'</div>'

						+'<div id="cmecallcontainer2" class="compactcme2 col-lg-9 col-md-10" style="">'
							+'<div id="recordStatusDiv" style="display: none; z-index: 102; width: 100%; position: absolute; color: #fff; top: 10px; font-weight: bold; text-align: center;">'
								+'<img src="images/temp/rec.gif" /><span>REC</span>'
							+'</div>'
							+'<video id="aRemoteVideo" audioId="" autoplay style="display: none; width: 100%; height: 100%; background: transparent;"></video>'
							// +'<div id="cmetscontainer" style="position: absolute;top: 518px;left: 70%;width: 29%;"></div>'
							+'<div id="aRemoteImage" style="margin-top: 9vh;" class="" align="center">'
							
								// +'<!-- 	+'<div style=" margin-top: -40px;display:none" id="showhost">'
								// 	 +'<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="/images/myprojects.png" projecttype="my_project" projectid="6031" >'
								//  +'</div>' -->'
								// +'<img id="sscheck" class="cursor" onclick="" title="Screen Share" src="' + path + '/images/temp/share.svg" style="display: none;width: 16px;position: absolute;margin-left: 18px;">' 
								// +'<img id="wbcheck" class="cursor" onclick="" title="White Board" src="' + path + '/images/temp/whiteboard.svg" style="display: none;width: 16px;position: absolute;margin-left: 26px;margin-top: 18px;">' 
								+'<span id="mutecheck" title="Audio is muted" style="display: none;">'+
									'<img style="margin-left: 191px;margin-top: 35px;width: 30px;position: absolute;" src="' + path + '/images/cme/mute.svg">'+
								'</span>'
								
								+'<img class="callConnectImage" src="" title="" style="border-style: hidden;height: 220px; cursor: pointer; border-radius: 50%; width: 220px; min-width: 100px;" onerror="userImageOnErrorReplace(this);">'
								+'<div style="margin-top: 1vh;" align="center">'
									
									
									+'<span class="callerScreenStatus" style="clear: both; font-size: 14px; color: #fff;"></span>'
								+'</div>'
								+'<div style="margin-top: 1vh;" align="center">'
									+'<span class="callConnectText" style="clear: both; font-size: 20px; color: #fff;"></span>'
								+'</div>'
								
								// +'<!--  +'<div style="margin-top: 1vh;" align="center">'
								// 		+'<div class="avTimer" id="aTimer" style="color: #fff;font-weight: bold;margin-top: 5px;padding: 5px;/*! background-color: rgba(113, 118, 126, 0.5); */max-width: 100px;"></div>'
								// 	  +'</div>'-->'

							+'</div>'
							+'<div id="tListDiv" class="transshow defaultScrollDiv acall1 px-3 py-1 mx-auto w-75" style="display: none;overflow-y: hidden;height: 45px;color: white;position: absolute;left: 12%;background-color: rgb(0, 0, 0, 0.3) !important;outline: 0;"></div>'//background-color: #323639;
						+'</div>'
					+'</div>'
					+'<div id="aMainOptionsDiv" class="avOptionOver d-flex justify-content-between align-items-center" align="center" style="width: 100%; text-align: center; bottom: 0px; position: absolute; z-index: 103; margin: 0px auto;background-color: #002432;">'
						+'<div class="p-2">'
							+'<img class=" videoOptionIcons mx-2 addnewuser"  defsrc="addchatuser.svg" toglsrc="addchatuser2.svg" src="images/cme/addchatuser.svg" style="display:none;cursor: pointer;" title="Add User" onclick="addConfUsers();">'
							+'<img class="expandcall videoOptionIcons mx-2"  defsrc="chatBack.svg" toglsrc="chatBack1.svg" src="images/cme/chatBack.svg" style="cursor: pointer;" title="Chat" onclick="cmeCallExpandChatFor();chatInCall();">'
							+'<img class="expandcall videoOptionIcons mx-2"  defsrc="risehand.svg" toglsrc="risehand.svg" src="images/cme/risehand.svg" style="cursor: pointer;" title="Raise Hand" onclick="">'
							+'<img class="expandcall videoOptionIcons mx-2" id="arecordImg" defsrc="record1.svg" toglsrc="record2.svg" src="images/cme/record1.svg" style="cursor: pointer;" title="Recording" onclick="recordVideo();">' 
							+'<img class="expandcall videoOptionIcons ascreenshareImg mx-2" id="ascreenshareImg" defsrc="vscreenshare.svg" toglsrc="screen2.svg" src="images/cme/vscreenshare.svg" style="cursor: pointer;" title="Screen Share" onclick="ChangeStream(this);">' 
							+'<img class="videoOptionIcons mx-2 " id="aMoreOptions" defsrc="more.svg" title="More" toglsrc="more.svg" src="images/cme/more.svg" style="cursor: pointer; margin-left: 2%; opacity: 1;" onclick="showvMoreOptions()">'
							+'<div style="display: inline; position: relative;">'
								+'<div id="aMoreOptionsDiv" class="rounded flex-column align-items-center " style="display: none; position: absolute; left: 0; bottom: 4px; width: 40px; background-color: rgba(113, 118, 126, 0.5);">'
									+'<img class="videoOptionIcons my-1 avTransImg" id="transcriptImg" defsrc="transscript.svg" toglsrc="transscript2.svg" src="images/cme/transscript.svg" style="cursor: pointer;" title="Transcripts" onclick="transcriptVideo();">' 
									+'<img class="videoOptionIcons my-1 avhglightImg" id="ahighlightImg" defsrc="avHL1.svg" toglsrc="avHL2.svg" src="images/cme/avHL1.svg" style="cursor: pointer;" title="Highlights" onclick="highlightVideo();">' 
									+'<img class="videoOptionIcons my-1" id="awhiteboardImg" defsrc="whiteboard.svg" toglsrc="whiteboard2.svg" src="images/cme/whiteboard.svg" style="cursor: pointer;" title="Whiteboard" onclick="cmeCallExpandChatFor();openWhiteboard();">' 
									// +'<img class="videoOptionIcons my-1" id="arecordImg" defsrc="record1.png" toglsrc="record2.png" src="images/cme/record1.png" style="cursor: pointer;" title="Recording" onclick="recordVideo();">' 
									// +'<img class="videoOptionIcons my-1" id="" defsrc="vpause.png" toglsrc="vplay.png" src="images/cme/vpause.png" style="cursor: pointer;" title="Hold" onclick="pauseAudio(this);">' 
									+'<img class="videoOptionIcons my-1 "  defsrc="recomd1.svg" toglsrc="recomd2.svg" src="images/cme/recomd1.svg" style="cursor: pointer;" title="Recommendations" onclick="cmehighlightVideo();highlightVideo();showRecommendationsInChat();">'

										+'<img class="compactcall videoOptionIcons my-1"  defsrc="chatBack.svg" toglsrc="chatBack1.svg" src="images/cme/chatBack.svg" style="cursor: pointer;" title="Chat" onclick="cmeCallExpandChatFor();chatInCall();">'
										+'<img class="compactcall videoOptionIcons my-1"  defsrc="risehand.svg" toglsrc="risehand.svg" src="images/cme/risehand.svg" style="cursor: pointer;" title="Raise Hand" onclick="">'
										+'<img class="compactcall videoOptionIcons my-1" id="arecordImg" defsrc="record1.svg" toglsrc="record2.svg" src="images/cme/record1.svg" style="cursor: pointer;" title="Recording" onclick="recordVideo();">' 
										+'<img class="compactcall videoOptionIcons ascreenshareImg my-1" id="ascreenshareImg" defsrc="vscreenshare.svg" toglsrc="screen2.svg" src="images/cme/vscreenshare.svg" style="cursor: pointer;" title="Screen Share" onclick="ChangeStream(this);">' 
									
								+'</div>'
							+'</div>'
						+'</div>'	
						+'<div class="p-2 rounded " style="background-color: #071821;">'
							+'<img class="videoOptionIcons mr-2 " id="aaudioImg" defsrc="mute1.svg" toglsrc="mute2.svg" src="images/cme/mute1.svg" style="cursor: pointer;" onclick="muteAudio(this);">' 
							// +'<img class="videoOptionIcons mx-2 " id="" title="video" defsrc="vedioGreen.png" toglsrc="" src="images/cme/vedioGreen.png" style="cursor: pointer;" onclick="">' 
							+'<img class="videoOptionIcons callHangUp " defsrc="phonedis.svg" src="images/cme/phonedis.svg" style="cursor: pointer;" onclick="hangup();">'
							+'<img class="videoOptionIcons ml-2" id="" defsrc="vpause.svg" toglsrc="vplay.svg" src="images/cme/vpause.svg" style="cursor: pointer;" title="Hold" onclick="pauseAudio(this);">' 
							// +'<img class="videoOptionIcons mx-2" id="astreamImg" defsrc="vpause.png" toglsrc="vplay.png" src="images/cme/vpause.png" style="cursor: pointer;" title="Hold" onclick="pauseAudio(this);">' 
						+'</div>'
						+'<div class="p-2">'
							+'<img class="videoOptionIcons mr-2" id="" onmouseover="showspeedud()" onmouseleave="hidespeedud()"  defsrc="volmeter.svg" toglsrc="volmeter.svg" src="images/cme/volmeter.svg" style="cursor: pointer;" title="" onclick="">' 
							+'<div id="udspeed" class=" align-items-center rounded px-1" style="display: none;position: absolute;right: 30px;bottom: 50px;background-color: #316381">'
								+'<img class="mr-1" src="images/cme/bandw_up.svg" style="width: 15px;  height: 15px;">'
								+'<div id="rttspeedaudio" style="color: #fff;white-space: nowrap;font-weight: bold;"></div>'
								+'<img class="ml-2 mr-1" src="images/cme/bandw_down.svg" style=" width: 15px;  height: 15px;">'
								+'<div id="ownspeedaudio" style="color: #fff;white-space: nowrap;font-weight: bold;"></div>'
							+'</div>'
									
							+'<div class="avTimer mr-2 float-right" id="aTimer" style="color: #fff;font-weight: bold;padding: 5px;"></div>'

						+'</div>'
						// +'<div style="height: 8%; width: 90%; background: none; margin: 10px auto; max-width: 700px;" align="center">'
						// 	+'<img class="videoOptionIcons avchatImg " id="achatImg" defsrc="chat1.png" toglsrc="chat2.png" src="images/temp/chat1.png" style="display:none;cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="chatFor();">' 
						// 	+'<img class="videoOptionIcons vbchatImg " defsrc="chatBack1.png" toglsrc="chatBack1.png" src="images/temp/chatBack1.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="chatBack("on");">' 
						// 	+'<img class="videoOptionIcons " id="aaudioImg" defsrc="mic.png" toglsrc="mic2.png" src="images/temp/mic.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="muteAudio(this);">' 
						// 	+'<img class="videoOptionIcons callHangUp " defsrc="phonedis.svg" src="images/temp/phonedis.svg" style="cursor: pointer;margin-left: 2%;margin-top: 1%;margin-right: 2%;" onclick="hangup();">'
						// 	+'<img class="videoOptionIcons " id="aMoreOptions" defsrc="more.png" toglsrc="more.png" src="images/temp/more.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 10px; opacity: 1;" onclick="showvMoreOptions()">'
						// 	+'<div style="display: inline; position: relative;">'
						// 		+'<div id="aMoreOptionsDiv" style="display: none; position: absolute; left: 0; padding: 8px; top: -240px; width: 50px; background-color: rgba(113, 118, 126, 0.5);">'
						// 			+'<img class="videoOptionIcons avTransImg" id="transcriptImg" defsrc="transcript_two.png" toglsrc="transcript_one.png" src="images/temp/transcript_one.png" style="cursor: pointer;" title="Transcripts" onclick="transcriptVideo();">' 
						// 			+'<img class="videoOptionIcons avhglightImg" id="ahighlightImg" defsrc="avHL1.png" toglsrc="avHL2.png" src="images/temp/avHL1.png" style="cursor: pointer; margin-top: 10px;" title="Highlights" onclick="highlightVideo();">' 
						// 			+'<img class="videoOptionIcons" id="ascreenshareImg" defsrc="vscreenshare.png" toglsrc="vscreenshare2.png" src="images/temp/vscreenshare.png" style="cursor: pointer; margin-top: 10px;" title="Screen Share" onclick="ChangeStream(this);">' 
						// 			+'<img class="videoOptionIcons" id="awhiteboardImg" defsrc="whiteboard.png" toglsrc="whiteboard.png" src="images/temp/whiteboard.png" style="cursor: pointer; margin-top: 10px;" title="Whiteboard" onclick="openWhiteboard();">' 
						// 			+'<img class="videoOptionIcons" id="arecordImg" defsrc="record1.png" toglsrc="record2.png" src="images/temp/record1.png" style="cursor: pointer; margin-top: 10px;" title="Recording" onclick="recordVideo();">' 
						// 			+'<img class="videoOptionIcons" id="astreamImg" defsrc="vpause.png" toglsrc="vplay.png" src="images/temp/vpause.png" style="cursor: pointer; margin-top: 10px;" title="Hold" onclick="pauseAudio(this);">' 
						// 			+'<img class="videoOptionIcons addnewuser"  defsrc="addchatuser.png" toglsrc="addchatuser.png" src="images/temp/addchatuser.png" style="display:none;cursor: pointer; margin-top: 10px;" title="Add User" onclick="addConfUsers();">'
						// 		+'</div>'
						// 	+'</div>'
						// +'</div>'
					+'</div>'	
					// +'<div class="d-flex justify-content-between align-items-start ml-2" style="position: absolute;z-index: 103;bottom: -12px;background: none;margin: 10px auto;min-width: 300px;" align="center">'
							
					// 		// +'<!-- +'<div id="rttspeedaudio" style="position: absolute;color: #fff;/* display: flex; */float: left; margin-left: -26px; position: ;/* float: left; *//* color: #fff; */font-weight: bold;margin-top: 0px;padding: 5px;"></div>'	 -->'
					// 		+'<div class="d-flex justify-content-between mx-1">'
					// 			+'<img src="images/temp/upload.png" style="width: 14px;  height: 14px; float: left;margin-top: 7px;">'
					// 			+'<div id="rttspeedaudio" style="color: #fff;white-space: nowrap;font-weight: bold;margin-top: 0px;padding: 5px;"></div>'
					// 		+'</div>'
							
					// 		+'<div class="d-flex justify-content-between mx-1">'
					// 			+'<img src="images/temp/download.png" style=" width: 14px;  height: 14px; float: left;margin-top: 7px;">'
					// 			+'<div id="ownspeedaudio" style="color: #fff;white-space: nowrap;font-weight: bold;margin-top: 0px;padding: 5px;"></div>'
					// 		+'</div>'
					// 		// +'<!-- +'<div id="ownspeedaudio" style="position: absolute;color: #fff;/* display: flex; */float: left; margin-left: 53px; position: ;/* float: left; *//* color: #fff; */font-weight: bold;margin-top: 0px;padding: 5px;"></div>'	
					// 		//  -->'
					// 		+'<div class="avTimer mx-1" id="aTimer" style="float: left;color: #fff;font-weight: bold;padding: 5px;"></div>'
					// 		+'<div class ="remotevideovoice" style="float: left;color: #fff;font-weight: bold;margin-top: 0px;padding: 5px;margin-top: -3px;">'
					// 			+'<div class="meterGraph" >'
					// 				+'<div class="meter" >'
					// 				+'</div>'
					// 			+'</div>'
					// 		+'</div>'
					// +'</div>'
					
					
				+'</div>'
				// +'<div id="avStatusDiv" style="width: 100%; text-align: center; position: absolute; top: 160px; right: 0; color: #fff; z-index: 102;">'
				// 	+'<span id="aStatusSpan" ></span>' 
				// 	+'<span id="sStatusSpan" "></span>' 
				// 	+'<span id="vStatusSpan" ></span>' 
				// 	+'<span id="vSwitch" ></span>'
				// +'</div>'

				+'<div id="screenFor" style="display: none; width: 100%; height: 100%; float: right;">'
					+'<div id="screen-content" style="height: 100%; width: 100%; top: 0;">'
						+'<div style="width: 100%; height: 100%; float: left; position: relative;">'
							+'<video id="sharedVideo" autoplay style="width: 100%; height: 100%; background: transparent;"></video>'
							+'<div style="text-align: right; top: 80px; right: 30px; position: absolute;">'
								+'<img class="callHangUp" src="images/cme/phonedis.svg" style="cursor: pointer;height:45px; width: 45px;" onclick="hangup();" title="">'
							+'</div>'
							+'<div style="width: 100%; text-align: right; right: 27px; top: 20px; position: absolute;">'
								+'<img id="screenUserImg" src="" style="cursor: pointer; width: 50px; height: 50px; border-radius: 50px" onerror="userImageOnErrorReplace(this);" title="">'
							+'</div>'
							+'<div style="text-align: right; top: 135px; right: 37px; position: absolute;" align="center">'
								
								// +'<!--  +'<img class="videoOptionIcons" id="vmuteImg" defsrc ="vmute1.png" toglsrc="vmute2.png" src="images/video/vmute1.png" style="cursor:pointer; margin-left: 2%; margin-top: 1%;margin-right: 2%;" onclick="muteVideo(this);">'
								//  +'<img class="videoOptionIcons avchatImg" id="vchatImg" defsrc ="chat1.png" toglsrc="chat2.png" src="images/video/chat1.png" style="cursor:pointer; margin-left: 2%; margin-top: 1%;margin-right: 2%;" onclick="chatFor();">'
								//  +'<img class="videoOptionIcons" id="vstreamImg" defsrc ="vpause.png" toglsrc="vplay.png" src="images/video/vpause.png" style="cursor:pointer; margin-left: 2%; margin-top: 1%;margin-right: 2%;" onclick="pauseVideo(this);">'
								//  +'<img class="videoOptionIcons" id="vrecordImg" defsrc ="record1.png" toglsrc="record2.png" src="images/video/record1.png" style="cursor:pointer; margin-left: 2%; margin-top: 1%;margin-right: 2%;" onclick="recordVideo();">'
								//  -->'

								+'<img class="videoOptionIcons" id="vaudioImg" defsrc="mic.png" toglsrc="mic2.png" src="images/temp/mic.png" style="cursor: pointer; margin-left: 2%; margin-top: 1%; margin-right: 2%;" onclick="muteAudio(this);">'
							+'</div>'
							+'<div id="share-video" align="center" style="width: 100%; margin-top: 2.5vh; z-index: 1000; bottom: 10px; left: 0px;">'
								+'<video id="shareVideo" autoplay muted style="width: 180px; height: 100px; background: transparent; border: none;"></video>'
							+'</div>'

						+'</div>'
					+'</div>'
				+'</div>'
				+'<div id="whiteboardFor" style="display: none; width: 100%; height: 100%; float: right; position: absolute; z-index: 1041; background-color: #fff;">'
					+'<div style=" position: absolute; left:8px; /*height: calc(100vh - 50px);*/ " align="center">'
						+'<div class="d-flex position-relative justify-content-between align-items-center">'
						   +'<div style="background-color: #fff;border-radius: 4px;box-shadow: 0 6px 12px rgb(0 0 0 / 18%); left:55px;" class="d-flex align-items-center mt-2 p-2 position-absolute">'
								+'<img class="mr-2" style="cursor: pointer;width: 24px;height: 24px;" title="Undo" onclick="undoWB();" src="images/cme/wboard/undo.svg">' 
								+'<img class="mr-2" style="cursor: pointer;width: 24px;height: 24px;" title="Clear All" onclick="undoAllWB();" src="images/cme/wboard/clearAll.svg">' 
								+'<img class="mr-2" style="cursor: pointer;width: 24px;height: 24px;" title="Save As Image" onclick="saveWB();" src="images/cme/wboard/download.svg">' 
								+'<img class="mr-2" style="cursor: pointer;width: 24px;height: 24px;" title="Show Saved Images" onclick="showWBImageList();" src="images/cme/wboard/listImage.svg">' 
								+'<img class="mr-1" id="stopWBbutton" style="cursor: pointer;width: 24px;height: 24px;" title="Stop Whiteboard" onclick="stopwbalert();" src="images/cme/wboard/stopwb.svg">' 
								+'<a id="link-to-image" target="_blank" style="display: none;"></a>'
							+'</div>'	
						    +'<video id="wbVideo" autoplay class="mt-2" style="width: 100%; height: 50px; background: transparent; cursor: pointer;" onclick="toggleWhiteBoard()"></video>'
							+'<img id="wbAudio" class="callConnectImage mt-2" src="" title="" style="border-style: hidden !important;display: none; height: 50px; cursor: pointer; border-radius: 50%; width: 50px; min-width: 50px;" onerror="userImageOnErrorReplace(this);" onclick="toggleWhiteBoard()">'
							
						+'</div>'
					+'</div>'
					+'<div id="wbImgListDiv" class="position-absolute" style="display:none; max-height: 100%;top: 0;right: 5px;background-color: #fff;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);">'
						+'<button type="button" onclick="event.stopPropagation();hideWBImageList()" class=" close p-0 " style="font-size: 16px;z-index: 1040;top: -2px;right: 2px;color: black;outline: none;">&times;</button>'
						+'<div id="wbImgList" class="d-flex flex-column align-items-center wsScrollBar m-1 p-1 mt-3 " style="max-height: calc(100vh - 80px);overflow-y: auto;"></div>'
					+'</div>'
					+'<div id="wbLoader" style="display: none; position: absolute; width: 90%; left: 0; height: 100%; z-index: 100; padding-top: 20%;" align="center">'
						+'<span style="padding: 10px; background-color: rgba(113, 118, 126, 0.1);">Preparing Whiteboard...</span>'
					+'</div>'
				+'</div>'
                +'<div id="" class="position-absolute d-flex justify-content-between align-items-center" style="max-width: 380px;top: 4px;z-index: 1021;height: 18px;font-size: 10px;color: #fff;padding-right: 4px;padding-left: 4px;right: 0;padding-top: 2px;">'
					+'<button type="button" onclick=";event.stopPropagation();loadMessages()" class="compactcmeclose close p-0 AgileCloseButton" style="font-size: 16px;z-index: 1040;top: -7px;right: 2px;color: white;outline: none;">&times;</button>'
					+'<img id="" class=" cmeExpandCollapse mx-1" src="images/cme/leftarrow.svg" title="Back to Chat" onclick="backMessages();chatBack(\'on\');event.stopPropagation();" style="width: 20px;height: 20px;">'
					+'<img id="" class="cmeMinMax cmeExpandCollapse mx-1" src="images/cme/cme_min.svg" title="Minimize" onclick="cmeMinMax(this);event.stopPropagation();" style="display: none;width: 20px;height: 20px;">'
                    +'<img id="" class="cmeFullCompact cmeExpandCollapse mx-1 mr-3" src="images/cme/fullscreen_max.svg" title="Expand" onclick="cmeExpandCollapse(this);event.stopPropagation();" style="width: 20px;height: 20px;">'
                +'</div>'
			+'</div>'

		+'</div>'
		+'<div id="callerScreen">'
			+'<div style="margin-top: 4vh;" class="" align="center">'
			
				+'<img id="callImageconnect_1" src="" title="" style="height: 120px; cursor: pointer; border-radius: 50%; width: 120px; margin-right: -50px; visibility: hidden;" onerror="userImageOnErrorReplace(this);" class="callSubImage">' 
				+'<img class="callConnectImage" src="" title="Nivin D" style="height: 170px;cursor: pointer;border-radius: 50%;width: 170px;min-width: 100px;position:relative;" onerror="userImageOnErrorReplace(this);">' 
				+'<img id="callImageconnect_2" src="" title="" style="height: 120px; cursor: pointer; border-radius: 50%; width: 120px; margin-left: -50px; visibility: hidden;" onerror="userImageOnErrorReplace(this);" class="callSubImage">' 
				
			// +'<!-- +'<img class="callConnectImage" src="" title="" style="height: 220px; cursor: pointer; border-radius: 50%; width: 220px; min-width: 100px;" onerror="userImageOnErrorReplace(this);">'
			//  -->'	

				+'<div style="margin-top: 1vh;" align="center">'
					+'<span class="callerScreenStatus" style="clear: both; font-size: 14px; color: #fff;"></span>'
				+'</div>'
				+'<div style="margin-top: 1vh;" align="center">'
					+'<span class="callConnectText" style="clear: both; font-size: 20px; color: #fff;"></span>'
				+'</div>'
				+'<div style="height: 8%; width: 100%; background: none; margin-top: 10px" align="center">'
					+'<img class="declineCallImgg callHangUp" src="images/cme/audioRed.svg" style="cursor: pointer; margin-top: 1%; width: 50px; height: 50px;" onclick="cancelAudioVideoCall();">'
				+'</div>'

			+'</div>'
		+'</div>'

		+'</div>'
	
	
	
	
	
	
	
	
	

	$('#cmecallcontact').append(UI);
	initCallElements();
}

function cmeCallExpandChatFor(){
	if($('#cmecontent').hasClass('cmecontainer-mob')){
		cmeExpandCollapse($('#cmeSwitchModeDiv').find('img.cmeFullCompact'));
		$('.compactcall').hide();
		$('.expandcall').show();
	}
}

function changeDateTime(time) {
	var t = new Date(time);
	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var hours = t.getHours();
	var minutes = t.getMinutes();
	var Ampm = "AM";

	if (hours >= 12) { // the value can equal or lower then 12 it's am 
		Ampm = "PM";
		hours = hours - 12; //the value can substract by 12 it's pm 
	}

	if (hours < 10) {
		hours = '0' + hours;
	}
	if (minutes < 10) {
		minutes = '0' + minutes;
	}
	return hours + ":" + minutes + " " + Ampm + " " + t.getFullYear() + '-' + monthNames[t.getMonth()] + '-' + ("0" + t.getDate()).slice(-2);
}

var myEditorTSData = "";
function ckeditor2() {
	var $ref=$('#transBody');
  ClassicEditor
  		// .create( document.querySelector( '#transBody' ) )
		  .create( $ref[0] ,{
			// your options
		} )
                .then( editor => {
					$('<style id="ckeditor-height" type="text/css" scoped>.ck-editor .ck-editor__editable_inline {min-height: 372px !important;}</style>').insertAfter($ref);

                    // console.log( editor );
                    myEditorTSData = editor;
                } )
                .catch( error => {
                    console.error( error );
                } );
}

function updateTheTextFiles(filepth, msgId, messageTitle, type) {

	// var editTranscript2 = $("#transBody").val();
	var editTranscript = myEditorTSData.getData(); ///$('textarea#comment1').val()
	console.log("update--"+editTranscript);
	// console.log("update2--"+editTranscript);
	let jsonbody = {
		"messageTitle": messageTitle,
		"updatetext": editTranscript,
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/updateExistingFileTranscript",
		type: "PUT",
		dataType: 'text',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			if(type =='1'){
				closeTSEditPopUp2();
			}
			else{
				closeTSEditPopUp();
			}
				$("#loadingBar").hide();
				timerControl("");

				if (result != "success") {
					console.log("Transcript file write error");
				} else {
					console.log("Transcript Successfully");
				}
				// chatAlertFun('Transcript Updated Successfully', 'warning');
				// $("#transEdit").hide();
				// $('#transBody').prop('readonly', true);
		}
	});
}

function readTextFile(fileid, ext, file, transId, messageTitle, type) {
		//  console.log("FILE===>"+file+"transId===>"+transId+"messageTitle---"+messageTitle);
	// var fileDate = new Date();
	// var fileTime = fileDate.getTime();
	// var rawFile = new XMLHttpRequest();
	// file = file + "?" + fileTime;

	let jsonbody = {
		"docId" : fileid,
    	"docExt" : ext
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/readingTextFile",
		type: "POST",
		dataType: 'text',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			var allText = result;
			// console.log("reading--"+allText);
			viewTranscript(allText, transId, file, messageTitle, type);
		}
	});
}

function cancelTheEditFile(canmsgId) {
	$('.jqte').css({ 'display': 'none' });
	$('#update_' + canmsgId).css({ 'display': 'none' });
	$('#cancel_' + canmsgId).css({ 'display': 'none' });
	$("textarea#transcript_" + canmsgId).css({ 'display': 'none' });
}
// $("div[id^=hosta_]").css("display", "none");


// $("div[class^=uid_]").on('click', function(){
// 	var id = $(this).split('_')[1];
// 	console.log("id--"+id);
// })

// $(".uid_"+id).on('click', function(){
// 	getNewConvId(id, this);
// })



//chat.ejs script
	var connection;
	var onlineType=false;
	var callspeed;
	var globalPresentDate="";
		$(document).ready(function(){
    		
    		$('#logoDiv').delay(5000).fadeOut('slow');
    		var src=lighttpdPath+"/userimages/"+userIdglb+"."+userImgType;
    		
    		$(".userI").attr('src',src);
    		$(".name").html(userFullname);
    		var date = new Date();
    		var date1=date.getDate();
    		var month=date.getMonth();
    		var year=date.getYear();
    		
    		var Month = ["Jan", "Feb", "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    		var monthNum = month+1;
    		monthNum = monthNum < 10 ? "0"+monthNum : monthNum;
    		date1 = date1 < 10 ? "0"+date1 : date1;
    		var forDate= Month[month]+" "+date1+", "+(1900+year);
    		globalPresentDate = (1900+year)+"-"+monthNum+"-"+date1;
    		
    		 connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
    		if (connection) {
    			//console.log("connection.effectiveType---"+connection.effectiveType);
    			//console.log("connection.downlink---"+connection.downlink);
    			//callspeed = connection.downlink;
    			// console.log("callspeed--->"+callspeed);
    			/*console.log("connection.downlinkMax ---"+connection.downlinkMax );
    			console.log("connection.type ---"+connection.type );
    			console.log("connection.rtt ---"+connection.rtt );
    			console.log("connection.online ---"+navigator.onLine ); */
    			connection.onchange = changeHandler;
    			
    		}
    		$("body").click(function(event ){
    			var $target = $(event.target);
    			 if(!$target.parents().is(".imgGroupedit")  && !$target.is(".actFeedOptionsDiv")){
           			$("body").find(".popup").hide();
           			$("body").find(".newpopup").hide();
                }
    			if(!$target.parents().is("#chat_link_new")  && !$target.is("#chat_Upload")){
           			$("body").find("#chat_Upload").hide();
                } 
    		});
    		
    		  var objTable = document.getElementById("cmecontent4");
  			
  			  objTable.addEventListener("scroll", function () {
  				if( $('#cmecontent4').scrollTop() + $('#cmecontent4').innerHeight() >= $('#cmecontent4')[0].scrollHeight){
  					getCallHistory("notFirstClick");
  				}
  				
  			  });
  		  $(".arr").hover(function(){
  				var src= $(this).attr('src');
  				if(src.indexOf("slideboxleft")>-1){
  					$(this).attr('src','/images/temp/slideboxleft2.png');
  					$(".arrow").css("left","-2px");
  					
  				}
  		    	}, function(){
  		    		var src= $(this).attr('src');
  		    		if(src.indexOf("slideboxleft2")>-1){
  	  					$(this).attr('src','/images/temp/slideboxleft.png');
  	  				    $(".arrow").css("left","0px");
  	  				  
  	  				}
  		  		}); 
    		
    		
    		
    	});
		function changeHandler(e) { 
			// console.log("after change connection.effectiveType---"+connection.effectiveType);
			// console.log("after change connection.downlink---"+connection.downlink);
			/*console.log("after change connection.downlinkMax ---"+connection.downlinkMax );
			console.log("after change connection.online ---"+navigator.onLine );
			console.log("connection.rtt ---"+connection.rtt ); */
			callspeed = connection.downlink;
			// console.log(callspeed)
			var windowWidth = $(window).width();
			if(windowWidth < 750){
				$("#globalNotification").css({'left':'10%','right':'10%'});
			}else{
				$("#globalNotification").css({'left':'30%','right':'30%'});
			}
			var reqSpeed = (userConnected.length + 1) * 0.05;
		 	if(callspeed <  reqSpeed){
				$("#globalNotification").show();
				$("#globalNotification").html("Your device may experience intermittent connectivity issue or low internet bandwidth for uninterrupted Video/Audio call streaming. This video/audio call may freeze while buffering.").delay(3200).hide(100);
			}
			// if(navigator.onLine == true){
			// 	if(onlineType ==true){
			// 		$("#globalNotification").show();
			// 		$("#globalNotification").html("Yay! Device is connected with the network").delay(3200).hide(100); 
			// 		window.opener.changeImg('online');
			// 		setTimeout( showPersonalConnectionMessages(),30000);
			// 	}
			// 	onlineType = false;
			// }else{
			// 	onlineType = true;
			// 	window.opener.changeImg('false');
			// 	$("#globalNotification").show();
			// 	$("#globalNotification").html("Your Device is not connected with the network").delay(3200).hide(100) ;
			// }
		}
		var user;
		var s;
	/* 	$('.userImage').click(function(){
			$('.chatCalHead').fadeOut().hide();
			$("#conversationList,#groupChat-box,#chat-box").css({'height':'91vh'});
		}); */
		
		
		// $("#conversationList, #chat-box, #groupChat-box, #cmecontent4, #cmecontentconv-mob, #cmecontentconv, #cmecontent1").on('scroll', function() {
		//    var st = $(this).scrollTop();
		//    var lastScrollTop = 600;
		//    /* if (st > lastScrollTop){
		// 	   $('.chatCalHead').fadeOut().hide();
		// 	   $("#conversationList,#groupChat-box,#chat-box").css({'height':'91vh'});
		//    }  */
		// });
		
		$(window).resize(function(){
			if(callType=="AV"){
				var windowWidthVideo = document.getElementById("local-video1").clientWidth;
			}else{
				var windowWidthVideo = document.getElementById("remote-audio").clientWidth;
			}
			
			/* if(windowWidthVideo > 911){
				$("div[id^=RemoteDiv_]").show();
			} */
			
			//$("div[id^=RemoteDiv_]").hide();
			if(windowWidthVideo<364){
				
				var dataIndex = $(".item:visible:first").index()+1;
				var endIndex= $(".item:visible:first").index()+2;
				
				
			}else if(windowWidthVideo>364 && windowWidthVideo<547){
				var dataIndex = $(".item:visible:first").index()+1;
				var endIndex= $(".item:visible:first").index()+3;
			}else if(windowWidthVideo>547 && windowWidthVideo<729){
				var dataIndex = $(".item:visible:first").index()+1;
				var endIndex= $(".item:visible:first").index()+4;
			}else if(windowWidthVideo>729 && windowWidthVideo<911){
				var dataIndex = $(".item:visible:first").index()+1;
				var endIndex= $(".item:visible:first").index()+5;
			}else if(windowWidthVideo>911 && windowWidthVideo<1093){
				var dataIndex = $(".item:visible:first").index()+1;
				var endIndex= $(".item:visible:first").index()+6;
			}else if(windowWidthVideo>1094 && windowWidthVideo<1276){
				var dataIndex = $(".item:visible:first").index()+1;
				var endIndex= $(".item:visible:first").index()+7;
			}else if(windowWidthVideo>1094 && windowWidthVideo<1276){
				var dataIndex = $(".item:visible:first").index()+1;
				var endIndex= $(".item:visible:first").index()+8;
			}else if(windowWidthVideo>1276 ){
				var dataIndex = $(".item:visible:first").index()+1;
				var endIndex= $(".item:visible:first").index()+10;
			}
			for(var i= dataIndex;i<endIndex; i++){
				 $( ".item" ).eq(i).show();
			}
			//resizeVideoSlider(windowWidthVideo);
			resizeVideo();
		}); 


		// var pageloaded = false;
		// $(window).on("load", function() {
			
		// 	showPersonalConnectionMessages();
		// 	namesFromColabus();
		// 	pageloaded = true;
		// 	console.log("---userName.length---"+userName.length);
		// 	/* for(var i=0;i<userName.length;i++){ 
		    	 
		    		 
		//     		  imgType= userName[i].imgType;
		    		 
		//     		  if(imgType=="" || imgType=="null" || imgType == null){
		    			  
		//     		  }else{
		//     			  console.log("inside");
		//     			  var id =userName[i].userId;
		//     			  imgurl = lighttpdPath+"/userimages/"+id+"."+imgType+"?"+imgTime;
		    			  
		//     			  console.log( $("#"+id).find("img").attr("src"));
		//     			  $("#"+id).find("img").attr("src",imgurl);
		//     		  }
		    		
		    	   
		//       } */
		// });
		
		
		// function sessionTimeOutMethod(result){
		// 	if(result == 'SESSION_TIMEOUT'){
		//     	alert('Session Has Timed Out!');
		//     	 /*** Below line of code- comment for CME alone and Uncomment for Colabus CME. **/
		//     	window.opener.doLogOut('seslogout');
		// 	}
		// }
		