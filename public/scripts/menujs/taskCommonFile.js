var tasktypeglb="";

/* function createTaskUI(result) {
    let UI = "";
    var mzProjectname = "";
    var Projectid = "";
    var mzPrjImgType = "";
    for(i=0;i<result.length;i++){
      var taskType =result[i].task_type; 
      var source_id=result[i].source_id;
      var Priority=result[i].priority;
      var Priority_image = (Priority == "")? "" : Priority == "1" ? "images/task/p-one.svg" : Priority == "2" ? "images/task/p-two.svg":Priority == "3" ? "images/task/p-three.svg" :Priority == "4" ? "images/task/p-four.svg":Priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
      var priorityTitle = Priority == "" ? "": Priority == "1" ? "Very important" : Priority == "2" ?"Important": Priority == "3" ? "Medium": Priority == "4"  ? "Low": Priority == "5"  ? "Very low": Priority == "6" ? "None" :"";
              
      var taskImage = "";
          var taskImageTitle = "";
      if(taskType == 'Tasks' && source_id == "0"){
        taskImage = "images/task/task.svg";
        taskImageTitle = "Task";
      }else if(taskType == 'Tasks'){
        taskImage = "images/task/convo-task.svg";
        taskImageTitle = "Conversation Task";
      }else if(taskType == "WorkspaceDocument" || taskType == 'Document'){
        taskImage = "images/task/doc-task.svg";
        taskImageTitle = "Document Task";
      }else if(taskType == 'Idea'){
        taskImage = "images/task/idea-task.svg";
        taskImageTitle = "Idea Task";
      }else if(taskType == 'Sprint'){
        taskImage = "images/task/sprint-task.svg";
        taskImageTitle = "Sprint Task";
      }else if(taskType == 'Highlight'){
        taskImage = "images/task/task.svg";
        taskImageTitle = "Highlight Task";
      }else if(taskType == 'Email'){
        taskImage = "images/task/email-task.svg";
        taskImageTitle = "Email Task";
      }else if(taskType == 'Event'){
        taskImage = "images/task/event.svg";
        taskImageTitle = "Event";
      }else if(taskType == 'Workflow'){
        taskImage = "images/task/workflow.svg";
        taskImageTitle = "Workflow Task";
      }else{
        taskImage = "images/task/task.svg";
        taskImageTitle = "Task";
      }
      var taskStatusImageTitle=result[i].display_status;
      var taskStatusImage = taskStatusImageTitle == "Completed" ? "images/task/completed.svg" : taskStatusImageTitle == "InProgress" ? "images/task/in-progress.svg" : taskStatusImageTitle == "Created" ? "images/task/in-progress.svg" : taskStatusImageTitle == "InComplete" ? "images/task/in-complete.svg" : taskStatusImageTitle == "NotCompleted" ? "images/task/not-completed.svg"  : "images/task/not-completed.svg";
      
      var trending=result[i].trending;
      var sentimentBgCol ="";
      if(trending >=-1 && trending <=-0.49){
          sentimentBgCol='images/tasksNewUI_old/meterred.png';
      }else if(trending >=-0.50 && trending <0){
          sentimentBgCol='images/tasksNewUI_old/meteryellow.png';
      }else if(trending == 0 || trending == 0.0){
          sentimentBgCol='images/tasksNewUI_old/meteryellow_center.png';
      }else if((trending >0 || trending > 0.0)&& trending <=0.49){
          sentimentBgCol='images/tasksNewUI_old/meteryellow_Lightgreen.png';
      }else if(trending >=0.5 && trending <=1){
          sentimentBgCol='images/tasksNewUI_old/meter_fullgreen.png';
      }else{
          trending = getValues(companyLabels,"Nodata");
          sentimentBgCol='images/tasksNewUI_old/meterDefault.png';  
      }
  
  
      var statusImg="";
      var userStatus="";
      var userTaskStatus = result[i].user_task_status;
      if(userTaskStatus=="Created"){
          userStatus="Created";
          statusImg = "images/task/user-start.svg";
      }else if(userTaskStatus=="Inprogress"){
          userStatus="Inprogress";
          statusImg = "images/task/user-inprogress.svg";
      }else if(userTaskStatus=="Paused"){
          userStatus="Paused";
          statusImg = "images/task/user-pause.svg";
      }else if(userTaskStatus=="Completed"){
          userStatus="Completed";
          statusImg = "images/task/user-completed.svg";
      }else{
          statusImg = "images/task/user-view.svg";
      }
  
      var taskStartDate=result[i].start_date;
      var taskEndDate=result[i].end_date;
      if(taskStartDate == "00-00-0000"){
            taskStartDate = "-";
      }else{
            taskStartDate = taskStartDate.replaceAll("-", " ");
      }
      if(taskEndDate == "00-00-0000"){
             taskEndDate = "-";
      }else{
            taskEndDate = taskEndDate.replaceAll("-", " ");
      }
        var estHour = result[i].total_estimated_hour ==null ? "0" :result[i].total_estimated_hour;
              var actualHour=result[i].act_hour ==null ? "0" :result[i].act_hour;
              var estMinute=result[i].total_estimated_minute ==null ? "0" :result[i].total_estimated_minute;
              var actualMinute=result[i].act_minute ==null ? "0" :result[i].act_minute;
              if(actualHour == ""){
                  actualHour="0";
              }
              if(actualMinute == ""){
                  actualMinute="0";
              }
              if(estHour == ""){
                  actualMinute="0";
              }
              if(estMinute == ""){
                  actualMinute="0";
              }
              var totalMinuteAll=0;var totalActualhour=0;var totalActualminute=0;
              var totalEstMinAll=0;var totalEstHr=0;var totalEstMin=0;
                  
              if(result[i].task_type != "Event"){
                      
                  totalEstMinAll = (parseInt(estHour)*60)+parseInt(estMinute);
                  totalEstHr = (totalEstMinAll)/(60);
                  totalEstMin = (totalEstMinAll)%60;
                  totalEstHr = Math.trunc(totalEstHr);totalEstMin = Math.trunc(totalEstMin);
  
                  totalMinuteAll=(parseInt(actualHour)*60)+parseInt(actualMinute);
                  totalActualhour=(totalMinuteAll)/(60);
                  totalActualminute=(totalMinuteAll)%60;
                  
                  totalActualhour = Math.trunc(totalActualhour);
                  totalActualminute = Math.trunc(totalActualminute);
              }
                  
              var color="";
              if(result[i].task_type == "Workflow"){
                  var totalEstMinuteAll =(parseInt(result[i].total_estimated_hour_workflow)*60)+parseInt(result[i].total_estimated_mins_workflow);
                  totalMinuteAll=(parseInt(result[i].total_task_working_hours_workflow)*60)+parseInt(result[i].total_task_working_mins_workflow);
                  if(totalMinuteAll > totalEstMinuteAll){
                      color = "color:red" ;
                  }
              }else{
                  var totalEstMinuteAll =(parseInt(estHour)*60)+parseInt(estMinute);
                  totalMinuteAll=(parseInt(actualHour)*60)+parseInt(actualMinute);
                  if(totalMinuteAll > totalEstMinuteAll){
                      color = "color:red" ;
                  }
              }
        var createdImg='';var createdName="";let createdId="";
        if(typeof result[i].createdByDetails[0]!='undefined'){
          createdImg=result[i].createdByDetails[0].imagePathUrl;
          createdName=result[i].createdByDetails[0].userName;
          createdId=result[i].createdByDetails[0].user_id;
        }else{
          console.log(result[0].task_id);
        }

        var width="";var taskTypeWidth="";var taskgroupmClass="";var userImgp="";var prjimgml="";var prjimgmr="";let tasknamepadding="";let taskcontent="";let task_id_width="width:6%;";let task_id_class="";let group_data="";
        var taskgrpmr="";var typepr="";
        if(typeof(projtName)=="undefined"&&typeof(prjid)=="undefined"){
            mzProjectname = result[i].project_name;
            Projectid = result[i].project_id;
            mzPrjImgType = result[i].project_image_type;
            width="width:80%";taskTypeWidth="width:6%";taskgroupmClass="ml-3";userImgp="pl-2";prjimgml="ml-4";prjimgmr="";
            taskgrpmr="mr-2";
            tasknamepadding=""
            taskcontent=""
            task_id_width="width:7%";
            task_id_class="pl-2";
            group_data="ml-2";
            typepr="pr-3";
        }else{
            Projectid = prjid;
            width="width:93%";taskTypeWidth="width:6%";taskgroupmClass="ml-1";userImgp="";prjimgml="";prjimgmr="";
            taskgrpmr="";
            tasknamepadding="pl-1"
            taskcontent="px-2";
            task_id_class="ml-1 pl-1";
        }
        let border="";
        let onclick="";
        
          
              if(taskType != "Workflow" && typeof(result[i].childwfid) == "undefined"){  
                UI +="<div taskType='"+taskType+"' class='mainTask' id='task_"+result[i].task_id+"'  style='"+border+";cursor:pointer'  >" 
              }else{
                UI +="<div  class='mainTask' id='WFtask_"+result[i].workflow_id+"'style='"+border+"'>" 
              }  
                UI+="<div  class='d-flex taskcontent "+taskcontent+" pt-2 pb-1 align-items-center' style='font-size:12px;' onclick='editTask("+result[i].task_id+");event.stopPropagation();'>" +
                    "<div id='' class='headerTaskPrj tasklistProjectImage' align='center' style='display:none;width:8%;'><img class='border rounded ml-4' style='width:35%;' title=\""+mzProjectname+"\" onerror=\"userImageOnErrorReplace(this)\" src=\""+lighttpdpath+"/projectimages/"+Projectid+"."+mzPrjImgType+"\"></div>" +
                    "<div class='d-flex mr-1 pl-4 headerTaskPrj "+typepr+"' title='"+taskImageTitle+"'style='position:relative;text-align:center;"+taskTypeWidth+";'>"+
                   "   <img src='"+taskImage+"' class='task-img img-responsive "+prjimgmr+"'>"

                    if(result[i].dep_task_id!=""){
                      UI+='<div id="showDepButton " class=" ml-1 d-flex flex-column "  onclick="fetchDependencyTask(\''+result[i].task_id+'\',\'SubList\');event.stopPropagation();" style="position:relative;"><img id="dependenctImage_'+result[i].task_id+'" src="images/task/dependencydown.svg" style="" class="img-responsive depicon"></div>'
                    }

                   UI+= "</div>"
                    if(taskType == "Workflow" && typeof(result[i].childwfid) == "undefined"){
                      UI+="<div class='types-data headerTaskPrj "+task_id_class+"' style='"+task_id_width+"'></div>" 
                    }else{
                      UI+="<div class='types-data headerTaskPrj "+task_id_class+"' style='"+task_id_width+"'>"+result[i].task_id+"</div>" 
                    }
                    
                    UI+="<div class='types-datacontent Taskname  defaultExceedCls "+tasknamepadding+"'  onclick='editTask("+result[i].task_id+");event.stopPropagation();' style='"+width+"' title='"+replaceSpecialCharacter(result[i].task_name)+"'>"+trancate(replaceSpecialCharacter(result[i].task_name),200)+"</div>" +
                " </div>" +
                "<div  class='d-flex taskcontent px-2 pb-2 align-items-center' onclick='editTask("+result[i].task_id+");event.stopPropagation();' style='font-size:12px;' >" +
                    "<div class=' types-data before-group tasklistProjectImage headerTaskPrj' style='display:none;width:7%'></div>" +
                    "<div class='d-flex pl-4 headerTaskPrj ml-1' title='"+taskImageTitle+"'style='text-align:center;width:6%;'>"+
                    "   <div title='"+result[i].group_name+"' class='task_group mt-1  rounded-circle "+taskgrpmr+"' style='background:"+result[i].group_code+"'></div>"+
                    "</div>" +
                    "<div class='types-data headerTaskPrj d-flex mr-1 justify-content-start onclick='editTask("+result[i].task_id+");event.stopPropagation();'  mt-1 group_data "+group_data+" ' style='width:6%'>"+
  
                      '<div>'
                     
                      if(taskType == 'Workflow' && typeof(result[i].childwfid) == "undefined"){
                          UI+='<img  class=" mt-0 ml-0" title="View" id = \"wfListImage_'+result[i].workflow_id+'\" onclick=\"listWFtaskSteps('+result[i].workflow_id+');\" src=\"images/task/dropdown.svg\" style="width: 24px;margin-top: 2px;cursor: pointer;"  class="img-responsive">'
                      }else if(userStatus != ""){
  
                          UI+="<img  class=\" mt-0 ml-0\" title='"+userStatus+"' id=\"taskPlay_"+result[i].task_id+"\" onclick=\"changeStatusOfUserTask("+result[i].task_id+","+Projectid+",'listView','"+taskType+"');event.stopPropagation();\" statusOfTaskUser=\""+userStatus+"\" src=\""+statusImg+"\" statusOfTaskUser=\""+userStatus+"\" style=\"width: 24px;margin-top: 2px;cursor: pointer;\" class=\"img-responsive\">"
  
                      }else if(userStatus == ""){
                          UI+="<img  class=\" mt-0 ml-0\" title='View' id='\"\"' src=\""+statusImg+"\" style=\"width: 24px;margin-top: 2px;cursor: default;\" class=\"img-responsive\">"
                      }
                      UI+='</div>'+
  
  
                      '<div onclick="showTaskComments('+result[i].task_id+');event.stopPropagation();" style="cursor:pointer"><img  class="ml-1 mt-0 " title="comment" id="" src="/images/task/task-comment.svg" style="width: 24px;margin-top: 2px;cursor: default;cursor:pointer" class="img-responsive"></div>'+
                    "</div>" +
                    "<div class='types-usercontent d-flex mt-1 "+userImgp+"' style='width:37%;'>" +
                      '<div style=""><img title="Created : '+createdName+'" class="mr-3 mt-0 rounded-circle" id="" src="'+createdImg+'" style="width: 27px;margin-top: 2px;cursor: default;" class="img-responsive"></div>'
                      
                      if(typeof(result[i].assignedIdDetails)!='undefined'){
                        for(let j=0;j<result[i].assignedIdDetails.length;j++){
                          let style="";let style1="";
                          if(j==0){
                            style="pl-2";style1="border-left: 1px solid #C9C7AC;";
                          }
                          if(j>4){
                            break;
                          }
                          if(taskType == 'Workflow' && typeof(result[i].childwfid) == "undefined"){
                            UI+='<div class="'+style+'"></div>'
                          }else{
                            UI+='<div class="'+style+'" style="'+style1+'"><img title="'+result[i].assignedIdDetails[j].userName+'" class="mr-3 mt-0 rounded-circle" id="" src="'+result[i].assignedIdDetails[j].imagePathUrl+'" onerror="userImageOnErrorReplace(this);" style="width: 27px;margin-top: 2px;cursor: default;" class="img-responsive"></div>'
                          }
                        }
                      }
                      UI+= " </div>" 

                    if(taskType == 'Workflow' && typeof(result[i].childwfid) == "undefined"){
                      UI+= "<div class='defaultExceedCls types-data' style='width: 9%;' ></div>" 
                      +"<div class='defaultExceedCls types-data' style='width: 9%;' ></div>" 
                    }else{
                      UI+= "<div class='defaultExceedCls types-data' style='width: 9%;' onclick='editTask("+result[i].task_id+");event.stopPropagation();' >"+taskStartDate+"</div>" 
                      +"<div class='defaultExceedCls types-data' style='width: 9%;' onclick='editTask("+result[i].task_id+");event.stopPropagation();' > "+taskEndDate+"</div>" 
                    }
                    UI+="<div class='pl-1 defaultExceedCls types-data' style='width: 7%;'onclick='editTask("+result[i].task_id+");event.stopPropagation();' >"+totalEstHr+" h "+totalEstMin+" m</div>" +
                    "<div class='pl-1 defaultExceedCls types-data' style='width: 7%;"+color+";' id='actTime_"+result[i].task_id+"'>"+actualHour+" h "+actualMinute+" m</div>" +
                    "<div class='types-data' style='width: 6%;'> <img class='ml-4' title='"+priorityTitle+"' src='"+Priority_image+"' style='width:24px;'  class='img-responsive'></div>" +
                    "<div class='types-data' style='width: 6%;'> <img class='ml-3' title='"+taskStatusImageTitle+"'  style='width:22px;' class='img-responsive' src='"+taskStatusImage+"'></div>" +
                    "<div class='types-data' style='width: 6%;'> <img class='ml-3' title='"+trending+"'  style='width:22px;' class='img-responsive' src='"+sentimentBgCol+"'></div>" +
                

                  " </div>" 
  
                  if(taskType != 'Workflow' && typeof(result[i].childwfid) == "undefined"){
                    UI +='<div id=\"TaskStatus_'+result[i].task_id+'\" class=\"taskcontent pl-4\" style=\"display:none;\">'
                            +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
                            +'</div>'
                        +'</div>'
                    UI+='<div class="task_comment_div_'+result[i].task_id+'  px-1 ml-4 my-2 mr-3" style="border: 1px solid rgb(180, 173, 173); border-radius: 10px;display:none"></div>'    
                  }else{
                    UI +='<div id=\"TaskStatus_'+result[i].workflow_id+'\" class=\"taskcontent pl-4\" style=\"display:none;\"></div>'
                    UI+='<div class="task_comment_div_'+result[i].workflow_id+'   px-1 my-2 ml-4 mr-3" style="border: 1px solid rgb(180, 173, 173); border-radius: 10px;display:none" ></div>'
                  }

                 
  
            UI+= "</div>";
            
            
            
    }
        return UI;
  } */

  //<div id="showDepButton " class="mt-1 ml-2" style="/* position: absolute; *//* top: 13px;  *//* right: 0; */" onclick="showDepTaskInListView(27460,this,'assignedTasks');event.stopPropagation();"><img src="/images/downarrowNewUI.png" style="width: 17px;/* float:left; */" class="img-responsive"></div>
  //let border="";
  //let onclick="";


  //// "   <div title='"+result[i].group_name+"' class='task_group mt-1 rounded-circle "+taskgroupmClass+"' style='background:"+result[i].group_code+"'></div>"+
////  "   <img src='"+taskImage+"' class='task-img img-responsive "+prjimgmr+"'>"

//'<div><img title="" class="mr-3 mt-0" id="" src="/images/task_nostatus.svg" style="width: 20px;margin-top: 2px;cursor: default;" class="img-responsive"></div>'+
///'<div onclick="showTaskComments('+result[i].task_id+');event.stopPropagation();" style="cursor:pointer"><img  class="ml-1 mt-0 " title="comment" id="" src="/images/task/task-comment.svg" style="width: 24px;margin-top: 2px;cursor: default;cursor:pointer" class="img-responsive"></div>'+
                    



function createTaskUI(result,place){

  let UI = "";
  var mzProjectname = "";
  var Projectid = "";
  var mzPrjImgType = "";
  let assigned="";
  let assigneduserId="";
  let assigneduserstatus="";
  let assignedstatusimage="";
  let assigneduser_task_status="";
  let loginuserid="";
  let Approvuserid="";
  let checkloginuserid="";
  let asignimg="";
  for(i=0;i<result.length;i++){
    var taskType =result[i].task_type; 
    var source_id=result[i].source_id;
    var approversList=result[i].approval_id;
    // console.log(approversList);

    source_id=typeof(source_id)!='undefined' && source_id!=""?source_id:"0";
    var Priority=result[i].priority;
    var Priority_image = (Priority == "")? "" : Priority == "1" ? "images/task/p-one.svg" : Priority == "2" ? "images/task/p-two.svg":Priority == "3" ? "images/task/p-three.svg" :Priority == "4" ? "images/task/p-four.svg":Priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
    var priorityTitle = Priority == "" ? "": Priority == "1" ? "Very important" : Priority == "2" ?"Important": Priority == "3" ? "Medium": Priority == "4"  ? "Low": Priority == "5"  ? "Very low": Priority == "6" ? "None" :"";
    assigned=  result[i].assignedIdDetails;      
    var taskImage = "";
    var taskImageTitle = "";
    
    if(taskType == 'Tasks' && source_id == "0" ){
      taskImage = "images/task/task.svg";
      taskImageTitle = "Task";
    }else if(taskType == 'Tasks' || taskType == "Assigned Task"){
      taskImage = "images/task/convo-task.svg";
      taskImageTitle = "Conversation Task";
    }else if(taskType == "WorkspaceDocument" || taskType == 'Document'){
      taskImage = "images/task/doc-task.svg";
      taskImageTitle = "Document Task";
    }else if(taskType == 'Idea'){
      taskImage = "images/task/idea-task.svg";
      taskImageTitle = "Idea Task";
    }else if(taskType == 'Sprint' || taskType == 'Sprint Task'){
      taskImage = "images/task/sprint-task.svg";
      taskImageTitle = "Sprint Task";
    }else if(taskType == 'Highlight'){
      taskImage = "images/task/task.svg";
      taskImageTitle = "Highlight Task";
    }else if(taskType == 'Email'){
      taskImage = "images/task/email-task.svg";
      taskImageTitle = "Email Task";
    }else if(taskType == 'Event'){
      taskImage = "images/task/event.svg";
      taskImageTitle = "Event";
    }else if(taskType == 'Workflow' && (result[i].type == "MyZone")){
      taskImage = "images/task/workflow-task.svg";
      taskImageTitle = "Workflow Task";
    }else if(taskType == 'Workflow'){
      taskImage = "images/task/workflow.svg";
      taskImageTitle = "Workflow Task";
    }else{
      taskImage = "images/task/task.svg";
      taskImageTitle = "Task";
    }
    var taskStatusImageTitle=result[i].display_status;
    var taskStatusImage = taskStatusImageTitle == "Completed" ? "images/task/completed.svg" : taskStatusImageTitle == "InProgress" ? "images/task/in-progress.svg" : taskStatusImageTitle == "Created" ? "images/task/in-progress.svg" : taskStatusImageTitle == "InComplete" ? "images/task/in-complete.svg" : taskStatusImageTitle == "NotCompleted" ? "images/task/not-completed.svg"  : "images/task/not-completed.svg";
    if(taskStatusImageTitle=="Created"){taskStatusImageTitle="In Progress";}
    else if(taskStatusImageTitle=="NotCompleted"){taskStatusImageTitle="Not Completed";}
    else if(taskStatusImageTitle=="InProgress"){taskStatusImageTitle="In Progress";}
    else if(taskStatusImageTitle=="InComplete"){taskStatusImageTitle="Incomplete";}
    var trending=result[i].trending;
    var sentimentBgCol ="";
    if(trending >=-1 && trending <=-0.49){
        sentimentBgCol='images/tasksNewUI_old/meterred.png';
    }else if(trending >=-0.50 && trending <0){
        sentimentBgCol='images/tasksNewUI_old/meteryellow.png';
    }else if(trending == 0 || trending == 0.0){
        sentimentBgCol='images/tasksNewUI_old/meteryellow_center.png';
    }else if((trending >0 || trending > 0.0)&& trending <=0.49){
        sentimentBgCol='images/tasksNewUI_old/meteryellow_Lightgreen.png';
    }else if(trending >=0.5 && trending <=1){
        sentimentBgCol='images/tasksNewUI_old/meter_fullgreen.png';
    }else{
        trending = getValues(companyLabels,"Nodata");
        sentimentBgCol='images/tasksNewUI_old/meterDefault.png';  
    }


    var statusImg="";
    var userStatus="";
    var userTaskStatus = result[i].user_task_status;
    if(userTaskStatus=="Created"){
        userStatus="Created";
        statusImg = "images/task/user-start.svg";
    }else if(userTaskStatus=="Inprogress"){
        userStatus="Inprogress";
        statusImg = "images/task/user-inprogress.svg";
    }else if(userTaskStatus=="Paused"){
        userStatus="Paused";
        statusImg = "images/task/user-pause.svg";
    }else if(userTaskStatus=="Completed"){
        userStatus="Completed";
        statusImg = "images/task/user-completed.svg";
    }else{
        statusImg = "images/task/user-view.svg";
    }

    var taskStartDate=result[i].start_date;
    var taskEndDate=result[i].end_date;
    if(taskStartDate == "00-00-0000"){
          taskStartDate = "-";
    }else{
          taskStartDate = taskStartDate.replaceAll("-", " ");
    }
    if(taskEndDate == "00-00-0000"){
           taskEndDate = "-";
    }else{
          taskEndDate = taskEndDate.replaceAll("-", " ");
    }
      var estHour = result[i].total_estimated_hour ==null ? "0" :result[i].total_estimated_hour;
            var actualHour=result[i].act_hour ==null ? "0" :result[i].act_hour;
            var estMinute=result[i].total_estimated_minute ==null ? "0" :result[i].total_estimated_minute;
            var actualMinute=result[i].act_minute ==null ? "0" :result[i].act_minute;
            if(actualHour == ""){
                actualHour="0";
            }
            if(actualMinute == ""){
                actualMinute="0";
            }
            if(estHour == ""){
                actualMinute="0";
            }
            if(estMinute == ""){
                actualMinute="0";
            }
            var totalMinuteAll=0;var totalActualhour=0;var totalActualminute=0;
            var totalEstMinAll=0;var totalEstHr=0;var totalEstMin=0;
            // console.log("result[i].task_id--->"+result[i].task_id);
            if(result[i].task_type != "Event"){
                    
                totalEstMinAll = (parseInt(estHour)*60)+parseInt(estMinute);
                totalEstHr = (totalEstMinAll)/(60);
                totalEstMin = (totalEstMinAll)%60;
                totalEstHr = Math.trunc(totalEstHr);totalEstMin = Math.trunc(totalEstMin);

                totalMinuteAll=(parseInt(actualHour)*60)+parseInt(actualMinute);
                totalActualhour=(totalMinuteAll)/(60);
                totalActualminute=(totalMinuteAll)%60;
                
                totalActualhour = Math.trunc(totalActualhour);
                totalActualminute = Math.trunc(totalActualminute);
            }
                
            let color="";
            if(result[i].task_type == "Workflow"){
                var totalEstMinuteAll =(parseInt(result[i].total_estimated_hour_workflow)*60)+parseInt(result[i].total_estimated_mins_workflow);
                totalMinuteAll=(parseInt(result[i].total_task_working_hours_workflow)*60)+parseInt(result[i].total_task_working_mins_workflow);
                if(totalMinuteAll > totalEstMinuteAll){
                    color = "color:red" ;
                }
            }else{
                var totalEstMinuteAll =(parseInt(estHour)*60)+parseInt(estMinute);
                totalMinuteAll=(parseInt(actualHour)*60)+parseInt(actualMinute);
                //alert(totalMinuteAll+"----"+totalEstMinuteAll);
                if(totalMinuteAll > totalEstMinuteAll){
                  //alert("here"+(totalMinuteAll > totalEstMinuteAll))
                    color = "color:red" ;
                }
            }
           // alert(color);
           var ahr = result[i].act_hour;var amin = result[i].act_minute;
           ahr = ahr=="-"?0:ahr;amin = amin=="-"?0:amin;
           if(result[i].task_type != "Workflow"){
            let actualTime=(parseInt(ahr)*60)+parseInt(amin);

            actualHour=Math.floor(actualTime/60);
            actualMinute=Math.floor(actualTime%60);
 
            let estHrMinNew=(parseInt(result[i].total_estimated_hour)*60)+parseInt(result[i].total_estimated_minute);
            totalEstHr=Math.floor(estHrMinNew/60);
            totalEstMin=Math.floor(estHrMinNew%60);
           }else if(result[i].task_type == "Workflow" && (mzTasktype == "MyZone" || mzTasktype == "historyTask")){
            
            let actualTime=(parseInt(ahr)*60)+parseInt(amin);

            actualHour=Math.floor(actualTime/60);
            actualMinute=Math.floor(actualTime%60);
 
            let estHrMinNew=(parseInt(result[i].total_estimated_hour)*60)+parseInt(result[i].total_estimated_minute);
            totalEstHr=Math.floor(estHrMinNew/60);
            totalEstMin=Math.floor(estHrMinNew%60);
           }else if(result[i].task_type == "Workflow" && place!='workflowsublist'){
            let actualTime=(parseInt(result[i].total_task_working_hours_workflow)*60)+parseInt(result[i].total_task_working_mins_workflow);

            actualHour=Math.floor(actualTime/60);
            actualMinute=Math.floor(actualTime%60);
 
            let estHrMinNew=(parseInt(result[i].total_estimated_hour_workflow)*60)+parseInt(result[i].total_estimated_mins_workflow);
            totalEstHr=Math.floor(estHrMinNew/60);
            totalEstMin=Math.floor(estHrMinNew%60);
           }

      var createdImg='';var createdName="";let createdId="";
      if(typeof result[i].createdByDetails[0]!='undefined'){
        createdImg=result[i].createdByDetails[0].imagePathUrl;
        createdName=result[i].createdByDetails[0].userName;
        createdId=result[i].createdByDetails[0].user_id;
      }else{
        console.log(result[0].task_id);
      }
      let editclick='onclick="editTask('+result[i].task_id+',\''+place+'\');event.stopPropagation();"'
      if(taskType=='Workflow' && place !='workflowsublist'){
            if(typeof(result[i].workflow_id)!='undefined' && result[i].workflow_id!=''){
              editclick='onclick="fetchWFdetails('+result[i].workflow_id+');event.stopPropagation();"';
            }
      }
      
      var width="";var taskTypeWidth="";var taskgroupmClass="";var userImgp="";var prjimgml="";var prjimgmr="";let tasknamepadding="";let taskcontent="";let task_id_width="width:6%;";let task_id_class="";let group_data="";
      var taskgrpmr="";var typepr="";
      if(gloablmenufor=="Myzone"){
          mzProjectname = result[i].project_name;
          Projectid = result[i].project_id;
          mzPrjImgType = result[i].project_image;
          width="width:80%";taskTypeWidth="width:6%";taskgroupmClass="ml-3";userImgp="pl-2";prjimgml="ml-4";prjimgmr="";
          taskgrpmr="mr-2";
          tasknamepadding=""
          taskcontent=""
          task_id_width="width:7%";
          task_id_class="pl-2";
          group_data="ml-2";
          typepr="pr-3";
      }else{
          Projectid = prjid;
          width="width:93%";taskTypeWidth="width:6%";taskgroupmClass="ml-1";userImgp="";prjimgml="";prjimgmr="";
          taskgrpmr="";
          tasknamepadding="pl-1"
          taskcontent="px-2";
          task_id_class="ml-1 pl-1";
      }
      
      for(let k=0;k<approversList.length;k++){
        Approvuserid=approversList[k].user_id;
      }
      checkloginuserid=result[i].assigned_id;
      loginuserid = checkloginuserid.includes(userIdglb);
      if(loginuserid == true || Approvuserid==userIdglb){
        asignimg="/images/task/list_assignee_cir.svg";
      }else{
        asignimg="/images/task/list_assignee_nocir.svg";
      }
      
      let border="";
      let onclick="";
      let grouptitle="";
      let groupcode="";
      let boxshadow="box-shadow: 0 6px 10px rgb(0 0 0 / 18%);";
      if(typeof(result[i].group_name)=='undefined'){
        grouptitle="-"
        boxshadow="";
      }

      if(taskType == 'Sprint' || taskType == 'Sprint Task'){
        grouptitle=result[i].sprint_group_name;
        groupcode=result[i].sprint_color_code;
        groupcode = typeof(groupcode)=="undefined"||groupcode==""?"#f2788f":groupcode;
      }else{
        grouptitle=result[i].group_name;
        groupcode=result[i].group_code;
      }
  
      if((taskType != "Workflow" && typeof(result[i].childwfid) == "undefined")){  
        UI +="<div taskType='"+taskType+"' class='mainTask' id='task_"+result[i].task_id+"'  style='"+border+";cursor:pointer'  >" //1
      }else if(taskType == "Workflow" && place=='workflowsublist'){
        UI +="<div taskType='"+taskType+"' class='mainTask' id='task_"+result[i].task_id+"'  style='"+border+";cursor:pointer'  >" //1
      }else if(taskType == "Workflow" && ( result[i].type == "MyZone")){
        UI +="<div taskType='"+taskType+"' class='mainTask' id='task_"+result[i].task_id+"'  style='"+border+";cursor:pointer'  >" //1
      }
      else{
        UI +="<div  class='mainTask' id='WFtask_"+result[i].workflow_id+"'style='"+border+"'>" //1
      }  
        UI+="<div  class='d-flex py-2 align-items-center position-relative' style='font-size:12px;' "+editclick+"  onmouseover='closeTaskParticipants(event);event.stopPropagation();'>" 
        //2
        if(Projectid == "0"){
          UI+="<div style='display:none;width:5%;text-align: center;' class='defaultExceedCls tasklistProjectImage'></div>"
        }else{
          UI+="<div style='display:none;width:5%;text-align: center;' class='defaultExceedCls tasklistProjectImage'><img class='border rounded ' style='width:25px;height:25px;' title=\""+mzProjectname+"\" onerror=\"userImageOnErrorReplace(this)\" src=\""+lighttpdpath+"/projectimages/"+Projectid+"."+mzPrjImgType+"\"/></div>"
        }
        UI+="<div class='d-flex pl-3 ' title='"+taskImageTitle+"' onclick='' style='width:5%'>"
          if(taskType == 'Workflow' && ( mzTasktype == "historyTask" && result[i].type == "MyZone" )){
            UI+="<img id='tTypeImage_"+result[i].task_id+"' src='"+taskImage+"' class='task-img img-responsive tTypeImage'>"
          }else if(taskType == 'Workflow' && typeof(result[i].childwfid) == "undefined"){
            UI+="<img id='tTypeImage_"+result[i].workflow_id+"' src='"+taskImage+"' class='task-img img-responsive'>"
            +'<img  class=" mt-0 ml-0 depicon" title="View" id = \"wfListImage_'+result[i].workflow_id+'\" onclick=\"listWFtaskSteps('+result[i].workflow_id+');event.stopPropagation();\" src=\"/images/task/dependencydown.svg\" style=""  class="img-responsive">'
          }
          else if(taskType == 'Workflow' && typeof(result[i].childwfid) != "undefined" && result[i].childwfid!="" && place=='workflowsublist' && result[i].childwfid!="0"){
            UI+="<img id='tTypeImage_"+result[i].task_id+"' src='/images/task/workflow-task.svg' class='task-img img-responsive'>"
            +'<img  class=" mt-0 ml-0 depicon" title="View" id = \"wfListImage_'+result[i].task_id+'\" onclick=\"fetchWFChild('+result[i].childwfid+','+result[i].task_id+');event.stopPropagation();\" src=\"/images/task/dependencydown.svg\" style=""  class="img-responsive">'
          }
          else if(typeof(result[i].dep_task_id)!='undefined' && result[i].dep_task_id!="-"){
            UI+="<img id='tTypeImage_"+result[i].task_id+"' src='"+taskImage+"' class='task-img img-responsive tTypeImage'>"
            +'<div id="showDepButton " class=" ml-1 mb-1 d-flex "  onclick="fetchDependencyTask(\''+result[i].task_id+'\',\'SubList\');event.stopPropagation();" style="position:relative;"><img id="dependenctImage_'+result[i].task_id+'" src="images/task/dependencydown.svg" style="" class="img-responsive depicon"></div>'
          }else{
            UI+="<img id='tTypeImage_"+result[i].task_id+"' src='"+taskImage+"' class='task-img img-responsive tTypeImage'>"
          }
        UI+="</div>"  //onmouseout=\"hideTaskParticipants("+result[i].task_id+",this);event.stopPropagation();\"
        +"<div style='padding-top: 2px;width:5%' onclick='' class='defaultExceedCls pl-3 '><div title='"+grouptitle+"' class='rounded-circle position-absolute' style='background:"+groupcode+" ;width:15px;height:15px;margin-top:-8px;"+boxshadow+"'></div></div>"
        +"<div style='width:7%;' class='defaultExceedCls pl-3' id='assignedPopup_"+result[i].task_id+"'><img class='position-relative mx-auto' onclick='event.stopPropagation();' onmouseover=\"openTaskParticipants("+result[i].task_id+",this);event.stopPropagation();\"  style='width: 20px;height:20px;' src='"+asignimg+"'/>"
          +"<div id='opentaskParticipantdiv_"+result[i].task_id+"' onclick='event.stopPropagation();' class='position-absolute participantsBox' style='display: none;'>"
            UI+=assigneePopup(result[i].assignedIdDetails,createdImg,createdName,taskType,result[i].task_id,Projectid,userStatus,place,result[i].type,approversList,createdId);
          UI+="</div>"
        +"</div>"
        if(taskType == 'Workflow' && ( result[i].type == "MyZone")){
          UI+="<div class='' style='width: 5%;'><span class='defaultExceedCls'>"+result[i].task_id+"</span></div>" 
        }else if(taskType == "Workflow" && typeof(result[i].childwfid) == "undefined"){
          UI+="<div class='' style='width: 5%;'></div>" 
        }else{
          UI+="<div class='' style='width: 5%;'><span class='defaultExceedCls'>"+result[i].task_id+"</span></div>" 
        }
        if (typeof(result[i].task_name)!='undefined') {
          UI += "<div class='defaultExceedCls'   " + editclick + " style='width: 34%;'><span class='defaultExceedCls'  title='" + replaceSpecialCharacter(result[i].task_name) + "'>" + trancate(replaceSpecialCharacter(result[i].task_name), 200) + "</span></div>"
            }
        if(taskType == 'Workflow' && (result[i].type == "MyZone")){
          UI+="<div class='defaultExceedCls' style='width: 9%;'  "+editclick+" > "+taskEndDate+"</div>" 
        }else if(taskType == 'Workflow' && typeof(result[i].childwfid) == "undefined"){
          ////UI+= "<div class='defaultExceedCls ' style='width: 9%;' ></div>" 
          UI+="<div class='defaultExceedCls ' style='width: 9%;' ></div>" 
        }else{
          ////UI+= "<div class='defaultExceedCls ' style='width: 9%;' onclick='editTask("+result[i].task_id+");event.stopPropagation();' >"+taskStartDate+"</div>" 
          UI+="<div class='defaultExceedCls' style='width: 9%;'  "+editclick+" > "+taskEndDate+"</div>" 
        }
        UI+="<div class='defaultExceedCls' style='width: 8%;padding-left:1px;'  "+editclick+">"+totalEstHr+" h "+totalEstMin+" m</div>" 
        +"<div class='defaultExceedCls pl-1' style='width: 6%;"+color+";' id='actTime_"+result[i].task_id+"'>"+actualHour+" h "+actualMinute+" m</div>"
        +"<div class='defaultExceedCls pr-2' style='width: 6%;text-align: center;'> <img class='' title='"+priorityTitle+"' src='"+Priority_image+"' style='width:18px;'  class='img-responsive'></div>" 
        +"<div class='defaultExceedCls pr-2' style='width: 6%;text-align: center;'> <img class='' id='tstatusImg_"+result[i].task_id+"' title='"+taskStatusImageTitle+"'  style='width:22px;' class='img-responsive' src='"+taskStatusImage+"'></div>" 
        +"<div class='defaultExceedCls pr-2' style='width: 6%;text-align: center;'> <img class='' title='"+trending+"'  style='width:22px;' class='img-responsive' src='"+sentimentBgCol+"'></div>" 
        
        if(taskType != 'Workflow' && typeof(result[i].childwfid) == "undefined"){
          UI+="<div class='defaultExceedCls pr-2' style='width: 3%;text-align: left;'> <img class='' title='Task Details' onclick='showTdetails("+result[i].task_id+");event.stopPropagation();' style='width:18px;' class='img-responsive' src='images/menus/info.svg'></div>" 
        }else if(taskType=='Workflow' && place=='workflowsublist'){
          UI+="<div class='defaultExceedCls pr-2' style='width: 3%;text-align: left;'> <img class='' title='Task Details' onclick='showTdetails("+result[i].task_id+");event.stopPropagation();' style='width:18px;' class='img-responsive' src='images/menus/info.svg'></div>" 
        }
        // else if(taskType=='Workflow' && typeof(result[i].childwfid) != "undefined" && result[i].childwfid!="" && place=='workflowsublist' && result[i].childwfid!="0"){
        //   UI+="<div class='defaultExceedCls pr-2' style='width: 3%;text-align: left;'> <img class='' title='Task Details' onclick='fetchWFChild("+result[i].task_id+");event.stopPropagation();' style='width:18px;' class='img-responsive' src='images/menus/info.svg'></div>" 
        // }
        else if(taskType == 'Workflow' && ( result[i].type == "MyZone")){
          UI+="<div class='defaultExceedCls pr-2' style='width: 3%;text-align: left;'> <img class='' title='Task Details' onclick='showTdetails("+result[i].task_id+");event.stopPropagation();' style='width:18px;' class='img-responsive' src='images/menus/info.svg'></div>" 
        }else{
          UI+="<div class='defaultExceedCls pr-2' style='width: 3%;text-align: left;'> <img class='' title='' onclick='event.stopPropagation();' style='width:15px;' class='img-responsive' src=''></div>" 
        }

        UI+= "</div>"//2

        if(taskType != 'Workflow' && typeof(result[i].childwfid) == "undefined"){
          UI +='<div id=\"TaskStatus_'+result[i].task_id+'\" class=\"taskcontent pl-4\" style=\"display:none;\">'
                  +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
                  +'</div>'
              +'</div>'
          UI+='<div class="task_comment_div_'+result[i].task_id+'  px-1 ml-4 my-2 mr-3" style="border: 1px solid rgb(180, 173, 173); border-radius: 10px;display:none"></div>'  
          UI+="<div id='tDetails_"+result[i].task_id+"' class='m-1 rounded border wsScrollBar position-relative tdetailsclass' style='height:auto;max-height: 350px;overflow: auto;display:none;    background: white;'></div>"  
        }else if(taskType=='Workflow' && typeof(result[i].childwfid) != "undefined" && result[i].childwfid!="" && place=='workflowsublist' && result[i].childwfid!="0"){
          UI +='<div id=\"TaskStatus_'+result[i].task_id+'\" class=\"taskcontent pl-4\" style=\"display:none;\">'
                +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
                +'</div>'
            +'</div>'
         UI+='<div class="task_comment_div_'+result[i].task_id+'  px-1 ml-4 my-2 mr-3" style="border: 1px solid rgb(180, 173, 173); border-radius: 10px;display:none"></div>'  
         UI+="<div id='tDetails_"+result[i].task_id+"' class='m-1 rounded border wsScrollBar position-relative tdetailsclass' style='height:auto;max-height: 350px;overflow: auto;display:none;    background: white;'></div>"  
    
      }
        else if(taskType == "Workflow" && place=='workflowsublist'){
            UI +='<div id=\"TaskStatus_'+result[i].task_id+'\" class=\"taskcontent pl-4\" style=\"display:none;\">'
                  +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
                  +'</div>'
              +'</div>'
           UI+='<div class="task_comment_div_'+result[i].task_id+'  px-1 ml-4 my-2 mr-3" style="border: 1px solid rgb(180, 173, 173); border-radius: 10px;display:none"></div>'  
           UI+="<div id='tDetails_"+result[i].task_id+"' class='m-1 rounded border wsScrollBar position-relative tdetailsclass' style='height:auto;max-height: 350px;overflow: auto;display:none;    background: white;'></div>"  
      
        }else if(taskType == 'Workflow' && ( result[i].type == "MyZone")){
          UI +='<div id=\"TaskStatus_'+result[i].task_id+'\" class=\"taskcontent pl-4\" style=\"display:none;\">'
                  +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
                  +'</div>'
              +'</div>'
          UI+='<div class="task_comment_div_'+result[i].task_id+'  px-1 ml-4 my-2 mr-3" style="border: 1px solid rgb(180, 173, 173); border-radius: 10px;display:none"></div>'  
          UI+="<div id='tDetails_"+result[i].task_id+"' class='m-1 rounded border wsScrollBar position-relative tdetailsclass' style='height:auto;max-height: 350px;overflow: auto;display:none;    background: white;'></div>"  

        }else{
          UI +='<div id=\"TaskStatus_'+result[i].workflow_id+'\" class=\"taskcontent pl-4\" style=\"display:none;\"></div>'
          UI+='<div class="task_comment_div_'+result[i].workflow_id+'   px-1 my-2 ml-4 mr-3" style="border: 1px solid rgb(180, 173, 173); border-radius: 10px;display:none" ></div>'
          //UI+="<div id='tDetails_"+result[i].workflow_id+"' class='m-1 rounded border wsScrollBar' style='height: 350px;overflow: auto;display:none;'></div>" 
        }


      UI+="</div>";//1

    
    }

   
    return UI;  


}
// assigneePopup(result[i].assignedIdDetails,createdImg,createdName,taskType,result[i].task_id,Projectid,userStatus);
function assigneePopup(assignedIdDetails,createdImg,createdName,taskType,taskid,Projectid,userStatus,place,historytype,approversList,createdId){
            ///console.log("taskid--->"+taskid);
            var UI="";
            var assigneduserId="";
            var assigneduser_task_status="";
            var assigneduserstatus="";
            var assignedstatusimage="";
            var approversImg="";
            var assigneeclick="";var assigneeclick2="";
 
            assigneeclick = place=="History"?"":"changeStatusOfUserTask("+taskid+","+Projectid+",'listView','"+taskType+"');";
            assigneeclick2 = place=="History"?"":"tListCheck("+taskid+",'"+taskType+"');";

            UI="<div class=' p-1 border rounded participantsBox1'>"
              +"<div style='border-bottom: 1px solid #beb3b3;cursor:default;'>"
                +"<p class='m-0 py-0 px-1' style='color:#888888;'>Created</p>"
                +"<div class='media p-1 align-items-center'>"
                if(createdId == userIdglb){
                  UI+="<img onclick='event.stopPropagation();getNewConvId("+createdId+", this);' src='"+createdImg+"' title='"+createdName+"'  onerror='userImageOnErrorReplace(this);' class='rounded-circle' style='width:22px;height:22px;'>"
                }else{
                  UI+="<img onclick='event.stopPropagation();getNewConvId("+createdId+", this);' src='"+createdImg+"' title='"+createdName+"'  onerror='userImageOnErrorReplace(this);' class='rounded-circle cursor' style='width:22px;height:22px;'>"
                }
                UI+="<div class='media-body pl-2 defaultExceedCls'>"
                    +"<p class='m-0 p-0 defaultExceedCls' title='"+createdName+"'>"+createdName+"</p>"
                  +"</div>"
                +"</div>"
              +"</div>"
              UI+="<div class='wsScrollBar' id='mainScroll_"+taskid+"' style='overflow-y: auto;height: 160px;'>"
              if( taskType != 'Workflow' || historytype=='MyZone'){
                UI+="<div class='wsScrollBar' style='border-bottom: 1px solid #beb3b3;'>"
                  +"<div class='d-flex'>"
                    +"<p class='m-0 py-0 px-1' style='color:#888888;width:70%;'>Assignees</p>"
                    +"<p class='m-0 py-0 pr-2' style='width:15%;'>Action</p>"
                    +"<p class='m-0 py-0 px-1' style='width:15%;'>Done</p>"
                  +"</div>"
                  if(typeof(assignedIdDetails)!='undefined'){
                    // console.log(assignedIdDetails.length);
                    if(assignedIdDetails.length!=0){
                    for(let j=0;j<assignedIdDetails.length;j++){
                      
                      assigneduserId=assignedIdDetails[j].user_id;
                      assigneduser_task_status=assignedIdDetails[j].user_task_status;
                      if(assigneduser_task_status=="Created"){
                        assigneduserstatus="Created";
                        assignedstatusimage = "images/task/user-start.svg";
                      }else if(assigneduser_task_status=="Inprogress"){
                        assigneduserstatus="Inprogress";
                        assignedstatusimage = "images/task/user-inprogress.svg";
                      }else if(assigneduser_task_status=="Paused"){
                        assigneduserstatus="Paused";
                        assignedstatusimage = "images/task/user-pause.svg";
                      }else if(assigneduser_task_status=="Completed"){
                        assigneduserstatus="Completed";
                        assignedstatusimage = "images/task/user-completed.svg";
                      }else{
                        assignedstatusimage = "images/task/user-view.svg";
                      }
                      
                      UI+="<div class='media px-1 py-2  align-items-center'>"
                      if(assigneduserId == userIdglb){
                          UI+="<img onclick='event.stopPropagation();getNewConvId("+assigneduserId+", this);' src='"+assignedIdDetails[j].imagePathUrl+"' title='"+assignedIdDetails[j].userName+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle' style='width:22px;height:22px;cursor:default;'>"
                      }else{
                         UI+="<img onclick='event.stopPropagation();getNewConvId("+assigneduserId+", this);' src='"+assignedIdDetails[j].imagePathUrl+"' title='"+assignedIdDetails[j].userName+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle cursor' style='width:22px;height:22px;cursor:default;'>"
                      }
                      UI+="<div class='media-body pl-2 defaultExceedCls d-flex align-items-center'>"
                            +"<div style='width: 70%;cursor:default;'><p class='m-0 p-0 defaultExceedCls'  title='"+assignedIdDetails[j].userName+"'>"+assignedIdDetails[j].userName+"</p></div>"
                            if(assigneduserId == userIdglb && assignedIdDetails[j].user_task_status != "Completed"){
                              UI+="<div style='width: 15%;'><img src='"+assignedstatusimage+"' title='"+assigneduserstatus+"' id=\"taskPlay_"+taskid+"\" onclick=\""+assigneeclick+"event.stopPropagation();\" statusOfTaskUser=\""+userStatus+"\" style='width:18px;height:18px;cursor:pointer;'></div>"
                              +"<div style='width: 15%;' class='mt-auto pl-2'><img onclick=\""+assigneeclick2+"event.stopPropagation();\" id='tasklistcheckbox_"+taskid+"' src='images/task/uncheck.svg' style='width:18px;height:18px;cursor:pointer;'></div>"//<input class='' type='checkbox' value='' style='cursor:pointer;' onclick=\"tListCheck("+taskid+",'"+taskType+"');\" id='tasklistcheckbox_"+taskid+"'>
                            }else if(assigneduserId == userIdglb && assignedIdDetails[j].user_task_status == "Completed"){
                              UI+="<div style='width: 15%;cursor:default;'><img src='"+assignedstatusimage+"' title='"+assigneduserstatus+"'   statusOfTaskUser=\""+userStatus+"\" style='width:18px;height:18px;'></div>"
                              +"<div style='width: 15%;cursor:default;' class='mt-auto pl-2'><img  src='images/task/check.svg' style='width:18px;height:18px;'></div>"//<input class='' type='checkbox' value='' id='tasklistcheckbox_"+taskid+"' disabled>
                            }else if(assigneduserId != userIdglb && assignedIdDetails[j].user_task_status == "Completed"){
                              UI+="<div style='width: 15%;cursor:default;'><img src='"+assignedstatusimage+"' title='"+assigneduserstatus+"'   statusOfTaskUser=\""+userStatus+"\" style='width:18px;height:18px;'></div>"
                              +"<div style='width: 15%;cursor:default;' class='mt-auto pl-2'><img  src='images/task/check.svg' style='width:18px;height:18px;'></div>"//<input class='' type='checkbox' value='' id='tasklistcheckbox_"+taskid+"' disabled>
                            }else{
                              UI+="<div style='width: 15%;cursor:default;'><img src='"+assignedstatusimage+"' title='"+assigneduserstatus+"' style='width:18px;height:18px;'></div>"
                              +"<div style='width: 15%;cursor:default;' class='mt-auto pl-2'><img src='images/task/uncheck.svg' style='width:18px;height:18px;'></div>"//<input class='' type='checkbox' value='' id='' disabled>
                            }
                            
                          UI+="</div>"
                      +"</div>"
                    
                      
                    }
                    }else{
                      UI+="<div class='media px-1 py-2  align-items-center'>"
                      +"<div>No Assignees</div>"
                    +"</div>"
                    }
                  }
                  // else{
                  //   UI+="<div class='media px-1 py-2  align-items-center'>"
                  //     +"<div>No user found</div>"
                  //   +"</div>"
                  // }
                  




                UI+="</div>"
                if(typeof(approversList)!='undefined' && approversList.length!=0){

                UI+="<div class='wsScrollBar' style=''>"
                  +"<div class='d-flex'>"
                    +"<p class='m-0 py-0 px-1' style='color:#888888;width:70%;'>Approvers</p>"
                    +"<p class='m-0 py-0 pr-2' style='width:30%;'></p>"
                  +"</div>"

                
                  
                  
                    // console.log("taskId--->"+taskId);
                    if(approversList.length!=0){
                    for(let k=0;k<approversList.length;k++){
                      approversImg= lighttpdpath + "/userimages/" + approversList[k].user_id+"."+approversList[k].user_image_type + "?" + d.getTime();
                      UI+="<div class='media px-1 py-2  align-items-center'>"
                      if(approversList[k].user_id == userIdglb){
                        UI+="<img onclick='event.stopPropagation();getNewConvId("+approversList[k].user_id+", this);' src='"+approversImg+"' title='"+approversList[k].user_full_name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle ' style='width:22px;height:22px;cursor:default;'>"
                      }else{
                        UI+="<img onclick='event.stopPropagation();getNewConvId("+approversList[k].user_id+", this);' src='"+approversImg+"' title='"+approversList[k].user_full_name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle cursor' style='width:22px;height:22px;cursor:default;'>"
                      }
                      UI+="<div class='media-body pl-2 d-flex align-items-center'>"
                            +"<div style='width: 70%;cursor:default;'><p class='m-0 p-0 defaultExceedCls'  title='"+approversList[k].user_full_name+"'>"+approversList[k].user_full_name+"</p></div>"
                            +"<div style='width: 15%;'>"
                                +'<img id="appNewImg_'+approversList[k].user_id+taskid+'" onclick="approveOptionList('+approversList[k].user_id+',\'editList\','+taskid+','+Projectid+');" src="/images/task/pending_approval.svg" title="Pending approval" onclick="" class="mr-3 appIgm" style="width:22px;height:22px;">'
                                
                            +"</div>"
                            +"<div style='width: 15%;'>"
                                +'<img onclick="" id="appListCheckBox_'+approversList[k].user_id+taskid+'" src="images/task/check.svg" style="width:18px;height:18px;cursor:pointer;display:none;margin-left: 8px;">'
                            +"</div>"
                          +"</div>"
                        +"</div>"
                    }
                  }else{
                  //   UI+="<div class='media px-1 py-2  align-items-center'>"
                  //   +"<div>No user found</div>"
                  // +"</div>"
                  }
                  UI+="</div>"
                  }


                

                


              }else if(taskType=='Workflow' && place=='workflowsublist'){
                UI+="<div class='wsScrollBar' style='overflow-y: auto;'>"
                  +"<div class='d-flex'>"
                    +"<p class='m-0 py-0 px-1' style='color:#888888;width:70%;'>Assignees</p>"
                    +"<p class='m-0 py-0 pr-2' style='width:15%;'>Action</p>"
                    +"<p class='m-0 py-0 px-1' style='width:15%;'>Done</p>"
                  +"</div>"
                  if(typeof(assignedIdDetails)!='undefined'){
                    for(let j=0;j<assignedIdDetails.length;j++){
                      assigneduserId=assignedIdDetails[j].user_id;
                      assigneduser_task_status=assignedIdDetails[j].user_task_status;
                      if(assigneduser_task_status=="Created"){
                        assigneduserstatus="Created";
                        assignedstatusimage = "images/task/user-start.svg";
                      }else if(assigneduser_task_status=="Inprogress"){
                        assigneduserstatus="Inprogress";
                        assignedstatusimage = "images/task/user-inprogress.svg";
                      }else if(assigneduser_task_status=="Paused"){
                        assigneduserstatus="Paused";
                        assignedstatusimage = "images/task/user-pause.svg";
                      }else if(assigneduser_task_status=="Completed"){
                        assigneduserstatus="Completed";
                        assignedstatusimage = "images/task/user-completed.svg";
                      }else{
                        assignedstatusimage = "images/task/user-view.svg";
                      }
                      // if(taskType == 'Workflow' && typeof(childwfid) == "undefined"){
                      //   UI+='<div class=""></div>'
                      // }else{
                        UI+="<div class='media px-1 py-2  align-items-center'>"
                        if(assigneduserId == userIdglb){
                          UI+="<img onclick='event.stopPropagation();getNewConvId("+assigneduserId+", this);' src='"+assignedIdDetails[j].imagePathUrl+"' title='"+assignedIdDetails[j].userName+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle ' style='width:22px;height:22px;cursor:default;'>"
                        }else{
                          UI+="<img onclick='event.stopPropagation();getNewConvId("+assigneduserId+", this);' src='"+assignedIdDetails[j].imagePathUrl+"' title='"+assignedIdDetails[j].userName+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle cursor' style='width:22px;height:22px;cursor:default;'>"
                        }
                        UI+="<div class='media-body pl-2 defaultExceedCls d-flex align-items-center'>"
                          +"<div style='width: 70%;cursor:default;'><p class='m-0 p-0 defaultExceedCls'  title='"+assignedIdDetails[j].userName+"'>"+assignedIdDetails[j].userName+"</p></div>"
                          if(assigneduserId == userIdglb && assignedIdDetails[j].user_task_status != "Completed"){
                            UI+="<div style='width: 15%;'><img src='"+assignedstatusimage+"' title='"+assigneduserstatus+"' id=\"taskPlay_"+taskid+"\" onclick=\"changeStatusOfUserTask("+taskid+","+Projectid+",'listView','"+taskType+"');event.stopPropagation();\" statusOfTaskUser=\""+userStatus+"\" style='width:18px;height:18px;cursor:pointer;'></div>"
                            +"<div style='width: 15%;' class='mt-auto pl-2'><img onclick=\"tListCheck("+taskid+",'"+taskType+"');\" id='tasklistcheckbox_"+taskid+"' src='images/task/uncheck.svg' style='width:18px;height:18px;cursor:pointer;'></div>"//<input class='' type='checkbox' value='' style='cursor:pointer;' onclick=\"tListCheck("+taskid+",'"+taskType+"');\" id='tasklistcheckbox_"+taskid+"'>
                          }else if(assigneduserId == userIdglb && assignedIdDetails[j].user_task_status == "Completed"){
                            UI+="<div style='width: 15%;cursor:default;'><img src='"+assignedstatusimage+"' title='"+assigneduserstatus+"'   statusOfTaskUser=\""+userStatus+"\" style='width:18px;height:18px;'></div>"
                            +"<div style='width: 15%;cursor:default;' class='mt-auto pl-2'><img  src='images/task/check.svg' style='width:18px;height:18px;'></div>"//<input class='' type='checkbox' value='' id='tasklistcheckbox_"+taskid+"' disabled>
                          }else if(assigneduserId != userIdglb && assignedIdDetails[j].user_task_status == "Completed"){
                            UI+="<div style='width: 15%;cursor:default;'><img src='"+assignedstatusimage+"' title='"+assigneduserstatus+"'   statusOfTaskUser=\""+userStatus+"\" style='width:18px;height:18px;'></div>"
                            +"<div style='width: 15%;cursor:default;' class='mt-auto pl-2'><img  src='images/task/check.svg' style='width:18px;height:18px;'></div>"//<input class='' type='checkbox' value='' id='tasklistcheckbox_"+taskid+"' disabled>
                          }else{
                            UI+="<div style='width: 15%;cursor:default;'><img src='"+assignedstatusimage+"' title='"+assigneduserstatus+"' style='width:18px;height:18px;'></div>"
                            +"<div style='width: 15%;cursor:default;' class='mt-auto pl-2'><img src='images/task/uncheck.svg' style='width:18px;height:18px;'></div>"//<input class='' type='checkbox' value='' id='' disabled>
                          }
                          
                        UI+="</div>"
                      +"</div>"
                     // }
                    }
                  }
                UI+="</div>"
                
                if(typeof(approversList)!='undefined' && approversList.length!=0){

                  UI+="<div class='wsScrollBar' style=''>"
                    +"<div class='d-flex'>"
                      +"<p class='m-0 py-0 px-1' style='color:#888888;width:70%;'>Approvers</p>"
                      +"<p class='m-0 py-0 pr-2' style='width:30%;'>Action</p>"
                    +"</div>"
  
                  
                    
                    
                      // console.log("taskId--->"+taskId);
                      if(approversList.length!=0){
                      for(let k=0;k<approversList.length;k++){
                        approversImg= lighttpdpath + "/userimages/" + approversList[k].user_id+"."+approversList[k].user_image_type + "?" + d.getTime();
                        UI+="<div class='media px-1 py-2  align-items-center'>"
                        if(approversList[k].user_id == userIdglb){
                          UI+="<img onclick='event.stopPropagation();getNewConvId("+approversList[k].user_id+", this);' src='"+approversImg+"' title='"+approversList[k].user_full_name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle' style='width:22px;height:22px;cursor:default;'>"
                        }else{
                          UI+="<img onclick='event.stopPropagation();getNewConvId("+approversList[k].user_id+", this);' src='"+approversImg+"' title='"+approversList[k].user_full_name+"' onerror='userImageOnErrorReplace(this);' class='rounded-circle cursor' style='width:22px;height:22px;cursor:default;'>"
                        }
                        UI+="<div class='media-body pl-2 d-flex align-items-center'>"
                              +"<div style='width: 70%;cursor:default;'><p class='m-0 p-0 defaultExceedCls'  title='"+approversList[k].user_full_name+"'>"+approversList[k].user_full_name+"</p></div>"
                              +"<div style='width: 15%;position: relative;'>"
                                  +'<img id="appNewImg_'+approversList[k].user_id+taskid+'" onclick="approveOptionList('+approversList[k].user_id+',\'editList\','+taskid+');" src="/images/task/pending_approval.svg" title="Pending approval" onclick="" class="mr-3 rounded-circle appIgm" style="width:18px;height:18px; margin-top: -5px;">'
                              +"</div>"
                              +"<div style='width: 15%;'>"
                                  +'<img onclick="" id="appListCheckBox_'+approversList[k].user_id+taskid+'" src="images/task/check.svg" style="width:18px;height:18px;cursor:pointer;display:none;margin-left: 8px;margin-bottom: 4px;">'
                              +"</div>"
                            +"</div>"
                          +"</div>"
                      }
                    }else{
                    //   UI+="<div class='media px-1 py-2  align-items-center'>"
                    //   +"<div>No user found</div>"
                    // +"</div>"
                    }
                    UI+="</div>"
                    }
                
              }
              UI+="</div>"
            UI+="</div>"
            UI+='<div class="position-absolute rounded p-2 border statusListNew1" id="apprList_'+taskid+'" style="background-color: rgb(255, 255, 255); color: black; font-size: 11px; box-shadow: rgba(0, 0, 0, 0.18) 0px 6px 12px; width: 137px; display: none;top:20px;left:3px;height: 130px;z-index: 1000;"></div>'
            
            

            return UI;


}

function createTaskGridUI(result){

  let UI = "";
  var mzProjectname = "";
  var Projectid = "";
  var mzPrjImgType = "";
  let assigned="";
  let assigneduserId="";
  let assigneduserstatus="";
  let assignedstatusimage="";
  let assigneduser_task_status="";
  let loginuserid="";
  // let approveuserid="";
  let checkloginuserid="";
  let asignimg="";
  var displayId="";
  var displayId1="";

  for(i=0;i<result.length;i++){
    var taskType =result[i].task_type; 
    var source_id=result[i].source_id;
    var Priority=result[i].priority;
    var approversList=result[i].approval_id;
    var Priority_image = (Priority == "")? "" : Priority == "1" ? "images/task/p-one.svg" : Priority == "2" ? "images/task/p-two.svg":Priority == "3" ? "images/task/p-three.svg" :Priority == "4" ? "images/task/p-four.svg":Priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
    var priorityTitle = Priority == "" ? "": Priority == "1" ? "Very important" : Priority == "2" ?"Important": Priority == "3" ? "Medium": Priority == "4"  ? "Low": Priority == "5"  ? "Very low": Priority == "6" ? "None" :"";
    assigned=  result[i].assignedIdDetails;      
    var taskImage = "";
        var taskImageTitle = "";
    if(taskType == 'Tasks' && source_id == "0"){
      taskImage = "images/task/task.svg";
      taskImageTitle = "Task";
    }else if(taskType == 'Tasks'){
      taskImage = "images/task/convo-task.svg";
      taskImageTitle = "Conversation Task";
    }else if(taskType == "WorkspaceDocument" || taskType == 'Document'){
      taskImage = "images/task/doc-task.svg";
      taskImageTitle = "Document Task";
    }else if(taskType == 'Idea'){
      taskImage = "images/task/idea-task.svg";
      taskImageTitle = "Idea Task";
    }else if(taskType == 'Sprint'){
      taskImage = "images/task/sprint-task.svg";
      taskImageTitle = "Sprint Task";
    }else if(taskType == 'Highlight'){
      taskImage = "images/task/task.svg";
      taskImageTitle = "Highlight Task";
    }else if(taskType == 'Email'){
      taskImage = "images/task/email-task.svg";
      taskImageTitle = "Email Task";
    }else if(taskType == 'Event'){
      taskImage = "images/task/event.svg";
      taskImageTitle = "Event";
    }else if(taskType == 'Workflow'){
      taskImage = "images/task/workflow.svg";
      taskImageTitle = "Workflow Task";
    }else{
      taskImage = "images/task/task.svg";
      taskImageTitle = "Task";
    }

    var taskStatusImageTitle=result[i].display_status;
    var taskStatusImage = taskStatusImageTitle == "Completed" ? "images/task/completed.svg" : taskStatusImageTitle == "InProgress" ? "images/task/in-progress.svg" : taskStatusImageTitle == "Created" ? "images/task/in-progress.svg" : taskStatusImageTitle == "InComplete" ? "images/task/in-complete.svg" : taskStatusImageTitle == "NotCompleted" ? "images/task/not-completed.svg"  : "images/task/not-completed.svg";
    if(taskStatusImageTitle=="Created"){taskStatusImageTitle="In Progress";}
    else if(taskStatusImageTitle=="NotCompleted"){taskStatusImageTitle="Not Completed";}
    else if(taskStatusImageTitle=="InProgress"){taskStatusImageTitle="In Progress";}
    else if(taskStatusImageTitle=="InComplete"){taskStatusImageTitle="Incomplete";}


    checkloginuserid=result[i].assigned_id;
      loginuserid = checkloginuserid.includes(userIdglb);
      // approveuserid = 
      if(loginuserid == true){
        asignimg="/images/task/list_assignee_cir.svg";
      }else{
        asignimg="/images/task/list_assignee_nocir.svg";
      }

      var createdImg='';var createdName="";let createdId="";
      if(typeof result[i].createdByDetails[0]!='undefined'){
        createdImg=result[i].createdByDetails[0].imagePathUrl;
        createdName=result[i].createdByDetails[0].userName;
        createdId=result[i].createdByDetails[0].user_id;
      }else{
        console.log(result[0].task_id);
      }

      var width="";var taskTypeWidth="";var taskgroupmClass="";var userImgp="";var prjimgml="";var prjimgmr="";let tasknamepadding="";let taskcontent="";let task_id_width="width:6%;";let task_id_class="";let group_data="";
      var taskgrpmr="";var typepr="";
      if(gloablmenufor=="Myzone"){
        mzProjectname = result[i].project_name;
        Projectid = result[i].project_id;
        mzPrjImgType = result[i].project_image;
        width="width:80%";taskTypeWidth="width:6%";taskgroupmClass="ml-3";userImgp="pl-2";prjimgml="ml-4";prjimgmr="";
        taskgrpmr="mr-2";
        tasknamepadding=""
        taskcontent=""
        task_id_width="width:7%";
        task_id_class="pl-2";
        group_data="ml-2";
        typepr="pr-3";
    }else{
        Projectid = prjid;
        width="width:93%";taskTypeWidth="width:6%";taskgroupmClass="ml-1";userImgp="";prjimgml="";prjimgmr="";
        taskgrpmr="";
        tasknamepadding="pl-1"
        taskcontent="px-2";
        task_id_class="ml-1 pl-1";
    }

    if(Projectid=="0" || Projectid=="undefined"){
      displayId="aaa"; 
    }else{
      displayId="bbbb"; 
    }

      var statusImg="";
    var userStatus="";
    var userTaskStatus = result[i].user_task_status;
    if(userTaskStatus=="Created"){
        userStatus="Created";
        statusImg = "images/task/user-start.svg";
    }else if(userTaskStatus=="Inprogress"){
        userStatus="Inprogress";
        statusImg = "images/task/user-inprogress.svg";
    }else if(userTaskStatus=="Paused"){
        userStatus="Paused";
        statusImg = "images/task/user-pause.svg";
    }else if(userTaskStatus=="Completed"){
        userStatus="Completed";
        statusImg = "images/task/user-completed.svg";
    }else{
        statusImg = "images/task/user-view.svg";
    }

    var trending=result[i].trending;
    var sentimentBgCol ="";
    if(trending >=-1 && trending <=-0.49){
        sentimentBgCol='images/tasksNewUI_old/meterred.png';
    }else if(trending >=-0.50 && trending <0){
        sentimentBgCol='images/tasksNewUI_old/meteryellow.png';
    }else if(trending == 0 || trending == 0.0){
        sentimentBgCol='images/tasksNewUI_old/meteryellow_center.png';
    }else if((trending >0 || trending > 0.0)&& trending <=0.49){
        sentimentBgCol='images/tasksNewUI_old/meteryellow_Lightgreen.png';
    }else if(trending >=0.5 && trending <=1){
        sentimentBgCol='images/tasksNewUI_old/meter_fullgreen.png';
    }else{
        trending = getValues(companyLabels,"Nodata");
        sentimentBgCol='images/tasksNewUI_old/meterDefault.png';  
    }

    var backgrounColor = result[i].group_code;
    if(typeof(backgrounColor) == "undefined"){
      backgrounColor = "white";
    }
    if(taskType == 'Sprint' || taskType == 'Sprint Task'){
      backgrounColor=result[i].sprint_color_code;
      backgrounColor = typeof(backgrounColor)=="undefined"||backgrounColor==""?"#f2788f":backgrounColor;
    }


    if(taskType == 'Workflow' && typeof(result[i].workflow_id) != "undefined"){
      UI+='<div class="gridBox" id="WFtask_'+result[i].workflow_id+'" style="background-color: '+backgrounColor+';"  onmouseover="closeTaskParticipants(event);event.stopPropagation();" >'
      +'<div class="dddd w-100" onclick="fetchWFdetails('+result[i].workflow_id+');event.stopPropagation();">'
      +'<div class="mt-2 px-1 d-flex justify-content-between w-100">'
          // if(Projectid != "0" || Projectid !="undefined"){
            +'<div class="px-2 tasklistProjectImage" id="'+displayId+'"  style="display:none;"><img title="'+mzProjectname+'" onerror="userImageOnErrorReplace(this)" src="'+lighttpdpath+"/projectimages/"+Projectid+"."+mzPrjImgType+'" class="rounded img-responsive" style="width: 20px;height: 20px;" ></div>'
          // }
        +'<div class="px-2"><img src="'+taskImage+'" class="task-img img-responsive" style="" ></div>'
        +'<div class="px-2">'
          +'<img class="position-relative" onclick="event.stopPropagation();" onmouseover="openTaskParticipants('+result[i].task_id+',this);event.stopPropagation();" style="width: 20px;height:20px;" src="'+asignimg+'"/>'
          +"<div id='opentaskParticipantdiv_"+result[i].task_id+"' onclick='event.stopPropagation();' class='position-absolute participantsBox' style='display: none;'>"
            UI+=assigneePopup(result[i].assignedIdDetails,createdImg,createdName,taskType,result[i].task_id,Projectid,userStatus,"",result[i].type,approversList,createdId);
          UI+="</div>"
        +'</div>'
        

          +"<div class='px-2'><img class='' title='"+priorityTitle+"' src='"+Priority_image+"' style='width:18px;'  class='img-responsive'></div>"
          +"<div class='px-2'><img class='' id='tstatusImg_"+result[i].task_id+"' title='"+taskStatusImageTitle+"'  style='width:20px;' class='img-responsive' src='"+taskStatusImage+"'></div>"
          +"<div class='px-2'><img class='' title='"+trending+"'  style='width:20px;' class='img-responsive' src='"+sentimentBgCol+"'></div>"
      +'</div>'

      +'<div class="mt-2 px-3 w-100 defaultExceedMultilineCls" style="" title="'+replaceSpecialCharacter(result[i].task_name)+'">'+replaceSpecialCharacter(result[i].task_name)+'</div>'
      
      
      +'</div>'

      +'<div class="gridBox1" id="task1_'+result[i].task_id+'" style="background-color: '+result[i].group_code+';">'
      +'</div>'

      // UI +='<div id=\"TaskStatus_'+result[i].task_id+'\" class=\"taskcontent1 pl-4\" style=\"display:none;position: absolute;width: 881px;z-index: 109;height: 130px;background: rgb(238, 245, 247);top: 102px;border: 1px solid grey;border-radius: 8px;\">'
      // +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
      // +'</div>'
      // +'</div>'
    
    +'</div>'

    }else if(typeof(result[i].dep_task_id)!='undefined' && result[i].dep_task_id!="-"){
      UI+='<div class="gridBox" id="task_'+result[i].task_id+'" style="background-color: '+backgrounColor+';" onmouseover="closeTaskParticipants(event);event.stopPropagation();" >'
      +'<div class="dddd w-100" onclick="editTask('+result[i].task_id+');event.stopPropagation();">'
      +'<div class="mt-2 px-1 d-flex justify-content-between w-100">'
        +'<div class="px-2 tasklistProjectImage" id="'+displayId+'"  style="display:none;"><img title="'+mzProjectname+'" onerror="userImageOnErrorReplace(this)" src="'+lighttpdpath+"/projectimages/"+Projectid+"."+mzPrjImgType+'" class="rounded img-responsive" style="width: 20px;height: 20px;" ></div>'
        +'<div class="px-2"><img src="'+taskImage+'" class="task-img img-responsive" style="" ></div>'
        +'<div class="px-2">'
          +'<img class="position-relative" onclick="event.stopPropagation();" onmouseover="openTaskParticipants('+result[i].task_id+',this);event.stopPropagation();" style="width: 20px;height:20px;" src="'+asignimg+'"/>'
          +"<div id='opentaskParticipantdiv_"+result[i].task_id+"' onclick='event.stopPropagation();' class='position-absolute participantsBox' style='display: none;'>"
            UI+=assigneePopup(result[i].assignedIdDetails,createdImg,createdName,taskType,result[i].task_id,Projectid,userStatus,"",result[i].type,approversList);
          UI+="</div>"
        +'</div>'
        

          +"<div class='px-2'><img class='' title='"+priorityTitle+"' src='"+Priority_image+"' style='width:18px;'  class='img-responsive'></div>"
          +"<div class='px-2'><img class='' id='tstatusImg_"+result[i].task_id+"' title='"+taskStatusImageTitle+"'  style='width:20px;' class='img-responsive' src='"+taskStatusImage+"'></div>"
          +"<div class='px-2'><img class='' title='"+trending+"'  style='width:20px;' class='img-responsive' src='"+sentimentBgCol+"'></div>"
      +'</div>'

      +'<div class="mt-2 px-3 w-100 defaultExceedMultilineCls" style="" title="'+replaceSpecialCharacter(result[i].task_name)+'">'+replaceSpecialCharacter(result[i].task_name)+'</div>'
      
      
      +'</div>'

      +'<div class="gridBox1" id="task1_'+result[i].task_id+'" style="background-color: '+result[i].group_code+';">'
      +'</div>'
      // UI +='<div id=\"TaskStatus_'+result[i].task_id+'\" class=\"taskcontent1 pl-4\" style=\"display:none;position: absolute;width: 881px;z-index: 109;height: 130px;background: rgb(238, 245, 247);top: 102px;border: 1px solid grey;border-radius: 8px;\">'
      // +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
      // +'</div>'
      // +'</div>'
    
    +'</div>'
    
    }else if(taskType == 'Workflow' && typeof(result[i].workflow_id) == "undefined"){
      UI+='<div class="gridBox" id="task_'+result[i].task_id+'" style="background-color: '+backgrounColor+';" onmouseover="closeTaskParticipants(event);event.stopPropagation();" >'
        +'<div class="dddd w-100" onclick="editTask('+result[i].task_id+');event.stopPropagation();">'
      +'<div class="mt-2 px-1 d-flex justify-content-between w-100">'
        +'<div class="px-2 tasklistProjectImage" id="'+displayId+'"  style="display:none;"><img title="'+mzProjectname+'" onerror="userImageOnErrorReplace(this)" src="'+lighttpdpath+"/projectimages/"+Projectid+"."+mzPrjImgType+'" class="rounded img-responsive" style="width: 20px;height: 20px;" ></div>'
        +'<div class="px-2"><img src="'+taskImage+'" class="task-img img-responsive" style="" ></div>'
        +'<div class="px-2">'
          +'<img class="position-relative" onclick="event.stopPropagation();" onmouseover="openTaskParticipants('+result[i].task_id+',this);event.stopPropagation();" style="width: 20px;height:20px;" src="'+asignimg+'"/>'
          +"<div id='opentaskParticipantdiv_"+result[i].task_id+"' onclick='event.stopPropagation();' class='position-absolute participantsBox' style='display: none;'>"
            UI+=assigneePopup(result[i].assignedIdDetails,createdImg,createdName,taskType,result[i].task_id,Projectid,userStatus,"",result[i].type,approversList,createdId);
          UI+="</div>"
        +'</div>'
        

          +"<div class='px-2'><img class='' title='"+priorityTitle+"' src='"+Priority_image+"' style='width:18px;'  class='img-responsive'></div>"
          +"<div class='px-2'><img class='' id='tstatusImg_"+result[i].task_id+"' title='"+taskStatusImageTitle+"'  style='width:20px;' class='img-responsive' src='"+taskStatusImage+"'></div>"
          +"<div class='px-2'><img class='' title='"+trending+"'  style='width:20px;' class='img-responsive' src='"+sentimentBgCol+"'></div>"
      +'</div>'

      +'<div class="mt-2 px-3 w-100 defaultExceedMultilineCls" style="" title="'+replaceSpecialCharacter(result[i].task_name)+'">'+replaceSpecialCharacter(result[i].task_name)+'</div>'
      +'</div>'
      // UI +='<div id=\"TaskStatus_'+result[i].task_id+'\" class=\"taskcontent1 pl-4\" style=\"display:none;position: absolute;width: 881px;z-index: 109;height: 130px;background: rgb(238, 245, 247);top: 102px;border: 1px solid grey;border-radius: 8px;\">'
      // +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
      // +'</div>'
      // +'</div>'
    
    +'</div>'
    }else{
      UI+='<div class="gridBox" id="task_'+result[i].task_id+'" style="background-color: '+backgrounColor+';" onmouseover="closeTaskParticipants(event);event.stopPropagation();" >'
        +'<div class="dddd w-100" onclick="editTask('+result[i].task_id+');event.stopPropagation();">'
      +'<div class="mt-2 px-1 d-flex justify-content-between w-100">'
        +'<div class="px-2 tasklistProjectImage" id="'+displayId+'" style="display:none;"><img title="'+mzProjectname+'" onerror="userImageOnErrorReplace(this)" src="'+lighttpdpath+"/projectimages/"+Projectid+"."+mzPrjImgType+'" class="rounded img-responsive" style="width: 20px;height: 20px;" ></div>'
        +'<div class="px-2"><img src="'+taskImage+'" class="task-img img-responsive" style="" ></div>'
        +'<div class="px-2">'
          +'<img class="position-relative" onclick="event.stopPropagation();" onmouseover="openTaskParticipants('+result[i].task_id+',this);event.stopPropagation();" style="width: 20px;height:20px;" src="'+asignimg+'"/>'
          +"<div id='opentaskParticipantdiv_"+result[i].task_id+"' onclick='event.stopPropagation();' class='position-absolute participantsBox' style='display: none;'>"
            UI+=assigneePopup(result[i].assignedIdDetails,createdImg,createdName,taskType,result[i].task_id,Projectid,userStatus,"",result[i].type,approversList,createdId);
          UI+="</div>"
        +'</div>'
        

          +"<div class='px-2'><img class='' title='"+priorityTitle+"' src='"+Priority_image+"' style='width:18px;'  class='img-responsive'></div>"
          +"<div class='px-2'><img class='' id='tstatusImg_"+result[i].task_id+"' title='"+taskStatusImageTitle+"'  style='width:20px;' class='img-responsive' src='"+taskStatusImage+"'></div>"
          +"<div class='px-2'><img class='' title='"+trending+"'  style='width:20px;' class='img-responsive' src='"+sentimentBgCol+"'></div>"
      +'</div>'

      +'<div class="mt-2 px-3 w-100 defaultExceedMultilineCls" style="" title="'+replaceSpecialCharacter(result[i].task_name)+'">'+replaceSpecialCharacter(result[i].task_name)+'</div>'
      +'</div>'
      // UI +='<div id=\"TaskStatus_'+result[i].task_id+'\" class=\"taskcontent1 pl-4\" style=\"display:none;position: absolute;width: 881px;z-index: 109;height: 130px;background: rgb(238, 245, 247);top: 102px;border: 1px solid grey;border-radius: 8px;\">'
      // +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
      // +'</div>'
      // +'</div>'
    
    +'</div>'

    }

    // UI+='<div class="gridBox" id="task_'+result[i].task_id+'" style="background-color: '+result[i].group_code+';" onclick="editTask('+result[i].task_id+');event.stopPropagation();" onmouseover="closeTaskParticipants(event);event.stopPropagation();" >'

    //   +'<div class="mt-1 px-1 d-flex justify-content-between w-100">'
    //     +'<div class="px-2"><img src="'+taskImage+'" class="task-img img-responsive" style="" ></div>'
    //     +'<div class="px-2">'
    //       +'<img class="position-relative" onclick="event.stopPropagation();" onmouseover="openTaskParticipants('+result[i].task_id+',this);event.stopPropagation();" style="width: 20px;height:20px;" src="'+asignimg+'"/>'
    //       +"<div id='opentaskParticipantdiv_"+result[i].task_id+"' onclick='event.stopPropagation();' class='position-absolute p-1 ml-0 border rounded participantsBox' style='display: none;'>"
    //         UI+=assigneePopup(result[i].assignedIdDetails,createdImg,createdName,taskType,result[i].task_id,Projectid,userStatus);
    //       UI+="</div>"
    //     +'</div>'
        

    //       +"<div class='px-2'><img class='' title='"+priorityTitle+"' src='"+Priority_image+"' style='width:18px;'  class='img-responsive'></div>"
    //       +"<div class='px-2'><img class='' id='tstatusImg_"+result[i].task_id+"' title='"+taskStatusImageTitle+"'  style='width:20px;' class='img-responsive' src='"+taskStatusImage+"'></div>"
    //       +"<div class='px-2'><img class='' title='"+trending+"'  style='width:20px;' class='img-responsive' src='"+sentimentBgCol+"'></div>"
    //   +'</div>'

    //   +'<div class="mt-2 px-3 w-100 defaultExceedMultilineCls" style="" title="'+replaceSpecialCharacter(result[i].task_name)+'">'+replaceSpecialCharacter(result[i].task_name)+'</div>'
      
    //   UI+=slideui(taskType,result[i].childwfid,result[i].dep_task_id,result[i].task_id,result[i].group_code);
    
    // +'</div>'

    

    
  }

  return UI;

}

function slideui(taskType,childwfid,dep_task_id,task_id,group_code) {
  var UI = "";

  if(taskType == 'Workflow' && typeof(childwfid) == "undefined"){
    UI+='<div class="gridBox1" id="task1_'+task_id+'" style="background-color: '+group_code+';">'
      +'</div>'
  }else if(typeof(dep_task_id)!='undefined' && dep_task_id!="-"){
    UI+='<div class="gridBox1" id="task1_'+task_id+'" style="background-color: '+group_code+';">'
      +'</div>'
  }


  return UI;
}




function tListCheck(tId,tType){
  if($('#tasklistcheckbox_'+tId).attr('src') == 'images/task/uncheck.svg'){
    taskidglb=tId;
    tasktypeglb=tType;
    $('#tasklistcheckbox_'+tId).attr('src','images/task/check.svg');
    confirmReset("Do you want to complete this task?",'delete','okCheck','cancelCheck',"Yes","No");
  }
}

function okCheck(){
  updateListViewTask(taskidglb, tasktypeglb, prjid, '','','','checkBoxUpdate');
  $('#opentaskParticipantdiv_'+taskidglb).css('display','none');
}
function cancelCheck(){
  $('#tasklistcheckbox_'+taskidglb).attr('src','images/task/uncheck.svg');
  taskidglb="";
}

 function showTdetails(tId){
  if($("#addTaskDiv").attr('changestatus')=='Y'){
    let messege="Do you want to save the changes?";
    conFunNew(messege,'warning','saveTask','canceltask');
    $("#addTaskDiv").attr('tId',tId);
  }else{
    $("#canceltask").trigger('onclick');
    detailsOfcomment(tId);
  }
}



function openTaskParticipants(tId,obj){
  // if(typeof(taskidglb)!="undefined" && taskidglb!=""){
  //   $('#opentaskParticipantdiv_'+taskidglb).css('display','none');
  //   taskidglb="";
  // }
  // if($('#opentaskParticipantdiv_'+tId).is(':visible')){
  //   taskidglb="";
  //   $('#opentaskParticipantdiv_'+tId).css('display','none');
  // }else{
  //   taskidglb=tId;
  //   $('#opentaskParticipantdiv_'+tId).css('display','block');
  // }
  $('div[id^=opentaskParticipantdiv_]').hide();
  taskidglb=tId;
  $(obj).next().show();

}

function hideTaskParticipants(tId,obj){
  $(obj).next().hide();
}

function closeTaskParticipants(event){
  var $target = $(event.target);
  if(!$target.is("#opentaskParticipantdiv_"+taskidglb) &&  !$target.parents().is("#opentaskParticipantdiv_"+taskidglb) && !$target.is("#assignedPopup_"+taskidglb) &&  !$target.parents().is("#assignedPopup_"+taskidglb)){
    //$('#opentaskParticipantdiv_'+taskidglb).css('display','none');
    $('div[id^=opentaskParticipantdiv_'+taskidglb+']').css('display','none');
    taskidglb="";
  }
  
}


  function trancate(string, n) {
    return string?.length > n ? string.substring(0, n - 1) + ". . ." : string;
  }

  function screenresize(screen){
    if(typeof(prjid)=="undefined" || prjid==""){
      if (screen.matches) {
        $('.types-usercontent').css('width','18%');
      }else{
        $('.types-usercontent').css('width','30%');
      }
    }else{
      if (screen.matches) {
        $('.types-usercontent').css('width','10%');
      }else{
        $('.types-usercontent').css('width','37%');
      }
    }
  }

  if(globalmenu=="Task"){
    $("#content").on('scroll',()=>{
      if( scrollFlag && $('#content').scrollTop() +$('#content').height() >  $("#taskList").height() -70){ 
           indexForTask=limitForTask+indexForTask;
           if(typeof(prjid)=="undefined" || prjid==""){
            fetchmzTasks(mzTasktype,"");
           }else{
            fetchTasks('');
           }
           scrollFlag=false;
           setTimeout( function(){
             scrollFlag=true;
           },1000);
      }
    })
  }
  
  function listTask(obj,mainId,moduleAction){

    if($(obj).attr('src').indexOf("Toggle.png") == -1 ){
      $('#loadingBar').addClass('d-flex');
      $(obj).attr('src', 'images/task1Toggle.png');
      let jsonbody = {
        "user_id":userIdglb,
        "company_id":companyIdglb,
        "source_id":mainId
      }
      $.ajax({
        url: apiPath+"/"+myk+"/v1/getTaskDetails/"+moduleAction,
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
         ///data:{act:"listConvoTask",feedId:feedId},
         error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $('#loadingBar').addClass('d-none').removeClass('d-flex');
                }, 
         success:function(result){
          
          $('#TaskListDiv_'+mainId).html(taskHeaderUI(mainId));
          $('#taskListContentDiv_'+mainId).html(createTaskUI(result));
          loadNewAccountLabel('conversationactFeed');
          $('#TaskListDiv_'+mainId).slideDown(600);
          
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
      });
    }else{
      $('#TaskListDiv_'+mainId).slideUp(function(){
           $(obj).attr('src', 'images/task/taskdropdown.svg'); 
           $('#TaskListDiv_'+mainId).html('');
      });
    }
		
	}

  function taskHeaderUI(mainId){
    let ui = "";
    
    ui +=
      "<div class='TaskDiv'>" +
      "<div class='d-flex px-2 py-1 min-vw-100 vw-100 headerBar'> " +
      "<div id='headertaskType' class='pl-0 headerTaskPrj' style='text-align:center;width:10%'>Type</div>" +
      "<div class='types pl-2 headerTaskPrj'> ID</div>" +
      "<div id='headerTaskname' class='types pl-2' style='width:50%' > Task</div>" +
      "<div class='pl-1 types' style='width:13%'> Start</div>" +
      "<div class='types' style='width:13%'> Due</div>" +
      "<div class='types'> Estimate</div>" +
      "<div class='types'> Actual</div>" +
      "<div class='types' style='width:8%'> Priority</div>" +
      "<div class='types' style='width:8%'> Status</div>" +
      "<div class='types' style='width:8%'> Trend</div>" +
      "</div>" +
      "</div>"+
      "<div id=\"taskListContentDiv_"+mainId+"\" class=\"\" style=\"\"></div>";
  
      
     
    $("#TaskListDiv_"+mainId).html(ui);
  
    
  }

  async function listWFtaskSteps(wfId){
    if($("#wfListDiv_"+wfId).length==0){
      $('#loadingBar').addClass('d-flex').removeClass('d-none'); 
      let jsonbody = {
        "user_id":userIdglb,
        "task_id":wfId
      }
    checksession();
     await $.ajax({ 
          url: apiPath+"/"+myk+"/v1/getTaskDetails/workflowSubList",
        //url:"http://localhost:8080/v1/getTaskDetails/workflowSubList",
          type:"POST",
          contentType:"application/json",
          dataType:'json',
          data: JSON.stringify(jsonbody),
          error: function(jqXHR, textStatus, errorThrown) {
            checkError(jqXHR,textStatus,errorThrown);
            $("#loadingBar").hide();
            timerControl("");
            }, 
          success:function(result){
            let UI=""
            UI+="<div class='ml-5 pb-2' id='wfListDiv_"+wfId+"'> "
                  UI+=wsTaskUi('subList');
                  UI+="<div class='Wflisting'>"
                  UI+=createTaskUI(result,'workflowsublist');
                  UI+="</div>"
                  UI+="</div>"
                 
               $(UI).insertAfter("#TaskStatus_"+wfId);
               $('#wfListImage_'+wfId).attr('src','images/task/dependencyup.svg')
               for (i = 0; i < result.length ; i++) {
                $('#tTypeImage_'+result[i].task_id).attr('src','images/task/workflow-task.svg');
               }
               $('.Wflisting').children().last().css('border-bottom','0');
              
          }
        });
    }else{
      $("#wfListDiv_"+wfId).toggle();
      if( $("#wfListDiv_"+wfId).is(":hidden")){
        $('#wfListImage_'+wfId).attr('src','/images/task/dependencydown.svg')
      }else{
        $('#wfListImage_'+wfId).attr('src','/images/task/dependencyup.svg')
      }
      $('.Wflisting').children().last().css('border-bottom','0');
    }
    
    $('#loadingBar').addClass('d-none').removeClass('d-flex');
  }
  

   function saveTask(){
      $("#savetask").trigger('onclick');
      $("#canceltask").trigger('onclick');
      let tId= $("#addTaskDiv").attr('tId');
      detailsOfcomment(tId)
   }
   function canceltask(){
    $("#canceltask").trigger('onclick');
    let tId= $("#addTaskDiv").attr('tId');
    detailsOfcomment(tId)
   }

   function detailsOfcomment(tId){
    $("#addTaskDiv").removeAttr('tId');
    if($('#tDetails_'+tId).is(':visible')){
      $('#tDetails_'+tId).hide().html('');
  
    }else{  
      var localOffsetTime=getTimeOffset(new Date());
      $('.tdetailsclass').hide().html('');
      jsonbody = {
        "user_id":userIdglb,
        "company_id":companyIdglb,
        "project_id":prjid,
        "localOffsetTime":localOffsetTime,
        "action":"fetchinsertFeed",
        "latestFeed":0,
        "parent_feed_id":"0",
        "feedType":"Task",
        "task_id":tId,
        "feedSubType" : "mainView"
      }
      $('#loadingBar').addClass('d-flex').removeClass('d-none');
      $.ajax({
        url: apiPath+"/"+myk+"/v1/getFeedDetails",
        // url:"http://localhost:8080/v1/getFeedDetails",
        type:"POST",
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR,textStatus,errorThrown);
                    $('#loadingBar').addClass('d-none').removeClass('d-flex');
                    //timerControl("");
                    },
        success: function (result) {
          let UI="";
          if(result!=""){
            UI=prepareCommentsUI(result, menuutype, prjid,'');
          }else{
            UI="<p class='p-2 m-0' style='font-size: 12px;text-align: center;'>No Comments found</p>"
          }
          $('#tDetails_'+tId).append(UI).show();
          if(result!=""){
            $('#tDetails_'+tId).prepend('<img src="images/conversation/edit.svg" class="position-absolute" title="Edit Task" onclick="editTask('+tId+');event.stopPropagation();" style="width: 15px;right: 12px;top: 3px;">');
          }
          $("[id^='aFeed1_']").attr('onmouseover','');
          $("[id^='actFeedDiv_']").attr('ondblclick','');
          /* $("[id^='view_']").attr('onclick','');
          $("[id^='customPlayer_']").attr('onclick','');
          $("[id^='vDownload_']").attr('onclick','');
          $("[id^='timeline_']").attr('onclick',''); */
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
        }
      });  
    }
  }


  async function getSprintlist(epicid,projectid){
    sprintStartdateGlb=new Date();;
    sprintEnddateGlb=null;
    projectid = typeof(projectid)=="undefined"||projectid=="undefined"?prjid:projectid;
    let jsonbody = {
        "story_id": epicid,
        "project_id": projectid,
        "company_id":companyIdglb,
        "user_id":userIdglb
    }
    
    await $.ajax({
        url: apiPath + "/" + myk + "/v1/fetchSprintGroupDetails",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(jsonbody),
        error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
             
        },
        success: function (result) {
          if(result!=""){  
            if(result.length > 1){
                $('#spGrouplist').html(groupList(result));
                $('#sprintlistcontent').html(sprintList(result));
            }else{
              if(result[0].sprintData.length!=0 && gloablmenufor!="Myzone" && globalmenu!="Task"){
              
                $('#sprintgroupcolor').css('background-color',result[0].color_code);
                $('#sprintgroupname').text(result[0].sprint_group_name).attr('title',result[0].sprint_group_name).attr('tsprgrpid',result[0].sprint_group_id).css('color','darkgray');
                $('#sprintname').text(result[0].sprintData[0].sprint_title).attr('title',result[0].sprintData[0].sprint_title).attr('tsprintid',result[0].sprintData[0].sprint_id).attr('tspgroupid',result[0].sprintData[0].sprint_group_id).css('color','darkgray');
                var sprintstdate = returnFormatedDate(result[0].sprintData[0].sprint_start_date);
                var sprintenddate = returnFormatedDate(result[0].sprintData[0].sprint_end_date);
                var splitstdate = result[0].sprintData[0].sprint_start_date.split("-");
                var splitenddate = result[0].sprintData[0].sprint_end_date.split("-");
                sprintStartdateGlb=new Date(splitstdate[2], splitstdate[0]-1, splitstdate[1]);
                sprintEnddateGlb=new Date(splitenddate[2], splitenddate[0]-1, splitenddate[1]);
                $('#startDateCalNewUI').text(sprintstdate).attr('parmanentvalue',sprintstdate);
                $('#endDateCalNewUI').text(sprintenddate).attr('parmanentvalue',sprintenddate);
                $('#spGrplistDiv , #sprintlistDiv').children('img').attr('onclick','').css('cursor','default');
                
                //calenderForTask();
                //calenderForTask1();
              }  
            }
            
            if(gloablmenufor=="Myzone"){
              $('#projectlistingDiv option[project_id="'+projectid+'"]').attr('selected','selected');
              prjid=$('#projectselect :selected').attr('project_id');
              if(result.length == 1){
                
                $('#spGrouplist').html(groupList(result));
                $('#sprintlistcontent').html(sprintList(result));
              }
            }  
            if(globalmenu=="Task"){
              if(result.length == 1){
                $('#spGrouplist').html(groupList(result));
                $('#sprintlistcontent').html(sprintList(result));
              }
            }
            
          }
        }
    });
  }

  function groupList(result){
    var ui="";
    var sprintdata="";
    var sprintGrpid="";var sprintGrpname="";var color="";
    if(result != ""){
        for(var i=0; i<result.length ;i++){
            sprintdata = result[i].sprintData;
            
            //for(var j=0; j<sprintdata.length ;j++){
            sprintGrpid = result[i].sprint_group_id//sprintdata[j].sprint_group_id;
            sprintGrpname = result[i].sprint_group_name//sprintdata[j].sprint_group_name;
            color = result[i].color_code;
            color = typeof(color)=="undefined"||color==""?"#df4e":color;
              
            ui+="<div id='spGrp_"+sprintGrpid+"' onclick=\"selectTasksprintGroup("+sprintGrpid+",'"+sprintGrpname+"','"+color+"');event.stopPropagation();\" spgroupname='"+sprintGrpname+"' spgroupid="+sprintGrpid+" class='p-1' style='cursor:pointer;'>"
              +"<div class='d-flex align-items-center'>"
                +"<div class='defaultExceedCls rounded-circle' style='margin-top:2px;width:15px;height:15px;background-color:"+color+"'></div>"
                +"<div class='defaultExceedCls ml-2' style='max-width:200px;'>"+sprintGrpname+"</div>"
              +"</div>"  
              +"<div id='spGrpSprintlist_"+sprintGrpid+"' class='spGrpSprintlistcls pl-2 py-1 m-1' style='background-color: #ddd8d8;display:none;'>"
                  ui+=sprintListsub(sprintdata,color)
              ui+="</div>"
            +"</div>"
            //}
    
        }
    }else{
        ui="<div class='d-flex justify-content-center'>No sprint group found</div>"
    }
    
    
    return ui;
  }

  function sprintList(result){
    var ui="";
    var sprintdata="";
    var sprid="";var sprName="";var sprStdate="";var sprEnddate="";var spGrpname="";var sGrpid="";var color="";
    var sprintstDateformat="";var sprintendDateformat="";var splitstdate="";var splitenddate="";
    if(result != ""){
        for(var i=0; i<result.length ;i++){

            sprintdata = result[i].sprintData;
            color = result[i].color_code;
            
            for(var j=0; j<sprintdata.length ;j++){
                sprid = sprintdata[j].sprint_id;
                sprName = sprintdata[j].sprint_title;
                sprStdate = sprintdata[j].sprint_start_date;
                sprintstDateformat = returnFormatedDate(sprStdate);
                sprEnddate = sprintdata[j].sprint_end_date;
                sprintendDateformat = returnFormatedDate(sprEnddate);
                spGrpname = sprintdata[j].sprint_group_name;
                sGrpid = sprintdata[j].sprint_group_id;
    
                ui+="<div id='subepicsprint_"+sprid+"'class='epicSprintgroup_"+sGrpid+" d-flex align-items-center p-1' onclick=\"sprintFortask("+sprid+","+sGrpid+",'"+sprName+"','"+spGrpname+"','"+color+"','"+sprintstDateformat+"','"+sprintendDateformat+"','"+sprStdate+"','"+sprEnddate+"');event.stopPropagation();\" epicsprintid="+sprid+" group='"+spGrpname+"' groupid='"+sGrpid+"' taskSpstartdate='"+sprStdate+"' taskSpenddate='"+sprEnddate+"'  style='cursor:pointer;'>"
                    +"<div class='defaultExceedCls ml-1' style='max-width:125px;'>"+sprName+"</div>"
                +"</div>"
            
            }
        }
    }else{
        ui="<div class='d-flex justify-content-center'>No sprint found</div>"
    }
    
    return ui;
  }

  function sprintListsub(sprintdata,color){
    var ui="";
    var sprid="";var sprName="";var sprStdate="";var sprEnddate="";var spGrpname="";var sGrpid="";
    var sprintstDateformat="";var sprintendDateformat="";var splitstdate="";var splitenddate="";
    if(sprintdata != ""){
        
            
            for(var j=0; j<sprintdata.length ;j++){
                sprid = sprintdata[j].sprint_id;
                sprName = sprintdata[j].sprint_title;
                sprStdate = sprintdata[j].sprint_start_date;
                sprintstDateformat = returnFormatedDate(sprStdate);
                sprEnddate = sprintdata[j].sprint_end_date;
                sprintendDateformat = returnFormatedDate(sprEnddate);
                spGrpname = sprintdata[j].sprint_group_name;
                sGrpid = sprintdata[j].sprint_group_id;
    
                ui+="<div id='epicsprint_"+sprid+"'class='epicSprintgroup_"+sGrpid+" d-flex align-items-center p-1' onclick=\"sprintFortask("+sprid+","+sGrpid+",'"+sprName+"','"+spGrpname+"','"+color+"','"+sprintstDateformat+"','"+sprintendDateformat+"','"+sprStdate+"','"+sprEnddate+"');event.stopPropagation();\" epicsprintid="+sprid+" group='"+spGrpname+"' groupid='"+sGrpid+"' taskSpstartdate='"+sprStdate+"' taskSpenddate='"+sprEnddate+"'  style='cursor:pointer;'>"
                    +"<div class='defaultExceedCls ml-1' style='max-width:125px;'>"+sprName+"</div>"
                +"</div>"
            
            }
      
    }else{
        ui="<div class='d-flex justify-content-center'>No sprint found</div>"
    }
    
    return ui;
  }

  function selectTasksprintGroup(tSGroupid,tSGroupname,color){
    $(".spGrpSprintlistcls").hide();
    $('#sprintlistcontent > div').addClass('d-flex').removeClass('d-none');
    $('#sprintgroupcolor').css('background-color',color);
    $('#sprintgroupname').text(tSGroupname);
    $('#sprintgroupname').attr('tSprGrpid',tSGroupid).attr('title',tSGroupname);
    $('#sprintlistcontent > :not(.epicSprintgroup_'+tSGroupid+')').addClass('d-none').removeClass('d-flex');
    //$('#spGrouplist').hide();
    $('#sprintname').text('').attr('tsprintid','').attr('tspgroupid','');
    $("#spGrpSprintlist_"+tSGroupid).show();
    var elmnt = document.getElementById("spGrp_"+tSGroupid);
    elmnt.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});

  }

  function sprintFortask(tSprintid,tSprGroupid,tSprintname,tSprGroupname,color,stdateformat,enddateformat,stdate,enddate){
    //$('#spGrouplist > div').addClass('d-flex').removeClass('d-none');
    $('#sprintname').text(tSprintname).attr('tSprintid',tSprintid).attr('tSpgroupid',tSprGroupid).attr('title',tSprintname);
    $('#sprintgroupcolor').css('background-color',color);
    $('#sprintgroupname').text(tSprGroupname);
    $('#sprintgroupname').attr('tSprGrpid',tSprGroupid).attr('title',tSprGroupname);
    $('#sprintlistcontent').hide();
    //$('#spGrouplist > :not(#spGrp_'+tSprGroupid+')').addClass('d-none').removeClass('d-flex');
    $('#startDateCalNewUI').text(stdateformat).attr('parmanentvalue',stdateformat);
    $('#endDateCalNewUI').text(enddateformat).attr('parmanentvalue',enddateformat);
    var splitstdate = stdate.split("-");
    var splitenddate = enddate.split("-");
    sprintStartdateGlb=new Date(splitstdate[2], splitstdate[0]-1, splitstdate[1]);
    sprintEnddateGlb=new Date(splitenddate[2], splitenddate[0]-1, splitenddate[1]);
    calenderForTask();
    calenderForTask1();
    $('.dhtmlxcalendar_material').hide();
    $('#spGrouplist').hide();
  }

  function showtaskSprintlist(){
    $('#sprintlistcontent').toggle();
    $('#spGrouplist').hide();
    var length1 = $('#sprintlistcontent').children('.d-none').length;
    var length2 = $('#sprintlistcontent').children().length;
    if(length1 == length2){
        $('#sprintlistcontent').prepend("<div class='d-flex justify-content-center tempcls'>No sprint found</div>");
    }else{
        $('.tempcls').remove();
    }
  }

  function showtaskSpGrp(){
    $('#spGrouplist').toggle();
    $('#sprintlistcontent').hide();
  }

  function calenTask(view){
    if($("#addTaskDiv").length!=0 && taskview==view){
      showTaskList();
    }else{
      taskview=view;
      $(".calendar-2").html("");
      $(".calendar-2").removeClass("elem-CalenStyle");
      if(gloablmenufor=="Task"){
        let jsonbody=	{
          "user_id":userIdglb,
          "company_id":companyIdglb,
          "limit":"",
          "index":"",
          "sortVal":"",
          "text":"",
          "project_id":prjid,
          "sortTaskType":""
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');
        $.ajax({
          url: apiPath + "/" + myk + "/v1/getTaskDetails/TeamZone",
          type: "POST",
          dataType: 'json',
          contentType: "application/json",
          data: JSON.stringify(jsonbody),
          beforeSend: function (jqXHR, settings) {
            xhrPool.push(jqXHR);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            timerControl("");
          },
          success: function (result) {
            $("#taskOptionview").hide();
            $("#taskList").html("");
            initlistCalendarTask(result);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
          }
        });    
      }else if(gloablmenufor=="Myzone"){
        let jsonbody=	{
          "user_id":userIdglb,
          "company_id":companyIdglb,
          "limit":"",
          "index":"",
          "sortVal":"",
          "text":"",
          "sortTaskType":""
        }
        $('#loadingBar').addClass('d-flex').removeClass('d-none');
        $.ajax({
          url: apiPath + "/" + myk + "/v1/getTaskDetails/"+mzTasktype,
          type:"POST",
          dataType:'json',
          contentType:"application/json",
          data: JSON.stringify(jsonbody),
          beforeSend: function (jqXHR, settings) {
            xhrPool.push(jqXHR);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            timerControl("");
          },
          success: function (result) {
            $("#taskOptionview").hide();
            $("#taskList").html("");
            initlistCalendarTask(result);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
          }
        });    
      }
    }  
  }

  function initlistCalendarTask(result){

    //plugin refer https://www.jqueryscript.net/time-clock/Mobile-friendly-Drag-Drop-Event-Calendar-Plugin-CalenStyle.html

    $(".TaskDiv").hide();
    $(".calendar-2").show();
    
    $(".calendar-2").CalenStyle({
    
        visibleView:"WeekView",
        fullMonthNames: ["January","February","March","April","May","June","July","August","September","October","November","December"],
        shortMonthNames: ["January","February","March","April","May","June","July","August","September","October","November","December"],
        viewsToDisplay: [
                {
                    viewName:"DayView",
                    viewDisplayName:"Day"
                },
                {
                    viewName:"WeekView",
                    viewDisplayName:"Week"
                },
                {
                    viewName:"DetailedMonthView",
                    viewDisplayName:"Month"
                }
                
        ], 
        headerComponents:{
            DatePickerIcon: "<span class='cContHeaderDatePickerIcon clickableLink icon-Calendar'><img src='images/landingpage/cal.svg' style='height:18px;width:18px;vertical-align: unset !important;'></span>",
            FullscreenButton: function(bIsFullscreen)
            {
                var sIconClass = (bIsFullscreen) ? "icon-Contract" : "icon-Expand";
                return "<span class='cContHeaderFullscreen clickableLink "+ sIconClass +"'></span>";
            },
            PreviousButton: "<span id='nextprev1' class='cContHeaderButton cContHeaderNavButton cContHeaderPrevButton clickableLink icon-Prev'><img src='/images/task/arrow_left.svg' style='height:18px;width:18px;vertical-align: unset !important;'></span>",
            NextButton: "<span id='nextprev2' class='cContHeaderButton cContHeaderNavButton cContHeaderNextButton clickableLink icon-Next'><img src='/images/task/arrow_right.svg' style='height:18px;width:18px;vertical-align: unset !important;'></span>",
            TodayButton: "<span class='cContHeaderButton cContHeaderToday clickableLink'></span>",
            HeaderLabel: "<span class='cContHeaderLabelOuter'><span class='cContHeaderLabel'></span></span>",
            HeaderLabelWithDropdownMenuArrow: "<span class='cContHeaderLabelOuter clickableLink'><span class='cContHeaderLabel'></span><span class='cContHeaderDropdownMenuArrow'></span></span>",
            MenuSegmentedTab: "<span class='cContHeaderMenuSegmentedTab'></span>",
            MenuDropdownIcon: "<span class='cContHeaderMenuButton clickableLink'>☰</span>"
        },
        calDataSource:[
					    {
					        sourceFetchType: "ALL",
					        sourceType: "JSON",						
					        source: {
						        eventSource: result
                                
						    },
                            config:{
                                changeColorBasedOn: "Event"
                            }   
					        
					    }
        ],
        isDragNDropInMonthView:false,
        isDragNDropInDetailView:false,
        isDragNDropInTaskPlannerView:false,
        hideEventTime:{
            Default: true,
            DetailedMonthView: true,
            MonthView: true,
            WeekView: true,
            DayView: true,
            CustomView: true,
            QuickAgendaView: true,
            TaskPlannerView: true,
            DayEventDetailView: true,
            AgendaView: true,
            WeekPlannerView: true
        },
        eventIcon:"Dot",
        
        cellClicked: function(sView, dSelectedDateTime, bIsAllDay, pClickedAt){
          var todayDate = new Date();
          var todayDateFormatted = todayDate.toString().split(" ")[1]+" "+todayDate.toString().split(" ")[2]+" "+todayDate.toString().split(" ")[3];;
          var selectedDate = dSelectedDateTime.toString().split(" ")[1]+" "+dSelectedDateTime.toString().split(" ")[2]+" "+dSelectedDateTime.toString().split(" ")[3];
          console.log(todayDateFormatted+" ||| "+selectedDate);
					console.log("Cell Clicked : " + sView + " ||| " + dSelectedDateTime + "||| " + todayDate + " ||| " + pClickedAt.x + " ||| " + pClickedAt.y + " ||| " + bIsAllDay);
          
          /* if (todayDateFormatted > selectedDate)
            alert("todayDate is greater than selectedDate"); 
          else if (todayDateFormatted < selectedDate)
            alert("todayDate is lesser than selectedDate");
          else
            alert("both are equal"); */
          if (todayDateFormatted > selectedDate){
            alertFunNew("Cannot create task for selected date","error"); 
          }else{
            createTaskfromCalendar(dSelectedDateTime);
          }  

				},
        
        eventClicked: function(visibleView, sElemSelector, oEvent){
            $('[id^=Event-]').removeClass("taskNodeSelect");
		    
            $(sElemSelector).addClass("taskNodeSelect"); 
            ///editTask(oEvent.task_id,"calendar");
            //console.log(JSON.stringify(oEvent));
            calenPopupUI(oEvent);
            
            

        }                

        
    
    });

    afterload();

    $("#cContHeaderMenuDetailedMonthView").trigger("click");
    
    $("#nextprev1 , #nextprev2, #cContHeaderMenuWeekView, #cContHeaderMenuDetailedMonthView, #cContHeaderMenuDayView").on("click", function(){
        afterload();
    });

}


function afterload(){

  //$('.cdvContRow3,.cdvDetailTableRow3,.cdvEventTimeLeft,.cdvEventTimeRight, .cPartialEvent, .cPartialEventLeft').addClass('d-none');

  var h1 = $('#content').height();
  var h2 = $('.cContHeader').height();
  var h3 = $('.cExceptDayEventDetailView').height();
  $('.calendar-1, .calendar-2, .calendarContInner, .cdvDetailTableMain').css('height',h1); //, .cdmvEventContMain, .cmvMonthTableMain, .cmvTableContainerOuter, .cmvTableContainer
  $('.cdvDetailTableMain').css('height',h1-h2);
  $('.cdvContRow2Main, .cdvCellHeaderAllDay').css('height',h1-h2-h3);
  $('.cdvCellHeaderAllDay').children('span').css({"border-top": "1px solid #DDD", "border-bottom": "1px solid #DDD", "border-right":"1px solid #DDD"});
  $('.cdvCellWeekNumberLabel, .cdvCellWeekNumber').css("border-right","1px solid #DDD");
  $('.cdvContRow2, .cdvContRow2Main').css('overflow','auto');

  $('.cs-icon-Next').append('<img src="/images/task/arrow_right.svg" style="height:18px;width:18px;vertical-align: unset !important;">');
  $('.cs-icon-Prev').append('<img src="/images/task/arrow_left.svg" style="height:18px;width:18px;vertical-align: unset !important;">');

  

  $('.cEventLink').css('z-index','10000');

}

function createTaskfromCalendar(selectedDate){
  insertCreateTaskUI();

  const d = new Date(selectedDate);
  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let nowDate=(monthNames[parseInt(d.getMonth())+1])	+" "+(parseInt(d.getDate())<10?"0"+d.getDate():d.getDate())+", "+d.getFullYear()	;	
  let amPm=parseInt(d.getHours())>11?"PM":"AM";	
  let nowTime=(parseInt(d.getHours())>12?((parseInt(d.getHours())-12)<10?("0"+(parseInt(d.getHours())-12)):(parseInt(d.getHours())-12)):(parseInt(d.getHours())<10?"0"+d.getHours():d.getHours()))+":"+(parseInt(d.getMinutes())<10?"0"+d.getMinutes():d.getMinutes())+" "+amPm;
  
  $("#startDateCalNewUI, #endDateCalNewUI").text(nowDate).attr("parmanentvalue",nowDate);
  $("#startTimeCalNewUI, #endTimeCalNewUI").text(nowTime).attr("parmanentvalue",nowTime);
  console.log(nowDate+" ||| "+nowTime);
}

function calenPopupUI(json){

  var ui="";

  $("#transparentDiv").show();
  $("#otherModuleTaskListDiv").html("");

  var taskImage = "";var taskImageTitle = "";
  var taskType = json.task_type;
  
    
  if(taskType == 'Tasks' && json.source_id == "0" ){
    taskImage = "images/task/task.svg";
    taskImageTitle = "Task";
  }else if(taskType == 'Tasks' || taskType == "Assigned Task"){
    taskImage = "images/task/convo-task.svg";
    taskImageTitle = "Conversation Task";
  }else if(taskType == "WorkspaceDocument" || taskType == 'Document'){
    taskImage = "images/task/doc-task.svg";
    taskImageTitle = "Document Task";
  }else if(taskType == 'Idea'){
    taskImage = "images/task/idea-task.svg";
    taskImageTitle = "Idea Task";
  }else if(taskType == 'Sprint' || taskType == 'Sprint Task'){
    taskImage = "images/task/sprint-task.svg";
    taskImageTitle = "Sprint Task";
  }else if(taskType == 'Highlight'){
    taskImage = "images/task/task.svg";
    taskImageTitle = "Highlight Task";
  }else if(taskType == 'Email'){
    taskImage = "images/task/email-task.svg";
    taskImageTitle = "Email Task";
  }else if(taskType == 'Event'){
    taskImage = "images/task/event.svg";
    taskImageTitle = "Event";
  }else if(taskType == 'Workflow' && (json.type == "MyZone")){
    taskImage = "images/task/workflow-task.svg";
    taskImageTitle = "Workflow Task";
  }else if(taskType == 'Workflow'){
    taskImage = "images/task/workflow.svg";
    taskImageTitle = "Workflow Task";
  }else{
    taskImage = "images/task/task.svg";
    taskImageTitle = "Task";
  }

  var Approvuserid="";var asignimg="";
  for(let k=0;k<json.approval_id.length;k++){
    Approvuserid=json.approval_id[k].user_id;
  }
  var checkloginuserid=json.assigned_id;
  var loginuserid = checkloginuserid.includes(userIdglb);
  if(loginuserid == true || Approvuserid==userIdglb){
    asignimg="/images/task/list_assignee_cir.svg";
  }else{
    asignimg="/images/task/list_assignee_nocir.svg";
  }

  var Priority=json.priority;
  var Priority_image = (Priority == "")? "" : Priority == "1" ? "images/task/p-one.svg" : Priority == "2" ? "images/task/p-two.svg":Priority == "3" ? "images/task/p-three.svg" :Priority == "4" ? "images/task/p-four.svg":Priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
  var priorityTitle = Priority == "" ? "": Priority == "1" ? "Very important" : Priority == "2" ?"Important": Priority == "3" ? "Medium": Priority == "4"  ? "Low": Priority == "5"  ? "Very low": Priority == "6" ? "None" :"";
    
  var taskStatusImageTitle=json.display_status;
  var taskStatusImage = taskStatusImageTitle == "Completed" ? "images/task/completed.svg" : taskStatusImageTitle == "InProgress" ? "images/task/in-progress.svg" : taskStatusImageTitle == "Created" ? "images/task/in-progress.svg" : taskStatusImageTitle == "InComplete" ? "images/task/in-complete.svg" : taskStatusImageTitle == "NotCompleted" ? "images/task/not-completed.svg"  : "images/task/not-completed.svg";
  taskStatusImageTitle = taskStatusImageTitle=="Created"?"In Progress":taskStatusImageTitle=="NotCompleted"?"Not Completed":taskStatusImageTitle=="InProgress"?"In Progress":taskStatusImageTitle=="InComplete"?"Incomplete":taskStatusImageTitle;
  var trending=json.trending;
  var sentimentBgCol ="";
  if(trending >=-1 && trending <=-0.49){
    sentimentBgCol='images/tasksNewUI_old/meterred.png';
  }else if(trending >=-0.50 && trending <0){
    sentimentBgCol='images/tasksNewUI_old/meteryellow.png';
  }else if(trending == 0 || trending == 0.0){
    sentimentBgCol='images/tasksNewUI_old/meteryellow_center.png';
  }else if((trending >0 || trending > 0.0)&& trending <=0.49){
    sentimentBgCol='images/tasksNewUI_old/meteryellow_Lightgreen.png';
  }else if(trending >=0.5 && trending <=1){
    sentimentBgCol='images/tasksNewUI_old/meter_fullgreen.png';
  }else{
    trending = getValues(companyLabels,"Nodata");
    sentimentBgCol='images/tasksNewUI_old/meterDefault.png';  
  }

  var grouptitle="";
  var groupcode="";
  if(taskType == 'Sprint' || taskType == 'Sprint Task'){
    grouptitle=json.sprint_group_name;
    groupcode=json.sprint_color_code;
    
  }else{
    grouptitle=json.group_name;
    groupcode=json.group_code;
  }
  groupcode = typeof(groupcode)=="undefined"||groupcode==""||groupcode=="undefined"?"#f2788f":groupcode;
  console.log("groupcode-->"+groupcode);

  var createdImg='';var createdName="";let createdId="";
  if(typeof json.createdByDetails[0]!='undefined'){
    createdImg=json.createdByDetails[0].imagePathUrl;
    createdName=json.createdByDetails[0].userName;
    createdId=json.createdByDetails[0].user_id;
  }else{
    console.log(json.task_id);
  }

  var statusImg="";
  var userStatus="";
  var userTaskStatus = json.user_task_status;
  if(userTaskStatus=="Created"){
      userStatus="Created";
      statusImg = "images/task/user-start.svg";
  }else if(userTaskStatus=="Inprogress"){
      userStatus="Inprogress";
      statusImg = "images/task/user-inprogress.svg";
  }else if(userTaskStatus=="Paused"){
      userStatus="Paused";
      statusImg = "images/task/user-pause.svg";
  }else if(userTaskStatus=="Completed"){
      userStatus="Completed";
      statusImg = "images/task/user-completed.svg";
  }else{
      statusImg = "images/task/user-view.svg";
  }

  var tname = $(".calentaskid_"+json.task_id+":first").find(".cdmvEventTitle").text();
  if(typeof(tname)=="undefined" || tname == "undefined" || tname.trim() == ""){
    tname = $(".calentaskid_"+json.task_id+":first").find(".cdvEventTitle").text();
  }
  
  ui='<div class="taskCalenPopup pophover" id="">'
    +"<div class='pophover1 mx-auto p-2' style='background-color:"+groupcode+";'>"
      +"<div class='d-flex align-items-center'>" 
        +"<div class='d-flex align-items-center'>" 
          +"<div class='p-1'><img src='"+taskImage+"' title='"+taskImageTitle+"' class='iconStyles'></div>"
          +"<div class='p-1 mx-2' onmouseover='showCalenAssignPopup(this);event.stopPropagation();' onmouseout='hideCalenAssignPopup(this);event.stopPropagation();'><img src='"+asignimg+"' title='' class='iconStyles'>"
            +"<div id='opentaskParticipantdiv_"+json.task_id+"' onclick='event.stopPropagation();' class='position-absolute participantsBox' style='display: none;'>"
              ui+=assigneePopup(json.assignedIdDetails,createdImg,createdName,taskType,json.task_id,json.project_id,userStatus,"",json.type,json.approval_id,createdId);
            ui+="</div>"
          +"</div>"
          +"<div class='p-1 mx-2'><img src='"+Priority_image+"' title='"+priorityTitle+"' class='iconStyles'></div>"
          +"<div class='p-1 mx-2'><img src='"+taskStatusImage+"' title='"+taskStatusImageTitle+"' class='iconStyles'></div>"
          +"<div class='p-1 mx-2'><img src='"+sentimentBgCol+"' title='"+trending+"' class='iconStyles'></div>"
          +"<div class='p-1 mx-2'><img src='/images/task/expand.svg' onclick='taskDetail("+json.task_id+");event.stopPropagation();' title='Detail' class='iconStyles'></div>"
        +"</div>"
        +"<div class='p-1 ml-auto'><img class='' style='width:12px;height:12px;cursor:pointer;' src='images/menus/close3.svg' onclick='closeCalenPopup();event.stopPropagation();'></div>"
      +"</div>"
      +"<div class='d-flex align-items-center py-2 px-1 defaultExceedCls'>"
        +"<div class='d-flex align-items-center' style='padding-left: 2px;'>"
          +"<span>ID : "+json.task_id+"</span>"
        +"</div>"
      +"</div>"
      //+"<div class='py-0 px-1 ' ><input id='' class='wsScrollBar' value='"+json.task_name+"' title='"+json.task_name+"' style='width: 97%;background-color: "+groupcode+";outline: none;border: none;resize: none;' readonly></div>"
      +"<div class='d-flex'>"
        +"<div class='py-0 px-1' style='width:93%;'><textarea id='editCalenTnameid' placeholder='Enter task name' class='wsScrollBar' value='"+tname+"' title='"+tname+"' style='width: 97%;background-color: "+groupcode+";height:170px;outline: none;border: none;resize: none;' readonly>"+tname+"</textarea></div>"
        +"<div class='py-0 px-1' style='width:7%;'><img src='/images/conversation/edit.svg' onclick='editCalenTname();event.stopPropagation();' title='Edit' class='iconStyles' style='float: right;'></div>"        
      +"</div>"
      +"<div class='py-0 px-1 d-none' ><textarea id='' class='wsScrollBar' value='"+json.task_desc+"' style='width: 97%;background-color: "+groupcode+";height:150px;outline: none;border: none;resize: none;' readonly></textarea></div>"

      +'<div class="pt-1 px-1 editCalenTnameOptions" style="visibility:hidden;text-align: end;">'
        +'<img src="/images/task/remove.svg" title="Cancel" class="mx-2" onclick="editCalenTname();event.stopPropagation();" style="width:25px;height:25px;cursor:pointer;">'
        +'<img src="/images/task/tick.svg" title="Attach" onclick="updateCalenTname('+json.task_id+');event.stopPropagation();" style="width:25px;height:25px;cursor:pointer;">'
      +'</div>'

    +'</div>' 

  +'</div>'


  $("#otherModuleTaskListDiv").html(ui).show();

  valueForView="calendarTask";
  
}

function taskDetail(taskid){
  closeCalenPopup();
  $(".TaskDiv").show();
  $(".elem-CalenStyle").hide();
  editTask(taskid,"calendar");
}

function closeCalenPopup(){
  $("#transparentDiv").hide();
  $("#otherModuleTaskListDiv").html("").hide();
}

function showCalenAssignPopup(obj){
  $(obj).children('div').show();
}
function hideCalenAssignPopup(obj){
  $(obj).children('div').hide();
}

function editCalenTname(){
  if($(".editCalenTnameOptions").css("visibility") == "hidden"){
    $("#editCalenTnameid").removeAttr("readonly");
    $("#editCalenTnameid").focus();
    $(".editCalenTnameOptions").css("visibility","visible");
  }else{
    $("#editCalenTnameid").attr("readonly", true);
    $("#editCalenTnameid").val($("#editCalenTnameid").text());
    $(".editCalenTnameOptions").css("visibility","hidden");
  }
}

function updateCalenTname(taskid){
  var taskname = $("textarea#editCalenTnameid").val();
  if(taskname.trim()==""){
    alertFunNew("Task name cannot be Empty",'error');
    $("textarea#editCalenTnameid").trigger("click");
  }else{
    let jsonbody = {
      "user_id" : userIdglb,
      "task_name" : taskname,
      "event_id" : taskid,
      "project_id" : prjid,
      "company_id": companyIdglb,
      "index": "",
      "limit": "",
      "sortTaskType": "",
      "sortVal": "",
      "text": ""
    }
  
  
    $.ajax({
      url: apiPath+"/"+myk+"/v1/updateTaskName",
      type: "PUT", 
      dataType:'json',
      contentType:"application/json",
      data: JSON.stringify(jsonbody),
      error: function(jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR,textStatus,errorThrown);
            
      },  
      success:function(data) {
        $(".calentaskid_"+taskid).find(".cdmvEventTitle, .cdvEventTitle").text(taskname);
        $("#editCalenTnameid").attr("readonly", true);
        $(".editCalenTnameOptions").css("visibility","hidden");
        closeCalenPopup();
      }
    });
  }
   

}
