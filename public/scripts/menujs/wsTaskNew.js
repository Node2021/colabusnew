//Global variable for task

var limitForTask = 0;
var indexForTask = 0;
var scrollFlag = true;
var globalcolor="#eef5f7";
var whitecolor="#FFF";
var sprintTaskinTask="";    //  glb var to create sprinttask in task
var taskview="listtask";

function changeSli(obj) {
  var valu = $(obj).val();
  $('#slide-range').val(valu);
  slidercolor();
}

function gridViewForTask(view) {
  if($("#addTaskDiv").length!=0 && taskview==view){
    showTaskList();
  }else{
    taskview=view;
    if($("#taskList").is(":hidden")){
      showTaskList();
    }
    $(".calendar-2").hide().html('').removeClass("elem-CalenStyle");
    $(".TaskDiv").show();
    //$('#grid_View').attr("title", "List View");
    //$('#grid_View').attr("src", "/images/task/list_view.svg");
    //$('#grid_View').attr("onclick", "backToListView();");
    $('#headerTask').addClass("d-none").removeClass("d-flex");
    $("#taskList").html('<div id="innerTaskList" class="d-flex flex-wrap justify-content-start" style="flex-wrap:wrap;width:97%;"></div>');
    //$("#taskList").addClass('d-flex flex-wrap justify-content-start');
    valueForView="grid";
    taskSortType="";
    taskFilterVal="";
          
    if(gloablmenufor=="Task"){
      fetchTasks('','firstClick');
    }else if(gloablmenufor=="Myzone"){
      // alert("Into myzone Grid");
      if(mzTasktype=="MyZone"){
        fetchmzTasks("MyZone","firstClick");
      }else if(mzTasktype=="assignedTask"){
        fetchmzTasks("assignedTask","firstClick");
      }else if(mzTasktype=="historyTask"){
        fetchmzTasks("historyTask","firstClick");
      }
      
    }
  
  }
}

function backToListView(view){
  
  if($("#addTaskDiv").length!=0 && taskview==view){
    showTaskList();
  }else{
    taskview=view;
    if($("#taskList").is(":hidden")){
      showTaskList();
    }
    $(".calendar-2").hide().html('').removeClass("elem-CalenStyle");
    $(".TaskDiv").show();
    //$('#grid_View').attr("src", "/images/task/grid_view.svg");
    //$('#grid_View').attr("title", "Grid View");
    //$('#grid_View').attr("onclick", "gridViewForTask();");
    $('#headerTask').addClass("d-flex").removeClass("d-none");
    valueForView="list";
    $("#taskList").html('');
    $("#taskList").removeClass('d-flex flex-wrap justify-content-start');
    taskSortType="";
    taskFilterVal="";
    // fetchTasks('','firstClick');
    if(gloablmenufor=="Task"){
      fetchTasks('','firstClick');
    }else if(gloablmenufor=="Myzone"){
      // alert("Into myzone Grid");
      if(mzTasktype=="MyZone"){
        fetchmzTasks("MyZone","firstClick");
      }else if(mzTasktype=="assignedTask"){
        fetchmzTasks("assignedTask","firstClick");
      }else if(mzTasktype=="historyTask"){
        fetchmzTasks("historyTask","firstClick");
      }
    }
  }
  
  
}

function changeSliforUpdate(obj,place) {
  var valu = $(obj).val();
  let id=$(obj).attr('id').split("_")[1];
  $('#slide_'+id).val(valu);
  slidercolor1(id,obj);
  if(place!='edit'){
    if(parseInt(valu)>0){
      $("#persontaskstatusdisplay_"+userIdglb).attr("src", "images/task/user-inprogress.svg");
      $("#persontaskstatusdisplay_"+userIdglb).attr("statusOfTaskUser", 'Inprogress');
    }
   
  }
}
function changeSliderInputvalue(obj,place) {
  var newVal = $(obj).val();
  $("#taskSlideAmount_" + userIdglb + "").val(newVal);
  slidercolor();
}
function slidercolor() {
  var val = $('#slide-range').val();
  var color = "linear-gradient(90deg, rgb(143, 204, 40)" + val + "%,rgb(0,0,0)" + val + "%)";
  $('#slide-range').css('background', color);
}
function slidercolor1(id,obj) {
  //alert(id)
  var val = $(obj).val();
  var color = "linear-gradient(90deg, rgb(143, 204, 40)" + val + "%,rgb(0,0,0)" + val + "%)";
  $('#slide_'+id).css('background', color);
}


function fetchTasks(type,firstclick) {
  console.log(valueForView);
  limitForTask = 30;
  if(firstclick=='firstClick' && valueForView=="list"){
    $("#taskList").html('');
    indexForTask = 0;
  }else if(firstclick=='firstClick' && valueForView=="grid"){
    $("#taskList").html('');
    indexForTask = 0;
  }else if(firstclick=='firstClick' && valueForView=="calendartask"){
    $("#taskList").html('');
    indexForTask = 0;
    calenTask('calendartask');
    return false;
  }

  
  $('#loadingBar').addClass('d-flex');
  // alert(valueForView);
  
  // alert(taskSortType);
  // alert(taskFilterVal);

  let jsonbody=	{
    "user_id":userIdglb,
    "company_id":companyIdglb,
    "limit":limitForTask,
    "index":indexForTask,
    "sortVal":taskSortType,
    "text":taskSearchWord,
    "project_id":prjid,
    "sortTaskType":taskFilterVal
  }
  //alert("---")
  //console.log(index + "---" + limit);
  $.ajax({
    url: apiPath + "/" + myk + "/v1/getTaskDetails/TeamZone",
   //url:"http://localhost:8080/v1/getTaskDetails/TeamZone",
    type: "POST",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      timerControl("");
    },
    success: function (result) {
      console.log(result);
      
     if(firstclick=='firstClick'){
       console.log("in first Click");
       if(valueForView=='list'){
        $("#taskList").show();
        $("#taskList").html(createTaskUI(result)) ;
       }else if(valueForView=='grid'){
        console.log("in grid");
        $("#taskList").show();
        $("#headerTask").addClass('d-none').removeClass('d-flex');
        $("#taskList").html('<div id="innerTaskList" class="d-flex flex-wrap justify-content-start" style="flex-wrap:wrap;width:97%;"></div>');
        $("#innerTaskList").html(createTaskGridUI(result)) ;
        // console.log("count play button"+$("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=taskPlay_]").length);
        // $("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=taskPlay_]").removeAttr("onclick");
        // $("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=tasklistcheckbox_]").removeAttr("onclick");
       }else{
        $("#taskList").html(createTaskUI(result)) ;
       }

     }else{
      if(valueForView=='list'){
        $("#taskList").append(createTaskUI(result)) ;
      }else if(valueForView=='grid'){
        $("#innerTaskList").append(createTaskGridUI(result)) ;
        // console.log("count play button"+$("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=taskPlay_]").length);
        // $("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=taskPlay_]").removeAttr("onclick");
        // $("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=tasklistcheckbox_]").removeAttr("onclick");
        
      }else{
        $("#taskList").append(createTaskUI(result)) ;
      }
     }
     if(document.querySelectorAll("#taskList .mainTask").length==0 && valueForView=='list'){
      $("#taskList").append("<div class='d-flex justify-content-center mt-3'>No Task Found</div>");
     }
     
     for(let i=0;i<result.length;i++){
      //  console.log(result[i].approval_id);
       for(let j=0;j<result[i].approval_id.length;j++){
        // console.log("status--->"+result[i].approval_id[j].user_task_status);
        // console.log("user_id--->"+result[i].approval_id[j].user_id);
        // $('#rolenew1_'+result[i].approval_id[j].user_id+result[i].task_id).val(result[i].approval_id[j].user_task_status);

        if(result[i].approval_id[j].user_task_status=="Pending approval"){
          // apprImgPath="/images/task/pending_approval.svg";
          // appTitle="Pending approval";
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/pending_approval.svg');
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','Pending approval');
          $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).hide();
        }else if(result[i].approval_id[j].user_task_status=="Approved"){
          // apprImgPath="/images/task/approved.svg";
          // appTitle="Approved";
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/approved.svg');
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','Approved');
          $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).show();
        }else if(result[i].approval_id[j].user_task_status=="Rejected"){
          // apprImgPath="/images/task/rejected.svg";
          // appTitle="rejected";
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/rejected.svg');
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','Rejected');
          $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).hide();
        }else if(result[i].approval_id[j].user_task_status=="On hold"){
          // apprImgPath="/images/task/on_hold.svg";
          // appTitle="On hold";
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/on_hold.svg');
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','On hold');
          $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).hide();
        }else if(result[i].approval_id[j].user_task_status=="Cancel"){
          // apprImgPath="/images/task/cancel.svg";
          // appTitle="Cancel";
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('src','/images/task/cancel.svg');
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).attr('title','Cancel');
          $('#appListCheckBox_'+result[i].approval_id[j].user_id+result[i].task_id).hide();
        }


        if(result[i].approval_id[j].user_id==userIdglb){

        }else{
          $('#appNewImg_'+result[i].approval_id[j].user_id+result[i].task_id).removeAttr("onclick");
        }
       }
      
      }
     var screen = window.matchMedia("(max-width: 1200px)");
     screenresize(screen);
     //screen.addListener(screenresize);
     $('#loadingBar').removeClass('d-flex').addClass('d-none');
    }
  });
}

function prepareAddTaskUiNew(type,taskId,docTaskClk,feedname){
  type=typeof(type)=='undefined'?"":type;
  taskId=typeof(taskId)=='undefined'?"0":taskId;
  let border=type!=""?"border-bottom:1px solid darkgray;":"";
  let place="dependencyList";
  let onclick="insertCreateTaskUI()";
  let displayClass=type!=''?'d-block':'d-none';
  if(type=='edit'){
    onclick="cancelEdit("+taskId+")"
  }
  
  //$(".addTaskDiv").remove();
  let UI="<div class='addTaskDiv' id='addTaskDiv' style='    '>";
  UI += '<div class="row ml-0 mr-0 " id="addTaskScroll" style="font-size:14px;"> '
             if(type!='workflow'){
                     UI +='<div class="col-12 task_type_divison tHeaderOptions">'

                          + '<div class="d-flex task_type_container " style="font-size: 14px;font-family:opensanssemibold;">'
                        
                        +'<div style="padding: 6px 8px 6px 0px;" >'

                            + '<div class=" task_type d-flex Active" style="height:24px;"  id="Tasks" onclick="switchTaskType(this)">'
                                  
                                  +'<div    class="pl-2 pr-1" style="padding-bottom:2px;"><img id="taskHeaderTitleimg" src="/images/task/headertaskpin.svg" style="height:20px ;width:20px"/></div>'
                                  +'<div id="taskHeaderTitle"  class="pr-3" style="padding-bottom:2px;    margin-top: 1px;">Task</div>'
                      
                      
                            +'</div>'

                        +'</div>'

                        +'<div id="tSprinttaskHeadertitleDiv"  style="padding: 6px 8px 6px 8px;" >'

                            + '<div id="tSprinttask"   style="height:24px;"    class=" task_type d-flex " onclick="switchTaskType(this)">'
                                  
                                  +'<div   class="pl-2 pr-1" style="padding-bottom:2px"><img src="/images/agile/sprinttask.svg" style="height:20px ;width:20px"/></div>'
                                  +'<div   class="pr-3" style="padding-bottom:2px;    margin-top: 1px;">Sprint Task</div>'
                      
                      
                            +'</div>'

                        +'</div>'

                        +'<div id="WFHeadertitleDiv"  style="padding: 6px 8px 6px 8px;" >'

                            + '<div id="Workflow"  style="height:24px;"   class=" task_type d-flex " onclick="switchTaskType(this)">'
                                  
                                  +'<div    class="pl-2 pr-1" style="padding-bottom:2px"><img src="/images/task/headerworkflow.svg" style="height:20px ;width:20px;"/></div>'
                                  +'<div   class="pr-3" style="padding-bottom:2px;    margin-top: 1px;">Workflow</div>'
                      
                      
                            +'</div>'

                        +'</div>'

                        +'<div id="EventHeadertitleDiv"  style="padding: 6px 8px 6px 8px;" >'

                            + '<div id="Event"   style="height:24px;"    class=" task_type d-flex " onclick="switchTaskType(this)">'
                                  
                                  +'<div   class="pl-2 pr-1" style="padding-bottom:2px"><img src="/images/task/headerevent.svg" style="height:20px ;width:20px"/></div>'
                                  +'<div   class="pr-3" style="padding-bottom:2px;    margin-top: 1px;">Event</div>'
                      
                      
                            +'</div>'

                        +'</div>'

                        // + '<div class="task_type" style="padding: 10px 8px 15px 8px;"><div id="Workflow"  onclick="switchTaskType(this)" class="typetask px-2"style="padding-top:2px;padding-bottom:2px">Workflow</div></div>'
                        // + '<div class="task_type" style="padding: 10px 8px 15px 8px;"><div  id="Event" onclick="switchTaskType(this)" class="typetask px-2" style="padding-top:2px;padding-bottom:2px">Event</div></div>'
                      + '</div>' 


                +'</div>'
             }
             UI += '<div class="col-10 p-0 taskAdd pb-4">'
                +'<input type="hidden" id="task_id">'
                +'<input type="hidden" id="task_type">'
                +'<input type="hidden" id="removeEmail" user="">'

                  +"<div id='tSprintStoryDiv' class='d-none align-items-center px-3 mt-3 position-relative' style='font-size: 12px;'>"
                    +"<div>Story</div>"
                    +"<div class='mx-1' onclick='showTstoryList();event.stopPropagation();' style='cursor:pointer;'><img src='images/task/dowarrow.svg' style='width:12px;height:12px;'></div>"
                    +"<div id='selectedStoryIntask' storyid='' class='defaultExceedCls' ></div>"
                    +"<div id='tSprintStoryListDiv' class='position-absolute rounded defaultScrollDiv border border-secondary py-3 pl-3 pr-2' style='display:none;'>"
                      +"<div id='tSprintStoryListDivsearch' class=''><span id='selectTaskstoriesspan' style='width: 90%;padding-left:5px;display:none'>Select story</span><input id='selectTaskstoriesinput' type='text' class='border-0' onkeyup='searchStoryinTask(this);event.stopPropagation();' style='outline:none;width: 90%; border-bottom: 1px solid rgb(108, 117, 125) !important;'><img src='/images/menus/search2.svg' class='float-right' onclick='shoTaskstorysearch()' style='width:18px;height:18px;cursor:pointer;'></div>"
                      +"<div id='tSprintStoryListDiv1' class='defaultScrollDiv' style='height: 190px;overflow-y: auto;overflow-x: hidden;'></div>"
                    +"</div>"
                  +"</div>"

                  + '<div class="px-3 mt-3">' 
                    +'<div style="">'
                        // +'<label style="width:100%;" class="pure-material-textfield-outlined" >'
                        // +'<input id="taskname" placeholder=" " style="height:40px;    font-size: 12px;">'
                        // +'<span id="taskspan" style="margin-top:-8px !important;font-size:12px">Task Name</span>'
                        // +'</label>'
                        // +'<div class="material-textfield" ><textarea  id="taskname" class="theightdemo material-textfield-input taskfield" onkeyup=" commentAutoGrow(event, this, \'task\', \'\'); ></textarea><label class="material-textfield-label">Task Name</label></div>'
                        +'<div class="material-textfield" ><textarea id="taskname" type="text" class="theightdemo material-textfield-input"  onkeyup=" commentAutoGrow(event, this, \'task\', \'\');changeImage(\'taskname\');"  placeholder=" " ></textarea><label class="material-textfield-label" style="    font-size: 12px;top:18px">Task Name</label></div>'
                    +'</div>'
                    + '<div class=" instruction" style="display:none;margin-top:6px;">'
                        // +'<label style="width:100%;" class="pure-material-textfield-outlined">'
                        // +'<input id="taskdesc" placeholder=" " style="    font-size: 12px;">'
                        // +'<span style="margin-top:-2px !important;font-size: 12px;">Instructions</span>'
                        // +'</label>'onkeyup=" commentAutoGrow(event, this, \'task\', \'\');"
                        // +'<div class="material-textfield" ><textarea  id="taskdesc" class="theightdemo material-textfield-input taskfield" onkeyup=" commentAutoGrow(event, this, \'task\', \'\');"></textarea><label class="material-textfield-label">Instructions</label></div>'
                        +'<div class="material-textfield" ><textarea id="taskdesc" type="text" class="theightdemo material-textfield-input"  onkeyup=" commentAutoGrow(event, this, \'task\', \'\');changeImage(\'instruction\');"  placeholder=" " ></textarea><label class="material-textfield-label" style="    font-size: 12px;top:18px">Instructions</label></div>'
                    + '</div>'
                  + '</div>'

                  + '<div class=" px-3 d-flex " id="project-due-div" style="margin-top: 6px;">'
                           + '<div   style="width:50%;">'
                                +'<input id="uploadForTask" type="file" onchange="uploadFileForTask(this,'+taskId+',\'CalendarDocTaskUpload\')" class="d-none"/>'
                                + '<div class="optionsDiv d-flex position-relative">'
                                      +' <div class="px-1 py-1 convoSubIcons">'
                                        + '<img id="participantsUIdivArea" title="Assignees" class="img-responsive task_icon" src="/images/task/assignee_cir.svg" onclick="AddUserToTheTask();" >'
                                      +'</div>' 
                                      +' <div class="px-1 py-1 convoSubIcons d-none">' 
                                        + '<img id="Calender" title="Calender" class="img-responsive task_icon" src="/images/task/Priority-Duedate_cir.svg" onclick="showDeadLinePopup();" >'
                                      +'</div>'
                                      +' <div class="px-1 py-1 convoSubIcons">'    
                                        + '<img id="instruction" title="Instruction" class="img-responsive task_icon" src="/images/task/taskInstructions_cir.svg" onclick="showInstruction();" >'
                                      +'</div>' 
                                      +' <div class="px-1 py-1 convoSubIcons">'    
                                       + '<img id="upload" title="Attach" class="img-responsive task_icon" src="/images/task/upload_cir.svg"  >'
                                      +'</div>'
                                      if(globalmenu=="Task"){
                                        UI+=' <div class="px-1 py-1 convoSubIcons">'   
                                          + '<img id="dependencyTask" title="Dependent Task" class="img-responsive task_icon" src="/images/task/DependentTask.svg" onclick="fetchDependencyTask('+taskId+',\''+place+'\');" >'
                                        +'</div>'
                                      }
                                      UI+=' <div class="px-1 py-1 convoSubIcons" >'     
                                          + '<img id="remindericon" title="Reminder" onclick="showReminder();" class="img-responsive  task_icon " reminderoption="" src="/images/task/reminder.svg"  >'
                                      +'</div>'

                                      +' <div class="px-1 py-1 convoSubIcons">'
                                        + '<img id="participantsUIdivArea1" title="Approvers" class="img-responsive task_icon" src="/images/task/approver_nofill.svg" onclick="AddUserToTheTask1();" >'
                                      +'</div>' 
                                      +' <div class="px-1 py-1 convoSubIcons">'     
                                          + '<img id="comments" title="Comments" class="img-responsive  task_icon '+displayClass+'" src="/images/task/task_comments.svg"  onclick="fetchPostedThingFiles(0,0,0)">'
                                      +'</div>'


                                      


                                     
                                      +' <div class="px-1 py-1 convoSubIcons d-none" onclick="showHistory();">'      
                                          + '<img id="historyicon" title="History" class="img-responsive task_icon" src="/images/task/History_cir.svg" onclick="showParticipantsNewUI();" >'
                                      +'</div>'
                                      +' <div  id="conversationfetch" class="px-1 py-1 convoSubIcons d-none">'  
                                        + '<img title="Conversations" class="img-responsive task_icon " src="/images/task/comment.svg" onclick="" >'
                                      +'</div>'

                                      if(typeof(docTaskClk)!="undefined"){
                                        if(docTaskClk[1]=="png" || docTaskClk[1]=="jpg" || docTaskClk[1]=="jpeg" || docTaskClk[1]=="gif" || docTaskClk[1]=="svg"){
                                          UI+=' <div  id="docTask" onclick="thumbnailOpenUi(this,'+docTaskClk[0]+',\''+docTaskClk[1]+'\','+docTaskClk[2]+');event.stopPropagation();" class="px-1 py-1 convoSubIcons d-none showicon">'  
                                        + '<img title="Document" class="img-responsive task_icon showicon " src="/images/task/documenttask.svg" onclick="" >'
                                        +'</div>'
                                        }else if(docTaskClk[1]=="mp3" || docTaskClk[1]=="wav" || docTaskClk[1]=="wma" || docTaskClk[1]=="mp4" || docTaskClk[1]=="m4v" || docTaskClk[1]=="mov" || docTaskClk[1]=="flv" || docTaskClk[1]=="f4v" || docTaskClk[1]=="ogg" || docTaskClk[1]=="ogv" || docTaskClk[1]=="wmv" || docTaskClk[1]=="vp6" || docTaskClk[1]=="vp5" || docTaskClk[1]=="mpg" || docTaskClk[1]=="avi" || docTaskClk[1]=="mpeg" || docTaskClk[1]=="webm"){
                                          UI+=' <div  id="docTask" onclick="openVideo('+docTaskClk[0]+',\''+docTaskClk[1]+'\','+docTaskClk[2]+');event.stopPropagation();" class="px-1 py-1 convoSubIcons d-none showicon">'  
                                          + '<img title="Document" class="img-responsive task_icon showicon " src="/images/task/documenttask.svg" onclick="" >'
                                          +'</div>'
                                        }else{
                                          UI+=' <div  id="docTask" onclick="viewordownloadUi('+docTaskClk[0]+',this,\''+docTaskClk[1]+'\','+docTaskClk[2]+');event.stopPropagation();" class="px-1 py-1 convoSubIcons d-none showicon">'  
                                          + '<img title="Document" class="img-responsive task_icon showicon " src="/images/task/documenttask.svg" onclick="" >'
                                          +'</div>'
                                        }
                                      }else{
                                        UI+=' <div  id="docTask" class="px-1 py-1 convoSubIcons d-none showicon">'  
                                          + '<img title="Document" class="img-responsive task_icon showicon " src="/images/task/documenttask.svg" onclick="" >'
                                        +'</div>'
                                      } 
                                      
                                      if(typeof(feedname)!="undefined"){
                                        UI+=' <div  id="docTaskLink" onclick="docTaskLinkOpen(\''+feedname+'\');event.stopPropagation();" class="px-1 py-1 convoSubIcons d-none showicon">'  
                                          + '<img title="Link" class="img-responsive task_icon showicon " src="images/task/attach.svg" onclick="" >'
                                          
                                        +'</div>'
                                        
                                      }else{
                                        UI+=' <div  id="docTaskLink" onclick="docTaskLinkOpen();event.stopPropagation();" class="px-1 py-1 convoSubIcons d-none showicon">'  
                                          + '<img title="Link" class="img-responsive task_icon showicon " src="images/task/attach.svg" onclick="" >'
                                          
                                        +'</div>'
                                        
                                      }
                              
                                UI+= '</div>'
                               
                                UI+='<div class="d-flex optionDetails"></div>'
                                + '</div>' 
                                if(type!=''){
                                UI+='<div class="d-flex" style="width: 24%;margin-top: 6px;font-size: 12px;">'
                                          +'<div>Task ID</div>'
                                          +'<div class="ml-2" style="color:darkgray">'+taskId+'</div>'

                                    +'</div>'
                              }
                              UI+= '<div id="temporarySaveDiv" class="d-flex justify-content-end" style="width:50%;font-size:12px;margin-top:5px">'
                              +' <img title="Voice" id="hLigthTaskVoiceStop " class="mr-1 mt-1" src="/images/task/voice2.svg" style="display: block; cursor: pointer;  height: 25px; width: 25px; background-color: white; border-radius: 25px;" alt="Image" >'
                              + '<img  title="Cancel" class="img-responsive mt-1" src="/images/task/cancel_1.svg" onclick="'+onclick+'" style="float:left;height:25px;width:25px;margin-right: 5px;cursor:pointer;">'
                              + '<img title="Save" class="img-responsive mt-1" src="/images/task/save_1.svg" onclick="commonTaskValidations(\'\',\'\','+taskId+');" style="float:left;height:25px;width:25px;margin-right: 5px;cursor:pointer;">'
                           + '</div>'
                  + '</div>'
                  +'<div class="dependency_comment_div defaultScrollDiv px-3 mt-3" style="display:none"></div>'

                  +'<div class="task_comment_div px-1 mt-3 px-3"  style="display:none">'
                      +"<div class=' d-flex  align-items-center pb-1'  style='border-bottom: 1px solid #b4adad;'>" 
                        +'<div class="pl-1 mr-1" style="font-size:12px;" >Conversations</div>'
                        +'<img class="" onclick="fetchPostedThingFiles(0,0,0);" src="images/task/dowarrow.svg" style="height:15px;width:15px; cursor:pointer;margin-top:2px">'
                      +"</div>"
                      +'<div class="task_comment_list_div px-1 mt-1 defaultScrollDiv defaultScrollDivmt-2 " id="taskcommentmain"  style="height: 334px;overflow-y:auto; border-bottom: 1px solid #b4adad; ">'
                      +'</div>'
                  +'</div>'
                  +'<div class="task_user_history  px-3 mt-3" ></div>'
                + '</div>'
                +'<div class="col-2 p-0 documentAdd  " style="border-left: 1px solid #DADBDB;">'
                  + '<div class="documentDiv p-1" >'
                    +'<span class="pl-1" ><img src="/images/task/doceumnt.svg"  style="height:20px;width:20px"/></span>'
                    + '<span class="pl-1 taskdocname" style="    font-size: 12px;"  >Task Level Document</span>'
                  + '</div>'
                  + '<div class="taskDocument defaultScrollDiv" style="max-height: 110px;overflow-y: auto;"></div>'
                + '</div>'
                
          + '</div>'
          // +'<div class="dependency_comment_div"></div>'
          +'<div class="task_user_new_div" ></div>'
          +'<div class="task_user_new_div1" ></div>'   
         if(type!='workflow'){
              UI+='<div class="row ml-0 mr-0 mt-1 savecancelDiv " id="user" style="font-size:14px;border-top:1px solid darkgray;"> '
              +'<div class="col-10 p-1 d-flex justify-content-between align-items-center" >'
                +'<div id="attachWFTemplateDiv" class="ml-2" style=" font-size: 13px; padding: 0px; float: left;visibility:hidden;" onclick=""><input style="margin-right:8px;float: left;margin-top: 3px;" onclick="" id="attachWFTemplate" type="checkbox"> <span class="Attach_Template_cLabelText">Save as a template</span></div>'
                +'<div class="d-flex">'
                +' <img title="Voice" id="hLigthTaskVoiceStop " class=" " src="/images/task/cassaendra1.svg" style="display: block; cursor: pointer;margin-right:10px;  height: 25px; width: 25px;  " alt="Image" >'
                if(type=='edit'){
                  UI +=  '<img id="deleteTask" title="delete" class="img-responsive" src="/images/task/deltete.svg" onclick="" style="float:left;height:25px;width:25px;margin-right: 10px;cursor:pointer;">'
                }
                UI +=  '<img title="Cancel" id="canceltask" class="img-responsive " src="/images/task/remove.svg" onclick="'+onclick+'" style="float:left;height:25px;width:25px;margin-right: 10px;cursor:pointer;">'
                + '<img  title="Save" id="savetask" class="img-responsive " src="/images/task/tick.svg" onclick="commonTaskValidations(\'\',\'\','+taskId+');" style="float:left;height:25px;width:25px;margin-right: 8px;cursor:pointer;">'
                +'</div>'
              +'</div>'    
            +'</div>'
         } 
         UI+='</div>'    
       return UI;                
}

function docTaskLinkOpen(link){
  window.open(link,"_blank");
}

function shoTaskstorysearch(){
  if($("#selectTaskstoriesinput").is(':visible')){
    $("#selectTaskstoriesinput").hide();
    $("#selectTaskstoriesspan").show();
  }else{
    $("#selectTaskstoriesinput").show().focus();
    $("#selectTaskstoriesspan").hide();
  }
}

function insertCreateTaskUI(docTaskOnclik,doctype,feedname) {
  let UI = "";
  
  if ($(".addTaskDiv").length == 0) {
    let UI=prepareAddTaskUiNew("","0",docTaskOnclik,feedname);
    //$(".TaskDiv").prepend(UI).show('slow');
    
    if(globalhlmenu=="Highlight"){

        $("#ideacreateTaskDivbody").html(UI);
        $('.tHeaderOptions').addClass('d-none');
    }else if(globalmenu=="Idea"){
      $("#ideacreateTaskDivbody").html(UI).show('slow');
      $('.tHeaderOptions').addClass('d-none');
      $('#taskHeaderTitle').text('Idea Task');
      $('#taskHeaderTitleimg').attr('src','images/task/idea-task.svg')
    }else if(globalmenu=="agile" || globalmenu=="sprint"){
      $("#agilecreateTaskDivbody").html(UI).show('slow');
      $('.tHeaderOptions').addClass('d-none');
      $('#taskHeaderTitle').text('Sprint Task');
      $('#taskHeaderTitleimg').attr('src','/images/agile/sprint_blue.svg');
    }else if(globalmenu=="activityFeed"){
      $("#popupcreateTaskDivbody").html(UI).show('slow');
      $('.tHeaderOptions').addClass('d-none');
      $('#taskHeaderTitle').text('Conversation Task');
      $(".popupTaskDiv").hide();
      $('#taskHeaderTitleimg').attr('src','/images/menus/conversation.svg')
    }else if(globalmenu=="wsdocument"){
      $("#popupcreateTaskDivbody").html(UI).show('slow');
      $('.tHeaderOptions').addClass('d-none');
      $('#taskHeaderTitle').text('Document Task');
      $(".popupTaskDiv").hide();
      $('#taskHeaderTitleimg').attr('src','/images/menus/doc.svg');
      
      if(doctype=="link"){
        $("#docTaskLink").removeClass("d-none").addClass('d-block');
      }else{
        $("#docTask").removeClass("d-none").addClass('d-block');
      }
      console.log("1122->"+docTaskOnclik)
    }
    else{
      sprintTaskinTask="";
      $("#addTaskHolderDiv").html(UI).show('slow');
      $("#taskList, .popupTaskDiv").hide();
      if($("#innerTaskList").length == 0){
        $("#headerTask").addClass('d-none').removeClass('d-flex');
      }
      if($(".elem-CalenStyle").length!=0){
        $(".elem-CalenStyle").hide();
        $(".TaskDiv").show();
      } 
      
    }
   
    if(globalmenu!="agile" && globalmenu!="sprint"){
      fetchGroups("setgroupwhilecreate");
    }
   
   // showDeadLinePopup();
    //replaceSaveWithDueDate();
    var element = document.getElementById("addTaskScroll");
    element.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
    
    createPopupDiv();
    if(globalhlmenu=="Highlight"){
      // $("#ideacreateTaskDivbody").html(UI);
      prjid=0;
      fetchProjectsForMyzone();
      $("#projectlistingDiv").addClass('d-flex').removeClass('d-none');
    }
    $("#upload").on('click',()=>{
     document.getElementById('uploadForTask').click();
    });
  
    $("#taskspan").on('click',()=>{
      $("#taskspan").css('margin-top','0px')
     });

  } else {
    if(globalhlmenu=="Highlight"){
      closeHLTask();
    }
    $("#headerTask, #taskList, .popupTaskDiv").show();
    if($("#innerTaskList").length == 0){
      $("#headerTask").addClass('d-flex').removeClass('d-none');
    }
    if($(".elem-CalenStyle").length!=0){
      $(".elem-CalenStyle").show();
      $(".TaskDiv").hide();
    }
    if(typeof($("#addTaskDiv").attr('changestatus'))=="undefined"){
      $(".addTaskDiv").fadeOut(300, function(){ $(this).remove();});
      if(globalmenu=='Task'){
        clearWFData();
      }
    }else{
      cancelEdit($('#task_id').val());
    }
    

  }
  if( gloablmenufor=="Myzone"){
    $("#projectlistingDiv").addClass('d-flex').removeClass('d-none');
    prjid=0;
    fetchProjectsForMyzone();
  }
  
}
function commonTaskValidations(userUncheck,estimateUncheck,taskId,approveUncheck){
  let taskName = $("#taskname").val();
  if(typeof(taskName)!='undefined' && taskName!='' ){
    let userData="[";
    let userData1="[";
    let useresimateHour=0;
    let userestimateMin=0;
    let onclick="insertNewTask";
    if(taskId!='0'){
      onclick="updateTask";
    }
    $('.userDetails > div').each(function () { 
      if($(this).attr('id')!='ignorediv'){
       // alert($(this).attr('id'));
        var id = $(this).attr('id').split('_')[2];
        userData+="{";
        userData+="\"id\":\""+id+"\",";
        userData+="\"status\":\""+$(this).attr('userstatus')+"\",";
        userData+="\"progress\":\""+$(this).find('#taskSlideAmount_'+id).val()+"\",";
        userData+="\"totalEstHr\":\""+$(this).find('.estimateHour').val()+"\",";
        userData+="\"totalEstmin\":\""+$(this).find('.estimateMin').val()+"\"";
      // userData+="\"userAction\":\""+$(this).find(".userAction").html()+"\"";dependenctTaskIdstotalEstHr
        userData+="},";
        useresimateHour=parseInt(useresimateHour)+parseInt($(this).find('.estimateHour').val());
        userestimateMin=parseInt(userestimateMin)+parseInt($(this).find('.estimateMin').val());
      }
      
    });
    userData=userData.length>1?userData.substring(0,userData.length-1)+"]":"[]";


    $('.userDetails1 > div').each(function () { 
      if($(this).attr('id')!='ignorediv'){
        var id = $(this).attr('id').split('_')[2];
        // var role = $(this).find('#commentApprove_'+id).val();
        // console.log("role-->>"+role);
        // if(userArray1.includes(id)==false){
          // userArray1.push(id);
          
          userData1+="{";
          userData1+="\"user_id\":\""+id+"\",";
          userData1+="\"task_user_status\":\""+$(this).find('#rolenew_'+id).val()+"\",";
          userData1+="\"sequence\":\""+$(this).attr('secuenceNumber')+"\"";
          userData1+="},";
  
        // }
      }
      
    });

    userData1=userData1.length>1?userData1.substring(0,userData1.length-1)+"]":"[]";
    if(userData=='[]' && userUncheck!=true && userData1=='[]' && approveUncheck!=true){
      userUncheck=true
      confirmFunNew(getValues(companyAlerts,"Alert_IncompleteTask"),"delete","commonTaskValidations",userUncheck,'',taskId,approveUncheck);
    }
    // else if(userData1=='[]' && approveUncheck!=true){
    //   // approveUncheck=true;
    //   // confirmFunNew("Task does not have an approver, do you want to save this task without approver?","delete","commonTaskValidations",userUncheck,'',taskId,approveUncheck);
    // }
    else{
      let estimateHour= $(".estimateDiv").find(".estimateHour").val();
      let estimateMin= $(".estimateDiv").find(".estimateMinute").val();

      let totalestimatecaltask=(parseInt(estimateHour)*60)+parseInt(estimateMin);
      let totalestimatecaluser=(parseInt(useresimateHour)*60)+parseInt(userestimateMin);

      let alertTaskEstTime=$(".estimateHour").val()+" h "+$(".estimateMinute").val()+" m";
      let alertUserEstTimeHrMin=useresimateHour+" h "+userestimateMin+" m";
      if((totalestimatecaltask>totalestimatecaluser) && estimateUncheck!=true){
        estimateUncheck=true;
        var message = "Task Estimate exceeds sum of Participant Estimates.";
			  conFunNew(message,'warning',onclick);
      }
      else if((totalestimatecaltask<totalestimatecaluser)&& estimateUncheck!=true){
        estimateUncheck=true;
        var message = "Total Participant Estimate exceeds Task Estimate.</br></br>Do you want to update Task Estimate?";
        confirmResetNew(message,'warning','resetEstimateTime',onclick,'Yes','No',useresimateHour,userestimateMin,taskId);
      }else if(((sprintTaskinTask=="sprintTaskinTask" || globalmenu=="agile" || globalmenu=="sprint") && $("#sprintgroupname").text().trim()=="")){
        alertFunNew("Please select sprint group",'error');
      }else if(((sprintTaskinTask=="sprintTaskinTask" || globalmenu=="agile" || globalmenu=="sprint") && $("#sprintname").text().trim()=="")){
        alertFunNew("Please select sprint",'error');
      }
      else{
        if(taskId!='0'){
          updateTask(taskId);
        }else{
          insertNewTask();
        }
        
      }
    }

  }else{
    alertFunNew("Task name cannot be Empty",'error');
  }
}

function resetEstimateTime(useresimateHour,userestimateMin,taskId){
  $(".estimateDiv").find(".estimateHour").val(useresimateHour);
  $(".estimateDiv").find(".estimateMinute").val(userestimateMin);
  if(taskId!='0'){
    updateTask(taskId);
  }else{
    insertNewTask();
  }
  
}
async function insertNewTask() {
  // alert("hree");
  let taskName = $("#taskname").val();
  let taskdesc = $("#taskdesc").val();
  let task_type = $('.Active').attr('id');

  let priority=$(".changePriority").attr('priority');
  let priorityId = priority==0?"6":priority;

  let rem=$("#eventReminder option:selected").val();
  let reminder= $("#remindericon").attr('reminder');
  
  let userArray=[];
  let userArray1=[];
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let userData="[";
  $('.userDetails > div').each(function () { 
    if($(this).attr('id')!='ignorediv'){
      var id = $(this).attr('id').split('_')[2];
      if(userArray.includes(id)==false){
        userArray.push(id);
        let estH= $(this).find('.estimateHour').val();
        let estM= $(this).find('.estimateMin').val()
        if(estM>59){
          estH=parseInt(estH)+Math.floor(parseInt(estM)/60);
          estM=Math.floor(parseInt(estM)%60);
        }
        userData+="{";
        userData+="\"id\":\""+id+"\",";
        userData+="\"status\":\""+$(this).attr('userstatus')+"\",";
        userData+="\"progress\":\""+$(this).find('#taskSlideAmount_'+id).val()+"\",";
        userData+="\"totalEstHr\":\""+estH+"\",";
        userData+="\"totalEstmin\":\""+estM+"\"";
      // userData+="\"userAction\":\""+$(this).find(".userAction").html()+"\"";dependenctTaskIdstotalEstHr
        userData+="},";

      }
    }
    
  });
  
  


  userArray=[];
  
  let dependencyTaskId="";
  $('.dependenctTaskIds > div').each(function () { 
    
    //if(typeof($(this).find(".imagecheck").attr('src'))!='undefined'){

      if($(this).find('.check').prop("checked")==true){
        let depTask=$(this).attr('id').split("_")[1];
        dependencyTaskId=dependencyTaskId+depTask+":N"+",";
      }
    //}
  });
  dependencyTaskId=dependencyTaskId.substring(0,dependencyTaskId.length-1);


  let saveDocTaskIds ="";
  $('.taskDocument > div').each(function () { 
    saveDocTaskIds=saveDocTaskIds+$(this).attr('id').split("_")[1]+","
  });
 
  userData=userData.length>1?userData.substring(0,userData.length-1)+"]":"[]";
  

  let startDate=$("#startDateCalNewUI").text();
  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let startdateMonth=monthNames.indexOf(startDate.substring(0,3))<10?"0"+monthNames.indexOf(startDate.substring(0,3)):monthNames.indexOf(startDate.substring(0,3));
  let startdateDate=startDate.substring(4,6)
  let start=startdateMonth.toString().trim()+"-"+startdateDate.trim()+"-"+startDate.split(",")[1].trim();

  let endDate=$("#endDateCalNewUI").text();
  let enddateMonth=monthNames.indexOf(endDate.substring(0,3))<10?"0"+monthNames.indexOf(endDate.substring(0,3)):monthNames.indexOf(endDate.substring(0,3));
  let enddateDate=endDate.substring(4,6)
  let end=enddateMonth.toString().trim()+"-"+enddateDate.trim()+"-"+endDate.split(",")[1].trim();


  let startfullTime=$("#startTimeCalNewUI").text();
  let hour=startfullTime.substring(startfullTime.length-2,startfullTime.length)=="PM"?(parseInt(startfullTime.split(":")[0].trim())+12):startfullTime.split(":")[0].trim();
  let startTime=hour+":"+startfullTime.split(":")[1].trim();


  let userData1="[";
  $('.userDetails1 > div').each(function () { 
    if($(this).attr('id')!='ignorediv'){
      var id = $(this).attr('id').split('_')[2];

      let startDate1=$("#resquestDateCalNewUI_"+id).text();
      var monthNames1 = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      let startdateMonth1=monthNames1.indexOf(startDate1.substring(0,3))<10?"0"+monthNames1.indexOf(startDate1.substring(0,3)):monthNames1.indexOf(startDate1.substring(0,3));
      let startdateDate1=startDate1.substring(4,6)
      let start1=startdateMonth1.toString().trim()+"-"+startdateDate1.trim()+"-"+startDate1.split(",")[1].trim();

      let startfullTime1=$("#resquestTimeCalNewUI_"+id).text();
      // let hour1=startfullTime1.substring(startfullTime1.length-2,startfullTime1.length)=="PM"?(parseInt(startfullTime1.split(":")[0].trim())+12):startfullTime1.split(":")[0].trim();
      // let startTime1=hour1+":"+startfullTime1.split(":")[1].trim();
      // var role = $(this).find('#commentApprove_'+id).val();
      // console.log("role-->>"+role);
      if(userArray1.includes(id)==false){
        userArray1.push(id);
        
        userData1+="{";
        userData1+="\"user_id\":\""+id+"\",";
        userData1+="\"task_user_status\":\""+$(this).find('#approveStatusImg_'+id).attr('title')+"\",";
        userData1+="\"sequence\":\""+$(this).attr('secuenceNumber')+"\",";
        userData1+="\"requested_date\":\""+start1+"\",";
        userData1+="\"requested_time\":\""+startfullTime1+"\"";
      // userData+="\"userAction\":\""+$(this).find(".userAction").html()+"\"";dependenctTaskIdstotalEstHr
        userData1+="},";

      }
    }
    
  });

  userArray1=[];

  userData1=userData1.length>1?userData1.substring(0,userData1.length-1)+"]":"[]";
  

  let endfullTime=$("#endTimeCalNewUI").text();
  let endhour=endfullTime.substring(endfullTime.length-2,endfullTime.length)=="PM"?(parseInt(endfullTime.split(":")[0].trim())+12):endfullTime.split(":")[0].trim();
  let endTime=endhour+":"+endfullTime.split(":")[1].trim();

  let estimateHour= $(".estimateHour").val();
  let estimateMin= $(".estimateMinute").val();

  if(estimateMin>59){
    estimateHour=parseInt(estimateHour)+Math.floor(parseInt(estimateMin)/60);
    estimateMin=Math.floor(parseInt(estimateMin)%60);
  }

  let group_id=$("#changeGroup").attr('group_id');

  var sourceid="";let sprintid = "";let sprintgroupid = "";
  if(globalhlmenu=="Highlight"){
    sourceid=globalhid;
    task_type="Highlight";
  }else if(globalmenu=="Idea"){
    sourceid=$('.ideaTask').attr('ideaid');
    task_type=globalmenu;
  }else if(globalmenu=="agile" || globalmenu=="sprint"){
    sourceid=$('.agileTask').attr('agileid');
    task_type="Sprint";
    sprintid = $('#sprintname').attr('tsprintid');
    sprintgroupid = $('#sprintgroupname').attr('tsprgrpid');
  }else if(sprintTaskinTask=="sprintTaskinTask"){
    sourceid=$('#selectedStoryIntask').attr('storyid');
    task_type="Sprint";
    sprintid = $('#sprintname').attr('tsprintid');
    sprintgroupid = $('#sprintgroupname').attr('tsprgrpid');
  }else if(globalmenu=="activityFeed"){
    sourceid=$('.popupTask').attr('srcid');
    task_type="Tasks";
  }else if(globalmenu=="wsdocument"){
    sourceid=$('.popupTask').attr('srcid');
    task_type="WorkspaceDocument";
  }
  

  console.log(JSON.parse(userData1));
  
  //alert(group_id)
  let jsonbody = {
    "user_id": userIdglb,
    "company_id": companyIdglb,
    "task_type": task_type,
    "task_name": taskName,
    "start_date":start,
    "end_date":end,
    "place": "task",
    //"recEnd":recEnd,
    //"recType":recType,
    "start_time":startTime,
    "end_time":endTime,
    "task_desc": taskdesc,
    "taskProject": prjid,
    "priority": priorityId,
    //"selectedTaskemailId":selectedTaskemailId,
    "reminder":reminder,
    //"recPatternValue":recPatternValue,
     "total_estimated_hour":estimateHour,
     "total_estimated_minute":estimateMin,
    //"sprintGrpId":sprintGrpId,
    // "sprintId":sprintId,
     "saveDocTaskIds":saveDocTaskIds,
    // "linkSavedIds":linkSavedIds,
     "dependencyTaskId":dependencyTaskId,
    // "existingDepTaskId":existingDepTaskId,
    //"source_id":sourceId,
    // "taskNameToFill":taskNameToFillGlobal,
    "taskDate": [],
    "userData": JSON.parse(userData),
    "approval_id" : JSON.parse(userData1),
    "project_id": prjid,
    "task_group_id":group_id,
    "source_id" : sourceid,
    "sprintGrpId":sprintgroupid,
    "sprintId":sprintid
  }
  console.log("jsonbody------->"+JSON.stringify(jsonbody))

  
  await $.ajax({
    url: apiPath + "/" + myk + "/v1/createTask/normalTask",
    type: "POST",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $("#loadingBar").hide();
      timerControl("");
    },
    success: function (resp) {
      if($(".elem-CalenStyle").length!=0){
        $("#addTaskHolderDiv").html("");
        $(".elem-CalenStyle").html("");
        $(".TaskDiv").hide(); 
        calenTask('calendartask');
        closeCalenPopup(); 
        
        /* $(".calentaskid_"+taskId).remove();
        $(".cmvTableContainer").find(".calentaskid_"+taskId).remove(); */
      }else{
      sprintTaskinTask="";
      $("#headerTask, #taskList, .popupTaskDiv").show();
      $("#headerTask").addClass('d-flex').removeClass('d-none');
      let valueofUserPercentage=$("#taskSlideAmount_"+userIdglb).val();
      $(".addTaskDiv").fadeOut(300, function(){ $(this).remove();});
      setTimeout(function () {
       
        if(parseInt(valueofUserPercentage)>0 && parseInt(valueofUserPercentage)<100){
          changeStatusOfTaskToProgress(resp[0].task_id, prjid,'Inprogress', 'editslide', +resp[0].task_type); 
        }else if(parseInt(valueofUserPercentage)==100){
          changeStatusOfTaskToProgress(resp[0].task_id, prjid,'Completed', 'editslide', +resp[0].task_type); 
        }
       
      },1000)

    
      if(globalhlmenu=="Highlight"){
        closeHLTask();
        closeHightlightCmePopUp();
        $('#hl_'+globalhid).find('.hTaskIcon').attr('onclick', 'editHLTask('+resp[0].task_id+', '+resp[0].source_id+');event.stopPropagation();').attr('tsid', resp[0].task_id).show();
        if(globalhltype == '1'){
          viewHighlight(globalhlmsgid);
          globalhltype = '';
        }
      }
      if(gloablmenufor=='Myzone'){
          if(valueForView=="grid"){
            $("#innerTaskList").prepend(createTaskGridUI(resp));
            if(resp[0].project_id!=0 || resp[0].project_id!="undefined"){
              $(".tasklistProjectImage").show();
            }
            
          }else{
            $("#taskList").prepend(createTaskUI(resp));
            $(".tasklistProjectImage").show(); 
          }
      }else{
        if(valueForView=="list"){
          $("#taskList").prepend(createTaskUI(resp));
        }else if(valueForView=="grid"){
          $("#innerTaskList").prepend(createTaskGridUI(resp));
          // $("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=taskPlay_]").removeAttr("onclick");
          // $("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=tasklistcheckbox_]").removeAttr("onclick");
        }else{
          $("#taskList").prepend(createTaskUI(resp));
        }
        if(globalmenu=="Idea"){
          $('.notaskdiv').remove();
          if(!$('#ideatask_'+sourceid).is(':visible')){
            $('#ideatask_'+sourceid).css('display','block');
          }
        }else if(globalmenu=="agile" || globalmenu=="sprint"){
          $('.notaskdiv').remove();
          if(glbsprintplace=="SB"){
            closeagileTask();
            updateStatus(sourceid,'In Progress');
            showSprintScrumboard(sprintid);
          }
        }else if(globalmenu=="activityFeed"){
          $('.notaskdiv').remove();
          if($("#listTaskoption_"+$(".popupTask").attr('srcid')).length==0){
            $('<div class="" style="align-self:end;"><img src="images/idea/task_nofill.svg" id="listTaskoption_'+$(".popupTask").attr('srcid')+'" onclick="commentsTask('+$(".popupTask").attr('srcid')+',\'listConvo\');event.stopPropagation();" class="pl-0 mr-1" title="" style="cursor:pointer;width:20px;height:20px;" alt="Image"></div>').insertAfter("#aFeed1_"+$(".popupTask").attr('srcid'));
          }
        }else if(globalmenu=="wsdocument"){
          $('.notaskdiv').remove();
          
        }
      }
      for(let i=0;i<resp[0].approval_id.length;i++){
      // $('#rolenew1_'+resp[0].approval_id[i].user_id+resp[0].task_id).val(resp[0].approval_id[i].user_task_status);
      if(resp[0].approval_id[i].user_task_status=="Pending approval"){
        // apprImgPath="/images/task/pending_approval.svg";
        // appTitle="Pending approval";
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/pending_approval.svg');
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','Pending approval');
        $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).hide();
      }else if(resp[0].approval_id[i].user_task_status=="Approved"){
        // apprImgPath="/images/task/approved.svg";
        // appTitle="Approved";
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/approved.svg');
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','Approved');
        $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).show();
      }else if(resp[0].approval_id[i].user_task_status=="Rejected"){
        // apprImgPath="/images/task/rejected.svg";
        // appTitle="rejected";
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/rejected.svg');
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','Rejected');
        $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).hide();
      }else if(resp[0].approval_id[i].user_task_status=="On hold"){
        // apprImgPath="/images/task/on_hold.svg";
        // appTitle="On hold";
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/on_hold.svg');
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','On hold');
        $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).hide();
      }else if(resp[0].approval_id[i].user_task_status=="Cancel"){
        // apprImgPath="/images/task/cancel.svg";
        // appTitle="Cancel";
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/cancel.svg');
        $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','Cancel');
        $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).hide();
      }

        if(resp[0].approval_id[i].user_id== userIdglb){

        }else{
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).removeAttr("onclick");
        }
      }
      $('#loadingBar').removeClass('d-flex').addClass('d-none'); 
    }
    }
  });
  
}

if(gloablmenufor=='Task'){
  //alert(prjid);
  fetchTasks('', 'firstClick');
}else if(globalmenu=="Idea" || globalmenu=="agile" || globalmenu=="sprint" || globalmenu=="wsdocument"){

}else{
  //prjid=0;
  //alert(prjid);
}

function changeStatusOfUserTask(taskId, projId, place, taskType) {

  var userTaskStatus = "";
  if (place == "listView") {
    if(valueForView=="calendarTask"){
      userTaskStatus = $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("statusOfTaskUser");
    }else{
      userTaskStatus = $("#taskPlay_" + taskId).attr("statusOfTaskUser");
    }  
    
  } else {
    userTaskStatus =   $("#persontaskstatusdisplay_"+userIdglb).attr("statusOfTaskUser");
  }
  //alert(userTaskStatus);
  if (userTaskStatus == "Created") {
    userTaskStatus = "Inprogress";
    if (place == "listView") {
      if(valueForView=="calendarTask"){
        $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("src", "images/task/user-inprogress.svg");
        $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("statusOfTaskUser", userTaskStatus);
        $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("title", userTaskStatus);
      }else{
        $("#taskPlay_" + taskId).attr("src", "images/task/user-inprogress.svg");
        $("#taskPlay_" + taskId).attr("statusOfTaskUser", userTaskStatus);
        $("#taskPlay_" + taskId).attr("title", userTaskStatus);
      }
      
    } else {
      $("#persontaskstatusdisplay_"+userIdglb).attr("src", "images/task/user-inprogress.svg");
      $("#persontaskstatusdisplay_"+userIdglb).attr("statusOfTaskUser", userTaskStatus);
      $("#taskPlay_" + taskId).attr("src", "images/task/user-inprogress.svg");
      $("#taskPlay_" + taskId).attr("statusOfTaskUser", userTaskStatus);
      $("#taskPlay_" + taskId).attr("title", userTaskStatus);
    }
    changeStatusOfTaskToProgress(taskId, projId, userTaskStatus, place, taskType);

  } else if (userTaskStatus == "Inprogress") {
    userTaskStatus = "Paused";
    if (place == "listView") {
      if(valueForView=="calendarTask"){
        $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("src", "images/task/user-pause.svg");
        $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("statusOfTaskUser", userTaskStatus);
        $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("title", userTaskStatus);
        $("#otherModuleTaskListDiv").find('#opentaskParticipantdiv_'+taskId).css('display','none');
      }else{
        $("#taskPlay_" + taskId).attr("src", "images/task/user-pause.svg");
        $("#taskPlay_" + taskId).attr("statusOfTaskUser", userTaskStatus);
        $("#taskPlay_" + taskId).attr("title", userTaskStatus);
        $('#opentaskParticipantdiv_'+taskId).css('display','none');
      }
      
    } else {
      $("#persontaskstatusdisplay_"+userIdglb).attr("src", "images/task/user-pause.svg");
      $("#persontaskstatusdisplay_"+userIdglb).attr("statusOfTaskUser", userTaskStatus);
      $("#taskPlay_" + taskId).attr("src", "images/task/user-pause.svg");
      $("#taskPlay_" + taskId).attr("statusOfTaskUser", userTaskStatus);
      $("#taskPlay_" + taskId).attr("title", userTaskStatus);
    }
    changeStatusOfTaskToProgress(taskId, projId, userTaskStatus, place, taskType);

  } else if (userTaskStatus == "Paused") {
    userTaskStatus = "Inprogress";
    if (place == "listView") {
      if(valueForView=="calendarTask"){
        $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("src", "images/task/user-inprogress.svg");
        $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("statusOfTaskUser", userTaskStatus);
        $("#otherModuleTaskListDiv").find("#taskPlay_" + taskId).attr("title", userTaskStatus);
      }else{
        $("#taskPlay_" + taskId).attr("src", "images/task/user-inprogress.svg");
        $("#taskPlay_" + taskId).attr("statusOfTaskUser", userTaskStatus);
        $("#taskPlay_" + taskId).attr("title", userTaskStatus);
      }
      
    } else {
      $("#persontaskstatusdisplay_"+userIdglb).attr("src", "images/task/user-inprogress.svg");
      $("#persontaskstatusdisplay_"+userIdglb).attr("statusOfTaskUser", userTaskStatus);
      $("#taskPlay_" + taskId).attr("src", "images/task/user-inprogress.svg");
      $("#taskPlay_" + taskId).attr("statusOfTaskUser", userTaskStatus);
      $("#taskPlay_" + taskId).attr("title", userTaskStatus);
    }

    changeStatusOfTaskToProgress(taskId, projId, userTaskStatus, place, taskType);

    if ($("#TaskStatus_" + taskId).is(":visible")) {
      $("#TaskStatus_" + taskId + "").slideToggle(function () {
        $("#TaskStatus_" + taskId + "").children('div.TaskStatusMainTextareaDiv').html("");
      });
    }

  } else if (userTaskStatus == "Completed") {
    userTaskStatus = "Completed";
  }
}


// var stopUserTaskTime="";
function changeStatusOfTaskToProgress(taskId, projId, taskStatus, place, taskType) {
  //alert("taskStatus:"+taskStatus);
  if(valueForView=="list"){
    $("#task_" + taskId).css("background-color", "");
  }
  if(taskStatus=="" || typeof(taskStatus) =='undefined'){
    taskStatus=$('#appNewImg_'+userIdglb+taskId).attr('title'); //$("#rolenew1_"+userIdglb+taskId).val();
  }

  console.log("taskStatus-->"+taskStatus);
  
  let jsonbody="";
  //$("#task_"+taskId).css("border-bottom", "1px solid #C1C5C8");
  if(place=='editslide'){
    let user_percentage=$("#taskSlideAmount_"+userIdglb).val();
    jsonbody = {
      "task_id": taskId,
      "user_id": userIdglb,
      "user_task_status": taskStatus,
      "user_percentage":user_percentage
    }
  }else{
    jsonbody = {
      "task_id": taskId,
      "user_id": userIdglb,
      "user_task_status": taskStatus
    }
  }

  var workingMinutes = ($('#workingMinutes').val() == '' || typeof ($('#workingMinutes').val()) == 'undefined') ? '0' : $('#workingMinutes').val();
  var workingHours = ($('#workingHours').val() == '' || typeof ($('#workingHours').val()) == 'undefined') ? '0' : $('#workingHours').val();
  var taskUserPercentage = $("#taskAmount_" + userIdglb + "").val();
  var taskComment = $('#UserCommentNewUI').val();

  // let jsonbody = {
  //   "task_id": taskId,
  //   "user_id": userIdglb,
  //   "user_task_status": taskStatus
  // }
  checksession();
  $.ajax({
    url: apiPath + "/" + myk + "/v1/changeTaskStatus",
    //url:"http://localhost:8080/v1/changeTaskStatus",
    type: "POST",
    contentType: "application/json",
    dataType: 'json',
    data: JSON.stringify(jsonbody),
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $("#loadingBar").hide();
      timerControl("");
    },
    success: function (result) {

      if (result[0].result == "success") {
        var actualHourresult = result[0].act_time;
        var actualHourTime = "";
        var actualminuteTime = "";
        if (typeof (actualHourresult) != "undefined" || actualHourresult != "") {
          actualHourTime = actualHourresult.split(":")[0];
          actualminuteTime = actualHourresult.split(":")[1];
        } else {
          actualHourTime = "";
          actualminuteTime = "";
        }

        ///var HourActResult =result.split("###")[4];
        ActHourResult = result[0].act_hour;
        ActMinuteResult = result[0].act_minute;
        var cmtId = result[0].idd;
        if (taskStatus == "Paused") {
          if (place == 'listView') {
            changeStatusOfTaskToPaused(taskId, projId, taskStatus, actualHourTime, actualminuteTime, cmtId, taskType);
          } else {
            /*  $("#emailPrtcpnt_"+userIdglb).find('#totalActworkingMinutes').val(ActHourResult);
             $("#emailPrtcpnt_"+userIdglb).find('#totalActworkingMinutes').val(ActMinuteResult);
             try{
               clickEventCommentDocSwith = "firstClick";
               var onclick=($("#emailPrtcpnt_"+userIdglb).attr("onclick"));
               var hastoonclick=onclick.split(";")[0];
               
               $("#emailPrtcpnt_"+userIdglb).attr("onclick",hastoonclick)
       
               $("#emailPrtcpnt_"+userIdglb).trigger("onclick");
               $("#emailPrtcpnt_"+userIdglb).attr("onclick",onclick)
             }catch(e){
               console.log(e);
             } */
             getTaskUserComment(taskId,userIdglb,'');
          }
        }
        if(place=='editslide'){
          $("#persontaskstatusdisplay_"+userIdglb).attr("src", "images/task/user-inprogress.svg");
          $("#persontaskstatusdisplay_"+userIdglb).attr("statusOfTaskUser", taskStatus);
          $("#slide_"+userIdglb).removeAttr('onchange');
        }
      }

    }
  });
}

function changeStatusOfTaskToPaused(taskId, projId, taskStatus, actualHourTime, actualminuteTime, cmtId, taskType) {
  if(valueForView!="list"){
    $("#updateStatusBlock").append(updateStatusUiNew(taskId));
  }
  
  if ($("#TaskStatus_" + taskId).is(":hidden")) {
    $("#TaskStatus_" + taskId).children('div.TaskStatusMainTextareaDiv').html("");
    if(valueForView=="list"){
      $("#task_" + taskId).css("background-color", "#eef5f7");
    }
    
    //$("#task_"+taskId).css("border-bottom", "none");

    $("#loadingBar").show();
    timerControl("start");
    if (taskType != "assignedEvent") {
      var localOffsetTime = getTimeOffset(new Date());
      checksession();
      $.ajax({
        url: apiPath + "/" + myk + "/v1/getHistoryMyTaskDetailView/normalTask?sprintHistoryValue=&task_type=" + taskType + "&task_id=" + taskId + "&user_id=" + userIdglb + "&company_id=" + companyIdglb + "",
        type: "GET",
        error: function (jqXHR, textStatus, errorThrown) {
          checkError(jqXHR, textStatus, errorThrown);
          $("#loadingBar").hide();
          timerControl("");
        },
        success: function (result) {
          checkSessionTimeOut(result);
          jsonData = result[0].taskParticipant;
          var taskstatusui = "";
          for (var i = 0; i < jsonData.length; i++) {
            if (userIdglb == jsonData[i].user_id) {

              var elapTime = jsonData[i].elapsed_time;
             
              var elapTotalTime = "";
              var elapHour = "";
              var elapMin = "";
              if (elapTime != "" && elapTime != "-" && elapTime != "00:00:00" && typeof(elapTime)!='undefined') {
                elapHour = parseInt(elapTime.split(":")[0]);
                elapMin = parseInt(elapTime.split(":")[1]);
                if (status == 'Completed') {
                  elapTotalTime = elapHour + " h " + (elapMin == 0 ? "" : elapMin + " m");
                } else if (elapHour == 0) {
                  elapTotalTime = (elapMin == 0 ? "" : elapMin + "m" + " ago");
                } else if (elapHour < 24) {
                  elapTotalTime = elapHour + " h " + (elapMin == 0 ? "" : elapMin + " m") + " ago";
                } else if (parseInt(elapHour) >= 24) {
                  var elapDay = parseInt(elapHour) / 24;
                  elapDay = Math.floor(elapDay);
                  if (elapDay == 1)
                    elapTotalTime = Math.floor(elapDay) + " day" + " ago";
                  else
                    elapTotalTime = Math.floor(elapDay) + " days" + " ago";
                }
              }
              
              taskstatusui += '<div class="d-flex w-100 my-2" id="taskCode1" style="" >'
                
                +'<div class="material-textfield"  style="    width: 21%;">'
                   +'<select  id="myTemplateComments" class="material-textfield-input"  onchange="fillCommentText(this)" style="padding-left: 8px; background: none; border: 1px solid #C1C5C8;border-radius: 6px;height:30px;    outline: none;width:100%">'
                   +'</select>'
                   +'	<label class="material-textfield-label" id="progcomment1" style="margin-left: 5px; font-size:12px; background-color: rgb(238, 245, 247);!important ">Task code</label>'
                +'</div>'
                
               + '<div id="cmttaskstat1" class="w-75 material-textfield" style="margin-left: 10px;    padding-right: 8px;">'
                // + '<input id="UserCommentNewUI" onkeyup="hashTrendingCodesInTask(event);" onclick="event.stopPropagation();" placeholder=" " class="theightdemo material-textfield-input Comments_cLabelHtml italicsForPlaceHolder" type="text" style="border-radius: 6px;font-size: 12px;padding-left:10px;border-radius: 3px;float:left;width: 95%;height: 33px;border: 1px solid #409bb5;outline:none;">'
                // +'<label class="material-textfield-label" style=" font-size: 12px; top: 16px; background-color: rgb(238, 245, 247); ">Progress Comments</label></div>'
                // + '</div>'
                    +'<div class="material-textfield" >'
                        +'<textarea id="UserCommentNewUI" type="text" class="theightdemo material-textfield-input"   placeholder=" " style="padding-left: 10px; background: none;border: 1px solid #C1C5C8;border-radius: 6px;resize: none;height:30px;    outline: none; width: 100%;"></textarea>'
                        +'<label class="material-textfield-label" id="progcomment" style=" font-size: 12px; top: 16px; background-color: rgb(238, 245, 247)!important; ">Progress Comments</label></div>'
                      
                    +'</div>'
                + '</div>'

                + '</div>'

                + '<div class="d-flex w-100 mt-3 mb-2" id="" style="">'

                + "<div id=\"\" class=\"w-25 d-flex\" style=\"margin-left: 5px;font-size:12px\">"
                + '<input id="slide-range" participantId = ' + jsonData[i].user_id + ' class="sliderange mt-2" type="range" min="0" max="100" onmouseup="changeSliderInputvalue(this);" onmousemove="slidercolor();" style="color: #848484;width: 70%;" value="' + jsonData[i].task_user_percentage + '">'
                + '<div class="ml-1" style="width:30%;">'
                + '<input id="taskSlideAmount_' + jsonData[i].user_id + '" name="taskSlideAmount" onblur="changeSli(this);" class="" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;background:#eef5f7;width:35px;text-align: right;outline: 0;border-width: 0 0 1px;color:black" value="' + jsonData[i].task_user_percentage + '">'
                + '<span class="">&nbsp;%</span>'
                + '</div>'
                + "</div>"

                + "<div id=\"actionact\" class=\"w-25 d-flex px-2\" style=\"height: 32px;font-size:12px\">"
                + "<div class=\"wsTaskLabels mr-2 align-self-center\" id=\"projectSection\" style=\"width: 48px;font-size: 12px !important;margin-top:-10px\">" + getValues(companyLabels, 'Actual') + "</div>"
                + "<input type=\"text\" value=\"0\" style='border-bottom:1px solid  #c1c5c8;height:22px; float:left;background:#eef5f7;width:35px;text-align: right;outline: 0;border-width: 0 0 1px;color:black' class=\"workingHours mt-0\" id=\"workingHours\" onclick=\"event.stopPropagation();\"><span class=\"px-1 align-self-center\" style=\"float: left;font-size:12px;margin-top: -10px;\">h</span>"
                + "<input type=\"text\" value=\"0\" style='border-bottom:1px solid  #c1c5c8;height:22px; float:left;background:#eef5f7;width:32px;text-align: right;outline: 0;border-width: 0 0 1px;color:black'  class=\"workingMinutes mt-0\" id=\"workingMinutes\" onclick=\"event.stopPropagation();\"><span class=\"px-1 align-self-center\" style=\"float: left;font-size:12px;margin-top: -10px;\">m</span>"
                + "</div>"
              if (elapTime != "") {
                taskstatusui += "<div id=\"actionelp\"  class=\"w-25 d-flex ml-sm-5 ml-md-0 ml-lg-0 ml-xl-0 align-items-center\" style=\"height: 32px;font-size: 12px;margin-top:-6px\">"
                  + "<div class=\"wsTaskLabels font-size:12px;\" id=\"elapsedTime\" style=\"font-size: 12px !important;\">Elapsed</div>"
                  + "<span class=\"taskElapHrSpan ml-2 font-size:12px;\"elapHr=\"" + elapHour + "\" elapMin=\"" + elapMin + "\"  style=\"font-size: 12px !important;float: left;\">" + elapTotalTime + "</span>"
                  + "</div>"
              }
              taskstatusui += "<div class=\"d-flex justify-content-end mt-2\" style=\"width:21%;\">"
                + "<img id=\"newUIForDeleteTaskStatus\" onclick=\"cancelTaskStatusCreation(" + taskId + ");event.stopPropagation();\" onmouseenter=\"highlightOnCancel(this)\" onmouseleave=\"unhighlightOnCancel(this)\" title=\"Cancel\" class=\"img-responsive imgReplacementForCancel\"  src=\"images/tasksNewUI_old/close.png\" style=\"margin: 0px 4px;float:right;width:25px;height:25px;cursor:pointer;\">"
                + "<img id=\"wsTaskSaveContainer\" onclick=\"updateListViewTask(" + taskId + ",'Tasks'," + projId + "," + cmtId + ");event.stopPropagation();\" onmouseenter=\"highlightOnSave(this)\" onmouseleave=\"unhighlightOnSave(this)\" title=\"Save\" class=\"img-responsive imgReplacementForSave\" src=\"images/tasksNewUI_old/save.png\" style=\"float:right;width:25px;height:25px;margin-left:4px;cursor:pointer;\" \">"
                + "</div>"


                + '</div>'
            }
          }
          
          if(valueForView=="" || valueForView=="list"){
            $("#TaskStatus_" + taskId + "").show().children('div.TaskStatusMainTextareaDiv').html(taskstatusui);
            $("#taskCode1").addClass("my-2").removeClass("my-3");
          }else if(valueForView=="grid" || valueForView=="calendarTask"){
            // $('#ideaShareListDiv').html('');
            $("#transparentDiv").show();
            $("#updateStatusBlock").show();
            if(valueForView=="grid"){
              $("#TaskStatus_" + taskId + "").show().children('div.TaskStatusMainTextareaDiv').html(taskstatusui);
            }else{
              $("#updateStatusBlock").find("#TaskStatus_" + taskId + "").show().children('div.TaskStatusMainTextareaDiv').html(taskstatusui);
            }
            
            $("#taskCode1").addClass("my-3").removeClass("my-2");
            $("#progcomment").css("background-color", "white");
            $("#progcomment1").css("background-color", "white");
            // $("#TaskStatus_" + taskId + "").show().children('div.TaskStatusMainTextareaDiv').html('');
          }
          
          FetchCommentsForTaskStatus(taskId);
          $('#workingHours').attr("value", parseInt(actualHourTime));
          $('#workingMinutes').attr("value", parseInt(actualminuteTime));
          slidercolor();

          $('#loadingBar').hide();
          timerControl("");
        }
      });

    }
  }
}

function updateStatusUiNew(taskId){
  var UI="";

  UI+='<div class="statusUi" style="" id="">'
          +'<div class="mx-auto" style="width: 854px;">'
            +'<div class="modal-content container px-3" style="max-height: 300px !important;z-index:1000;padding-bottom: 29px;">'
              +'<div class="" style="">'
                +"<button type='button' onclick=\"cancelTaskStatusCreation(" + taskId + ");event.stopPropagation();\" class='close p-0' style='top: 0px;right: 5px;color: black;outline: none;opacity: unset;'>&times;</button>"
              +'</div>'
              +'<div class="modal-body p-0 justify-content-center wsScrollBar" style="max-height: 164px;height: 164px;overflow: auto;margin-top: 0px;margin-bottom: -28px;">'
                +'<div id=\"TaskStatus_'+taskId+'\" class=\"taskcontent1 pl-4\" style=\"display:none;position: absolute;width: 820px;z-index: 109;height: 130px;top: 34px;border-radius: 8px;font-size: 12px;\">'
                  +'<div class=\"TaskStatusMainTextareaDiv\" style=\"\">'
                  +'</div>'
                +'</div>'
              +'</div>'
            +'</div>'
          +'</div>'
        +'</div>'

    return UI;
}

 function FetchCommentsForTaskStatus(taskId) {
  
  checksession();
   $.ajax({
    url: apiPath + "/" + myk + "/v1/getCommentCodes?task_id=" + taskId + "&company_id=" + companyIdglb + "",
    type: "GET",
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
    },
    success: function (result) {
    
      $('#templateComments').empty();
      $('#myTemplateComments').append("<option class='w-25 optionselect' style='max-width:25%;' data-limit='10'>Select</option>");
      if (result) {
        $.each(result, function (index, value) {
          $('#myTemplateComments').append("<option class='w-25 optionselect' style='max-width:25%;background:white' title='"+value.quotes+"' data-limit='10' value='"+value.quotes+"'>" + trancate(value.quotes,45) + "</option>");
        });
      }

    }
  });
}
function fillCommentText(obj) {
  $("#UserCommentNewUI").val($(obj).val());
  $("#UserCommentNewUI").focus();
}

function cancelTaskStatusCreation(taskId) {
  if(valueForView=="list"){
    $("#task_" + taskId).css("background-color", "");
  }
  
  if(valueForView=="grid" || valueForView=="calendarTask"){
    if(valueForView=="grid"){
      $("#transparentDiv").hide();
      $("#TaskStatus_" + taskId + "").children('div.TaskStatusMainTextareaDiv').html("");
    }else{
      $("#updateStatusBlock").find("#TaskStatus_" + taskId + "").children('div.TaskStatusMainTextareaDiv').html("");
    }
    
    $("#updateStatusBlock").html('');
    $("#updateStatusBlock").hide();
  }else{
    $("#TaskStatus_" + taskId + "").slideToggle(function () {
      $("#TaskStatus_" + taskId + "").children('div.TaskStatusMainTextareaDiv').html("");
    });
  }
}


function switchTaskType(obj) {
  let active=$(".Active").attr('id');
  $(".Active").removeClass('Active');
  $(obj).addClass('Active');
  let nowActive=$(obj).attr('id');

  sprintTaskinTask="";
  $('#tSprintStoryDiv').addClass('d-none').removeClass('d-flex');
  $('.groupDiv').addClass('d-flex').removeClass('d-none');
  $('.thirdDiv').addClass('d-none').removeClass('d-flex');
  $('.actualDiv ').removeClass('mr-auto');
  $("#taskname").val('');
  $('#dependencyTask').parent().show();

  if(active !="Workflow" && $(obj).attr('id')=='Workflow'){
    changeUItoWF(); 
    $(".taskdocname").text('Workflow Document'); //createWorkFlowTask()
    $('#savetask').attr("onclick", "createWorkFlowTask()");
    $(".taskDocument").addClass('WfDocument')
    $(".taskDocument").removeClass('taskDocument');
    $(".WfDocument").css('max-height','200px');
    
    if(gloablmenufor=="Myzone"){
      $("#projectlistingDiv").addClass('d-flex').removeClass('d-none');
      prjid=0;
      fetchProjectsForMyzone();
    }
    $("#attachWFTemplateDiv").css('visibility','visible');
  }else if(active =="Workflow" && $(obj).attr('id')!='Workflow'){
    let UI=prepareAddTaskUiNew();
    //$(".TaskDiv").prepend(UI).show('slow');
    $("#addTaskHolderDiv").html(UI);
   // showDeadLinePopup();
    //replaceSaveWithDueDate();
    var element = document.getElementById("addTaskScroll");
    element.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
    
    createPopupDiv();
    $("#upload").on('click',()=>{
     document.getElementById('uploadForTask').click();
    });
  
    $("#taskspan").on('click',()=>{
      $("#taskspan").css('margin-top','0px')
     });
    clearWFData();
    if(gloablmenufor=="Myzone"){
      $("#projectlistingDiv").addClass('d-flex').removeClass('d-none');
      prjid=0;
      fetchProjectsForMyzone();
    }
    $(".Active").removeClass('Active');
    $("#"+nowActive).addClass('Active');
    
    if($(obj).attr('id')=='tSprinttask'){
      sprintTaskinTask="sprintTaskinTask";
      $('#tSprintStoryDiv').addClass('d-flex').removeClass('d-none');
      taskStoryList();
      $('.groupDiv').addClass('d-none').removeClass('d-flex');
      $('.thirdDiv').addClass('d-flex').removeClass('d-none');
      $('.actualDiv ').addClass('mr-auto');
      $('#dependencyTask').parent().hide();
      $('#taskname').val($("#selectedStoryIntask").text()).focus();
      
    }

  }else if($(obj).attr('id')=='tSprinttask'){
    sprintTaskinTask="sprintTaskinTask";
    $('#tSprintStoryDiv').addClass('d-flex').removeClass('d-none');
    taskStoryList();
    $('.groupDiv').addClass('d-none').removeClass('d-flex');
    $('.thirdDiv').addClass('d-flex').removeClass('d-none');
    $('.actualDiv ').addClass('mr-auto');
    $('#dependencyTask').parent().hide();
    $('#taskname').val($("#selectedStoryIntask").text()).focus();
    
  }
}

function highlightOnCancel(image) {
  image.src = "images/tasksNewUI_old/closetwo.png";
}
function unhighlightOnCancel(image) {
  image.src = "images/tasksNewUI_old/close.png";
}
function highlightOnSave(image) {
  image.src = "images/tasksNewUI_old/savetwo.png";
}
function unhighlightOnSave(image) {
  image.src = "images/tasksNewUI_old/save.png";
}

function updateListViewTask(taskId, taskType, projId, cmtId,place,saveCommentStatus,checkBoxUpdate) {
  place=typeof(place)=='undefined'?"":place;
  saveCommentStatus=typeof(saveCommentStatus)=='undefined'?"Y":saveCommentStatus;
  let taskCompletionAmount="";
  if(place!=""){
    taskCompletionAmount = parseInt($('#slide_' + userIdglb).val());
  }else{
    taskCompletionAmount  = parseInt($('#taskSlideAmount_' + userIdglb).val());
  }
  if(checkBoxUpdate=="checkBoxUpdate"){
    taskCompletionAmount=100;
  }
  var taskAction = 'Created';
  if(valueForView=="list"){
    $("#task_" + taskId).css("background-color", "");
  }
  
  if (taskCompletionAmount >= 100) {
    taskAction = 'Completed';
    taskCompletionAmount = '100';
  }
  var workingMinutes = ($('#workingMinutes').val() == '' || typeof ($('#workingMinutes').val()) == 'undefined') ? '0' : $('#workingMinutes').val();
  var workingHours = ($('#workingHours').val() == '' || typeof ($('#workingHours').val()) == 'undefined') ? '0' : $('#workingHours').val();
  var myComment = $('#UserCommentNewUI').val();

  if(place!=''){
    workingHours=$("#user_id_"+userIdglb).find(".workinghour").val();
    workingMinutes=$("#user_id_"+userIdglb).find(".workingMinutes").val();
    //alert(workingHours+"---"+workingMinutes);
  }
  if(workingMinutes>59){
    workingHours=parseInt(workingHours)+Math.floor(parseInt(workingMinutes)/60);
    workingMinutes=Math.floor(parseInt(workingMinutes)%60);
  }

  let commentJson="[";
  if(place!=''){
    // if(place=='edit'){
    $('.usercommentdiv > div').each(function () { 
      if($(this).attr('id')!='no-comments' ){
        if($(this).attr('class').includes('comment')){
          var id = $(this).attr('id').split('_')[2];
          let oHour=$(this).find('#workingHours').val();
          let oMin=$(this).find('#workingMinutes').val();;
          let actualHourFlag  ="Y";
          let hour=$(this).find('#workingHours').val();
          let min=$(this).find('#workingMinutes').val();

          if(min>59){
            hour=parseInt(hour)+Math.floor(parseInt(min)/60);
            min=Math.floor(parseInt(min)%60);
          }

          if(typeof(hour)=="undefined" && typeof(min)=="undefined"){
            hour="0";
            min="0";
          }

          commentJson+="{";
          commentJson+="\"id\":\""+id+"\",";
          commentJson+="\"value\":\""+$(this).find(".task_comment_user").val()+"\",";
          commentJson+="\"workingHourValue\":\""+hour+"\",";
          commentJson+="\"workingMinuteValue\":\""+min+"\",";
          commentJson+="\"actualHourFlag\":\""+actualHourFlag+"\"";
         // userData+="\"userAction\":\""+$(this).find(".userAction").html()+"\"";dependenctTaskIdstotalEstHr
         commentJson+="},";
        }
      }
      
    });
  // }
  // }else{
  //   $('.usercommentdiv > div').each(function () { 
  //     // if($(this).attr('id')!='no-comments' ){
  //       if($(this).attr('class').includes('commentId')){
  //         var id = $(this).attr('id').split('_')[2];
  //         let oHour=$(this).find('#workingHours').val();
  //         let oMin=$(this).find('#workingMinutes').val();;
  //         let actualHourFlag  ="Y";
  //         let hour="";
  //         let min="";

  //         // if(min>59){
  //         //   hour=parseInt(hour)+Math.floor(parseInt(min)/60);
  //         //   min=Math.floor(parseInt(min)%60);
  //         // }

  //         commentJson+="{";
  //         commentJson+="\"id\":\""+id+"\",";
  //         commentJson+="\"value\":\""+$(this).find("#commentApprove_"+id).val()+"\",";
  //         commentJson+="\"workingHourValue\":\""+hour+"\",";
  //         commentJson+="\"workingMinuteValue\":\""+min+"\",";
  //         commentJson+="\"actualHourFlag\":\""+actualHourFlag+"\"";
  //        // userData+="\"userAction\":\""+$(this).find(".userAction").html()+"\"";dependenctTaskIdstotalEstHr
  //        commentJson+="},";
  //       }
  //     // }
      
  //   });
  // }
  }
  commentJson=commentJson.length>1?commentJson.substring(0,commentJson.length-1)+"]":"[]";
  //console.log(commentJson)
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody = {
    "taskCompComm": myComment,
    "tAction": taskAction,
    "projHashTagName": "",
    "taskCompAmount": taskCompletionAmount,
    "workingHours": workingHours,
    "selectedUserName": "",
    "project_id": projId,
    "workingMinutes": workingMinutes,
    "saveCommentStatus": saveCommentStatus,
    "commentJson": JSON.parse(commentJson),
    "event_id": taskId,
    "user_id": userIdglb,
    "company_id": companyIdglb,
    "task_type": taskType,
    "cmtId": cmtId,
    "task_id": taskId
  }
  //console.log("jsonbody-->"+JSON.stringify(jsonbody));  document.getElementById('FileUpload_'+Id+'').click();
  checksession();
  $.ajax({
    url: apiPath + "/" + myk + "/v1/updateUserCompletion",
    type: "PUT",
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
    },
    success: function (result) {
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      //console.log("result-->"+JSON.stringify(result))
      if (taskAction == 'Completed') {
        changeStatusOfTaskToProgress(taskId, projId, taskAction);
        if(valueForView=="calendarTask"){
          /* $("#otherModuleTaskListDiv").find("#taskPlay_"+taskId).attr("src", "images/task/user-completed.svg");
          $("#otherModuleTaskListDiv").find("#taskPlay_"+taskId).attr("onclick", "");
          $("#otherModuleTaskListDiv").find("#taskPlay_"+taskId).attr("title", "Completed");
          $("#otherModuleTaskListDiv").find("#tasklistcheckbox_" + taskId).attr('onclick','');
          $("#otherModuleTaskListDiv").find("#tasklistcheckbox_" + taskId).attr('src','images/task/check.svg'); */
          $(".calendar-2").html("");
          $(".calendar-2").removeClass("elem-CalenStyle");
          calenTask('calendartask');
          closeCalenPopup();
        }else{
          $("#taskPlay_"+taskId).attr("src", "images/task/user-completed.svg");
          $("#taskPlay_"+taskId).attr("onclick", "");
          $("#taskPlay_"+taskId).attr("title", "Completed");
          $("#tasklistcheckbox_" + taskId).attr('onclick','');
          $("#tasklistcheckbox_" + taskId).attr('src','images/task/check.svg');
        }
        
        taskidglb="";
      }
      if(valueForView=="grid" || valueForView=="calendarTask"){
        if(valueForView=="grid"){
          $("#transparentDiv").hide();
          $("#TaskStatus_" + taskId + "").children('div.TaskStatusMainTextareaDiv').html("");
        }else{
          if($("#updateStatusBlock").find("#TaskStatus_" + taskId + "").children('div.TaskStatusMainTextareaDiv').children().length!=0){
            $("#updateStatusBlock").find("#TaskStatus_" + taskId + "").children('div.TaskStatusMainTextareaDiv').html("");
          }else{
            
            calenTask('calendartask');
            closeCalenPopup();
          }
        }
        $("#updateStatusBlock").html('');
        $("#updateStatusBlock").hide();
      }else{
        $("#TaskStatus_" + taskId + "").slideToggle(function () {
          $("#TaskStatus_" + taskId + "").children('div.TaskStatusMainTextareaDiv').html("");
        });
      }

      //if (taskType == "Tasks" || taskType == "Idea") {
        //fetchTasks('','firstClick');
        var actualtime = result[0].act_hour;
        var actualminute = result[0].act_minute;
        var estHour = result[0].total_estimated_hour == null ? "0" : result[0].total_estimated_hour;
        var estMinute = result[0].total_estimated_minute == null ? "0" : result[0].total_estimated_minute;
        var esttimeparse = (parseInt(estHour) * 60) + parseInt(estMinute);
        var actualtimeparse = (parseInt(actualtime) * 60) + parseInt(actualminute);
        //$('#actTime_' + taskId).text('');
        actualtime=Math.floor(actualtimeparse/60);
        actualminute=Math.floor(actualtimeparse%60);
        //$('#actTime_' + taskId).text(actualtime + " h " + actualminute + " m");
        
        if (actualtimeparse > esttimeparse) {
          let color = "red";
          //$('#actTime_' + taskId).css('color', color);
          $('[id^=actTime_'+taskId+']').css('color', color);
        }
        var taskStatusImageTitle=result[0].display_status;
        var taskStatusImage = taskStatusImageTitle == "Completed" ? "images/task/completed.svg" : taskStatusImageTitle == "InProgress" ? "images/task/in-progress.svg" : taskStatusImageTitle == "Created" ? "images/task/in-progress.svg" : taskStatusImageTitle == "InComplete" ? "images/task/in-complete.svg" : taskStatusImageTitle == "NotCompleted" ? "images/task/not-completed.svg"  : "images/task/not-completed.svg";
        //$("#tstatusImg_" + taskId).attr('src',taskStatusImage);
        //$("#tstatusImg_" + taskId).attr('title',taskStatusImageTitle);
        if(taskStatusImageTitle=="Created"){taskStatusImageTitle="In Progress";}
        else if(taskStatusImageTitle=="NotCompleted"){taskStatusImageTitle="Not Completed";}
        else if(taskStatusImageTitle=="InProgress"){taskStatusImageTitle="In Progress";}
        else if(taskStatusImageTitle=="InComplete"){taskStatusImageTitle="Incomplete";}

        $('[id^=actTime_'+taskId+']').text('');
        $('[id^=actTime_'+taskId+']').text(actualtime + " h " + actualminute + " m");
        $('[id^=tstatusImg_'+taskId+']').attr('src',taskStatusImage);
        $('[id^=tstatusImg_'+taskId+']').attr('title',taskStatusImageTitle);

      //}

      
    }

  });


}
async function AddUserToTheTask() {
  $(".task_user_new_div1").hide();
  $(".task_user_new_div").show();
  var jsonData = "";
  await $.ajax({
    url: apiPath + "/" + myk + "/v1/fetchParticipants?user_id=" + userIdglb + "&company_id=" + companyIdglb + "&project_id=" + prjid,
    type: "GET",
    ///data:{act:'fetchAllUsersListForSpecChar',projectId:projectId,userId: userId,menuType:menuType,docFolId:docFolId,taskId:taskId},
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      timerControl("");
    },
    success: function (result) {

      jsonData = result;
      result = prepareUIforspecialcharacterNewforTask(jsonData, prjid, "type");
      
      $(".deadlinepopup").hide();
      $(".optionDetails").append(result);
      userSearch();

      if(glbsprintplace=="SB"){
        $('#taskassignee_'+userIdglb).trigger('click');
        calenderForTask();
        //calenderForTask1();
      }
      $(".dhtmlxcalendar_material").hide();
    }
  });
}

function AddUserToTheTask1() {
  $(".task_user_new_div").hide();
  $(".task_user_new_div1").show();
  
  var jsonData = "";
  $.ajax({
    url: apiPath + "/" + myk + "/v1/fetchParticipants?user_id=" + userIdglb + "&company_id=" + companyIdglb + "&project_id=" + prjid,
    type: "GET",
    ///data:{act:'fetchAllUsersListForSpecChar',projectId:projectId,userId: userId,menuType:menuType,docFolId:docFolId,taskId:taskId},
    beforeSend: function (jqXHR, settings) {
      xhrPool.push(jqXHR);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      timerControl("");
    },
    success: function (result) {
      // console.log(JSON.stringify(result));
      jsonData = result;
      result = prepareUIforspecialcharacterNewforTask1(jsonData, prjid, "type");
      
      $(".deadlinepopup").hide();
      $(".optionDetails").append(result);

      $('#tag').css("left", "186px");
      userSearch();
    }
  });
}

function prepareUIforspecialcharacterNewforTask(jsonData, prjid, type) {
  $('#tag').remove();
  var sessionuserId = userIdglb;
  var UI="";
  UI = "<div class=\"atTag col-12 d-flex  position-absolute \" id='tag' style='z-index:2;font-size:12px;'>"
    +'<div class="border border-secondary py-3 pl-3 pr-2 touchablePop atListPop position-absolute rounded" style="background-color:white;margin-top: -2px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;display:block;">'
  +'<div class="d-flex  w-100 justify-content-between">'
  +"<div class=\" pb-1 textinvite\">Select participants</div>"
  +'<div class=" d-flex searchclass justify-content-between" onclick="userSearch();"><input type="text" onkeyup="searchUser(this);" id="invite" placeholder="" class="projInviteUser border-0" style=" border-bottom: 1px solid rgb(108, 117, 125) !important; display: none;width: 90%;"><img  src="/images/menus/search2.svg" style="height:18px;width:18px; cursor:pointer;"></div>'
  +'</div>'
  +"<div class=\"aTList\" style='margin-top:5px;height:190px;overflow-y:auto;overflow-x: hidden;width: 290px;'>"
  for(i=0;i<jsonData.length;i++){
    // var selecteduserId = jsonData[i].user_id
      let imgName = lighttpdpath+"/userimages/"+jsonData[i].user_image+"?"+d.getTime();
    if($(".userDetails").find("#user_id_"+jsonData[i].user_id).length==0 && $(".userDetails1").find("#user_id1_"+jsonData[i].user_id).length==0){
      var username=jsonData[i].user_full_name;
      UI+="<div id='taskassignee_"+jsonData[i].user_id+"' class=\"participant  align-items-center\" style ='display:flex; padding:10px 5px;' onclick=\"addUserToData('" + prjid + "','" + username + "','" + jsonData[i].user_id + "',this,'"+imgName+"')\">"
      if(jsonData[i].user_id == userIdglb){
        UI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);"  title="'+jsonData[i].user_full_name+'" src="'+imgName+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle" style="width:30px;height:30px;">'
      }else{
         UI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);"  title="'+jsonData[i].user_full_name+'" src="'+imgName+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle cursor" style="width:30px;height:30px;">'
      }
       UI+= '<div class="names defaultExceedCls" style="font-size: 12px;" title="'+ jsonData[i].user_full_name+'">'
        + jsonData[i].user_full_name
        +'</div>'
              + "</div > ";
    }
  }
   UI+="</div>" 
  UI+="</div>"
  +"</div>"
  return UI;
}
function prepareUIforspecialcharacterNewforTask1(jsonData, prjid, type) {
  $('#tag').remove();
  var sessionuserId = userIdglb;
  var UI="";
  UI = "<div class=\"atTag col-12 d-flex  position-absolute \" id='tag' style='z-index:2;font-size:12px;'>"
    +'<div class="border border-secondary py-3 pl-3 pr-2 touchablePop atListPop position-absolute rounded" style="background-color:white;margin-top: -2px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;">'
  +'<div class="d-flex  w-100 justify-content-between">'
  +"<div class=\" pb-1 textinvite\">Select participants</div>"
  +'<div class=" d-flex searchclass justify-content-between" onclick="userSearch();"><input type="text" onkeyup="searchUser(this);" id="invite" placeholder="" class="projInviteUser border-0" style=" border-bottom: 1px solid rgb(108, 117, 125) !important; display: none;width: 90%;"><img  src="/images/menus/search2.svg" style="height:18px;width:18px; cursor:pointer;"></div>'
  +'</div>'
  +"<div class=\"aTList\" style='margin-top:5px;height:190px;overflow-y:auto;overflow-x: hidden;width: 290px;'>"
  for(i=0;i<jsonData.length;i++){
    // var selecteduserId = jsonData[i].user_id
      let imgName = lighttpdpath+"/userimages/"+jsonData[i].user_image+"?"+d.getTime();
    if($(".userDetails1").find("#user_id1_"+jsonData[i].user_id).length==0 && $(".userDetails").find("#user_id_"+jsonData[i].user_id).length==0){
      var username=jsonData[i].user_full_name;
      UI+="<div class=\"participant  align-items-center\" style ='display:flex; padding:10px 5px;' onclick=\"addUserToData1('" + prjid + "','" + username + "','" + jsonData[i].user_id + "',this,'"+imgName+"')\">"
      if(jsonData[i].user_id == userIdglb){
        UI+='<img id="personimagedisplay"  onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);"  src="'+imgName+'"  title="'+jsonData[i].user_full_name+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle" style="width:30px;height:30px;">'
      }else{
        UI+='<img id="personimagedisplay"  onclick="event.stopPropagation();getNewConvId('+jsonData[i].user_id+', this);"  src="'+imgName+'"  title="'+jsonData[i].user_full_name+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle cursor" style="width:30px;height:30px;">'
      }
      UI+= '<div class="names defaultExceedCls" style="font-size: 12px;" title="'+ jsonData[i].user_full_name+'">'
        + jsonData[i].user_full_name
        +'</div>'
              + "</div > ";
    }
  }
   UI+="</div>" 
  UI+="</div>"
  +"</div>"
  return UI;
}
function replaceSaveWithDueDate(){
  let UI="";
  const d = new Date();
  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let nowDate=(monthNames[parseInt(d.getMonth())+1])	+" "+(parseInt(d.getDate())<10?"0"+d.getDate():d.getDate())+", "+d.getFullYear()	;	
  let amPm=parseInt(d.getHours())>11?"PM":"AM";	
  let nowTime=(parseInt(d.getHours())>12?((parseInt(d.getHours())-12)<10?("0"+parseInt(d.getHours())-12):(parseInt(d.getHours())-12)):(parseInt(d.getHours())<10?"0"+d.getHours():d.getHours()))+":"+(parseInt(d.getMinutes())<10?"0"+d.getMinutes():d.getMinutes())+" "+amPm;
  UI+=
        '<div class="">'
        +'<div><span style="float: left;color:black;min-width:28px;">Start :</span><span id="startofTask" value="" style="padding-left: 8px;border: none;cursor:pointer;color:#747778" onclick="DisplayOfStart();">'+nowDate+' '+ nowTime+'</span></div>'
        +'<div><span style="float: left;color:black;min-width:28px;">dUE :</span><span id="endofTask" value="" style="padding-left: 12px;border: none;cursor:pointer;color:#747778;margin-left:1px" onclick="DisplayOfStart();">'+nowDate+' '+ nowTime+'</span></div>'
      +'</div>'
      + '<div class="">'
        +'<div><span style="float: left;color:black;min-width:28px;">Estimated :</span><span id="estimated" value="" style="padding-left: 8px;border: none;cursor:pointer;color:#747778" onclick="DisplayOfStart();">0 h 0m</span></div>'
        +'<div><span style="float: left;color:black;min-width:28px;">Priority :</span><span id="" value="" style="padding-left: 8px;border: none;cursor:pointer;" onclick="DisplayOfStart();"><img id="priorityHrOfTask" onclick="DisplayOfStart();" style="margin-left: 29px;float: left;cursor:pointer;width: 18px;margin-top: 0px;" title="" src="images/task/p-six.svg" class="img-responsive"/></span></div>'

      +'</div>'

  
  $("#temporarySaveDiv").html(UI);
  $("#temporarySaveDiv").removeClass('justify-content-start').addClass('justify-content-around');
  $(".savecancelDiv").show('slow');         
  if($(".deadlinepopup").length>0){
    $("#startofTask").text(($("#startDateCalNewUI").text())+" "+($("#startTimeCalNewUI").text().split(":")[0]).toString().trim()+":"+($("#startTimeCalNewUI").text().split(":")[1]).toString().trim()+ ($("#startTimeCalNewUI").text().substring($("#startTimeCalNewUI").text().length-2,$("#startTimeCalNewUI").text().length)));

    $("#endofTask").text(($("#endDateCalNewUI").text())+" "+($("#endTimeCalNewUI").text().split(":")[0]).toString().trim()+":"+($("#endTimeCalNewUI").text().split(":")[1]).toString().trim()+ ($("#endTimeCalNewUI").text().substring($("#endTimeCalNewUI").text().length-2,$("#endTimeCalNewUI").text().length)));
    
    let estimateTime=$("#estimateHour").text()+" h "+$("#estimateMinute").text()+" m"
    $("#estimated").text(estimateTime);


    let Priority=$("#priority option:selected").val();
    Priority = Priority==0?"6":Priority;
    var Priority_image = (Priority == "")? "" : Priority == "1" ? "images/task/p-one.svg" : Priority == "2" ? "images/task/p-two.svg":Priority == "3" ? "images/task/p-three.svg" :Priority == "4" ? "images/task/p-four.svg":Priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
    $("#priorityHrOfTask").attr('src',Priority_image) ;
  }
}
function addUserToData(prjid, userName, userid, obj,imageSource,type,taskId,elapsedTime) {
  type=typeof(type)=='undefined'?"":type;
  let UserUI = "";
 // let imageSource=$(obj).find('img').attr('src');
  let disable=(userid==userIdglb)?"":"disabled='disabled'";
  let disable1=(type=="")?"":"disabled='disabled'"

  let colorof=disable!=""?"color:darkgray":"color:black"
  let colorof1=type!=""?"color:darkgray":"color:black"
  if(typeof(elapsedTime)=='undefined'){
    elapsedTime="-";
  }else if(elapsedTime==""){
    elapsedTime="-";
  }

  let progressOnclick="";
  if(userid==userIdglb){
    progressOnclick="setProgress(this);"
  }
  taskId=typeof(taskId)!='undefined'?taskId:"0";
  let slideClick="";

  $(".task_user_new_div1").hide();
  $(".task_user_new_div").show();
  if ($(".userTobeAddedDiv").length == 0) {
   UserUI += '<div class="row m-0 userTobeAddedDiv" style="font-size:12px">'
                //  + '<div class="row  p-1  mt-2 ml-2" style="background-color:darkgrey;width:98%">'
                //         +'<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 ">'
                //           + '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>'
                //           + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>'
                //           + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Progress</div>'
                //           + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Estimated Time</div>'
                //           + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Actual Time</div>'
  
                //         +'</div>'
                //    + '</div>'
               // +'<div class="w-100 ml-3" style="border-bottom:1px solid #e2e2e2;"></div>'
                
                UserUI+= '<div class="col-10 mb-1 pl-2 defaultScrollDiv" style="padding-right: 0px;">'
                                 +'<div  id="ignorediv" style="width:100%;box-shadow: rgb(0 0 0 / 18%) 0px 2px 6px;                                 height: 28px;border:1px solid darkgray"  >'


                                  + '<div class="row  py-1 pl-2 pr-1    ml-1 pl" style="width:100%;">'
                                  // +'<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 row">'background-color:darkgrey;
                                    // + '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="max-width: 13%;padding: 0px 0px 0px 10px;">Assignee</div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 d-flex" style=" max-width: 14%;padding-left: 6px;">Progress</div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding-left:22px; max-width: 13%;">Actual</div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style=" padding-left: 9px; max-width: 12%;">Estimate</div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding:0px;max-width: 12%;" >Total Actual</div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding:0px" >Elapsed</div>'
             
                                   //+'</div>'
                                  + '</div>'





                                 +'</div>'

                                 +'<div class="  userDetails defaultScrollDiv mt-1" style="background-color:#FFFFFF;width:100%;padding-right: 0px;padding-left:0px"> </div>'
                                
                
                          +'</div>'   
                UserUI+= '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 documentAdd p-0 " style="background-color:#FFFFFF;width:98%;border-left: 1px solid #DADBDB;">' 
                             
                                    + '<div class="documentDiv p-1">'
                                      +'<span class="pl-1" ><img src="/images/task/doceumnt.svg"  style="height:20px;width:20px"/></span>'
                                      + '<span class="pl-1" >Attachments</span>'
                                    + '</div>'
                                    + '<div class="userdocument p-1 defaultScrollDiv" style="max-height: 150px;overflow-y: auto;">'

                                    
                                + '</div>'
                          + '</div>'

                     +'</div>'
               
       //$(UserUI).insertBefore(".savecancelDiv");
       $(".task_user_new_div").append(UserUI);
    }
    UserUI = "";
    let oc="";
    UserUI+='<div class="row  px-1 pt-3 pb-1 mt-0 ml-1 usercount " id="user_id_'+userid+'" userStatus="N" onmouseover="useroptionUI('+userid+')" style="cursor:pointer;" onmouseleave="removeHover('+userid+');" onclick="colorthisdiv(this);" ondblclick="getTaskUserComment('+taskId+','+userid+',\'\');event.stopPropagation();" >'

               + '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="max-width:5%" >'
                      +'<div class="d-flex" >'
                      if(userid == userIdglb){
                        UserUI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+userid+', this);"  src="'+imageSource+'"  title="'+userName+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle " style="width:30px;height:30px; margin-top: -5px;">'
                      }else{
                        UserUI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+userid+', this);"  src="'+imageSource+'"  title="'+userName+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle cursor" style="width:30px;height:30px; margin-top: -5px;">'
                      }
                      UserUI+='</div>'
                    // +'<div class=" defaultExceedCls mt-1" title="'+userName+'">'+userName+'</div>'
               +'</div>'

               + '<div align="center" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pr-2" style="padding:0px;max-width:9%">'
                    +'<img id="persontaskstatusdisplay_'+userid+'" src="images/task/user-start.svg" style="width:18px;cursor:pointer;    margin-top: -3px;">'
               +'</div>'
               
               + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 d-flex flex-column" style="padding:0px;max-width:14%">'
                  +'<input   id="slide_'+userid+'" '+disable+' class="sliderange mt-2 mr-2" type="range" min="0" max="100" onmouseup="changeSliderInputvalue(this,\'edit\');" onmousemove="slidercolor1(\''+userid+'\',this);" style="color: black; width: 84%; background: linear-gradient(90deg, rgb(165, 235, 45) 2%, rgb(0,0,0) 2%);padding-left: 0px !important;z-index:1" value="0" onchange="changePicofInprogress();">'
                  //  if(userid==userIdglb){
                  //    UserUI+='<input type="text" id="taskSlideAmount_'+userid+'"  class="userslide" onblur="changeSliforUpdate(this,\''+type+'\');" permanentvalue="0" value="0" class="ml-1" style="border:none;height:20px;outline:0; width:24px;text-align: right;color:black;border-bottom:1px solid  #c1c5c8;background:white;" >'
                
                  //  }else{
                  //    UserUI+='<input id="taskSlideAmount_'+userid+'" class="userslide" onblur="changeSliforUpdate(this,\''+type+'\');"  disabled="disabled" class="ml-1" value="0" style="border:none;height:20px;outline:0; width:24px;text-align:center;color:darkgray;border-bottom:1px solid  #c1c5c8;text-align: right;background:white;"/>'
                  //  }
                //  UserUI+='<div class="percent" style="margin-left: 3px;padding-top: 1px;'+colorof+'" >%</div>'
                +'<div class="d-flex" style="width: 90%;height: 4px;margin-top: 3px;z-index:0">'  
                       +'<div class="d-flex"  style="width: 25%;height:100%;" ><div title="0%" perval="0" onclick="'+progressOnclick+'" style="width: 31%;height:100%;border-left: 1px solid darkgray;cursor:pointer;    margin-left: 6px;"></div></div>'
                       +'<div class="d-flex"  style="width: 25%;height:100%;" ><div title="25%" perval="25" onclick="'+progressOnclick+'" style="width:31%;height:100%;border-left: 1px solid darkgray;cursor:pointer;    margin-left: 2px;"></div></div>'
                       +'<div  class="d-flex" style="width: 25%;height:100%;" ><div title="50%" perval="50" onclick="'+progressOnclick+'" style="width: 31%;height:100%;border-left: 1px solid darkgray;cursor:pointer;"></div></div>'
                       +'<div  class="d-flex" style="width: 25%;height:100%;" ><div title="75%" perval="75" onclick="'+progressOnclick+'" style="width: 31%;height:100%;border-left: 1px solid darkgray;cursor:pointer;margin-left: -3px;"></div></div>'
                       +'<div  class="d-flex" style="width: 10px;height:100%;" ><div title="100%" perval="100" onclick="'+progressOnclick+'" style="width: 100%;height:100%;border-left: 1px solid darkgray;cursor:pointer;    margin-left: -5px;"></div></div>'
                +'</div>'
                
                +'<div class="d-flex justify-content-center mt-2" style="width:84%">'        
                  if(userid==userIdglb){
                    UserUI+='<input type="text" id="taskSlideAmount_'+userid+'"  class="userslide" onblur="changeSliforUpdate(this,\''+type+'\');" permanentvalue="0" value="0" class="ml-1" style="border:none;height:20px;outline:0; width:24px;text-align: right;color:black;border-bottom:1px solid  #c1c5c8;background:white;" >'
                                    
                  }else{
                      UserUI+='<input id="taskSlideAmount_'+userid+'" class="userslide" onblur="changeSliforUpdate(this,\''+type+'\');"  disabled="disabled" class="ml-1" value="0" style="border:none;height:20px;outline:0; width:24px;text-align:center;color:darkgray;border-bottom:1px solid  #c1c5c8;text-align: right;background:white;"/>'
                  }
                  UserUI+='<div class="percent" style="margin-left: 3px;padding-top: 1px;'+colorof+'" >%</div>'
                +'</div>'

              +'</div>'

         + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 " style="padding-left:13px;max-width:12%">'
               +'<input type="text" '+oc+' '+disable+' class="workinghour" permanentvalue="0" value="0" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;background:white;width:23px;text-align: right;outline: 0;border-width: 0 0 1px;'+colorof+'" ><div class="hour1 ml-1" style="float:left;color:black;width:16px;margin-top: 1px;'+colorof+'">h</div><input  permanentvalue="0" '+oc+'  '+disable+' class="workingMinutes" type="text"  value="0" style="border-bottom:1px solid  #c1c5c8;background:white;height:20px; float:left;width:18px;text-align: right;outline: 0;border-width: 0 0 1px;'+colorof+'" onkeyup="validateMinute(this)"><div class="min1 ml-1 " style="float:left;color:black;margin-top:1px;'+colorof+'">m</div>'
         +'</div>'// comment actual



         + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 " style="padding-left:8px;max-width:12%">'
               +'<input type="text" '+oc+' '+disable1+' class="estimateHour" onblur="sumUptoEstimate('+userid+');" value="0" style="border-bottom:1px solid  #c1c5c8;height:20px;background:white; float:left;width:23px;text-align: right;outline: 0;border-width: 0 0 1px;'+colorof1+'" ><div class="hour ml-1" style="float:left;width:16px;margin-top: 1px;'+colorof1+'">h</div><input '+oc+' '+disable1+' onkeyup="validateMinute(this)"  onblur="sumUptoEstimate('+userid+');"  class="estimateMin" type="text"  value="0" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;background:white;width:18px;text-align: right;outline: 0;border-width: 0 0 1px;'+colorof1+'"><div class="min ml-1 " style="float:left;margin-top:1px;'+colorof1+'">m</div>'
         +'</div>'
         + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  actualhour " style="color:darkgray;padding:0px;max-width: 12%;" >'

         +'<input type="text" disabled="disabled" value="0" class="acthour" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:23px;text-align: right;outline: 0;border-width: 0 0 1px;background:white;color:darkgray"><div class=" ml-1" style="float:left;width:16px;margin-top: 1px;color:darkgray">h</div><input disabled="disabled"  type="text" value="0" class="actmin"  style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:18px;text-align: right;outline: 0;border-width: 0 0 1px;color:darkgray;background:white;"><div class=" ml-1 " style="float:left;margin-top:1px;color:darkgray">m</div>'
         
         
         
         +'</div>'
         + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  elapsedTime " style="color:darkgray;padding:0px;max-width: 12%;" >'+elapsedTime+'</div>'
         + '<div class="position-relative more"  style="cursor:pointer; margin-left: auto !important;" >'
         if(userid==userIdglb){
           UserUI+='<input id="uploadForTaskuser" type="file" onchange="uploadFileForTask(this,'+taskId+',\'TaskFileUpload\');" class="d-none"/>'
         }
         UserUI+='<div id="subcommentDiv1_'+userid+'" class="actFeedOptionsDiv " style="border-radius: 8px; top: -8px; margin-right: 20px; display: none;">' 
         //+'<div class="arrow-right"></div>/images/calender/DocumentBlack.png
         UserUI+='<div class="d-flex align-items-center">'
               if(type!=''){
                
                       UserUI+='<img class="mx-1" title="Details" id="expand" src="images/task/expand.svg" onclick="getTaskUserComment('+taskId+','+userid+',\'\',\'assign\');event.stopPropagation();"style="width:18px;height:18px"/>'

                       
                         if(userid==userIdglb){
                           UserUI+='<img class="mx-1" title="Upload" id="userupload" src="/images/task/attach.svg" style="width:18px;height:18px"/>'
                         }
                         //UserUI+=  '<img class="mr-2" title="Comments and Upload" src="/images/task/Comments_cir.svg" onclick="getTaskUserComment('+taskId+','+userid+',\'\');event.stopPropagation();" style="width:20px;height:20px"/>'
                                 // +'<img  src="/images/calender_old/DocumentBlack.png" onclick="fetchUserLevelDoc(\'\','+taskId+','+userid+')" style="width:20px;height:20px"/>'
                     
                       }
                       UserUI+='<img class="mx-1" onclick="editFunction('+userid+',\'on\');event.stopPropagation();" title="Edit" id="edit" src="images/conversation/edit.svg" onclick="getTaskUserComment('+taskId+','+userid+',\'\',\'assign\');event.stopPropagation();" style="width:18px;height:18px"/>'
                       UserUI+='<img class="mx-1" title="Delete" onclick="removeUserTask('+userid+',\''+type+'\');event.stopPropagation();" src="/images/task/minus.svg" id="user_delete" style="width:18px;height:18px;"/>'
                       UserUI+='</div>'
                 UserUI+='</div>'
          
           +'</div>'
           /*           
           +'<div class="w-100 pr-4 d-flex mb-1">'
              +'<div style="width:10%"></div>'
              +'<div class="d-flex" style="width:14%">'  
                       +'<div  perval="0" style="width: 19%;border-left: 1px solid darkgray;min-height: 10px;cursor:pointer;" title="0%" onclick="'+progressOnclick+'"></div>'
                       +'<div perval="25" style="width: 19%;border-left: 1px solid darkgray;min-height: 10px;cursor:pointer;" title="25%" onclick="'+progressOnclick+'"></div>'
                       +'<div  perval="50" style="width: 19%;border-left: 1px solid darkgray;min-height: 10px;cursor:pointer;" title="50%" onclick="'+progressOnclick+'"></div>'
                       +'<div  perval="75" style="width: 19%;border-left: 1px solid darkgray;min-height: 10px;cursor:pointer;" title="75%" onclick="'+progressOnclick+'"></div>'
                       +'<div  perval="100" style="width: 25%;border-left: 1px solid darkgray;min-height: 10px;cursor:pointer;" title="100%" onclick="'+progressOnclick+'"></div>'
              +'</div>'        




           +'</div>'
           */           
           /*           
          +'<div class="w-100 pr-4 d-flex">'
              +'<div style="width:8%"></div>'
              +'<div class="d-flex justify-content-center" style="width:13%">'        
              if(userid==userIdglb){
                 UserUI+='<input type="text" id="taskSlideAmount_'+userid+'"  class="userslide" onblur="changeSliforUpdate(this,\''+type+'\');" permanentvalue="0" value="0" class="ml-1" style="border:none;height:20px;outline:0; width:24px;text-align: right;color:black;border-bottom:1px solid  #c1c5c8;background:white;" >'
                                
              }else{
                  UserUI+='<input id="taskSlideAmount_'+userid+'" class="userslide" onblur="changeSliforUpdate(this,\''+type+'\');"  disabled="disabled" class="ml-1" value="0" style="border:none;height:20px;outline:0; width:24px;text-align:center;color:darkgray;border-bottom:1px solid  #c1c5c8;text-align: right;background:white;"/>'
              }
              UserUI+='<div class="percent" style="margin-left: 3px;padding-top: 1px;'+colorof+'" >%</div>'
              UserUI+='</div>'


          +'</div>'
            */

           UserUI+='<div class="userselect w-100 pr-4"></div>'
           UserUI+='<div class="col-lg-12  mt-1  mb-1 " title="'+userName+'">'+userName+'</div>'
           UserUI+='<div class="usercontainer w-100 pr-4"  ondblclick="event.stopPropagation();"></div>'
           UserUI+='</div>'
          
           
     //   +'</div>'

    
     //  UserUI+='</div>'

    $(".userDetails").append(UserUI);
    
    if(userid==userIdglb){
      $("#userupload").on('click',()=>{
       
          document.getElementById('uploadForTaskuser').click();
      });
      if($("#task_id").val()!=''){
        editSelect(userid,taskId)
      }
    }
    if(obj!=''){

    $(obj).fadeOut(10, function(){ $(this).remove();});
    $(".participant  *").filter(function() {
    //	console.log(value + "-----" + $(this).find(".names").text());
      $(this).parent().toggle($(this).text().toLowerCase().indexOf("") > -1);
    });
  }  
  if(type==''){
    distributeEstimateTime();
  }
  changeImage('user')

  
  
}

function addUserToData1(prjid, userName, userid, obj,imageSource,type,taskId,elapsedTime,nowDate,approver_date,comment,appStatus,sequence,appStatus1,nowTime,approver_time) {
  // console.log(comment);
  // console.log(appStatus);
  // console.log("sequence--->"+sequence);
  type=typeof(type)=='undefined'?"":type;
  let UserUI = "";
  let apprImgPath="/images/task/pending_approval.svg";
  let appTitle="Pending approval";

  

  if(appStatus1=="Pending approval"){
    apprImgPath="/images/task/pending_approval.svg";
    appTitle="Pending approval";
  }else if(appStatus1=="Approved"){
    apprImgPath="/images/task/approved.svg";
    appTitle="Approved";
  }else if(appStatus1=="Rejected"){
    apprImgPath="/images/task/rejected.svg";
    appTitle="Rejected";
  }else if(appStatus1=="On hold"){
    apprImgPath="/images/task/on_hold.svg";
    appTitle="On hold";
  }else if(appStatus1=="Cancel"){
    apprImgPath="/images/task/cancel.svg";
    appTitle="Cancel";
  }else{
    apprImgPath="/images/task/pending_approval.svg";
    appTitle="Pending approval";
  }
 // let imageSource=$(obj).find('img').attr('src');
  let disable=(userid==userIdglb)?"":"disabled='disabled'";
  let disable1=(type=="")?"":"disabled='disabled'"

  let colorof=disable!=""?"color:darkgray":"color:black"
  let colorof1=type!=""?"color:darkgray":"color:black"
  if(typeof(elapsedTime)=='undefined'){
    elapsedTime="-";
  }else if(elapsedTime==""){
    elapsedTime="-";
  }
  

  if(typeof(appStatus)=='undefined'){
    appStatus="N";
  }else if(appStatus==""){
    appStatus="N";
  }

  if(typeof(approver_date)=='undefined'){
    approver_date="-";
  }else if(approver_date==""){
    approver_date="-";
  }

  if(typeof(approver_time)=='undefined'){
    approver_time="";
  }else if(approver_time==""){
    approver_time="";
  }

  if(typeof(comment)=='undefined'){
    comment="";
  }else if(comment==""){
    comment="";
  }

  let progressOnclick="";
  if(userid==userIdglb){
    progressOnclick="setProgress(this);"
  }
  taskId=typeof(taskId)!='undefined'?taskId:"0";
  let slideClick="";

  if(typeof(nowDate)=='undefined' && typeof(nowTime)=='undefined'){
    const d = new Date();
    var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    nowDate=(monthNames[parseInt(d.getMonth())+1])	+" "+(parseInt(d.getDate())<10?"0"+d.getDate():d.getDate())+", "+d.getFullYear()	;	
    let amPm=parseInt(d.getHours())>11?"PM":"AM";	
    nowTime=(parseInt(d.getHours())>12?((parseInt(d.getHours())-12)<10?("0"+(parseInt(d.getHours())-12)):(parseInt(d.getHours())-12)):(parseInt(d.getHours())<10?"0"+d.getHours():d.getHours()))+":"+(parseInt(d.getMinutes())<10?"0"+d.getMinutes():d.getMinutes())+" "+amPm;
  }else if(nowDate=="" && nowTime==""){
    const d = new Date();
    var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    nowDate=(monthNames[parseInt(d.getMonth())+1])	+" "+(parseInt(d.getDate())<10?"0"+d.getDate():d.getDate())+", "+d.getFullYear()	;	
    let amPm=parseInt(d.getHours())>11?"PM":"AM";	
    nowTime=(parseInt(d.getHours())>12?((parseInt(d.getHours())-12)<10?("0"+(parseInt(d.getHours())-12)):(parseInt(d.getHours())-12)):(parseInt(d.getHours())<10?"0"+d.getHours():d.getHours()))+":"+(parseInt(d.getMinutes())<10?"0"+d.getMinutes():d.getMinutes())+" "+amPm;
  }

  

  // console.log(nowDate+"---"+nowTime);

  // if ($(".userTobeAddedDiv").length == 0) {
    $(".task_user_new_div").hide();
    $(".task_user_new_div1").show();
    // $(".documentAdd").hide();
  // }


  if ($(".userTobeAddedDiv1").length == 0) {
   UserUI += '<div class="row m-0 userTobeAddedDiv1" style="font-size:12px">'
                //  + '<div class="row  p-1  mt-2 ml-2" style="background-color:darkgrey;width:98%">'
                //         +'<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 ">'
                //           + '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>'
                //           + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>'
                //           + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Progress</div>'
                //           + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">Estimated Time</div>'
                //           + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Actual Time</div>'
  
                //         +'</div>'
                //    + '</div>'
               // +'<div class="w-100 ml-3" style="border-bottom:1px solid #e2e2e2;"></div>'
                
                UserUI+= '<div class="col-10 mb-1 pl-2 defaultScrollDiv" style="padding-right: 0px;">'
                                 +'<div  id="ignorediv" style="width:100%;box-shadow: rgb(0 0 0 / 18%) 0px 2px 6px;                                 height: 28px;border:1px solid darkgray"  >'


                                  + '<div class="row  py-1 pl-2 pr-1    ml-1 pl" style="width:100%;">'
                                  // +'<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 row">'background-color:darkgrey;
                                    // + '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="max-width: 14%;padding: 0px 0px 0px 10px;">Approver</div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 d-flex" style=" max-width: 11%;padding-left: 6px;">Status</div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding-left:22px; max-width: 20%;">Request Date</div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style=" padding-left: 42px; max-width: 19%;">Approval Date</div>'
                                     + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 ml-4" style="padding-left: 42px;max-width: 12%;" >Elapsed</div>'
                                    //  + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 ml-5" style="padding:0px" >Comments</div>'
             
                                   //+'</div>'
                                  + '</div>'





                                 +'</div>'

                                 +'<div class="  userDetails1 defaultScrollDiv mt-1" style="background-color:#FFFFFF;width:100%;padding-right: 0px;padding-left:0px"> </div>'
                                
                
                          +'</div>'   
                UserUI+= '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 documentAdd p-0 " style="background-color:#FFFFFF;width:98%;border-left: 1px solid #DADBDB;">' 
                             
                                    + '<div class="documentDiv p-1">'
                                      +'<span class="pl-1" ><img src="/images/task/doceumnt.svg"  style="height:20px;width:20px"/></span>'
                                      + '<span class="pl-1" >Attachments</span>'
                                    + '</div>'
                                    + '<div class="userdocument p-1 defaultScrollDiv" style="max-height: 150px;overflow-y: auto;">'

                                    
                                + '</div>'
                          + '</div>'

                     +'</div>'
               
       //$(UserUI).insertBefore(".savecancelDiv");
       $(".task_user_new_div1").append(UserUI);
    }
    var secuenceNumber="";
    // if()
    // if($('.usercount').length !=0){
    //   var all=$('div.usercount:last').attr('secuenceNumber');
    //   var count =parseInt(all) + 1;
    //   secuenceNumber=count;
    // }else{
    //   secuenceNumber=1;
    // }

    if(type=='edit'){
      secuenceNumber=sequence;
    }else{

      if($('.usercount').length !=0){
        var all=$('div.usercount:last').attr('secuenceNumber');
        var count =parseInt(all) + 1;
        secuenceNumber=count;
      }else{
        secuenceNumber=1;
      }
      console.log(secuenceNumber);
    }
    UserUI = "";
    let oc="";
    UserUI+='<div class="row  px-1 pt-3 pb-1 mt-0 ml-1 usercount " secuenceNumber="'+secuenceNumber+'" id="user_id1_'+userid+'" appStatus="'+appStatus+'" userStatus="N" onmouseover="useroptionUI('+userid+')" style="cursor:pointer;" onmouseleave="removeHover('+userid+');" onclick="colorthisdiv(this);" >'

               + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="max-width:14%" >'
                      +'<div class="d-flex" >'
                      if(userid == userIdglb){
                        UserUI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+userid+', this);"  src="'+imageSource+'"  title="'+userName+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle" style="width:30px;height:30px; margin-top: -5px;">'
                      }else{
                        UserUI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+userid+', this);"  src="'+imageSource+'"  title="'+userName+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle cursor" style="width:30px;height:30px; margin-top: -5px;">'
                      }
                      UserUI+='</div>'
                    // +'<div class=" defaultExceedCls mt-1" title="'+userName+'">'+userName+'</div>'
               +'</div>'

               + '<div align="center" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pr-2" style="max-width:7%">'
                +'<div class="d-flex" style="position: relative;">'
                  +'<img id="approveStatusImg_'+userid+'" src="'+apprImgPath+'" title="'+appTitle+'" onclick="approveOptionList('+userid+',\'edit\');" class="mr-3  appIgm" style="width:22px;height:22px;">'
                  +'<div class="position-absolute rounded p-2 border statusListNew" id="apprList_'+userid+'" style="background-color: rgb(255, 255, 255); color: black; font-size: 12px; box-shadow: rgba(0, 0, 0, 0.18) 0px 6px 12px; width: 151px; display: none;top:-5px;left:32px;z-index: 1000;height: 130px;"></div>'
                +'</div>'
               +'</div>'
               
               + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding-left: 62px;max-width:20%">'
                  // +'<div class="defaultExceedCls" style="" id="requestDate" >'+request_date+'</div>'
                  +'<div class="requestDateDiv d-flex">'
                      +'<div class="d-flex" style="margin-left:3px;color:black;border-bottom:1px solid darkgray;white-space: nowrap;">'
                        +'<div id="resquestDateCalNewUI_'+userid+'" parmanentvalue="'+nowDate+'" style=" width: 72px">'+nowDate+'</div>'
                        +'<div id="resquestTimeCalNewUI_'+userid+'"  parmanentvalue="'+nowTime+'" style=" width: 52px;margin-left:3px;">'+nowTime+'</div>'
                      +'</div>'
                      +'<div id="calenderForTask2_'+userid+'"   onclick="showCalender1(this);" style="margin-left:7px;margin-top: -2px;cursor:pointer">'
                        +'<img src="images/landingpage/cal.svg" style="width:15px" />'
                      +'</div>'
                  +'</div>'
                  +'<div class="position-absolute p-1 time_calender1 border border-secondary" id="time_calender1_'+userid+'" style=" border:1px solid black;z-index:2 ;                            background:white;font-size:14px;color:#64696F;display:none;    margin-top: 28px;max-width:412px;width:305px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;" >'
                         +'<div class=" mt-1 " >'
                +'<div class="d-flex justify-content-around ml-1" style="max-width: 281px;">'      //Time Div Starts
                      +'<div class="mt-2"> Time </div>'
                        +'<div>'
                          +' <div><img class="ml-1" src="/images/arrowUpNew.png" class="removeonClickBtnIcon" onclick="increaseValue(\'hour\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                          +' <div style="margin-left:2px" id="hour">10</div>'
                          +' <div><img class="ml-1" src="/images/arrowDownNew.png" class="removeonClickBtnIcon" onclick="decreaseValue(\'hour\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                          
                        +' </div>'
                        +'<div>'
                          +' <div><img class="ml-1" src="/images/arrowUpNew.png" class="removeonClickBtnIcon"  onclick="increaseValue(\'minute\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                          +' <div style="margin-left:2px" id="minute">10</div>'
                          +' <div><img class="ml-1" src="/images/arrowDownNew.png" class="removeonClickBtnIcon" onclick="decreaseValue(\'minute\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                          
                        +' </div>'
                        +'<div>'
                          +' <div><img class="ml-1" src="/images/arrowUpNew.png" class="removeonClickBtnIcon"  onclick="increaseValue(\'second\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                          +' <div style="margin-left:2px" id="second">10</div>'
                          +' <div><img class="ml-1" src="/images/arrowDownNew.png" class="removeonClickBtnIcon" onclick="decreaseValue(\'second\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                          
                        +' </div>'
                          +'<div class="d-flex mt-2 justify-content-around mainampm" style="border:1px solid rgb(111, 158, 176);height:22px;width:50px;cursor:pointer" onclick="changeAmPm(this);">'
                              +'<div class="rounded-circle colorDiv " style="height:18px;width:18px;background-color:rgb(111, 158, 176);margin-top:1px"></div>'
                              +'<div class="ampm" >AM</div>'
                          +' </div>'       
                        +'</div>' //Time div ends
        
                        +'<div id="calender_task2_'+userid+'" class="mt-2  " style="height:265px;margin-left: 23px;"> </div>'
                    //Calender Div 
                    +'</div>'

                +'</div>'
               +'</div>'

              + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 ml-5 " style="max-width:19%;padding-left: 0px;">'
                +'<div class="d-flex" style="" id="approveDate" >'
                  +'<div id="approvDateCalNewUI_'+userid+'" style="">'+approver_date+'</div>'
                  +'<div id="approvTimeCalNewUI_'+userid+'"  style="margin-left:3px;">'+approver_time+'</div>'
                +'</div>'
              +'</div>'

              + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 ml-4  elapsedTime " style="color:darkgray;padding:0px;max-width: 12%;" >'+elapsedTime+'</div>'

              
        
              UserUI+='<div class="position-relative more" style="cursor:pointer; margin-left: auto !important;">'
              if(userid==userIdglb){
                UserUI+='<input id="uploadForTaskuser1" type="file" onchange="uploadFileForTask(this,'+taskId+',\'TaskFileUpload\');" class="d-none"/>'
              }
              UserUI+='<div id="subcommentDiv11_'+userid+'" class="actFeedOptionsDiv " style="border-radius: 8px; top: -8px; margin-right: 20px; display: none;">' 
          
              UserUI+=showOptionnew(userid,taskId,type);
             //  UserUI+='<div class="d-flex align-items-center">'
             //        if(type!=''){
                     
             //                UserUI+='<img class="mx-1" title="Details" id="expand" src="images/task/expand.svg" onclick="getTaskUserComment('+taskId+','+userid+',\'\');event.stopPropagation();"style="width:18px;height:18px"/>'
     
                            
             //                  if(userid==userIdglb){
             //                    UserUI+='<img class="mx-1" title="Upload" id="userupload" src="/images/task/attach.svg" style="width:18px;height:18px"/>'
             //                  }
                                                   
             //                }
             //                UserUI+='<img class="mx-1" onclick="editFunction('+userid+',\'on\');event.stopPropagation();" title="Edit" id="edit" src="images/conversation/edit.svg" onclick="getTaskUserComment('+taskId+','+userid+',\'\');event.stopPropagation();" style="width:18px;height:18px"/>'
             //                UserUI+='<img class="mx-1" title="Delete" onclick="removeUserTask('+userid+',\''+type+'\');event.stopPropagation();" src="/images/task/minus.svg" id="user_delete" style="width:18px;height:18px;"/>'
             //                UserUI+='</div>'
                      UserUI+='</div></div>'


          if(userid==userIdglb){
           UserUI+='<div class="userselect1 w-100 pr-4"><div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 p-0 mt-2" style="max-width: 99%;float: left;margin-left:13px;">'
                  UserUI+='<div class="material-textfield">'
                    UserUI+='<textarea id="UserCommentNewUI" type="text" class="theightdemo material-textfield-input" placeholder=" " style="padding-left: 10px; background: none;border: 1px solid #C1C5C8;border-radius: 6px;resize: none;height:30px;    outline: none; width: 100%;padding-top:6px"></textarea>'
                    UserUI+='<label class="material-textfield-label" style="font-size: 12px; top: 16px; background: rgb(238, 245, 247);">Comments</label>'
                  UserUI+='</div>'
           UserUI+='</div></div>'
          }   
           UserUI+='<div class="col-lg-12  mt-1  mb-1 " title="'+userName+'">'+userName+'</div>'
           UserUI+='<div class="usercontainer w-100 pr-4"  ondblclick="event.stopPropagation();"></div>'
           
            
             +'</div>'
           UserUI+='</div>'
          
           


    $(".userDetails1").append(UserUI);
    
    if(userid==userIdglb){
      $("#user_id1_"+userid).addClass("commentId");
      $("#userupload1").on('click',()=>{
       
          document.getElementById('uploadForTaskuser1').click();
      });
      if($("#task_id").val()!=''){
        editSelect(userid,taskId)
      }
    }else{
      $('#approveStatusImg_'+userid).removeAttr('onclick');
    }

    $(".cmntsApp").val(comment);
    if(obj!=''){

    $(obj).fadeOut(10, function(){ $(this).remove();});
    $(".participant  *").filter(function() {
    //	console.log(value + "-----" + $(this).find(".names").text());
      $(this).parent().toggle($(this).text().toLowerCase().indexOf("") > -1);
    });
  }  
  // if(type==''){
  //   distributeEstimateTime();
  // }
  changeImage('Approvers');
  

  
  
}

function approveOptionList(userId,place,taskid,projectId){
  UI="";
  if(place=="edit"){
    UI+='<div class="d-flex wsScrollBar" style="flex-direction: column;overflow-y: auto;">'

      +'<div class="d-flex pb-1" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/pending_approval.svg" title="Pending approval" class="mr-1" style="width:20px;height:20px;">'
        +'<div>Pending approval</div>'
      +'</div>'

      +'<div class="d-flex pb-1" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/approved.svg" title="Approved"  class="mr-1" style="width:20px;height:20px;">'
        +'<div>Approved</div>'
      +'</div>'

      +'<div class="d-flex pb-1" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/rejected.svg" title="Rejected"  class="mr-1" style="width:20px;height:20px;">'
        +'<div>Rejected</div>'
      +'</div>'

      +'<div class="d-flex pb-1" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/on_hold.svg" title="On hold"  class="mr-1" style="width:20px;height:20px;">'
        +'<div>On hold</div>'
      +'</div>'

      +'<div class="d-flex pb-1" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/cancel.svg" title="Cancel"  class="mr-1" style="width:20px;height:20px;">'
        +'<div>Cancel</div>'
      +'</div>'

    +'</div>'
  }else{
    UI+='<div class="d-flex wsScrollBar" style="flex-direction: column;overflow-y: auto;">'

      +'<div class="d-flex pb-1" id="pending_approval_'+userId+taskid+'" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/pending_approval.svg" title="Pending approval" class="mr-1" style="width:20px;height:20px;">'  
        +'<div>Pending approval</div>'
      +'</div>'

      +'<div class="d-flex pb-1" id="approved_'+userId+taskid+'" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/approved.svg" title="Approved"  class="mr-1" style="width:20px;height:20px;">'  
        +'<div>Approved</div>'
      +'</div>'

      +'<div class="d-flex pb-1" id="rejected_'+userId+taskid+'" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/rejected.svg" title="Rejected"  class="mr-1" style="width:20px;height:20px;">'
        +'<div>Rejected</div>'
      +'</div>'

      +'<div class="d-flex pb-1" id="on_hold_'+userId+taskid+'" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/on_hold.svg" title="On hold"  class="mr-1" style="width:20px;height:20px;">'  
        +'<div>On hold</div>'
      +'</div>'

      +'<div class="d-flex pb-1" id="cancel_'+userId+taskid+'" onclick="selectAppOptions(this,'+userId+',\''+place+'\','+taskid+','+projectId+');" style="cursor: pointer;">'
        +'<img id="approveStatusImg" src="/images/task/cancel.svg" title="Cancel"  class="mr-1" style="width:20px;height:20px;">'  
        +'<div>Cancel</div>'
      +'</div>'

    +'</div>'
  }

  
  if(place=="edit"){
    $('#apprList_'+userId).html(UI);
    $('#apprList_'+userId).show();
  }else{
    if(valueForView=="calendarTask"){
      $("#otherModuleTaskListDiv").find('#apprList_'+taskid).html(UI);
      $("#otherModuleTaskListDiv").find('#apprList_'+taskid).show();
    }else{
      $('#apprList_'+taskid).html(UI);
      $('#apprList_'+taskid).show();
    }
    
    var appNewStatus= $('#appNewImg_'+userId+taskid).attr('title');
    var left = $('#appNewImg_'+userId+taskid).position().left;
    var top = $('#appNewImg_'+userId+taskid).position().top;

    $('#appNewImg_'+userId+taskid).addClass('newList1');
    if(valueForView=="calendarTask"){
      $("#otherModuleTaskListDiv").find('#apprList_'+taskid).css('top','150px');
      $("#otherModuleTaskListDiv").find('#apprList_'+taskid).css('left','200px');
    }else{
      $('#apprList_'+taskid).css('top',top+22);
      $('#apprList_'+taskid).css('left',left-9);
    }
    
    console.log(left+"<----->"+top);
    if(appNewStatus=="Pending approval"){
      $('#pending_approval_'+userId+taskid).css('background','#eef5f7');
    }else if(appNewStatus=="Approved"){
      $('#approved_'+userId+taskid).css('background','#eef5f7');
    }else if(appNewStatus=="Rejected"){
      $('#rejected_'+userId+taskid).css('background','#eef5f7');
    }else if(appNewStatus=="On hold"){
      $('#on_hold_'+userId+taskid).css('background','#eef5f7');
    }else if(appNewStatus=="Cancel"){
      $('#cancel_'+userId+taskid).css('background','#eef5f7');
    }
    // $('#opentaskParticipantdiv_'+taskid).css('height','310px');
    // $('#mainScroll_'+taskid).css('height','255px');
  }

}

function selectAppOptions(obj,userId,place,taskId,projId){
  
  if(place=="edit"){
    var imgPath=$(obj).find('#approveStatusImg').attr('src');
    var imgTitel=$(obj).find('#approveStatusImg').attr('title');
    $('#approveStatusImg_'+userId).attr('src', imgPath);
    $('#approveStatusImg_'+userId).attr('title', imgTitel);
    if(imgTitel=="Pending approval"){
      $("#calenderForTask2_"+userId).attr("onclick", "showCalender1(this);");
      $("#UserCommentNewUI").val("");
    }else if(imgTitel=="Approved"){
      $("#calenderForTask2_"+userId).removeAttr("onclick");
      $("#UserCommentNewUI").val("Approved");
    }else if(imgTitel=="Rejected"){
      $("#calenderForTask2_"+userId).removeAttr("onclick");
      $("#UserCommentNewUI").val("Rejected");
    }else if(imgTitel=="On hold"){
      $("#calenderForTask2_"+userId).attr("onclick", "showCalender1(this);");
      $("#UserCommentNewUI").val("Approval is on hold");
    }else if(imgTitel=="Cancel"){
      $("#calenderForTask2_"+userId).removeAttr("onclick");
      $("#UserCommentNewUI").val("Request is cancelled");
    }
    $('#apprList_'+userId).hide();
  }else{
    // var userUncheck=true;
    var imgPath=$(obj).find('#approveStatusImg').attr('src');
    var imgTitel=$(obj).find('#approveStatusImg').attr('title');
    confirmFunNew("Do you want to update this action?","delete","changeAppStatusList",imgPath,imgTitel,userId,taskId,projId);
    
  }

}

function changeAppStatusList(imgPath,imgTitel,userId,taskId,projId){
  if(valueForView=="calendarTask"){
    $("#otherModuleTaskListDiv").find('#appNewImg_'+userId+taskId).attr('src', imgPath);
    $("#otherModuleTaskListDiv").find('#appNewImg_'+userId+taskId).attr('title', imgTitel);
    if(imgTitel=="Pending approval"){

    }else if(imgTitel=="Pending approval"){
      $("#otherModuleTaskListDiv").find('#appListCheckBox_'+userId+taskId).hide();
    }else if(imgTitel=="Approved"){
      $("#otherModuleTaskListDiv").find('#appListCheckBox_'+userId+taskId).show();
    }else if(imgTitel=="Rejected"){
      $("#otherModuleTaskListDiv").find('#appListCheckBox_'+userId+taskId).hide();
    }else if(imgTitel=="On hold"){
      $("#otherModuleTaskListDiv").find('#appListCheckBox_'+userId+taskId).hide();
    }else if(imgTitel=="Cancel"){
      $("#otherModuleTaskListDiv").find('#appListCheckBox_'+userId+taskId).hide();
    }
    $("#otherModuleTaskListDiv").find('#apprList_'+taskId).hide();
  }else{
    $('#appNewImg_'+userId+taskId).attr('src', imgPath);
    $('#appNewImg_'+userId+taskId).attr('title', imgTitel);
    if(imgTitel=="Pending approval"){

    }else if(imgTitel=="Pending approval"){
      $('#appListCheckBox_'+userId+taskId).hide();
    }else if(imgTitel=="Approved"){
      $('#appListCheckBox_'+userId+taskId).show();
    }else if(imgTitel=="Rejected"){
      $('#appListCheckBox_'+userId+taskId).hide();
    }else if(imgTitel=="On hold"){
      $('#appListCheckBox_'+userId+taskId).hide();
    }else if(imgTitel=="Cancel"){
      $('#appListCheckBox_'+userId+taskId).hide();
    }
    $('#apprList_'+taskId).hide();
    // $('#opentaskParticipantdiv_'+taskId).css('height','220px');
    // $('#mainScroll_'+taskId).css('height','160px')
  }  
    changeStatusOfTaskToProgress(taskId, projId, "", "", "");
    if(valueForView=="calendarTask"){
      $(".calendar-2").html("");
      $(".calendar-2").removeClass("elem-CalenStyle");
      calenTask('calendartask');
      closeCalenPopup();
    }
}

function showOptionnew(userid,taskId,type){
  UI="";

  UI+='<div class="d-flex align-items-center">'
                 if(type!=''){
                  
                  UI+='<img class="mx-1" title="Details" id="expand" src="images/task/expand.svg" onclick="getTaskUserComment('+taskId+','+userid+',\'\',\'approv\');event.stopPropagation();"style="width:18px;height:18px"/>'
  
                         
                           if(userid==userIdglb){
                            UI+='<img class="mx-1" title="Upload" id="userupload1" src="/images/task/attach.svg" style="width:18px;height:18px"/>'
                           }
                                                
                         }
                         UI+='<img class="mx-1" onclick="editFunction('+userid+',\'on\');event.stopPropagation();" title="Edit" id="edit" src="images/conversation/edit.svg" onclick="getTaskUserComment('+taskId+','+userid+',\'\',\'approv\');event.stopPropagation();" style="width:18px;height:18px"/>'
                         UI+='<img class="mx-1" title="Delete" onclick="removeUserTask('+userid+',\''+type+'\');event.stopPropagation();" src="/images/task/minus.svg" id="user_delete" style="width:18px;height:18px;"/>'
                         UI+='</div>'

    return UI;
}
 function useroptionUI(userid){
	$('#subcommentDiv1_'+userid).show();
  $('#subcommentDiv11_'+userid).show();
  //changeColor($("#user_id_"+userid),'on');
  colorthisdiv($("#user_id_"+userid));
  colorthisdiv($("#user_id1_"+userid));
  
 
}
function removeHover(user_id){
  
  $('#subcommentDiv1_'+user_id).hide();
  $('#subcommentDiv11_'+user_id).hide();
  $("#user_id_"+user_id).removeClass('taskcolordiv');
  $("#user_id1_"+user_id).removeClass('taskcolordiv');
  changeColor($("#user_id_"+user_id),'off');
  changeColor($("#user_id1_"+user_id),'off');
}


function calenderForTask() {
  if(globalmenu!="agile" && globalmenu!="sprint" && sprintTaskinTask!="sprintTaskinTask"){
    myCalendar = new dhtmlXCalendarObject("calender_task1");
    var today = new Date();
    myCalendar.setSensitiveRange(today, null);
    myCalendar.setDateFormat("%Y-%m-%d");
    myCalendar.hideTime();
    $('.dhtmlxcalendar_material').show();
    $('.dhtmlxcalendar_material').css({'z-index':'9000','border-radius':'5px','background-color':'#FFFFFF'});
    myCalendar.attachEvent("onClick",function(date){
      changeDatainReal('calender');
    });
  }else{
    myCalendar = new dhtmlXCalendarObject("calender_task1");
    myCalendar.setSensitiveRange(sprintStartdateGlb, sprintEnddateGlb);
    myCalendar.setDateFormat("%Y-%m-%d");
    myCalendar.hideTime();
    $('.dhtmlxcalendar_material').show();
    $('.dhtmlxcalendar_material').css({'z-index':'9000','border-radius':'5px','background-color':'#FFFFFF'});
    myCalendar.attachEvent("onClick",function(date){
      changeDatainReal('calender');
    });
  }
  
 
}

function calenderForTask1(userid) {
 
  myCalendar = new dhtmlXCalendarObject("calender_task2_"+userid);
  if(globalmenu!="agile" && globalmenu!="sprint"){
    var today = new Date();
    myCalendar.setSensitiveRange(today, null);
  }else{
    myCalendar.setSensitiveRange(sprintStartdateGlb, sprintEnddateGlb);
  }
  
  myCalendar.setDateFormat("%Y-%m-%d");
  myCalendar.hideTime();
  $('.dhtmlxcalendar_material').show();
  $('.dhtmlxcalendar_material').css({'z-index':'9000','border-radius':'5px','background-color':'#FFFFFF'});
  myCalendar.attachEvent("onClick",function(date){
    changeDatainReal1('calender',userid);
  });
 
}

function searchInContacts(obj){
  var value = $(obj).val().toLowerCase();
  $(".participant  *").filter(function() {
    //	console.log(value + "-----" + $(this).find(".names").text());
      $(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });
}
function removeUserTask(userid,type){
 $("#user_id_"+userid).remove();
 $("#user_id1_"+userid).remove();
 if(type=='edit'){
  let remove=$("#removeEmail").attr('user');
  remove=remove.toString()+userid.toString()+",";
  $("#removeEmail").attr('user',remove);

 }
 changeImage('user');
 changeImage('Approvers');
}

function showCalender1(obj){
  let id=$(obj).attr('id');
  let id1 = id.split('_')[0];
  let userid = id.split('_')[1];

  $("#time_calender1_"+userid).toggle();
  $(".activestartend").removeClass("activestartend");
  $(obj).addClass('activestartend');

    let fullhour=$("#resquestTimeCalNewUI_"+userid).text().substring(0,$("#resquestTimeCalNewUI_"+userid).text().length-2);
    $("#hour").text(fullhour.split(":")[0].trim());
    $("#minute").text(fullhour.split(":")[1].trim());
    $("#second").text("00");
    $(".mainampm").addClass($("#resquestTimeCalNewUI_"+userid).text().substring($("#resquestTimeCalNewUI_"+userid).text().length-2,$("#resquestTimeCalNewUI_"+userid).text().length).toLowerCase()+"active");
    if($("#resquestTimeCalNewUI_"+userid).text().substring($("#resquestTimeCalNewUI_"+userid).text().length-2,$("#resquestTimeCalNewUI_"+userid).text().length).toLowerCase()=="pm"){
      $(".ampm").text("PM");
      $('.colorDiv').css('background-color','#FFF');
    }else{
      $(".ampm").text("AM");
      $('.colorDiv').css('background-color','rgb(111, 158, 176)');
    }

    calenderForTask1(userid);

}
function showCalender(obj){
  
  let id=$(obj).attr('id');
  // if(id=="calenderForTask2"){
  //   $(".time_calender1").toggle();
  // }else{
    $(".time_calender").toggle();
  // }
  

  $(".activestartend").removeClass("activestartend");
  $(obj).addClass('activestartend');
  
  if(id=="calenderForTask"){
    let fullhour=$("#startTimeCalNewUI").text().substring(0,$("#startTimeCalNewUI").text().length-2);
    $("#hour").text(fullhour.split(":")[0].trim());
    $("#minute").text(fullhour.split(":")[1].trim());
    $("#second").text("00");
    $(".mainampm").addClass($("#startTimeCalNewUI").text().substring($("#startTimeCalNewUI").text().length-2,$("#startTimeCalNewUI").text().length).toLowerCase()+"active");
    if($("#startTimeCalNewUI").text().substring($("#startTimeCalNewUI").text().length-2,$("#startTimeCalNewUI").text().length).toLowerCase()=="pm"){
      $(".ampm").text("PM");
      $('.colorDiv').css('background-color','#FFF');
    }else{
      $(".ampm").text("AM");
      $('.colorDiv').css('background-color','rgb(111, 158, 176)');
    }

  }else{
    let fullhour=$("#endTimeCalNewUI").text().substring(0,$("#endTimeCalNewUI").text().length-2);
    $("#hour").text(fullhour.split(":")[0].trim());
    $("#minute").text(fullhour.split(":")[1].trim());
    $("#second").text("00");
    $(".mainampm").addClass($("#endTimeCalNewUI").text().substring($("#endTimeCalNewUI").text().length-2,$("#endTimeCalNewUI").text().length).toLowerCase()+"active");
    if($("#endTimeCalNewUI").text().substring($("#endTimeCalNewUI").text().length-2,$("#endTimeCalNewUI").text().length).toLowerCase()=="pm"){
      $(".ampm").text("PM");
      $('.colorDiv').css('background-color','#FFF');
    }else{
      $(".ampm").text("AM");
      $('.colorDiv').css('background-color','rgb(111, 158, 176)');
    }
  }
  $(".dhtmlxcalendar_material").show();
  // let selectedOne=$(".selected-deadline").attr('id');
  // if(selectedOne.includes('start')==true){
  //   let fullhour=$("#startTimeCalNewUI").text().substring(0,$("#startTimeCalNewUI").text().length-2);
  //   $("#hour").text(fullhour.split(":")[0].trim());
  //   $("#minute").text(fullhour.split(":")[1].trim());
  //   $("#second").text(fullhour.split(":")[2].trim());
  //   $(".mainampm").addClass($("#startTimeCalNewUI").text().substring($("#startTimeCalNewUI").text().length-2,$("#startTimeCalNewUI").text().length).toLowerCase()+"active");
  //   if($("#startTimeCalNewUI").text().substring($("#startTimeCalNewUI").text().length-2,$("#startTimeCalNewUI").text().length).toLowerCase()=="pm"){
  //     $(".ampm").text("PM");
  //     $('.colorDiv').css('background-color','#FFF');
  //   }else{
  //     $(".ampm").text("AM");
  //     $('.colorDiv').css('background-color','rgb(111, 158, 176)');
  //   }
  // }else{
  //   let fullhour=$("#endTimeCalNewUI").text().substring(0,$("#endTimeCalNewUI").text().length-2);
  //   $("#hour").text(fullhour.split(":")[0].trim());
  //   $("#minute").text(fullhour.split(":")[1].trim());
  //   $("#second").text(fullhour.split(":")[2].trim());
  //   $(".mainampm").addClass($("#endTimeCalNewUI").text().substring($("#endTimeCalNewUI").text().length-2,$("#endTimeCalNewUI").text().length).toLowerCase()+"active");
  //   if($("#endTimeCalNewUI").text().substring($("#endTimeCalNewUI").text().length-2,$("#endTimeCalNewUI").text().length).toLowerCase()=="pm"){
  //     $(".ampm").text("PM");
  //     $('.colorDiv').css('background-color','#FFF');
  //   }else{
  //     $(".ampm").text("AM");
  //     $('.colorDiv').css('background-color','rgb(111, 158, 176)');
  //   }
  // }

}

function changeDatainReal1(type,userid){

  let selectedOne=$(".activestartend").attr('id');
  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	let changeDate=myCalendar.getFormatedDate();
  //alert(changeDate)
  changeDate=monthNames[parseInt(changeDate.split("-")[1])]	+" "+changeDate.split("-")[2]+", "+changeDate.split("-")[0]	;	$("#addTaskDiv").attr('changestatus','Y');
  console.log(changeDate+"-------"+requDate);
  
  if(type=='calender'){
    if(selectedOne.includes('calenderForTask2_')){
      $("#resquestDateCalNewUI_"+userid).text(changeDate);
      $("#resquestDateCalNewUI_"+userid).attr('parmanentvalue',changeDate)
      let startDate=$("#resquestDateCalNewUI_"+userid).text();
      var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      let startdateMonth=monthNames.indexOf(startDate.substring(0,3))<10?"0"+monthNames.indexOf(startDate.substring(0,3)):monthNames.indexOf(startDate.substring(0,3));
      let startdateDate=startDate.substring(4,6)
      let start=startdateMonth.toString().trim()+"-"+startdateDate.trim()+"-"+startDate.split(",")[1].trim();

      let endDate=$("#endDateCalNewUI").text();
      let enddateMonth=monthNames.indexOf(endDate.substring(0,3))<10?"0"+monthNames.indexOf(endDate.substring(0,3)):monthNames.indexOf(endDate.substring(0,3));
      let enddateDate=endDate.substring(4,6)
      let end=enddateMonth.toString().trim()+"-"+enddateDate.trim()+"-"+endDate.split(",")[1].trim();


      let startfullTime=$("#resquestTimeCalNewUI_"+userid).text();
      let hour=startfullTime.substring(startfullTime.length-2,startfullTime.length)=="PM"?(parseInt(startfullTime.split(":")[0].trim())+12):startfullTime.split(":")[0].trim();
      if(parseInt(hour)>23){
        hour="12";
      }
      let startTime=hour+":"+startfullTime.split(":")[1].trim().substring(0,2);
      if(requDate!=changeDate){
        console.log("inn");
        $("#UserCommentNewUI").val("Request date is changed '"+changeDate+" "+startfullTime+"'");
      }

      let endfullTime=$("#endTimeCalNewUI").text();
      let endhour=endfullTime.substring(endfullTime.length-2,endfullTime.length)=="PM"?(parseInt(endfullTime.split(":")[0].trim())+12):endfullTime.split(":")[0].trim();
      if(parseInt(endhour)>23){
        endhour="12";
      }

      let endTime=endhour+":"+endfullTime.split(":")[1].trim().substring(0,2);

      let startDateto=start+" "+startTime;
      let startDatecompare=new Date(startDateto);
      let endDatecompare=new Date(end+" "+endTime);

      

    }
  }


}


function changeDatainReal(type){
  let selectedOne=$(".activestartend").attr('id');
  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	let changeDate=myCalendar.getFormatedDate();
  changeDate=monthNames[parseInt(changeDate.split("-")[1])]	+" "+changeDate.split("-")[2]+", "+changeDate.split("-")[0]	;	$("#addTaskDiv").attr('changestatus','Y');	       
  //alert(changeDate);
  if(type=='calender'){
    if(selectedOne=='calenderForTask'){
      $("#startDateCalNewUI").text(changeDate);
      $("#startDateCalNewUI").attr('parmanentvalue',changeDate)
      let startDate=$("#startDateCalNewUI").text();
      var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      let startdateMonth=monthNames.indexOf(startDate.substring(0,3))<10?"0"+monthNames.indexOf(startDate.substring(0,3)):monthNames.indexOf(startDate.substring(0,3));
      let startdateDate=startDate.substring(4,6)
      let start=startdateMonth.toString().trim()+"-"+startdateDate.trim()+"-"+startDate.split(",")[1].trim();

      let endDate=$("#endDateCalNewUI").text();
      let enddateMonth=monthNames.indexOf(endDate.substring(0,3))<10?"0"+monthNames.indexOf(endDate.substring(0,3)):monthNames.indexOf(endDate.substring(0,3));
      let enddateDate=endDate.substring(4,6)
      let end=enddateMonth.toString().trim()+"-"+enddateDate.trim()+"-"+endDate.split(",")[1].trim();


      let startfullTime=$("#startTimeCalNewUI").text();
      let hour=startfullTime.substring(startfullTime.length-2,startfullTime.length)=="PM"?(parseInt(startfullTime.split(":")[0].trim())+12):startfullTime.split(":")[0].trim();
      if(parseInt(hour)>23){
        hour="12";
      }
      let startTime=hour+":"+startfullTime.split(":")[1].trim().substring(0,2);
      

      let endfullTime=$("#endTimeCalNewUI").text();
      let endhour=endfullTime.substring(endfullTime.length-2,endfullTime.length)=="PM"?(parseInt(endfullTime.split(":")[0].trim())+12):endfullTime.split(":")[0].trim();
      if(parseInt(endhour)>23){
        endhour="12";
      }

      let endTime=endhour+":"+endfullTime.split(":")[1].trim().substring(0,2);

      let startDateto=start+" "+startTime;
      let startDatecompare=new Date(startDateto);
      let endDatecompare=new Date(end+" "+endTime);
      
      if(startDatecompare>endDatecompare){
        $("#endDateCalNewUI").text(changeDate);
        $("#endDateCalNewUI").attr('parmanentvalue',changeDate)
      }
    }else{
      $("#endDateCalNewUI").text(changeDate);
      saveAsTemporaryData();
    }
  }
 

}
function saveAsTemporaryData(){
  $(".activestartend").removeClass("activestartend");

  let startDate=$("#startDateCalNewUI").text();

  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let startdateMonth=monthNames.indexOf(startDate.substring(0,3))<10?"0"+monthNames.indexOf(startDate.substring(0,3)):monthNames.indexOf(startDate.substring(0,3));
  let startdateDate=startDate.substring(4,6)
  let start=startdateMonth.toString().trim()+"-"+startdateDate.trim()+"-"+startDate.split(",")[1].trim();

  let endDate=$("#endDateCalNewUI").text();
  let enddateMonth=monthNames.indexOf(endDate.substring(0,3))<10?"0"+monthNames.indexOf(endDate.substring(0,3)):monthNames.indexOf(endDate.substring(0,3));
  let enddateDate=endDate.substring(4,6)
  let end=enddateMonth.toString().trim()+"-"+enddateDate.trim()+"-"+endDate.split(",")[1].trim();


  let startfullTime=$("#startTimeCalNewUI").text();
  let hour=startfullTime.substring(startfullTime.length-2,startfullTime.length)=="PM"?(parseInt(startfullTime.split(":")[0].trim())+12):startfullTime.split(":")[0].trim();
  if(parseInt(hour)>23){
    hour="12";
  }
  let startTime=hour+":"+startfullTime.split(":")[1].trim().substring(0,2);
  

  let endfullTime=$("#endTimeCalNewUI").text();
  let endhour=endfullTime.substring(endfullTime.length-2,endfullTime.length)=="PM"?(parseInt(endfullTime.split(":")[0].trim())+12):endfullTime.split(":")[0].trim();
  if(parseInt(endhour)>23){
    endhour="12";
  }
  let endTime=endhour+":"+endfullTime.split(":")[1].trim().substring(0,2);

  let startDatecompare=new Date(start+" "+startTime);
  let endDatecompare=new Date(end+" "+endTime);
  //alert(start+"----"+startTime);
  if(startDatecompare>endDatecompare){
    alertFunNew("Due Date cannot be earlier than Start Date",'error');
    $("#startTimeCalNewUI").text($("#startTimeCalNewUI").attr('parmanentvalue'));
    $("#startDateCalNewUI").text($("#startDateCalNewUI").attr('parmanentvalue'));

    $("#endTimeCalNewUI").text($("#endTimeCalNewUI").attr('parmanentvalue'));
    $("#endDateCalNewUI").text($("#endDateCalNewUI").attr('parmanentvalue'));

  }else{
    // let selectedOne=$(".selected-deadline").attr('id');
    // $("#startofTask").text(($("#startDateCalNewUI").text())+" "+($("#startTimeCalNewUI").text().split(":")[0])+":"+($("#startTimeCalNewUI").text().split(":")[1])+ ($("#startTimeCalNewUI").text().substring($("#startTimeCalNewUI").text().length-2,$("#startTimeCalNewUI").text().length)));
  
    // $("#endofTask").text(($("#endDateCalNewUI").text())+" "+($("#endTimeCalNewUI").text().split(":")[0])+":"+($("#endTimeCalNewUI").text().split(":")[1])+ ($("#endTimeCalNewUI").text().substring($("#endTimeCalNewUI").text().length-2,$("#endTimeCalNewUI").text().length)));

    // $("#startDateCalNewUI").attr("permanentValue",$("#startDateCalNewUI").text());
    // $("#endDateCalNewUI").attr("permanentValue",$("#endDateCalNewUI").text());
    // $("#startTimeCalNewUI").attr("permanentValue",$("#startTimeCalNewUI").text());
    // $("#endTimeCalNewUI").attr("permanentValue",$("#endTimeCalNewUI").text());
    // $(".deadlinepopup").toggle('slow');
    
    // //alert($("#estimateHour").text());

    // $("#estimateHour").attr("permanentValue",$("#estimateHour").text());
    // $("#estimateMinute").attr("permanentValue",$("#estimateMinute").text());

    // let estimateTime= $("#estimateHour").attr("permanentValue")+" h "+$("#estimateMinute").attr("permanentValue")+" m"
    // $("#estimated").text(estimateTime);

    // let Priority=$("#priority option:selected").val();
    // Priority = Priority==0?"6":Priority;
    // var Priority_image = (Priority == "")? "" : Priority == "1" ? "images/task/p-one.svg" : Priority == "2" ? "images/task/p-two.svg":Priority == "3" ? "images/task/p-three.svg" :Priority == "4" ? "images/task/p-four.svg":Priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
    // $("#priorityHrOfTask").attr('src',Priority_image) ;

    // $("#priority").attr("permanentValue",$("#priority option:selected").val());
    // $("#eventReminder").attr("permanentValue",$("#eventReminder").val());
    $("#startTimeCalNewUI").attr('parmanentvalue', $("#startTimeCalNewUI").text());
    $("#endDateCalNewUI").attr('parmanentvalue', $("#endDateCalNewUI").text())
  }



}
function increaseValue(type,obj){
    let hour="";
    let minute="";
    let second="";
    if(type=='hour'){
         hour=(parseInt($("#hour").text())+1)>12?"1":(parseInt($("#hour").text())+1);
         $("#hour").text(parseInt(hour)<10?"0"+hour:hour);
         minute=parseInt($("#minute").text())
         second=parseInt($("#second").text())
    }else if(type=="minute"){
         hour=parseInt($("#hour").text());
         minute=parseInt($("#minute").text())+1>59?"1":parseInt($("#minute").text())+1;
         $("#minute").text(parseInt(minute)<10?"0"+minute:minute);
         second=parseInt($("#second").text());
    }else{
         hour=parseInt($("#hour").text());
         minute=parseInt($("#minute").text())
         second=parseInt($("#second").text())+1>59?"1":parseInt($("#second").text())+1;
         $("#second").text(parseInt(second)<10?"0"+second:second);
    }
    let selectedOne=$(".activestartend").attr('id');
    
    let fullTime=(parseInt(hour)<10?"0"+hour:hour)+" :" +(parseInt(minute)<10?"0"+minute:minute)+" "+($(".ampm").text());
    if(selectedOne=='calenderForTask'){
      $("#startTimeCalNewUI").text(fullTime);
    }else{
      $("#endTimeCalNewUI").text(fullTime);
    }

}
function decreaseValue(type){
    if(type=='hour'){
      hour=(parseInt($("#hour").text())-1)<1?"12":(parseInt($("#hour").text())-1);
      $("#hour").text(parseInt(hour)<10?"0"+hour:hour);
      minute=parseInt($("#minute").text())
      second=parseInt($("#second").text())
    }else if(type=="minute"){
        hour=parseInt($("#hour").text());
        minute=parseInt($("#minute").text())-1<0?"59":parseInt($("#minute").text())-1;
        $("#minute").text(parseInt(minute)<10?"0"+minute:minute);
        second=parseInt($("#second").text());
    }else{
        hour=parseInt($("#hour").text());
        minute=parseInt($("#minute").text())
        second=parseInt($("#second").text())-1<0?"59":parseInt($("#second").text())-1;
        $("#second").text(parseInt(second)<10?"0"+second:second);
    }
    let selectedOne=$(".activestartend").attr('id');
    
    let fullTime=(parseInt(hour)<10?"0"+hour:hour)+" :" +(parseInt(minute)<10?"0"+minute:minute)+" "+($(".ampm").text());
    if(selectedOne=='calenderForTask'){
      $("#startTimeCalNewUI").text(fullTime);
    }else{
      $("#endTimeCalNewUI").text(fullTime);
    }
}
function changeAmPm(obj){
  let ampm="";
  
  if($(obj).attr('class').includes('amactive')){
    $(obj).removeClass("amactive");
    $(obj).addClass("pmactive");
    $(obj).find(".ampm").text("PM");
    ampm="PM"
    $('.colorDiv').css('background-color','#FFF');
  }else{
    $(obj).removeClass("pmactive");
    $(obj).addClass("amactive");
    $(obj).find(".ampm").text("AM");
    ampm="AM"
    $('.colorDiv').css('background-color','rgb(111, 158, 176)');
  }
  let selectedOne=$(".selected-deadline").attr('id');
  let hour=$("#hour").text();
  let minute=$("#minute").text();
  let second=$("#second").text();
  let fullTime=(parseInt(hour)<10?"0"+parseInt(hour):hour)+" :" +(parseInt(minute)<10?"0"+parseInt(hour):minute)+" "+(ampm);    //: "+(parseInt(second)<10?"0"+parseInt(hour):second)+"
  if(selectedOne.includes('start')==true){
    $("#startTimeCalNewUI").text(fullTime);
    
  }else{
    $("#endTimeCalNewUI").text(fullTime);
  }

}
function uploadFileForTask(input,taskId,place){
  var docType="";
  if(globalmenu=="Idea"){
    docType="assignedIdea";
  }else{
    docType="Tasks";
  }
  let inputFiles = input.files[0];
  if (input.files && input.files[0]) {
    if(input.files[0].size >= 25000000){
      confirmReset(getValues(companyAlerts,"Alert_ResetMoreSize"),'delete','docUploadInComments','cancelTheDragAndDropUpload');
      $('.alertMainCss').css({'width': '28.5%'});
      $('.alertMainCss').css({'left': '45%'});
      $('#BoxConfirmBtnOk').css({'width': '21%'});
      $('#BoxConfirmBtnOk').val('Continue');
    }else{
      commonUploadForTask(inputFiles,taskId,place,'','Project',docType,place);
    }
  }

}
async function commonUploadForTask(inputFiles,resourceId,place,menuTypeId,taskType,docType,place){
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  $("#addTaskDiv").attr('changestatus','Y');
  var formData = new FormData();
  formData.append('file', inputFiles);
  formData.append('user_id', userIdglb);
  formData.append('company_id', companyIdglb);
  formData.append('resourceId', resourceId);
  formData.append('projId', prjid);
  formData.append('place', place);
  formData.append('menuTypeId', menuTypeId);
  formData.append('taskType', taskType);
  formData.append('docType',docType);
  
    $.ajax({
      url: apiPath+"/"+myk+"/v1/upload", 
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      dataType:'text',
      data: formData,
      error: function(jqXHR, textStatus, errorThrown) {
              checkError(jqXHR,textStatus,errorThrown);
              $('#loadingBar').addClass('d-none').removeClass('d-flex');
      },
      success: function(url) {
          let documentId=url.split('@@##')[1]+",";
          if(place=='TaskFileUpload'){
            fetchUserLevelDoc('',resourceId,userIdglb);
          }else{
            getDocumentDetails(resourceId,'upload','cal',documentId,docType);
          } 
         $(".hortree-label-active").find('.stepdoc').show();
         let id=$(".hortree-label-active").find('.stepdoc').attr('id').split("_")[1];
         //alert(id);
         showTaskUI(id);
         
         $(".workflowUI").attr('documentcount','1');
      }
    });
    $('#loadingBar').addClass('d-none').removeClass('d-flex');
}
function getDocumentDetails(task_id,type,menuType,documentIds,taskType){
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonBody={
    "task_id":task_id,
    "docTaskId":task_id,
    "docTaskType":"",
    "type":type,
    "docTaskprjId":prjid,
    "menuType":menuType,
    "documentIds":documentIds,
    "taskType":taskType,
    "user_id":userIdglb,
    "company_id":companyIdglb
  }//--->
  $.ajax({
    url: apiPath + "/" + myk + "/v1/getTaskLevelDoc",
    type: "POST",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonBody),
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
    },
    success: function (resp) {
      let UI= prepareDocUI(resp);
      //console.log(UI);
      
      if(task_id!=0){
        $(".taskDocument").html('');
      }
      $(".taskDocument").append(UI);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
    }
  });
}

function prepareDocUI(resp){
  let UI='';
  for(i=0;i<resp.length;i++){
    let ext=resp[i].document_physical_name;
    ext = ext.substr(ext.lastIndexOf(".")+1).toLowerCase();
    let onclickexpand="";
    let onclickDownload="";
    
    if(ext.toLowerCase() == "png" || ext.toLowerCase() == "jpg" || ext.toLowerCase() == "jpeg" || ext.toLowerCase() == "gif" || ext.toLowerCase() == "svg"){
     //onclick="thumbnailOpenUi(this,"+resp[i].document_id+",'"+ext+"',"+resp[i].task_document_id+",'task');"
     //ondoubleckick="expandImage("+resp[i].document_id+",'"+ext+"')";

     onclickexpand="expandImage("+resp[i].document_id+",'"+ext+"','task')";
     onclickDownload=" downloadActFile("+resp[i].document_id+","+prjid+",'"+ext+"',"+resp[i].task_document_id+",'task')";
     //expandImage("+docid+",'"+ext+"','"+place+"');
     //downloadActFile("+docid+","+prjid+",'"+ext+"',"+feedid+",'"+place+"')
    }else if(ext.toLowerCase() == "mp3" || ext.toLowerCase() == "wav" || ext.toLowerCase() == "wma" || ext.toLowerCase() == "mp4" || ext.toLowerCase() == ".m4v" || ext.toLowerCase() == "mov" || ext.toLowerCase() == "flv" || 
      ext.toLowerCase() == "f4v" || ext.toLowerCase() == "ogg" || ext.toLowerCase() == "ogv" || ext.toLowerCase() == "wmv" ||
      ext.toLowerCase() == "vp6" || ext.toLowerCase() == "vp5" || ext.toLowerCase() == "mpg" || ext.toLowerCase() == "avi" ||
      ext.toLowerCase() == "mpeg" || ext.toLowerCase() == "webm"){
      
    }else{
     
      //onclick="viewordownloadUi("+resp[i].document_id+",this,'"+ext+"',"+resp[i].task_document_id+",'task')";
      onclickexpand="viewdocument("+resp[i].document_id+",'"+ext+"','task')";
      onclickDownload=" downloadActFile("+resp[i].document_id+","+prjid+",'"+ext+"',"+resp[i].task_document_id+",'task')";
      //viewdocument("+docid+",'"+ext+"','"+place+"');
     // downloadActFile("+docid+","+prjid+",'"+ext+"',"+feedid+",'"+place+"')
    }
    UI+='<div class="d-flex pb-2 mt-2 position-relative taskdoc" onmouseleave="hideGroupEdit('+resp[i].task_document_id+',\'taskdocument\');" onmouseover="showGroupEdit('+resp[i].task_document_id+',\'taskdocument\');"  style="cursor: pointer;border-bottom:1px solid #EAEAEA" id="doc_'+resp[i].task_document_id+'">'
            +'<div><img  class="float-left mt-1 ml-2 " style="width:24px;" src="/images/document/'+ext+'.svg" onerror="imageOnFileNotErrorReplace(this)" id="view_27623"></div>'
            +'<div class="ml-2 mt-1 defaultExceedCls"  style="font-size:12px">'+resp[i].document_physical_name+'</div>'
            +'<div id="subcommentDiv_taskdoc_'+resp[i].task_document_id+'" class="actFeedOptionsDiv" style="border-radius: 8px; top: 1px; margin-right: 5px; display: none;"><div class="d-flex align-items-center"><img src="images/conversation/expand.svg" onclick="'+onclickexpand+'" title="View" style="width:15px;height:15px;" class="mx-1" /> <img src="images/conversation/download2.svg"  title="dounload" style="width:15px;height:15px;"  class="mx-1"  onclick="'+onclickDownload+'" /><img class="mx-1" title="Delete" onclick="deleteTaskDocument('+resp[i].task_document_id+')" src="/images/task/minus.svg" id="delete_group" style="width:18px;height:18px;"></div></div>'
       +'</div>'
  }
  return UI;
}
async function fetchDependencyTask(taskId,place){
    let condition=false;
    if(place=="SubList" && $("#dependencyDiv_"+taskId).length==0){
      condition=true;
    }
    if(place!="SubList" && $(".DependentTaskDiv").length==0){
        condition=true;
    }
    $(".task_comment_div").hide().find('.task_comment_list_div').html('');
    $(".task_user_history").hide();
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    if(condition){
      let jsonbody = {
            "project_id":prjid,
            "user_id":userIdglb,
            "company_id":companyIdglb,
            "task_id":taskId
       }
      await $.ajax({
        url:apiPath+"/"+myk+"/v1/getTaskDetails/"+place,
       // url:"http://localhost:8080/v1/getTaskDetails/"+place,
        type:"POST",
        contentType:"application/json",
        dataType:'json',
        data: JSON.stringify(jsonbody),
        error: function(jqXHR, textStatus, errorThrown) {
                  checkError(jqXHR,textStatus,errorThrown);
                  $("#loadingBar").hide();
                  timerControl("");
                }, 
        success:function(result){
          if(place!='SubList'){
            let UI=prepareDependencyUIlikeTask(result);
            $(".dependency_comment_div").append(UI).show();
            
            let depTasks=$("#dependencyTask").attr('deptasks');
            if(typeof(depTasks)!='undefined'){
              let depTasks1=depTasks.split(",");
              for(i=0;i<depTasks1.length;i++){
                
                $("#imageCheck_"+depTasks1[i]).attr('atribute','checked');
                $("#imageCheck_"+depTasks1[i]).prop('checked', true);
                $("#depTask_"+depTasks1[i]).attr('select','E');
                let clone= $("#depTask_"+depTasks1[i]).clone();
                $("#depTask_"+depTasks1[i]).remove();
                $(".dependenctTaskIds").prepend(clone);

              }
            }

            $("#entersearch").on('click',()=>{
              $("#dependency_text").hide();
             //$("#entersearch").hide();
              $("#dependencysearch").show();
              $("#dependencysearch").focus();
            });
            $("#backsearch").on('click',()=>{
              fetchDependencyTask(taskId,place);
            });
            $("#dependencysearch").on('keyup',()=>{
              var value = $("#dependencysearch").val().toLowerCase();
              $(".dependenctTaskIds  .dependecyTasksList").filter(function() {
                 // console.log(value + "-----" + $(this).attr('class'));
                  $(this).toggle($(this).find(".dependency_task_name").text().toLowerCase().indexOf(value) > -1);
                });
            })
          }        
          else{
              let UI=""
              UI+="<div class='ml-5 pb-2 listdeptask' id='dependencyDiv_"+taskId+"' style='height:auto;'> "
                    UI+=wsTaskUi('subList');
                    UI+="<div class='depList'>"
                    UI+=createTaskUI(result,'dependency');
                    UI+="</div>"
                    UI+="</div>"
                 $(UI).insertAfter("#TaskStatus_"+taskId);
                // alert($(".depList").children().last().attr('id'))
                 $(".depList").children().last().css('border-bottom','0');
                 $('#dependenctImage_'+taskId).attr('src','/images/task/dependencyup.svg')
          }
        }
      });
    }else{
      if(place=="SubList" ){
        $("#dependencyDiv_"+taskId).toggle();
        if( $("#dependencyDiv_"+taskId).is(":hidden")){
          $('#dependenctImage_'+taskId).attr('src','/images/task/dependencydown.svg')
        }else{
          $('#dependenctImage_'+taskId).attr('src','/images/task/dependencyup.svg')
        }
      }else{
        $(".dependency_comment_div").toggle();
      }
    }
    $('#loadingBar').addClass('d-none').removeClass('d-flex');
  
}
function prepareDependencyTaskUI(result){
  var depTaskUI = "";
  if(result){
      var json = eval(result);

      depTaskUI+="<div class=' d-flex justify-content-between align-items-center pb-1' >" 
                  +'<div class="d-flex w-75">'
                    +'<div id="" class="pl-1 mr-1" style="font-size:12px;" >Dependent Task</div>'
                    +'<img class="" id="backsearch" src="images/task/dowarrow.svg" style="height:15px;width:15px; cursor:pointer;margin-top:2px">'
                  +'</div>'
                  +'<input type="text" class="pl-1"  id="dependencysearch" style="display:none;border: 0;width: 90%;outline: none;border-bottom:1px solid #b4adad"/>'
                  +'<div>'
                    +'<img class="mt-1 mr-1" id="entersearch" src="images/menus/search2.svg" style="height:18px;width:18px; cursor:pointer;margin-top:2px">'
                   // +'<img class="" id="backsearch" src="/images/back1.png" style="height:15px;width:15px; cursor:pointer;margin-top:2px">'
                  +'</div>'
                +"</div>"

      depTaskUI+='<div class="pl-2 pr-0 pb-2 pt-2 w-100 dependencyTask" style="border-top: 1px solid #b4adad; border-bottom: 1px solid #b4adad; ">'
      
      depTaskUI+='<div class="dependenctTaskIds defaultScrollDiv" style="height:180px;overflow:auto;">'
      for(var i = 0; i < json.length; i++){
          var taskStatusImageTitle = json[i].task_status;
          var taskStatusImage = taskStatusImageTitle == "Completed" ? "images/calender_old/Completed.png" : taskStatusImageTitle == "InProgress" ? "images/calender_old/InProgress.png" : taskStatusImageTitle == "Created" ? "images/calender_old/InProgress.png" : taskStatusImageTitle == "InComplete" ? "images/calender_old/incompleteDetailPic.png" : taskStatusImageTitle == "NotCompleted" ? "images/calender_old/NotCompleted.png"  : "images/calender_old/NotCompleted.png";

          json[i].task_name = replaceSpecialCharacter(json[i].task_name);
         
          let taskName = replaceSpecialCharacter(json[i].task_name);
          depTaskUI+='<div class="dependecyTasksList pb-2 " select="N" id="depTask_'+json[i].task_id+'" style="border-bottom:1px solid lightgray">'
                        
                          +'<div class="task_details d-flex  p-2" task_id="'+json[i].task_id+'">'
                            +'<input id="imageCheck_'+json[i].task_id+'" class="check mt-1"  type="checkbox" style="cursor:pointer"  onchange="changeImage(\'dependency\');" />'
                            +'<div class="ml-2"><img src="images/task/task.svg" style="height:20px;width:20px"/></div>'
                            +'<div class="ml-2 dependency_task_name">'+taskName+'</div>'
                            // +'<div  style="margin-top:-2px;margin-left:auto;"  ><img class="imagecheck" id="imageCheck_'+json[i].task_id+'" src="images/task_old/gryright.png" atribute="uncheck"  onclick="checkDependency(this,\''+json[i].task_id+'\')"style="height:18px;width:18px;cursor:pointer"/></div>'

                          +'</div>'

                          +'<div class="d-flex ml-5" style="font-size:12px;color:#999999;">'
                            +'<div class="pr-2 pl-2" style="border-right:1px solid #999999;"> From : '+json[i].start_date+' &nbsp;&nbsp;To : '+json[i].end_date+'</div>'
                            +'<div class="ml-2 pr-2 "  style="border-right:1px solid #999999;"> Created by :'+json[i].task_created_by+'</div>'
                            +'<div class="ml-3" style="margin-top:-2px"><img src="'+taskStatusImage+'" style="height:13px;width:13px"/></div>'

                          +'</div>'
                    +'</div>'
      }
      if(result.length==0){
        depTaskUI+='<div class="ml-2 p-2">No Task Found</div>'
      }
      depTaskUI+='</div>'
      depTaskUI+='</div>'
      

  }
  return depTaskUI;
}

function checkDependency(obj,taskId){
  if($(obj).attr('atribute')=='uncheck'){
    $(obj).attr('src','images/task_old/right.png');
    $(obj).attr('atribute','checked')
  }else{
    $(obj).attr('src','images/task_old/gryright.png');
    $(obj).attr('atribute','uncheck');
  }
    
}


var requDate="";

function editTask(taskId,sprinttype){
  if($('#tDetails_'+taskId).is(':visible')){
    $('#tDetails_'+taskId).hide().html('');
  }
  let sprintHistoryValue = typeof(sprinttype)=="undefined"||sprinttype=="undefined"||sprinttype=="calendar"?"Present":sprinttype;
  /* if($("#task_"+taskId).attr('taskType')=='Sprint'){
    sprintHistoryValue=sprinttype;
  } */
  if(valueForView=='list'){
    $("#task_"+taskId).css('background-color','#eef5f7');
  }else if(valueForView=='grid'){
      // $("#task_"+taskId).hide();
      $("#innerTaskList").find("div[id^=task_]").removeClass("taskNodeSelect");
      $("#innerTaskList").find("div[id^=WFtask_]").removeClass("taskNodeSelect");
      $("#task_"+taskId).addClass("taskNodeSelect");
  }
  let UI=prepareAddTaskUiNew("edit",taskId);
  if(globalhlmenu=="Highlight"){
    $("#ideacreateTaskDivbody").html(UI);
    $('.tHeaderOptions').addClass('d-none');
  }else if(globalmenu=="Idea"){
    $("#ideacreateTaskDivbody").html(UI).show('slow');
    $('#WFHeadertitleDiv ,#EventHeadertitleDiv').addClass('d-none');
    $('#taskHeaderTitle').text('Idea Task');
    $('#taskHeaderTitleimg').attr('src','images/task/idea-task.svg');
    $('.tHeaderOptions').addClass('d-none');
  }else if(globalmenu=="agile" || globalmenu=="sprint"){
    $("#agilecreateTaskDivbody").html(UI).show('slow');
    $('#WFHeadertitleDiv ,#EventHeadertitleDiv').addClass('d-none');
    $('#taskHeaderTitle').text('Sprint Task');
    $('#taskHeaderTitleimg').attr('src','/images/agile/sprint_blue.svg');
    $('.tHeaderOptions').addClass('d-none');
    
  }else if(globalmenu=="activityFeed"){
    $("#popupcreateTaskDivbody").html(UI).show('slow');
    $('#WFHeadertitleDiv ,#EventHeadertitleDiv').addClass('d-none');
    $('#taskHeaderTitle').text('Conversation Task');
    $('#taskHeaderTitleimg').attr('src','/images/menus/conversation.svg');
    $('.tHeaderOptions').addClass('d-none');
  }else if(globalmenu=="wsdocument"){
    $("#popupcreateTaskDivbody").html(UI).show('slow');
    $('#WFHeadertitleDiv ,#EventHeadertitleDiv').addClass('d-none');
    $('#taskHeaderTitle').text('Document Task');
    $('#taskHeaderTitleimg').attr('src','/images/menus/doc.svg');
    $('.tHeaderOptions').addClass('d-none');
  }
  else{
    $("#addTaskHolderDiv").html(UI).show('slow');
  }
  sprintStartdateGlb=new Date(); sprintEnddateGlb=null;

  createPopupDiv();
 
  $("#taskspan").css('margin-top','0px')
  var element = document.getElementById("addTaskScroll");
  element.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
  
  $("#upload").on('click',()=>{
    document.getElementById('uploadForTask').click();
   });
  //showDeadLinePopup('edit');
  //replaceSaveWithDueDate();
  $("#task_id").val(taskId);
  $(".Active").removeClass('Active');
  $('.dhtmlxcalendar_material').hide();
  $.ajax({ 
    url: apiPath+"/"+myk+"/v1/getHistoryMyTaskDetailView/normalTask?sprintHistoryValue="+sprintHistoryValue+"&task_id="+taskId+"&user_id="+userIdglb+"&company_id="+companyIdglb+"",
    // url:"http://localhost:8080/v1/getHistoryMyTaskDetailView/normalTask?sprintHistoryValue="+sprintHistoryValue+"&task_id="+taskId+"&user_id="+userIdglb+"&company_id="+companyIdglb+"",
    type:"GET",
    error: function(jqXHR, textStatus, errorThrown) {
            checkError(jqXHR,textStatus,errorThrown);
            $("#loadingBar").hide();
            timerControl("");
            }, 
    success:function(result){
      /* if($(".elem-CalenStyle").length!=0){
        $(".elem-CalenStyle").hide();
        $(".TaskDiv").show();
      } */ 
      $("#headerTask, #taskList, .popupTaskDiv").hide();
      $("#headerTask").addClass('d-none').removeClass('d-flex');
     // console.log(result)
      let startDate=returnFormatedDate(result[0].start_date)
      let startTime=returnFormatedTime(result[0].start_time);
      let endDate=returnFormatedDate(result[0].end_date)
      let endTime=returnFormatedTime(result[0].end_time);

      $("#"+result[0].task_type).addClass('Active');
      $("#Tasks").removeAttr('onclick');
      $("#Workflow").removeAttr('onclick');
      $("#Event").removeAttr('onclick');
      $("#tSprinttask").removeAttr('onclick');
      getDocumentDetails(taskId,'upload','cal','',result[0].task_type)
      $("#taskname").val(result[0].task_name);
      $("#taskdesc").val(result[0].task_desc);
      if(result[0].task_desc!=""){
        $("#instruction").attr('src','/images/task/intr.svg');
      }

      if(gloablmenufor=='Myzone'){
        $("#projectlistingDiv").addClass('d-flex').removeClass('d-none');
        let UI="";
        if(result[0].project_id=="0"){
          UI='<option >Select Workspace</option>'
        }else{
          UI='<option value="'+result[0].project_id+'">'+result[0].project_name+'</option>'
        }
        $("#projectselect").html(UI);
        $("#projectselect").attr('disabled','disabled');
        prjid=result[0].project_id;
        
      }
      if(globalhlmenu=="Highlight"){
        // $("#ideacreateTaskDivbody").html(UI);
        // prjid=0;
        // fetchProjectsForMyzone();
        // $("#projectlistingDiv").addClass('d-flex').removeClass('d-none');
        $("#projectlistingDiv").addClass('d-flex').removeClass('d-none');
        let UI="";
        if(result[0].project_id=="0"){
          UI='<option >Select Workspace</option>'
        }else{
          UI='<option value="'+result[0].project_id+'">'+result[0].project_name+'</option>'
        }
        $("#projectselect").html(UI);
        $("#projectselect").attr('disabled','disabled');
        prjid=result[0].project_id;
      }
      if(result[0].dependencyTaskId!="0"){
        $("#dependencyTask").attr('src','/images/task/task_dependency.svg');
      }
      $("#endDateCalNewUI").attr('permanentValue',endDate);
      $("#endTimeCalNewUI").attr('permanentValue',endTime);
      $("#startDateCalNewUI").attr('permanentValue',startDate);
      $("#startTimeCalNewUI").attr('permanentValue',startTime);

      $("#endDateCalNewUI").text(endDate);
      $("#endTimeCalNewUI").text(endTime);
      $("#startDateCalNewUI").text(startDate);
      $("#startTimeCalNewUI").text(startTime);

      $(".estimateHour").val(result[0].total_estimated_hour);
      $(".estimateMinute").val(result[0].total_estimated_minute);
       
      if($(".estimateMinute").val()!="0" || $(".estimateHour").val()!="0"){
        $(".estimateHour").attr('modified','modified');
        $(".estimateMinute").attr('modified','modified');
      }
      let aH=result[0].act_hour;
      let aM=result[0].act_min;
      if(parseInt(aM)>60){
        aH=parseInt(aH)+Math.floor(parseInt(aM)/60);
        aM=Math.floor(parseInt(aM)%60);
      }
      $("#taskactualhour").val(aH);
      $("#taskactualmin").val(aM);


      $(".estimateHour").attr('permanentValue',result[0].total_estimated_hour);
      $(".estimateMinute").attr('permanentValue',result[0].total_estimated_minute);
      $("#totalHourIn").val( $('#estimateHour').text());
      $("#totalMinIn").val($('#estimateMinute').text())
      $("#task_type").val(result[0].task_type);
      if(result[0].source_id!="" || result[0].task_type!="Tasks"){
        $("#dependencyTask").removeClass('d-block').addClass('d-none');
        $("#dependencyTask").parent().removeClass('d-block').addClass('d-none');
      }

      if(result[0].task_type=="Sprint"){
        sprintTaskinTask="sprintTaskinTask";
        let sprinttitle=typeof(result[0].sprint_title)=='undefined'?'-':result[0].sprint_title;
        $(".thirdDiv").addClass('d-flex').removeClass('d-none');
        $("#reminder").css('border-bottom','1px solid black');
       
        $('.groupDiv').addClass('d-none').removeClass('d-flex');
        $('.actualDiv ').addClass('mr-auto');
        $('#spGrplistDiv , #sprintlistDiv').children('img').attr('onclick','').css('cursor','default');
        $('#sprintgroupcolor').css('background-color',result[0].sprint_color_code);
        $('#sprintgroupname').text(result[0].sprint_name).attr('tsprgrpid',result[0].sprintGrpId).attr('title',result[0].sprint_name).css('color','darkgray');
        $('#sprintname').text(result[0].sprint_title).attr('title',result[0].sprint_title).attr('tsprintid',result[0].sprintId).attr('tspgroupid',result[0].sprintGrpId).css('color','darkgray');
        
        var stdate = typeof(result[0].sprint_start_date)=="undefined"||result[0].sprint_start_date=="undefined"?result[0].start_date:result[0].sprint_start_date;
        var enddate = typeof(result[0].sprint_end_date)=="undefined"||result[0].sprint_end_date=="undefined"?result[0].end_date:result[0].sprint_end_date;

        var splitstdate = stdate.split("-");
        var splitenddate = enddate.split("-");
        sprintStartdateGlb=new Date(splitstdate[0], splitstdate[1]-1, splitstdate[2]);
        sprintEnddateGlb=new Date(splitenddate[0], splitenddate[1]-1, splitenddate[2]);
        
        calenderForTask();

        $('.dhtmlxcalendar_material').hide();
        
      }else if(result[0].task_type=='Tasks' && result[0].source_id != "0"){
        $("#conversationfetch").addClass('d-block').removeClass('d-none');
        $("#conversationfetch").attr('onclick','showTaskFeed('+result[0].source_id+',"Task",'+result[0].project_id+');event.stopPropagation();');//fetchPostedThingFiles('+ result[0].source_id+',"","0","","Task",'+result[0].project_id+')
      }else if(result[0].task_type=="WorkspaceDocument" || result[0].task_type=="Document"){
        $("#docTask").addClass('d-block').removeClass('d-none');
        let ext=result[0].document_name;
        ext = ext.substr(ext.lastIndexOf(".")+1).toLowerCase();
        let onclick="";
        let ondoubleclick="";
      
        if(ext.toLowerCase() == "png" || ext.toLowerCase() == "jpg" || ext.toLowerCase() == "jpeg" || ext.toLowerCase() == "gif" || ext.toLowerCase() == "svg"){
            onclick="thumbnailOpenUi(this,"+result[0].source_id+",'"+ext+"',"+result[0].source_id+",'"+result[0].task_type+"');"
            ondoubleclick="expandImage("+result[0].source_id+",'"+ext+"','"+result[0].task_type+"')";
        }else if(ext.toLowerCase() == "mp3" || ext.toLowerCase() == "wav" || ext.toLowerCase() == "wma" || ext.toLowerCase() == "mp4" || ext.toLowerCase() == ".m4v" || ext.toLowerCase() == "mov" || ext.toLowerCase() == "flv" || 
          ext.toLowerCase() == "f4v" || ext.toLowerCase() == "ogg" || ext.toLowerCase() == "ogv" || ext.toLowerCase() == "wmv" ||
          ext.toLowerCase() == "vp6" || ext.toLowerCase() == "vp5" || ext.toLowerCase() == "mpg" || ext.toLowerCase() == "avi" ||
           ext.toLowerCase() == "mpeg" || ext.toLowerCase() == "webm"){
        
        }else{
        
          onclick="viewordownloadUi("+result[0].source_id+",this,'"+ext+"',"+result[0].source_id+",'"+result[0].task_type+"')";
        }
        $("#docTask").attr('onclick',onclick),
        $("#docTask").attr('ondblclick',ondoubleclick);
      }
      if(result[0].document_type=="link"){
        $("#docTask").removeClass("d-block").addClass("d-none");
        $("#docTaskLink").removeClass("d-none").addClass("d-block");
        $("#docTaskLink").removeAttr("onclick").attr("onclick","docTaskLinkOpen('"+result[0].document_name+"');event.stopPropagation();");

      }
      let priority=result[0].priority=="6"?"0":result[0].priority
      var Priority_image = (priority == "")? "" : priority == "1" ? "images/task/p-one.svg" : priority == "2" ? "images/task/p-two.svg":priority == "3" ? "images/task/p-three.svg" :priority == "4" ? "images/task/p-four.svg":priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
      $(".changePriority").attr('priority',priority);
      $('.changePriority').attr('src',Priority_image);
     
  
      if(result[0].createdById==userIdglb){
        $("#deleteTask").attr('onclick','deleteTask('+taskId+',\''+result[0].task_type+'\',"N")');
      }else{
        $("#deleteTask").css('opacity','0.5');
      }

      let reminderType=result[0].reminder==""?"select":result[0].reminder
      
     
      if(reminderType!="select"){
        $("#remindericon").attr('src','/images/task/clock-Invt.svg');
        $("#remindericon").attr('reminder',reminderType);
      }
      if(result[0].taskCommentCount!="0"){
        $("#comments").attr('src','/images/task/comments2.svg');
      }

      if(typeof(result[0].task_group_id)!='undefined'){
        $("#changeGroup").attr('group_id',result[0].task_group_id);
        $("#changeGroup").css('background',result[0].group_code);
        $("#changeGroup").attr('title',result[0].group_name);
      }
      if(result[0].taskParticipant.length>0){
        $("#participantsUIdivArea").attr('src','/images/task/assignee_cir2.svg')
      }
      //// for participent
      for(i=0;i<result[0].taskParticipant.length;i++){
        let imgName = lighttpdpath + "/userimages/" + result[0].taskParticipant[i].user_id+"."+result[0].taskParticipant[i].user_image_type + "?" + d.getTime();
        // alert(calculateElapseTime(result[0].taskParticipant[i].elapsed_time) +"----"+result[0].taskParticipant[i].elapsed_time)
        addUserToData(prjid, result[0].taskParticipant[i].user_full_name, result[0].taskParticipant[i].user_id, '',imgName,'edit',taskId,calculateElapseTime(result[0].taskParticipant[i].elapsed_time) );

        let image=result[0].taskParticipant[i].user_task_status;
        if(image=="Paused"){
          image="images/task/user-pause.svg"
        }else if(image=='Inprogress'){
          image="images/task/user-inprogress.svg"
        }else if(image=='Completed'){
          image="images/task/user-completed.svg"
        }else{
          image="images/task/user-start.svg"
        }
        
        $("#persontaskstatusdisplay_"+result[0].taskParticipant[i].user_id).attr('src',image);
        $("#persontaskstatusdisplay_"+result[0].taskParticipant[i].user_id).attr("statusOfTaskUser",result[0].taskParticipant[i].user_task_status);
        if(result[0].taskParticipant[i].user_id==userIdglb){
          $("#persontaskstatusdisplay_"+result[0].taskParticipant[i].user_id).attr('onclick','changeStatusOfUserTask('+taskId+','+prjid+',\'editView\',\''+result[0].task_type+'\');');
          $("#taskSlideAmount_"+userIdglb).attr('permanentvalue',result[0].taskParticipant[i].task_user_percentage);
          if(result[0].taskParticipant[i].user_task_status!="Inprogress"){
           
           
            let mouseup='changeStatusOfTaskToProgress('+taskId+', '+prjid+',\'Inprogress\', \'editslide\', '+result[0].task_type+')' 
            $("#slide_"+userIdglb).attr('onchange',mouseup)
            
          }

        }


        $("#user_id_"+ result[0].taskParticipant[i].user_id).attr('userstatus','E');
        $("#user_id_"+result[0].taskParticipant[i].user_id).find("#taskSlideAmount_"+result[0].taskParticipant[i].user_id).val(result[0].taskParticipant[i].task_user_percentage);
        $("#user_id_"+result[0].taskParticipant[i].user_id).find("#taskSlideAmount_"+result[0].taskParticipant[i].user_id).trigger('onblur');

        $("#user_id_"+result[0].taskParticipant[i].user_id).find(".estimateHour").val(result[0].taskParticipant[i].task_est_hours);
       
        $("#user_id_"+result[0].taskParticipant[i].user_id).find(".estimateMin").val(result[0].taskParticipant[i].task_est_mins);

        if(parseInt(result[0].taskParticipant[i].task_est_hours)>0 || parseInt(result[0].taskParticipant[i].task_est_mins)>0){
          $("#user_id_"+result[0].taskParticipant[i].user_id).attr('modified','modified');
        }


        let actualTime=(parseInt(result[0].taskParticipant[i].working_hours)*60)+parseInt(result[0].taskParticipant[i].working_minutes);

        let actualtimehour=Math.floor(actualTime/60);
        let actualtimeMin=Math.floor(actualTime%60);
       // $("#user_id_"+result[0].taskParticipant[i].user_id).find(".actualhour").text(actualTime);
       $("#user_id_"+result[0].taskParticipant[i].user_id).find(".acthour").val(actualtimehour);
       $("#user_id_"+result[0].taskParticipant[i].user_id).find(".actmin").val(actualtimeMin);

       //distributeEstimateTime();

      }

      /// for Approvers
      console.log(result[0].approval_id);
      for(i=0;i<result[0].approval_id.length;i++){
        let imgName = lighttpdpath + "/userimages/" + result[0].approval_id[i].user_id+"."+result[0].approval_id[i].user_image_type + "?" + d.getTime();
      //   // alert(calculateElapseTime(result[0].taskParticipant[i].elapsed_time) +"----"+result[0].taskParticipant[i].elapsed_time)
        // let stD=result[0].approval_id[i].requested_date.split(" ")[1]+" "+result[0].approval_id[i].requested_date.split(" ")[0]+","+" "+result[0].approval_id[i].requested_date.split(" ")[2];
        // console.log("stD-->"+stD);
        let stTime=result[0].approval_id[i].requested_time.split(" ")[0];
        let stM=result[0].approval_id[i].requested_time.split(" ")[1];
        let stTime1=stTime+":00"+" "+stM;
        let startDate1=returnFormatedDate(result[0].approval_id[i].requested_date);
        let startTime1=returnFormatedTime(stTime1);
        let appD="";
        let ap1Time1="";

        if(result[0].approval_id[i].approved_date=="" && result[0].approval_id[i].approved_time==""){
          appD="";
          ap1Time1="";
        }else{
          appD=returnFormatedDate(result[0].approval_id[i].approved_date);
          

          let apTime=result[0].approval_id[i].approved_time.split(" ")[0];
          let apM=result[0].approval_id[i].approved_time.split(" ")[1];
          let apTime1=apTime+":00"+" "+apM;
          ap1Time1=returnFormatedTime(apTime1);
        }
        requDate=startDate1;
        console.log("requDate---->"+requDate);
        
        // console.log(calculateElapseTime(result[0].approval_id[i].elapsed_time));
        // console.log(startDate1+"----"+startTime1);
        // console.log(appD+"----"+ap1Time1);

        addUserToData1(prjid, result[0].approval_id[i].user_full_name, result[0].approval_id[i].user_id, '',imgName,'edit',taskId,calculateElapseTime(result[0].approval_id[i].elapsed_time),startDate1,appD,"","E",result[0].approval_id[i].sequence,result[0].approval_id[i].user_task_status,startTime1,ap1Time1);
        
        if(result[0].approval_id[i].user_task_status=="Approved"){
          $("#calenderForTask2_"+result[0].approval_id[i].user_id).removeAttr("onclick");
        }else if(result[0].approval_id[i].user_task_status=="Rejected"){
          $("#calenderForTask2_"+result[0].approval_id[i].user_id).removeAttr("onclick");
        }else if(result[0].approval_id[i].user_task_status=="Cancel"){
          $("#calenderForTask2_"+result[0].approval_id[i].user_id).removeAttr("onclick");
        }

        // $("#resquestTimeCalNewUI_"+result[0].approval_id[i].user_id).attr('permanentValue',startTime1);
        // $("#resquestDateCalNewUI_"+result[0].approval_id[i].user_id).attr('permanentValue',startDate1);


        // $("#resquestTimeCalNewUI_"+result[0].approval_id[i].user_id).text(startTime1);
        // $("#resquestDateCalNewUI_"+result[0].approval_id[i].user_id).text(startDate1);

        // if(result[0].approval_id[i].user_task_status=="Pending approval"){
        //   // apprImgPath="/images/task/pending_approval.svg";
        //   // appTitle="Pending approval";
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('src','/images/task/pending_approval.svg');
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('title','Pending approval');
        // }else if(result[0].approval_id[i].user_task_status=="Approved"){
        //   // apprImgPath="/images/task/approved.svg";
        //   // appTitle="Approved";
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('src','/images/task/approved.svg');
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('title','Approved');
        // }else if(result[0].approval_id[i].user_task_status=="Rejected"){
        //   // apprImgPath="/images/task/rejected.svg";
        //   // appTitle="rejected";
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('src','/images/task/rejected.svg');
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('title','Rejected');
        // }else if(result[0].approval_id[i].user_task_status=="On hold"){
        //   // apprImgPath="/images/task/on_hold.svg";
        //   // appTitle="On hold";
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('src','/images/task/on_hold.svg');
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('title','On hold');
        // }else if(result[0].approval_id[i].user_task_status=="Cancel"){
        //   // apprImgPath="/images/task/cancel.svg";
        //   // appTitle="Cancel";
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('src','/images/task/cancel.svg');
        //   $('#appNewImg_'+result[0].approval_id[i].user_id).attr('title','Cancel');
        // }


        // $('#rolenew_'+result[0].approval_id[i].user_id).val(result[0].approval_id[i].user_task_status);
        // console.log(result[0].approval_id[i].approval_status);
      //   let image=result[0].taskParticipant[i].user_task_status;
      //   if(image=="Paused"){
      //     image="images/task/user-pause.svg"
      //   }else if(image=='Inprogress'){
      //     image="images/task/user-inprogress.svg"
      //   }else if(image=='Completed'){
      //     image="images/task/user-completed.svg"
      //   }else{
      //     image="images/task/user-start.svg"
      //   }
        
      //   $("#persontaskstatusdisplay_"+result[0].taskParticipant[i].user_id).attr('src',image);
      //   $("#persontaskstatusdisplay_"+result[0].taskParticipant[i].user_id).attr("statusOfTaskUser",result[0].taskParticipant[i].user_task_status);
      //   if(result[0].taskParticipant[i].user_id==userIdglb){
      //     $("#persontaskstatusdisplay_"+result[0].taskParticipant[i].user_id).attr('onclick','changeStatusOfUserTask('+taskId+','+prjid+',\'editView\',\''+result[0].task_type+'\');');
      //     $("#taskSlideAmount_"+userIdglb).attr('permanentvalue',result[0].taskParticipant[i].task_user_percentage);
      //     if(result[0].taskParticipant[i].user_task_status!="Inprogress"){
           
           
      //       let mouseup='changeStatusOfTaskToProgress('+taskId+', '+prjid+',\'Inprogress\', \'editslide\', '+result[0].task_type+')' 
      //       $("#slide_"+userIdglb).attr('onchange',mouseup)
            
      //     }

      //   }


      //   $("#user_id_"+ result[0].taskParticipant[i].user_id).attr('userstatus','E');
      //   $("#user_id_"+result[0].taskParticipant[i].user_id).find("#taskSlideAmount_"+result[0].taskParticipant[i].user_id).val(result[0].taskParticipant[i].task_user_percentage);
      //   $("#user_id_"+result[0].taskParticipant[i].user_id).find("#taskSlideAmount_"+result[0].taskParticipant[i].user_id).trigger('onblur');

      //   $("#user_id_"+result[0].taskParticipant[i].user_id).find(".estimateHour").val(result[0].taskParticipant[i].task_est_hours);
       
      //   $("#user_id_"+result[0].taskParticipant[i].user_id).find(".estimateMin").val(result[0].taskParticipant[i].task_est_mins);

      //   if(parseInt(result[0].taskParticipant[i].task_est_hours)>0 || parseInt(result[0].taskParticipant[i].task_est_mins)>0){
      //     $("#user_id_"+result[0].taskParticipant[i].user_id).attr('modified','modified');
      //   }


      //   let actualTime=(parseInt(result[0].taskParticipant[i].working_hours)*60)+parseInt(result[0].taskParticipant[i].working_minutes);

      //   let actualtimehour=Math.floor(actualTime/60);
      //   let actualtimeMin=Math.floor(actualTime%60);
      //  // $("#user_id_"+result[0].taskParticipant[i].user_id).find(".actualhour").text(actualTime);
      //  $("#user_id_"+result[0].taskParticipant[i].user_id).find(".acthour").val(actualtimehour);
      //  $("#user_id_"+result[0].taskParticipant[i].user_id).find(".actmin").val(actualtimeMin);

      //  //distributeEstimateTime();

      }
      if(sprinttype=="History"){
        $('#savetask, #deleteTask, #hLigthTaskVoiceStop, #delete_group').removeAttr('onclick').css('opacity','0.5');
        $("[id^='slide_']").attr('disabled','disabled');
        $("[id^='persontaskstatusdisplay_1']").removeAttr('onclick');
        $("#userupload, #edit, #user_delete").removeAttr('onclick').css('opacity','0.5');
        $('.workinghour, .workingMinutes, .userslide, .estimateHour, .estimateMinute, #taskname, #taskdesc').attr('readonly',true);
        $('.priorityoption').removeAttr('onclick');
      }

      if(result[0].taskParticipant != ""){
        if(typeof(result[0].taskParticipant[0].historyData)!="undefined"){
          if(result[0].task_type=="Sprint" && result[0].taskParticipant[0].historyData.length>0){
            $("#historyicon").parent().removeClass('d-none');
            
            $(".taskDocument").css('max-height','130px')
            prepareTaskHistoryinTask(result[0].taskParticipant[0].historyData);
          }
        }
      }
      
      $("#dependencyTask").attr('depTasks',result[0].dependencyTaskId);

      let userData="[";
      let userData1="[";

      $('.userDetails > div').each(function () { 
        if($(this).attr('id')!='ignorediv'){
         // alert($(this).attr('id'));
          var id = $(this).attr('id').split('_')[2];
          userData+="{";
          userData+="\"id\":\""+id+"\",";
          userData+="\"status\":\""+$(this).attr('userstatus')+"\",";
          userData+="\"progress\":\""+$(this).find('#taskSlideAmount_'+id).val()+"\",";
          userData+="\"totalEstHr\":\""+$(this).find('.estimateHour').val()+"\",";
          userData+="\"totalEstmin\":\""+$(this).find('.estimateMin').val()+"\"";
        // userData+="\"userAction\":\""+$(this).find(".userAction").html()+"\"";dependenctTaskIdstotalEstHr
          userData+="},";
        }
        
      });
      userData=userData.length>1?userData.substring(0,userData.length-1)+"]":"[]";
  
  
      $('.userDetails1 > div').each(function () { 
        if($(this).attr('id')!='ignorediv'){
          var id = $(this).attr('id').split('_')[2];
          // var role = $(this).find('#commentApprove_'+id).val();
          // console.log("role-->>"+role);
          // if(userArray1.includes(id)==false){
            // userArray1.push(id);
            
            userData1+="{";
            userData1+="\"user_id\":\""+id+"\",";
            userData1+="\"task_user_status\":\""+$(this).find('#rolenew_'+id).val()+"\",";
            userData1+="\"sequence\":\""+$(this).attr('secuenceNumber')+"\"";
            userData1+="},";
    
          // }
        }
        
      });
  
      userData1=userData1.length>1?userData1.substring(0,userData1.length-1)+"]":"[]";

      if(userData=='[]' && userData1=='[]'){
        $(".task_user_new_div").hide();
        $(".task_user_new_div1").hide();
      }else if(userData!='[]' && userData1!='[]'){
        console.log("both are pre");
        $(".task_user_new_div").show();
        $(".task_user_new_div1").hide();
      }else if(userData=='[]' && userData1!='[]'){
        $(".task_user_new_div").hide();
        $(".task_user_new_div1").show();
      }else if(userData!='[]' && userData1=='[]'){
        $(".task_user_new_div").show();
        $(".task_user_new_div1").hide();
      }
      

    }

    
  });
}
function returnFormatedDate(date){
  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	let changeDate=date;
  changeDate=monthNames[parseInt(changeDate.split("-")[0])]	+" "+changeDate.split("-")[1]+", "+changeDate.split("-")[2]	;		  
  return changeDate;
}

function returnFormatedTime(time){
  let fullTime=(time.split(":")[0])+":" +(time.split(":")[1])+" "+(time.split(":")[2]).substring((time.split(":")[2]).length-2,(time.split(":")[2]).length);
  return fullTime;
}

function getTaskUserComment(taskId,user_id,sprintHistory,place){
  // $(".taskcolordiv").find('.selectedit').find('.material-textfield-label').css('background','#FFF');
  // $(".taskcolordiv").removeClass('taskcolordiv');
  // $("#user_id_"+user_id).addClass('taskcolordiv')
  // $("#user_id_"+user_id).find('.selectedit').find('.material-textfield-label').css('background','#eef5f7');
  colorthisdiv($("#user_id_"+user_id))
  if(($("#user_id_"+user_id).find(".usercontainer").find(".usercommentdiv").is(":hidden") || $("#user_id_"+user_id).find(".usercontainer").find(".usercommentdiv").length==0) && ($("#user_id1_"+user_id).find(".usercontainer").find(".usercommentdiv").is(":hidden") || $("#user_id1_"+user_id).find(".usercontainer").find(".usercommentdiv").length==0)){
    //alert("if");
    $("#user_id_"+user_id).find(".usercommentdiv").remove();
    var localOffsetTime = getTimeOffset(new Date());
    $.ajax({
      url: apiPath+"/"+myk+"/v1/getUserComments?task_id="+taskId+"&user_id="+user_id+"&sprintHistoryValue="+sprintHistory+"&localoffsetTimeZone="+localOffsetTime+"",
      type:"GET",
      error: function(jqXHR, textStatus, errorThrown) {
              checkError(jqXHR,textStatus,errorThrown);
              $("#loadingBar").hide();
              timerControl("");
              }, 
      success:function(result){
      let UI=prepareUserCommentUI(result,taskId,user_id,place);
     
      //$(".userdocumentdiv").hide();
      if(place=="assign"){
        $("#user_id_"+user_id).find(".usercontainer").append(UI);
      }else{
        $("#user_id1_"+user_id).find(".usercontainer").append(UI);
      }
      
      
      
      //editSelect(user_id,taskId);
      fetchUserLevelDoc('',taskId,user_id)
        
      }
    });
    //$("#user_id_"+user_id).find(".selectedit").toggle();
  }else{
    $("#user_id_"+user_id).find(".usercommentdiv").toggle();
    $("#user_id1_"+user_id).find(".usercommentdiv").toggle();
  }
  //else{
    //alert("else");
    
  //}
}
 function prepareUserCommentUI(result,taskId,user_id,place){
  
    let UI=""
    let userPercentage="";
    let userHour="";
    let userMin="";
    let hmUI="";
    $(".usercommentdiv").remove();
    UI+="<div class='usercommentdiv noselect  p-1  mt-1 ml-0 '  ondblclick='event.stopPropagation();' style='border-top:1px solid #e2e2e2;background:#FFF;'>"
    let disabled=user_id!=userIdglb?"disabled='disabled'":""
    if($("#user_id_"+user_id).find(".estimateHour").attr('disabled')=='disabled'){
      disabled="disabled='disabled'";
    }
    if(result.length>0){
      for(i=0;i<result.length;i++){
        if(result[i].comment_type=="User Level Comment"){
          userPercentage=result[i].user_percentage;
          userHour= result[i].working_hours;
          userMin= result[i].working_minutes;
          if(place=="approv"){
            userPercentage = "-"
            userHour = "-";
            userMin = "-";
            hmUI="-";
          }else{
            userPercentage=userPercentage+'&nbsp;%';
            userHour=userHour;
            userMin=userMin;
            hmUI='<input id="workingHours" style="width:22px;border:0;text-align: right;border-bottom:1px solid rgb(193, 197, 200);background:#FFF" disabled="disabled" value="'+userHour+'"/> <div class="ml-1" style="float:left;color:black;width:16px;margin-top: 1px;">h</div> <input id="workingMinutes" onkeyup="validateMinute(this)" style="width:17px;border:0;text-align: right;border-bottom:1px solid rgb(193, 197, 200);background:#FFF" disabled="disabled" value="'+userMin+'"/><div  class="ml-1" style="float:left;color:black;width:16px;margin-top: 1px;">m</div>';

          }

           UI+='<div class="row py-2  pl-3 comment" onmouseover="changeColorofComment(this,\'on\');" onmouseleave="changeColorofComment(this,\'off\');" id="task_comment_'+result[i].task_comment_id+'" style="border-bottom:1px solid #e2e2e2;;    margin-right: -6px;    margin-left: -3px;" >'
            UI+='<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 d-flex p-0" style="max-width:17%">'
              
                    +'<div class="pr-2" >'+result[i].commented_date+'</div>'
                    +'<div class="mt-1" style="border-right:1px solid black;height:10px;"></div>'
                    +'<div class="ml-2">'+result[i].sort_by_date+'</div>'
              
              +'</div>'
              +'<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 d-flex   " style="justify-content: flex-start;max-width: 11%;padding-right: 0px;padding-left: 0px;" >'+userPercentage+'</div>'
              +'<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 d-flex " style="max-width: 12%;padding-left: 6px;padding-right: 0px;">'+hmUI+'</div>'
              +'<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 d-flex defaultExceedCls p-0 pl-1" style="max-width: 58%;" title="'+result[i].task_comment+'">'
                  +'<input id="taskcommentinput_'+result[i].task_comment_id+'" class="defaultExceedCls task_comment_user" style="width:100%;border:0;padding-left: 8px;background:#FFF" disabled="disabled" value="'+result[i].task_comment+'"/>'
              +'</div>'
          +'</div>'
        }else if(result[i].comment_type=="User Level Document"){
          let ext=result[i].task_comment;
          ext = ext.substr(ext.lastIndexOf(".")+1).toLowerCase();
          let onclick="";
          let ondoubleckick="";
          let createdTime=result[i].created_time;
          createdTime=createdTime.split(":")[0]+":"+createdTime.split(":")[1]+" "+createdTime.split(":")[2].toString().substring(createdTime.split(":")[2].toString().length-2,createdTime.split(":")[2].toString().length);

          let createdDate=result[i].commented_date;
          createdDate=createdDate.split(",")[0].split(" ")[1]+" "+createdDate.split(",")[0].split(" ")[0]+"  " +createdDate.split(",")[1];
          if(ext.toLowerCase() == "png" || ext.toLowerCase() == "jpg" || ext.toLowerCase() == "jpeg" || ext.toLowerCase() == "gif" || ext.toLowerCase() == "svg"){
           onclick="thumbnailOpenUi(this,"+result[i].document_id+",'"+ext+"',"+result[i].task_comment_id+",'task');"
           ondoubleckick="expandImage("+result[i].document_id+",'"+ext+"')";
          }else if(ext.toLowerCase() == "mp3" || ext.toLowerCase() == "wav" || ext.toLowerCase() == "wma" || ext.toLowerCase() == "mp4" || ext.toLowerCase() == ".m4v" || ext.toLowerCase() == "mov" || ext.toLowerCase() == "flv" || 
            ext.toLowerCase() == "f4v" || ext.toLowerCase() == "ogg" || ext.toLowerCase() == "ogv" || ext.toLowerCase() == "wmv" ||
            ext.toLowerCase() == "vp6" || ext.toLowerCase() == "vp5" || ext.toLowerCase() == "mpg" || ext.toLowerCase() == "avi" ||
            ext.toLowerCase() == "mpeg" || ext.toLowerCase() == "webm"){
            
          }else{
           
            onclick="viewordownloadUi("+result[i].document_id+",this,'"+ext+"',"+result[i].task_comment_id+",'task')";
          }

          UI+='<div class="row py-2  pl-3 document" onmouseover="changeColorofComment(this,\'on\');" onmouseleave="changeColorofComment(this,\'off\');"  id="task_document_'+result[i].task_comment_id+'" style="border-bottom:1px solid #e2e2e2;;    margin-right: -6px;    margin-left: -3px;" >'
            UI+='<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 d-flex p-0" style="max-width:17%">'
              
                    +'<div class="pr-2" >'+createdDate+'</div>'
                    +'<div class="mt-1" style="border-right:1px solid black;height:10px;"></div>'
                    +'<div class="ml-2">'+createdTime+'</div>'
              
              +'</div>'
              +'<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 d-flex   " style="justify-content: flex-start;max-width: 11%;padding-right: 0px;padding-left: 0px;" >-</div>'
              +'<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 d-flex " style="max-width: 12%;padding-right: 0px;padding-left: 10px;">-</div>'
              +'<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 d-flex defaultExceedCls p-0 pl-1" style="max-width: 58%;;    min-height: 28px;" title="'+result[i].task_comment+'">'
              UI+='<div class="d-flex pb-2  position-relative taskdoc" style="cursor: pointer;" id="doc_'+result[i].task_comment_id+'">'
              +'<div ><img  class="float-left  ml-2 " style="width:16px;" src="/images/document/'+ext+'.svg" onerror="imageOnFileNotErrorReplace(this)" id=""></div>'
              +'<div class="ml-2  defaultExceedCls" ondblclick="'+ondoubleckick+'"  onclick="'+onclick+'"  style="font-size:12px">'+result[i].task_comment+'</div>'
         +'</div>'
              +'</div>'
          +'</div>'
        }
      }
    }else{
      
      UI+='<div class="d-flex justify-content-center" id="no-comments">No Comments</div>'
    }
    UI+="</div>"
    
    return UI;
}
async function editSelect(user_id,taskId){
  let UI="";
    if($(".selectedit").length>0){
   
      $(".selectedit").remove();
    }
   // else{
      if(user_id==userIdglb){
        // alert("----") 
        let obj=$("#user_id_"+userIdglb);
        await $.ajax({
          url: apiPath + "/" + myk + "/v1/getCommentCodes?task_id=" + taskId + "&company_id=" + companyIdglb + "",
          type: "GET",
          error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
          },
          success: function (res) {
                 UI+="<div class='row noselect selectedit mb-1 mt-3' style='display:flex' ondblclick='event.stopPropagation();'>"
                           +'<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 ml-3" style="max-width: 13%;"></div>'
                           +'<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-left: 7px;max-width: 16%;padding-right: 7px;">'
                                  
                           +'<div class="material-textfield" >'
                                         +'<select  id="commentcode" class="material-textfield-input"  onchange="fillCommentText(this)" style="padding-left: 8px; background: none; border: 1px solid #C1C5C8;border-radius: 6px;height:30px;    outline: none;width:100%">'
                                         +'<option class="optionselect">Select</option>'
                                             for(i=0;i<res.length;i++){
                                                 UI+='<option class="optionselect" title="'+res[i].quotes+'" value="'+res[i].quotes+'">'+trancate(res[i].quotes,45)+'</option>' 
                                             }
                        
                        
                                   UI+='</select>'
                                   +'	<label class="material-textfield-label" style="margin-left: 5px; font-size:12px; background-color: ##FFF; ">Task code</label>'
                                +'</div>'
     
                           +'</div>'
                           +'<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 ml-0 p-0" style="max-width: 66%;">'
                                             
                          // +'<input id="UserCommentNewUI" placeholder="comments" type="text" style="width:100%;height:30px;    border: 1px solid #409bb5;border-radius: 3px;padding: 3px 3px 3px 5px;" />'

                          // +'<div class="defaultCls material-textfield taskfield " style="font-size:12px;padding:10px 0px 0px; border-bottom:1px solid #c1c5c8;">   <input type="text" id="UserCommentNewUI" class="material-textfield-input" = placeholder=" " style="/* width: 97%; */background: none;height: 20px;width:100%;resize:none;    padding: 0px 10px 5px 10px;overflow-y:hidden;border:0" />		<label class="material-textfield-label" style="margin-left: 5px;font: inherit;margin-bottom:0px">Comments</label></div>'

                          +'<div class="material-textfield" >'
                                  +'<textarea id="UserCommentNewUI"  type="text" class="theightdemo material-textfield-input"   placeholder=" " style="padding-left: 10px; background: none;border: 1px solid #C1C5C8;border-radius: 6px;resize: none;height:30px;    outline: none; width: 100%;padding-top:6px"></textarea>'
                                  +'<label class="material-textfield-label" style=" font-size: 12px; top: 16px; background-color: #FFF; ">Progress Comments</label></div>'
                                    
                           +'</div>'
     
     
                 +'</div>'
                 $(".selectedit").remove();
                 $("#user_id_"+user_id).find(".userselect").append(UI);
                 $("#UserCommentNewUI").on('keyup',()=>{
                  colorthisdiv(obj);
                 });
                 
          }
        });
        if($("#user_id_"+user_id).find(".estimateHour").attr('disabled')!='disabled' && user_id==userIdglb){
               
            editFunction(user_id,'on');
         }
    //}
 }
   

}
function fetchUserLevelDoc(sprintHistoryVal,taskId,user_id){
      //alert("here");       
  $.ajax({
            url:apiPath+"/"+myk+"/v1/getUserLevelDoc?sprintTaskType="+sprintHistoryVal+"&task_id="+taskId+"&user_id="+user_id+"",
            type:"GET",
            ///data:{act:"fetchTaskDocuments",taskId:currentTaskId,taskType: taskType,userId:uId,docLocation:docLocation,type:'myTask',sprintHistoryValueGlo:sprintHistoryVal},
            error: function(jqXHR, textStatus, errorThrown) {
                    checkError(jqXHR,textStatus,errorThrown);
                    $("#loadingBar").hide();
                    timerControl("");
                    }, 
            success:function(result){
              let UI=prepareUserDocUI(result); 
              //$(".usercommentdiv").hide();
             // $("#user_id_"+user_id).find(".usercontainer").append(UI);
              $(".userdocument").html(UI);
            }
        });
}

function prepareUserDocUI(resp){
    let UI=""; 
    // $(".userdocumentdiv").remove();
    
    // if(result.length>0){
    //   UI+='<div class="userdocumentdiv  mt-2 ml-5 pr-2 pt-2 pb-2" style="border-top: 1px solid black;border-bottom: 1px solid black;">'
    //   for(i=0;i<result.length;i++){
    //     let ext=result[i].document_name;
    //     var createdtime = result[i].created_time;
		// 		var crtime1 = createdtime.split(':')[0];
		// 		var crtime2 = createdtime.split(':')[1];
		// 		var crtime3 = createdtime.split(' ')[1];
		// 		var createdtime1 = crtime1+":"+crtime2+" "+crtime3;
    //     ext = ext.substr(ext.lastIndexOf(".")+1).toLowerCase();
    //       UI+='<div class="justify-content-around pb-1 mt-2" id="user_doc_'+result[i].task_document_id+'" style="display:flex;">'

    //       +'<div class="w-50" style="display:flex">'
    //           +'<div><img style="height:15px;width:15px" src="/images/document/'+ext+'.svg" /></div>'
    //           +'<div class="ml-2" title="'+result[i].document_title+'">'+result[i].document_name+'</div>'
    //       +'</div>'

    //       +'<div class="w-50  justify-content-between" style="display:flex">'
    //         +'<div><img style="height:25px;width:25px;margin-top: -5px" class="rounded-circle" src="'+result[i].createdByDetails[0].imagePathUrl+'" /></div>'
    //         +'<div title="'+result[i].created_date+'">'+result[i].created_date+'&nbsp;'+createdtime1+'</div>'
    //         +'<div onclick="removeDoc('+result[i].task_document_id+',\'Task\')"><img style="height:15px;width:15px;cursor:pointer" src="/images/menus/user-remove.svg" /></div>'
    //       +'</div>'


    //       +'</div>'
    //   }
    //   UI+='</div>'
      
    // }else{
     
      
    // }
    for(i=0;i<resp.length;i++){
      let ext=resp[i].document_name;
      ext = ext.substr(ext.lastIndexOf(".")+1).toLowerCase();
     // let onclick="";
     // let ondoubleckick="";
      
      if(ext.toLowerCase() == "png" || ext.toLowerCase() == "jpg" || ext.toLowerCase() == "jpeg" || ext.toLowerCase() == "gif" || ext.toLowerCase() == "svg"){
        onclickexpand="expandImage("+resp[i].document_id+",'"+ext+"','task')";
        onclickDownload=" downloadActFile("+resp[i].document_id+","+prjid+",'"+ext+"',"+resp[i].task_document_id+",'task')";
      }else if(ext.toLowerCase() == "mp3" || ext.toLowerCase() == "wav" || ext.toLowerCase() == "wma" || ext.toLowerCase() == "mp4" || ext.toLowerCase() == ".m4v" || ext.toLowerCase() == "mov" || ext.toLowerCase() == "flv" || 
        ext.toLowerCase() == "f4v" || ext.toLowerCase() == "ogg" || ext.toLowerCase() == "ogv" || ext.toLowerCase() == "wmv" ||
        ext.toLowerCase() == "vp6" || ext.toLowerCase() == "vp5" || ext.toLowerCase() == "mpg" || ext.toLowerCase() == "avi" ||
        ext.toLowerCase() == "mpeg" || ext.toLowerCase() == "webm"){
        
      }else{
       
        onclickexpand="viewdocument("+resp[i].document_id+",'"+ext+"','task')";
        onclickDownload=" downloadActFile("+resp[i].document_id+","+prjid+",'"+ext+"',"+resp[i].task_document_id+",'task')";
      }
      UI+='<div class="d-flex pb-2 mt-2 position-relative taskdoc" onmouseleave="hideGroupEdit('+resp[i].task_document_id+',\'userdocument\');" onmouseover="showGroupEdit('+resp[i].task_document_id+',\'userdocument\');" style="cursor: pointer;border-bottom:1px solid #EAEAEA" id="doc_'+resp[i].task_document_id+'">'
              +'<div ><img  class="float-left mt-1 ml-2 " style="width:24px;" src="/images/document/'+ext+'.svg" onerror="imageOnFileNotErrorReplace(this)" id="view_27623"></div>'
              +'<div class="ml-2 mt-1 defaultExceedCls" style="font-size:12px">'+resp[i].document_name+'</div>'
              +'<div id="subcommentDiv_userdoc_'+resp[i].task_document_id+'" class="actFeedOptionsDiv" style="border-radius: 8px; top: 1px; margin-right: 5px; display: none;"><div class="d-flex align-items-center"><img src="images/conversation/expand.svg" onclick="'+onclickexpand+'" title="View" style="width:15px;height:15px;" class="mx-1" /> <img src="images/conversation/download2.svg"  title="dounload" style="width:15px;height:15px;"  class="mx-1"  onclick="'+onclickDownload+'" /><img class="mx-1" title="Delete" onclick="deleteUserDoc('+resp[i].task_document_id+')" src="/images/task/minus.svg" id="delete_group" style="width:18px;height:18px;"></div></div>'
         +'</div>'
    }
    return UI;
    
}
function removeDoc(glbDocId,ttpe){
  $.ajax({ 
    url: apiPath+"/"+myk+"/v1/deleteUserDoc?task_doc_id="+glbDocId,
    type:"DELETE",
    success:function(result){
     $("#user_doc_"+glbDocId).remove();
    }
  });
}

async function updateTask(taskId){
  let taskName = $("#taskname").val();
  let taskdesc = $("#taskdesc").val();
  let task_type = $('#task_type').val();
  taskId=typeof(taskId)=='undefined'?$("#task_id").val():taskId;
  let priority=$(".changePriority").attr('priority');
  let priorityId = priority==0?"6":priority;

  let rem=$("#eventReminder option:selected").val();
  let reminder= $("#remindericon").attr('reminder');
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let userArray=[];
  let userArray1=[];
  let userData="[";
  $('.userDetails > div').each(function () { 
    if($(this).attr('id')!='ignorediv'){
      var id = $(this).attr('id').split('_')[2];
      if(userArray.includes(id)==false){
        userArray.push(id);

        let estH= $(this).find('.estimateHour').val();
        let estM= $(this).find('.estimateMin').val()
        if(estM>59){
          estH=parseInt(estH)+Math.floor(parseInt(estM)/60);
          estM=Math.floor(parseInt(estM)%60);
        }
        userData+="{";
        userData+="\"id\":\""+id+"\",";
        userData+="\"status\":\""+$(this).attr('userstatus')+"\",";
        userData+="\"progress\":\""+$(this).find('#taskSlideAmount_'+id).val()+"\",";
        userData+="\"totalEstHr\":\""+estH+"\",";
        userData+="\"totalEstmin\":\""+estM+"\"";
      // userData+="\"userAction\":\""+$(this).find(".userAction").html()+"\"";dependenctTaskIdstotalEstHr
        userData+="},";
      }
    }
    
  });

  


  
  userArray=[];
  

  let dependencyTaskId="";
  let existingDepTaskId="";
  $('.dependenctTaskIds > div').each(function () { 
    
   // if(typeof($(this).find(".imagecheck").attr('src')!='undefined')){
      if($(this).find('.check').prop("checked")==true){
        let depTask=$(this).attr('id').split("_")[1];
        dependencyTaskId=dependencyTaskId+depTask+":"+$(this).attr('select')+",";
        if($(this).attr('select')=='E'){
          existingDepTaskId=existingDepTaskId+depTask+",";
        }
      }
    //}
  });
  dependencyTaskId=dependencyTaskId.substring(0,dependencyTaskId.length-1);
  existingDepTaskId=existingDepTaskId.substring(0,existingDepTaskId.length-1);


  let saveDocTaskIds ="";
  $('.taskDocument > div').each(function () { 
    saveDocTaskIds=saveDocTaskIds+$(this).attr('id').split("_")[1]+","
  });
 
  userData=userData.length>1?userData.substring(0,userData.length-1)+"]":"[]";
  
  let removeEmailId=$("#removeEmail").attr('user');
 // alert(removeEmailId)
  let startDate=$("#startDateCalNewUI").text();
  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let startdateMonth=monthNames.indexOf(startDate.substring(0,3))<10?"0"+monthNames.indexOf(startDate.substring(0,3)):monthNames.indexOf(startDate.substring(0,3));
  let startdateDate=startDate.substring(4,6)
  let start=startdateMonth.toString().trim()+"-"+startdateDate.trim()+"-"+startDate.split(",")[1].trim();

  let endDate=$("#endDateCalNewUI").text();
  let enddateMonth=monthNames.indexOf(endDate.substring(0,3))<10?"0"+monthNames.indexOf(endDate.substring(0,3)):monthNames.indexOf(endDate.substring(0,3));
  let enddateDate=endDate.substring(4,6)
  let end=enddateMonth.toString().trim()+"-"+enddateDate.trim()+"-"+endDate.split(",")[1].trim();


  let startfullTime=$("#startTimeCalNewUI").text();
  let hour=startfullTime.substring(startfullTime.length-2,startfullTime.length)=="PM"?(parseInt(startfullTime.split(":")[0].trim())+12):startfullTime.split(":")[0].trim();
  let startTime=hour+":"+startfullTime.split(":")[1].trim();
  

  let endfullTime=$("#endTimeCalNewUI").text();
  let endhour=endfullTime.substring(endfullTime.length-2,endfullTime.length)=="PM"?(parseInt(endfullTime.split(":")[0].trim())+12):endfullTime.split(":")[0].trim();
  let endTime=endhour+":"+endfullTime.split(":")[1].trim();

  let estimateHour= $(".estimateHour").val();
  let estimateMin= $(".estimateMinute").val();

  if(estimateMin>59){
    estimateHour=parseInt(estimateHour)+Math.floor(parseInt(estimateMin)/60);
    estimateMin=Math.floor(parseInt(estimateMin)%60);
  }


  let userData1="[";
  $('.userDetails1 > div').each(function () { 
    if($(this).attr('id')!='ignorediv'){
      var id = $(this).attr('id').split('_')[2];
      var appStat=$(this).attr('appStatus');
      // var role = $(this).find('#commentApprove_'+id).val();
      // console.log("id-->>"+id);
      // console.log("task_user_status-->>"+$(this).find('#rolenew_'+id).val());
      // console.log("sequence-->>"+$(this).attr('secuenceNumber'));
      // console.log("type-->>"+$(this).attr('appStatus'));

      let startDate1=$("#resquestDateCalNewUI_"+id).text();
      var monthNames1 = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      let startdateMonth1=monthNames1.indexOf(startDate1.substring(0,3))<10?"0"+monthNames1.indexOf(startDate1.substring(0,3)):monthNames1.indexOf(startDate1.substring(0,3));
      let startdateDate1=startDate1.substring(4,6)
      let start1=startdateMonth1.toString().trim()+"-"+startdateDate1.trim()+"-"+startDate1.split(",")[1].trim();

      let startfullTime1=$("#resquestTimeCalNewUI_"+id).text();
      // let hour1=startfullTime1.substring(startfullTime1.length-2,startfullTime1.length)=="PM"?(parseInt(startfullTime1.split(":")[0].trim())+12):startfullTime1.split(":")[0].trim();
      // let startTime1=hour1+":"+startfullTime1.split(":")[1].trim();


      if(userArray1.includes(id)==false){
        userArray1.push(id);
        
        userData1+="{";
        userData1+="\"user_id\":\""+id+"\",";
        userData1+="\"task_user_status\":\""+$(this).find('#approveStatusImg_'+id).attr('title')+"\",";
        userData1+="\"sequence\":\""+$(this).attr('secuenceNumber')+"\",";
        if(appStat=="E"){
          userData1+="\"type\":\""+appStat+"\",";
          userData1+="\"requested_date\":\""+start1+"\",";
          userData1+="\"requested_time\":\""+startfullTime1+"\"";
        }else{
          userData1+="\"type\":\""+appStat+"\",";
          userData1+="\"requested_date\":\""+start1+"\",";
          userData1+="\"requested_time\":\""+startfullTime1+"\"";
        }
        
        userData1+="},";

      }
    }
    
  });
  userArray1=[];
  userData1=userData1.length>1?userData1.substring(0,userData1.length-1)+"]":"[]";


  let taskCompComm=$("#UserCommentNewUI").val();
  let workingHours=$(".workinghour").val();
  let workingMinutes=$(".workingminute").val();

  let group_id=$("#changeGroup").attr('group_id');
  // console.log(JSON.parse(userData1));
  let jsonbody = {
    "user_id": userIdglb,
    "company_id": companyIdglb,
    "task_type": task_type,
    "task_name": taskName,
    "start_date":start,
    "end_date":end,
    "place": "task",
    //"recEnd":recEnd,
    //"recType":recType,
    "start_time":startTime,
    "end_time":endTime,
    "task_desc": taskdesc,
    "taskProject": prjid,
    "priority": priorityId,
    //"selectedTaskemailId":selectedTaskemailId,
    "reminder":reminder,
    //"recPatternValue":recPatternValue,
     "total_estimated_hour":estimateHour,
     "total_estimated_minute":estimateMin,
    "removeEmailId":removeEmailId,
    //"sprintGrpId":sprintGrpId,
    // "sprintId":sprintId,
     "saveDocTaskIds":saveDocTaskIds,
    // "linkSavedIds":linkSavedIds,
     "dependencyTaskId":dependencyTaskId,
    "existingDepTaskId":existingDepTaskId,
    //"source_id":sourceId,
    // "taskNameToFill":taskNameToFillGlobal,
    "taskDate": [],
    "userData": JSON.parse(userData),
    "approval_id" : JSON.parse(userData1),
    "project_id": prjid,
    "event_id":taskId,
    "taskCompComm":taskCompComm,
    "workingHours":workingHours,
    "workingMinutes":workingMinutes,
    "task_group_id":group_id
  }
  console.log(jsonbody);
  await $.ajax({
    url: apiPath + "/" + myk + "/v1/updateTask",
    //url:"http://localhost:8080/v1/updateTask",
    type: "PUT",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
    },
    success: function (resp) {
      sprintTaskinTask="";
      
      if($(".elem-CalenStyle").length!=0 || valueForView=="calendarTask"){
        $(".elem-CalenStyle").show();
        $(".TaskDiv").hide();
        /* $(".calendar-2").html("");
        $(".calendar-2").removeClass("elem-CalenStyle");
        calenTask('calendartask');
        closeCalenPopup();  */
      }else{
        
        $("#headerTask").addClass('d-flex').removeClass('d-none');
      }  
      $("#headerTask, #taskList, .popupTaskDiv").show();
      // alert("here");
      // console.log(resp[0].approval_id);
      $(".addTaskDiv").fadeOut(300, function(){ $(this).remove();});
      
    
     // $("#taskList").prepend(createTaskUI(resp));
     if(task_type=='Workflow'){
      $("#WFtask_"+resp[0].workflow_id).remove();
    }else{
      console.log("task is deleted");
       $("#task_"+taskId).remove();
    }
     if(valueForView=="list"){
          // console.log("inside list!!!!!!!");
         $("#taskList").prepend(createTaskUI(resp));
     }else if(valueForView=="grid"){
      $("#innerTaskList").prepend(createTaskGridUI(resp));
      //  $("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=taskPlay_]").removeAttr("onclick");
      //  $("#taskList").find("div[id^=opentaskParticipantdiv_]").find("img[id^=tasklistcheckbox_]").removeAttr("onclick");
     }else{
      // console.log("inside list!!!!!!!");
      $("#taskList").prepend(createTaskUI(resp));
     }
     if(globalhlmenu=="Highlight"){
      closeHLTask();
      // closeHightlightCmePopUp();
      $('#hl_'+globalhid).find('.hTaskIcon').show();
      // viewHighlight(globalhlmsgid);
     }
     if(gloablmenufor=='Myzone'){
       
          if(valueForView=="grid"){
            if(resp[0].project_id!=0 || resp[0].project_id!="undefined"){
              $(".tasklistProjectImage").show();
            }
        }else{
          $(".tasklistProjectImage").show();
        }
      }
      if(valueForView!="calendarTask"){
        $('#loadingBar').addClass('d-none').removeClass('d-flex');
      }
      let   saveCommentStatus="N";
      // if($('#UserCommentNewUI').length>0 && $('#UserCommentNewUI').val()!=''){
      //   saveCommentStatus="Y";
      //   updateListViewTask(taskId, task_type, prjid, '', 'edit',saveCommentStatus)
      // }else if($('#UserCommentNewUI').length>0){
      //   //alert("calling")
      //   updateListViewTask(taskId, task_type, prjid, '', 'edit',saveCommentStatus)
      // }
      let condition=false;
    //  alert(($(".workinghour").attr('permanentvalue')!=$(".workinghour").val()) || ($(".workingMinutes").attr('permanentvalue')!=$(".workingMinutes").val()));
      // alert($(".workinghour").attr('permanentvalue')) +"----"+($("#user_id_"+userIdglb).find(".workinghour").val());
       //alert($(".workingMinutes").attr('permanentvalue')) +"----"+($("#user_id_"+userIdglb).find(".workingMinutes").val())
      // alert(($("#user_id_"+userIdglb).find(".workingMinutes").val()) != $("#user_id_"+userIdglb).find(".workingMinutes").attr('permanentvalue'));
      if($("#taskSlideAmount_"+userIdglb).attr('permanentvalue')!=$("#taskSlideAmount_"+userIdglb).val()){
        saveCommentStatus="Y";
        condition=true;
      }else if(($("#user_id_"+userIdglb).find(".workinghour").val()) != $("#user_id_"+userIdglb).find(".workinghour").attr('permanentvalue')){
        saveCommentStatus="Y";
        condition=true;
      }else if(($("#user_id_"+userIdglb).find(".workingMinutes").val()) != $("#user_id_"+userIdglb).find(".workingMinutes").attr('permanentvalue')){
        saveCommentStatus="Y";
        condition=true;
      }
      else if($('#UserCommentNewUI').length>0 && $('#UserCommentNewUI').val()!=''){
        
         condition=true;
      }if($('#UserCommentNewUI').val()!="" && typeof($('#UserCommentNewUI').val())!='undefined'){
        saveCommentStatus="Y";
        condition=true;
      }
      // console.log(resp[0].approval_id);
      
      //alert(condition);
      // if($('.userDetails1 > div').find('#commentApprove_'+id).val()!=""){
      //   updateListViewTask(taskId, task_type, prjid, '', 'edit1',saveCommentStatus)
      // }else{
        updateListViewTask(taskId, task_type, prjid, '', 'edit',saveCommentStatus)
      // }
      // console.log("userid--->"+resp[0].approval_id[0].user_id);
      for(let i=0;i<resp[0].approval_id.length;i++){
        // console.log("userid--->"+resp[0].approval_id[i].user_id);
        // $('#rolenew1_'+resp[0].approval_id[i].user_id+resp[0].task_id).val(resp[0].approval_id[i].user_task_status);

        if(resp[0].approval_id[i].user_task_status=="Pending approval"){
          // apprImgPath="/images/task/pending_approval.svg";
          // appTitle="Pending approval";
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/pending_approval.svg');
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','Pending approval');
          $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).hide();
        }else if(resp[0].approval_id[i].user_task_status=="Approved"){
          // apprImgPath="/images/task/approved.svg";
          // appTitle="Approved";
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/approved.svg');
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','Approved');
          $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).show();
        }else if(resp[0].approval_id[i].user_task_status=="Rejected"){
          // apprImgPath="/images/task/rejected.svg";
          // appTitle="rejected";
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/rejected.svg');
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','Rejected');
          $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).hide();
        }else if(resp[0].approval_id[i].user_task_status=="On hold"){
          // apprImgPath="/images/task/on_hold.svg";
          // appTitle="On hold";
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/on_hold.svg');
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','On hold');
          $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).hide();
        }else if(resp[0].approval_id[i].user_task_status=="Cancel"){
          // apprImgPath="/images/task/cancel.svg";
          // appTitle="Cancel";
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('src','/images/task/cancel.svg');
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).attr('title','Cancel');
          $('#appListCheckBox_'+resp[0].approval_id[i].user_id+resp[0].task_id).hide();
        }


        if(resp[0].approval_id[i].user_id==userIdglb){

        }else{
          // console.log("userid--->"+resp[0].approval_id[i].user_id+" task---->"+resp[0].task_id);
          $('#appNewImg_'+resp[0].approval_id[i].user_id+resp[0].task_id).removeAttr("onclick");
        }
      }

      if(glbsprintplace=="SB"){
        showSprintScrumboard($('#sprintname').attr('tsprintid'));
      }
      
    }
  });
}
function cancelEdit(id){
  $("#headerTask, #taskList, .popupTaskDiv").show();
  $("#headerTask").addClass('d-flex').removeClass('d-none');
  $(".addTaskDiv").fadeOut(300, function(){ $(this).remove();});
  $("#task_"+id).show();
  $(".headerBar").css('position','sticky')
  if(globalhlmenu=='Highlight'){
    closeHLTask();
  }
  if(valueForView=='list'){
    $("#task_"+id).css('background-color','');
  }else{
    $("#task_"+id).removeClass('taskNodeSelect');
  }
  if(globalmenu=='Task'){
    clearWFData();
  }
  if($(".elem-CalenStyle").length!=0){
    $(".elem-CalenStyle").show();
    $(".TaskDiv").hide();
  }
  sprintTaskinTask="";
}
function showInput(obj){
  let id=$(obj).attr('id').split("_")[1];
  $("#taskcommentinput_"+id).removeClass('d-none').addClass('d-block');
  $("#taskcommentuser_"+id).removeClass('d-block').addClass('d-none');
  $("#taskcommentinput_"+id).focus();
}
function commentUpdate(obj){
  let id=$(obj).attr('id').split("_")[1];
  $("#taskcommentuser_"+id).text($("#taskcommentinput_"+id).val());
}
function showInstruction(){
  $(".instruction").toggle();
}
function deleteTask(taskId,tasktype,status){
  if(status!='Y'){
    confirmFunNew("Are you sure you want to delete this task?","delete","deleteTask",taskId,tasktype,'Y');
  }
  if(status=='Y'){
    let jsonbody = {
      "task_id":taskId+",",
      "task_type":tasktype,
      "user_id":userIdglb,
      "company_id":companyIdglb
  
  }
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  $.ajax({ 
    url: apiPath+"/"+myk+"/v1/deleteTask",
    type:"DELETE",
    contentType:"application/json",
  data: JSON.stringify(jsonbody),
    ///data:{taskId:tskId,taskType:tskType, act:'deleteTask',confirmation:confirmation, levelId:levelIdWFGlobal, storyId:ideaStoryIdGlo,sprintGrpId:forSelectingsprintGrpId},
    error: function(jqXHR, textStatus, errorThrown) {
            checkError(jqXHR,textStatus,errorThrown);
            $('#loadingBar').addClass('d-none').removeClass('d-flex');
            }, 
    success:function(result){

      if(globalhlmenu=='Highlight'){
        closeHLTask();
        $('#hl_'+globalhid).find('.hTaskIcon').hide();
      }
      
      if($(".elem-CalenStyle").length!=0 || valueForView=="calendarTask"){
        $("#addTaskHolderDiv").html("");
        $(".elem-CalenStyle").html("");
        $(".TaskDiv").hide(); 
        calenTask('calendartask');
        closeCalenPopup(); 
        
        /* $(".calentaskid_"+taskId).remove();
        $(".cmvTableContainer").find(".calentaskid_"+taskId).remove(); */
      }else{
        $("#headerTask").addClass('d-flex').removeClass('d-none');
      }
      if(valueForView!="calendarTask"){
        $("#headerTask, #taskList, .popupTaskDiv").show();
        sprintTaskinTask="";
        cancelEdit(taskId);
        $("#task_"+taskId).remove();


        $('#loadingBar').addClass('d-none').removeClass('d-flex');
      }
      
      
    }
    });  
  }  
}
function userSearch(){
  $("#invite").show();
  $("#invite").focus();
  $(".textinvite").hide();
  $(".searchclass").addClass('w-100');
}
function searchUser(obj){
  var value = $(obj).val().toLowerCase();
  $(".participant  *").filter(function() {
      $(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });
}

function prepareTaskHistoryinTask(json, taskType){
  let UserUI = "";
  UserUI += '<div class="UserHistory" style="font-size:12px;border-bottom: 1px solid #b4adad;">'
 
  UserUI+='<div class="row mx-0 " >'
          +'<div  id="ignorediv" style="width:100%;box-shadow: rgb(0 0 0 / 18%) 0px 2px 6px;                                 height: 28px;border:1px solid darkgray"  >'


            + '<div class="row  py-1 pl-2 pr-1    ml-1 pl" style="width:100%;">'
            // +'<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 row">'background-color:darkgrey;
              // + '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>'
              + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="max-width: 9%;padding: 0px 10px;">Assignee</div>'
              + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 d-flex p-0" style=" max-width: 15%;">Progress</div>'
              //+ '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding-left:22px; max-width: 13%;">Actual</div>'
              + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style=" max-width: 12%;">Estimate</div>'
              + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding:0px;max-width: 12%;" >Total Actual</div>'
              //+ '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding:0px" >Elapsed</div>'

            //+'</div>'
            + '</div>'
          + '</div>'
  UserUI+= '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0 defaultScrollDiv userHistoryDetails " style="height:200px; overflow-y:auto; overflow-x: hidden;">'
           +'</div>'  

       +'</div>'
    +'</div>'
    $(".task_user_history").append(UserUI);
    //$(UserUI).append(".task_user_history");
    UserUI = "";
    for(i=0;i<json.length;i++){ 
      UserUI = "";
      UserUI+="<div class='w-100' id='sprint_"+json[i].sprintId+"'>" 
        
          +'<div class="history_header Header d-flex ">'
          
              +'<div class="px-2"> History-'+json[i].sprint_title+'</div>'
              +'<div class="px-2">'
                +'<span class="mx-1">Est-</span>'
                +'<span class="mx-1">'+json[i].total_estimated_hour+'</span>'
                +'<span class="mx-1">h<span>'
                +'<span class="mx-1">'+json[i].total_estimated_minute+'</span>' 
                +'<span class="mx-1">m</span>' 
              +'</div>'
              +'<div class="px-2 d-flex">'
                +'<div>Act </div>'
                +'<div class="user_act_hour"> </span>'  
             + '</div>'

          +'</div>'

          
        
        
        +"</div>"
        +'<div class="historydetails"></div>'
        +'</div>'
        $(".userHistoryDetails").append(UserUI);
        
        for(j=0;j<json[i].userhistoryData.length;j++){
          UserUI="";
          let userid=json[i].userhistoryData[j].user_id;
          
          let imageSource=lighttpdpath+"/userimages/"+userid+"."+json[i].userhistoryData[j].user_image_type+"?"+d.getTime();
          console.log(imageSource);
          let userName=json[i].userhistoryData[j].user_full_name;
          
          let image=json[i].userhistoryData[j].user_task_status;
          let actualMinute=json[i].userhistoryData[j].working_hours+" h "+json[i].userhistoryData[j].working_minutes+" m";
          if(image=="Paused"){
            image="images/task/user-pause.svg"
          }else if(image=='Inprogress'){
            image="images/task/user-inprogress.svg"
          }else{
            image="images/task/user-start.svg"
          }
          /*
          UserUI+='<div class="row  p-1  mt-1 ml-1 "  >'
                  + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">'
                        +'<div class="d-flex">'
                          +'<img id="personimagedisplay" src="'+imageSource+'" onerror="imageOnProjNotErrorReplace(this);" class="mr-3 rounded-circle " style="width:30px;height:30px;">'
                          +'<div class="mt-1">'+userName+'</div>'
                        +'</div>'
                +'</div>'
                + '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">'
                      +'<img src="'+image+'"  class="mr-3  " style="width:25px;cursor:pointer">'
                +'</div>'
                 + '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 d-flex" style="padding-right:0px">'
                      +'<input  class="sliderange mt-2 mr-2" type="range" min="0" max="100" style="color: black; width: 70%; background: linear-gradient(90deg, rgb(165, 235, 45) 2%, rgb(0,0,0) 2%);padding-left: 0px !important;border-bottom:1px solid #c1c5c8;" value="'+json[i].userhistoryData[j].task_user_percentage+'">'
              
                      UserUI+='<input value="'+json[i].userhistoryData[j].task_user_percentage+'"  disabled="disabled" class="ml-2"  style="border:none;height:20px;outline:0; width:28px;font-size:90%;text-align:center;color:black;border-bottom:1px solid  #c1c5c8;"/>'
                      
                      UserUI+='<div class="ml-2" >%</div>'
                +'</div>'
                + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 " style="padding-left:10px">'
                      +'<input type="text" class="estimateHour" value="'+json[i].userhistoryData[j].task_est_hours+'" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:22px;font-size:90%;text-align:center;color:black;outline: 0;border-width: 0 0 1px;" ><div class="hour ml-1" style="float:left;color:black;width:25px;margin-top: 1px;">&nbsp;h</div><input class="estimateMin" type="text"  value="'+json[i].userhistoryData[j].task_est_mins+'" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:22px;font-size:90%;text-align:center;color:black;outline: 0;border-width: 0 0 1px;"><div class="min ml-1 " style="float:left;color:black;width:25px;">&nbsp;m</div>'
                +'</div>'
                + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 actualhour " style="color:darkgray" >'+actualMinute+'</div>'
         +"</div>"
          */

         UserUI+='<div class="row  px-1 pt-3 pb-1 mt-0 ml-1 " id="user_id_'+userid+'" userStatus="N" onmouseover="useroptionUI('+userid+')" style="cursor:pointer;" onmouseleave="removeHover('+userid+');"  onclick="colorthisdiv(this);" ondblclick="getTaskUserComment('+taskId+','+userid+',\'\');event.stopPropagation();">'

                    + '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="max-width:5%" >'
                            +'<div class="d-flex" >'
                            if(userid == userIdglb){
                              UserUI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+userid+', this);"  src="'+imageSource+'" title="'+userName+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle" style="width:30px;height:30px; margin-top: -5px;">'
                            }else{
                              UserUI+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+userid+', this);"  src="'+imageSource+'" title="'+userName+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle cursor" style="width:30px;height:30px; margin-top: -5px;">'
                            }
                            UserUI+='</div>'
                          // +'<div class=" defaultExceedCls mt-1" title="'+userName+'">'+userName+'</div>'
                    +'</div>'

                    + '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="padding:0px;max-width:4%">'
                          +'<img  src="'+image+'"  class="mr-3  ml-1" style="width:25px;cursor:pointer;    margin-top: -3px;">'
                    +'</div>'
                    
                    + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 d-flex" style="padding:0px;max-width:16%">'
                        +'<input disabled="disabled" class="sliderange mt-2 mr-2" type="range" min="0" max="100" style="color: black; width: 70%; background: linear-gradient(90deg, rgb(165, 235, 45) 2%, rgb(0,0,0) 2%);padding-left: 0px !important;" value="'+json[i].userhistoryData[j].task_user_percentage+'">'
                        +'<input disabled="disabled" class="ml-1" value="'+json[i].userhistoryData[j].task_user_percentage+'" style="border:none;height:20px;outline:0; width:24px;text-align:center;color:darkgray;border-bottom:1px solid  #c1c5c8;text-align: right;"/>'
                        +'<div class="percent" style="margin-left: 3px;padding-top: 2px;color:darkgray" >%</div>'
                    +'</div>'
                    
                    + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 " style="padding-left:8px;max-width:12%">'
                          +'<input type="text" disabled="disabled" class="estimateHour"  value="'+json[i].userhistoryData[j].task_est_hours+'" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:23px;text-align: right;outline: 0;border-width: 0 0 1px;color:darkgray;" ><div class="hour ml-1" style="float:left;width:16px;margin-top: 1px;color:darkgray;">h</div><input disabled="disabled" class="estimateMin" type="text"  value="'+json[i].userhistoryData[j].task_est_mins+'" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:18px;text-align: right;outline: 0;border-width: 0 0 1px;color:darkgray;"><div class="min ml-1 " style="float:left;margin-top:1px;color:darkgray;">m</div>'
                    +'</div>'
                    
                    + '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  actualhour " style="color:darkgray;padding:0px;max-width: 12%;" >'+actualMinute+'</div>'
                  //+ '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  elapsedTime " style="color:darkgray;padding:0px;" >'+elapsedTime+'</div>'
              
                    +'<div class="col-lg-12  mt-1  mb-1 " title="'+userName+'">'+userName+'</div>'
                //UserUI+='<div class="usercontainer w-100 pr-4"></div>'
                +'</div>'





        
         $("#sprint_"+json[i].sprintId).find(".user_act_hour").append('<span class="mx-1">-&nbsp; '+json[i].userhistoryData[j].working_hours+'&nbsp;h&nbsp;'+json[i].userhistoryData[j].working_minutes+'&nbsp;m</span>');
         $("#sprint_"+json[i].sprintId).find(".historydetails").append(UserUI);
        }
       
    }
  
}
function showHistory(){
  $(".task_comment_div").hide().find('.task_comment_list_div').html('');
  $(".task_user_history").toggle();
  $(".dependency_comment_div").hide();
}
function createPopupDiv(from){
  let UI="";
  const d = new Date();
  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let nowDate=(monthNames[parseInt(d.getMonth())+1])	+" "+(parseInt(d.getDate())<10?"0"+d.getDate():d.getDate())+", "+d.getFullYear()	;	
  let amPm=parseInt(d.getHours())>11?"PM":"AM";	
  let nowTime=(parseInt(d.getHours())>12?((parseInt(d.getHours())-12)<10?("0"+(parseInt(d.getHours())-12)):(parseInt(d.getHours())-12)):(parseInt(d.getHours())<10?"0"+d.getHours():d.getHours()))+":"+(parseInt(d.getMinutes())<10?"0"+d.getMinutes():d.getMinutes())+" "+amPm;
  UI+='<div>'

      +'<div class="firstDiv d-flex  justify-content-between">'

          +'<div class="startDiv d-flex">'

                +'<div style="width:34px; text-align:right;">Start :</div>'
                +'<div class="d-flex" style="margin-left:3px;color:black;border-bottom:1px solid darkgray;white-space: nowrap;">'
                  +'<div id="startDateCalNewUI" parmanentvalue="'+nowDate+'" style=" width: 72px">'+nowDate+'</div>'
                  +'<div id="startTimeCalNewUI"  parmanentvalue="'+nowTime+'" style=" width: 52px;margin-left:3px;">'+nowTime+'</div>'
                
                +'</div>'
                +'<div id="calenderForTask"   onclick="showCalender(this);" style="margin-left:7px;margin-top: -2px;cursor:pointer">'
                        +'<img src="images/landingpage/cal.svg" style="width:15px" />'
                +'</div>'

          +'</div>'//StartDiv ends here 
          +'<div class="estimateDiv d-flex">'
                +'<div style="width:66px; text-align:right;"> Estimate : </div>'
                +'<div class="ml-1 d-flex" style="color:black; width: 80px;">'
                    +'<input type="text" class="estimateHour" onblur="distributeEstimateTime(\'estimateDiv\')" value="0" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:30px;text-align:right;color:black;outline: 0;border-width: 0 0 1px;background: white;">'
                    +'<div style="margin-left:1px;margin-top:1px">h</div>' 
                    +'<input type="text" class="estimateMinute" onblur="distributeEstimateTime(\'estimateDiv\')" value="0" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:18px;margin-left:6px;text-align:right;color:black;outline: 0;border-width: 0 0 1px;background: white;" onkeyup="validateMinute(this)">'
                    +'<div style="margin-left:1px;margin-top:1px">m</div>'
                +'</div>'

          +'</div>'//estimate Div ends here 
          +'<div class="priorityDiv d-flex  justify-content-end " style="cursor:pointer; width:90px;">'

                +'<div class="d-flex">'    
                  +'<div>Priority </div>'

                  +'<div class="ml-1"><img src="images/task/dowarrow.svg" style="height:12px;width:12px"></div>'

                +'</div>'
                +'<div align="center" style="width:26px;">'//ml-2 17px
                  +'<img priority="6" class="changePriority" src="images/task/p-six.svg" style="width:18px;" />'
                +'</div>'




          +'</div>'//Priority div ends here 

      +'</div>'  //firstDiv ends here 


      +'<div class="secondDiv d-flex  justify-content-between mt-1">'
               +'<div class="endDiv d-flex" style="" >'

                  +'<div style="width:34px; text-align:right;" >Due :</div>'
                  +'<div class="d-flex" style="margin-left:3px;color:black;border-bottom:1px solid darkgray;white-space: nowrap;">'
                    +'<div id="endDateCalNewUI"  parmanentvalue="'+nowDate+'" style=" width: 72px">'+nowDate+'</div>'
                    +'<div id="endTimeCalNewUI"   parmanentvalue="'+nowTime+'" style=" width: 52px;margin-left:3px;">'+nowTime+'</div>'
                  
                  +'</div>'
                  +'<div id="calenderForTask1"  onclick="showCalender(this);" style="margin-left:7px;margin-top: -2px;cursor:pointer">'
                        +'<img src="images/landingpage/cal.svg" style="width:15px" />'
                  +'</div>'

               +'</div>'//endDiv ends here
               +'<div class="position-absolute p-1 time_calender border border-secondary" style=" border:1px solid black;z-index:2 ;                            background:white;font-size:14px;color:#64696F;display:none;    margin-top: 28px;max-width:412px;width:305px;box-shadow: rgb(0 0 0 / 18%) 0px 6px 12px;" >'
                         +'<div class=" mt-1 " >'
                +'<div class="d-flex justify-content-around ml-1" style="max-width: 281px;">'      //Time Div Starts
                    +'<div class="mt-2"> Time </div>'
                    +'<div>'
                      +' <div><img class="ml-1" src="/images/arrowUpNew.png" class="removeonClickBtnIcon" onclick="increaseValue(\'hour\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                      +' <div style="margin-left:2px" id="hour">10</div>'
                      +' <div><img class="ml-1" src="/images/arrowDownNew.png" class="removeonClickBtnIcon" onclick="decreaseValue(\'hour\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                      
                    +' </div>'
                    +'<div>'
                      +' <div><img class="ml-1" src="/images/arrowUpNew.png" class="removeonClickBtnIcon"  onclick="increaseValue(\'minute\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                      +' <div style="margin-left:2px" id="minute">10</div>'
                      +' <div><img class="ml-1" src="/images/arrowDownNew.png" class="removeonClickBtnIcon" onclick="decreaseValue(\'minute\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                      
                    +' </div>'
                    +'<div>'
                      +' <div><img class="ml-1" src="/images/arrowUpNew.png" class="removeonClickBtnIcon"  onclick="increaseValue(\'second\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                      +' <div style="margin-left:2px" id="second">10</div>'
                      +' <div><img class="ml-1" src="/images/arrowDownNew.png" class="removeonClickBtnIcon" onclick="decreaseValue(\'second\');" style=" height: 10px;width: 10px;float:left;cursor:pointer;"></div>'
                      
                    +' </div>'
                      +'<div class="d-flex mt-2 justify-content-around mainampm" style="border:1px solid rgb(111, 158, 176);height:22px;width:50px;cursor:pointer" onclick="changeAmPm(this);">'
                          +'<div class="rounded-circle colorDiv " style="height:18px;width:18px;background-color:rgb(111, 158, 176);margin-top:1px"></div>'
                          +'<div class="ampm" >AM</div>'
                      +' </div>'       
                    +'</div>' //Time div ends
    
                    +'<div id="calender_task1" class="mt-2  " style="height:265px;margin-left: 23px;"> </div>'
                 //Calender Div 
                +'</div>'

                +'</div>'



                +'<div class="actualDiv d-flex" >'
                +'<div style="width:66px; text-align:right;"> Actual : </div>'
                +'<div class="ml-1 d-flex" style="color:darkgray;width: 80px;">'
                    +'<input disabled="disabled" type="text" id="taskactualhour"  value="0" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:30px;text-align:right;color:darkgray;outline: 0;border-width: 0 0 1px;background: white;">'
                    +'<div style="margin-left:1px;margin-top:1px;">h</div>' 
                    +'<input disabled="disabled" type="text"  id="taskactualmin"  value="0" style="border-bottom:1px solid  #c1c5c8;height:20px; float:left;width:18px;margin-left:6px;text-align:right;color:darkgray;outline: 0;border-width: 0 0 1px;background: white;">'
                    +'<div style="margin-left:1px;margin-top:1px">m</div>'
                +'</div>'

              +'</div>'//actualDiv ends here 

              +'<div class="groupDiv d-flex  justify-content-end " style="cursor:pointer ; width:90px;">'

                  +'<div class="group_text">'    
                   +'<div class="d-flex">'    
                      +'<div>Group </div>'

                      +'<div class="ml-1"><img src="images/task/dowarrow.svg" style="height:12px;width:12px"></div>'

                   +'</div>'

                  +'</div>'
                  +'<div class="group_box" align="center" style="width:26px;">'
                    +'<div id= "changeGroup" title="Web Development" class="task_group   rounded-circle" style="background:#0000ff ;margin-top:2px;" group_id="3"></div>'
                  +'</div>'



        +   '</div>'//groupDiv div ends here 


      +'</div>' //secondDiv ends here 

      +'<div class="d-none  mt-1"  id="projectlistingDiv" style="margin-top: 9px;">' //project listing div starts here margin-left: -33px;
            +'<div style="text-align:right;margin-right: 10px;">Workspace :</div>'          
            +'<select id="projectselect" onchange="changeProjectid();" style="margin-top: -3px;margin-left: -6px;">'

                  +'<option project_id="0">Select Workspace</option>'


            +'</select>'

      +'</div>'

      +'<div class="thirdDiv  mt-1 d-none align-items-center">'
          +'<div class="d-flex align-items-center" style="margin-top: 5px;">'

                  +'<div id="">Sprint Group &nbsp;</div>'
                  +'<div id="spGrplistDiv" class="position-relative"><img src="images/task/dowarrow.svg" onclick="showtaskSpGrp();event.stopPropagation();" style="cursor:pointer;height:12px;width:12px;"><div id="spGrouplist" class="position-absolute wsScrollBar" style="display:none;"></div></div>'
                  +'<div id="sprintgroupcolor" class="defaultExceedCls ml-1 rounded-circle" style="width:15px;height:15px;"></div>'
                  +'<div id="sprintgroupname" tSprGrpid="" class="defaultExceedCls ml-1" style="max-width: 80%;width:100px;"></div>'

          +'</div>'
          +'<div class="d-flex align-items-center ml-2" style="margin-top: 3px;">'

                  +'<div id=""><img src="/images/agile/sprint_blue.svg" style="width:16px;height:16px;">&nbsp;</div>'
                  +'<div id="sprintlistDiv" class="position-relative ml-1"><img src="images/task/dowarrow.svg" onclick="showtaskSprintlist();event.stopPropagation();" style="cursor:pointer;height:12px;width:12px;"><div id="sprintlistcontent" class="position-absolute wsScrollBar" style="display:none;"></div></div>'
                  +'<div id="sprintname" tSprintid="" tSpgroupid="" class="defaultExceedCls ml-1" style="max-width: 80%;width:100px;"></div>'

          +'</div>'
      +'</div>'

      +'</div>'

      +'</div>'





      +'</div>'

      $("#temporarySaveDiv").html(UI);
      $("#temporarySaveDiv").removeClass('justify-content-start').addClass('justify-content-around');
      $(".group_text").on('click',()=>{
          $("#priorityDivcustom").hide();
          $("#groupdivision").remove();
          fetchGroups();
      })
      $(".group_box").on('click',()=>{
        $("#priorityDivcustom").hide();
        $("#groupdivision").remove();
        
        fetchGroups();
    })
      $(".priorityDiv").on('click',()=>{
        $("#groupdivision").hide();
        $("#priorityDivcustom").toggle();
      })
      
      if(globalmenu!="agile" && globalmenu!="sprint"){
        calenderForTask();
      }
      
      addPriority();
     
}
function changePriority(obj,place){
  let image="";
  let Priority=$(obj).attr('priority');
  Priority=typeof(Priority)=='undefined'?'':Priority;
  var Priority_image = (Priority == "")? "" : Priority == "1" ? "images/task/p-one.svg" : Priority == "2" ? "images/task/p-two.svg":Priority == "3" ? "images/task/p-three.svg" :Priority == "4" ? "images/task/p-four.svg":Priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
  
  
  if(place=='workflow'){
    $(".changePriorityWf").attr('src',Priority_image)
    $(".changePriorityWf").attr('priority',Priority);
  }else{
    $(".changePriority").attr('src',Priority_image)
    $(".changePriority").attr('priority',Priority);
  }
}
function fetchGroups(place,obj){
  $.ajax({
    url: apiPath + "/" + myk + "/v1/getGroupDetails/"+companyIdglb,
    type: "GET",
    error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
    },
    success: function (result) {
      let UI="";
      UI=prepareGroupUI(result,place);
    
       if(place=='workflow'){
        $(".groupDivwf").append(UI);
       }else if(place=='workflowfrom'){
        $(".groupDivwf").append(UI);
        changeGroup($("#group_"+result[0].group_id),'workflow');
        $('.groupdivision').remove();
      
       }else if(place=="returnui"){
          $(UI).insertAfter(obj);
          $("#groupdivision").css('left','100%');
       }else if(place=="setgroupwhilecreate"){
        $('#changeGroup').attr('group_id',result[0].group_id).attr('title',result[0].group_name).css('background',result[0].group_code);
       }else{
        $(".groupDiv").append(UI);
       }
      
    }
  })
}
function changeGroup(obj,place){
  let group_color=$(obj).attr('groupcode');

  if(place=='workflow'){
    $("#changeGroupwf").css('background',group_color);
    $("#changeGroupwf").attr('group_id',$(obj).attr('groupid'));
    $(".groupdivisionwf").remove();
    $("#changeGroupwf").attr('title',$(obj).attr('groupname'))
  }else if(place=='returnui'){
    let id=$(".workflowUI").attr('idoftask');
    showTaskUI(id);
    $("#block_"+id).css('background',group_color);
    $("#block_"+id).css('border-color',group_color);
    $('div[id^=WfOption_]').hide();
    $(".workflowUI").attr('group_color',group_color);
    $(".workflowUI").attr('groupid',$(obj).attr('groupid'));
    
  }
  else{
    $("#changeGroup").css('background',group_color);
    $("#changeGroup").attr('group_id',$(obj).attr('groupid'));
    $(".groupdivision").remove();
    $("#changeGroup").attr('title',$(obj).attr('groupname'))
  }
}
function addPriority(place){
  let priorityUi="";
  let id="priorityDivcustom"
  if(place=='workflow'){
    id="priorityDivcustomwf"
  }
  priorityUi+='<div id="'+id+'" class="defaultScrollDiv '+id+'" style=" margin-top: 21px; position: absolute;color: black;background: #FFF;border: 1px solid rgb(166, 166, 166);padding: 7px;height: 129px;overflow: auto;border-radius: 2px;font-size: 12px;width: 150px;z-index: 12;text-align: left;display: none;">'


        +'<div style="padding: 2px 0px;display: flex;">	<span class="defaultExceedCls priorityoption" groupid="138" groupcode="#FF2A69FF" onclick="changePriority(this,\''+place+'\');" priority="1" style="width: 100%;cursor: pointer;float:left;" title="Very Important"><span style=" width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: -1px;"><img src="images/task/p-one.svg" style="width: 15px;height: 15px;"/></span>'+getValues(companyLabels,"Very_Important")+'</span></div>'

        +'<div style="padding: 2px 0px;display: flex;">	<span class="defaultExceedCls priorityoption" groupid="138" groupcode="#FF2A69FF"  onclick="changePriority(this,\''+place+'\');" priority="2" style="width: 100%;cursor: pointer;float:left;" title="Important"><span style=" width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: -1px;"><img src="images/task/p-two.svg" style="width: 15px;height: 15px;"/></span>'+getValues(companyLabels,"Important")+'</span></div>'

        +'<div style="padding: 2px 0px;display: flex;">	<span class="defaultExceedCls priorityoption"  groupid="138" groupcode="#FF2A69FF"  onclick="changePriority(this,\''+place+'\');" priority="3" style="width: 100%;cursor: pointer;float:left;" title="Medium"><span style=" width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: -1px;"><img src="images/task/p-three.svg" style="width: 15px;height: 15px;"/></span>'+getValues(companyLabels,"Medium")+'</span></div>'

        +'<div style="padding: 2px 0px;display: flex;">	<span class="defaultExceedCls priorityoption" groupid="138" groupcode="#FF2A69FF"  onclick="changePriority(this,\''+place+'\');" priority="4"  style="width: 100%;cursor: pointer;float:left;" title="Low"><span style=" width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: -1px;"><img src="images/task/p-four.svg" style="width: 15px;height: 15px;"/></span>'+getValues(companyLabels,"Low")+'</span></div>'

        +'<div style="padding: 2px 0px;display: flex;">	<span class="defaultExceedCls priorityoption" groupid="138" groupcode="#FF2A69FF"  onclick="changePriority(this,\''+place+'\');" priority="5" style="width: 100%;cursor: pointer;float:left;" title="Very Low"><span style=" width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: -1px;"><img src="images/task/p-five.svg" style="width: 15px;height: 15px;"/></span>'+getValues(companyLabels,"Very_Low")+'</span></div>'

       priorityUi+="</div>"

       if(place=='workflow'){
        $(".priorityDivWf").append(priorityUi);
       }else{
        $(".priorityDiv").append(priorityUi);
       }
}
function editFunction(userid,editStatus){
  colorthisdiv($("#user_id_"+userid))
  if(editStatus=='on'){
    $(".taskcolordiv").find('.selectedit').find('.material-textfield-label').css('background','#FFF');
    $(".taskcolordiv").removeClass('taskcolordiv');
    $("#user_id_"+userid).addClass('taskcolordiv');
    $("#user_id_"+userid).find('.selectedit').find('.material-textfield-label').css('background','#eef5f7');
    $("#user_id_"+userid).find(".estimateHour").removeAttr('disabled').css('color','black').css('background','#eef5f7');
    $("#user_id_"+userid).find(".estimateMin").removeAttr('disabled').css('color','black').css('background','#eef5f7');
    $("#user_id_"+userid).find("#edit").attr('onclick','editFunction('+userid+',"off")')
    $("#user_id_"+userid).find(".hour").css('color','black').css('background','#eef5f7');
    $("#user_id_"+userid).find(".min").css('color','black').css('background','#eef5f7');
    if(userid==userIdglb){
        $("#user_id_"+userid).find('.usercommentdiv  > div').each(function () { 
            $(this).find("#workingHours").removeAttr('disabled').css('color','black').css('background','#eef5f7');
            $(this).find("#workingMinutes").removeAttr('disabled').css('color','black').css('background','#eef5f7');
            $(this).find("#workingHours").next().css('color','black')
            $(this).find("#workingMinutes").next().css('color','black')
            $(this).find(".task_comment_user").removeAttr('disabled').css('color','black').css('background','#eef5f7');
          
      });
      
      $("#user_id1_"+userid).find('.usercommentdiv  > div').each(function () { 
        // $(this).find("#workingHours").removeAttr('disabled').css('color','black').css('background','#eef5f7');
        // $(this).find("#workingMinutes").removeAttr('disabled').css('color','black').css('background','#eef5f7');
        // $(this).find("#workingHours").next().css('color','black')
        // $(this).find("#workingMinutes").next().css('color','black')
        $(this).find(".task_comment_user").removeAttr('disabled').css('color','black').css('background','#eef5f7');
      
  });
      //alert( $(".selectedit").html())
      if($(".selectedit").length>0){
        $("#user_id_"+userid).find(".workinghour").removeAttr('disabled').css('color','black').css('background','#eef5f7');
        $("#user_id_"+userid).find(".workingMinutes").removeAttr('disabled').css('color','black').css('background','#eef5f7');
      }
    }

  }else{
    
    $(".taskcolordiv").find('.selectedit').find('.material-textfield-label').css('background','#FFF');
    $(".taskcolordiv").removeClass('taskcolordiv');
    $("#user_id_"+userid).removeClass('taskcolordiv');
    $("#user_id_"+userid).find('.selectedit').find('.material-textfield-label').css('background','#FFF');
    $("#user_id_"+userid).find("#edit").attr('onclick','editFunction('+userid+',"on")');
    $("#user_id_"+userid).find(".estimateHour").attr('disabled','disabled').css('color','darkgray').css('background','#FFF');
    $("#user_id_"+userid).find(".estimateMin").attr('disabled','disabled').css('color','darkgray').css('background','#FFF');
    $("#user_id_"+userid).find(".hour").css('color','darkgray').css('background','#FFF');
    $("#user_id_"+userid).find(".min").css('color','darkgray').css('background','#FFF');
    if(userid==userIdglb){
        $("#user_id_"+userid).find('.usercommentdiv  > div').each(function () { 
            $(this).find("#workingHours").attr('disabled','disabled').css('color','darkgray').css('background','#FFF');
            $(this).find("#workingMinutes").attr('disabled','disabled').css('color','darkgray').css('background','#FFF');
            // $(this).find(".task_comment_user").attr('onclick',"");
            $(this).find("#workingHours").next().css('color','darkgray')
            $(this).find("#workingMinutes").next().css('color','darkgray')
            $(this).find(".task_comment_user").attr('disabled','disabled').css('color','darkgray').css('background','#FFF');
           
      }); 
      //if($(".selectedit").length>0){
        // $("#user_id_"+userid).find(".workinghour").attr('disabled','disabled');
        // $("#user_id_"+userid).find(".workingMinutes").attr('disabled','disabled');
     // }
    }

  }
 
}
function showTaskComments(taskid){
  $("#taskAdd").attr('task_id',taskid);
  fetchPostedThingFiles(0,0,0,0,'list',0,taskid);
}
function colorthisdiv(obj){
  //alert("colo")
  $(".taskcolordiv").find('.selectedit').find('.material-textfield-label').css('background','#FFF');
  $(".taskcolordiv").find(".estimateHour").css('background','#FFF');
  $(".taskcolordiv").find(".estimateMin").css('background','#FFF');
  $(".taskcolordiv").css('background',whitecolor);
  $(".taskcolordiv").find(".acthour").css('background','#FFF');
  $(".taskcolordiv").find(".actmin").css('background','#FFF');

  $(".taskcolordiv").find(".workinghour").css('background','#FFF');
  $(".taskcolordiv").find(".workingMinutes").css('background','#FFF');
  
  $(".taskcolordiv").find(".userslide").css('background','#FFF');
  $(".taskcolordiv").find(".hour").css('background','#FFF');
  $(".taskcolordiv").find(".min").css('background','#FFF');
  //if(userid==userIdglb){
      $(".taskcolordiv").find('.usercommentdiv  > div').each(function () { 
          // $(this).find("#workingHours").css('background','#FFF');
          // $(this).find("#workingMinutes").css('background','#FFF');
          // $(this).find(".task_comment_user").css('background','#FFF');
        
    }); 
    //alert( $(".selectedit").html())
    if($(".selectedit").length>0){
      $(".taskcolordiv").find(".workinghour").css('background','#FFF');
      $(".taskcolordiv").find(".workingMinutes").css('background','#FFF');
    }
  //}
  $(".taskcolordiv").removeClass('taskcolordiv');
  $(obj).addClass('taskcolordiv');
  $(obj).find('.selectedit').find('.material-textfield-label').css('background','#eef5f7' ); 
  $(".taskcolordiv").find('.selectedit').find('.material-textfield-label').css('background','#eef5f7');
  $(".taskcolordiv").find(".estimateHour").css('background','#eef5f7');
  $(".taskcolordiv").find(".estimateMin").css('background','#eef5f7');
  $(".taskcolordiv").find(".userslide").css('background','#eef5f7');
  $(".taskcolordiv").find(".hour").css('background','#eef5f7');
  $(".taskcolordiv").find(".min").css('background','#eef5f7');

  
  $(".taskcolordiv").find(".acthour").css('background','#eef5f7');
  $(".taskcolordiv").find(".actmin").css('background','#eef5f7');

  $(".taskcolordiv").find(".workinghour").css('background','#eef5f7');
  $(".taskcolordiv").find(".workingMinutes").css('background','#eef5f7');
  //if(userid==userIdglb){
      $(".taskcolordiv").find('.usercommentdiv  > div').each(function () { 
          // $(this).find("#workingHours").css('background','#eef5f7');
          // $(this).find("#workingMinutes").css('background','#eef5f7');
          // $(this).find(".task_comment_user").css('background','#eef5f7');
        
    }); 
    //alert( $(".selectedit").html())
    if($(".selectedit").length>0){
      $(".taskcolordiv").find(".workinghour").css('background','#eef5f7');
      $(".taskcolordiv").find(".workingMinutes").css('background','#eef5f7');
    }
}

function showReminder(){
  if($("#reminderOption").length>0){
    $(".reminderOption").toggle();
  }
  else{
    let reminderOption="";
    reminderOption+='<div id="reminderOption" class="defaultScrollDiv reminderOption" style=" margin-top: 21px; position: absolute;color: black;background: #FFF;border: 1px solid rgb(166, 166, 166);padding: 7px;height: 108px;overflow: auto;border-radius: 2px;font-size: 12px;width: 150px;z-index: 3;text-align: left;">'
  
  
          +'<div style="padding: 2px 0px;display: flex;" id="Minutes">	<span class="defaultExceedCls remoption" reminderoption="'+getValues(companyLabels,"Minutes")+'" onclick="changeRemimder(this);" priority="1" style="width: 100%;cursor: pointer;float:left;" title="Very Important"><span style=" width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: -1px;margin-right:0px"><img src="images/temp/tick.png" id="tickmark" style="width: 10px;height: 10px;display:none"/></span>'+getValues(companyLabels,"Minutes")+'</span></div>'
  
          +'<div style="padding: 2px 0px;display: flex;" id="Hour">	<span class="defaultExceedCls remoption" reminderoption="'+getValues(companyLabels,"Hour")+'" onclick="changeRemimder(this);" priority="1" style="width: 100%;cursor: pointer;float:left;" title="Very Important"><span style=" width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: -1px;margin-right:0px"><img src="images/temp/tick.png" id="tickmark" style="width: 10px;height: 10px;display:none"/></span>'+getValues(companyLabels,"Hour")+'</span></div>'
  
          +'<div style="padding: 2px 0px;display: flex;" id="Daily">	<span class="defaultExceedCls remoption" reminderoption="'+getValues(companyLabels,"chart_Daily")+'" onclick="changeRemimder(this);" priority="1" style="width: 100%;cursor: pointer;float:left;" title="Very Important"><span style=" width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: -1px;margin-right:0px"><img src="images/temp/tick.png" id="tickmark" style="width: 10px;height: 10px;display:none"/></span>'+getValues(companyLabels,"chart_Daily")+'</span></div>'
  
          +'<div style="padding: 2px 0px;display: flex;" id="Weekly">	<span class="defaultExceedCls remoption" reminderoption="'+getValues(companyLabels,"Weekly")+'" onclick="changeRemimder(this);" priority="1" style="width: 100%;cursor: pointer;float:left;" title="Very Important"><span style=" width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: -1px;margin-right:0px"><img src="images/temp/tick.png" id="tickmark" style="width: 10px;height: 10px;display:none"/></span>'+getValues(companyLabels,"Weekly")+'</span></div>'
  
          reminderOption+="</div>"
  
          $(reminderOption).insertAfter("#remindericon");
          if(typeof($("#remindericon").attr('reminder'))!='undefined' && $("#remindericon").attr('reminder')!=''){
           
            let reminderselected=$("#remindericon").attr('reminder');
            if(reminderselected.includes('Daily')){
              $("#Daily").find('#tickmark').show();
            }else if(reminderselected.includes('Week')){
              $("#Weekly").find('#tickmark').show();
            }
            else if(reminderselected.includes('Hour')){
              $("#Hour").find('#tickmark').show();
            }
            else if(reminderselected.includes('Minutes')){
              $("#Minutes").find('#tickmark').show();
            }
           
          }
          
  }
}
function changeRemimder(obj){
  $("[id^=tickmark]").hide();
  $(obj).find('#tickmark').show();
  $("#remindericon").attr('reminder',$(obj).attr('reminderoption'));
  changeImage('reminder');
}
function divideEstimateHours(time,users){
    let divisible=(time%users==0)?true:false;
    if(divisible){
      return time/users;
    }else{
      let peruser=time/users;
      let timeexceeds=time%users;
      
      return peruser+"@@"+timeexceeds;
    }
}

function isValidDate(dateObject){
  return new Date(dateObject).toString() !== 'Invalid Date';
}
function validateUserInputDate(obj,place){

  let startDate=$(obj).text();
  
  let conditioncheck=false;
  if(place=="date"){
  
  var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  let startdateMonth=monthNames.indexOf(startDate.substring(0,3))<10?"0"+monthNames.indexOf(startDate.substring(0,3)):monthNames.indexOf(startDate.substring(0,3));
  let startdateDate=startDate.substring(4,6)
  let start=startdateMonth.trim()+"-"+startdateDate.trim()+"-"+startDate.split(",")[1].trim();
    if(isValidDate(start)){
      conditioncheck=true;
    }

  }else{
     let startDate=$("#startTimeCalNewUI").text();
     let hour=startfullTime.substring(startfullTime.length-2,startfullTime.length)=="PM"?(parseInt(startfullTime.split(":")[0].trim())+12):startfullTime.split(":")[0].trim();
     let startTime=hour+":"+startfullTime.split(":")[1].trim().substring(0,2);
  }


  if(!conditioncheck){
    alertFun("Please use the given format or use calender picker",'warning');
  }

  // var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  // let startdateMonth=monthNames.indexOf(startDate.substring(0,3))<10?"0"+monthNames.indexOf(startDate.substring(0,3)):monthNames.indexOf(startDate.substring(0,3));
  // let startdateDate=startDate.substring(4,6)
  // let start=startdateMonth.trim()+"-"+startdateDate.trim()+"-"+startDate.split(",")[1].trim();

  // let endDate=$("#endDateCalNewUI").text();
  // let enddateMonth=monthNames.indexOf(endDate.substring(0,3))<10?"0"+monthNames.indexOf(endDate.substring(0,3)):monthNames.indexOf(endDate.substring(0,3));
  // let enddateDate=endDate.substring(4,6)
  // let end=enddateMonth.trim()+"-"+enddateDate.trim()+"-"+endDate.split(",")[1].trim();


  // let startfullTime=$("#startTimeCalNewUI").text();
  // let hour=startfullTime.substring(startfullTime.length-2,startfullTime.length)=="PM"?(parseInt(startfullTime.split(":")[0].trim())+12):startfullTime.split(":")[0].trim();
  // let startTime=hour+":"+startfullTime.split(":")[1].trim().substring(0,2);
  

  // let endfullTime=$("#endTimeCalNewUI").text();
  // let endhour=endfullTime.substring(endfullTime.length-2,endfullTime.length)=="PM"?(parseInt(endfullTime.split(":")[0].trim())+12):endfullTime.split(":")[0].trim();
  // let endTime=endhour+":"+endfullTime.split(":")[1].trim().substring(0,2);

  // let startDatecompare=new Date(start+" "+startTime);
  // let endDatecompare=new Date(end+" "+endTime); && $("#task_id").val()==''
}

function sumUptoEstimate(userid){
  $("#user_id_"+userid).attr('modified','modified');
  $("#addTaskDiv").attr('changestatus','Y');
  if( $(".estimateDiv").find(".estimateHour").attr('modified')!='modified'){
    let estHr=0;
    let estMin=0;
    $('.userDetails > div').each(function () { 
      if($(this).attr('id')!='ignorediv'){
      // alert($(this).attr('id'));
      estHr=parseInt(estHr)+parseInt($(this).find('.estimateHour').val());
      estMin=parseInt(estMin)+parseInt($(this).find('.estimateMin').val());
      
      }
      
    });
    if(estMin>59){
      estHr=parseInt(estHr)+Math.floor(parseInt(estMin)/60);
      estMin=Math.floor(parseInt(estMin)%60);
    }
    $(".estimateDiv").find(".estimateHour").val(estHr);
    $(".estimateDiv").find(".estimateMinute").val(estMin);
  }

}
function distributeEstimateTime(place){
   $("#addTaskDiv").attr('changestatus','Y');
  //if($("#task_id").val()==''){
    if(place=='estimateDiv'){
      $(".estimateDiv").find(".estimateHour").attr('modified','modified');
      $(".estimateDiv").find(".estimateMinute").attr('modified','modified');
    }
    if(parseInt($(".estimateHour").val())>0 || parseInt($(".estimateMinute").val())>0){
      let time=parseInt( $(".estimateDiv").find(".estimateHour").val())*60+(parseInt($(".estimateDiv").find(".estimateMinute").val()));

      let estHr=0;
      let estMin=0;
      let count=0;
      $('.userDetails > div').each(function () { 
        if($(this).attr('modified')=='modified'){
         // alert($(this).attr('id'));
         estHr=parseInt(estHr)+parseInt($(this).find('.estimateHour').val());
         estMin=parseInt(estMin)+parseInt($(this).find('.estimateMin').val());
         count++;
         
        }
        
      });
     //alert(count);
      if(count!=0){
        let distributedTime=parseInt(estHr)*60+parseInt(estMin);
        distributedTime=time-distributedTime;
        if(distributedTime>0){
            let usercount=document.querySelectorAll(".userDetails .usercount").length;
            if($(".userDetails").find("#user_id_"+userIdglb).length>0 && $("#task_id").val()!=''){
              usercount=usercount-1
            }
            let remaining_count=usercount-count;
            //alert(remaining_count)
            if(remaining_count>0){
              let divided_time=divideEstimateHours(distributedTime,remaining_count);
              if(divided_time.toString().includes("@@")==false){
                if(parseInt(divided_time)>=60){
                  let divided_hour=divided_time/60;
                  let divided_min=divided_time%60;
                  $('.userDetails > div').each(function () { 
                    if($(this).attr('modified')!='modified'){
                        $(this).find('.estimateHour').val(Math.floor(divided_hour))
                        $(this).find('.estimateMin').val(Math.floor(divided_min))
                    }
                  });
                }else{
                  $('.userDetails > div').each(function () { 
                    if($(this).attr('modified')!='modified'){
                      $(this).find('.estimateHour').val('');
                      $(this).find('.estimateMin').val(Math.floor(divided_time));
                    }
                  });
                }
              }else{
                let divided_times=divided_time.toString().split("@@")[0];
                let extra_time=divided_time.toString().split("@@")[1];
               // alert(divided_times+"---"+Math.floor(divided_times%60))
                if(parseInt(divided_times)>=60){
                  let divided_hour=divided_times/60;
                // alert(divided_hour.toFixed(0))
                  let divided_min=divided_times%60;
                  $('.userDetails > div').each(function () { 
                    if($(this).attr('modified')!='modified'){
                      $(this).find('.estimateHour').val(Math.floor(divided_hour));
                      $(this).find('.estimateMin').val(Math.floor(divided_min));
                    }
                  });
                }else{
                  $('.userDetails > div').each(function () { 
                    if($(this).attr('modified')!='modified'){
                      $(this).find('.estimateHour').val('');
                      $(this).find('.estimateMin').val(Math.floor(parseInt(divided_times)));
                    }
                  });
                }
                if(parseInt(extra_time)>=60){
                  let extra_hour=extra_time/60;
                  let extra_min=extra_time%60;
                  let valh=$('.userDetails').children().last().find('.estimateHour').val();
                  let valm=$('.userDetails').children().last().find('.estimateMin').val();
                  //$('.userDetails').first().find('.estimateHour').val(extra_hour.toFixed(0));
                 // $('.userDetails').first().find('.estimateMin').val(extra_min);
                 
                 $('.userDetails').children().last().find('.estimateHour').val(Math.floor((parseInt(valh)+parseInt(extra_hour))));
                 $('.userDetails').children().last().find('.estimateMin').val(Math.floor((parseInt(valm)+parseInt(extra_min))));
                 
                }else{
                 // $('.userDetails').first().find('.estimateMin').val(extra_time);
                  let val=$('.userDetails').children().last().find('.estimateMin').val();
                 
                  $('.userDetails').children().last().find('.estimateMin').val(Math.floor((parseInt(val)+parseInt(extra_time))));
                }
              
              }
            }
        }


      }else{
        let usercount=document.querySelectorAll(".userDetails .usercount").length;
        //alert(usercount)
        
        let time=parseInt($(".estimateHour").val())*60+(parseInt($(".estimateMinute").val()));
       
        let divided_time=divideEstimateHours(time,usercount);
        if(divided_time.toString().includes("@@")==false){
          if(parseInt(divided_time)>=60){
              let divided_hour=divided_time/60;
              let divided_min=divided_time%60;
              $('.userDetails > div').each(function () { 
                $(this).find('.estimateHour').val(Math.floor(divided_hour))
                $(this).find('.estimateMin').val(Math.floor(divided_min))
              });
          }else{
            $('.userDetails > div').each(function () { 
              $(this).find('.estimateHour').val('');
              $(this).find('.estimateMin').val(Math.floor(divided_time));
            });
          }
        }else{
          let divided_times=divided_time.toString().split("@@")[0];
          let extra_time=divided_time.toString().split("@@")[1];
          //alert(divided_times+"---"+extra_time)
          if(parseInt(divided_times)>=60){
            let divided_hour=divided_times/60;
           // alert(divided_hour.toFixed(0))
            let divided_min=divided_times%60;
            $('.userDetails > div').each(function () { 
              $(this).find('.estimateHour').val(Math.floor(divided_hour));
              $(this).find('.estimateMin').val(Math.floor(divided_min));
            });
          }else{
            $('.userDetails > div').each(function () { 
              $(this).find('.estimateHour').val('');
              $(this).find('.estimateMin').val(Math.floor(parseInt(divided_times)));
            });
          }
          if(parseInt(extra_time)>=60){
            let extra_hour=extra_time/60;
            let extra_min=extra_time%60;
            let valh=$('.userDetails').children().last().find('.estimateHour').val();
            let valm=$('.userDetails').children().last().find('.estimateMin').val();
            //$('.userDetails').first().find('.estimateHour').val(extra_hour.toFixed(0));
           // $('.userDetails').first().find('.estimateMin').val(extra_min);
           
           $('.userDetails').children().last().find('.estimateHour').val(Math.floor((parseInt(valh)+parseInt(extra_hour))));
           $('.userDetails').children().last().find('.estimateMin').val(Math.floor((parseInt(valm)+parseInt(extra_min))));
           
          }else{
           // $('.userDetails').first().find('.estimateMin').val(extra_time);
            let val=$('.userDetails').children().last().find('.estimateMin').val();
           
            $('.userDetails').children().last().find('.estimateMin').val(Math.floor((parseInt(val)+parseInt(extra_time))));
          }
        }
      }

    }
 // }
} 
function changePicofInprogress(){
 // if(parseInt(valu)>0){
    $("#persontaskstatusdisplay_"+userIdglb).attr("src", "images/task/user-inprogress.svg");
    $("#persontaskstatusdisplay_"+userIdglb).attr("statusOfTaskUser", 'Inprogress');
  //}
}
function changeColor(obj,status){
    if(!$(obj).attr('class').includes('taskcolordiv')){
      if(status=='on'){
        $(obj).css('background',globalcolor)
        $(obj).find('.selectedit').find('.material-textfield-label').css('background',globalcolor ); 
        $(obj).find('.selectedit').find('.material-textfield-label').css('background',globalcolor);
        $(obj).find(".estimateHour").css('background',globalcolor);
        $(obj).find(".estimateMin").css('background',globalcolor);
        $(obj).find(".userslide").css('background',globalcolor);
        $(obj).find(".hour").css('background',globalcolor);
        $(obj).find(".min").css('background',globalcolor);
     
       
        $(obj).find(".acthour").css('background',globalcolor);
        $(obj).find(".actmin").css('background',globalcolor);
     
        $(obj).find(".workinghour").css('background',globalcolor);
        $(obj).find(".workingMinutes").css('background',globalcolor);
       //if(userid==userIdglb){
            $(obj).find('.usercommentdiv  > div').each(function () { 
              //  $(this).find("#workingHours").css('background',globalcolor);
              //  $(this).find("#workingMinutes").css('background',globalcolor);
              //  $(this).find(".task_comment_user").css('background',globalcolor);
             
         }); 
         //alert( $(".selectedit").html())
         if($(".selectedit").length>0){
            $(obj).find(".workinghour").css('background',globalcolor);
            $(obj).find(".workingMinutes").css('background',globalcolor);
         }
     }else{
        $(obj).css('background',whitecolor)
        $(obj).find('.selectedit').find('.material-textfield-label').css('background',whitecolor ); 
        $(obj).find('.selectedit').find('.material-textfield-label').css('background',whitecolor);
        $(obj).find(".estimateHour").css('background',whitecolor);
        $(obj).find(".estimateMin").css('background',whitecolor);
        $(obj).find(".userslide").css('background',whitecolor);
        $(obj).find(".hour").css('background',whitecolor);
        $(obj).find(".min").css('background',whitecolor);
        $(obj).find(".acthour").css('background',whitecolor);
        $(obj).find(".actmin").css('background',whitecolor);
     
        $(obj).find(".workinghour").css('background',whitecolor);
        $(obj).find(".workingMinutes").css('background',whitecolor);
       //if(userid==userIdglb){
            $(obj).find('.usercommentdiv  > div').each(function () { 
               $(this).find("#workingHours").css('background',whitecolor);
               $(this).find("#workingMinutes").css('background',whitecolor);
               $(this).find(".task_comment_user").css('background',whitecolor);
             
         }); 
         //alert( $(".selectedit").html())
         if($(".selectedit").length>0){
            $(obj).find(".workinghour").css('background',whitecolor);
            $(obj).find(".workingMinutes").css('background',whitecolor);
         }
     }
    }
}
function changeImage(place){
  $("#addTaskDiv").attr('changestatus','Y');
  if(place=='user'){
    let usercount=document.querySelectorAll(".userDetails .row").length;
    if(usercount>0){
      $("#participantsUIdivArea").attr('src','/images/task/assignee_cir2.svg')
    }else{
      $("#participantsUIdivArea").attr('src','/images/task/assignee_cir.svg')
    }

  }else if(place=='Approvers'){

    let usercount=document.querySelectorAll(".userDetails1 .row").length;
    if(usercount>0){
      $("#participantsUIdivArea1").attr('src','/images/task/approver_fill.svg')
    }else{
      $("#participantsUIdivArea1").attr('src','/images/task/approver_nofill.svg')
    }

  }else if(place=='instruction'){
      if($("#taskdesc").val()!=""){
        $("#instruction").attr('src','/images/task/intr.svg')
      }else{
        $("#instruction").attr('src','/images/task/taskInstructions_cir.svg')
      }
  }else if(place=='dependency'){
    
    let count=0;
    $('.dependenctTaskIds > div').each(function () { 
     if($(this).find('.check').prop("checked")==true){
            count++;
           // break;
       }
    
    });
    if(count>0){
        $("#dependencyTask").attr('src','/images/task/task_dependency.svg')
    }else{
        $("#dependencyTask").attr('src','/images/task/DependentTask.svg')
    }
  }else if(place=='reminder'){
    $("#remindericon").attr('src','/images/task/clock-Invt.svg')
  }else if(place=='comment'){
     let usercount=document.querySelectorAll(".task_comment_list_div .conversation").length;
    // alert(usercount)
     if(usercount>0){
      $("#comments").attr('src','/images/task/comments2.svg');
     }else{
      $("#comments").attr('src','/images/task/task_comments.svg');
     }
  }else{
    let usercount=document.querySelectorAll(".userDetails1 .row").length;
    if(usercount>0){
      $("#participantsUIdivArea1").attr('src','/images/task/approver_fill.svg')
    }else{
      $("#participantsUIdivArea1").attr('src','/images/task/approver_nofill.svg')
    }
  }
}
var flagNew = true;
function prepareAddGroupUI(group_color,place){
  flagNew = true;
  let UI="";
  let style="padding-top: 7px;";
  if(place=='returnui'){
    style="padding-top: 4px;";
  }

  let color=typeof(group_color)!='undefined' && group_color!=''?group_color:"#F00";
  UI+='<div class="addGroupDiv mt-1">'


          +'<div class="material-textfield" ><textarea id="group_create_name" type="text" class="theightdemo material-textfield-input"  onkeyup=" commentAutoGrow(event, this, \'task\', \'\');"  style="    height: 34px;resize:none;outline:none;border:1px solid #C1C5C8;    border-radius: 6px;font-size: 12px; padding-left: 10px;'+style+'" placeholder=" " ></textarea><label class="material-textfield-label" style="    font-size: 12px;top:18px;margin-left:8px">Group Name</label></div>'
          +'<div class="d-flex justify-content-between ">'
                +'<input type="text" id="group_color"  data-inline="true"              value="'+color+'" onclick="removeColorBox();" style="border-radius: 6px;resize:none;outline:none;border:1px solid #C1C5C8;text-transform:uppercase;width:85px "/>'
              +'<div class="d-flex justify-content-end mt-1">'
                +'<img id="" title="Cancel" class="img-responsive " src="/images/task/remove.svg" style="float:left;height:25px;width:25px;margin-right: 2px;cursor:pointer;" onclick="cancelCreateGroup(\''+place+'\');">'
                +'<img id="group_save" title="Save" class="img-responsive " src="/images/task/tick.svg" style="float:left;height:25px;width:25px;margin-right: 2px;cursor:pointer;" onclick="addGroup(\''+place+'\');" >'
              +'</div>'
          +'</div>'
          +'<div id="colorBoxNew" class="colorGrid pt-2">'
          +'</div>'
          +'<div id="wantMore" class="wantMoreColor d-flex justify-content-center pt-2">'
            +'<button type="button" id="showMore" onclick="showMoreColor();" class="btn btn-sm rounded">Show Colors</button>'
          +'</div>'
          
      +'</div>'

    if(place=='workflow'){
    
      $(UI).insertBefore(".group_div_wf");
      $(".new_group").addClass('d-none').removeClass('d-flex');
      $(".group_div_wf").hide();
      $("#addgroupid").hide();
      $("#groupdivisionwf").show();
      $("#groupdivisionwf").css('width','250px');
      $("#groupdivisionwf").css('height','258px'); 
   
    }else if(place=='returnui'){
      $(UI).insertBefore(".group_div");
      $(".addGroupDiv").show();
      $("my-container").css('height','270px')
    }
    else{
      $(UI).insertBefore(".group_div");
    }
      $(".new_group").addClass('d-none').removeClass('d-flex');
      $(".group_div").hide();
      $("#addgroupid").hide();
      $("#groupdivision").show();
      $("#groupdivision").css('width','250px');
      $("#groupdivision").css('height','110px');
      
      
     // let color=typeof(group_color)!='undefined'?group_color:"#f00"
    //   $("#group_color").spectrum({
    //     color: color,
       
    //     change: function(color) {
    //       $("#group_color").val(color.toHexString()) ; // #ff0000
    //     },
    //       // chooseText:'<img title="Save" class="img-responsive " src="/images/task/tick.svg"  style="float:left;height:25px;width:25px;margin-right: 8px;cursor:pointer;">',
    //       // cancelText: '<img title="Save" class="img-responsive " src="/images/task/tick.svg"  style="float:left;height:25px;width:25px;margin-right: 8px;cursor:pointer;">',,'width':'250px'
    //       show: function(tinycolor) { 
           
    //       },
    //       showButtons: false,
    //       beforeShow:()=>{
    //         return true;
    //       }
         
    //   });
      
    //   $("#group_color").spectrum("show");
    //   $("#group_color").val(color) ;
    // //  $(".sp-button-container").find('button').html('<img title="Save" class="img-responsive" src="/images/task/tick.svg"  style="float:left;height:25px;width:25px;margin-right: 3px;cursor:pointer;margin-left:-12px;    margin-top: -1px;">')
    //   //$(".sp-cancel").html('<img title="cancel" class="img-responsive mt-1" src="/images/task/remove.svg"  style="float:left;height:25px;width:25px;margin-right: 8px;cursor:pointer;">');position: absolute; top: 333px; left: 1189.88px;
    //   //$(".sp-button-container").find('button').css({'background':'none','color':'transparent','border':'0'})
    //   $("#group_color").next().css({'border-radius':'6px','margin-top':'2px','outline':'none','border':'1px solid #C1C5C8'});
    //   $(".sp-picker-container").css('width','219px');
    //   $(".sp-container").css('top','347px !important');
     
   
    $("#group_color").minicolors({
     
     
      control: 'hue',
    
      format: 'hex',
      hide: function(){
        $(".minicolors-panel").show();
      },
      change:function(){
      //  console.log($("#group_color").val().toUpperCase())
      //   $("#group_color").val($("#group_color").val().toUpperCase());
      //   $("#group_color").attr('value',$("#group_color").val().toUpperCase());
      },
      inline: false,
     
      theme: 'default',
      swatches: []
      
    });
    $("#group_color").removeClass("minicolors-input").addClass("minicolors-inputnew");
    // $(".minicolors-inputnew").click(function(){
    //   flagNew=true;
    //   
    //   if(flagNew==true){
    //     $(".minicolors-panel").hide();
    //   }
    //   else{
    //     console.log("hii");
    //     $(".minicolors-panel").show();
    //     // flagNew=true;
    //   }
      
    // });
    colorGridView();

    $(".minicolors-swatch-color").css("background-color", "#f39a4e");
    $("#group_color").val("#f39a4e");

   
    $("#groupdivision").css('height','258px'); 
       setTimeout(function(){ 
  
       
         $(".minicolors-panel").hide();
    
  
      },300);
      // console.log(flagNew);
      
}

function removeColorBox(){
  if(flagNew){
    $("#group_color").addClass("minicolors-input");
    $(".minicolors-panel").hide();
    flagNew=true;
    // $("#group_color").removeClass("minicolors-input");
  }else{
    $(".minicolors-panel").show();
  }
}

function showMoreColor(){
  flagNew = false;
  $("#colorBoxNew").hide();
  $("#wantMore").hide();
  $("#showMore").hide();
  $("#group_color").addClass("minicolors-input");
  $("#group_color").removeClass("minicolors-inputnew");
  $(".minicolors-panel").show();
}


function cancelCreateGroup(place){
  if(place=='workflow'){
    $(".addGroupDiv").remove();
    $(".new_group").addClass('d-flex').removeClass('d-none');
    $(".group_div_wf").show();
    $("#groupdivisionw").show();
    $(".sp-container").remove();
    $("#addgroupid").show();
  // $("#groupdivision").css('width','200px');
    $("#groupdivisionwf").css('height','166px');
  }else{
    $(".addGroupDiv").remove();
    $(".new_group").addClass('d-flex').removeClass('d-none');
    $(".group_div").show();
    $("#groupdivision").show();
    $(".sp-container").remove();
    $("#addgroupid").show();
  // $("#groupdivision").css('width','200px');
    $("#groupdivision").css('height','166px');
  }
  mouseOver = true;
}
function addGroup(place){
  let group_create_name=$("#group_create_name").val();
  let group_color=$("#group_color").val();
  if(typeof(group_create_name)=='undefined' || group_create_name==''){
    alertFunNew("Group name cannot be Empty",'error');
    return false;
  }
  //ajax call here 
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody={
    "group_name":group_create_name,
    "group_code":group_color,
    "created_by":userIdglb,
    "company_id":companyIdglb,
    "group_desc":""
  }
  timerControl("");
  $.ajax({
    url: apiPath + "/" + myk + "/v1/insertGroupDetails",
    //url:"http://localhost:8080/v1/insertGroupDetails",
    type: "POST",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    success: function (result) {
     
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      timerControl("");
      let UI=prepareGroupUI(result,'returnui');
      if(place=='workflow'){
        $(".group_div_wf").prepend(UI);
        changeGroup($("#group_"+result[0].group_id),place)
      }else if(place=='returnui'){
        $(".group_div").prepend(UI);
        changeGroup($("#group_"+result[0].group_id),place)
      }
      else{
        $(".group_div").prepend(UI);
        $("#group_"+result[0].group_id).trigger('onclick');
      }
      
    }
    
  });
  cancelCreateGroup(place);
  
 

}
function setProgress(obj){
 let percentage= $(obj).attr('perval');
 $("#taskSlideAmount_"+userIdglb).val(percentage);
 $("#taskSlideAmount_"+userIdglb).trigger('onblur');
 $("#slide_"+userIdglb).trigger('onchange');
}
 function editGroup(group_id,place){
  let group_name=$("#group_"+group_id).attr('groupname');
 
  let group_code=$("#group_"+group_id).attr('groupcode');
  prepareAddGroupUI(group_code,place);
   
  // setTimeout(function(){ 
  
   
  
    
  
  // },300);
  $("#group_create_name").val(group_name);
  $("#group_save").attr('onclick','updateGroup('+group_id+',\''+place+'\')');

}
function showGroupEdit(group_id,place){
  if(place=='group'){
    $("#subcommentDiv_group_"+group_id).show();
  }else if(place=='taskdocument'){
    $("#subcommentDiv_taskdoc_"+group_id).show();
  }else if(place=='userdocument'){
    $("#subcommentDiv_userdoc_"+group_id).show();
  }
 
}
function hideGroupEdit(group_id,place){
  if(place=='group'){
    $("#subcommentDiv_group_"+group_id).hide();
  }else if(place=='taskdocument'){
    $("#subcommentDiv_taskdoc_"+group_id).hide();
  }
  else if(place=='userdocument'){
    $("#subcommentDiv_userdoc_"+group_id).hide();
  }
}

function updateGroup(group_id,place){
  let group_create_name=$("#group_create_name").val();
  let group_color=$("#group_color").val();
  if(typeof(group_create_name)=='undefined' || group_create_name==''){
    alertFunNew("Group name cannot be Empty",'error');
    return false;
  
  }
  //ajax call here 
  $('#loadingBar').addClass('d-flex').removeClass('d-none');
  let jsonbody={
    "group_name":group_create_name,
    "group_code":group_color,
    "created_by":userIdglb,
    "company_id":companyIdglb,
    "group_desc":"",
    "group_id":group_id
  }
  timerControl("");
  $.ajax({
    url: apiPath + "/" + myk + "/v1/updateTaskGroup",
   // url:"http://localhost:8080/v1/updateTaskGroup",
    type: "PUT",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    success: function (result) {
     
      $('#loadingBar').addClass('d-none').removeClass('d-flex');
      timerControl("");
      $("#group_"+group_id).parent().remove();
      let UI=prepareGroupUI(result,'edit');
      
      if(place=='workflow'){
        $(".group_div_wf").prepend(UI);
        changeGroup($("#group_"+result[0].group_id),place)
      }else{
        $(".group_div").prepend(UI);
        $("#group_"+result[0].group_id).trigger('onclick');
      }
       
       //$("#group_"+result[0].group_id).trigger('onclick');
    }
    
  });
  cancelCreateGroup(place);
  
}
function prepareGroupUI(result,place){
  let UI="";
  let classof="groupdivision";
  let classofgroup="group_div";
  if(place=='workflow'){
    classof="groupdivisionwf";
    classofgroup="group_div_wf"
  }
  let style="";
  if(place=='returnui'){
    style="margin-top:-6px";
  }
  if(place!="edit"){
    UI+='<div id="'+classof+'" class="defaultScrollDiv '+classof+'" style=" margin-top: 21px; position: absolute;color: black;background: #FFF;border: 1px solid rgb(166, 166, 166);padding: 7px;height: 143px;overflow: auto;border-radius: 2px;font-size: 12px;width: 250px;z-index: 12;text-align: left;">'
    // +'<div onclick="prepareAddGroupUI()" class="new_group d-flex" style="border-bottom:1px solid darkgray" title="New Group">'
    //   +'<img src="https://cdn2.iconfinder.com/data/icons/bold-application/500/plus-512.png" style="height:20px;width:20px">'
      
    // +'</div>'
  
    UI+='<div id ="addgroupid" onclick="prepareAddGroupUI(\'\',\''+place+'\');event.stopPropagation();" style="padding: 2px 0px;display: flex;"><span style="margin-right: 8px;"><img src="images/task/plus.svg" style=" width: 15px;height: 15px;"/></span><span style="font-size: 12px;    margin-top: 1px;font-weight: 700;color: #5e5b5b;margin-bottom: 2px;" class="defaultExceedCls">Add new group</span></div>'
  
  
    +'<div class="'+classofgroup+'">'
  }
  for(i=0;i<result.length;i++){
    // UI+='<option title="'+result[i].group_name+'" value="'+result[i].group_id+'" groupcode="'+result[i].group_code+'">'+result[i].group_name.substring(0,12)+'</option>'

    UI+='<div style="padding: 2px 0px;display: flex; position:relative" onmouseleave="hideGroupEdit('+result[i].group_id+',\'group\');" onmouseover="showGroupEdit('+result[i].group_id+',\'group\');">	<div  groupname="'+result[i].group_name+'" id="group_'+result[i].group_id+'" class="defaultExceedCls" groupid="'+result[i].group_id+'" groupcode="'+result[i].group_code+'" onclick="changeGroup(this,\''+place+'\');" style="width: 100%;cursor: pointer;float:left;" title="'+result[i].group_name+'"><div class="rounded-circle" style="background-color:'+result[i].group_code+'; border-radius:2px; min-width: 15px;height: 15px;float: left;margin-right: 8px;margin-top: 1px;"></div><div class="defaultExceedCls" style="'+style+'">'+result[i].group_name+'</div></div>'
    
      +'<div id="subcommentDiv_group_'+result[i].group_id+'" class="actFeedOptionsDiv" style="border-radius: 8px; top: -3px; margin-right: 5px; display: none;"><div class="d-flex align-items-center"><img class="mx-1" title="Edit" id="edit_group" src="images/conversation/edit.svg" onclick="editGroup('+result[i].group_id+',\''+place+'\')" style="width:18px;height:18px"><img class="mx-1" title="Delete" onclick="deleteGroup('+result[i].group_id+')" src="/images/task/minus.svg" id="delete_group" style="width:18px;height:18px;"></div></div>'
    +'</div>'
  }
  if(place!="edit"  ){
   
    UI+='</div>'
    UI+="</div>"
  }
  return UI;
}
function deleteGroup(group_id,checked){
  if(checked!='Y'){
    let messege="Do you want to delete this group ?"
    conFunNew(messege,'warning','deleteGroup','',group_id,'Y');
  }

  else{
    $.ajax({
      url: apiPath + "/" + myk + "/v1/deleteTaskGroup?task_group_id="+group_id,
      //url:"http://localhost:8080/v1/deleteTaskGroup?task_group_id="+group_id,
      type: "DELETE",
      success: function (result) {
        $("#group_"+group_id).parent().remove();
      }
    });
    if($("#changeGroup").attr('group_id')==group_id){
     // $("#changeGroup").attr('group_id','0');
      //$("#changeGroup").attr('title','')
      //$("#changeGroup").css('background','');
      //$("#changeGroup").removeClass('task_group');
    }
  }
}
function deleteTaskDocument(taskdocid,checked){
  if(checked!='Y'){
    let messege="Do you want to delete this document ?"
    conFunNew(messege,'warning','deleteTaskDocument','',taskdocid,'Y');
  }
  else{
    $.ajax({
      url: apiPath + "/" + myk + "/v1/DeleteTaskLevelDocuments/normalTask?doc_id="+taskdocid,
      type: "DELETE",
      success: function (result) {
        $("#doc_"+taskdocid).remove();
      }
    });
  }
}

  function deleteUserDoc(docid,checked){
    if(checked!='Y'){
      let messege="Do you want to delete this document ?"
      conFunNew(messege,'warning','deleteUserDoc','',docid,'Y');
    }
   else{
    $.ajax({
      url: apiPath + "/" + myk + "/v1/deleteUserDoc?task_doc_id="+docid,
      type: "DELETE",
      success: function (result) {
        $("#doc_"+docid).remove();
      }
    });
   }
  }

  function prepareDependencyUIlikeTask(result){
    let ui="";
    ui +=
    "<div class='DependentTaskDiv' style=' '>" 
    +"<div class=' d-flex justify-content-between align-items-center pb-1' style='    border-bottom: 1px solid darkgray;' >" 
    +'<div class="d-flex w-75">'
      +'<div id="" class="pl-1 mr-1" style="font-size:12px;" >Dependent Task</div>'
      +'<img class="" id="backsearch" src="images/task/dowarrow.svg" style="height:15px;width:15px; cursor:pointer;margin-top:2px">'
    +'</div>'
    //+'<input type="text" class="pl-1"  id="dependencysearch" style="display:none;border: 0;width: 90%;outline: none;border-bottom:1px solid #b4adad"/>'
    +'<div>'
    //   +'<img class="mt-1 mr-1" id="entersearch" src="images/menus/search2.svg" style="height:18px;width:18px; cursor:pointer;margin-top:2px">'
     // +'<img class="" id="backsearch" src="/images/back1.png" style="height:15px;width:15px; cursor:pointer;margin-top:2px">'
    +'</div>'
  +"</div>"
    +"<div class='d-flex py-1 headerBar w-100 mt-1' style='z-index: 1;'> " +
    
        "<div class='defaultExceedCls pl-2' id='headertaskType' style='width: 4%;'></div>"+
        "<div class='defaultExceedCls pl-2' id='headertaskType' style='width: 8%;'>Type</div>"+
        "<div class='defaultExceedCls' id='headertaskgroup' style='width: 8%;'>Group</div>"+
        "<div class='defaultExceedCls' id='headertaskgroup' style='width: 12%;'>Created by</div>"+
        "<div class='defaultExceedCls' style='width: 6%;'>ID</div>"+
        "<div class='defaultExceedCls' id='headerTaskname' style='width: 30%;'>Task</div>"+
        //"<div class='defaultExceedCls' style='width: 9%;'>Start</div>"+
        "<div class='defaultExceedCls' style='width: 9%;'>Due</div>"+
      
        "<div class='defaultExceedCls' style='width: 8%;text-align: center;'>Priority</div>"+
        "<div class='defaultExceedCls' style='width: 8%;text-align: center;'>Status</div>"+
        "<div class='defaultExceedCls' style='width: 6%;text-align: center;'>Trend</div>"+
   
  
  
    "</div>"
   
    + "<div class='dependenctTaskIds defaultScrollDiv' id='DependenttaskList' style='font-size: 12px;height:200px;overflow:auto;'>" 
    
    for(i=0;i<result.length;i++){


      let taskStatusImageTitle = result[i].task_status;
      let  taskStatusImage = taskStatusImageTitle == "Completed" ? "images/calender_old/Completed.png" : taskStatusImageTitle == "InProgress" ? "images/calender_old/InProgress.png" : taskStatusImageTitle == "Created" ? "images/calender_old/InProgress.png" : taskStatusImageTitle == "InComplete" ? "images/calender_old/incompleteDetailPic.png" : taskStatusImageTitle == "NotCompleted" ? "images/calender_old/NotCompleted.png"  : "images/calender_old/NotCompleted.png";


      let Priority=result[i].priority;
      let Priority_image = (Priority == "")? "" : Priority == "1" ? "images/task/p-one.svg" : Priority == "2" ? "images/task/p-two.svg":Priority == "3" ? "images/task/p-three.svg" :Priority == "4" ? "images/task/p-four.svg":Priority == "5" ?  "images/task/p-five.svg":"images/task/p-six.svg";
      let priorityTitle = Priority == "" ? "": Priority == "1" ? "Very important" : Priority == "2" ?"Important": Priority == "3" ? "Medium": Priority == "4"  ? "Low": Priority == "5"  ? "Very low": Priority == "6" ? "None" :"";
      
      let trending=result[i].trending;
      let sentimentBgCol ="";
      if(trending >=-1 && trending <=-0.49){
          sentimentBgCol='images/tasksNewUI_old/meterred.png';
      }else if(trending >=-0.50 && trending <0){
          sentimentBgCol='images/tasksNewUI_old/meteryellow.png';
      }else if(trending == 0 || trending == 0.0){
          sentimentBgCol='images/tasksNewUI_old/meteryellow_center.png';
      }else if((trending >0 || trending > 0.0)&& trending <=0.49){
          sentimentBgCol='images/tasksNewUI_old/meteryellow_Lightgreen.png';
      }else if(trending >=0.5 && trending <=1){
          sentimentBgCol='images/tasksNewUI_old/meter_fullgreen.png';
      }else{
          trending = getValues(companyLabels,"Nodata");
          sentimentBgCol='images/tasksNewUI_old/meterDefault.png';  
      }

        ui+="<div class='d-flex py-1  w-100'  select='N' style='z-index: 1;font-size:12px;border-bottom:1px solid darkgray' id='depTask_"+result[i].task_id+"'> " +
    
        "<div class='defaultExceedCls pl-2' id='' style='width: 4%;'>"
            +'<input id="imageCheck_'+result[i].task_id+'" class="check mt-1"  type="checkbox" style="cursor:pointer"  onchange="changeImage(\'dependency\');" />'
        +"</div>"+
        "<div class='defaultExceedCls pl-3' id='' style='width: 8%;'>"
            +'<img id="" src="images/task/task.svg" class="task-img img-responsive tTypeImage" />'
        +"</div>"+
        "<div class='defaultExceedCls pl-3' id='' style='width: 8%;'>"
          +"<div title='"+result[i].group_name+"' class='rounded-circle' style='background:"+result[i].group_code+" ;width:15px;height:15px;box-shadow: 0 6px 10px rgb(0 0 0 / 18%);'></div>"
        
        +"</div>"+
        "<div class='defaultExceedCls pl-3' id='' style='width: 12%;'>"
        if(result[i].user_id == userIdglb){
          ui+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+result[i].user_id+', this);"  src="https://testbeta.colabus.com:1933///userimages/'+result[i].created_by+'.jpeg" title="'+result[i].created_by+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle" style="width:25px;height:25px;">'
        }else{
          ui+='<img id="personimagedisplay" onclick="event.stopPropagation();getNewConvId('+result[i].user_id+', this);"  src="https://testbeta.colabus.com:1933///userimages/'+result[i].created_by+'.jpeg" title="'+result[i].created_by+'" onerror="userImageOnErrorReplace(this);" class="mr-3 rounded-circle cursor" style="width:25px;height:25px;">'
        }
        ui+="</div>"+
        "<div class='defaultExceedCls' style='width: 6%;'>"+result[i].task_id+"</div>"+
        "<div class='defaultExceedCls' id='' style='width: 30%;'>"+result[i].task_name+"</div>"+
        //"<div class='defaultExceedCls' style='width: 9%;'>Start</div>"+
        "<div class='defaultExceedCls' style='width: 9%;'>"+result[i].end_date+"</div>"+
      
        "<div class='defaultExceedCls pl-3' style='width: 8%;text-align: center;'>"
              +'<img id="personimagedisplay" src="'+Priority_image+'" onerror="imageOnProjNotErrorReplace(this);" class="mr-3  "  title="'+priorityTitle+'" style="width:20px;height:20px;">'
        +"</div>"+
        "<div class='defaultExceedCls pl-3' style='width: 8%;text-align: center;'>"
            +'<img id="personimagedisplay" src="'+taskStatusImage+'" onerror="imageOnProjNotErrorReplace(this);" class="mr-3  "  title="'+taskStatusImageTitle+'" style="width:20px;height:20px;">'
        +"</div>"+
        "<div class='defaultExceedCls pl-3' style='width: 6%;text-align: center;'>"
            +'<img id="personimagedisplay" src="'+sentimentBgCol+'" onerror="imageOnProjNotErrorReplace(this);" class="mr-3  "  title="'+trending+'" style="width:20px;height:20px;">'
        +"</div>"

      +"</div>"  

    }
    
    ui+="</div>" +
    "</div>";

    

    return ui;
  }

  function changeColorofComment(obj,status){
      if(status==="on"){
       // alert( $(obj).find("#workingHours").html());
        $(obj).find("#workingHours").css('background',globalcolor);
        $(obj).find("#workingMinutes").css('background',globalcolor);
        $(obj).find(".task_comment_user").css('background',globalcolor);
        $(obj).css('background',globalcolor);
      }else{
        $(obj).find("#workingHours").css('background',whitecolor);
        $(obj).find("#workingMinutes").css('background',whitecolor);
        $(obj).find(".task_comment_user").css('background',whitecolor);
        $(obj).css('background',whitecolor);
  
      }
  } 

  //Myzone related modifications

 function  fetchProjectsForMyzone(){
  let jsonbody={
    "user_id":userIdglb,
    "companyId":companyIdglb,
    "workspaceType":"existing"
}
 
  $.ajax({
    url: apiPath + "/" + myk + "/v1/projectList",
    type: "POST",
    dataType: 'json',
    contentType: "application/json",
    data: JSON.stringify(jsonbody),
    success: function (result) {
            let UI="";
            for(i=0;i<result.length;i++){
              UI+='<option project_id="'+result[i].project_id+'"  project_name="'+result[i].project_name+' ">'+result[i].project_name+'    </option>'
            }
            $("#projectselect").append(UI);
       }
   });
 }

 function changeProjectid(){
   prjid=$('#projectselect :selected').attr('project_id');
   if(sprintTaskinTask=="sprintTaskinTask"){
    taskStoryList();
   }
 }
 function validateMinute(obj){
        let min=$(obj).val();
        if(parseInt(min)>59){
          alertFunNew("Minute value should be between 0 to 59.",'error','min',obj);
          $(obj).val('0');
        
       
        }
        
  
 }

 async function taskStoryList(){
  if($("#tSprintStoryListDiv1").children().length == 0 || gloablmenufor=="Myzone"){
    $('#loadingBar').addClass('d-flex').removeClass('d-none');
    await $.ajax({ 
      url: apiPath+"/"+myk+"/v1/loadStoryForProjectTask?proj_Id="+prjid+"&user_id="+userIdglb+"&company_id="+companyIdglb,
      type:"GET",
      error: function(jqXHR, textStatus, errorThrown) {
          checkError(jqXHR,textStatus,errorThrown);
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
          timerControl("");
      }, 
      success:function(result){
          $("#tSprintStoryListDiv1").html("");
          if(gloablmenufor=="Myzone"){
            $("#sprintgroupcolor").css("background-color","");
            $("#sprintgroupname").text("").attr("tsprgrpid","").attr("title","");
            $("#sprintname").attr("tsprintid","").attr("tspgroupid","").attr("title","").text("");
            $("#taskname").val("");
            const d = new Date();
            var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            let nowDate=(monthNames[parseInt(d.getMonth())+1])	+" "+(parseInt(d.getDate())<10?"0"+d.getDate():d.getDate())+", "+d.getFullYear()	;	
            let amPm=parseInt(d.getHours())>11?"PM":"AM";	
            let nowTime=(parseInt(d.getHours())>12?((parseInt(d.getHours())-12)<10?("0"+(parseInt(d.getHours())-12)):(parseInt(d.getHours())-12)):(parseInt(d.getHours())<10?"0"+d.getHours():d.getHours()))+":"+(parseInt(d.getMinutes())<10?"0"+d.getMinutes():d.getMinutes())+" "+amPm;
            $("#startDateCalNewUI , #endDateCalNewUI").attr("parmanentvalue",nowDate).text(nowDate);
            $("#startTimeCalNewUI , #endTimeCalNewUI").attr("parmanentvalue",nowTime).text(nowTime);
            
          }
          if(result!=""){
            $("#tSprintStoryListDiv1").append(prepareTSprintStoryListDiv(result));
            $("#selectedStoryIntask").text(result[0].epic_title).attr('storyid',result[0].epic_id);
            $("#taskname").val(result[0].epic_title);
            if(gloablmenufor=="Myzone"){
              getSprintlist(result[0].epic_id,result[0].epic_project);
            }else{
              getSprintlist(result[0].epic_id);
            }
          }else{
            $('#spGrouplist').html("<div class='pt-3' style='text-align: center;'>No sprint group found</div>");
            $('#sprintlistcontent').html("<div class='pt-3' style='text-align: center;'>No sprint found</div>");
            $("#tSprintStoryListDiv1").html("<div class='pt-3' style='text-align: center;'>No stories found</div>");
            $("#selectedStoryIntask").text("").attr("storyid","");
          }
          
          $('#loadingBar').addClass('d-none').removeClass('d-flex');
      }
    });    
  }else{

  }
  
 }
 
 function prepareTSprintStoryListDiv(result){
    var ui="";
    var wid = "";//gloablmenufor=="Myzone"?result[i].epic_title+" - "+result[i].epic_project_name:result[i].epic_title;
    /* if(gloablmenufor=="Myzone"){
      ui="<div class='d-flex align-items-center py-1 px-2' style='background-color: #245A89;'><div class='w-50'>Stories</div><div class='w-50'>Projects</div></div>"
    }  */ 
    if(result!=""){
      for(var i=0;i<result.length;i++){
        wid = gloablmenufor=="Myzone"?result[i].epic_title+" - "+result[i].epic_project_name:result[i].epic_title;
        ui+="<div id='tStory_"+result[i].epic_id+"' onclick='tSelStory(this);event.stopPropagation();' stprid="+result[i].epic_project+" stname='"+result[i].epic_title+"' stId="+result[i].epic_id+" class='tStoryCls' style='padding:5px 5px;'>"
          +"<div  class='defaultExceedCls ' title='"+wid+"' style='cursor:pointer;'>"+result[i].epic_title+"</div>"
          /* if(gloablmenufor=="Myzone"){
            ui+="<div class='defaultExceedCls w-50' title='"+result[i].epic_project_name+"'>-&nbsp;"+result[i].epic_project_name+"</div>"
          } */
        +"</div>"
      }
    }else{
      ui="<div style='text-align:center;font-size:12px;'>No Stories Found</div>"
    }
    

    return ui;
 }
 
 function showTstoryList(){
    $('#tSprintStoryListDiv').toggle();
    $("#selectTaskstoriesinput").focus();
 }

 function tSelStory(obj){
    $("#selectedStoryIntask").text($(obj).attr('stname')).attr('storyid',$(obj).attr('stId'));
    $("#taskname").val($(obj).attr('stname'));
    if(gloablmenufor=="Myzone"){
      getSprintlist($(obj).attr('stId'),$(obj).attr('stprid'));
      $('#projectlistingDiv option[project_id="'+$(obj).attr('stprid')+'"]').attr('selected','selected');

      $("#sprintgroupcolor").css("background-color","");
      $("#sprintgroupname").text("").attr("tsprgrpid","").attr("title","");
      $("#sprintname").attr("tsprintid","").attr("tspgroupid","").attr("title","").text("");
      //$("#taskname").val($("#selectedStoryIntask").text());
      const d = new Date();
      var monthNames = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      let nowDate=(monthNames[parseInt(d.getMonth())+1])	+" "+(parseInt(d.getDate())<10?"0"+d.getDate():d.getDate())+", "+d.getFullYear()	;	
      let amPm=parseInt(d.getHours())>11?"PM":"AM";	
      let nowTime=(parseInt(d.getHours())>12?((parseInt(d.getHours())-12)<10?("0"+(parseInt(d.getHours())-12)):(parseInt(d.getHours())-12)):(parseInt(d.getHours())<10?"0"+d.getHours():d.getHours()))+":"+(parseInt(d.getMinutes())<10?"0"+d.getMinutes():d.getMinutes())+" "+amPm;
      $("#startDateCalNewUI , #endDateCalNewUI").attr("parmanentvalue",nowDate).text(nowDate);
      $("#startTimeCalNewUI , #endTimeCalNewUI").attr("parmanentvalue",nowTime).text(nowTime);
      
    }else{
      getSprintlist($(obj).attr('stId'));
    }
    
 }

function searchStoryinTask(obj){

  var value = $(obj).val().toLowerCase();
  $(".tStoryCls  *").filter(function() {
      $(this).parent().toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });

}