
//-------------------------------->// workflow template new functions //-------------------------------------------------->
//-- workflowTemplateFuntions.js includes the commen functions used in workspace and calendar module---------------------->


var levelCkdId= '';
var customLabels="";
var wfView = "";
var isiPad = navigator.userAgent.match(/iPad/i) != null;
var hideButton="NO";	    
var flowStatusTab="";      
/*function workflowTemplateNew(projId) { 
   //customLabels = custlabel;//------used in workflowTemplateFuntions.js
   wrkFlowTempProjId = projId;
   tempID = '';
   $('#loadingBar').show();
   timerControl("start");
   $("#transparentDiv").show();
   //$('div#wstansparentDiv').show();
   $("div#projAdminContents").css("display", "none");
   $("div#inviteUserDataContiner").css("display", "none");
   //$("div#createWorkflowTempDivNew").css("display", "block");
   $('div#workFlowMain').css('display', 'block');
   $("div#workFlowTemplateViewContentsNew").hide();
   $("div#projAdminDisplayDiv").show();
   //$("div#createWorkflowTempDivNew").css("display", "block");
   $("div.wrkspaceProjDetails").hide();
   $('div.wrkspaceProjDetails').html('');
   $.ajax({
	   url : path + "/paLoad.do",
	   type : "POST",
	   data : {
		   act : "loadAllWorkflowTemplate",
		   projId : wrkFlowTempProjId,
		   localprojectType : localprojectType
	   },
	   success : function(result) {
		   //parent.checkSessionTimeOut(result);
		   //var iframeHeight = $(window).height()  - 250;
		   //$("div#workFlowTemplateContentsNew").css("height", iframeHeight + "px");
			prepareTemplateUI(result);
		   //$("div#tabContentDiv").hide();
		   if (localprojectType == 'sharedProject') {
			   $(".workflowDelete").hide();
			   $(".workflowCreate").hide();
			   $('.workflowUpdate').hide();
			   $('.workflowCopy').hide();
		   }else{
			   $(".workflowDelete").show();
			   $(".workflowCreate").show();
			   $('.workflowUpdate').show();
			   $('.workflowCopy').show();
		   }
		   //loadCustomLabel("WF TemplateList");
		   //scrollTemplate('workFlowTemplateContentsNew');
		   $("div#workFlowContainer").on("mouseover","div.actFeedHover",function(){
						   if(!$(this).find('div.actFeedOptionsDiv ').is(':visible')){
							   $('div.actFeedHover').find('div.actFeedOptionsDiv ').hide();
						   }
		   });
		   $('#loadingBar').hide();
			timerControl("");
	   }
   });
}*/
		function workflowTemplateNew(projId,type) { 
			$('#loadingBar').show();
		   timerControl("start");
		   var wrkFlowTempProjId = projId;
		   var tempID = '';
		   $('#loadingBar').show();
		   timerControl("start");
		   var ckeHeight = getHeightDynamically();
		   if(type == 'sysAdmin'){
				 ckeHeight = ckeHeight - 30;
			 }
		   $("div#workFlowResultContainer").html("");
		   $("div#taskCompletionCodesContainer").hide();
			$("div#fullProjectDetailsContainer").hide();
			$("div#inviteUsersToProjectWorkspace").hide();
			$("div#apporvalRequestContainer").hide();
			$("div#optionalDrivesContainer").hide();
		   $("div#templateContainer").show();
		   
		   $.ajax({
			   url : path + "/paLoad.do",
			   type : "POST",
			   data : {
						   act : "loadAllWorkflowTemplate",
						   projId : wrkFlowTempProjId,
						   localprojectType : localprojectType
					   },
			   error: function(jqXHR, textStatus, errorThrown) {
					   checkError(jqXHR,textStatus,errorThrown);
					   $("#loadingBar").hide();
					   timerControl("");
					   }, 
			   success : function(result) {
				   checkSessionTimeOut(result);
					var data = prepareTemplateUI(result);
					$("div#workflowContainer").css("height", ($('#rightProjectContainer').height()-45)+"px");
					$("div#workFlowResultContainer").html(data);
				   /*if (localprojectType == 'sharedProject') {
					   $(".workflowDelete").hide();
					   $(".workflowCreate").hide();
					   $('.workflowUpdate').hide();
					   $('.workflowCopy').hide();
				   }else{
					   $(".workflowDelete").show();
					   $(".workflowCreate").show();
					   $('.workflowUpdate').show();
					   $('.workflowCopy').show();
				   } */
				   
				   var projType = $('#hiddenProjSettingType').val();
				   if(projType !='myProject'){
					 $('.workflowDeleteOptDiv,.workflowUpdateOptDiv,.workflowCopyOptDiv').hide();
				   }
				   
				   $("div#workFlowContainer").on("mouseover","div.actFeedHover",function(){
								   if(!$(this).find('div.actFeedOptionsDiv ').is(':visible')){
									   $('div.actFeedHover').find('div.actFeedOptionsDiv ').hide();
								   }
				   });
				   popUpScrollBar('workflowContainer');
			   }
		   });
		   $('#loadingBar').hide();
		   timerControl("");
		}

function cancelWorkflowTemplateNew() {
   //$("#createWorkflowTempDivNew").hide();
   //$("div#inviteUserDataContiner").css('display', 'none');
   //$("div#tabContentDiv").show();
   $("#workFlowMain").hide();
   $('#workFlowCreate').hide();
   $("#transparentDiv").hide();
   $('#wfTemplateListContainer').html("");
   $("#wfTemplatePopDiv").hide();
   $("#copyTempIcon").hide();
}


function createWFTemplate() {
   $("#transparentDiv").show();
   $("input#wfName").val("");
   $('#wfLevelContainer').html('');
   $('#wfTemplateListContainer').html("");
   $("#copyTempIcon").show();
   var projectId = $("input#globalProjectId").val();
   $('#wf_projId').val(projectId);
   $('#wf_tempId').val("0");
   $('#deleteLevel').hide();
   $('div#workFlowMain').css('display','none');
   $('div#workFlowCreate').css('display','block');
   levelCkdId='';
   $("div.workflowTemplateList").css("background-color", "");
   var seHtml = putStartEndFlowDiagram();
   $('#wfLevelContainer').html(seHtml);
   $('#wfName').removeAttr('readonly');
   $('#addNewLevel').show();
   $('#changeWfbtns').attr('onclick','saveWFL()');
   $("div#changeWfbtns").html("SAVE");
   $('#addLvl').show();
   $('#addLvl').attr('onclick','addLevel()');
   attachSelectEvent();
}




function addLevel(){
	  var html = '';
	  var levelDivLength = $('#wfLevelContainer').find('div.levelDivs').length;
	  //alert("levelDivLength::>>>"+levelDivLength);
	   if((levelDivLength != 0 )&& ((levelDivLength % 4) == 0 )){ //----------------- per row 4 steps
		 var i=$('#wfLevelContainer').find('div.levelDivs').length /4; 
		  html = generateWFTemplateNode(i);
	   }
	   html += generateWFTemplateStep(0,'','');
	   $('textarea.levelInput').val('');     
	   if($('#wfLevelContainer').find('div.stepSelected').length > 0){
		   $('div.stepSelected').next('div.wfArrow').after(html);
		   $('div.stepSelected').parent('.wfStart').next('div.wfArrow').after(html);
		   var stepXml = generateStepXml();
		   var seHtml = putStartEndFlowDiagram();
		   $('#wfLevelContainer').html(seHtml);
			var html = generateWFTemplate(stepXml);
			$(html).insertBefore('#wfLevelContainer div.wfEnd'); 
			if (!isiPad) {  
			 $("div#wfLevelContainer").mCustomScrollbar('update');
		   }
			attachSelectEvent();
			editLevel($('.newStep').find('p.levelP'));
			tabActive($('.newStep'));
	   }else{ 
		   $(html).insertBefore('#wfLevelContainer div.wfEnd');
		   if (!isiPad) {  
			 $("div#wfLevelContainer").mCustomScrollbar('update');
		   }
		   editLevel($('.newStep').find('p.levelP'));
		   attachSelectEvent();
		   tabActive($('.newStep'));
	   }  
	   loadCustomLabel("WF TemplateList");
 // }
}

   function editLevel(obj){
		 var levelName = $(obj).attr('title');
		 $(obj).hide();
		 $(obj).next('textarea').val(levelName).show().focus();
   }
   
   var confirmLevelObj;
   function editComplete(obj){
	   var levelName = $(obj).val().trim();
		 if(levelName!=''){
			 $(obj).hide();
			 $(obj).prev('p').text(levelName).attr('title',levelName).show();
			 $(obj).parents('div.wf_level').removeClass('newStep');
			 var stepIndex = $(obj).parents('.wf_level').attr('stepIndex');
			 if(stepIndex!=''){
				   $('#taskDefinition_'+stepIndex).text(levelName);
				   $('#wfNewTaskName').val($('#taskDefinition_'+stepIndex).text());
			   }
		 }else{
			 confirmLevelObj=obj;
			confirmReset(getValues(companyAlerts,"Alert_WFLevelEmpty"),'delete','editLevelContinue','editLevelCancel');
		}
   }

   function editLevelContinue(){
	   $(confirmLevelObj).focus();
   }
   
   function editLevelCancel(){
	   var levelName = $(confirmLevelObj).prev('p').text();
		if(levelName!=""){
		   $(confirmLevelObj).hide();
		   $(confirmLevelObj).prev('p').show();
		 }else{
		   $(confirmLevelObj).parents('div.wf_level').next('div.wfArrow').remove();
		   $(confirmLevelObj).parents('div.wf_level').remove();
		   var stepXml = generateStepXml();
		   var seHtml = putStartEndFlowDiagram();
		   $('#wfLevelContainer').html(seHtml);
			var html = generateWFTemplate(stepXml);
			$(html).insertBefore('#wfLevelContainer div.wfEnd');
			if (!isiPad) {  
			   $("div#wfLevelContainer").mCustomScrollbar('update');
		   }
			attachSelectEvent();
			loadCustomLabel("WF TemplateList");
		 }
   }
function clearLevel(){
   $('textarea.levelInput').val('');   
}

var templateFlagChanges = false;
function saveWFL(){
		 var wf_name = $('#wfName').val().trim();
		 if(wf_name==""){
		   alertFun(getValues(companyAlerts, "Alert_WFEmpty"),'warning');  
		   $('#wfName').val('');
		   return false;
		 }
		 var levelXml=generateStepXml();
		 var projId = $('#wf_projId').val();
		 var tempId = $('#wf_tempId').val();
		  $('#loadingBar').show();
		  timerControl("start");
		$.ajax({
			   url: path+"/workspaceAction.do",
			   type:"POST",
			   data:{act:"saveWorkflowTemplate",templateName:wf_name,levelXml:levelXml,projId:projId,tempId:tempId,levelCkdId:levelCkdId},
			   error: function(jqXHR, textStatus, errorThrown) {
					   checkError(jqXHR,textStatus,errorThrown);
					   $("#loadingBar").hide();
					   timerControl("");
					   }, 
			   success:function(result){
				 parent.checkSessionTimeOut(result);
				 templateFlagChanges = true;
				 //workflowTemplateNew(wrkFlowTempProjId);
				  $('#loadingBar').hide();
				 timerControl("");
				 cancelWorkflowTemplateNew();
				 workflowTemplateNew(projId);
				}
		   });	
}

function generateStepXml(){
	 var levelXml="<xml>";
	 $('#wfLevelContainer').find('div.levelDivs').each(function(){
		  var l_name = $(this).find('p.levelP').html();
		  var l_index = $(this).index();
		  var l_id = $(this).find('input.levelId').val();
		  var doc_attach = $(this).find('img.docIcon').hasClass('docChecked') ? 'Y' :'N';
		  levelXml +="<wfLevel>";
		  levelXml +=" <id>"+l_id+"</id>";
		  levelXml +=" <name>"+l_name+"</name>";
		  levelXml +=" <order>"+l_index+"</order>";
		  levelXml +=" <doc_attach>"+doc_attach+"</doc_attach>";
		  levelXml +="</wfLevel>"
	 });
	 levelXml +="</xml>";
	 return levelXml;
}

function setUncheckAll(obj){ 
 if($(obj).is(':checked')){
   $('#deleteLevel').show();
 }else{
	var checkedLength =  $('#wfLevelContainer').find('div.wf_level:visible input[type=checkbox]:checked').length;
	if(checkedLength <1)
	  $('#deleteLevel').hide();
 }
}

function attachDocumentToStep(obj){ 
	 if($(obj).hasClass('docChecked')){
	   confirmLevelObj=obj;
	   confirmReset(getValues(companyAlerts, "Alert_WFDocAttachCancel"),'reset','cancelAttachDocument','confirmAttachDocument');
	 }else{
	   confirmLevelObj=obj;
	   confirmReset(getValues(companyAlerts, "Alert_WFDocAttachConfirm"),'reset','confirmAttachDocument','cancelAttachDocument');
	 }
}

function confirmAttachDocument(){
	$(confirmLevelObj).addClass('docChecked').removeClass('WF_NoAttachDocument_cLabelTitle').addClass('WF_AttachDocument_cLabelTitle');
	$(confirmLevelObj).attr('src', 'images/tasksNewUI/docman.png');
	//$(confirmLevelObj).css({'width':'23px', 'margin':'2px 3px'});
	loadCustomLabel("WF TemplateList");
}
function cancelAttachDocument(){
  $(confirmLevelObj).removeClass('docChecked').addClass('WF_NoAttachDocument_cLabelTitle').removeClass('WF_AttachDocument_cLabelTitle');
  $(confirmLevelObj).attr('src', 'images/calender/DocumentBlack.png');
  //$(confirmLevelObj).css({'width':'auto'});
  loadCustomLabel("WF TemplateList");
}

function deleteLevel(obj){
	$(obj).parents('div.wf_level').next('div.wfArrow').remove();
	$(obj).parents('div.wf_level').remove();
	if($(obj).attr('levelId') !='0'){
	  levelCkdId += $(obj).attr('levelId')+',';
	}
	var stepXml = generateStepXml();
	var seHtml = putStartEndFlowDiagram();
	$('#wfLevelContainer').html(seHtml);
	 var html = generateWFTemplate(stepXml);
	 $(html).insertBefore('#wfLevelContainer div.wfEnd');
	 if (!isiPad) {  
	   $("div#wfLevelContainer").mCustomScrollbar('update');
	}
	 attachSelectEvent();
	loadCustomLabel("WF TemplateList");
}
function updateWFTask() {
   var selId = tempID;
   if (selId) {
	   showTemplateDetails('update');
   } else {
	   alertFun(getValues(companyAlerts, "Alert_SelectTemplate"),'warning');
	   return false;
   }
}

function moveLevel(obj){
 setTimeout(function(){
	 if($(obj).hasClass('sb_nextStage')){
	   if($(".owl-carousel").length > 0){
		   $(".owl-carousel").data('owlCarousel').destroy();
	   }
	   var currentObj = $(obj).parents('div.wf_level').clone();
	   var nextObj = $(obj).parents('div.wf_level').nextAll('div.wf_level:first').clone();
	   $(obj).parents('div.wf_level').nextAll('div.wf_level:first').replaceWith(currentObj);
	   $(obj).parents('div.wf_level').replaceWith(nextObj);
	   if($(".owl-carousel").length > 0){
		   newStepSlider();
	   }
	   
	   $('#crumbs').find('.wf_level').css('border','1px solid #919392');
	   $('#crumbs').find('.stepSelected').css('border','1px solid blue');
	   attachSelectEvent();
	 }else{
	   if($(".owl-carousel").length > 0){
		   $(".owl-carousel").data('owlCarousel').destroy();
	   }
	   var currentObj = $(obj).parents('div.wf_level').clone();
	   var nextObj = $(obj).parents('div.wf_level').prevAll('div.wf_level:first').clone();
	   $(obj).parents('div.wf_level').prevAll('div.wf_level:first').replaceWith(currentObj);
	   $(obj).parents('div.wf_level').replaceWith(nextObj);
	   if($(".owl-carousel").length > 0){
		   newStepSlider();
	   }
	   $('#crumbs').find('.wf_level').css('border','1px solid #919392');
	   $('#crumbs').find('.stepSelected').css('border','1px solid blue');
	   attachSelectEvent();
	 }  
	  //var sliderindex = $('#crumbs').find('.stepSelected').index();
	 var sliderindex = $('#crumbs').find('.stepSelected').parent('.owl-item').index();
	 if(sliderindex > 9){
		 $('.owl-carousel').trigger('owl.goTo', (parseInt(sliderindex) - 4) , true);
		 //bxslider.goToSlide((parseInt(sliderindex)-2));
	  }
	  
 },200); 
	   
}   
 


	   var gTempId=""
	   var viewPlaceWFT = ''; //--->this variable is used when template view popup is closed
	   function viewWorkFlowTempLevels(id, place) {
			gTempId = id;
			if(place == "calendar" || place == "calendarDepWF"  ){
				viewPlaceWFT = place;
		   }
			showTemplateDetails('view');
			 $("input#wfName").attr("readonly", true); 
	   }
	   
	   function showTemplateDetails(mAction,place){
			var tempId=gTempId;
			viewTemplate();
			$("#wfTaskNewUIdivArea").hide();
			$("#wfTemplatePopDivNew").hide();
			$("#justHrLine").hide();
			$("#participantListbotLine").hide();
			$("#hideWfTemplateId").show();
			$("#wfTemplateView").show();
			$("#wfLevelContainer").show();
			$("#transparentDiv").hide();
			$("#participantsDetailsHeader").hide();
			$("#footerTasknewUI").hide();
			$("#wfLevelDetailsDiv").hide();
			$("#participantsImgListNewUI").hide();
			$("div#changeWfbtns").html("CANCEL");
			$("div#changeWfbtns").attr("onclick", "closeWfDetails();");
			$("#addLvl").hide();
			$('#deleteLevel').hide();
			levelCkdId='';
			$('#titleText').text(getValues(companyLabels,'Work_Flow'));
			$('#titleTextNewUI').text(getValues(companyLabels,'Preview_Template'));
			if(place=='Calendar'){
			  $('#wfName').val($('#tempSpan_'+tempId).attr('title'));
			}else{
			$('#wfName').val($('#viewWorkFlowTemplate_'+tempId).find('div.workflowProjTemplates span.tempName').text());
			   if(wfView == 'workspaceWFT'){
				   var projId = $('#projIdTaskHidden').val();
				   $('#wf_projId').val(projId);	
				   //$('#createWorkflowPopUp').css({"display":"block"});
				 }else{
					  var pid = $("input#globalProjectId").val();
					  $('#wf_projId').val(pid);
				 }
			 $('#wf_tempId').val(tempId);
		   }
			 $('#loadingBar').show();
			 timerControl("start");
			 checksession();
			 $.ajax({
				 
				   url: apiPath+"/"+myk+"/v1/fetchTemplateDetails?temp_id="+tempId+"&user_id="+userIdglb+"&company_id="+companyIdglb+"&viewAction="+viewPlaceWFT+"",
				   type:"GET",
				   error: function(jqXHR, textStatus, errorThrown) {
						   checkError(jqXHR,textStatus,errorThrown);
						   $("#loadingBar").hide();
						   timerControl("");
						   }, 
				   success:function(result){
					
					 var seHtml = putStartEndFlowDiagram();
					 $('#wfLevelContainer').html(seHtml);
					  var html = generateWFTemplate(result);
					  $("input#wfName").val("");
					 $("input#wfName").val(result[0].template_name);
					  $(html).insertBefore('#wfLevelContainer div.wfEnd');
					  /*if (!isiPad) {  
					   $("div#wfLevelContainer").mCustomScrollbar('update');
					  }*/
					  $("div#wfLevelContainer").mCustomScrollbar("destroy");
					   popUpScrollBar('wfLevelContainer');
					   
					  if(mAction=='view'){
						 $('#wfName').attr('readonly','true');
						 //$('#addNewLevel').hide();
						 $('#wfTemplateSave').hide();
						 $('div.viewWflevel').find('.levelDeleteIcon').hide();
						 $('p.levelP').removeAttr('onclick');
						 $('div.viewWflevel').find('img.sb_nextStage,img.sb_prevStage').hide();
						 $('div.stepImgDiv').find('img.docIcon').removeAttr('onclick');
					  }else{
						 $('#wfName').removeAttr('readonly');
						 //$('#addNewLevel').show();
						 $('#wfTemplateSave').show();
						 $('.levelDeleteIcon').show();
						 attachSelectEvent();
						  $('div.wf_level:first').find('img.sb_prevStage').hide();
						$('div.wf_level:last').find('img.sb_nextStage').hide();
					  }
					  loadCustomLabel("WF TemplateList");
					 $('#loadingBar').hide();
					timerControl("");
					}
			   });	
	   }

	   var globalTemplateId="";
	   function deleteWFTemplate(id) {
		   //$("div#workFlowTemplateViewContents").hide();
			   globalTemplateId=id
		   if (globalTemplateId) {
			   confirmFun(getValues(companyAlerts,"Alert_delete"),'delete','deleteWFTemplateConfirm');
		   } else {
			   parent.alertFun(getValues(companyAlerts, "Alert_SelectTemplate"),'warning');
			   return false;
		   }
	   }

	   function deleteWFTemplateConfirm() {
		   $('#loadingBar').show();
		   timerControl("start");
		   var projectId = $("input#globalProjectId").val();
		   $.ajax({
			   url : path + "/paLoad.do",
			   type : "POST",
			   data : {
						   act : 'deleteTemplateFromlist',
						   templateId : globalTemplateId,
						   projId : projectId
					   },
			   error: function(jqXHR, textStatus, errorThrown) {
					   checkError(jqXHR,textStatus,errorThrown);
					   $("#loadingBar").hide();
					   timerControl("");
					   }, 
			   success : function(data) {
				   checkSessionTimeOut(data);
				   templateFlagChanges = true;
				   $('#wfTemplate_'+globalTemplateId).remove();
				   globalTemplateId = "";
				   alertFun(getValues(companyAlerts,"Alert_WFdel"), "warning");
				   workflowTemplateNew(projectId);
				   $('#loadingBar').hide();
				   timerControl("");
				   }
			   });
	   }

   var gTempCopyId="";
   function copyWorkFlowTemplate(id){
		   gTempCopyId=id;
		   $("#transparentDiv").show();
		   $("div#workFlowTemplateViewContents").hide();
		   copyWFTemplateConfirm();
   }

function copyWFTemplateConfirm() {
   $('#loadingBar').show();
   timerControl("start");
   $.ajax({
	   url : path + "/workspaceAction.do",
	   type : "POST",
	   data : {act : 'loadProjectsToCopyTemplate'},
	   error: function(jqXHR, textStatus, errorThrown) {
			   checkError(jqXHR,textStatus,errorThrown);
			   $("#loadingBar").hide();
			   timerControl("");
			   }, 
	   success : function(data) {
		   parent.checkSessionTimeOut(data);
		   $('#projectPopupDiv').html(data);
			$('#projectPopupDiv').show();
			popUpScrollBar('projTopicListDiv');
			$('.ideaTransparent').show();
		   $('#loadingBar').hide();
		   timerControl("");
	   }
   });
}

   function closeProjectsPopup(){
	 $("#transparentDiv").hide();
	 $('#projectPopupDiv').html('').hide();
	 $('.ideaTransparent').hide();
	 tempID='';selProjId = '';
	 var pId =$("input#globalProjectId").val(); 
	workflowTemplateNew(pId);
	  
   }
   var selProjId='';
   function addToProject(project_id,actType){
	   selProjId=project_id;
	   confirmFun(getValues(companyAlerts, "Alert_WFCopy"), "delete", "copyTemplateConfirm");
	}

	function copyTemplateConfirm(){
	var workFlowProjectId = $("input#globalProjectId").val()
	 $('#loadingBar').show();
	 timerControl("start");
	  $.ajax({
		  url: path + "/workspaceAction.do",
		  type:'POST',
		  data:{act : 'copyTemplateToProject', tempId:gTempCopyId, selProjId : selProjId, projId : workFlowProjectId},
		  error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				   $("#loadingBar").hide();
				   timerControl("");
				   }, 
		  success:function(data){
				checkSessionTimeOut(data);
				closeProjectsPopup();
				templateFlagChanges = true;
				alertFun(getValues(companyAlerts,"Alert_TemplateAdded"), 'warning');
				 workflowTemplateNew(workFlowProjectId);
				$('#loadingBar').hide();
				timerControl("");
				gTempCopyId="";
			   $("#transparentDiv").hide();
		  }
	  });
	}

function attachSelectEvent(){
  /*$('.wfStart>div, .wf_level').click(function(){
	   alert($(this).attr("stepIndex"));
	   $('.wf_level').css('border','1px solid #919392').removeClass('stepSelected');
	   $('.wfStart>div').css('border','none').removeClass('stepSelected');
	   $('#crumbs div.wf_level').removeClass('activeTab').removeClass('stepSelected').css('border','1px solid #919392');
	   $(this).css('border','1px solid blue').addClass('stepSelected').addClass('activeTab');
   });*/
   if($("#crumbs").find('div.wfLevelBxslider').length > 0){
	   $('div.wfLevelBxslider').find('img.sb_prevStage, img.sb_nextStage').show();
	   $('div.wfLevelBxslider:first').find('img.sb_prevStage').hide();
	   $('div.wfLevelBxslider:last').find('img.sb_nextStage').hide();
   }else{
	   $('div.wf_level').find('img.sb_prevStage, img.sb_nextStage').show();
	   $('div.wf_level:first').find('img.sb_prevStage').hide();
	   $('div.wf_level:last').find('img.sb_nextStage').hide();
   }
}
//function putStartEndFlowDiagram(){
   //alert("putStartEndFlowDiagram in workflowtemplatefunction.js");
  //var html ='<div class="wfStart" style="float: left;margin: 3em 0 0 0">'
			  /*+'  <div style="background-color: green; border-radius: 40px /25px; float: left; height: 50px; width: 80px; margin-top: 50%;color: rgb(255, 255, 255); transform: translateY(-20%);display:table;">'
		   +'    <span style="display:table-cell;vertical-align:middle;text-align:center;">Start</span>'
		   +'  </div>'*/
		   //+'    <img src="'+path+'/images/workspace/start.png" style = "width: 80%">'
		   //+'</div>'
		   //+'<div class="wfArrow" style="float: left;margin: 3.8em 0 0 0">'
		   //+'    <img src="'+path+'/images/workspace/wf_marrow.png" style = "width: 80%;">'
		   //+'</div>'
		   //+'<div class="wfEnd" style="float: left;margin: 3em 0 0 0">'
		   /*+' <div style="background-color: #EB2727;  border-radius: 40px /25px; float: left; height: 50px; width: 80px; margin-top: 50%;color: rgb(255, 255, 255); transform: translateY(-20%);display:table;">'
		   +'   <span style="display:table-cell;vertical-align:middle;text-align:center;">End</span>'
		   +' </div>'*/
			//+' <img src="'+path+'/images/workspace/end.png" style = "width: 80%">'
		   //+'</div> ';
	  //return html;
//}

function putStartEndFlowDiagram(){
   var html ='<div class="wfStart" style="float: left;margin-top: 49px;">'
					   +'    <img src="images/workspace/start.png" style = "width: 80%">'
			  +'  </div>'
			  +'<div class="wfArrow" style="float: left;margin-top: 61px;">'
				   +'<img src="images/workspace/wf_marrow.png" style = "width: 80%;">'
			  +'</div>'
			  +'<div class="wfEnd" style="float: left;margin-top: 49px;">'
					 +' <img src="images/workspace/end.png" style = "width: 80%">'
			  +'</div>';
	 return html;
}

function generateWFTemplate(data){
	   var json = data;
		 var html ='';
		 for(var i=0;i< json.length;i++){
			  if((i!=0) && (i%4 == 0)){  //---------per row 4 steps
				  var j = i/4;
				  html += generateWFTemplateNode(j);
			   }
			   html+= generateWFTemplateStep(json[i].level_id,json[i].level_name,json[i].level_document);
		  }
		  
   return html;
}

function generateWFTemplateNode(j){
   var html ='<div class="wfCountDiv_'+j+'" style="height: 115px; float: left; width: 51px; margin: 10px 5px 5px 0px;">'
			  +'  <div  style="display:table;background-color: #fff;border:1px solid #000;color:#000; border-radius: 30px; float: left; height: 30px; width: 30px; margin-top: 47px; ">'
		   +'     <span style="display:table-cell;vertical-align:middle;text-align:center;font-size:14px;">'+j+'</span>'
		   +'  </div>'
		   +'</div>'
		   +'<hr class="wfHr_'+j+'" style="width: 100%; border: medium none; float: left;">'
		   +'<div class="wfCountDiv_'+j+'" style="height: 115px; float: left; width: 51px; margin: 10px 5px 5px 0px;">'
		   +'  <div  style="display:table;background-color: #fff;border:1px solid #000;color:#000; border-radius: 30px; float: right; height: 30px; width: 30px; margin-top: 47px; margin-right: 5px;">'
		   +'     <span style="display:table-cell;vertical-align:middle;text-align:center;font-size:14px;">'+j+'</span>'
		   +'  </div>'
		   +'</div>'
		   +'<div class="wfCountArrow_'+j+'" style="float: left;margin: 61px 0 0 0">'
		   +'<img src="images/workspace/wf_marrow.png" style = "width: 80%;">'
			  +'</div>';
		   
		return html;  
}


function generateWFTemplateStep(levelId,levelName,levelDoc){
	var newStepClass="";
	var docChecked="";  
	levelName = replaceSpecialCharacterForTitle(levelName);
	levelName= unicodeTonotificationValue(levelName);	    
	if(levelName=='')
		 newStepClass="newStep";
	if(levelDoc!='' && levelDoc=='Y')
		 docChecked="checked";     
	   var html ='<div class="wf_level levelDivs viewWflevel '+newStepClass+'" stepIndex="" onclick="tabActive(this);event.stopPropagation();" style="margin:10px 5px 5px 0px;overflow:hidden">'
			   //+'  <div style="height:25px;margin:0 5px;width:100px;display:none;" class="SB_dragContainerDiv">'    
			   //+'     <div style="margin: 0px;float:left;cursor:pointer;height:18px;width:18px;display:none;">'
			   //+'          <input class="level_chbox" type="checkbox" onclick="setUncheckAll(this);" levelId="'+levelId+'" style="margin:3px;cursor:pointer;" />'
			   //+'     </div>'
			   //+'     <div class="" style="cursor: default;width:50px;height:100%;float:left;"></div>'
			   //+'     <img class="levelDeleteIcon" onclick="deleteLevel(this);" levelId="'+levelId+'" src="'+path+'/images/workspace/wfminus.png" title="'+getValues(customLabels,'Delete')+'"style="float: right; margin: 2px 1px; height: 15px; width: 15px;cursor:pointer;">'
			   //+'  </div>'
				+'  <div style="float: left;margin: 5px 5px;clear:both;">'
			   +'	  <div style="height:75px;float:left;width:100%;"> '    
			   +'	    <p title="'+levelName+'" style="margin:0px !important;cursor:pointer;height:100%;overflow:hidden;"  onclick="editLevel(this);" class="levelP">'+levelName+'</p>'   
			   //+'      <textarea style="width:98%;height:100%;background:none;border:1px solid #bfbfbf;border-radius:2px;display:none;" onblur="editComplete(this);"></textarea>'
			   +'      <textarea  onblur="editComplete(this);" style="width:100%;height:100%;border:none;display:none;"></textarea>'
			   
			   +'	    <input type="hidden" class="levelId" value="'+levelId+'" />'   
			   +'    </div>'
				+'  </div>'
				+'  <div class="stepImgDiv" style="height:25px;background:none;border-top:1px solid #999990;clear:both;margin:0 4%;width:92%;" >'  
				//+'		<div style="  width: 30%;  float: left;">';
				if(levelDoc!='' && levelDoc=='Y')
				   html+= '  <img style="float: left;cursor:pointer;height: 26px;width: 26px;" levelId="'+levelId+'" src="images/tasksNewUI/docman.png" onclick="attachDocumentToStep(this);" class="imgCss docIcon docChecked WF_AttachDocument_cLabelTitle"> ';
				else
				   html+= '  <img style="float: left;cursor:pointer;height: 26px;width: 26px;" levelId="'+levelId+'" src="images/calender/DocumentBlack.png" onclick="attachDocumentToStep(this);" class="imgCss docIcon WF_NoAttachDocument_cLabelTitle"> '  ;
				//html+= '</div>'    
		   html+= '		<div align="center" style=" height: 20px; width: 40%; float: left; vertical-align: middle;margin-top:2px;">'
				+'          <img src="images/workspace/leftarrowworkflow.png" onclick="moveLevel(this);" class="sb_prevStage WF_Previous_cLabelTitle" title="" style = "float: none;margin-right: 2px;">'
				+' 		    <img src="images/workspace/rightarrowworkflow.png" onclick="moveLevel(this);" class="sb_nextStage WF_Next_cLabelTitle" title="" style = "float: none;margin-right: 0px;margin-left: 2px;">'
				+'      </div>'
				+'		<div style="  width: 26%;  float: left;margin-top: 2px;"">'
				+'          <img class="levelDeleteIcon" onclick="deleteLevel(this);" levelId="'+levelId+'" src="images/workspace/wfminus.png" title="Delete" style="float: right; margin: 4px 1px; height: 15px; width: 15px;cursor:pointer;">'
				+'      </div>'
				+'  </div>'
			   +'</div>'
			   +'<div class="wfArrow" style="float: left;margin-top: 61px;">'
			   +'<img src="images/workspace/wf_marrow.png" style = "width: 80%;">'
			   +'</div>';
   return html;                  
}
function generateWFTemplateStepForTask(tabIndex,levelId,levelName,levelDoc,taskId,workflowstep_blur){ 
	   var newStepClass="";
	   var docChecked="";
	   var grayedOutClass="";
	   var backgrdcol = "";
	   if(workflowstep_blur == 'Y'){
			  grayedOutClass = "grayedOut";
			  backgrdcol = "background-color: #ffffff";
		}else{
		   grayedOutClass = "";
		   backgrdcol = "background-color: #8cc63f";
		}
	   if(levelDoc!='' && levelDoc=='Y')
		 docChecked="checked";     
	   var html ='<div class="wf_level levelDivs '+newStepClass+' '+grayedOutClass+'" stepIndex='+tabIndex+' onclick=\"tabActive(this);showTabDetails(this);event.stopPropagation();\"  levelId=\"'+levelId+'\"  taskId = \"'+taskId+'\" style="border: 1px solid #999999;float:left;border-radius: 5px;margin:10px 5px 5px 0px;">'
			   //+'<input type="radio" name="" id="">'
			   +'<div class="radioBtn" onclick="disableThisTask(this);event.preventDefault();"  id=radBtn_'+tabIndex+' style="cursor:pointer;border-radius: 5px;margin-left: -3px;margin-top: -3px;border:  1px solid rgb(145, 147, 146);float:left;height: 10px;width: 10px;'+backgrdcol+'"></div>'
			   +'  <div class="wfonclick" style="float: left;margin: 5px 5px;clear:both;width:95%;">'
			   +'    <div style="height:75px;float:left;width:100%;"> '    
			   +'      <p title="'+levelName+'" style="width:100%;margin:0px !important;cursor:pointer;height:100%;overflow:hidden;" ondblclick="editLevel(this);" class="levelP">'+levelName+'</p>'   
			   +'      <textarea  onblur="editComplete(this);" style="width:100%;height:100%;border:none;display:none;"></textarea>'
			   +'      <input type="hidden" class="levelId" value="'+levelId+'" />'   
			   +'    </div>'
			   +'  </div>'
			   +'  <div class="stepImgDiv" style="height:30px;background:none;border-top:1px solid #999990;clear:both;margin:0 4%;width:92%;" >'
			   //+      '  <img style="float: left;margin-left: -6px;cursor:pointer;height: 26px;width: 26px;margin-top: 2px;" levelId="'+levelId+'" src="'+path+'/images/calender/DocumentBlack.png" onclick="attachDocumentToStep(this);" class="imgCss docIcon WF_NoAttachDocument_cLabelTitle pull-left"> '    
			   //+'      <div style="  width: 28%;  float: left;">';
			   if(levelDoc!='' && levelDoc=='Y')
				  html+= '  <img style="float: left;margin-left: -6px;cursor:pointer;height: 26px;width: 26px;margin-top: 1px;" levelId="'+levelId+'" src="images/tasksNewUI/docman.png" onclick="attachDocumentToStep(this);" class="imgCss docIcon docChecked WF_AttachDocument_cLabelTitle"> ';
			   else
				  html+= '  <img style="float: left;margin-left: -6px;cursor:pointer;height: 26px;width: 26px;margin-top: 1px;" levelId="'+levelId+'" src="images/calender/DocumentBlack.png" onclick="attachDocumentToStep(this);" class="imgCss docIcon WF_NoAttachDocument_cLabelTitle"> '  ;
			   //html+= '</div>'  
		   html+='          <img id="stepAttachImgNewUI" src="images/tasksNewUI/subLvlAttach.png" onclick="showUploadedWrkflowDoc(\'\',\'steplevel\',\'\',\''+taskId+'\');showUploadedWrkflowDocOptn(this,\'steplevel\');event.stopPropagation();" style = "height:22px;width:22px;float: left;margin-top: 2px;cursor:pointer;">'  
			   +'      <div align="center" style=" height: 20px;margin-top: 2px;width: 39px;float: left;vertical-align: middle;">'
			   +'          <img src="images/workspace/leftarrowworkflow.png" onclick="moveLevel(this);" class="sb_prevStage WF_Previous_cLabelTitle" title="" style = "float: none;margin-right: 2px;">'
			   +'          <img src="images/workspace/rightarrowworkflow.png" onclick="moveLevel(this);" class="sb_nextStage WF_Next_cLabelTitle" title="" style = "float: none;margin-right: 0px;margin-left: 2px;">'
			   +'      </div>'
			   +'      <div class="pull-right" style="margin-top: 3px;width: 72px;float: left;margin-right:0px !important;">'
			   +'          <img id="WFdependencyTask_'+tabIndex+'" style="float: left;margin-left: 6px;margin-top: 2px;cursor: pointer;opacity: 1;width: 20px;height: 18px;" src="images/calender/wfDependency.png" >'
			   +'          <img id="createNew_'+tabIndex+'" onclick="addWFCustomLevel(this);event.stopPropagation();event.preventDefault();" class="levelDeleteIcon" levelId="'+levelId+'" src="images/tasksNewUI/add.png" title="Add Step" style="float: left;cursor:pointer;width: 16px;height: 16px;margin-left: 5px;margin-top: 4px;">'
			   +'          <img id="deleteStep_'+tabIndex+'" class="levelDeleteIcon" onclick="deleteCustomStep(this,\''+mode+'\');" levelId="'+levelId+'" src="images/tasksNewUI/minus.png" title="Delete" style="margin-left: 6px;float: left;width: 16px;height: 16px;cursor:pointer;margin-top: 4px;">'
			   +'      </div>'
			   +'  </div>'
			   +'</div>'
			   +'<div class="wfArrow" style="float: left;margin-top: 61px;">'
			   +'<img src="images/workspace/wf_marrow.png" style = "width: 80%;">'
			   +'</div>';
		   
   html=html.replaceAll("CH(51)","\"").replaceAll("CHR(26)", ":").replaceAll("CHR(41)", " ").replaceAll("CH(52)","'").replaceAll("CH(20)","/").replaceAll("CH(70)","\\").replaceAll("&#39;","'").replaceAll("&quot;","\"").replaceAll("CH(80)","-").replaceAll("CH(81)","/").replaceAll("CH(82)",",");
   return html;
}

/*function generateWFTemplateStepForTask(tabIndex,levelId,levelName,levelDoc,taskId,workflowstep_blur){ 
		var newStepClass="";
		var docChecked="";
		var grayedOutClass="";
		//alert("mode:>>>"+mode);
	//if(levelName=='')
	//     newStepClass="newStep";
		 levelName = replaceSpecialCharacterForTitle(levelName);    	    				    						
		levelName= unicodeTonotificationValue(levelName);       
	if(workflowstep_blur == 'Y'){
		grayedOutClass = "grayedOut";
	}else{
		grayedOutClass = "";
	}
	if(levelDoc!='' && levelDoc=='Y')
		 docChecked="checked";     
	   var html ='<div class="wf_level levelDivs '+newStepClass+' '+grayedOutClass+'" stepIndex='+tabIndex+' onclick=\"tabActive(this);showTabDetails(this);\"  levelId=\"'+levelId+'\"  taskId = \"'+taskId+'\" style="margin:10px 5px 5px 0px;">'
			   +'  <div style="float: left;margin: 5px 5px;clear:both;width:95%;">'
			   +'	  <div style="height:75px;float:left;width:100%;"> '    
			   +'	    <p title="'+levelName+'" style="width:100%;margin:0px !important;cursor:pointer;height:100%;overflow:hidden;" ondblclick="editLevel(this);" class="levelP">'+levelName+'</p>'   
			   +'      <textarea  onblur="editComplete(this);" style="width:100%;height:100%;border:none;display:none;"></textarea>'
			   +'	    <input type="hidden" class="levelId" value="'+levelId+'" />'   
			   +'    </div>'
				+'  </div>'
				+'  <div class="stepImgDiv" style="height:25px;background:none;border-top:1px solid #999990;clear:both;margin:0 4%;width:92%;" >'  
				+'		<div style="  width: 28%;  float: left;">';
				if(levelDoc!='' && levelDoc=='Y')
				   html+= '  <img style="float: left;margin: 1px 3px;cursor:pointer;" levelId="'+levelId+'" src="'+path+'/images/workspace/wf_doc_man.png" onclick="attachDocumentToStep(this);" class="imgCss docIcon docChecked WF_AttachDocument_cLabelTitle"> ';
				else
				   html+= '  <img style="float: left; margin: 2px 4px;cursor:pointer;" levelId="'+levelId+'" src="'+path+'/images/workspace/wf_doc1.png" onclick="attachDocumentToStep(this);" class="imgCss docIcon WF_NoAttachDocument_cLabelTitle"> '  ;
				html+= '</div>'    
				+'		<div align="center" style=" height: 20px; width: 40%; float: left; vertical-align: middle;">'
				+'          <img src="'+path+'/images/workspace/leftarrowworkflow.png" onclick="moveLevel(this);" class="sb_prevStage WF_Previous_cLabelTitle" title="" style = "float: none;margin-right: 2px;">'
				+' 		    <img src="'+path+'/images/workspace/rightarrowworkflow.png" onclick="moveLevel(this);" class="sb_nextStage WF_Next_cLabelTitle" title="" style = "float: none;margin-right: 0px;margin-left: 2px;">'
				+'      </div>'
				+'		<div style="  width: 32%;  float: left;">'
				+'          <img id="WFdependencyTask_'+tabIndex+'" style="float: left; margin-left: 2%; margin-top: 2px; cursor: pointer; opacity: 1;width: 22px;" src="'+path+'/images/calender/wfDependency.png" >'
				+'          <img id="deleteStep_'+tabIndex+'" class="levelDeleteIcon" onclick="deleteCustomStep(this,\''+mode+'\');" levelId="'+levelId+'" src="'+path+'/images/workspace/wfminus.png" title="Delete" style="float: right; margin: 4px 1px; height: 15px; width: 15px;cursor:pointer;">'
				+'      </div>'
				+'  </div>'
			   +'</div>'
			   +'<div class="wfArrow" style="float: left;margin: 3.8em 0 0 0">'
			   +'<img src="'+path+'/images/workspace/wf_marrow.png" style = "width: 80%;">'
			   +'</div>';
   return html;                  
}*/


   function createWFLCancel(){
	   if(viewPlaceWFT == 'calendar' ){
		   $('#createWorkflowPopUp').hide();
		   $('body').find('#createTaskPopup, #wfTemplatePopDiv').show();
		   $('#wfName').val("");
			 tempID='';levelCkdId = '';
	   }else if(viewPlaceWFT == 'calendarDepWF' ){
		   $('#createWorkflowPopUp').hide();
		   $('body').find('#createTaskPopup').show();
		   $('#wfName').val("");
			 tempID='';levelCkdId = '';
	   }else{
			 $('#createWorkflowPopUp').hide();
			$('.ideaTransparent').hide();
			 $('#wfName').val("");
			 tempID='';levelCkdId = '';
			 $("div.workflowTemplateList").css("background-color", "");
		 }
		 $('#createTaskPopup').hide();
		 $('#transparentDiv').hide();
   }


   var flowIdGlo = '';
   var tempIdGlo = '';
   function loadWFLevelContent(flowId,tempId,taskName, lvlId){
	   var flowId1 = flowId;
	   var tempId1 = tempId;
	   flowIdGlo = flowId;
	   tempIdGlo = lvlId;
	   estFlag = true;
	   updateResetValFlag = true;
	   levelCkdId='';
	   userListFlag = true;
	   wfPlaceFlag == "Workflow";
	   $("body").css("overflow", "hidden");	
	   $("div#transparentDiv").show();
	   $('#loadingBar').show();
	   timerControl("start");
		  $('#viewWorkFlowTempLevelContainer').css('display','block');
		  $("div#viewWorkFlowTempLevelContainer").show();
	   //$("div#createWorkflowTempDivNew").hide();
	   if(view!="listV"){
			$('#viewWorkFlowTempLevelContainer .workflowTempVeiwL').val($('#assignedTaskName'+flowId).attr('title'));
	   }else{
			  var title = $('div[taskType=Workflow][taskId='+flowId+'],div[taskType=workflowTask][taskId='+flowId+']').find('div.rTasksName').find('span.taskSub').attr('title');
			  $('#viewWorkFlowTempLevelContainer .workflowTempVeiwL').val(title);
	   }   
	   $.ajax({
			  url: path+"/workspaceAction.do",
		   type:"POST",
		   data:{act:"fetchWorkflowSteps",tempId:tempId1,flowId:flowId1},
		   error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				   $("#loadingBar").hide();
				   timerControl("");
				   }, 
		   success:function(result){
			   checkSessionTimeOut(result);
			   var xmlData = result.split('</wfLevel>');
			   var stepLength = xmlData.length-1;
				var w =parseInt( $('#wfTemplateDiv').width()-20);
				  var tabW = parseInt(170);
				  var tabCount = Math.floor(w/tabW) ;
				   var html ='<div id="crumbs" style = "float: left;padding-left: 10px;max-height: 15%;overflow: hidden;;width: 100%;" >';
				if(xmlData.length >0){
					 var j=0;
					 for(var i=0;i< xmlData.length-1;i++){
						if(j==0 ){
							   html+='   <ul style = "padding: 0">';
						 }
						 html+='<li>';
						 //html+=' <a href="#1" class="liAnch" onclick="tabActive(this);getTabContent(this);" ondblclick=\"editWFCustomStep(this);\" flowId="'+flowId+'" stepIndex="0" levelId="'+getValues(xmlData[i],"id")+'" action="'+getValues(xmlData[i],"status")+'" >';
						 html+=' <a href="#1" class="liAnch"  flowId="'+flowId+'" stepIndex="0" levelId="'+getValues(xmlData[i],"id")+'" action="'+getValues(xmlData[i],"status")+'" >';
						 html+='   <span style="width: 100%; float: left; height: 100%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;font-size:14px;padding: " title="'+getValues(xmlData[i],"name")+'">'+getValues(xmlData[i],"name")+'</span>';
						 html+="    <input type='text' value='' class='inputWFCustomStep' style='color: #848484;display:none;width:100%;border:1px solid #bfbfbf; height:20px;' onkeydown='setWFStepName(event,this);' onblur='checkWFStepName(this);'/>"; 
						html+=' </a>';
						 html+='</li>';
					
				   
						 j++;
					
						if(j!=0 && (j%tabCount==0)){
							  html+='    </ul></br>';
							  j=0;
						}
						flowStatusTab = getValues(xmlData[i],"flowStatus");
					}
				  }
				html+='</div>'
					+'<div id="levelContentDiv" class = "wsTaskBaseDivCss" style="border-top: 1px solid rgb(191, 191, 191);height: 75%;float: left;"></div>';
				   
				  $('#wfTemplateDiv').html(html);
				 var wid = $('#wfTemplateDiv').width();
				  var heightNew = $('#wfTemplateDiv').height();
				  var crumbsHeight = $('#crumbs').height();
				  var heightOfDiv = heightNew - crumbsHeight - 78;
				  $('#levelContentDiv').css('height',heightOfDiv +'px')
				  //$('#levelContentDiv').css('width',(wid-20)+'px');
				  $('#workflowNameInDetails').val(taskName);
			
				  //$('#levelContentDiv').css('height',($('#wfTemplateDiv').height()- $('#crumbs').height()-20)+'px');
				  getTabContent($('#crumbs ul:first-child li:first-child a'));
				  tabActive($('#crumbs ul:first-child li:first-child a'));
				  $('#crumbs ul li a').css({'padding-top':'3px'});
				  if(!isiPad){
					 $("div#crumbs").mCustomScrollbar("destroy");
				   popUpScrollBar('crumbs');
				  } 
				  var tim=null;
				 $('#wfTemplateDiv').find('a.liAnch').click(function(e){
				   var aObj = $(this);
					  if(tim){
						  clearTimeout(tim);
						 tim=null;
						 if(!$(aObj).hasClass('activeTab')){
						   tabActive(aObj);
						   getTabContent(aObj);
						 } 
						 editWFCustomStep(aObj);
				   }else{
					   tim=setTimeout(function(){
							 tim=null;
							  tabActive(aObj);
							  getTabContent(aObj);
						},250)
					}
			  });//onclick
			  
			 //$('#crumbs').css('width', (stepLength*190)+'px');//------190 is step headers approx width .need to change this value whenever hearder width changes.
			}
		   });
   }

	var userListFlag = true;
	var userListResult='';
	
   function getTabContent(obj){
		  var levelId = $(obj).attr('levelId');
		  var flowId = $(obj).attr('flowId');
		  if(levelId == "" || flowId == ""){
			  levelId = tempIdGlo;
			  flowId = flowIdGlo ;
			  
		  }
		  levelIdWFGlobal = levelId;
		  
		  $('#loadingBar').show();
		 timerControl("start");
		  $.ajax({ 
		   url: path+"/workspaceAction.do",
		   type:"POST",
		   data:{act:'getLevelTabDetails',levelId:levelId,flowId:flowId,parId:'assignedWorkFlow',taskType:'assignedWorkFlow'},
		   error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				   $("#loadingBar").hide();
				   timerControl("");
				   }, 
		   success:function(result){
			   checkSessionTimeOut(result);
			   var data = result.split('###@###');
			   $("#levelContentDiv").html(data[0]);
			   $('.ui-slider-disabled').find("a").css('background', 'url("images/handleDisable.png") repeat-x scroll 50% 50% rgba(0, 0, 0, 0)');
			   $( ".ui-slider-disabled" ).on( "mousedown", function(event) {
				   parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');
			   });		   
			   $( ".userCompPerSlider" ).on( "click", function(event) {
				   parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');
			   });
			   if(!isiPad){
					  $("div#prtcpntSection").mCustomScrollbar("destroy");
				   popUpScrollBar('prtcpntSection');
				}
					  $('#levelContentDiv .closePopup').hide();
					  $('#levelContentDiv .hideOptionDiv').css('right','10px');
					  //$('#levelContentDiv #participantMail').css('position','relative');
					  $('input.hasTimePicker').timepicker({ 'timeFormat': 'H:i' });
				   initwfdhtmlxdatecalNew("wFStart","wFEnd"); 
				   $('#levelTaskSubContainer').height($('#wfTemplateDiv').height()- $('#crumbs').height()-20 -40);
				   
				   var heightNew = $('#wfTemplateDiv').height();
					var crumbsHeight = $('#crumbs').height();
					var heightOfDiv = heightNew - crumbsHeight - 78;
					$('#levelContentDiv').css('height',heightOfDiv +'px')
				   $('#wfParticipantEmail').height($('#levelTaskSubContainer').height() - 230);
				   initWFSlider();   
				   var taskRemainder=$("#taskRemainder").val();
				   var taskRemaindertype=$("#taskRemaindertype").val();
					  parId='assignedWorkFlow';
					  myTaskId = data[1]; 
					  var userUI = $('#existingTaskUsers').html(); // existingTaskUsers is a div in CalendarDao where we are storing user details.
					  prepareExistingTaskUserUI(jQuery.parseJSON(userUI));
				  
					  $('#loadingBar').hide();
					  timerControl("");
					  
					  $(".taskSub").each(function(){
					   var title = $(this).attr('title').replaceAll("CHR(41)"," ").replaceAll("CHR(39)","'").replaceAll("CHR(40)","\"");
					   $(this).attr('title',title);
				   });
				   if(flowStatusTab=="Completed"){
					   $("#ideaTaskBtnUpdSave").attr('onclick','').removeClass(".ideaSaveBtn").addClass('ideaSaveBtnDisable');
				   }
				   if(hideButton=="YES"){
					   $(".workflowTempVeiwL").prop('disabled', true).css('background','none');
					   $("#ideaTaskBtnUpdSave").hide();
				   }
				   var projId = $('#wfProjID').val();
				   var popH=$("#wfUserListContainer").height();
				   var mH=popH/2;
			   
				   document.getElementById("remDrpDown").value=taskRemainder;
				   document.getElementById("remTypeDrpDown").value=taskRemaindertype;
					  $('#wfUserListContainer').html('');
				   $('#wfUserListContainer').append('<div id=\"loadingB\" align=\"center\" style=\"margin-top:'+mH+'px;overflow:hidden;\">loading...</div>');
				   loadSprintUsers(projId, "Workflow");
				   /*if(userListFlag){
					   $.ajax({ 
							   url: path+'/calenderAction.do',
							   type:"POST",
							   data:{act:'fetchParticipants',projId:projId,userId:UserId},
								  success:function(result){
								   checkSessionTimeOut(result);
									 $('#levelContentDiv').find('#wfUserListContainer').html(result);
									  $('#loadingBar').hide();
									  timerControl("");
									  userListResult= result;
									  userListFlag = false;
									  $('#wfUserListContainer').find('div[id^=participant_]').each(function(){
										 $(this).find('img[name=addParticipants], div[id^=particpantName_]')
										 .attr('ondblclick','addWFParticipantEmail(this)');
									  });
									  
									  $('.odd, .even').each(function() {
									   var userId = $(this).attr('id').split('_')[1];
									   $('#wfUserListContainer').find('#participant_'+userId).css('display','none');
								   });
								   
								   if(projId == '0'){
										  $("select#userProject").find("option#0").attr("selected", "selected");
									  }else{
										  $("select#userProject").find("option#"+projId).attr("selected", "selected");
									   $("select#userProject").prop("disabled",true);
								   }
								   if(!isiPad) {
									   $("div#wfUserContainer").mCustomScrollbar("destroy");
									   popUpScrollBar('wfUserContainer');
								   }
									  if($('[id^=participant_]:visible').length < 1){
										  $('#wfUserListContainer').html("");
										  $('#wfUserListContainer').append('<div id="innerDiv18" align="center"> No Participant</div>');
									  }
								  }
						   });
						}/*else{
							  if(projId!= '0'){
								 $("select#userProject").find("option#"+projId).attr("selected", "selected");
								 $("select#userProject").prop("disabled",true);
							  }else{
								 $("select#userProject").find("option#0").attr("selected", "selected");
							  }
							 $('#wfUserListContainer').html(userListResult);
					  
							  $('#wfUserListContainer').find('div[id^=participant_]').each(function(){
									 $(this).find('img[name=addParticipants], div[id^=particpantName_]').attr('ondblclick','addWFParticipantEmail(this)');
							  });
						   $('.odd, .even').each(function() {
							   var id = $(this).attr('id');
							   var userId = id.split('_')[1];
							   $('#wfUserListContainer').find('#participant_'+userId).css('display','none');
						   });
						   if($('[id^=participant_]:visible').length < 1){
							   $('#wfUserListContainer').append('<div id="innerDiv19" align="center"> No Participant</div>');
						   }
							  if(!isiPad) {
							   scrollBar('wfUserListContainer');
						  }
					   }*/
				   }
				 });
			 
   }

function addWFParticipantEmail(obj) {			/*Add Participants to the Task */		
   var addParticipantId = $(obj).attr('name');
   var projectUserCount = $('.userCheck').length;
   //var divCount = $('#participantEmail').length;
   var isiPad = navigator.userAgent.match(/iPad/i) != null;
   
	   $('.userCheck').each(function() {
		   var partcipantId = $(this).prev().attr('id');
		   var prtcpntId = $(this).prev().attr('value');
		   var participantName = $('#'+partcipantId).text();
		   if(participantName.length > 17)
			   participantName = participantName.substring(0,16)+'...';
		   var participantEmail = $('#'+partcipantId).attr('title');
		   $('.addParticipant').hide();
		   
		   $('.createEvent').css('display','block');
		   $('.createEvent1').css('display','none');
		   var emailDiv = "";
			if(selectedTaskemailId.indexOf(participantEmail+"_"+prtcpntId)<0){
				   taskWFEmailDiv(prtcpntId,partcipantId,participantName,participantEmail);
			   }
		   if(!isiPad)
			   scrollBar('wfUserListContainer');
	   });
	   
	   if(projectUserCount == '0') {
		   participantEmail = $(obj).attr('title');
		   prtcpntId = $(obj).parent().attr('value');
		   participantName = $('#particpantName_'+prtcpntId).text();
		   if(participantName.length > 17) {
			   participantName = participantName.substring(0,16)+'...';
		   }
		   partcipantId = $('#particpantName_'+prtcpntId).attr('id');
		   taskWFEmailDiv(prtcpntId,partcipantId,participantName,participantEmail);
		   if(!isiPad)
			   scrollBar('wfUserListContainer');
	   }
	   
	   
	   progressBar();
	   var isiPad = navigator.userAgent.match(/iPad/i) != null;
	   if(!isiPad)	{
		   $("#wfParticipantEmail").mCustomScrollbar("update");
	   }	
	   $('#checkAll').removeClass().addClass('removeAll');	
	   //divCount++;
	   $( ".ui-slider-handle" ).on( "mousedown", function(event) {
							  parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');});
	   $( ".userCompPerSlider" ).on( "click", function(event) {
							  parent.alertFun(getValues(companyAlerts,"Alert_CantUpProgress"),'warning');});
   
   if($('[id^=participant_]:visible').length < 1){
	  $('#userListContainer').append('<div id="innerDiv16" style="margin-top:-459px;" align="center"> No Participant</div>');
   }
}

function taskWFEmailDiv(prtcpntId,partcipantId,participantName,participantEmail){    /*  Add Users in Assigned Task Popup  */
   selectedTaskemailId=selectedTaskemailId+participantEmail+"_"+prtcpntId+',';
	   emailDiv = "<div  style=\"float:left;width: 100%;\" value=\""+prtcpntId+"\" id=\"emailPrtcpnt_"+prtcpntId+"\" class=\"updatePrtcpnt\">"
			   +"<div align=\"left\" style=\"font-size:88%;height:25px;overflow:hidden;float:left;color:#848484;width:25%;font-weight:normal;float:left;\" title=\""+$('#'+partcipantId).text()+"\">"+participantName+"</div>"
			   +"<div align=\"left\" style=\"float:left\" title=\"0\">"
			   +"<div style=\"float:left;\"><div class=\"demo\" style=\"float:left;width:188px;font-weight:normal;\"> "
			   +"<div class=\"userCompPerSlider userCompPerSlider1\"  style=\"width: 75%;float:left;margin:6px 0px 0px 0px;background: none repeat scroll 0 0 #C1C5C6;color:#848484;\">0</div>"
			   +"<div style=\"float:left;width:15%;text-align:right;margin-top: -3px;\"><span style=\"float:none;width:30px;color:#848484;font-size:88%;\" id=\"taskAmount\">0%</span></div>"
			   +"</div>"
			   +"</div></div>"
			   +"<div style=\"font-size:88%;float: left;height:20px;width:20%;font-weight:normal;color: #848484;padding-left:1.5%;\">Created</div>"
			   +"<div style=\"font-size:90%;float: left; height: 15px; font-weight: normal; color: rgb(132, 132, 132); width: 4%;overflow:hidden;text-align:right;margin-top: 3px;\">0</div>"
			   +"<div class=\"min\" style=\"float:left;padding-left:1%;color:#848484;\">h</div>"
			   +"<div style=\"font-size:90%;float: left; height: 15px; font-weight: normal; color: rgb(132, 132, 132); width: 5%;overflow:hidden;text-align:right;margin-top: 3px;\">0</div>"
			   +"<div class=\"min\" style=\"float:left;padding-left:1%;color:#848484;width:4.2%;\">m</div>"
			   +"<div id=\"userWFOptions_"+prtcpntId+"\" class=\"hideOptionDiv\" style=\"background-color: #ffffff;border: 1px solid #a1a1a1;border-radius: 5px;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.6);display: none;float: right;height: auto;margin-right: 25px;margin-top: 3px;max-height: 105px; min-width: 100px;padding-left: 5px;position: absolute;right: 39%;width: 160px;z-index: 5010;\">"
			   +"</div>"
			   
			   +"<div id="+prtcpntId+" task_id=\"\" task_type=\"\" email=\""+participantEmail+"\" class=\"options\" onclick=\"showWFTaskUserOptions(this);\" style=\"float:left;margin-left:8px;width:20px;margin-top:0px;position:relative;\"> <img id=\"taskUserOption_"+prtcpntId+"\" title=\""+getValues(Labels,'Options')+"\" class=\"options\" src=\"images/more.png\" style=\"float: right;height:17px;width:8px;padding-left:2px;margin-top:1px;\">"
			   +"</div>"
			   +"</div>"
			   +"<div id=\"userComments_"+prtcpntId+"\" class=\"commentsSec\" style=\"float: left;width:100%;cursor:pointer;font-weight:normal;\" ></div>"
			   +"<div style=\"float: left;width:100%;cursor:pointer;font-weight:normal;display:none;margin-top:10px;\" class=\"documentSection\" id=\"documentSection_"+prtcpntId+"\"></div>";
	   $("div#wfUserListContainer").find('#participant_'+prtcpntId).hide();
	   var isiPad = navigator.userAgent.match(/iPad/i) != null;
	   if(!isiPad)	{
		   $('#addParticipantTasks').find('#prtcpntContainer').find('div#prtcpntSection').append(emailDiv);
		   $("div#wfParticipantEmail").mCustomScrollbar("update");
		   $("div#wfUserListContainer").mCustomScrollbar("update");
	   }
	   else 
		   $('#prtcpntContainer').find('div#prtcpntSection').append(emailDiv);
}

   var wFcontentData="complete";
   var inCompFlag = true;
   
   function updateAssignedTasksWF(type){
	   place = '';
	   taskType1 = type; 	
	   wfPlaceFlag == "Workflow";	
	   var taskid = $('#crumbs div.activeTab').attr('taskid');
	   var m = dateTimeValidationWF();
		if(m == false)
			return false;
		$('input#workFlowName').css('border','');
		$('input.workingMinutes,input.workingHours').css('border','1px solid #bfbfbf');
	   var title = $('#workFlowName').val().trim();
	   if(!title || title.match(/^\s*$/)) {
		   $('input#workFlowName').css('border','1px solid red');
		   return false;
	   }
	   
	   /*Below code is to check whether Task Name is exceeding total characters length. */
	   var LengthFlag = false;
	   if(!charExceedWfTaskFlag){
			   if(title.length >250){
					   LengthFlag = true;
			   }
	   }else{
			   if(title.length>250){
				   LengthFlag = false;
				   title = title.substring(0, 250);
			   }
			   $('#workFlowName').html(title);
	   }
	   if(LengthFlag){
		   confirmFun(getValues(companyAlerts,"Alert_Conv_Exceeding"),"delete","ExceedCharactersConfirmUpdateWfTask");
		   charExceedWfTaskType = type ;
		   return false;
	   }
	   /* Above code is to check whether Task Name is exceeding total characters length. */
	   
	   if($('#crumbs').find('.wf_level').length == 0){
		   parent.alertFun(getValues(companyAlerts,"Alert_NoneStep"),'warning');
		   return null;
	   }
	   if($('[id^=estmatedMinute_]').val()>59 ){
		   alertFun(getValues(companyAlerts,"Alert_MinValue"),'warning');
			$('[id^=estmatedMinute_]').css('border','1px solid red');
			return null;
	   }else{
			$('[id^=estmatedMinute_]').css('border','1px solid #BFBFBF');
		}
			
		if($.isNumeric($('[id^=estmatedMinute_]').val()) == false || $('[id^=estmatedMinute_]').val()<0){
		   parent.alertFun(getValues(companyAlerts,"Alert_InvalidNumber"),'warning');
		   $('[id^=estmatedMinute_]').css('border','1px solid red');
			return null;
	   }else{
			$('[id^=estmatedMinute_]').css('border','1px solid #BFBFBF');
		} 
	   if($.isNumeric($('[id^=estmatedHour_]').val()) == false || $('[id^=estmatedHour_]').val()<0){
		   parent.alertFun(getValues(companyAlerts,"Alert_InvalidNumber"),'warning');
		   $('[id^=estmatedHour_]').css('border','1px solid red');
			return null;
	   }else{
			$('[id^=estmatedHour_]').css('border','1px solid #BFBFBF');
		}   
		 
	   var projId = $('#wfProject option:selected').attr('id');    
	   var prioId = $('select#wfPriority option:selected').attr('value');         
	   var modelId = $('#hiddenTempId').val();    
	   var dStatus = validateRequiredFields();
	   var flowId = $("#hiddenWorkFlowId").val();
	   var wfstatus = true;
	   var wfComp = true;
	   $("div.wFactive").each(function (i) {
		   var ids = $(this).attr('id');
		   var tId = ids.split('_')[1];
		   var wFDescription = $('#wFDescription_'+tId).val();
		   var wFTitle = $('#taskDefinition_'+tId).parent().attr('title');
		   var wFStartDate = $('#wFStart_'+tId).val();
		   wFStartDate = wFStartDate.split('-');
		   wFStartDate = wFStartDate[2]+'-'+wFStartDate[0]+'-'+wFStartDate[1];
		   var wFEndDate = $('#wFEnd_'+tId).val();
		   wFEndDate = wFEndDate.split('-');
		   wFEndDate = wFEndDate[2]+'-'+wFEndDate[0]+'-'+wFEndDate[1];
		   var wFStartTime = convertTime($('#wFStartTime_'+tId).val());
		   var wFEndTime = convertTime($('#wFEndTime_'+tId).val());
		   //var flowId = $('#wfFlowID_'+tId).val();
		   if(wFStartTime != ''){
			   wFStartTime = wFStartTime;
		   }else{
			   wFStartTime = '00:00:00';
		   }
		   
		   if(wFEndTime != ''){
			   wFEndTime = wFEndTime;
		   }else{
			   wFEndTime = '23:59:59';
		   }
		   wfPlaceFlag = "Workflow";
		   wfIdFrReset = tId;
		   /*if(inCompFlag){
			   if($("div[id ^= wFParticipantEmail_"+tId+"]").find("div[id^=emailPrtcpnt_]").length < 1){
				   confirmFun("Do you want to save this task with incomplete status.","delete","updateAssignedTasksWorkFlow");
				   inCompFlag = false;
				   return false;	
			   }
			   if(inCompFlag == false){
				   wfComp = false;
				   return false;
			   }
		   }
		   if(wfComp == false){
			   return false;
		   }*/
		   if(estFlag == true){
			   if($(this).find("div[id ^= wFParticipantEmail_"+tId+"]").find("div[id^=emailPrtcpnt_]").length > 0){
				   var totalEstTime = parseInt($('#estmatedHour_'+tId).val())*60 + parseInt($('#estmatedMinute_'+tId).val()); 
				   var totalUserEstTime = 0;
				   var estStatus = true;
				   $(this).find("div[id ^= wFParticipantEmail_"+tId+"]").find("div[id^=emailPrtcpnt_]").each(function(){
					   var min = $(this).find('input#estMinTimeNewUI').val();
					   var hour = $(this).find('input#estHrTimeNewUI').val();
					   //var min1 = $(this).find('input#workingMinutes').val();
					   //var hour1 = $(this).find('input#workingHours').val();
					   if(isNaN(min)){
						   parent.alertFun(getValues(companyAlerts,"Alert_InvalidNumber"),'warning');
						   $(this).find('input#estMinTimeNewUI').css('border','1px solid red');
						   estStatus = false;
						   return false;
					   }else if(isNaN(hour)){
						   parent.alertFun(getValues(companyAlerts,"Alert_InvalidNumber"),'warning');
						   $(this).find('input#estHrTimeNewUI').css('border','1px solid red');
						   estStatus = false;
						   return false;
					   }else if(parseInt(min) > 59){
						   parent.alertFun(getValues(companyAlerts,"Alert_ErrEstTime"),'warning');
						   $(this).find('input#estMinTimeNewUI').css('border','1px solid red');
						   estStatus = false;
						   return false;
					   }
					   /*if(isNaN(min1)){
						   parent.alertFun("Invalid Number",'warning');
						   $(this).find('input#workingMinutes').css('border','1px solid red');
						   estStatus = false;
						   return false;
					   }else if(isNaN(hour1)){
						   parent.alertFun("Invalid Number",'warning');
						   $(this).find('input#workingHours').css('border','1px solid red');
						   estStatus = false;
						   return false;
					   }else if(parseInt(min1) > 59){
						   parent.alertFun("Error in users estimated time. Minutes value should be between 0 to 59",'warning');
						   $(this).find('input#workingMinutes').css('border','1px solid red');
						   estStatus = false;
						   return false;
					   }*/
					   $(this).find('input.workingMinutes,input.workingHours').css('border','1px solid #bfbfbf');
					   hour = hour * 60 ;
					   hour = hour + parseInt(min);
					   totalUserEstTime = parseInt(totalUserEstTime)+parseInt(hour);
				   });
				   if(estStatus == false){
					   wfstatus = false;
						 return false;
				   }else if(parseInt(totalEstTime) != parseInt(totalUserEstTime)){
					   taskUserTotalEst = totalUserEstTime;
					   wfstatus=false;
					   updateResetValFlag = true;
					   /*if(parseInt(totalEstTime) <  parseInt(totalUserEstTime) && parseInt(totalEstTime) != 0){
						   confirmReset(getValues(companyAlerts,"Alert_EstTimeNotMatch"),'delete','resetEstimatedTime','updateAssignedTasksWFForParam');
					   }else{
						   confirmCopyMoveCancel(getValues(companyAlerts,"Alert_EstTimeNotMatch"),"clear","resetEstimatedTime","updateAssignedTasksWFForParam","Yes","No","cancel");
					   }*/
					   var alertTaskEstTime = $('#estmatedHour_'+tId).val() +" h "+ $('#estmatedMinute_'+tId).val()+" m";
					   var alertUserEstTimeHr = totalUserEstTime / (60);
					   var alertUserEstTimeMin = totalUserEstTime % 60;
					   var alertUserEstTimeHrMin = alertUserEstTimeHr+" h "+alertUserEstTimeMin+" m";
					   if(parseInt(totalEstTime) >  parseInt(totalUserEstTime)){
						   var message = "Warning</br></br>Task Estimated Time: "+alertTaskEstTime+" </br>Participant Estimated Time: "+alertUserEstTimeHrMin+" </br> </br>Task Estimate exceeds sum of Participant Estimates.";
						   confirmFun(message,'delete','updateAssignedTasksWFForParam');
					   }else{
						   var message = "Warning</br></br>Task Estimated Time: "+alertTaskEstTime+" </br>Participant Estimated Time: "+alertUserEstTimeHrMin+" </br> </br>Particiapnt Estimated Time exceeds Task Estimate.</br></br>Want to update Task Estimated Time?";
						   confirmReset(message,'clear','resetEstimatedTime','updateAssignedTasksWFForParam','Yes','No');
					   }
					   
					   return false;
				   }
			   }
		   }
		   
		   if(!wfstatus){
			   return false;
		   }
		   
		   var reminder = $('#remDrpDown_'+tId).val();
		   var remType = $('#remTypeDrpDown_'+tId).val();
		   var taskName = $('#dd span').text();
		   var taskType = $('#selectTask').val();
		   if(taskName == 'Assigned Tasks') {
			   taskName = 'assignedWorkflow';
			   var backgroundColor = '#F5CB02';
			   var borderColor = '#FBAD3D';
		   }
		   if(taskName == getValues(Labels,'MY_TASKS')) {
			   taskName = 'myWorkflow';
			   var backgroundColor = '#2EB5E0';
			   var borderColor = '#49BFE6';
		   }
		   var wFStart = wFStartDate+" "+wFStartTime;
		   var wFEnd = wFEndDate+" "+wFEndTime;
		   var textColor = '#FFFFFF';
		   var taskDetails = $('#taskDateDetails_'+tId).val();
		   if(taskDetails == ''){
			   wFTaskDateXml = wFTaskDateXml+wFDateXml(tId);
		   }else{
			   wFTaskDateXml = wFTaskDateXml+taskDetails;
		   }
		   
	   });
	   
	   wFTaskDateXml += "</WFTaskDates>";
	   
	   if(!wfstatus){
		   return false;
	   }
	   if(dStatus == 'invalidDate'){ 
		   parent.alertFun(getValues(companyAlerts,"Alert_InvalidNumber"),'warning');
			  return false;
	   }else{
			  $('[id^=wFStart_]').css("border","");
		   $('[id^=wFEnd_]').css("border","");
	   }
	   
	   //********************************NEW FUNCTIONALITY******************************************************************************	     
	   
	   if(dStatus == 'invalid' && wfContentData!="incompleteData"){ 
		   confirmFun(getValues(companyAlerts,"Alert_IncompleteTask"),"delete","updateAssignedTasksWorkFlow");
		   return false;
	   }
	   if(dStatus=='valid' || dStatus=='invalid'){
		   var taskXmlData = constructTaskXml(); /*-------- Xml values includes task instructions, users, 
		   start date, end date, start time, end time, reminder, reminder type, estm hr, estm min ,level id ,level name --------*/
		   var taskId = $('#taskId').val();
		   var flowName = title;
		   if(taskXmlData == '[]'){
			   parent.alertFun(getValues(companyAlerts,"Alert_InvalidWorkFlow"),'warning');
			   return null;
		   }else{
			   var levelUpdXml = prepareLevelXml();
			   let jsonbody = {
					"workflow_id": flowId,
					"workflow_name": flowName,
					"priority": prioId,
					"removeEmailId": removeEmailId,
					"taskProject": projId,
					"user_id": userIdglb,
					"company_id": companyIdglb,
					"saveDocTaskIds":saveDocTaskIds,
					"taskDate": JSON.parse(taskXmlData)
				}
				checksession();
			   $('#loadingBar').show();
			   timerControl("start");
			   $.ajax({
				   url: apiPath+"/"+myk+"/v1/updateWFTask", 
				   type: "PUT",
				   contentType:"application/json",
				   data: JSON.stringify(jsonbody),
				  /*  data: {act:'updateWFTask',taskType:taskType1, taskid:taskid,flowId:flowId, flowName:flowName, 
						   taskPriority:prioId, taskXml: taskXmlData,
						   status:"", removeEmailId:removeEmailId, wFTaskDateXml: wFTaskDateXml,taskProject:projId,
						   levelUpdXml:levelUpdXml,saveDocTaskIds:saveDocTaskIds},  */
				   error: function(jqXHR, textStatus, errorThrown) {
						   checkError(jqXHR,textStatus,errorThrown);
						   $("#loadingBar").hide();
						   timerControl("");
						   },  
				   success: function (result) {
					   
					   inCompFlag = true;
					   selectedTaskemailId='';
					   removeEmailId='';
					   charExceedWfTaskType = "";
					   charExceedWfTaskFlag = false;
					   parent.alertFun(getValues(companyAlerts,"Alert_TaskUp"),'warning');
					   updateResetValFlag = false;
					   estFlag = true;
					  /*  var myTaskDatas = result.split('#@#@')[0];
					   var assignedTaskDatas = result.split('#@#@')[1];
					   var historyDatas = result.split('#@#@')[2]; */
					   updateDates="no";
					   if(view!="listV"){
						   if(update == 'yes') {
							   taskCalendar($('#dd span').text());
						   }
						   }    
				   if(update == 'yes' && curr_event != null) {
					   calendar.fullCalendar('updateEvent', curr_event);
					   calendar.fullCalendar('refetchEvents');
				   }
				   /*calendarViewDatas(myTaskDatas,assignedTaskDatas,historyDatas);
				   if(sortTaskType == 'assignedTasks') {
				   } */
				   if($('#taskMainTab').length == 0){
					   loadTaskTabData('' , 'firstClick');
				   }else{
					   loadTaskListViewData('assignedTask', 'task');
				   }
			   
				   $('#commonHtmlTaskUI').remove();
					 $('#newTaskUI').hide();
					 $("#newTaskUI").css("height","");
					 $('.newrowTab').hide();
					 $('.oldrowTab').show();
					 var topH = $("#tabMainDiv").innerHeight();
					 $(".contentCls").css("top",topH);
				   parent.$("#loadingBar").hide();
				   parent.timerControl("");
					  }
				  });
				  wfContentData="complete";
		   }   
	   }else if(dStatus == 'invalidTime'){
		   parent.alertFun(getValues(companyAlerts,"Alert_ValidEndTime"),'warning');
	   }else{
		   parent.alertFun(getValues(companyAlerts,"Alert_Participant"),'warning');
	   }
   }
   
   
   
   
   function updateAssignedTasksWFOld(type) {		/*Update assigned Tasks */
	   place = '';
	   taskType1 = type; 	
	   wfPlaceFlag == "Workflow";	
	   var flowName = $('#workflowNameInDetails').val().trim();
	   if((!flowName || flowName.match(/^\s*$/))){
		   parent.alertFun(getValues(companyAlerts,"Alert_FlowName"),'warning');
		   return null;
	   }
	   var taskName = $('#wfEventName').val().trim();
	   $('#wfEventName, #wFStart, #wFEnd').css('border','1px solid #c0c0c0');
	   if((!taskName || taskName.match(/^\s*$/))){
		   $('#wfEventName').css("border","1px solid red");
		   return null;
	   }	
	   var startDate = $('#wFStart').val();
	   var endDate = $('#wFEnd').val();
	   var startTime = $('#wFStartTime').val();
	   var endTime = $('#wFEndTime').val();
	   /*		if(!startDate) {
			   $('#wFStart').css("border","1px solid red");
			   return null;
		   }
		   if(!endDate) {
			   $('#wFEnd').css("border","1px solid red");
			   return null;
		   }				*/	
	   var sdArray=startDate.split('-');
	   var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
	   var edArray=endDate.split('-');
	   var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
	   var d1=new Date(sd);
	   var d2=new Date(ed);
	   if(d1 > d2){
		   parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
		   $('#wFStart').css("border","1px solid red");
		   $('#wFEnd').css("border","1px solid red");
		   return false;
	   }
	   if(startDate == endDate) {
		   var st = parseInt(startTime.replace(':', ''), 10); 
			var et = parseInt(endTime.replace(':', ''), 10);
		   if(st >= et){
				  parent.alertFun(getValues(customalertData,"Alert_ValidEndTime"),'warning');
				  $('#wFEndTime').css("border","1px solid red");
				  return null;
		   }
	   }	
	   if($.isNumeric($('#wfEstHour').val()) == false || $('#wfEstHour').val()<0){
			  parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
			$('#wfEstHour').css('border','1px solid red');
			return null;
	   } else{
			  $('#wfEstHour').css('border','1px solid #BFBFBF');
		  }
				
		if($.isNumeric($('#wfEstMinute').val()) == false){
			  parent.alertFun(getValues(customalertData,"Alert_InvalidNumber"),'warning');
			$('#wfEstMinute').css('border','1px solid red');
			return null;
	   } else{
		   $('#wfEstMinute').css('border','1px solid #BFBFBF');
	   }
		  if($('#wfEstMinute').val()>59 || $('#wfEstMinute').val()<0){
			  parent.alertFun(getValues(companyAlerts,"Alert_MinValue"),'warning');
			$('#wfEstMinute').css('border','1px solid red');
			return null;
		  } else{
			  $('#wfEstMinute').css('border','1px solid #BFBFBF');
		  }
				
		 var oldUser=$('[id^=emailPrtcpnt_]').attr('value');
	   var workflow_id=$(".activeTab").attr("flowid");
	   if((!startDate) || (!endDate)  || ($('#participantEmail').find('#prtcpntSection').children('div:visible').length < 1) ){
		   if(wFcontentData!="incompleteData"){
			   parent.confirmFun(getValues(companyAlerts,"Alert_IncompleteTask"),"delete","updateAssignedTasksWorkFlow");
			   return false;	
		   }
	   }else{
			   if(estFlag == true){
				   var totalEstTime = parseInt($('#wfEstHour').val())*60 + parseInt($('#wfEstMinute').val()); 
				   var totalUserEstTime = 0;
				   var estStatus = true;
				   wfPlaceFlag = "WorkflowEdit";
				   estFlag = true;
				   $('#prtcpntSection').find("div[id^=emailPrtcpnt_]").each(function(){
					   var min = $(this).find('input.workingMinutes').val();
					   var hour = $(this).find('input.workingHours').val();
					   if(isNaN(min)){
						   parent.alertFun(getValues(companyAlerts,"Alert_InvalidNumber"),'warning');
						   $(this).find('input.workingMinutes').css('border','1px solid red');
						   estStatus = false;
						   return false;
					   }else if(isNaN(hour)){
						   parent.alertFun(getValues(companyAlerts,"Alert_InvalidNumber"),'warning');
						   $(this).find('input.workingHours').css('border','1px solid red');
						   estStatus = false;
						   return false;
					   }else if(parseInt(min) > 59){
						   parent.alertFun(getValues(companyAlerts,"Alert_ErrEstTime"),'warning');
						   $(this).find('input.workingMinutes').css('border','1px solid red');
						   estStatus = false;
						   return false;
					   }
					   $(this).find('input.workingMinutes,input.workingHours').css('border','1px solid #bfbfbf');
					   hour = hour * 60 ;
					   hour = hour + parseInt(min);
					   totalUserEstTime = parseInt(totalUserEstTime)+parseInt(hour);
				   });
				   if(estStatus == false)
					 return false;
					
				   else if(parseInt(totalEstTime) != parseInt(totalUserEstTime)){
					   taskUserTotalEst = totalUserEstTime;
					   /* if(parseInt(totalEstTime) <  parseInt(totalUserEstTime) && parseInt(totalEstTime) != 0){
						   confirmReset(getValues(companyAlerts,"Alert_EstTimeNotMatch"),'delete','resetEstimatedTime','updateAssignedTasksWFForParam');
					   }else{
						   confirmCopyMoveCancel(getValues(companyAlerts,"Alert_EstTimeNotMatch"),"clear","resetEstimatedTime","updateAssignedTasksWFForParam","Yes","No","cancel");
					   }*/
					   var alertTaskEstTime = $('#wfEstHour').val() +" h "+ $('#wfEstMinute').val()+" m";
					   var alertUserEstTimeHr = totalUserEstTimeNewUI / (60);
					   var alertUserEstTimeMin = totalUserEstTimeNewUI % 60;
					   var alertUserEstTimeHrMin = alertUserEstTimeHr+" h "+alertUserEstTimeMin+" m";
					   if(parseInt(totalEstTime) >  parseInt(totalUserEstTime)){
						   var message = "Warning</br></br>Task Estimated Time: "+alertTaskEstTime+" </br>Participant Estimated Time: "+alertUserEstTimeHrMin+" </br> </br>Task Estimate exceeds sum of Participant Estimates.";
						   confirmFun(message,'delete','updateAssignedTasksWFForParam');
					   }else{
						   var message = "Warning</br></br>Task Estimated Time: "+alertTaskEstTime+" </br>Participant Estimated Time: "+alertUserEstTimeHrMin+" </br> </br>Particiapnt Estimated Time exceeds Task Estimate.</br></br>Want to update Task Estimated Time?";
						   confirmReset(message,'clear','resetEstimatedTime','updateAssignedTasksWFForParam','Yes','No');
					   }
					   return false;
				   }
			   }
		   }
	   var description = $('#taskInstructionsTxt').val();
	   var taskRemainder = $('#remDrpDown option:selected').text();
	   var taskRemainderBy = $('#remTypeDrpDown option:selected').text();
	   var prevStartDate=$('#prevStartDate1').val(); 
	   var prevEndDate=$('#prevEndDate1').val(); 
	   var taskProjId = $('#wfProjID').val();
		  var taskId = $('#wfTaskID').val();
		  var flowId = $('#wfFlowID').val();
		  var totalHour=$('#wfEstHour').val();
	   var totalMinute=$('#wfEstMinute').val();
	   var budgetedHour=$('#wfEstimatedHour').val();
	   var budgetedMinute=$('#wfEstimatedMinute').val();	
	   var taskDateXml=$('#wfTaskDateXml').val();
	   //var comment = $('#myTaskComment').val().trim();
	   var comment = '';// Needs to be changed to the above line once comments are being inserted
	   /*var userPercentage = parseInt( $('#userTaskAmount').html());
	   var actHour = $('#workingHours').val();
		var actMin = $('#workingMinutes').val();*/
		   
		var hrStatus = wfTestEstimatedHour();
		var userXml = prepareUserXml();
		if(hrStatus){
		   var levelUpdXml = prepareLevelXml();
		   parent.$("#loadingBar").show();
		   parent.timerControl("start");
		   $.ajax({
				  url: path+'/calenderAction.do',
			   type:"POST",
			   data:{  act:"updateWFTask", 
					   taskType:type, taskId:taskId, flowName:flowName, flowId:flowId, taskName:taskName, 
						 startDate:startDate, endDate:endDate, startTime:startTime, endTime:endTime,
						 description:description, selectedTaskEmailId:selectedTaskemailId, taskProject:taskProjId,
						 removeEmailId:removeEmailId, taskRemainder:taskRemainder, taskRemainderBy:taskRemainderBy,
						 taskDateXml:taskDateXml, prevStartDate:prevStartDate, prevEndDate:prevEndDate,
						 totalHour:totalHour, totalMinute:totalMinute, budgetedMinute:budgetedMinute,
						 budgetedHour:budgetedHour, levelUpdXml:levelUpdXml, oldUser:oldUser,
						 comment:comment,  userXml:userXml //,userPercentage:userPercentage, actHour:actHour, actMin:actMin,
					 },
		   error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				   $("#loadingBar").hide();
				   timerControl("");
				   }, 
		   success:function(result){
			   parent.checkSessionTimeOut(result);
			   selectedTaskemailId='';
			   removeEmailId='';
			   parent.alertFun(getValues(companyAlerts,"Alert_TaskUp"),'warning');
			   
			   //updateResetValFlag = false;
			   estFlag = true;
			   var myTaskDatas = result.split('#@#@')[0];
			   var assignedTaskDatas = result.split('#@#@')[1];
			   var historyDatas = result.split('#@#@')[2];
			   updateDates="no";
			   if(view!="listV"){
				   if(update == 'yes') {
					   taskCalendar($('#dd span').text());
				   }
			   }    
			   if(update == 'yes' && curr_event != null) {
				   calendar.fullCalendar('updateEvent', curr_event);
				   calendar.fullCalendar('refetchEvents');
			   }
			   /*calendarViewDatas(myTaskDatas,assignedTaskDatas,historyDatas);
			   if(sortTaskType == 'assignedTasks') {
				   
			   }*/
			   if($('#taskMainTab').length == 0){
				   loadTaskTabData('' , 'firstClick');
			   }else{
				   loadTaskListViewData('assignedTask', 'task');
			   }
			   
			   
			   parent.$("#loadingBar").hide();
			   parent.timerControl("");
		   }
	   });
	   wFcontentData="complete";
	   
   }
}

function updateAssignedTasksWorkFlow(){
   wfContentData="incompleteData";
   updateAssignedTasksWF('assignedWorkFlow');
}

   function prepareLevelXml(){
		  var levelXml = "<levelData>";
		  $("#crumbs li a").each(function (i) {
			 levelXml +="<tLevel>";
			 levelXml +=" <tLevelId>"+$(this).attr('levelid')+"</tLevelId>";
			 levelXml +=" <tLevelClass>"+$(this).attr('class')+"</tLevelClass>";
			 levelXml +=" <tLevelName>"+$(this).children('span').text()+"</tLevelName>";
			 levelXml +="</tLevel>";
		  });
		  levelXml += "</levelData>";
		  return levelXml;
   }


function wfTestEstimatedHour(){
  var flag=true;
  var b_hr=$('#wfEstHour').val() !='' ? parseInt($('#wfEstHour').val()) : 0;
  var e_hr=$('#wfEstimatedHour').val() ? parseInt($('#wfEstimatedHour').val()) : 0;
  
  var b_min=$('#wfEstMinute').val() ? parseInt($('#wfEstMinute').val()) : 0;
  var e_min=$('#wfEstimatedMinute').val() ? parseInt($('#wfEstimatedMinute').val()) : 0;
  
  if( e_hr!=0 && e_min!=0 && b_hr!=0 && b_min!=0){
	 if( b_hr!=e_hr && b_min!=e_min ){ 
			   confirmIdeaId=e_hr;
			   confirmIdeaIdmin=e_min;
			   parent.confirmReset(getValues(companyAlerts,"Alert_BudgetedMinHrMin"),'reset','confirmTaskMinuteHourDivClose','showIdeaMorePopup');
			   saveTaskFlag=true;
			flag=false;
	   } 
  }
  else if(e_hr!=0 || e_min!=0){
	  if( b_hr!=e_hr || b_min!=e_min ){
		if(b_hr!=e_hr){
			confirmIdeaId=e_hr;
			parent.confirmReset(getValues(customalertData,"Alert_Unmatch_BudgetedHr"),'reset','confirmTaskHourDivClose','showIdeaMorePopup');
			saveTaskFlag=true;
			flag=false;
		  }
		  if(b_min!=e_min){
			   confirmIdeaIdmin=e_min;	
			parent.confirmReset(getValues(customalertData,"Alert_Unmatch_BudgetedMin"),'reset','confirmTaskMinDivClose','showIdeaMorePopup');
			saveTaskFlag=true;
			flag=false;
		  }
	 }
   
  }else{
	saveTaskFlag=false;
  }

  return flag;
} 

   function tabActive(obj){
	   
	   $('#crumbs div.wf_level').removeClass('activeTab').removeClass('stepSelected').css('border','1px solid #919392');
		 $('#wfLevelContainer div.wf_level').removeClass('activeTab').removeClass('stepSelected').css('border','1px solid #919392');
		 $(obj).css('border','1px solid blue').addClass('stepSelected').addClass('activeTab');
		 //$('#crumbs div.wf_level').css('border','1px solid #919392').removeClass('stepSelected');
		 //$(obj).css('border','1px solid blue').addClass('stepSelected');
	 }
   
function closeWorkflowViewNew() {
	parent.$("div#transparentDiv").hide();
	$('div#listtransperant').hide();
   $("div#viewWorkFlowTempLevelContainer").hide();
   $("div#createWorkflowTempDivNew").show();
   tempID='';levelCkdId = '';
   $("body").css("overflow", "auto");
   $("div.workflowTemplateList").css("background-color", "");
   $('#levelContentDiv').html('');
}

function searchWfPage(obj){                       /*search for workflow task participants */
   $('div.participants').hide();
   var name = $(obj).val();
   if(!name) {
	   $('div.participants').show();
	   $('.searchUserImg').attr('src','images/calender/search.png');
	   var isiPad = navigator.userAgent.match(/iPad/i) != null;
	   if(!isiPad){
		   $('#wfUserListContainer').mCustomScrollbar("update");
	   }	
	   hideWfPrtcpnts();
	   return null;
   }
   else {					
	   $('.searchUserImg').attr('src','images/remove.png').css({'width':'17px','height':'16px'}).attr('onclick','clearSearchWfUser(this)');
	   $('div.participants').find('span.hidden_searchName:contains("'+name+'")').parents('div.participants').show();
	   var isiPad = navigator.userAgent.match(/iPad/i) != null;
	   if(!isiPad){
		   $('#wfUserListContainer').mCustomScrollbar("update");
	   }	
	   hideWfPrtcpnts();
	   return null;
   }
}

function clearSearchWfUser(obj){   /* Clear Search Field  */
   //var id = $(obj).prev().attr('id');		
   $(obj).prev().val("");			
   $('.searchUserImg').attr('src','images/calender/search.png');
   $('div.participants').show();
   var isiPad = navigator.userAgent.match(/iPad/i) != null;
   if(!isiPad){
	   $("#wfUserListContainer").mCustomScrollbar("update");	
   }
   hideWfPrtcpnts();		
   return null;
}

function hideWfPrtcpnts(){				
   $('.odd, .even, .participantDetails, .updatePrtcpnt, .participantDetails').each(function() {
	   var id = $(this).attr('id');
	   var userId = id.split('_')[1];
	   $('#participant_'+userId).css('display','none');
   });
}

function getProjectUsersWf(obj){   /* Get users of particular project  */	
	var popH=$("#wfUserListContainer").height();
	var mH=popH/2;
	$('#wfUserListContainer').html('');
	$('#wfUserListContainer').append('<div id=\"loadingB\" align=\"center\" style=\"margin-top:'+mH+'px;overflow:hidden;\">loading...</div>');
	var text = $(obj).val();				
	if(text == getValues(customLabels,'Select') || text == "" || text == "0") {
				projId=0;
				  getAllUsersWf();
			$("select#userProject").find("option[id='0']").attr("selected", "selected");
			scrollBar('wfUserListContainer');
		if(update == 'yes') {
			  $('.odd, .even, .updatePrtcpnt, .userContainer').each(function() {
			   var id = $(this).attr('id');
			   var userId = id.split('_')[1];
			   $('#participant_'+userId).css('display','none');
		   });
		}
		else{
		   $('div.participantDetails').each(function() {
			   var id = $(this).attr('id');
			   var userId = id.split('_')[1];
			   $('#participant_'+userId).css('display','none');
			});
		}	
	}
	else if(text == getValues(customLabels,'All_Users')) {
			  projId = 0;
			  var optVal = $(obj).val();
			  $("select#project,select#wfProject").val(optVal);
			  $('#project option:selected').attr('selected', optVal);
			 getAllUsersWf();
	}
	else {	
				$('#wfUserListContainer').html('');
		   //	$('#wfUserListContainer').append('<div id="loadingB" align="center" style="margin-top:230px;overflow:hidden">loading...</div>');
			   var optVal = $(obj).val();
			   var optId = $(obj).attr('id');
			   $("select#project,select#wfProject").val(optVal);
			   $("select#userProject").val(optVal);
			   projId=$("#"+optId+" option:selected").attr('id');
			   $.ajax({ 
				  url: path+'/calenderAction.do',
				  type:"POST",
				  data:{act:'fetchParticipants',projId:projId,userId:UserId},
				  error: function(jqXHR, textStatus, errorThrown) {
						   checkError(jqXHR,textStatus,errorThrown);
						   $("#loadingBar").hide();
						   timerControl("");
						   }, 
				  success:function(result){
					  parent.checkSessionTimeOut(result);				
					  $('#wfUserListContainer,#ideaUsersDiv').html(result);
					  parent.$('#loadingBar').hide();
					  parent.timerControl("");
					  if(update == 'yes') {
						  $('.odd, .even, .updatePrtcpnt, .participantDetails, .userContainer').each(function() {
						   var id = $(this).attr('id');
						   var userId = id.split('_')[1];
						   $('#participant_'+userId).css('display','none');
					   });
					   if($('[id^=participant_]:visible').length < 1){$('#wfUserListContainer').append('<div id="innerDiv15" align="center"> No Participant</div>');}
					  }
					  else{
						   $('div.participantDetails').each(function() {
							   var id = $(this).attr('id');
							   var userId = id.split('_')[1];
							   $('#participant_'+userId).css('display','none');
							  }); $('#innerDiv1').hide();
							  if($('[id^=participant_]:visible').length < 1){
							   $('#wfUserListContainer').append('<div id="innerDiv15" align="center"> No Participant</div>');
						   } 
					   }
						  var isiPad = navigator.userAgent.match(/iPad/i) != null;
						 if(!isiPad) {	
							 scrollBar('wfUserListContainer');
						   scrollBar('ideaUsersDiv');
						  }
				  }
			});
   }
}


   function getAllUsersWf() {		/*All User Details*/

	   var popH=$("#wfUserListContainer").height();
	   var mH=popH/2;
				   $('#wfUserListContainer').html('');
				   $('#wfUserListContainer').append('<div id=\"loadingB\" align=\"center\" style=\"margin-top:'+mH+'px;overflow:hidden;\">loading...</div>');
	   $.ajax({ 
		 url: path+'/calenderAction.do',
		   type:"POST",
			data:{act:'fetchAllUsers'},
			error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				   $("#loadingBar").hide();
				   timerControl("");
				   }, 
		 success:function(result){
			 parent.checkSessionTimeOut(result);
			 $('#wfUserListContainer').attr('align','left');
			  $('#wfUserListContainer,#userContainer').css('height',popH);
			$('#wfUserListContainer').css('margin-top','0px');
			 $('#wfUserListContainer').html(result);
			  parent.$('#loadingBar').hide();
			  parent.timerControl("");
			  var isiPad = navigator.userAgent.match(/iPad/i) != null;
		   if(!isiPad){
				 scrollBar('wfUserListContainer');     
				 scrollBar('ideaUsersDiv');
		   }  	
			  if(update == 'yes') {
				 $('.odd, .even, .updatePrtcpnt').each(function() {
				   var id = $(this).attr('id');
				   var userId = id.split('_')[1];
				   $('#participant_'+userId).css('display','none');
			   });
		   }
		   else{
			   $('div.participantDetails').each(function() {
				   var id = $(this).attr('id');
				   var userId = id.split('_')[1];
				   $('#participant_'+userId).css('display','none');
				  });
			  }
			  if($('[id^=participant_]:visible').length < 1){
				  $('#wfUserListContainer').append('<div id="innerDiv18" align="center"> No Participant</div>');
			  }
			  
		  }
	});
	$("select#userProject option[id='0']").attr("selected", "selected");
}

function showWFMorePopup(){					
   var taskId=$("#wfTaskID").val(); 
	var startDate=$('#wFStart').val();
   var endDate=$('#wFEnd').val();
  
   if(startDate == "" || startDate == "Date" || startDate == null){
		   $('#wFStart').css("border","1px solid red");
		   parent.alertFun(getValues(customalertData,"Alert_taskStartDate"),'warning');
		   return false;		
	   }else{
		   $('#wFStart').css("border","1px solid #BFBFBF");
	   }
	   if(endDate == "" || endDate == "Date" || endDate == null){
		   $('#wFEnd').css("border","1px solid red");
		   parent.alertFun(getValues(customalertData,"Alert_taskEndDate"),'warning');
		   return false;		
	   }else{
		   $('#wFEnd').css("border","1px solid #BFBFBF");
	   }
	   var sdArray=startDate.split('-');
	   var sd=sdArray[2]+'/'+sdArray[0]+'/'+sdArray[1];
	   var edArray=endDate.split('-');
	   var ed=edArray[2]+'/'+edArray[0]+'/'+edArray[1];
	   var d1=new Date(sd);
	   var d2=new Date(ed);
	   if(d1 > d2){
			 parent.alertFun(getValues(customalertData,"Alert_InvalidDate"),'warning');
			 $('#wFStart').css("border","1px solid red");
			 $('#wFEnd').css("border","1px solid red");
			 return false;
		}else{
			 $('#wFStart').css("border","1px solid #BFBFBF");
			 $('#wFEnd').css("border","1px solid #BFBFBF");
		}
		
	  var totalHr=$('#wfEstHour').val(); 
	  var totalMin=$('#wfEstMinute').val(); 
	  if(! /^[0-9]+$/.test(totalHr)){
		   parent.alertFun(getValues(customalertData,"Alert_numericValues"),'warning');
		   $('#wfEstHour').css("border","1px solid red");
		   return false;
	  }else{
		  $('#wfEstHour').css("border","1px solid #BFBFBF");
	  }
	  
	  if(! /^[0-9]+$/.test(totalMin)){
		   parent.alertFun(getValues(customalertData,"Alert_numericValues"),'warning');
		   $('#wfEstMinute').css("border","1px solid red");
		   return false;
	  }else{
		  $('#wfEstMinute').css("border","1px solid #BFBFBF");
	  }
	  if(totalMin>59 || totalMin < 0){
		   parent.alertFun(getValues(companyAlerts,"Alert_MinValue"),'warning');
			   $('#wfEstMinute').css("border","1px solid red");
			   return false;
	  }else{
			  $('#wfEstMinute').css("border","1px solid #BFBFBF");
	  }	  
		
	 var xmlData=$("#wfTaskDateXml").val().trim();
	 var prevStartDate=$("#prevStartDate").val().trim(); 
	 var prevEndDate=$("#prevEndDate").val().trim(); 
	 
	 if(prevStartDate!='' || prevEndDate!='' ){
			if(prevStartDate==startDate && prevEndDate==endDate ){
				$('#wfTaskEhourPopupDiv').show();
				 if($('#wfTaskEhourPopupDiv').find('#wfTaskDateHourDiv').attr('class')){
				  }else{
					 if(!isiPad){
						scrollBar('wfTaskDateHourDiv');
						$('div#wfTaskDateHourDiv .mCSB_container').css('margin-right','15px');
						$('div#wfTaskDateHourDiv .mCSB_scrollTools').css('margin-right','0px');
					 }  
				 }
					var estimatedHour=$('#wfEstimatedHour').val();
					var estimatedMinute=$('#wfEstimatedMinute').val();  
					if(estimatedHour=='0'){
						var datesLength;
						if(!isiPad){
						  datesLength= $('#wfTaskDateHourDiv .mCSB_container').children('div.estimatedHourDiv').length;
						}else{
						  datesLength= $('#wfTaskDateHourDiv').children('div.estimatedHourDiv').length;
						}
						var hourPerDay=totalHr/datesLength;
						hourPerDay=Math.round(hourPerDay);
						if(hourPerDay==0){
						  hourPerDay=totalHr;
						}
						var i=0;
						var j=datesLength-1;
						var countHour=0;
						var k=1;
						 $('#wfTaskDateHourDiv').children('.estimatedHourDiv').each(function() {
								if(i==j){
									   var lastday=0;
									   countHour=0;
									   $('#wfTaskDateHourDiv').children('.estimatedHourDiv').each(function() {
										 var hour = $(this).children('div').children('input.ehour').val().trim();
										 if(hour==''){
											hour='0';
										  }
										  if(k<=i){
											 countHour=countHour+parseInt(hour);
										  }
										 k++;
									   });
									  lastday=parseInt(totalHr)-countHour;
									  $(this).children('div').children('input.ehour').val(lastday);
								}else{
									  $(this).children('div').children('input.ehour').val(hourPerDay);
									  countHour=countHour+hourPerDay;
									  if(countHour>parseInt(totalHr)){
											 hourPerDay=parseInt(totalHr)-[countHour-hourPerDay];
											 if(hourPerDay<0){
											   hourPerDay=0;
											 }
									  }else if(countHour==parseInt(totalHr)){
											hourPerDay=0;
									  }else{
											var d=parseInt(totalHr)-countHour;
											if(d<hourPerDay){
											   hourPerDay=d;
											   countHour=parseInt(totalHr);
											 }
									  }
							   }
							i++;
						 });
				   }
				   
				   if(estimatedMinute=='0'){
						var datesLength;
						if(!isiPad){
						  datesLength= $('#wfTaskDateHourDiv .mCSB_container').children('div.estimatedHourDiv').length;
						}else{
						  datesLength= $('#wfTaskDateHourDiv').children('div.estimatedHourDiv').length;
						}
						var minutePerDay=totalMin/datesLength;
						minutePerDay=Math.round(minutePerDay);
						if(minutePerDay==0){
						  minutePerDay=totalMin;
						}
						var i=0;
						var j=datesLength-1;
						var countMinute=0;
						var k=1;
						 $('#wfTaskDateHourDiv').children('.estimatedHourDiv').each(function() {
								if(i==j){
									   var lastdaymin=0;
									   countMinute=0;
									   $('#wfTaskDateHourDiv').children('.estimatedHourDiv').each(function() {
										 var minute = $(this).children('div').children('input.emin').val().trim();
										 if(minute==''){
											minute='0';
										  }
										  if(k<=i){
											 countMinute=countMinute+parseInt(minute);
										  }
										  k++;
									   });
									  lastdaymin=parseInt(totalMin)-countMinute;
									  $(this).children('div').children('input.emin').val(lastdaymin);
								}else{
									  $(this).children('div').children('input.emin').val(minutePerDay);
									  countMinute=countMinute+minutePerDay;
									  if(countMinute>parseInt(totalMin)){
											 minutePerDay=parseInt(totalMin)-[countMinute-minutePerDay];
											 if(minutePerDay<0){
											   minutePerDay=0;
											 }
									  }else if(countMinute==parseInt(totalMin)){
											minutePerDay=0;
									  }else{
											var d=parseInt(totalMin)-countMinute;
											if(d<minutePerDay){
											   minutePerDay=d;
											   countMinute=parseInt(totalMin);
											 }
									  }
							   }
							i++;
						 });
				   }
				   
				   
				   
			}else{					
				parent.confirmFun(getValues(customalertData,"Alert_Reset_Data"),"clear","wfDataRegenerateConfirm");
			}
	 }else{	
		   loadWFTaskDate();
	 }
}
function wfDataRegenerateConfirm(){
	loadWFTaskDate();
}
function wfDateIncludeClicked(obj){
	   var cId = $(obj).attr('id');
	   var flag = document.getElementById(cId).checked;
	   
	   if(!flag){
		   $(obj).parents('div.wfEstHourDiv').hide().removeClass('dTactive').addClass('dTinactive');
		   $(obj).parents('div.wfEstHourDiv').children('input.edCheckbox').val("Unchecked");
		   if(!isiPad){
			 $("div#wfTaskDateHourDiv").mCustomScrollbar("update");
		   }
	   }else{
		   $(obj).parents('div.wfEstHourDiv').show().removeClass('dTinactive').addClass('dTactive');
		   $(obj).parents('div.wfEstHourDiv').children('input.edCheckbox').val("Checked");
		   if(!isiPad){
			 $("div#wfTaskDateHourDiv").mCustomScrollbar("update");
		   }
	   }
   }

function dateWFEDclick(obj){
   var objId = $(obj).attr('id');
   if(objId == 'dht'){
	   $('#wfTaskDateHourDiv div.dTinactive').show();
   }if(objId == 'hht'){
	   $('#wfTaskDateHourDiv div.dTinactive').hide();
   }
   if(!isiPad){
	   $("div#wfTaskDateHourDiv").mCustomScrollbar("update");
   }
}
function hideWFTaskHourDiv(){
	$('#wfTaskEhourPopupDiv').hide();
}
function loadWFTaskDate(){
		  parent.$("#loadingBar").show();
		  parent.timerControl("start");
		  var totalHr=$('#wfEstHour').val(); 
		  var taskId=$("#wfTaskID").val();
		   var startDate=$('#wFStart').val();
		  var endDate=$('#wFEnd').val();
		  var totalHr=$('#wfEstHour').val(); 
		  var totalMin=$('#wfEstMinute').val(); 
		  if(! /^[0-9]+$/.test(totalHr)){
				   parent.alertFun(getValues(customalertData,"Alert_numericValues"),'warning');
				   $('#wfEstHour').css("border","1px solid red");
				   return false;
		  }else{
				   $('#wfEstHour').css("border","1px solid #BFBFBF");
		  }
		  if(! /^[0-9]+$/.test(totalMin)){
					parent.alertFun(getValues(customalertData,"Alert_numericValues"),'warning');
				   $('#wfEstMinute').css("border","1px solid red");
				   return false;
		  }else{
				   $('#wfEstMinute').css("border","1px solid #BFBFBF");
		  }
		  var datesLength=""; 
			  $.ajax({
				  url: path+'/calenderAction.do',
			   type:"POST",
			   data:{act:"getWFTaskMoreData",startDate:startDate,endDate:endDate,taskId:taskId },
			   error: function(jqXHR, textStatus, errorThrown) {
					   checkError(jqXHR,textStatus,errorThrown);
					   $("#loadingBar").hide();
					   timerControl("");
					   }, 
			   success:function(result){
				   parent.checkSessionTimeOut(result);
				   
				   $('#wfTaskDateHourDiv').html(result);
				   $('#wfTaskEhourPopupDiv').show();
				   if(!isiPad){
					 scrollBar('wfTaskDateHourDiv');
					 $('div#wfTaskDateHourDiv .mCSB_container').css('margin-right','15px');
					 $('div#wfTaskDateHourDiv .mCSB_scrollTools').css('margin-right','0px');
					 datesLength= $('#wfTaskDateHourDiv .mCSB_container').children('div.wfEstHourDiv').length;
				   }else{
					 datesLength= $('#wfTaskDateHourDiv').children('div.wfEstHourDiv').length;
				   }  
					if(totalHr>0){
					var hourPerDay=totalHr/datesLength;
					hourPerDay=Math.round(hourPerDay);
					if(hourPerDay==0){
					  hourPerDay=totalHr;
					}
					var i=0;
					var j=datesLength-1;
					var countHour=0;
					 $('.wfEstHourDiv').each(function() {
						if(i==j){
								var lastday=0;
								   countHour=0;
								   $('.wfEstHourDiv').each(function() {
									 var hour = $(this).children('div').children('input.ehour').val().trim();
									 if(hour==''){
										hour='0';
									  }
									 countHour=countHour+parseInt(hour);
								   });
								  lastday=parseInt(totalHr)-countHour;
								  $(this).children('div').children('input.ehour').val(lastday);
								  
						}else{
							   $(this).children('div').children('input.ehour').val(hourPerDay);
								  countHour=countHour+hourPerDay;
								  if(countHour>parseInt(totalHr)){
										 hourPerDay=parseInt(totalHr)-[countHour-hourPerDay];
										 if(hourPerDay<0){
										   hourPerDay=0;
										 }
								  }else if(countHour==parseInt(totalHr)){
										hourPerDay=0;
								  }else{
										var d=parseInt(totalHr)-countHour;
										if(d<hourPerDay){
										   hourPerDay=d;
										   countHour=parseInt(totalHr);
										 }
								  }
						}
						i++;
					 });
				   }
				   if(totalMin>0){
					var minutePerDay=totalMin/datesLength;
					minutePerDay=Math.round(minutePerDay);
					if(minutePerDay==0){
					  minutePerDay=totalMin;
					}
					var i=0;
					var j=datesLength-1;
					var countMinute=0;
					 $('.wfEstHourDiv').each(function() {
						if(i==j){
								var lastdaymin=0;
								   countMinute=0;
								   $('.wfEstHourDiv').each(function() {
									 var minute = $(this).children('div').children('input.emin').val().trim();
									 if(minute==''){
										minute='0';
									  }
									 countMinute=countMinute+parseInt(minute);
								   });
								  lastdaymin=parseInt(totalMin)-countMinute;
								  $(this).children('div').children('input.emin').val(lastdaymin);
						}else{
							   $(this).children('div').children('input.emin').val(minutePerDay);
								  countMinute=countMinute+minutePerDay;
								  if(countMinute>parseInt(totalMin)){
										 minutePerDay=parseInt(totalMin)-[countMinute-minutePerDay];
										 if(minutePerDay<0){
										   minutePerDay=0;
										 }
								  }else if(countMinute==parseInt(totalMin)){
										minutePerDay=0;
								  }else{
										var d=parseInt(totalMin)-countMinute;
										if(d<minutePerDay){
										   minutePerDay=d;
										   countMinute=parseInt(totalMin);
										 }
								  }
						}
						i++;
					 });
				   }
				   
				   parent.$("#loadingBar").hide();
				   parent.timerControl("");
			   }
		   }); 
}

function getWFDateXml(){
  taskDateXml = "<epicTaskDate>";
		var i=1;
		var count=0;
		var countMin=0;
		$('.wfEstHourDiv').each(function() {
		   var date = $(this).children('div').children('span.taskDate').text().trim();
		   var hour = $(this).children('div').children('input.ehour').val().trim();
		   var minute=$(this).children('div').children('input.emin').val().trim();
		   var status = $(this).children('input.edCheckbox').val();
		   var day = $(this).children('div').children('span.taskDay').text().trim();
		   
			 taskDateXml +="<taskDate id=\""+i+"\">"
			 taskDateXml +="<date>"+date+"</date>";
			 taskDateXml +="<day>"+day+"</day>";
			  taskDateXml +="<hour>"+hour+"</hour>";
			  taskDateXml +="<minute>"+minute+"</minute>";
			  taskDateXml +="<status>"+status+"</status>";
			 taskDateXml +="</taskDate>";
			 i++;
			 if(hour==''){
			   hour='0';
			 }
			 if(minute==''){
			   minute='0';
			 }
			 count=count+parseInt(hour);
			 countMin=countMin+parseInt(minute);
		});
		if(countMin>59 || countMin < 0){
		   parent.alertFun(getValues(companyAlerts,"Alert_TotMinVal"),'warning');
			   $('.emin').css("border","1px solid red");
			   return false;
	   }else{
			  $('.emin').css("border","1px solid #BFBFBF");
	   }
	taskDateXml +="</epicTaskDate>";
	$('#wfTaskDateXml').val(taskDateXml);			
	
	var startDate=$('#wFStart').val();
	var endDate=$('#wFEnd').val();
	$('#prevStartDate').val(startDate);
	$('#prevEndDate').val(endDate);
	$('#wfEstimatedHour').val(count);
	$('#wfEstimatedMinute').val(countMin);
	var totalHr=$('#wfEstHour').val();
	var totalMin=$('#wfEstMinute').val(); 
 
	if(totalHr>0 || totalMin>0){
		 if(count!=0 && count!=totalHr && countMin!=0 && countMin!=totalMin){
			 confirmIdeaIdMin=countMin;
			 confirmIdeaId=count;
			 parent.confirmReset(getValues(companyAlerts,"Alert_BudgetedMinHrMin"),'close','confirmTaskMinuteHourDivClose','showEpicTaskBurndownPopup');
		 }else if(count!=0 && count!=totalHr){
			 confirmIdeaId=count;
		   parent.confirmFun(getValues(customalertData,"Alert_Unmatch_BudgetedHr"),"close","confirmTaskHourDivClose");
		 
		 }else if(countMin!=0 && countMin!=totalMin){
			 confirmIdeaIdMin=countMin;
		   parent.confirmFun(getValues(companyAlerts,"Alert_Unmatch_BudgetedHr"),"close","confirmTaskMinDivClose");
		 }else if(countMin!=0 && count!=0){
			 $('input#wfEstHour').val(count);
			 $('input#wfEstMinute').val(countMin);
			hideWFTaskHourDiv();
		 }else if(count!=0){
			$('input#wfEstHour').val(count);
			hideWFTaskHourDiv();
	   }else if(countMin!=0){
			  $('input#wfEstMinute').val(countMin);
		   hideWFTaskHourDiv();
	   }else if(count!=0 && countMin!=0 && countMin!=0 && count!=0){
		   $('input#wfEstHour').val(count);
		   $('input#wfEstMinute').val(countMin);
		   hideWFTaskHourDiv();
			   
	  }else{
			hideWFTaskHourDiv();
		   }
	 }else{ 
	   $('input#wfEstHour').val(count);
	   $('input#wfEstMinute').val(countMin);
	   hideWFTaskHourDiv();
	 }	
}
var confirmIdeaId='';
var confirmIdeaIdMin='';

function confirmTaskMinuteHourDivClose(){
	$('input#wfEstHour').val(confirmIdeaId); 
	$('input#wfEstMinute').val(confirmIdeaIdMin); 
   hideWFTaskHourDiv();
} 
function confirmTaskHourDivClose(){
	$('input#wfEstHour').val(confirmIdeaId); 
	hideWFTaskHourDiv();
}
function confirmTaskMinDivClose(){
	$('input#wfEstMinute').val(confirmIdeaIdMin); 
	hideWFTaskHourDiv();
}

function loadWFTemplatePopup(){
   if($('#wfTemplatePopDivNew').is(':hidden')){
   $('#wfTemplatePopDivNew').show();
   $('#wrkFlowTempId').text('Workflow Templates');
   scrollBar('wfTemplateListContainer');
 }else{
   $('#wfTemplatePopDivNew').hide();
 } 
}

   var confirmTempId='';
	
   function selectWFTemplate(obj){
	   var projId = $(obj).attr('projectId');
	   var tempId = $(obj).attr('tempId');
		//$('#userProject').prop('disabled','disabled');
		var d = new Date();
	   var month = d.getMonth()+1;
	   var day = d.getDate();
	   var currentDate = (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + '-' + d.getFullYear();
	   
	   if($(obj).parent('div').children('img.wfTempSelected').length < 1){
		   mid = tempId;
		   $('#taskWFDetails').html("");
			  $("#loadingBar").show();
			  timerControl("start");
			  cntntVal = '';
			  $('#hiddenTempId').val(tempId);
			  $('#wfTemplateListContainer').find('div.wfTempCss').children('img.wfTempSelected').remove();
			  $(obj).parent('div').append('<img id = \"stageSelectedImg\"  src="images/StageSelected.png" style="float:left;margin-top:2px;margin-left:5px;height:15px;width:15px;visibility:visible;" class="wfTempSelected">');
			  if(($('.tabNameCls').find("#taskTab").find('#taskType').text() != "") && (projId !='0')){
				  $('select#userProject option[id="'+projId+'"],select#wfProject option[id="'+projId+'"]').prop("selected", "selected");
		   }
			  
	   
			  //$('select#wfProject').removeAttr('disabled');
			  //$('#wfProject').prop('disabled','disabled');
		   var wid = $('#taskWFDetails').width();
		   crumbWidth = wid;
		   checksession();
		   $.ajax({
			   url: apiPath+"/"+myk+"/v1/getTemplatestepDetails?fileNameId="+tempId+"&user_id="+userIdglb+"&company_id="+companyIdglb+"",
				type: "GET",
				error: function(jqXHR, textStatus, errorThrown) {
					   checkError(jqXHR,textStatus,errorThrown);
					   $("#loadingBar").hide();
					   timerControl("");
					   },   
			   success: function (data) {
					  
					  createHtmlForStepJson(data);
					  /*var wFHtml = data.split('^&*');
					  $('#taskWFDetails').html(wFHtml[0]);*/
					//$('#crumbs ul li a').css({'width':'95px','font-size':'14px','padding-left':'20px'});
				   alterHeightOfOthrContentAndUserDivs();
					  $('.wFEDatePicker').val(currentDate);
					  $('input.hasTimePicker').timepicker({ 'timeFormat': 'H:i' });
				   cntntVal = 0;
				   templateSelectedFlag = true;
				   $('#mainTaskDiv_0').find('.contTitle').css('width','0px');
					  $('.WFTaskActive').css('border','');
				   $('#taskWFDetails').find('.taskRows').hide();
				   tabActive($('#crumbs').find('.wf_level').first());
				   showTabDetails($('#crumbs').find('.wf_level').first());
				   //$('#addNewLevel').hide();
				   $('#crumbs').find('.wf_level').first().css('border','1px solid blue').addClass('stepSelected');
				   attachSelectEvent();
				   if(!isiPad)
					   scrollBar('taskWFDetails');
				   $('#loadingBar').hide();
				   timerControl("");
				   $('#wfTemplatePopDiv').hide();
				   showAttachTemplateDiv();
				   $('#stepIndexImg').show();
				   $('#wfHrline').show();
				   $('#wfBotBorderline').show();
				  /* $("#commonHtmlTaskUI").css("height",$(".navbar-header").innerHeight() - ($(".nav-tabs").innerHeight() + $("#rowTab").innerHeight()));
				  $("#newTaskUI").css("height",$("#commonHtmlTaskUI").innerHeight());
				  var topH = $("#tabMainDiv").innerHeight();
				  $(".contentCls").css("top",topH);*/
			   }
		   });
		   projId = $('select#userProject option:selected').attr('id');	
		   projId = $('select#wfProject option:selected').attr('id');  
		   checksession();
		   $.ajax({ 
			   url: apiPath+"/"+myk+"/v1/fetchParticipants?user_id="+userIdglb+"&company_id="+companyIdglb+"&project_id="+prjid,
			   type:"GET",
			   error: function(jqXHR, textStatus, errorThrown) {
					   checkError(jqXHR,textStatus,errorThrown);
					   $("#loadingBar").hide();
					   timerControl("");
					   }, 
			   success:function(result){
				   $("#userListContainer").html("");
					 parent.checkSessionTimeOut(result);
				   participantListUi(result,prjid);
				   userListResult=result;
					  var isiPad = navigator.userAgent.match(/iPad/i) != null;
				   if(!isiPad)	{
						  scrollBar('userListContainer');
					  }	
			  }
		   });
		 }else{
			 confirmTempId = tempId;
			 parent.confirmFun(getValues(companyAlerts,"Alert_remove"),"delete","confirmRemoveWFTemplate");
		 }
		 
 
   }
   function confirmRemoveWFTemplate(){
		 $('#tempSpan_'+confirmTempId).parent('div').children('img.wfTempSelected').remove();
		 templateSelectedFlag = true;
		 if($('.tabNameCls').find("#taskTab").find('#taskType').text() != ""){
			 $('select#userProject option[id="allUsers"], select#wfProject option[id="0"]').prop("selected", "selected");
			 $("#userProject,#wfProject").attr("disabled", false);
			 $('select#userProject option[value="All Users"]').prop("selected", "selected");
		 }
	   prevIndex = 0;
		 $('#hiddenTempId').val('');
		 $('#taskWFDetails').html('');
		 generateTabWithoutTemplate();
		 $('#wfTemplatePopDiv').hide();
		 attachSelectEvent();
		 //showAttachTemplateDiv();
   }
		   
   function prepareTemplateUI(jsonResult){
		   var UI="";
		   if(jsonResult == "[]"){
			   UI = "<span style=\"float:left;width:95%; font-family: OpenSansRegular;font-size:15px;margin-left:5px;\">No Templates to display</span>";
			 }
			 var jsonData = jQuery.parseJSON(jsonResult);
			 if(jsonData){
			 var json = eval(jsonData);
				if(jsonData.length>0){
				   for(var i=0;i<json.length;i++){
						   UI +=  " <div ondblclick=\"getDetailsOfTemplate("+json[i].templateId+")\"  id=\"wfTemplate_"+json[i].templateId+"\" class = \"taskCompCodes actFeedHover\" style=\"float:left;width:99%;height:40px;cursor:pointer;\">" 
							 +	" <span style=\" overflow: hidden;text-overflow: ellipsis;white-space: nowrap;margin:0px;padding-top:5px;width:99%;\" id = \"wfTempSpan_"+json[i].templateId+"\" class = \"workFlowListSpan taskCodeComm\" >"+json[i].templateName+"</span>" 
							 +	" <div id=\"wfsubcommentDiv_"+json[i].templateId+"\" class=\"hideDivsOnEdit taskCodeComm actFeedHoverImgDiv\" onclick=\"editWfCodes(this);\" style=\"float:left;margin-top:2px;\">" 
						   +		"<img alt = \"Image\" style = \"cursor:pointer;\" class=\"img-responsive margin Options_cLabelTitle\" src=\"images/more.png\"  title=\"Options\">"
						   +	"</div>"
							   
						   +	" <div id=\"WorkeditDivContainer_"+json[i].templateId+"\" style =\"height: 92%;display: none;\">"
						   +		"<textarea  id=\"wfTemp_main_commentTextarea_"+json[i].templateId+"\" class = \"main_commentTxtArea\" style =\"height: 98%;width: 90%;\" onkeypress =\"enterKeyValidCommentActFeed(event,2838);\"></textarea>"
						   + 		"<div align =\"center\" class = \"main_commentTxtArea_btnDiv\" style =\"height: 98%;width: 6%;\">"
						   +    		"<img alt=\"Image\" style=\"margin-top: 6px;cursor:pointer;\"  src=\"images/workspace/post.png\" class=\"img-responsive\">"
						   + 		"</div>"
						   +	"</div>"
							   
						   +	" <div class=\"actFeedOptionsDiv taskCodeComm\" id=\"wfsubcommentDiv1_"+json[i].templateId+"\" style=\"display: none;margin: -5px 20px 0px 0px\">" 
						   +		"<div class=\"workSpace_arrow_right\" style=\"margin-top: -5px; float: right; position: absolute; right: -16px;\">"
						   +			"<img src=\"images/arrow.png\"></div><div style=\"width:99%;height:auto;padding:0 0 3px ;cursor:pointer;float:left;border-bottom:1px solid #ccc;\">"
						   +			" <span onclick=\"viewWorkFlowTempLevels("+json[i].templateId+")\" style=\"float: left; height: 20px;margin-left: 5px; overflow: hidden; width: 60px;\" class=\"Reply_cLabelText\">View</span>"
						   +		" </div>"
								   
						   + 		" <div class='workflowUpdateOptDiv' style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-bottom:1px solid #ccc;\" >"
						   +			"<span onclick =\"updateWfTemplate("+json[i].templateId+")\"  style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\" class=\"Edit_cLabelText\">Update</span>"
						   + 		"</div>"
								   
								   
								   
						   + 		" <div class='workflowCopyOptDiv' style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-bottom:1px solid #ccc;\" >"
						   +			"<span onclick =\"copyWorkFlowTemplate("+json[i].templateId+")\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\" class=\"Edit_cLabelText\">Copy</span>"
						   + 		"</div>"
								   
						   +		" <div class='workflowDeleteOptDiv' style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;\" >" 
						   + 			"<span onclick=\"deleteWFTemplate("+json[i].templateId+");\" style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\" class=\"Task_cLabelText\">Delete</span>"
						   +		"</div>"
						   + 	"</div>"
						   + " </div>" ;
						   }
				   }
			 }	
			return UI;
		   }
		   
   function editWfCodes(obj){
		   var id = $(obj).attr('id').split('_')[1];
		   if ($('#wfsubcommentDiv1_'+id).is(":hidden")) {
			   $('div#workflowContentContainer').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
				  $('.actFeedOptionsDiv').hide();
				  $('.actFeedReplyOptionsDiv').hide();
				  $('#wfsubcommentDiv1_'+id).slideToggle(function(){
					  $(this).css('overflow','');
					  setWFOptionCss(obj);
				  });
			 }else{
				  $('div#workflowContentContainer').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','');
				 $('#wfsubcommentDiv1_'+id).slideToggle('slow');
			 } 
   }
   
  function setWFOptionCss(obj){
		var idd = $(obj).attr('id').split('_')[1];
		var t=$(obj).offset().top;
		var oh=$(window).height()-t - $('#saveDetailsContainer').height();
		var h=$('#wfsubcommentDiv1_'+idd).height();
		$('#wfsubcommentDiv1_'+idd).css('margin','-5px 20px 0px 0px');
		$('#wfsubcommentDiv1_'+idd+' .workSpace_arrow_right').css('margin-top','-5px');
		h=$('#wfsubcommentDiv1_'+idd).height();
		if(oh<h){
			var fh=h-oh+25;
			var fh1=h-oh+10;
			$('#wfsubcommentDiv1_'+idd).css('margin-top','-'+fh+'px');
			$('#wfsubcommentDiv1_'+idd+' .workSpace_arrow_right').css('margin-top',fh1+'px');
		} 
 }
   
   
   function viewTemplate(){
		   $("#transparentDiv").show();
		   $("input#wfName").val("");
		   $('#wfLevelContainer').html('');
		   $('#deleteLevel').hide();
		   $('div#workFlowMain').css('display','none');
		   $('div#workFlowCreate').css('display', 'block');
		   $("div.workflowTemplateList").css("background-color", "");
		   $('#addNewLevel').show();
		   $('#wfTemplateSave').show();
		   attachSelectEvent();
   }
   
   function closeWfDetails(){
		$("div#changeWfbtns").attr("onclick", "saveWFL();");
		$("div#changeWfbtns").html("SAVE");
		$("#addLvl").show();
		cancelWorkflowTemplateNew();
   }
   
   var upGtempId = "";
   function updateWfTemplate(id){
	   $('#loadingBar').show();
		 timerControl("start");
	   $("#addLvl").show();
	   $("div#changeWfbtns").html("UPDATE");
	   $("div#changeWfbtns").attr("onclick", "saveUpdateWFL();");
	   upGtempId = id;
	   viewTemplate();
	   $.ajax({
		   url: path+"/workspaceAction.do",
		   type:"POST",
		   data:{act:"fetchTemplateSteps",tempId:id},
		   error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				   $("#loadingBar").hide();
				   timerControl("");
				   }, 
		   success:function(result){
				 checkSessionTimeOut(result);
				 result = result.split("#@$#@#$");
				 var seHtml = putStartEndFlowDiagram();
				 $('#wfLevelContainer').html(seHtml);
				  var html = generateWFTemplate(result[0]);
				  $("input#wfName").val("");
				 $("input#wfName").val(result[1]);
				  $(html).insertBefore('#wfLevelContainer div.wfEnd');
				  attachSelectEvent();
				  $("div#wfLevelContainer").mCustomScrollbar("destroy");
				 popUpScrollBar('wfLevelContainer');
				 $('#loadingBar').hide();
				 timerControl("");
				 $("div#changeWfbtns").attr("onclick", "saveUpdateWFL();");
				 $("input#wfName").attr("readonly", false); 
				}
		   });
		 
   }
   
   function saveUpdateWFL(){
	   $("div#workFlowCreate").hide();
	   $("#transparentDiv").hide();
	   var wf_name = $('#wfName').val().trim();
		 if(wf_name==""){
		   alertFun(getValues(companyAlerts, "Alert_WFEmpty"),'warning');  
		   $('#wfName').val('');
		   return false;
		 }
		 var levelXml=generateStepXml();
		 var pid = $("input#globalProjectId").val();
	   $('#loadingBar').show();
	   timerControl("start");
	   $.ajax({
		   url: path+"/workspaceAction.do",
		   type:"POST",
		   data:{act:"saveWorkflowTemplate",templateName:wf_name,levelXml:levelXml,projId:pid,tempId:upGtempId,levelCkdId:levelCkdId},
		   error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				   $("#loadingBar").hide();
				   timerControl("");
				   }, 
		   success:function(result){
			   checkSessionTimeOut(result);
			   $('#loadingBar').hide();
			   timerControl("");
			   alertFun(getValues(companyAlerts,"Alert_TemplateSaved"), 'warning');
			   $("div#changeWfbtns").attr("onclick", "saveWFL();");
			   workflowTemplateNew(pid);
			   upGtempId="";
			   $("#addLvl").hide();
			}
	   });
   }
   
   function closeWorkFlwTemp(){
	   closeWfDetails();		
   }
   
   function closeConfirm(){
	   $(confirmLevelObj).hide();
   }
   
   var wfCalendar;
   function initwfdhtmlxdatecalNew(WFstart,WFEnd) {
		  wfCalendar = new dhtmlXCalendarObject(["wFStart","wFEnd"]);
	   wfCalendar.setDateFormat("%m-%d-%Y");
	   $('.dhtmlxcalendar_container').css({'z-index':'11000','border-radius':'5px','border':'2px solid #BFBFBF','background-color':'#FFFFFF'});
	   wfCalendar.hideTime();
	   wfCalendar.attachEvent("onClick",function(date){
			   //$('#taskTotalEhour').val('0');
		});
		  var today = new Date();
	   wfCalendar.setSensitiveRange(today, null);
	   wfCalendar.setPosition('right');
	   $('.dhtmlxcalendar_container').css({'z-index':'10000','border-radius':'5px','border':'2px solid #BFBFBF','background-color':'#FFFFFF' });
   }

	function setWFSens(id, k, sensVar) {
	   // update range
	   if (k == "min") {
		   wfCalendar.setSensitiveRange(byWFId(id).value, null);
	   }else {
		   if(byWFId(id).value)
			   wfCalendar.setSensitiveRange(new Date(), byWFId(id).value);
		   else
			   wfCalendar.setSensitiveRange(new Date(), null);
	   }
	}
	
	function byWFId(id) {
	   return document.getElementById(id);
	}

   function initWFSlider(){ 
	   $('div.userCompPerSlider').each(function() {
		   var value = parseInt( $( this ).text(), 10 );
		   $( this ).empty().slider({
			   value: value,
			   range: "min",
			   animate: true,
			   disabled: true ,
				  orientation: "horizontal"
		  });
	   });
	   if(jQuery.browser.msie){
		   $('#vas').css('height','20px');
	   }
   }
   
   function getDetailsOfTemplate(templateId){
	   viewWorkFlowTempLevels(templateId);
   }
   
   function loadCopyTemplatePopup(){
	 if($('#wfTemplatePopDiv').is(':hidden')){
		   $('#wfTemplatePopDiv').show();
		   $('#wrkFlowTempId').text('Workflow Templates');
		   if($('#wfTemplateListContainer').find('div.wfTempCss').length <1){
			   $('#loadingBar').show();
			   timerControl("start");
			   var projId = $('#wf_projId').val();
			   $.ajax({
					   url: path+'/calenderAction.do',
						  type:"POST",
						data:{act:'createWorkFlowTask', projIdFrTaskTemplates:projId,from:'workspace'},
						error: function(jqXHR, textStatus, errorThrown) {
									   checkError(jqXHR,textStatus,errorThrown);
									   $("#loadingBar").hide();
									   timerControl("");
						   }, 
					   success:function(result){
							   checkSessionTimeOut(result);
							   if(result == "[]"){
								   $('#wfTemplatePopDiv').find('#wfTemplateListContainer').html("<span style = \"padding-left: 38%;\">No Templates Available.</span>");
								}else{
								   $('#wfTemplatePopDiv').find('#wfTemplateListContainer').html(prepareHtmlForWFTemplates(jQuery.parseJSON(result),"workspace"));
								   $('#wfTemplatePopDiv').find('#wfTemplateListContainer').find('img.wfTempViewIcon').hide();
							   }
							   popUpScrollBar('wfTemplateListContainer');
							   $('#loadingBar').hide();
							   timerControl("");
						  }
				});
		  }
	 }else{
	   $('#wfTemplatePopDiv').hide();
	 } 
  }
   
  function prepareHtmlForWFTemplates(result,from){
	   var templateUI = "";
	   if(result){
		   var json = eval(result);
		   var projName = "";
		   for(var i = 0; i < json.length ;i++){
			   json[i].template_name = replaceSpecialCharacter(json[i].template_name);
			   projName = "";
			   if(json[i].project_name!=""){
				 projName = " - <i style='font-weight: bold;'>"+replaceSpecialCharacter(json[i].project_name)+"</i>";
			   }
			   templateUI += 	"<div ondblclick = \"event.stopPropagation();\" class=\"optionMenuCss wfTempCss\" >"
							   +"<div ondblclick = \"event.stopPropagation();\" class=\"wfTempCss\" style=\"color:#616161;float:left;width:99%;text-decoration:none;height: 100%\" class=\"optionMenuOpacity\">"
								   +"<span class=\"wfTempCss\" id='tempSpan_"+json[i].wf_template_id+"' style=\"color: #000;float: left;font-weight: normal;margin: 2px 10px 0; overflow: hidden; text-overflow:ellipsis;white-space: nowrap;width: 80%;\" projectId=\""+json[i].project_id+"\" tempId=\""+json[i].wf_template_id+"\" ondblclick = \"event.stopPropagation();\" "
								   if(from=="task")
									 templateUI +=" onclick=\"selectWFTemplate(this);\" "
								   else
									 templateUI +=" onclick=\"copyWFTemplate(this);\" "  
									 
								   templateUI +="  title=\""+json[i].template_name+"\" >"+replaceSpecialCharacter(json[i].wf_template_name)+""+projName+"</span>"                                 
								   +"<img class=\"wfTempCss wfTempViewIcon\" src=\"images/calender/view.png\" onclick = \"viewWorkFlowTempLevels("+json[i].wf_template_id+", 'calendar');\" title='View template' style=\"float:left;margin-top:2px;border:none;\">"
								   //+"<img id = \"stageSelectedImg\"  class=\"wfTempSelected\" style=\"float:left;margin-top:2px;margin-left:5px;height:15px;width:15px;visibility:hidden;\" src=\""+path+"/images/StageSelected.png\">"
							   +"</div>"
							   +"</div>"
		   }
	   }
					   
	   return templateUI;
   }	
   
   function copyWFTemplate(obj){
	  var tempId = $(obj).attr('tempId');
	   
	 if($(obj).parent('div').children('img.wfTempSelected').length < 1){
	   $('#wfTemplateListContainer').find('div.wfTempCss').children('img.wfTempSelected').remove();
	   $(obj).parent('div').append('<img id = \"stageSelectedImg\"  src="images/StageSelected.png" style="float:left;margin-top:2px;margin-left:5px;height:15px;width:15px;visibility:visible;" class="wfTempSelected">');
	   $('#wfTemplatePopDiv').hide();   	
	   $('#loadingBar').show();
	   timerControl("start");
	   $.ajax({
		   url: path+"/workspaceAction.do",
		   type:"POST",
		   data:{act:"fetchTemplateSteps",tempId:tempId},
		   error: function(jqXHR, textStatus, errorThrown) {
				   checkError(jqXHR,textStatus,errorThrown);
				   $("#loadingBar").hide();
				   timerControl("");
				   }, 
		   success:function(result){
				 checkSessionTimeOut(result);
				 result = result.split("#@$#@#$");
				 var seHtml = putStartEndFlowDiagram();
				 $('#wfLevelContainer').html(seHtml);
				  var html = generateWFTemplate(result[0]);
				  $(html).insertBefore('#wfLevelContainer div.wfEnd');
				  attachSelectEvent();
				  $('#wfLevelContainer').find('div.wf_level').find('input.levelId').val("0");
				  $('#wfLevelContainer').find('div.wf_level').find('img.docIcon,img.levelDeleteIcon ').attr("levelId","0");
				  $("div#wfLevelContainer").mCustomScrollbar("destroy");
				 popUpScrollBar('wfLevelContainer');
				 $('#loadingBar').hide();
				 timerControl("");
			   }
		   });
	}else{
	  confirmTempId = tempId;
	  confirmFun(getValues(companyAlerts,"Alert_remove"),"delete","confirmRemoveCopyTemplate");
	}
   }
   
   function confirmRemoveCopyTemplate(){
	   $('#tempSpan_'+confirmTempId).parent('div').children('img.wfTempSelected').remove();
		 $('#wfTemplatePopDiv').hide();
		 var seHtml = putStartEndFlowDiagram();
	   $('#wfLevelContainer').html(seHtml);
   }
   

   