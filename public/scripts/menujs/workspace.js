
  var jsonData = '';
  var index = '';
  var limit = '';
  var type = '';
  var txt = '';
  var ipadVal = '';
  var myprojBoolen = true;
  var myProjLimit = 0;
  
  var projSettingL="";
  var j = 0;
  var defImage = "";
  var d1 = "";
  var data = "";
  var projType = "";
  var projName = "";
  var projArchStatus = "";
  var mAct="";
  var unlockProjectId = "";
  var unlockProjName = "";
  var availProjBoolen = true;
  var availProjLimit = 0;
  var l=0;
  
  var sortType = "";
  var searchVal = "";
  var searchType = '';
  var searchProjType = 'visible';
  var projectUsersStatus="";
  var wrkspaceImgType = '';
  var filterType='';
  var rid = roleid.split("\"")[1];

  function fetchWsData(type,projectID,place){

	let jsonbody = {
		"user_id" : userIdglb,
		"project_id": projectID
	}
	$('#userList ul').html('');
	$('#teamContentDiv ul').html('');
	$('#loadingBar').addClass('d-flex').removeClass('d-none');
	$.ajax({
		url: apiPath + "/" + myk + "/v1/loadProjectDetails",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  $('#loadingBar').addClass('d-none').removeClass('d-flex');
		},
		success: function (result) { 
		 
			createNewWorkspace(type);

			$('#wsPopupHeader').text("Edit Workspace");
			$('.statusDiv').show();
			$('#projName').val(result[0].project_name);
			$('#projTagName').val(result[0].project_tag_name);
			$('#comment1').val(result[0].projDesc);
			$('#startDate, #startDateWSStatus').val(result[0].project_start_date);
			$('#endDate, #endDateWSStatus').val(result[0].project_end_date);
			$('#changeDynamic').text('Update');
			$('#changeDynamic').attr('onclick','createNewWorkspaceProject("updateWSProject",'+projectID+')');
			$('#settingProjectId').val(projectID);
			$('#commentStatusValue').val(result[0].project_overall_status);
			$('#hideDiv').addClass('pImg');
			$('.statusDiv, #statusColorcodediv').css("background-color",result[0].project_background_colour);
			if(result[0].project_image_type!=""){$('#hideDiv, #statusPagePrjimg').attr('src',lighttpdpath+"projectimages/"+projectID+"."+result[0].project_image_type+"?"+d.getTime());}else{$('#hideDiv, #statusPagePrjimg').attr('src',lighttpdpath+"projectimages/dummyLogo.png");}
			if(result[0].project_download_status == "checked"){$('#emailAttachmentCheckVal').attr('checked','checked');}
			if(result[0].project_status == "private"){$('#privacyImg').attr('src','images/workspace/pvt.svg');}else{$('#privacyImg').attr('src','images/workspace/pub.svg');$('#privacyImg').attr('value','public');}
			if(result[0].archive_status == "A"){$('#privacyImg1').attr('src','images/workspace/arc.svg');}else{$('#privacyImg1').attr('src','images/workspace/act.svg');$('#privacyImg1').attr('value','Y');}
			if(result[0].project_hiden_status == "unhide"){$('#unhide').attr("value","unhide").attr('src','images/workspace/eye1.svg');}else{$('#unhide').attr("value","hide").attr('src','images/workspace/eye2.svg');}
			
			$("#chartCanvas").removeAttr("id").attr("id","chartCanvas_"+projectID+"")
			//$('#advUpdate1').attr('onclick','addUsers('+projectID+',"advancePage")');
			if(result[0].proj_user_status == "PO"){
				$('.saveUpdateDiv, #plusPop').show();
				$('#advUpdate1').attr('onclick','addUsers('+projectID+',"advancePage","workspace")');
			}else{
				$('.saveUpdateDiv, #plusPop, #advUpdate1, #wsSaveUpdateDiv').hide();
			} 
			
			pendingUsersList(projectID);

			getTheProjectUsers(projectID);

			getColorCodes();

			getStatusComments(projectID);

			getTaskCodes(projectID);

			sentimentScoreforProject(projectID);
			if(place.match("team")){
				showAdv();
				wstabs('#tab01',1);
				$("#tab02").attr('onclick','');
				$("#tab03").attr('onclick','');
				$("#tab04").attr('onclick','');
				$("#tab05").attr('onclick','');
				$('#advUpdate1').attr('onclick','addUsers('+projectID+',"advancePage","teams")');
				$('#cancel1').attr('onclick','closeAdvOptions("team")');
				$('#cancel2').attr('onclick','closeAdvOptions("team")');
				$('#cancel3').attr('onclick','closeAdvOptions("team")');
				
				// console.log("hiii");
			}else{

			}

			$('#loadingBar').addClass('d-none').removeClass('d-flex');

		}
	});

  } 

  function openTcodeLibrary(){
	$('#tCodeLibraryListDiv').html('');
	$('#tCodeMainDiv').hide();
	$('#tCodeLibraryDiv').show();
	var settingdPrjid = $('#settingProjectId').val(); 
	let jsonbody = {
		"companyId" : companyIdglb,
    	"project_id" : settingdPrjid
	}
	$('#loadingBar').addClass('d-flex').removeClass('d-none');
	$.ajax({
		url: apiPath + "/" + myk + "/v1/loadCompanyQuotes",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  $('#loadingBar').addClass('d-none').removeClass('d-flex');
		},
		success: function (result) {
			var companyCodes="";
			if(result.length>0){
				for(var i=0;i<result.length;i++){
					companyCodes += "<div class='d-flex align-items-center py-2 tCodeLibListValue' style='border-bottom: 1px solid #B7BBBE;' id=\"tCodeLibList"+result[i].quotes_id+"\">"
					+ "<input style=\"float:left;width:5%;cursor:pointer;\" type=\"checkbox\" class='tCodeLibListCheckValue' id=\"check_"+result[i].quotes_id+"\">"
					+ "<span style=\"float:left;width:95%;font-size: 12px;\" title=\""+result[i].quotes+"\" class='defaultExceedCls'>"+result[i].quotes+"</span>"
					+ "</div> "
				}
			}else{
				companyCodes = "<div style=\"float:left;margin-left:10px;\">No library codes to display</div>";
			}
			$('#tCodeLibraryListDiv').append(companyCodes);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
		}
	});
  }

  var taskCheckedCodes="";
  function checkTCodes() {
	taskCheckedCodes = "";
	var jsonvalue = '[';
	$('.tCodeLibListCheckValue').each(function() {
		var cId = $(this).attr('id');
		var cId1 = $(this).attr('id').split("_")[1];
		var flag = document.getElementById(cId).checked;
		if (flag) {
			jsonvalue+="{";
			jsonvalue+="\"quotes_id\":\""+cId1+"\"";
			jsonvalue+="},";
		}
	});
	if(jsonvalue.length>1){
		taskCheckedCodes = jsonvalue.substring(0, jsonvalue.length-1);
		taskCheckedCodes=taskCheckedCodes+"]";
	}else{
		taskCheckedCodes ="[]";
	}
	if (taskCheckedCodes == null || taskCheckedCodes == '' || taskCheckedCodes == '[]') {
		alertFun(getValues(companyAlerts,"Alert_CheckCode"),'warning');
		taskCheckedCodes = "[]";
		return false;
	}
	
	confirmFun(getValues(companyAlerts,"Alert_AddCodeLib"), "clear","addSelectedTaskCodes");
  }

  function addSelectedTaskCodes(){
	var settingdPrjid = $('#settingProjectId').val(); 
	let jsonbody = {
		"user_id" : userIdglb,
		"companyId" : companyIdglb,
		"project_id" : settingdPrjid,
		"inviteUser":JSON.parse(taskCheckedCodes)
	}
	
	$.ajax({
		url: apiPath + "/" + myk + "/v1/insertQuotesToDbFromComp",
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  
		},
		success: function (result) {
			if(result == "success"){
				getTaskCodes(settingdPrjid);
				alertFun(getValues(companyAlerts,"Alert_CodeAdd"),'warning');
			}else{
				alertFun('Failed to add','warning');
			}
			$('#tCodeMainDiv').show();
			$('#tCodeLibraryDiv').hide();
  

		}
	}); 
  }

  function backToTaskCodes(){
	$('#tCodeMainDiv').show();
	$('#tCodeLibraryDiv').hide();
  }

  function getTaskCodes(projectID){
	$('#statusCommentDiv').html('');
	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"project_id" : projectID,
    	"companyId" : companyIdglb
	}
	$('#taskCompletionCodeDiv').html('');
	$.ajax({
		url: apiPath + "/" + myk + "/v1/loadProjQuotes",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  
		},
		success: function (result) {
			if(result.length>0){
				$('#taskCompletionCodeDiv').append(taskCodeUI(result,projectID));
			}else{
				$('#taskCompletionCodeDiv').append("<div id='emptyTaskcodeInput' class='mt-3' style='color:#999999;font-size:12px;'>No codes to display</div>");
			} 
			

		}
	});
	
  }

  function taskCodeUI(result,projectID){
		var ui="";
				
		for(var i=0;i<result.length;i++){
			ui+="<div id=\"wsCode_"+result[i].quotes_id+"\" class=\"workspaceCodes pl-2\" style=\"position:relative;width:100%;height:40px;border-bottom:1px solid #bebebe;\">"
			
					+"<div class=\"d-flex\" id=\"taskCodeDisplayArea_"+result[i].quotes_id+"\" onmouseover=\"addBgcolor("+result[i].quotes_id+",'taskcode');event.stopPropagation();\" onmouseout=\"removeBgcolor("+result[i].quotes_id+",'taskcode');event.stopPropagation();\" style='height:40px;'>"
		
						+'<p id="quote_'+result[i].quotes_id+'" title="'+result[i].quotes+'" class="align-self-center m-0 defaultExceedCls" style="width:98%;font-size: 12px;">'+result[i].quotes+'</p>'

						+'<div class="d-flex align-self-center" style="width:2%;">'
							+'<img id="tCodeMoreOptionsimage_'+result[i].quotes_id+'" style="display:none;cursor:pointer;width:5px;" class=" img-responsive position-relative" src="/images/more.png" title="Options" onclick="showTCodeOption('+result[i].quotes_id+');event.stopPropagation();">'
							+"<div id=\"taskCodeOptionsDiv_"+result[i].quotes_id+"\" class=\"actFeedOptionsDiv position-absolute d-none rounded\" style=\"border: 1px solid #c1c5c8;background-color: #fff;right: 5px;top:5px;text-align: left;z-index: 1;\">"			//px-2 rounded text-align-center position-absolute font-weight-bold
							/* 	+"<div id=\"\" class='py-1' onclick=\"openTaskcodeEditArea("+result[i].quotes_id+");\" style=\"color:#6f6d6d;font-size:12px;cursor:pointer;\">Edit</div>"
								+"<div id=\"\" class='py-1' onclick=\"showReorderOption("+result[i].quotes_id+");\" style=\"color:#6f6d6d;font-size:12px;cursor:pointer;\">Reorder</div>"
								+"<div id=\"\" class='py-1' onclick='deleteTaskCode("+result[i].quotes_id+")' style=\"color:#6f6d6d;font-size:12px;cursor:pointer;\">Delete</div>"
							 */
								+'<div class="d-flex align-items-center">'
									+"<img src=\"/images/conversation/edit.svg\" onclick=\"openTaskcodeEditArea("+result[i].quotes_id+");\" title=\"Edit\" class=\"image-fluid mx-1\" style=\"height:20px;width:20px;cursor:pointer;\">"
									+'<img src="/images/temp/reorder.png" title="Reorder"  id="" onclick="showReorderOption('+result[i].quotes_id+');" class="image-fluid mx-1" style="height: 17px;width:17px;cursor:pointer;">'
									+'<img src="/images/conversation/delete.svg" title="Delete" id="" onclick="deleteTaskCode('+result[i].quotes_id+')" class="image-fluid mx-1" style="height: 15px;width:15px;cursor:pointer;">'
								+'</div>'
							+"</div>"
						+'</div>'
									
					+'</div>'

					+"<div id='taskCodeEditArea_"+result[i].quotes_id+"' class='my-1 d-none' style='height:40px;'>"
						+ "<textarea id='taskQuoteEdit_"+result[i].quotes_id+"' class=\"pb-0 pt-1 px-2 rounded-left wsScrollBar\" style='resize: none;height: 35px;width: 94%;float: left;border: 1px solid #bebebe;font-size: 12px;outline:none;'></textarea>"
						+ "<div align='center' class=\"rounded-right\" style='height: 35px;float: left;width: 5%;border: 1px solid #bebebe;'>"
							+"<img class=\"img-responsive mt-1\" onclick='postEditedTaskCode();' src=\"images/conversation/post.svg\" style=\"width: 20px;cursor:pointer;\">"
						+ "</div>"
					+"</div>"

					+ " <div id=\"quoteReorderDiv_"+result[i].quotes_id+"\"  class=\"quoteReorderDivs\" style=\"margin-top:-38px;width:35px;border-radius:35px;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.6);border:1px solid #A1A1A1;position:absolute;display:none;height:35px;background-color:#ffffff;right:50%;padding:5px;z-index:5000;\" >"
						+ " <div align=\"center\" style=\"padding: 2px; margin: -2px auto; width: 14px; height: 12px;cursor:pointer;\" onclick=\"reorder('top','"+result[i].quotes_id+"');\" ><img src=\"images/arrow-top.png\" style=\"cursor:pointer;height:10px;width:10px;float:left;\"/></div>"
						+ " <div align=\"center\" style=\"width: 14px; height: 12px; padding: 2px; margin: 4px auto 0px;cursor:pointer;\" onclick=\"reorder('down','"+result[i].quotes_id+"');\" ><img src=\"/images/arrow-down.png\" style='cursor:pointer;height:10px;width:10px;float:left;'/></div>"
					+ " </div>"

				+'</div>';
		}
			
		return ui;
		
  }

  function trigTCodeOptions(qId){
	$('#tCodeMoreOptionsimage_'+qId).trigger('click');
  }

  function showReorderOption(qId){
	$('#taskCodeOptionsDiv_'+qId).removeClass('d-block').addClass('d-none');
	if ($('#quoteReorderDiv_' + qId).is(":hidden")) {
		settingsTaskCodeId=qId;
		$('.quoteReorderDivs').hide();
		$('#quoteReorderDiv_'+qId).show();
	} else {
		settingsTaskCodeId="";
		$('#quoteReorderDiv_'+qId).hide();
	}
	
  }

  function reorder(position, quoteId) {
	var nextQuoteId = "";
	if (position == "top") {
		if ($('#wsCode_' + quoteId).prevAll('div:visible').length) {
			nextQuoteId = $('#wsCode_' + quoteId).prevAll(
					'div:visible').attr('id').trim().split('_')[1];
			if (nextQuoteId != 'undefined') {
				var newQData = $('div#wsCode_' + quoteId).clone();
				$('div#wsCode_' + quoteId).remove();
				$(newQData).insertBefore('div#wsCode_' + nextQuoteId);
				updateTaskCodeOrder(quoteId,nextQuoteId);
			}
		} else {
			alertFun(getValues(companyAlerts,"Alert_reorderNotPermissible"), 'warning');
		}
	} else if (position == "down") {
		if ($('#wsCode_' + quoteId).nextAll('div:visible').length) {
			nextQuoteId = $('#wsCode_' + quoteId).nextAll(
					'div:visible').attr('id').trim().split('_')[1];
			if (nextQuoteId != 'undefined') {
				var newQData = $('div#wsCode_' + quoteId).clone();
				$('div#wsCode_' + quoteId).remove();
				$(newQData).insertAfter('div#wsCode_' + nextQuoteId);
				updateTaskCodeOrder(quoteId,nextQuoteId);
			}
		} else {
			alertFun(getValues(companyAlerts,"Alert_reorderNotPermissible"), 'warning');
		}
	}
  }

  function updateTaskCodeOrder(quoteId,nextQuoteId){
	let jsonbody = {
		"currentId" : quoteId,
    	"targetId" : nextQuoteId
	}
	$('#loadingBar').addClass('d-flex').removeClass('d-none');
	$.ajax({
		url: apiPath + "/" + myk + "/v1/updateQuoteOrder",
		type: "PUT",
		//dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  $('#loadingBar').addClass('d-none').removeClass('d-flex');
		},
		success: function (result) { 
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
		}
	});
  }

  function openTaskcodeEditArea(qId){
	settingsTaskCodeId=qId;
	var qCode = $('#quote_'+qId).text();
	$('#taskCodeDisplayArea_'+qId).removeClass('d-flex').addClass('d-none');
	$('#taskCodeEditArea_'+qId).removeClass('d-none').addClass('d-flex');
	$('#taskQuoteEdit_'+qId).val(qCode).focus();
  }

  function postEditedTaskCode(){
	var editedCode = $('#taskQuoteEdit_'+settingsTaskCodeId).val().trim();
	var settingdPrjid = $('#settingProjectId').val(); 
	if(editedCode != ""){
		let jsonbody = {
			"project_id" : settingdPrjid,
			"quotes" : editedCode,
			"quotes_id" : settingsTaskCodeId
		}
		$('#loadingBar').addClass('d-flex').removeClass('d-none');
		$.ajax({
			url: apiPath + "/" + myk + "/v1/insertUpdateQuotesToDb",
			type: "PUT",
			dataType: 'json',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
			  checkError(jqXHR, textStatus, errorThrown);
			  $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) { 
				
				if(result != ""){
					$('#quote_'+settingsTaskCodeId).text(result[0].quotes);
				}else{
					alertFun("Failed to update",'warning');
				}
				$('#taskCodeDisplayArea_'+settingsTaskCodeId).removeClass('d-none').addClass('d-flex');
				$('#taskCodeEditArea_'+settingsTaskCodeId).removeClass('d-flex').addClass('d-none');
				$('#taskCodeOptionsDiv_'+settingsTaskCodeId).removeClass('d-block').addClass('d-none'); 
				settingsTaskCodeId="";
				$('#loadingBar').addClass('d-none').removeClass('d-flex');
			}
		});
	}else{
		alertFun("Task code cannot be empty",'warning');
	}
	
  }
  var delQid="";
  function deleteTaskCode(qId){
	delQid=qId;
	confirmFun(getValues(companyAlerts,"Alert_DelCodeConfirm"), "clear","deleteTaskCodeconfirm");  
	
  }

  function deleteTaskCodeconfirm(){
	var settingdPrjid = $('#settingProjectId').val(); 
	let jsonbody = {
		"project_id" : settingdPrjid,
    	"quotes_id" : delQid
	}
	$('#loadingBar').addClass('d-flex').removeClass('d-none');
	$.ajax({
		url: apiPath + "/" + myk + "/v1/deleteProjQuotes",
		type: "DELETE",
		//dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  $('#loadingBar').addClass('d-none').removeClass('d-flex');
		},
		success: function (result) { 
			
			if(result == "success"){
				$('#wsCode_'+delQid).remove();
			}else{
				alertFun("Failed to delete",'warning');
			}
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			delQid="";
		}
	});
  }

  function postTaskCode(){
	var settingdPrjid = $('#settingProjectId').val(); 
	var taskQuotes = $('#taskQuote').val().trim(); 
	if(taskQuotes != ""){
		let jsonbody = {
			"user_id" : userIdglb,
			"companyId" : companyIdglb,
			"project_id" : settingdPrjid,
			"quotes" : taskQuotes
		}
		$('#loadingBar').addClass('d-flex').removeClass('d-none');
		$.ajax({
			url: apiPath + "/" + myk + "/v1/insertQuotesToDb",
			type: "POST",
			dataType: 'json',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
			  checkError(jqXHR, textStatus, errorThrown);
			  $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) {
				$('#taskQuote').val(''); 
				if(result != ""){
					$('#emptyTaskcodeInput').remove(); 
					$('#taskCompletionCodeDiv').prepend(taskCodeUI(result));
				}else{
					alertFun("Failed to post",'warning');
				}
				$('#loadingBar').addClass('d-none').removeClass('d-flex');
			}
		});
	}else{
		alertFun("Task code cannot be empty",'warning');
	}

	

  }

  function showTCodeOption(qId){
	if($('#taskCodeOptionsDiv_'+qId).is(':hidden')){
		$("[id^=taskCodeOptionsDiv_]").removeClass('d-block').addClass('d-none');
		$('#taskCodeOptionsDiv_'+qId).addClass('d-block').removeClass('d-none');
	}else{
		$('#taskCodeOptionsDiv_'+qId).removeClass('d-block').addClass('d-none');
	}
  }

  function postStatusComment(){
	var settingdPrjid = $('#settingProjectId').val(); 
	var oldStatus = $('#commentStatusValue').val(); 
	var cmtStatus = $('#statCmtText').val().trim(); 
	if(cmtStatus != ""){
		let jsonbody = {
			"companyId" : companyIdglb,
			"project_id" : settingdPrjid,
			"statusChecked" : "N",
			"oldStatus" : oldStatus,
			"comment" : cmtStatus,
			"project_tag_name" : "",
			"selectedUserName" : "",
			"changeStatus" : "",
			"user_id" : userIdglb
		}
		$('#loadingBar').addClass('d-flex').removeClass('d-none');
		$.ajax({
			url: apiPath + "/" + myk + "/v1/updateProjectStatus",
			type: "POST",
			//dataType: 'json',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
			  checkError(jqXHR, textStatus, errorThrown);
			  $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) {
				$('#statCmtText').val(''); 
				if(result == "success"){
					getStatusComments(settingdPrjid);
				}else{
					alertFun("Failed to post",'warning');
				}
				$('#loadingBar').addClass('d-none').removeClass('d-flex');
			}
		});
	}else{
		alertFun("Status cannot be empty",'warning');
	}
	
  }

  function getStatusComments(projectID){
	$('#statusCommentDiv').html('');
	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"project_id" : projectID,
		"companyId" : companyIdglb,
		"localOffsetTime" : localOffsetTime,
		"globalstatuscommentId" : "",
		"globalstatuscommentText" :""
	}

	$.ajax({
		url: apiPath + "/" + myk + "/v1/loadProjCommentList",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  
		},
		success: function (result) { 
			var ui="";
			var createdElapTime="";
			var userimg="";
			if(result.length>0){
				for(var i=0;i<result.length;i++){
					if(typeof(result[i].modified_elapsed_time) != "undefined"){
						createdElapTime = result[i].modified_elapsed_time; 
						createdElapTime = calculateElapseTime(createdElapTime);
					}else{
						createdElapTime = result[i].last_updated_timestampTZ;;
					}
					if(typeof(result[i].user_image_type) != "undefined"){
						userimg = lighttpdpath+"/userimages/"+result[i].user_id+"."+result[i].user_image_type+"?"+d.getTime();
					}else{
						userimg="";
					}	
					

					ui+='<div id="statusCommentDivList_'+result[i].project_status_comment_id+'" onmouseover="showStatusOptions('+result[i].project_status_comment_id+');" onmouseout="hideStatusOptions('+result[i].project_status_comment_id+');" class="d-flex my-2" style="height:75px;width:100%;">'
							+'<div style="width:2%;background-color:'+result[i].colour_name+'">'
							+'</div>'
							+"<div id=\"statusCommentDivListcontent_"+result[i].project_status_comment_id+"\" class='rounded-right' style=\"width:98%;height: 100%;border: 1px solid #bdb7b7;position:relative;\"  >"				//onclick=\"showStatusEditDelete("+result[i].project_status_comment_id+",'"+result[i].comment+"');event.stopPropagation();\"
								+'<div style="height: 100%;width:100%;">'
							
									+'<div class="media border" style="height:100%;">'
									+'<img src="'+userimg+'" class="mt-2 ml-3 border rounded-circle" style="width:35px;">'
										+'<div class="media-body mt-3 ml-2" style="width:92%;">'
										+'<h4 class="m-0 defaultExceedCls" style="font-size: 12px;">'+result[i].name+'&nbsp;&nbsp; <span style="font-size:12px;">Posted : '+createdElapTime+'</span></h4>'
											+'<div class="media mt-2 defaultExceedCls wsScrollBar" style="overflow-y: auto;height: 33px;">'
												+'<div class="media-body w-100">'
													+'<p id="statCommentPara_'+result[i].project_status_comment_id+'" title="'+result[i].comment+'" class="m-0 defaultExceedCls" style="font-size: 12px;">'+result[i].comment+'</p>'
												+'</div>'
											+'</div>'      
										+'</div>'
									+'</div>'

								+'</div>'
								//+'<div id="optionsStatusDiv_'+result[i].project_status_comment_id+'">
								
								+'<div id="statuscommentDiv_'+result[i].project_status_comment_id+'" class="actFeedOptionsDiv py-1" style="display:none;z-index:10000;border-radius:5px;top:25px;right:10px;">' 
										+'<div class="d-flex align-items-center">'
											+"<img src=\"/images/conversation/edit.svg\" title=\"Edit\" onclick=\"editStatusComment("+result[i].project_status_comment_id+",'"+result[i].comment+"',"+projectID+");\" id=\"statEdt_"+result[i].project_status_comment_id+"\"  class=\"image-fluid\" style=\"margin:0px 5px;cursor:pointer;height: 18px;width:18px;\">"
											+'<img src="/images/conversation/delete.svg" title="Delete" id="statDel_'+result[i].project_status_comment_id+'"  onclick="deleteStatusComment('+result[i].project_status_comment_id+');" class="image-fluid" style="margin:0px 5px;height: 16px;width:16px;cursor:pointer;">'
										+'</div>'
								+'</div>'
								
								//+'</div>'
							+'</div>'
							+'<div id="editArea_'+result[i].project_status_comment_id+'" style="width:98%;height: 100%;display:none;">'
								+'<textarea id="statCmtTextEdit_'+result[i].project_status_comment_id+'" class="rounded-right" placeholder="" style="outline:none;font-size: 12px;height: 100%;width:100%;border: 1px solid #ccc2c2;resize:none;"></textarea>'
								+"<img class=\"ml-auto mt-2\" src=\"/images/conversation/post.svg\" title=\"Post\" onclick=\"postEditedStatusComment();\" style=\"width:20px;cursor:pointer;float: right;\">"
              				+'</div>'
						+'</div>';
				}
			}
			
			$('#statusCommentDiv').append(ui);
			
		}
	});
	  
  }

  function showStatusOptions(statCmtid){
	$('#statuscommentDiv_'+statCmtid).show();
  }
  function hideStatusOptions(statCmtid){
	$('#statuscommentDiv_'+statCmtid).hide();
  }

  
  /* function showStatusEditDelete(statCmtId,statCmt){
	var ui="";
	$('#optionsStatusDiv_'+statCmtId).html('');
	
	ui='<div id="statuscommentDiv_'+statCmtId+'" class="actFeedOptionsDiv py-1" style="display:block;z-index:10000;border-radius:5px;top:25px;right:10px;">' 
		+'<div class="d-flex align-items-center">'
			+"<img src=\"/images/conversation/edit.svg\" title=\"Edit\" onclick=\"editStatusComment("+statCmtId+",'"+statCmt+"');\" id=\"statEdt_"+statCmtId+"\"  class=\"image-fluid mr-2 ml-1\" style=\"cursor:pointer;height: 18px;width:18px;\">"
			+'<img src="/images/conversation/delete.svg" title="Delete" id="statDel_'+statCmtId+'"  onclick="deleteStatusComment('+statCmtId+');" class="image-fluid mx-2" style="height: 16px;width:16px;cursor:pointer;">'
		+'</div>'
	+'</div>'

	$('#optionsStatusDiv_'+statCmtId).append(ui);

  } */

  function editStatusComment(statCmtId,statCmt,projectID){
	settingsStatusCmtId =  statCmtId;
	settingsStatusCmt = $('#statCommentPara_'+statCmtId).text().trim();
	wsSettingsGlobalProjectId = projectID;
	$('#statusCommentDivListcontent_'+statCmtId).hide();
	$('#editArea_'+statCmtId).show();
	$('#statCmtTextEdit_'+statCmtId).val(settingsStatusCmt).focus();
	$('#statusCommentDivList_'+statCmtId).next().addClass('mt-5');
  }

  function dontClearedit(){
	
  }

  function postEditedStatusComment(){
	var localOffsetTime = getTimeOffset(new Date());
	var editedComment = $('#statCmtTextEdit_'+settingsStatusCmtId).val().trim();
	if(editedComment != ""){
		let jsonbody = {
			"project_id" : wsSettingsGlobalProjectId,
			"localOffsetTime" : localOffsetTime,
			"globalstatuscommentId" : settingsStatusCmtId,
			"globalstatuscommentText" : editedComment
		}
		$('#loadingBar').addClass('d-flex').removeClass('d-none');
		$.ajax({
			url: apiPath + "/" + myk + "/v1/upadteCommentsInStatus",
			type: "POST",
			dataType: 'json',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
			  checkError(jqXHR, textStatus, errorThrown);
			  $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) { 
				
				if(result != ""){
					$('#statCommentPara_'+settingsStatusCmtId).text(result[0].comment);
					$('#statusCommentDivListcontent_'+settingsStatusCmtId).show();
					$('#editArea_'+settingsStatusCmtId).hide();
					$('#statCmtTextEdit_'+settingsStatusCmtId).val('');
					$('#statusCommentDivList_'+settingsStatusCmtId).next().removeClass('mt-5');
					settingsStatusCmtId="";
					settingsStatusCmt="";
	
	
	
				}else{
					alertFun("Failed to update",'warning');
				}
				$('#loadingBar').addClass('d-none').removeClass('d-flex');
			}
		});
	}else{
		alertFun("Status cannot be empty",'warning');
	}
	
  }

  var deletestatCmtId="";
  function deleteStatusComment(statCmtId){
	deletestatCmtId=statCmtId;
	confirmFun(getValues(companyAlerts,"Alert_DelCmt"), "clear","deleteStatusCommentConfirm");  
  }

  function deleteStatusCommentConfirm(){
	let jsonbody = {
		"globalstatuscommentId":deletestatCmtId
	}
	$('#loadingBar').addClass('d-flex').removeClass('d-none');
	$.ajax({
		url: apiPath + "/" + myk + "/v1/deleteProjCommentList",
		type: "POST",
		//dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  $('#loadingBar').addClass('d-none').removeClass('d-flex');
		},
		success: function (result) { 
			
			if(result == "success"){
				$('#statusCommentDivList_'+deletestatCmtId).remove();
			}else{
				alertFun("Failed to delete",'warning');
			}
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			deletestatCmtId="";
		}
	});
  }

  function getColorCodes(){
	$.ajax({
		url: apiPath + "/" + myk + "/v1/loadProjStatus",
		type: "POST",
		
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  
		},
		success: function (result) { 
			
			var html='<div class="attachListOptions rounded p-1 border d-block" style="width: 140px;background-color: #fff;font-size: 11px;top: -58px;border-color: #c1c5c8 !important;box-shadow: 0 6px 12px rgb(0 0 0 / 18%);left: 0px;">'
			+'<span align="left" style="font-family: opensanssemibold;font-size: 12px;">'+getValues(companyLabels,"WS_Select_Status")+'</span>';
			if(result.length>0){
				for(var i=0;i<result.length;i++){
					html+='<div id="status_'+result[i].lov_id+'" class=\"statusColourCodes\" status_id="'+result[i].lov_id+'"  status_code="'+result[i].lov_value+'"  style="width:100%;border-top:1px solid #cccccc;height: 25px;padding: 2% 1%;color:#000000;cursor:pointer;" onclick="setProjStatus(this)">' 	 	 	 		    	    	
					html+='  <div style="background-color: '+result[i].lov_value+';height: 4px;margin-top: 8px;"></div>'
					html+='</div>'
				}
			}
			html+='</div>'
			
			$('#statusColorcode').append(html);
		}
	});
  }

  function setProjStatus(obj){
	var status_id = $(obj).attr('status_id');
	var status_code = $(obj).attr('status_code');
	$('#statusColorcodediv, .statusDiv').css('background-color',status_code);
	$('#commentStatusValue').val(status_id);
  }

  function pendingUsersList(wsprojectID){
	let jsonbody = {
		"project_id":wsprojectID
	}

	$.ajax({
		url: apiPath + "/" + myk + "/v1/getAllApproveUsers",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
		  checkError(jqXHR, textStatus, errorThrown);
		  
		},
		success: function (result) { 
			if(result.length>0){
				$('#pendingContentDiv').append(pendingUsersUI(result,wsprojectID));
				var pName=$('#projName').val();
				for(var i=0;i<result.length;i++){
					$('#pendingTextarea_'+result[i].user_id).text('Your request to subscribe to workspace '+pName+' has been approved. if you need further assistance, contact the project owners');
				}
			}else{
				$('#pendingContentDiv').append("<div class='p-3' style='color:#999999;font-size:12px;'>No pending subscription for this project</div>");
			}
			
		}
	});
  }

  function pendingUsersUI(result,wsprojectID){
	  var ui = "";
	  for(var i=0;i<result.length;i++){
		ui+='<div class="m-0">'
				+'<div id="teamUser_'+result[i].user_id+'" class="" style="border-bottom:1px solid #bfbaba;">'

					+'<div id="openPending_'+result[i].user_id+'"  class="d-flex py-1" style="border-bottom:1px solid #bfbaba;background-color: #e9f6f9;">'
						+'<div class="w-25">'
							+'<div id="teamUserrr_'+result[i].user_id+'" class="d-flex" >'
								+'<div class="px-2 py-2" style=""><img id="" src="'+result[i].userImage+'" onerror=\"imageOnProjNotErrorReplace(this);\" class="border rounded-circle" style="width: 25px;height: 25px;"></div>'
								+'<div class="px-2 py-2 pb-0 defaultExceedCls" style="font-size: 14px;">'+result[i].name+'</div>'
							+'</div>'
							+"<div class='px-2' style=''><select name=\"role\" class='selectOp' id='newuserrole_"+result[i].user_id+"' style='font-size: 12px;margin-top: 3px;'>"
									+"<option id='' value=\"PO\">Owner</option>"
									+"<option id='' value=\"TM\">Team Member</option>"
									+"<option id='' value=\"OB\">Observer</option>"
							+"</select></div>"
							+'<div class="px-2">'
								+"<span style=\"font-size: 12px;\">Deny</span><img id=\"apprDeny\" src=\"images/workspace/approve.svg\" class=\"mx-2\" onclick=\"approvedeny("+result[i].user_id+",'"+result[i].name+"');\"  value=\"approved\" style=\"cursor:pointer;width:30px;height: 15px;\"><span style=\"font-size: 12px;\">Approve</span>"
							+'</div>'
						+'</div>'	
						+'<div class="w-75 pr-1">'
							+'<div class="">'
								+'<textarea id="pendingTextarea_'+result[i].user_id+'" class="my-1 rounded" style="width:100%;font-size: 12px;" rows="2" id=""></textarea>'
								+"<img class=\"ml-auto\" src=\"/images/conversation/post.svg\" title=\"Post\" onclick=\"approveordenyuser("+wsprojectID+",'"+result[i].emailAddress+"','"+result[i].name+"',"+result[i].user_id+");\" style=\"width:4%;cursor:pointer;float: right;\">"
							+'</div>'
					
					
						+'</div>'
					+'</div>'

					+'<div id="defaultPending_'+result[i].user_id+'" class="d-flex">'	//onclick="openPendingReq('+result[i].user_id+')"
						+'<div class="px-2 py-2" style="width: 5%;"><img id="" src="'+result[i].userImage+'" onerror=\"imageOnProjNotErrorReplace(this);\" class="border rounded-circle" style="width: 25px;height: 25px;"></div>'
						+'<div class="px-2 py-2 pb-0 defaultExceedCls" style="width: 70%;font-size: 14px;" emailaddress="'+result[i].emailAddress+'">'+result[i].name+'</div>'
					+'</div>'		
				+'</div>'
			+'</div>'
	  }	
	return ui; 

  }

  function openPendingReq(id){
	$('#defaultPending_'+id).removeClass('d-flex').addClass('d-none');
	$('#openPending_'+id).removeClass('d-none').addClass('d-flex');
  }

  function approvedeny(uId,uName){
	var pName=$('#projName').val();
	if($('#apprDeny').attr('value') == 'deny'){
		$('#apprDeny').attr('src','images/workspace/approve.svg').attr('value','approved'); 
		$('#pendingTextarea_'+uId).text('Your request to subscribe to workspace '+pName+' has been approved. if you need further assistance, contact the project owners');
	  }else{
		$('#apprDeny').attr('src','images/workspace/deny.svg').attr('value','deny');
	 	$('#pendingTextarea_'+uId).text('Your request to subscribe to workspace '+pName+' has been denied. if you need further assistance, contact the project owners');
	  }
  }

  function approveordenyuser(wsprojectID,email,name,appUserid){
	
	var role = $('#newuserrole_'+appUserid).val();
	var message = $('#pendingTextarea_'+appUserid).val();
	var pjName = $('#projName').val();
	var approveordeny = $('#apprDeny').attr('value');
	$('#loadingBar').addClass('d-flex').removeClass('d-none');
	var jsonbody = { 
		"project_id" : wsprojectID,
		"user_id" : appUserid,
		"emailAddress" : email,
		"NAME" : name,
		"type" : role,
		"projName" : pjName,
		"message" : message,
		"companyId" : companyIdglb,
		"luserId" : userIdglb,
		"status":approveordeny,
		"reqHost" : "https://testbeta.colabus.com"
	}
	console.log("jsonbody-->"+JSON.stringify(jsonbody));
    $.ajax({
	  url: apiPath+"/"+myk+"/v1/saveApproveUserList",
	  type:"POST",
	  ///dataType:'json',
	  contentType:"application/json",
	  data: JSON.stringify(jsonbody),
	  error: function(jqXHR, textStatus, errorThrown) {
		checkError(jqXHR,textStatus,errorThrown);
		$('#loadingBar').addClass('d-none').removeClass('d-flex');
		
		},
	  success : function(result) {
		$('#teamUser_'+appUserid).remove();
		if(result == "success"){
			//$("#teamContentDiv ul").html('');
			//$("#userList ul").html(''); 
			getTheProjectUsers(wsprojectID);

		}else{
			alertFun("User request denied", 'warning');
		}
		$('#loadingBar').addClass('d-none').removeClass('d-flex');
	  }
	});  
  
  }

  function getTheProjectUsers(wsprojectID){
	$("#teamContentDiv ul").html('');
	$("#userList ul").html('');   
	let jsonbody1={
		"project_id":wsprojectID
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getTheProjectUsers",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody1),
		error: function (jqXHR, textStatus, errorThrown) {
		checkError(jqXHR, textStatus, errorThrown);
		
		},
		success: function (result) { 
			
			$('#userList ul').append(listUsers(result,wsprojectID));
			$('#inviteUsers').val('');
			$('#teamContentDiv ul').append(listUsers(result,wsprojectID,"advancePage"));
			
			for(var i=0;i<result.length;i++){
				var id = result[i].user_id;
				var role = result[i].userType;
				$("#role_"+id+" option[value="+role+"]").attr('selected','selected');  
			}
		}
	});

  }

  function listUsers(result,wsprojectID,advancePage){
	var UI ="";
	for(var i=0;i<result.length;i++){
		var id = result[i].user_id;
		var name = result[i].name;
		var email = result[i].emailAddress;
		var role = result[i].userType;
		var img = lighttpdpath+"/userimages/"+id+"."+result[i].user_image_type+"?"+d.getTime();

		UI  += "<li id='user_"+id+"' updateStatus='' class='prjUserClsNew userStat_"+id+"'  onmouseover='addBgcolor("+id+");' onmouseout='removeBgcolor("+id+");' userrole='"+role+"' email='"+email+"' name='"+name+"' style='display: flex;justify-content: space-between;height:43px !important;width: 100%;border-bottom: 1px solid #cdc4c4;background-color: #fff;border-top-left-radius: 10px;border-top-right-radius: 10px;color: #464242;' >"
		   +"<div class=\"d-flex pt-1\" style='text-overflow: ellipsis;overflow: hidden;white-space: nowrap;'><img id=\"personimagedisplay\"  src=\""+img+"\" onerror=\"imageOnProjNotErrorReplace(this);\" class=\"mr-3 rounded-circle profImg\" style=\"\"><span class='profNam ml-1 pt-1' style='font-size: 12px;'>"+name+"</span></div>"
		   +"<div class=\"pt-1\" id=\"teamroleSection\" style='display: flex;justify-content: space-between;'>"
					+"<div class=\"dropDow\" style=\"\"><select name=\"role\" class=\"selectOp\" id=\"role_"+id+"\" onclick=\"checkOwnerStatus("+id+",'"+advancePage+"');\" onchange=\"changeInvStatus("+id+")\" style='font-size: 12px;margin-top: 3px;color: #5e5b5b;'>"
					  +"<option id='teamow_' value=\"PO\">Owner</option>"
					  +"<option id='teamtm_' value=\"TM\">Team Member</option>"
					  +"<option id='teamob_' value=\"OB\">Observer</option>"
					+"</select></div>"
					+"<div class=\"closeOp\" style=\"width: auto;\"><img onclick=\"removeUserUpdate("+id+","+wsprojectID+",'"+role+"','"+advancePage+"')\" class=\"closeAp\" src=\"images/task/minus.svg\" style=\"width: 15px; height: 15px; cursor: pointer;\"></div>"
			  +"</div>"
		   +"</li>";

		
	
	}
	return UI;
  }

  function checkOwnerStatus(id,advancePage){
	var count=0;
	var uId="";
	var value="";
	if(advancePage == "advancePage"){
		$('#teamContentDiv li').each(function(){

			uId = $(this).attr('id').split('_')[1];
			if(uId != id){
			  value = $(this).find('.dropDow').find('.selectOp').val();
			  if(value == "PO"){
				count++;
			  }
			}
			
		});
	}else{
		$('#userList li').each(function(){

			uId = $(this).attr('id').split('_')[1];
			if(uId != id){
			  value = $(this).find('.dropDow').find('.selectOp').val();
			  if(value == "PO"){
				count++;
			  }
			}
			
		});
	}
	
	if(count == 0){
		alertFun("There must be one project admin for a project", 'warning');
	}
  }

  function changeInvStatus(id){
	
	$('.userStat_'+id).attr('updateStatus','update');
  
  }

  function sentimentScoreforProject(proId){
	var avgSentimentScore="";
	var localOffsetTime=getTimeOffset(new Date());
	
	let jsonbody={
		"companyId" : companyIdglb,
		"reportType" : "project",
		"projectId" : proId,
		"fType" : "MAX",
		"score" : "",
		"jiraFilterMain" : "",
		"jiraFilterSub" : "",
		"localOffsetTime" : localOffsetTime
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchSentimentAnalysis",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function(jqXHR, textStatus, errorThrown) {
			checkError(jqXHR,textStatus,errorThrown);
		}, 
	    success:function(result){
		    var subArray = [];
		    var subDateArray=[];
		    var bgArray=[];
		    var nArray=[];
		    if(result.length >0){
			    avgSentimentScore = result[0].avgSentimentScore;
			    var jsonData = result[0].projData;
		        for(var i=0; i<jsonData.length; i++){
		        	subDateArray[i]=jsonData[i].createdTime;
		        	nArray[i]=jsonData[i].notifyId;
					if(jsonData[i].sentiment_score == 0 || jsonData[i].sentiment_score == 0.0){
		        		jsonData[i].sentiment_score =  0.005;
		        	}
		        	if(jsonData[i].sentiment_score >=0.25 && jsonData[i].sentiment_score <=1){
		        	    bgArray[i]='#008000';
					    subArray[i]=Math.abs(jsonData[i].sentiment_score);
					}else if(jsonData[i].sentiment_score >=-0.25 && jsonData[i].sentiment_score <=0.25){
						bgArray[i]='#FFC200';
					    subArray[i]=Math.abs(jsonData[i].sentiment_score);
					}else{
					 	bgArray[i]='#ff0000';
					    subArray[i]=Math.abs(jsonData[i].sentiment_score);
					}
				}
				var ssub = [];
				
					        
				creatChartforProjectScore(subArray,ssub,bgArray,proId,nArray,subDateArray);
			}else{
			  	creatChartforProjectScore(subArray,ssub,bgArray,proId,nArray,subDateArray);
			}
		}
	});
}

var $chart;
function creatChartforProjectScore(subArray,ssub,bgArray,proId,nArray,subDateArray){
	if (typeof $chart !== "undefined"){
		$chart.destroy();
	}
	var ctx = document.getElementById('chartCanvas_'+proId).getContext('2d');
	ctx.height = 50;
	chart = new Chart(ctx,
	{
	    // The type of chart we want to create
	    type: 'bar',
		// The data for our dataset
	    data:
	    {
			labels: subDateArray,
			datasets: 
			[{
	            label:nArray ,
	            backgroundColor: bgArray,
	    		data: subArray,
			}]
		},
		options: 
		{
			responsive : true,
       		showTooltips: true,
			maintainAspectRatio: false,
			scales: 
			{
				xAxes: 
					[{
					barPercentage: 0.3,
					gridLines: 
					{
						display:false
					},
					ticks: 
					{
			            stepSize: 1,
			            min: 0,
			            minRotation: 30
					}
					}],
				yAxes: 
					[{
					display: true,
                   	ticks: 
                       	{
						min:0,
						stepValue: 0.1,
						max: 1
						}
					}]
			},
			legend: 
			{
				display: false,
			},
			tooltips: 
			{
				callbacks: 
				{
					label: function(tooltipItem) 
					{
						return tooltipItem.yLabel ;
					}
				}
			}
				
		}
	});
	$chart = chart;
}




/////above all newly added codes by arulprakash

  function  prepareChartUi(json){
	var status_comment="";
	var subArray = [];
	var ssub = [];
	var subDateArray = [];
	var backgroundCol = [];
	var nArray=[];
	var projectId = $('#settingProjectId').val();
	var taskCmtUI = "";
	var temp= 0;
	var j = 0;
	var dot = "..."
	for(var i = json.length-1; i >= 0 ;i-- ){
		subArray[j] = '1';
		temp = j;
		ssub[j]= temp;
		subDateArray[j] = json[i].barGraphDate;
		backgroundCol[j] = json[i].colour_name;
		if(json[i].status_comment != "" && json[i].status_comment != null){
			status_comment = jsonData[i].status_comment;
			status_comment = status_comment.replaceAll("CHR(50)","'").replaceAll("CH(52)","'").replaceAll("CHR(26)",":").replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(40)","\"").replaceAll("CH(70)","\\");
		}
		if(status_comment.length<20){
			dot = ""
		}else{
			dot = "..."
		}
		nArray[j]=json[i].updated_by +" : "+status_comment.substring(0, 20)+dot;
		j++;
	}
	var maxValue ="";
	if(subDateArray.length <= 20){
		maxValue = 50;
	}else if(subDateArray.length <= 100 && subDateArray.length > 20){
		maxValue =50;
	}else {
		maxValue =100;
	}
	creatChartStatus(subArray,ssub,backgroundCol,projectId,nArray,subDateArray,maxValue);
 }

 //sentiment graph in the worksapce for the project and users
 var $charts;
 function creatChartStatus(subArray,ssub,bgArray,proId,nArray,subDateArray,maxValue){
	if (typeof $charts !== "undefined"){
	 $charts.destroy();
	}
	$('.chartcontain').css({'display':'none'});
	$('#chart-container_'+proId).css({'display':'block'});
	var ctx = document.getElementById('myChart_'+proId).getContext('2d');
	ctx.height = 100;
	chart = new Chart(ctx,
	{
	 type: 'bar',
	 data:{
			 labels: subDateArray,
			 datasets: 
			 [{
				 label:nArray ,
				 backgroundColor: "white",
				 data: ssub,
			 },
			 {
				 label:nArray ,
				 backgroundColor: bgArray,
				 data: subArray,
			 },
			 ]
		 },
	 options:{
			 scaleShowLabels: false,
			 responsive : true,
	         showTooltips: true,
			 onClick:click,
			 maintainAspectRatio: false,
			 
	 scales:{
	 		xAxes:[{
					stacked: true,
					barPercentage: 1.0,
					categoryPercentage: 1.0,
					gridLines:{
					 display:false
					},
					ticks:{
					 maxRotation: 90,
					 minRotation: 90
					}
			}],
			yAxes:[{
					stacked: true,
					display: true,
					gridLines:{
					 display:false
					},
					ticks:{
					 display:false,
					 min:0,
					 stepValue: 1,
					 max:maxValue,
					}
			}]
	 },
	 legend:{
	  display: false,
	 },
	 tooltips:{
	  callbacks:{
				label: function(tooltipItem,label){
				 return label.datasets[tooltipItem.datasetIndex].label[tooltipItem.index] ;
				}
				}
	 }
			 
	}
 });
$charts = chart;
}

  


  function loadTabData(menuType){
 		searchVal = '';
		searchType = '';  
		sortType='';
		filterType='';
  		$("img#goBackToMainScreen").hide();
  		var check = $('img#hiddenProject').attr("onclick");
   		if( check == 'showActiveProjects()'){
  			$("img#hiddenProject").attr('src','images/hidden.png');
			$('img#hiddenProject').attr('title','show hidden projects');
			$("img#hiddenProject").attr("onclick","showHiddenProjects()");
  		}
  		globalProjectId="";
  		searchProjType = 'visible';
  		if($('#createWorkflowTempDivNew').css('display') == 'block'){
  			$("#createWorkflowTempDivNew").hide();
  		}
  		
  		$('#myzoneBreadcrum').children('span.breadCrumProjName').remove();
 		$('#myzoneBreadcrum').children('img.breadcrumImg').remove();
  		$("#sortbysentiment").css({'display':'block'});
       if(menuType == 'project'){
      	   $("#wsSearchBox").val("");
	 	   $('#sortWrkspace').val("sort");
	       $('#filterWrkspace').val("myWrkVisible");
           loadMyProjectsJson();
           $("#projTab").addClass('tabActive');
           $("#projTab").removeClass('tabInactive');
          // $('#projTab').css({'width':'9%'});
           
           $("#publicProjTab").removeClass('tabActive');
           $("#publicProjTab").addClass('tabInactive');
           
           $('#myProInfo').css({'display':'block'});
           $('#subscribeProInfo').css({'display':'none'});
           $("#searchProject").show();
           $("#addProject").show();
           $("#csvUpload").show();
           $("#hiddenProject").show();
       	   $('#tzsub').css({'display':'block'});
           $('#recentlyused').css({'display':'block'});
           $('#workspaceSearchDiv').css({'height':'95px'});
           
        } else if(menuType == 'publicproject') {
           showAvailableProjJson();
           $("#sortbysentiment").css({'display':'none'});
           $("#publicProjTab").addClass('tabActive');
           $("#publicProjTab").removeClass('tabInactive');
           // $('#projTab').css({'width':'9%'});
           $("#projTab").removeClass('tabActive');
           $("#projTab").addClass('tabInactive');
           
           $('#myProInfo').css({'display':'none'});
           $("div#projectSettingsContainerDiv").hide();
           $('#subscribeProInfo').css({'display':'block'});
           
           $("#searchProject").show();
           $("#addProject").hide();
           $("#csvUpload").hide();
           $("#hiddenProject").hide();
           $('#tzsub').css({'display':'none'});
           $('#recentlyused').css({'display':'none'});
           $('#workspaceSearchDiv').css({'height':'68px'});
       }
  }
  
  	$(function(){
		  loadMyprojectJsonNew();
		  showAvailableProjJsonNew();
	});

	function filterWorkspace(){
		var taskFilterVal = $('#exsitFilter').find("option:selected").attr('value');
		var taskSortType = $('#exsitSort').find("option:selected").attr('value');
		var taskSearchWord = $("#exsitsSearch").val();
		var type = "";
		if(taskSearchWord.length != 0){
			type="search";
		}
		// var selectedVal = $(obj).val();
		$("div#work").html("");
		loadMyprojectJsonNew(taskSortType,taskFilterVal,taskSearchWord,type);
	}

	function subscribeFilterWork(){
		var taskFilterVal1 = $('#subscribeFilter').find("option:selected").attr('value');
		var taskSearchWord = $("#subscribeSearch").val();
		var type = "";
		if(taskSearchWord.length != 0){
			type="search";
		}
		$("div#subscribe").html("");
		showAvailableProjJsonNew(taskFilterVal1,taskSearchWord,type);
	}

	function clearsearchWork(){
		$('#exsitsSearch').val("");
		filterWorkspace();
	}

	function clearsearchSubscribeWork(){
		$('#subscribeSearch').val("");
		subscribeFilterWork();
	}

	function loadMyprojectJsonNew(taskSortType,taskFilterVal,taskSearchWord,type){
		$('#loadingBar').removeClass('d-none').addClass('d-flex');
		$("div#work").html('');
		
		let jsonbody = { 
			"user_id" : userIdglb,
		//     "ipadVal" : ipadVal,
		    "sortType" : taskFilterVal,
		    "type" : type,
		    "txt" : taskSearchWord,
		    "companyId" : companyIdglb,
		    "workspaceType" : "existing",
		//     "sortHideProjValue" : "",
		    "filterType" : taskSortType
		  }
		  checksession();
		  $.ajax({
			url: apiPath+"/"+myk+"/v1/projectList", //// http://localhost:8080/v1/projectList  apiPath+"/"+myk+"/v1/loadProjDDList
			type:"POST",
			dataType:'json',
			contentType:"application/json",
			data: JSON.stringify(jsonbody),
			error: function(jqXHR, textStatus, errorThrown) {
				checkError(jqXHR,textStatus,errorThrown);
				$('#loadingBar').removeClass('d-flex').addClass('d-none');
				timerControl("");
				},
			success : function(result){
				jsonData = result;
				// console.log("Result for projects::::"+JSON.stringify(result));
				result= prepareUI();
				$("div#work").append(result);
				observer.observe();//---- to lazy load user images

				if(typeof(newPrjId) != "undefined" || newPrjId != null || newPrjId == ''){
					var target = $('#projectImageId_'+newPrjId);
					setTimeout(function(){
						target[0].scrollIntoView();
					}, 500);
					$('#projectImageId_'+newPrjId).css('border','2px solid #3399FF');
					if(typeof(projActionType) != 'undefined' && projActionType != null  && projActionType != ""){
						$("#projSetting_"+newPrjId).trigger('click');
					}
					newPrjId="";
				}
				$('#loadingBar').removeClass('d-flex').addClass('d-none');
			}
		  });

	}
 
  function loadMyProjectsJson(){
	  
  	  searchVal = $("#wsSearchBox").val().toLowerCase();
	  sortType = $('#sortWrkspace').val();
	  filterType=$('#filterWrkspace').val();
	  alert("look1 :"+sortType);
	  //console.log("fn loadMyProjectsJson"+filterType+" searchVal "+searchVal+" sortType "+sortType);
	  
	  $("div#projectSettingsContainerDiv").hide();
      $("#loadingBar").show();
      timerControl("start");
      $("div#tabContentDiv").html("");
	  index=0;
	  limit=20;
	  type="";
	 var isiPad = navigator.userAgent.match(/iPad/i) != null;
	  if (isiPad) {
		 ipadVal = "YES";
	  } else {
		ipadVal = "NO";
	  }	
	  let jsonbody = { 
		"user_id" : userIdglb,
	//     "ipadVal" : ipadVal,
	    "sortType" : sortType,
	    "type" : searchType,
	    "txt" : searchVal,
	    "companyId" : companyIdglb,
	    "workspaceType" : "existing",
	//     "sortHideProjValue" : "",
	    "filterType" : filterType
	  }
	  checksession();
	  $.ajax({
		url: apiPath+"/"+myk+"/v1/projectList", //// http://localhost:8080/v1/projectList  apiPath+"/"+myk+"/v1/loadProjDDList
		type:"POST",
		dataType:'json',
		contentType:"application/json",
		data: JSON.stringify(jsonbody),
		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		success : function(result){
			
			jsonData = result;
			console.log("Result for projects::::"+JSON.stringify(result));
            $("div#tabContentDiv").show();
			
            if(sortType == "sentimentscore"){
          		//$('#workspaceSearchDiv').css({'display':'none'});
	            $('#tabContentDiv').css({'margin-top':'-1%'});
	        	var defImage = lighttpdpath + "/projectimages/dummyLogo.png";
	        	var UI ="";
	        	var team ="";
	        	var teamUI="";
	        	 var projStatus ="";
	        	 for(var i = 0; i < jsonData.length;  i++){
	        	  teamUI ="";
	        	    team = jsonData[i].teamMembers;
	        	   projStatus=jsonData[i].projStatus;
	        	 	UI += '	<div class="row projImgMainDiv_'+jsonData[i].projectID+'"  style="background:white;height:18vh;margin-left:1vw;margin-right:1vw;border-bottom: 1px solid #ccc;"> '
       	        	+'<div class="col-lg-1 col-md-1 col-sm-1 col-xs-6" id="projImgMainDiv_'+jsonData[i].projectID+'"  onclick =viewSentimentGraph('+jsonData[i].projectID+',"","project",'+jsonData[i].sentimentScore+') style="cursor:pointer;height:18vh;padding-top:1vh;padding-left:2vw"> ' 
	       	        	
	       	        	+'<div class="pStyle row" id="proj_'+jsonData[i].projectID+'">'
	       	        	+'<div id="sysScore_'+jsonData[i].projectID+'" style="color:white;font-size:9px;font-weight:bold;text-align:center;background:#FFC200;height:19px;width:19px;margin-top:1.4vh;margin-left:4.4vw;position:absolute;border-radius:2em"></div>'
	       	        	+'<img onerror="imageOnProjErrorReplace(this)" src="'+jsonData[i].imageUrl+'" style="width:60px;height:60px;border-radius:6px;margin-left:11%;margin-top:3vh;margin-bottom:1%;border: 1px solid rgb(191, 191, 191);"  ></img>'
	       	        	+'<div class="defaultExceedCls" style="width:62px;text-align:center;font-family: OpenSansRegular;font-size: 11px;font-weight:bold;margin-left:10%" id="projName_'+jsonData[i].projectID+'">'+jsonData[i].ProjTitle+'</div>'
	       	        	
	       	       		+'</div>'
	       	        +'</div>'
       	        	+'<div class="col-lg-11 col-md-11 col-sm-11 col-xs-6"  style="height:18vh;"> ' 
	       	        	+'<div id="b_'+jsonData[i].projectID+'" class="row" style="float: left;width: 100%;height: 15vh;margin-top:1%;margin-left: -1.2vw;border-right: 1px solid #ccc;">'
		       	        	+'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:15vh;margin-left:-0.1%">'
			       	        	+'<ul style="height:100%;margin-left:0.1vw" id="teamMember_'+jsonData[i].projectID+'" class="bxslider" >'
			       	        	+ prepareUIforteam(team,projStatus)
			       	        	+'</ul>'
		       	        	+'</div>'
	       	        	+'</div>'
       	        	+'</div>'
       	        	//+'<div class="col-md-2 col-sm-2" id="chartContainer_'+jsonData[i].projectID+'" style="height:100%;float:right;position:initial"> ' 
       	        	//+'<canvas id="myChart3" class="myChart"></canvas>'
       	        	//+'<div class="createBtn cButton" style="margin-top:1%;margin-right:42%; color: #ffffff;font-family: opensanscondbold;font-size: 10px;padding: 4px 18px !important;"  >MORE</div>'
       	        	//+'</div>'
       	        
       	       	+'</div> ';
       	       	
       	       	UI+='<div class="row chartcontain"  id="chart-container_'+jsonData[i].projectID+'" style="background:white;height:54vh;margin-left:1%;margin-right:1%;border-bottom: 1px solid #ccc;display:none;z-index:100">'
	       	       	    +'<div id ="avgScore_'+jsonData[i].projectID+'" class="row " style="padding-top:1%;">'
	       	       	    	+'<div class="col-md-3">'
	       	       	    	+'</div>'
	       	       	    	+'<div class="col-md-2 " style="margin-left:-2%;height: 5vh;padding-top: 1vh;text-align-last: end;">Average Score : '
	       	       	    	+'</div>'
	       	       	    	+'<div class="col-md-1" id="score_'+jsonData[i].projectID+'" style="height: 5vh;color:white;font-size: 11px;text-align: end;border-radius: 17px;width: 5vh;padding-left: 1.2vh;padding-top: 1.2vh; " class="col-md-1">'
	       	       	    	+'</div>'
	       	       	    +'</div>'
	       	       	    +'<div class="row" style="height:46vh">'
	       	       	    	+'<div class="col-md-8 viewBorder"  style="height:46vh">'
			       	       	    +'<div id="chart_'+jsonData[i].projectID+'" class="col-md-12"  style="padding-top:2%;position: relative; height:45vh">'
			       	       	    +'<canvas id="myChart_'+jsonData[i].projectID+'" ></canvas>'
			       	       	    +'</div>'
		       	       	    +'</div>'
		       	       	    +'<div  class="col-md-4 viewPef" style="height:21vh">'
		       	       	    	+'<div class ="row " style="display:block;height:1vh;width:60vh;margin-bottom:4%">'
		       	       	    		+'<div class="col-md-12" >'
		       	       	    			+'<div id="head_'+jsonData[i].projectID+'" style="height:1%;font-size: 15px;font-family: opensanscondbold;">Overall Project Performance :</div>'
		       	       	    		+'</div>'
		       	       	    		
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row projectP" style="display:block;height:20vh;width:50vh;margin-bottom:4%">'
		       	       	    		+'<canvas  id="myChartp_'+jsonData[i].projectID+'" ></canvas>'
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row userP" style="display:none">'
		       	       	    		+'<div class="col-md-7">'
		       	       	    			+'<div id="TotalTask_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" >Total Tasks </div>'
		       	       	    			+'<div id ="TaskCompleted_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" >Tasks Completed </div>'
		       	       	    			+'</div>'
		       	       	    		+'<div class="col-md-5">'
		       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;" >: <span id="totalTask_'+jsonData[i].projectID+'"></sapn></div>'
		       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;" >: <span id ="taskCompleted_'+jsonData[i].projectID+'"></sapn></div>'
		       	       	    			+'</div>'
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row taskU" style="display:none">'
		       	       	    		+'<div class="col-md-7">'
		       	       	    			+'<div id="scHead_'+jsonData[i].projectID+'" style="font-size: 15px;font-family: opensanscondbold;" >Index (1 to 10):</div>'
		       	       	    			+'<div id="TaskBeyond_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" >Tasks Beyond End Date </div>'
		       	       	    			+'<div id="TaskNotComp_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;">Tasks Pending   </div>'
		       	       	    		+'</div>'
		       	       	    		+'<div class="col-md-5 taskU" style="display:none">'
		       	       	    			+'<div id="ScHead_'+jsonData[i].projectID+'"></br></div>'
		       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;" >: <span  id="taskBeyond_'+jsonData[i].projectID+'"></sapn></div>'
		       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;"> : <span id="taskNotComp_'+jsonData[i].projectID+'"></sapn></div>'
		       	       	    		+'</div>'
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row " style="height:23vh;width:60Vh">'
		       	       	    		+'<div class="col-md-12 view">'
		       	       	    		+'</div>'
		       	       	    	+'</div>'
		       	       	    +'</div>'
	       	       	    +'</div>'
       	       	    +'</div>';
       	       	//var team = jsonData[i].teamMembers;
       	       
				// teamUI = prepareUIforteam(team);
				 //alert(teamUI);
				 //$("#teamMember_"+jsonData[i].projectID+"").append(teamUI);
				
       	    } 
	       		$("div#tabContentDiv").append(UI);	 
	        	$('.bxslider').bxSlider({minSlides: 10,maxSlides: 11,slideWidth: 66,slideMargin: 5,infiniteLoop: false});
	        	var team="";
	        	for(var k = 0; k < jsonData.length;  k++){
	        	team ="";
	        			if(jsonData[k].sentimentScore >=0.25 && jsonData[k].sentimentScore <=1){
			        	   $('#sysScore_'+jsonData[k].projectID).css({'background-color':'#008000'});
			        	}else if(jsonData[k].sentimentScore >=-0.25 && jsonData[k].sentimentScore <0.25){
			        		$('#sysScore_'+jsonData[k].projectID).css({'background-color':'#FFC200'});
			        	}else{
			        	$('#sysScore_'+jsonData[k].projectID).css({'background-color':'#ff0000'});
			        	}
			         team = jsonData[k].teamMembers;
			         for(var m = 0; m < team.length;  m++){
			         if(team[m].sentimentscore >=0.25 && team[m].sentimentscore <=1){
			        	   $('#sysScore_'+team[m].projectid+"_"+team[m].userId).css({'background-color':'#008000'});
			        	}else if(team[m].sentimentscore >=-0.25 && team[m].sentimentscore <0.25){
			        		$('#sysScore_'+team[m].projectid+"_"+team[m].userId).css({'background-color':'#FFC200'});
			        	}else{
			        	$('#sysScore_'+team[m].projectid+"_"+team[m].userId).css({'background-color':'#ff0000'});
			        	}
			         }
	        	 }
	        	 
	        	
	        	
				//$('#myChart1').css({'height':'65px','width':'100px','margin-top':'2%'});
				//$('#myChart2').css({'height':'92px','width':'340px','margin-top':'2%'});
				//$('#myChart3').css({'height':'92px','width':'340px','margin-top':'2%'});
				//$('#myChart1b').css({'margin-top':'15%'});
				//$('.myChart').css({'height':'92px','width':'340px','margin-top':'2%'});
				$('.cButton').css({'margin-top':'-30%'});
				$('.bx-wrapper').css({'max-width':'100%','height':'100%','box-shadow':'none'});
				$('.bx-viewport').css({'width':'auto','margin-left':'8px'});
				$('.bx-pager').css({'margin-left': '10px','margin-right':'10px','display':'none'});
				$('.bxslider').css({'margin-left':'0.1vw'});
				//$('.bx-prev').css({'margin-left':'-3.8%','margin-top':'-0.8%','z-index':'100','opacity': '0.4'});
				//$('.bx-next').css({'margin-right':'-6.4%','margin-top':'-0.8%','z-index':'100','opacity': '0.4'});
				//$(".bx-prev").css("background":'url(path+"/images/leftSliderArrow.jpg")');
				//$('.bx-prev').text('').css("float","left").append("<div style=\"border-right: 1px solid #aaa7a7;margin-left: -2px;margin-top: -117px;width: 27px;height: 107px;\"><img  src=\""+path+"/images/leftSliderArrow.jpg\" style=\"opacity: 0.8;margin-left: 4px;margin-top: 45px;width: 18px;height: 25px;\"> </div>" );
				// $('.bx-next').text('').css("background","url('images/arrowright.png') no-repeat");
				// $('.bx-prev').text('').css("background","url('images/arrowleft1.png') no-repeat");
				//$('#myChart1').css({'height':'105px','width':'340px','margin-top':'2%'});
				$('.bx-prev').text('').css("float","left").append("<div style=\"margin-left: -30px;margin-top: -126px;width: 27px;height: 15vh;border-right: 1px solid #ccc;\"><img  src=\"images/leftSliderArrow.jpg\" style=\"opacity: 0.7;margin-left: 5px;margin-top: 34px;width: 18px;height: 25px;\"> </div>" );
			    $('.bx-next').text('').css("float","right").append("<div style=\"margin-top: -110px;width: 27px;height: 61px;margin-right:-28px\"><img  src=\"images/rightSliderArrow.jpg\" style=\"opacity: 0.7;margin-left: 20px;margin-top: 17px;width: 18px;height: 25px;\"> </div>" );
			    
	        }else if(sortType == "status"){
	        
          		//$('#workspaceSearchDiv').css({'display':'none'});
	            $('#tabContentDiv').css({'margin-top':'-1%'});
	        	var defImage = lighttpdpath + "/projectimages/dummyLogo.png";
	        	var UI ="";
	        	var team ="";
	        	var teamUI="";
	        	var projStatus ="";
	        	 for(var i = 0; i < jsonData.length;  i++){
	        	  teamUI ="";
	        	    team = jsonData[i].teamMembers;
	        	    projStatus=jsonData[i].projStatus;
	        	 	UI += '	<div class="row projImgMainDiv_'+jsonData[i].projectID+'"  style="background:white;height:18vh;margin-right:1vw;margin-left:1vw;border-bottom: 1px solid #ccc;"> '
       	        	+'<div class="col-lg-1 col-md-1 col-sm-1 col-xs-6" id="projImgMainDiv_'+jsonData[i].projectID+'"  onclick =viewStatusGraph('+jsonData[i].projectID+',"","project","status") style="cursor:pointer;height:18vh;padding-top:1vh;padding-left:2vw"> ' 
	       	        	
	       	        	+'<div class="pStyle row" id="proj_'+jsonData[i].projectID+'">'
	       	        	//+'<div id="sysScore_'+jsonData[i].projectID+'" style="color:white;font-size:9px;font-weight:bold;text-align:center;height:19px;width:19px;margin-top:1.4vh;margin-left:4.4vw;position:absolute;border-radius:2em"></div>'
	       	        	+'<img onerror="imageOnProjErrorReplace(this)" src="'+jsonData[i].imageUrl+'" style="width:60px;height:60px;margin-left:11%;margin-top:3vh;margin-bottom:1%;border: 1px solid rgb(191, 191, 191);"  ></img>'
	       	        	+'<div id="sysScore_'+jsonData[i].projectID+'"  style="float: left;width: 60px;margin-top: -1%;margin-left:11%;height: 5px;clear: both;" ></div>'
	       	        	+'<div class="defaultExceedCls" style="width:62px;text-align:center;font-family: OpenSansRegular;font-size: 11px;font-weight:bold;margin-left:10%" id="projName_'+jsonData[i].projectID+'">'+jsonData[i].ProjTitle+'</div>'
	       	       		+'</div>'
	       	        +'</div>'
       	        	+'<div class="col-lg-11 col-md-11 col-sm-11 col-xs-6"  style="height:18vh;"> ' 
	       	        	+'<div id="b_'+jsonData[i].projectID+'" class="row" style="float: left;width: 100%;height: 15vh;margin-top:1%;margin-left: -12px;border-right: 1px solid #ccc;">'
		       	        	+'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:15vh;margin-left:-2px">'
			       	        	+'<ul style="height:100%;margin-left:0.1vw" id="teamMember_'+jsonData[i].projectID+'" class="bxslider" >'
			       	        	+ prepareUIforStatusteam(team,projStatus)
			       	        	+'</ul>'
		       	        	+'</div>'
	       	        	+'</div>'
       	        	+'</div>'
       	        	//+'<div class="col-md-2 col-sm-2" id="chartContainer_'+jsonData[i].projectID+'" style="height:100%;float:right;position:initial"> ' 
       	        	//+'<canvas id="myChart3" class="myChart"></canvas>'
       	        	//+'<div class="createBtn cButton" style="margin-top:1%;margin-right:42%; color: #ffffff;font-family: opensanscondbold;font-size: 10px;padding: 4px 18px !important;"  >MORE</div>'
       	        	//+'</div>'
       	        
       	       	+'</div> ';
       	       	
       	       	UI+='<div class="row chartcontain"  id="chart-container_'+jsonData[i].projectID+'" style="background:white;height:54vh;margin-left:1%;margin-right:1%;border-bottom: 1px solid #ccc;display:none;z-index:100">'
	       	       	    
	       	       	    +'<div class="row" style="height:46vh">'
	       	       	    	+'<div class="col-md-8 viewBorder"  style="height:50vh;margin-top:1%">'
			       	       	    +'<div id="chart_'+jsonData[i].projectID+'" class="col-md-12"  style="padding-top:2%;position: relative; height:50vh">'
			       	       	    +'<canvas id="myChart_'+jsonData[i].projectID+'" ></canvas>'
			       	       	    +'</div>'
		       	       	    +'</div>'
		       	       	    +'<div  class="col-md-4 viewPef" style="height:21vh;margin-top:2%">'
		       	       	    	+'<div class ="row " style="display:block;height:1vh;width:60vh;margin-bottom:4%">'
		       	       	    		+'<div class="col-md-12" >'
		       	       	    			+'<div id="head_'+jsonData[i].projectID+'" style="height:1%;font-size: 15px;font-family: opensanscondbold;">Overall Project Performance :</div>'
		       	       	    		+'</div>'
		       	       	    	
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row projectP" style="display:block;height:20vh;width:50vh;margin-bottom:4%">'
		       	       	    		+'<canvas  id="myChartp_'+jsonData[i].projectID+'" ></canvas>'
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row userP" style="display:none">'
		       	       	    		+'<div class="col-md-7">'
		       	       	    			+'<div id="TotalTask_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" >Total Tasks </div>'
		       	       	    			+'<div id ="TaskCompleted_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" >Tasks Completed </div>'
		       	       	    			+'</div>'
		       	       	    		+'<div class="col-md-5">'
		       	       	    			+'<div id="totalTask_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" ></div>'
		       	       	    			+'<div id ="taskCompleted_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" ></div>'
		       	       	    			+'</div>'
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row taskU" style="display:none">'
		       	       	    		+'<div class="col-md-7">'
		       	       	    			+'<div id="TaskBeyond_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" >Tasks Beyond End Date </div>'
		       	       	    			+'<div id="TaskNotComp_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;">Tasks Pending   </div>'
		       	       	    		+'</div>'
		       	       	    		+'<div class="col-md-5 taskU" style="display:none">'
		       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;" >: <span  id="taskBeyond_'+jsonData[i].projectID+'"></sapn></div>'
		       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;"> : <span id="taskNotComp_'+jsonData[i].projectID+'"></sapn></div>'
		       	       	    		+'</div>'
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row " style="height:23vh;width:60Vh">'
		       	       	    		+'<div class="col-md-12 view">'
		       	       	    		+'</div>'
		       	       	    	+'</div>'
		       	       	    +'</div>'
	       	       	    +'</div>'
       	       	    +'</div>';
       	       	//var team = jsonData[i].teamMembers;
       	       
				// teamUI = prepareUIforteam(team);
				 //alert(teamUI);
				 //$("#teamMember_"+jsonData[i].projectID+"").append(teamUI);
				
       	    } 
	       		$("div#tabContentDiv").append(UI);	 
	        	$('.bxslider').bxSlider({minSlides: 10,maxSlides: 11,slideWidth: 66,slideMargin: 5,infiniteLoop: false});
	        	var team="";
	        	for(var k = 0; k < jsonData.length;  k++){
	        	team ="";
	        			if(jsonData[k].sentimentScore =="99"){
			        	   $('#sysScore_'+jsonData[k].projectID).css({'background-color':'#00783B'});
			        	}else if(jsonData[k].sentimentScore == "100"){
			        		$('#sysScore_'+jsonData[k].projectID).css({'background-color':'#00A652'});
			        	}else if(jsonData[k].sentimentScore == "101"){
			        		$('#sysScore_'+jsonData[k].projectID).css({'background-color':'#00D466'});
			        	}else if(jsonData[k].sentimentScore == "102"){
			        		$('#sysScore_'+jsonData[k].projectID).css({'background-color':'#F1DB14'});
			        	}else if(jsonData[k].sentimentScore == "103"){
			        		$('#sysScore_'+jsonData[k].projectID).css({'background-color':'#FCB040'});
			        	}else{
			        	$('#sysScore_'+jsonData[k].projectID).css({'background-color':'#C52C31'});
			        	}
			     
	        	 }
	        	 
	        	
	        	
				//$('#myChart1').css({'height':'65px','width':'100px','margin-top':'2%'});
				//$('#myChart2').css({'height':'92px','width':'340px','margin-top':'2%'});
				//$('#myChart3').css({'height':'92px','width':'340px','margin-top':'2%'});
				//$('#myChart1b').css({'margin-top':'15%'});
				//$('.myChart').css({'height':'92px','width':'340px','margin-top':'2%'});
				$('.cButton').css({'margin-top':'-30%'});
				$('.bx-wrapper').css({'max-width':'100%','height':'100%','box-shadow':'none'});
				$('.bx-viewport').css({'width':'auto','margin-left':'8px'});
				$('.bx-pager').css({'margin-left': '10px','margin-right':'10px','display':'none'});
				$('.bxslider').css({'margin-left':'0.1vw'});
				//$('.bx-prev').css({'margin-left':'-3.8%','margin-top':'-0.8%','z-index':'100','opacity': '0.4'});
				//$('.bx-next').css({'margin-right':'-6.4%','margin-top':'-0.8%','z-index':'100','opacity': '0.4'});
				//$(".bx-prev").css("background":'url(path+"/images/leftSliderArrow.jpg")');
				//$('.bx-prev').text('').css("float","left").append("<div style=\"border-right: 1px solid #aaa7a7;margin-left: -2px;margin-top: -117px;width: 27px;height: 107px;\"><img  src=\""+path+"/images/leftSliderArrow.jpg\" style=\"opacity: 0.8;margin-left: 4px;margin-top: 45px;width: 18px;height: 25px;\"> </div>" );
				// $('.bx-next').text('').css("background","url('images/arrowright.png') no-repeat");
				// $('.bx-prev').text('').css("background","url('images/arrowleft1.png') no-repeat");
				//$('#myChart1').css({'height':'105px','width':'340px','margin-top':'2%'});
				$('.bx-prev').text('').css("float","left").append("<div style=\"margin-left: -30px;margin-top: -126px;width: 27px;height: 15vh;border-right: 1px solid #ccc;\"><img  src=\"images/leftSliderArrow.jpg\" style=\"opacity: 0.7;margin-left: 5px;margin-top: 34px;width: 18px;height: 25px;\"> </div>" );
			    $('.bx-next').text('').css("float","right").append("<div style=\"margin-top: -110px;width: 27px;height: 61px;margin-right:-28px\"><img  src=\"images/rightSliderArrow.jpg\" style=\"opacity: 0.7;margin-left: 20px;margin-top: 17px;width: 18px;height: 25px;\"> </div>" );
			
				
	        }else{
	            
				$('#tabContentDiv').css({'margin-top':'0%'});
            		$(".sCal").css({'display':'none'});
					result= prepareUI();
					$("div#work").append(result);
					$('.sBac').removeClass("sCalBcActive").addClass("sCalBcInActive");
    				$('#MAX').removeClass("sCalBcInActive").addClass("sCalBcActive");
					 if(newProjId != null || newProjId == ''){
					    	$('#projectImageId_'+newProjId).css('border','2px solid #3399FF');
					    	if(typeof(projActionType) != 'undefined' && projActionType != null  && projActionType != ""){
					    		$("#projSetting_"+newProjId).trigger('click');
					    	}
					    }
					
					 
					$("#loadingBar").hide();
		     		timerControl("");
		     		
		     		if(userroleid =='6' || userroleid =='7' ){  //-------------------> validation only for Terafin
			           $('.accessOnlyAgile').removeAttr('onclick').css('opacity','0.3');
			           $('input.accessOnlyAgile').removeAttr('onkeyup').removeAttr('onkeypress').attr('readonly','true')
			        }
	        }
	        $("#loadingBar").hide();
		    timerControl("");
		}
	});
  }
  
  var $chart1;
    function creatChart1(subArray,subDateArray,projId,bgArray1){  
   
    		 if (typeof $chart1 !== "undefined") {
				    $chart1.destroy();
				  }

 	 
			var ctx1 = document.getElementById('myChart1').getContext('2d');
			
			
			chart = new Chart(ctx1, {
			    // The type of chart we want to create
			    type: 'bar',
			
			    // The data for our dataset
			    data: {
			        labels: subDateArray,
			        
			        datasets: [{
			            label: "",
			            backgroundColor: bgArray1,
			    		data: subArray,
			    		scaleOverride:true,
					  scaleSteps:9,
					  scaleStartValue:0,
					  scaleStepWidth:10
			           
			        }]
			    },
			
			
			
			    options: {
			    			scales: {
								        xAxes: [{
								            	  categoryPercentage: 1.0,
          										  barPercentage: 1.0,
          										  gridLines: {
										                offsetGridLines: true
										            }
          										  
            									
            									
       
								        }],
								       yAxes: [{
								        	 
				                            display: true,
				                            
				                            ticks: {
				                              	min:-1,
				                                stepValue: 0.1,
				                                max: 1
				                            }
				                        }]
								    },
							legend: {
						        display: false,
						       
						    },
						    tooltips: {
						        callbacks: {
						           label: function(tooltipItem) {
						                  return tooltipItem.yLabel;
						           }
						        }
				    }
			    }
			});
			
			$chart1 = chart;
    }
    var $chart2;
    function creatChart2(subArray,subDateArray,projId,bgArray2){  
    		 if (typeof $chart2 !== "undefined") {
				    $chart2.destroy();
				  }

 	 
			var ctx2 = document.getElementById('myChart2').getContext('2d');
			
			
			chart = new Chart(ctx2, {
			    // The type of chart we want to create
			    type: 'bar',
			
			    // The data for our dataset
			    data: {
			        labels: subDateArray,
			        
			        datasets: [{
			            label: "",
			            backgroundColor: bgArray2,
			    		data: subArray,
			    		scaleOverride:true,
					  scaleSteps:9,
					  scaleStartValue:0,
					  scaleStepWidth:10
			           
			        }]
			    },
			
			
			
			    options: {
			    			scales: {
								        xAxes: [{
								            	  categoryPercentage: 1.0,
          										  barPercentage: 1.0,
          										  gridLines: {
										                offsetGridLines: true
										            }
          										  
            									
            									
       
								        }],
								       yAxes: [{
								        	 
				                            display: true,
				                            
				                            ticks: {
				                              	min:-1,
				                                stepValue: 0.1,
				                                max: 1
				                            }
				                        }]
								    },
							legend: {
						        display: false,
						       
						    },
						    tooltips: {
						        callbacks: {
						           label: function(tooltipItem) {
						                  return tooltipItem.yLabel;
						           }
						        }
				    }
			    }
			});
			$chart2 = chart;
    }
    
    var $chart3;
    function creatChart3(subArray,subDateArray,projId,bgArray3){  
    		 if (typeof $chart3 !== "undefined") {
				    $chart3.destroy();
				  }

 	 
			var ctx3 = document.getElementById('myChart3').getContext('2d');
			
			
			chart = new Chart(ctx3, {
			    // The type of chart we want to create
			    type: 'bar',
			
			    // The data for our dataset
			    data: {
			        labels: subDateArray,
			        
			        datasets: [{
			            label: "",
			            backgroundColor: bgArray3,
			    		data: subArray,
			    		scaleOverride:true,
  scaleSteps:9,
  scaleStartValue:0,
  scaleStepWidth:10
			           
			        }]
			    },
			
			
			
			    options: {
			    			scales: {
								        xAxes: [{
								            	  categoryPercentage: 1.0,
          										  barPercentage: 1.0,
          										  gridLines: {
										                offsetGridLines: true
										            }
          										  
            									
            									
       
								        }],
								       yAxes: [{
								        	 
				                            display: true,
				                            
				                            ticks: {
				                              	min:-1,
				                                stepValue: 0.1,
				                                max: 1
				                            }
				                        }]
								    },
							legend: {
						        display: false,
						       
						    },
						    tooltips: {
						        callbacks: {
						           label: function(tooltipItem) {
						                  return tooltipItem.yLabel;
						           }
						        }
				    }
			    }
			});
			$chart3 = chart;
    }
  
  function prepareUIforSA(projArray){
  	var defImage = lighttpdpath + "/projectimages/dummyLogo.png";
	var UI = "";
		 if(projArray[2] != undefined){
		UI += '	<div class="row projImgMainDiv1"  style="background:white;height:21vh;margin-left:1%;margin-right:1%;border-bottom: 1px solid #ccc;"> '
       	        	+'<div class="col-md-2 col-sm-2" id="projectImage3"  style="height:100%;padding:0%"> ' 
       	        	+'<div id="sysScore3" style="color:white;font-size:9px;font-weight:bold;padding-top:4%;text-align:center;background:#ff0000;height:23%;width:15%;margin-top:3%;margin-left:42%;position:absolute;border-radius:50%"></div>'
       	        	+'<img src="'+defImage+'" style="width:82px;height:82px;border-radius:6px;margin-left:11%;margin-top:3vh;margin-bottom:1%;border: 1px solid rgb(191, 191, 191);"  ></img>'
       	        	+'<div class="defaultExceedCls" style="width:82px;text-align:center;font-family: OpenSansRegular;font-size: 11px;font-weight:bold;margin-left:10%" id="projName3"></div>'
       	        	+'</div>'
       	        	+'<div class="col-md-8 col-sm-8"  id="teamMember3" style="margin-left:-5%;height:99%;padding:0px"> ' 
       	        	
       	        	+'</div>'
       	        	+'<div class="col-md-2 col-sm-2" id="chartContainer_'+projArray[2]+'" style="height:100%;float:right;position:initial"> ' 
       	        	+'<canvas id="myChart3" class="myChart"></canvas>'
       	        	+'<div class="createBtn " style="margin-top:1%;margin-right:42%; color: #ffffff;font-family: opensanscondbold;font-size: 10px;padding: 4px 18px !important;"  >MORE</div>'
       	        	+'</div>'
       	        
       	       	+'</div> ';
       	       	
       	    }
		
       	    
       	 if(projArray[1] != undefined){
		UI += '	<div class="row projImgMainDiv1"  style="background:white;height:21vh;margin-left:1%;margin-right:1%;border-bottom: 1px solid #ccc;"> '
       	        	+'<div class="col-md-2 col-sm-2" id="projectImage2"  style="height:100%;padding:0%"> ' 
       	        	+'<div id="sysScore2" style="color:white;font-size:9px;font-weight:bold;padding-top:4%;text-align:center;background:#FFC200;height:23%;width:15%;margin-top:3%;margin-left:42%;position:absolute;border-radius:50%"></div>'
       	        	+'<img src="'+defImage+'" style="width:82px;height:82px;border-radius:6px;margin-left:11%;margin-top:3vh;margin-bottom:1%;border: 1px solid rgb(191, 191, 191);"  ></img>'
       	        	+'<div class="defaultExceedCls" style="width:82px;text-align:center;font-family: OpenSansRegular;font-size: 11px;font-weight:bold;margin-left:10%" id="projName2"></div>'
       	        	+'</div>'
       	        	+'<div class="col-md-8 col-sm-8"  id="teamMember2" style="margin-left:-5%;height:99%;padding:0px"> ' 
       	        	
       	        	+'</div>'
       	        	+'<div class="col-md-2 col-sm-2" id="chartContainer_'+projArray[1]+'" style="height:100%;float:right;position:initial"> ' 
       	        	+'<canvas id="myChart2" class="myChart" ></canvas>'
       	        	+'<div class="createBtn " style="margin-top:1%;margin-right:42%; color: #ffffff;font-family: opensanscondbold;font-size: 10px;padding: 4px 18px !important;"  >MORE</div>'
       	        	+'</div>'
       	        
       	       	+'</div> ';
       	       	
       	    }
       	 
		if(projArray[0] != undefined){
		UI += '	<div class="row projImgMainDiv1"  style="background:white;height:21vh;margin-left:1%;margin-right:1%"> '
       	        	+'<div class="col-md-2 col-sm-2" id="projectImage1"  style="height:100%;padding:0%"> ' 
       	        	+'<div id="sysScore1" style="color:white;font-size:9px;font-weight:bold;padding-top:4%;text-align:center;background:#008000 ;height:23%;width:15%;margin-top:3%;margin-left:42%;position:absolute;border-radius:50%"></div>'
       	        	+'<img src="'+defImage+'" style="width:82px;height:82px;border-radius:6px;margin-left:11%;margin-top:3vh;margin-bottom:1%;border: 1px solid rgb(191, 191, 191);"  ></img>'
       	        	+'<div class="defaultExceedCls" style="width:82px;text-align:center;font-family: OpenSansRegular;font-size: 11px;font-weight:bold;margin-left:10%" id="projName1"></div>'
       	        	+'</div>'
       	        	+'<div class="col-md-8 col-sm-8"  id="teamMember1" style="margin-left:-5%;height:99%;padding:0px;"> ' 
       	        	
       	        	+'</div>'
       	        	+'<div class="col-md-2 col-sm-2" id="chartContainer_'+projArray[0]+'" style="height:100%;float:right;position:initial"> ' 
       	        	+'<canvas id="myChart1" ></canvas>'
       	        	+'<div id ="myChart1b" class="createBtn " style="margin-top:1%;margin-right:42%; color: #ffffff;font-family: opensanscondbold;font-size: 10px;padding: 4px 18px !important;"  >MORE</div>'
       	        	+'</div>'
       	        	
       	       	+'</div> ';
       	       	
       	    }else{
       	    
       	    
       	    }
		
				 
			  
	
    UI=UI.replaceAll("ch(20)","'").replaceAll("ch(30)","chr(dbl)").replaceAll("ch(50)","[").replaceAll("ch(51)","]").replaceAll("ch(curly)","{").replaceAll("ch(clcurly)","}").replaceAll("ch(backslash)","\\");	
 	return UI;
  
  }
  
  
  
  
  function prepareUI() {
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	var UI = "";
	defImage = lighttpdpath + "/projectimages/dummyLogo.png";
    j=jsonData.length;
	for (var i = 0; i < j ; i++) {
   		var projectID = jsonData[i].project_id;
	    var ProjTitle = jsonData[i].project_name;//jsonData[i].projTitle
		var funcFlag = "";//jsonData[i].funcFlag
		var funClick = "";//jsonData[i].funClick
		var imageUrl = jsonData[i].imageUrl; // url for images imageUrl
		var imageClass = "";///jsonData[i].imageClass
		var projName = jsonData[i].project_name; ///project_name ///projName
		var projectArchiveStatus = jsonData[i].project_user_status;///project_user_status///status
		var cancelL = "";///jsonData[i].cancelL
		var ctxPath = jsonData[i].ctxPath;
	    projSettingL = "";///jsonData[i].projSettingL
		var projectUsersStatus = jsonData[i].project_user_role; ///project_user_role//proj_user_status
		var projAccessStatus = jsonData[i].projAccessStatus;
		var projectType="";
		if(projectUsersStatus=="PO"){
			projectType="MyProjects";
		}else{
			projectType="SharedProjects";
		}
		if(jsonData[i].project_status=="I"){
			ProjTitle = "Click to unlock"
		}

		if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="PO" && jsonData[i].project_status=="A" )){
			funcFlag = "psubscribed";
			funClick = "readMoreProjects(this);";
		}

		if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="I" )){
			funcFlag = "pLocked";
			funClick = "readMoreProjects(this);";	
			imageUrl = imageUrl+("images/Projects/locked_project.png");
			imageClass ="display:none";
			funClick = "unlockProject(this);";
		}

		if( ( projectUsersStatus=="TM" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="SU" && jsonData[i].project_status=="Y" )){
			funcFlag = "psubscribed";
			funClick = "readMoreProjects(this);";
		}

	
		UI += "	<div class=\"projImgMainDiv\"> <div id=\"projectImage_"+projectID+"\" onclick=\"projectRedirection('"+projectID+"','"+projName.replaceAll("'","CHR(39)")+"','"+projectUsersStatus+"','"+jsonData[i].project_status+"','"+projectType+"','"+imageUrl+"');switchZone('landingpageTOtz');\" class=\"projImageContainer\"  >";
       	   if(projAccessStatus =='relative'){
	       	     	 UI+="<div class=\"settingIcon\"> " ;
	       	     if(projectUsersStatus=="PO"){
				    	UI+="   <img class='Owned_by_cLabelTitle shareUserIcon'  title=\"owned by\" src=\"images/myprojects.png\"  projectType=\"my_project\"  projectId=\""+projectID+"\" onclick=\"showProjectPop(this);\"  />";  
				}else{
						UI+="   <img class='Shared_by_cLabelTitle shareUserIcon'  title=\"shared by\" src=\"images/sharedprojects.png\"  projectType=\"shared_project\"  projectId=\""+projectID+"\" onclick=\"showProjectPop(this);\" />";  
				}
				if(projectUsersStatus=="PO"){
						UI+=" <img id=\"projSetting_"+projectID+"\" title=\""+projSettingL+"\" style=\""+imageClass+";float:right;\" class=\"wrkSpaceProjOption accessOnlyAgile\" src=\"images/settings.png\" onclick=\"fetchWsData('updateWSProject',"+projectID+",'workspace');event.stopPropagation();\">"+" </div> ";
							
				}else{
						UI+=" <img id=\"projSetting_"+projectID+"\" title=\""+projSettingL+"\" style=\""+imageClass+";float:right;\" class=\"wrkSpaceProjOption accessOnlyAgile\" src=\"images/settings.png\" onclick=\"fetchWsData('updateWSProject',"+projectID+",'workspace');event.stopPropagation();\">"+" </div> ";						   
				}
		       	        
       	      
			   UI+= " 	<img   style='width:82px;height:82px;'  title=\""
				+ ProjTitle
				+ "\" id=\"projectImageId_"
				+ projectID
				+ "\"  "
				+ " 	class=\"lozad "
				+ funcFlag
				+ " projImageCls\" src='/images/workspace/projectImage.svg'  data-src=\""
				+ imageUrl+""
				+ "\" onerror=\"javascript:userImageOnErrorReplace(this);\" />";
			}else{
				UI+= " 	<img   style='width:82px;height:82px;'  title=\""
				+ ProjTitle
				+ "\" id=\"projectImageId_"
				+ projectID
				+ "\"  "
				+ " 	class=\"lozad "
				+ funcFlag
				+ " projImageCls\" src='/images/workspace/projectImage.svg'  data-src=\""
				+ imageUrl+""
				+ "\" onclick=\"displayProjDesc(this,"+projectID+",'myProject');\" onerror=\"javascript:userImageOnErrorReplace(this);\" />";
			
			}     
				
				
				
				 if(projectUsersStatus=="PO"){
			  UI+=" 	<div id=\"projectName_"
				+ projectID
				+ "\"  style=\"display:none;\" >"
				+ projName
				+ "</div>"
				+ " 	<div id=\"projectType_"
				+ projectID
				+ "\"  style=\"display:none;\" >MyProjects</div>"
				+ " 	<div id=\"projectArchiveStatus_"
				+ projectID
				+ "\"  style=\"display:none;\" >"
				+ projectArchiveStatus
				+ "</div>"
				+ " 	<div id=\"projectUsersStatus_"
				+ projectID
				+ "\"  style=\"display:none;\" >"
				+ projectUsersStatus
				+ "</div>";
             }else{
             	
              UI+="	<div id=\"projectName_"
				+ projectID
				+ "\"  style=\"display:none;\" >"
				+ projName
				+ "</div>"
				+ " 	<div id=\"projectType_"
				+ projectID
				+ "\"  style=\"display:none;\" >SharedProjects</div>"
				+ " 	<div id=\"projectArchiveStatus_"
				+ projectID
				+ "\"  style=\"display:none;\" >"
				+ projectArchiveStatus
				+ "</div>"
				+ " 	<div id=\"projectUsersStatus_"
				+ projectID
				+ "\"  style=\"display:none;\" >"
				+ projectUsersStatus
				+ "</div>";
             }
			  UI+=" 	<div class=\"projNotifications\" id=\"notificationContainer_"
				+ projectID
				+ "\" style=\" display:none; background-color: #FFFFFF;border: 1px solid #A1A1A1; border-radius: 5px 5px 5px 5px;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.6);min-height:145px;min-width:180px;margin-top: -90px;left:140px;height:auto;position: absolute;z-index: 5010;\" >"
				+ " 	<div id=\"arrowNotDiv\" ></div>  "
				+ " 	<div title=\""
				+ cancelL
				+ "\" onclick=\"closeProjNotificationPopUp();\" style=\" float: right; margin-top: 2px; padding: 1px 5px 0px 5px;cursor:pointer;\"> "
				+ " 	<img style=\"float:right;height:16px;width:16px;\" src=\""
				+ "images/workspace/workspace_cancel.png\"></div>"
				+ "   </div>"
				+ " 	<div title=\""+ProjTitle+"\" data-toggle=\"tooltip\" data-placement=\"bottom\"  class=\"workspaceProjTitle prjName\"   align=\"center\" >"
				+ projName
				+ "<span class=\"myProjHidden_field\" style=\"display:none\">"
				+ ProjTitle
				+ "</span></div> "
				+ " 	</div> "
				+ "   <div class=\"wrkspaceProjDetails\"   id=\"projectDetailsDiv_"
				+ projectID + "\" style=\"display:none;\"> </div> </div>"  ;

	
	}
    UI=UI.replaceAll("ch(20)","'").replaceAll("ch(30)","chr(dbl)").replaceAll("ch(50)","[").replaceAll("ch(51)","]").replaceAll("ch(curly)","{").replaceAll("ch(clcurly)","}").replaceAll("ch(backslash)","\\");	
 	return UI;
  }
  
function readMoreProjects(obj){
	
        if(userroleid =='6' || userroleid =='7'){  //-------------------> validation only for Terafina
	       mAct="sprintMenu";
	    }else{
	       mAct="LoadActivityFeed";
	    }
        
        projectId = obj.id.split('_')[1];
		projType = $("#projectType_" + projectId).text();
		projName = $("#projectName_" + projectId).text();
		projectUsersStatus=$("#projectUsersStatus_"+ projectId).text();
		var projtitle =  $("#projectImageId_" + projectId).attr("title");
		projArchStatus = $("div#projectArchiveStatus_" + projectId).text();
		var projImgSrc = $("img#projectImageId_" + projectId).attr('src');
		window.sessionStorage.projectidglb = projectId;
		window.sessionStorage.projectnameglb = projName;
		window.sessionStorage.projectimageglb = projImgSrc;
		window.location = hostname+"/postlogin?pAct="+mAct+"&roleid="+rid;
		/* window.location.href = path+"/Redirect.do?pAct="+mAct+"&projId=" + projectId
			+ "&projType=" + projType + "&projName=" + projName
			+ "&projArchStatus=" + projArchStatus + "&projImgSrc=" + projImgSrc + "&projectUsersStatus=" + projectUsersStatus; */
       // updateRecentProjectData(projectId);
  }
  
 
  function unlockProject(obj) {
	unlockProjectId = obj.id.split('_')[1];
	unlockProjName = $("div#projectName_" + unlockProjectId).text();
	confirmFun(getValues(companyAlerts,"Alert_DoUlcPrj"), "clear",
			"unlockProjAftConfirm");
  }
  
  function unlockProjAftConfirm() {
    $("#loadingBar").show();
    timerControl("start");
	projectId = unlockProjectId;
	projectName = unlockProjName;
	$.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : {
					act : "unlockClosedProj",
					projectId : projectId,
					projectName : projectName
				},
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
		success : function(result) {
			checkSessionTimeOut(result);
			if (result == "success") {
				parent.alertFun(getValues(companyAlerts,"Alert_PrjUnlock"), 'warning');
				loadMyProjectsJson();
				$("#loadingBar").hide();
    			timerControl("start");
			} else {
				parent.alertFun(getValues(companyAlerts,"Alert_PrjNoUnlock"), 'warning');
				$("#loadingBar").hide();
     			timerControl("");
			}
		}
	});
  }

  function showAvailableProjJsonNew(taskFilterVal1,taskSearchWord,type){
	$('div#subscribe').html('');
	  let jsonbody = {
	    "user_id" : userIdglb,
	//     "ipadVal" : "",
	    "sortType" : taskFilterVal1,
	    "type" : type,
	    "txt" : taskSearchWord,
	    "companyId" : companyIdglb,
	    "workspaceType" : "",
	//     "sortHideProjValue" : "",
	//     "filterType" : ""
	}
	checksession();
	$.ajax({
		url: apiPath+"/"+myk+"/v1/projectList",////apiPath+"/"+myk+"/v1/loadProjDDList
		type:"POST",
		dataType:'json',
		contentType:"application/json",
		data: JSON.stringify(jsonbody),
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
	   success : function(result) {
		
		  jsonData = result;
		//   console.log("Result for subcribe::::"+JSON.stringify(result));
		  
		  result=preparePublicUI();
		  $('div#subscribe').append(result);
		  observer.observe();//---- to lazy load user images
		//   $("div#tabContentDiv").show();
        //   $("div#tabContentDiv").append(result);
        //   availableProject();
        //   	sortType = '';
		// 	searchVal = '';
		// 	searchType = ''; 
          $("#loadingBar").hide();
    	  timerControl("");
	   }
	});
  }

  function showAvailableProjJson() {
    //console.log("showAvailableProjJson-->sortType"+sortType+"searchVal  "+searchVal+"searchType  "+searchType);
    $("#loadingBar").show();
    timerControl("start");
    $("img#goBackToMainScreen").hide();
	$("div#tabContentDiv").html("");
	$("div#loadingAvaiProj").remove();
	$("div#showhiddenProj").hide();
	$("select#sortInWs").attr("onchange", "sortPublicProjects(this)");
	$("input#noteSearch").attr("onclick", "searchAvailProjectsElements();");
	$("input#noteSearch").attr('src', '/images/search.png');
	$("select#sortInWs").attr("value", "Default");
	$("input#searchBox").val('');
	searchVal= $("#wsSearchBox").val();
	$("#createNewProj").hide();
	$("select#sortInWs").val('sort');
	$("img#myProjIcon").css('opacity', '0.6');
	$("img#sharedProjIcon").css('opacity', '0.6');
	$("img#publicProjIcon").css('opacity', '1');
	$("#smorgasHeaderSortDiv").hide();
	$("#smorgasHeaderSortDiv").css("right", "50px");
	$('#uploadCsvFile').hide();
	projHideStatus = "availProjects";
	searchProjType = 'public';
	availProjBoolen = true;
	availProjLimit = 0;
	index=0;
	let jsonbody = {
	    "user_id" : userIdglb,
	//     "ipadVal" : "",
	    "sortType" : sortType,
	    "type" : searchType,
	    "txt" : searchVal,
	    "companyId" : companyIdglb,
	    "workspaceType" : "",
	//     "sortHideProjValue" : "",
	//     "filterType" : ""
	}
	checksession();
	$.ajax({
		url: apiPath+"/"+myk+"/v1/projectList",////apiPath+"/"+myk+"/v1/loadProjDDList
		type:"POST",
		dataType:'json',
		contentType:"application/json",
		data: JSON.stringify(jsonbody),
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
	   success : function(result) {
		
		  jsonData = result;
		  //console.log("Result===>"+result);
		  
		  result=preparePublicUI();
		  
		  $("div#tabContentDiv").show();
          $("div#tabContentDiv").append(result);
          availableProject();
          	sortType = '';
			searchVal = '';
			searchType = ''; 
          $("#loadingBar").hide();
    	  timerControl("");
	   }
	});
  }

  function preparePublicUI() {
	  var UI = "";
	  defImage = lighttpdpath + "/projectimages/dummyLogo.png";
	  l=jsonData.length;
	  for (i = 0; i < l ; i++) {
		var projectID = jsonData[i].project_id;
		var projectTitle = jsonData[i].project_name;///projTitle
		var funcFlag = "tbsubscribed";///jsonData[i].funcFlag
		var funClick = "checkForSubscription('"+projectID+"')";
		var imageUrl = jsonData[i].imageUrl;
		var projName = jsonData[i].project_name;
		var projectArchiveStatus = jsonData[i].project_status;///status
		var cancelL = "";//jsonData[i].cancelL
		var ctxPath = jsonData[i].ctxPath;
		var projOwnersL = "";///jsonData[i].projOwnersL

		if(projectTitle.length>10){
			projectTitle = projectTitle.substring(0,10);
		}

		/* if((jsonData[i].project_user_status=="TM" && jsonData[i].project_user_status=="Y") || (jsonData[i].project_user_status=="SU" && jsonData[i].project_user_status=="Y")){
			funcFlag = "psubscribed";
			funClick = "readMoreProjects(this);";
			break;
		}else if((jsonData[i].project_user_status=="TM" && jsonData[i].project_user_status=="N") || (jsonData[i].project_user_status=="SU" && jsonData[i].project_user_status=="N")){
			funcFlag = "pUsrUnSbscribed";
			funClick = "";
			break;
		}else if((jsonData[i].project_user_status=="TM" && jsonData[i].project_user_status=="N") || (jsonData[i].project_user_status=="SU" && jsonData[i].project_user_status=="N")){
			funcFlag = "pAdminUnSbscribed";
			funClick = "";
			break;
		}else if((jsonData[i].project_user_status=="TM" && jsonData[i].project_user_status=="N")){
			funcFlag = "waitApproval";
			funClick = "";
			break;
		}else if((jsonData[i].project_user_status=="TM" && jsonData[i].project_user_status=="N")){
			funcFlag = "subCanceled";
			funClick = "";
			break;
		} */


		UI += "<div   class=\"projImgMainDiv\"><div id=\"projectImage_"+projectID+"\" class=\"projImageContainer\" ><div style=\"float: right;position: absolute;width: 80px;\">  <img title=\""
				+ projOwnersL
				+ "\"   class=\"wrkSpaceAvailProjOption\" src=\"images/owner.png\" "
				+ "style=\"margin-left:3px\" onclick=\"PublicProjOwners(this,"
				+ projectID
				+ " );\"></div><img  style='width:82px;height:82px;'  title=\""
				+ projectTitle
				+ "\" id=\"projectImageId_"
				+ projectID
				+ "\""
				+ " 	class=\"lozad "
				+ funcFlag
				+ " projImageCls\" src='/images/workspace/projectImage.svg'  data-src=\""
				+ imageUrl
				+ "\" onclick=\""
				+ funClick
				+ ";\" onerror=\"javascript:userImageOnErrorReplace(this);\" />"
				+ "   <div class=\"wrkspaceProjDetails\" style=\"margin-top:0px;display:none;background-color: #fff;border: 1px solid #B7BBBE; box-shadow: -2px 4px 10px #cccccc;\" id=\"publicProjOwnersList_"
				+ projectID
				+ "\"> </div>"
				+ "   <div class=\"projNotifications\" id=\"notificationContainer_"
				+ projectID
				+ "\" style=\" display:none; background-color: #FFFFFF;border: 1px solid #A1A1A1; border-radius: 5px 5px 5px 5px;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.6);min-height:145px;min-width:180px;margin-top: -90px;left:140px;height:auto;position: absolute;z-index: 5010;\" >"
				+ " 	<div id=\"arrowNotDiv\" ></div>  "
				+ " 	<div title=\""
				+ cancelL
				+ "\" onclick=\"closeProjNotificationPopUp();\" style=\" float: right; margin-top: 2px; padding: 1px 5px 0px 5px;cursor:pointer;\"> "
				+ " 	<img style=\"float:right;height:16px;width:16px;\" src=\""
				+ "images/workspace/workspace_cancel.png\"></div>"
				+ "	</div>"
				+ "	<input type=\"hidden\" value=\""
				+ projectID
				+ "\" id=\"availProjId\">"
				+ "	<div id=\"projectName_"
				+ projectID
				+ "\"  style=\"display:none;\" >"
				+ projectTitle
				+ "</div>"
				+ "	<div id=\"projectType_"
				+ projectID
				+ "\"  style=\"display:none; \" >MyProjects</div>"
				+ "   <div align=\"center\" class=\"availProjTitle workspaceProjTitle prjName\">"
				+ projName
				+ "<span class=\"availProjHidden_field\" style=\"display:none;\">"
				+ projectTitle.toLowerCase()
				+ "</span></div> "
				+ "   </div> "
				+ "   </div> ";

	}
	 return UI;
}

function checkForSubscription(suProjectid){
	
	let jsonbody = {
		   "project_id" : suProjectid,
    		"user_id" : userIdglb
	}	
	$.ajax({
		url: apiPath+"/"+myk+"/v1/getUserStatusforSub",
		type:"POST",
		//dataType:'json',
		contentType:"application/json",
		data: JSON.stringify(jsonbody),
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                
				}, 
	   success : function(result) {
			
			if(result == ""){
				subscribeAvailProject(suProjectid);
			}else{
				alertFun(getValues(companyAlerts,"Alert_WaitingApproval"), 'warning');
			}

		
		  
	   }
	});
	
 
}

function subscribeAvailProject(suProjectid){
	
	let jsonbody = {
		    "project_id":suProjectid
	}
	$.ajax({
		url: apiPath+"/"+myk+"/v1/subscribeAvailProject",
		type:"POST",
		dataType:'json',
		contentType:"application/json",
		data: JSON.stringify(jsonbody),
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                
				}, 
	   success : function(result) {
		   if(result != ""){
				$('#transparentDiv').show();
				$('div#availableProjDivData').html(subscribePopup(result)).show();
		   }
	   }
	});
}

function subscribePopup(result){
	var ui="";
	var currProjId = result[0].project_id;
	var projName = result[0].project_name;
	var projProf = result[0].projDesc;
	var project_image_type = result[0].project_image_type;

	var imageUrl = "";

	if(typeof(project_image_type)=="undefined" || project_image_type=="" || project_image_type=="-"){
		imageUrl = lighttpdpath + "/projectimages/dummyLogo.png";
	}else{
		imageUrl = lighttpdpath+"projectimages/"+currProjId+"."+project_image_type+"?"+d.getTime();
	}

	ui+='<div class="modal3" id="">'
    +'<div class="modal-dialog">'
      +'<div class="modal-content container">'
      
        
        +'<div class="modal-header py-2 pl-0" style="border-bottom: 1px solid #64696F;">'
          +'<p class="modal-title" style="font-size:14px;">Subscribe Project</p>'
          +'<button type="button" onclick="closeSUpopup();" class="close p-0" data-dismiss="modal3" style="top: 25px;right: 25px;color: black;outline: none;">&times;</button>'
        +'</div>'
        
        
        +'<div class="modal-body py-2 pl-0" style="height: 150px;">'
          +'<div class="media p-0">'
			+'<img title="'+projName+'" src="'+imageUrl+'" alt="" class="mr-3 rounded border" style="width:95px;height:90px;">'
			+'<div class="media-body defaultExceedCls" style="height:100px;width:75%;">'
			  +'<h6>'+projName+'</h6>'
			  +'<p>'+projProf+'</p>'    
			+'</div>'
		  +'</div>'
        +'</div>'
        
        
        +'<div class="modal-footer py-2 px-0" style="border-top: none !important;">'
          +'<button type="button" class="btn" style="width: 20%;background-color:#00BFF3;color: #fff;" data-dismiss="modal" onclick="subscribeClick('+currProjId+');">Subscribe</button>'
        +'</div>'
        
      +'</div>'
    +'</div>'
  +'</div>'

   return ui;
}
  
function closeSUpopup(){
	$('#transparentDiv').hide();
	$('div#availableProjDivData').html('').hide();
}

  function availableProject() {
		$("img.tbsubscribed").click(function() {
			//$("div#transparentDiv").show();
			
			//$("div#wstansparentDiv").show();
			$('#loadingBar').show();
			timerControl("start");
			
			var projId = $(this).attr('id');
			projId = projId.split('_')[1];
			
			$("div#availableProjDivData").show();
			$("div#wstansparentDiv").show();

			$.ajax({
				url : path + "/workspaceAction.do",
				type : "POST",
				data : {
							act : "subscribeAvailProject",
							projId : projId
						},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						},	 
				success : function(result) {
					checkSessionTimeOut(result);
					$('div#availableProjDivData').html(result);
					//var isiPad = navigator.userAgent.match(/iPad/i) != null;
					$('div#availableProjDivData').css("top", "25%");
					$('div#availableProjDivData').css("left", "30%");
					popUpScrollBar('publicProjectDesc');
					$('#loadingBar').hide();
					timerControl("");
					loadCustomLabel('onload');
				}
			});
		});

		$("img.waitApproval").click(
				function() {
					//parent.alertFun("Project was subscribed already;Pending Project Admin's Approval",'warning');
					alertFun(getValues(companyAlerts,"Alert_WaitingApproval"), 'warning');
					//alertFun("You have already subscribed to this project. Approval is Pending.",'warning');
				});

		$("img.subCanceled").click(
				function() {
					// parent.alertFun("Subscription has been denied",'warning');
					alertFun(getValues(companyAlerts,
							"Alert_SubcriptionDenied"), 'warning');
				});
	}  
 
  function subscribeClick(cPId) {
		$('#loadingBar').addClass('d-flex').removeClass('d-none');
		$.ajax({
			url: apiPath+"/"+myk+"/v1/loadSubscribeProjDetail?projId="+cPId+"&userName="+userFullname+"&userEmailId="+userEmail+"&userIdF="+userIdglb+"&companyId="+companyIdglb+"&reqHost=https://testbeta.colabus.com",
			type:"POST",
			/* dataType:'json',
			contentType:"application/json",
			data: JSON.stringify(jsonbody), */
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $('#loadingBar').addClass('d-none').removeClass('d-flex');
					}, 
			success : function(result) {
				if(result == "success"){
					alertFun(getValues(companyAlerts,"Alert_WaitApprovalPrj"),'warning');
				}else{
					alertFun('Subscription Failed','warning');
				}
				closeSUpopup();
				showAvailableProjJsonNew();
				$('#loadingBar').addClass('d-none').removeClass('d-flex');
				$('#transparentDiv').hide();
			}
		});
	}
  
  function closeAvalProjPopup(projId) {
		$("div.availProjectDataDiv").hide();
		$("div#availableProjDivData").hide();
		$("div#transparentDiv").hide();
		$("div#wstansparentDiv").hide();

	}
	
	var projNameFlag = '';
	var emailFlag = '';
	var attchVal = '';
	function saveWorkspaceProjName(projectId) {
		var obj = $('#wsImgUploadForm').data('obj');
		projNameFlag = true;
		emailFlag = true;
	   	var upload = $('#hideDiv_'+projectId).data('imgName');
	   	if(typeof(upload) == 'undefined'){
	   		upload = "";
	   	}else{
	   		submitProjImageUploadFormNew();
	   	}
	    var existingProjectName = 	$('#projNameHidden').val();
	    var splChar = /[!#@$`~:?<>\\\^&*(){}[\]<>?/|\-\"]/;
	    var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			
		var projName = $("#projectName").val().trim();
		var value = $('select#projStatusType option:selected').val();
		
		projName = projName.replace('<br>','');
		projName = projName.trim();
		
		if(projName == existingProjectName){
			projNameFlag = false;
		}
		var projectType = $("img#changePrjImg").val()
		
		/*var projEmail = $('#workspaceProjEmailId').val().trim();
		var accPwd = $('#workspaceProjEmailPwd').val().trim();
		var hostName = $('#workspaceEmailHostname').val().trim();
		var accType = $("select#workspaceEmailActType option:selected").attr('value');
		var portNumber = $('#workspaceEmailPortNum').val().trim();
   		var securityType = $("select#workspaceEmailSecType option:selected").attr('value');
     	var emailAttach = '';
   		if ($('#workspaceEmailAttach').is(":checked")){
  			emailAttach = 'checked';
		}else{
			emailAttach = 'unChecked';
		}
		var fetchEmails = '';
		if ($('#fetchEmailsFromDate').is(":checked")){
  			fetchEmails = 'currentDate';
		}else{
			fetchEmails = 'completeEmails';
		}
		
		if(hostName != '' || accType != '' || projEmail != '' || accPwd != ''){
    		emailFlag = false;
    		
    		if(!emailReg.test(projEmail) ) {
			 	alertFun("Please enter valid Email",'warning');
			 	timerControl("");	
			    $('#loadingBar').hide();
				return false;
			}	
			
    		if(projEmail == '' || projEmail == 'null'){
    			alertFun("Please enter the Email Address.", "warning");
            	$("#workspaceProjEmailId").focus();
             	timerControl("");	
				$('#loadingBar').hide();
             	return false;
    		}
    		if(accPwd =='' || accPwd == 'null'){
            	alertFun("Please enter the Email Password",'warning');
            	$("#workspaceProjEmailPwd").focus();
             	timerControl("");	
				$('#loadingBar').hide();
            	return false;
		   	}
     		if(hostName == '' || hostName == 'null'){
     			alertFun("Please enter the Email Host.", "warning");
        		$("#workspaceEmailHostname").focus();
            	timerControl("");	
				$('#loadingBar').hide();
            	return false;
     		}
	     		
    		if(accType == '' || accType == 'null'){
    			alertFun("Please select the Account Type.", "warning");
    			$("#workspaceEmailActType").focus();
             	timerControl("");	
				$('#loadingBar').hide();
             	return false;
    		}
    		
    		if(isNaN(portNumber)) {
            	alertFun("Please enter valid port number.",'warning');
             	timerControl("");	
				$('#loadingBar').hide();
                	return false;
            }
    	}*/
		if (!splChar.test(projName)) {
			if(projName == ""){
				alertFun(getValues(companyAlerts,"Alert_PrjNmeNoEmt"), "warning");
				return false;
			}else{
				if(projNameFlag != false){
					/*checkIfProjectNameExists(projName, projectId, projName, projectType, upload, 
					projEmail, accPwd, hostName, accType, portNumber, securityType, emailAttach,  fetchEmails);*/
					checkIfProjectNameExists(projName, projectId);
				}else if(projNameFlag == false && upload == "" && emailFlag == true){
					alertFun(getValues(companyAlerts,"Alert_PrjDetUnChanged"), "warning");
				}else{
					/*updateProjectDetails(projName, projectId, projName, projectType, upload, 
					projEmail, accPwd, hostName, accType, portNumber, securityType, emailAttach,  fetchEmails);*/
					updateProjectDetails(projName, projectId)
				}
			}
		}else if(projName == ""){
			alertFun(getValues(companyAlerts,"Alert PrjNmeNoEmt"), "warning");
			return false;
		}else {
			alertFun(getValues(companyAlerts,"Alert_ProjNameChar"), 'warning');
		}
	}
	
	/*function checkIfProjectNameExists(projName, projectId, projName, projectType, upload, 
	projEmail, accPwd, hostName, accType, portNumber, securityType, emailAttach,  fetchEmails){
		$.ajax({
			url : path + "/workspaceAction.do",
			type: "POST",
			data: {act:"checkProjectNameExists",projName:projName},
			success: function(result) {
				if (result == 'Yes') {
					alertFun("Project Name already exists",'warning');
					$('#workspaceproName').focus();
					$('#loadingBar').hide();
					timerControl("");
					return false;
				} else {
					/*updateProjectDetails(projName, projectId, projName, projectType, upload, 
					projEmail, accPwd, hostName, accType, portNumber, securityType, emailAttach,  fetchEmails);
					updateProjectDetails(projName, projectId);
				}
			}
		});
   	}*/
   	
   	function checkIfProjectNameExists(projName, projectId){
		$.ajax({
			url : path + "/workspaceAction.do",
			type: "POST",
			data: {act:"checkProjectNameExists",projName:projName},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success: function(result) {
				checkSessionTimeOut(result);
				if (result == 'Yes') {
					alertFun(getValues(companyAlerts,"Alert_ProjNameExist"),'warning');
					$('#loadingBar').hide();
					timerControl("");
					return false;
				} else {
					//updateProjectDetails(projName, projectId, projName, projectType, upload, projEmail, accPwd, hostName, accType, portNumber, securityType, emailAttach,  fetchEmails);
					updateProjectDetails(projName, projectId);
				}
			}
		});
   	}
   	
   	/*function updateProjectDetails(projName, projectId, projName, projectType, upload,
   	projEmail, accPwd, hostName, accType, portNumber, securityType, emailAttach, fetchEmails){
   		$.ajax({
			url:  	path + "/workspaceAction.do",
			type: 	"POST",
			data: 	{act: "updateProjectName",projId:projectId, projName:projName, projectType:projectType, uploadedImg:upload,
			projEmail:projEmail, accPwd:accPwd, hostName:hostName, accType:accType,  portNumber:portNumber, securityType:securityType,
			emailAttach:emailAttach, fetchEmails:fetchEmails
					},
			success : function(result){
				loadTabData("project");
				$('#projUploadIdNew').val('');
			}
		});
   	}*/	
   	
    var projectImgUploadFromGallery = false;
   	function updateProjectDetails(projName, projectId,  projectCode, startDate, endDate, projPrivicyType, projstat, projDesc, position,attachedCheckedValue){
   		$.ajax({
			url:  	path + "/workspaceAction.do",
			type: 	"POST",
			data: 	{act: "updateProjectName",projId:projectId, projName:projName, projectCode: projectCode,startDate:startDate,endDate:endDate, 
			         projPrivicyType: projPrivicyType, projStatus:projstat, projDesc: projDesc , uPhotoId:uPhotoId,
					 uDefaultType:uDefaultType, fileExtension:ImageUploadProjectFlag, fileName:fileName ,attachedCheckedValue:attachedCheckedValue},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success : function(result){
				checkSessionTimeOut(result);
				alertFun(getValues(companyAlerts,"Alert_WSDetCng"),'warning');
				$("img#goBackToMainScreen").hide();
			    $("div#projectSettingsContainerDiv").hide();
			    if(position=='workspace'){
				  loadTabData("project");
				}else if(position=='sysAdmin'){
				  loadSysAdminTabData('projects','sysAdmin');
				}  
				$('#projUploadIdNew').val('');
				projectImgUploadFromGallery = false;
			}
		});
   	}
	
	function closePopUp(){
	var temp="Create";
	var text="New Project";
	$("#changeDynamic").html("");
	$("#textChange").html("");
	$("#changeDynamic").html(temp);
	$("#textChange").html(text);
	$("#changeDynamic").removeAttr( "onclick","");
	$("#changeDynamic").attr("onclick", "createNewWorkspaceProject();");
	$("#changeImage").removeAttr( "onclick","");
	$("#changeImage").attr("onclick", "closePopup();");
	$("#hideDiv").show();
	$("#upload_link").show();
	closePopup();
	}
	
	var hideProjId = "";
	var hideStatus = "";
	var hideProjType = ""
	
	function onclickHideStatus() {
			$('#loadingBar').show();
			timerControl("start");
			projType = $("div#projectType_" +hideProjId).text();
			if (hideStatus == "hide") {
				$("img#wrkSpaceHideImage").attr("src",
						"/images/hidden.png");
				$("img#wrkSpaceHideImage").attr('onclick',
						'hideWorkSpaceProject(' + hideProjId + ',"unhide")');
				hideStatus = "unhide";
			} else {
				$("img#wrkSpaceHideImage").attr("src",
						"images/showhidden.png");
				$("img#wrkSpaceHideImage").attr('onclick',
						'hideWorkSpaceProject(' + hideProjId + ',"hide")');
				hideStatus = "hide";
			}
			$.ajax({
				url : path + "/workspaceAction.do",
				type : "POST",
				data : {
							act : "hideWorkSpaceProject",
							projId : hideProjId,
							status : hideStatus,
							projType : hideProjType
						},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success : function(result) {
			        checkSessionTimeOut(result);
					//$("select#sortInWs").val('Sort');
					var position = $('#hiddenProjSettingPosition').val();
					if(position=='workspace'){
					  loadTabData("project");
					}else{
					  loadSysAdminTabData('projects','sysAdmin');
					} 
					
				}
			});
			$('#loadingBar').hide();
			timerControl("");
		}
		
		
	
	var archiveProjId = "";
	function archiveMyProjects() {
	$('#loadingBar').show();
	timerControl("start");
	$.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : {
					act : "updateArchiveStatus",
					projId : archiveProjId
				},
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
		success : function(result) {
			 checkSessionTimeOut(result);
			 loadTabData("project");
			$('#loadingBar').hide();
			timerControl("");
		}
	});
}

function unarchiveMyProjects() {
	$('#loadingBar').show();
	timerControl("start");
	$.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : {
					act : "updateUnarchiveStatus",
					projId : archiveProjId
				},
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
		success : function(result) {
			checkSessionTimeOut(result);
			archiveProjId="";
			loadTabData("project");
			$('#loadingBar').hide();
			timerControl("");
		}
	});
}

function updateArchiveStatus(projId, status) {
	var r="";
	if(status == "A"){
	confirmFun(getValues(companyAlerts,"Alert_DoActivatePrj"), "delete", "unarchiveMyProjects");
	}
	else {
	confirmFun(getValues(companyAlerts,"Alert_DoArchPrj"), "delete", "archiveMyProjects");
	}
}

function showMyProjectOwners(projId) {
	if($("#myProjUsersListDiv_" + projId).is(':hidden')){
		$("#loadingBar").show();
		timerControl("start");
		$("#myProjUsersListDiv_" + projId).toggle();
		$.ajax({
			url : path + "/workspaceAction.do",
			type : "POST",
			data : {
						act : "loadMyProjUsers",
						projectId : projId,
						projType : 'PO'
					},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success : function(result) {
				checkSessionTimeOut(result);
				$("#myProjUsersListDiv_" + projId).html(result);
				/*if (!isiPad) {
					scrollProjUsers('myProjUsersList');
				}*/
				//$("div#myProjDetailData").mCustomScrollbar("update");
				
				var wT = $('#projSetting_' + projId).offset().top;
				var woh = $(window).height() - wT;
				setTimeout(function() {
					var popH = $("div#projectDetailsDiv_" + projId).height();
					if (woh < popH) {
						var fh = popH - woh + 10;
						var fh1 = popH - woh + 5;
						$('#projectDetailsDiv_' + projId).css('margin-top','-' + fh + 'px');
						$('#arrowDiv').css('margin-top', fh1 + 'px');
					}
					$("#loadingBar").hide();
					timerControl("");
				}, 100);
			 }
		});
		
   }else{
       $("#myProjUsersListDiv_" + projId).hide();
       var wT = $('#projSetting_' + projId).offset().top;
	   var woh = $(window).height() - wT;
	  
		var popH = $("div#projectDetailsDiv_" + projId).height();
		if (woh < popH) {
			var fh = popH - woh + 10;
			var fh1 = popH - woh + 5;
			$('#projectDetailsDiv_' + projId).css('margin-top','-' + fh + 'px');
			$('#arrowDiv').css('margin-top', fh1 + 'px');
		}
	
   }
}

function showEmailConfigure(projId) {
	    $("#loadingBar").show();
	 	timerControl("start");
	 if($("#emailConfigureDiv").is(':hidden')){
	 	var attchVal = "";
	 	$("#emailConfigureDiv").toggle();
 		var wL = $('#projSetting_'+projId).offset().left;
 		var wT = $('#projSetting_'+projId).offset().top;
 		var woh = $(window).height() - wT;
 		setTimeout(function() {
 			var popH = $("div#projectDetailsDiv_"+projId).height();
 			if (woh < popH) {
 				var fh = popH - woh + 10;
 				var fh1 = popH - woh + 5;
 				$("#projectDetailsDiv_"+projId).css('margin-top','-' + fh + 'px');
 				$('#arrowDiv').css('margin-top', fh1 + 'px');
 			}
 		}, 100);
 		/*$("div#emailConfigureDetails").mCustomScrollbar("destroy");
	 	//}
	 	$("div#myProjDetailData").mCustomScrollbar("update");*/
	 			$.ajax({
	 				url  : "${path}/workspaceAction.do",
	 				type : "POST",
	 				data : {
			 					act : "loadEmailConfigDetails",
			 					projId : projId
			 				},
			 		error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
	 				success : function(result) {
	 					checkSessionTimeOut(result);
	 					if (localprojectType == 'sharedProject') {
	 						$("input#workspaceProjEmailId").attr('readOnly', 'true');
	 						$("input#workspaceEmailHostname").attr('readOnly','true');
	 						$("input#workspaceProjEmailPwd").attr('readOnly','true');
	 						$("select#workspaceEmailActType").attr('disabled','true');
	 					}
	 					$("input#workspaceProjEmailId").val(getValues(result, "projEmail"));
	 					$("input#workspaceEmailHostname").val(getValues(result, "projEmailHost"));
	 					$("select#workspaceEmailActType").val(getValues(result, "projEmailActType"));
	 					$("input#workspaceProjEmailPwd").val(getValues(result, "projEmailPwd"));

	 					$("input#workspaceEmailPortNum").val(getValues(result, "projEmailPort"));
	 					$("select#workspaceEmailSecType").val(getValues(result, "projEmailSecNum"));
	 					if (getValues(result, "projEmailAttch") == "unChecked") {
	 						attchVal = false;
	 					} else {
	 						attchVal = true;
	 					}
	 					$("input#workspaceEmailAttach").attr("checked", attchVal);
	 					/*if (!isiPad) {
	 						scrollEmailConfigContent('emailConfigureDetails');
	 					}*/
	 					
	 				}
	 			});
	 		$("#loadingBar").hide();
	 		timerControl("");
	}else{
       $("#emailConfigureDiv").hide();
       var wT = $('#projSetting_' + projId).offset().top;
	   var woh = $(window).height() - wT;
	  
		var popH = $("div#projectDetailsDiv_" + projId).height();
		if (woh < popH) {
			var fh = popH - woh + 10;
			var fh1 = popH - woh + 5;
			$('#projectDetailsDiv_' + projId).css('margin-top','-' + fh + 'px');
			$('#arrowDiv').css('margin-top', fh1 + 'px');
		}
	}
	$("#loadingBar").hide();
	timerControl("");
 }
 
 function upDateProjAccess(projId) {
	$('#loadingBar').show();
	timerControl("start");
	$("div#userProfileDivContiners").css("display", "block");
	$("div#userDetailsDivContent").css("display", "block");
	$("div#userOptionsDivContent").css("display", "none");
	var windowHeight = $(window).height() + 100;
	$("div#userProfileDivContiners").css("display", "block");
	$("div#updateUserAccessData").css("display", "block");
	$("div#userOptionsDivContent").css("display", "none");
	$("div#updateUserAccess").css("width", "400px");
	$("div#updateUserAccess").css("margin-left", "300px");
	$("div#projectAccess").show();
	if ($.browser.safari) {
		$("div#projectAccess").css("float", "right");
		$("div#projectAccess").css("margin-left", "100px");
		//$("div#projectAccess").css("margin-top","-2px");
	}
	$("div#projectAccess").css("float", "right");
	$.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : {
					act : "loadWrkSpaceProjAccess",
					projId : projId
				},
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
		success : function(result) {
			parent.checkSessionTimeOut(result);
			var res = result.split("##$$##");
			$("div#updateUserAccessData").html(res[0]);
			if (localprojectType == 'sharedProject') {
				$("select#projectAccessType").attr('disabled', 'true');
			}
		}
	});
	$('#loadingBar').hide();
	timerControl("");
}

function clearCodes() {
	if ($('textarea#taskCompTextAreaVal').val().trim() == '') {
	} else {
		clearCommentedCodes()
		//parent.confirmFun(deleteCodes, "clear", "clearCommentedCodes");
	}
}

function clearCommentedCodes() {
	$('textarea#taskCompTextAreaVal').val('');
	$('textarea#taskCompTextAreaVal').focus();
}

function prepareCompanyCodeUI(jsonResult){
 var companyCodes="";
 	if(jsonResult == "[]"){
 	companyCodes = "<div style=\"float:left;margin-left:10px;\">No library codes to display</div>";
 	}
 var jsonData = jQuery.parseJSON(jsonResult);
    
    if(jsonData){
    	 var json = eval(jsonData);
    	 if(jsonData.length>0){
    	 	 for(var i=0;i<json.length;i++){
    	 	companyCodes += "<div id=\"quotesContainer_"+json[i].codeId+"\"  style=\"margin-bottom:5px;font-size:13px;background:white;min-height:50px;border-radius:5px 5px 5px 5px;border:1px solid #cccccc;color:#000000;font-family:'thoma';width:97%;margin-left:20px;overflow:auto;\" class=\"projAdminProjCont\">"
			  + "<div style=\"width:20px;float:left;margin-top:15px;margin-left:10px;\"><input type=\"checkbox\" class=\"compCheckValues\" id=\"check_"+json[i].codeId+"\"></div>"
			  + "<div style=\"float:left;width:95%;\"><span style=\"float:left;text-indent:4px;padding:10px;\">"+json[i].Codes+"</span></div>"
			  + "</div> ";
    	 	 }
    	 }
 	} 
 	$("#compQuotesList").html(companyCodes);
 }
 


 function prepareProjQuotes(jsonResult){
  var codesDisplay="";
 	if(jsonResult == "[]"){
 		codesDisplay = "<div style=\"float:left;margin-left:5px;\">No codes to display</div>" ;
	 }
 var jsonData = jQuery.parseJSON(jsonResult);
    if(jsonData){
    	 var json = eval(jsonData);
    	 if(jsonData.length>0){
    	 	 for(var i=0;i<json.length;i++){
    	 	
    	 	 codesDisplay +=  " <div id=\"quotesContainer_"+json[i].codeId+"\" style=\"width:100%;\" class = \"taskCompCodes actFeedHover\"> "
    	 	 				+ " <span title=\" "+json[i].Codes+" \" style=\"width:98%;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;margin:0px;\" id=\"editable_"+json[i].codeId+"\" class = \"taskCompCodeSpan taskCodeComm\">"+json[i].Codes+" </span>"
		  					+ " <div id=\"quoteReorderDiv_"+json[i].codeId+"\"  class=\"quoteReorderDivs\" style=\"margin-top:-3px;width:35px;border-radius:35px;box-shadow: 0 1px 4px 0 rgba(168, 168, 168, 0.6);border:1px solid #A1A1A1;position:absolute;display:none;height:35px;background-color:#ffffff;right:50%;padding:5px;z-index:5000;\" >"
					
							+ " <div align=\"center\" style=\"padding: 2px; margin: -2px auto; width: 14px; height: 12px;cursor:pointer;\" onclick=\"reorder('top','"+json[i].codeId+"');\" ><img src=\"images/arrow-top.png\" style=\"cursor:pointer;height:10px;width:10px;float:left;\"/></div>"
							+ " <div style=\"height:15px;display:none;\"><img src=\"images/Idea/arrow-left.png\" onclick=\"\" style=\"opacity:0.3;cursor:pointer;float:left;height:10px;width:10px;margin-top:4px;\"/><img src=\"images/Idea/arrow-right.png\" onclick=\"\" style=\"opacity:0.3;cursor:pointer;float:right;height:10px;width:10px;margin-top:4px;\"/></div>"
							+ " <div align=\"center\" style=\"width: 14px; height: 12px; padding: 2px; margin: 4px auto 0px;cursor:pointer;\" onclick=\"reorder('down','"+json[i].codeId+"');\" ><img src=\"images/arrow-down.png\" style='cursor:pointer;height:10px;width:10px;float:left;'/></div>"
							+ " </div>"
		  			        
		  			        + " <div id=\"subcommentDiv1_"+json[i].codeId+"\" class=\"taskCodeComm actFeedHoverImgDiv\"  style=\"float: left; padding-top: 3px;\"> ";
		  			        if($("input#globalOnclickCheck").val() == 'sharedProject'){
		  			        codesDisplay = codesDisplay  + " <img id=\"editTaskCodesId\" alt=\"Image\" style=\"padding-left:0px;cursor:pointer;margin-top:0px;\" class=\" img-responsive  Options_cLabelTitle \" src=\"images/more.png\" title=\"Options\" > ";
		  			        }else {
		  			        codesDisplay = codesDisplay + " <img id=\"editTaskCodesId\" alt=\"Image\" style=\"padding-left:0px;cursor:pointer;margin-top:0px;\" class=\" img-responsive  Options_cLabelTitle \" src=\"images/more.png\" title=\"Options\" onclick=\"editTaskCodesNew("+json[i].codeId+",'workspace');\"> ";
		  			        }
						   
							codesDisplay = codesDisplay + " </div>" 
					
							+ " <div id = \"editDivContainer_"+json[i].codeId+"\" class = \"editQCode\" style = \"height: 96%;margin: 0 0 1%;display: none;\"> "
						 	+ " <textarea  id = \"main_commentTextarea_"+json[i].codeId+"\" class = \"main_commentTxtArea\" style = \"height: 100%;width: 94%;\"></textarea>" 
		     			    + " <div onclick=\"saveEditQuotes("+json[i].codeId+")\"  align = \"center\" class = \"main_commentTxtArea_btnDiv\" style = \"height: 100%;width: 6%;background-color:#fff;\">" 
		        			+ " <img alt=\"Image\" style=\"cursor:pointer;\" src=\"images/post.png\" class=\"img-responsive\">"
		      			    + " </div>"
					        + " </div>"

					        + " <div class=\"actFeedOptionsDiv taskCodeComm\" id=\"subcommentDiv11_"+json[i].codeId+"\" style=\"display: none;margin: -5px 40px 0px 0px;\">" 
						    + " <div class=\"workSpace_arrow_right\" id = \"taskArrow_"+json[i].codeId+"\" style=\"margin-top: -3%; float: right; position: absolute; right: -22%;\">"

							+ " <img src=\"images/arrow.png\"></div> "
							
							+ "<div onclick=\"editQuotes("+json[i].codeId+");\" style=\"width:99%;height:auto;padding:0 0 3px ;cursor:pointer;float:left;border-bottom:1px solid #ccc;\">" 
							+ " <span class=\"Reply_cLabelText taskCodesEdit\">Edit</span>"
						    + " </div>"
						
						    + " <div onclick=\"showStageReorder("+json[i].codeId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-bottom:1px solid #ccc;\" >"
							+ "<span class=\"Edit_cLabelText iReorder taskCodesEdit\">Reorder</span>"
						    + " </div>"
						
						    + " <div  onclick=\"deleteQuotes("+json[i].codeId+");\" style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;\">"  
							+ " <span class=\"Task_cLabelText taskCodesEdit\">Delete</span>"
						    + " </div>"
					        + " </div>"
		  			+ " </div>"
    	 	 }
    	 }
    }
    return codesDisplay;
 } 
 
	function editTaskCodes(id){
		var id = $(obj).attr('id').split('_')[1];
			if ($('#taskCompsubcommentDiv11_'+id).is(":hidden")) {
	   			$('.actFeedOptionsDiv').hide();
	   			$('.actFeedReplyOptionsDiv').hide();
	   			$('#taskCompsubcommentDiv11_'+id).slideToggle(function(){
	   				$(this).css('overflow','');
	   			});
  			}else{
  			$('#taskCompsubcommentDiv11_'+id).slideToggle('slow');
  			} 
	   }
 
 function cancelQuotes() {
	$("div#taskCompletionMain").hide();
	$('textarea#main_commentTextarea').val("");
	$('#transparentDiv').hide();
	$('#wstansparentDiv').hide();
}

 function loadProjQuotes(projId, type) {
 		$('#loadingBar').show();
	    timerControl("start");
 		$("div#createTaskCodes").html("");
 		var UI =  "<div style=\"float:left;width:100%;margin:5px;\" class=\"taskCompCodeInputCss\" id=\"Task_taskCompCodeInput\">"
				+ "<textarea style=\"width: 86%;padding:8px;background:none;\" class=\"main_commentTxtArea\" id=\"inputTaskCodes\"></textarea>"
		     	+ "<div align=\"center\" style=\"width: 6%;\" class=\"main_commentTxtArea_btnDiv\" id=\"taskCodesCredt\" onclick=\"AddTaskCompQuotes("+projId+")\">"
		        +	"<img class=\"img-responsive Post_code_cLabelTitle\" title=\" \" src=\"images/workspace/post.png\" style=\"margin-top: 10px;cursor:pointer;\" alt=\"Image\">"
		      	+ "</div>"
		      	+ "<div align=\"center\" style=\"margin-top:5px;float:left;margin-left:1%;\" class=\"main_commentTxtArea_optDiv Library_code_cLabelTitle\" title=\" \" onclick=\"showCompQuotes()\" id=\"updateTaskFuns\">"
		        +	"<img class=\"img-responsive\" src= \"images/library.png\" style=\"cursor: pointer;\" alt=\"Image\">"
		        + "</div>"
		  	    + " </div>";
		 $("div#createTaskCodes").html(UI);
		 $("div#createTaskCodes").show();
	    $('#loadingBar').show();
		timerControl("start");
  		var dynamicHeight = getHeightDynamically('projectQuotes');
  		if(type == 'sysAdmin'){
  			dynamicHeight = dynamicHeight - 30;
  		}
  		if($("div.gearCss").css('display') == 'block' ) {
	  		$("div.gearCss").hide();
	  		$("div#arrowDiv").hide();
  		}
		if($("span#task_code_change").html() == 'TASK LIBRARY CODE'){
			$("span#task_code_change").html("");
			$("span#task_code_change").html("TASK COMPLETION CODES");
			$("div#Task_taskCompCodeInput").show();
			loadCustomLabel('settings');
	    }
	    if($("div#compCodeSaveCancel").css('display') == 'block' ){
			$("div#compCodeSaveCancel").hide();
	     }
		
		$('#task_main_commentTextarea').val('');
		$("div#templateContainer").hide();
		$("div#fullProjectDetailsContainer").hide();
		$("div#apporvalRequestContainer").hide();
		$("div#inviteUsersToProjectWorkspace").hide();
		$("div#templateContainer").hide();
		$("div#optionalDrivesContainer").hide();
		$("div#integrationContainer").hide();
		$("div#projectStatusContainer").hide();
		$("div#taskCompletionCodesContainer").show();
		//$("div#taskCompletionCodesContainer").css("height",dynamicHeight+"px");
			$.ajax({
				url : path + "/paLoad.do",
				type : "POST",
				data : {
							act : "loadProjQuotes",
							projectId : projId
						},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success : function(result) {
				     checkSessionTimeOut(result);
				     result=result.toString().replaceAll("CHR(50)","<br>").replaceAll("CH(70)","\\").replaceAll("CHR(26)",":");
					  var data = prepareProjQuotes(result);
					  $("div#taskCodeResultContainer").html("");
					  $("div#taskCodeResultContainer").html(data);
					  $("div#taskLibraryCodeContainer").hide()
					  $("div#taskCodeResultContainer").show();
					  var height = $("div#rightProjectContainer").height() - $("div#createTaskCodes").height() - 20 ;
					  $("div#taskCodeResultContainer").css('height',+height+'px');
					  popUpScrollBar('taskCodeResultContainer');
					  $('#loadingBar').hide();
					  timerControl("");
					  
					  var projType = $('#hiddenProjSettingType').val();
    				  if(projType !='myProject'){
    				     $('div.taskCodeComm').hide();
    				     $('#taskCodesCredt, #updateTaskFuns').attr('onclick','dontHavePrivilegeTC();');
    				     $('#inputTaskCodes').attr('disabled','true');
       			      }
				}
			});
			
			if($("input#globalOnclickCheck").val() == 'sharedProject'){
				$("img#editTaskCodesId").attr('onclick',"");
			}
			
 	}
 
 var workSpaceArrowDiv = "";
var workspaceArrowDivCss = "";
var localprojectType = "";
//var userRegType = "";
	
	function displayProjDesc(obj, projId, projectType){
	        $('#loadingBar').show();
	        timerControl("start");
	        $("div#projectSettingsContainerDiv").html("");
	        $("input#globalOnclickCheck").val(projectType);
			$("img#goBackToMainScreen").show();
			$("img#searchProject").hide();
			$("img#hiddenProject").hide();
			$("img#csvUpload, #myProInfo").hide();
			$("img#addProject").hide();
		 	$("div#tabContentDiv").hide();
		 	$("input#globalProjectId").val(projId);
		 	$("input#projUploadIdNew").val(projId);
		 	if($('div#projectDetailsContainer').css('display') == 'block'){
				$("div#projectDetailsContainer").remove();
		    }
		 	loadWskspaceProjDetails(projId, projectType, t,'workspace')
    }

	var projDescFlag=true;
	var projectdescresult="";
	function loadWskspaceProjDetails(projId, projectType, woh, position) {
		$("div#projectSettingsContainerDiv").html("");
		$("input#globalProjectId").val(projId);
		var findText="dummyLogo.png"
		var attchVal = "";
		if($('#myzoneBreadcrum').children('img.breadcrumImg').length<1){
			var bcrum =' <img class="breadcrumImg img-responsive" onerror="imageOnProjNotErrorReplace(this);" src="" onclick=""/>'
			               +'<span class="breadCrumProjName"></span>';
			$('#myzoneBreadcrum').append(bcrum);
		}
		$.ajax({
				url : path + "/workspaceAction.do",
				type : "POST",
				data : {
						act : "loadProjectDetails",
						projectId : projId,
						projectType : projectType
						},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success : function(result) {
					checkSessionTimeOut(result);
					var jsonResult = result.split('^&^%$#%$&')[0];
					var projectdescresult = result.split('^&^%$#%$&')[1];
					var temp = prepareSettingsIcon(jsonResult,position);
					$('#projUploadId').val(projId);
					$('#projUploadIdNew').val(projId);
					saveInviteData = "";
					var attchVal="";
              		$("div#projectSettingsContainerDiv").html(temp);
              		if(globprojectEmailAttachmentStatus == 'checked'){
						attchVal =true;
					}
					else{
						attchVal= false;
					}
              		$("input#emailAttachmentCheckVal").attr("checked",attchVal);
              		loadCustomLabel('settings');
              		$('#projNameHidden').val($('#projectName').val());
              		
              		$('#myzoneBreadcrum').children('span.breadCrumProjName').text($('#projectName').val());
 					$('#myzoneBreadcrum').children('img.breadcrumImg').attr('src',$('#hideDiv_'+projId).attr('src'));
              		
              		$('#leftProjectContainer,#rightProjectContainer').height($("div#projectSettingsContainerDiv").height()-60);
              		$("div#proDetId span").css('color','#000000');
              		$('#hiddenProjSettingType').val(projectType);
              		uploadPopUp('workspace');
              		loadCkEditor2(position,projId);
              		$('#hiddenProjSettingPosition').val(position);
					$("textarea#projDescription").val(getValues(projectdescresult,"projDesc"));
					
					if(companyType == "Social" ){
					   $('#wrkFId,#tccId').remove();
					}else if( companyPlan == 'Business_0' ){
						$('#wrkFId,#tccId').addClass("tabopacity").children('span').removeAttr('onclick');
						$(".tabopacity").attr('title','Upgrade to use this feature. Contact your System Administrator.');		
					}
					
					initProjectCalender();
				    CKEDITOR.instances.projDescription.setData(getValues(projectdescresult,"projDesc"));
				    projDescFlag=true;
					CKEDITOR.instances['projDescription'].on('blur', function(e) {
					    if (e.editor.checkDirty()) {
					       projDescFlag = false;
					    }
					});
				    $('#projStatusType option[value='+$("input#projStatusHidden").val()+']').attr('selected','selected');
				    
				    $('td.cke_contents').css('border','1px solid balck');
              		var check = $("input#checkForChangePermission").val();
              //----------------------------------------------------------> 
              //**** Important Note ****
              //---- Dont put any conditions in setting UI preparation method below scripts will do the job depending on project type - DJ. 
              // --------------------------------------------------------->
              		if(projectType != 'myProject'){
              		     $('div#appUserId').hide();
              		     $('#emailAttachmentDiv').hide();
              		     $('#projectName,#projectTagName').attr('disabled','true');
              		     $('#imgChgType').removeAttr('onclick');
              		     $('#projStatusType').attr('disabled','true');
              		     $('#upload_link_proj').hide();
              		     $("img#addAdministrators,img#addTeamMembers,img#addObserversNew").attr("onclick","").hide();  
        			     $('#createNewWorkFlow').attr('onclick','dontHavePrivilegeWF();');
        			     $('#taskCodesCredt, #updateTaskFuns').attr('onclick','dontHavePrivilegeTC();');
        			     
        			     $("button#chgSaveBtnClick").attr("onclick","").hide();   
                      }
                       
              		$("div#taskCompletionCodesContainer").hide();
              		$("div#projectStatusContainer").hide();
	 				$("div#inviteUsersToProjectWorkspace").hide();
	 				$("div#apporvalRequestContainer").hide();
					$("div#templateContainer").hide();
					$("div#integrationContainer").hide();
					$("div#fullProjectDetailsContainer").show();
				    $("div#projectSettingsContainerDiv").show();
              		
              		if(typeof(notfUserId) == "undefined")
              			notfUserId = "";
              		
              		if(typeof(notfProjId) == "undefined")
              			notfProjId = "";
              		
              		if(projectType == 'myProject' && typeof(notstype) != 'undefined' && notstype == "projStatusComment"){
                          commonFunctionality($('#projStatusId >span'),'status');
                    }
              		
              		if(notfProjId != "" && notfUserId != ""){
              		 	if(notType == 'workspace_approval'){
	       					commonFunctionality($('#appUserId >span'),'apporval');
	    			 	}
	    			} 
              		 
              		if(typeof(projActionType) != 'undefined' && projActionType != null  && projActionType != "" && projActionType == "status"){
              			 commonFunctionality($('#projStatusId >span'),'status');
              			 projActionType="";
			    	} 
              		
              		if(typeof(projActionType) != 'undefined' && projActionType != null  && projActionType != "" && projActionType == "projectDetails"){
             			 commonFunctionality($('#proDetId >span'),'projectDetails');
             			 projActionType="";
			    	} 
              		
              		if(typeof(projActionType) != 'undefined' && projActionType != null  && projActionType != "" && projActionType == "inviteUser"){
            			 commonFunctionality($('#invUserId >span'),'inviteUser');
            			 projActionType="";
			    	}
              		
              		if(typeof(projActionType) != 'undefined' && projActionType != null  && projActionType != "" && projActionType == "apporval"){
              			 commonFunctionality($('#appUserId >span'),'apporval');
              			 projActionType="";
   			    	}
              		
              		if(typeof(projActionType)!= 'undefined' && projActionType != null  && projActionType != "" && projActionType == "workFlow"){
           			 commonFunctionality($('#wrkFId >span'),'workFlow');
           			 projActionType="";
			    	}
              		
              		if(typeof(projActionType) != 'undefined' && projActionType != null  && projActionType != "" && projActionType == "taskCodes"){
           			 commonFunctionality($('#tskComCod >span'),'taskCodes');
           			 projActionType="";
			    	}
              		
              		if(typeof(projActionType)!= 'undefined' && projActionType != null  && projActionType != "" && projActionType == "optionalDrives"){
           			 commonFunctionality($('#optDrive >span'),'optionalDrives');
           			 projActionType="";
			    	}
              		
              		if(typeof(projActionType)!= 'undefined' && projActionType != null  && projActionType != "" && projActionType == "integration"){
              			 commonFunctionality($('#wrkInt >span'),'integration');
              			 projActionType="";
   			    	}
              		
	    			if(position == 'team'){
	    			   commonFunctionality($('#invUserId >span'),'inviteUser');
	    			   $('#proDetId > span, #appUserId > span, #wrkFId > span, #tskComCod > span, #optDrive > span, #wrkInt > span, #projStatusId >span').removeAttr('onclick');
	    			   $('#proDetId > span, #appUserId > span, #wrkFId > span, #tskComCod > span, #optDrive > span, #wrkInt > span, #projStatusId >span').css('cursor','');
	    			}
					$('#loadingBar').hide();
					timerControl("");
				}
			});
			return true;
 }
 

 
 function dontHavePrivilegeWF(){
    alertFun(getValues(companyAlerts,"Alert_NoPrivForTemp"), 'warning');
 }
 function dontHavePrivilegeTC(){
    alertFun(getValues(companyAlerts,"Alert_NoPrivTaskComp"), 'warning');
 }
 function imageOnProjErrorReplace(obj) {
	$(obj).attr('src', lighttpdpath + "/projectimages/dummyLogo.png");
 }

var defaultphotoId = "";
var pfileName = "";

function closeProjDetailPopUp() {
	defaultphotoId = "";
	pfileName = "";
	$("div.wrkspaceProjDetails").hide();
	$('div.wrkspaceProjDetails').html('');
	$("#emailConfigureDetails").mCustomScrollbar("update");
	setTimeout(function() {
		$("div#myProjDetailData").mCustomScrollbar("update");
		$("div#sharedProjDetailData").mCustomScrollbar("update");
	}, 50);
 }
 
 
 function ProjectUploadPopup() {
	$('#ProjectUploadOptns').slideToggle("slow");
}

function onMouseOverDiv(obj) {
	$(obj).addClass("mouseOvrClass");
}

function onMouseOutDiv(obj) {
	$(obj).removeClass("mouseOvrClass");
}

function  showHiddenProjects(){
				$('#loadingBar').show();
				$("#hiddenProject").attr('src','images/showhidden.png');
				$('#hiddenProject').attr('title','show active projects');
				$("#hiddenProject").attr("onclick","showActiveProjects()");
				searchProjType = 'hidden';
				timerControl("start");
				$.ajax({
				url : path + "/workspaceAction.do",
				type : "POST",
				data : { act : "showHiddenProjects",
							sortType: sortType,
							txt: searchVal,
							type: searchType
						 },
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success : function(result) {
					checkSessionTimeOut(result);
				prepareHiddenprojectUI(result);
				sortType = '';
				searchVal = '';
				searchType = '';
			    $('#loadingBar').hide();
				timerControl("");
				}
			});
}

function prepareHiddenprojectUI(jsonResult){
	
var projectUI="";
	   	if(jsonResult == "[]"){
	   			var temp="No hidden projects available"
	   			projectUI = "<div\"><h4>"+temp+"</h4></div>";
	   }
	    var jsonData = jQuery.parseJSON(jsonResult);
	   	 if(jsonData){
    	 var json = eval(jsonData);
    	 if(jsonData.length>0){
    	 	 for(var i=0;i<json.length;i++){
    	 	 projectUI += "<div class=\"projImgMainDiv\"> " 	
    	 	 		   + " <div id=\"projectImage_"+json[i].projId+"\" onclick=\"recentProjectInsert('"+json[i].projId+"')\" class=\"projImageContainer\"> "
    	 	 		   + " <div class=\"settingIcon\"> "
    	 	 		   + "<img id=\""+json[i].projId+"\"class=\"Owned_by_cLabelTitle shareUserIcon\" projectType='"+json[i].project_type+"' onclick=\"showProjectPop(this);\" projectid="+json[i].projId+"  src=\"images/myprojects.png\"  title=\"owned by\">"
    	 	 		   + " <img id=\"projSetting_"+json[i].projId+"\"  class=\"wrkSpaceProjOption\" src=\"images/settings.png\" style=\"float:right;\" title=\"Project settings\" onclick = \"displayProjDesc(this,"+json[i].projId+",'myProject');\" >"
    	 	 		   + "</div>" 
    	 	 		   + "<img id=\"projectImageId_"+json[i].projId+"\" class=\"psubscribed projImageCls\" onerror=\"javascript:imageOnProjNotErrorReplace(this);\" onclick=\"readMoreProjects(this);\" title=\""+json[i].projName+"\" src=\" "+json[i].proj_img_url+" \">"
    	 	 		   + " <div id=\"projectName_"+json[i].projId+"\" class=\"workspaceProjTitle prjName\" align=\"center\" data-placement=\"bottom\" data-toggle=\"tooltip\" title=\" "+json[i].projName+" \">" 
    	 	 		   + " " +json[i].projName+ ""
    	 	 		   + "</div>" 
    	 	 		   + "</div>" 
    	 	 		   + "   <div class=\"wrkspaceProjDetails\"  id=\"projectDetailsDiv_"+json[i].projId+"\" style=\"display:none;\"> </div>"
    	 	 		  
    	 	 		   + "</div>" ;
    	 	 	}
			}
	}
	$("#tabContentDiv").html("");
	$("#tabContentDiv").html(projectUI);
}


 /*vasu my functionality ends here.*/

/* popup functionality for listing of project owner */

var myProj_arrowDiv="";
var global_arrowDiv_css="";
function prjUserData(id,projType,topOffset, wT){
	
	$("#loadingBar").show();  
	timerControl("start");
	
	$('#popDiv_project_'+id).html("");
	var oh=$(window).height()-topOffset;

	let jsonbody = {
		"project_id" : id,
		"user_id" : userIdglb,
		"companyId" : companyIdglb
	}
	checksession();
		$.ajax({
		      	url: apiPath+"/"+myk+"/v1/fetchProjectDetails",
				type:"POST",
				dataType:'json',
				contentType:"application/json",
				data: JSON.stringify(jsonbody),
				/////data:{action:"fetchProjectDetails",projectId:id,projectType:projType},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						},	 
				success:function(result){
					parent.checkSessionTimeOut(result);
					//$('#projectDetailsDiv_'+id).html(result);
					
					$('div#agileProjects div.mCS_no_scrollbar').css('height','100%');
					$('#projectDetailsDiv_'+id).show();
					$("#projectOwnerList").css('max-height','210px');
					$('#projectDetailsDiv_'+id).find('#arrowDiv').addClass(workSpaceArrowDiv);
					var popH=$('#projectDetailsDiv_'+id).height();
					prepareShareOwnedPopupUi(result,projType,id);
					loadCustomLabel("Project Details");
					 if(oh<popH){
						 
					     var fh=popH-6;
					     var fh1=popH-85;
					 
					     $('#projectDetailsDiv_'+id).css('margin-top','-'+fh+'px');
					     $('#arrowDiv').css('margin-top',+fh1+'px');
					  
					 }else{
					      $('#projectDetailsDiv_'+id).css('margin-top','-97px');
					    } 
					 popUpScrollBar('projectOwnerList');
				     $(".mCustomScrollBox").css('max-height','200px');
					$("#loadingBar").hide();
					timerControl("");
				}
	        });
}

function prepareShareOwnedPopupUi(result,projType,id){

	var ui = "";
	$('#projectDetailsDiv_'+id).html('');
	$('#publicProjOwnersList_'+id).html('');
  	if(result){
		var json = eval(result);
		ui+="<div id='arrowDiv' class='arrow-left' ><img src='images/leftrightarrow.png'  style='float:right;' /></div>"
		+"<div style='padding:8px 8px;color:#808080;font-family:tahoma;font-size:13px;width:215px;float:left;background-color:#fff; border: 1px solid #B7BBBE;box-shadow: -2px 4px 10px #cccccc;border-radius: 3px;'>"
		+"<div style=\"height: 25px;border-bottom:1px solid #BFBFBF;padding-left:5px;\">"
		if(projType == "my_project"){
			ui+="<span style='float:left;max-width:100px;height: 18px;font-family:opensanscondbold;font-size:14px;overflow:hidden;' class='Owned_by_cLabelText' ></span><span style='float:left;' >&nbsp: </span>"
		}else{
			ui+="<span style='float:left;max-width:100px;height: 18px;font-family:opensanscondbold;font-size:14px;overflow:hidden;' class='Shared_by_cLabelText Shared_by_cLabelTitle defaultExceedCls' title=' '></span><span style='float:left;' >&nbsp: </span>"
		}
		ui+="<div class='Close_cLabelTitle' style='float: right; margin-top: 2px; padding: 1px 5px 0px 5px;cursor:pointer;' onclick='updateTopicCancel();' ><img src='images/close.png' style='float:right;' /></div>"
		+"</div>"
		+"<div id='projectOwnerList' style='max-height:200px;padding-top:10px;overflow-y:auto;'>"
		for(var i = 0 ; i < json.length; i++){
			ui+="<div style='height: 50px;padding-left:5px;'>"
			+"<div class='ideaFeedImage' style='box-shadow: none;' title='"+json[i].name+"'>"
			+"<img id='' class='imgProp' title='"+json[i].name+"' style='width:36px;height:36px;cursor:pointer;border-radius: 50%;' onerror='imageOnErrorReplace(this)' src='"+json[i].imageUrl+"?"+d.getTime()+"'>"
			+"</div>"
			+"<div style='float:left;padding-left:8px;margin-top:2px;width:135px;'><span style='float:left;width:100%;' class='textExceedCss' title='"+json[i].name+"'>"+json[i].name+"</span></div>"
			+"</div>"
		}	

		ui+="</div>"
		+"</div>"
		$('#projectDetailsDiv_'+id).append(ui);
		$('#publicProjOwnersList_'+id).append(ui);
	}	

}
  

	function showProjectPop(obj){
		var projId=$(obj).attr('projectid');
		$('div[id^="projectDetailsDiv_"]').hide();
	   	if($('#projectDetailsDiv_'+projId).is(':hidden')){
		$('.popDivClass').hide().html("");
			/*var w=$(obj).parent().width();
		     
		     var p_type=$(obj).attr('projectType');
			 var w=$(obj).parent().width();
			 var h=$(window).width();
			     h=h/2;
			 var l=$(obj).offset().left;
			 var t=$(obj).offset().top;
			 selectedTopicemailId=",";
		     deletedTopicUserId=",";
		     var oh=$(window).height()-t;
		     global_arrowDiv_css="";
		     var popHeight="";
		     var popArrow="";
		     
		     
		    
		     
	      //$('#publicProjOwnersList_'+projId).css('margin-top','-110px');
			 if(l<h){
				 
				 $('#projectDetailsDiv_' + projId).css('left', l-6+'px');
				 $('#projectDetailsDiv_' + projId).css('right', '');
				 $('#projectDetailsDiv_' + projId).css('margin-left', '-9px');


				 
			   //  $('#projectDetailsDiv_'+projId).css('left',77+'px');
			    // $('#projectDetailsDiv_'+projId).css('right','');
			     myProj_arrowDiv="arrow-left";
			     
			   }else{
			     var or=$(window).width() - (l + $(obj).outerWidth(true));
			     $('#projectDetailsDiv_'+projId).css('left','');
			   
			  //   $('#projectDetailsDiv_'+projId).css('right',130+'px');
			     $('#projectDetailsDiv_'+projId).css('right',or-14+'px');

			     myProj_arrowDiv="arrow-right";*/
			var p_type = $(obj).attr('projectType');
			selectedTopicemailId=",";
		    deletedTopicUserId=",";
			/*var menuHeight = $(window).width();
	    	menuHeight = menuHeight / 2;
	    	
			var wL = $(obj).offset().left;
			var wT = $(obj).offset().top;
			var woh = $(window).height() - wT;
			workspaceArrowDivCss = "";
			if (wL < menuHeight) {
				$('#projectDetailsDiv_' + projId).css('left', wL-18+'px');
				$('#projectDetailsDiv_' + projId).css('right', '');
				workSpaceArrowDiv = "arrow-left1";
			} else {
				workSpaceArrowDiv = "arrow-right1";
				var or=$(window).width() - (wL + $(obj).outerWidth(true));
		     	$('#projectDetailsDiv_'+projId).css('left','');
		     	$('#projectDetailsDiv_'+projId).css('right',or-124+'px');
			}
			$('#projectDetailsDiv_'+projId).toggle();
			*/
			
			
			 var menuHeight = $(window).width();
		    menuHeight = menuHeight / 2;
			var wL = $(obj).offset().left;
			var wT = $(obj).offset().top;
			var woh = $(window).height() - wT;
			workspaceArrowDivCss = "";
			if (wL < menuHeight) {
				$('#projectDetailsDiv_' + projId).css('left', wL-10+'px');
				$('#projectDetailsDiv_' + projId).css('right', '');
				$('#projectDetailsDiv_' + projId).css('margin-left', '-9px');
				workSpaceArrowDiv="arrow-left";
				$('#projectDetailsDiv_'+projId).toggle();
			} else {
				workSpaceArrowDiv="arrow-right";
				var or=$(window).width() - (wL + $(obj).outerWidth(true));
			     $('#projectDetailsDiv_'+projId).css('left','');
			     $('#projectDetailsDiv_'+projId).css('right',or-25+'px');
			     $('#projectDetailsDiv_'+projId).toggle();
			}
			
			
			$('#arrowDiv').addClass(workSpaceArrowDiv);
		 prjUserData(projId,p_type,t,wT);
	  }else{
		   $('#projectDetailsDiv_'+projId).toggle(function(){
		   $(this).html('');
	   });
	}	
}
	
function loadCustomLabel(place){
	console.log("WSplace---"+place);
	  var keyList="";
	  if(place=="Project Details"){
  		keyList="Shared by,Close,Owned by";	
    }else if(place=="WF TemplateList"){
    		keyList="Close,Create,Update,Copy,View,Delete,Cancel,WF Name,WF AddLevel,WF AttachDocument,WF NoAttachDocument,WF Next,WF Previous,WF Level";	
    }else if(place=='onload'){
	     keyList="TEAM ZONE,my inZone,Workspace,Subscribe,Subscribe Project,Create inZone,Upload csv file,Show Hidden inZone,Search and sort,Ws Search,Recently Used,Sentiment,Status,Name,My Workspaces Visible,My Workspaces Hidden,All My Workspaces,Title,Owned by,Upload CSV file,Download sample CSV file,SAVE,Team Zone,My Zone,Enterprise Zone,Start New inZone,Existing inZone,Contacts,Tasks,DOCUMENTS,Messages,Notes,Blogs,Gallery,Analytics,System Admin,Integration,All Scrums,Show active projects";	
	}else if(place=='settings'){
	     keyList="Create Workflow,Request From,Add Owners,Add Observers,Add TeamMembers,Archive,ACTIVE,Upload from system,Upload from gallery,inZone Details,SAVE,Invite Users,Approval,WORKFLOW TEMPLATE,TASK COMPLETION CODES,Name,Project Status,Optional Drives,Integration,Privacy,WS Active,Start,WS End,Hash Code,starts at,ends at,WS Hide,Comment,Owner,WS Team Members,Observers,WS Lib Code,Post code,Library code,save attachment";	
	}
    
	    var labelData= companyLabels;
	 
		var labelKeyList=keyList.split(',');
		var labelValue="";
		var labelKey="";
		for(var i=0;i<=labelKeyList.length-1;i++){
			labelKey=labelKeyList[i].replaceAll(' ','_');
			labelKey=labelKey.replaceAll('/','_');
			labelKey=labelKey.replaceAll('-','_');
			labelValue=getValues(labelData,labelKey);
			
			$('.'+labelKey+'_cLabelTitle').attr('title',labelValue);
			$('.'+labelKey+'_cLabelText').text(labelValue);
			$('.'+labelKey+'_cLabelLink').text(labelValue);
			$('.'+labelKey+'_cLabelPlaceholder').attr('placeholder',labelValue);
			$('.'+labelKey+'_cLabelHtml').html(labelValue);
			$('.'+labelKey+'_cLabelValue').val(labelValue);
	
	}

}

function imageOnErrorReplace(obj) {
	Initial(obj);
}

function Initial(obj){
	$(obj).initial({
		name:$(obj).attr("title"),
		height:64,
		width:64,
		charCount:2,
		textColor:"#fff",
		fontSize:30,
		fontWeight:700
	}); 
}
  
function updateTopicCancel(){
    $('.wrkspaceProjDetails').html('').hide();
  }

/* end of pop Up functionality  */

/* save or update project details ends */

var global_arrowDiv = "";
var global_arrowDiv_css = "";

function PublicProjOwners(obj, projId) {
  if($('#publicProjOwnersList_' + projId).is(':hidden')){
	$('.wrkspaceProjDetails').hide();
	$('.wrkspaceProjDetails').html("");
	/*var w = $(obj).parent().width();
	var h=$(window).width();
	h = h / 2;
	var l = $(obj).offset().left;
	var t = $(obj).offset().top;
	selectedTopicemailId = ",";
	deletedTopicUserId = ",";
	var oh = $(window).height() - t;
	global_arrowDiv_css = "";
	var popHeight = "";
	var popArrow = "";

	// $('#publicProjOwnersList_'+projId).css('margin-top','-110px');
	if (l < h) {
		$('#publicProjOwnersList_' + projId).css('left', l-6 + 'px');
		$('#publicProjOwnersList_' + projId).css('right', '');
		$('#publicProjOwnersList_' + projId).css('margin-left', '-9px');
		global_arrowDiv = "arrow-left";
	} else {
		var or = $(window).width() - (l + $(obj).outerWidth(true));
		$('#publicProjOwnersList_' + projId).css('left', '');
		$('#publicProjOwnersList_'+projId).css('right',or-14+'px');
		global_arrowDiv = "arrow-right";
		$('#publicProjOwnersList_'+projId).toggle();
	}*/
	
	
	
	 var menuHeight = $(window).width();
		    menuHeight = menuHeight / 2;
			var wL = $(obj).offset().left;
			var wT = $(obj).offset().top;
			var woh = $(window).height() - wT;
			workspaceArrowDivCss = "";
			if (wL < menuHeight) {
				$('#publicProjOwnersList_' + projId).css('left', wL-10+'px');
				$('#publicProjOwnersList_' + projId).css('right', '');
				$('#publicProjOwnersList_' + projId).css('margin-left', '-9px');
				global_arrowDiv="arrow-left";
				$('#publicProjOwnersList_'+projId).toggle();
				$("#arrowDiv").css('margin-top','9px');
			} else {
				global_arrowDiv="arrow-right";
				var or=$(window).width() - (wL + $(obj).outerWidth(true));
			     $('#publicProjOwnersList_'+projId).css('left','');
			     $('#publicProjOwnersList_'+projId).css('right',or-15+'px');
			     $('#publicProjOwnersList_'+projId).toggle();
			     $("#arrowDiv").css('margin-top','9px');
			}
	
	
	
        updateSharedProject(projId, wT);
    }else{
    $('#publicProjOwnersList_' + projId).toggle(function(){
      $(this).html('');
    });
  }	
	
}

function updateSharedProject(id, topOffset) {
	$("#loadingBar").show();
	timerControl("start");
	
	$('#popDiv_project_' + id).html("");
	let jsonbody = {
		"project_id" : id,
		"user_id" : userIdglb,
		"companyId" : companyIdglb
	}
	checksession();
		$.ajax({
		      	url: apiPath+"/"+myk+"/v1/fetchProjectDetails",
				type:"POST",
				dataType:'json',
				contentType:"application/json",
				data: JSON.stringify(jsonbody),
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
		success : function(result) {
			
			$('#arrowDiv').addClass(global_arrowDiv);
			prepareShareOwnedPopupUi(result,"my_project",id);
			loadCustomLabel('onload');
			//$('#publicProjOwnersList_' + id).css('margin-top', '-110px');
			var publicProjH = $('#publicProjOwnersList_' + id).height();
			var oh = $(window).height() - topOffset;

			if (oh < publicProjH) {
				var fh = publicProjH -6;
				var fh1 = publicProjH - 85;

				$('#publicProjOwnersList_' + id).css('margin-top','-' + fh + 'px');
				$('#arrowDiv').css('margin-top', fh1 + 'px');
			}else{
			    $('#publicProjOwnersList_' + id).css('margin-top','-97px');
			}
			popUpScrollBar('projectOwnerList');
			$(".mCustomScrollBox").css('max-height','200px');
			$('#publicProjOwnersList_' + id).show();
			/* 
			$("#availProjOwnerList").css('max-height','150px');
			popUpScrollBar('availProjOwnerList');
			$(".mCustomScrollBox").css('max-height','140px'); */
			$("#loadingBar").hide();
			timerControl("");
		}
	});
}




function replyDivData(obj) {
	var id = $(obj).attr('id').split('_')[1];
	if ($('#quotesOption_' + id).is(":hidden")) {
		$('.codeOptionsDiv').hide();
		$('.actFeedReplyOptionsDiv').hide();
		$('#quotesOption_' + id).slideToggle();
	} else {
		$('#quotesOption_' + id).slideToggle('slow');
	}
}

function AddTaskCompQuotes(projectId) {

			if($("input#globalOnclickCheck").val() == 'sharedProject'){
				$("div#taskCodesCredt").attr("onclick", "")
				$("div#updateTaskFuns").attr("onclick", "")
			}
			
			else {
				$('#loadingBar').show();
			    timerControl("start");
				var codes = $('textarea#inputTaskCodes').val().trim();
				if (codes == null || codes == '') {
					alertFun(getValues(companyAlerts,"Alert_EntCode"), 'warning');
					$('#loadingBar').hide();
					timerControl("");
					return false;
				}
				$.ajax({
					url : path + "/paLoad.do",
					type : "POST",
					data : {
								act : "insertQuotesToDb",
								projectId : projectId,
								quotes : codes
							},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success : function(result) {
						checkSessionTimeOut(result);
						if (result == 'success') {
							 compCodesFlagChanges = true;
							 alertFun(getValues(companyAlerts,"Alert_CodeAdd"), 'warning');
						$('textarea#inputTaskCodes').val("");
						loadProjQuotes(projectId);
					} else {
						alertFun(getValues(companyAlerts,"Alert_CodeNotAdd"), 'warning');
					}
					$('#loadingBar').hide();
					timerControl("");
				}
			});
	 }
}


function closeCreateQuotes() {
	$("#createNewCodesDiv").hide();
	$("#wstansparentDiv").hide();
	$("#transparentDiv").hide();

}

		
	/*Code : SAGAR , function to close the invite user popup details starts here & also user image replace.*/
	function goPrevious() {
	$("div#inviteUserWrkspaceContiner").css('display', 'none');
	$("div#inviteUserAccess").hide();
	$("div#wstansparentDiv").hide();
	parent.$("div#transparentDiv").hide();
	$("div#userListProjectName span").text("");
	addedUserIds = "";
	if($('#adminUserListContainer').css('display') == 'block'){
		$("#adminUserListContainer").hide();
    }
	}
   
   function closeCreateProj() {
	$("div#userProfileMain").css('display', 'none');
	$("div#createNewWrkSpaceProject").css('display', 'none');
	parent.$('#transparentDiv').hide();
	$('#wstansparentDiv').hide();
	$("div#updateUserAccess").hide();
	$("div#inviteUserAccess").hide();
	$("div#inviteUserMainDiv").hide();
	$('div#projDescMain').css('display','none');
	$("div#editorDiv").html("");
	$("input#wrkspaceProjName").val("");
	$("input#wrkspaceProjName").css("width", "80%");
	//$("div#userProfileMain").mCustomScrollbar("update");
	if($('#adminUserListContainer').css('display') == 'block'){
		$("#adminUserListContainer").hide();
    }
	$("div#userListProjectName span").text("");
    }
    
    function imageOnErrorReplaceUser(obj) {
	/*$(obj).attr('src', "" + lighttpdPath + "userimages/userImage.png");*/
    	Initial(obj);
    }
    
    function Initial(obj){
    	$(obj).initial({
    		name:$(obj).attr("title"),
    		height:64,
    		width:64,
    		charCount:2,
    		textColor:"#fff",
    		fontSize:30,
    		fontWeight:700
    	}); 
    }

	function showInviteUserSubPropUp(obj,obserListDiv, obserArrow,teamListDiv,teamArrow) {
		$("button#chgSaveBtnClick").attr("onclick", "").attr("onclick", "callUsersForProjcts();");
		$('#SubscribeUserlistContainer').show();
		$('#subscribeUserArrow').show();
		$('#wsInviteTransparent').show();
		var buttonOffsetTop = $('#addObservers').offset().top;
		var inviteUserOffset = $('#inviteUserMainDiv').offset().top;
	   	var popUpHeight = $('#inviteUserMainDiv').height();
	   	var totalHeight = inviteUserOffset + popUpHeight;
	   	var offHeight = totalHeight - buttonOffsetTop;
	   	var halfoffHeight = (offHeight / 2) + 20;
	   	if(offHeight > 300){
	   		if($('div#inviteUserContainer').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
				///$('#SubscribeUserlistContainer').css({'margin-left': '399px','top': offHeight / 5 + 10 + 'px'});
		  	}else{
		    	///$('#SubscribeUserlistContainer').css({'margin-left': '399px','top': offHeight / 5 + 10 + 'px'});
		  	}
	   		
	   		var popUpOffset = $('#SubscribeUserlistContainer').offset().top;
	   		var arrOffset = buttonOffsetTop - popUpOffset - 25;
	   		///$('#subscribeUserArrow').css({'right': '259px','top': arrOffset+ 'px'});
	   		
	   	}else{
	   		if($('div#inviteUserContainer').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
				///$('#SubscribeUserlistContainer').css({'margin-left': '399px','top': '204px'});
		  	}else{
	   			////$('#SubscribeUserlistContainer').css({'margin-left': '399px','top': '204px'});
	   		}
	   		var popUpOffset = $('#SubscribeUserlistContainer').offset().top;
	   		var arrOffset = buttonOffsetTop - popUpOffset - 25;
	   		////$('#subscribeUserArrow').css({'right': '259px','top': arrOffset+ 'px'});
	   	}
	    $("#addSubscribeUsersParticipants").hide();
		$('.stdInvUsersPopUp').hide();
		$('.stdInvUsersPopUp1').hide();
	}
	
	/* invite user function */
	var saveInviteData = "";
	function AddinivitedUsers() {
	  saveInviteData = saveInviteUserData();
	
    if (saveInviteData == "" && userDeleted == false) {
		parent.alertFun(getValues(companyAlerts,"Alert_AddUsr"), "warning");
	} else if (saveInviteData != "") {
		$('#loadingBar').show();
		timerControl("start");
		saveInviteData = "<userData>" + saveInviteData + "</userData>";
		$.ajax({
			url : path + "/paLoad.do",
			type : "POST",
			data : {
						act : "saveInivtedEntUsersToProj",
						saveInviteData : saveInviteData,
						projId : wrkFlowProjId
					},	
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success : function(data) {
				parent.checkSessionTimeOut(data);
				saveInviteData = "";
				inviteUserXmlData = "";
				//closeCreateProj();
				$('#loadingBar').hide();
				timerControl("");
				$(".findUsersList").css("background","");
				addedUserIds = "";
			}
		});
	}
	userDeleted = new Boolean(false);
	}
	
	
	function saveInviteUserData() {
	var userId = "";
	var inviteUserXmlData = "";
	var proj_user_id = "";
	var userList = "";
	var userFname = "";
	var userLName = "";
	var userPassword = "";
	var userLoginName = "";
	var userEmailId = "";

	$('#sysAdminUserContainer .poNewUser')
			.each(
					function() {
						userId = $(this).find('input.po_userId').val();
						actionType = $(this).find('input.po_prevStatus').val();
						proj_user_id = $(this).find('input.po_proj_user_id')
								.val();
						userList = $(this).find('input.po_user_list').val();
						userFname = $(this).find('input.po_user_Fname').val();
						userLName = $(this).find('input.po_user_Lname').val();
						userPassword = $(this).find('input.po_user_Password')
								.val();
						userLoginName = $(this).find('input.po_user_LoginName')
								.val();
						userEmailId = $(this).find('input.po_user_Email').val();

						inviteUserXmlData += "<user userid=\"" + userId + "\">";
						inviteUserXmlData += "<action>PO</action>";
						inviteUserXmlData += "<projUserId>" + proj_user_id
								+ "</projUserId>";
						inviteUserXmlData += "<userList>" + userList
								+ "</userList>";
						inviteUserXmlData += "<userFname>" + userFname
								+ "</userFname>";
						inviteUserXmlData += "<userLName>" + userLName
								+ "</userLName>";
						inviteUserXmlData += "<userPassword>" + userPassword
								+ "</userPassword>";
						inviteUserXmlData += "<userLoginName>" + userLoginName
								+ "</userLoginName>";
						inviteUserXmlData += "<userEmailId>" + userEmailId
								+ "</userEmailId>";
						inviteUserXmlData += "</user>";
					});
 
	$('#sysAdminTeamMainDiv .tmNewUser')
			.each(
					function() {
						userId = $(this).find('input.tm_userId').val();
						actionType = $(this).find('input.tm_prevStatus').val();
						proj_user_id = $(this).find('input.tm_proj_user_id')
								.val();
						user_list = $(this).find('input.tm_user_list').val();

						userFname = $(this).find('input.tm_user_Fname').val();
						userLName = $(this).find('input.tm_user_Lname').val();
						userPassword = $(this).find('input.tm_user_Password')
								.val();
						userLoginName = $(this).find('input.tm_user_LoginName')
								.val();
						userEmailId = $(this).find('input.tm_user_Email').val();

						inviteUserXmlData += "<user userid=\"" + userId + "\">";
						inviteUserXmlData += "<action>TM</action>";
						inviteUserXmlData += "<projUserId>" + proj_user_id
								+ "</projUserId>";
						inviteUserXmlData += "<userList>" + userList
								+ "</userList>";
						inviteUserXmlData += "<userFname>" + userFname
								+ "</userFname>";
						inviteUserXmlData += "<userLName>" + userLName
								+ "</userLName>";
						inviteUserXmlData += "<userPassword>" + userPassword
								+ "</userPassword>";
						inviteUserXmlData += "<userLoginName>" + userLoginName
								+ "</userLoginName>";
						inviteUserXmlData += "<userEmailId>" + userEmailId
								+ "</userEmailId>";
						inviteUserXmlData += "</user>";
					});

	$('#sysAdminSubscribeMainDiv .suNewUser')
			.each(
					function() {
						userId = $(this).find('input.su_userId').val();
						actionType = $(this).find('input.su_prevStatus').val();
						proj_user_id = $(this).find('input.su_proj_user_id')
								.val();
						userList = $(this).find('input.su_user_list').val();

						userFname = $(this).find('input.su_user_Fname').val();
						userLName = $(this).find('input.su_user_Lname').val();
						userPassword = $(this).find('input.su_user_Password')
								.val();
						userLoginName = $(this).find('input.su_user_LoginName')
								.val();
						userEmailId = $(this).find('input.su_user_Email').val();

						inviteUserXmlData += "<user userid=\"" + userId + "\">"
						inviteUserXmlData += "<action>SU</action>";
						inviteUserXmlData += "<projUserId>" + proj_user_id
								+ "</projUserId>";
						inviteUserXmlData += "<userList>" + userList
								+ "</userList>";
						inviteUserXmlData += "<userFname>" + userFname
								+ "</userFname>";
						inviteUserXmlData += "<userLName>" + userLName
								+ "</userLName>";
						inviteUserXmlData += "<userPassword>" + userPassword
								+ "</userPassword>";
						inviteUserXmlData += "<userLoginName>" + userLoginName
								+ "</userLoginName>";
						inviteUserXmlData += "<userEmailId>" + userEmailId
								+ "</userEmailId>";
						inviteUserXmlData += "</user>";

					});

	
	$('#sysAdminUserContainer').children('[id^=AdminUsersContainer_]').removeClass("poNewUser");
	$('#sysAdminTeamMainDiv').children('[id^=TeamMembersContainer_]').removeClass("tmNewUser");
	$('#sysAdminSubscribeMainDiv').children('[id^=SubscribeUsersContainer_]').removeClass("suNewUser");
	return inviteUserXmlData;
  }

     
		function goBack() {
			$("div#createNewWrkSpaceProject").hide();
			$("div#userProfileMain").hide();
			parent.$('#transparentDiv').hide();
			$('#wstansparentDiv').hide();
			$("div#updateUserAccess").hide();
			$("#uploadCsv4NewProj").hide();
		}
		
		var deleteId = "";
		function deleteQuotes(codeId) {
				deleteId="";
				deleteId = codeId;
				confirmFun(getValues(companyAlerts,"Alert_DelCodeConfirm"), "delete","deleteProjectQuotes");

		}
		
		function deleteProjectQuotes() {
			$('#loadingBar').show();
			timerControl("start");
			var projectId = $("input#globalProjectId").val();
				$.ajax({
						url : path + "/paLoad.do",
						type : "POST",
						data : {
									act : "deleteProjQuotes",
									deleteId : deleteId,
									projectId : projectId
								},
						error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
						success : function(data) {
							checkSessionTimeOut(data);
							if (data == "success") {
								alertFun(getValues(companyAlerts,"Alert_CodeDel"), 'warning');
								loadProjQuotes(projectId);
							} else {
								alertFun(getValues(companyAlerts,"Alert_CodeNotDel"), 'warning');
								$('#loadingBar').hide();
								timerControl("");
							}
						}
					});
		}
		
	var CodeId="";
	function editQuotes(codeId){
		var value = $("#editable_"+codeId+"").html();
		$('#subcommentDiv11_'+codeId).css({'display':'none'});
		$('#subcommentDiv1_'+codeId).css({'display':'none'});
		$('.editQCode').css({'display':'none'});
		$('.actFeedOptionsDiv').css({'display':'none'});
		$('.taskCompCodes').css({'display':'block'});
		$('.taskCompCodeSpan').css({'display':'block'});
		$('.quoteReorderDivs').css({'display':'none'});
		$("#editDivContainer_"+codeId+"").show();
		$("#main_commentTextarea_"+codeId+"").val(value);
		$("#editable_"+codeId+"").hide();
	}
		
		function saveEditQuotes(codeId) {
			$('#loadingBar').show();
			timerControl("start");
			var codes = $("#main_commentTextarea_"+codeId+"").val().trim();
			var projectId = $("input#globalProjectId").val();
			codes=codes.replaceAll("<br>","CHR(50)").replaceAll("\n","CHR(50)").replaceAll("\\","CH(70)");
			if (codes == null || codes == '') {
				parent.alertFun(getValues(companyAlerts,"Alert_EntCode"), 'warning');
				$('#loadingBar').hide();
				timerControl("");
				return false;
			}
			$.ajax({
				url : path + "/paLoad.do",
				type : "POST",
				data : {
							act : "insertUpdateQuotesToDb",
							projectId : projectId,
							quotes : codes,
							quoteId : codeId
						},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success : function(result) {
					checkSessionTimeOut(result);
					if (result == 'success') {
						alertFun(getValues(companyAlerts,"Alert_CodeSave"), 'warning');
						$("textarea#main_commentTextarea_"+codeId+"").val("");
						loadProjQuotes(projectId);
					} else {
						alertFun(getValues(companyAlerts,"Alert_CodeNotSave"), 'warning');
					}
					$('#loadingBar').hide();
					timerControl("");
				}
			});
	}
		
		function showStageReorder(quoteId) {
			$('#quotesOption_' + quoteId).slideUp('slow');
			$('.editQCode').css({'display':'none'});
			$('.actFeedOptionsDiv').css({'display':'none'});
			$('.taskCompCodes').css({'display':'block'});
			$('.taskCompCodeSpan').css({'display':'block'});
			$('.actFeedHoverImgDiv').css({'display':'block'});
			$('div#stageContainer').find('div.mCS_no_scrollbar_y').css('height', '100%');
			if ($('#quoteReorderDiv_' + quoteId).is(":hidden")) {
				$(".quoteReorderDivs").hide();
				$('#quoteReorderDiv_' + quoteId).show();
			} else {
				$('#quoteReorderDiv_' + quoteId).hide();
			}
		}
		
		
			
			function updateQuoteOrder(currentId, targetId) {
				$("#loadingBar").show();
				timerControl("start");
				$.ajax({
					url : path + "/paLoad.do",
					type : "POST",
					data : {
								act : "updateQuoteOrder",
								currentId : currentId,
								targetId : targetId
							},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success : function(result) {
						checkSessionTimeOut(result);
						$("#loadingBar").hide();
						timerControl("");
					}
				});
			}
			
			function clearEditedCodes() {
			   if ($('textarea#taskCompTextAreaValEdit').val().trim() != '') {
					confirmFun(getValues(companyAlerts,"Alert_DelCode"), "clear", "clearEditedCommentedCodes");
				} else {
					confirmFun(getValues(companyAlerts,"Alert_DelCode"), "clear", "clearEditedCommentedCodes");
				}
			}
			
			function clearEditedCommentedCodes() {
				$('textarea#taskCompTextAreaValEdit').val('');
				$('textarea#taskCompTextAreaValEdit').focus();
			}
			
			function cancelEditedCodes(){
				$("#quotesContainer_"+CodeId).show();
			    $("#EquotesContainer_"+CodeId+"_edit").hide()
				$("#wstansparentDiv").hide();
				parent.$("#transparentDiv").hide();
			}
			
			function imageOnErrorReplacePrj(obj) {
				$(obj).attr('src', lighttpdpath + "/projectimages/dummyLogo.png");
			}
			
			var tempID = "";
			function getWrkFlowTemplateId(templateId) {
				$("div.workflowTemplateList").css("background-color", "");
				$("div#viewWorkFlowTemplate_" + templateId).css("background", "#b1adad");
				tempID = templateId;
			}
			
	var globalProjectId="";
	var globprojectEmailAttachmentStatus="";
	var glbprojectDesc="";
	function prepareSettingsIcon(jsonResult,position){
		var UI="";
		wrkspaceImgType = '';
		var project_hidden_status="";
		$('#projNameHidden').val('');
		$('#projectIdHidden').val('');
		$('#projCodeHidden').val('');
		$('#projCodeHidden').val('');
		defImage = lighttpdpath + "/projectimages/dummyLogo.png";
		var jsonData = jQuery.parseJSON(jsonResult);
		if(jsonData){
    		var json = eval(jsonData);
    		if(jsonData.length>0){
    			for(var i=0;i<json.length;i++){
    	 			globalProjectId=json[i].project_id;
    	 			glbprojectDesc=json[i].projectDesc;
    	 			project_hidden_status=json[i].project_hidden_status
    	 			if(position=='workspace')
    	 			  wrkspaceImgType = $('#projectImageId_'+json[i].project_id).attr('src');
    	 			else if(position=='sysAdmin')  
    	 			  wrkspaceImgType = $('#project_'+json[i].project_id).attr('imgsrc');
    	 			else if(position=='team')  
    	 			  wrkspaceImgType = projImgSrc;  
    	 			
    	 			$("input#hiddenCurrentProjSrc").val(wrkspaceImgType);
    	 			
    	 			if(typeof(notfUserId) == "undefined")
              			notfUserId = "";
              		
              		if(typeof(notfProjId) == "undefined")
              			notfProjId = "";
    	 			
    	 			if(notfProjId != "" && notfUserId != ""){
	              		 	if(notType == 'workspace_approval'){
		       					$("input#hiddenCurrentProjSrc").val(notprjImagePath);
		    			 	}
	    			 }
    	 			if(typeof(notstype) != 'undefined' && notstype == "projStatusComment"){
                            $("input#hiddenCurrentProjSrc").val(projImgSrc);
                            wrkspaceImgType = projImgSrc;
                      }
    	 			var d = new Date();
  	    			var time = d.getTime();
  	    			ImageUploadProjectFlag = json[i].project_image_type; 
					
					/*if(projImgType != ''){
						var imgType =  projImgType.split(".")[1];
					}*/
					
					var archiveImage  = json[i].poStatus == "A"  ? "images/active.png" : "images/inactive.png";
					var archiveStatusTitle =  json[i].poStatus == "A" ? "Active": "Inactive";
					var projectName = json[i].projectName;
					projectName = replaceSpecialCharacter(projectName);
					var projectTagName = json[i].projectTagName;
					var projectEmailAttachmentStatus = json[i].project_emailattachment_status;
					globprojectEmailAttachmentStatus=projectEmailAttachmentStatus;
					//$('#projNameHidden').val(projectName); 
					$('#projCodeHidden').val(projectTagName);
					$('#projEmailAttachmentHidden').val(projectEmailAttachmentStatus);
					$("input#projStatusTypeHidden").val(json[i].project_status);
					$("input#projStatusHidden").val(json[i].poStatus);
					$("input#checkForChangePermission").val(json[i].proj_user_status);
					var imageUrlw  = json[i].project_user_hidden_status == "unhide"  ? "images/hidden.png" :  "images/showhidden.png";
				    var imgTile =  json[i].project_user_hidden_status==""+getValues(companyLabels,"unhide")+"" ? ""+getValues(companyLabels,"hide")+"": ""+getValues(companyLabels,"unhide")+"";
				    var statusG="",statusA="",statusR="";
				    /*if(json[i].project_overall_status=="Green"){
				       statusG ="selected"
				    }else if(json[i].project_overall_status=="Amber"){
				       statusA ="selected"
				    }else if(json[i].project_overall_status=="Red"){
				       statusR ="selected"
				    }*/
				    
				    
					UI+= " <div id=\"projectDetailsContainer\" style=\"background:#FFFFFF;width:100%;float:left;height:100%;\">"
							+ "<div id=\"leftProjectContainer\" style=\"float:left;width:18%;height:450px;border-right: 1px solid #DDDDDD;\">"
								+ "<div id=\"proDetId\"  style=\"width:100%;float:left;border-bottom: 1px solid #DDDDDD;\">"
									+ "<span class=\"wsProjName inZone_Details_cLabelText\" onclick=\"commonFunctionality(this,'projectDetails')\" style=\"float:left;margin-top:7%;margin-left:10%;height:30px;cursor:pointer;color:#C2C2C2\"></span>"
								+ "</div>"
								+ "<div  id=\"projStatusId\"  style=\"width:100%;float:left;border-bottom: 1px solid #DDDDDD;\">"
									+ "<span class=\"wsProjName Project_Status_cLabelText\" onclick=\"commonFunctionality(this,'status')\" style=\"float:left;margin-top:7%;margin-left:10%;height:30px;cursor:pointer;color:#C2C2C2\"></span>"
								+ "</div>"
								+ "<div  id=\"invUserId\"  style=\"width:100%;float:left;border-bottom: 1px solid #DDDDDD;\">"
									+ "<span class=\"wsProjName Invite_Users_cLabelText\" onclick=\"commonFunctionality(this,'inviteUser')\" style=\"float:left;margin-top:7%;margin-left:10%;height:30px;cursor:pointer;color:#C2C2C2\"></span>"
								+ "</div>"
								+ "<div id=\"appUserId\"  style=\"width:100%;float:left;border-bottom: 1px solid #DDDDDD;\">"
									+ "<span class=\"wsProjName Approval_cLabelText\" onclick=\"commonFunctionality(this,'apporval')\"  style=\"float:left;margin-top:7%;margin-left:10%;height:30px;cursor:pointer;color:#C2C2C2\"></span>"
								+ "</div>"
								
								//if(companyType != "Social" && companyPlan != 'Business_0' && companyPlan != 'Business_92' ){
									UI += "<div  id=\"wrkFId\"  style=\"width:100%;float:left;border-bottom: 1px solid #DDDDDD;\">"
										+ "<span class=\"wsProjName WORKFLOW_TEMPLATE_cLabelText\" onclick=\"commonFunctionality(this,'workFlow')\" style=\"float:left;margin-top:7%;margin-left:10%;height:30px;cursor:pointer;color:#C2C2C2\"></span>"
									+ "</div>"
									+ "<div  id=\"tskComCod\"  style=\"width:100%;float:left;border-bottom: 1px solid #DDDDDD;\">"
										+ "<span class=\"wsProjName TASK_COMPLETION_CODES_cLabelText\" onclick=\"commonFunctionality(this,'taskCodes')\"  style=\"float:left;margin-top:7%;margin-left:10%;height:30px;cursor:pointer;color:#C2C2C2\"></span>"
									+ "</div>"
									+ "<div  id=\"optDrive\"  style=\"width:100%;float:left;border-bottom: 1px solid #DDDDDD;\">"
										+ "<span class=\"wsProjName Optional_Drives_cLabelText\" onclick=\"commonFunctionality(this,'optionalDrives')\"  style=\"float:left;margin-top:7%;margin-left:10%;height:30px;cursor:pointer;color:#C2C2C2\"></span>"
									+ "</div>"
									+ "<div  id=\"wrkInt\"  style=\"width:100%;float:left;border-bottom: 1px solid #DDDDDD;\">"
										+ "<span class=\"wsProjName Integration_cLabelText\" onclick=\"commonFunctionality(this,'integration')\"  style=\"float:left;margin-top:7%;margin-left:10%;height:30px;cursor:pointer;color:#C2C2C2\"></span>"
									+ "</div>"
								//}
								
							UI+= "</div>"
							+ "<div id=\"rightProjectContainer\" style=\"float:left;width:82%;height:450px;\">"
								//start of project details container 
								+ "<div id=\"fullProjectDetailsContainer\" style=\"float:left;width:100%;height:100%;\">"
										+"<div id=\"leftProjectDetailsContainer\" style=\"float:left;width:30%;height:100%;border-right: 1px solid #DDDDDD;overflow-y:auto;\">"
											+"<div style=\"float:left;width:95%;margin-top:5%;margin-left:3%;\">"
												+ "<span class=\"Name_cLabelText\" style=\"float:left;width:40%;font-family: OpenSansRegular;font-size:14px;\"></span>"
												+ "<span style=\"float:left;width:1%;font-size:14px;\"> : </span>"
												//+ "<div id=\"projectName\" contenteditable=\"true\" style=\"float:left;width:55%;margin-left:10px; text-overflow: ellipsis;overflow:hidden;white-space: nowrap;\">"+projectName+"</div>"
												+ "<input id=\"projectName\" class=\"defaultExceedCls\" type=\"text\" style=\"margin-left:10px;width:55%;float:left;font-size: 13px;border: 0;background:none;\" value=\""+projectName+"\" />"
												
											+"</div>"
											+"<div style=\"float:left;width:95%;margin-top:5%;margin-left:3%;\">"
												+ "<span class=\"Hash_Code_cLabelText\" style=\"float:left;width:40%;font-family: OpenSansRegular;font-size:14px;\"></span>"
												+ "<span style=\"float:left;width:1%;font-size:14px;\"> : </span>"
												+ "<span style=\"float:left;width:10px;margin-left:10px;\">#</span>"
												+ "<input id=\"projectTagName\" class=\"defaultExceedCls\" type=\"text\" onkeyup=\"specCharReplaceProjectCode(this)\"  style=\"width:50%;float:left;font-size: 13px;border: 0;background:none;\" value=\""+projectTagName+"\" />"
											+"</div>"
											+"<div style=\"float:left;width:95%;margin-top:5%;margin-left:3%;\">"
												+ "<span class=\"defaultExceedCls Privacy_cLabelText Privacy_cLabelTitle\" title=\"\" style=\"float:left;width:40%;font-family: OpenSansRegular;font-size:14px;\"></span>" 
												+ "<span style=\"float:left;width:1%;font-size:14px;\"> : </span>" ;
												if(json[i].project_status == 'private'){
												UI = UI + "<img onclick=\"changePrjImg(this)\"  decideimg=\"private\" id=\"imgChgType\" style=\"float:left;margin-left:10px;\" src=\"images/private.png\" >";
												}else {
												UI = UI + "<img onclick=\"changePrjImg(this)\"  decideimg=\"public\" id=\"imgChgType\" style=\"float:left;margin-left:10px;\" src=\"images/public.png\" >";
												}
											 UI = UI +"</div>"
											+"<div style=\"float:left;width:95%;margin-top:5%;margin-left:3%;\">"
												+ "<span class=\"WS_Active_cLabelText\" style=\"float:left;width:40%;font-family: OpenSansRegular;font-size:14px;\"></span>"
												+ "<span style=\"float:left;width:1%;font-size:14px;\"> : </span>"
												+	  "<select onchange=\"projectStatus(this);\" id=\"projStatusType\" class = \"emailConfiguration\" style = \"border-bottom: 1px solid #bfbfbf;float:left;width:45%;margin-top:-10px;margin-left:10px;background:none;\" >"
														+"<option class=\"ACTIVE_cLabelHtml\" value=\"Y\"></option>"
														+"<option class=\"Archive_cLabelHtml\" value=\"A\">Archive</option>"
							        					//+"<option value=\"D\">Disable</option>"
						            			+   "</select>"
											+"</div>"
											+"<div id=\"\" class=\"form-group\" style=\"clear:both;float: left;margin-top: 5%;padding-bottom: 4%;margin-bottom: 4%;margin-right: 2%;margin-left:2%;padding-left:2px;width: 94%;border-bottom: 1px solid rgb(221, 221, 221);\">"
                                                +"<label style=\"float: left;font-weight: normal;margin-right:5px;margin-top: 2px;width: 15%;\"  class=\"projPopLabel Start_cLabelText Start_cLabelTitle defaultExceedCls\" title=\"\"></label>"
                                                +"<input class=\"starts_at_cLabelPlaceholder\" placeholder=\"\" id=\"startDate\" class=\"hasDatePicker startsAt taskStEnDate\" onclick=\"setSens('endDate', 'max');\" readonly=\"true\" style=\"width: 32%;height: 25px;\" type=\"text\" value='"+json[i].project_start_date+"'>"
                                                +"<input class=\"ends_at_cLabelPlaceholder\" placeholder=\"\" id=\"endDate\" class=\"hasDatePicker endsAt taskStEnDate\" onclick=\"setSens('startDate', 'min');\" readonly=\"true\" style=\"width: 32%;height: 25px;float:right;margin:0px;\" type=\"text\" value='"+json[i].project_end_date+"'>"
                                                +"<label style=\"float: right;font-weight: normal;margin-right:5px;margin-left: 5px;margin-top: 2px;width: 15%;\"  class=\"projPopLabel defaultExceedCls WS_End_cLabelText WS_End_cLabelTitle\" title=\"\"></label>"
                                                +"<input type='hidden' id='hiddenProjStartDate' value='"+json[i].project_start_date+"' />"
                                                +"<input type='hidden' id='hiddenProjEndDate' value='"+json[i].project_end_date+"' />"
                                            +"</div>"
											/*+"<div style=\"float:left;width:95%;margin-top:4%;margin-left:3%;\">"
												+ "<span style=\"float:left;width:40%;font-family: OpenSansRegular;font-size:14px;\"> Image </span>"
												+ "<span style=\"float:left;width:1%;font-size:14px;\"> : </span>"
											+"</div>"*/
											+"<div style=\"height: 122px;float:left;width:96%;margin-left:2%;margin-right: 2%;\">"
											+"  <div style=\"width: 152px;height: 100%;position:relative;margin:0px auto;\">"
											+"    <div style=\"width: 115px;float: left;\">"
											+"       <img style = \"width: 115px;height: 115px;border: 1px solid #B7BBBE;background: #ffffff;\" onerror=\"javascript:imageOnProjNotErrorReplace(this);\" src = \""+wrkspaceImgType+"\" id = \"hideDiv_"+json[i].project_id+"\" >"
											+"       <div id='projStatusUpdateMainDiv' style=\"width: 115px;height: 7px;background-color: "+json[i].project_background_colour+";\"></div>"
											+"    </div>"
											+"    <div title=\"upload\" id = 'projectIconUpload' style = 'width: 30px;height: 100%;float:right;position: relative;'>"
											+"		 <img id=\"wrkSpaceHideImage\" style=\"position: absolute;cursor:pointer;float:right;top: 0px;right: 0px;\" onclick=\"hideProject("+json[i].project_id+",'"+json[i].project_user_hidden_status+"')\" title = \""+imgTile+"\" src=\""+imageUrlw+"\">" 
											+" 		 <img id=\"upload_link_proj\" style=\"bottom: 0;left: 0;position: absolute;cursor:pointer;float:left;\" src=\"images/upload.png\" >" 
											+        uploadProjpopUp()
											+"       <iframe id=\"upload_projup_image\" name=\"upload_projup_image\" src=\"\" style=\"display:none\"></iframe>"
									        +"       <form action=\""+path+"/WorkspaceProjectImageUpload?projectIdForUpload="+json[i].project_id+"\" enctype=\"multipart/form-data\" method=\"post\" name=\"wsImgUploadForm\" id=\"wsImgUploadForm\" > "					
											+"         <label class=\"topicImageUpload\" style=\"margin-top:-3px;width:160px;height:20px;position:absolute;display:none;\"> "
											+"           <a style=\"color:#FFFFFF;margin-left:-5px;\" href=\"#\"></a>"
											+"			 <input type=\"file\" title=\"upload image\"  onchange=\"readNewURL(this);\" nId = "+json[i].project_id+" class=\"topic_file\" value=\"\" name=\"filenameproj\" id=\"filenameproj\">" 			
											+"			 <input type=\"hidden\" value=\"\" name=\"projUploadIdNew\" id=\"projUploadIdNew\">"
											+"         </label>"
											+"       </form>"
											+"    </div>"
											+"  </div>"
											+"</div>"
											
											 				
											
											/*+"<div style=\"float: left; margin-top: 4%; padding-bottom: 2%; padding-top: 3%; margin-left: 3%; margin-right: 3%; width: 94%; border-bottom: 1px solid rgb(221, 221, 221); border-top: 1px solid rgb(221, 221, 221);\">"
												+ "<span style=\"float:left;width:40%;font-family: OpenSansRegular;font-size:14px;\">"+( json[i].project_user_hidden_status =="unhide" ? "Hide":"Unhide") +"</span>"
												+ "<span style=\"float:left;width:1%;font-size:14px;\"> : </span>"
												//+ "<img id=\"wrkSpaceHideImage\" style=\"float:left;display:block;cursor:pointer;margin-top:-2px;margin-left:10px;\" onclick=\"hideProject("+json[i].project_id+",'"+json[i].project_user_hidden_status+"')\" title = \""+imgTile+"\" src=\""+imageUrlw+"\">"
											+"</div>"*/
											+"<div id=\"emailAttachmentDiv\" class=\"\" style=\"clear:both;float: left; margin-top: 5%; padding-bottom: 2%; padding-top: 3%; margin-left: 3%; margin-right: 2%; width: 94%; border-bottom: 1px solid rgb(221, 221, 221); border-top: 1px solid rgb(221, 221, 221);\">"
											//+"<div id=\"emailAttachmentDiv\" class=\"form-group\" style=\"float: left;width: 100%;padding-top: 3%;padding-right: 3%;padding-left: 3%;margin-bottom: 0px;\">"
												+"<label style=\"float: left;width: 86%;font-weight: normal;\"  class=\"projPopLabel defaultExceedCls save_attachment_cLabelHtml save_attachment_cLabelTitle\" > </label>"
												+"<div style=\"float: left;width: 18px;\"><input type=\"checkbox\" class=\"workspaceProjEmailConDiv\" id=\"emailAttachmentCheckVal\" style=\"height:15px;\"></div>"
											+"</div>"
											 
											/*+"<div id=\"\" class=\"form-group\" style=\"float: left;width: 100%;padding-top: 5%;padding-right: 3%;padding-left: 3%;margin-bottom: 10px;\">"
												+"<label style=\"float: left;font-weight: normal;margin-right:5px;margin-top: 2px;\"  class=\"projPopLabel\" >Start</label>"
												+"<input placeholder=\"starts at\" id=\"startDate\" class=\"hasDatePicker startsAt taskStEnDate\" onclick=\"setSens('endDate', 'max');\" readonly=\"true\" style=\"width: 32%;height: 25px;\" type=\"text\" value='"+json[i].project_start_date+"'>"
												+"<input placeholder=\"ends at\" id=\"endDate\" class=\"hasDatePicker endsAt taskStEnDate\" onclick=\"setSens('startDate', 'min');\" readonly=\"true\" style=\"width: 32%;height: 25px;float:right;\" type=\"text\" value='"+json[i].project_end_date+"'>"
												+"<label style=\"float: right;font-weight: normal;margin-right:5px;margin-left: 5px;margin-top: 2px;\"  class=\"projPopLabel\" >End</label>"
												+"<input type='hidden' id='hiddenProjStartDate' value='"+json[i].project_start_date+"' />"
												+"<input type='hidden' id='hiddenProjEndDate' value='"+json[i].project_end_date+"' />"
											+"</div>"*/
										+ "</div>"
										+"<div id=\"rightProjectDetailsContainer\" style=\"float:left;width:68%;height:98%;margin-top:1%;margin-left:1%;\">"
							 							+"<textarea id=\"projDescription\" name=\"projDescription\" style=\"float:left;display:none;resize:none;height:100%;width:100%;overflow:auto;\">"
							 							+ "</textarea>"
										+ "</div>"
								+"</div>"
								//end of project details container 
								// start of template container 
								+ "<div id=\"templateContainer\" style=\"float:left;width:100%;display:none;height:100%;\">"	
									+ "<div id=\"workflowContentContainer\" style=\"float:left;width:100%;height:100%;\">"
									    + "<div id=\"workFlowHeader\" class=\"workFlowHeaderCss\" style=\"height:40px;\" >"
						 	               +" <img id=\"createNewWorkFlow\" class=\"wrkRightIcon img-responsive Create_Workflow_cLabelTitle\" onclick=\"createWFTemplate();\" title=\"\" src=\"images/add.png\" style=\"margin: 12px 17px 0px 0px;float: right; height: 20px;width: 20px;\">"
						 				+"</div>"
										+ "<div id=\"workflowContainer\" style=\"float:left;width:100%;overflow:auto;\">"
											  +"<div id=\"workFlowResultContainer\" style=\"float:left;width:100%;padding:0px 10px;\">"
						 					  +"</div>" 
										+ "</div>"
									+ "</div>"
								+ "</div>"					
							// end of template container
								// start of Invite user 
								+ "<div id=\"inviteUsersToProjectWorkspace\" style=\"float:left;width:100%;height:100%;position:relative;display:none;\">"
							 	+ 	"<div id=\"displayUsersContainer\" style=\"float:left;width:100%;height:100%;overflow-y:auto;overflow-x:hidden;\">"
								 	+     "<div id=\"workspaceAdminDiv\" style=\"float:left;width:100%;\">"
								 	+         "<div style=\"width:100%;float:left;\" id=\"callAdmins\" >"
								 	+ "<div style=\"float:left;width:100%;border-bottom: 1px solid #DDDDDD;margin-left:8px;margin-top:15px;\">"
								 	+             "<div style=\"float:left;width:15%;\"><span class=\"Owner_cLabelHtml\" style=\"border-bottom: 3px solid rgb(77, 154, 174);float: left;font-family: opensanscondbold;\"></span></div>"
								 	+              "<img class=\"Add_Owner_cLabelTitle\" style=\"float:left;%;display:block;cursor:pointer;margin-top:2px;height:20px;width:20px;\" title = \"\" id = \"addAdministrators\"  src=\"images/add.png\" onclick = \"callCommonUsers('PO');\">"
								 	+ "</div>"
								 	+   "<div id=\"adminResultContainer\" style=\"float:left;width:100%;min-height:100px;\">"
								 	+   "</div>"
								 	+         "</div>"
								 	+     "</div>"
								 	+     "<div id=\"workspaceTeamMembersDiv\" style=\"float:left;width:100%;\">"
								 	+         "<div style=\"width:100%;float:left;\" id=\"callAdmins\" >"
								 	+ "<div style=\"float:left;width:100%;border-bottom: 1px solid #DDDDDD;margin-left:8px;margin-top:15px;\">"
								 	+             "<div style=\"float:left;width:15%;\"><span class=\"WS_Team_Members_cLabelHtml\" style=\"border-bottom: 3px solid rgb(77, 154, 174);float: left;font-family: opensanscondbold;\"></span></div>"
								 	+              "<img class=\"Add_TeamMembers_cLabelTitle\" style=\"float:left;display:block;cursor:pointer;margin-top:2px;height:20px;width:20px;\" title = \"\" id = \"addTeamMembers\"  src=\"images/add.png\" onclick = \"callCommonUsers('TM')\">"
									+     "</div>"
								 	+              "<div id=\"teamMemberResultContainer\" style=\"float:left;width:100%;min-height:100px;\">"
								 	+               "</div>"
								 	+         "</div>"
								 	+     "</div>"
								 	+     "<div id=\"workspaceObserversDiv\" style=\"float:left;width:100%;\">"
								 	+         "<div style=\"width:100%;float:left;\" id=\"callAdmins\" >"
								 	+ "<div style=\"float:left;width:100%;border-bottom: 1px solid #DDDDDD;margin-left:8px;margin-top:15px;\">"
								 	+            "<div style=\"float:left;width:15%;\"><span class=\"Observers_cLabelHtml\" style=\"border-bottom: 3px solid rgb(77, 154, 174);float: left;font-family: opensanscondbold;\"></span></div>"
								 	+              "<img class=\"Add_Observers_cLabelTitle\"style=\"float:left;display:block;cursor:pointer;margin-top:2px;width:20px;height:20px;\" title = \"\" id = \"addObserversNew\"  src=\"images/add.png\" onclick = \"callCommonUsers('SU')\">"
								 	+ "</div>"
								 	+              "<div id=\"observseResultContainer\" style=\"float:left;width:100%;min-height:100px;\">"
								 	+               "</div>"
								 	+         "</div>"
							 	+     "</div>"
							 	+ 	"</div>"
							 	
							 	+ "<div id = \"commonUserListContainers\"  class = \"popAdminUserListContainer\" style = \"display: none;width: 650px; height: 450px; top: 1%; float: left; margin-left: 5%; margin-top: 8%; position: fixed;\">"
							                + "<div style=\"float:left;width:100%;\">"
							                     +"<img src=\"images/close.png\"  id=\"popupClose\" style=\"float: right;cursor: pointer;margin-top:10px;margin-right:10px;\" class=\"closeCreateProj\" onclick=\"cancelCommonListContainer()\">"
							                 +"</div>"
							                 
							                  + "<div style=\"float:left;width:100%;\">"
							                  	+"<span id=\"changeUsersFonts\" style=\"float:left;width:80%;margin-left:20px;\"></span>"
							                  +"</div>"
							                    
						                	+ "<div id=\"adminUserListDetails\"  style=\"padding:3% ;color:#808080;width:100%;float:left;height: 100%;width: 100%;\">"
						                		+ "<div style=\"float:left;height:10%;border-bottom:1px solid #BFBFBF;padding-bottom: 3%;width: 100%;\">"
						                    		+ "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">"
						                        		+ "<tbody>"
							                      			+ "<tr>"
												       			+ "<td style=\"padding-right:5px;width: 90%;\"><div style=\"float:left; border:1px solid #cccccc;padding:3px; border-radius:3px; width:100%;\">"
												            		+ " <input onkeyup=\"searchUserInList(this);\" id=\"searchAdminUsersList\" type=\"text\" value=\"\"  style=\"width:75%;border:none;\">"
												           			+ " <img id=\"adminSearchIcon\" src=\"images/searchTwo.png\" style=\"display:block;width:15px;height:15px;float:right;cursor:pointer;margin-top:4px;\"/>" 
												            	+ "</td>"
							                          		+ "</tr>"
						                        		+ "</tbody>"
						                      		+ "</table>"
						                    	+ "</div>"
						                    	+ "<div id=\"sysAdminUsers\"  class=\"projAdminUsers\" style=\"float:left;height:70%;margin-top:5px;overflow-y:auto;width:100%;\"></div>"
						                    	+ 	"<div style=\"float:left;width:100%;margin-top:10px;border-top:1px solid #BFBFBF;\" id=\"addAdministratorsDiv\">" 
						                    	
						                    	+  "<img id=\"addAdminUserParticipants\"  onclick=\"addCommonUsersDiv();\" src=\"images/add.png\" style=\"display:none;float:right;margin-top:5px;cursor:pointer;\">"
						                        
						                        if(companyType == "Social"){
						                    		UI = UI + "<img title=\"Invite Users\" id=\"invite_std_user\"  onclick=\"inviteStdUsers();\" src=\"images/add_std_user.png\" style=\"display:none;float:left;cursor:pointer;margin-top:5px;height:25px;width:25px;\">";
						                    	} 
						                    	
						                    	UI = UI	+ "</div>"
						                  		+ "</div>"
                						+ "</div>"
							 	+ "</div>"
								// end of invite user 
								+"<div id=\"taskCompletionCodesContainer\" style=\"float:left;width:100%;height:100%;display:none;\">"
									+ "<div id=\"createTaskCodes\" style=\"float:left;width:100%;padding:10px;\">"
							 	    + "</div>"
							 	    + "<div id=\"taskCodeResultContainer\" style=\"float:left;width:100%;padding:0px 10px;\">"
							 	    + "</div>"
							 	    + "<div id=\"taskLibraryCodeContainer\" style=\"float:left;width:100%;height:100%;\">"
							 				+ " <div style=\"font-family: opensanscondbold; padding: 10px; height: 40px;\" id=\"taskCompCodeSubHeader\">"
									  				+ " <span style=\"border-bottom:3px solid #569aaf; float:left; font-size: 14px;height:20px;\" id=\"task_code_change\">TASK COMPLETION CODES</span>"
									  	    + " </div>"
									  	    
									  	    + "<div id=\"taskLibraryCodeResultContainer\" style=\"float:left;width:100%;padding:0px 10px;\">"
							 	            + "</div>"
									  	    
							 	    + "</div>"
								+"</div>"
								//end of task completion codes conatiner 
								+"<div id=\"projectStatusContainer\" style=\"float:left;width:100%;height:100%;display:none;\">"
									+ "<div id=\"projectStatusList\" style=\"float:left;width:45%;padding:10px;height: 100%;\">"
									//+ "  <div class=\" taskLabel wsTaskLabels \" style=' display:none;'>Enable Status Update</div>"
									//+ "  <div style=\"float: left;width: 18px;padding-top: 1%; display:none;\">"
									//+ "     <input id=\"projStatusUpdateCheckVal\" "+(json[i].project_status_checked =='Y' ? "checked":"" )+" style=\"margin-top: 0px;\" type=\"checkbox\">"
									//+ "  </div>"
									+"  <div id='statusUpdateDiv1' style='float:left;width:100%;height: 150px;position: relative;'>"
									+ "   <div class=\" taskLabel wsTaskLabels \" style=\"clear:both;padding-top: 10px;display:none;\">Status Update</div>"
									//+ "  <div style=\"float: left;width: 100px;margin-top: 5px;\" >"
									//+ "     <select id='projStatusSel' class='taskSelectCss form-control' style=\"height: 28px;\"><option>Select</option><option value='Green' "+statusG+">Green</option><option value='Amber' "+statusA+">Amber</option><option value='Red' "+statusR+">Red</option></select>"
									//+ "  </div>"
									 +"   <div align=\"\" style=\"width: 130px;height: 100%;position:relative;padding-top: 22px;\">"
									 +"    <div style=\"width: 115px;\"> "
									 +"      <img id='projStatusUpdateIcon' style = \"width: 115px;height: 115px;margin-top: 3%;border: 1px solid #B7BBBE;background: #ffffff;\" onerror=\"javascript:imageOnProjNotErrorReplace(this);\" src = \""+wrkspaceImgType+"\" >"
									 +"      <div id='projStatusUpdateDiv' style=\"width: 115px;height: 7px;background-color: "+json[i].project_background_colour+";\"></div>"
									 +"   </div>"
									 +"   <img id=\"showStatusListDivId\" src=\"images/downarrow.png\" onclick=\"showStatusListDiv();\" style=\"position: relative;width: 12px;cursor:pointer;float:right;margin-top:-6px;\">"
									 +"   <div id='projStatusListDiv' align=\"left\" style=\"width: 115px;  border: 1px solid rgb(161, 161, 161); background: rgb(255, 255, 255) none repeat scroll 0% 0%; padding: 2px 6px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); border-radius: 2px; overflow: hidden; margin-top: 5px; display: none; position: absolute; bottom: -10px; right: -122px;z-index: 100;\"></div>"
									 +"   </div>"
									 //+"   <img id=\"showStatusListDivId\" src=\""+path+"/images/downarrow.png\" onclick=\"showStatusListDiv();\" style=\"width: 12px;margin-top: 115px;bottom: 0;margin-left: 28%;cursor:pointer;\">"
									// +"   <div id='projStatusListDiv' style=\"width: 115px;height: 130px;clear: both;top: 100%;border: 1px solid rgb(161, 161, 161);background: none 0% 0% repeat scroll rgb(255, 255, 255);padding: 2px 6px;box-shadow: rgba(168, 168, 168, 0.6) 0px 1px 4px 0px;border-radius: 2px;overflow: hidden;display:none;margin-top:5px;\"></div>"
									+"  </div>"
									+"  <div id='statusUpdateDiv2' style='float:left;width:100%;padding-top:10px;'>"
									+ "   <div class=\" taskLabel wsTaskLabels Comment_cLabelHtml \" style=\"clear:both;text-align:left;\"></div>"
									+ "   <div style=\"float: left;width: 100%;margin-top: 6px;height:90%;clear:both;\" >"
									+ "       <textarea id=\"statusComment\" class=\"wsInTxtArea\" onkeyup=\"specialCharAtAndHash(event);\" style=\"resize: none;width: 100%;\"></textarea>"
									+ "<div id=\"atSpeicalCharDis\" class=\"activityfieldUserListMain activityfieldMainuserslist\" style=\"display: none;position:relative;\">"
                                         +"<ul class=\"activityfielduserList\"></ul>"
                                    + "</div>"
                                    + "<div id=\"hashSpeicalCharDis\" class=\"projUserListMain proTagNameId\" style=\"display: none;position:relative;\">"
                                         +"<ul id=\"hashSpeicalCharList\" class=\"projuserList\"></ul>"
                                    + "</div>"
									+"        <input type='hidden' id='hiddenProjPrevStatus' value='"+json[i].project_overall_status+"' />"
									+"        <input type='hidden' id='hiddenProjCurrentStatus' value='' />"
									+ "   </div>"
									+ " </div>"
							 	    + "</div>"
							 	    + "<div id=\"projectStatusCommentListContainer\" style=\"float: left; width: 55%; padding: 10px 10px; height: 100%;border-left: 1px solid #dddddd;\">"
							 	    + " <div id=\"projectStatusCommentHeader\" style=\"float:left;width:100%;border-bottom: 1px solid #cccccc;margin-bottom: 2px;\"> "
							 	    +             "<span style=\"border-bottom: 3px solid rgb(77, 154, 174);float: left;font-family: opensanscondbold;\">"+getValues(companyLabels,"history_StatusComment")+"</span>"
							 	    +              "<div style=\"float: right;margin-right: 1px;margin-top: 1px;\">"
							 	    +                    "<img id=\"barGraphStatus\" onclick=\"changetoBarGraph(this);\" src=\"images/barGraphForStatus.png\" style=\"height: 20px;display:none;cursor:pointer;\">"
							 	    +              "</div>"
                                    +  "</div>"
                                    +      "<div id=\"projectStatusCommentContainer\" style=\"float: left; width: 100%; padding: 10px 10px; height: 100%;\">"
							 	    +     "</div>"
							 	    + "<canvas id=\"myChart_"+json[i].project_id+"\" style=\"float: left; width: 100%; padding: 10px 10px; height: 100%;display:none;\" ></canvas>"
							 	    + "</div>"
							 	   
								+"</div>"
								//end of project status conatiner 
								//start of apporval request container 
									+"<div id=\"apporvalRequestContainer\" style=\"float:left;width:100%;height:100%;\">"
							 			 +"<div id=\"appLeftContainer\" style=\"float:left;width:15%;height:440px;\">"
							 			 	+"<div id=\"appImageContainer\" style=\"float:left;margin-top:25%;margin-left:10%;\">"
							 			 		+"<img src=\""+$("input#hiddenCurrentProjSrc").val()+"\" style=\"width:115px;height:115px;border: 1px solid #B7BBBE;float:left;\" onerror=\"javascript:imageOnProjNotErrorReplace(this);\" >"
							 			 		+"   <div id='projStatusApproval' style=\"width:115px;height:7px;float: left;\"></div>"
							 			 	+"</div>"
							 			 	+ "<div style=\"float:left;margin-left:10%;width:90%;\">"
							 			 		+ "<span title=\""+$("input#projNameHidden").val()+"\" style=\"float: left; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;margin-top:2px;\">"+$("input#projNameHidden").val()+"</span>"
							 			 	+"</div>"
							 			 +"</div>"
							 			 +"<div id=\"appRightContainer\" style=\"float:left;width:85%;height:100%;\">"
							 			 	+"<div style=\"float:left;width:100%;padding-top:5%;padding-left:10px;height:16%;\">"
							 			 		+ "<span class=\"Request_From_cLabelHtml\" style=\"float:left;width:98%;height:30px;border-bottom: 1px solid #DDDDDD;\"> </span>"
							 			 	+"</div>"
							 			 	+ "<div id=\"apporvalResult\" style=\"overflow:auto;float:left;width:100%;height:82%;\">"
							 	            + "</div>"
							 			 +"</div>"
							 	    +"</div>"
								//end of apporval request container 
							  //start of optional drives container
							  	+"<div id=\"optionalDrivesContainer\" style=\"display:none;float:left;width:100%;height:100%;\">"
								  		+"<div id=\"optionalDrivesContent\"  style=\"float:left;width:50%;margin:3%;\" >"
							    			///+ "<div style=\"float:left;width:100%;\" > <img id=\"21\" class=\"p_drive_uncheck\" style=\"float:left;\" src=\""+path+"/images/selcettwo.png\" ><span style=\"float:left;width:50%;margin-left:2%;margin-top:-4px;\">Colabus</span></div>"
							    			///+ "<div style=\"float:left;width:100%;margin-top:4px;\" > <img id=\"22\" class=\"p_drive_uncheck\" onclick=\"changeDrive(this)\" style=\"float:left;\" src=\""+path+"/images/selcet.png\" ><span style=\"float:left;width:50%;margin-left:2%;margin-top:-4px;\">Google Drive</span></div>"
							    			///+ "<div style=\"float:left;width:100%;margin-top:4px;\" > <img id=\"23\" class=\"p_drive_uncheck\" onclick=\"changeDrive(this)\" style=\"float:left;\" src=\""+path+"/images/selcet.png\" ><span style=\"float:left;width:50%;margin-left:2%;margin-top:-4px;\">OneDrive</span></div>"
					    				+"</div>" 
							  	+ "</div>"
							  	+"<div id=\"integrationContainer\" style=\"display:none;float:left;width:100%;height:100%;\">"
								  		+"<div id=\"integrationContent\"  style=\"float:left;width:50%;margin:3%;\" >"
							    			+ "<div id=\"slack\" style=\"display:none;float:left;width:15%;margin-top:4px;\" > <img  style=\"float:left;height:100%;width:100%;\" src=\"images/slack.png\" ><div style='text-align:center'>Slack</div></div>"
							    			+ "<div id=\"spark\" style=\"display:none;float:left;width:15%;margin-top:4px;margin-left:5%\" > <img style=\"float:left;height:100%;width:100%;\" src=\"images/sparkLogo.png\" ><div style='text-align:center'>Spark</div></div>"
							    			///+ "<div style=\"float:left;width:100%;margin-top:4px;\" > <img id=\"23\" class=\"p_drive_uncheck\" onclick=\"changeDrive(this)\" style=\"float:left;\" src=\""+path+"/images/selcet.png\" ><span style=\"float:left;width:50%;margin-left:2%;margin-top:-4px;\">OneDrive</span></div>"
					    				+"</div>" 
							  	+ "</div>"
							  //end of optional drive container 
							+ "</div>" // end of rightProjectContainer
							+    "<div id=\"saveDetailsContainer\" style=\"width:100%;float:left;border-top:1px solid #DDDDDD;height:60px;\">"
							 	+    	"<button id=\"chgSaveBtnClick\" onclick=\"saveUpdatedprojectDetails()\" type = \"button\" class = \"btn btn-info\" style=\"padding: 4px 15px !important;font-family:opensanscondbold;text-transform: uppercase;float:right;margin:10px;\"><span class=\"SAVE_cLabelHtml\"></span> </button>"
							 	//+       "<img id=\"barGraphStatus\" src=\""+path+"/images/barGraphForStatus.png\" style=\"margin-top: 13px;float: right;height: 27px;display:none;\">"
							 	+    	"<button id=\"chgAddBtnClick\" onclick=\"saveCompQuotesToProj()\" type = \"button\" class = \"btn btn-info\" style=\"display:none;padding: 4px 15px !important;font-family:opensanscondbold;text-transform: uppercase;float:right;margin:10px;\">ADD</button>"
							 	+    	"<button id=\"cancelForCompanyCodes\" onclick = \"goBackToTaskCodes();\"  type = \"button\" class = \"btn btn-info\" style=\"display:none;padding: 4px 15px !important;font-family:opensanscondbold;text-transform: uppercase;float:right;margin:10px;\">BACK</button>"
							 	/////+    	"<button id=\"saveForProjectDrives\" onclick = \"saveProjectDrives();\"  type = \"button\" class = \"btn btn-info\" style=\"display:none;padding: 4px 15px !important;font-family:opensanscondbold;text-transform: uppercase;float:right;margin:10px;\">SAVE</button>"
						    +    "</div>"
							
					     + "</div>"
					     
					}//end of for loop
				 }// end of if loop 
			}// end of condition for json   
				   return   UI;  
	}
	
	function changetoBarGraph(obj){
	    
	    if($(obj).attr('src') == 'images/barGraphForStatus.png'){
            $(obj).attr('src',path+'/images/calender/comments.png');
            $(obj).css('margin-bottom',"1px");
            $(obj).css('height',"");
            $('#myChart_'+$('#projUploadId').val()).show();
            $('#projectStatusCommentContainer').hide();
        }else{
            $(obj).attr('src','images/barGraphForStatus.png');
            $(obj).css('height',"20px");
            $('#projectStatusCommentContainer').show();
            $('#myChart_'+$('#projUploadId').val()).hide();
            
        }
    }
    	
	function uploadProjpopUp(){
		   
		 return  "<div style=\"width: 170px; min-width: 100px; background-color: rgb(255, 255, 255); height: auto; z-index: 5010; border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); border: 1px solid rgb(161, 161, 161); float: left; padding-left: 5px; margin-left: 3%; margin-top: 6%; position: fixed; display:none;\" id=\"Pro_Img_Upload\">"
			         +"<div style=\"float:left;margin-left:-20px;margin-top:10px;position:absolute;\">"
			           +"<img src=\"images/arrowLeft.png\" style=\"float:left;\">"
			         +"</div>"
			         +"<div style=\"height:28px\" class=\"optionList\">"
			          +"<span style=\"margin-left:18px; margin-top: 5px;width:75%\" class=\"OptionsFont Upload defaultExceedCls Upload_from_system_cLabelHtml Upload_from_system_cLabelTitle\" title=\"\" id=\"upload_Proj_Desc_Img\"> </span>"
			        +"</div>"
			         +"<div onclick=\"getUserAlbums()\" style=\"width:100%;height:23px;margin-bottom:3px;cursor:pointer;float:left\">"
			           +"<span style=\"margin-left: 18px; margin-top: 1px;width:75%\" class=\"OptionsFont Attach_Link defaultExceedCls Upload_from_gallery_cLabelHtml Upload_from_gallery_cLabelTitle\" title=\"\"> </span>"
			         +"</div>"
			      +"</div>"
	}	
	var resultset="";
	function commonFunctionForSpecialChar(type){
	var projectId = $("input#globalProjectId").val();
	var menuType = "";
	       if(type == "projectSatusComment"){
	           menuType = 'projectSatusComment';
	           $.ajax({ 
                  url: path+'/calenderAction.do',
                  type:"POST",
                  data:{act:'fetchAllUsersListForSpecChar',projectId:projectId,userId: userId,menuType:menuType},
                  error: function(jqXHR, textStatus, errorThrown) {
                        checkError(jqXHR,textStatus,errorThrown);
                        $("#loadingBar").hide();
                        timerControl("");
                        }, 
                    success:function(result){
                        parent.checkSessionTimeOut(result);
                        jsonData = jQuery.parseJSON(result);
                        resultset = jsonData;
                        $('.activityfieldMainuserslist:visible ul').html(prepareUIforspecialcharacter(jsonData,projectId));
                    }
                    
                });
	       
	       }
	}
	
	
	function prepareUIforspecialcharacter(jsonData,projectId){
    var sessionuserId = userId;
    var html="";
        for(i=0;i<jsonData.length;i++){
            selecteduserId = jsonData[i].userId
            if(jsonData[i].userId != sessionuserId){
                var username=jsonData[i].userName;
                html+="<li style =\"font-size: 12px;color: #fff; margin: 4% !important;cursor:pointer;border-bottom: 1px solid #cccccc;\" onclick=\"selectUserName('"+projectId+"','"+username+"')\">"+username+"</li>";
            }
        }
        return html;    
 
 }
	function selectUserName(projId,username){
	   $('.activityfieldMainuserslist').hide();
       var data=$('#statusComment').val();
       var focusIndex = document.getElementById('statusComment').selectionStart;
       var bTxt= data.substring(0,elementindex);
       var aTxt = data.substring(focusIndex,data.length);
       data = bTxt+username+" "+aTxt;
       $('#statusComment').val(data).focus();
	}
	
	var elementindex='';;
	var elementhashindex='';
	var regex = new RegExp("^[a-zA-Z0-9]+$");
	function specialCharAtAndHash(event){
	   var testAtData = $('#statusComment').val();
         var testAtChar = document.getElementById('statusComment').selectionStart;
        
         var testAtIndex = parseInt(testAtChar)-1;
			   if (testAtData.charAt(testAtIndex)=="@" && !regex.test(testAtData.charAt(testAtIndex-1)) ) {
		                    elementhashindex='';
		                    elementindex = document.getElementById('statusComment').selectionStart;
		                    //alert("=================>>>"+$('#statusComment').position().left);
		                          $('#hashSpeicalCharDis').css('display','none');
		                            var data=$('#statusComment').val();
		                            atIndex = parseInt(elementindex)-1 ;
		                            //console.log("atIndex==>>> "+atIndex);
		                            if(atIndex==0 || !regex.test(data.charAt(atIndex-1))){
		                               $('#atSpeicalCharDis').css('display','block');
		                               if(resultset == '' || resultset == '[]'){
		                                  //console.log("if user");
		                                  commonFunctionForSpecialChar('projectSatusComment');
		                              }else{
		                                  //console.log("else user");
		                                  $('#atSpeicalCharDis:visible ul').html(prepareUIforspecialcharacter(resultset,projectId));
		                               }
		                               if($('#atSpeicalCharDis').find('li').length < 1)
		                                  $('#atSpeicalCharDis:visible ul').html('No data Found');
		                           }
		        }else if(event.keyCode == 32){ 
                            $('#hashTagSearchInputBox').hide();
                            $('#hashTagSearchInputBox1').hide();
                            $('.activityfieldMainuserslist').hide();
                            elementindex = "";
                            atIndex ="";
                 }else if(elementindex!=""){
                            $('#hashTagSearchInputBox').hide();
                            $('#hashTagSearchInputBox1').hide();
                            var data=$('#statusComment').val();
                           //alert("data====>>> "+data);
                           // alert('atIndex:'+data.charAt(atIndex)+":");
                            if(data.charAt(atIndex)=="@" && !regex.test(data.charAt(atIndex-1))){
                                    var focusIndex = document.getElementById('statusComment').selectionStart;
                                    var aTxt = data.substring(elementindex,focusIndex).toLowerCase().trim();
                                    //console.log('aTxt:'+aTxt);
                                    $('#atSpeicalCharDis').css('display','show');
                                    $('.activityfielduserList li').show();
                                    $('.activityfieldMainuserslist').show();
                                    if(aTxt !=""){
                                        $('.activityfielduserList li').each(function(){
                                            val = $(this).text().toLowerCase();
                                            if(val.indexOf(aTxt)!= -1 ){
                                                $(this).show();
                                            }else{
                                                $(this).hide();
                                            }
                                        });
                                        
                                        //console.log('user length:'+$('.activityfielduserList li:visible').length);
                                        
                                        if($('.activityfielduserList li:visible').length < 1)
                                           $('.activityfieldMainuserslist').hide();
                                        else
                                           $('.activityfieldMainuserslist').show();
                                   }     
                          }else{
                            $('#hashTagSearchInputBox').hide();
                            $('#hashTagSearchInputBox1').hide();
                            $('.activityfieldMainuserslist').css('display','none');
                             elementindex = "";
                             atIndex ="";
                          }   
                }else {
                    $('.activityfieldMainuserslist:visible').hide();
                    $('#hashTagSearchInputBox').hide();
                    $('#hashTagSearchInputBox1').hide();
                }
            if (testAtData.charAt(testAtIndex)=="#") {
                     elementhashindex = document.getElementById('statusComment').selectionStart;
                     hashIndex = parseInt(elementhashindex)-1 ;
                     $('#atSpeicalCharDis').css('display','none');
                     $('#hashSpeicalCharDis').css('display','block');
                     //$('.proTagNameId').css('margin-top','45px');
                    if(hashResult == '[]' || hashResult == ''){ 
                        ajaxCallforHashCode('','','projStatusComment');
                    }else{
                        $('#hashSpeicalCharDis:visible ul').html(preparingUIforHash(hashResult,projectId,'projStatusComment'));
                    }
                        
                }else if( event.keyCode == 32){
                    elementhashindex='';
                    hashIndex =""; 
                    $('#hashTagSearchInputBox').hide();
                    $('#hashTagSearchInputBox1').hide();
                    $('#hashSpeicalCharDis:visible').hide();
                }else if(elementhashindex!=""){
                            $('#hashTagSearchInputBox').hide();
                            $('#hashTagSearchInputBox1').hide();
                            var data=$('#statusComment').val();
                            $('#atSpeicalCharDis').css('display','none');
                            //console.log('hashIndex:'+data.charAt(hashIndex)+":");
                            if(data.charAt(hashIndex)=="#"){
                                    var focusIndex = document.getElementById('statusComment').selectionStart;
                                    var aTxt = data.substring(elementhashindex,focusIndex).toLowerCase().trim();
                                    //console.log('hashTxt:'+aTxt);
                                    $('#hashSpeicalCharList').show();
                                    $('#hashSpeicalCharDis').show();
                                    if(aTxt !=""){
                                        $('#hashSpeicalCharList li').each(function(){
                                            val = $(this).text().toLowerCase();
                                            if(val.indexOf(aTxt)!= -1 ){
                                                $(this).show();
                                            }else{
                                                $(this).hide();
                                            }
                                        });
                                        
                                        
                                        if($('#hashSpeicalCharList li:visible').length < 1)
                                           $('#hashSpeicalCharDis').hide();
                                        else
                                           $('#hashSpeicalCharDis').show();
                                 }   
                          }else{
                             $('#atSpeicalCharDis').css('display','none');
                             $('#hashTagSearchInputBox').hide();
                             $('#hashTagSearchInputBox1').hide();
                             $('.proTagNameId').css('display','none');
                             elementhashindex = "";
                             hashIndex ="";
                          } 
                }else{
                    $('.proTagNameId:visible').hide();
                    $('#hashTagSearchInputBox').hide();
                    $('#hashTagSearchInputBox1').hide();
                }
       }
                
/*function to close the invite user popup details ends here*/

		function changePrivacy(obj){
					 if($(obj).attr('value') == 'private'){
						 $(obj).attr('src','images/public.png').attr('value','public'); 
					 }else{
						 $(obj).attr('src','images/private.png').attr('value','private');
					 }
		}
		
			 function showActiveProjects(){
			    $("#hiddenProject").attr('src','images/hidden.png');
			    searchProjType = 'visible';
				loadTabData('project')
				$('#hiddenProject').attr('title','show hidden projects');
				$("#hiddenProject").attr("onclick","showHiddenProjects()");
			 }
			 
	 function showCompQuotes() {
	 	//$("button#chgSaveBtnClick").attr("onclick", "").attr("onclick", "saveCompQuotesToProj();");
		$('#loadingBar').show();
		timerControl("start");
		$("div#createTaskCodes , div#taskCodeResultContainer").hide();
		$("div#compCodeSaveCancel , div#taskLibraryCodeContainer ").show();
		$("button#chgSaveBtnClick").hide();
		$('#chgAddBtnClick').show();
		$("button#cancelForCompanyCodes").show();
		var projectId = $("input#globalProjectId").val();
				$.ajax({
					url : path + "/paLoad.do",
					type : "POST",
					data : {
								act : "loadCompanyQuotes",
								projectId : projectId
							},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
				    success : function(result) {
					checkSessionTimeOut(result);
					    result = result.toString().replaceAll("CHR(50)","<br>").replaceAll("CH(70)","\\").replaceAll("CHR(26)",":");
						var data = prepareCompanyCodeUI(result);
						$("div#taskLibraryCodeResultContainer").height($('#rightProjectContainer').height()-40);
						$("div#taskLibraryCodeResultContainer").html("");
						$("div#taskLibraryCodeResultContainer").append(data);
						popUpScrollBar('taskLibraryCodeResultContainer');
						/*var isiPad = navigator.userAgent.match(/iPad/i) != null;
						if (!isiPad) {
							loadCompQuotesScroll('compQuotesList');
						}*/
					$('#loadingBar').hide();
					timerControl("");
				}
		 });
	}
					
	function prepareCompanyCodeUI(jsonResult){
		 var companyCodes="";
		 if(jsonResult == "[]"){
		 	companyCodes = "<div style=\"float:left;margin-left:10px;\">No library codes to display</div>";
		 }
		 var jsonData = jQuery.parseJSON(jsonResult);
		 if(jsonData){
	    	 var json = eval(jsonData);
	    	 if(jsonData.length>0){
	    	 	 for(var i=0;i<json.length;i++){
	    	 	companyCodes += "<div id=\"quotesContainer_"+json[i].codeId+"\"  class=\"taskCompCodes actFeedHover\">"
					  + "<div style=\"width: 3%; float: left; padding-top: 2px;\"> <input type=\"checkbox\" class=\"compCheckValues\" id=\"check_"+json[i].codeId+"\"></div>"
					  + "<div><span title=\""+json[i].Codes+"\" style=\"float:left;overflow:hidden;width:95%;text-overflow:ellipsis;white-space:nowrap;\" >"+json[i].Codes+"</span></div>"
					  + "</div> ";
	    	 	    }
	    	     }
		 	 } 
		return companyCodes;
	}
 					
 			function closeCompQuotes(){
 					$("span#task_code_change").html("TASK COMPLETION CODES");
 					$("div#Task_taskCompCodeInput").show();
 					$("div#compCodeSaveCancel").hide();
 					loadProjQuotes(globalProjectId);
 			}
 					
 					var checkedCompIds = "";
					function saveCompQuotesToProj() {
						checkedCompIds = "";
						$('.compCheckValues').each(function() {
							var cId = $(this).attr('id');
							var cId1 = $(this).attr('id').split("_")[1];
							var flag = document.getElementById(cId).checked;
							if (flag) {
								checkedCompIds = checkedCompIds + cId1 + ",";
							}
						});
						
						if (checkedCompIds == null || checkedCompIds == '') {
							alertFun(getValues(companyAlerts,"Alert_CheckCode"),'warning');
							checkedCompIds = "";
						    return false;
						}
						
						confirmFun(getValues(companyAlerts,"Alert_AddCodeLib"), "clear","saveCompanyCodes");
					}
					var compCodesFlagChanges = false;
					function saveCompanyCodes() {
						$('#loadingBar').show();
						timerControl("start");
						var projectId = $("input#globalProjectId").val();
						$.ajax({
							url : path + "/paLoad.do",
							type : "POST",
							data : {
										act : "insertQuotesToDbFromComp",
										projectId : globalProjectId,
										checkedCompIds : checkedCompIds
									},
							error: function(jqXHR, textStatus, errorThrown) {
					                checkError(jqXHR,textStatus,errorThrown);
					                $("#loadingBar").hide();
									timerControl("");
									}, 
							success : function(result) {
								checkSessionTimeOut(result);
								if (result == 'success') {
									compCodesFlagChanges = true;
									checkedCompIds = "";
									alertFun(getValues(companyAlerts,"Alert_CodeAdd"), 'warning');
									$('#loadingBar').hide();
						            timerControl("");
									closeCompQuotes();
									$("div#taskCompletionCodesContainer").mCustomScrollbar("destroy");
									$("button#cancelForCompanyCodes").hide();
									$('#chgAddBtnClick').hide();
									$('#chgSaveBtnClick').hide();
								} else {
									alertFun(getValues(companyAlerts,"Alert_CodeNotAdd"), 'warning');
								}
								
							}
						});
					     
					}
			 
	function editTaskCodesNew(id,type){
	   
		$(".actFeedOptionsDiv").css({'right':'0px'});
		if ($('#subcommentDiv11_'+id).is(":hidden")) {
			if(type == 'workspace'){
				$('div#taskCompletionCodesContainer').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
			}else {
				$('div#compCodesResultContainer').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','100%');
			}
			$('.actFeedOptionsDiv').hide();
   			$('.actFeedReplyOptionsDiv').hide();
   			$('#subcommentDiv11_'+id).slideToggle(function(){
   				$(this).css('overflow','');
   				if($('div#taskCompCodeContainer').find('.mCSB_container').hasClass('mCS_no_scrollbar')){
	  				$('#subcommentDiv11_'+id).css({'margin-right':'7%'});
	    		}
	    		setTCOptionCss($('#subcommentDiv1_'+id));
   			});
  		}else{
  		     $('div#taskCompletionCodesContainer').find('div.mCS_no_scrollbar_y,div.mCS_no_scrollbar').css('height','');
  			$('#subcommentDiv11_'+id).slideToggle('slow');
  		} 
	}
	
	function setTCOptionCss(obj){
	
	     var idd = $(obj).attr('id').split('_')[1];
         var t=$(obj).offset().top;
		 var oh=$(window).height()-t - $('#saveDetailsContainer').height();
		 var h=$('#subcommentDiv11_'+idd).height();
	     $('#subcommentDiv11_'+idd).css('margin','-5px 40px 0px 0px');
	     $('#subcommentDiv11_'+idd+' .workSpace_arrow_right').css('margin-top','-5px');
	     h=$('#subcommentDiv11_'+idd).height();
	     if(oh<h){
	         var fh=h-oh+25;
		     var fh1=h-oh+10;
		     $('#subcommentDiv11_'+idd).css('margin-top','-'+fh+'px');
		     $('#subcommentDiv11_'+idd+' .workSpace_arrow_right').css('margin-top',fh1+'px');
		 } 
  }
	
	
		
	function showTeamQuotesPopUp(obj, Id){
	 	 var winWidth=$(obj).parent().width();
	     var l = $(obj).offset().left;
		 var divTop = $(obj).offset().top;
		 var top = $(obj).offset().top;
	     var cHeight = $(window).height()-top;
	     var popH = $('#subcommentDiv11_'+Id).height();
	     if(cHeight < popH){
			      var divHeight = popH-cHeight+65;
			      var divHeight1 = popH-cHeight+90;
			      var arrowHeight = popH-cHeight+58;
			      var arrowHeight1 = popH-cHeight+59;
			      $('div#taskArrow_'+Id).css('margin-top',arrowHeight+'px');
			  } else{
			      $('div#taskArrow_'+Id).css('margin-top','-5%');
			  }
 	} 
 	
 	
 	function uploadPopUp(type){
 		if(type == 'sysAdmin'){
 			$("#upload_link_proj_sysAdmin").on('click', function(e){
   	    	e.preventDefault();
   	        $("#filenameprojSysAdmin:hidden").trigger('click');
   	    });
 		}else {
 			$("#upload_link_proj").on('click', function(e){
   	    	e.preventDefault();
   	    	$("#Pro_Img_Upload").slideToggle(function(){
                $(this).css('overflow','');
            });
   	        //$("#filenameproj:hidden").trigger('click');
   	    });
   	    
 			
 		 $("#upload_Proj_Desc_Img").click(function(e){
 			e.preventDefault();
   	        $("#filenameproj:hidden").trigger('click'); 
 		 });	
 		}
 	}	
 	
 	
 	function submitProjImageUploadFormNew() {
 		var formname = "wsImgUploadForm";
 		var uploadForm = document.getElementById(formname);
		uploadForm.target = 'upload_projup_image';
		uploadForm.submit();
		Imageglobal = true;
	}
	var Imageglobal=true;
	function readNewURL(input) {
		var Id = $(input).attr('nId');
		$('#projUploadIdNew').val(Id);
		if (input.files && input.files[0]) {
	    	var reader = new FileReader();
			reader.onload = function (e) {
				var src = e.target.result;
	   			$('#hideDiv_'+Id).attr('src', e.target.result);
	   			$("#Pro_Img_Upload").hide();
	   			Imageglobal = false;
	   		};
	    	reader.readAsDataURL(input.files[0]);
		}
		
		var upload = $('#filenameproj').val();
		$('#wsImgUploadForm').data('obj',this);
	    $('#hideDiv_'+Id).data('imgName',upload);
	  
	}
   	
   	function showProjectNewDesc(){
   		$('#projDescMain').css("display","block");
   		$('#transparentDiv').show();
   	}
   	
   	
   	function showEmailConfig(obj){
   		var UI = "";
   		var height = getHeightDynamically('emailConfig');
   		$("button#chgSaveBtnClick").attr("onclick", "").attr("onclick", "saveProjectEmailConfig();");
   		$("div#inviteUsersToProjectWorkspace").hide();
   		$("div#projectDescriptionContainer").hide();
   		$("div#apporvalRequestContainer").hide();
   		$("div#templateContainer").hide();
   		$("div#integrationContainer").hide();
   		$("div#configureEmailContainer").show();
   		$("span#configEmailText").addClass('tabActive');
   		$("span#inviteUserText").removeClass('tabActive');
   		$("span#projectDesctxt").removeClass('tabActive');
   		$("span#apporvalText").removeClass('tabActive');
   		$("div#templateText").removeClass('tabActive');
   		
   		UI = "<div id=\"emailContainerDetails\" style=\"float:left;width:100%;\">"
				+ "<div id=\"teamAlias\" style=\"width:35%;float:left;\">"
				+	"<span class=\"emailConfig\">Team Alias : </span>"
				+	"<input style=\"float:left;width:50%;\" type=\"text\" value=\"\" class=\"emailConfiguration\" id=\"workspaceProjEmailId\" placeholder=\"eg: abc@example.com\">"
				+ "</div>"
				
				+ "<div id=\"teamPassword\" style=\"width:35%;float:left;\">"
				+	"<span class=\"emailConfig\">Password : </span>"
				+	"<input style=\"float:left;width:50%;\" type=\"password\" value=\"\" class=\"emailConfiguration\" id=\"workspaceProjEmailPwd\" placeholder=\"password\">"
				+ "</div>"
				
				+ "<div id=\"teamIncomingEmail\" style=\"width:35%;float:left;\">"
				+	"<span class=\"emailConfig\">Incoming  : </span>"
				+	"<input style=\"float:left;width:50%;\" type=\"text\" value=\"\" class=\"emailConfiguration\" id=\"workspaceEmailHostname\" placeholder=\"incomig mail server\">"
				+ "</div>"
				
				+ "<div id=\"teamAccount\" style=\"width:35%;float:left;\">"
				+	"<span class=\"emailConfig\">Account : </span>"
				+	  "<select id=\"workspaceEmailActType\" class = \"emailConfiguration\" style = \"border-bottom: 1px solid #bfbfbf;float:left;width:50%;\" >"
				+		   "<option value=\"\">Select</option>"
				+		  "<option value=\"pop3\">pop3</option>"
		        +           "<option value=\"imaps\">imaps</option>"
	            +     "</select>"
				+ "</div>"
				
				+ "<div id=\"teamPort\" style=\"width:35%;float:left;\">"
				+	"<span class=\"emailConfig\">Port: </span>"
				+	"<input style=\"float:left;width:50%;\" type=\"text\" value=\"\"class=\"emailConfiguration\" id=\"workspaceEmailPortNum\" placeholder=\"port number\">"
				+ "</div>"
				
				+ "<div id=\"teamSecurity\" style=\"width:35%;float:left;\">"
				+	"<span class=\"emailConfig\">Security: </span>"
				+	"<select id=\"workspaceEmailSecType\" class = \"emailConfiguration\"  style = \"border-bottom: 1px solid #bfbfbf;float:left;width:50%;\"  onchange=\"SecuritySsl(this);\">"
				+		  	 "<option value=\"None\">None</option>"
				+		   "<option value=\"SSL/TLS\">SSL/TLS</option>"
				+		    "<option value=\"SSL/TLS (Accept all certificates)\">SSL/TLS (Accept all certificates)</option>"
				+		   "<option value=\"STARTTLS\">STARTTLS</option>"
				+		 	"<option value=\"STARTTLS (Accept all certificates)\">STARTTLS (Accept all certificates)</option>"
				+   	"</select>"
				+ "</div>"
				
				+ "<div id=\"teamAttachments\" style=\"width:35%;float:left;\">"
				+	"<span class=\"emailConfig\">Save attachments to folder: </span>"
					+ "<div style=\"float:left;margin-top:15px;\">"
					+	"<input style=\"float:left;width:50%;\" type=\"checkbox\" checked=\"checked\" class=\"workspaceProjEmailConDiv\" id=\"workspaceEmailAttach\" style=\"float: left;\">"
					+ "</div>"
				+ "</div>"
				
				+ "<div id=\"teamEmail\" style=\"width:35%;float:left;\">"
				+	"<span class=\"emailConfig\">Fetch emails from</span>"
				+ "<div id=\"\" style=\"float:left;width:50%;margin-top:15px;\">"
				+   "<img onclick=\"changePrjEmail(this)\"  val=\"today\" id=\"imgForMail\" style=\"float:left;margin-left:10px;\" >"
				+ "</div>"
				+ "</div>";
				$("div#configureEmailResult").html(UI);
				$("div#configureEmailResult").css("height",height+"px");
				
				var projectId = $("input#globalProjectId").val();
				
				$.ajax({
				url: path + "/workspaceAction.do",
				type:"POST",
				data:{act:"getEmailDetails",projectId:projectId},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
					checkSessionTimeOut(result);
					addValuesForEmail(result);
				    }
		        });
   	}
   	
   var imgExtension = "";
   	function updateWorkSpaceImage(imgName,projectId){
   		var d = new Date();
        setTimeout( function(){
			imgExtension = imgName.split(".")[1];
			$("#projectImageId_" +projectId).attr("src", lighttpdPath+"projectimages/"+imgName+"?"+d.getTime()).load(function(){
			      
			});
		}, 5000);
		
	}	
	
	function showWsSearchAndSort(){
		$("select#sortWrkspace").val('sort');
		$('#wsSearchBox').val('');
		$("#ClearComSearch").hide();
      	$('#workspaceSearchDiv').slideToggle(function(){
      		$(this).css('overflow','');
      	});
	}
	
	
	function sortWrkspace(obj){
		//console.log("sortworkspace"+obj);
		if(searchProjType == 'visible'){
			loadMyProjectsJson();
		}else if(searchProjType == 'hidden'){
			showHiddenProjects();
		}else if(searchProjType == 'public'){
			showAvailableProjJson();
		}
	}
	
	function searchWrkspace(){
	//console.log("searchwrkspace");
		searchType = 'search';
		if(searchProjType == 'visible'){
			loadMyProjectsJson();
		}else if(searchProjType == 'hidden'){
			showHiddenProjects();
		}else if(searchProjType == 'public'){
			showAvailableProjJson();
		}
		searchVal='';
	}
	
	/*upload a cvs file to create new project*/
	function uploadCsvFile(){
		$('#transparentDiv').show();
		$("#csvUploadFileName").text("");
		if($('#uploadCsv4NewProj').is(":hidden")){
  			$("#uploadCsv4NewProj").show();
  		}
		
		$("#noteColumn").attr("href", lighttpdPath + "/csvFileExample.csv");
	}	
	
	function closeCvsDiv(){
		$("#uploadCsv4NewProj").hide();
	  	$('#transparentDiv').hide();
	}
	
	function submitCsvFileForm(e){
		var FileName = $('#CsvFileName').val();
		var extension = FileName.split(".");
		if(extension[1] == "csv" || extension[1] == "CSV"){
			$("#csvUploadFileName").text(FileName);
			$("#uploadCsvFileButton").click(function(){
				var formname = e.form.name;
				var colform = document.getElementById(formname);
				colform.target = 'upload_target';
				$('#loadingBar').show();
				colform.submit();
			});
		}else{
			alertFun(getValues(companyAlerts,"Alert_UpdCsv"),"warning");
			return false;
		}
		
	}	
	
	function setCsvFileForProj(file){
   		$.ajax({
			url: path + "/workspaceAction.do",
			type:"POST",
			data:{act:"saveCsvFileDetails",fileName:file},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
				var res = result.split("#@#");
				if(res[0] == "projectCreated"){
					alertFun(getValues(companyAlerts,"Alert_NewPrj"),"warning");
					$('#transparentDiv').hide();
					$("#uploadCsv4NewProj").hide();
					$("#csvUploadFileName").text("");
					loadTabData("project");
					$('#loadingBar').hide();
				}
				if(res[0] == "failure"){
					$('#transparentDiv').hide();
					$("#uploadCsv4NewProj").hide();
					$("#csvUploadFileName").text("");
					loadTabData("project");
					$('#loadingBar').hide();
				}
					
				if(res[1] && res[2] != ""){
					alertFun(getValues(companyAlerts,"Alert_Prj '"+res[2]+"' Alert_AlreadyExist '"+res[1]+"' "),"warning");
					$("#csvUploadFileName").text("");
				}
			}
		});
	}
	
		 function getHeightDynamically(type){
	 	    var winHeight = $(window).height();
		 	var ckeHeight = "";
		 	if(type == 'projectDesc'){
		 	ckeHeight = eval(winHeight) - ( $("div#myNavbar").height() +  $("div#tabMainDiv").height() + $("div#headerContainer").height() + $("div#subContainer").height() + 145 );
		 	}
		 	
		 	else {
		 	ckeHeight = eval(winHeight) - ( $("div#myNavbar").height() +  $("div#tabMainDiv").height() + $("div#headerContainer").height() + $("div#saveDetailsContainer").height());
		 	}
			return ckeHeight;
	    }
    
	    function showSubscribedUsr(projectId){
	    		$('#loadingBar').show();
				timerControl("start");
	    		$("button#chgSaveBtnClick").attr("onclick", "").attr("onclick", "SaveApproveProjectUsers();");
			 	$("div#taskCompletionCodesContainer").hide();
			 	$("div#projectStatusContainer").hide();
	 			$("div#fullProjectDetailsContainer").hide();
	 			$("div#inviteUsersToProjectWorkspace").hide();
	 			$("div#templateContainer").hide();
	 			$("div#optionalDrivesContainer").hide();
	 			$("div#apporvalRequestContainer").show();
	 			$("div#projStatusApproval").css('background-color', $("#projStatusUpdateMainDiv").css('background-color'));
	 			//var ckeHeight = $('#apporvalRequestContainer').height()- $('#appRightContainerHeader').height()-10;
			    //$("div#apporvalResult").css("height",ckeHeight+"px");
				$.ajax({
					url: path + "/landPageNotAction.do",
					type:"POST",
					data:{act:"getAllApproveUsers",projId:projectId},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
							checkSessionTimeOut(result);
							var data = prepareNotificationUI(result);
							$("div#apporvalResult").html(data);
							popUpScrollBar('apporvalResult');
							notfProjId='';
						   $('#loadingBar').hide();
						   timerControl("");
					}
				});	
	  }
	  
	  function closeProjectDetails(){
	  	$("div#projectSettingsContainerDiv").hide();
	  	$("img#goBackToMainScreen").hide();
		$("img#searchProject").show();
		$("img#hiddenProject").show();
		$("img#csvUpload, #myProInfo").show();
		$("img#addProject").show();
		var type = $("input#hiddenClickForSettings").val();
		$('#myzoneBreadcrum').children('span.breadCrumProjName').remove();
 		$('#myzoneBreadcrum').children('img.breadcrumImg').remove();
		/*if(type == 'taskCodes'){
			if(compCodesFlagChanges == false){
				if($("textarea#inputTaskCodes").length == 1){
					alertFun("Project details not changed", "warning");
				}
			}
		}
		if(type == 'workFlow'){
			if(templateFlagChanges == false){
				alertFun("Project details not changed", "warning");
			}
		}
		if(type == 'inviteUser'){
			if(inviteUserflagChanges == false){
				alertFun("Project details not changed", "warning");
			}
		} */
	  	loadTabData("project");
	  }
	  
	  function saveProjectDescription(projectId) {
	  			$('#loadingBar').show();
			    timerControl("start");
	            $('textarea#projDescription').val(CKEDITOR.instances.projDescription.getData());
		        var projDesc = $('textarea#projDescription').val();
				$.ajax({
					url : path + "/workspaceAction.do",
					type : "POST",
					data : {
								act : "updateWrkSpaceProjDesc",
								projId : projectId,
								projDesc : projDesc
							},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success : function(result) {
						checkSessionTimeOut(result);
						alertFun(getValues(companyAlerts,"Alert_PrjDesc"), "warning");
						$('#loadingBar').hide();
						timerControl("");
					}
				});
		}
		
	function radioCheckedApporval(Obj){
		  var userid = $(Obj).attr('name');
		  var type = $(Obj).attr('type');
		  $("div#Notsuser_"+userid+"").find('.apporvalHeader').removeClass('radiochk').addClass('radioUnchk');
		  $(Obj).addClass('radiochk').removeClass('radioUnchk');
		  if(type == "DU"){
		  	$("div#deny_"+userid+"").show();
		  }
		  if(type != "DU"){
		  	$("div#deny_"+userid+"").hide();
		  }
		  $("input#apporvalHiddenType").val(type);
	}
	
	var userList = "";
    var approveFlag = "false";
	function SaveApproveProjectUsers(){
			$('#loadingBar').show();
			timerControl("start");
			userList =  getSubscribeUsersData();
				if(approveFlag == 'false'){
				$.ajax({
						url: path + "/landPageNotAction.do",
						type:"POST",
						data:{act:"saveApproveUserList",userList:userList},
						error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
						success:function(result){
							checkSessionTimeOut(result);
						if(result== "success"){
							approveFlag == false;
							alertFun(getValues(companyAlerts,"Alert_PrjReqGrant"),'warning');
						}
						$('#loadingBar').hide();
						timerControl("");
					}
				});
			}
	  }
	  
	  function getSubscribeUsersData(){
 		var userId = "";
 		var userCheckedType="";
 		var userEmail="";
 		var uData = "<userData>";
 		var projectId = $("input#globalProjectId").val();
 		var type = $("input#apporvalHiddenType").val();
 		$("div#apporvalResult").find("div[id^=Notsuser_]").each(function(){
 			    userId = $(this).attr("id").split("_")[1];
	 			uData +="<user id=\""+userId+"\">";
	 			uData +="<id>"+userId+"</id>";
				uData +="<member>"+type+"</member>";
				uData +="</user>";
 		});
 		uData += "<projId>"+projectId+"</projId>";
 		uData +="</userData>";
 		return uData;
	} 
	
	var denyMessage = "";
	function SaveDenyUsersReason(userId){
	 	var message=$('#message_'+userId).val().trim();
	 	closeDenyUserReasonDiv(userId);
	 	denyMessage= message; 
 	}
 	
 	function callUsersForProjcts(){
 		///checkForChanges();
 		var userId = ""; 
 		var userName = "";
 		var userEmail="";
 		var userXMLdata="";
 		var details="";
 		var flag=true;
 		
 		$("div#displayUsersContainer").find('div[id^=AdminUsersContainer_]').css('border','none').css('border-bottom','1px solid rgb(193, 197, 200)');
 		
 		$("div#adminResultContainer").find('div[id^=AdminUsersContainer_]').each(function() {
 				 userId = $(this).attr("id").split('_')[1]; 
				  
				 if ($("div#displayUsersContainer").find(".userAvailable_"+userId+"").length > 1 ){
				 	 alertFun(getValues(companyAlerts,"Alert_NoMultiRole"),'warning');
				 	 $("div#displayUsersContainer").find(".userAvailable_"+userId+"").css('border','1px solid red');
				 	 flag=false;
				 	 return false;
				 }
		});
 		
 		if(!flag) 
 		 return false;
 		
 		$("div#teamMemberResultContainer").find('div[id^=AdminUsersContainer_]').each(function() {
 			 userId = $(this).attr("id").split('_')[1]; 
				  
 			 if ($("div#displayUsersContainer").find(".userAvailable_"+userId+"").length > 1 ){
				 	 alertFun(getValues(companyAlerts,"Alert_NoMultiRole"),'warning');
				 	 $("div#displayUsersContainer").find(".userAvailable_"+userId+"").css('border','1px solid red');
				 	flag=false;
				 	return false;
		    }
 			
 		});
 		
 		if(!flag) 
 		 return false;
 		
 		userXMLdata = PrepareProjInviteUserXmlData();
 		/*
 		$("div#adminResultContainer .addAdminUsers").each(function() {
			 details = $(this).attr("userdetails");
			 userId = details.split('_')[1]; 
			 userName = details.split('_')[2];   
 			 userEmail = details.split('_')[3];  
 			 userXMLdata += "<user>";
 			 userXMLdata += "<userid>" + userId + "</userid>";
 			 userXMLdata += "<userName>" + userName + "</userName>";
 			 userXMLdata += "<userEmailId>" + userEmail + "</userEmailId>";
 			 userXMLdata += "<userType>PO</userType>";
 			 userXMLdata += "</user>";
 		});
 		
 		$("div#teamMemberResultContainer .addTeamMembers").each(function() {
 			 details = $(this).attr("userdetails");
 			 userId = details.split('_')[1]; 
 			 userName = details.split('_')[2];   
 			 userEmail = details.split('_')[3];  
 			 userXMLdata += "<user>";
 			 userXMLdata += "<userid>" + userId + "</userid>";
 			 userXMLdata += "<userName>" + userName + "</userName>";
 			 userXMLdata += "<userEmailId>" + userEmail + "</userEmailId>";
 			 userXMLdata += "<userType>TM</userType>";
 			 userXMLdata += "</user>";
 		});
 		
 		$("div#observseResultContainer .addObservers").each(function() {
 			 details = $(this).attr("userdetails");
 			 userId = details.split('_')[1]; 
 			 
 			 userName = details.split('_')[2];   
 			 userEmail = details.split('_')[3];  
 			 userXMLdata += "<user>";
 			 userXMLdata += "<userid>" + userId + "</userid>";
 			 userXMLdata += "<userName>" + userName + "</userName>";
 			 userXMLdata += "<userEmailId>" + userEmail + "</userEmailId>";
 			 userXMLdata += "<userType>SU</userType>";
 			 userXMLdata += "</user>";
 		}); */
 		 
 		if(userXMLdata!= ""){
 			var projectId = $("input#globalProjectId").val();
 			$('#loadingBar').show();
		    timerControl("start");
		    userXMLdata = "<userData>" + userXMLdata + "</userData>";
		    $.ajax({
				url : path+"/paLoad.do",
				type : "POST",
				data : {
							act : "saveInivtedEntUsersToProj",
							saveInviteData : userXMLdata,
							projId : projectId
						},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success : function(data) {
					checkSessionTimeOut(data);
					if(data == "success"){
						 inviteUserflagChanges = true;
						 alertFun(getValues(companyAlerts,"Alert_UsrAdded"), "warning");
						 inviteUsersToProject('workspace');
					}
					userXMLdata=""
					$('#loadingBar').hide();
					timerControl("");
					$(".findUsersList").css("background","");
				}
			});
 		}
 	}
	
 	function PrepareProjInviteUserXmlData(){
 		var adminData="";
 		var teamMemberData="";
 		var observerData="";
 		$("div#adminResultContainer .addAdminUsers").each(function() {
			 details = $(this).attr("userdetails");
			 userId = details.split('_')[1]; 
			 userName = details.split('_')[2];   
			 userEmail = details.split('_')[3];  
			 adminData += "<user>";
			 adminData += "<userid>" + userId + "</userid>";
			 adminData += "<userName>" + userName + "</userName>";
			 adminData += "<userEmailId>" + userEmail + "</userEmailId>";
			 adminData += "<userType>PO</userType>";
			 adminData += "</user>";
		});
		
		$("div#teamMemberResultContainer .addTeamMembers").each(function() {
			 details = $(this).attr("userdetails");
			 userId = details.split('_')[1]; 
			 userName = details.split('_')[2];   
			 userEmail = details.split('_')[3];  
			 teamMemberData += "<user>";
			 teamMemberData += "<userid>" + userId + "</userid>";
			 teamMemberData += "<userName>" + userName + "</userName>";
			 teamMemberData += "<userEmailId>" + userEmail + "</userEmailId>";
			 teamMemberData += "<userType>TM</userType>";
			 teamMemberData += "</user>";
		});
		
		$("div#observseResultContainer .addObservers").each(function() {
			 details = $(this).attr("userdetails");
			 userId = details.split('_')[1]; 
			 
			 userName = details.split('_')[2];   
			 userEmail = details.split('_')[3];  
			 observerData += "<user>";
			 observerData += "<userid>" + userId + "</userid>";
			 observerData += "<userName>" + userName + "</userName>";
			 observerData += "<userEmailId>" + userEmail + "</userEmailId>";
			 observerData += "<userType>SU</userType>";
			 observerData += "</user>";
		});
		
		return adminData+teamMemberData+observerData;
 	}
 	
	function goBackToTaskCodes(){
		$("div#taskCompletionCodesContainer").mCustomScrollbar("destroy");
		$("button#cancelForCompanyCodes, #chgAddBtnClick").hide();
		loadProjQuotes($("input#globalProjectId").val());
	}
	
	function prepareNotificationUI(jsonResult){
		var jsonData = jQuery.parseJSON(jsonResult);
   		var UI="";
   		if(jsonData == ""){
   			UI = "<span style=\"float:left;margin-left:10px;font-family:OpenSansRegular;\">"+getValues(companyLabels,"Nopending")+"</span>"
   		}
   		
   		else {
   			if(jsonData){
    	 	var json = eval(jsonData);
    	 	if(jsonData.length>0){
    	 	 for(var i=0;i<json.length;i++){
    	 	 		UI +=  "<div class=\"notsUserDetails\" id=\"Notsuser_"+json[i].notificationId+"\" style=\"float:left;width:96%;border-bottom:1px solid #BFBFBF;margin-top:5px;margin-left:10px;\">"
								+ "<div id=\"userNotsname_"+json[i].notificationId+"\"style=\"float:left;height:35px;width:20%;margin-top:3px;\" title=\""+json[i].userName+"\" >"+json[i].userName+"</div>"
								+"<div style=\"float:left;width:10%;margin-top:5px;\"><img id=\"appReq_"+json[i].notificationId+"\"  deciapprreq=\"approve\" onclick=\"changeApporvalRequest("+json[i].notificationId+",this)\" src=\"images/approve.png\"></div>"
								+	  "<select class=\"emailConfiguration\" id=\"workspaceApp_"+json[i].notificationId+"\" style = \"border-bottom: 1px solid #bfbfbf;float:right;width:25%;display:block;margin-top:1px;\" >"
				                +		   "<option value=\"PO\">Administrator</option>"
				                +		   "<option selected =\"selected\" value=\"TM\">Team Member</option>"
		                        +          "<option value=\"SU\">Observer</option>"
	                            +     "</select>"
	                            +"<div  class=\"col-sm-12 col-xs-12\" style=\"height:45px;padding-left:0px; padding-right:0px;display:block;width:100%;\">"
	                            	+"<textarea id=\"textCommonArea_"+json[i].notificationId+"\" style=\"width:96%;\"class=\"main_commentTxtArea\">Your request to subscribe to "+$("input#projNameHidden").val()+" has been approved.If you need further assistance,contact the project admin.</textarea>"
	                            	+"<div id=\"reqIdsApp\" class=\"main_commentTxtArea_btnDiv\" align=\"center\">"
	                            		+"<div onclick=\"sendAppReq("+json[i].notificationId+",'"+json[i].userEmail+"','"+json[i].userName+"')\" style=\"width: 55%;\">"
	                            			+"<img class=\"img-responsive\" alt=\"Image\" style=\"margin-top: 10px;cursor:pointer;\" src=\"images/workspace/post.png\">"
	                            		+"</div>"
	                            	+"</div>"
	                            +"</div>"
							+ "</div>" ;
    	 	 	}
    	 	}
    	} 
   		
   		}
    	return UI;
	}
	
	//Below hidden field added for project code
	function specCharReplaceProjectCode(obj){
        var projTagVal = $(obj).val();
        if(projTagVal == '-'){
           projTagVal='';
        }
	    var res;
        if (!projTagVal) {
           return false;
	    } else {
	        res = projTagVal.toLowerCase();
	        res = res.replace(/[^a-z0-9]|\s+|\r?\n|\r/gmi, " ");
	        res = res.replace(/[\W_]+/g,"");
	    }
	    $("#projectTagName").val(res);
    }

	
	function checkIfProjectCodeExists(projectCode,projectId){
		$('#loadingBar').show();
		timerControl("start");
	    var retFlag = true;
	    $.ajax({
		url : path + "/workspaceAction.do",
		dataType: "text",
		type : "POST",
		data : { act : "updateProjectCode",projectCode:projectCode,projectId:projectId},
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
		success : function(result){
			checkSessionTimeOut(result);
				$("#loadingBar").hide();
	        	timerControl("");
		    
		     	var existingProjectCodVal = 	$('#projCodeHidden').val().trim();
		        if(existingProjectCodVal == "-"){
		     		retFlag = true;
		        }else if(result == "failure"){
				    alertFun(getValues(companyAlerts,"Alert_PrjCodeExist"), 'warning');
				    retFlag = false;
		        }else if(result == "success"){
		            alertFun(getValues(companyAlerts,"Alert_OldPrjCode"), 'warning');
		            retFlag = true;
		        }
		        return retFlag;
		   }
		});
	}
	
   function saveImagForProject(){
   	var obj = $('#wsImgUploadForm').data('obj');
    submitProjImageUploadFormNew(obj);
   }
   
   function hideProject(projId,status){
   		$('#loadingBar').show();
		timerControl("start");
		hideProjId = projId;
		hideStatus = status;
		
		if (status == "unhide") {
			confirmFun(getValues(companyAlerts,"Alert_HidePrj"),'delete','onclickHideStatus');
			//confirmFun(getValues(customalertData, "Alert_HideProj"),
				//	"clear", "onclickHideStatus");
		} else {
			confirmFun(getValues(companyAlerts,"Alert_NoLongerHide"),'delete','onclickHideStatus');
			///confirmFun("Do you want to unhide this project ?",
					//"clear", "onclickHideStatus");
		}
		$("#loadingBar").hide();
	    timerControl("");
	  
   }
   
   
   function changePrjImg(obj){
   	var src = $(obj).attr('src'); // "static/images/banner/blue.jpg"
	var tarr = src.split('/');      // ["static","images","banner","blue.jpg"]
	var file = tarr[tarr.length-1]; // "blue.jpg"
	var data = file.split('.')[0]; // "blue"
   	if(data == 'public'){
   		$("img#imgChgType").attr("src","images/private.png");
   		$("input#globalProjImageType").val('1');
   		$("img#imgChgType").attr('decideimg',getValues(companyLabels,"Private"));
   	}else {
   		$("img#imgChgType").attr("src","images/public.png");
   		$("input#globalProjImageType").val('2');
   		$("img#imgChgType").attr('decideimg',getValues(companyLabels,"PUBLIC"));
   	}
   }
   
   
   
   function changePrjEmail(obj){
   	var src = $(obj).attr('src'); 
	var tarr = src.split('/');     
	var file = tarr[tarr.length-1]; 
	var data = file.split('.')[0];
   	if(data == 'today'){ 
   		$("img#imgForMail").attr("src","images/all.png");
   		$("img#imgForMail").attr('val','all');
   	}else {
   		$("img#imgForMail").attr("src","images/today.png");
   		$("img#imgForMail").attr('val','today');
   	 }
   }
   
   function recentProjectInsert(projectId){
      /* $("#loadingBar").show();
      timerControl("start");
      $.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : {act : "updateRecentProjectData",projectId : projectId,},
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
		success : function(result){
		  checkSessionTimeOut(result);
		  $("#loadingBar").hide();
         timerControl("");
		}
	}); */
   }
   
   function saveProjectEmailConfig(){
		   var projTeamAlias = $("input#workspaceProjEmailId").val().trim();
		   var incomingMailServer = $("input#workspaceEmailHostname").val().trim();
		   var portNumber = $("input#workspaceEmailPortNum").val().trim();
		   var password = $("input#workspaceProjEmailPwd").val().trim();
		   var accountType = $("select#workspaceEmailActType option:selected").attr('value');
		   var security = $("select#workspaceEmailSecType option:selected").attr('value');
		   var emailAttach='';
		   var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		   var projectId = $("input#globalProjectId").val();
		   
		   $("#loadingBar").show();
		   timerControl("start");
		   if ($('#workspaceEmailAttach').is(":checked")){
		  			emailAttach = 'checked';
		   }else{
					emailAttach = 'unChecked';
				}
			
			if(!emailReg.test(projTeamAlias) ) {
			 	alertFun(getValues(companyAlerts,"Alert_EmailInvalid"),'warning');
			 	timerControl("");	
			    $('#loadingBar').hide();
				return false;
			}	
					
		  	if(projTeamAlias == '' || projTeamAlias == 'null'){
		 	   alertFun(getValues(companyAlerts,"Alert_EnterEmailAddress"), "warning");
		       $("#workspaceProjEmailId").focus();
		       timerControl("");	
			   $('#loadingBar').hide();
		       return false;
		  	}
		    		
		     if(password =='' || password == 'null'){
		          alertFun(getValues(companyAlerts,"Alert_EnterEmailPwd"),'warning');
		          $("#workspaceProjEmailPwd").focus();
		          timerControl("");	
				  $('#loadingBar').hide();
		          return false;
		     }
		     
		     if(incomingMailServer == '' || incomingMailServer == 'null'){
		     	  alertFun(getValues(companyAlerts,"Alert_EnterEmailHost"), "warning");
		          $("#workspaceEmailHostname").focus();
		          timerControl("");	
				  $('#loadingBar').hide();
		          return false;
		     }
			     		
		    if(accountType == '' || accountType == 'null'){
		    	  alertFun(getValues(companyAlerts,"Alert_EnterAccountType"), "warning");
		    	  $("#workspaceEmailActType").focus();
		          timerControl("");	
				  $('#loadingBar').hide();
		          return false;
		   }
		    		
		   if(isNaN(portNumber)) {
		          alertFun(getValues(companyAlerts,"Alert_PortNumber"),'warning');
		          timerControl("");	
				  $('#loadingBar').hide();
		          return false;
		    }
		  
		  var fetchEmails = '';
		   
			if ($('img#imgForMail').attr('val') == 'all'){
				fetchEmails = 'completeEmails';
			}
			  else{
				fetchEmails = 'currentDate';
			}
		  
		  
		  
		    $.ajax({
				    url : path + "/workspaceAction.do",
				    type : "POST",
				    data : {act : "updateEmailDetails",projTeamAlias:projTeamAlias,incomingMailServer:incomingMailServer,
				                   portNumber:portNumber,password:password,accountType:accountType,security:security,
				                   emailAttach:emailAttach,fetchEmails:fetchEmails,projectId:projectId},
				    error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
				    success : function(result){
				    	checkSessionTimeOut(result);
				    if(result=='success'){
				    	 alertFun(getValues(companyAlerts,"Alert_EmailConfigUp"), "warning");
				    }
				    $("#loadingBar").hide();
		            timerControl("");
				}
			});
		    
   }


 
  
   function callCommonUsers(type){
		   $("#loadingBar").show();
		   timerControl("start");
		   $("input#hiddenFieldForuserType").val(type);
		   $('#transparentDiv').show();
		   if(companyType == "Social"){ 
		   	$("img#invite_std_user").show();
		   	
		   	 	$.ajax({
				    url : path + "/workspaceAction.do",
				    type : "POST",
				    data : {act : "getStandardUsers"},
				    error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
				    success : function(data){
				    	checkSessionTimeOut(data);
				    preparecommonUIforUsers(data);
				    
					    if(type=='PO'){
					    	$("span#changeUsersFonts").html(getValues(companyLabels,"Add_Owner")).attr('type',type);
					    	$("div#adminResultContainer").find("div[id^=AdminUsersContainer_]").each(function(){
					    		$("div#sysAdminUsers").find("div#AdminUsersContainer_"+($(this).attr('id').split('_')[1])).hide();
					    	});
					    }
					    if(type=='TM'){
					    	$("span#changeUsersFonts").html(getValues(companyLabels,"Add_TeamMembers")).attr('type',type);
					    	$("div#teamMemberResultContainer").find("div[id^=AdminUsersContainer_]").each(function(){
					    		$("div#sysAdminUsers").find("div#AdminUsersContainer_"+($(this).attr('id').split('_')[1])).hide();
					    	});
				    	}
				    	 if(type=='SU'){
				    		$("span#changeUsersFonts").html(getValues(companyLabels,"Add_Observers")).attr('type',type);
				    		$("div#observseResultContainer").find("div[id^=AdminUsersContainer_]").each(function(){
				    		$("div#sysAdminUsers").find("div#AdminUsersContainer_"+($(this).attr('id').split('_')[1])).hide();
				    		});
				    	}
				    }
			  });
		   } 
		   else {
		   	$.ajax({
				    url : path + "/workspaceAction.do",
				    type : "POST",
				    data : {act : "getAllActiveUsersOfCompany"},
				    error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
				    success : function(data){
				    	checkSessionTimeOut(data);
				    preparecommonUIforUsers(data);
				    $('#searchAdminUsersList').val('');
				    $('img#adminSearchIcon').attr('src','images/searchTwo.png');
		    		$('img#adminSearchIcon').attr("onclick", "");
				    if(type=='PO'){
				    	$("span#changeUsersFonts").html(getValues(companyLabels,"Add_Owner")).attr('type',type);
				    	$("div#adminResultContainer").find("div[id^=AdminUsersContainer_]").each(function(){
				    		$("div#sysAdminUsers").find("div#AdminUsersContainer_"+($(this).attr('id').split('_')[1])).hide();
				    	});
				    }
				   
				    if(type=='TM'){
				    	$("span#changeUsersFonts").html(getValues(companyLabels,"Add_TeamMembers")).attr('type',type);
				    	$("div#teamMemberResultContainer").find("div[id^=AdminUsersContainer_]").each(function(){
				    		$("div#sysAdminUsers").find("div#AdminUsersContainer_"+($(this).attr('id').split('_')[1])).hide();
				    	});
				    }
				    
				    if(type=='SU'){
				    	$("span#changeUsersFonts").html(getValues(companyLabels,"Add_Observers")).attr('type',type);
				    	$("div#observseResultContainer").find("div[id^=AdminUsersContainer_]").each(function(){
				    		$("div#sysAdminUsers").find("div#AdminUsersContainer_"+($(this).attr('id').split('_')[1])).hide();
				    	});
				    }
					}
				});
		   }
		    $("#loadingBar").hide();
			timerControl("");
   }
  
   function preparecommonUIforUsers(jsonResult){
 		var jsonData = jQuery.parseJSON(jsonResult);
        var data="";
        if(jsonData){ 
        	var json = eval(jsonData);
        	 for(var i=0;i<json.length;i++){
        	 data  +="<div class=\"projAdminUsersDiv\" email=\""+json[i].user_email1+"\" ondblclick=\"addOnDbclick(this)\" id=\"AdminUsersContainer_"+json[i].user_id+"\" style=\"height:35px;width: 99%;float:left; padding-bottom: 5px;padding-top: 3px; border-bottom: 1px solid #BFBFBF;\">"
						 + "<div id=\"projAdminUserName_"+json[i].user_id+"\" title=\""+json[i].userName+"\"  style=\"width: 70%;float:left;color: #848484;overflow: hidden;padding-left: 5px;margin-top: 3px;display:none; overflow: hidden;text-overflow: ellipsis;white-space: nowrap;\">"+json[i].userName+"</div>"
						 + "<span style=\"display:none;\" class=\"hiddenUsersForResult\">"+json[i].userName.toLowerCase()+"</span>"
						 + "<img id=\"projAdminUserImage_"+json[i].user_id+"\" src=\""+json[i].imgUrl+"\"  title=\""+json[i].userName+"\" style=\"height:25px; width: 25px;float: left;border-radius:50%;\" onerror=\"javascript:imageOnErrorUserImgReplace(this)\">"
						 + "<div class=\"projAdminProjUserName\" id=\"projAdminUserName_"+json[i].user_id+"_"+json[i].user_email1+"\"   style=\"cursor:pointer;width: 70%;float:left;color: #848484;padding-left: 5px;margin-top: 3px;font-size:13px;font-family:Tahoma;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;\">"+json[i].userName+"</div>"
						 + "<div id=\"userSelectDiv_"+json[i].user_id+"\" class=\"userUnChecked\" style=\"float: right;width:25px;height: 25px;cursor:pointer;margin:5px 5px 0 0;\" onclick=\"checkProjAdminUsers(this,'projAdminUsersDiv','addAdminUserParticipants','projAdminTeamUsersDiv','addTeamUsersParticipants','userListSharedDiv','addSubscribeUsersParticipants');\"></div>"
						 + "</div>"; 
        	 }
        }
        else {  
        data = "<p style='font-family:tahoma;font-size:13px;'>No users available.</p>" ;
        }
        $("div#commonUserListContainers").show();
        $("div#sysAdminUsers").html(data);
        popUpScrollBar('sysAdminUsers');
        $("input#searchAdminUsersList").val("");
 	}
 	
   function cancelCommonListContainer(){
   	 $("div#commonUserListContainers").hide();
   	 $('#transparentDiv').hide();
   }
   
  function prepareAddUserUI(UserId,UserName,email,userImage,type){
     var data="";
       data = "<div class=\"findUsersList actFeedHover userAvailable_"+UserId+"\" userDetails = \"adminUsersContainer_"+UserId+"_"+UserName+"_"+email+"_tm\" id=\"AdminUsersContainer_"+UserId+"\" style=\"background:#cccccc;float:left;width:99%;border-bottom: 1px solid rgb(193, 197, 200);margin-left:8px;\">"
				+ "<div id=\"\" style=\"float:left;width:5%;\"><img style=\"height:30px;width:30px;margin:10px;border-radius:50%;\" src="+userImage+" onerror=\"imageOnErrorReplaceUser(this);\"></div>"
				+ "<div class=\"defaultExceedCls\" style=\"float:left;width:40%;margin-top:15px;\">"+UserName+"</div>"
				+ "<div id=\"\" style=\"float:left;width:40%;margin-top:15px;\">"+email+"</div>"
				+ "<div class = \"actFeedHoverImgDiv\" onclick=\"deleteAddedUsers("+UserId+",'"+type+"');\" title=\"Delete\" style=\"float:right;width:5%;margin-top:15px;margin-right:-3px;\"><img style=\"height:15px;width:15px;float: left;\" src=\"images/minus.png\" /></div>"
			+"</div>";
			
			return data;
  }
 
   function addCommonUsersDiv(){
   		var UserId="";
  		var email="";
  		var UserName="";
  		var userImage="";
  		var data="";
  		var type = $("span#changeUsersFonts").attr('type');
  		
   		$('.userChecked').each(function() {
	   		UserId = $(this).prev().attr('id').split('_')[1];
	   		email =  $(this).prev().attr('id').split('_')[2];
	   		UserName =  $(this).prev().text();
	   		userImage = $("img#projAdminUserImage_"+UserId).attr('src');
	   		 data  = prepareAddUserUI(UserId,UserName,email,userImage,type);
   		 /*data = "<div class=\"findUsersList actFeedHover userAvailable_"+UserId+"\" userDetails = \"adminUsersContainer_"+UserId+"_"+UserName+"_"+email+"_tm\" id=\"AdminUsersContainer_"+UserId+"\" style=\"background:#cccccc;float:left;width:99%;border-bottom: 1px solid rgb(193, 197, 200);margin-left:8px;\">"
									+ "<div id=\"userImage\" style=\"float:left;width:5%;\"><img style=\"height:30px;width:30px;margin:10px;\" src="+userImage+"></div>"
									+ "<div id=\"userName\" style=\"float:left;width:40%;margin-top:15px;\">"+UserName+"</div>"
									+ "<div id=\"userEmail\" style=\"float:left;width:40%;margin-top:15px;\">"+email+"</div>"
									+ "<div class = \"actFeedHoverImgDiv\" onclick=\"deleteAddedUsers("+UserId+",'"+type+"');\" title=\"Delete\" style=\"float:right;width:5%;margin-top:15px;margin-right:-3px;\"><img style=\"height:15px;width:15px;float: left;\" src=\""+path+"/images/minus.png\" /></div>"
								+"</div>";
		*/
   		
   		
   		
   		if(type == "PO"){ 
   			if ($("div#adminResultContainer").find("div[id^=AdminUsersContainer_]").length < 1 ){
   			$("div#adminResultContainer").html(data);
   			}else {
   			$("div#adminResultContainer").prepend(data);
   			}
   			
   		 	$("div#AdminUsersContainer_"+UserId).addClass('addAdminUsers');
   		 	
   		 }
   		 
   		if(type  == "TM"){ 
   			if ($("div#teamMemberResultContainer").find("div[id^=AdminUsersContainer_]").length < 1 ){
   				$("div#teamMemberResultContainer").html(data);
   			}
   			else {
   				$("div#teamMemberResultContainer").prepend(data);
   			}
   		 	$("div#AdminUsersContainer_"+UserId).addClass('addTeamMembers');
   		 	$("div#teamMemberResultContainer").css({"height":"","margin":""});
   		 }
   		
   		 if(type  == "SU"){ 
   		 	if ($("div#observseResultContainer").find("div[id^=AdminUsersContainer_]").length < 1 ){
   		 	    $("div#observseResultContainer").html(data);
   			}
   		 	else {
   		 		$("div#observseResultContainer").prepend(data);
   		 	}
   		 	$("div#AdminUsersContainer_"+UserId).addClass('addObservers');
   		 	$("div#observseResultContainer").css({"height":"","margin":""});
   		 }
   		  
   		  $("div#sysAdminUsers").find("div#AdminUsersContainer_"+UserId).hide();
   		  $("div#sysAdminUsers").find("div#userSelectDiv_"+UserId).removeClass('userChecked').addClass('userUnChecked');
   		  $("div#displayUsersContainer").mCustomScrollbar("update");
   		});		
   }
   
   function deleteAddedUsers(delUserId,delUserType){
   	  if(delUserType == 'PO'){
	     $("div#adminResultContainer").find("div.userAvailable_"+delUserId+"").remove();
	  }else if(delUserType == 'TM'){
	     $("div#teamMemberResultContainer").find("div.userAvailable_"+delUserId+"").remove();
	  }else{
	     $("div#observseResultContainer").find("div.userAvailable_"+delUserId+"").remove();
	  } 
   		
   		if($.trim($('#teamMemberResultContainer').html()).length == 0){
   			$("div#teamMemberResultContainer").css({"margin":"10px"});
   			$("div#teamMemberResultContainer").html("No users to display");
   		}
   		if($.trim($('#observseResultContainer').html()).length == 0){
   			$("div#observseResultContainer").css({"margin":"10px"});
   			$("div#observseResultContainer").html("No users to display");
   		}
   		
   		var temp =  $("div#displayUsersContainer").find('div[id^=AdminUsersContainer_]').length - 1 ; 
   		var testval = $("input#globalTotalUsersCheck").val()
   		if(testval > 1){
   			$("input#globalTotalUsersCheck").val(temp);
   		}
   		$("div#displayUsersContainer").mCustomScrollbar("update");
   }
   
 function searchUserInList(obj){
     var type = $("span#changeUsersFonts").attr('type');
     $("div#sysAdminUsers").find('div.projAdminUsersDiv').hide();	
     var name = $('input#'+$(obj).attr('id')).val().toLowerCase();
	   if(!name){ 
		    $("div#sysAdminUsers").find('div.projAdminUsersDiv').show();
		    $('img#adminSearchIcon').attr('src','images/searchTwo.png');
		    $('img#adminSearchIcon').attr("onclick", "");
	    
	   }else {
	    $('img#adminSearchIcon').attr('src','images/workspace/remove.png');
	    $('img#adminSearchIcon').attr("onclick", "clearNameFields();");
	   	$("div#sysAdminUsers").find('div.projAdminUsersDiv').find('span.hiddenUsersForResult:contains("'+name+'")').parents('div.projAdminUsersDiv').show();
	   }
	   //var typpe = $('#changeUsersFonts').attr('type');
	   if(type=='PO'){ 
	    	$("div#adminResultContainer").find("div[id^=AdminUsersContainer_]").each(function(){
	    		$("div#sysAdminUsers").find("div#AdminUsersContainer_"+($(this).attr('id').split('_')[1])).hide();
	    	});
		}
		if(type=='TM'){
	    	$("div#teamMemberResultContainer").find("div[id^=AdminUsersContainer_]").each(function(){
	    		$("div#sysAdminUsers").find("div#AdminUsersContainer_"+($(this).attr('id').split('_')[1])).hide();
	    	});
	    }
	    
	    if(type=='SU'){
	    	$("div#observseResultContainer").find("div[id^=AdminUsersContainer_]").each(function(){
	    		$("div#sysAdminUsers").find("div#AdminUsersContainer_"+($(this).attr('id').split('_')[1])).hide();
	    	});		    
	   }
	   
	   $("div#sysAdminUsers").mCustomScrollbar("update");
   }
   
   function addValuesForEmail(jsonResult){
   		var jsonData = jQuery.parseJSON(jsonResult);
        var data="";
        if(jsonData){
        	var json = eval(jsonData);
        	 for(var i=0;i<json.length;i++){
        	 		$("input#workspaceProjEmailId").val(json[i].projectEmail);
        	 		$("input#workspaceEmailHostname").val(json[i].emailHost);
        	 		$("input#workspaceEmailPortNum").val(json[i].portNumber);
        	 		$("input#workspaceProjEmailPwd").val(json[i].emailPassword);
        	 		if(json[i].emailFetch == 'currentDate'){
        	 			$("img#imgForMail").attr('src','images/today.png');
        	 		}
        	 		else {
        	 			$("img#imgForMail").attr('src','images/all.png');
        	 		}
        	 }
         }
   }
   
   function clearNameFields(){
   		$("input#searchAdminUsersList").val("");
   		$("div#sysAdminUsers").find('div.projAdminUsersDiv').show();
   		$('img#adminSearchIcon').attr('src','images/searchTwo.png');
		$('img#adminSearchIcon').attr("onclick", "");
		$("div#sysAdminUsers").mCustomScrollbar("update");
   }
   
   function addOnDbclick(obj){
	   var UserId = $(obj).attr('id').split('_')[1];
	   var UserName = $('#projAdminUserName_'+UserId).html();
	   var userImage = $('#projAdminUserImage_'+UserId).attr('src');
	   var email = $(obj).attr('email');
	   var type = $("span#changeUsersFonts").attr('type');
	   var data="";
   	
   	 data  = prepareAddUserUI(UserId,UserName,email,userImage,type);
   	
   	if(type == "PO"){
   			if ($("div#adminResultContainer").find("div[id^=AdminUsersContainer_]").length < 1 ){
   			$("div#adminResultContainer").html(data);
   			}else {
   			$("div#adminResultContainer").prepend(data);
   			}
   			
   		 	$("div#AdminUsersContainer_"+UserId).addClass('addAdminUsers');
   		 	
   		 }
   		 
   		if(type  == "TM"){
   			if ($("div#teamMemberResultContainer").find("div[id^=AdminUsersContainer_]").length < 1 ){
   				$("div#teamMemberResultContainer").html(data);
   			}
   			else {
   				$("div#teamMemberResultContainer").prepend(data);
   			}
   		 	$("div#AdminUsersContainer_"+UserId).addClass('addTeamMembers');
   		 	$("div#teamMemberResultContainer").css({"height":"","margin":""});
   		 }
   		
   		 if(type  == "SU"){
   		 	if ($("div#observseResultContainer").find("div[id^=AdminUsersContainer_]").length < 1 ){
   		 	    $("div#observseResultContainer").html(data);
   			}
   		 	else {
   		 		$("div#observseResultContainer").prepend(data);
   		 	}
   		 	$("div#AdminUsersContainer_"+UserId).addClass('addObservers');
   		 	$("div#observseResultContainer").css({"height":"","margin":""});
   		 }
   		  $("div#sysAdminUsers").find("div#AdminUsersContainer_"+UserId).hide();
   		  $("div#sysAdminUsers").find("div#userSelectDiv_"+UserId).removeClass('userChecked').addClass('userUnChecked');
   		  $("div#displayUsersContainer").mCustomScrollbar("update");
   }
   
   
    function commonFunctionality(obj,type){
    	$(".wsProjName").css('color','#C2C2C2');
    	$(obj).css('color','#000000');
    	var projectId = $("input#globalProjectId").val();
    	$('#chgSaveBtnClick,#chgAddBtnClick, #cancelForCompanyCodes').hide();
    	$('#chgSaveBtnClick').attr('onclick','');
		$("input#hiddenClickForSettings").val(type);
		var projType = $('#hiddenProjSettingType').val();
    	
    	if(type == 'taskCodes'){
    		loadProjQuotes(projectId,'workspace');
    		$('#chgSaveBtnClick').hide();
    		$('#barGraphStatus').hide();
    		$('#projectStatusContainer').hide();
    	}else if(type == 'workFlow'){
    		workflowTemplateNew(projectId,'workspace');
    		$('#chgSaveBtnClick').hide();
    		$('#barGraphStatus').hide();
    		$('#projectStatusContainer').hide();
    	}else if(type == 'status'){
    	    resultset = "";
    	    hashResult = "";
    		loadProjStatus(projectId,'workspace');
    		if(projType =='myProject'){
    		  $('#chgSaveBtnClick').show().attr("onclick", "updateProjctStatus();");
    		}
    		$('#barGraphStatus').show();
    		$('#barGraphStatus').attr('src' , 'images/barGraphForStatus.png');
    	}else if(type == 'inviteUser'){ 
    		inviteUsersToProject('workspace','unchange')
    		if(projType =='myProject'){
    		  $('#chgSaveBtnClick').show().attr("onclick", "callUsersForProjcts();");
    		}
    		$('#barGraphStatus').hide();
    		$('#projectStatusContainer').hide();
    	}else if(type == 'projectDetails') {
    		showProjectDesc(projectId,'workspace');
    		if(projType =='myProject'){
    		  $('#chgSaveBtnClick').show().attr('onclick','saveUpdatedprojectDetails();');
    		}  
    		$('#barGraphStatus').hide();
    		$('#projectStatusContainer').hide();
    	}else if( type == 'apporval'){
    		showSubscribedUsr(projectId);
    		$('#chgSaveBtnClick').hide();
    		$('#barGraphStatus').hide();
    		$('#projectStatusContainer').hide();
    	}
    	else if(type == 'optionalDrives'){
    		loadOptionalDrives();
    		$('#barGraphStatus').hide();
    		$('#projectStatusContainer').hide();
    	}
    	else if(type == 'integration'){
    		loadIntegrationDetails();
    		$('#barGraphStatus').hide();
    		$('#projectStatusContainer').hide();
    	}
    }
    
    function showProjectDesc(projId,type) {
		    $('#projIdHidden').val(projId);
		    $("div#taskCompletionCodesContainer").hide();
		    $("div#projectStatusContainer").hide();
	 		$("div#inviteUsersToProjectWorkspace").hide();
	 		$("div#apporvalRequestContainer").hide();
	 		$("div#integrationContainer").hide();
			$("div#templateContainer").hide();
			$("div#optionalDrivesContainer").hide();
			$("div#fullProjectDetailsContainer").show();
     }
     
  function updateProjctStatus(){
 			 $('#loadingBar').show();
			timerControl("start");
			resultset = "";
            hashResult = "";
			var statusChecked = $('#projStatusUpdateCheckVal').attr('checked') ? "Y" : "N" ;
			var status = $('#projStatusSel option:selected').attr('value');
			var projectId = $("input#globalProjectId").val();
			var oldStatus = $('#hiddenProjPrevStatus').val();
			var changeStatus = $('#hiddenProjCurrentStatus').val();
			var statusComment = $('#statusComment').val();
			var pattern1 = /@\w+/g;
            var selectedUserName="";
            var patt = /#\w+/g;
            var projHashTagName="";
            var projHashTag = null;
            var selectedUser=null;
            
	        while(projHashTag=patt.exec(statusComment)){
	                projHashTagName=projHashTagName+projHashTag+",";
	        }
	        while(selectedUser=pattern1.exec(statusComment)){
	                selectedUserName=selectedUserName+selectedUser+",";
	         }
	       
			//alert(statusChecked+"-"+status);
			if(statusComment != null && statusComment != ""){
				$.ajax({
					url : path + "/paLoad.do",
					type : "POST",
					data : {
								act : "updateProjectStatus",
								projectId : projectId,
								status : status,
								oldStatus : oldStatus,
								changeStatus : changeStatus,
								statusComment : statusComment,
								statusChecked : statusChecked,
								projHashTagName:projHashTagName,
								selectedUserName:selectedUserName
							},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success : function(result) {
						checkSessionTimeOut(result);
						if (result == 'success') {
						    projCommentList(projectId);
						    $('#statusComment').val('');
						    statusComment = "";
							alertFun(getValues(companyAlerts,"Alert_StatusUp"), 'warning');
						} else {
							alertFun(getValues(companyAlerts,"Alert_StatusNoUp"), 'warning');
							$('#loadingBar').hide();
                            timerControl("");
						}
						//alert($("#projStatusUpdateDiv").css("background-color"));
						$("#fullProjectDetailsContainer").find("#projStatusUpdateMainDiv").css('background-color', $("#projStatusUpdateDiv").css('background-color'));
					}
				});
			}else{
			        alertFun(getValues(companyAlerts,"Alert_CmtFldNoEmt"), 'warning');
			        $('#loadingBar').hide();
                    timerControl("");
			        
			}
			
  
  }
  
    
 function loadProjStatus(projId, type) {
        $('#loadingBar').show();
	    timerControl("start");
 		var dynamicHeight = getHeightDynamically('projectStatus');
  		if(type == 'sysAdmin'){
  			dynamicHeight = dynamicHeight - 30;
  		}
  		$("div#templateContainer").hide();
		$("div#fullProjectDetailsContainer").hide();
		$("div#apporvalRequestContainer").hide();
		$("div#inviteUsersToProjectWorkspace").hide();
		$("div#templateContainer").hide();
		$("div#optionalDrivesContainer").hide();
		$("div#integrationContainer").hide();
		$("div#projectStatusContainer").show();
		$("div#taskCompletionCodesContainer").hide();
		$('#hiddenProjCurrentStatus').val("");
			$.ajax({
				url : path + "/paLoad.do",
				type : "POST",
				data : {
							act : "loadProjStatus",
							projectId : projId
						},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success : function(result) {
				    checkSessionTimeOut(result);
				     result=result.toString().replaceAll("CHR(50)","<br>").replaceAll("CH(70)","\\").replaceAll("CHR(26)",":");
					  var data = projStatusList(result);
					  $('#projStatusListDiv').html(data);
					  $("div#projectStatusCommentContainer").html("");
					  //$("div#projectStatusCommentContainer").html(data);
					  //$("div#projectStatusCommentContainer").show();
					  var height = $("div#projectStatusList").height() - $("div#statusUpdateDiv1").height() - 15;
					  $("div#statusUpdateDiv2").css('height',+height+'px');
					  
					  
					  
					  var height1 = $("div#projectStatusCommentListContainer").height() - $("div#projectStatusCommentHeader").height();
                      $("div#projectStatusCommentContainer").css('height',+height1+'px');
                      
                      popUpScrollBar('projectStatusCommentContainer');
					  var projType = $('#hiddenProjSettingType').val();
    				  if(projType !='myProject'){
    				     //$('div.taskCodeComm').hide();
    				    // $('#taskCodesCredt, #updateTaskFuns').attr('onclick','dontHavePrivilegeTC();');
    				    // $('#inputTaskCodes').attr('disabled','true');
       			      }
       			      projCommentList(projId);
				}
			});
			
			if($("input#globalOnclickCheck").val() == 'sharedProject'){
				//$("img#editTaskCodesId").attr('onclick',"");
			}
			//$('#loadingBar').hide();
            //timerControl("");
 	}
	
	function projCommentList(projId){
	   $('#loadingBar').show();
       timerControl("start");
	   var localOffsetTime=getTimeOffset(new Date());
	       $.ajax({
                url : path + "/paLoad.do",
                type : "POST",
                data : {
                            act : "loadProjCommentList",
                            localOffsetTime : localOffsetTime,
                            globalstatuscommentId:globalstatuscommentId,
                            globalstatuscommentText:globalstatuscommentText,
                            projectId : projId
                        },
                error: function(jqXHR, textStatus, errorThrown) {
                        checkError(jqXHR,textStatus,errorThrown);
                        $("#loadingBar").hide();
                        timerControl("");
                        }, 
                success : function(result) {
                    checkSessionTimeOut(result);
                     jsonData = jQuery.parseJSON(result);
                     $("div#projectStatusCommentContainer").html(prepareCommentsUIforProj(jsonData));
                     prepareChartUi(jsonData);
                     if($('#barGraphStatus').attr('src') == 'images/barGraphForStatus.png'){
                            $('#barGraphStatus').css('height','20px');
                            $('#projectStatusCommentContainer').show();
                            $('#myChart_'+$('#projUploadId').val()).hide();
                     }else{
			            $('#projectStatusCommentContainer').hide();
			            $('#myChart_'+$('#projUploadId').val()).show();
			          }
                     popUpScrollBar('projectStatusCommentContainer');
                     
                     if(typeof(notstype) != 'undefined' && notstype == "projStatusComment"){
                        $('#statusCmtId_'+notFolid).css("background-color","").addClass('notHightlightCls');
                        if($("#projectStatusCommentContainer").find('div.mCSB_scrollTools').css('display') == 'block'){ 
                            $("#projectStatusCommentContainer").mCustomScrollbar("scrollTo", "#statusCmtId_"+notFolid);
                        }
                     }
                     notstype = "";
                     $('#loadingBar').hide();
                     timerControl("")
                     }
              });
            //$('#loadingBar').hide();
            //timerControl("");
	
	}
	 
    
    function  prepareCommentsUIforProj(json){
	   var status_comment="";
	   //var subArray = [];
      // var ssub = [];
      // var subDateArray = [];
      // var backgroundCol = [];
      // var nArray=[];
	   var projectId = $("input#globalProjectId").val();
	   //var jsonData = jQuery.parseJSON(result);
	   var taskCmtUI = "";
	  // var temp= 0;
	   //if(jsonData){
	       //var json = eval(jsonData);
	       //if(jsonData.length>0){
		       for(var i = 0; i < json.length ;i++){
		                   // subArray[i] = '1';
		                   // temp = i;
		                   // ssub[i]= temp;
		                   // subDateArray[i] = json[i].barGraphDate;
		                   // backgroundCol[i] = json[i].colour_name;
		                    
		                    
			       taskCmtUI += "<div id = \"statusCmtId_"+json[i].comment_id+"\" style=\"background-color: rgb(255, 255, 255); border-radius: 3px; min-height: 25px; margin-left: 0px; padding-left: 1%; padding-right: 1%; margin-bottom: 10px; float: left; width: 100%; border: 1px solid rgb(191, 191, 191); height: auto;\" class=\"usrComments\">"
		                         +"<div style=\"float: left; font-weight: normal; padding-top: 5px; width: 100%; font-size: 11px; height: 26%;\" id=\"usrComments\" class=\"usrComments defaultNameDateTimestamp\">"
		                            +"<div style=\"height: 100%;float: left;width: 13%;\">"
		                                +"<div  style=\"width: 100%;float: left;margin: 0px 0px 0px 1px;\" > "
		                                   +"<div style=\"float: right;width: 100%;height: 7px;margin: 3px;background-color: "+json[i].colour_name+"\"></div>"
		                                +"</div>"
		                            +"</div>" 
		                            //+"<div style=\"float: left; color: black; background-color: rgb(132, 132, 132); margin-left: 5px; height: 15px; width: 1px;\"></div>"
		                            +"<div style=\"height: 100%; width: 14%;padding-left:10px;\"></div>"
		                            //+"<div style=\"float: left; color: black; background-color: rgb(132, 132, 132); width: 1px; height: 15px;\"></div>"
		                            //+"<div title=\"Spent\" style=\"float: left; padding-left: 4px; overflow: hidden; width: 15%;\">Updated By - </div>"  
		                            
		                            //+"<div style=\"float: left; color: black; background-color: rgb(132, 132, 132); margin-left: 5px; height: 15px; width: 2px;\"></div>"
		                            //+"<div title=\"Spent\" style=\"float: left; padding-left: 4px; overflow: hidden; width: 17%;\">Updated On : </div>"
		                            +"<img class=\"\" onclick=\"deleteCommentList("+json[i].comment_id+")\" style=\"height: 8px;width: 8px;float: right;margin-right: 5px;margin-top: 2px;cursor:pointer;\" src=\"images/close.png\" >"
		                            +"<div style=\"float: right;margin-right: 5px;height: 17px;overflow: hidden;margin-right: 16px;margin-left: 10px;margin-top: -1px;\"> "+json[i].last_updated_timestamp+" </div>"
		                            +"<div class =\"defaultExceedCls\" style=\"float: right;height: 17px;overflow: hidden;padding-right: 1px;margin-left: 7px;max-width: 36%;margin-top: -1px;\"> "+json[i].updated_by+" </div>"  
		                           // +"<div style=\"float: left; overflow: hidden; padding-right: 5px;\"> </div>"
		                        +"</div>"
		                        if(json[i].status_comment != "" && json[i].status_comment != null){
		                            status_comment = jsonData[i].status_comment;
                                    status_comment = status_comment.replaceAll("CHR(50)","'").replaceAll("CH(52)","'").replaceAll("CHR(26)",":").replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(40)","\"").replaceAll("CH(70)","\\");
	                                taskCmtUI += "<div class=\"\" id=\"commentTextarea_"+json[i].comment_id+"\" onclick=\"checkingTheVal(this)\"  style=\"width: 100%; height: 50px; float: left; overflow: auto; border-width: 1px medium medium; border-style: solid none none; border-color: rgb(191, 191, 191) -moz-use-text-color -moz-use-text-color; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; border-image: none; resize: none; color: rgb(132, 132, 132); font-size: 11px;\">"+status_comment+"</div>"
	                                taskCmtUI += "<textarea class=\"commentTextClss\" id=\"commentTextareaDiv_"+json[i].comment_id+"\" onblur=\"editProjStatusDiv(this);\" style=\"display:none;width: 100%; height: 50px; float: left; overflow: auto; border-width: 1px medium medium; border-style: solid none none; border-color: rgb(191, 191, 191) -moz-use-text-color -moz-use-text-color; -moz-border-top-colors: none; -moz-border-right-colors: none; -moz-border-bottom-colors: none; -moz-border-left-colors: none; border-image: none; resize: none; color: rgb(132, 132, 132); font-size: 11px;\">"+status_comment+"</textarea>"
	                            }
	                           // nArray[i]=json[i].updated_by +" : "+status_comment;
	                    taskCmtUI += "<textarea id=\"OldcommentTextarea_"+json[i].comment_id+"\" style=\"display:none;\"  >"+status_comment+"</textarea>"
	                    taskCmtUI += "</div>"
		           }
		        /*   var maxValue ="";
		           if(subDateArray.length <= 20){
		            maxValue = 20;
		           }else if(subDateArray.length <= 100 && subDateArray.length > 20){
		            maxValue =50;
		           }else {
		            maxValue =100;
		           } */
		           //creatChartStatus(subArray,ssub,backgroundCol,projectId,nArray,subDateArray,maxValue);
		      // }
          //}
          //taskCmtUI.replaceAll("CHR(39)","'").replaceAll("CH(51)","\"").replaceAll("CH(52)","\'").replaceAll("CHR(26)",":").replaceAll("CHR(40)","\"");
          return taskCmtUI;
    }
    var GbcommentId="";
    function deleteCommentList(commentId){
        GbcommentId=commentId;
        parent.confirmReset(getValues(companyAlerts,"Alert_DelCmt"),'reset','deleteCommentListConfirm','cancelDelete');
    }
    function cancelDelete(){
        $('#confirmDiv').hide();
    }
    function deleteCommentListConfirm(){
        var projectId = $("input#globalProjectId").val();
        $('#loadingBar').show();
       timerControl("start");
       $.ajax({
                url : path + "/paLoad.do",
                type : "POST",
                data : {
                            act : "deleteProjCommentList",
                            commentId : GbcommentId
                        },
                error: function(jqXHR, textStatus, errorThrown) {
                        checkError(jqXHR,textStatus,errorThrown);
                        $("#loadingBar").hide();
                        timerControl("");
                        }, 
                success : function(result) {
                     checkSessionTimeOut(result);
                    projCommentList(projectId);
                   }
              });
    
    }
    function checkingTheVal(obj){
         var statuscommentId = $(obj).attr("id").split("_")[1];
         $('textarea#commentTextareaDiv_'+statuscommentId).show();
         $('div#commentTextarea_'+statuscommentId).hide();
         $('textarea#commentTextareaDiv_'+statuscommentId).focus();
    }
	var globalstatuscommentId="";
	var globalstatuscommentText="";
	function editProjStatusDiv(obj){
	   var statusText = $(obj).val().trim();
	   var statuscommentId = $(obj).attr("id").split("_")[1];
	   globalstatuscommentId = statuscommentId;
	   globalstatuscommentText = statusText;
	   //alert("statusText:>>"+statusText);
	    var oldData = $('#OldcommentTextarea_'+statuscommentId).val();
	    if(statusText!='' && oldData != statusText){
            parent.confirmReset(getValues(companyAlerts,"Alert_Save"),'reset','saveModifiedChanges','CancelChanges');
            //projCommentList(projectId);
		  }else if(statusText == ''){
		      alertFun(getValues(companyAlerts,"Alert_EntTxtInside"),'warning');
		  }
	
	}
	
	function CancelChanges(){
	   $('#commentTextarea_'+globalstatuscommentId).text($('#OldcommentTextarea_'+globalstatuscommentId).val());
	   $('#commentTextareaDiv_'+globalstatuscommentId).val($('#OldcommentTextarea_'+globalstatuscommentId).val());
	   $('#commentTextarea_'+globalstatuscommentId).show();
	   $('.commentTextClss').hide();
	}
	function saveModifiedChanges(){
	   var projectId = $("input#globalProjectId").val();
	   projCommentList(projectId);
	}
	function projStatusList(jsonResult){
		 var html='<span align="left" style="font-family: opensanssemibold;font-size: 12px;">'+getValues(companyLabels,"WS_Select_Status")+'</span>';
		 var jsonData = jQuery.parseJSON(jsonResult);
	     if(jsonData){
	    	 var json = eval(jsonData);
	    	 if(jsonData.length>0){
	    	 	 for(var i=0;i<json.length;i++){
					html+='<div id="status_'+json[i].status_id+'" class=\"statusColourCodes\" status_id="'+json[i].status_id+'"  status_code="'+json[i].status_code+'"  style="width:100%;border-top:1px solid #cccccc;height: 25px;padding: 2% 1%;color:#000000;cursor:pointer;" onclick="setProjStatus(this)">' 	 	 	 		    	    	
				  	html+='  <div style="background-color: '+json[i].status_code+';height: 4px;margin-top: 8px;"></div>'
					html+='</div>'
	    	 	 }
	    	 }
	     }	 	 
		return html;
	}
	
	function showStatusListDiv(){
	   var colourId = $('#hiddenProjCurrentStatus').val();
	   $('#projStatusListDiv').find('.statusColourCodes').show();
	   if(colourId != null && colourId != ""){
	       colourId = $('#hiddenProjCurrentStatus').val();
	   }else{
	       colourId = $('#hiddenProjPrevStatus').val();
	   }
	  $('#projStatusListDiv').find('#status_'+colourId).hide();
	  if($('#projStatusListDiv').is(":hidden")){
	     $('#projStatusListDiv').slideDown();
	  }else{
	     $('#projStatusListDiv').slideUp();
	  }
	}
	
	
	
	
	 function loadCkEditor2(type,projectId) {
			var ckeHeight = getHeightDynamically('projectDesc');
			ckeHeight = $("#rightProjectContainer").height()-120;
			var editor = CKEDITOR.instances['projDescription'];
	 	 try {
	 	   CKEDITOR.replace('projDescription', {
	 	      height: ckeHeight,
	           filebrowserUploadUrl:path+"/workspaceAction.do?act=ckWrkUploadImage&projectId="+projectId,
	        });
	 	} catch (err) {
	    	 alertFun(getValues(companyAlerts,"Alert_HandleError"),'warning');
	 	}
	}
	function saveUpdatedprojectDetails(){
		 var existingProjectName = 	$('#projNameHidden').val().trim();
		 var existingProjectCode = 	$('#projCodeHidden').val().trim();
		 var existingEmailAttachmentStatus = 	$('#projEmailAttachmentHidden').val().trim();
		 var existingProjectPrivicyStatus = $('input#projStatusTypeHidden').val().trim();
		 var existingProjectStatus = $('input#projStatusHidden').val().trim();
		 var position  = $('#hiddenProjSettingPosition').val().trim();
		 var prevStartDate  = $('#hiddenProjStartDate').val().trim();
		 var prevEndDate  = $('#hiddenProjEndDate').val().trim();
		 
		 var projectId = $("input#globalProjectId").val();
		 var projectCode = $("#projectTagName").val().trim();
		 var upload = $('#hideDiv_'+projectId).data('imgName');
		 //var splChar = /[!#@$`~:?<>\\\^&*(){}[\]<>?/|\-\"]/;
		 var splChar = /^\s*[a-zA-Z0-9,&,',_,.,@,\s]+\s*$/;
		 var projName = $("#projectName").val().trim();
		     projName = projName.replace('<br>','');
		     projName = projName.trim();
		 var projectNameFlag= true;
		 var projectCodeFlag= true;
		 var projectPrivicyStatusFlag=true;
		 var projStatFlag=true;
		 var imgUpdated="";
		 var projPrivicyType=$("img#imgChgType").attr('decideimg');
		 var projstat = $("select#projStatusType option:selected").attr('value');
		 var emailAttachFlag=true;
		 var startDate = $("input#startDate").val();
		 var endDate = $("input#endDate").val();
		 
		 $('textarea#projDescription').val(CKEDITOR.instances.projDescription.getData());
         var projDesc = $('textarea#projDescription').val();
		 if(glbprojectDesc == projDesc){
		    projDescFlag=true;
		 }else{
		   projDescFlag=false;
		 }
          // var projDesc = CKEDITOR.instances.projDescription.getData();
  		projDesc=projDesc.replaceAll('&quot;','"').replaceAll("&lt;","<").replaceAll("&gt;",">");
            var colAttDwnload = "checked";
		    var attchVal = "";
			if($('#emailAttachmentCheckVal').attr('checked')) {
				    colAttDwnload = "checked"
				} else {
					colAttDwnload = "unChecked"
			}
	     	
		 var changeFlag=true;
		 if(existingProjectName != projName){
		 	changeFlag= false;
		 	projectNameFlag = false;
		 }else if(existingProjectCode != projectCode){
		 	changeFlag=false;
		    projectCodeFlag =false;
		 }else if(!projDescFlag){
		 	changeFlag = false;
	   	 }else if(existingProjectPrivicyStatus != projPrivicyType){
	   		changeFlag = false;
	   	 }else if(existingProjectStatus != projstat){
	   		changeFlag = false;
	   	 }else if(typeof(upload) != 'undefined' || projectImgUploadFromGallery == true ){
	   		 changeFlag = false;
	   	 }else if(existingEmailAttachmentStatus != colAttDwnload){
	   		changeFlag= false;
	   		emailAttachFlag = false;
	   	 }else if(prevStartDate != startDate || prevEndDate != endDate){
	   		changeFlag= false;
	   	 }
	   	 
	   	 
	   	 
	   	 
		 if(!changeFlag){
			    if(projName == ""){
			 	   alertFun(getValues(companyAlerts,"Alert_PrjNmeNoEmt"), "warning");
			 	   return false;
			 	}
			 	if(!splChar.test(projName)) {
			 	   alertFun(getValues(companyAlerts,"Alert_ProjNameChar"), 'warning');
			 	   return false;
			 	}
			 	if(!projectNameFlag){
			 	   $("#loadingBar").show();
        		   timerControl("start");
			 	    $.ajax({
						url : path + "/workspaceAction.do",
						type: "POST",
						data: {act:"checkProjectNameExists",projName:projName,projectId:projectId},
						error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
						success: function(result) {
							checkSessionTimeOut(result);
						        $('#loadingBar').hide();
								timerControl("");
								if (result == 'Yes') {
									alertFun(getValues(companyAlerts,"Alert_ProjNameExist"),'warning');
									return false;
								} else {
								   if(!projectCodeFlag){
									    $('#loadingBar').show();
										timerControl("start");
									    var retFlag = true;
									    $.ajax({
											url : path + "/workspaceAction.do",
											dataType: "text",
											type : "POST",
											data : { act : "updateProjectCode",projectCode:projectCode,projectId:projectId},
											error: function(jqXHR, textStatus, errorThrown) {
									                checkError(jqXHR,textStatus,errorThrown);
									                $("#loadingBar").hide();
													timerControl("");
													}, 
											success : function(result){
												checkSessionTimeOut(result);
												$("#loadingBar").hide();
									        	timerControl("");
										    
										     	var existingProjectCodVal = 	$('#projCodeHidden').val().trim();
										        if(existingProjectCodVal == "-"){
										     		retFlag = true;
										        }else if(result == "failure"){
												    alertFun(getValues(companyAlerts,"Alert_PrjCodeExist"), 'warning');
												    retFlag = false;
										        }else if(result == "success"){
										            alertFun(getValues(companyAlerts,"Alert_OldPrjCode"), 'warning');
										            retFlag = true;
										        }
										        if(retFlag){
										           if(typeof(upload) != 'undefined'){
													   var obj = $('#wsImgUploadForm').data('obj');
										 			   submitProjImageUploadFormNew(obj);
										 			   imgUpdated="projectImgUpdated";
									 			       upload='undefined';
										   	       }
										           updateProjectDetails(projName, projectId, projectCode, startDate, endDate, projPrivicyType, projstat, projDesc,position);
										        }
										     }
										});
	    						   }else{
	    						       if(typeof(upload) != 'undefined'){
										   var obj = $('#wsImgUploadForm').data('obj');
							 			   submitProjImageUploadFormNew(obj);
							 			   imgUpdated="projectImgUpdated";
						 			       upload='undefined';
							   	       }
	    						       updateProjectDetails(projName, projectId, projectCode, startDate, endDate, projPrivicyType, projstat, projDesc,position,colAttDwnload);
	    						   }	 
							    }
						}
					});
			 	    
			 	}else if(!projectCodeFlag){
										$('#loadingBar').show();
										timerControl("start");
									    var retFlag = true;
									    $.ajax({
											url : path + "/workspaceAction.do",
											dataType: "text",
											type : "POST",
											data : { act : "updateProjectCode",projectCode:projectCode,projectId:projectId},
											error: function(jqXHR, textStatus, errorThrown) {
									                checkError(jqXHR,textStatus,errorThrown);
									                $("#loadingBar").hide();
													timerControl("");
													}, 
											success : function(result){
												checkSessionTimeOut(result);
												$("#loadingBar").hide();
									        	timerControl("");
										    
										     	var existingProjectCodVal = 	$('#projCodeHidden').val().trim();
										        if(existingProjectCodVal == "-"){
										     		retFlag = true;
										        }else if(result == "failure"){
												    alertFun(getValues(companyAlerts,"Alert_PrjCodeExist"), 'warning');
												    retFlag = false;
										        }else if(result == "success"){
										            alertFun(getValues(companyAlerts,"Alert_OldPrjCode"), 'warning');
										            retFlag = true;
										        }
										        if(retFlag){
										          if(typeof(upload) != 'undefined'){
													   var obj = $('#wsImgUploadForm').data('obj');
										 			   submitProjImageUploadFormNew(obj);
										 			   imgUpdated="projectImgUpdated";
									 			       upload='undefined';
										   	       }
										           updateProjectDetails(projName, projectId, projectCode, startDate, endDate, projPrivicyType, projstat, projDesc,position,colAttDwnload);
										        }
										     }
										});
				}else{
				   if(typeof(upload) != 'undefined'){
					   var obj = $('#wsImgUploadForm').data('obj');
		 			   submitProjImageUploadFormNew(obj);
		 			   imgUpdated="projectImgUpdated";
	 			       upload='undefined';
		   	       }
				   updateProjectDetails(projName, projectId, projectCode, startDate, endDate, projPrivicyType, projstat, projDesc,position,colAttDwnload);
				}
				
			 	//if(typeof(upload) != 'undefined'){
			   	//   var obj = $('#wsImgUploadForm').data('obj');
	 			//   submitProjImageUploadFormNew(obj);
	 			//   imgUpdated="projectImgUpdated";
	 			//   upload='undefined';
		   	    //}
			 	
		 }else{
	   	   	alertFun(getValues(companyAlerts,"Alert_PrjDetUnChanged"), 'warning');
	   	   	return false;
	 	 }
		 
		 
		// $("#loadingBar").show();
        // timerControl("start");
		 /*
		 $.ajax({
			url : path + "/workspaceAction.do",
			type : "POST",
			data : {
				act : "updateWrkSpaceProjDesc",
				projId : projectId,
				projDesc : projDesc
			},
			success : function(result) {
				checkSessionTimeOut(result);
				alertFun("Project description updated.", "warning");
				$('#loadingBar').hide();
				timerControl("");
			}
		 });
		 
		 
		if(projectNameFlag == false)
		 {
		 	if(projName == ""){
		 	   alertFun("Project name cannot be empty.", "warning");
		 	   $("#loadingBar").hide();
		       timerControl("");
		       return false;
		 	}
		 	if (splChar.test(projName)) {
		 	   alertFun("Project name should not contain special characters", 'warning');
		 	   $("#loadingBar").hide();
		       timerControl("");
		 	   return false;
		 	}
		 	checkIfProjectNameExists(projName, projectId);
		 }
		 
		 if(projectPrivicyStatusFlag == false){
		 	changeProjectImg(projectId);
		 }
		
		if( projStatFlag == false){
		 	changeProjectStatus(projectId);
		 }
		 
		if(projectCodeFlag == false){
		    checkIfProjectCodeExists(projectCode,projectId);
		 }
		 
		if(imgUpdated == 'projectImgUpdated'){
		 	alertFun("Project Image updated", 'warning');
		 	$("#loadingBar").hide();
		    timerControl("");
		 }
		 
		 else {
		 	  alertFun("Project details unchanged", 'warning');
		 	  $("#loadingBar").hide();
		      timerControl("");
		 }
	*/
	$("#fullProjectDetailsContainer").find("#projStatusUpdateMainDiv").attr('background-color', $("#projStatusUpdateDiv").attr('background-color'));
	}
	
	 function changeProjectImg(pid){
   		$("#loadingBar").show();
		timerControl("start");
   		var type = $("img#imgChgType").attr('decideimg');
   		$.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : { act : "changeProjectType",type:type,pId:pid },
		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		success : function(result){
			checkSessionTimeOut(result);
			$("#loadingBar").hide();
	        timerControl("");
			if(result == "success")
			{
			 alertFun(getValues(companyAlerts,"Alert_PrjDetUp"), 'warning');
			}
			else {
			alertFun(getValues(companyAlerts,"Alert_PrjTypeNoCng"), 'warning');
			}
		}
	  });
	   $("#loadingBar").hide();
	   timerControl("");
   }
   
   function changeProjectStatus(projectId){
		$('#loadingBar').show();
		timerControl("start");
		var projstat = $("select#projStatusType option:selected").attr('value');
		$.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : { act : "changeStatusOfProject", status:projstat,projectId:projectId },
		error: function(jqXHR, textStatus, errorThrown) {
                checkError(jqXHR,textStatus,errorThrown);
                $("#loadingBar").hide();
				timerControl("");
				}, 
		success : function(result){
			checkSessionTimeOut(result);
			if(result == "success")
			{
			 alertFun(getValues(companyAlerts,"Alert_PrjDetUp"), 'warning');
			}
		}
	  });
	  $("#loadingBar").hide();
	  timerControl("");
   }
   
   function changeApporvalRequest(id,obj){
   	var src = $(obj).attr('src'); // "static/images/banner/blue.jpg"
	var tarr = src.split('/');      // ["static","images","banner","blue.jpg"]
	var file = tarr[tarr.length-1]; // "blue.jpg"
	var data = file.split('.')[0]; // "blue"
	var text = "Your request to subscribe to "+$("input#projNameHidden").val()+" has been approved. If you need further assistance,contact the project admin."
	var text2 = "Your request to subscribe to "+$("input#projNameHidden").val()+"  has been denied. However, if you feel this needs further clarification, contact the Project Owner(s) for this project."
	$("textarea#textCommonArea_"+id+"").val("");
   	if(data == 'deny'){
   		$("img#appReq_"+id+"").attr("src","images/approve.png");
   		$("select#workspaceApp_"+id+"").show();
   		$("textarea#textCommonArea_"+id+"").val(text);
   		$("img#appReq_"+id+"").attr('deciapprreq','approve');
   	}else {
   		$("img#appReq_"+id+"").attr("src","images/deny.png");
   		$("select#workspaceApp_"+id+"").hide();
   		$("textarea#textCommonArea_"+id+"").val(text2);
   		$("img#appReq_"+id+"").attr('deciapprreq','deny');
   	}
  }
  
  function sendAppReq(id,email,name){
    var type = "DU";
	var permission = $("img#appReq_"+id+"").attr('deciapprreq');
	if( permission == 'approve'){
		type = $("select#workspaceApp_"+id+" option:selected").val();
	}
	var projectId = $("input#globalProjectId").val();
	var projectName = $("input#projNameHidden").val();
	var message = $("textarea#textCommonArea_"+id+"").val();
	
	$('#loadingBar').show();
	timerControl("start");
	$.ajax({
			url: path + "/landPageNotAction.do",
			type:"POST",
			data:{act:"saveApproveUserList",id:id,email:email,name:name,type:type,projectId:projectId,projectName:projectName,message:message},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
			if(result== "success"){
				checkSessionTimeOut(result);
				$("div#Notsuser_"+id+"").remove();
				approveFlag == false;
				
				if( permission == 'approve'){
				  alertFun(getValues(companyAlerts,"Alert_PrjReqGrant"),'warning');
				}
				else {
					alertFun(getValues(companyAlerts,"Alert_PrjReqDeny"),'warning');
				}
				
				if ($("div#apporvalResult").find("div[id^=Notsuser_]").length < 1 ){
					if (!isiPad) {
						$("div#apporvalResult").html('<span style="float:left;margin-left:10px;font-family:OpenSansRegular;">No pending subscription for the workspace</span>');
					}
					else{
					   $("div#apporvalResult .mCSB_container").html('<span style="float:left;margin-left:10px;font-family:OpenSansRegular;">No pending subscription for the workspace</span>');
					}
		   		}
			}
			$("#loadingBar").hide();
	        timerControl("");
		}
	 }); 
  }
   
   //code by sagar: For Social User invitations
   
   function inviteStdUsers(){
   		confirmFun(getValues(companyAlerts,"Alert_InvAllUsr"), "clear", "callInviteUserScreen");
   }
 	
   function callInviteUserScreen(){
   	var projectId = $("input#globalProjectId").val();
   	//$("div#inviteUsersToProjectWorkspace").hide();
   	$("div#commonUserListContainers").hide();
   	addCompanyContact('company','create','module',projectId)
   }
   
   function loadOptionalDrives(){
   		$('#loadingBar').show();
		timerControl("start");
   		$("div#templateContainer").hide();
   		$("div#inviteUsersToProjectWorkspace").hide();
   		$("div#taskCompletionCodesContainer").hide();
   		$("div#projectStatusContainer").hide();
   		$("div#apporvalRequestContainer").hide();
   		$("div#fullProjectDetailsContainer").hide();
   		$("div#integrationContainer").hide();
   		$("div#optionalDrivesContainer").show();
   		///$("button#saveForProjectDrives").show();
   		var projectId = $("input#globalProjectId").val();
   		$("div#optionalDrivesContent").html("");
   		getDynamicDrivesvalues();
   		
   		setTimeout(function() {
   		$.ajax({
			url:path+"/WorkspaceDocumentAction.do",
			type:"POST",
			data:{act:"getProjectSpecificDrivesForcheckBox",projId:projectId},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
				$("#loadingBar").hide();
	    		timerControl("");
					var jsonData = jQuery.parseJSON(result);
				    if(jsonData){
				    	 var json = eval(jsonData);
				    	 if(jsonData.length>0){
				    	 	 for(var i=0;i<json.length;i++){
				    	 		if($("div#optionalDrivesContent").find("img#driveIds_"+json[i].typeId+"")){
				    	 		    $("input#mailIds_"+json[i].typeId+"").val(json[i].default_email_address);
				    	 			if(json[i].changeIcon == "Y"){
				    	 			        $("img#driveIds_"+json[i].typeId+"").removeClass('p_drive_uncheck');
				    	 			        $("img#driveIds_"+json[i].typeId+"").addClass('p_drive_check');
				    	 			        $("img#driveIds_"+json[i].typeId+"").attr('src','images/selcettwo.png');
					    	 				$("div#otherOptions_"+json[i].typeId+"").show();
				    	 					$("img#chgLogIn_"+json[i].typeId+"").attr('src','images/onSwitch.png');
								  			if(json[i].value == "Google Drive"){
								  			   $("img#chgLogIn_"+json[i].typeId+"").attr('onclick', 'projectGDlogoutFun('+json[i].typeId+')');
								  			   $("img#chgLogIn_"+json[i].typeId+"").attr('title', 'Log out Google Drive');
								  			}else if(json[i].value == "OneDrive") {
								  			   $("img#chgLogIn_"+json[i].typeId+"").attr('onclick', 'projectODlogoutFun('+json[i].typeId+')');
								  			   $("img#chgLogIn_"+json[i].typeId+"").attr('title', 'Log out One Drive');
										   	}else if(json[i].value == "Box") {
										   	   $("img#chgLogIn_"+json[i].typeId+"").attr('onclick', 'projectBoxlogoutFun('+json[i].typeId+')');
								  			   $("img#chgLogIn_"+json[i].typeId+"").attr('title', 'Log out Box');
								  			}else if(json[i].value == "Drop Box") {
										   	   $("img#chgLogIn_"+json[i].typeId+"").attr('onclick', 'projectDBlogoutFun('+json[i].typeId+')');
								  			   $("img#chgLogIn_"+json[i].typeId+"").attr('title', 'Log out Dropbox');
								  			}
								  	}
				    	 			if(json[i].value == "Colabus" ){
				    	 				$("img#driveIds_"+json[i].typeId+"").removeAttr("onclick");
				    	 			}
				    	 		}
				    	 	 }
				    	 }
				 	} 	
			}
		});
		}, 2000);
   }
   
   function loadIntegrationDetails(){
   		$('#loadingBar').show();
		timerControl("start");
   		$("div#templateContainer").hide();
   		$("div#inviteUsersToProjectWorkspace").hide();
   		$("div#taskCompletionCodesContainer").hide();
   		$("div#projectStatusContainer").hide();
   		$("div#apporvalRequestContainer").hide();
   		$("div#fullProjectDetailsContainer").hide();
   		$("div#optionalDrivesContainer").hide();
   		$("div#integrationContainer").show();
   		///$("button#saveForProjectDrives").show();
   		var projectId = $("input#globalProjectId").val();
   		
   		$.ajax({
			url:path+"/WorkspaceDocumentAction.do",
			type:"POST",
			data:{act:"getIntegrationDetails",projId:projectId},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
			$("#loadingBar").hide();
	    	timerControl("");
				var jsonData = jQuery.parseJSON(result);
				    if(jsonData){
				    	 var json = eval(jsonData);
				    	 if(jsonData.length>0){
					    	 for(var i=0;i<json.length;i++){
						    	 if(jsonData[i].sparkStaus =='Y' ){
						    	 	$('#spark').css({'display':'block'});
						    	 }
						    	 if(jsonData[i].slackStaus =='Y' ){
						    	 	$('#slack').css({'display':'block'});
						    	 }
						    	 if(jsonData[i].slackStaus =='' && jsonData[i].sparkStaus == '' ){
						    	 	$('#integrationContent').html("<p>No Integration found </p>");
						    	 	$('#integrationContent').css({'text-align':'end'});
						    	 }
						    	 if(jsonData[i].slackStaus =='' && jsonData[i].sparkStaus == 'Y' ){
						    	 	$('#spark').css({'margin-left':'0%'});
						    	 }
					    	 }
				    	 }
				    }
				  
			}
		});
   }
   
   function saveProjectDrives(){
   	$('#loadingBar').show();
	timerControl("start");
   	var projectId = $("input#globalProjectId").val();
   	var driveCheckIds='';
   	var emailIds='';
   	
   	//previous code.
	  /*$('.p_drive_check').each(function() {
	   		driveCheckIds += $(this).attr('id') + ",";
	  });*/
	   	
   	/*var lastChar = driveCheckIds.slice(-1);
	if (lastChar == ',') {
	    driveCheckIds = driveCheckIds.slice(0, -1);
	}*/
	
	
	
	 $('.companySetItem').each(function() {
	   		//driveCheckIds += $(this).attr('id') + ",";
	   		if($(this).val()!=""){
	   			if(!validateEmailJs($(this).val(),true,true) ) {
				 	alertFun(getValues(companyAlerts,"Alert_EmailInvalid"), "warning");
					return false;
			   }else {
			   		driveCheckIds +=  $(this).attr('id') + "_" + $(this).val() + ",";
			   } 
	   		}
	  });
	   	
   	var lastChar = driveCheckIds.slice(-1);
	if (lastChar == ',') {
	    driveCheckIds = driveCheckIds.slice(0, -1);
	}
	
	$.ajax({
		     url:path+"/WorkspaceDocumentAction.do",
		     type:'POST',
		     dataType:'text',
		     data:{act:'insertProjectSpecificDrives',projectId:projectId,driveCheckIds:driveCheckIds,emailIds:emailIds},
		     error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		     success:function(result){
		    	 checkSessionTimeOut(result);
		     		 $("#loadingBar").hide();
	 				 timerControl(""); 	
		  			if(result == "success"){
		  				alertFun(getValues(companyAlerts,"Alert_PrjDrive"), "warning");
		  			}
		  			else {
		  				alertFun(getValues(companyAlerts,"Alert_PrjDriveFail"), "warning");
		  			}
			 }
 	 });
   }
   
   
   function changeDrive(obj){
	   	var className =  $(obj).attr('class');
	   	var type = $(obj).attr('type');
	   	var id = $(obj).attr('id').split('_')[1];
	   	if(type == "Google Drive" ){
	   		$("img#chgLogIn_"+id+"").attr("onclick", "clkGdrive('workspace',this)");
	   		$("input#hiddenGoogleId").val(id);
	   	}else if(type == "OneDrive") {
	   		$("img#chgLogIn_"+id+"").attr("onclick", "clkOdrive('workspace',this)");
	   		$("input#hiddenOnedriveId").val(id);
	   	}else if(type == "Box") {
	   		$("img#chgLogIn_"+id+"").attr("onclick", "clkBdrive('workspace',this)");
	   		$("input#hiddenBoxId").val(id);
	   	}else if(type == "Drop Box") {
	   		$("img#chgLogIn_"+id+"").attr("onclick", "clkDdrive('workspace',this)");
	   		$("input#hiddenDropboxId").val(id);
	   		
	   	}
   	
	   	if( className == "p_drive_uncheck"){
	   		$(obj).removeClass('p_drive_uncheck');
	   		$(obj).addClass('p_drive_check');
	   		$(obj).attr('src','images/selcettwo.png');
	   		$("div#otherOptions_"+id+"").show();
	   	}else {
	   		//$("input#mailIds_"+id+"").val("");
   			if(type == "Google Drive" ){
   				confirmFun(getValues(companyAlerts,"Alert_WlogoutGdrive"), "delete", "logOutFromGoogle");
   				///projectGDlogout(id);
   			}else if(type == "Drop Box"){
   			    confirmFun(getValues(companyAlerts,"Alert_WlogoutDbox"), "delete", "logOutFromDropbox");
   			}else if(type == "Box"){
   			    confirmFun(getValues(companyAlerts,"Alert_WlogoutBox"), "delete", "logOutFromBox");
   			}else if(type == "OneDrive"){
   			    confirmFun(getValues(companyAlerts,"Alert_WlogoutObox"), "delete", "logOutFromOnedrive");
   			}
   			else{
   				// code to log out from one drive.
   				$(obj).removeClass('p_drive_check');
	   			$(obj).addClass('p_drive_uncheck');
	   			$(obj).attr('src','images/selcet.png');
	   			$("div#otherOptions_"+id+"").hide();
   			}
	   	}
   }
   
   function logOutFromGoogle(){
   	  var id = $("input#hiddenGoogleId").val();
   	  $("img#driveIds_"+id+"").removeClass('p_drive_check');
      $("img#driveIds_"+id+"").addClass('p_drive_uncheck');
   	  $("img#driveIds_"+id+"").attr('src','images/selcet.png');
   	  projectGDlogout(id);
   }
   
   function logOutFromDropbox(){
      var id = $("input#hiddenDropboxId").val();
   	  $("img#driveIds_"+id+"").removeClass('p_drive_check');
      $("img#driveIds_"+id+"").addClass('p_drive_uncheck');
   	  $("img#driveIds_"+id+"").attr('src','images/selcet.png');
   	  projectDBlogout(id);
   }
   
   function logOutFromBox(){
      var id = $("input#hiddenBoxId").val();
   	  $("img#driveIds_"+id+"").removeClass('p_drive_check');
      $("img#driveIds_"+id+"").addClass('p_drive_uncheck');
   	  $("img#driveIds_"+id+"").attr('src','images/selcet.png');
   	  projectBoxlogout(id);
   }
   
   function getDynamicDrivesvalues(){
   var htmldata="";
	   $.ajax({
			     url:path+"/WorkspaceDocumentAction.do",
			     type:'POST',
			     dataType:'text',
			     data:{act:'getValuesFromLov'},
			     error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						},	 
			     success:function(result){
			    	 checkSessionTimeOut(result);
			  			if(result != "[]"){
			  				htmldata = prepareDrivesUI(result);
			  				$("div#optionalDrivesContent").append(htmldata);
			  			}
			  			else {
			  				alertFun(getValues(companyAlerts,"Alert_PrjDriveFail"), "warning");
			  			}
				 }
	 	 });
   }
   
   function prepareDrivesUI(jsonResult){
   		var data="";
	   	var jsonData = jQuery.parseJSON(jsonResult);
	    if(jsonData){
	    	 var json = eval(jsonData);
	    	 if(jsonData.length>0){
	    	 	 for(var i=0;i<json.length;i++){
	 				if(json[i].value == "Colabus" ){
				    	 data +=  "<div style=\"float:left;width:100%;margin-top:4px;\" > <img id=\"driveIds_"+json[i].Id+"\" type=\""+json[i].value+"\" class=\"p_drive_check\" style=\"float:left;\" src=\"images/selcettwo.png\" ><span style=\"float:left;width:50%;margin-left:2%;margin-top:-4px;\">"+json[i].value+"</span></div>";
				      }
	 				else{
	 					data +=  "<div style=\"float:left;width:100%;margin-top:20px;\" > "
	 					 + "<img id=\"driveIds_"+json[i].Id+"\" class=\"p_drive_uncheck\" type=\""+json[i].value+"\" onclick=\"changeDrive(this)\" style=\"float:left;\" src=\"images/selcet.png\" > " 
	 					 + " <span style=\"float:left;width:20%;margin-left:2%;margin-top:-4px;\">"+json[i].value+"</span>"
	 					 	 + "<input id=\"mailIds_"+json[i].Id+"\" class=\"form-control\" placeholder=\"Enter Email\" style=\"float:left;width:60%;margin-left:1px;margin-top:-10px;\" >" ; 
	 					 	 	if(json[i].value == "Google Drive" ){
	 					 	 	   data = data + "<div style=\"cursor:pointer;float:left;margin-left:5%;margin-top:-7px;\" > <img title=\"Configure Google Drive\" id=\"chgLogIn_"+json[i].Id+"\" onclick=\"clkGdrive('workspace',this)\" src=\"images/offSwitch.png\"></div>" ;
	 					 	 	}else if(json[i].value == "OneDrive") {
	 					 	 	   data = data + "<div style=\"cursor:pointer;float:left;margin-left:5%;margin-top:-7px;\" > <img title=\"Configure One Drive\" id=\"chgLogIn_"+json[i].Id+"\" onclick=\"clkOdrive('workspace',this)\" src=\"images/offSwitch.png\"></div>" ; 
	   							}else if(json[i].value == "Box") {
	 					 	 	   data = data + "<div style=\"cursor:pointer;float:left;margin-left:5%;margin-top:-7px;\" > <img title=\"Configure Box\" id=\"chgLogIn_"+json[i].Id+"\" onclick=\"clkBdrive('workspace',this)\" src=\"images/offSwitch.png\"></div>" ; 
	   							}else if(json[i].value == "Drop Box") {
	 					 	 	   data = data + "<div style=\"cursor:pointer;float:left;margin-left:5%;margin-top:-7px;\" > <img title=\"Configure Dropbox\" id=\"chgLogIn_"+json[i].Id+"\" onclick=\"clkDdrive('workspace',this)\" src=\"images/offSwitch.png\"></div>" ; 
	   							}
	 					 	
	 					 		/*+ "<div id=\"otherOptions_"+json[i].Id+"\" style=\"float:left;display:none;width:100%;margin:3%;\" >" 
		 					 		+ "<input id=\"mailIds_"+json[i].Id+"\" class=\"companySetItem\" placeholder=\"Enter Email\" style=\"float:left;width:30%;margin-left:1px;\" >"
		 					 		+ "<button id=\"callDynamicDrives_"+json[i].Id+"\" class=\"btn btn-info\" type=\"button\" style=\"padding: 4px 15px ! important; font-family: opensanscondbold; text-transform: uppercase; float: left;width:15%;margin-left: 5%;\">LOGIN</button>"
		 					 		+ "<div style=\"cursor:pointer;float:left;margin-left:5%;\" > <img id=\"chgLogIn_"+json[i].Id+"\" src=\""+path+"/images/offSwitch.png\"></div>"
		 					 	+ "</div>"*/
	 					 data = data + " </div>";
	 				}
	    	 	 }
	    	 }
	 	} 
	 	return data;
   }

 //for testing purpose
     var timer="";
	 var child="";
	 var reConfigchild="";
	 var checkedDrvId="";
	 
  function clkGdrive(gtype,obj){
	 var projectId = $("input#globalProjectId").val();
	 var projectName = $("input#projNameHidden").val();
	 var projImgSrc = $("input#hiddenCurrentProjSrc").val();
	 var tempId= obj.id.split('_')[1];
	 var email = $("input#mailIds_"+tempId+"").val().trim();
	 var id = $(obj).attr('id').split('_')[1];
	 checkedDrvId=id;
	 var checkedValCls=$("#driveIds_"+id+"").attr('class');
	 if(email == ""){
	 	alertFun(getValues(companyAlerts,"Alert_EntEmailId"), "warning");
		return false;
	 }
	 if(!validateEmailJs(email)) {
		alertFun(getValues(companyAlerts,"Alert_EmailInvalid"), "warning");
		return false;
	}
	if(checkedValCls == "p_drive_uncheck"){  
	    alertFun(getValues(companyAlerts,"Alert_SelGDriveOpt"), "warning");
		return false;
	}
	  var mAct="wsGdDrive";
	  $('#projId').val(projectId);
	 	$('#repositoryOpts').hide();
	 	$.ajax({
			url:path+'/GDServlet',
			type:'POST',
			dataType:'text',
			data:{action:'chkTkn',gtype:gtype,projectId:projectId,enteredEmail:email},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
					if(result !="success"){
					 reConfigchild=window.open('https://accounts.google.com/logout','','toolbar=0,status=0,width=0,height=0,scrollbars=1');
					//commented by sagar.
					     setTimeout(function(){
					       if(reConfigchild!=null)
					         reConfigchild.close();
						  $.ajax({
								url:path+'/GDServlet',
								type:'POST',
								dataType:'text',
								data:{action:'getGDFiles',gtype:gtype,projectId:projectId,enteredEmail:email},
								error: function(jqXHR, textStatus, errorThrown) {
						                checkError(jqXHR,textStatus,errorThrown);
						                $("#loadingBar").hide();
										timerControl("");
										}, 
								success:function(result){
									checkSessionTimeOut(result);
								      ///$('#gDrive').attr('url',''+result+'');
									  ///child = window.open(''+$('#gDrive').attr('url')+'','','toolbar=0,status=0,width=900,height=500,scrollbars=1');
									  child = window.open(result,'','toolbar=0,status=0,width=900,height=500,scrollbars=1');
							          timer = setInterval(checkChildForProjects, 500);
									}				
								});
						          },2000);
					}else{
					     window.location.href = path+'/Redirect.do?pAct='+mAct+'&gtype=workspace&projectId='+projectId+'&projectName='+projectName+'&projImgSrc='+projImgSrc;
	 				}
			   }
		   });
	 }
	 
	 function checkChildForProjects() {
	     var mAct="wsGdDrive";
	     var projectId = $("input#globalProjectId").val();
   	     var driveCheckIds='';
    	 var emailIds='';
    	 
	    if (child.closed) {
	    	clearInterval(timer);
	    	$('#wstransparentDiv').hide();
	    	parent.$('#transparentDiv').hide();
	    	$.ajax({
				url:path+'/GDServlet',
				type:'POST',
				dataType:'text',
				data:{action:'chkTkn',gtype:'workspace',projectId:projectId},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
					checkSessionTimeOut(result);
				        if(result=='error'){
						    $('#loadingBar').hide();
	  			   		    timerControl(""); 
						}else{
						var id="";
					       $('.form-control').each(function() {
						   		if($(this).val()!=""){
								   		driveCheckIds +=  $(this).attr('id') + "_" + $(this).val() + ",";
								   		id= $(this).attr('id').split('_')[1];
						   		}
						   });
						   	
					   		var lastChar = driveCheckIds.slice(-1);
							if (lastChar == ',') {
						       driveCheckIds = driveCheckIds.slice(0, -1);
							}
						   $.ajax({
							     url:path+"/WorkspaceDocumentAction.do",
							     type:'POST',
							     dataType:'text',
							     data:{act:'insertProjectSpecificDrives',projectId:projectId,driveCheckIds:driveCheckIds,emailIds:emailIds},
							     error: function(jqXHR, textStatus, errorThrown) {
						                checkError(jqXHR,textStatus,errorThrown);
						                $("#loadingBar").hide();
										timerControl("");
										},	 
							     success:function(result){
							    	    checkSessionTimeOut(result);
							     		 $("#loadingBar").hide();
						 				 timerControl(""); 	
							  			 if(result == "success"){
							  				alertFun(getValues(companyAlerts,"Alert_GDriveConfig"),'warning');
							  				$("img#chgLogIn_"+checkedDrvId+"").attr('src','images/onSwitch.png');
							  				$("img#chgLogIn_"+checkedDrvId+"").attr('onclick', 'projectGDlogoutFun('+checkedDrvId+')');
							  				$("img#chgLogIn_"+checkedDrvId+"").attr('title', 'LogOut');
							  				$("input#hiddenGoogleIdStatus").val('Y');
							  			 }
							  			 else {
							  				alertFun(getValues(companyAlerts,"Alert_PrjImpFail"), "warning");
							  			 }
								 }
					 	 });
	 					}
				
						/*if(result=='error'){
						}else{
						    window.location.href = path+'/Redirect.do?pAct='+mAct+'&gtype=workspace&projectId='+projectId+'&projectName='+projectName+'&projImgSrc='+projImgSrc;
						}*/
				}
			});
	    }
	}
	
	
	function projectGDlogoutFun(id){
	  confirmFun(getValues(companyAlerts,"Alert_WlogoutGdrive"), "delete", "logOutFromGoogle");
	}
	
	function projectGDlogout(id){
	
	var temp = $("input#hiddenGoogleIdStatus").val();
		var projectId = $("input#globalProjectId").val();
	    $('#loadingBar').show();
	  	timerControl("start");
			   	$.ajax({
					url:path+'/GDServlet',
					type:'POST',
					dataType:'text',
					data:{action:'logout',gtype:'workspace',projectId:projectId},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
						checkSessionTimeOut(result);
			        if(result == "success"){
					   
					   setTimeout(function() {
					   $("img#chgLogIn_"+id+"").attr('src','images/offSwitch.png');
					   //$("img#chgLogIn_"+id+"").attr('onclick', '');
					   $("#driveIds_"+id+"").removeClass('p_drive_check');
	   				   $("#driveIds_"+id+"").addClass('p_drive_uncheck');
	   				   $("#driveIds_"+id+"").attr('src','images/selcet.png');
	   				   ////$("input#mailIds_"+id+"").val("");
	   				   
	   				   $("img#chgLogIn_"+id+"").attr('title', 'Configure Google Drive');
	   				   $("input#hiddenGoogleIdStatus").val('N');
	   				   $("img#chgLogIn_"+id+"").attr('onclick', 'clkGdrive("workspace",this)');
	   				   alertFun(getValues(companyAlerts,"Alert_LogoutGdrive"),'warning');
	   				   },2000);
					 }
					$('#loadingBar').hide();
			   		timerControl("");
					}
				});
			//}
			
	}
	
/// code by sagar : function to implement project specific one drive...

	 var otimer="";
	 var ochild="";
	 var onedriveCheckedId="";
	 function clkOdrive(gtype,obj){
	 var projectId = $("input#globalProjectId").val();
	 var projectName = $("input#projNameHidden").val();
	 var projImgSrc = $("input#hiddenCurrentProjSrc").val();
	 var tempId= obj.id.split('_')[1];
	 var email = $("input#mailIds_"+tempId+"").val().trim();
	 var id = $(obj).attr('id').split('_')[1];
	 onedriveCheckedId=id;
	 var checkedValCls=$("#driveIds_"+id+"").attr('class');
	 if(email == ""){
	 	alertFun(getValues(companyAlerts,"Alert_EntEmailId"), "warning");
		return false;
	 }
	 if(!validateEmailJs(email)) {
		alertFun(getValues(companyAlerts,"Alert_EmailInvalid"), "warning");
		return false;
	}
	if(checkedValCls == "p_drive_uncheck"){  
	    alertFun(getValues(companyAlerts,"Alert_SelODriOpt"), "warning");
		return false;
	}
	  	var mAct="wsOneDrive";
			$.ajax({
				url:path+'/OnedriveServlet',
				type:'POST',
				dataType:'text',
				data:{action:'chkODriveTkn',gtype:gtype,projectId:projectId},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){  
					tkn = result;
					if(tkn!="success"){	
					reConfigchild=window.open('https://login.live.com/oauth20_logout.srf','','toolbar=0,status=0,width=0,height=0,scrollbars=1');
				    setTimeout(function(){
					reConfigchild.close();
					  $.ajax({
							url:path+'/OnedriveServlet',
							type:'POST',
							dataType:'text',
							data:{action:'getODFiles',gtype:gtype,projectId:projectId},
							error: function(jqXHR, textStatus, errorThrown) {
					                checkError(jqXHR,textStatus,errorThrown);
					                $("#loadingBar").hide();
									timerControl("");
									}, 
							success:function(result){
								checkSessionTimeOut(result);
								/*$('#oDrive').attr('url',''+result+'');
								ochild = window.open(''+$('#oDrive').attr('url')+'','','toolbar=0,status=0,width=900,height=500,scrollbars=1');
								*/
								ochild = window.open(result,'','toolbar=0,status=0,width=900,height=500,scrollbars=1');
					        	otimer = setInterval(checkODriveChildForProjects, 500);
							}
					  });
					 },2000);
					}else{
					     ///window.location.href = path+'/Redirect.do?pAct='+mAct+'&gtype=workspace&projectId='+projectId+'&projectName='+projectName+'&projImgSrc='+projImgSrc;
	 				}
			}
		});
	 }
	 
	 // check function for one drive for project specific drives 
	 
	  function checkODriveChildForProjects() {
	  var mAct="wsOneDrive";
	  var projectId = $("input#globalProjectId").val();
   	     var driveCheckIds='';
    	 var emailIds='';
	    if (ochild.closed) {
	    	clearInterval(otimer);
	    	$('#rtransparentDiv').hide();
	        $('#transparentDiv').hide();
	    	$.ajax({
				url:path+'/OnedriveServlet',
				type:'POST',
				dataType:'text',
				data:{action:'chkODriveTkn',gtype:"workspace",projectId:projectId},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
						if(result=='error'){
							$('#loadingBar').hide();
	  			   		    timerControl(""); 
						}else{
							// new enhancements for the functionality 
							var id="";
					        $('.form-control').each(function() {
						   		if($(this).val()!=""){
								   	driveCheckIds +=  $(this).attr('id') + "_" + $(this).val() + ",";
								   	id= $(this).attr('id').split('_')[1];
						   		}
						    });
						   	
					   		var lastChar = driveCheckIds.slice(-1);
							if (lastChar == ',') {
						      driveCheckIds = driveCheckIds.slice(0, -1);
							}
						   
						   	$.ajax({
							     url:path+"/WorkspaceDocumentAction.do",
							     type:'POST',
							     dataType:'text',
							     data:{act:'insertProjectSpecificDrives',projectId:projectId,driveCheckIds:driveCheckIds,emailIds:emailIds},
							     error: function(jqXHR, textStatus, errorThrown) {
						                checkError(jqXHR,textStatus,errorThrown);
						                $("#loadingBar").hide();
										timerControl("");
										}, 	
							     success:function(result){
							    	   checkSessionTimeOut(result);
							     		 $("#loadingBar").hide();
						 				 timerControl(""); 	
							  			 if(result == "success"){
							  				alertFun(getValues(companyAlerts,"Alert_ODriConfig"),'warning');
							  				$("img#chgLogIn_"+onedriveCheckedId+"").attr('src','images/onSwitch.png');
							  				$("img#chgLogIn_"+onedriveCheckedId+"").attr('onclick', 'projectODlogout('+onedriveCheckedId+')');
							  				$("img#chgLogIn_"+onedriveCheckedId+"").attr('title', 'LogOut');
							  			 }
							  			 else {
							  				alertFun(getValues(companyAlerts,"Alert_PrjImpFail"), "warning");
							  			 }
								 }
					 	 });
						   ///window.location.href = path+'/Redirect.do?pAct='+mAct+'&gtype=workspace&projectId='+projectId+'&projectName='+projectName+'&projImgSrc='+projImgSrc;
	 					}
				}
			});
	    		
	    }
	}
   
     /********* Box functionality starts *******/
     var boxchild = "";
     var boxCheckedId="";
     function clkBdrive(gtype,obj){
        var mAct="bDrive";
	   	var projectId = $("input#globalProjectId").val();
	 	var projectName = $("input#projNameHidden").val();
	 	var projImgSrc = $("input#hiddenCurrentProjSrc").val();
	 	var id = $(obj).attr('id').split('_')[1];
	 	boxCheckedId=id;
	 	var tempId= obj.id.split('_')[1];
	 	var email = $("input#mailIds_"+tempId+"").val().trim();
	 	var checkedValCls=$("#driveIds_"+id+"").attr('class');
		if(email == ""){
	 		alertFun(getValues(companyAlerts,"Alert_EntEmailId"), "warning");
			return false;
	 	}
	 	if(!validateEmailJs(email)) {
			alertFun(getValues(companyAlerts,"Alert_EmailInvalid"), "warning");
			return false;
		}
		if(checkedValCls == "p_drive_uncheck"){  
	    	alertFun(getValues(companyAlerts,"Alert_SelBoxOpt"), "warning");
			return false;
		}
	   	$.ajax({
			url:path+'/BoxResponse',
			type:'POST',
			dataType:'text',
			data:{action:'chkBoxTkn',gtype:gtype,projectId:projectId,enteredEmail:email},
			success:function(result){
					if(result !="success"){	
					reConfigchild=window.open('https://www.box.com/logout','','toolbar=0,status=0,width=0,height=0,scrollbars=1');
				    setTimeout(function(){
					reConfigchild.close();
						$.ajax({
							url:path+'/BoxResponse',
							type:'POST',
							dataType:'text',
							data:{action:'getdata',gtype:gtype,projectId:projectId,enteredEmail:email},
							success:function(result){
								checkSessionTimeOut(result);
								boxchild = window.open(''+result+'','','scrollbars=1,toolbar=0,status=0,width=900,height=500');
								timer = setInterval(checkBoxChildWS, 500); 							
						    }	
		                  });	
	                  
					},2000);																																								
				}
			}
		});
     }
	
	function checkBoxChildWS(){
		var mAct="bDrive";
		var projectId = $("input#globalProjectId").val();
	    var gtype="workspace";
	    var driveCheckIds='';
	    var emailIds='';
		if (boxchild.closed) {
		    clearInterval(timer);
		   $('#transparentDiv').hide();
		   $.ajax({
				url:path+'/BoxResponse',
				type:'POST',
				dataType:'text',
				data:{action:'chkBoxTkn',gtype:gtype,projectId:projectId},
				success:function(result){
					 checkSessionTimeOut(result);
					 if(result=="error"){
							 $('#transparentDiv').hide();
							}else{
						   var id="";
					       $('.form-control').each(function() {
						   		if($(this).val()!=""){
								   		driveCheckIds +=  $(this).attr('id') + "_" + $(this).val() + ",";
								   		id= $(this).attr('id').split('_')[1];
						   		}
						   });
						   	
					   		var lastChar = driveCheckIds.slice(-1);
							if (lastChar == ',') {
						    driveCheckIds = driveCheckIds.slice(0, -1);
							}
							
						   $.ajax({
							     url:path+"/WorkspaceDocumentAction.do",
							     type:'POST',
							     dataType:'text',
							     data:{act:'insertProjectSpecificDrives',projectId:projectId,driveCheckIds:driveCheckIds,emailIds:emailIds},
							     success:function(result){
							    	    checkSessionTimeOut(result);
							     		 $("#loadingBar").hide();
						 				 timerControl(""); 
							  			 if(result == "success"){
							  				alertFun(getValues(companyAlerts,"Alert_BoxConfig"),'warning');
							  				$("img#chgLogIn_"+boxCheckedId+"").attr('src','images/onSwitch.png');
							  				$("img#chgLogIn_"+boxCheckedId+"").attr('onclick', 'projectBoxlogoutFun('+boxCheckedId+')');
							  				$("img#chgLogIn_"+boxCheckedId+"").attr('title', 'LogOut');
							  			 }
							  			 else {
							  				alertFun(getValues(companyAlerts,"Alert_PrjImpFail"), "warning");
							  			 }
							  			 $('#transparentDiv').hide();
								 }
					 	 });
	 					}
				}
		      });
		    }
		}
		
		function projectBoxlogout(id){
	    var projectId = $("input#globalProjectId").val();
	    $('#loadingBar').show();
	  	timerControl("start");
		$.ajax({
			url:path+'/BoxResponse',
			type:'POST',
			dataType:'text',
			data:{action:'logout',gtype:'workspace',projectId:projectId},
			success:function(result){
				checkSessionTimeOut(result);
			        if(result == "success"){
					   
					   setTimeout(function() {
					   $("img#chgLogIn_"+id+"").attr('src','images/offSwitch.png');
					   //$("img#chgLogIn_"+id+"").attr('onclick', '');
					   $("#driveIds_"+id+"").removeClass('p_drive_check');
	   				   $("#driveIds_"+id+"").addClass('p_drive_uncheck');
	   				   $("#driveIds_"+id+"").attr('src','images/selcet.png');
	   				   ////$("input#mailIds_"+id+"").val("");
	   				   
	   				   $("img#chgLogIn_"+id+"").attr('title', 'Configure Box');
	   				   $("img#chgLogIn_"+id+"").attr('onclick', 'clkBdrive("workspace",this)');
	   				   alertFun(getValues(companyAlerts,"Alert_Logoutbox"),'warning');
	   				   },2000);
					 }
					$('#loadingBar').hide();
			   		timerControl("");
					}
				});
			//}
			
	}
     /*********Box functionality ends **********/
   
     /********* DropBox functionality starts *******/
    
     
    var dropboxChild = "";
    var dropboxCheckedId="";
	function clkDdrive(gtype,obj){
	    var mAct="dropBox";
	   	var projectId = $("input#globalProjectId").val();
	 	var projectName = $("input#projNameHidden").val();
	 	var projImgSrc = $("input#hiddenCurrentProjSrc").val();
	 	var id = $(obj).attr('id').split('_')[1];
	 	dropboxCheckedId=id;
	 	var tempId= obj.id.split('_')[1];
	 	var email = $("input#mailIds_"+tempId+"").val().trim();
	 	var checkedValCls=$("#driveIds_"+id+"").attr('class');
		if(email == ""){
	 		alertFun(getValues(companyAlerts,"Alert_EntEmailId"), "warning");
			return false;
	 	}
	 	if(!validateEmailJs(email)) {
			alertFun(getValues(companyAlerts,"Alert_EmailInvalid"), "warning");
			return false;
		}
		if(checkedValCls == "p_drive_uncheck"){  
	    	alertFun(getValues(companyAlerts,"Alert_DropboxOpt"), "warning");
			return false;
		}
		$.ajax({
			url:path+'/DropBoxServlet',
			type:'POST',
			dataType:'text',
			data:{action:'chkTkn',gtype:gtype,projectId:projectId},
			success:function(result){
				checkSessionTimeOut(result);
				if(result !="success"){
				   reConfigchild=window.open('https://www.dropbox.com/logout','','toolbar=0,status=0,width=0,height=0,scrollbars=1');
				   setTimeout(function(){
					  reConfigchild.close();
					  $.ajax({
							url:path+'/DropBoxServlet',
							type:'POST',
							dataType:'text',
							data:{action:'getDropBoxAuthURL',gtype:gtype,projectId:projectId,enteredEmail:email},
							success:function(result){
								 checkSessionTimeOut(result);
								 
								 dropboxChild = window.open(result,'','scrollbars=1,toolbar=0,status=0,width=900,height=500');
					             timer = setInterval(chkdropboxChildWS, 500);							
						    }	
					});	
					},2000);
			  }else{
			  }
			}
		});
	}
	
	function chkdropboxChildWS(){
	    var projectId = $("input#globalProjectId").val();
	    var mAct="dropBox";
	    var gtype="workspace";
	    var driveCheckIds='';
    	var emailIds='';
		if (dropboxChild.closed) {
		    clearInterval(timer);
		    $.ajax({
				url:path+'/DropBoxServlet',
				type:'POST',
				dataType:'text',
				data:{action:'chkTkn',gtype:gtype,projectId:projectId},
				success:function(result){
					checkSessionTimeOut(result);
						if(result=="error"){
							 $('#transparentDiv').hide();
						}else{
						   var id="";
					       $('.form-control').each(function() {
						   		if($(this).val()!=""){
								   		driveCheckIds +=  $(this).attr('id') + "_" + $(this).val() + ",";
								   		id= $(this).attr('id').split('_')[1];
						   		}
						   });
						   	
					   		var lastChar = driveCheckIds.slice(-1);
							if (lastChar == ',') {
						    driveCheckIds = driveCheckIds.slice(0, -1);
							}
							
						    $.ajax({
							     url:path+"/WorkspaceDocumentAction.do",
							     type:'POST',
							     dataType:'text',
							     data:{act:'insertProjectSpecificDrives',projectId:projectId,driveCheckIds:driveCheckIds,emailIds:emailIds},
							     success:function(result){
							    	    checkSessionTimeOut(result);
							     		 $("#loadingBar").hide();
						 				 timerControl(""); 	
							  			 if(result == "success"){
							  				alertFun(getValues(companyAlerts,"Alert_DropboxConfig"),'warning');
							  				$("img#chgLogIn_"+dropboxCheckedId+"").attr('src','images/onSwitch.png');
							  				$("img#chgLogIn_"+dropboxCheckedId+"").attr('onclick', 'projectDBlogoutFun('+dropboxCheckedId+')');
							  				$("img#chgLogIn_"+dropboxCheckedId+"").attr('title', 'LogOut');
							  			 }
							  			 else {
							  				alertFun(getValues(companyAlerts,"Alert_PrjImpFail"), "warning");
							  			 }
							  			 $('#transparentDiv').hide();
								 }
					 	 });
	 					}
							}
							});
		}
	}
	
	function projectDBlogoutFun(id){
	  confirmFun(getValues(companyAlerts,"Alert_WlogoutDbox"), "delete", "logOutFromDropbox");
	}
	
	function projectBoxlogoutFun(id){
	  confirmFun(getValues(companyAlerts,"Alert_WlogoutBox"), "delete", "logOutFromBox");
	}
	
	function projectODlogoutFun(id){
	  confirmFun(getValues(companyAlerts,"Alert_WlogoutObox"), "delete", "logOutFromOnedrive");
	}
	
    function logOutFromDropbox(){
   	  var id = $("input#hiddenDropboxId").val();
   	  $("img#driveIds_"+id+"").removeClass('p_drive_check');
      $("img#driveIds_"+id+"").addClass('p_drive_uncheck');
   	  $("img#driveIds_"+id+"").attr('src','images/selcet.png');
   	  projectDBlogout(id);
    }
   
    function logOutFromOnedrive(){
   	  var id = $("input#hiddenOnedriveId").val();
   	  $("img#driveIds_"+id+"").removeClass('p_drive_check');
      $("img#driveIds_"+id+"").addClass('p_drive_uncheck');
   	  $("img#driveIds_"+id+"").attr('src','images/selcet.png');
   	  projectODlogout(id);
    }
    
    function projectODlogout(id){
        var projectId = $("input#globalProjectId").val();
	    $('#loadingBar').show();
	  	timerControl("start");
		$.ajax({
			url:path+'/OnedriveServlet',
			type:'POST',
			dataType:'text',
			data:{action:'logout',gtype:'workspace',projectId:projectId},
			success:function(result){
				checkSessionTimeOut(result);
			        if(result == "success"){
					   
					   setTimeout(function() {
					   $("img#chgLogIn_"+id+"").attr('src','images/offSwitch.png');
					   //$("img#chgLogIn_"+id+"").attr('onclick', '');
					   $("#driveIds_"+id+"").removeClass('p_drive_check');
	   				   $("#driveIds_"+id+"").addClass('p_drive_uncheck');
	   				   $("#driveIds_"+id+"").attr('src','images/selcet.png');
	   				   ////$("input#mailIds_"+id+"").val("");
	   				   
	   				   $("img#chgLogIn_"+id+"").attr('title', 'Configure Onedrive');
	   				   $("img#chgLogIn_"+id+"").attr('onclick', 'clkOdrive("workspace",this)');
	   				   alertFun(getValues(companyAlerts,"Alert_LogoutOdrive"),'warning');
	   				   },2000);
					 }
					$('#loadingBar').hide();
			   		timerControl("");
					}
				});
			//}
			
    }
    
    function projectDBlogout(id){
	    var projectId = $("input#globalProjectId").val();
	    $('#loadingBar').show();
	  	timerControl("start");
		$.ajax({
			url:path+'/DropBoxServlet',
			type:'POST',
			dataType:'text',
			data:{action:'logout',gtype:'workspace',projectId:projectId},
			success:function(result){
				checkSessionTimeOut(result);
			        if(result == "success"){
					   setTimeout(function() {
					   $("img#chgLogIn_"+id+"").attr('src','images/offSwitch.png');
					   $("#driveIds_"+id+"").removeClass('p_drive_check');
	   				   $("#driveIds_"+id+"").addClass('p_drive_uncheck');
	   				   $("#driveIds_"+id+"").attr('src','images/selcet.png');
	   				   
	   				   $("img#chgLogIn_"+id+"").attr('title', 'Configure Drop Box');
	   				   $("input#hiddenGoogleIdStatus").val('N');
	   				   $("img#chgLogIn_"+id+"").attr('onclick', 'clkDdrive("workspace",this)');
	   				   alertFun(getValues(companyAlerts,"Alert LogoutDropbox"),'warning');
	   				   },2000);
					 }
					$('#loadingBar').hide();
			   		timerControl("");
					}
				});
			//}
			
	} 
    /*********DropBox functionality ends **********/
    
    
  