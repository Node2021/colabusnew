var limitAct1 = 0;
var index1 = 0;
var firstClickFlag1 = 'true';
var flag1 = 'true';
var indexFlag1 = 'true';
var glbtaskType="";
var glbtabType="";
var glbmyzone="";

$(window).scroll(function(){
		if($(window).scrollTop() + $(window).height() >= $(document).height()){
			if(firstClickFlag1 != 'false'){
				index1 = index1 + limitAct1;
				indexFlag1 = 'false';
			}
			if(flag1 == 'true' && firstClickFlag1 == 'true'){
			 if(glbtaskType == 'historyTask'){
				loadHistoryTaskListViewData('');
			 }else if(glbtaskType == 'myTask'){
				loadMyTaskListViewData('');
			 }else if(glbtaskType == 'assignedTask'){
			   loadAssignedTaskListViewData('');
			 }else{
			  loadMyTaskListViewData('');
			 }
			}
		}
		//$('.oldrowTab').show();
		//$('.newrowTab').hide();
		if($(window).scrollTop() <= ($('#taskContainer').offset().top - $("#tabMainDiv").innerHeight() - 40) &&  $('#newTaskUI').css("display") == "block"){
			 $('.oldrowTab').hide();
			 $('.newrowTab').show();
			 var topH = $("#tabMainDiv").innerHeight();
			 $(".contentCls").css("top",topH);
		}else{
			$('.oldrowTab').show();
			$('.newrowTab').hide();
		}
 });
var prjid = window.sessionStorage.projectidglb; 
function loadMyTaskListViewData(firstClick){                   /*-- For CalenderListView fuction start--*/
		globalViewSet="calendarView";
		$("#loadingBar").show();
		timerControl("start");
		$("#taskContentDiv").show();
		globalSaveMode = "listViewMyTask";
		globVarClickPlace = "";
		flagView = "";
		$('#rowTab').show();
		$("#calendarContainer").hide();
		$("#calListViewData").hide();
		$("#mytaskDashboard").hide();
		$('.wsHideIcons, .googleTask').css("display", "block");
		$("#googleTaskImg").attr("onclick","switchToGoogleTasks();");
		var tcd = isNaN($('#tabContentDiv').height());
		if(tcd == NaN){
			tcd = 0;
		}	
		var actConvDivHeight = tcd - $('#rowTab').height();
		//$("body").css("overflow", "auto");
		$("body").css("overflow-y", "auto");
		$("body").css("overflow-x", "hidden !important;");
		$('.googleColabusTask').css("display", "none");
	   // $("#mytaskdatalist").hide();
		 limitAct1 = Math.round(actConvDivHeight / 44) + 15;
		
		if(firstClick == 'firstClick'){
		   limitAct1 = Math.round(actConvDivHeight / 44) + 15;
		   index1 = 0;
		   flag1 = 'true';
		   indexFlag1 = 'true';
		   firstClickFlag1 = 'false';
		   $("div#taskContainer").html('');
			$('select#sortTasks option[value="All"]').prop("selected", "selected");
		   $('select#sortInWs option[value="sort"]').prop("selected", "selected");
		   $("#searchBox").val("");
		   sortVal='';
		   sortTType='all';
		   searchWord='';
		   parent.$("#loadingBar").show();
		  parent.timerControl("start");
		   ///$('#transparentDiv').show();
		} 
	//	$(".calendarDesign").hide();
		$("#calViewData").show().attr("onclick","loadTaskCalendarViewData('','"+firstClick+"')");
		//$("#wrkCreateTask").attr("onclick","createTaskUI('','Tasks', '', 'listViewMyTask')");
		$("#wrkCreateTask").attr("onclick","newUicreateTaskUI('create','0','Tasks', '', 'listViewMyTask')");
		let jsonbody = {
			    "user_id":userIdglb,
			    "company_id":companyIdglb,
			    "limit":limitAct1,
			    "index":index1,
			    "sortVal":sortVal,
			    "text":searchWord,
			    "sortTaskType":sortTType
			
		}
		checksession();
		$.ajax({ 
			   url: apiPath+"/"+myk+"/v1/getTaskDetails/MyZone",
			   type:"POST",
			   contentType:"application/json",
			   dataType:'json',
			   data: JSON.stringify(jsonbody),
			   beforeSend: function (jqXHR, settings) {
					xhrPool.push(jqXHR);
				},	
			   error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$("#loadingBar").hide();
						timerControl("");
						}, 
			   success:function(result){
				  
				  jsonData = result;
					  if(jsonData.length>0){
						  result = prepareUI();
					  }else{
						  flag1 = 'false';
						  if($('.docListViewRowCls').length > 0){
							  result="";
						  }else{
							  result = '<div id="innerDiv6" align="center" style="margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>';
						  }
						//result='<div id="innerDiv6" align="center" style="color:#ffffff;margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>';
					}  
				  $("#taskContainer").append(result);
				  $("#taskContainer .docListViewBorderCls").on("mouseover",function(){
					   $(this).find('.hover-comment-icons').show();
				  }).on("mouseout", function(){
					  $(this).find('.hover-comment-icons').hide();
				  });
				  $("#taskContentDiv").show();
				   $("#taskContainer").show();
					   var topH = $("#tabMainDiv").height();
				  $(".taskListing").css("top",topH);
					  indexFlag1 = 'true';
				   firstClickFlag1 = 'true';
				  // $('#transparentDiv').hide();
					if($("#taskLayout").is(':visible')==true){
							$('#transparentDiv').show();
					}
					else{
						$('#transparentDiv').hide();
					}
					 if(NotificationType !='null' && NotificationType !=''){
						loadNotificationsFun(NotificationTaskId,NotificationType);
					  }
					 if(voiceCreate != null && voiceCreate != ''){
						 voiceRedirectionFunction(); // Newly added function for redirecting to task creation form by voice command. 
					 }
					 
				   $("#loadingBar").hide();
				   timerControl("");      	
				}
		 });
}

function loadAssignedTaskListViewData(firstClick){
	globalViewSet="calendarView";
	$("#loadingBar").show();
	timerControl("start");
	$('.wsHideIcons').css("display", "block");
	$('.googleTask').css("display", "none");
	$("#taskContentDiv").show();
	$('#rowTab').show();
	$("#mytaskDashboard").hide();
	$("#mytaskdatalist").hide();
	globalSaveMode = "listViewAssignedTask";
	$("#calViewData").show();
	$("#calListViewData").hide();
	globVarClickPlace = "";
	flagView = "";
	//$("body").css("overflow", "auto");
	$("body").css("overflow-y", "auto");
	$("body").css("overflow-x", "hidden !important;");
	//$("#wrkCreateTask").attr("onclick","createTaskUI('','Tasks', '','listViewAssignedTask')");
	$("#wrkCreateTask").attr("onclick","newUicreateTaskUI('','0','Tasks', '','listViewAssignedTask')");
	$('.googleColabusTask').css("display", "none");
	$("#calendarContainer").hide();
	var tcd = isNaN($('#tabContentDiv').height());
		if(tcd == NaN){
			tcd = 0;
		}
	var actConvDivHeight = tcd - $('#rowTab').height();
	limitAct1 = Math.round(actConvDivHeight / 44) + 15;
	
		 if(firstClick == 'firstClick'){
			 limitAct1 = Math.round(actConvDivHeight / 44) + 15;
			 index1 = 0;
			 flag1 = 'true';
			 indexFlag1 = 'true';
			 firstClickFlag1 = 'false';
			 $(".taskListing").css("top",$("#tabMainDiv").height());
			 $('#transparentDiv').show();
			 $("div#taskContainer").html('');
			 $('select#sortTasks option[value="All"]').prop("selected", "selected");
			$('select#sortInWs option[value="sort"]').prop("selected", "selected");
			$("#searchBox").val("");
			sortVal='';
			sortTType='all';
			searchWord='';
			 parent.$("#loadingBar").show();
			parent.timerControl("start");
		} 
	
		let jsonbody = {
			    "user_id":userIdglb,
			    "company_id":companyIdglb,
			    "limit":limitAct1,
			    "index":index1,
			    "sortVal":sortVal,
			    "text":searchWord,
			    "sortTaskType":sortTType
			
		}
		checksession();
		$.ajax({ 
			   url: apiPath+"/"+myk+"/v1/getTaskDetails/assignedTask",
			   type:"POST",
			   contentType:"application/json",
			   dataType:'json',
			   data: JSON.stringify(jsonbody),
			   error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					$("#loadingBar").hide();
					timerControl("");
					}, 
		   success:function(result){					
			checkSessionTimeOut(result);
			jsonData = result;
				if(jsonData.length>0){
					result = prepareUI();
				}else{
					flag1 = 'false';
					if($('.docListViewRowCls').length > 0){
						  result="";
					  }else{
						  result = '<div id="innerDiv6" align="center" style="margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>';
					  }
					//result='<div id="innerDiv6" align="center" style="color:#ffffff;margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>';
				}  
				$("#taskContainer").show();
				$("#taskContainer").append(result);
				$("#taskContainer .docListViewBorderCls").on("mouseover",function(){
					   $(this).find('.hover-comment-icons').show();
				}).on("mouseout", function(){
					  $(this).find('.hover-comment-icons').hide();
				});
				   var topH = $("#tabMainDiv").height();
				$(".taskListing").css("top",topH);
				   indexFlag1 = 'true';
				firstClickFlag1 = 'true';
				if($("#taskLayout").is(':visible')==true){
						$('#transparentDiv').show();
				}
				else{
					$('#transparentDiv').hide();
				}	
				
				   $("#loadingBar").hide();
				timerControl("");   
			 
			 }
	   });
}

function loadHistoryTaskListViewData(firstClick){
 globalViewSet="calendarView";
 $("#loadingBar").show();
 timerControl("start");
 $('.wsHideIcons, .googleTask').css("display", "none");
 $("#taskContentDiv").show();
 $('#rowTab').show();
 $("#mytaskDashboard").hide();
 $("#mytaskdatalist").hide();
 $("#googleColabusTask").hide();
 $("#googleTask").hide();
 globVarClickPlace = "";
 flagView = "";
 //$("body").css("overflow", "auto");
 $("body").css("overflow-y", "auto");
 $("body").css("overflow-x", "hidden !important;");
 $('.googleColabusTask').css("display", "none");
 $("#calendarContainer").hide();
 $("#calViewData").show();
	$("#calListViewData").hide();
 var tcd = isNaN($('#tabContentDiv').height());
 if(tcd == NaN){
	tcd = 0;
 }	
 var actConvDivHeight = tcd - $('#rowTab').height();
	 limitAct1 = Math.round(actConvDivHeight / 44) + 15;
	 if(firstClick == 'firstClick'){
	 limitAct1 = Math.round(actConvDivHeight / 44) + 15;
		 index1 = 0;
		 flag1 = 'true';
	 indexFlag1 = 'true';
	 firstClickFlag1 = 'false';
	 $("div#taskContainer").html('');
	  $('select#sortTasks option[value="All"]').prop("selected", "selected");
	 $('select#sortInWs option[value="sort"]').prop("selected", "selected");
	 $("#searchBox").val("");
	sortVal='';
	sortTType='all';
	searchWord='';
	}
 
	let jsonbody = {
		    "user_id":userIdglb,
		    "company_id":companyIdglb,
		    "limit":limitAct1,
		    "index":index1,
		    "sortVal":sortVal,
		    "text":searchWord,
		    "sortTaskType":sortTType
		
	}
	checksession();
	$.ajax({ 
		   url: apiPath+"/"+myk+"/v1/getTaskDetails/historyTask",
		   type:"POST",
		   contentType:"application/json",
		   dataType:'json',
		   data: JSON.stringify(jsonbody),
		   error: function(jqXHR, textStatus, errorThrown) {
				checkError(jqXHR,textStatus,errorThrown);
				$("#loadingBar").hide();
				timerControl("");
				}, 
	   success:function(result){
	   checkSessionTimeOut(result);
	   
	   jsonData = result;
			if(jsonData.length>0){
				result = prepareUI();
			}else{
				flag1 = 'false';
				if($('.docListViewRowCls').length > 0){
					result="";
				}else{
					result = '<div id="innerDiv6" align="center" style="margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>';
				}
				//result='<div id="innerDiv6" align="center" style="color:#ffffff;margin-top:20px;font-size:15px;font-family:icon;">No Task Available</div>';
			}  
			   $("#taskContainer").append(result);
			   $("#taskContainer .docListViewBorderCls").on("mouseover",function(){
				   $(this).find('.hover-comment-icons').show();
			}).on("mouseout", function(){
				  $(this).find('.hover-comment-icons').hide();
			});
			   indexFlag1 = 'true';
			firstClickFlag1 = 'true';
			var topH = $("#tabMainDiv").height();
			$(".taskListing").css("top",topH);
			$("#loadingBar").hide();
			timerControl("");   
	
		}
   });
}