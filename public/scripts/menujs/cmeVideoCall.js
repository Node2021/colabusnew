var localVideo;
var localStream;
var selfStream;
var remoteStream = [];
var remoteVideo;

var videoInterval = "";
var audioInterval = "";
var serverConnection;
//var type;
var timeCount;
var callType = "";
var NameforChatRoom = "";
var vGroupId = "";
var vUserId = "";
var callId = "";
var vConvId;
var shareVideo;
var sharedVideo;
var foraudiocall = 0;
var forvideocall = 0;
var ongoingCall = "off";
var userConnected = [];
var userOnCall = [];
var userSpeed = [];
var userCall;
var callConf = "off";
var recordStatus = "off";
var receiveChannel = null;
var dataConstraint;
var isFirefoxCall = navigator.userAgent.indexOf('Firefox') > -1;
var isChromeCall = navigator.userAgent.indexOf('Chrome') > -1;
var audioContext = [];
var mediaStreamSource = [];
var processor = [];
var userDevice = [];
var muteArray = [];
var createMuteUI = [];
var switchAdminArray = [];
var stazaofMute = [];
var localStorageMute = [];
var adminArray = [];
var whoAdmin = "";
var whiteboardArray = [];
var screenshareArray = [];
var audioBandwidth = 50;
var videoBandwidth = 256;
var tilePreparationArray = [];
var whoEverconnected = [];
var limitCall = 0;
var indexCall = 0;
var queueArray = [];
var tid = "";
var ttype = "";
var cmeexpandcollapsecheck = "";
var cmemuteaudio = "";
var cmecallhold = "";
var globalwb = "";

var peerConnectionConfig = {

	'iceServers': [
		//{'urls': 'stun:stun.l.devchat.colabus.com:3478'},
		//{'urls': 'stun:stun.l.devchat.colabus.com:3479'},
		{ 'urls': 'stun:stun.l.google.com:19302' },
		/*{
		  'urls': 'turn:104.238.119.35:3478?transport=udp',
		  'credential': 'password1',
		  'username': 'username1'
		},
		{
		  'urls': 'turn:104.238.119.35:3478?transport=tcp',
		  'credential': 'password1',
		  'username': 'username1'
		},*/
		{
			'urls': 'turn:numb.viagenie.ca',
			'credential': 'Ajmeerkhan21!',
			'username': 'ajmeerkhan.s@stridus.com'
		}

	]

};
function initCallElements() { //--- this function is called inside cme.js after call related UI is prepared, to get reference of call related elements and store it as gobal.
	localVideo = document.getElementById('localVideo');
	remoteVideo = document.getElementById('remoteVideo');
	shareVideo = document.getElementById('shareVideo');
	sharedVideo = document.getElementById('sharedVideo');
	aRemoteVideo = document.getElementById('aRemoteVideo');
}
var qvgaConstraints = {
	video: { width: { exact: 320 }, height: { exact: 240 } }
};

var vgaConstraints = {
	video: { width: { exact: 640 }, height: { exact: 480 } }
};

var hdConstraints = {
	video: { width: { exact: 1280 }, height: { exact: 720 } }
};

var fullHdConstraints = {
	video: { width: { exact: 1920 }, height: { exact: 1080 } }
};

var video_constraints = {
	audio: true,
	video: { width: { exact: 320 }, height: { exact: 240 } }

};


var audio_constraints = {
	video: false,
	audio: true
};

var screen_constraints_moz = {
	audio: true,
	video: {
		mediaSource: "screen",
	}
};
var callerType = "receiver";

function startavCall(id, isCaller, call_type) {
	if ($('#userId_' + id).hasClass('user_box_status_away') === true) {
		conFunNew11(getValues(companyAlerts,"Alert_AwayCall"),'warning','startCall','endCall', id, isCaller, call_type);
	}
	else{
		startCall(id, isCaller, call_type);
	}
}

function endCall(){
	videoAlertNotifn("Notification sent, currently this user is away.");
}

function startCall(id, isCaller, call_type) {
	whoAdmin = userIdglb;
	$("#chatMoreOptionsDiv").removeClass('d-flex');
	closeContact();
	// closeCallChat();

	//  alert("inside call -->"+cme.length);
	/*if(pageloaded == false){
		videoAlertNotifn("Wait, we are connecting with the video server.");
		return;
	}*/
	if ((userConnected.length > 3) && (jQuery.inArray("ios", userDevice) || jQuery.inArray("android", userDevice))) {
		videoAlertNotifn("One of the users are on the mobile device.");
		//  return;
	}
	callerType = "caller";
	$(".addnewuser").show();
	if (switchAdminArray.indexOf(id.toString()) == -1) {
		switchAdminArray.push(id.toString());
	}

	$(".callSubImage").css('visibility', 'hidden');
	/*if(isChromeVoice){
			  $('#hLightVoiceStartNew').show(); 
			  $('#hLigthTaskVoiceStart').show();
			  try
			  {
				  recognizationToggleStart('','hLightTaskSpeechType','hLightTaskSpeechType');
			  }catch(e){
				  console.log('e1:'+e);
		  }
			  loadVoices(); 
	 }*/

	if (callConf == "on") {
		if (call_type == 'video') {
			callType = "AV";
		} else if (call_type == "screenshare") {
			callType = "AS";
		} else {
			callType = "A";
		}
		var statusBusy = chatUserArray.getUserStatus(id);

		if (statusBusy === false) {
			getClientSpeed(id, callType);
		} else {

			callBusyAccepted(id, "callbusy");
		}


	} else {

		if (ongoingCall == "on") {
			videoAlertNotifn('User busy with another call...');
			return;
		}
		//console.log("start-------------->"+id);

		//type =isCaller; 


		if (call_type == 'video') {
			callType = "AV";
		} else {
			callType = "A";
		}

		pageReady();

		$('.callConnectImage').attr('src', $('div#userId_' + id).find('img').attr('data-src'));
		$('.callConnectImage').attr('title', $('div#userId_' + id).find('.user_box_name').text());
		$('.callerScreenStatus').text("Calling");
		$('.callConnectText').text($('div#userId_' + id).find('.user_box_name').text());
		$('#callerScreen img.callHangUp').attr('onclick', 'cancelAudioVideoCall(' + id + ')');
		if (callType == "A") {
			//remoteVideo.style.backgroundImage = "url('"+$('#callConnectImage').attr('src')+"')";
			$('.callHangUp').attr('src', path + '/images/cme/audioRed.svg');
			localVideo.style.display = "none";
			// $("#host_" + id).css("border", "3px solid #FFCA00");
	}/*else if(callType == "AS" ){
			  	  //remoteVideo.style.backgroundImage = "url('"+$('#callConnectImage').attr('src')+"')";
			  	  $('.callHangUp').attr('src',path+'/images/temp/phonedis.svg');
			  	  shareVideo.style.display = "none";
			  }*/else {
			remoteVideo.style.backgroundImage = "";
			$('.callHangUp').attr('src', path + '/images/cme/audioRed.svg');
			localVideo.style.display = "block";
		}


		var statusBusy = chatUserArray.getUserStatus(id); //-- defined in chat.js
		//console.log("statusBusy:"+statusBusy);
		if (statusBusy === false) {
			insertCallHistory(id, callType);
			//   alert("false");
		} else {
			callBusyAccepted(id, "callbusy");
			// alert("true");
		}
	}
}

function insertCallHistory(id, callType) {
	callId = ""
	// alert("insertcall history");
	var d = new Date();
	NameforChatRoom = "userMeet_" + id + "_" + d.getFullYear() + d.getMonth() + d.getDate() + d.getHours() + d.getMinutes() + d.getSeconds();
	createCallGroup();

	let jsonbody = {
		"user_id": userIdglb,
		"toUser": id,
		"company_id": companyIdglb,
		"callType": callType,
		"NameforChatRoom": NameforChatRoom
	}
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/insertCallHistory",
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			callId = result;
			transDocId = "0";
			callTransFileCreation(callId);
			if ($('#userId_' + id).hasClass('dnd') === true) {
				videoAlertNotifn("User is busy.");
				// var res = updateCallResponse(id, "callnotresponded");
				callBusyAccepted(id, "callbusy");
			}
			else if ($('#userId_' + id).hasClass('online') === true) {
				$('.callerScreenStatus').text("Ringing...");
				if (shareStaus !== "on") {
					videoCallSound("outgoing_call");
				}
				getSenderMediaAccess(id);

			} else {
				videoAlertNotifn("Notification sent, currently this user is unavailable.");
				var res = updateCallResponse(id, "callnotresponded");
				console.log("res--"+res);
				if (res !== ""){
					console.log("res inside--"+res);
					closeVideoCall();
				}
			}

		}
	});
}


var senderBrowserType;
if (isFirefoxCall) {
	senderBrowserType = "mozila";
}
if (isChromeCall) {
	senderBrowserType = "chrome";
}

function getSenderMediaAccess(id) {
	// console.log("getSenderMediaAccess");
	if (navigator.mediaDevices.getUserMedia) {
		if (callType == "AS") {
			/*
			if (isFirefoxCall) {
				navigator.mediaDevices.getUserMedia(screen_constraints_moz).then(getSenderMediaSuccess).catch(handleGetUserMediaError);
				resetRecordStreams();
			} else if (isChromeCall) {
				//sourceId=null;
				//checkExtensionStatus("dpblaocfmngfpgjdfkebibghkmlahpln");//this code for local
				checkExtensionStatus("ppcbnhnfpkbmjacikndoipjpcpcdmmmj");//this code for the other instances
			} else {
				navigator.mediaDevices.getUserMedia(screen_constraints_moz).then(getSenderMediaSuccess).catch(handleGetUserMediaError);
			}
			*/
		} else {
			if (isFirefoxCall) {
				navigator.getUserMedia = navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
				navigator.getUserMedia(callType == "AV" ? video_constraints : audio_constraints, function (data) {
					getSenderMediaSuccess(data, id);
				}, function (error) {
					console.log(error);

				});
			} else if (isChromeCall) {
				sourceId = null;
				navigator.mediaDevices.getUserMedia(callType == "AV" ? video_constraints : audio_constraints).then(function (data) { getSenderMediaSuccess(data, id) }).catch(handleGetUserMediaError);
			}
			//navigator.mediaDevices.getUserMedia(callType =="AV"? video_constraints: audio_constraints).then(getSenderMediaSuccess).catch(handleGetUserMediaError);
		}
	} else {
		videoAlertNotifn("Your browser does not support getUserMedia API.");
	}
}


var muteEnabled = true;
var audioStream;
var videoStream;
var screenStream;
var screenStreamTrack;
var label;
var sender;

function getSenderMediaSuccess(stream, id) {
	// console.log("getSenderMediaSuccess");
	var jid = id + "@" + cmeDomain + ".colabus.com";
	localStream = stream;//---- Contains all three streams(Video,Screen,Audio)


	if (callType == "AS") {
		shareVideo.srcObject = localStream;
	} else {
		// console.log("localStream--" + localStream);
		localVideo.srcObject = localStream;
	}
	//console.log("media success-------------->");
	/* if(shareStaus !=="on"){
		   videoCallSound("outgoing_call");
	 }*/

	sendDndPresence();
	if (tilePreparationArray.indexOf(userIdglb.toString()) == -1) {
		tilePreparationArray.push(userIdglb.toString());
	}
	// console.log("sending participant array-->" + tilePreparationArray);
	// console.log(callConf);
	if (callConf != "on") {
		// console.log("callconfon");
		var jsonText = '{"calltype":"' + callType + '","type":"callrequest","devicetype":"web","NameforChatRoom":"' + NameforChatRoom + '","browsertype":"' + senderBrowserType + '","callid":"' + callId + '","admin":"' + userIdglb + '","participant" :"' + tilePreparationArray + '"}';
		notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
	}







}

var callflag = false;
function answerCall() {
	callflag = true;

	var Id = $("#incoming").attr('callerid');
	if (receiverDevice !== "web") {
		$("#astreamImgr").hide();
		if (callConf == "on") {
			//crossPlatformResponse();
			//return; 
		}

	}
	receiveCallGroup();

	stopCalltimer();

	getConvId(Id);
	insertReciverCallHistory(callId);
	//console.log("answer-------------->"+callerId);
	openGroupVideoChatBox(Id);
	// $('#updatecallmsg').html('');
	$('#sound').html("");
	$("#chatCalHead").hide(900);
	$("#chatHeader").show(900);
	$('#incoming').hide();
	$('.callSubImage').css('visibility', 'hidden');
	pageReady();
	$("#host_" + Id).css("border", "3px solid #FFCA00");
	var uName = $('div#userId_' + Id).find('.user_box_name').text();
	if($('.callConnectImage').attr('title') == uName){
		$('.callConnectImage').css("border", "4px solid #FFCA00");
	}
	else{
		$('.callConnectImage').css("border", "none");
	}

	if (tilePreparationArray.length > 1) {
		var imgCnt = 0;
		for (i = tilePreparationArray.length - 1; i >= 0; i--) {
			imgCnt++;
			var tileId = tilePreparationArray[i];
			var userName = $('div#userId_' + tileId).find('.user_box_name').text();
			var imgType = $('#userId_' + tileId).attr('imgtype');
			var imgUrl = lighttpdpath + "/userimages/" + tileId + "." + imgType;
			$("#callImageconnect_" + imgCnt).attr("title", userName).attr("src", imgUrl).css('visibility', 'visible');

			if (imgCnt == 2) {
				break;
			}
		}
	}
	clearCallFlash();
	if (callType == "A") {
		//remoteVideo.style.backgroundImage = "url('"+$('#callConnectImage').attr('src')+"')";
		localVideo.style.display = "none";
	} else if (callType == "AS") {
		sharedVideo.style.backgroundImage = "";
		shareVideo.style.display = "none";
	} else {
		remoteVideo.style.backgroundImage = "";
		localVideo.style.display = "block";

	}

	getReciverMediaAccess(Id);


}



function getReciverMediaAccess(id) {
	if (navigator.mediaDevices.getUserMedia) {
		if (callType == "AS") {
			navigator.mediaDevices.getUserMedia(video_constraints).then(getRecieverMediaSuccess).catch(handleGetUserMediaError);
		} else {
			if (isFirefoxCall) {
				navigator.getUserMedia(callType == "AV" ? video_constraints : audio_constraints, function (stream) {

					getRecieverMediaSuccess(stream, id);


				}, function (error) {
					console.log(error);

				});
			} else if (isChromeCall) {
				sourceId = null;
				navigator.mediaDevices.getUserMedia(callType == "AV" ? video_constraints : audio_constraints).then(function (stream) { getRecieverMediaSuccess(stream, id) }).catch(handleGetUserMediaError);
			}


			//navigator.mediaDevices.getUserMedia(callType =="AV"? video_constraints: audio_constraints).then(getRecieverMediaSuccess).catch(handleGetUserMediaError);
		}
	} else {
		videoAlertNotifn('Your browser does not support getUserMedia API.');
	}
}

function getRecieverMediaSuccess(stream, id) {
	//console.log("media success-------------->");
	localStream = stream;


	if (callType == "AS") {
		shareVideo.srcObject = stream;
	} else {
		localVideo.srcObject = stream;
	}


	if (shareStaus != "on") {
		if (tilePreparationArray.length < 2) {
			var jsonText = '{"calltype":"' + callType + '","type":"callaccepted","devicetype":"web","browsertype":"' + senderBrowserType + '","callid":"' + callId + '"}';
			notify = $msg({ to: id + '@' + cmeDomain + '.colabus.com', "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
			serverConnection.send(notify);
		} else {
			var jsonText = '{"calltype":"' + callType + '","type":"callacceptedingroup","devicetype":"web","browsertype":"' + senderBrowserType + '","callid":"' + callId + '"}';
			notify = $msg({ to: id + '@' + cmeDomain + '.colabus.com', "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
			serverConnection.send(notify);
			for (i = 0; i < tilePreparationArray.length; i++) {
				makeOffer(tilePreparationArray[i]);
			}
		}
	}
	if (tilePreparationArray.length > 1) {
		var jsonText = '{"calltype":"' + callType + '","type":"connecting","callid":"' + callId + '" ,"newuser":"' + userIdglb + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
	}


	if (browserCrossStatus == "on") {
		if (callType == "A") {
			$('#ascreenshareImg').removeAttr('onclick').css({ 'cursor': 'default', 'opacity': '0.5' });
		} else if (callType == "AV") {
			$('#vscreenshareImg').removeAttr('onclick').css({ 'cursor': 'default', 'opacity': '0.5' });
		}
	}
	if (tilePreparationArray.indexOf(userIdglb.toString()) == -1) {
		tilePreparationArray.push(userIdglb.toString());
	}

}

function handleGetUserMediaError(e) {
	// console.log("hendleerrormedia");
	switch (e.name) {
		case "NotFoundError":
			videoAlertNotifn("Unable to open your call because no camera and/or microphone were found.");
			break;
		case "SecurityError":
		case "PermissionDeniedError":
			// Do nothing; this is the same as the user canceling the call.
			break;
		default:
			videoAlertNotifn("Error opening your camera and/or microphone: " + e.message);
			break;
	}

	closeVideoCall();
}



function initDurationTimer() {
	var connection1 = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
	if (callType == "A") {
		$("#ownspeedaudio").text(connection1.downlink + " Mbps");
		$("#ownspeedaudio").html(connection1.downlink + " Mbps");


	}
	else {
		$("#ownspeedvideo").text(connection1.downlink + " Mbps");
		$("#ownspeedvideo").html(connection1.downlink + " Mbps");
	}

	$('.avTimer').timer({
		format: '%M:%S',
		duration: '59s',
		callback: function () {
			var total = $('#vTimer').data('seconds');
			//console.log("tot sec:"+total);
			var connection1 = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
			if (callType == "A") {
				$("#ownspeedaudio").text(connection1.downlink + " Mbps");
				$("#ownspeedaudio").html(connection1.downlink + " Mbps");
			}
			else {

				$("#ownspeedvideo").text(connection1.downlink + " Mbps");
				$("#ownspeedvideo").html(connection1.downlink + " Mbps");
			}
			if (total === 3599) { //change to hour format (3600 - 1 to prevent display bug)
				this.format = '%H:%M:%S';
			}

		},
		repeat: true
	});

}
function stopDurationTimer() {
	$(".avTimer").timer('remove');
	$(".avTimer").html("");
}

function pageReady() {
	$('#cmecallcontact').show();
	$('.cmeMainScreenDivs').hide();
	// setCmeSwitchModeDiv();
	$('#chatContent').hide();
	$('#videoContent').show();
	//$('#video-content').show();
	$('#videoContent #callerScreen').show();
	$("#videoFor, #audioFor, #screenFor").hide();
	// $('#local-video').show();
	$('#avStatusDiv span').text("");
	$('img.videoOptionIcons').each(function () {
		$(this).attr('src', path + '/images/cme/' + $(this).attr('defsrc')).css({ 'cursor': 'pointer', 'opacity': '1' });
	});
	$("#previewDiv").hide();
	$('#recordStatusDiv').hide();
	$('#vrecordImg,#arecordImg').attr('onclick', 'recordVideo()');
	$("#highlightFor").find("#hListDiv").html("");
	wbFlag = true; //--->used in whiteboard code
	$("#wbImgList").html("");

	if ($("#cmecontent").is(':hidden')) {
		$("#cmecontent").removeClass("d-none");
		//setCmeSwitchModeDiv();
	}

	if($('#cmecontent').hasClass('cmecontainer-mob')){
		$('.expandcall').hide();
		$('.compactcall').show();
	}
	else{
		$('.expandcall').show();
		$('.compactcall').hide();
	}
	// if($('#cmecontent').hasClass('cmecontainer-mob')){
	// 	cmeExpandCollapse($('#cmeSwitchModeDiv').find('img.cmeFullCompact'));
	// 	// cmeexpandcollapsecheck='1';
	// }
	cmetranscriptVideo();
	cmehighlightVideo();
	// $('.callHangUp').attr('src', path + '/images/cme/phonedis.svg');
	// if($('#cmetscontainer').is(":hidden")){
	// 	$('#cmetscontainer').show();
	// 	// $("#transparentDiv").hide();
	// }
}

var busyId = "";
var busyCallId = "";
var receiverImg = "";
var receiverDevice = "";
var userData = [];
function gotMessageFromServer(id, message) {
	var signal = JSON.parse($(message).children('json').text());
	// console.log("signal-->:"+signal.type);
	//  console.log("Array-->"+signal.muteArray);
	var muteF = signal.muteArray;
	if (typeof muteF != "undefined" && muteF != "" && muteF != null && muteF != "null") {
		//console.log(muteF.length);
		createMuteUI = muteF.split(",");
		//  console.log("createMuteUI-->"+createMuteUI.length);

	}

	/*var switchadmin=signal.switchAdminArray;
	console.log("----->+ "+switchadmin);
	if(typeof switchadmin!="undefined" && switchadmin!="" && switchadmin!=null && switchadmin!="null"){
		
		   adminArray = switchadmin.split(",");
		   console.log("adminArray-->"+adminArray);
		   if(adminArray[0]==userIdglb){
			  switchAdminArray=adminArray;
			  user=="owner";
			  $(".addnewuser").show();
			  $(".userdisconnect").show();
			 user
		}
		
	 }
	*/
	// console.log(signal.muteArray.length);
	// console.log(signal);

	//transferid
	if (typeof signal.transferid != "undefined" && signal.transferid != "" && signal.transferid != null && signal.transferid != "null") {

		var isAdmin = signal.transferid;
		if (isAdmin == userIdglb) {
			chatAlertFun('You are now  host of the call.', 'warning');
			user = "owner";
			$(".addnewuser").show();
			$(".userdisconnect").show();
			whoAdmin = isAdmin;
			$("div[id^=hosta_]").css("display", "none");
			$("#hosta_" + whoAdmin).css("display", "block");
			$("div[id^=host_]").css("border", "none");
			$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");
			$("div[id^=hostp_]").css("display", "none");
			$("#hostp_" + whoAdmin).css("display", "block");
		}
		else {
			whoAdmin = isAdmin;
			$("div[id^=host_]").css("border", "none");
			$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");
			$("div[id^=hostp_]").css("display", "none");
			$("#hostp_" + whoAdmin).css("display", "block");
			$("div[id^=hostp_]").css("display", "none");
			$("#hostp_" + whoAdmin).css("display", "block");
		}


	}
	//console.log("who admin:--> "+signal.admin)
	if (typeof signal.admin != "undefined" && signal.admin != "" && signal.admin != null && signal.admin != "null") {
		whoAdmin = signal.admin;
		//console.log("who admin inside:--> "+signal.admin)
	}


	if (signal.calltype == "AV") {
		if (userConnected.length < 2) {
			//$("#local-video").css("display","block");
			document.getElementById('localVideo').srcObject = localStream;
			//	console.log(id +"===="+document.getElementById('remoteVideo'));

			document.getElementById('remoteVideo').srcObject = remoteStream[userConnected[0]];
		}

	}
	if (signal.type == "hostswitch") {
		//alert("enter in second");
		var hId = signal.hostid;
		var uid = signal.uid;
		$("#host_" + uid).css("border", "none");
		var uName = $('div#userId_' + uid).find('.user_box_name').text();
		
		//alert(hId);
		if (hId == userIdglb) {
			chatAlertFun('You are now  host of the call.', 'warning');
			user = "owner";
			$(".addnewuser").show();
			$(".userdisconnect").show();
			var hName = userFullname;
	
			// if($('.callConnectImage').attr('title') == uName){
			// 	$('.callConnectImage').css("border", "none");
			// }
			if($('.callConnectImage').attr('title') == hName){
				$('.callConnectImage').css("border", "4px solid #FFCA00");
			}
			else{
				$('.callConnectImage').css("border", "none");
			}
			whoAdmin = hId;
			$("div[id^=hosta_]").css("display", "none");
			$("#hosta_" + hId).css("display", "block");
			$("div[id^=host_]").css("border", "none");
			$("#host_" + hId).css("border", "3px solid #FFCA00");
			$("div[id^=hostp_]").css("display", "none");
			$("#hostp_" + hId).css("display", "block");
			$("div[id^=hostu_]").css("border", "none");
			$("#hostu_" + hId).css("border", "3px solid #FFCA00");
			$("div[id^=hostb_]").css("display", "none");
			$("#hostb_" + hId).css("display", "block");
		}
		else {
			var hName = $('div#userId_' + hId).find('.user_box_name').text();
	
			// if($('.callConnectImage').attr('title') == uName){
			// 	$('.callConnectImage').css("border", "none");
			// }
			if($('.callConnectImage').attr('title') == hName){
				$('.callConnectImage').css("border", "4px solid #FFCA00");
			}
			else{
				$('.callConnectImage').css("border", "none");
			}
			whoAdmin = hId;
			$("div[id^=hosta_]").css("display", "none");
			$("#hosta_" + hId).css("display", "block");
			$("div[id^=host_]").css("border", "none");
			$("#host_" + hId).css("border", "3px solid #FFCA00");
			$("div[id^=hostp_]").css("display", "none");
			$("#hostp_" + hId).css("display", "block");
			$("div[id^=hostu_]").css("border", "none");
			$("#hostu_" + hId).css("border", "3px solid #FFCA00");
			$("div[id^=hostb_]").css("display", "none");
			$("#hostb_" + hId).css("display", "block");
		}
		
	}

	if (signal.type == 'callrequest') {
		//answerCall(id,false);
		// console.log("signal call id:" + signal.callid);

		if (typeof (peerConnections[id]) != "undefined" && peerConnections[id] != null && peerConnections[id] != "") {
			if (callId != signal.callid) {
				busyId = id + "@" + cmeDomain + ".colabus.com";
				busyCallId = signal.callid;
				busyVideoCall();
			} else {
				// console.log("request with same call id--->");
			}
		} else {

			sendDndPresence();
			if ($('#chatAlertDiv').is(":visible")) { //---this is to hide alert msg when there is a incoming call
				chatAlertClose();
				clearTimeout(vAlertRef);
			}
			checkChatWindowSizeForCall();
			videoCallSound("cv_call");
			callId = signal.callid;
			NameforChatRoom = signal.NameforChatRoom;
			// console.log("---else---->");
			callType = signal.calltype;
			var jid = id + "@" + cmeDomain + ".colabus.com";

			var full_jid = $(message).attr('from');
			var jidImage = Strophe.getBareJidFromJid(full_jid);
			var jid_id = Gab.jid_to_id(jidImage);
			var imgType = $('#userId_' + id).attr('imgtype');
			var imgUrl = lighttpdpath + "/userimages/" + id + "." + imgType;
			// console.log("imgUrl--" + imgUrl);
			receiverImg = imgUrl;
			receiverDevice = signal.devicetype;
			setCallFlash();
			//closeHightlightPopUp();
			var badge_url = "";
			//ChangeImagecall(imgUrl,$('div#'+id+' div.user_box_name').text(),badge_url);
			//alert(signal.participant)
			if (typeof signal.participant != "undefined" && signal.participant != "" && signal.participant != null && signal.participant != "null") {
				tilePreparationArray = signal.participant.split(",");

			}
			//console.log("reciving participant array-->"+tilePreparationArray);	
			if (callType == "A") {
				$('img.answerCallImg').attr('src', path + '/images/cme/audioGreen.svg');
				$('img.declineCallImg').attr('src', path + '/images/cme/audioRed.svg');
				badge_url = 'images/cme/audioGreen.svg';
			} else if (callType == "AS") {
				$('#screenUserImg').attr('src', imgUrl);
			} else {
				$('img.answerCallImg').attr('src', path + '/images/cme/camGreen.svg');
				$('img.declineCallImg').attr('src', path + '/images/cme/audioRed.svg');
				badge_url = 'images/cme/camGreen.svg';
			}

			// ChangeImagecall(imgUrl,$('div#userId_'+id+'').find('.user_box_name').text(),badge_url);
			$('#cmecallcontact').show();
			$('.cmeMainScreenDivs').hide();
			//setCmeSwitchModeDiv();
			
			$("#callImage").attr('title', $('div#userId_' + id).find('.user_box_name').text());
			$("#callImage").attr("src", imgUrl);
			$('#callConnect').text($('div#userId_' + id).find('.user_box_name').text());
			$('.callConnectImage').attr('title', $('div#userId_' + id).find('.user_box_name').text());
			$('.callConnectImage').attr('src', imgUrl);
			$('.callConnectText').text($('div#userId_' + id).find('.user_box_name').text());
			$('.callerScreenStatus').text("Connecting...");
			$('.callSubImages').css('visibility', 'hidden');
			$('.callSubImage').css('visibility', 'hidden');
			var uName = $('div#userId_' + id).find('.user_box_name').text();
			if($('.callConnectImage').attr('title') == uName){
				$('.callConnectImage').css("border", "4px solid #FFCA00");
			}
			else{
				$('.callConnectImage').css("border", "none");
			}

			$(".incomingCall").show();
			$("#videoContent").hide();
			if ($("#cmecontent").is(':hidden')) {
				$("#cmecontent").removeClass("d-none");
				//setCmeSwitchModeDiv();
			}

			$("#incoming").attr('callerid', id);
			timeCount = setTimeout(function () { callTimer(); }, 30000);
			//browserType(signal.browsertype);
			//userData.push(parseInt(id));
			//createRecieverPeerConnection(id);
		}
	} else if (signal.type == 'callconfrequest' || signal.type == 'callconfrequestEx') {
		//answerCall(id,false);
		//console.log("signal call id:"+signal.callid);
		callConf = "on";
		if (typeof (peerConnections[id]) != "undefined" && peerConnections[id] != null && peerConnections[id] != "") {
			if (callId != signal.callid) {
				busyId = id + "@" + cmeDomain + ".colabus.com";
				busyCallId = signal.callid;
				busyVideoCall();
			} else {
				//console.log("request with same call id--->");	
			}

		} else {
			$("#incoming").attr('callerid', id);
			sendDndPresence();
			if ($('#chatAlertDiv').is(":visible")) { //---this is to hide alert msg when there is a incoming call
				chatAlertClose();
				clearTimeout(vAlertRef);
			}
			checkChatWindowSizeForCall();
			callId = signal.callid;
			//console.log("---else---->");
			callType = signal.calltype;
			NameforChatRoom = signal.NameforChatRoom;
			var jid = id + "@" + cmeDomain + ".colabus.com";
			videoCallSound("cv_call");
			if (typeof signal.participant != "undefined" && signal.participant != "" && signal.participant != null && signal.participant != "null") {
				tilePreparationArray = signal.participant.split(",");

			}
			if (typeof signal.screenshare != "undefined" && signal.screenshare != "" && signal.screenshare != null && signal.screenshare != "null") {
				screenshareArray = signal.screenshare.split(",");

			}
			console.log("3rd participant array-->"+tilePreparationArray);	
			if (signal.type == 'callconfrequest') {
				console.log("callconfrequest");
				var full_jid = $(message).attr('from');
				var jidImage = Strophe.getBareJidFromJid(full_jid);
				var jid_id = Gab.jid_to_id(jidImage);
				var imgType = $('#' + jid_id).attr('imgType');
				var imgUrl = lighttpdpath + "/userimages/" + id + "." + imgType;
				receiverImg = imgUrl;
				if (callType == "A") {
					$('img.answerCallImg').attr('src', path + '/images/cme/audioGreen.svg');
					$('img.declineCallImg').attr('src', path + '/images/cme/audioRed.svg');
				} else if (callType == "AS") {
					$('#screenUserImg').attr('src', imgUrl);
				} else {
					$('img.answerCallImg').attr('src', path + '/images/cme/camGreen.svg');
					$('img.declineCallImg').attr('src', path + '/images/cme/audioRed.svg');
				}
				$("#callImage").attr('title', $('div#userId_' + id).find('.user_box_name').text());
				$("#callImage").attr("src", imgUrl);
				$('#callConnect').text($('div#userId_' + id).find('.user_box_name').text());
				$('.callConnectImage').attr('title', $('div#userId_' + id).find('.user_box_name').text());
				$('.callConnectImage').attr('src', imgUrl);
				$('.callConnectText').text($('div#userId_' + id).find('.user_box_name').text());
				$('.callerScreenStatus').text("Connecting...");
				$('.callSubImages').css('visibility', 'hidden');
				var uName = $('div#userId_' + id).find('.user_box_name').text();
				if($('.callConnectImage').attr('title') == uName){
					$('.callConnectImage').css("border", "4px solid #FFCA00");
				}
				else{
					$('.callConnectImage').css("border", "none");
				}

				var imgCnt = 0;
				for (i = tilePreparationArray.length - 1; i >= 0; i--) {
					imgCnt++;
					var tileId = tilePreparationArray[i];
					var userName = $('div#userId_' + tileId).find('.user_box_name').text();
					imgType = $('#userId_' + tileId).attr('imgtype');
					var imgUrl = lighttpdpath + "/userimages/" + tileId + "." + imgType;
					$("#callImage_" + imgCnt).attr("title", userName).attr("src", imgUrl).css('visibility', 'visible');
					if (imgCnt == 2) {
						break;
					}
				}
				$("#cmecallcontact").show();
				$('.cmeMainScreenDivs').hide();
				$(".incomingCall").show();
				if ($("#cmecontent").is(':hidden')) {
					$("#cmecontent").removeClass("d-none");
					//setCmeSwitchModeDiv();
				}
				//setCmeSwitchModeDiv();

				timeCount = setTimeout(callTimer, 30000);
				console.log("timecount");
			} else {
				var full_jid = $(message).attr('from');
				var jidImage = Strophe.getBareJidFromJid(full_jid);
				var jid_id = Gab.jid_to_id(jidImage);
				var imgType = $('#' + id).attr('imgtype');
				// var imgType = $('#'+jid_id).attr('imgType');
				var imgUrl = lighttpdpath + "/userimages/" + id + "." + imgType;
				receiverImg = imgUrl;
				$('.callSubImage').css('visibility', 'hidden');
				$("#callImage").attr('title', $('div#userId_' + id).find('.user_box_name').text());
				$("#callImage").attr("src", imgUrl);
				$('#callConnect').text($('div#userId_' + id).find('.user_box_name').text());
				$('.callConnectImage').attr('title', $('div#userId_' + id).find('.user_box_name').text());
				$('.callConnectImage').attr('src', imgUrl);
				$('.callConnectText').text($('div#userId_' + id).find('.user_box_name').text());
				$('.callerScreenStatus').text("Connecting...");
				$(".incomingCall").show();
				if ($("#cmecontent").is(':hidden')) {
					$("#cmecontent").removeClass("d-none");
					//setCmeSwitchModeDiv();
				}
				$("#loadingBar").hide();
				timerControl("");
				var imgCnt = 0;
				// alert("tilePreparationArray-->"+tilePreparationArray +"imgcnt--->"+imgCnt);

				answerCall();
			}

			receiverDevice = signal.devicetype;
			setCallFlash();
			closeHightlightPopUp();

			//browserType(signal.browsertype);
			//console.log("userData--")
			userData = [];
			if (signal.userOnCallInfo.indexOf(",") > -1) {
				var userArray = signal.userOnCallInfo.split(',');
				for (var i = 0; i < userArray.length; i++) {
					userData.push(userArray[i]);
				}
			} else {
				userData.push(signal.userOnCallInfo);
			}


		}
	} else if (signal.type == "callrestrictformobile") {

		videoAlertNotifn("The user is on mobile");

	} else if (signal.type == 'getspeed') {

		sendClientSpeed(id, signal.calltype);

	} else if (signal.type == 'sendspeed') {
		var receiverSpeed = signal.callspeed;
		var reqSpeed = "";
		if (callType == "A") {
			reqSpeed = (userConnected.length + 1) * 0.05;
		}
		else {
			reqSpeed = (userConnected.length + 1) * 0.2;
		}

		//console.log("receiverSpeed---"+receiverSpeed+"---reqSpeed--"+reqSpeed);
		if (receiverSpeed == "undefined" || receiverSpeed > reqSpeed) {
			videoAlertNotifn($('div#userId_' + id + '').find('.user_box_name').text() + " is experiencing Insufficient Internet Connection.");

		}
		if (userConnected.length > 3 && signal.devicetype != "web") {
			videoAlertNotifn(" The user is on the mobile device.");
			// return;
		}
		createAnotherCall(id, callType);
		/*if(receiverSpeed == "undefined" || receiverSpeed >  reqSpeed ){
			createAnotherCall(id,callType); 
		}else{
			//videoAlertNotifn("You may experience poor connection or intermittent connectivity issues.");
			videoAlertNotifn($('div#'+id+' div.user_box_name').text()+" is experiencing Insufficient Internet Connection. You cannot connect to this User");
		}*/

	} else if (signal.type == 'speedFromConnectedUser') {
		var receiverSpeed = signal.callspeed;
		checkSpeedForAddUser(id, receiverSpeed);

	} else if (signal.type == 'callaccepted' || signal.type == ' callacceptedingroup') {

		$('#callerScreen img.callHangUp').attr('onclick', 'hangup();');
		// $('.callHangUp').attr('src', path + '/images/cme/phonedis.svg');

		callflag = true;

		$('#sound').html("");
		if (callConf == "off") {
			openGroupVideoChatBox(id);
			// $('#updatecallmsg').html('');
		}
		getConvId(id);
		receiverDevice = signal.devicetype;
		if (receiverDevice !== "web") {
			$("#astreamImgr").hide();
		}
		updateCallResponse(id, signal.type);
		//browserType(signal.browsertype);
		//console.log("inside callaccepted");
		if (user == "owner") {
			userDevice[id] = signal.devicetype;

		}
		if (signal.type != ' callacceptedingroup') {
			makeOffer(id);
		}

		sendingDocIdTranscript();
	} else if (callflag == true) {

		if (signal.type == 'offer') {
			//console.log("from---"+id+"==offer -- callconf:"+callConf + "---"+signal.sdp);
			//typeof(peerConnections[id])!="undefined" && peerConnections[id]!=null && peerConnections[id]!=""	
			var pc = getPeerConnection(id);

			try {


				// Only create answers in response to offers
				if (isFirefoxCall) {

					if (signal.sdp != null) {
						pc.setRemoteDescription(new RTCSessionDescription(signal.sdp)).then(function () {
							pc.createAnswer().then(function (sdp) {


								pc.setLocalDescription(sdp);
								sdp.sdp = setMediaBitrates(sdp.sdp);
								//  console.log("inside answer set local description");
								///  console.log(sdp.sdp);
								var body = JSON.stringify({ "calltype": callType, "type": "answer", "sdp": sdp, "callid": callId });
								var message = $msg({ to: id + '@' + cmeDomain + '.colabus.com', "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(body);
								Gab.connection.send(message);
							}).catch(errorHandler);
						})
					}

				} else {
					pc.setRemoteDescription(new RTCSessionDescription(signal.sdp), function () {
						//console.log('Setting remote description by offer');
						pc.createAnswer().then(function (sdp) {
							pc.setLocalDescription(sdp);
							sdp.sdp = setMediaBitrates(sdp.sdp);
							//console.log("inside answer set local description");
							///console.log(sdp.sdp);
							var body = JSON.stringify({ "calltype": callType, "type": "answer", "sdp": sdp, "callid": callId });
							var message = $msg({ to: id + '@' + cmeDomain + '.colabus.com', "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(body);
							Gab.connection.send(message);
						}).catch(errorHandler)
					});
				}


			} catch (e) {
				console.log('e1:' + e);
			}

		} else if (signal.type == 'answer') {
			try {

				var pc = getPeerConnection(id);
				try {
					if (isFirefoxCall) {

						pc.setRemoteDescription(new RTCSessionDescription(signal.sdp));

					} else {
						pc.setRemoteDescription(new RTCSessionDescription(signal.sdp), function () {
							console.log('Setting remote description by answer');
						}, function (e) {
							console.error(e);
						});
					}

				} catch (eMsg) {
					console.log('eMsg:' + eMsg);
				}

			} catch (e) {
				console.log('e1:' + e);
			}

		} else if (signal.type == 'candidate') {
			var pc = getPeerConnection(id);
			if (signal.ice) {
				// console.log('signal---'+JSON.stringify(signal));
				//console.log('Adding ice candidates---'+JSON.stringify(signal.ice));
				pc.addIceCandidate(new RTCIceCandidate(signal.ice));
			}
		} else if (signal.type == 'userdisconnect') {
			//console.log("*** call not responded notification from other peer");
			hangupFromRemote();
			//var idx=muteArray.indexOf(userIdglb);
			muteArray = [];
			createMuteUI = [];
			stazaofMute = [];
			screenshareArray = [];
			foraudiocall = 0;
			forvideocall = 0
			whiteboardArray = [];

			$(".arr").attr('src', path + '/images/temp/slideboxright.png');
			// $(".arrow").css("left","-37px");
			$(".arr").css("width", "41px");

		} else if (signal.type == 'callrequestcancelled') {
			//-- this executes when caller initiated the call but decided to disconnect before the opposite user answer
			stopCalltimer();
			sendAvailablePresence();
			//console.log("*** Disconnect hang up notification from dialer.");
			if (userConnected.length < 1) {
				var res = updateCallResponse(userIdglb, signal.type);
				alert("res-" + res);
				$("#cmecallcontact").hide();
				$('.cmeMainScreenDivs').show();
				//setCmeSwitchModeDiv();
				console.log("res--"+res);
				if (res !== ""){
					console.log("res inside--"+res);
					closeVideoCall();
				}
			} else {
				$("#notification").hide();
			}
		} else if (signal.type == 'calldeclined') {
			//console.log("*** Declined hang up notification from other peer");
			callDeclinedAccepted(id);
		}else if (signal.type == 'callnotresponded') {
			// console.log("*** call not responded notification from other peer");
			// $("#notification").hide();
			callNotRespondedAccepted();
		}
	} else if (signal.type == 'callrequestcancelled') {
		//-- this executes when caller initiated the call but decided to disconnect before the opposite user answer
		stopCalltimer();
		sendAvailablePresence();
		//console.log("*** Disconnect hang up notification from dialer.");
		$("#notification").hide();
		var res = updateCallResponse(userIdglb, signal.type);
		console.log("***res--"+res);
		// alert("res--"+res);
		$("#cmecallcontact").hide();
		$('.cmeMainScreenDivs').show();
		// if(cmeexpandcollapsecheck == '1'){
		// 	cmeExpandCollapse($('#cmeSwitchModeDiv').find('img.cmeFullCompact'));
		// 	//setCmeSwitchModeDiv();
		// }
		// else{
		// 	//setCmeSwitchModeDiv();
		// }
		if(res !=""){
			closeVideoCall();
		}
	} else if (signal.type == 'calldeclined') {
		//console.log("*** Declined hang up notification from other peer");
		callDeclinedAccepted(id);
	} else if (signal.type == 'callbusy') {
		//console.log("*** Busy hang up notification from other peer");
		callBusyAccepted(id, signal.type);
	} else if (signal.type == 'callnotresponded') {
		// console.log("*** call not responded notification from other peer");
		callNotRespondedAccepted();
	} else if (signal.type == 'callcrossdevice') {
		//console.log("*** call not responded notification from other peer");
		callCrossDeviceAccepted(id);
	} else if (signal.type == 'offer') {	 //----- this is to just ignore call request for multiple chat window in case in any one window call is accepted
		stopCalltimer();
		$('#sound').html("");
		$(".incomingCall").hide();
		$('#cmecallcontact').hide();
		$('.cmeMainScreenDivs').show();
		//setCmeSwitchModeDiv();
		sendAvailablePresence();
	} else if (signal.type == 'calldeclinemultiuser') {
		if ($("#incoming").is(':visible')) {
			//console.log("inside videoo");
			stopCalltimer();
			closeVideoCall();
		}

	}
	else if (signal.type == "stanzaofData") {

		//console.log(callsignal.stazaofMute);
		/*var a=callsignal.stazaofMute;
		stazaofMute=a.split(",");
		$("div[id^=raudiopar_]").css("display","none");
		$("div[id^=raudiotiles_]").css("display","none");
		for(i=0;i<stanzaofMute.length;i++){
			$("#raudiopar_"+stanzaofMute[i]).show();
			$("#raudiotiles_"+stanzaofMute[i]).show();
		}*/
		var a = callsignal.stazaofMute;
		$("div[id^=raudiopar_]").css("display", "none");
		$("div[id^=raudiotiles_]").css("display", "none");
		if (typeof a != "undefined" && a != "" && a != null && a != "null") {
			stazaofMute = a.split(",");
			//console.log(stazaofMute+"---"+stazaofMute.length);
			for (i = 0; i < stazaofMute.length; i++) {
				$("#raudiopar_" + stazaofMute[i]).show();
				$("#raudiotiles_" + stazaofMute[i]).show();
			}

		}
		var wb = callsignal.whiteboard;

		$("div[id^=whiteboard_]").css("display", "none");

		if (typeof wb != "undefined" && wb != "" && wb != null && wb != "null") {
			whiteboardArray = wb.split(",");
			$("#whiteboard_" + whiteboardArray[0]).show();
		}


		var ss = callsignal.screen;

		$("div[id^=sharescreen_]").css("display", "none");

		if (typeof ss != "undefined" && ss != "" && ss != null && ss != "null") {
			screenshareArray = ss.split(",");
			$("div[id^=sharescreen_]").css("display", "none");
			$("div[id^=sharescreenu_]").css("display", "none");
			$("#sharescreen_" + screenshareArray[0]).show();
			/*var Uid=screenshareArray[0];
			   if(callType=="A"){
				   $('#RemoteDiv_'+Uid).attr('screenshare','Y');
				   //if($('#aRemoteVideo').attr('audioId') != id){
					   $("#aRemoteImage_"+Uid).hide();
					   $("#remoteAudio_"+Uid).show();
				   //}
					   if($('#aRemoteVideo').attr('audioId') != Uid){
						   $("#aRemoteImage").show();
						   $("#aRemoteVideo").hide();
					   }else{
						   $("#aRemoteImage").hide();
						   $("#aRemoteVideo").show();
					   }
				} 
				switchVideo(Uid);*/
		}




		//console.log(stazaofMute+"---"+stazaofMute.length+"@@@@");
	}
	if (signal.type == "addExternalUser") {
		//alert(signal.callid+"---"+callId)

		if (user == "owner") {
			if (signal.callid == callId) {
				var name = $('div#userId_' + signal.idrequest).find('.user_box_name').text();


				if (queueArray.length > 0) {
					if (queueArray.indexOf(id.toString()) == -1) {
						queueArray.push(signal.idrequest.toString());
						var count = queueArray.length - 1;
						/*   Ui += "<div id='connuser' class='' style='height:60px;padding-top: 7px;padding-bottom: 7px;width: 100%; background-color: rgba(113, 118, 126, 0.6);' > "
								  +"<div class='col-xs-2' style='padding-top:0.5%'> <img  onerror='userImageOnErrorReplace(this);' style='height: 34px; min-height: 45px; width:45px;min-width: 45px;border-radius:39px'/></div>"
								  +"<div class='user_box_name col-xs-4' id='statuschange' style='font-family: OpenSansRegular;font-weight: bold;color:#fff; font-size: 14px; width: 50%; padding-top: 7px;padding-left: 20px;' value=''>"+count+" peoples are in th queue.</div>"
									+"</div>";
							  $("#notification").html('').append(Ui);
							  $("#notification").show(500);*/
					}
				} else {
					queueArray.push(signal.idrequest.toString());
					callConfirm(("Would you like to add " + name + " in the call ?"), "delete", "AdduserAccepted", "declinereq", "none", signal.idrequest, signal.idrequest);

				}

			} else {
				var hostid = signal.idrequest;
				var jid = hostid + "@" + cmeDomain + ".colabus.com";
				var jsonText = '{"calltype":"' + callType + '","type":"callinactive","callid":"' + signal.callid + '","hostidrequest":"' + userIdglb + '"}';
				notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
				serverConnection.send(notify);
			}
		}
		else {
			if (signal.callid == callId) {
				var hostid = signal.idrequest;
				var jid = hostid + "@" + cmeDomain + ".colabus.com";
				var jsonText = '{"calltype":"' + callType + '","type":"hostchanged","callid":"' + callId + '","hostidrequest":"' + userIdglb + '"}';
				notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
				serverConnection.send(notify);
			}
			else {
				var hostid = signal.idrequest;
				var jid = hostid + "@" + cmeDomain + ".colabus.com";
				var jsonText = '{"calltype":"' + callType + '","type":"callinactive","callid":"' + signal.callid + '","hostidrequest":"' + userIdglb + '"}';
				notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
				serverConnection.send(notify);
			}
		}


	}
	if (signal.type == "deqExternalUser") {
		$('#videoContent #callerScreen').hide();
		$("#cmecallcontact").hide();
		$("#changescreen").show();
		$("#chatContent").show();
		$("#loadingBar").hide();
		timerControl("");
		var name = $('div#userId_' + signal.hostidrequest).find('.user_box_name').text();
		videoAlertNotifn("your call has been declined.");

	}
	if (signal.type == "hostchanged") {

		$("#loadingBar").hide();
		timerControl("");
		$('#videoContent #callerScreen').hide();
		$("#chatContent").show();
		videoAlertNotifn("Try again..");
	}
	if (signal.type == "callinactive") {

		$("#loadingBar").hide();
		timerControl("");
		$('#videoContent #callerScreen').hide();
		$("#cmecallcontact").hide();
		$("#changescreen").show();
		$("#chatContent").show();
		videoAlertNotifn("Call is inactive.");
		updateCallStatus(signal.callid, "closed");
		showongoingCalls();
	}


}


var callFlashStatus=null;			
function setCallFlash(){
	   if(callFlashStatus!=null){
		  clearCallFlash();
	   }
	   callFlashStatus = setInterval(function(){
									if($('#chatIcon').attr('src').indexOf('chatgreen.svg')!= -1 ){
									   $('#chatIcon').attr('src',path+'/images/temp/chatFlash2.png');
									   document.title="Incoming call"
									   document.getElementById('favicon').href = path+"/images/temp/chatFlash2.png";
									 }else{
									   $('#chatIcon').attr('src',path+'/images/menus/chatgreen.svg');
									   document.title="Colabus"
									   document.getElementById('favicon').href = path+"/images/favicon.ico";
									}
							   },2000);						
}

function clearCallFlash(){
	  $('#chatIcon').attr('src',path+'/images/menus/chatgreen.svg');
	  clearInterval(callFlashStatus);
	  callFlashStatus = null;
	  document.title="Colabus"
	  document.getElementById('favicon').href = path+"/images/favicon.ico";
}	  


function errorHandler(error) {
	console.log('error handler:::' + error);
}


function hangup() {
	//trace('Ending call');

	if (typeof recorderAv != "undefined" && recorderAv != null) {
		//recordVideoScreen("off","hangup");

		recordVideo("hangup");
	} else {
		if (user == "owner") {
			/*var jsonText = '{"calltype":"'+callType+'","type":"hostdisconnect","callid":"'+callId+'"}';
			notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
			serverConnection.send(notify); */
			/*console.log(switchAdminArray);
			var ix=switchAdminArray.indexOf(userIdglb.toString());
			if(ix>-1){
				switchAdminArray.splice(ix,1);
			}*/
			/*	 var jsonText = '{"calltype":"'+callType+'","type":"calldisconnect","callid":"'+callId+'","switchAdminArray":"'+switchAdminArray+'"}';
				 notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
				 serverConnection.send(notify); */
			if (userConnected.length > 1) {
				callConfirm(("Would you like to end the call for everyone?"), "delete", "Alldisconnect", "switchtohost", "inline")
			}
			else {
				console.log("calldisconnect stanza sending");
				var jsonText = '{"calltype":"' + callType + '","type":"calldisconnect","callid":"' + callId + '"}';
				notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
				serverConnection.send(notify);
				updateCallStatus(callId, "closed");
				hangupwithrecord();
				foraudiocall = 0;
				forvideocall = 0
				whiteboardArray = [];
				screenshareArray = [];
				muteArray = [];
				createMuteUI = [];
				stazaofMute = [];

				 callId="";	
				shareStaus = "off";
				$(".arr").attr('src', path + '/images/temp/slideboxright.png');
				//$(".arrow").css("left","-37px");
			}




		} else {
			disconnectReqVideoCall();
			sendAvailablePresence();
			hangupwithrecord();
			foraudiocall = 0;
			forvideocall = 0
			whiteboardArray = [];
			screenshareArray = [];
			muteArray = [];
			createMuteUI = [];
			stazaofMute = [];
			shareStaus = "off";
			$(".arr").attr('src', path + '/images/temp/slideboxright.png');
			//$(".arrow").css("left","-37px");
		}



	}
}

function hangupFromRemote() {
	//trace('Ending call');
	if (typeof recorderAv != "undefined" && recorderAv != null) {
		//recordVideoScreen("off","hangup");
		recordVideo("hangupAccepted");
	} else {
		hangupwithrecord();
	}
}

function hangupwithrecord() {
	var res = updateCallResponse(userIdglb, "calldisconnect");
	//console.log(res+" ----");
	console.log("res--"+res);
	if (res !== ""){
		console.log("res inside--"+res);
		closeVideoCall();
	}
}


function closeVideoCall() {
	console.log("user--"+user+"whoAdmin--"+whoAdmin);
	if(user == "owner" || whoAdmin == userIdglb){
		updateCallStatus(callId, "closed");
		backMessages();
	}
	else{
		backMessages();
	}
	// if(whoAdmin == userIdglb){
	// 	updateCallStatus(callId, "closed");
	// }
	closeContact();
	closeCallChat();
	$('#videoChatText').val('');
	$('#audiocallchat').html('');
	$('#videocallchat').html('');
	$('#mutecheck').hide();
	$('.transshow').html('').hide();
	// console.log("inside close video call");
	console.log("userConnected.length in close-----" + userConnected);
	// for(var i=0;i<userConnected.length-1;i++){
	for (var i = 0; i < userConnected.length; i++) {
		try {
			if (peerConnections[userConnected[i]] !== null) {
				peerConnections[userConnected[i]].close();
			}
			if (mediaStreamSource[userConnected[i]] == null) {
				mediaStreamSource[userConnected[i]].disconnect(processor[userConnected[i]]);
			}
			if (processor[userConnected[i]] != null) {
				processor[userConnected[i]].disconnect(audioContext[userConnected[i]].destination);
			}
			if (audioContext[userConnected[i]] != null) {
				audioContext[userConnected[i]].close();
			}
			/*var pc = getPeerConnection(userConnected[i]);
			var ids = userConnected[i];
				  pc.removeTrack(peerSsender[ids]);
				  peerSsender[ids]=[]; */


		}
		catch (e) {
			console.log(e);
		}
		/* var userName = $('div#'+userConnected[i]+' div.user_box_name').text();
		 console.log(userName);
		 callDisconnectAccepted(callType,userName,userConnected[i]);*/
	}
	// getCallHistoryByCallId(callId);
	

	$(".addnewuser").hide();
	$("#cmecallcontact").hide();
	$('.cmeMainScreenDivs').show();
	// if(cmeexpandcollapsecheck == '1'){
	// 	cmeExpandCollapse($('#cmeSwitchModeDiv').find('img.cmeFullCompact'));
	// 	//setCmeSwitchModeDiv();
	// }
	// else{
	// 	//setCmeSwitchModeDiv();
	// }
	callConf = "off";
	callflag = false;
	addconfon = false;
	// resizeVideo();

	if (peerConnections) {

		if (callType == "AS") {
			if (sharedVideo.srcObject) {
				sharedVideo.srcObject.getTracks().forEach(track => track.stop());
				sharedVideo.srcObject = null;
			}

			if (shareVideo.srcObject) {
				shareVideo.srcObject.getTracks().forEach(track => track.stop());
				shareVideo.srcObject = null;
			}
			try {
				localStream.getTracks().forEach(track => track.stop());
			} catch (exe) {
				console.log(exe.message);
			}

			localStream = null;
			remoteStream = [];
			sourceId = null;
		} else if (callType == "A") {
			try {
				if (screenStreamTrack != null) {
					if (callType == "A") {
						localStream.removeTrack(screenStreamTrack);
					} else {
						localStream.removeTrack(screenStreamTrack);

					}
					localScreenStream.stop();
				}
			} catch (e) {
				console.log(e)
			}

			if (localVideo.srcObject) {
				try {
					localStream.getTracks().forEach(track => track.stop());
				} catch (exe) {
					console.log(exe.message);
				}

				localStream = null;
				aRemoteVideo.srcObject = null;
				remoteStream = [];
			}


			$("#remote-audio").html("");
		} else {
			// console.log("inside video call 2");
			try {
				if (screenStreamTrack != null) {
					if (callType == "A") {
						localStream.removeTrack(screenStreamTrack);
					} else {
						localStream.removeTrack(screenStreamTrack);

					}
					localScreenStream.stop();
				}
			} catch (e) {
				console.log(e)
			}


			if (remoteVideo.srcObject) {
				remoteVideo.srcObject.getTracks().forEach(track => track.stop());
				remoteVideo.srcObject = null;
				remoteStream = [];
			}

			if (localVideo.srcObject) {
				localVideo.srcObject.getTracks().forEach(track => track.stop());
				localVideo.srcObject = null;
				try {
					localStream.getTracks().forEach(track => track.stop());
				} catch (exe) {
					console.log(exe.message);
				}

				localStream = null;
			}

			$("#local-video1").html("");
			console.log("inside video call 3");
		}

		peerConnections = [];
		// recognizationToggleStop();
		glbRecResult = "";
		$('.transcriptClass').html("");
		$('.transcriptClass').remove();
		$('#transcriptFor').css({ 'display': 'none' });
	}

	shareScreen = "off";
	shareStatus = "off";
	screenHost = false;
	shareStaus = "off";
	tilePreparationArray = [];
	userDevice = [];
	$("#aRemoteImage").show();
	$("#aRemoteVideo").hide();

	stopDurationTimer();
	$('#sound').html("");
	$(".incomingCall").hide();
	$("#chatContent").show();
	$("#videoContent").hide();
	$("#chatFor").hide();
	$("#highlightFor").hide().find("#hListDiv").html("");
	$("#avCallContainer").css({ 'width': '100%' });
	$('#recordStatusDiv').hide();
	$('#whiteboardFor').hide();
	$('#local-video1').html("");
	if ($('#cmecontent1').is(':visible')) {
		// showPersonalConnectionMessages();
		cmeMessages();
	}


	if (ongoingCall == "on") {
		if ($('#chatVideo').is(':visible')) {
			document.getElementById("chatVideo").srcObject = null;
			$("#chatVideo").hide();
		}
		if ($('#chatAudio').is(':visible')) {
			document.getElementById("chatAudio").srcObject = null;
			$("#chatAudio").hide();
		}
		ongoingCall = "off";
	}
	//$('#video-content').hide();

	sendAvailablePresence();
	$("#notification").html("");
	$("#notification").hide();
	userConnected = [];
	whoEverconnected = [];
	$("#video-content").css({ 'height': '100%' });
	$('#avStatusDiv').find('span#vStatusSpan,span#aStatusSpan,span#sStatusSpan').removeClass("avStatusSpan");
	$("#local-video1").hide();
	if (user == "owner") {

		Gab.connection.send(
			$pres({
				to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com' + "/" + "user_" + userIdglb + "_" + userFullname + "_" + userImgType,
				type: 'unavailable'
			}).c('x', { xmlns: "http://jabber.org/protocol/muc#owner" }));

	} else {
		Gab.connection.send(
			$pres({
				to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com' + "/" + "user_" + userIdglb + "_" + userFullname + "_" + userImgType,
				type: 'unavailable'
			}).c('x', { xmlns: "http://jabber.org/protocol/muc#user" }));
	}
	NameforChatRoom = "";
	user = "";
	queueArray = [];
	  callId="";
	  transDocId = "0";
	$("#astreamImgr").show();
	clearCallFlash();
	$("#cmecallcontact").hide();
	$('.cmeMainScreenDivs').show();
	// if(cmeexpandcollapsecheck == '1'){
	// 	cmeExpandCollapse($('#cmeSwitchModeDiv').find('img.cmeFullCompact'));
	// 	//setCmeSwitchModeDiv();
	// }
	// else{
	// 	//setCmeSwitchModeDiv();
	// }
	
}



function disconnectReqVideoCall() {
	console.log("inside disconnectReqVideoCall");
	var jsonText = '{"calltype":"' + callType + '","type":"calldisconnect","callid":"' + callId + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
}

function diclineVideoCall() {
	if(user == "owner" || whoAdmin == userIdglb){
		updateCallStatus(callId, "closed");
	}
	backMessages();
	// if(whoAdmin == userIdglb){
	// 	updateCallStatus(callId, "closed");
	// }
	var Id = $("#incoming").attr('callerid');
	var jsonText = '{"calltype":"' + callType + '","type":"calldeclined","callid":"' + callId + '"}';
	//  alert("calid--"+callId);
	stopCalltimer();
	var jid = Id + "@" + cmeDomain + ".colabus.com";
	var jsonText = '{"calltype":"' + callType + '","type":"calldeclined","callid":"' + callId + '"}';
	notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);

	$('#sound').html("");
	$(".incomingCall").hide();
	$("#cmecallcontact").hide();
	$('.cmeMainScreenDivs').show();
	// if(cmeexpandcollapsecheck == '1'){
	// 	cmeExpandCollapse($('#cmeSwitchModeDiv').find('img.cmeFullCompact'));
	// 	//setCmeSwitchModeDiv();
	// }
	// else{
	// 	//setCmeSwitchModeDiv();
	// }
	updateCallResponse(userIdglb, "calldeclined");

	// if ($('#callhistorydiv').is(':visible')) {
	// 	getCallHistoryByCallId(callId);
	// }
	Id = Id + "-" + cmeDomain + "-colabus-comm";
	if ($('#' + Id).is(':visible')) {
		// openNewChatBox(Id,"video");
		openNewCmeChatBox(Id, "", "", "", "video");
	}
	clearCallFlash();
	sendAvailablePresence();
	closeContact();
	closeCallChat();
	$('#videoChatText').val('');
	$('#audiocallchat').html('');
	$('#videocallchat').html('');
	$('#mutecheck').hide();
	$('.transshow').html('').hide();
	transDocId = "0";
}

function sendDndPresence() {
	var dndPresence = $pres().c('show').t('dnd');
	serverConnection.send(dndPresence);
}

function sendAvailablePresence() {
	// var dndPresence = $pres();
	var dndPresence = $pres().c('show').t('dndno');
	serverConnection.send(dndPresence);
}

function cancelAudioVideoCall(id) {
	// $("#cmecallcontact").hide();
	// $('.cmeMainScreenDivs').show();
	// if(cmeexpandcollapsecheck == '1'){
	// 	cmeExpandCollapse($('#cmeSwitchModeDiv').find('img.cmeFullCompact'));
	// 	//setCmeSwitchModeDiv();
	// }
	// else{
	// 	//setCmeSwitchModeDiv();
	// }
	// stopCalltimer();
	//var id= $("#incoming").attr('callerid');
	var jid = id + "@" + cmeDomain + ".colabus.com";
	var jsonText = '{"calltype":"' + callType + '","type":"callrequestcancelled","callid":"' + callId + '"}';
	notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);



	var jsonText = '{"calltype":"' + callType + '","type":"cancelconnecting","callid":"' + callId + '" ,"Reuser":"' + id + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);

	if (userConnected.length < 1) {
		var res = updateCallResponse(userIdglb, "callrequestcancelled");
		console.log("res--"+res);
		if (res !== ""){
			console.log("res inside--"+res);
			closeVideoCall();
		}
	} else {
		$("#notification").hide();
	}
	//  resizeVideo();

}

function callDisconnectAccepted(type, senderName, userid) {
	stopCalltimer();
	try {
		try {
			if (audioContext[userid] != null) {
				audioContext[userid].close();
			}
		} catch (e) {
			console.log(e)
		}

		try {
			if (peerConnections[userid] != null) {
				peerConnections[userid].close();
			}
		} catch (e) {
			console.log(e)
		}

		try {
			if (mediaStreamSource[userid] != null) {
				mediaStreamSource[userid].disconnect(processor[userid]);
			}

		} catch (e) {
			console.log(e);
		}

		try {
			if (processor[userid] != null) {
				processor[userid].disconnect(audioContext[userid].destination);
			}

		} catch (e) {
			console.log(e);
		}





	} catch (e) {
		console.log("exception is --" + e);
	}


	peerConnections[userid] = null;
	remoteStream[userid] = null;
	if (typeof recorderAv != "undefined" && recorderAv != null) {
		//recordVideoScreen("off","hangupAccepted");
		recordVideo("hangupAccepted");
	} else {

		//hangupwithrecord(); 
	}

	//console.log("before---"+userConnected);
	userConnected.splice(userConnected.indexOf(parseInt(userid)), 1);
	userDevice[parseInt(userid)] = null;

	//console.log("after---"+userConnected);
	if (userConnected.length == 0) {
		/*$("#localVideo_"+userid+"").remove();
		remoteVideo.srcObject=localStream;$("#local-video1").append('<video id="localVideo_'+id+'"*/
		updateCallStatus(callId, "closed");
		hangupwithrecord();

		callId="";	
		if (type == 'calldisconnect') {
			videoAlertNotifn("" + senderName + " has left the call.");
			muteArray = [];
			createMuteUI = [];
			stazaofMute = [];
			foraudiocall = 0;
			forvideocall = 0
			whiteboardArray = [];
			screenshareArray = [];
			tilePreparationArray = [];
			$(".arr").attr('src', path + '/images/temp/slideboxright.png');
			// $(".arrow").css("left","-37px");

		}

	} else {
		/* if(userConnected.length > 4 && callerType =="caller"){
			 $("#adduser").hide();
		 }else if(userConnected.length <= 4 && callerType =="caller"){
			 $("#adduser").show();
		 }*/
		if ($("#notification").is(":hidden")) {

		}
		else {
			if ($("#notification").find("#connuser").length != 0) {
				$("#notification").hide();
				// $("#notification").hide(500);
			}
		}
		if (callType == "AV") {
			$("#RemoteDiv_" + userid).remove();
			var remotevideoid = $("#local-video1").children().eq(0).attr('id').split('RemoteDiv_')[1];
			//console.log("remotevideoid----"+remotevideoid);
			if ($('#remoteVideo').attr('videoId') != remotevideoid) {
				switchVideo(remotevideoid);
			}
		} else if (callType == "A") {
			$("#RemoteDiv_" + userid).remove();

			var remoteaudioid = $("#remote-audio").children().eq(0).attr('id').split('RemoteDiv_')[1];
			if ($('#aRemoteVideo').attr('audioId') != remoteaudioid) {
				switchVideo(remoteaudioid);
			}
		}

		$("#participants_" + userid).remove();
		if (tilePreparationArray.indexOf(userid.toString()) > -1) {
			tilePreparationArray.splice(tilePreparationArray.indexOf(userid.toString()), 1);
		}
		if (whoEverconnected.indexOf(userid.toString()) > -1) {
			whoEverconnected.splice(whoEverconnected.indexOf(userid.toString()), 1);
		}
		var cnt = parseInt(tilePreparationArray.length);
		$("#pcount").html(cnt);
		$(".pcnt").html(cnt);
		// tilesPopup();
		/*
		$("#"+callId+"mm").html("");
		$("#"+callId+"mm").append("<div style='width:100%;display:inline-block;padding-left: 6px;font-family: opensanscondbold;font-size: 15px;color: #747778;padding-top: 1px;padding-bottom: 2px; border-bottom: 1px solid #CED2D5'>Participants:</div>");
		for(var i=0;i<userConnected.length;i++){
				  var imageSrc = $('#'+userConnected[i]).find('img').attr('src');
				  var title = $('#'+userConnected[i]).find('.user_box_name').attr('value');
				    
					$("#"+callId+"mm").append("<div class='videoUser' id='participants_"+userConnected[i]+"' onclick='switchVideo("+userConnected[i]+")' style='cursor:pointer;width:100%;height:45px;display:inline-block'><img title= '"+title+"' style=\"margin-left: 10px;float:left;height:35px;width:35px;margin-top:5px;margin-bottom:5px\" class=\"img-circle \" onerror=\"userImageOnErrorReplace(this);\" src=\""+imageSrc+"\">"
							+"<div style='margin-top:8px;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;width:60%'>"+title+""
							+"<p id='videochat_"+userConnected[i]+"' style='display:none;color:#08B627;font-size:11px'></p>"
							
						+"</div>"
						+"<div style='padding-top:12px'>"
							+"<span onclick='userDisconnect("+userConnected[i]+")' id='rdisc_"+userConnected[i]+"' style='margin-right: 5px;float:right;display:block'><img src='"+path+"/images/temp/audioRed.png' style='height:30px;width:30px' /></span>"
							+"<span id='raudio_"+userConnected[i]+"' title='Audio is muted' style='margin-right: 5px;float:right;display:none'><img src='"+path+"/images/temp/ramute.png' /></span>"
							+"<span id= 'rvideo_"+userConnected[i]+"' title='Video is paused' style='margin-right: 5px;float:right;display:none'><img src='"+path+"/images/temp/rvpause.png' /></span>"
						+"</div>"
					+"</div>"	  
					);
			}
		*/
		// resizeVideo();
		// resizeAudio();
		//alert("in the end");
	}

}
function switchVideo(id) {
	// $("#videoChatText").val("");
	//alert("ins---"+id);
	/* $(".item").removeClass("tile_active");
	 $("#RemoteDiv_"+id+"").addClass("tile_active");*/
	if (tilePreparationArray.indexOf(id.toString()) == -1) {

		if (screenshareArray.length > 0) {
			id = screenshareArray[0];
		} else {
			id = tilePreparationArray[tilePreparationArray.length - 1];
		}
	}
	if (callType == "AV") {

		$("div[id^=RemoteDiv_]").css("display", "");
		//$("#RemoteDiv_"+id).hide();
		// $("div[id^=RemoteDiv_]").css({ 'border': '1px solid #cccccc' });
		// $("#RemoteDiv_" + id).css({ 'border': '1px solid #4C92A7' });
		$('#remoteVideo').attr('videoId', '' + id);
		remoteVideo.srcObject = remoteStream[id];
		if (id == userIdglb) {
			// selfStream.getAudioTracks()[0].enabled = false;
			remoteVideo.srcObject = selfStream;

		}
		//remoteVideo.srcObject= document.getElementById('localVideo_'+id).srcObject;chatBack1.png
	} else {

		var imgurl = $('div#userId_' + id).find('img').attr('data-src');
		var userName = $('div#userId_' + id).find('.user_box_name').text();
		$('#aRemoteVideo').attr('audioId', '' + id);
		// $("div[id^=RemoteDiv_]").css("display", "inline-table");
		// $("div[id^=RemoteDiv_]").css({ 'border': '1px solid #cccccc' });
		// $("div[id^=RemoteDiv_]").css({ 'background': 'rgb(45, 131, 162)' });
		// $("#RemoteDiv_" + id + "").css({ 'border': '1px solid  #cccccc' });
		//$("#RemoteDiv_"+id+"").hide();
		// $("#RemoteDiv_" + id + "").css({ 'background': '#0B4860' });

		var sharedBy = $('#RemoteDiv_' + id).attr('screenshare');
		/*var sharedBy='N';
		if(screenshareArray.indexOf(id.toString())>-1){
			sharedBy='Y';
		}*/
		//   alert(sharedBy);
		if (sharedBy == 'Y') {
			aRemoteVideo.srcObject = document.getElementById('remoteAudio_' + id).srcObject;
			$("#aRemoteVideo").show();
			$("#aRemoteImage").hide();
		} else {
			$("#aRemoteVideo").hide();
			$("#aRemoteImage").show();
			$(".callConnectImage").attr('src', imgurl);
			$(".callConnectImage").attr('title', userName);
			$(".callConnectText").text(userName);
			if (id == userIdglb) {
				var imgsource = lighttpdpath + "//userimages//" + userIdglb + "." + userImgType;
				$(".callConnectImage").attr('src', imgsource);
				$(".callConnectImage").attr('title', userFullname);
				$(".callConnectText").text(userFullname);
			}
			// if( $("#host_"+id).css('border') == ' 1px solid yellow') {
			// 	$(".callConnectImage").css('border', '4px solid yellow');
			// }
			// else{
			// 	$(".callConnectImage").css('border', 'none');
			// }
			if ($("#RemoteDiv_" + id).attr("audiomute") == 'Y') {
				$("#mutecheck").show();
			}
			else{
				$("#mutecheck").hide();
			}
			if ($("#RemoteDiv_" + id).attr("screenshare") == 'Y') {
				$("#sscheck").show();
			}
			else{
				$("#sscheck").hide();
			}
			var uName ='';
			if(whoAdmin == userIdglb){
				uName = userFullname;
			}
			else{
				uName = $('div#userId_' + whoAdmin).find('.user_box_name').text();
			}
				
			if($('.callConnectImage').attr('title') == uName){
				$('.callConnectImage').css("border", "4px solid #FFCA00");
			}
			else{
				$('.callConnectImage').css("border", "none");
			}
		}

	}
	if (callType == "AV") {
		var w = document.getElementById("local-video1").clientWidth;
	} else {
		var w = document.getElementById("remote-audio").clientWidth;
	}
	if (w < 364) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 2;
	} else if (w > 364 && w < 547) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 3;
	} else if (w > 547 && w < 729) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 4;
	} else if (w > 729 && w < 911) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 5;
	} else if (w > 911 && w < 1093) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 6;
	} else if (w > 1094 && w < 1276) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 7;
	} else if (w > 1094 && w < 1276) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 8;
	} else if (w > 1276) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 10;
	}

	for (var i = dataIndex; i < endIndex; i++) {
		$(".item").eq(i).show();
	}
	//audiomute="" onhold="" videomute="" 
	$('#avStatusDiv').find('span#vStatusSpan,span#aStatusSpan,span#sStatusSpan').text("");
	$('#avStatusDiv').find('span#vStatusSpan,span#aStatusSpan,span#sStatusSpan').removeClass("avStatusSpan");
	if ($("#RemoteDiv_" + id).attr("audiomute") == 'Y') {
		$('#avStatusDiv').find('span#aStatusSpan').addClass("avStatusSpan").text("On mute");
	}
	if ($("#RemoteDiv_" + id).attr("videomute") == 'Y') {
		$('#avStatusDiv').find('span#vStatusSpan').addClass("avStatusSpan").text("Video paused");
	}

	if ($("#RemoteDiv_" + id).attr("onhold") == 'Y') {
		$('#avStatusDiv').find('span#sStatusSpan').addClass("avStatusSpan").text("On hold");
		$('#avStatusDiv').find('span#vStatusSpan,span#aStatusSpan').removeClass("avStatusSpan").text("");
	}
	resizeChatCall();

}

function callFailedDueToOffline() {
	stopCalltimer();
	closeVideoCall();
	videoAlertNotifn("Call disconnected.");
}

function callDeclinedAccepted(id) {
	if (userConnected.length < 1) {
		closeVideoCall();
		updateCallResponse(userIdglb, "calldeclined");
	} else {
		$("#notification").hide();
		callDeclineMultiUser(id);
		var jsonText = '{"calltype":"' + callType + '","type":"cancelconnecting","callid":"' + callId + '" ,"Reuser":"' + id + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);

	}
	videoAlertNotifn("Call is Declined.");
	stopCalltimer();

}

function callDeclineMultiUser(id) {
	var jid = id + "@" + cmeDomain + ".colabus.com";
	var jsonText = '{"calltype":"' + callType + '","type":"calldeclinemultiuser","callid":"' + busyCallId + '"}';
	notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
}

function busyVideoCall() {
	var jsonText = '{"calltype":"' + callType + '","type":"callbusy","callid":"' + busyCallId + '"}';
	notify = $msg({ to: busyId, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
}

function callBusyAccepted(id, type) {

	var res = updateCallResponse(id, type);
	if (userConnected.length < 1) {

		console.log("res--"+res);
		if (res !== ""){
			console.log("res inside--"+res);
			closeVideoCall();
		}
	} else {

		addConfUser("off");
	}
	videoAlertNotifn("User is busy.");
}

function callNotResponded() {
	var Id = $("#incoming").attr('callerid');
	var jid = Id + "@" + cmeDomain + ".colabus.com";
	var jsonText = '{"calltype":"' + callType + '","type":"callnotresponded","callid":"' + callId + '"}';
	notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
	updateCallResponse(userIdglb, "callnotresponded");
	// $("#notification").hide();
}

function callTimer() {
	clearTimeout(timeCount);
	// $("#notification").hide();
	callNotResponded();
	closeVideoCall();
}

function stopCalltimer() {
	clearTimeout(timeCount);
	$('#sound').html("");
	$("#notification").hide();
}
function callNotRespondedAccepted() {
	if (userConnected.length < 1) {
		closeVideoCall();
	} else {
		$("#notification").hide();
	}
	
	videoAlertNotifn("The user did not receive the call.");
}

var vAlertRef = null;
function videoAlertNotifn(msg) {
	alertFunNew(msg, 'warning');
	//console.log('vAlertRef:'+vAlertRef);
	if (vAlertRef != null) {
		clearTimeout(vAlertRef);
	}
	//console.log('vAlertRef:'+vAlertRef);
	vAlertRef = setTimeout(function () {
		if (vAlertRef != null) {
			hideConfirmBox();
			vAlertRef = null;
		}
	}, 10000);
}


function videoCallSound(filename) {

	$('#sound').html('<audio loop autoplay="autoplay"><source src="' + path + '/sound/' + filename + '.mp3" type="audio/mpeg" /><source src="' + path + '/sound/' + filename + '.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="' + path + '/sound/' + filename + '.mp3" /></audio>');
}


function checkChatWindowSize() {
	if (($("#highlightFor").is(":visible") == false) && ($("#transcriptFor").is(":visible") == false)) {
		if (window.innerWidth < 650) {
			if ($("#chatFor").is(":visible") == true) {
				$("#chatFor").css({ 'width': '50%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '50%' });
				window.resizeTo(640, 600);
				window.screenX = 300;
			} else {
				$("#chatFor").css({ 'width': '50%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '100%' });
				window.resizeTo(320, 600);
				window.screenX = 300;
			}
		} else {
			if ($("#chatFor").is(":visible") == true) {
				$("#chatFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '70%' });
			} else {
				$("#chatFor").css({ 'width': '30%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '100%' });
			}
		}

		if (window.innerWidth < 650) {
			if ($("#chatFor").is(":visible") == true) {
				$("#chatFor").css({ 'width': '50%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '50%' });
				window.resizeTo(640, 600);
				window.screenX = 300;
			} else {
				$("#chatFor").css({ 'width': '50%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '100%' });
				window.resizeTo(320, 600);
				window.screenX = 300;
			}
		} else {
			if ($("#chatFor").is(":visible") == true) {
				$("#chatFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '70%' });
			} else {
				$("#chatFor").css({ 'width': '30%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '100%' });
			}
		}


	} else {
		if (window.innerWidth < 650) {
			if ($("#chatFor").is(":visible") == true) {
				$("#chatFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '40%' });
				$("#highlightFor").css({ 'width': '30%' });
				$("#transcriptFor").css({ 'width': '30%' });
				window.resizeTo(900, 600);
				window.screenX = 200;
			} else {
				$("#chatFor").css({ 'width': '50%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '50%' });
				$("#highlightFor").css({ 'width': '50%' });
				$("#transcriptFor").css({ 'width': '50%' });
				window.resizeTo(320, 600);
				window.screenX = 300;
			}
		} else {
			if ($("#chatFor").is(":visible") == true) {
				$("#chatFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '40%' });
			} else {
				$("#chatFor").css({ 'width': '30%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '70%' });
			}
		}

	}
}

function checkChatWindowSizeForCall() {
	//console.log('window.innerHeight:'+window.innerHeight)
	window.focus();
	if (window.innerHeight < 600) {
		var height = screen.availHeight;
		height = height - ((20 / 100) * height);
		window.resizeTo(340, height);
		window.screenY = 100;
	}
}


function insertReciverCallHistory(calId) {
	var d = new Date();
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/insertReciverCallHistory?callId="+calId+"&userId="+userIdglb,
		type: "PUT",
		dataType: 'text',
		contentType: "application/json",
		// data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			console.log(result);
			checkSessionTimeOut(result);
		}

	});


}

function updateCallResponse(id, resType) {
	// alert("callId--"+callId);
	var res = false;
	// $('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/updateCallResponse?deviceType=web&callId=" + callId + "&toUserId=" + id + "&resType=" + resType + "&companyId=" + companyIdglb,
		type: "PUT",
		dataType: 'text',
		// contentType: "application/json",
		// data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			// $('#loadingBar').addClass('d-none').removeClass('d-flex');
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			// $('#loadingBar').addClass('d-none').removeClass('d-flex');
			// console.log(result);
			res = true;
		}

	});

	// $.ajax({
	// 	     url:path+"/ChatAuth",
	// 		 type:"post",
	// 	     data:{act:"updateCallResponse",callId:callId,toUserId:id,resType:resType,deviceType:"web",device:'web'},
	// 	     mimeType: "textPlain",
	// 	     async:false,
	// 	     success:function(result){
	// 	    	 	sessionTimeOutMethod(result);
	// 	    	    if(result != "SESSION_TIMEOUT"){
	// 	    	    	res = true;
	// 	    	    }
	// 		 } 
	// 	}); 

	return res;
}







function showvMoreOptions() {
	//console.log("--more show--->");
	if (callType == "AV") {
		if ($('#vMoreOptionsDiv').is(":visible")) {
			$('#vMoreOptionsDiv').removeClass('d-flex');
		} else {
			$('#vMoreOptionsDiv').addClass('d-flex');
		}


		if (!Array.isArray(screenshareArray) || !screenshareArray.length) {
			$('#vscreenshareImg').attr('onclick', 'ChangeStream("#vscreenshareImg")')
				.css({ 'cursor': 'pointer', 'opacity': '1' })
				.attr('title', 'Screen Share');
		}
		else {
			if (screenshareArray.indexOf(userIdglb) > -1) {

			}
			else {
				$('#vscreenshareImg').removeAttr('onclick')
					.css({ 'cursor': 'default', 'opacity': '0.5' })
					.attr('title', 'The other user is already sharing the screen.');
			}
		}
	} else {
		if ($('#aMoreOptionsDiv').is(":visible")) {
			$('#aMoreOptionsDiv').removeClass('d-flex');
		} else {
			$('#aMoreOptionsDiv').addClass('d-flex');
		}
		//alert("screenshareArray-"+screenshareArray);
		if (!Array.isArray(screenshareArray) || !screenshareArray.length) {
			$('#ascreenshareImg').attr('onclick', 'ChangeStream("#ascreenshareImg")')
				.css({ 'cursor': 'pointer', 'opacity': '1' })
				.attr('title', 'Screen Share');
		}
		else {
			if (screenshareArray.indexOf(userIdglb) > -1) {

			}
			else {
				$('#ascreenshareImg').removeAttr('onclick')
					.css({ 'cursor': 'default', 'opacity': '0.5' })
					.attr('title', 'The other user is already sharing the screen.');
			}
		}
	}
}
function showvMainOptions() {
	//console.log("--main show--->");
	$('.avOptionOver').css("visibility", "visible");
}
//onmouseover="showvMainOptions()" onmouseout="hidevMainOptions()" 
function hidevMainOptions() {
	$('.avOptionOver').css("visibility", "hidden");
	$('#vMoreOptionsDiv').hide();
	$('#aMoreOptionsDiv').hide();
	//console.log("--main hide--->");
}

function showspeedud(){
	$('#udspeed').addClass('d-flex');
}

function hidespeedud(){
	$('#udspeed').removeClass('d-flex');
}

function showspeedud2(){
	$('#udspeed2').addClass('d-flex');
}

function hidespeedud2(){
	$('#udspeed2').removeClass('d-flex');
}





var shareScreen = "off";
var shareStaus = "off";


var browserCrossStatus = "off";
function browserType(type) {

	if (senderBrowserType != type) {
		browserCrossStatus = "on";
		if (callType == "A") {
			$('#ascreenshareImg').removeAttr('onclick').css({ 'cursor': 'default', 'opacity': '0.5' });
			$('#ascreenshareImg').attr('title', 'Currently this feature is available between same browser (e.g: Firefox to Firefox browser).');
			shareScreen = "off";
		} else if (callType == "AV") {
			$('#vscreenshareImg').removeAttr('onclick').css({ 'cursor': 'default', 'opacity': '0.5' });
			$('#vscreenshareImg').attr('title', 'Currently this feature is available between same browser (e.g: Firefox to Firefox browser).');
			shareScreen = "off";
		}

	} else {
		browserCrossStatus = "off";
	}
}

var user;
function createCallGroup() {
	user = "owner";
	Gab.connection.send(
		$pres({
			to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com' + "/" + "user_" + userIdglb + "_" + userFullname + "_" + userImgType
		}).c('x', { xmlns: "http://jabber.org/protocol/muc#owner" }));

	// var message = $msg({to : 'tst1@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat" }).c('body').t("hi from caller"); // preapring the chat stanza to send		 
	//Gab.connection.send(message);  // sending the chat message  

}

function receiveCallGroup() {

	Gab.connection.send(
		$pres({
			to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com' + "/" + "user_" + userIdglb + "_" + userFullname + "_" + userImgType
		}).c('x', { xmlns: "http://jabber.org/protocol/muc#user" }));

	// var message = $msg({to : 'tst1@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat" }).c('body').t("hi from receiver"); // preapring the chat stanza to send		 
	//Gab.connection.send(message);  // sending the chat message  

}
var callOwner = "";
function createAnotherCall(id, callType, type) {
	//console.log("userConnected---"+userConnected);
	if (type != "ExUser") {
		showCalling(id);
		var jsonText = '{"calltype":"' + callType + '","type":"callconfrequest","devicetype":"web","browsertype":"' + senderBrowserType + '","callid":"' + callId + '","NameforChatRoom":"' + NameforChatRoom + '","userOnCallInfo":"' + userConnected + '","callid":"' + callId + '","muteArray":"' + muteArray + '","admin":"' + userIdglb + '","participant" :"' + tilePreparationArray + '","screenshare":"' + screenshareArray + '"   }';
		notify = $msg({ to: id + "@" + cmeDomain + ".colabus.com", "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
		$("#statuschange").html("Ringing");
		// timeCount = setTimeout(callTimer, 30000);
	} else {
		var jsonText = '{"calltype":"' + callType + '","type":"callconfrequestEx","devicetype":"web","browsertype":"' + senderBrowserType + '","callid":"' + callId + '","NameforChatRoom":"' + NameforChatRoom + '","userOnCallInfo":"' + userConnected + '","callid":"' + callId + '","muteArray":"' + muteArray + '","admin":"' + userIdglb + '","participant" :"' + tilePreparationArray + '","screenshare":"' + screenshareArray + '"   }';
		notify = $msg({ to: id + "@" + cmeDomain + ".colabus.com", "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
		// showConnecting(id,user);
		// timeCount = setTimeout(callTimer, 30000);
	}

	//To send the joining user to all the caller

	//shareStaus ="on";
	//console.log(jsonText);
	addConfUser("off");

	//console.log("create another ->"+id);
	if (switchAdminArray.indexOf(id.toString()) == -1) {
		switchAdminArray.push(id.toString());
	}
	insertCallUserHistory(id, callType, callId);
}

function getClientSpeed(id, callType) {
	var jsonText = '{"calltype":"' + callType + '","type":"getspeed","devicetype":"web","browsertype":"' + senderBrowserType + '","callid":"' + callId + '","NameforChatRoom":"' + NameforChatRoom + '","userOnCallInfo":"' + userConnected + '","callid":"' + callId + '"}';
	notify = $msg({ to: id + "@" + cmeDomain + ".colabus.com", "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
}
function sendClientSpeed(id, callType) {
	var jsonText = '{"calltype":"' + callType + '","type":"sendspeed","devicetype":"web","browsertype":"' + senderBrowserType + '","callid":"' + callId + '","NameforChatRoom":"' + NameforChatRoom + '","userOnCallInfo":"' + userConnected + '","callid":"' + callId + '","callspeed":"' + callspeed + '"}';
	notify = $msg({ to: id + "@" + cmeDomain + ".colabus.com", "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
}



var speedCounter = 0;
var speedCounterFlag = false;
var speedCounterTimer;
function getAllConnectedUsersSpeed() {
	//setting to default values when clicked on add user. 
	speedCounter = 0;
	speedCounterFlag = false;
	var reqSpeed = "";
	if (callType == "A") {
		reqSpeed = (userConnected.length + 1) * 0.06;
	}
	else {
		reqSpeed = (userConnected.length + 1) * 0.2;
	}
	// var reqSpeed = (userConnected.length + 1) * 0.06;

	if ((callspeed != "undefined" && callspeed <= reqSpeed)) { // checking any users is facing connectivity issue
		//	videoAlertNotifn("You are experiencing Insufficient Internet Connection. You cannot connect to this User");
		videoAlertNotifn("You are experiencing Insufficient Internet Connection.");

	}

	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();

	var jsonText = '{"calltype":"' + callType + '","type":"getSpeedFromConnectedUser","callid":"' + callId + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);; // preapring the chat stanza to send		 
	serverConnection.send(notify);

	speedCounterTimer = setTimeout(function () { // maximum 5secs waits for responds from connected users.
		speedCounterFlag = true; // setting this flag to true to not to check speed from remaining users.
		$("#loadingBar").hide();
		timerControl("");
	}, 5000);
}

function sendClientSpeedFromConnectedUser(id, callType) {
	// console.log("id,callType:"+id+" ---"+callType);
	var jsonText = '{"calltype":"' + callType + '","type":"speedFromConnectedUser","devicetype":"web","browsertype":"' + senderBrowserType + '","callid":"' + callId + '","NameforChatRoom":"' + NameforChatRoom + '","userOnCallInfo":"' + userConnected + '","callid":"' + callId + '","callspeed":"' + callspeed + '"}';
	notify = $msg({ to: id + "@" + cmeDomain + ".colabus.com", "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
}

function checkSpeedForAddUser(id, receiverSpeed) {
	speedCounter++;
	var reqSpeed = (userConnected.length + 1) * 0.6;
	//	console.log("receiverSpeed---"+receiverSpeed+"---reqSpeed--"+reqSpeed);
	/*if( !speedCounterFlag &&(receiverSpeed != "undefined" && receiverSpeed <=  reqSpeed  )){ // checking any users is facing connectivity issue
		videoAlertNotifn($('div#'+id+' div.user_box_name').text()+" is experiencing Insufficient Internet Connection. You cannot connect to this User");
		speedCounter = userConnected.length; // setting total no of connected users  to speedcounter to hide loading bar.
		speedCounterFlag=true; // setting this flag to true to not to check speed from remaining users.
		addConfUser("off"); // calling this method to go back to call.
		clearTimeout(speedCounterTimer);
	}*/

	if (speedCounter == userConnected.length) {
		$("#loadingBar").hide();
		timerControl("");
	}
}



var addconfon = false;
function addConfUser(type) {
	//$("#loadingBar").show();
	//timerControl("start");
	//$("#loadMsg").hide();

	callConf = "on";
	// alert("outside on");
	if (type == "on") {
		alert("inside on");
		getAllConnectedUsersSpeed();


		/* if(userConnected.length > 4 && callerType =="caller"){
			 //videoAlertNotifn("Your device or One or more of the participants may experience intermittent connectivity issue or low internet bandwidth for uninterrupted Video/Audio call streaming. This video/audio call may freeze while buffering.");
			 videoAlertNotifn("You may experience poor connection or intermittent connectivity issues.");
		 }*/
		ongoingCall = "on";
		addconfon = true;
		$("#videoContent").hide();
		$("#cmecallcontact").hide();
		// $("#chatContent").show();
		contacts();//added
		$("#chat-content").hide();
		$("#chat-frame3").hide();
		$("#chat-frame").show();
		$(".backChatMain").attr('onclick', 'addConfUser("off")');
		$(".backChatMain").attr('title', 'Back to call');
		$("#out").hide();
		$("#logout").hide();
		$("#addGuest").hide();
		if (callType == "AV") {
			$(".audiocall").hide();
			$(".videocall").show();
			// $("#cmecallcontact").show();
		} else {
			$(".videocall").hide();
			$(".audiocall").show();
			// $("#cmecallcontact").show();
		}

		//$(".backChatMain").hide();
	} else {
		ongoingCall = "off";
		addconfon = false;
		$("#chatVideo").hide();
		$("#chatAudio").hide();
		$("#chatContent").hide();
		$("#chat-frame").hide();
		$("#chat-frame3").show();
		$(".backChatMain").attr('onclick', "backMessages()");
		$(".backChatMain").attr('title', 'Messages');
		$("#videoContent").show();
		$("#cmecallcontact").show();
		$("#logout").show();
		$("#addGuest").show();
		$(".videocall").show();
		$(".audiocall").show();
		$("#out").show();
		//$(".backChatMain").show();
	}
	//$("#loadingBar").hide();	
	//timerControl("");
}

function showCalling(id) {
	$("#notification").show();
	var name = $('#userId_' + id).find('.user_box_name').text();
	var imageSrc = $('#userId_' + id).find('img').attr('data-src');
	var Ui = 
			// "<div id='adduser' class='' style='height:60px;padding-top: 7px;padding-bottom: 7px;width: 100%; background-color: rgba(113, 118, 126, 0.6);' > "
			// 	+ "<div class='col-xs-3' style='padding-left: 8px;padding-top:0.5%'> <img src='" + imageSrc + "' onerror='userImageOnErrorReplace(this);' style='height: 34px; min-height: 45px; width:45px;min-width: 45px;border-radius:39px'/></div>"
			// 	+ "<div class='user_box_name col-xs-4' id='statuschange' style='font-family: OpenSansRegular;font-weight: bold;color:#fff; font-size: 14px; width: 50%; padding-top: 7px;padding-left: 20px;' value='" + name + "'>Calling...</div>"
			// 	+ "<div class='col-xs-2' onclick='cancelAudioVideoCall(" + id + ")' style='cursor:pointer;min-width: 35px;padding-top:11px;float: right;padding-right: 8px;padding-left: 0px;max-width: 13%;;'><img src='" + path + "/images/temp/audioRed.png'   style='width: 25px;height: 25px;float:right'/></div>"
			// + "</div>";
			'<div class="d-flex justify-content-end align-items-center" style="height:60px;width: 100%; background-color: rgba(113, 118, 126, 0.6);">'
				+'<div class="p-2 "><img src="' + imageSrc + '" title="'+name+'" onerror="userImageOnErrorReplace(this);" style="height: 34px; min-height: 45px; width:45px;min-width: 45px;border-radius:39px"></div>'
				+'<div class="p-2 user_box_name col-xs-4" id="statuschange" style="font-family: OpenSansRegular;font-weight: bold;color:#fff; font-size: 14px; width: 50%;" value="' + name + '">Calling...</div>'
				+'<div class="p-2 " style="cursor:pointer;min-width: 35px;float: right;max-width: 13%;;"><img onclick="event.stopPropagation();cancelAudioVideoCall(' + id + ')" src="images/cme/audioRed.svg"  style="width: 25px;height: 25px;float:right"></div>'
			+'</div>'
	$("#notification").html(Ui);
}





//--------------------------------------------------------------------------------> 


var peerConnections = {};
var peerVsender = {};
var peerSsender = {};
var peerAsender = {};
var asender;
var vsender;
var ssender;
var dataChannel = {};
function getPeerConnection(id) {

	if (peerConnections[id]) {
		//console.log('inside get peer connection---'+ id);
		return peerConnections[id];
	}
	const videoTracks = localStream.getVideoTracks();
	const audioTracks = localStream.getAudioTracks();
	//console.log('videoTracks2.length:'+videoTracks.length);
	//console.log('audioTracks2.length:'+audioTracks.length);

	var pc = new RTCPeerConnection(peerConnectionConfig);
	var sendChannel = pc.createDataChannel('sendDataChannel', dataConstraint);
	dataChannel[id] = sendChannel;
	dataChannel[id].onopen = onSendChannelStateChange;
	dataChannel[id].onclose = onSendChannelStateChange;
	pc.ondatachannel = receiveChannelCallback;
	peerConnections[id] = pc;
	if (videoTracks.length > 0) {
		if (callType == "AV") {
			if (shareScreen == "on" && screenHost == true) {
				//console.log("in video call inside screen share ");
				try {
					pc.removeTrack(peerVsender[id]);
				} catch (err) {
					console.log("error:" + err);
				}
				ssender = pc.addTrack(screenStreamTrack, localStream);
				peerSsender[id] = ssender;
			} else {
				videoStream = videoTracks[0];//---- Extracting video stream
				//console.log("videoTracks.length---"+videoTracks.length);
				vsender = pc.addTrack(videoStream, localStream);
				//console.log("adding stream");
				peerVsender[id] = vsender;
			}

		} else {
			//console.log("in aduio call inside video track length >0");
			ssender = pc.addTrack(screenStreamTrack, localStream);
			peerSsender[id] = ssender;
			//makeOffer(id);
		}
	}
	if (audioTracks.length > 0) {
		audioStream = audioTracks[0];//---- Extracting audio stream
		asender = pc.addTrack(audioStream, localStream);
		peerAsender[id] = asender;
	}
	//pc.addStream(localStream);
	pc.onicecandidate = function (evnt) {
		//console.log("evnt.candidate---"+JSON.stringify(evnt.candidate));
		if (evnt.candidate != null) {
			var body = JSON.stringify({ "type": "candidate", "candidate": evnt.candidate.candidate, "ice": evnt.candidate, "callid": callId, "id": evnt.candidate.sdpMid, "label": evnt.candidate.sdpMLineIndex, });
			var message = $msg({ to: id + "@" + cmeDomain + ".colabus.com", "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(body);
			serverConnection.send(message);
		}
	};
	pc.onaddstream = function (evnt) {
		showFileAfterCallConnect();
		try {
			// recognizationToggleStart('', 'globalVoiceCall', 'globalVoiceCall', '');
		} catch (e) {
			console.log('e1:' + e);
		}
		//console.log('Received new stream');
		if (callType == "AV") {
			$('#videoContent #callerScreen').hide();
			$('#videoContent #audioFor #screenFor').hide();
			$('#videoContent #videoFor').show();
			$("#videoContent ").show();
			$('.callHangUp').attr('src', path + '/images/cme/phonedis.svg');
		} else if (callType == "A") {
			$('#videoContent #callerScreen').hide();
			$('#videoContent #videoFor #screenFor').hide();
			$('.callerScreenStatus').text("Connected");//.delay(5000).text("");
			$('#videoContent #audioFor').show();
			$('.callHangUp').attr('src', path + '/images/cme/phonedis.svg');
			
		}


	};

	//code for user disconnection

	/*	pc.oniceconnectionstatechange = function(event) {
			  //pc.iceConnectionState === "failed" ||  pc.iceConnectionState === "disconnected" ||
			  console.log(pc.iceConnectionState +"-----"+id );  
			  if (pc.iceConnectionState === "closed" ||pc.iceConnectionState === "failed" ||  pc.iceConnectionState === "disconnected") {
					 if(id!=whoAdmin){	
						 userDisconnect(id);
						 var AuserName = $('div#'+id+' div.user_box_name').text();
						 videoAlertNotifn(""+AuserName+" has left the call .");
					 }
				}
		};	*/




	pc.ontrack = function (event) {

		remoteStream[id] = event.streams[0];
		/*const videoTracks1 = event.streams[0].getVideoTracks();
		  const audioTracks1 = event.streams[0].getAudioTracks();*/
		//console.log('videoTracks3.length:'+videoTracks1.length);
		//console.log('audioTracks3.length:'+audioTracks1.length);

		if (callType == "AV") {
			uploadSpeed();
			$("#local-video1").css("display", "");
			$("#video-content").css("height", "100%");
			$("#local-video").hide();

			//video tiles preparation
			console.log("the tiles preparation array is before insert ---- "+tilePreparationArray);

			if (tilePreparationArray.indexOf(id.toString()) == -1) {
				tilePreparationArray.push(id.toString());
			}
			console.log("the tiles preparation array is after insert ---- "+tilePreparationArray);
			// $("#local-video1").html('');
			// for(i=tilePreparationArray.length-1;i>=0;i--){
			for (i = 0; i < tilePreparationArray.length; i++) {
				var tileId = tilePreparationArray[i];
				var userName = $('div#userId_' + tileId).find('.user_box_name').text();
				var displayprop1 = "none";
				var dis = "none";
				if (stazaofMute.indexOf(tileId.toString()) > -1) {
					displayprop1 = "block";

				}
				if (userConnected.indexOf(tileId.toString()) > -1) {
					bgdisplay = "none";
				}
				var propm = "";
				if (userIdglb == tileId) {
					imgurl = lighttpdpath + "//userimages//" + userIdglb + "." + userImgType;
					userName = userFullname;
					if (user == "owner") {
						dis = "block";
					}
					propm = "muted  autoplay";
					//alert(dis);
					//	var muteDisplay1="none";

				}
				if (remoteVideo.srcObject == null || $('#remoteVideo').attr('videoId') == tileId) {
					$('#remoteVideo').attr('videoId', '' + tileId);
					remoteVideo.srcObject = remoteStream[tileId];
					if ($("#localVideo_" + tileId).length == 0) {
						var ui = addvideoTiles(tileId, userName, bgdisplay, displayprop1, dis, propm);
						$("#local-video1").prepend(ui);
						// $("#local-video1").prepend('<div  class="item" id = "RemoteDiv_' + tileId + '" audiomute="" onhold="" videomute="" style="height:137px;width:182px;float: left;display:none;border:1px solid #cccccc;">'
						// 	+ '<div id="bgTrans_' + tileId + '" class="" style="z-index: 1;position: absolute;opacity: 0.8;width: 182px;float: left;background-color: grey;height: 137px;display: ' + bgdisplay + ';">'

						// 	+ '<div style="color: white;position: absolute;top: 34%;left: 30%;">Connecting...</div>'

						// 	+ '</div>'
						// 	+ ' <div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 180px;position:absolute;z-index:1000">'
						// 	+ ' <div class="defaultWordBreak" style="width:150px;float:left;font-size: 12px; color: lightgrey; margin-top: 2px;    white-space: pre;">' + '  ' + userName + '</div>'
						// 	+ '<div class="" style="width:30px;float:right;display:none" onclick="userDisconnect(' + tileId + ')" ><img src="' + path + '/images/temp/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/></div>'
						// 	+ '</div>'
						// 	+ ' <video id="localVideo_' + tileId + '" ondblclick="enterText(' + tileId + ');"   onclick="switchVideo(' + tileId + ')" autoplay ' + propm + ' style="cursor:pointer;width: 180px;height:135px ; background: transparent;    border: none;"></video>'
						// 	+ ' <div style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">'
						// 	+ ' <div style="padding-top:12px;/*! display: none; */">'
						// 	+ ' <div id="host_' + tileId + '" style="display:' + dis + ';"><img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  ></div>'
						// 	+ '   <span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display:' + displayprop1 + ';margin-right: -8px;">'
						// 	+ '<img style="width:18px;    margin-top: 8px;" src="' + path + '/images/temp/mic2.png"></span>'
						// 	+ '</div>'
						// 	+ ' <div class="meterGraph" >'
						// 	+ '  <div class="meter" ></div>'
						// 	+ ' </div></div></div>'
						// );
						if (tileId != userIdglb) {
							document.getElementById('localVideo_' + tileId).srcObject = remoteStream[tileId];
						} else {
							selfStream = document.getElementById('localVideo').srcObject;
							document.getElementById('localVideo_' + userIdglb).srcObject = document.getElementById('localVideo').srcObject;

						}

					}


				}
				else {
					if ($("#localVideo_" + tileId).length == 0) {
						var ui = addvideoTiles(tileId, userName, bgdisplay, displayprop1, dis, propm);
						$("#local-video1").prepend(ui);
						// 	'<div  class="item" id = "RemoteDiv_' + tileId + '" audiomute="" onhold="" videomute="" style="height:137px;width:182px;float: left;display:none;border:1px solid #cccccc;">'
						// 	+ '<div id="bgTrans_' + tileId + '" class="" style="z-index: 1;position: absolute;opacity: 0.8;width: 182px;float: left;background-color: grey;height: 137px;display: ' + bgdisplay + ';">'

						// 	+ '<div style="color: white;position: absolute;top: 34%;left: 30%;">Connecting...</div>'

						// 	+ '</div>'
						// 	+ ' <div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 180px;position:absolute;z-index:1000">'
						// 	+ ' <div class="defaultWordBreak" style="width:150px;float:left;font-size: 12px; color: lightgrey; margin-top: 2px;    white-space: pre;">' + '  ' + userName + '</div>'
						// 	+ '<div class="" style="width:30px;float:right;display:none" onclick="userDisconnect(' + tileId + ')" ><img src="' + path + '/images/temp/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/></div>'
						// 	+ '</div>'
						// 	+ ' <video id="localVideo_' + tileId + '" ondblclick="enterText(' + tileId + ');"   onclick="switchVideo(' + tileId + ')" autoplay  ' + propm + ' style="cursor:pointer;width: 180px;height:135px ; background: transparent;    border: none;"></video>'
						// 	+ ' <div style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">'
						// 	+ ' <div style="padding-top:12px;/*! display: none; */">'
						// 	+ ' <div id="host_' + tileId + '" style="display:' + dis + ';"><img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  ></div>'
						// 	+ '   <span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display:' + displayprop1 + ';margin-right: -8px;">'
						// 	+ '<img style="width:18px;    margin-top: 8px;" src="' + path + '/images/temp/mic2.png"></span>'
						// 	+ '</div>'
						// 	+ ' <div class="meterGraph" >'
						// 	+ '  <div class="meter" ></div>'
						// 	+ ' </div></div></div>'
						// );
						if (tileId != userIdglb) {
							document.getElementById('localVideo_' + tileId).srcObject = remoteStream[tileId];
						} else {
							selfStream = document.getElementById('localVideo').srcObject;
							document.getElementById('localVideo_' + userIdglb).srcObject = document.getElementById('localVideo').srcObject;

						}
					}

				}





			}
			for (i = tilePreparationArray.length - 1; i >= 0; i--) {
				var tileId = tilePreparationArray[i];
				if (tileId != userIdglb) {
					try {
						if (typeof remoteStream[tileId] != "undefined" && remoteStream[tileId] != "" && remoteStream[tileId] != null) {
							document.getElementById('localVideo_' + tileId).srcObject = remoteStream[tileId];
						}
					} catch (e) {
						console.log(e);
					}


				} else {
					selfStream = document.getElementById('localVideo').srcObject;
					document.getElementById('localVideo_' + userIdglb).srcObject = document.getElementById('localVideo').srcObject;

				}



			}


			for (i = 0; i < userConnected.length; i++) {
				//alert(userConnected[i]);
				$("#bgTrans_" + userConnected[i]).hide();
			}
			$("#bgTrans_" + userIdglb).hide();
			$("#notification").hide();
			if (!userConnected.includes(id)) {
				if ((userConnected.indexOf(id)) == -1) {
					userConnected.push(id);
				}
				for (i = 0; i < userConnected.length; i++) {

					$("#bgTrans_" + userConnected[i]).hide();
				}
				if (recordStatus == "on") {//-----This is for recording. If recording is in progress then adding newly connected stream also.
					addStreamToRecord(id);
				}
				resizeVideo();
				if (userConnected.length > 1) {
					$("#notification").hide();
					wbResize();
					$("div[id^=RemoteDiv_]").css("display", "inline-table");
					var rid = $('#remoteVideo').attr('videoId');
					// $("div[id^=RemoteDiv_]").css({ 'border': '1px solid #cccccc' });
					// $("#RemoteDiv_" + rid + "").css({ 'border': '1px solid #4C92A7' });
					var pcount = parseInt(tilePreparationArray.length);
					$(".pcnt").html(pcount);
					$("#" + callId + "mm").html("");
					$("#" + callId + "mm").append("<div  style='width:100%;display:inline-block;padding-left: 6px;font-family: opensanscondbold;font-size: 15px;color: #747778;padding-top: 1px;padding-bottom: 2px; border-bottom: 1px solid #CED2D5'>Participants: <span id='pcount'>" + pcount + "<span></div>");
					for (i = tilePreparationArray.length - 1; i >= 0; i--) {
						if (tilePreparationArray[i] != userIdglb) {
							var imageSrc = $('#' + tilePreparationArray[i]).find('img').attr('src');
							var displayprop = "none";
							if (stazaofMute.indexOf(tilePreparationArray[i].toString()) > -1) {
								displayprop = "block";
							}
							var ssprop = "none";
							if (screenshareArray.indexOf(tilePreparationArray[i]) > -1) {
								ssprop = "block"
							}
							var wbprop = "none";
							if (whiteboardArray.indexOf(tilePreparationArray[i]) > -1) {
								wbprop = "block"
							}
							var title = $('#userId_' + tilePreparationArray[i]).find('.user_box_name').attr('value');
							if (($("#" + callId + "mm").find("#participants_" + tilePreparationArray[i])).length == 0) {
								$("#" + callId + "mm").append("<div id='participants_" + tilePreparationArray[i] + "' class='videoUser' ondblclick='enterText(" + tilePreparationArray[i] + ");' onclick='switchVideo(" + tilePreparationArray[i] + ")' style='position:relative;cursor:pointer;width:100%;height:45px;display:inline-block'><img title= '" + title + "' style=\"margin-left: 10px;float:left;height:35px;width:35px;margin-top:5px;margin-bottom:5px\" class=\"img-circle rounded-circle\" onerror=\"userImageOnErrorReplace(this);\" src=\"" + imageSrc + "\">"

									+ '<div id="hostp_' + tilePreparationArray[i] + '" style="display:none;position:absolute;left:32px" onclick="">'
									+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031" style=""   >'
									+ '</div>'


									+ "<div style='margin-top:13px;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;width:60%'>" + title + ""
									+ "<p id='videochat_" + tilePreparationArray[i] + "' style='display:none;color:#08B627;font-size:11px'></p>"

									+ "</div>"
									+ "<div /*style='padding-top:12px'*/>"
									+ "<div style='position:absolute;background-color:#fff;right:0;padding-left:10px'>"
									+ "<span class='userdisconnect' onclick='userDisconnect(" + tilePreparationArray[i] + ")' id='rdisc_" + tilePreparationArray[i] + "' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img src='" + path + "/images/cme/phonedis.svg' style='height:24px;width:24px' /></span>"
									+ "<span id='raudiopar_" + tilePreparationArray[i] + "' title='Audio is muted' style='margin-top: 10px;margin-right: 5px;float:right;display:" + displayprop + "'><img style='width:24px'  src='" + path + "/images/temp/mic.svg' /></span>"
									+ "<span id='sharescreen_" + tilePreparationArray[i] + "' title='Screen share' style='margin-top: 10px;margin-right: 5px;;float:right;display:" + ssprop + "'><img style='width:24px' src='" + path + "/images/temp/share.svg' /></span>"
									+ "<span id='whiteboard_" + tilePreparationArray[i] + "' title='White Board' style='margin-top: 10px;margin-right: 5px;float:right;display:" + wbprop + "'><img   style='width:24px' src='" + path + "/images/temp/whiteboard.svg' /></span>"

									+ "<span id= 'rvideo_" + tilePreparationArray[i] + "' title='Video is paused' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img  style='width:24px' src='" + path + "/images/temp/pause.svg' /></span>"
									+ "</div>"
									+ "</div>"
									+ "</div>"
								);
							}

						}
					}
				} else {
					if (!($('#chatFor').is(':visible'))) {
						chatFor();
					}
				}
				



			}









			if ($("#participants_" + userIdglb).length == 0) {
				//console.log("----"+$("#participants_"+userIdglb).length);
				var todisplay = "none";
				if (user == "owner") {
					todisplay = "block";
				}
				var toD1 = "none";
				if (stazaofMute.indexOf(userIdglb.toString()) > -1) {
					toD1 = "block"
				}
				//console.log("userFullmain--->"+userFullName);
				var ssprop1 = "none";
				if (screenshareArray.indexOf(userIdglb) > -1) {
					ssprop1 = "block"
				}
				var wbprop1 = "none";
				if (whiteboardArray.indexOf(userIdglb) > -1) {
					wbprop1 = "block"
				}
				var imgs = lighttpdpath + "//userimages//" + userIdglb + "." + userImgType;
				$("#" + callId + "mm").append("<div id='participants_" + userIdglb + "' class='videoUser' ondblclick='enterText(" + userIdglb + ");' onclick='switchVideo(" + userIdglb + ")' style='position:relative;cursor:pointer;width:100%;height:45px;display:inline-block'><img title= '" + userFullname + "' style=\"margin-left: 10px;float:left;height:35px;width:35px;margin-top:5px;margin-bottom:5px\" class=\"img-circle rounded-circle\" onerror=\"userImageOnErrorReplace(this);\" src=\"" + imgs + "\">"

					+ '<div id="hosta_' + userIdglb + '" style="position:absolute;left:32px;display:' + todisplay + ';" onclick="oncallswitch();">'
					+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031" style="" >'
					+ '</div>'



					+ "<div style='margin-top:13px;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;width:60%'>" + userFullname + ""
					+ "<p id='videochat_" + userIdglb + "' style='display:none;color:#08B627;font-size:11px'></p>"

					+ "</div>"
					+ "<div /*style='padding-top:12px'*/>"
					//+'<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="/images/myprojects.png" projecttype="my_project" projectid="6031"  >'
					+ "<div style='position:absolute;background-color:#fff;right:0;padding-left:10px'>"
					+ "<span class='userdisconnect' onclick='hangup();' id='rdisc_" + userIdglb + "' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img src='" + path + "/images/cme/phonedis.svg' style='height:24px;width:24px' /></span>"
					+ "<span id='raudiohostpar_" + userIdglb + "' title='Audio is muted' style='margin-top: 10px;margin-right: 5px;float:right;display:" + toD1 + "'><img style='width:24px' src='" + path + "/images/temp/mic.svg' /></span>"
					+ "<span id='whiteboardu_" + userIdglb + "' title='White Board' style='margin-top: 10px;margin-right: 5px;float:right;display:" + ssprop1 + "'><img style='width:24px' src='" + path + "/images/temp/whiteboard.svg' /></span>"
					+ "<span id='sharescreenu_" + userIdglb + "' title='Screen share' style='margin-top: 10px;margin-right: 5px;float:right;display:" + wbprop1 + "'><img style='width:24px' src='" + path + "/images/temp/share.svg' /></span>"

					+ "<span id= 'rvideo_" + userIdglb + "' title='Video is paused' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img style='width:24px' src='" + path + "/images/temp/pause.svg' /></span>"
					+ "</div>"
					+ "</div>"
					+ "</div>"
				);
			}




			if (Array.isArray(screenshareArray) && screenshareArray.length) {

				if (screenshareArray[0] != userIdglb) {
					switchVideo(screenshareArray[0]);

				}

			}
			tilesPopup();

			if (user != "owner") {
				$("div[id^=host_]").css("border", "none");
				$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");
				$("div[id^=hostp_]").css("display", "none");
				$("#hostp_" + whoAdmin).css("display", "block");
				$("div[id^=hostu_]").css("border", "none");
				$("#hostu_" + whoAdmin).css("border", "3px solid #FFCA00");
				$("div[id^=hosta_]").css("display", "none");
				$("#hosta_" + whoAdmin).css("display", "block");
				$("div[id^=hostb_]").css("display", "none");
				$("#hostb_" + whoAdmin).css("display", "block");
			}

			if (forvideocall == 0) {
				$("#chatFor").toggle();
				$("#chatFor").toggle();
				$("#chatFor").hide();
				forvideocall++;
				$("#videoFor").find(".arrow").css("left", "-2px");
				$("#videoFor").find(".arr").attr('src', path + '/images/temp/slideboxright.png');
				$("#videoFor").find(".arrow").css("left", "-2px");
				$("#videoFor").find(".arr").css("width", "41px");
				$("#avCallContainer").css({ 'width': '100%' });


			}
			$(".userdisconnect").hide();
			if (user == "owner") {
				$("#host_" + userIdglb).css("border", "3px solid #FFCA00");
				$(".userdisconnect").show();
				updateCallStatus(callId, "inprogress");
			}

			$("#" + callId + "mm").find(".videoUser").sort(Ascending_sort).appendTo($("#" + callId + "mm"));
			resizeChatCall();

			$("div[id^=RemoteDiv_]").css("display", "inline-table");
			$("div[id^=RemoteDiv_]").css({ 'display': 'inline-table' });
			audioContext[id] = new AudioContext();
			mediaStreamSource[id] = audioContext[id].createMediaStreamSource(remoteStream[id]);
			processor[id] = audioContext[id].createScriptProcessor(2048, 1, 1);
			mediaStreamSource[id].connect(audioContext[id].destination);
			mediaStreamSource[id].connect(processor[id]);
			processor[id].connect(audioContext[id].destination);
			processor[id].onaudioprocess = function (e) {
				var inputData = e.inputBuffer.getChannelData(0);
				var inputDataLength = inputData.length;
				var total = 0;
				for (var i = 0; i < inputDataLength; i++) {
					total += Math.abs(inputData[i++]);
				}

				var rms = Math.sqrt(total / inputDataLength);
				rms = rms * 100;

				// console.log("rms--"+rms);
				if(rms > 10){
					$('#host_'+id).addClass('call-animation2');
				}
				else{
					$('#host_'+id).removeClass('call-animation2');
				}

				//$('#participants_'+id).find('.meter').css('width',  rms + '%');
				// $('#RemoteDiv_' + id).find('.meter').css('width', rms + '%');
				if (callType == "AV") {
					var remoteid = $('#remoteVideo').attr('videoid');
				} else {
					var remoteid = $('#aRemoteVideo').attr('audioid');
				}
				if (id == remoteid) {
					if(rms > 10){
						$('.callConnectImage').addClass('call-animation2');
					}
					else{
						$('.callConnectImage').removeClass('call-animation2');
					}

					$(".remotevideovoice").find('.meter').css('width', rms + '%');
				}
			};

			/*
			 if(shareScreen == "on" && screenHost == true){
				 
				 var pc = getPeerConnection(id);
				 pc.removeTrack(peerVsender[id]);
				 ssender = pc.addTrack(screenStreamTrack, localStream);
				 peerSsender[id]=ssender;
				 makeOffer(id); 
			 }
			 */

			if (user == "owner") {
				if (queueArray.indexOf(id.toString()) > -1) {
					queueArray.splice(queueArray.indexOf(id.toString()), 1);
					handleQueue();
				}
			}



			/*if(userData.length > 0){
				//console.log("userData---"+userData);
				makeOffer(userData[0]);
				userData.splice(0,1);
				}*/
			if (user == "owner") {
				stanzaofData();
			}

		} else if (callType == "A") {

			//Implementation of Tiles prep
			//console.log("the tiles preparation array is before insert ---- "+tilePreparationArray);
			uploadSpeed();
			whoEverconnected.push(id.toString());
			/*if(user=="owner" && (tilePreparationArray.indexOf(id.toString()) == -1) && (tilePreparationArray.length>1) ){
				   var jsonText = '{"calltype":"'+callType+'","type":"sendall","callid":"'+callId+'" ,"newuseradded":"'+id+'"}';
						   notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
				   serverConnection.send(notify);
			
			}*/

			//console.log("userData--"+userData);
			//console.log("userConnected--"+userConnected);

			if (tilePreparationArray.indexOf(id.toString()) == -1) {
				tilePreparationArray.push(id.toString());
			}
			// console.log("the tiles preparation array is after insert ---- " + tilePreparationArray);
			$("#remote-audio").html("");
			// for (i = 0; i < tilePreparationArray.length; i++) {
			for (i = tilePreparationArray.length - 1; i >= 0; i--) {//edited

				var tileId = tilePreparationArray[i];
				// console.log(tileId);
				var bgdisplay = "block";
				if (userConnected.indexOf(tileId.toString()) > -1) {
					bgdisplay = "none";
				}

				var imgurl = $('div#userId_' + tileId).find('img').attr('data-src');
				var userName = $('div#userId_' + tileId).find('.user_box_name').text();
				var displayprop1 = "none";
				var displayp = "none";
				var dis = "none";
				if (userIdglb == tileId) {
					imgurl = lighttpdpath + "//userimages//" + userIdglb + "." + userImgType;
					userName = userFullname;

					if (user == "owner") {
						dis = "block";
					}
					//alert(dis);
					//	var muteDisplay1="none";

				}
				/// alert(imgurl+"---"+tileId);
				// $("#remote-audio").css("display", "flex");//comment
				//	console.log(createMuteUI);
				//console.log(createMuteUI.indexOf(userConnected[i].toString));

				if (stazaofMute.indexOf(tileId.toString()) > -1) {
					displayprop1 = "block";

				}

				if (aRemoteVideo.srcObject == null || $('#aRemoteVideo').attr('audioId') == tileId) {
					$('#aRemoteVideo').attr('audioId', '' + tileId);
					aRemoteVideo.srcObject = remoteStream[tileId];

					if ($("#remoteAudio_" + tileId).length == 0) {
						var ui = addTiles('1',tileId, bgdisplay, displayprop1, imgurl, userName);
						$("#remote-audio").append(ui);


					
							// '<div class="item" id = "RemoteDiv_' + tileId + '" screenshare="" audiomute="" onhold="" videomute="" style="float: left;">'
							// 	+ '<div id="bgTrans_' + tileId + '" class="" style="z-index: 1;position: absolute;opacity: 0.8;float: left;background-color: grey;height: 137px;display: ' + bgdisplay + ';">'
							// 		+ '<div style="color: white;position: absolute;top: 34%;left: 30%;">Connecting...</div>'

							// 	+ '</div>'
							// 	// + '<div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 178px;position:absolute;z-index:1000">'+
							// 	// 	'<div class="defaultWordBreak" style="white-space:pre;width:147px;float:left;text-align: initial;font-size: 12px; color: lightgrey; margin-top: 2px;">' + '  ' + userName + '</div>'+
							// 	// 	'<div class="" style="width:30px;float:right;display:none"  onclick="userDisconnect(' + tileId + ')" >'+
							// 	// 		'<img src="' + path + '/images/temp/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/>'+
							// 	// 	'</div>'+
							// 	// '</div>'
							// 	+'<video id="remoteAudio_' + tileId + '" ondblclick="enterText(' + tileId + ');" onclick="switchVideo(' + tileId + ')" autoplay  style="cursor:pointer;padding-left:5px;width: 180px;height: 135px ; background: transparent;    border: none;display:none"></video>' +
							// 	'<div  id="aRemoteImage_' + tileId + '" onclick="switchVideo(' + tileId + ')" style="margin-top: 1vh;margin-left:1vh;" class="" align="center">' +
							// 		'<div id="host_' + tileId + '" style="display:' + dis + ';">' +
							// 			'<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  >' +
							// 		'</div>' +
							// 		'<img  title="' + userName + '" src="' + imgurl + '" title="" style="height: 100px;cursor: pointer;border-radius: 50%;width: 100px;min-width: 100px;" onerror="userImageOnErrorReplace(this);" > ' +
							// 	'</div>'+
							// 	'<div style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">' +
							// 		'<div style="padding-top:12px;/*! display: none; */">'+
							// 			'<span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display:' + displayprop1 + ';margin-right: -8px;">'+
							// 				'<img style="width:18px ;   margin-top: 5px;" src="' + path + '/images/temp/mic2.png">'+
							// 			'</span>'+
							// 		'</div>' +
							// 		'<div class="meterGraph" >'+
							// 			'<div class="meter" ></div>'+
							// 		'</div>'+
							// '</div></div>');
					}
					document.getElementById('remoteAudio_' + tileId).srcObject = remoteStream[tileId];
					//for adding uer tiles to audio call
				} else {
					if ($("#remoteAudio_" + tileId).length == 0) {
						var ui = addTiles('1',tileId, bgdisplay, displayprop1, imgurl, userName);
						$("#remote-audio").append(ui);

						
							// '<div class="item" id = "RemoteDiv_' + tileId + '" screenshare="" audiomute="" onhold="" videomute="" style="float: left;">'
							// 	+ '<div id="bgTrans_' + tileId + '" class="" style="z-index: 1;position: absolute;opacity: 0.8;float: left;background-color: grey;height: 137px;display: ' + bgdisplay + ';">'
							// 		+ '<div style="color: white;position: absolute;top: 34%;left: 30%;">Connecting...</div>'
							// 	+ '</div>'

							// 	// + '<div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 178px;position:absolute;z-index:1000">'+
							// 	// 	'<div class="defaultWordBreak" style="white-space:pre;width:147px;float:left;text-align: initial;font-size: 12px; color: lightgrey; margin-top: 2px;">' + '  ' + userName + '</div>'+
							// 	// 	'<div class="" style="width:30px;float:right;display:none"  onclick="userDisconnect(' + tileId + ')" >'+
							// 	// 	'<img src="' + path + '/images/temp/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/>'+
							// 	// '</div>'+
							// 	// +'</div>'
							// 	+'<video id="remoteAudio_' + tileId + '" ondblclick="enterText(' + tileId + ');" onclick="switchVideo(' + tileId + ')"  autoplay  style="cursor:pointer;padding-left:5px;width: 180px;height: 135px ; background: transparent;    border: none;display:none"></video>' +
							// 	'<div  id="aRemoteImage_' + tileId + '" onclick="switchVideo(' + tileId + ')" style="margin-top: 1vh;margin-left:1vh;" class="" align="center">' +
							// 		'<div id="host_' + tileId + '" style="display:none;">' +
							// 			'<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  >' +
							// 		'</div>' +
							// 		'<img  title="' + userName + '" src="' + imgurl + '" title="" style="height: 100px;cursor: pointer;border-radius: 50%;width: 100px;min-width: 100px;" onerror="userImageOnErrorReplace(this);" > ' +
							// 	'</div>'+
							// 	'<div style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">' +
							// 		'<div style="padding-top:12px;/*! display: none; */">'+
							// 			'<span id="raudiotiles_' + tileId + '" title="Audio is muted" style="margin-right: -8px;display:' + displayprop1 + '">'+
							// 				'<img style="margin-top: 5px;width:18px" src="' + path + '/images/temp/mic2.png">'+
							// 			'</span>'+
							// 		'</div>' +
							// 		'<div class="meterGraph" >'+
							// 			'<div class="meter" ></div>'+
							// 		'</div>'+
							// '</div></div>');
					}
					document.getElementById('remoteAudio_' + tileId).srcObject = remoteStream[tileId];
				}
				var pcount = parseInt(tilePreparationArray.length);
				$(".pcnt").html(pcount);
				$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");
				tilesPopup();
			}
			//}

			if (Array.isArray(screenshareArray) && screenshareArray.length) {

				if (screenshareArray[0] != userIdglb) {
					$('#RemoteDiv_' + screenshareArray[0]).attr('screenshare', 'Y');
					$('#sharescreen_'+screenshareArray[0]).show();
					if(callType == 'A'){
						// $("#aRemoteImage_" + screenshareArray[0]).hide();
						// $("#remoteAudio_" + screenshareArray[0]).show();
					}
					if (callType == "AV") {
						// $("#aRemoteImage_" + screenshareArray[0]).hide();
						// $("#remoteAudio_" + screenshareArray[0]).show();
					}
					
					switchVideo(screenshareArray[0]);
				}
			}

			$("#bgTrans_" + userIdglb).hide();
			$("#bgTrans_" + id).hide();
			$("#bgTrans_" + whoAdmin).hide();

			$("#notification").hide();

			/*console.log($(("#RemoteDiv_")+whoAdmin).find("hostimg").length +"-----"+ whoAdmin);
			if($(("#RemoteDiv_")+whoAdmin).find("hostimg").length==0){
				$("#aRemoteImage_"+whoAdmin).prepend(
					'<img class="Owned_by_cLabelTitle shareUserIcon" title="owned by" src="/images/myprojects.png" projecttype="my_project" projectid="6031" id="hostimg" >'
				);
				document.getElementById('remoteAudio_'+whoAdmin).srcObject = remoteStream[whoAdmin];
			}*/
			for (i = 0; i < userConnected.length; i++) {
				//alert(userConnected[i]);
				$("#bgTrans_" + userConnected[i]).hide();
			}
			for (i = 0; i < whoEverconnected.length; i++) {
				//alert(userConnected[i]);
				$("#bgTrans_" + whoEverconnected[i]).hide();
			}

			if (!userConnected.includes(id)) {


				if ((userConnected.indexOf(id)) == -1) {
					userConnected.push(id);
				}
				for (i = 0; i < userConnected.length; i++) {
					//alert(userConnected[i]);
					$("#bgTrans_" + userConnected[i]).hide();
				}
				// resizeAudio();
				if (userConnected.length > 1) {
					$("#notification").hide();
					wbResize();
					var pcount = parseInt(tilePreparationArray.length);
					$(".pcnt").html(pcount);
					$("div[id^=RemoteDiv_]").css("display", "inline-table");
					var rid = $('#aRemoteVideo').attr('audioId');
					// $("div[id^=RemoteDiv_]").css({ 'border': '1px solid #cccccc' });
					// $("div[id^=RemoteDiv_]").css({ 'background': 'rgb(45, 131, 162)' });
					// $("#RemoteDiv_"+rid+"").css({'border':'1px solid red'});
					// $("#RemoteDiv_" + rid + "").css({ 'background': '#0B4860' });
					$("#" + callId + "mm").html("");
					$("#" + callId + "mm").append("<div style='width:100%;display:inline-block;padding-left: 6px;font-family: opensanscondbold;font-size: 15px;color: #747778;padding-top: 1px;padding-bottom: 2px; border-bottom: 1px solid #CED2D5'>Participants: <span id='pcount'>" + pcount + "<span></div>");


					for (i = tilePreparationArray.length - 1; i >= 0; i--) {
						if (tilePreparationArray[i] != userIdglb) {
							var displayprop = "none";
							//console.log(createMuteUI);
							//console.log(createMuteUI.indexOf(userConnected[i].toString));

							if (stazaofMute.indexOf(tilePreparationArray[i].toString()) > -1) {
								displayprop = "block";
							}

							var ssprop = "none";
							if (screenshareArray.indexOf(tilePreparationArray[i]) > -1) {
								ssprop = "block"
							}
							var wbprop = "none";
							if (whiteboardArray.indexOf(tilePreparationArray[i]) > -1) {
								wbprop = "block"
							}


							//console.log(userConnected[i]+"----"+displayprop);
							var imageSrc = $('#userId_' + tilePreparationArray[i]).find('img').attr('data-src');
							var title = $('#userId_' + tilePreparationArray[i]).find('.user_box_name').attr('value');

							if (($("#" + callId + "mm").find("#participants_" + tilePreparationArray[i])).length == 0) {
								$("#" + callId + "mm").append("<div class='videoUser' id='participants_" + tilePreparationArray[i] + "' ondblclick='enterText(" + tilePreparationArray[i] + ");' onclick='switchVideo(" + tilePreparationArray[i] + ")' style='cursor:pointer;width:100%;height:45px;display:inline-block;position:relative'><img title= '" + title + "' style=\"margin-left: 10px;float:left;height:35px;width:35px;margin-top:5px;margin-bottom:5px\" class=\"img-circle rounded-circle\" onerror=\"userImageOnErrorReplace(this);\" src=\"" + imageSrc + "\">"

									+ '<div id="hostp_' + tilePreparationArray[i] + '" style="display:none;position:absolute;left:32px">'
									+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031" style=""  >'
									+ '</div>'


									+ "<div style='margin-top:13px;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;width:60%'>" + title + ""
									+ "<p id='videochat_" + tilePreparationArray[i] + "' style='display:none;color:#08B627;font-size:11px'></p>"

									+ "</div>"
									+ "<div /*style='padding-top:12px'*/>"
									+ "<div style='position:absolute;background-color:#fff;right:0;padding-left:10px'>"
									+ '<span class="userdisconnect"  onclick="userDisconnect(' + tilePreparationArray[i] + ')" id="user_pannel" style="margin-top: 10px;margin-right: 5px;float: right;"><img src="' + path + '/images/cme/phonedis.svg" style="height:24px;width:24px"></span>'

									+ "<span id='sharescreen_" + tilePreparationArray[i] + "' title='Screen share' style='margin-top: 10px;margin-right: 5px;float:right;display:" + ssprop + "'><img style='width:24px' src='" + path + "/images/temp/share.svg' /></span>"
									+ "<span id='whiteboard_" + tilePreparationArray[i] + "' title='White Board' style='margin-top: 10px;margin-right: 5px;float:right;display:" + wbprop + "'><img   style='width:24px' src='" + path + "/images/temp/whiteboard.svg' /></span>"
									+ "<span id='raudiopar_" + tilePreparationArray[i] + "' title='Audio is muted' style='margin-top: 10px;margin-right: 5px;float:right;display:" + displayprop + "'><img style='width:24px' src='" + path + "/images/temp/mic.svg' /></span>"
									+ "</div>"
									+ "</div>"
									+ "</div>"
								);
							}





						}





					}
				} else {

					if (!($('#chatFor').is(':visible'))) {
						chatFor();
					}
				}

			}
			if (user == "owner" && userConnected.length > 1) {

				$("#showhost").show();
			}
			else {
				$("#showhost").hide();
			}
			//console.log("-----"+user+"------"+whoAdmin);
			var imgs = lighttpdpath + "//userimages//" + userIdglb + "." + userImgType;

			//if(userConnected.length>1){
			var dis = "none";
			if (user == "owner") {
				dis = "block";
			}
			var muteDisplay1 = "none";
			if (stazaofMute.indexOf(userIdglb.toString()) > -1) {
				muteDisplay1 = "block"
			}
			//console.log("stazaofMute"+stazaofMute);
			if ($("#remoteAudio_" + userIdglb).length == 0) {
				var ui = addTiles('2', dis, muteDisplay1,'', imgs);
				$("#remote-audio").prepend(ui);

				
				
					// '<div class="item" id = "RemoteDiv_' + userIdglb + '" screenshare="" audiomute="" onhold="" videomute="" style="float: left;">'+
					// 	// '<div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 178px;position:absolute;z-index:1000">'+
					// 	// 	'<div class="defaultWordBreak" style="white-space:pre;width:147px;float:left;text-align: initial;font-size: 12px; color: lightgrey; margin-top: 2px;">' + '  ' + userFullname + '</div>'+
					// 	// 	'<div class="" style="width:30px;float:right;display:none"  onclick="hangup();" >'+
					// 	// 		'<img src="' + path + '/images/temp/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/>'+
					// 	// 	'</div>'+
					// 	// '</div>'
					// 	+'<video id="remoteAudio_' + userIdglb + '" ondblclick="enterText(' + userIdglb + ');" onclick="switchVideo(' + userIdglb + ')" autoplay  style="cursor:pointer;padding-left:5px;width: 180px;height: 135px ; background: transparent;    border: none;display:none"></video>'
					// 	+ '<div  id="aRemoteImage_' + userIdglb + '" onclick="switchVideo(' + userIdglb + ')" style="margin-top: 1vh;margin-left:1vh;" class="" align="center">'
					// 		+ '<div id="hostu_' + userIdglb + '" style="display:' + dis + ';">'
					// 			+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  >'
					// 		+ '</div>'
					// 		+ '<img  title="' + userFullname + '" src="' + imgs + '" title="" style="height: 100px;cursor: pointer;border-radius: 50%;width: 100px;min-width: 100px;" onerror="userImageOnErrorReplace(this);" > '
					// 	+ '</div>'+
					// 	'<div style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">'
					// 		+ '<div style="padding-top:12px;/*! display: none; */">'+
					// 			'<span id="raudiohost_' + userIdglb + '" title="Audio is muted" style="display:' + muteDisplay1 + ';margin-right: 5px; float: right;margin-top: -5px;">'+
					// 				'<img style="    margin-top: 5px;width:18px" src="' + path + '/images/temp/mic2.png">'+
					// 			'</span>'+
					// 		'</div>'
					// 		+ '<div class="meterGraph" ><div class="meter" ></div>'+
					// 		'</div>'+
					// '</div></div>');
			}
			document.getElementById('remoteAudio_' + userIdglb).srcObject = remoteStream[userIdglb];
			var pcount = parseInt(tilePreparationArray.length);
			$(".pcnt").html(pcount);
			$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");




			//}
			if ($("#participants_" + userIdglb).length == 0) {
				var todisplay = "none";
				if (user == "owner") {
					todisplay = "block";
				}
				var prop = "none";
				if (stazaofMute.indexOf(userIdglb.toString()) > -1) {
					prop = "block";
				}
				var ssprop1 = "none";
				if (screenshareArray.indexOf(userIdglb) > -1) {
					ssprop1 = "block"
				}
				var wbprop1 = "none";
				if (whiteboardArray.indexOf(userIdglb) > -1) {
					wbprop1 = "block"
				}
				$("#" + callId + "mm").append("<div class='videoUser' id='participants_" + userIdglb + "' ondblclick='enterText(" + userIdglb + ");' onclick='switchVideo(" + userIdglb + ")' style='cursor:pointer;width:100%;height:45px;display:inline-block;position:relative'><img title= '" + userFullname + "' style=\"margin-left: 10px;float:left;height:35px;width:35px;margin-top:5px;margin-bottom:5px\" class=\"img-circle rounded-circle\" onerror=\"userImageOnErrorReplace(this);\" src=\"" + imgs + "\">"


					+ '<div id="hosta_' + userIdglb + '" style="position:absolute;left:32px;display:' + todisplay + ';" onclick="oncallswitch();">'
					+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031" style="" >'
					+ '</div>'


					+ "<div style='margin-top:13px;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;width:60%'>" + userFullname + ""
					+ "<p id='videochat_" + userIdglb + "' style='display:none;color:#08B627;font-size:11px'></p>"

					+ "</div>"
					+ "<div /*style='padding-top:12px'*/>"
					+ "<div style='position:absolute;background-color:#fff;right:0;padding-left:10px'>"
					//+'<span id="raudiohostpar_'+userIdglb+'" title="Audio is muted" style="display:none;margin-right: 5px;"><img src="'+path+'/images/temp/mic_2.png"></span>'	
					+ '<span class="userdisconnect1" onclick="hangup();" id="user_pannel" style="margin-top: 10px;margin-right: 5px;float: right;"><img src="' + path + '/images/cme/phonedis.svg" style="height:24px;width:24px"></span>'
					+ "<span id='whiteboardu_" + userIdglb + "' title='White Board' style='margin-top: 10px;margin-right: 5px;float:right;display:" + wbprop1 + "'><img style='width:24px' src='" + path + "/images/temp/whiteboard.svg' /></span>"
					+ "<span id='sharescreenu_" + userIdglb + "' title='Screen share' style='margin-top: 10px;margin-right: 5px;float:right;display:" + ssprop1 + "'><img style='width:24px' src='" + path + "/images/temp/share.svg' /></span>"
					+ '<span id="raudiohostpar_' + userIdglb + '" title="Audio is muted" style="margin-top: 10px;margin-right: 5px;display:' + prop + ';float:right"><img style="width:24px" src="' + path + '/images/temp/mic.svg"></span>'
					+ "</div>"
					+ "</div>"
					+ "</div>"
				);
			}




			tilesPopup();


			if (user != "owner") {
				//console.log("whoadmin-->"+whoAdmin);
				$("div[id^=host_]").css("border", "none");
				$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");
				$("div[id^=hostp_]").css("display", "none");
				$("#hostp_" + whoAdmin).css("display", "block");
				$("div[id^=hosta_]").css("display", "none");
				$("#hosta_" + whoAdmin).css("display", "block");
				$("div[id^=hostu_]").css("border", "none");
				$("#hostu_" + whoAdmin).css("border", "3px solid #FFCA00");
				$("div[id^=hostb_]").css("display", "none");
				$("#hostb_" + whoAdmin).css("display", "block");

			}
			$(".userdisconnect").hide();
			if (user == "owner") {
				$("#host_" + userIdglb).css("border", "3px solid #FFCA00");
				$(".userdisconnect").show();
				updateCallStatus(callId, "inprogress");
			}






			if (foraudiocall == 0) {
				foraudiocall++;
				$("#chatFor").toggle();
				$("#chatFor").toggle();
				$("#chatFor").hide();

				$("#audioFor").find(".arrow").css("left", "-2px");
				$("#audioFor").find(".arr").attr('src', path + '/images/temp/slideboxright.png');
				$("#audioFor").find(".arrow").css("left", "-2px");
				$("#audioFor").find(".arr").css("width", "41px");
				$("#avCallContainer").css({ 'width': '100%' });


				/*	if(userConnected.length<2){
						checkChatWindowSizeForCall();
					}else{
							
					}*/


			}
			console.log("userconnected--->"+userConnected);
			for (i = 0; i < userConnected.length; i++) {
				//alert(userConnected[i]);
				$("#bgTrans_" + userConnected[i]).hide();
			}
			$("div[id^=RemoteDiv_]").css("display", "inline-table");
			audioContext[id] = new AudioContext();
			mediaStreamSource[id] = audioContext[id].createMediaStreamSource(remoteStream[id]);
			processor[id] = audioContext[id].createScriptProcessor(2048, 1, 1);
			mediaStreamSource[id].connect(audioContext[id].destination);
			mediaStreamSource[id].connect(processor[id]);
			processor[id].connect(audioContext[id].destination);
			processor[id].onaudioprocess = function (e) {
				var inputData = e.inputBuffer.getChannelData(0);
				var inputDataLength = inputData.length;
				// console.log("inputDataLength--"+inputDataLength+"id--"+id);
				var total = 0;
				for (var i = 0; i < inputDataLength; i++) {
					total += Math.abs(inputData[i++]);
					// console.log("total--"+total);
				}

				var rms = Math.sqrt(total / inputDataLength);
				rms = rms * 100;
				// console.log("rms--"+rms);
				if(rms > 10){
					$('#host_'+id).addClass('call-animation2');
				}
				else{
					$('#host_'+id).removeClass('call-animation2');
				}
				//$('#participants_'+id).find('.meter').css('width',  rms + '%');
				$('#RemoteDiv_' + id).find('.meter').css('width', rms + '%');
				if (callType == "AV") {
					var remoteid = $('#remoteVideo').attr('videoid');
				} else {
					var remoteid = $('#aRemoteVideo').attr('audioid');
				}


				if (id == remoteid) {
					if(rms > 10){
						$('.callConnectImage').addClass('call-animation2');
					}
					else{
						$('.callConnectImage').removeClass('call-animation2');
					}

					$(".remotevideovoice").find('.meter').css('width', rms + '%');
				}
			};

			/*
			if(shareScreen == "on" && screenHost == true){
				
				 var pc = getPeerConnection(id);
				 ssender = pc.addTrack(screenStreamTrack, localStream);
				 peerSsender[id]=ssender;
				 makeOffer(id); 
				 var jsonText = '{"calltype":"'+callType+'","type":"screenshare","status":"'+shareScreen+'","callid":"'+callId+'"}';
					 notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
					 serverConnection.send(notify);
			 }
		   */
			$("#" + callId + "mm").find(".videoUser").sort(Ascending_sort).appendTo($("#" + callId + "mm"));
			resizeChatCall();
			//   autogrowParticipants();

			// $("#"+callId+"mm").css("overflow" ,"auto")

			$("#" + callId + "mm").parent().css({ 'overflow': 'auto' }, { 'resize': 'vertical' }, { 'min-height': '35.2vh' }, { "max-height": "70vh" });
			if (user == "owner") {
				if (queueArray.indexOf(id.toString()) > -1) {
					queueArray.splice(queueArray.indexOf(id.toString()), 1);
					handleQueue();
				}
			}


			/* if(userData.length > 0){
				 makeOffer(userData[0]);
				 userData.splice(0,1);
				 }*/
			if (user == "owner") {
				stanzaofData();
			}
		}
		if (user == "owner") {
			$(".userdisconnect").show();
		}
		if (callType == "AV") {
			var windowWidthVideo = document.getElementById("local-video1").clientWidth;
		} else {
			var windowWidthVideo = document.getElementById("remote-audio").clientWidth;
		}

		/* if(windowWidthVideo > 911){
			$("div[id^=RemoteDiv_]").show();
		} */

		$("div[id^=RemoteDiv_]").show();
		if (windowWidthVideo < 364) {

			var dataIndex = $(".item:visible:first").index() + 1;
			var endIndex = $(".item:visible:first").index() + 2;


		} else if (windowWidthVideo > 364 && windowWidthVideo < 547) {
			var dataIndex = $(".item:visible:first").index() + 1;
			var endIndex = $(".item:visible:first").index() + 3;
		} else if (windowWidthVideo > 547 && windowWidthVideo < 729) {
			var dataIndex = $(".item:visible:first").index() + 1;
			var endIndex = $(".item:visible:first").index() + 4;
		} else if (windowWidthVideo > 729 && windowWidthVideo < 911) {
			var dataIndex = $(".item:visible:first").index() + 1;
			var endIndex = $(".item:visible:first").index() + 5;
		} else if (windowWidthVideo > 911 && windowWidthVideo < 1093) {
			var dataIndex = $(".item:visible:first").index() + 1;
			var endIndex = $(".item:visible:first").index() + 6;
		} else if (windowWidthVideo > 1094 && windowWidthVideo < 1276) {
			var dataIndex = $(".item:visible:first").index() + 1;
			var endIndex = $(".item:visible:first").index() + 7;
		} else if (windowWidthVideo > 1094 && windowWidthVideo < 1276) {
			var dataIndex = $(".item:visible:first").index() + 1;
			var endIndex = $(".item:visible:first").index() + 8;
		} else if (windowWidthVideo > 1276) {
			var dataIndex = $(".item:visible:first").index() + 1;
			var endIndex = $(".item:visible:first").index() + 10;
		}
		/*for(var i= dataIndex;i<endIndex; i++){
			 $( ".item" ).eq(i).show();
		}*/
		resizeVideo();
		initDurationTimer();

	}
	return pc;
}



function getDataChannel(id) {
	console.log('inside get peer chanel---'+ id);
	if (dataChannel[id]) {

		return dataChannel[id];
	}

	dataConstraint = null;
	var pc = getPeerConnection(id);
	var sendChannel = pc.createDataChannel('sendDataChannel', dataConstraint);
	dataChannel[id] = sendChannel;
	dataChannel[id].onopen = onSendChannelStateChange;
	dataChannel[id].onclose = onSendChannelStateChange;
	console.log("finished--"+dataChannel[id].readyState);
}

function makeOffer(id) {
	//console.log('inside make offer---'+ id);
	var pc = getPeerConnection(id);
	pc.createOffer(function (sdp) {

		pc.setLocalDescription(sdp);
		sdp.sdp = setMediaBitrates(sdp.sdp);
		// console.log(sdp.sdp)
		// console.log('Creating an offer for---'+ id);
		var body = JSON.stringify({ "calltype": callType, "type": "offer", "sdp": sdp, "callid": callId });
		var message = $msg({ to: id + '@' + cmeDomain + '.colabus.com', "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(body);
		Gab.connection.send(message);
	}, function (e) {
		console.log(e);
	},
		{ mandatory: { OfferToReceiveVideo: true, OfferToReceiveAudio: true } });
}








function chatAlertFun(info, type) {
	$('#chatAlertDiv').find("#confirmContent").text(info);
	$('#chatAlertOverlay').show();
	$('#chatAlertDiv').show();
}

function chatAlertClose() {
	$('#chatAlertOverlay').hide();
	$('#chatAlertDiv').hide();
	$('#chatAlertDiv').find("#confirmContent").text("");
}

function muteAudio(obj) {

	// if ((callType == "A") && ($("#astreamImg").attr('src').indexOf('vpause.png') == -1)) { //--- this is to check call is in Hold or not. If hold then ignoring other options like mute audio, mute video
	// 	return false;
	// } else if ((callType == "AV") && ($("#vstreamImg").attr('src').indexOf('vpause.png') == -1)) {
	// 	return false;
	// }
	if(cmecallhold == ''){//--- this is to check call is in Hold or not. If hold then ignoring other options like mute audio, mute video
		if ($(obj).attr('src').indexOf('mute1.svg') != -1) {
			cmemuteaudio = '1';
			$(obj).attr('src', path + '/images/cme/' + $(obj).attr('toglsrc'));
			localStream.getAudioTracks()[0].enabled = false;
			var jsonText = '{"calltype":"' + callType + '","type":"audiostatus","status":"off","callid":"' + callId + '"}';
			notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
			serverConnection.send(notify);
			muteArray.push(userIdglb.toString());
			$("#raudiohostpar_" + userIdglb).show();
			$("#raudiohost_" + userIdglb).show();//added
			$("#raudiotiles_" + userIdglb).show();
			$('#RemoteDiv_' + userIdglb).attr('audiomute', 'Y');
			stazaofMute.push(userIdglb.toString());
			if($('.callConnectImage').attr('title') == userFullname){
				$('#mutecheck').css('display', '');
			}



		} else {
			cmemuteaudio = '';
			$(obj).attr('src', path + '/images/cme/' + $(obj).attr('defsrc'));
			localStream.getAudioTracks()[0].enabled = true;
			var jsonText = '{"calltype":"' + callType + '","type":"audiostatus","status":"on","callid":"' + callId + '"}';
			notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
			serverConnection.send(notify);
			var idx = muteArray.indexOf(userIdglb.toString());
			if (idx > -1) {
				muteArray.splice(idx, 1);
			}

			var idx_of = stazaofMute.indexOf(userIdglb.toString());
			if (idx_of > -1) {
				stazaofMute.splice(idx_of, 1);
			}
			$("#raudiohostpar_" + userIdglb).hide();
			$("#raudiohost_" + userIdglb).hide();//added
			$("#raudiotiles_" + userIdglb).hide();
			$('#RemoteDiv_' + userIdglb).attr('audiomute', '');
			if($('.callConnectImage').attr('title') == userFullname){
				$('#mutecheck').css('display', 'none');
			}

		}
	}
}

function muteVideo(obj) {
	if ($("#vstreamImg").attr('src').indexOf('vpause.svg') == -1) {
		return false;  //--- this is to check call is in Hold or not. If hold then ignoring other options like mute audio, mute video
	}

	if ($(obj).attr('src').indexOf('mute1.svg') != -1) {
		$(obj).attr('src', path + '/images/cme/' + $(obj).attr('toglsrc'));
		localStream.getVideoTracks()[0].enabled = false;
		var jsonText = '{"calltype":"' + callType + '","type":"videostatus","status":"off","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
	} else {
		$(obj).attr('src', path + '/images/cme/' + $(obj).attr('defsrc'));
		localStream.getVideoTracks()[0].enabled = true;
		var jsonText = '{"calltype":"' + callType + '","type":"videostatus","status":"on","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
	}

}

function pauseVideo(obj) { //--- for video call
	if ($(obj).attr('src').indexOf('vpause.png') != -1) {
		$(obj).attr('src', path + '/images/cme/' + $(obj).attr('toglsrc'));

		//-- If Hold then setting default status & disabling mute audio and mute video options
		$('#vaudioImg').attr('src', path + '/images/temp/' + $('#vaudioImg').attr('defsrc'));
		$('#vmuteImg').attr('src', path + '/images/temp/' + $('#vmuteImg').attr('defsrc'));
		$('#vaudioImg,#vmuteImg').css({ 'cursor': 'default', 'opacity': '0.5' });

		localStream.getAudioTracks()[0].enabled = false;
		localStream.getVideoTracks()[0].enabled = false;
		//remoteStream.getAudioTracks()[0].enabled = false;
		//remoteStream.getVideoTracks()[0].enabled = false;
		var jsonText = '{"calltype":"' + callType + '","type":"streamstatus","status":"off","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
	} else {

		$(obj).attr('src', path + '/images/cme/' + $(obj).attr('defsrc'));

		//-- If Play then enabling mute audio and mute video options
		$('#vaudioImg,#vmuteImg').css({ 'cursor': 'pointer', 'opacity': '1' });

		localStream.getAudioTracks()[0].enabled = true;
		localStream.getVideoTracks()[0].enabled = true;
		//remoteStream.getAudioTracks()[0].enabled = true;
		//remoteStream.getVideoTracks()[0].enabled = true;
		var jsonText = '{"calltype":"' + callType + '","type":"streamstatus","status":"on","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);

	}
}

function pauseAudio(obj) { //-- for audio call
	if ($(obj).attr('src').indexOf('vpause.svg') != -1) {
		cmecallhold = '1';
		$(obj).attr('src', path + '/images/cme/' + $(obj).attr('toglsrc'));

		//-- If Hold then setting default status & disabling mute audio options
		$('#aaudioImg').attr('src', path + '/images/cme/' + $('#aaudioImg').attr('defsrc')).css({ 'cursor': 'default', 'opacity': '0.5' });

		localStream.getAudioTracks()[0].enabled = false;
		//remoteStream.getAudioTracks()[0].enabled = false;
		var jsonText = '{"calltype":"' + callType + '","type":"streamstatus","status":"off","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
	} else {
		cmecallhold = '';
		$(obj).attr('src', path + '/images/cme/' + $(obj).attr('defsrc'));

		//-- If Play then enabling mute audio options
		$('#aaudioImg').css({ 'cursor': 'pointer', 'opacity': '1' });

		localStream.getAudioTracks()[0].enabled = true;
		//remoteStream.getAudioTracks()[0].enabled = true;
		var jsonText = '{"calltype":"' + callType + '","type":"streamstatus","status":"on","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
	}
}

function gotVideoRoomMsg(message, id) {

	var callsignal = JSON.parse($(message).children('json').text());
	//console.log(callsignal);
	//console.log("inside gotVideoRoomMsg  -- callsignal"+callsignal.type+"--callsignal.status--"+callsignal.status);


	if (callsignal.type == "hostswitch") {
		//console.log("enter");
		var hId = callsignal.hostid;
		var uid = callsignal.uid;
		$("#host_" + uid).css("border", "none");
		//alert(hId);
		if (hId == userIdglb) {
			chatAlertFun('You are now  host of the call.', 'warning');
			user = "owner";
			$(".addnewuser").show();
			$(".userdisconnect").show();
			var hName = userFullname;
	
			// if($('.callConnectImage').attr('title') == uName){
			// 	$('.callConnectImage').css("border", "none");
			// }
			if($('.callConnectImage').attr('title') == hName){
				$('.callConnectImage').css("border", "4px solid #FFCA00");
			}
			else{
				$('.callConnectImage').css("border", "none");
			}
			whoAdmin = hId;
			$("div[id^=hosta_]").css("display", "none");
			$("#hosta_" + hId).css("display", "block");
			$("div[id^=host_]").css("border", "none");
			$("#host_" + hId).css("border", "3px solid #FFCA00");
			$("div[id^=hostp_]").css("display", "none");
			$("#hostp_" + hId).css("display", "block");
			$("div[id^=hostu_]").css("border", "none");
			$("#hostu_" + hId).css("border", "3px solid #FFCA00");
			$("div[id^=hostb_]").css("display", "none");
			$("#hostb_" + hId).css("display", "block");

		}
		else {
			var hName = $('div#userId_' + hId).find('.user_box_name').text();
	
			// if($('.callConnectImage').attr('title') == uName){
			// 	$('.callConnectImage').css("border", "none");
			// }
			if($('.callConnectImage').attr('title') == hName){
				$('.callConnectImage').css("border", "4px solid #FFCA00");
			}
			else{
				$('.callConnectImage').css("border", "none");
			}
			whoAdmin = hId;
			$("div[id^=host_]").css("border", "none");
			$("#host_" + hId).css("border", "3px solid #FFCA00");
			$("div[id^=hostp_]").css("display", "none");
			$("#hostp_" + hId).css("display", "block");
			$("div[id^=hosta_]").css("display", "none");
			$("#hosta_" + hId).css("display", "block");
			$("div[id^=hostu_]").css("border", "none");
			$("#hostu_" + hId).css("border", "3px solid #FFCA00");
			$("div[id^=hostb_]").css("display", "none");
			$("#hostb_" + hId).css("display", "block");
		}
		disableEnableWBoptions();
	}



	if (callsignal.type == 'audiostatus') {
		//console.log("*** audio status from other peer");
		var uName = $('div#userId_' + id).find('.user_box_name').text();
		if (callsignal.status == 'off') {
			// Here you write code for video tiles
			$('#RemoteDiv_' + id).attr('audiomute', 'Y');//audiomute="" onhold="" videomute="" 
			if($('.callConnectImage').attr('title') == uName){
				$('#mutecheck').css('display', '');
			}
			$('#raudio_' + id).show();
			$('#raudiof_' + id).show();
			$('#raudiov_' + id).show();
			$("#raudiopar_" + id).show();
			$("#raudiotiles_" + id).show();
			if ($('#remoteVideo').attr('videoid') == id || $('#aRemoteVideo').attr('audioId') == id) {
				$('#avStatusDiv').find('span#aStatusSpan').addClass("avStatusSpan").text("On mute");
			}
			if (user == "owner") {
				muteArray.push(id.toString());
			}
			stazaofMute.push(id.toString());
		} else {
			$('#RemoteDiv_' + id).attr('audiomute', '');
			if($('.callConnectImage').attr('title') == uName){
				$('#mutecheck').css('display', 'none');
			}
			$('#raudio_' + id).hide();
			$('#raudiof_' + id).hide();
			$('#raudiov_' + id).hide();
			$("#raudiopar_" + id).hide();
			$("#raudiotiles_" + id).hide();
			if ($('#remoteVideo').attr('videoid') == id || $('#aRemoteVideo').attr('audioId') == id) {

				$('#avStatusDiv').find('span#aStatusSpan').removeClass("avStatusSpan").text("");
			}
			var idx = muteArray.indexOf(id.toString());
			if (idx > -1) {
				muteArray.splice(idx, 1);
			}
			var idx_of = stazaofMute.indexOf(id.toString());
			if (idx_of > -1) {
				stazaofMute.splice(idx_of, 1);
			}
		}
	} else if (callsignal.type == 'streamstatus') {
		//console.log("*** stream status from other peer");
		if (callsignal.status == 'off') {
			// Here you write code for video tiles
			$('#RemoteDiv_' + id).attr('onhold', 'Y');

			if ($('#remoteVideo').attr('videoid') == id || $('#aRemoteVideo').attr('audioId') == id) {
				$('#avStatusDiv').find('span#sStatusSpan').addClass("avStatusSpan").text("On hold");
				$('#avStatusDiv').find('span#vStatusSpan,span#aStatusSpan').removeClass("avStatusSpan").text("");
			}

			localStream.getAudioTracks()[0].enabled = false;
			if (callType == "AV") {
				localStream.getVideoTracks()[0].enabled = false;
			}
		} else {
			// code for video tiles
			$('#RemoteDiv_' + id).attr('onhold', '');

			if ($('#remoteVideo').attr('videoid') == id || $('#aRemoteVideo').attr('audioId') == id) {
				$('#avStatusDiv').find('span#sStatusSpan').removeClass("avStatusSpan").text("");
			}

			localStream.getAudioTracks()[0].enabled = true;
			if (callType == "AV") {
				localStream.getVideoTracks()[0].enabled = true;
			}
		}
	} else if (callsignal.type == 'videostatus') {
		//console.log("*** video mute status from other peer");
		if (callsignal.status == 'off') {

			$('#RemoteDiv_' + id).attr('videomute', 'Y');
			$('#rvideo_' + id).show();
			if ($('#remoteVideo').attr('videoid') == id) {
				$('#avStatusDiv').find('span#vStatusSpan').addClass("avStatusSpan").text("Video paused");
			}
		} else {
			$('#RemoteDiv_' + id).attr('videomute', '');
			$('#rvideo_' + id).hide();
			if ($('#remoteVideo').attr('videoid') == id) {
				$('#avStatusDiv').find('span#vStatusSpan').removeClass("avStatusSpan").text("");
			}
		}

	} else if (callsignal.type == 'screenshare') {
		//  alert("---here");
		if (callsignal.status == 'on') {
			shareScreen = "on";
			if (callType == "A") {
				$('#RemoteDiv_' + id).attr('screenshare', 'Y');
				$('#screenshare_'+id).show();
				//alert($('#RemoteDiv_'+id).attr('screenshare'));
				//if($('#aRemoteVideo').attr('audioId') != id){

				// $("#aRemoteImage_" + id).hide();
				// $("#remoteAudio_" + id).show();

				//}
				if ($('#aRemoteVideo').attr('audioId') != id) {
					$("#aRemoteImage").show();
					$("#aRemoteVideo").hide();
				} else {
					$("#aRemoteImage").hide();
					$("#aRemoteVideo").show();
				}
			}
			switchVideo(id);
		} else if (callsignal.status == 'off') {
			shareScreen = "off";
			if (callType == "A") {
				$('#RemoteDiv_' + id).attr('screenshare', 'N');
				$('#screenshare_'+id).hide();
				//if($('#aRemoteVideo').attr('audioId') != id){
				$("#remoteAudio_" + id).hide();
				$("#aRemoteImage_" + id).show();
				//}
				switchVideo(id);
				$("#aRemoteVideo").hide();
				$("#aRemoteImage").show();
			}
		}
	} else if (callsignal.type == 'streamsg') {
		//  alert("stream....");
		if (callType == "A") {
			if (callsignal.status == 'off') {
				$('.ascreenshareImg').removeAttr('onclick')
					.css({ 'cursor': 'default', 'opacity': '0.5' })
					.attr('title', 'The other user is already sharing the screen.');
				if (screenshareArray.indexOf(id.toString()) == -1) {
					screenshareArray.push(id.toString());
				}
				$("div[id^=sharescreen_]").css("display", "none");
				$("div[id^=sharescreenu_]").css("display", "none");
				$("#sharescreen_" + id).show();

			} else {
				$('.ascreenshareImg').attr('onclick', 'ChangeStream("#ascreenshareImg")')
					.css({ 'cursor': 'pointer', 'opacity': '1' })
					.attr('title', 'Screen Share');
				var idx_ss = screenshareArray.indexOf(id.toString());
				if (idx_ss > -1) {
					screenshareArray.splice(idx_ss, 1)
				}
				$("#sharescreen_" + id).hide();

			}
		} else if (callType == "AV") {
			if (callsignal.status == 'off') {
				$('#vscreenshareImg').removeAttr('onclick')
					.css({ 'cursor': 'default', 'opacity': '0.5' })
					.attr('title', 'The other user is already sharing the screen.');
				if (screenshareArray.indexOf(id.toString()) == -1) {
					screenshareArray.push(id.toString());
				}
				$("div[id^=sharescreen_]").css("display", "none");
				$("div[id^=sharescreenu_]").css("display", "none");
				$("#sharescreen_" + id).show();
			} else {
				//screenshareArray.push(id.toString());
				$('#vscreenshareImg').attr('onclick', 'ChangeStream("#vscreenshareImg")')
					.css({ 'cursor': 'pointer', 'opacity': '1' })
					.attr('title', 'Screen Share');
				var idx_ss = screenshareArray.indexOf(id.toString());
				if (idx_ss > -1) {
					screenshareArray.splice(idx_ss, 1);
				}
				$("#sharescreen_" + id).hide();

			}
		}


	} else if (callsignal.type == 'videohighlight') {
		//console.log("inside highlight");
		//toggleWhiteBoard();
		// if ($("#cmePopupTS").is(":visible") == true) {
		// 	$("#cmePopupTS").hide();
		// }
		if ($("#hListDiv").is(":visible") == false) {
			// console.log("inside highlightFor");
			highlightVideo();

			// if (callsignal.status == 'new' || callsignal.status == 'edit') {
			// 	getHighlight("", "", callsignal.hId);
			// } else if (callsignal.status == 'delete') {
			// 	$("#hListDiv").find('#hl_' + callsignal.hId).remove();
			// } else if (callsignal.status == 'task') {
			// 	$('#hl_' + callsignal.hId).find('img.hTaskIcon').attr('title', 'View Task').attr('src', path + '/images/temp/htask2.png').attr('onclick', 'fetchHighlightTaskDetailsView(' + callsignal.hId + ')');
			// }
		} 
		// else {
		// 	checkHighlightWindowSize();
			if (callsignal.status == 'new' || callsignal.status == 'edit') {
				getHighlight("", "", callsignal.hId);
			} else if (callsignal.status == 'delete') {
				$("#hListDiv").find('#hl_' + callsignal.hId).remove();
			} else if (callsignal.status == 'task') {
				$('#hl_' + callsignal.hId).find('img.hTaskIcon').attr('title', 'View Task').attr('src', path + '/images/temp/htask2.png').attr('onclick', 'fetchHighlightTaskDetailsView(' + callsignal.hId + ')');
			}
		// }
		

	} else if (callsignal.type == 'whiteboard') {
		if (callsignal.status == 'on') {
			whiteboardArray.push(id.toString());
			cmeCallExpandChatFor();
			$("#whiteboard_" + id).show();
			disableEnableWBoptions();
			setWhiteBoard("reciever");
		} else if (callsignal.status == 'ready') {
			$("#wbLoader").hide();
			$("#whiteboardFor").children("iframe").contents().find("#tool-box").show();
		} else if (callsignal.status == 'off') {
			$("#whiteboard_" + id).hide();
			$("#whiteboardu_" + userIdglb).hide();
			var idx_wb = whiteboardArray.indexOf(id.toString());
			if (idx_wb > -1) {
				whiteboardArray.splice(idx_wb, 1);
			}
			hideWhiteBoard();
		} else if (callsignal.status == 'image') {
			var wbImgSrc = callsignal.src;
			var wbImgName = callsignal.filename;
			var file_id = wbImgName.split('.')[0];
			var file_ext = wbImgName.split('.')[1];
			prepareWBImageList(wbImgSrc,file_id,file_ext);
		} 
	} else if (callsignal.type == 'videochat') {
		toggleWhiteBoard();
		if ($("#cmecallcontainer3").is(":visible") == false) {
			cmeCallExpandChatFor();
			chatInCall();
		} else {
			checkChatWindowSize();
		}
		videoChatUI(message);
	} else if (callsignal.type == 'aVTextTranscript') {
		glbRecResult = callsignal.transText;
		userFullName = callsignal.uFirstName;
		tranTimer = callsignal.tTimer;
		transDocId = callsignal.docId;
		// console.log("TextResult and FullName============>"+glbRecResult+" UserName "+userFullName+"Timer-->"+tranTimer);
		showTranscript(glbRecResult, userFullName, tranTimer);

	} else if (callsignal.type == 'audioVideoTranscript') {
		transDocId = callsignal.docId;
		$('#ascreenshareImg').attr('onclick', 'ChangeStream("#ascreenshareImg")')
			.css({ 'cursor': 'pointer', 'opacity': '1' })
			.attr('title', 'Screen Share');
		// console.log("transDocId========"+transDocId);
	} else if (callsignal.type == 'calldisconnect') {
		var recieverName = $('div#userId_' + id).find('.user_box_name').text();
		var ix = switchAdminArray.indexOf(id.toString());
		if (ix > -1) {
			switchAdminArray.splice(ix, 1);
		}
		callDisconnectAccepted(callsignal.type, recieverName, id);
		/* muteArray=[];
		 createMuteUI=[];
		 stazaofMute=[];
		 screenshareArray=[];
		 whiteboardArray=[];
		 foraudiocall=0;
		 forvideocall=0
		 $(".arr").attr('src',path+'/images/temp/slideboxright.png');
		 $(".arrow").css("left","-37px");*/
		var idx_of = stazaofMute.indexOf(id.toString());
		if (idx_of > -1) {
			stazaofMute.splice(idx_of, 1);
		}

		var idx_ss = screenshareArray.indexOf(id.toString());
		if (idx_ss > -1) {
			screenshareArray.splice(idx_ss, 1)
		}


	} else if (callsignal.type == 'removeRemoteUser') {
		var removeUserId = callsignal.removeUserId;
		var recieverName = $('div#userId_' + removeUserId).find('.user_box_name').text();
		if (userIdglb != removeUserId) {
			callDisconnectAccepted(callsignal.type, recieverName, removeUserId);
		}
		if (callType == "AV") {
			if (userConnected.length < 2) {
				//$("#local-video").css("display","block");
				document.getElementById('localVideo').srcObject = localStream;
				document.getElementById('remoteVideo').srcObject = remoteStream[userConnected[0]];
			}

		}
		var idx_of = stazaofMute.indexOf(removeUserId.toString());
		if (idx_of > -1) {
			stazaofMute.splice(idx_of, 1);
		}

		var idx_ss = screenshareArray.indexOf(removeUserId.toString());
		if (idx_ss > -1) {
			screenshareArray.splice(idx_ss, 1)
		}
		// autogrowParticipants();

	} else if (callsignal.type == 'getSpeedFromConnectedUser') {
		sendClientSpeedFromConnectedUser(id, callsignal.calltype);
	} else if (callsignal.type == 'hostdisconnect') {
		hangupFromRemote();
		muteArray = [];
		createMuteUI = [];
		stazaofMute = [];
		foraudiocall = 0;
		forvideocall = 0
		whiteboardArray = [];
		screenshareArray = [];
		$(".arr").attr('src', path + '/images/temp/slideboxright.png');
		// $(".arrow").css("left","-37px");
	}
	else if (callsignal.type == "stanzaofData") {

		// console.log(callsignal.stazaofMute);
		/*var a=callsignal.stazaofMute;
		stazaofMute=a.split(",");
		$("div[id^=raudiopar_]").css("display","none");
		$("div[id^=raudiotiles_]").css("display","none");
		for(i=0;i<stanzaofMute.length;i++){
			$("#raudiopar_"+stanzaofMute[i]).show();
			$("#raudiotiles_"+stanzaofMute[i]).show();
		}*/
		//alert(callsignal);
		var a = callsignal.stazaofMute;
		$("div[id^=raudiopar_]").css("display", "none");
		$("div[id^=raudiotiles_]").css("display", "none");
		if (typeof a != "undefined" && a != "" && a != null && a != "null") {
			stazaofMute = a.split(",");
			console.log(stazaofMute+"---"+stazaofMute.length);
			for (i = 0; i < stazaofMute.length; i++) {
				$("#raudiopar_" + stazaofMute[i]).show();
				$("#raudiotiles_" + stazaofMute[i]).show();
			}

		}
		var wb = callsignal.whiteboard;

		$("div[id^=whiteboard_]").css("display", "none");

		if (typeof wb != "undefined" && wb != "" && wb != null && wb != "null") {
			whiteboardArray = wb.split(",");
			$("#whiteboard_" + whiteboardArray[0]).show();
		}


		var ss = callsignal.screen;

		$("div[id^=sharescreen_]").css("display", "none");

		if (typeof ss != "undefined" && ss != "" && ss != null && ss != "null") {
			screenshareArray = ss.split(",");
			$("div[id^=sharescreen_]").css("display", "none");
			$("div[id^=sharescreenu_]").css("display", "none");
			$("#sharescreen_" + screenshareArray[0]).show();
			shareScreen = "on";
			/*  var Uid=screenshareArray[0];
				  if(callType=="A"){
					  $('#RemoteDiv_'+Uid).attr('screenshare','Y');
					  //if($('#aRemoteVideo').attr('audioId') != id){
						  $("#aRemoteImage_"+Uid).hide();
						  $("#remoteAudio_"+Uid).show();
					  //}
						  if($('#aRemoteVideo').attr('audioId') != Uid){
							  $("#aRemoteImage").show();
							  $("#aRemoteVideo").hide();
						  }else{
							  $("#aRemoteImage").hide();
							  $("#aRemoteVideo").show();
						  }
				   } 
				   switchVideo(Uid);*/
		}




		/*		shareScreen="on";
				var Uid="1433";
						if(callType=="A"){
							$('#RemoteDiv_'+Uid).attr('screenshare','Y');
							//if($('#aRemoteVideo').attr('audioId') != id){
								$("#aRemoteImage_"+Uid).hide();
								$("#remoteAudio_"+Uid).show();
							//}
								if($('#aRemoteVideo').attr('audioId') != Uid){
									$("#aRemoteImage").show();
									$("#aRemoteVideo").hide();
								}else{
									$("#aRemoteImage").hide();
									$("#aRemoteVideo").show();
								}
						 } 
						 switchVideo(Uid);*/

		//console.log(stazaofMute+"---"+stazaofMute.length+"@@@@");
	}
	else if (callsignal.type == "connecting") {
		//alert("here");
		showConnecting(callsignal.newuser, user);
		var cType = callsignal.calltype;

		if (cType == "A") {
			// alert(cType);
			if ($("#remote-audio").find("#RemoteDiv_" + callsignal.newuser).length == 0) {
				var tileId = callsignal.newuser
				var imgurl = $('div#userId_' + tileId).find('img').attr('data-src');
				var userName = $('div#userId_' + tileId).find('.user_box_name').text();
				var ui = addTiles('', tileId,'','', imgurl, userName);
				$("#remote-audio").prepend(ui);

				
				
					// '<div class="item" id = "RemoteDiv_' + tileId + '" screenshare="" audiomute="" onhold="" videomute="" style="float: left;">'
					// 	+ '<div id="bgTrans_' + tileId + '" class="" style="z-index: 1;position: absolute;opacity: 0.8;float: left;background-color: grey;height: 137px;display: block;">'
					// 		+ '<div style="color: white;position: absolute;top: 34%;left: 30%;">Connecting...</div>'
					// 	+ '</div>'
					// 	// + '<div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 178px;position:absolute;z-index:1000">'+
					// 	// 	'<div class="defaultWordBreak" style="white-space:pre;width:147px;float:left;text-align: initial;font-size: 12px; color: lightgrey; margin-top: 2px;">' + '  ' + userName + '</div>'+
					// 	// 	'<div class="" style="width:30px;float:right;display:none"  onclick="userDisconnect(' + tileId + ')" >'+
					// 	// 		'<img src="' + path + '/images/temp/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/>'+
					// 	// 	'</div>'+
					// 	// '</div>'
					// 	+'<video id="remoteAudio_' + tileId + '" ondblclick="enterText(' + tileId + ');" onclick="switchVideo(' + tileId + ')" autoplay  style="cursor:pointer;padding-left:5px;width: 180px;height: 135px ; background: transparent;    border: none;display:none"></video>' +
					// 	'<div  id="aRemoteImage_' + tileId + '" onclick="switchVideo(' + tileId + ')" style="margin-top: 1vh;margin-left:1vh;" class="" align="center">' +
					// 		'<div id="host_' + tileId + '" style="display:none;">' +
					// 			'<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  >' +
					// 		'</div>' +
					// 		'<img  title="' + userName + '" src="' + imgurl + '" title="" style="height: 60px;cursor: pointer;border-radius: 50%;width: 60px;" onerror="userImageOnErrorReplace(this);" > ' +
					// 	'</div>'+
					// 	'<div style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">' +
					// 		'<div style="padding-top:12px;/*! display: none; */">'+
					// 			'<span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display: none;margin-right: 51px;">'+
					// 				'<img style="width:18px ;   margin-top: 5px;" src="' + path + '/images/temp/mic2.png">'+
					// 			'</span>'+
					// 		'</div>' +
					// 		'<div class="meterGraph" ><div class="meter" ></div></div>'+
					// 	'</div>'+
					// '</div>');
			}
			var pcount = parseInt(tilePreparationArray.length);
			$(".pcnt").html(pcount);
			$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");
			tilesPopup();
		}
	}
	else if (callsignal.type == "cancelconnecting") {
		$("#notification").hide();
		// $("#notification").hide(500);
	}
	/*else if(callsignal.type=="sendall"){
		alert("here");
		var newId=callsignal.newuseradded;
		var imgurl = $('div#' + newId + ' div img').attr('src');
		var userName = $('div#' + newId + ' div.user_box_name').text();
		alert(newId);
	}*/
	return true;
}

var shareScreen = "off";
var shareStaus = "off";
var screenHost = false;
/*function ChangeStream(obj){
    
	if(isChromeCall){
		 $('#vSwitch').html('');
		  
			   if(shareScreen == "off" ){
					    
						 if(callType !=="A"){	
							 //peerConnection.removeTrack(sender);
						 }
						 sourceId=null;
						 screenHost = true;
					   //   checkExtensionStatus("dpblaocfmngfpgjdfkebibghkmlahpln");//local
						checkExtensionStatus("ppcbnhnfpkbmjacikndoipjpcpcdmmmj");//this code for the other instances
					    
						 $(obj).attr('src',path+'/images/temp/'+$(obj).attr('toglsrc'));
						 var jsonText = '{"calltype":"'+callType+'","type":"streamsg","status":"'+shareScreen+'","recordoppstatus":"'+recordStatus+'","callid":"'+callId+'"}';
						 notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
						 serverConnection.send(notify);
						 shareScreen = "on";
						 shareStaus = "on"; 	
						 //if(callType=="A"){
						   var jsonText = '{"calltype":"'+callType+'","type":"screenshare","status":"'+shareScreen+'","callid":"'+callId+'"}';
						   notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
						   serverConnection.send(notify);
						// }
						   if(screenStreamTrack != null){
										localVideo.srcObject = localScreenStream;
							 }
				   if(screenshareArray.indexOf(userIdglb.toString())==-1){
					   screenshareArray.push(userIdglb.toString());
				   }
			   	
				   $("#sharescreenu_"+userIdglb).show();
				 }else{
						   screenHost = false;
							   $(obj).attr('src',path+'/images/temp/'+$(obj).attr('defsrc'));
						   var jsonText = '{"calltype":"'+callType+'","type":"streamsg","status":"'+shareScreen+'","recordoppstatus":"'+recordStatus+'","callid":"'+callId+'"}';
						   notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
						   serverConnection.send(notify);
						 shareScreen = "off";
						// if(callType=="A"){
							 var jsonText = '{"calltype":"'+callType+'","type":"screenshare","status":"'+shareScreen+'","callid":"'+callId+'"}';
							 notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
						 serverConnection.send(notify);
						// }
				   if(screenStreamTrack != null){
						if(callType=="A"){
								 localStream.removeTrack(screenStreamTrack);
							 }else{
								 localStream.removeTrack(screenStreamTrack);
							    
							 }
							 localScreenStream.stop();
						   for(var i=0;i<userConnected.length;i++){
								var pc = getPeerConnection(userConnected[i]);
								var ids = userConnected[i];
								pc.removeTrack(peerSsender[ids]);
								peerSsender[ids]=[];
							   if(callType !="A"){
								vsender = pc.addTrack(videoStream,localStream);
						peerVsender[ids]=vsender;
							   }
								makeOffer(userConnected[i]);
					   }
				   }
					    
					   var idx_ss=screenshareArray.indexOf(userIdglb.toString());
			   if(idx_ss>-1){
				   screenshareArray.splice(idx_ss,1)
			   }
					   $("#sharescreenu_"+userIdglb).hide(); 
						 shareStaus = "off";
						 
						 localVideo.srcObject = localStream;	
						 resetRecordStreams();
				 }
		   
	}else{
	    
		$('#vSwitch').html('');
		shareStaus = "on";
		 
		   if(shareScreen == "off" ){
			   if(callType !=="A"){	
				 //peerConnection.removeTrack(sender);
			 }
	 	
			navigator.getUserMedia = navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
		 navigator.getUserMedia(screen_constraints_moz, function (ScreenStream) {
			 localScreenStream= ScreenStream;
			 localVideo.srcObject = localScreenStream;
			 localStream.addTrack(ScreenStream.getVideoTracks()[0]);
			 getScreenMediaSuccess(localStream);
		 }, function (error) {
		   console.log(error);
		   if(callType=="A"){
				ChangeStream('#ascreenshareImg');
				cancelValidation('on','#ascreenshareImg');
			    
		   }else if(callType=="AV"){
			   ChangeStream('#vscreenshareImg');
					   cancelValidation('on','#vscreenshareImg');
				   }
		 });
	   	
		    
			 $(obj).attr('src',path+'/images/temp/'+$(obj).attr('toglsrc'));
			 var jsonText = '{"calltype":"'+callType+'","type":"streamsg","status":"'+shareScreen+'","recordoppstatus":"'+recordStatus+'","callid":"'+callId+'"}';
			 notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
			 serverConnection.send(notify);
			 shareScreen = "on";
			 shareStaus = "on"; 	
			 //if(callType=="A"){
			   var jsonText = '{"calltype":"'+callType+'","type":"screenshare","status":"'+shareScreen+'","callid":"'+callId+'"}';
			   notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
			   serverConnection.send(notify);
				// }
			localVideo.srcObject = localScreenStream;
					 
				 }else{
				   $(obj).attr('src',path+'/images/temp/'+$(obj).attr('defsrc'));
			   var jsonText = '{"calltype":"'+callType+'","type":"streamsg","status":"'+shareScreen+'","recordoppstatus":"'+recordStatus+'","callid":"'+callId+'"}';
			   notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
			   serverConnection.send(notify);
			 shareScreen = "off";
			// if(callType=="A"){
				 var jsonText = '{"calltype":"'+callType+'","type":"screenshare","status":"'+shareScreen+'","callid":"'+callId+'"}';
				 notify = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
				 serverConnection.send(notify);
			// }
				if(callType=="A"){
						 localStream.removeTrack(screenStreamTrack);
					 }else{
						 localStream.removeTrack(screenStreamTrack);
					    
					 }
					 localScreenStream.stop();
		   for(var i=0;i<userConnected.length;i++){
				var pc = getPeerConnection(userConnected[i]);
				var ids = userConnected[i];
				pc.removeTrack(peerSsender[ids]);
				peerSsender[ids]=[];
			   if(callType !="A"){
				vsender = pc.addTrack(videoStream,localStream);
			peerVsender[ids]=vsender;
			   }
				makeOffer(userConnected[i]);
		   }
		    
			 shareStaus = "off";
			 
			 localVideo.srcObject = localStream;
			 resetRecordStreams();
				}
	}
	   
}*/
function screensteamerror(err) {
	console.error("Error: " + err);
	if (callType == "A") {
		ChangeStream('#ascreenshareImg');
		cancelValidation('on', '#ascreenshareImg');

	} else if (callType == "AV") {
		ChangeStream('#vscreenshareImg');
		cancelValidation('on', '#vscreenshareImg');
	}
}


function ChangeStream(obj) {



	$('#vSwitch').html('');

	if (shareScreen == "off") {

		if (callType !== "A") {
			//peerConnection.removeTrack(sender);
		}
		sourceId = null;
		screenHost = true;
		var displayMediaStreamConstraints = {
			video: {
				"aspectRatio": 1.6,
				"frameRate": 30,
				"height": 720,
				"resizeMode": "crop-and-scale",
				"width": 1280,
				"cursor": "always",
				"displaySurface": "monitor",
				"logicalSurface": true
			}
		};

		if (navigator.mediaDevices.getDisplayMedia) {
			navigator.mediaDevices.getDisplayMedia(displayMediaStreamConstraints).then(function (stream) {

				localScreenStream = stream;
				localVideo.srcObject = localScreenStream;
				localStream.addTrack(stream.getVideoTracks()[0]);
				getScreenMediaSuccess(localStream);
				/*stream.oninactive = () => { // Click on browser UI stop sharing button
					  console.info("Recording has ended");
					  stopCapture();
					}*/
			}).catch(screensteamerror);
		} else {
			navigator.getDisplayMedia(displayMediaStreamConstraints).then(function (stream) {

				localScreenStream = stream;
				localVideo.srcObject = localScreenStream;
				localStream.addTrack(stream.getVideoTracks()[0]);
				getScreenMediaSuccess(localStream);
				/*stream.oninactive = () => { // Click on browser UI stop sharing button
					  console.info("Recording has ended");
					  stopCapture();
					}*/
			}).catch(screensteamerror);
		}


		// $(obj).attr('src', path + '/images/cme/' + $(obj).attr('toglsrc'));
		$('.ascreenshareImg').attr('src', path + '/images/cme/' + $('.ascreenshareImg').attr('toglsrc'));
		var jsonText = '{"calltype":"' + callType + '","type":"streamsg","status":"' + shareScreen + '","recordoppstatus":"' + recordStatus + '","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
		shareScreen = "on";
		shareStaus = "on";
		//if(callType=="A"){
		var jsonText = '{"calltype":"' + callType + '","type":"screenshare","status":"' + shareScreen + '","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
		// }
		if (screenStreamTrack != null) {
			localVideo.srcObject = localScreenStream;
		}
		if (screenshareArray.indexOf(userIdglb.toString()) == -1) {
			screenshareArray.push(userIdglb.toString());
		}
		$("div[id^=sharescreen_]").css("display", "none");
		$("div[id^=sharescreenu_]").css("display", "none");
		$("#sharescreenu_" + userIdglb).show();
		$("#RemoteDiv_" + userIdglb).attr('screenshare', 'Y');
	} else {
		screenHost = false;
		// shareScreen = "off";
		// $(obj).attr('src', path + '/images/cme/' + $(obj).attr('defsrc'));
		$('.ascreenshareImg').attr('src', path + '/images/cme/' + $('.ascreenshareImg').attr('defsrc'));
		var jsonText = '{"calltype":"' + callType + '","type":"streamsg","status":"' + shareScreen + '","recordoppstatus":"' + recordStatus + '","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
		shareScreen = "off";
		// if(callType=="A"){
		var jsonText = '{"calltype":"' + callType + '","type":"screenshare","status":"' + shareScreen + '","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
		// }
		if (screenStreamTrack != null) {
			if (callType == "A") {
				localStream.removeTrack(screenStreamTrack);
			} else {
				localStream.removeTrack(screenStreamTrack);

			}
			localScreenStream.stop();
			for (var i = 0; i < userConnected.length; i++) {
				var pc = getPeerConnection(userConnected[i]);
				var ids = userConnected[i];
				pc.removeTrack(peerSsender[ids]);
				peerSsender[ids] = [];
				if (callType != "A") {
					vsender = pc.addTrack(videoStream, localStream);
					peerVsender[ids] = vsender;
				}
				makeOffer(userConnected[i]);
			}
		}

		var idx_ss = screenshareArray.indexOf(userIdglb.toString());
		if (idx_ss > -1) {
			screenshareArray.splice(idx_ss, 1)
		}
		$("#sharescreenu_" + userIdglb).hide();
		$("#RemoteDiv_" + userIdglb).attr('screenshare', 'N');
		shareStaus = "off";

		localVideo.srcObject = localStream;
		resetRecordStreams();
	}





}









function cancelValidation(status, obj) {
	shareScreen = "off";
	if (status == "off") {
		$(obj).attr('src', path + '/images/temp/' + $(obj).attr('toglsrc'));
	} else {
		$(obj).attr('src', path + '/images/temp/' + $(obj).attr('defsrc'));
	}
	if (callType == "AV") {
		localVideo.srcObject = localStream;
	}
	var jsonText = '{"calltype":"' + callType + '","type":"screenshare","status":"' + shareScreen + '","callid":"' + callId + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);

}

var localScreenStream;
function checkExtensionStatus(extensionId) {

	getChromeExtensionStatus(extensionId, function (status) {
		if (status == 'installed-enabled') {
			getScreenConstraints(function (error, screen_constraints) {
				if (error) {
					return alert(error);
				}
				//navigator.getUserMedia = navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
				navigator.getUserMedia = navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
				if (callType == "AS") {

					/*navigator.getUserMedia(audio_constraints, function (stream) {
						navigator.getUserMedia({video: screen_constraints}, function (ScreenStream) {
							localStream=stream;
							localStream.addTrack(ScreenStream.getVideoTracks()[0]);
							getScreenMediaSuccess(localStream);
						  
						  sendDndPresence();
						  var jsonText = '{"calltype":"'+callType+'","type":"callrequest","devicetype":"web","browsertype":"'+senderBrowserType+'","callid":"'+callId+'"}';
						  notify = $msg({to: jid,"type" : "normal"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
						  serverConnection.send(notify);
						}, function (error) {
						  console.log(error);
						  stream.getTracks().forEach(track => track.stop());
						  disconnectReqVideoCall();
						  closeVideoCall();
						});
					  }, function (error) {
						 console.log(error);
						 stream.getTracks().forEach(track => track.stop());
						 disconnectReqVideoCall();
						 closeVideoCall();
					  }); */

				} else {

					navigator.getUserMedia({ video: screen_constraints }, function (ScreenStream) {
						localScreenStream = ScreenStream;
						localVideo.srcObject = localScreenStream;
						localStream.addTrack(ScreenStream.getVideoTracks()[0]);
						getScreenMediaSuccess(localStream);
						resetRecordStreams(); // this method is used to reset streams for recording.
					}, function (error) {
						console.log(error);
						shareScreen = "on";

						//cancelValidation('on');
						if (callType == "A") {
							ChangeStream('#ascreenshareImg');
							cancelValidation('on', '#ascreenshareImg');

						} else if (callType == "AV") {
							ChangeStream('#vscreenshareImg');
							cancelValidation('on', '#vscreenshareImg');
						}
					});

				}


			});
		}

		if (status == 'installed-disabled') {
			if (callType == "A") {
				ChangeStream('#ascreenshareImg');
			} else if (callType == "AV") {
				ChangeStream('#vscreenshareImg');
			}
			chatAlertFun('Please enable the extesnion.', 'warning');
		}

		if (status == 'not-installed') {
			if (callType == "A") {
				ChangeStream('#ascreenshareImg');
			} else if (callType == "AV") {
				ChangeStream('#vscreenshareImg');
			}
			chatAlertFun('Please install the "Colabus Screen Sharing" extension from the chrome web store.', 'warning');
		}

	});

}
function getScreenMediaSuccess(stream) {

	//localStream = stream;//---- Contains all three streams(Video,Screen,Audio)

	const videoTracks = stream.getVideoTracks();
	const audioTracks = stream.getAudioTracks();
	//console.log('videoTracks.length:'+videoTracks.length);
	//console.log('audioTracks.length:'+audioTracks.length);

	if (callType == "A") {
		stream.getVideoTracks()[0].onended = function () {
			shareScreen = "on";
			ChangeStream('#ascreenshareImg');
			return;
		};
		shareScreen = "on";
		screenStreamTrack = videoTracks[0];
		// screenStreamTrack.applyConstraints({ width: 1280, height: 720});
		//ssender = peerConnection.addTrack(screenStreamTrack, localStream);
		for (var i = 0; i < userConnected.length; i++) {
			var pc = getPeerConnection(userConnected[i]);
			var ids = userConnected[i];
			try {
				ssender = pc.addTrack(screenStreamTrack, localStream);
			} catch (err) {
				console.log("error:" + err);
				pc.removeTrack(peerSsender[ids]);
				ssender = pc.addTrack(screenStreamTrack, localStream);
			}
			peerSsender[ids] = ssender;
			makeOffer(userConnected[i]);
		}
		//peerConnection.createOffer().then(createdDescription).catch(errorHandler);

	} else if (callType == "AV") {
		stream.getVideoTracks()[1].onended = function () {
			shareScreen = "on";
			ChangeStream('#vscreenshareImg');
			return;
		};
		screenStreamTrack = videoTracks[1];

		//screenStreamTrack.applyConstraints({ width: 1280, height: 720});
		shareScreen = "on";
		for (var i = 0; i < userConnected.length; i++) {
			var pc = getPeerConnection(userConnected[i]);
			var ids = userConnected[i];

			try {
				pc.removeTrack(peerVsender[ids]);
				ssender = pc.addTrack(screenStreamTrack, localStream);

			} catch (err) {
				console.log("error:" + err);
				pc.removeTrack(peerSsender[ids]);
				ssender = pc.addTrack(screenStreamTrack, localStream);
			}
			peerSsender[ids] = ssender;
			makeOffer(userConnected[i]);
		}

	} else if (callType == "AS") {
		screenStreamTrack = videoTracks[0];
		ssender = peerConnection.addTrack(screenStreamTrack, localStream);
		audioStream = audioTracks[0];//---- Extracting audio stream
		asender = peerConnection.addTrack(audioStream, localStream);

	}

}




//------------->highlight functions starts from here-----------------------> 

function highlightVideo() {
	commType = "highLightSpeechType";
	// if (isChromeVoice) {
	// 	$("#hMsg").css('border', '1px solid red').focus();
	// 	$("#hLightDesc").css('border', '0px none');
	// 	try {
	// 		recognizationToggleStop();
	// 		recognizationToggleStart('', 'highLightSpeechType', 'highLightSpeechType');
	// 		recognizationToggleStart('', 'globalVoiceCall', 'globalVoiceCall', '');
	// 	} catch (e) {
	// 		console.log('e1:' + e);
	// 	}
	// } else {
	// 	$('#hLightVoiceStartNew').hide();
	// 	$('#hLightVoiceStopNew').hide();
	// }

	// BackFromRecomd();
	// $("#highlightFor").toggle();
	// console.log('--h1--'+$("#mainHL").is(":visible"));
	if ($("#mainHL").is(":visible") == true) {

	}
	if($('#cmePopupHL').is(":visible") == true){
		$('#cmePopupHL').hide();
		$("#transparentDiv").hide();
	}else{
		$('#cmePopupHL').show();
		$("#transparentDiv").show();
		BackFromCmeHL();
		// $('#cmePopupTS').hide();
	}


	// if ($("#mainHL").is(":visible") == true) {
	// 	$("img.avhglightImg").attr('src', path + '/images/temp/' + $("img.avhglightImg").attr('toglsrc'));
	// 	$("img.avTransImg").attr('src', path + '/images/temp/' + $("img.avTransImg").attr('defsrc'));
	// 	setHighlightTime();
	// 	hAddFlag = false;
	// 	$("#hMsg").focus();
	// 	$("#hListDiv").html("");
	// 	// backToHighlights();
	// 	getHighlight(callId, "", "");
	// 	$('#hMainDiv').css('height', '100%');
	// 	$('#hAddDiv').hide();
	// 	addNewHighlight();
	// 	$('#transcriptFor').css({ 'display': 'none' });
	// } else {
	// 	$("img.avhglightImg").attr('src', path + '/images/temp/' + $("img.avhglightImg").attr('defsrc'));
	// 	clearInterval(setHLtimer);
	// }
	// checkHighlightWindowSize();
	// resizeRemoteStream();
}

var setHLtimer;
function setHighlightTime() {
	if (setHLtimer != null) {
		clearInterval(setHLtimer);
	}
	setHLtimer = setInterval(function () {
		$("#hTime").text($('#aTimer').text());
	}, 500);
}

function checkHighlightWindowSize() {
	if ($("#chatFor").is(":visible") == false) {
		//alert('if--->');
		if (window.innerWidth < 650) {
			if ($("#highlightFor").is(":visible") == true) {
				$("#highlightFor").css({ 'width': '50%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '50%' });
				window.resizeTo(640, 600);
				window.screenX = 300;
			} else if ($("#transcriptFor").is(":visible") == true) {
				$("#transcriptFor").css({ 'width': '50%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '50%' });
				window.resizeTo(640, 600);
				window.screenX = 300;
			} else {
				$("#highlightFor").css({ 'width': '50%', 'display': 'none' });
				$("#transcriptFor").css({ 'width': '50%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '100%' });
				window.resizeTo(320, 600);
				window.screenX = 300;
			}
		} else {
			if ($("#highlightFor").is(":visible") == true) {
				$("#highlightFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '70%' });
			} else if ($("#transcriptFor").is(":visible") == true) {
				$("#transcriptFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '70%' });
			} else {
				$("#highlightFor").css({ 'width': '30%', 'display': 'none' });
				$("#transcriptFor").css({ 'width': '30%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '100%' });
			}
		}
	} else {
		//alert('else--->');
		if (window.innerWidth < 650) {
			if ($("#highlightFor").is(":visible") == true) {
				$("#highlightFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '40%' });
				$("#chatFor").css({ 'width': '30%' });
				window.resizeTo(900, 600);
				window.screenX = 200;
			} else if ($("#transcriptFor").is(":visible") == true) {
				$("#transcriptFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '40%' });
				$("#chatFor").css({ 'width': '30%' });
				window.resizeTo(900, 600);
				window.screenX = 200;
			} else {
				$("#highlightFor").css({ 'width': '30%', 'display': 'none' });
				$("#transcriptFor").css({ 'width': '30%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '50%' });
				$("#highlightFor").css({ 'width': '50%' });
				$("#transcriptFor").css({ 'width': '50%' });
				window.resizeTo(320, 600);
				window.screenX = 300;
			}
		} else {
			if ($("#highlightFor").is(":visible") == true) {
				$("#highlightFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '40%' });
			} else if ($("#transcriptFor").is(":visible") == true) {
				$("#transcriptFor").css({ 'width': '30%', 'display': 'block' });
				$("#avCallContainer").css({ 'width': '40%' });
			} else {
				$("#highlightFor").css({ 'width': '30%', 'display': 'none' });
				$("#transcriptFor").css({ 'width': '30%', 'display': 'none' });
				$("#avCallContainer").css({ 'width': '70%' });
			}
		}

	}
}

var projectId = "";
var hAddFlag = false;
function addNewHighlight() {
	if (hAddFlag) {
		sendingHighlight("add");
		hAddFlag = false;
	}

	$('#hMainDiv').css('height', '70%');
	$('#hAddDiv').show();
	hAddFlag = true;
	$("#hTime").text($('#aTimer').text());
	$("#hMsg").val("").focus();
	$("#hLightDesc").val("");
}
var projDDListData = "";
function fetchWorkspaceProjects(type, loadType) {
	//$('#speechRecDivAreaId').removeClass('speechRecNewCls');
	var projects = "";
	/* $.ajax({
		  url : path + "/workspaceAction.do",
		  type : "POST",
		  data : {
				  act : "loadProjDDList",
				  userId : userIdglb,
				  sortValue : "",
				  sortHideProjValue : "",
				  ipadVal : "",
				  sortType: "",
				  txt: "",
				  type: "",
				  device:'web',
			  },
		  error: function(jqXHR, textStatus, errorThrown) {
				  checkError(jqXHR,textStatus,errorThrown);
				  $("#loadingBar").hide();
				  timerControl("");
				  }, 
		  success : function(result){
				  sessionTimeOutMethod(result);
				  if(result != "SESSION_TIMEOUT"){
						  //checkSessionTimeOut(result);
						  projDDListData = result;
						  $('#hlProjListDDContainerSub').html(prepareProjListDdUINewUI());
						  $('#hlProjListDDContainerSub').prepend('<div id="proj_0" class="HighlighProjListNewUI" style="width:100%;border-bottom:1px solid #cccccc;height: 36px;padding: 2% 1%;color:#000000;cursor:pointer;" onclick="loadDDProjNewUI(this)">Select Workspace</div>');
						  //$('#hlProjListDDContainerSub:first-child')
						  //alert(type+"<-->"+$('#hlightproject').val()+"<-->"+loadType);
						  if($('#hlProjListDDContainerSub').children('div').length < 2){
							$('#hlProjListDDContainerSub').html("<span>No projects found.</span>");
						  }else if(type == 'update'){
							  if($('#hlightproject').val() == 0){
								  $('#hlProjListDDContainerSub').find('.HighlighProjListNewUI:eq(0)').trigger('click');
							  }else{
								  $('#hlProjListDDContainerSub').find('#proj_'+$('#hlightproject').val()).trigger('click');
							  }
						  	
							   if($('#taskParticipantsInpFiled').val() != ''){
									  var taskParticipant = $('#taskParticipantsInpFiled').val();
									  var taskParticipantnew = jQuery.parseJSON(taskParticipant);  
									  for(var i = 0; i < taskParticipantnew.length ;i++){
													commonUIForUserList(taskParticipantnew[i].user_id,taskParticipantnew[i].fname);
											}
							   }
						  	
						  }else if(type == 'view'){
							  if($('#hlightproject').val() == 0){
								  $('#hlProjListDDContainerSub').find('.HighlighProjListNewUI:eq(0)').trigger('click');
							  }else{
								  $('#hlProjListDDContainerSub').find('#proj_'+$('#hlightproject').val()).trigger('click');
							  }
						  	
						  }else{
							  $('#hlProjListDDContainerSub').find('.HighlighProjListNewUI:eq(1)').trigger('click');
						  }
						  if(loadType == 'voiceTask'){
							  selectVoiceWorkspace();
							  $('#speechRecDivAreaId').addClass('speechRecNewCls');
						  }
				  }
				 
			   }
	  }); */


}


function prepareProjListDdUINewUI() {
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	var UI = "";
	var recentFlag = false;
	var projArray = projDDListData.split('#@#');
	for (var j = 0; j < projArray.length; j++) {
		var jsonData = jQuery.parseJSON(projArray[j]);
		for (var i = 0; i < jsonData.length; i++) {
			var projectID = jsonData[i].projectID;
			var ProjTitle = jsonData[i].ProjTitle;
			var funcFlag = jsonData[i].funcFlag;
			var funClick = jsonData[i].funClick;
			var imageUrl = jsonData[i].imageUrl; // url for images
			var imageClass = jsonData[i].imageClass;
			var projName = jsonData[i].projName;
			var projectArchiveStatus = jsonData[i].projectArchiveStatus;
			var cancelL = jsonData[i].cancelL;
			var ctxPath = jsonData[i].ctxPath;
			projSettingL = jsonData[i].projSettingL;
			var projectUsersStatus = jsonData[i].projectUsersStatus;
			projName = projName.replaceAll("ch(20)", "'");
			if (jsonData[i].projStatus != "I" && projectId != projectID) {
				recentFlag = true;

				UI += "	 <div id=\"proj_" + projectID + "\" class=\"HighlighProjListNewUI\" style=\"width:100%;border-bottom:1px solid #cccccc;height: 42px;padding: 3px 4px;color:#000000;cursor:pointer;\" onclick=\"loadDDProjNewUI(this)\">"
				if (projectUsersStatus == "PO") {
					UI += " 	<div id=\"projectName_" + projectID + "\"  style=\"display:none;\" >" + projName + "</div>"
						+ " 	<div id=\"projectType_" + projectID + "\"  style=\"display:none;\" >MyProjects</div>"
						+ " 	<div id=\"projectArchiveStatus_" + projectID + "\"  style=\"display:none;\" >" + projectArchiveStatus + "</div>"
						+ " 	<div id=\"projectUsersStatus_" + projectID + "\"  style=\"display:none;\" >" + projectUsersStatus + "</div>";
				} else {

					UI += "		<div id=\"projectName_" + projectID + "\"  style=\"display:none;\" >" + projName + "</div>"
						+ " 	<div id=\"projectType_" + projectID + "\"  style=\"display:none;\" >SharedProjects</div>"
						+ " 	<div id=\"projectArchiveStatus_" + projectID + "\"  style=\"display:none;\" >" + projectArchiveStatus + "</div>"
						+ " 	<div id=\"projectUsersStatus_" + projectID + "\"  style=\"display:none;\" >" + projectUsersStatus + "</div>";
				}
				UI += "	    <img id=\"projectImageId_" + projectID + "\" style=\"float:left;height:27px;width:30px;margin-top: 1%;border: 1px solid #cccccc;\" onerror=\"imageOnProjNotErrorReplace(this);\" src=\"" + imageUrl + "\" >"
				UI += "	    <span class='defaultExceedCls' style=\"float:left;margin: 2% 0 0 5px;width:78%;font-size:13px;font-family: OpenSansRegular,cabin,Arial,sans-serif;\" title=\"" + ProjTitle + "\">" + projName + "</span>"
				UI += "	    <input type='hidden' class='projNameClss' value='" + projName.toLowerCase() + "'></input>"
				UI += "	 </div>"

			}
		}
		if (j == 0 && recentFlag == true) {
			UI += "	<hr style=\"border: 1px solid #cccccc;margin: 0px;\">"
		}
	}
	UI = UI.replaceAll("ch(20)", "'").replaceAll("ch(30)", "chr(dbl)").replaceAll("ch(50)", "[").replaceAll("ch(51)", "]").replaceAll("ch(curly)", "{").replaceAll("ch(clcurly)", "}").replaceAll("ch(backslash)", "\\");
	return UI;
}
function loadDDProjNewUI(obj) {
	var projectId = obj.id.split('_')[1];
	if (projectId == 0) {
		$('#hlProjIcon').attr('src', '').hide();
		$('#hlProjName').text('Select Workspace');
		$('#hlightproject').val(projectId);
	} else {
		var projType = $("#projectType_" + projectId).text();
		var projName = $("#projectName_" + projectId).text();
		var projectUsersStatus = $("#projectUsersStatus_" + projectId).text();
		var projtitle = $("#projectImageId_" + projectId).attr("title");
		var projArchStatus = $("div#projectArchiveStatus_" + projectId).text();
		var projImgSrc = $("img#projectImageId_" + projectId).attr('src');
		$('#hlProjIcon').attr('src', projImgSrc).show();
		$('#hlProjName').text(projName);
		$('#hlightproject').val(projectId);
	}
	fetchhlightTaskUsers(projectId);
	$('#projListDDContainer').hide();

}
function cancelCreateHighlight() {
	var hmsg = $("#hMsg").val().trim();
	var hdesc = $("#hLightDesc").val().trim();

	if (hmsg != "" || hdesc != "") {
		conFunNew11(getValues(companyAlerts, "Alert_ResetConf"), "warning", "resetHighlightText", "hideTheConfirmBox");
	}

	highLightNameSpeechNew = '';
	highLightDescSpeechNew = '';
}

function resetHighlightText() {
	$("#hMsg").val("");
	$("#hLightDesc").val("");
	setHighlightTime();
	hAddFlag = false;

}

function hideTheConfirmBox() {
	$("#confirmDiv").hide();
}

function validateAddHLEvent(event) {
	if ((event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey) {
		sendingHighlight('save');
	}
}
function validateUpdHLEvent(event, hId) {
	if ((event.keyCode == 10 || event.keyCode == 13) && event.ctrlKey) {
		updateHighlight(hId);
	}
}


function sendingHighlight(action) {
try{
	if (isChromeVoice) {
		$("#hMsg").css('border', '1px solid red').focus();
		$("#hLightDesc").css('border', '0px none');
	} else {
		$("#hMsg").css('border', '0px none');
		$("#hLightDesc").css('border', '0px none');
	}
	$('.hlDiv').each(function () {
		if ($(this).find('.hMsgEdit').is(':visible')) {
			updateHighlight($(this).attr('id').split("_")[1]);
		}
	});
	var d = new Date();
	var timeZone = getTimeOffset(d);
	var hTime = $("#hTime").text();
	var hMsg = $("#hMsg").val().trim();
	var hDesc = $("#hLightDesc").val().trim();
	//var hTime = $('#vTimer').text();
	//var hMsg = "";
	//var hDesc = "";

	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;
	var fileName = "Highlight " + dateTime;
	hAddFlag = false;
	// var cid = $('#user_')
	let jsonbody = {
		"callId" : callId,
		"sender" : userIdglb,
		"hl_msg" : hMsg,
		"hl_time" : hTime,
		"hl_desc" : hDesc,
		"convId" : vConvId,
    	"fileName" : fileName
	}
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/insertHighlight",
		// url: "http://localhost:8080/v1/insertHighlight",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
	// $.ajax({
	// 	url: path + "/ChatAuth",
	// 	type: "post",
	// 	data: { act: "insertHighlight", sender: userIdglb, hMsg: hMsg, hDesc: hDesc, hTime: hTime, timeZone: timeZone, callId: callId, fileName: fileName, convId: "0", device: 'web' },
	// 	mimeType: "textPlain",
	// 	success: function (result) {
			checkSessionTimeOut(result);
			// if (result != "SESSION_TIMEOUT") {
				$("#hMsg").val("").focus();
				$("#hLightDesc").val("");
				if (action == 'save') {
					$("#hLightDesc").attr('highLightMsg', '');
					$("#hMsg").attr('highLightMsg', 'active');
					cancelCreateHighlight();
				}
				if (result != "failed") {
					var newhId = result[0].hl_id;
					var jsonText = '{"calltype":"' + callType + '","type":"videohighlight","status":"new","callid":"' + callId + '","hId":"' + newhId + '"}';
					var message = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);; // preapring the chat stanza to send		 
					serverConnection.send(message);  // sending the highlight message
					var UI = prepareHighlightUI(result, "edit");
					$("#hListDiv").append(UI);
					//editHightlight(newhId);
				}
			// }
		}
	});
} catch (eMsg) {
	console.log(eMsg);
}

}

var glbgethighlights='';
function getHighlight(callId, msgId, hId) {
	var d = new Date();
	var timeZone = getTimeOffset(d);
	let jsonbody = {
		"callId" : callId,
		"msgid" : msgId,
		"host_id" : hId
	}
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getHighlights",
		// url: "http://localhost:8080/v1/getHighlights",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			glbgethighlights= result;

			// if (result != "SESSION_TIMEOUT") {
				//alert(result);
				if (result != "[]") {
					try{

					if (callId != '') { //-- this is in place of video call
						var UI = prepareHighlightUI(result, "edit");
						$("#hListDiv").append(UI);
					} else if (msgId != '') {//-- this is in place of chat history
						var UI = prepareHighlightUI(result, "view");
						$(".hListDiv").append(UI);
					} else { //-- this is in case of edit/fetch highlight based on highlight id
						var UI = prepareHighlightUI(result, "edit");
						if ($("#hListDiv").find('#hl_' + hId).length > 0)
							$('#hl_' + hId).replaceWith(UI);
						else
							$("#hListDiv").append(UI);
					}
					} catch (eMsg) {
						console.log(eMsg);
					}
				}
			// }
		}
	});

	// $.ajax({
	// 	url: path + "/ChatAuth",
	// 	type: "post",
	// 	data: { act: "getHighlight", sender: userIdglb, timeZone: timeZone, callId: callId, msgId: msgId, hId: hId, device: 'web' },
	// 	mimeType: "textPlain",
	// 	success: function (result) {
	// 		sessionTimeOutMethod(result);
	// 		if (result != "SESSION_TIMEOUT") {
	// 			//alert(result);
	// 			if (result != "[]") {

	// 				if (callId != '') { //-- this is in place of video call
	// 					var UI = prepareHighlightUI(result, "edit");
	// 					$("#hListDiv").html(UI);
	// 				} else if (msgId != '') {//-- this is in place of chat history
	// 					var UI = prepareHighlightUI(result, "view");
	// 					$("#hListDiv").html(UI);
	// 				} else { //-- this is in case of edit/fetch highlight based on highlight id
	// 					var UI = prepareHighlightUI(result, "edit");
	// 					if ($("#hListDiv").find('#hl_' + hId).length > 0)
	// 						$('#hl_' + hId).replaceWith(UI);
	// 					else
	// 						$("#hListDiv").append(UI);
	// 				}
	// 			}
	// 		}
	// 	}
	// });

}
function showRecommendationsInChat(id) {
	// highlightVideo();
	// var convId = $("#user_" + id).attr("cid");

	// var cloneObj = $('#recomdContainerListDiv').children().clone();
	// $("#chat-content").find("#text-box").hide();
	// $("#chat-content").find("#chat-dialog").hide();
	// $("#chat-content #chat-recomd-dialog").html("").show();
	// $(cloneObj).appendTo("#chat-content #chat-recomd-dialog");
	// $("#chatMoreOptionsDiv").hide();
	$("#chatMoreOptionsDiv").removeClass('d-flex');
	$('.contacts').text('Recommendations');
	$('.contacts2').text('Recommendations');
	$('.contsearch').hide();
	$('.contsearch2').hide();
	$('.backcontact').show();
	$('.backcontact2').hide();
	$('#tMainDiv').hide();
	$('#mainHL').hide();
	var cloneObj = $('#recomdContainerListDiv').children().clone();

	$(".recomdContainerListDivForTrans").html("").show();
	$(cloneObj).appendTo(".recomdContainerListDivForTrans");

	$("#transcriptContainer").hide();
	$(".recomdContainerForTrans").show();
	$(".recomTSListDiv").hide();

	// fetchRecomdData(vConvId, "call");
	fetchRecomdData(vConvId, "chat");
}

function showChatFromRecomdView(id) {
	$("#chat-content #chat-recomd-dialog").html("").hide();
	$("#chat-content").find("#text-box").show();
	$("#chat-content").find("#chat-dialog").show();
	// $("#chatMoreOptionsDiv").hide();
	$("#chatMoreOptionsDiv").removeClass('d-flex');
	//$('li#'+id).trigger('click');
}

function showRecommendations() {
	$("#hlightContainer").hide();
	$("#recomdContainer").show();
	fetchRecomdData(vConvId, "call");
}

function showTransRecommendations() {
	$('.contacts').text('Recommendations');
	$('.contacts2').text('Recommendations');
	$('.contsearch').hide();
	$('.contsearch2').hide();
	$('.backcontact').show();
	$('.backcontact2').show();
	$('#tMainDiv').hide();
	$('#mainHL').hide();
	var cloneObj = $('#recomdContainerListDiv').children().clone();

	$(".recomdContainerListDivForTrans").html("").show();
	$(cloneObj).appendTo(".recomdContainerListDivForTrans");

	$("#transcriptContainer").hide();
	$(".recomdContainerForTrans").show();
	$(".recomTSListDiv").hide();

	fetchRecomdData(vConvId, "call");
}

function fetchRecomdRec(convId, type){
	tid = convId;
	ttype = type;
	//console.log(tid+"--tid--ttype--"+ttype);
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	// $("#loadingBar").show();
	// timerControl("start");
	// $("#loadMsg").hide();

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"user_id": userIdglb,
		"convId" : convId,
		"localTZ": localOffsetTime,
		"type" : type,
		"type1" : "recordings"
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchRecomdData",
		// url: "http://localhost:8080/v1/fetchRecomdData",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			// if (result != "SESSION_TIMEOUT") {
				var recJsonData = result;
				// var wbJsonData = result.split("##@##")[2];
				// var docJsonData = result.split("##@##")[3];
				// var taskJsonData = result.split("##@##")[4];
				// var TransJsonData = result.split("##@##")[5];
				// var convJsonData = result.split("##@##")[6];

				var jsonData = "";
				// console.log(recJsonData);
				if (recJsonData.length != 0) {

					$(".recomRECListDiv").html("");
					jsonData = recJsonData;
					for (i = jsonData.length - 1; i >= 0; i--) {
						var UI = "";
						var t = "";
						var d = new Date();
						var time = jsonData[i].created_time;
						time = time.replace("CHR(26)", ":");

						var T = time.split("#@#")[1];
						var date = time.split("#@#")[0];

						var m = d.getMonth();
						m = months[m];
						var date1 = d.getDate();
						date1 = (date1 > 10) ? (date1) : ('0' + date1);
						var D = date1 + "-" + m + "-" + d.getFullYear();

						if (date == D) {
							t = "Today  " + T;
						} else {
							t = T + " , " + date;
						}

						var ioType = jsonData[i].io_type;
						var duration = jsonData[i].duration;
						var type = jsonData[i].type;
						var callType = jsonData[i].call_type;
						var messageType = jsonData[i].msg_type;
						//var message=jsonData[i].message.replace("CHR(26)",":");
						//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
						var message = jsonData[i].msg;
						var message = replaceSpecialCharacter(message);
						var body = textTolink(message);
						var fileExt = jsonData[i].file_ext;
						var file_id = jsonData[i].file_id;



						UI = '<div align="left" class="cmeTask"  onmouseover="showHLmoreoptions(this);event.stopPropagation();" onmouseout="hideHLmoreoptions(this);event.stopPropagation();"  style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;">'
								+ '<div class="row" style="float: left;display:flex;margin-left: 0px;font-size: 12px;color: #000;width: 100%;">'
									+ "<span class='defaultExceedCls ' title='" + message + "." + fileExt + "' src='" + jsonData[i].filePath + "' onclick='cmeopenVideo(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "</span>"
									// + "<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='Download(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/downloadBlue.png'></span>"

								+ '</div>'
								+ '<div class="chat-income-time" style="float: left;padding: 0px;width: 100%;">' + t + '</div>'

								UI +='<div id="hlfloatoptionsID" class="actFeedOptionsDiv d-none mt-3 mr-2" style="border-radius:8px;right: unset;left: 185px;">' 
										+'<div class="d-flex align-items-center">'
											+"<span  src='" + jsonData[i].filePath + "' onclick='cmeopenVideo(this)'  callType='" + callType + "' extenstion = '" + fileExt + "'>"
											+"<img src=\"images/conversation/expand.svg\" title=\"View\" id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:14px;\"></span>"
											+"<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='Download(this)'  callType='" + callType + "' extenstion = '" + fileExt + "'>"
											+'<img src="images/conversation/download2.svg" title="Download" id="" class="image-fluid cursor" style="margin: 0px 6px;height: 15px;width:18px;"></span>'
										+'</div>'
								+'</div>'
							+ '</div>';

						$(".recomRECListDiv").append(UI);

					}
				} else {
					$('.recomRECListDiv').html('<span style="font-size: 12px;">No data found.</span>');
				}
			}
		});
}

function fetchRecomdWB(convId, type){
	tid = convId;
	ttype = type;
	//console.log(tid+"--tid--ttype--"+ttype);
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	// $("#loadingBar").show();
	// timerControl("start");
	// $("#loadMsg").hide();

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"user_id": userIdglb,
		"convId" : convId,
		"localTZ": localOffsetTime,
		"type" : type,
		"type1" : "whiteBoards"
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchRecomdData",
		// url: "http://localhost:8080/v1/fetchRecomdData",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			// if (result != "SESSION_TIMEOUT") {
				var wbJsonData = result;
				// var docJsonData = result.split("##@##")[3];
				// var taskJsonData = result.split("##@##")[4];
				// var TransJsonData = result.split("##@##")[5];
				// var convJsonData = result.split("##@##")[6];

				var jsonData = "";
				// console.log(wbJsonData);

				if (wbJsonData.length != 0) {
					$(".recomWBListDiv").html("");
					jsonData = wbJsonData;
					for (i = jsonData.length - 1; i >= 0; i--) {
						var UI = "";
						var t = "";
						var d = new Date();
						var time = jsonData[i].created_time;
						time = time.replace("CHR(26)", ":");

						var T = time.split("#@#")[1];
						var date = time.split("#@#")[0];

						var m = d.getMonth();
						m = months[m];
						var date1 = d.getDate();
						date1 = (date1 > 10) ? (date1) : ('0' + date1);
						var D = date1 + "-" + m + "-" + d.getFullYear();

						if (date == D) {
							t = "Today  " + T;
						} else {
							t = T + " , " + date;
						}

						var ioType = jsonData[i].io_type;
						var duration = jsonData[i].duration;
						var type = jsonData[i].type;
						var callType = jsonData[i].call_type;
						var messageType = jsonData[i].msg_type;
						//var message=jsonData[i].message.replace("CHR(26)",":");
						//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
						var message = jsonData[i].msg;
						var message = replaceSpecialCharacter(message);
						var body = textTolink(message);
						var fileExt = jsonData[i].file_ext;
						var file_id = jsonData[i].file_id;



						UI = '<div align="left" class="cmeTask"  onmouseover="showHLmoreoptions(this);event.stopPropagation();" onmouseout="hideHLmoreoptions(this);event.stopPropagation();" style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;">'
								+ '<div class="row" style="float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;display: flex;">'
									+ "<a target='_blank' class='defaultExceedCls' style='color: #000;' onclick='expandImage(\"" + jsonData[i].file_id + "\",\"" + fileExt + "\",\"Document\");' >" + message + "</a>"
									// + "<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadNew(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/downloadBlue.png'></span>"
								+ '</div>'
								+ '<div class="chat-income-time" style="float: left;padding: 0px;width: 100%;">' + t + '</div>'

								UI +='<div id="hlfloatoptionsID" class="actFeedOptionsDiv d-none mt-3 mr-2" style="border-radius:8px;right: unset;left: 203px;">' 
										+'<div class="d-flex align-items-center">'
											+"<img src=\"images/conversation/expand.svg\" title=\"View\" onclick='expandImage(\"" + jsonData[i].file_id + "\",\"" + fileExt + "\",\"Document\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
											+"<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadNew(this)'  callType='" + callType + "' extenstion = '" + fileExt + "'>"
											+'<img src="images/conversation/download2.svg" title="Download" id="" class="image-fluid cursor" style="margin: 0px 6px;height: 15px;width:18px;"></span>'
										+'</div>'
								+'</div>'
							+ '</div>';

						$(".recomWBListDiv").append(UI);

					}
				} else {
					$(".recomWBListDiv").html('<span style="font-size: 12px;">No data found.</span>');
				}
		}
	});
}

function fetchRecomdDoc(convId, type) {
	tid = convId;
	ttype = type;
	//console.log(tid+"--tid--ttype--"+ttype);
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	// $("#loadingBar").show();
	// timerControl("start");
	// $("#loadMsg").hide();

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"user_id": userIdglb,
		"convId" : convId,
		"localTZ": localOffsetTime,
		"type" : type,
		"type1" : "documents"
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchRecomdData",
		// url: "http://localhost:8080/v1/fetchRecomdData",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			// if (result != "SESSION_TIMEOUT") {
				var docJsonData = result;
				

				var jsonData = "";
				// console.log(docJsonData);

				if (docJsonData.length != 0) {
					$(".recomDOCListDiv").html("");
					jsonData = docJsonData;
					for (i = jsonData.length - 1; i >= 0; i--) {
						var UI = "";
						var t = "";
						var d = new Date();
						var time = jsonData[i].created_time;
						time = time.replace("CHR(26)", ":");

						var T = time.split("#@#")[1];
						var date = time.split("#@#")[0];

						var m = d.getMonth();
						m = months[m];
						var date1 = d.getDate();
						date1 = (date1 > 10) ? (date1) : ('0' + date1);
						var D = date1 + "-" + m + "-" + d.getFullYear();

						if (date == D) {
							t = "Today  " + T;
						} else {
							t = T + " , " + date;
						}

						var ioType = jsonData[i].io_type;
						var duration = jsonData[i].duration;
						var type = jsonData[i].type;
						var callType = jsonData[i].call_type;
						var messageType = jsonData[i].msg_type;
						//var message=jsonData[i].message.replace("CHR(26)",":");
						//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
						var message = jsonData[i].msg;
						var message = replaceSpecialCharacter(message);
						var body = textTolink(message);
						var fileExt = jsonData[i].file_ext;
						var file_id = jsonData[i].file_id;

						UI = '<div align="left" class="cmeTask"  onmouseover="showHLmoreoptions(this);event.stopPropagation();" onmouseout="hideHLmoreoptions(this);event.stopPropagation();" style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;">'
								+ '<div class="row" style="float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;display: flex;">'
									+ "<a target='_blank' class='defaultExceedCls' style='color: #000;' href='" + jsonData[i].filePath + "' >" + message + "</a>"
									// + "<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadDoc(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/downloadBlue.png'></span>"
								+ '</div>'
								+ '<div class="chat-income-time" style="float: left;padding: 0px;width: 100%;">' + t + '</div>'

								if(fileExt == "png"){
									UI +='<div id="hlfloatoptionsID" class="actFeedOptionsDiv d-none mt-3 mr-2" style="border-radius:8px;right: unset;left: 236px;">' 
											+'<div class="d-flex align-items-center">'
												+"<img src=\"images/conversation/expand.svg\" title=\"View\" onclick='expandImage(\"" + jsonData[i].file_id + "\",\"" + fileExt + "\",\"Document\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
												+"<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadNew(this)'  callType='" + callType + "' extenstion = '" + fileExt + "'>"
												+'<img src="images/conversation/download2.svg" title="Download" id="" class="image-fluid cursor" style="margin: 0px 6px;height: 15px;width:18px;"></span>'
											+'</div>'
									+'</div>'
								}
								else if(fileExt == "webm"){
									UI +='<div id="hlfloatoptionsID" class="actFeedOptionsDiv d-none mt-3 mr-2" style="border-radius:8px;right: unset;left: 236px;">' 
											+'<div class="d-flex align-items-center">'
												+"<span  src='" + jsonData[i].filePath + "' onclick='cmeopenVideo(this)'  callType='" + callType + "' extenstion = '" + fileExt + "'>"
												+"<img src=\"images/conversation/expand.svg\" title=\"View\" id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:14px;\"></span>"
												+"<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadDoc(this)'  callType='" + callType + "' extenstion = '" + fileExt + "'>"
												+'<img src="images/conversation/download2.svg" title="Download" id="" class="image-fluid cursor" style="margin: 0px 6px;height: 15px;width:18px;"></span>'
											+'</div>'
									+'</div>'
								}
								else{
									UI +='<div id="hlfloatoptionsID" class="actFeedOptionsDiv d-none mt-3 mr-2" style="border-radius:8px;right: unset;left: 236px;">' 
											+'<div class="d-flex align-items-center">'
											+"<img src=\"images/conversation/expand.svg\" title=\"View\" onclick='viewdocument(\"" + jsonData[i].file_id + "\",\"" + fileExt + "\",\"Document\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
											+"<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadTxt(this)'  callType='" + callType + "' extenstion = '" + fileExt + "'>"
											+'<img src="images/conversation/download2.svg" title="Download" id="" class="image-fluid cursor" style="margin: 0px 6px;height: 15px;width:18px;"></span>'
											+'</div>'
									+'</div>'
								}
						UI += '</div>';

						$(".recomDOCListDiv").append(UI);

					}
				} else {
					$(".recomDOCListDiv").html('<span style="font-size: 12px;">No data found.</span>');
				}
		}
	});
}

function fetchRecomdTask(convId, type) {
	tid = convId;
	ttype = type;
	// console.log(tid+"--tid--ttype--"+ttype);
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	// $("#loadingBar").show();
	// timerControl("start");
	// $("#loadMsg").hide();

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"user_id": userIdglb,
		"convId" : convId,
		"localTZ": localOffsetTime,
		"type" : type,
		"type1" : "tasks"
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchRecomdData",
		// url: "http://localhost:8080/v1/fetchRecomdData",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			// console.log(result);
			// if (result != "SESSION_TIMEOUT") {
				var taskJsonData = result;

				var jsonData = "";
				// console.log(taskJsonData);

				if (taskJsonData.length != 0) {
					$(".recomTASKListDiv").html("");
					jsonData = taskJsonData;
					for (i = jsonData.length - 1; i >= 0; i--) {
						var UI = "";

						var taskId = jsonData[i].task_id;
						var tName = jsonData[i].task_name;
						var source_id = jsonData[i].source_id;
						var taskImage = "";
						var taskImageTitle = "";
						var taskStatusImageTitle = jsonData[i].taskStatusImageTitle;
						var taskStatusImage = path + '/' + jsonData[i].taskStatusImage;
						var taskSub = jsonData[i].task_name;
						// taskSub = replaceSpecialCharacter(taskSub);
						// taskSub = unicodeTonotificationValue(taskSub);
						var sentimentBgCol = "";
						var trending = jsonData[i].trending;
						var userTaskStatus = jsonData[i].user_task_status;
						var taskStartDate = jsonData[i].start_date;
						var taskEndDate = jsonData[i].end_date;
						var projectid = jsonData[i].project_id;
						var taskType =jsonData[i].task_type; 

						if (trending >= -1 && trending <= -0.49) {
							sentimentBgCol = path + '/images/temp/meterred.png';
						} else if (trending >= -0.50 && trending < 0) {
							sentimentBgCol = path + '/images/temp/meteryellow.png';
						} else if (trending == 0 || trending == 0.0) {
							sentimentBgCol = path + '/images/temp/meteryellow_center.png';
						} else if ((trending > 0 || trending > 0.0) && trending <= 0.49) {
							sentimentBgCol = path + '/images/temp/meteryellow_Lightgreen.png';
						} else if (trending >= 0.5 && trending <= 1) {
							sentimentBgCol = path + '/images/temp/meter_fullgreen.png';
						} else {
							trending = 'No Data';
							sentimentBgCol = path + '/images/temp/meterDefault.png';
						}

						var statusImg = "";
						var userStatus = "";
						if (userTaskStatus == "Created") {
							userStatus = "Created";
							statusImg = path + "/images/temp/task_created.svg";
						} else if (userTaskStatus == "Inprogress") {
							userStatus = "Inprogress";
							statusImg = path + "/images/temp/task_inprogress.svg";
						} else if (userTaskStatus == "Paused") {
							userStatus = "Paused";
							statusImg = path + "/images/temp/task_paused.svg";
						} else if (userTaskStatus == "Completed") {
							userStatus = "Completed";
							statusImg = path + "/images/temp/task_completed.svg";
						} else {
							statusImg = path + "/images/temp/task_nostatus.svg";
						}

						if(taskType == 'Tasks' && source_id == "0" ){
							taskImage = "images/task/task.svg";
							taskImageTitle = "Task";
						  }else if(taskType == 'Tasks' || taskType == "Assigned Task"){
							taskImage = "images/task/convo-task.svg";
							taskImageTitle = "Conversation Task";
						  }else if(taskType == "WorkspaceDocument" || taskType == 'Document'){
							taskImage = "images/task/doc-task.svg";
							taskImageTitle = "Document Task";
						  }else if(taskType == 'Idea'){
							taskImage = "images/task/idea-task.svg";
							taskImageTitle = "Idea Task";
						  }else if(taskType == 'Sprint' || taskType == 'Sprint Task'){
							taskImage = "images/task/sprint-task.svg";
							taskImageTitle = "Sprint Task";
						  }else if(taskType == 'Highlight'){
							taskImage = "images/task/task.svg";
							taskImageTitle = "Highlight Task";
						  }else if(taskType == 'Email'){
							taskImage = "images/task/email-task.svg";
							taskImageTitle = "Email Task";
						  }else if(taskType == 'Event'){
							taskImage = "images/task/event.svg";
							taskImageTitle = "Event";
						  }else if(taskType == 'Workflow' && (taskType == "MyZone")){
							taskImage = "images/task/workflow-task.svg";
							taskImageTitle = "Workflow Task";
						  }else if(taskType == 'Workflow'){
							taskImage = "images/task/workflow.svg";
							taskImageTitle = "Workflow Task";
						  }else{
							taskImage = "images/task/task.svg";
							taskImageTitle = "Task";
						  }

						/*UI='<div class="row " style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;border-bottom: 1px solid #bfbfbf;" align="left">'
								   +"<div class=\"taskId_"+taskId+"\" onclick=\"newUicreateTaskUI('update','"+taskId+"','"+tName+"','"+source_id+"','','','','','highlighttasklist');event.stopPropagation();\" >"
								+"<div class=\"row \" style=\"float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;cursor:pointer;\" >"		//fetchRecomdnTaskDetailsView('+taskId+')	newUicreateTaskUI('update','"+data[i].htaskid+"', 'Highlight','"+data[i].hid+"','"+data[i].hmsgid+"','','','','highlightTask')	
									+'<div class="tabContentHeaderName" align="left" style="padding-top: 4px;width: 10%;"><img  title="'+taskImageTitle+'" src="'+taskImage+'" class="img-responsive taskimgClss" style=""> </div>'
										+'<div style="padding-top: 5px;width:80%;padding-left: 0px;font-size: 12px;" title="'+taskSub+'" class="col-sm-6 col-xs-6 tabContentHeaderName defaultExceedCls">'+taskSub+'</div>'
									+'<div style="padding-top: 0px;width: 10%;float:right;" class="tabContentHeaderName defaultExceedCls"> <img title="'+taskStatusImageTitle+'" style="width:20px;float:right;" class="img-responsive" src="'+taskStatusImage+'"></div>'
									+'</div>'
								+"<div class=\"row\" style=\"float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;cursor:pointer;\" >"		//fetchRecomdnTaskDetailsView('+taskId+')	newUicreateTaskUI('update','"+data[i].htaskid+"', 'Highlight','"+data[i].hid+"','"+data[i].hmsgid+"','','','','highlightTask')	
									+'<div class="tabContentHeaderName" align="left" style="padding-top: 4px;width: 10%;">'
									 if(userStatus != ""){
									   UI+="<img title=\"\" id=\"taskPlay_"+taskId+"\" onclick=\"changeStatusOfUserTask("+taskId+","+projectid+",'listView','highlighttasklist');event.stopPropagation();\" src=\""+statusImg+"\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 15px;margin-top: 2px;margin-left: 3px;\" class=\"img-responsive\">"
									 }else if(userStatus == ""){
									   UI+="<img title=\"\" id=\"\" src=\""+statusImg+"\" style = \"width: 20px;margin-top: 2px;cursor: default;margin-left: 3px;\" class=\"img-responsive\">"
									 }
									UI+="</div>"
										+'<div style="padding-top: 5px;height:100%;width:42%;padding-left: 0px;font-size: 12px;" title="'+taskStartDate+'" class="col-sm-6 col-xs-6 tabContentHeaderName defaultExceedCls">'+taskStartDate+'</div>'
										+'<div style="padding-top: 5px;height:100%;width:42%;padding-left: 0px;font-size: 12px;" title="'+taskEndDate+'" class="col-sm-6 col-xs-6 tabContentHeaderName defaultExceedCls">'+taskEndDate+'</div>'
									+'<div style="width:10%;padding-top: 0px;padding-left:9px;padding-right:0px;max-width:40px;float:right;" class="col-sm-3 col-xs-3 tabContentHeaderName defaultExceedCls" align="center"><img title="'+trending+'" src="'+sentimentBgCol+'" style="width:22px;height:23px;"></div>'
								    
								   +'</div>'
								   +"<div id=\"TaskStatus_"+taskId+"\" class=\"epicCommonListDivCss TaskCommentContainer\" type=\"highlight\" style=\"padding: 5px 15px 0px;float: left;width: 100%;margin-left: 0px;margin-right: 0px;margin-bottom: 0px;border-radius: 0px;display: none;overflow: hidden;\">"
								   +"<div class=\"TaskStatusMainTextareaDiv\" style=\"float:left;width:100%;padding-right: 20px;padding-bottom: 5px;\">"
								   +"</div>"
										 +"</div>"
							 +"</div>"
								 +'</div>'*/

						UI = '<div class="cmeTask row" style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;" align="left">'
							+ "<div class=\"row taskId_" + taskId + "\" onclick=\"newUicreateTaskUI('update','" + taskId + "','" + tName + "','" + source_id + "','','','','','highlighttasklist');event.stopPropagation();\" style=\"float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;cursor:pointer;\">"
							+ '<div class="tabContentHeaderName" align="left" style="padding-top: 4px;padding-left: 3px;width: 10%;">'
						if (userStatus != "") {
							UI += "<img title=\"\" id=\"taskPlay_" + taskId + "\" onclick=\"changeStatusOfUserTask(" + taskId + "," + projectid + ",'listView','highlighttasklist');event.stopPropagation();\" src=\"" + statusImg + "\" statusOfTaskUser=\"" + userStatus + "\" style = \"width: 15px;margin-top: 2px;margin-left: 3px;\" class=\"img-responsive\">"
						} else if (userStatus == "") {
							UI += "<img title=\"\" id=\"\" src=\"" + statusImg + "\" style = \"width: 20px;margin-top: 2px;cursor: default;margin-left: 3px;\" class=\"img-responsive\">"
						}
						UI += "</div>"
							+ '<div id="tksub" style="padding-top: 5px;height:100%;width:60%;padding-left: 0px;font-size: 12px;margin-left: -4% " title="' + taskSub + '" class="col-sm-6 col-xs-6 tabContentHeaderName defaultExceedCls">' + taskSub + '</div>'
							+ '<div style="width:15%;padding-top: 0px;padding-left:0px;padding-right:0px;max-width:40px;float:right;" class="col-sm-3 col-xs-3 tabContentHeaderName defaultExceedCls" align="center"><img title="' + trending + '" src="' + sentimentBgCol + '" style="width:25px;height:25px;"></div>'
							+ '<div style="padding-top: 4px;width: 15%;float:right;" class="tabContentHeaderName defaultExceedCls"> <img title="' + taskImageTitle + '" style="width:20px;float:right;" class="img-responsive" src="' + taskImage + '"></div>'
							+ '</div>'

							+ "<div id=\"TaskStatus_" + taskId + "\" class=\"epicCommonListDivCss TaskCommentContainer taskcss\" type=\"highlight\" style=\"padding: 5px 15px 0px;float: left;width: 100%;margin-left: 0px;margin-right: 0px;margin-bottom: 0px;border-radius: 0px;display: none;overflow: hidden;\">"
							+ "<div class=\"TaskStatusMainTextareaDiv\" style=\"float:left;width:100%;padding-right: 20px;padding-bottom: 5px;\">"
							+ "</div>"
							+ "</div>"

							+ '</div>';


						// if (tPlace == "highlighttasklist") {
							$(".recomTASKListDiv").append(UI);
						// } else {
						// 	$(".recomTASKListDiv").append(UI);
						// }


					}
				} else {
					$(".recomTASKListDiv").html('<span style="font-size: 12px;">No data found.</span>');
				}
			}
	});
}

function fetchRecomdConv(convId, type){
	tid = convId;
	ttype = type;
	//console.log(tid+"--tid--ttype--"+ttype);
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	// $("#loadingBar").show();
	// timerControl("start");
	// $("#loadMsg").hide();

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"user_id": userIdglb,
		"convId" : convId,
		"localTZ": localOffsetTime,
		"type" : type,
		"type1" : "conversations"
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchRecomdData",
		// url: "http://localhost:8080/v1/fetchRecomdData",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			// if (result != "SESSION_TIMEOUT") {
				var convJsonData = result;

				var jsonData = "";
				// console.log(convJsonData);

				if (convJsonData.length != 0) {
					$(".recomCONVListDiv").html("");
					jsonData = convJsonData;
					for (i = jsonData.length - 1; i >= 0; i--) {
						var UI = "";
						var subjName = jsonData[i].name;
						subjName = replaceSpecialCharacter(subjName);
						subjName = unicodeTonotificationValue(subjName);
						var projectName = jsonData[i].project_name;
						projectName = replaceSpecialCharacter(projectName);
						projectName = unicodeTonotificationValue(projectName);
						var projImgSrc = "";//lighttpdpath+"/projectimages/"+jsonData[i].notification_proj_img_path;
						var d = new Date();
						var userImg = lighttpdpath + "/userimages/" + jsonData[i].createdBy +"."+ jsonData[i].user_image_type+ "?" + d.getTime();;

						UI = "<div class='cmeTask' id=\"notifListMain_" + jsonData[i].notification_id + "\" style=\"border-bottom: 1px solid #CED2D5;margin-top: 5px;float:left;width:100%;\"> "
							+ "<div class=\"defaultCls\" style=\"float:left; vertical-align: middle;width:100%\">"
							+ "<div class=\"notfSubHeader\"  style=\"margin-top:2px;\">" + jsonData[i].notification_header + "</div>"
							+ "</div>"
							+ "<div style=\"margin-bottom: 8px;margin-top:5px;\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 defaultCls\">"
							+ "<div align=\"center\" class=\"defaultCls\" style=\"float: left;height: 45px;\">"
							+ "<img class=\"img-responsive notfProjIcon\" title=\"" + projectName + "\" src=\"" + projImgSrc + "\" onerror=\"javascript:imageOnProjNotErrorReplace(this);\">"
							+ "</div>"
							+ "<div class=\"defaultCls notfNameClsWid\" style=\"float: left; vertical-align: top;\">"
							+ "<span style=\"width:100%\" class=\"notfContentCls notfContentHover defaultExceedCls\" title=\"" + subjName + "\">" + subjName + "</span>"
							+ "<span style=\"float: left;\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 defaultCls defaultNameDateTimestamp\">" + jsonData[i].created_timestamp + "</span>"
							+ "<div style=\"float: left;margin: 0px 0px;vertical-align: middle;\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 defaultCls\">"
							+ "<img class=\"col-lg-2 col-md-2 col-sm-2 col-xs-1 defaultCls img-responsive notfUserImgIcon\" style=\"border-radius:50%;\" title=\"" + replaceSpecialCharacter(jsonData[i].createdName) + "\" src=\"" + userImg + "\" onerror=\"javascript:userImageOnErrorReplace(this);\">"
							+ "<span class=\"col-lg-10 col-md-9 col-sm-8 col-xs-9 defaultCls notfUserName\">" + replaceSpecialCharacter(jsonData[i].createdName) + "</span>"
							+ "</div>"
							+ "</div>"
							+ "</div>"
							+ "</div> ";

						$(".recomCONVListDiv").append(UI);

					}
				} else {
					$(".recomCONVListDiv").html('<span style="font-size: 12px;">No data found.</span>');
				}
		}
	});
}

function fetchRecomdTS(convId, type){
	tid = convId;
	ttype = type;
	//console.log(tid+"--tid--ttype--"+ttype);
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	// $("#loadingBar").show();
	// timerControl("start");
	// $("#loadMsg").hide();

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"user_id": userIdglb,
		"convId" : convId,
		"localTZ": localOffsetTime,
		"type" : type,
		"type1" : "transcript"
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchRecomdData",
		// url: "http://localhost:8080/v1/fetchRecomdData",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			// if (result != "SESSION_TIMEOUT") {
				var TransJsonData = result;
				var jsonData = "";
				// console.log(TransJsonData);

				if (TransJsonData.length != 0) {
					$(".recomTSListDiv").html("");
					jsonData = TransJsonData;
					//console.log("JsonData TransJsonData::==>"+jsonData);
					for (i = jsonData.length - 1; i >= 0; i--) {
						var UI = "";
						var t = "";
						var d = new Date();
						var time = jsonData[i].created_time;
						time = time.replace("CHR(26)", ":");

						var T = time.split("#@#")[1];
						var date = time.split("#@#")[0];

						var m = d.getMonth();
						m = months[m];
						var date1 = d.getDate();
						date1 = (date1 > 10) ? (date1) : ('0' + date1);
						var D = date1 + "-" + m + "-" + d.getFullYear();

						if (date == D) {
							t = "Today  " + T;
						} else {
							t = T + " , " + date;
						}

						var ioType = jsonData[i].io_type;
						var duration = jsonData[i].duration;
						var type = jsonData[i].type;
						var callType = jsonData[i].call_type;
						var messageType = jsonData[i].msg_type;
						//var message=jsonData[i].message.replace("CHR(26)",":");
						//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
						var message = jsonData[i].msg;
						if (message != null) {
							message = replaceSpecialCharacter(message);
						}
						var body = textTolink(message);
						var file_id = jsonData[i].file_id;
						var fileExt = jsonData[i].file_ext;



						UI = "<div align='left' class='cmeTask' onmouseover='showHLmoreoptions(this);event.stopPropagation();' onmouseout='hideHLmoreoptions(this);event.stopPropagation();' style='float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;'>"
							+ "<div class='row' style='float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;display: flex;'>"
							+ "<a style='color: #000;' class='defaultExceedCls' href='#' onclick='readTextFile(\"" + jsonData[i].file_id + "\",\"" + fileExt + "\",\"" + jsonData[i].filePath + "\",\"" + jsonData[i].cm_id + "\",\"" + message + "\");'>" + message + "</a>"
							// + "<a style='color: #000;' class='defaultExceedCls' href='#' onclick=\"viewdocument(" + file_id + ",'" + fileExt + "','Document');\" ;'>" + message + "</a>"
							// + "<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadTxt(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/downloadBlue.png'></span>"
							//+"<a target='_blank' style='color:#000;' href='"+jsonData[i].filePath+"' >"+message+"</a>"
							+ "</div>"
							+ "<div class='chat-income-time' style='float: left;padding: 0px;width: 100%;'>" + t + "</div>"

							UI +='<div id="hlfloatoptionsID" class="actFeedOptionsDiv d-none mt-3 mr-2" style="border-radius:8px;right: unset;left: 125px;">' 
										+'<div class="d-flex align-items-center">'
										+"<img src=\"images/conversation/expand.svg\" title=\"View\" onclick='readTextFile(\"" + jsonData[i].file_id + "\",\"" + fileExt + "\",\"" + jsonData[i].filePath + "\",\"" + jsonData[i].cm_id + "\",\"" + message + "\");event.stopPropagation();' id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
										+"<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadTxt(this)'  callType='" + callType + "' extenstion = '" + fileExt + "'>"
										+'<img src="images/conversation/download2.svg" title="Download" id="" class="image-fluid cursor" style="margin: 0px 6px;height: 15px;width:18px;"></span>'
										+'</div>'
								+'</div>'
							+ "</div>";

						$(".recomTSListDiv").append(UI);

					}
				} else {
					$(".recomTSListDiv").html('<span style="font-size: 12px;">No data found.</span>');
				}
			}
		});
}

function showHLmoreoptions(obj) {
	$(obj).find('#hlfloatoptionsID').removeClass('d-none').addClass('d-flex');
}

function hideHLmoreoptions(obj) {
	$(obj).find('#hlfloatoptionsID').removeClass('d-flex').addClass('d-none');
}

function fetchRecomdData(convId, type) {
	tid = convId;
	ttype = type;
	//console.log(tid+"--tid--ttype--"+ttype);
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"user_id": userIdglb,
		"convId" : convId,
		"localTZ": localOffsetTime,
		"type" : type,
		"type1" : "highlight"
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/fetchRecomdData",
		// url: "http://localhost:8080/v1/fetchRecomdData",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			// if (result != "SESSION_TIMEOUT") {
				var hlistJsonData = result;
				// var recJsonData = result.split("##@##")[1];
				// var wbJsonData = result.split("##@##")[2];
				// var docJsonData = result.split("##@##")[3];
				// var taskJsonData = result.split("##@##")[4];
				// var TransJsonData = result.split("##@##")[5];
				// var convJsonData = result.split("##@##")[6];

				var jsonData = "";
				// console.log(hlistJsonData.length);

				if (hlistJsonData.length != 0) {
					$(".recomHListDiv").html("");
					jsonData = hlistJsonData;
					for (i = jsonData.length - 1; i >= 0; i--) {
						var UI = "";
						var t = "";
						var d = new Date();
						var time = jsonData[i].created_time;
						time = time.replace("CHR(26)", ":");

						var T = time;
						var T = time.split("#@#")[1];
						var date = time.split("#@#")[0];

						var m = d.getMonth();
						m = months[m];
						var date1 = d.getDate();
						date1 = (date1 > 10) ? (date1) : ('0' + date1);
						var D = date1 + "-" + m + "-" + d.getFullYear();

						if (date == D) {
							t = "Today  " + T;
						} else {
							t = T + " , " + date;
						}

						var ioType = jsonData[i].io_type;
						var duration = jsonData[i].duration;
						var type = jsonData[i].type;
						var callType = jsonData[i].call_type;
						var messageType = jsonData[i].msg_type;
						//var message=jsonData[i].message.replace("CHR(26)",":");
						//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
						var message = jsonData[i].msg;
						var message = replaceSpecialCharacter(message);
						var body = textTolink(message);
						var fileExt = jsonData[i].file_ext;
						var file_id = jsonData[i].file_id;



						UI = '<div align="left" class="cmeTask"  onmouseover="showHLmoreoptions(this);event.stopPropagation();" onmouseout="hideHLmoreoptions(this);event.stopPropagation();" style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;">'
								+ '<div class="row" style="float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;display: flex;">'
									+ '<a style="color: #000;" class="defaultExceedCls" href="#" onclick="viewHighlight(' + jsonData[i].cm_id + ');">' + message + '.' + fileExt + '</a>'
									// + "<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadTxt(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/downloadBlue.png'></span>"
								+ '</div>'
								+ '<div class="chat-income-time" style="float: left;padding: 0px;width: 100%;">' + t + '</div>'

								UI +='<div id="hlfloatoptionsID" class="actFeedOptionsDiv d-none mt-3 mr-2" style="border-radius:8px;right: unset;left: 225px;">' 
										+'<div class="d-flex align-items-center">'
										+"<img src=\"images/conversation/expand.svg\" title=\"View\" onclick=\"viewHighlight(" + jsonData[i].cm_id + ");event.stopPropagation();\" id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:20px;width:14px;\">"
										+"<span src='" + jsonData[i].filePath + "' id='" + file_id + "' onclick='DownloadTxt(this)'  callType='" + callType + "' extenstion = '" + fileExt + "'>"
										+'<img src="images/conversation/download2.svg" title="Download" id="" class="image-fluid cursor" style="margin: 0px 6px;height: 15px;width:18px;"></span>'
										+'</div>'
								+'</div>'
							+ '</div>';

						$(".recomHListDiv").append(UI);

					}
				} else {
					$(".recomHListDiv").html('<span style="font-size: 12px;">No data found.</span>');
				}

				// if (recJsonData != '[]') {

				// 	$(".recomRECListDiv").html("");
				// 	jsonData = recJsonData;
				// 	for (i = jsonData.length - 1; i >= 0; i--) {
				// 		var UI = "";
				// 		var t = "";
				// 		var d = new Date();
				// 		var time = jsonData[i].time;
				// 		time = time.replace("CHR(26)", ":");

				// 		var T = time.split("#@#")[1];
				// 		var date = time.split("#@#")[0];

				// 		var m = d.getMonth();
				// 		m = months[m];
				// 		var date1 = d.getDate();
				// 		date1 = (date1 > 10) ? (date1) : ('0' + date1);
				// 		var D = date1 + "-" + m + "-" + d.getFullYear();

				// 		if (date == D) {
				// 			t = "Today  " + T;
				// 		} else {
				// 			t = T + " , " + date;
				// 		}

				// 		var ioType = jsonData[i].ioType;
				// 		var duration = jsonData[i].duration;
				// 		var type = jsonData[i].type;
				// 		var callType = jsonData[i].callType;
				// 		var messageType = jsonData[i].messageType;
				// 		//var message=jsonData[i].message.replace("CHR(26)",":");
				// 		//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
				// 		var message = jsonData[i].message;
				// 		var message = replaceSpecialCharacter(message);
				// 		var body = textTolink(message);
				// 		var fileExt = jsonData[i].fileExt;



				// 		UI = '<div align="left" class="row" style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;">'
				// 			+ '<div class="row" style="float: left;display:flex;margin-left: 0px;font-size: 12px;color: #000;width: 100%;">'
				// 			+ "<span class='defaultExceedCls ' title='" + message + "." + fileExt + "' src='" + jsonData[i].filePath + "' onclick='cmeopenVideo(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>"
				// 			+ "<span src='" + jsonData[i].filePath + "' id='" + message + "' onclick='Download(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/downloadBlue.png'></span>"

				// 			+ '</div>'
				// 			+ '<div class="chat-income-time" style="float: left;padding: 0px;width: 100%;">' + t + '</div>'
				// 			+ '</div>';

				// 		$(".recomRECListDiv").append(UI).hide();

				// 	}
				// } else {
				// 	$(".recomRECListDiv").html('<span style="font-size: 12px;">No data found.</span>').hide();
				// }
				// if (wbJsonData != '[]') {
				// 	$(".recomWBListDiv").html("");
				// 	jsonData = wbJsonData;
				// 	for (i = jsonData.length - 1; i >= 0; i--) {
				// 		var UI = "";
				// 		var t = "";
				// 		var d = new Date();
				// 		var time = jsonData[i].time;
				// 		time = time.replace("CHR(26)", ":");

				// 		var T = time.split("#@#")[1];
				// 		var date = time.split("#@#")[0];

				// 		var m = d.getMonth();
				// 		m = months[m];
				// 		var date1 = d.getDate();
				// 		date1 = (date1 > 10) ? (date1) : ('0' + date1);
				// 		var D = date1 + "-" + m + "-" + d.getFullYear();

				// 		if (date == D) {
				// 			t = "Today  " + T;
				// 		} else {
				// 			t = T + " , " + date;
				// 		}

				// 		var ioType = jsonData[i].ioType;
				// 		var duration = jsonData[i].duration;
				// 		var type = jsonData[i].type;
				// 		var callType = jsonData[i].callType;
				// 		var messageType = jsonData[i].messageType;
				// 		//var message=jsonData[i].message.replace("CHR(26)",":");
				// 		//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
				// 		var message = jsonData[i].message;
				// 		var message = replaceSpecialCharacter(message);
				// 		var body = textTolink(message);
				// 		var fileExt = jsonData[i].fileExt;



				// 		UI = '<div align="left" class="row" style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;">'
				// 			+ '<div class="row" style="float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;display: flex;">'
				// 			+ "<a target='_blank' class='defaultExceedCls' style='color: #000;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>"
				// 			+ "<span src='" + jsonData[i].filePath + "' id='" + message + "' onclick='DownloadNew(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/downloadBlue.png'></span>"
				// 			+ '</div>'
				// 			+ '<div class="chat-income-time" style="float: left;padding: 0px;width: 100%;">' + t + '</div>'
				// 			+ '</div>';

				// 		$(".recomWBListDiv").append(UI).hide();

				// 	}
				// } else {
				// 	$(".recomWBListDiv").html('<span style="font-size: 12px;">No data found.</span>').hide();
				// }

				// if (docJsonData != '[]') {
				// 	$(".recomDOCListDiv").html("");
				// 	jsonData = docJsonData;
				// 	for (i = jsonData.length - 1; i >= 0; i--) {
				// 		var UI = "";
				// 		var t = "";
				// 		var d = new Date();
				// 		var time = jsonData[i].time;
				// 		time = time.replace("CHR(26)", ":");

				// 		var T = time.split("#@#")[1];
				// 		var date = time.split("#@#")[0];

				// 		var m = d.getMonth();
				// 		m = months[m];
				// 		var date1 = d.getDate();
				// 		date1 = (date1 > 10) ? (date1) : ('0' + date1);
				// 		var D = date1 + "-" + m + "-" + d.getFullYear();

				// 		if (date == D) {
				// 			t = "Today  " + T;
				// 		} else {
				// 			t = T + " , " + date;
				// 		}

				// 		var ioType = jsonData[i].ioType;
				// 		var duration = jsonData[i].duration;
				// 		var type = jsonData[i].type;
				// 		var callType = jsonData[i].callType;
				// 		var messageType = jsonData[i].messageType;
				// 		//var message=jsonData[i].message.replace("CHR(26)",":");
				// 		//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
				// 		var message = jsonData[i].message;
				// 		var message = replaceSpecialCharacter(message);
				// 		var body = textTolink(message);
				// 		var fileExt = jsonData[i].fileExt;



				// 		UI = '<div align="left" class="row" style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;">'
				// 			+ '<div class="row" style="float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;display: flex;">'
				// 			+ "<a target='_blank' class='defaultExceedCls' style='color: #000;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>"
				// 			+ "<span src='" + jsonData[i].filePath + "' id='" + message + "' onclick='DownloadDoc(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/downloadBlue.png'></span>"
				// 			+ '</div>'
				// 			+ '<div class="chat-income-time" style="float: left;padding: 0px;width: 100%;">' + t + '</div>'
				// 			+ '</div>';

				// 		$(".recomDOCListDiv").append(UI).hide();

				// 	}
				// } else {
				// 	$(".recomDOCListDiv").html('<span style="font-size: 12px;">No data found.</span>').hide();
				// }

				// if (taskJsonData != '[]') {
				// 	$(".recomTASKListDiv").html("");
				// 	jsonData = taskJsonData;
				// 	for (i = jsonData.length - 1; i >= 0; i--) {
				// 		var UI = "";

				// 		var taskId = jsonData[i].taskId;
				// 		var tName = jsonData[i].tName;
				// 		var source_id = jsonData[i].source_id;
				// 		var taskImage = path + '/' + jsonData[i].taskImage;
				// 		var taskImageTitle = jsonData[i].taskImageTitle;
				// 		var taskStatusImageTitle = jsonData[i].taskStatusImageTitle;
				// 		var taskStatusImage = path + '/' + jsonData[i].taskStatusImage;
				// 		var taskSub = jsonData[i].taskSub;
				// 		taskSub = replaceSpecialCharacter(taskSub);
				// 		taskSub = unicodeTonotificationValue(taskSub);
				// 		var sentimentBgCol = "";
				// 		var trending = jsonData[i].trending;
				// 		var userTaskStatus = jsonData[i].userTaskStatus;
				// 		var taskStartDate = jsonData[i].taskStartDate;
				// 		var taskEndDate = jsonData[i].taskEndDate; projectid
				// 		var projectid = jsonData[i].projectid;

				// 		if (trending >= -1 && trending <= -0.49) {
				// 			sentimentBgCol = path + '/images/temp/meterred.png';
				// 		} else if (trending >= -0.50 && trending < 0) {
				// 			sentimentBgCol = path + '/images/tasksNewUI/meteryellow.png';
				// 		} else if (trending == 0 || trending == 0.0) {
				// 			sentimentBgCol = path + '/images/tasksNewUI/meteryellow_center.png';
				// 		} else if ((trending > 0 || trending > 0.0) && trending <= 0.49) {
				// 			sentimentBgCol = path + '/images/tasksNewUI/meteryellow_Lightgreen.png';
				// 		} else if (trending >= 0.5 && trending <= 1) {
				// 			sentimentBgCol = path + '/images/tasksNewUI/meter_fullgreen.png';
				// 		} else {
				// 			trending = 'No Data';
				// 			sentimentBgCol = path + '/images/tasksNewUI/meterDefault.png';
				// 		}

				// 		var statusImg = "";
				// 		var userStatus = "";
				// 		if (userTaskStatus == "Created") {
				// 			userStatus = "Created";
				// 			statusImg = path + "/images/task_created.svg";
				// 		} else if (userTaskStatus == "Inprogress") {
				// 			userStatus = "Inprogress";
				// 			statusImg = path + "/images/task_inprogress.svg";
				// 		} else if (userTaskStatus == "Paused") {
				// 			userStatus = "Paused";
				// 			statusImg = path + "/images/task_paused.svg";
				// 		} else if (userTaskStatus == "Completed") {
				// 			userStatus = "Completed";
				// 			statusImg = path + "/images/task_completed.svg";
				// 		} else {
				// 			statusImg = path + "/images/task_nostatus.svg";
				// 		}

				// 		/*UI='<div class="row " style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;border-bottom: 1px solid #bfbfbf;" align="left">'
				// 				   +"<div class=\"taskId_"+taskId+"\" onclick=\"newUicreateTaskUI('update','"+taskId+"','"+tName+"','"+source_id+"','','','','','highlighttasklist');event.stopPropagation();\" >"
				// 				+"<div class=\"row \" style=\"float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;cursor:pointer;\" >"		//fetchRecomdnTaskDetailsView('+taskId+')	newUicreateTaskUI('update','"+data[i].htaskid+"', 'Highlight','"+data[i].hid+"','"+data[i].hmsgid+"','','','','highlightTask')	
				// 					+'<div class="tabContentHeaderName" align="left" style="padding-top: 4px;width: 10%;"><img  title="'+taskImageTitle+'" src="'+taskImage+'" class="img-responsive taskimgClss" style=""> </div>'
				// 						+'<div style="padding-top: 5px;width:80%;padding-left: 0px;font-size: 12px;" title="'+taskSub+'" class="col-sm-6 col-xs-6 tabContentHeaderName defaultExceedCls">'+taskSub+'</div>'
				// 					+'<div style="padding-top: 0px;width: 10%;float:right;" class="tabContentHeaderName defaultExceedCls"> <img title="'+taskStatusImageTitle+'" style="width:20px;float:right;" class="img-responsive" src="'+taskStatusImage+'"></div>'
				// 					+'</div>'
				// 				+"<div class=\"row\" style=\"float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;cursor:pointer;\" >"		//fetchRecomdnTaskDetailsView('+taskId+')	newUicreateTaskUI('update','"+data[i].htaskid+"', 'Highlight','"+data[i].hid+"','"+data[i].hmsgid+"','','','','highlightTask')	
				// 					+'<div class="tabContentHeaderName" align="left" style="padding-top: 4px;width: 10%;">'
				// 					 if(userStatus != ""){
				// 					   UI+="<img title=\"\" id=\"taskPlay_"+taskId+"\" onclick=\"changeStatusOfUserTask("+taskId+","+projectid+",'listView','highlighttasklist');event.stopPropagation();\" src=\""+statusImg+"\" statusOfTaskUser=\""+userStatus+"\" style = \"width: 15px;margin-top: 2px;margin-left: 3px;\" class=\"img-responsive\">"
				// 					 }else if(userStatus == ""){
				// 					   UI+="<img title=\"\" id=\"\" src=\""+statusImg+"\" style = \"width: 20px;margin-top: 2px;cursor: default;margin-left: 3px;\" class=\"img-responsive\">"
				// 					 }
				// 					UI+="</div>"
				// 						+'<div style="padding-top: 5px;height:100%;width:42%;padding-left: 0px;font-size: 12px;" title="'+taskStartDate+'" class="col-sm-6 col-xs-6 tabContentHeaderName defaultExceedCls">'+taskStartDate+'</div>'
				// 						+'<div style="padding-top: 5px;height:100%;width:42%;padding-left: 0px;font-size: 12px;" title="'+taskEndDate+'" class="col-sm-6 col-xs-6 tabContentHeaderName defaultExceedCls">'+taskEndDate+'</div>'
				// 					+'<div style="width:10%;padding-top: 0px;padding-left:9px;padding-right:0px;max-width:40px;float:right;" class="col-sm-3 col-xs-3 tabContentHeaderName defaultExceedCls" align="center"><img title="'+trending+'" src="'+sentimentBgCol+'" style="width:22px;height:23px;"></div>'
								    
				// 				   +'</div>'
				// 				   +"<div id=\"TaskStatus_"+taskId+"\" class=\"epicCommonListDivCss TaskCommentContainer\" type=\"highlight\" style=\"padding: 5px 15px 0px;float: left;width: 100%;margin-left: 0px;margin-right: 0px;margin-bottom: 0px;border-radius: 0px;display: none;overflow: hidden;\">"
				// 				   +"<div class=\"TaskStatusMainTextareaDiv\" style=\"float:left;width:100%;padding-right: 20px;padding-bottom: 5px;\">"
				// 				   +"</div>"
				// 						 +"</div>"
				// 			 +"</div>"
				// 				 +'</div>'*/

				// 		UI = '<div class="row" style="float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;" align="left">'
				// 			+ "<div class=\"row taskId_" + taskId + "\" onclick=\"newUicreateTaskUI('update','" + taskId + "','" + tName + "','" + source_id + "','','','','','highlighttasklist');event.stopPropagation();\" style=\"float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;cursor:pointer;\">"
				// 			+ '<div class="tabContentHeaderName" align="left" style="padding-top: 4px;padding-left: 3px;width: 10%;">'
				// 		if (userStatus != "") {
				// 			UI += "<img title=\"\" id=\"taskPlay_" + taskId + "\" onclick=\"changeStatusOfUserTask(" + taskId + "," + projectid + ",'listView','highlighttasklist');event.stopPropagation();\" src=\"" + statusImg + "\" statusOfTaskUser=\"" + userStatus + "\" style = \"width: 15px;margin-top: 2px;margin-left: 3px;\" class=\"img-responsive\">"
				// 		} else if (userStatus == "") {
				// 			UI += "<img title=\"\" id=\"\" src=\"" + statusImg + "\" style = \"width: 20px;margin-top: 2px;cursor: default;margin-left: 3px;\" class=\"img-responsive\">"
				// 		}
				// 		UI += "</div>"
				// 			+ '<div id="tksub" style="padding-top: 5px;height:100%;width:60%;padding-left: 0px;font-size: 12px;margin-left: -4% " title="' + taskSub + '" class="col-sm-6 col-xs-6 tabContentHeaderName defaultExceedCls">' + taskSub + '</div>'
				// 			+ '<div style="width:15%;padding-top: 0px;padding-left:0px;padding-right:0px;max-width:40px;float:right;" class="col-sm-3 col-xs-3 tabContentHeaderName defaultExceedCls" align="center"><img title="' + trending + '" src="' + sentimentBgCol + '" style="width:25px;height:25px;"></div>'
				// 			+ '<div style="padding-top: 4px;width: 15%;float:right;" class="tabContentHeaderName defaultExceedCls"> <img title="' + taskStatusImageTitle + '" style="width:20px;float:right;" class="img-responsive" src="' + taskStatusImage + '"></div>'
				// 			+ '</div>'

				// 			+ "<div id=\"TaskStatus_" + taskId + "\" class=\"epicCommonListDivCss TaskCommentContainer taskcss\" type=\"highlight\" style=\"padding: 5px 15px 0px;float: left;width: 100%;margin-left: 0px;margin-right: 0px;margin-bottom: 0px;border-radius: 0px;display: none;overflow: hidden;\">"
				// 			+ "<div class=\"TaskStatusMainTextareaDiv\" style=\"float:left;width:100%;padding-right: 20px;padding-bottom: 5px;\">"
				// 			+ "</div>"
				// 			+ "</div>"

				// 			+ '</div>';


				// 		if (tPlace == "highlighttasklist") {
				// 			$(".recomTASKListDiv").append(UI);
				// 		} else {
				// 			$(".recomTASKListDiv").append(UI).hide();
				// 		}


				// 	}
				// } else {
				// 	$(".recomTASKListDiv").html('<span style="font-size: 12px;">No data found.</span>').hide();
				// }

				// if (convJsonData != '[]') {
				// 	$(".recomCONVListDiv").html("");
				// 	jsonData = convJsonData;
				// 	for (i = jsonData.length - 1; i >= 0; i--) {
				// 		var UI = "";
				// 		var subjName = jsonData[i].notification_name;
				// 		subjName = replaceSpecialCharacter(subjName);
				// 		subjName = unicodeTonotificationValue(subjName);
				// 		var projectName = jsonData[i].notification_proj_name;
				// 		projectName = replaceSpecialCharacter(projectName);
				// 		projectName = unicodeTonotificationValue(projectName);
				// 		var projImgSrc = "";//lighttpdpath+"/projectimages/"+jsonData[i].notification_proj_img_path;
				// 		var userImg = lighttpdpath + "/userimages/" + jsonData[i].notification_created_userImg;

				// 		UI = "<div id=\"notifListMain_" + jsonData[i].notification_id + "\" style=\"border-bottom: 1px solid #CED2D5;margin-top: 5px;float:left;width:100%;\"> "
				// 			+ "<div class=\"defaultCls\" style=\"float:left; vertical-align: middle;width:100%\">"
				// 			+ "<div class=\"notfSubHeader\"  style=\"margin-top:2px;\">" + jsonData[i].notification_header + "</div>"
				// 			+ "</div>"
				// 			+ "<div style=\"margin-bottom: 8px;margin-top:5px;\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 defaultCls\">"
				// 			+ "<div align=\"center\" class=\"defaultCls\" style=\"float: left;height: 45px;\">"
				// 			+ "<img class=\"img-responsive notfProjIcon\" title=\"" + projectName + "\" src=\"" + projImgSrc + "\" onerror=\"javascript:imageOnProjNotErrorReplace(this);\">"
				// 			+ "</div>"
				// 			+ "<div class=\"defaultCls notfNameClsWid\" style=\"float: left; vertical-align: top;\">"
				// 			+ "<span style=\"width:100%\" class=\"notfContentCls notfContentHover defaultExceedCls\" title=\"" + subjName + "\">" + subjName + "</span>"
				// 			+ "<span style=\"float: left;\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 defaultCls defaultNameDateTimestamp\">" + jsonData[i].notification_created_timestamp + "</span>"
				// 			+ "<div style=\"float: left;margin: 0px 0px;vertical-align: middle;\" class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 defaultCls\">"
				// 			+ "<img class=\"col-lg-2 col-md-2 col-sm-2 col-xs-1 defaultCls img-responsive notfUserImgIcon\" style=\"border-radius:50%;\" title=\"" + replaceSpecialCharacter(jsonData[i].notification_created_userName) + "\" src=\"" + userImg + "\" onerror=\"javascript:userImageOnErrorReplace(this);\">"
				// 			+ "<span class=\"col-lg-10 col-md-9 col-sm-8 col-xs-9 defaultCls notfUserName\">" + replaceSpecialCharacter(jsonData[i].notification_created_userName) + "</span>"
				// 			+ "</div>"
				// 			+ "</div>"
				// 			+ "</div>"
				// 			+ "</div> ";

				// 		$(".recomCONVListDiv").append(UI).hide();

				// 	}
				// } else {
				// 	$(".recomCONVListDiv").html('<span style="font-size: 12px;">No data found.</span>').hide();
				// }

				// if (TransJsonData != '[]') {
				// 	$(".recomTSListDiv").html("");
				// 	jsonData = TransJsonData;
				// 	//console.log("JsonData TransJsonData::==>"+jsonData);
				// 	for (i = jsonData.length - 1; i >= 0; i--) {
				// 		var UI = "";
				// 		var t = "";
				// 		var d = new Date();
				// 		var time = jsonData[i].time;
				// 		time = time.replace("CHR(26)", ":");

				// 		var T = time.split("#@#")[1];
				// 		var date = time.split("#@#")[0];

				// 		var m = d.getMonth();
				// 		m = months[m];
				// 		var date1 = d.getDate();
				// 		date1 = (date1 > 10) ? (date1) : ('0' + date1);
				// 		var D = date1 + "-" + m + "-" + d.getFullYear();

				// 		if (date == D) {
				// 			t = "Today  " + T;
				// 		} else {
				// 			t = T + " , " + date;
				// 		}

				// 		var ioType = jsonData[i].ioType;
				// 		var duration = jsonData[i].duration;
				// 		var type = jsonData[i].type;
				// 		var callType = jsonData[i].callType;
				// 		var messageType = jsonData[i].messageType;
				// 		//var message=jsonData[i].message.replace("CHR(26)",":");
				// 		//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
				// 		var message = jsonData[i].message;
				// 		if (message != null) {
				// 			message = replaceSpecialCharacter(message);
				// 		}
				// 		var body = textTolink(message);
				// 		var fileExt = jsonData[i].fileExt;



				// 		UI = "<div align='left' class='row' style='float:left;position:relative;width:100%;margin:0px;padding: 5px 0px;'>"
				// 			+ "<div class='row' style='float: left;margin-left: 0px;font-size: 12px;color: #000;width: 100%;display: flex;'>"
				// 			+ "<a style='color: #000;' class='defaultExceedCls' href='#' onclick='readTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ",&quot;" + message + "&quot;);'>" + message + "</a>"
				// 			+ "<span src='" + jsonData[i].filePath + "' id='" + message + "' onclick='DownloadTxt(this)'  callType='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/downloadBlue.png'></span>"
				// 			//+"<a target='_blank' style='color:#000;' href='"+jsonData[i].filePath+"' >"+message+"</a>"
				// 			+ "</div>"
				// 			+ "<div class='chat-income-time' style='float: left;padding: 0px;width: 100%;'>" + t + "</div>"
				// 			+ "</div>";

				// 		$(".recomTSListDiv").append(UI).hide();

				// 	}
				// } else {
				// 	$(".recomTSListDiv").html('<span style="font-size: 12px;">No data found.</span>');
				// }


				// $("#loadingBar").hide();
				// timerControl("");
			// }
		}
	});
}

function ToggleRecomdType(type, obj) {
	// console.log("inside ToggleRecomdType");
	if (type == "HL") {
		if ($(obj).parent().next('.recomHListDiv').is(':hidden')){
			$('.recomHListDiv').show();
		}
		else{
			$('.recomHListDiv').hide();
		}
	} else if (type == "REC") {
		if ($(obj).parent().next('.recomRECListDiv').is(':hidden')){
			fetchRecomdRec(vConvId, "call");
			$('.recomRECListDiv').show();
		}
		else{
			$('.recomRECListDiv').hide();
		}
	} else if (type == "WB") {
		if ($(obj).parent().next('.recomWBListDiv').is(':hidden')){
			fetchRecomdWB(vConvId, "call");
			$('.recomWBListDiv').show();
			globalwb = "wb";
		}
		else{
			$('.recomWBListDiv').hide();
			// globalwb = "";
		}
	} else if (type == "DOC") {
		if ($(obj).parent().next('.recomDOCListDiv').is(':hidden')){
			$('.recomDOCListDiv').show();
			fetchRecomdDoc(vConvId, "call");
			globalwb = "wb";
		}
		else{
			$('.recomDOCListDiv').hide();
			// globalwb = "";
		}
	} else if (type == "TASK") {
		if ($(obj).parent().next('.recomTASKListDiv').is(':hidden')){
			$('.recomTASKListDiv').show();
			fetchRecomdTask(vConvId, "chat");
		}
		else{
			$('.recomTASKListDiv').hide();
		}
	} else if (type == "CONV") {
		if ($(obj).parent().next('.recomCONVListDiv').is(':hidden')){
			$('.recomCONVListDiv').show();
			fetchRecomdConv(vConvId, "call");
		}
		else{
			$('.recomCONVListDiv').hide();
		}
	} else if (type == "TS") {
		if ($(obj).parent().next('.recomTSListDiv').is(':hidden')){
			fetchRecomdTS(vConvId, "call");
			$('.recomTSListDiv').show();
		}
		else{
			$('.recomTSListDiv').hide();
		}
	}
}

function BackFromRecomd() {
	$("#recomdContainer").hide();
	$("#hlightContainer").show();
	$(".recomHListDiv").html("");
	$(".recomRECListDiv").html("");
	$(".recomWBListDiv").html("");
	$(".recomDOCListDiv").html("");
	$('.recomTASKListDiv').html("");
	$('.recomTSListDiv').html("");
	$('.recomCONVListDiv').html("");
}

function BackFromTransRecomd() {
	$("#recomdContainerForTrans").hide();
	$("#transcriptContainer").show();
}

var myCalendar;
function manualTaskcreate(id) {
	$("div.hlDiv").hide();
	$('#hMainDiv').css('height', '100%');
	$("#hAddDiv").hide();
	$('#hl_' + id).show();
	$("div.hlDiv").find('div[id^=hlTaskContainerDiv_]').html("");
	$("div.hlDiv").find('div[id^=hlEditContainer_]').show();
	$("div.hlDiv").removeClass("hEditBg");
	$("div.hlDiv").find('img.hEditHide').show();

	//$('#hlEditContainer_'+id).hide();
	$('#hlTaskContainerDiv_' + id).html(prepareHighlightTaskUI(id)).show();
	$('#hl_' + id).addClass("hEditBg");
	$('#hl_' + id).find('img.hEditHide').hide();
	$('#hl_' + id).css({ 'border-left': '1px solid rgb(233, 230, 230)', 'border-right': '1px solid rgb(233, 230, 230)' });
	var d = new Date();
	var month = d.getMonth() + 1;
	var day = d.getDate();
	var currentDate = (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + '-' + d.getFullYear();
	var taskval = $('#hlightMsg_' + id).text();
	var taskDesc = $('#hlightDes_' + id).text();
	if (taskval == '' || typeof (taskval) == 'undefined') {
		taskval = '';
		$('#hlightTaskHeader').text("Highlight");
	} else {
		$('#hlightTaskName').val(taskval);
		$('#hlightTaskHeader').text(taskval);
	}
	$('div#chatTaskDivArea_' + id).show();

	$('#hlightTaskDesc').val(taskDesc);
	$('#hlightdatepicker').val(currentDate);
	$('#hlightdesc').hide();
	$('#hlightTimeNewUI').show();
	//$("div.hlDiv").attr('onmouseover','').attr('onmouseout','').css({'border-bottom':'','background-color':''});
	$('.hOptionDiv').hide();
	$("#recordStart").hide();
	fetchWorkspaceProjects('create', 'manualTask');
	myCalendar = new dhtmlXCalendarObject('hlightdatepicker');
	var today = new Date();
	myCalendar.setDateFormat("%m-%d-%Y");
	myCalendar.setSensitiveRange(today, null);
	myCalendar.hideTime();
	//fetchhlightTaskUsers('0');
}

function voiceTaskCreate(id) {
	commType = "hLightTaskSpeechType";
	$("div.hlDiv").hide();
	$('#hMainDiv').css('height', '100%');
	$("#hAddDiv").hide();
	$('#hl_' + id).show();
	$("div.hlDiv").find('div[id^=hlTaskContainerDiv_]').html("");
	$("div.hlDiv").find('div[id^=hlEditContainer_]').show();
	$("div.hlDiv").removeClass("hEditBg");
	$("div.hlDiv").find('img.hEditHide').show();
	$('#hl_' + id).css({ 'border-left': '1px solid rgb(233, 230, 230)', 'border-right': '1px solid rgb(233, 230, 230)' });

	//$('#hlEditContainer_'+id).hide();
	$('#hlTaskContainerDiv_' + id).html(prepareHighlightTaskUI(id)).show();
	$('#hl_' + id).addClass("hEditBg");
	$('#hl_' + id).find('img.hEditHide').hide();
	$('#hlTaskContainerDiv_' + id).find('div.speechRecDiv, img.speechRecImg').show();
	$('#query-btn-next').trigger("click");

	var d = new Date();
	var month = d.getMonth() + 1;
	var day = d.getDate();
	var currentDate = (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + '-' + d.getFullYear();
	var taskval = $('#hlightMsg_' + id).text();
	var taskDesc = $('#hlightDes_' + id).text();
	if (taskval == '' || typeof (taskval) == 'undefined') {
		taskval = '';
		$('#hlightTaskHeader').text("Highlight");
	} else {
		$('#hlightTaskName').val(taskval);
		$('#hlightTaskHeader').text(taskval);
	}
	$('div#chatTaskDivArea_' + id).show();

	$('#hlightTaskDesc').val(taskDesc);
	$('#hlightdatepicker').val(currentDate);
	$('#hlightdesc').hide();
	$('#hlightTimeNewUI').show();
	//$("div.hlDiv").attr('onmouseover','').attr('onmouseout','').css({'border-bottom':'','background-color':''});
	$('.hOptionDiv').hide();
	//$("#recordStart").show();
	//$('#recordStart').css('box-shadow','0 0 7px 4px red');
	fetchWorkspaceProjects('create', 'voiceTask');
	myCalendar = new dhtmlXCalendarObject('hlightdatepicker');
	var today = new Date();
	myCalendar.setDateFormat("%m-%d-%Y");
	myCalendar.setSensitiveRange(today, null);
	myCalendar.hideTime();
	//fetchhlightTaskUsers('0');
	//toggleStartStop();
}


function fetchhlightTaskUsers(projId) {
	//var projId = $("#hlightproject option:selected").attr('id');
	$.ajax({
		url: path + '/calenderAction.do',
		type: "POST",
		data: { act: 'fetchParticipants', projId: projId, userId: userIdglb, device: 'web' },
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$("#loadingBar").hide();
			timerControl("");
		},
		success: function (result) {
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				//parent.checkSessionTimeOut(result);
				$('#highlightuserlist').html(result);
				$('#highlightuserlist').append('<div id="nonePartList" style="float:left;display:none;color:rgb(0, 0, 0);">No Participants Found</div>');
				$('#highlightuserlist').find("div[id^=particpantName_]").css('color', '#000');
			}
		}
	});


}


function showdhtmlContainer(obj) {
	$("div.dhtmlxcalendar_container").show();
	$("div.dhtmlxcalendar_container").css('display', 'block').css('background-color', 'white').css('border', '1px solid blue');
	$("div.dhtmlxcalendar_container").css("top", $(obj).offset().top + 6).css("left", $(obj).offset().left);
	$(".dhtmlxcalendar_time_cont").hide();
	myCalendar.hideTime();
}
function addTaskParticipants() {
	// $('#saveAndCloseTimeAndDate').hide();
	$('#projListDDContainer').hide();
	$('#saveAndCloseTimeAndDate').css('margin-top', '100px');
	$('#highlightuserlist').show();
	hideAddedParticipantsInList();
}
/*function addParticipantEmailDb(obj){		 //------>used in taskuicodenew.js
	var id = $(obj).attr('id').split("_")[1];
	 var name = $(obj).text();
	 commonUIForUserList(id,name);
}*/
function commonUIForUserList(id, name) {

	var UI = "<li id='user_" + id + "' class='prjUserCls' style='font-size:11px;height:24px;margin:2px;'>"
		+ "<div style='width:auto; float:left; font-size:11px;'>" + name + "</div>"
		+ "<div style='float: left; width: auto; margin-right: 10px; margin-left: 6px;'><img onclick='removeUserinCme(" + id + ",\"" + name + "\")' email ='' src='" + path + "/images/close.png' style='width: 7px; height: 7px; cursor: pointer;float:left;margin-top:5px;'></div>"
		+ "</li>";

	//$('#userforProj').hide();
	$('#hlightAddedUserList ul').append(UI);
	$('#highlightuserlist').find('#participant_' + id).hide();
}

function removeUserinCme(id, name) {
	$('#hlightAddedUserList ul').children('#user_' + id).remove();
	$('#highlightuserlist').find('#participant_' + id).show();
	deleteId = name + "_" + id;
	removeEmailId = removeEmailId + deleteId + ',';
}
function showProjectListViewNewUI(obj) {
	$('#projListDDContainer').show();
}
/*function prepareUserXml(){		 //------>used in taskuicodenew.js
	var userXml = "<userXml>"
	$('#hlightAddedUserList').find('li[id^=user_]').each(function(){
		var id = $(this).attr('id').split('_')[1];
		userXml += "<user>";
		userXml += "<id>"+id+"</id>";
		userXml += "<status>N</status>";
		//userXml += "<progress>"+$('#taskAmount_'+id).text()+"</progress>";
		userXml += "<progress>0</progress>";
		userXml += "<estHr>0</estHr>";
		userXml += "<estMin>0</estMin>";
		userXml += "<userAction>Created</userAction>";
		userXml += "</user>";
	});
	userXml += "</userXml>"
	return userXml;
}*/

function addHighlightTask(hid) {
	var d = new Date();
	var month = d.getMonth() + 1;
	var day = d.getDate();
	var currentDate = (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + '-' + d.getFullYear();
	var tName = $('#hlightTaskName').val().trim();
	if (tName == '') {
		$('#hlightTaskName').css('border', '1px solid red');
	} else {
		$('#hlightTaskName').css('border', '1px solid #D1D7D7')
	}

	var tDesc = $('#hlightTaskDesc').val();
	var projId = $("#hlightproject").val();
	var end = $('#hlightdatepicker').val();
	var start = currentDate;
	var priority = $("#priority option:selected").attr('value');
	var userXml = prepareUserXml();
	if (tName != '') {
		$.ajax({
			url: path + '/calenderAction.do',
			type: "POST",
			data: {
				act: 'addtask', title: tName, description: tDesc, start: start, sourceId: hid, end: end, taskProject: projId, priorityId: priority, taskType: 'Highlight', userXml: userXml, device: 'web'

			},
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				$("#loadingBar").hide();
				timerControl("");
			},
			success: function (result) {
				sessionTimeOutMethod(result);
				if (result != "SESSION_TIMEOUT") {
					//checkSessionTimeOut(result);
					chatAlertFun('Task Created Successfully', 'warning');
					textToVoiceSynthesizer('Task Created Successfully');
					$('.chatTaskHightCls').hide();
					validaterId = 0;
					$('#hl_' + hid).find('img.hTaskIcon').attr('title', 'View Task').attr('src', path + '/images/temp/htask2.png').attr('onclick', 'fetchHighlightTaskDetailsView(' + hid + ')');
					cancelHighLightTask(hid);
					recognizationToggleStop();
					var jsonText = '{"calltype":"' + callType + '","type":"videohighlight","status":"task","callid":"' + callId + '","hId":"' + hid + '"}';
					var message = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);; // preapring the chat stanza to send		 
					serverConnection.send(message);  // sending the highlight message 
					sendEmailToTaskUser(userXml, result.split("-@@##$$-")[1], projId);
				}
			}
		});
	}


}

/* function refreshHighlightTask(hid){
		textToVoiceSynthesizer('Task Created Successfully');
		$('.chatTaskHightCls').hide();
		validaterId = 0;
		cancelHighLightTask(hid);
		recognizationToggleStop();
		var jsonText = '{"calltype":"'+callType+'","type":"videohighlight","status":"task","callid":"'+callId+'","hId":"'+hid+'"}';
		var message = $msg({to : NameforChatRoom+'@conference.'+cmeDomain+'.colabus.com', "type" : "groupchat","roomType":"conference"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);; // preapring the chat stanza to send		 
		serverConnection.send(message);  // sending the highlight message 
 } */

function sendEmailToTaskUser(userXml, taskId, projId) {

	$.ajax({
		url: path + '/calenderAction.do',
		type: "POST",
		data: {
			act: 'sendEmailToTaskUser', taskId: taskId, taskProject: projId, userXml: userXml, device: 'web'

		},
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$("#loadingBar").hide();
			timerControl("");
		},
		success: function (result) {
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				//checkSessionTimeOut(result);
			}

		}
	});

}


function cancelHighLightTask(id) {
	validaterId = 0;
	$("div.hlDiv").find('div[id^=chatTaskDivArea_]').hide();
	$('#voiceHLightTaskHidden').val('');
	$("div.hlDiv").find('#hlEditContainer_' + id).show();
	$('#hl_' + id).css({ 'border-left': 'none', 'border-right': 'none' });
	//$("div.hlDiv").attr('onmouseover','showHlOptions('+$('#hl_'+id)').attr('onmouseout','hideHlOptions('+$('#hl_'+id)').css({'border-bottom':'','background-color':''});
	$('#hl_' + id).removeClass("hEditBg");
	$('#hl_' + id).find('img.hEditHide').show();
	$('.hlDiv').show();
	$('#hMainDiv').css('height', '70%');
	$("#hAddDiv").show();
	recognizationToggleStop();
}

/*Manual task creation in highlights common method*/

function prepareHighlightTaskUI(hid) {
	var UI = "";
	UI += "<div id='chatTaskDivArea_" + hid + "' class='chatTaskHightCls' style='display:none;width:100%;height:auto;float:left;font-size:12px;'>"
		+ "<input type=\"hidden\" id=\"voiceHLightTaskHidden\" value=\"hLightVoiceTaskHidden\">"
		+ '<div style="width: 100%;padding: 8px 10px;background-color: rgb(25, 73, 96);float: left;color: #fff;font-size: 13px;">Task</div>'
		+ '<div style="width: 100%;float: left;padding: 0px 10px;">'
		/*
		+'<div style="float:left;padding: 8px 0px 4px 0px;width:100%;border-bottom: 1px solid #D1D7D7;margin-bottom: 10px;">'
			+'<span id="hlightTaskHeader" >Highlight</span>'
		+'</div>'
		*/
		+ "<div style='float:left;margin: 10px 0px 3px 0px;width:100%;'>"
		//+"<span>Task </span>"
		+ "<input type=\"text\" id=\"hlightTaskName\" placeholder=\"Task Name\" style='height:32px;width:100%;border: 1px solid #D1D7D7;padding: 5px;'>"
		+ "</div>"
		+ "<input type=\"hidden\" id=\"taskParticipantsInpFiled\" value=\"\">"
		+ "<div style='float:left;margin: 3px 0px;width:100%;'>"
		//+"<span>Task </span>"
		+ "<textarea type=\"text\" id=\"hlightTaskDesc\" placeholder=\"Instructions\" style='float:left;width:100%;height: 44px;border: 1px solid #d1d7d7;padding: 5px;'></textarea>"
		+ "</div>"
		+ "<div id=\"projListDivNewUI\" style='float:left;margin: 3px 0px;width:100%;position:relative;'>"
		+ '<div class=\"quesErrDiv\" style="float: left;width: 100%;padding: 6px;border: 1px solid #d1d7d7;background-color: #fff;min-height: 39px;" onclick="showProjectListViewNewUI(this);">'
		+ '<img id="hlProjIcon" class="breadcrumImg img-responsive" onerror="imageOnProjNotErrorReplace(this);" src="" style="display:none;margin-top:0px !important;">'
		+ '<span id="hlProjName" class="" style="margin-top: 3px;float: left;"></span>'
		+ '<input id="hlightproject" type="hidden"  value="" >'
		+ '<img src="' + path + '/images/temp/hlArrowDown.png" style="float: right;height: 8px;width: 7px;margin-top: 10px;">'
		+ '</div>'
		+ '<div id="projListDDContainer" style="display: none;left: 0;margin-left: 0;width: 100%;height:174px !important;">'
		+ '<div id="hlProjListDDContainerSub" style="padding-bottom:4px;margin-top: 6px;margin-right: 5px;/*! text-align: right; *//* height:28px; */margin-bottom: 5px;margin-left: 5px;width: 98%;">'
		+ "</div>"
		+ "</div>"
		/*
		+"<select id='hlightproject' onchange='workspaceChangeEvent(this);' class='taskSelectCss form-control' name='Workspace' style = 'padding: 6px !important;height:32px;border-radius:0px;border: 1px solid #D1D7D7;width: 100%;'>"
		   +"<optgroup id='hlightProjectOptions' label=''>"
		   +"</optgroup>"
		 +"</select>" 
		*/
		+ "</div>"

		+ "<div style='margin: 3px 0px; width:100%;float:left;'>"
		+ "<div class=\"quesErrDiv\" style='float: left;width: 49%;border: 1px solid #d1d7d7;background-color: #fff;' >"
		+ '<span style="float: left;margin-top: 7px;margin-left: 5px;margin-right: 2px;">Due:</span>'
		+ "<input type='text' id='hlightdatepicker' readonly class='hasDatePicker startsAt taskStEnDate' onclick='showdhtmlContainer(this);' style='height: 30px;float: left;width: 66%;padding: 5px;border-radius: 0px;border: none;margin:0px;'>"
		+ "</div>"
		+ "<div style='float: right;width: 49%;'>"
		+ "<select class=\"quesErrDiv\" id=\"priority\" class=\"taskSelectCss form-control\"  style = \"height:32px;float:right;width: 99%;padding: 6px !important;border: 1px solid #d1d7d7;border-radius: 0px;\" onchange=\"changeType(this)\">"
		+ "<option value = \"0\" class=\"drpDwnOptions Select_cLabelText\"  >Priority</option>"
		+ "<option value = \"1\"  style = \"background-image: url('images/calender/VeryImportant.png');background-repeat: no-repeat;padding-left: 15%;\" class=\"drpDwnOptions Very_Important_cLabelText\" >Very Important</option>"
		+ "<option value = \"2\" style = \"background-image: url('images/calender/Important.png');background-repeat: no-repeat;padding-left: 15%;\" class=\"drpDwnOptions Important_cLabelText\" >Important</option>"
		+ "<option value = \"3\" style = \"background-image: url('images/calender/medium.png');background-repeat: no-repeat;padding-left: 15%;\" class=\"drpDwnOptions Medium_cLabelText\" >Medium</option>"
		+ "<option value = \"4\" style = \"background-image: url('images/calender/Low.png');background-repeat: no-repeat;padding-left: 15%;\" class=\"drpDwnOptions Low_cLabelText\" >Low</option>"
		+ "<option value = \"5\" style = \"background-image: url('images/calender/VeryLow.png');background-repeat: no-repeat;padding-left: 15%;\" class=\"drpDwnOptions Very_Low_cLabelText\" >Very Low</option>"
		//+"<option value = \"6\" style = \"background-image: url('images/calender/none.png');background-repeat: no-repeat;padding-left: 15%;\" class=\"drpDwnOptions \" >None</option>"
		+ "</select>"
		+ "</div>"
		+ "</div>"
		+ "<div class=\"quesErrDiv\" style='float:left;margin: 3px 0px;width:100%;height:60px;border: 1px solid #d1d7d7;background-color: #fff;position: relative;'>"
		+ "<input type='text' id='hlighttextarea' placeholder='Add Participants'  onkeyup= 'filterHighlightTaskUsers(event);' onfocus='addTaskParticipants()' onclick='' style='cursor:pointer;width: 97%;padding:3px;float:left;margin-left: 7px;border:0px;'></input>"
		+ "<div id='hlightAddedUserList' style='height:30px;width:100%;float:left;overflow-x:hidden;'><ul style='list-style-type:none;margin-left: -35px;'></ul></div>"
		+ "<div id='highlightuserlist' style='height: 158px;width: 100%;float: left;background-color: lightblue;overflow-x: hidden;position: absolute;top: 58px;z-index: 1;padding: 10px;display: none;'>"
		+ "</div>"
		+ "</div>"
		+ '<div class="" style="display:none; float: left; margin: 3px 0px; width: 100%; height: 110px; border: 1px solid rgb(209, 215, 215); background-color: rgb(255, 255, 255); position: relative;">'
		+ '<span style="margin-left: 7px;padding: 3px;float: left;">Latest Comment:</span>'
		+ '<div id="hlightLatestUserComment" style="height: 80px; width: 100%; float: left; overflow-x: hidden;">'
		+ '<blockquote style="font-size: 13px;padding: 5px 10px;border: 0;margin-bottom: 10px;">'
		+ '<p style="font-size: 13px;"></p>'
		+ '<footer></footer>'
		+ '</blockquote>'
		+ '</div>'
		+ '</div>'
		+ "<div style='display:none;float:left;padding: 8px 0px 5px 0px; width:100%;height:auto;font-size: 13px;color:green;'>"
		+ "<div id='queryMsg' style=\"float: left;width: 100%;\"></div>"
		+ "<div id='answerMsg' style=\"float: left;width: 100%;margin-top: 3px;\"></div>"
		+ "</div>"
		+ "<div style='display:none;float:left;width:100%;height:auto;font-size: 13px;'>"
		+ "<button id='query-btn-next' onclick='queryNext(0,\"Y\");' style='border: none;float: right;margin-top: 3px;margin-bottom: 3px;color: black;font-size: 11px;'>Say \'Skip\' or \'Create Task \'</button>"
		+ "</div>"
		+ "<div class='speechRecDiv' id='speechRecDivAreaId' style='display:none;float:left;width:100%;height:50px;border: 1px solid #d1d7d7;background-color:  #fdf5b4;padding-top: 5px;margin-bottom: 3px;margin-left: 0px;margin-right: 0px; '>"
		+ "<textarea id='note-textarea' rows='5'  placeholder='Say Something or \"Skip\" or \"Create Task \"' style='width: 100%;height: 100%;padding-left: 5px;float:left;border:0px;background:none;'></textarea>"
		+ "</div>"

		+ '</div>'
		+ "<div id=\"saveAndCloseTimeAndDate\" style=\"width: 100%;padding: 6px 0px;background-color:#194960;margin-top:10px;margin-bottom:5px;float:left;\">"
		+ "<img id=\"saveAndUpdateHlightTaskId\" title=\"Save\" onclick = \"addHighlightTask(" + hid + ");\" class=\"img-responsive\" src=\"" + path + "/images/temp/hupdate.png\" style=\"float:right;width: 22px;height: 22px;margin-left: 3px;cursor:pointer;margin-right: 6px;\"> "
		+ "<img id=\"cancelHlightTaskUI\" title=\"Cancel\"  onclick=\"cancelHighLightTask(" + hid + ");\" class=\"img-responsive\" src=\"" + path + "/images/temp/hcancel.png\" style=\"margin-left: 3px;margin-right: 3px;float:right;width: 22px;height: 22px;cursor:pointer;\" > "
		+ "<div id=\"recordStart\" class=\"speechRecDiv\" style=\"float: right;box-shadow:none;margin-right: 6px;margin-top: -1px;\">"
		/*+"<img id=\"start-record-btn\"  src='"+path+"/images/record.png'  style=\"cursor:pointer;margin: 2px 3px;height: 22px;width: 14px;\" alt=\"Image\">"*/
		+ "<img id=\"hLigthTaskVoiceStart\"  src='" + path + "/images/glbrecordDefault.png'  style=\"cursor:pointer;margin: 2px 3px;height: 22px;width: 30px;background-color:white;border-radius:25px;\" alt=\"Image\" onclick=\"recognizationToggleStart('','hLightTaskSpeechType','hLightTaskSpeechType');\">"
		+ "<img id=\"hLigthTaskVoiceStop\" src='" + path + "/images/glbrecord.png'  style=\"display:none;cursor:pointer;margin: 2px 3px;height: 22px;width: 30px;background-color:white;border-radius:25px;\" alt=\"Image\" onclick=\"recognizationToggleStop();\">"
		+ "</div>"
		/*+'<img id="start-record-btn" class="speechRecImg"  alt="Image" src="'+path+'/images/record.png" onclick="toggleStartStop();" style="display:none;margin-left: 3px;margin-top: 2px;cursor: pointer;height: 22px;float: right;margin-right: 5px;box-shadow:none">'*/
		+ "</div>"
		+ "</div>";

	return UI;
}

function filterHighlightTaskUsers(event) {
	$('#saveAndCloseTimeAndDate').css('margin-top', '100px');
	$('div.participants').hide();
	var name = $('input#hlighttextarea').val();
	if (!name) {
		$('div.participants').show();
		$('#nonePartList').hide();
		hideAddedParticipantsInList();
		return null;
	} else {
		$('div.participants').find('span.hidden_searchName:contains("' + name + '")').parents('div.participants').show();
		if ($('#highlightuserlist').find('.participants:visible').length < 1) {
			$('#nonePartList').show();
		} else {
			$('#nonePartList').hide();
		}
		hideAddedParticipantsInList();
		return null;
	}
}
function hideAddedParticipantsInList() {
	$('#hlightAddedUserList').find('li[id^=user_]').each(function () {
		var id = $(this).attr('id').split('_')[1];
		$('#participant_' + id).css('display', 'none');
	});
	if ($('#highlightuserlist').find('.participants:visible').length < 1) {
		$('#nonePartList').show();
	} else {
		$('#nonePartList').hide();
	}
}

function PrepareUIforTaskViewNewUI(result) {
	var jsonData = jQuery.parseJSON(result);
	for (var i = 0; i < jsonData.length; i++) {

		var taskId = jsonData[i].taskId;
		var taskType = jsonData[i].taskType;

		var PriorityId = jsonData[i].Priority != '' ? jsonData[i].Priority : '0';
		var taskfullname = jsonData[i].taskfullname;
		var taskfullname = replaceSpecialCharacter(taskfullname);
		var taskDescription = jsonData[i].taskDescription;
		var taskDescription = replaceSpecialCharacter(taskDescription);
		var startDate = jsonData[i].start;
		var endDate = jsonData[i].end;
		var startTime = jsonData[i].startTime;
		var compTime = jsonData[i].compTime;
		var createdBy = jsonData[i].createdBy;
		var taskSourceId = jsonData[i].taskSourceId;
		var taskParticipant = jsonData[i].taskParticipant;
		var projectId = jsonData[i].projectId != '' ? jsonData[i].projectId : '0';
		$('#hlightproject').val(projectId);
		$('#taskParticipantsInpFiled').val(JSON.stringify(taskParticipant));
		/*setTimeout(function(){
			$.each(taskParticipant,function(i,jObj){
				$('#highlightuserlist').find('#participant_'+jObj.user_id).find('#particpantName_'+jObj.user_id).trigger('click');
			});
			//if(projectId != '' && projectId != 0 ){
				// $('#hlProjListDDContainerSub').find('#proj_'+projectId).trigger('click');
				//$('select#hlightproject option[id="'+projectId+'"]').prop("selected", "selected");
				//$("#hlightproject").prop("disabled", true);
			//}
			//$("#hlightproject").prop("disabled", true);resizetex
		   }, 1500);*/


		$('#hlightTaskHeader').text(taskfullname);
		$('#hlightTaskName').val(taskfullname);
		$('#hlightTaskDesc').val(taskDescription);
		$('select#priority option[value="' + PriorityId + '"]').prop("selected", "selected");
		$('#hlightdatepicker').val(endDate);
		$('#saveAndUpdateHlightTaskId').attr('onclick', 'updateHighlightTask(' + taskId + ',' + taskSourceId + ')');
	}



}

function fetchHighlightTaskDetailsView(hId) {
	$("div.hlDiv").hide();
	$('#hMainDiv').css('height', '100%');
	$("#hAddDiv").hide();
	$('#hl_' + hId).show();
	$('.dhtmlxcalendar_container').remove();
	$("div.hlDiv").find('div[id^=hlTaskContainerDiv_]').html("");
	$("div.hlDiv").find('div[id^=hlEditContainer_]').show();
	$("div.hlDiv").removeClass("hEditBg");
	$("div.hlDiv").find('img.hEditHide').show();
	$('#hl_' + hId).css({ 'border-left': '1px solid rgb(233, 230, 230)', 'border-right': '1px solid rgb(233, 230, 230)' });

	//$('#hlEditContainer_'+hId).hide();
	$('#hlTaskContainerDiv_' + hId).html(prepareHighlightTaskUI(hId)).show();
	$('#chatTaskDivArea_' + hId).show();

	$('#hl_' + hId).addClass("hEditBg");
	$('#hl_' + hId).find('img.hEditHide').hide();
	//$('#saveAndUpdateHlightTaskId').attr('onclick','updateHighlightTask('+hId+')');

	$("#recordStart").hide();
	myCalendar = new dhtmlXCalendarObject('hlightdatepicker');
	var today = new Date();
	myCalendar.setDateFormat("%m-%d-%Y");
	myCalendar.setSensitiveRange(today, null);
	myCalendar.hideTime();

	if (hId != '' && typeof (hId) != 'undefined' && hId != 0) {
		$.ajax({
			url: path + '/blogAction.do',
			type: "POST",
			data: { act: 'fetchHighlightTaskDetailsView', hId: hId, device: 'web' },
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				$("#loadingBar").hide();
				timerControl("");
			},
			success: function (result) {
				sessionTimeOutMethod(result);
				if (result != "SESSION_TIMEOUT") {
					//checkSessionTimeOut(result);
					PrepareUIforTaskViewNewUI(result);
					fetchWorkspaceProjects('update', 'manualTask');
				}
			}
		});
	}

}
var removeEmailId = "";
function updateHighlightTask(TaskId, hid) {
	var d = new Date();
	var month = d.getMonth() + 1;
	var day = d.getDate();
	var currentDate = (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + '-' + d.getFullYear();
	var tName = $('#hlightTaskName').val().trim();
	var tDesc = $('#hlightTaskDesc').val();
	var projId = $("#hlightproject").val();
	var end = $('#hlightdatepicker').val();
	var start = currentDate;
	var priority = $("#priority option:selected").attr('value');
	var userXml = prepareUserXml();
	if (tName == '') {
		$('#hlightTaskName').css('border', '1px solid red');
	} else {
		$('#hlightTaskName').css('border', '1px solid #D1D7D7')
	}
	if (tName != '') {
		$.ajax({
			url: path + '/calenderAction.do',
			type: "POST",
			data: {
				act: 'updateTask', eId: TaskId,
				taskType: 'Highlight',
				title: tName,
				description: tDesc,
				start: start,
				startTime: '00:00:00',
				endTime: '23:59:59',
				end: end,
				priorityId: priority,
				taskProject: projId,
				estimatedHour: '0',
				estimatedMinute: '0',
				userXml: userXml,
				removeEmailId: removeEmailId,
				taskDateXml: "<taskHour></taskHour>",
				device: 'web'

			},
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				$("#loadingBar").hide();
				timerControl("");
			},
			success: function (result) {
				sessionTimeOutMethod(result);
				if (result != "SESSION_TIMEOUT") {
					//checkSessionTimeOut(result);
					chatAlertFun('Task Updated Successfully', 'warning');
					$('.chatTaskHightCls').hide();
					$('#hl_' + hid).find('img.hTaskIcon').attr('title', 'View Task').attr('src', path + '/images/temp/htask2.png').attr('onclick', 'fetchHighlightTaskDetailsView(' + hid + ')');
					cancelHighLightTask(hid);
				}
			}
		});
	}
}
function prepareHighlightUI(result, action) {
	var UI = "";
	var data = result;
	var d = new Date();
	try{
	if (data) {
		// data = eval(data);
		for (var i = 0; i < data.length; i++) {

			UI += "<div class='hlDiv cmeTask' id='hl_" + data[i].hl_id + "' onmouseover='showHlOptions(this)' onmouseout='hideHlOptions(this)' style='border-top:1px solid rgb(233, 230, 230); position:relative;'>"
				+ "<div style='float: left;width: 100%; padding: 10px;'>"
				+ "<div class='hTime' style='font-size: 11px;margin-bottom: 0px;'>"
				+ "<img src='" + lighttpdpath + "/userimages/" + data[i].user_image + "?" + d.getTime() +"' title='" + data[i].user_name + "' onerror=\"userImageOnErrorReplace(this);\" style=\"height: 35px; min-height: 35px; width:35px;min-width: 35px;border-radius:50%;margin-right: 5px;\">"
				+ "<span>" + data[i].user_name + "</span>&nbsp;&nbsp;-&nbsp;&nbsp;<span>" + data[i].hl_time + "</span>"
				+ "</div>"
				+ "<div id='hlEditContainer_" + data[i].hl_id + "'>"
				+ "<div class='hMsg' id='hlightMsg_" + data[i].hl_id + "' onclick='selectText(this.id, this, " + data[i].hl_id + ");' style='font-size: 12px;font-weight: bold;padding-left:40px;margin-top:-5px;'>" + replaceSpecialCharacter(data[i].hl_msg) + "</div>"
				+ "<div class='hDesc' id='hlightDes_" + data[i].hl_id + "' onclick='selectText(this.id, this, " + data[i].hl_id + ");'  style='font-size: 12px;margin-bottom: 2px;margin-top: 0px;padding-left:40px;'>" + replaceSpecialCharacter(data[i].hl_desc) + "</div>"
				+ "<input class='hMsgEdit' placeholder='Highlight' onkeypress='validateUpdHLEvent(event," + data[i].hl_id + ");' style='width: 100%; border: 0px none;  margin-top: 10px;padding: 4px;font-size: 12px;display:none;'>"
				+ "<textarea class='hDescEdit' placeholder='Description' rows='' cols='' onkeypress='validateUpdHLEvent(event," + data[i].hl_id + ");' style='margin-top: 3px; width: 100%; height:60px; border: 0px none; padding: 4px;font-size: 12px;display:none;'></textarea>"
				+ "<div class='hEditIconsDiv' style='margin-top: 5px;display:none;' align='right'>"
				+ "<img style='width: 22px; cursor: pointer;margin-left: 6px;' title='Cancel' onclick='cancelUpdateHighlight(" + data[i].hl_id + ");' src='" + path + "/images/task/remove.svg'>"
				+ "<img style='width: 22px; cursor: pointer;margin-left: 6px;' title='Update' onclick='updateHighlight(" + data[i].hl_id + ");' src='" + path + "/images/task/tick.svg' >"
				+ "</div>"
				+ "</div>"
				+ "</div>"

				+ "<div class='hOptionDiv' style='display:none;position: absolute; right: 10px;  top: 15px;'>";
			if (data[i].htaskstatus == 'N') {
				UI += "<img  class='hTaskIcon ' tsid= '" + data[i].taskid + "' style='display: none;width: 22px;cursor:pointer;' src='" + path + "/images/temp/htask.png' title='View Task' onclick=\"editHLTask('" + data[i].taskid + "', '" + data[i].hl_id + "');event.stopPropagation();\"  >";// onclick='openHLTaskPopup(this," + data[i].hl_id + ");'
			} 
			else {
				UI += "<img  class='hTaskIcon ' tsid= '" + data[i].taskid + "' style='width: 22px;cursor:pointer;' src='" + path + "/images/temp/htask.png' title='View Task' onclick=\"editHLTask('" + data[i].taskid + "', '" + data[i].hl_id + "');event.stopPropagation();\"  >";// onclick='openHLTaskPopup(this," + data[i].hl_id + ");'
			// 	UI += "<img  class='hTaskIcon hEditHide' tsid= '" + data[i].taskid + "' style='display: none;width: 22px;cursor:pointer;' src='" + path + "/images/temp/htask2.png' title='View Task'  onclick=\"editHLTask('" + data[i].taskid + "', '" + data[i].hl_id + "');event.stopPropagation();\" >";		//newUicreateTaskUI('update','"+taskId+"', '"+taskListType+"','"+source_id+"','Present','"+taskImageTitle+"'); fetchHighlightTaskDetailsView("+data[i].hid+");
			}
			UI += "<img  class='hEditIcon hEditHide' style='width: 22px;margin-left:4px;cursor:pointer;" + (action == 'view' ? "visibility:hidden;" : "") + "' src='" + path + "/images/temp/hedit.png' title='Edit'  onclick='editHightlight(" + data[i].hl_id + ");' >"
				+ "<img  class='hDeleteIcon hEditHide' style='width: 22px;margin-left:4px;cursor:pointer;" + (action == 'view' ? "visibility:hidden;" : "") + "' src='" + path + "/images/temp/hdelete.png' title='Delete' onclick='delHightlight(this," + data[i].hl_id + ");' >"
				+ "</div>"
				+ "<div id='hlTaskOptionDiv_" + data[i].hl_id + "' class=\"actFeedOptionsDiv\" style='display: none;right: 106px;top: 0px;font-size:12px;width:50px;'>"
				+ "<div style='margin-top: -2px; float: right; position: absolute; right: -16px;' class=\"workSpace_arrow_right\">"
				+ "<img src='" + path + "/images/arrow.png'>"
				+ "<div onclick=\"newUicreateTaskUI('','0','Highlight','" + data[i].hl_id + "','" + data[i].hmsgid + "','" + replaceSpecialCharacter(data[i].hl_msg) + "','','','highlightTask');\" style='width:99%;height:auto;padding:0 0 3px ;cursor:pointer;float:left'>"		//manualTaskcreate("+data[i].hid+");newUicreateTaskUI('','0','Highlight','"+data[i].hid+"','','','','','highlightTask')
				+ "<img style='width: 24px;margin-left:4px;cursor:pointer;' src='" + path + "/images/temp/hltManual.png' >"
				+ "</div>"
				+ "<div id=\"hLightVoiceIconTaskNew\" class=\"hLightVoiceTaskClss\" onclick=\"newUicreateTaskUI('','0','Highlight','" + data[i].hl_id + "','" + data[i].hmsgid + "','" + replaceSpecialCharacter(data[i].hl_msg) + "','','','highlightVoiceTask');\" style='width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;display:none;'>"		//onclick=\"voiceTaskCreate("+data[i].hid+");\"		newUicreateTaskUI('','0','Highlight','"+data[i].hid+"','"+data[i].hmsgid+"','"+replaceSpecialCharacter(data[i].hmsg)+"','','','highlightVoiceTask');
				+ "<img style='width: 24px;margin-left:4px;cursor:pointer;' src='" + path + "/images/temp/hltVoice.png' >"
				+ "</div>"
				+ "</div>"
				+ "<div class='hTaskContainerDivs' id='hlTaskContainerDiv_" + data[i].hl_id + "' style='background-color:#eef5f7;float:left;width:100%;display:none;'>"
				+ "</div>"
				+ "</div>"
				+ "</div>"
		}

	}
	} catch (eMsg) {
		console.log(eMsg);
	}
	// $("#hListDiv").html(UI);
	return UI;
}

// if(!$target.parents().is("#createHLTask") && !$target.is("#createHLTask")  ){
// 	$("body").find("#createHLTask").hide();
//  }
var globalhlmenu='';
var globalhltype='';
function editHLTask(taskid, hid){
	globalhid=hid;
	globalhlmenu = 'Highlight';
	HighlightTaskUI(hid);
	editTask(taskid);
	// createPopupDiv();
	// insertCreateTaskUI();
}

var glbhltitleobj='';
function selectText(id, obj, hid){
	try{
		globalhid=hid;
		glbhltitleobj = obj;
		
		var sel, range;
		var el = document.getElementById(id); //get element id
		if (window.getSelection && document.createRange) { //Browser compatibility
		sel = window.getSelection();
			if(sel.toString() == ''){ //no text selection
				window.setTimeout(function(){
					range = document.createRange(); //range object
					range.selectNodeContents(el); //sets Range
					sel.removeAllRanges(); //remove all ranges from selection
					sel.addRange(range);//add Range to a Selection.
					// if($('#hl_'+hid).find('.hTaskIcon').is(":hidden")){
						$('#createHLTask').show();
						$('#createHLTask').attr('onclick', 'openHLTaskPopup(this,'+hid+')');
					// }
				},1);
			}
			else{
				// console.log("ELSE IF");
				// if($('#hl_'+hid).find('.hTaskIcon').is(":hidden")){
					$('#createHLTask').show();
					$('#createHLTask').attr('onclick', 'openHLTaskPopup(this,'+hid+')');
				// }
			}
		}else if (document.selection) { //older ie
			sel = document.selection.createRange();
			if(sel.text == ''){ //no text selection
				range = document.body.createTextRange();//Creates TextRange object
				// console.log(range);
				range.moveToElementText(el);//sets Range
				// console.log(range.moveToElementText(el));
				range.select(); //make selection.
				// console.log(range.select());
				// if($('#hl_'+hid).find('.hTaskIcon').is(":hidden")){
					$('#createHLTask').show();
					$('#createHLTask').attr('onclick', 'openHLTaskPopup('+hid+')');
				// }
			}
			else{
				// console.log("ELSE IF");
				// if($('#hl_'+hid).find('.hTaskIcon').is(":hidden")){
					$('#createHLTask').show();
					$('#createHLTask').attr('onclick', 'openHLTaskPopup('+hid+')');
				// }
			}
		}
	} catch (eMsg) {
		console.log(eMsg);
	}
}

function openHLTaskPopup(obj, hid){
	var type = $(obj).attr('type');
	console.log("type--"+type);
	if(type == '1'){
		globalhltype = '1';
		// $("#cmePopupHL").hide();
	}
	// let jsonbody = {
	// 	"user_id":userIdglb,
	// 	"company_id":companyIdglb,
	// 	"limit":"",
	// 	"index":"",
	// 	"sortVal":"",
	// 	"text":"",
	// 	"project_id":prjid,
	// 	"sortTaskType":"idea",
	// 	"source_id" : ideaid
	// }
  	// $('#loadingBar').addClass('d-flex').removeClass('d-none');  

	// $.ajax({
	// 	url: apiPath + "/" + myk + "/v1/getTaskDetails/TeamZone",
	// 	type: "POST",
    // 	contentType: "application/json",
	// 	data: JSON.stringify(jsonbody),
	// 	error: function (jqXHR, textStatus, errorThrown) {
	// 		checkError(jqXHR, textStatus, errorThrown);
	// 		$('#loadingBar').addClass('d-none').removeClass('d-flex');  
	// 	},
	// 	success: function (result) {
		globalhlmenu = 'Highlight';
			HighlightTaskUI(hid);
			// createPopupDiv();
			insertCreateTaskUI();
			// $("#ideacreateTaskDivbody").html(prepareAddTaskUiNew());
			
			// $('#ideaTasksListHeaderDiv').append(wsTaskUi("HL"));

			// $("#ideaTasksListDivbody").html(createTaskUI(glbgethighlghts));
			
			
			var glbhltitle= $(glbhltitleobj).text();
			$('#taskname').val(glbhltitle).focus();
			
			// createIdeaTask('Testing hl');
			
			// if(place=="options"){
			// 	$('.createIdeaTaskbutton').trigger('click');
			// 	$("#headerTask, #taskList, .popupTaskDiv").hide();
			// }
			
			// if($("#ideaTasksListDivbody").html().length==0){
			// 	$("#ideaTasksListDivbody").append("<div class='d-flex justify-content-center mt-3 notaskdiv'>No Task Found</div>");
			// }
			var disable = $('#editIdeaMainDiv_'+hid).parent('#mainIdea_'+hid).attr('disableclass');
			if(disable == "disabledragndrop"){
				$('.createIdeaTaskbutton').removeAttr('onclick').addClass('d-none');
			}
			var disableshare = $('#editIdeaMainDiv_'+hid).parent('#mainIdea_'+hid).attr('icomplete');
			if(disableshare == "Y"){
				$('.createIdeaTaskbutton').removeAttr('onclick').addClass('d-none');
			}
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			
	// 	}
	// });
}

function createIdeaTask(ideatitle){
	insertCreateTaskUI();
	// $('#taskname').val(ideatitle).focus();
  }

function HighlightTaskUI(hid, title){
	var ui="";
	$('#otherModuleTaskListDiv').html('');
  $("#transparentDiv").show();
  
  ui='<div class="hlTask" id="" ideaid='+hid+'>'
    +'<div class="" style="">'
      +'<div class="modal-content px-2" style="z-index:1000 !important;max-height: 550px;height: 550px;width: 88%;margin-left: 120px;">'
        +'<div class="modal-header pt-2 pb-0 px-0" style="border-bottom: 1px solid #b4adad;height:23px;">'
          +'<div class=" d-flex  align-items-center w-100" style="">'
            +'<div class="pl-2 d-none" style="font-size:12px;"><img src="images/task/idea-task.svg" style="width:17px;height:17px;"><span class="pl-2">Highlight Task</span></div>'
            +"<div class=\"ml-auto pr-3 pb-1 d-none\" title=\"Create Task\"><img class=\"createIdeaTaskbutton\" onclick=\"createIdeaTask('"+title+"');event.stopPropagation();\" src=\"images/task/plus.svg\" style=\"height:18px;width:18px; cursor:pointer;\"></div>"
          +'</div>'
          //+'<button type="button" onclick="closeIdeaTask();" class="close p-0" style="top: 20px;right: 25px;color: black;outline: none;">&times;</button>'
          +'<img src="images/menus/close3.svg" onclick="closeHLTask();" class="" style="cursor:pointer;width:10px;height:10px;margin-top:1px;margin-right:0px;">'
        +'</div>'
        +'<div class="p-0 wsScrollBar" style="overflow: auto;height: inherit;">'
          +'<div class="py-1 w-100 task_type_divison" style="color:#fff;font-size:12px;">'
            +'<div class="pl-2 d-flex align-items-center" style="font-size:14px;">'
              +'<img src="images/temp/htask.png" style="width:17px;height:17px;">'
              +'<span class="pl-2" style="">Highlight Task</span>'
            +'</div>'
          +'</div>'
          +'<div id="ideacreateTaskDivbody" class="p-0" style=""></div>'
          +'<div class="popupTaskDiv">'
            +'<div id="ideaTasksListHeaderDiv" class="p-0" style=""></div>'
            +'<div id="ideaTasksListDivbody" class="p-0" style=""></div>'
          +'</div>'
        +'</div>'
        
      +'</div>'
    +'</div>'
  +'</div>'

  $('#otherModuleTaskListDiv').append(ui).show();
}

function closeHLTask(){
  $('#otherModuleTaskListDiv').html('').hide();
  globalhlmenu='';
//   globalhid='';
//   $("#transparentDiv").hide();
}


function showTaskIcons(hId) {
	if (isChromeVoice) {
		$('.hLightVoiceTaskClss').show();
	}

	// console.log("visible:"+$('div#hlTaskOptionDiv_'+hId).is(':visible'));
	if ($('div#hlTaskOptionDiv_' + hId).is(':visible')) {
		$('div#hlTaskOptionDiv_' + hId).hide();
	} else {
		$("div.hlDiv").find('div[id^=hlTaskOptionDiv_]').hide();
		$('div#hlTaskOptionDiv_' + hId).show();
	}
}


var globalhid='';
var globalhlmsgid='';
function viewHighlight(msgId, type) {
	//sessionStorage.chatBoxOnLogin1 = "login";
	globalhlmsgid = msgId;
	$('#ideaTransparent').show();
	$("#transparentDiv").hide();
	$('#cmePopup').hide();
	$('#cmePopupHL').hide();
	// $('#cmePopupTS').hide();
	$('#cmerecommendations').show();
	// $('#ideaTransparent').removeClass('ideaTransparent').addClass('transparentDivChat');
	$('#cmerecommendations').html('');
	// if (!($('#HightlightPopUp').is(':visible'))) {
		//$("#loadingBar").show();	
		//timerControl("start");
	var UI='<div class="SBactiveSprintlistCls" style="top:60px !important;" id="SBactiveSprintlistId" selectedstories="">'
				+'<div class="mx-auto w-50" style="max-width: 600px;">'
					+'<div class="modal-content container px-0" style="max-height: 500px !important;">'
						+'<div class="modal-header p-2 pl-3 d-flex align-items-center" style="border-bottom: 1px solid #b4adad !important;">'
							+'<span id="" class="hListHeaderItems modal-title" style="color:black;font-size: 16px !important;" onclick="">Meeting Highlights</span>'  
							+'<span id="" class="hProjHeader modal-title" style="display: none;color:black;font-size: 16px !important;" onclick="">Workspace</span>'  
							// +'<span id="groups" class="modal-title cursor ml-4" style="font-size:14px;" onclick="groupPopup()">GROUPS</span>'
							// +'<span class="modal-title" style="width:20%;font-size:14px;">Start Date</span>'
							// +'<span class="modal-title" style="width:20%;font-size:14px;">End Date</span>'
							// +'<input  id="searchText6" class="border rounded d-none searchHeader" style="width:86%;font-weight: normal;font-size: 13px;height: auto;padding: 3px 8px;"  placeholder="Search" onkeyup="searchContactsAV()">'
							+'<img id="createHLTask" type="1" src="images/temp/htask.png" class="mr-2 ml-auto cursor" onclick="" title="Create Task" style="width: 22px;display: none;">'
							+'<img id="" src="images/cme/leftarrow_blue.svg" title="Back" onclick="backToHighlights()" class="hProjHeader mr-4 cursor "  style="display: none;width: 22px;">'
							+'<img id="" src="images/temp/associate.png" title="Share To Workspace" onclick="getAllProjectsForHighlights()" class="hListHeaderItems mr-4 cursor "  style="width: 22px;">'
						+'</div>'
						+'<div class="" style="">'
						if(type == '1'){
							UI+='<button type="button" onclick="closeHightlightCmePopUp2();event.stopPropagation();" class="close p-0 AgileCloseButton" style="top: 5px;right: 10px;color: black;outline: none;">&times;</button>'
						}
						else{
							UI+='<button type="button" onclick="closeHightlightCmePopUp();event.stopPropagation();" class="close p-0 AgileCloseButton" style="top: 5px;right: 10px;color: black;outline: none;">&times;</button>'
						}
						UI+='</div>'
						+'<div class="modal-body p-0 m-0" style="height: 420px;">'
							+ "<div class='hListDiv wsScrollBar' id='hListDiv' style='height:100%; overflow:auto; padding: 0px;width: 100%; max-height: 420px;'></div>"
							+ "<div class='px-2 wsScrollBar' id='hProjListDiv' style='height: 100%;max-height: 420px;overflow:auto;0px 10px;display:none;'></div>"
							+ "<input type='hidden' id='ascMsgId' value='" + msgId + "' />"
						+'</div>'
					+'</div>'
				+'</div>'
			+'</div>'

		// var UI = "<div class='modal-dialog row'  id='HightlightPopUp' style='width: 98%; z-index: 10000; font-family: OpenSansRegular; margin-top: -29%;margin-left: 30%;margin-right: 1%;'>"
		// 	+ "<div class='modal-content hModalContent'>"
		// 	+ "<div class='modal-header' style='padding:1.5vh'>"
		// 	+ "<button type='button' class='close' onclick='closeHightlightCmePopUp()' id='popupClose' style='margin-top:-32px;margin-right: -46px;' data-dismiss='modal'>&times;</button>"
		// 	+ "<span  class='hListHeaderItems' style='margin-top: 0.5vh;float:left;font-size: 15px; font-family: opensansbold; font-weight: bold;'>&nbsp;Meeting Highlights</span>"
		// 	+ '<span class="hProjHeader" style="display:none;margin-top: 0.5vh;float:left;font-size: 15px; font-family: opensansbold; font-weight: bold;">Workspace</span>'
		// 	+ '<img class="hProjHeader" title="Back" onclick="backToHighlights()" style="display:none;cursor: pointer; float: right; height: 22px;    min-height: 18px; min-width: 16px; width: 22px;margin-right: 10px;" src="' + path + '/images/cme/leftarrow_blue.svg">'
		// 	+ '<img class="hListHeaderItems" src="' + path + '/images/temp/associate.png" title="Share To Workspace" onclick="getAllProjectsForHighlights()" style="cursor:pointer;float: right;margin-right: 10px;">'
		// 	+ "</div>"
		// 	+ "<div class='modal-body' style='padding: 0px 15px;'>"
		// 	+ "<div class='row'>"
		// 	+ "<div class='col-sm-12' >"
		// 	+ "<div class='row'>"
		// 	+ "<div class='col-sm-12' id='hListDiv' style='height:60vh; overflow:auto; padding:0px 5px;width: 100%;'></div>"
		// 	+ "<div class='col-sm-12' id='hProjListDiv' style='height:68vh; overflow:auto;0px 10px;display:none;'></div>"
		// 	+ "<input type='hidden' id='ascMsgId' value='" + msgId + "' />"
		// 	+ "</div>"
		// 	+ "</div>"
		// 	+ "</div>"
		// 	+ "</div>"

		// 	+ " </div>"
		// 	+ "</div>";
	// }
	//new ui closed
	$('#cmerecommendations').html(UI);
	$("#transparentDiv").show();
	getHighlight('', msgId,'');
	resizeRemoteStream();
}

function closeHightlightCmePopUp2(){
	// $('#cmePopupHL').show();
	// $('#cmePopupTS').show();
	$('#cmerecommendations').html('').hide();
	$("#transparentDiv").hide();
}

function closeHightlightCmePopUp(){
	$('#cmePopupHL').show();
	// $('#cmePopupTS').show();
	$('#cmerecommendations').html('').hide();
	$("#transparentDiv").show();
}

function closeHightlightPopUp() {
	$('#viewHightlightPopUp').html('');
	$('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
	$('#ideaTransparent').hide();
	resizeRemoteStream();
}

function delHightlight(obj, hId) {
	conFunNew11(getValues(companyAlerts,"Alert_delete"),'warning','deleteHightlight','', obj, hId);
}


function deleteHightlight(obj, hId) {
	// var dFlag = confirm("Do you want to delete?");
	// if (!dFlag)
	// 	return false;
	// $.ajax({
	// 	url: path + "/ChatAuth",
	// 	type: "post",
	// 	data: { act: "deleteHighlight", sender: userIdglb, hId: hId, device: 'web' },
	// 	mimeType: "textPlain",
	// 	success: function (result) {
		let jsonbody = {
			"hl_id" : hId
		}
		checksession();
		$.ajax({
			url: apiPath + "/" + myk + "/v1/deleteHighlight",
			type: "DELETE",
			dataType: 'text',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				timerControl("");
			},
			success: function (result) {
			// alert(result);
			checkSessionTimeOut(result);
			// if (result != "SESSION_TIMEOUT") {
				if (result == "success") {
					$(obj).parents('div.hlDiv').remove();
					var jsonText = '{"calltype":"' + callType + '","type":"videohighlight","status":"delete","callid":"' + callId + '","hId":"' + hId + '"}';
					var message = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);; // preapring the chat stanza to send		 
					serverConnection.send(message);  // sending the highlight message 
				}
			// }
		}
	});
}


function editHightlight(hId) {
	$('.hlDiv').each(function () {
		if ($(this).find('.hMsgEdit').is(':visible')) {
			updateHighlight($(this).attr('id').split("_")[1]);
		}
	});
	var hMsg = $('#hl_' + hId).find('div.hMsg').text();
	var hDesc = $('#hl_' + hId).find('div.hDesc').text();
	$('#hl_' + hId).find('img.hEditHide').hide();
	$('#hl_' + hId).find('div.hMsg, div.hDesc').hide();
	$('#hl_' + hId).find('div.hEditIconsDiv').show();
	$('#hl_' + hId).find('.hMsgEdit').val(hMsg).show().focus();
	$('#hl_' + hId).find('.hDescEdit').val(hDesc).show();
	$('#hl_' + hId).addClass("hEditBg");
}
/*
function onblurMsgUpdate(hId){
   var hMsg = $('#hl_'+hId).find('.hMsgEdit').val();
   $('#hl_'+hId).find('.hMsgEdit').hide();
   $('#hl_'+hId).find('div.hMsg').text(hMsg).show();
   var hDesc = $('#hl_'+hId).find('.hDesc').text();
   $('#hl_'+hId).find('.hDescEdit').focus();
   updateHighlight(hId,hMsg,hDesc);
}
function onblurDescUpdate(hId){
   var hDesc = $('#hl_'+hId).find('.hDescEdit').val();
   $('#hl_'+hId).find('.hDescEdit').hide();
   $('#hl_'+hId).find('div.hDesc').text(hDesc).show();
   var hMsg = $('#hl_'+hId).find('.hMsg').text();
   updateHighlight(hId,hMsg,hDesc);
}
*/

function updateHighlight(hId) {
	var hMsg = $('#hl_' + hId).find('.hMsgEdit').val();
	$('#hl_' + hId).find('.hMsgEdit').hide();
	$('#hl_' + hId).find('div.hMsg').text(hMsg).show();
	var hDesc = $('#hl_' + hId).find('.hDescEdit').val();
	$('#hl_' + hId).find('div.hDesc').text(hDesc).show();
	$('#hl_' + hId).find('.hDescEdit').hide();

	$('#hl_' + hId).find('img.hEditHide').show();
	$('#hl_' + hId).find('div.hEditIconsDiv').hide();
	$('#hl_' + hId).removeClass("hEditBg");
	var d = new Date();
	var timeZone = getTimeOffset(d);

	// $.ajax({
	// 	url: path + "/ChatAuth",
	// 	type: "post",
	// 	data: { act: "updateHighlight", sender: userIdglb, hMsg: hMsg, hDesc: hDesc, timeZone: timeZone, hId: hId, device: 'web' },
	// 	mimeType: "textPlain",
	// 	success: function (result) {
		let jsonbody = {
			"sender" : userIdglb,
			"hl_msg" : hMsg,
			"hl_id" : hId,
			"hl_desc" : hDesc
		}
		checksession();
		$.ajax({
			url: apiPath + "/" + myk + "/v1/updateHighlight",
			type: "PUT",
			dataType: 'text',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				timerControl("");
			},
			success: function (result) {
			//alert('update: '+result);
			checkSessionTimeOut(result);
			// if (result != "SESSION_TIMEOUT") {
				if (result != []) {
					var jsonText = '{"calltype":"' + callType + '","type":"videohighlight","status":"edit","callid":"' + callId + '","hId":"' + hId + '"}';
					var message = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);; // preapring the chat stanza to send		 
					serverConnection.send(message);  // sending the highlight message
				}
			// }
		}
	});

}

function cancelUpdateHighlight(hId) {
	$('#hl_' + hId).find('.hMsgEdit').hide();
	$('#hl_' + hId).find('.hDescEdit').hide();

	$('#hl_' + hId).find('div.hMsg').show();
	$('#hl_' + hId).find('div.hDesc').show();

	$('#hl_' + hId).find('img.hEditHide').show();
	$('#hl_' + hId).find('div.hEditIconsDiv').hide();
	$('#hl_' + hId).removeClass("hEditBg");
}

function lockHighlightTime() {
	clearInterval(setHLtimer);
	$("#hTime").text($('#aTimer').text());
}

function showHlOptions(obj) {
	$(obj).find('.hOptionDiv').show();
}
function hideHlOptions(obj) {
	$(obj).find('.hOptionDiv').hide();
}

function getAllProjectsForHighlights() {
	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();
	$("#hListDiv").hide();
	$("#hProjListDiv").html("").show();

	$('.hListHeaderItems').hide();
	$('.hProjHeader').show();
	var projects ='';

	let jsonbody = {
		"user_id":userIdglb,
		"companyId":companyIdglb,
		"workspaceType":"existing"
	}


	$.ajax({
		url: apiPath + "/" + myk + "/v1/projectList",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			var jsonData = result;
					// for (var j = 0; j < projArray.length ; j++) {
					// 	var jsonData = jQuery.parseJSON(projArray[j]);
						for (var i = 0; i < jsonData.length ; i++) {
							var projName = jsonData[i].project_name;
							var ProjTitle = jsonData[i].project_name;
							var projId = jsonData[i].project_id;
							var imageUrl = jsonData[i].imageUrl; // url for images
							projects ="<div pId=\""+projId+"\" class='d-flex justify-content-left w-100 p-1 text-dark' onclick='AssociateToProject(this);' style='height: 39px;border-bottom:1px solid #bfbfbf;cursor:pointer;' >"+
											  "<img  style='float:left;height:30px;width:30px;margin-right: 2%;border: 1px solid #cccccc;' onerror='imageOnProjNotErrorReplace(this);' src='"+ imageUrl+"?"+new Date().getTime()+"'>"+
											"<div style='float: left;width: 73%;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;margin-top: 5px;' title='"+ProjTitle+"'>"+projName+"</div>"+
										  "<div class='userUnChecked' style='float: right;width: 10%;margin-top: 8px;padding: 0px;background-position: 90% center;position: absolute;right: 0;'></div>"+
											"</div>"
							projects = projects.replaceAll("ch(20)","'").replaceAll("ch(30)","chr(dbl)").replaceAll("ch(50)","[").replaceAll("ch(51)","]").replaceAll("ch(curly)","{").replaceAll("ch(clcurly)","}").replaceAll("ch(backslash)","\\");	
										$('#hProjListDiv').append(projects);
							
						}																																														
					// }
		}
	});
	/* $.ajax({
			url:path+"/workspaceAction.do",
			type:"POST",
			data:{act : "loadProjDDList",
						userId : userIdglb,
						sortValue : "",
						sortHideProjValue : "",
						ipadVal : "",
						sortType: "",
						txt: "",
						type: "",
						device:'web',},
			mimeType: "textPlain",
			success:function(result){
					//checkSessionTimeOut(result);
				//console.log(result);
				sessionTimeOutMethod(result);
				if(result != "SESSION_TIMEOUT"){  
					var projArray = result.split('#@#');
					for (var j = 0; j < projArray.length ; j++) {
						var jsonData = jQuery.parseJSON(projArray[j]);
						for (var i = 0; i < jsonData.length ; i++) {
							var projName = jsonData[i].projName;
							var ProjTitle = jsonData[i].ProjTitle;
							var projId = jsonData[i].projectID;
							var imageUrl = jsonData[i].imageUrl; // url for images
							projects ="<div pId=\""+projId+"\" onclick='AssociateToProject(this);' style='height: 41px;border-bottom:1px solid #bfbfbf;padding: 5px 0px 5px 2px;cursor:pointer;position:relative;' >"+
											  "<img  style='float:left;height:30px;width:30px;margin-right: 2%;border: 1px solid #cccccc;' onerror='imageOnProjNotErrorReplace(this);' src='"+ imageUrl+"?"+new Date().getTime()+"'>"+
											"<div style='float: left;width: 73%;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;margin-top: 5px;' title='"+ProjTitle+"'>"+projName+"</div>"+
										  "<div class='userUnChecked' style='float: right;width: 10%;margin-top: 8px;padding: 0px;background-position: 90% center;position: absolute;right: 0;'></div>"+
											"</div>"
							projects = projects.replaceAll("ch(20)","'").replaceAll("ch(30)","chr(dbl)").replaceAll("ch(50)","[").replaceAll("ch(51)","]").replaceAll("ch(curly)","{").replaceAll("ch(clcurly)","}").replaceAll("ch(backslash)","\\");	
										$('#hProjListDiv').append(projects);
							
						}																																														
					}
				
				$("#loadingBar").hide();	
				timerControl("");
			  }
				
		   }
		
	   }); */
}
function backToHighlights() {
	$("#hProjListDiv").hide();
	$("#hListDiv").show();
	$('.hProjHeader').hide();
	$('.hListHeaderItems').show();
}
function AssociateToProject(obj) {
	conFunNew11(getValues(companyAlerts,"Alert_ShareHL"),'warning','AssociateToHLProject','', obj);
}

function AssociateToHLProject(obj) {
	var ascStatus = '';
	var projId = $(obj).attr('pId');

	$(obj).addClass('hEditBg');
	$(obj).find('div.userUnChecked').addClass('userChecked').removeClass('userUnChecked');
	
	// var cflag = conFunNew11("Do you want to share the highlights to this workspace?");
	// if (cflag) {
		// $("#loadingBar").show();
		timerControl("start");
		$("#loadMsg").hide();
		var msgId = $('#ascMsgId').val();

		// $.ajax({
		// 	url: path + "/ChatAuth",
		// 	type: "post",
		// 	data: { act: "associateToProject", userId: userIdglb, msgId: msgId, projId: projId, deviceType: "web", device: 'web' },
		// 	mimeType: "textPlain",
		// 	success: function (result) {
		let jsonbody = {
			"user_id" : userIdglb,
			"project_id" : projId,
			"msgid" : msgId,
			"company_id" : companyIdglb
		}
		checksession();
		$.ajax({
			url: apiPath + "/" + myk + "/v1/associateToProject",
			type: "POST",
			dataType: 'text',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				timerControl("");
			},
			success: function (result) {
				//alert(result);
				checkSessionTimeOut(result);
				// if (result != "SESSION_TIMEOUT") {
					// $("#loadingBar").hide();
					backToHighlights();
					timerControl("");
				// }
			}
		});
	// } else {
	// 	$(obj).find('div.userChecked').addClass('userUnChecked').removeClass('userChecked');
	// 	$(obj).removeClass('hEditBg');
	// }
}

//------------->highlight functions ends here-----------------------> 
var conversationId = '';
function getConvId(id) {
	let jsonbody = {
		"fromUser": userIdglb,
		"toUser": id
	}
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/checkConvId",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			// console.log(result);
			conversationId = result;
			checkSessionTimeOut(result);
			vConvId = result;
			$("#" + callId + "m").attr("conid", vConvId);
			$("#" + callId + "m").attr("index", 0);
			//getPreviousMessages(vConvId,callId);
			//fileSharing(callId+'m');
		}
	});

}


//---------datachannel methods ------------>
//-------- whiteboard code---------->
var designer;
var captureStream;
var wbFlag = true;
function setWhiteBoard(type) {
	boardAccessType = type;

	if ($('#mainHL').is(':visible')) {
		highlightVideo();
	}
	if ($('#chatFor').is(':visible')) {
		chatFor();
	}
	if ($('#tListMainDiv').is(':visible')) {
		transcriptVideo();
	}
	$("#wbLoader").show();
	if (callType == "A") {
		$("#audioFor").hide();
		$('#wbVideo').hide();
		var url = $("#aRemoteImage .callConnectImage").attr("src");
		var title = $("#aRemoteImage .callConnectImage").attr("title");
		$("#wbAudio").attr("src", url);
		$("#wbAudio").attr("title", title);
		$('#wbAudio').show();
	} else if (callType == "AV") {
		$("#videoFor").hide();
		$('#wbAudio').hide();
		$('#wbVideo').show();

		var wbVideo = document.getElementById("wbVideo");
		wbVideo.srcObject = document.getElementById('remoteVideo').srcObject;
	}
	wbFlag = false;
	wbResize();
	disableEnableWBoptions();
	$("#whiteboardFor").children("iframe").remove();
	$("#whiteboardFor").show();

	designer = new CanvasDesigner();
	designer.widgetHTML = path + '/scripts/board2/widget.html';
	// console.log("addSyncListener--");
	/*
	 * -----Below settings done it in widget.html file.
	 *
	designer.widgetJsURL = path+'/scripts/board/widget.js';
	designer.setSelected('pencil');
	
	designer.setTools({
		pencil: true,
		text: true,
		image: false,
		eraser: true,
		line: true,
		arrow: true,
		dragSingle: false,
		dragMultiple: false,
		arc: false,
		rectangle: true,
		quadratic: false,
		bezier: false,
		marker: true,
		zoom: false
	});
	*/
	designer.appendTo(document.getElementById("whiteboardFor"));
	
	// console.log("addSyncListener--");
	designer.addSyncListener(function (data) {
		// console.log("data--");
		// console.log("data--"+JSON.stringify(data));
		sendData(JSON.stringify(data));
	});
	//console.log("designer--"+designer);
}

var boardAccessType;
function boardReady() {
	designer.sync();
	if (boardAccessType == 'reciever') {
		var jsonText = '{"calltype":"' + callType + '","type":"whiteboard","status":"ready","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
		$("#wbLoader").hide();
		$("#whiteboardFor").children("iframe").contents().find("#tool-box").show();
	}
}

var wbOwner=false;
function openWhiteboard() {
	// console.log("wb flag:"+wbFlag);
	/*if(isChromeCall){
		   recognizationToggleStart('','globalVoiceCall','globalVoiceCall','');
	}*/
	if (wbFlag) {//--- 1st it will be true, once whiteboard is opened then setting to false until stop sharing.
		var jsonText = '{"calltype":"' + callType + '","type":"whiteboard","status":"on","callid":"' + callId + '"}';
		notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
		whiteboardArray.push(userIdglb.toString());
		$("#whiteboardu_" + userIdglb).show();
		wbOwner = true;
		setWhiteBoard("sender");
	} else {
		if ($('#mainHL').is(':visible')) {
			highlightVideo();
		}
		if ($('#chatFor').is(':visible')) {
			chatFor();
		}
		if ($('#tListMainDiv').is(':visible')) {
			transcriptVideo();
		}
		if (callType == "A") {
			$("#audioFor").hide();
		} else if (callType == "AV") {
			$("#videoFor").hide();
		}
		wbResize();
		$("#whiteboardFor").show();
	}
}

function wbResize() {
	//console.log($(window).width()+'--window--'+$(window).height())
	//console.log(screen.width+'--screen--'+screen.height);
 /*
	window.resizeTo(screen.width, screen.height - 50);
	window.screenX = 0;
	window.screenY = 0;
	*/
}
function disableEnableWBoptions(){
	//console.log("wbFlag:"+wbFlag+" wbOwner:"+wbOwner+" whoAdmin:"+whoAdmin);
	if(!wbFlag){
		if(wbOwner == true || whoAdmin ==userIdglb){
			$('#stopWBbutton').attr('onclick','stopwbalert()').css('opacity','1').css('cursor','pointer');
		}else{
			$('#stopWBbutton').removeAttr('onclick').css('opacity','0.4').css('cursor','default');
		}
	}
}

function toggleWhiteBoard() {
	$("#whiteboardFor").hide();
	if (callType == "A") {
		$("#audioFor").show();
	} else if (callType == "AV") {
		$("#videoFor").show();
	}
}

function stopSharingWhiteboard() {
	var jsonText = '{"calltype":"' + callType + '","type":"whiteboard","status":"off","callid":"' + callId + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
	hideWhiteBoard();
	$("#wbLoader").hide();
	var idx_wb = whiteboardArray.indexOf(userIdglb.toString());
	if (idx_wb > -1) {
		whiteboardArray.splice(idx_wb, 1);
	}
	$("#whiteboardu_" + userIdglb).hide();
}
function hideWhiteBoard() {
	wbFlag = true;
	wbOwner = false;
	toggleWhiteBoard();
	document.getElementById("wbVideo").srcObject = null;
}
function undoWB() {
	// console.log("designer.pointsLength:"+designer.pointsLength);
	designer.undo(-1);
}
function undoAllWB() {
	//designer.undo('all');
	designer.clearCanvas();
	// console.log("alldesigner.pointsLength:"+designer.pointsLength);
	//designer.sync();
	// console.log("alldesigner.pointsLength:"+designer.pointsLength);
}

function saveWB() {
	designer.toDataURL('image/png', function (dataURL) {
		//window.open(dataURL);
		//var linkToImage = document.getElementById('link-to-image');
		//linkToImage.href = dataURL;
		//linkToImage.download = 'image.' + ('image/png').split('/')[1];
		//linkToImage.click();
		$("#loadingBar").show();
		$("#loadingBar").find("#loadMsg").hide();
		timerControl("start");
		$("#loadMsg").hide();

		var data = atob(dataURL.substring("data:image/png;base64,".length)),
			asArray = new Uint8Array(data.length);

		for (var i = 0, len = data.length; i < len; ++i) {
			asArray[i] = data.charCodeAt(i);
		}
		var blob = new Blob([asArray.buffer], { type: "image/png" });
		var today = new Date();
		var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
		var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		var dateTime = date + ' ' + time;
		var fileName = "Whiteboard " + dateTime + ".png";
		// var formData = new FormData();
		// formData.append('file', blob, fileName);
		var fd = new FormData();
			fd.append('file', blob, fileName);
			fd.append('place', "ChatFileSharing");
			fd.append('user_id', userIdglb);
			fd.append('company_id', companyIdglb);
			fd.append('resourceId', "0");
			fd.append('convotype', "N");
			fd.append('menuTypeId', callId);
			fd.append('subMenuType', "chat");

		// var device = 'web';

		// $.ajax({
		// 	url: path + '/ChatFileSharing?userId=' + userIdglb + '&callId=' + callId + '&device=' + device + '&conId=0&status=N',
		// 	type: "POST",
		// 	data: formData,
		// 	processData: false,
		// 	contentType: false,
		// 	cache: false,
		// 	mimeType: "textPlain",
		// 	error: function (jqXHR, textStatus, errorThrown) {
		// 		checkError(jqXHR, textStatus, errorThrown);
		// 		$("#loadingBar").hide();
		// 		$("#loadingBar").find("#loadMsg").show();
		// 		timerControl("");
		// 	},
		// 	success: function (result) {
		checksession();
		$.ajax({
			url: apiPath + "/" + myk + "/v1/upload",
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			dataType: 'text',
			data: fd,
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				// $('#loadingBar').addClass('d-none').removeClass('d-flex');
			},
			success: function (result) {
				//console.log("result:"+result);
				checkSessionTimeOut(result);
				// if (result != "SESSION_TIMEOUT") {
					// result = result.split("@@")[0];
					//result = result.split("@@")[0];
					//var dbfile_name = result.split('//')[3];
					//var file_id = dbfile_name.split('.')[0];
					//var file_ext = dbfile_name.split('.')[1];
					var dbfile_path = result.split("@@")[0];
					var dbfile_name = result.split("@@")[2];
					var file_id = dbfile_name.split('.')[0];
					var file_ext = dbfile_name.split('.')[1];
					prepareWBImageList(dbfile_path,file_id,file_ext);


					var jsonText = '{"calltype":"' + callType + '","type":"whiteboard","status":"image","callid":"' + callId + '","src":"' + dbfile_path + '","filename":"' + dbfile_name + '"}';
					notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
					serverConnection.send(notify);

					$("#loadingBar").hide();
					timerControl("");
				// }
			}
		});

	});
}

function prepareWBImageList(result,file_id,file_ext){
	var UI = '<div  onmouseover="showwbmoreoptions(this);event.stopPropagation();" onmouseout="hidewbmoreoptions(this);event.stopPropagation();" style="position: relative;">'
				+'<img id="wbsrc" onclick="editwbconfirm(this);"  wbsrc='+result+' style="height: 60px;width: 100px;border:1px solid #bfbfbf;margin-top:5px;" src="' + result + '">'
				+'<div id="wbfloatoptionsID" class=" d-none mt-3 mr-2" style="border-radius:8px;bottom: 4px;position: absolute;padding: 4px;width: auto;height: auto;z-index: 1;border: 1px solid #c1c5c8;background-color: #fff;left: 35px;">' 
					+'<div class="d-flex align-items-center">'
					+"<img src=\"/images/conversation/expand.svg\" title=\"Expand\" onclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\" id='' class=\"image-fluid cursor\" style=\"margin: 0px 6px;height:12px;width:12px;\">"
					+'<img src="/images/conversation/edit.svg" title="Edit" wbsrc='+result+' id="" class="image-fluid cursor" style="margin: 0px 6px;height: 15px;width:15px;" onclick="editwbconfirm(this);event.stopPropagation();">'
					+'</div>'
				+'</div>'
			+'</div>'
	$('#wbImgList').children('span').remove();	
	$('#wbImgList').append(UI);
	$('#wbImgListDiv').show();
}
function hideWBImageList(){
	$('#wbImgListDiv').hide();
}
function showWBImageList(){
	if($('#wbImgList').children().length < 1){
        $('#wbImgList').html("<span>No data found</span>");
	}
	$('#wbImgListDiv').show();
}
function stopwbalert(){
	conFunNew11(getValues(companyAlerts,"Alert_StopWB"),'warning','stopSharingWhiteboard');
}

// var glbwbobj = "";
function editwbconfirm(obj){
	// glbwbobj=obj;
	// confirmFunNew(getValues(companyAlerts,"Alert_EditConf"),'warning','editWhiteboardImage', obj);
	confirmCopyMoveCancelNew(getValues(companyAlerts,"Alert_EditConf"),'warning','editWhiteboardImage','overrideWhiteboardImage','Insert', 'Overwrite', 'Cancel', obj);
}

function showwbmoreoptions(obj) {
	$(obj).find('#wbfloatoptionsID').removeClass('d-none').addClass('d-flex');
}

function hidewbmoreoptions(obj) {
	$(obj).find('#wbfloatoptionsID').removeClass('d-flex').addClass('d-none');
}

function editWhiteboardImage(obj){
	var objsrc = $(obj).attr('wbsrc');
	designer.editWbImageInCanvas(objsrc);
}

function overrideWhiteboardImage(obj){
	var objsrc = $(obj).attr('wbsrc');
	undoAllWB();
	designer.editWbImageInCanvas(objsrc);
}

function sendData(data) {
	for (var i = 0; i < userConnected.length; i++) {
		var id = userConnected[i];
		dataChannel[id].send(data);
	}
	//console.log('Sent Data: ' + data);
}

function onSendChannelStateChange() {

}

function receiveChannelCallback(event) {
	// console.log('Receive Channel Callback'+event);
	receiveChannel = event.channel;
	receiveChannel.onmessage = onReceiveMessageCallback;
	receiveChannel.onopen = onReceiveChannelStateChange;
	receiveChannel.onclose = onReceiveChannelStateChange;
}

function onReceiveMessageCallback(event) {
	var callsignal = JSON.parse(event.data);
	// console.log("inside receive channel");
	// console.log(event.data);
	var obj = callsignal.type;
	// console.log("obj"+obj);
	if(obj == 'aVTextTranscript'){
		glbRecResult = callsignal.transText;
		userFullName = callsignal.uFirstName;
		tranTimer = callsignal.tTimer;
		transDocId = callsignal.docId;
		showTranscript(glbRecResult, userFullName, tranTimer);
	}
	else{
		// console.log('Received Message:'+event.data);
		designer.syncData(JSON.parse(event.data));
	}
}

function onReceiveChannelStateChange() {
	var readyState = receiveChannel.readyState;
	// console.log('Receive channel state is: ' + readyState);
}

function closeCallChat(){
	$('#cmecallcontainer3').hide();
	$('#videocallcontainer3').hide();
	if($('#cmecontent').hasClass('cmecontainer')){
		$('#cmecallcontainer2').removeClass('col-lg-7').addClass('col-lg-11');
		$('#videocallcontainer2').addClass('col-lg-10').removeClass('col-lg-6');
		$('#cmecallcontainer').removeClass('col-lg-3').addClass('col-lg-1');
		$('#videocallcontainer2').removeClass('col-lg-8').addClass('col-lg-10');
	}else{
		// $('#cmecallcontainer2').removeClass('col-lg-7').addClass('col-lg-11');

	}

}

function chatInCall(type){
	if(type == '1'){
		if(callType == 'AV'){
			$('#videocallcontainer3').show();
			$('#videocallcontainer2').removeClass('col-lg-10').addClass('col-lg-6');
			$('#videoChatText').focus();
		}else{
			$('#cmecallcontainer3').show();
			$('#cmecallcontainer2').removeClass('col-lg-11').addClass('col-lg-7');
			$('#videoChatText').focus();
		}
	}else{
		if(callType == 'AV'){
			if($('#videocallcontainer3').is(':hidden')){
				$('#videocallcontainer3').show();
				$('#videocallcontainer2').removeClass('col-lg-10').addClass('col-lg-6');
				$('#videoChatText').focus();
			}else{
				$('#videocallcontainer3').hide();
				$('#videocallcontainer2').addClass('col-lg-10').removeClass('col-lg-6');
			}
		}
		else{
			if($('#cmecallcontainer3').is(':hidden')){
				$('#cmecallcontainer3').show();
				$('#cmecallcontainer2').removeClass('col-lg-11').addClass('col-lg-7');
				$('#videoChatText').focus();
			}else{
				$('#cmecallcontainer3').hide();
				$('#cmecallcontainer2').removeClass('col-lg-7').addClass('col-lg-11');
			}
		}
	}
	
}

function tilesPopup(){
	for (var i = 0; i < tilePreparationArray.length; i++) {
		
			var imageSrc = $('#userId_' + tilePreparationArray[i]).find('img').attr('data-src');
			var muteDis = "none";
			var wbprop = "none";
			var ssprop = "none";
			var title = $('#userId_' + tilePreparationArray[i]).find('.user_box_name').attr('value');
			if (tilePreparationArray[i] == userIdglb) {
				title = userFullname;
			}

			if (stazaofMute.indexOf(tilePreparationArray[i].toString()) > -1) {
				muteDis = "block";
				$('#raudiotiles_'+tilePreparationArray[i]).css('display', 'block');
			}
			if (whiteboardArray.indexOf(tilePreparationArray[i].toString()) > -1) {
				wbprop = "block"
			}
			if (screenshareArray.indexOf(tilePreparationArray[i].toString()) > -1) {
				ssprop = "block"
			}
			var UI ='';
			$("#tile_" + tilePreparationArray[i]).html('');
			
				if(callType == "AV"){
					UI='<div id="tileshow_' + tilePreparationArray[i] + '" class="flex-column rounded p-1 vcall5" style="display: none;min-height: 40px;z-index: 1;color: white;position: absolute;background-color: #316381;width: 240px;">'
				}
				else{
					UI='<div id="tileshow_' + tilePreparationArray[i] + '" class="flex-column rounded p-1" style="display: none;min-height: 40px;z-index: 1;color: white;position: absolute;top: -12px;left: 14px;background-color: #316381;width: 240px;">'
				}
				if (tilePreparationArray[i] == userIdglb) {
						UI+='<div id="hostb_' + tilePreparationArray[i] + '" onclick="oncallswitch();" class="pl-1" style="display:none;">'
						+'<div class="d-flex align-items-center cursor">Host</div>'
				}else{
					UI+='<div id="hostb_' + tilePreparationArray[i] + '" class="pl-1 mt-1" style="display:none;">'
					+'<div class="d-flex align-items-center">Host</div>'
				}
					
					UI+='</div>'
					+'<div id="" class="d-flex align-items-center">'
						+'<div id="" class="mr-auto pl-1">' + title + '</div>'
						if (tilePreparationArray[i] == userIdglb) {
							UI+='<img id="rdisc_' + tilePreparationArray[i] + '" class="userdisconnect1 cursor mr-2" onclick="hangup();" title="" src="' + path + '/images/cme/phonedis.svg" style="width: 18px;">' 
						}
						else{
							UI+='<img class="mr-2 cursor" type="chat" onclick="chatBack(\'on\');getNewConvId(' + tilePreparationArray[i] + ', this);" title="Message" src="' + path + '/images/cme/chatBack.svg" style="width: 18px;">' 
							+'<img class="mr-1 cursor" onclick="cmeCallExpandChatFor();chatInCall(\'1\');enterText(' + tilePreparationArray[i] + ')" title="Message" src="' + path + '/images/cme/at_new.svg" style="width: 22px;">' 
							+'<img id="rdisc_' + tilePreparationArray[i] + '" class="userdisconnect cursor mr-2" onclick="userDisconnect(' + tilePreparationArray[i] + ')" title="" src="' + path + '/images/cme/phonedis.svg" style="width: 18px;">' 
						}
						UI+='</div>'
				+'</div>'

				$("#tile_" + tilePreparationArray[i]).append(UI);

			// if (($("#" + callId + "mm").find("#participants_" + tilePreparationArray[i])).length == 0) {
			// 	$("#" + callId + "mm").append("<div class='videoUser' id='participants_" + tilePreparationArray[i] + "' ondblclick='enterText(" + tilePreparationArray[i] + ");' onclick='switchVideo(" + tilePreparationArray[i] + ")' style='cursor:pointer;width:100%;height:45px;display:inline-block;position:relative'><img title= '" + title + "' style=\"margin-left: 10px;float:left;height:35px;width:35px;margin-top:5px;margin-bottom:5px\" class=\"img-circle rounded-circle\" onerror=\"userImageOnErrorReplace(this);\" src=\"" + imageSrc + "\">"

			// 		+ '<div id="hostb_' + tilePreparationArray[i] + '" style="display:none;position:absolute;left:32px">'
			// 		+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031" style=""  >'
			// 		+ '</div>'

			// 		+ "<div style='margin-top:13px;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;width:60%'>" + title + ""
			// 		+ "<p id='videochat_" + tilePreparationArray[i] + "' style='display:none;color:#08B627;font-size:11px'></p>"

			// 		+ "</div>"
			// 		+ "<div /*style='padding-top:12px*/'>"
			// 		+ "<div style='position:absolute;background-color:#fff;right:0;padding-left:10px'>"
			// 		+ "<span class='userdisconnect' onclick='userDisconnect(" + tilePreparationArray[i] + ")' id='rdisc_" + tilePreparationArray[i] + "' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img src='" + path + "/images/temp/phonedis.svg' style='height:24px;width:24px'  /></span>"
			// 		+ "<span id='whiteboard_" + tilePreparationArray[i] + "' title='White Board' style='margin-top: 10px;margin-right: 5px;float:right;display:" + wbprop + "'><img style='    width: 24px;' src='" + path + "/images/temp/whiteboard.svg' /></span>"
			// 		+ "<span id='sharescreen_" + tilePreparationArray[i] + "' title='Screen share' style='margin-top: 10px;margin-right: 5px;float:right;display:" + ssprop + "'><img style='width:24px' src='" + path + "/images/temp/share.svg' /></span>"
			// 		+ "<span id='raudio_" + tilePreparationArray[i] + "' title='Audio is muted' style='margin-top: 10px;margin-right: 5px;float:right;display:" + muteDis + "'><img style='width:24px' src='" + path + "/images/temp/mic.svg' /></span>"
			// 		+ "<span id= 'rvideo_" + tilePreparationArray[i] + "' title='Video is paused' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img style='width:24px' src='" + path + "/images/temp/pause.svg' /></span>"
			// 		+ "</div>"
			// 		+ "</div>"
			// 		+ "</div>"
			// 	);
			// }
		// }
		// else{
		// 	var title = userFullname;
		// 		// if (user == "owner") {
		// 		// 	todisplay = "block";
		// 		// }
		// 		var muteDisplay = "none";
		// 		if (stazaofMute.indexOf(userIdglb.toString()) > -1) {
		// 			muteDisplay = "block"
		// 			$('#raudiotiles_'+userIdglb).css('display', 'block');
		// 		}

		// 		var wbprop1 = "none";
		// 		var ssprop1 = "none";
		// 		console.log("whiteboardArray"+whiteboardArray);
		// 		if (whiteboardArray.indexOf(userIdglb.toString()) > -1) {
		// 			wbprop1 = "block"
		// 		}
		// 		if (screenshareArray.indexOf(userIdglb.toString()) > -1) {
		// 			ssprop1 = "block"
		// 		}
		// 		var UI='';
		// 	$("#tile_" + tilePreparationArray[i]).html('');
			
		// 		if(callType == "AV"){
		// 			UI='<div id="tileshow_' + tilePreparationArray[i] + '" class="flex-column rounded" style="display: none;height: 40px;z-index: 1;color: white;position: absolute;bottom: 42px;left: 64px;background-color: #316381;width: 240px;">'
		// 		}
		// 		else{
		// 			UI='<div id="tileshow_' + tilePreparationArray[i] + '" class="flex-column rounded" style="display: none;height:40px;z-index: 1;color: white;position: absolute;top: -12px;left: 14px;background-color: #316381;width: 240px;">'
		// 		}
		// 			UI+='<div id="hostb_' + tilePreparationArray[i] + '" onclick="oncallswitch();" class="pl-1" style="display:none;">'
		// 				+'<div class="d-flex justify-content-start align-items-center">Host</div>'
		// 			+'</div>'
		// 			+'<div id="" class="d-flex align-items-center">'
		// 				+'<div id="" class="mr-auto pl-1">' + title + '</div>'
		// 				// +'<img id="sharescreenu_' + tilePreparationArray[i] + '" class="mr-2 mt-1 cursor" onclick="" title="Screen Share" src="' + path + '/images/temp/share.svg" style="display: '+ssprop1+';width: 16px;">' 
		// 				// +'<img id="whiteboardu_' + tilePreparationArray[i] + '" class="mr-2 mt-1 cursor" onclick="" title="White Board" src="' + path + '/images/temp/whiteboard.svg" style="display: '+wbprop1+';width: 16px;">' 
		// 				// +'<img class="mr-2 mt-1 cursor" onclick="getNewConvId(' + tilePreparationArray[i] + ');chatBack("on");" title="Message" src="' + path + '/images/menus/conversation.svg" style="width: 16px;">' 
		// 				// +'<img class="mr-1 cursor" onclick="chatInCall();enterText(' + tilePreparationArray[i] + ');" title="Message" src="' + path + '/images/cme/at_new.svg" style="width: 22px;">' 
		// 				+'<img id="rdisc_' + tilePreparationArray[i] + '" class="userdisconnect1 cursor mr-2 mt-1" onclick="hangup()" title="" src="' + path + '/images/cme/phonedis.svg" style="width: 18px;">' 
		// 			+'</div>'
		// 		+'</div>'

		// 		$("#tile_" + tilePreparationArray[i]).append(UI);
		// }
	}
	if (callType == "AV") {
		if ($("#videoFor").find(".arr").attr('src').indexOf("slideboxleft") != -1) {
			$("#videoFor").find(".arr").attr('src', path + '/images/temp/slideboxright.png')
			$("#videoFor").find(".arrow").css("left", "-2px");
			//$("#videoFor").find(".arr").css("width","36px");
		}
		else {
			$("#videoFor").find(".arr").attr('src', path + '/images/temp/slideboxleft.png');
			$("#videoFor").find(".arrow").css("left", "0px");
			//$("#videoFor").find(".arr").css('transform','rotate(90deg)');
			$("#videoFor").find(".arr").css("width", "41px");
		}
	}
	else if (callType == "A") {

		if ($("#audioFor").find(".arr").attr('src').indexOf("slideboxleft") != -1) {
			$("#audioFor").find(".arr").attr('src', path + '/images/temp/slideboxright.png');
			$("#audioFor").find(".arrow").css("left", "-2px");
			//$("#audioFor").find(".arr").css("width","36px");
		}
		else {
			$("#audioFor").find(".arr").attr('src', path + '/images/temp/slideboxleft.png');
			//$("#audioFor").find(".arrow").css("left","-37px");
			$("#audioFor").find(".arrow").css("left", "0px");
			//	$("#audioFor").find(".arr").css('transform','rotate(90deg)');
			$("#audioFor").find(".arr").css("width", "41px");
		}
	}

	$(".userdisconnect").hide();
	if (user == "owner") {
		$(".userdisconnect").show();
	}
	// if ($("#chatFor").is(":visible") == true) {
	// 	$("img.avchatImg").attr('src', path + '/images/temp/' + $("img.avchatImg").attr('toglsrc'));
	// } else {
	// 	$("img.avchatImg").attr('src', path + '/images/temp/' + $("img.avchatImg").attr('defsrc'));
	// }
	// checkChatWindowSize();
	// $("#" + callId + "m").find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
	resizeRemoteStream();
	// var imgs = lighttpdpath + "//userimages//" + userIdglb + "." + userImgType;
	// $("#tile_" + userIdglb).html('');
	// if ($("#tile" + userIdglb).length == 0) {
	// 	var todisplay = "none";
	// 	if (user == "owner") {
	// 		todisplay = "block";
	// 	}
	// 	// var muteDisplay = "none";
	// 	// if (stazaofMute.indexOf(userIdglb.toString()) > -1) {
	// 	// 	muteDisplay = "block"
	// 	// }

	// 	// var wbprop1 = "none";
	// 	// var ssprop1 = "none";
	// 	// //console.log("whiteboardArray"+whiteboardArray)
	// 	// if (whiteboardArray.indexOf(userIdglb.toString()) > -1) {
	// 	// 	wbprop1 = "block"
	// 	// }
	// 	// if (screenshareArray.indexOf(userIdglb.toString()) > -1) {
	// 	// 	ssprop1 = "block"
	// 	// }

	// 	$("#tile_" + userIdglb).html('');
	// 	$("#tile_" + userIdglb).append('<div id="tileshow_' + userIdglb + '" class="flex-column rounded p-1" style="display: none;height: 38px;z-index: 1;color: white;position: absolute;top: -12px;left: 14px;background-color: #316381;width: 200px;">'
	// 			+'<div id="hosta_' + userIdglb + '" class="pl-1" style="display:' + todisplay + ';">'
	// 				+'<div class="d-flex justify-content-start align-items-center">Host</div>'
	// 			+'</div>'
	// 			+'<div id="" class="d-flex align-items-center">'
	// 				+'<div class="mr-auto pl-1">' + userFullname + '</div>'
	// 				+'<img class="px-2 cursor" title="Message" src="' + path + '/images/menus/conversation.svg" style="width: 32px;">' 
	// 				+'<img id="user_pannel" class="userdisconnect1 cursor mr-2" onclick="hangup();" title="" src="' + path + '/images/temp/phonedis.svg" style="width: 18px;">' 
	// 			+'</div>'
	// 		+'</div>');

		// $("#" + callId + "mm").append("<div class='videoUser' id='participants_" + userIdglb + "' ondblclick='enterText(" + userIdglb + ");' onclick='switchVideo(" + userIdglb + ")' style='position:relative;cursor:pointer;width:100%;height:45px;display:inline-block'><img title= '" + userFullname + "' style=\"margin-left: 10px;float:left;height:35px;width:35px;margin-top:5px;margin-bottom:5px\" class=\"img-circle rounded-circle\" onerror=\"userImageOnErrorReplace(this);\" src=\"" + imgs + "\">"

		// 	+ '<div id="hosta_' + userIdglb + '" style="position:absolute;left:32px;display:' + todisplay + ';" onclick="oncallswitch();">'
		// 	+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/myprojects.png" projecttype="my_project" projectid="6031" style="" >'
		// 	+ '</div>'


		// 	+ "<div style='margin-top:13px;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;width:60%'>" + userFullname + ""
		// 	+ "<p id='videochat_" + userIdglb + "' style='display:none;color:#08B627;font-size:11px'></p>"

		// 	+ "</div>"
		// 	+ "<div /*style='padding-top:12px'*/>"
		// 	//+'<span id="raudiohostpar_'+userIdglb+'" title="Audio is muted" style="display:'+muteDisplay+';margin-right: 5px"><img src="'+path+'/images/temp/mic1.png"></span>'	
		// 	+ "<div style='position:absolute;background-color:#fff;right:0;padding-left:10px'>"
		// 	+ '<span class="userdisconnect1" onclick="hangup();" id="user_pannel" style="margin-top: 10px;margin-right: 5px;float: right;"><img src="' + path + '/images/temp/phonedis.svg" style="height:24px;width:24px"></span>'
		// 	+ "<span id='whiteboardu_" + userIdglb + "' title='White Board' style='margin-top: 10px;margin-right: 5px;float:right;display:" + wbprop1 + "'><img style='width:24px' src='" + path + "/images/temp/whiteboard.svg' /></span>"
		// 	+ "<span id='sharescreenu_" + userIdglb + "' title='Screen Share' style='margin-top: 10px;margin-right: 5px;float:right;display:" + ssprop1 + "'><img style='width:24px' src='" + path + "/images/temp/share.svg' /></span>"
		// 	+ '<span id="raudiohostpar_' + userIdglb + '" title="Audio is muted" style="margin-top: 10px;margin-right: 5px;display:' + muteDisplay + ';float:right"><img style="width:24px" src="' + path + '/images/temp/mic.svg"></span>'
		// 	+ "<span id= 'rvideohost_" + userIdglb + "' title='Video is paused' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img  style='width:24px' src='" + path + "/images/temp/pause.svg' /></span>"
		// 	+ "</div>"
		// 	+ "</div>"
		// 	+ "</div>"
		// );
	// }

	if (user != "owner") {
		$("div[id^=host_]").css("border", "none");
		$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");
		$("div[id^=hostp_]").css("display", "none");
		$("#hostp_" + whoAdmin).css("display", "block");
		$("div[id^=hostu_]").css("border", "none");
		$("#hostu_" + whoAdmin).css("border", "3px solid #FFCA00");
		$("div[id^=hosta_]").css("display", "none");
		$("#hosta_" + whoAdmin).css("display", "block");
		$("div[id^=hostb_]").css("display", "none");
		$("#hostb_" + whoAdmin).css("display", "block");
	}
	else{
		$("#hosta_" + whoAdmin).css("display", "block");
		$("#hosta_" + whoAdmin).attr("onclick", "oncallswitch();");
		$("#hostb_" + whoAdmin).css("display", "block");
		$("#hostb_" + whoAdmin).attr("onclick", "oncallswitch();");
	}
	$("#" + callId + "mm").find(".videoUser").sort(Ascending_sort).appendTo($("#" + callId + "mm"));
}

function chatFor() {
	$("#" + callId + "mm").html("");
	var pcount = parseInt(tilePreparationArray.length);
	$(".pcnt").html(pcount);
	tilesPopup();
	$("#" + callId + "mm").append("<div  style='width:100%;display:inline-block;padding-left: 6px;font-family: opensanscondbold;font-size: 15px;color: #747778;padding-top: 1px;padding-bottom: 2px; border-bottom: 1px solid #CED2D5'>Participants: <span id='pcount'>" + pcount + "<span></div>");
	for (var i = 0; i < tilePreparationArray.length; i++) {
		if (tilePreparationArray[i] != userIdglb) {
			var imageSrc = $('#userId_' + tilePreparationArray[i]).find('img').attr('data-src');
			var title = $('#userId_' + tilePreparationArray[i]).find('.user_box_name').attr('value');
			var muteDis = "none";
			var wbprop = "none";
			var ssprop = "none";
			if (stazaofMute.indexOf(tilePreparationArray[i].toString()) > -1) {
				muteDis = "block";
			}
			if (whiteboardArray.indexOf(tilePreparationArray[i].toString()) > -1) {
				wbprop = "block"
			}
			if (screenshareArray.indexOf(tilePreparationArray[i].toString()) > -1) {
				ssprop = "block"
			}
			if (($("#" + callId + "mm").find("#participants_" + tilePreparationArray[i])).length == 0) {
				// $("#" + callId + "mm").append("<div class='videoUser' id='participants_" + tilePreparationArray[i] + "' ondblclick='enterText(" + tilePreparationArray[i] + ");' onclick='switchVideo(" + tilePreparationArray[i] + ")' style='cursor:pointer;width:100%;height:45px;display:inline-block;position:relative'><img title= '" + title + "' style=\"margin-left: 10px;float:left;height:35px;width:35px;margin-top:5px;margin-bottom:5px\" class=\"img-circle rounded-circle\" onerror=\"userImageOnErrorReplace(this);\" src=\"" + imageSrc + "\">"

				// 	+ '<div id="hostb_' + tilePreparationArray[i] + '" style="display:none;position:absolute;left:32px">'
				// 	+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031" style=""  >'
				// 	+ '</div>'


				// 	+ "<div style='margin-top:13px;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;width:60%'>" + title + ""
				// 	+ "<p id='videochat_" + tilePreparationArray[i] + "' style='display:none;color:#08B627;font-size:11px'></p>"

				// 	+ "</div>"
				// 	+ "<div /*style='padding-top:12px*/'>"
				// 	+ "<div style='position:absolute;background-color:#fff;right:0;padding-left:10px'>"
				// 	+ "<span class='userdisconnect' onclick='userDisconnect(" + tilePreparationArray[i] + ")' id='rdisc_" + tilePreparationArray[i] + "' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img src='" + path + "/images/temp/phonedis.svg' style='height:24px;width:24px'  /></span>"
				// 	+ "<span id='whiteboard_" + tilePreparationArray[i] + "' title='White Board' style='margin-top: 10px;margin-right: 5px;float:right;display:" + wbprop + "'><img style='    width: 24px;' src='" + path + "/images/temp/whiteboard.svg' /></span>"
				// 	+ "<span id='sharescreen_" + tilePreparationArray[i] + "' title='Screen share' style='margin-top: 10px;margin-right: 5px;float:right;display:" + ssprop + "'><img style='width:24px' src='" + path + "/images/temp/share.svg' /></span>"
				// 	+ "<span id='raudio_" + tilePreparationArray[i] + "' title='Audio is muted' style='margin-top: 10px;margin-right: 5px;float:right;display:" + muteDis + "'><img style='width:24px' src='" + path + "/images/temp/mic.svg' /></span>"
				// 	+ "<span id= 'rvideo_" + tilePreparationArray[i] + "' title='Video is paused' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img style='width:24px' src='" + path + "/images/temp/pause.svg' /></span>"
				// 	+ "</div>"
				// 	+ "</div>"
				// 	+ "</div>"
				// );

			}
		}
	}
	$("#chatFor").toggle();
	/* if($("#arr").attr('src').indexOf("arrowl")!=-1){
		 $("#arr").attr('src','/images/temp/arrowr.png')
	 }
	 else{
		 $("#arr").attr('src','/images/temp/arrowl.png');
	 }*/

	if (callType == "AV") {
		if ($("#videoFor").find(".arr").attr('src').indexOf("slideboxleft") != -1) {
			$("#videoFor").find(".arr").attr('src', path + '/images/temp/slideboxright.png')
			$("#videoFor").find(".arrow").css("left", "-2px");
			//$("#videoFor").find(".arr").css("width","36px");
		}
		else {
			$("#videoFor").find(".arr").attr('src', path + '/images/temp/slideboxleft.png');
			$("#videoFor").find(".arrow").css("left", "0px");
			//$("#videoFor").find(".arr").css('transform','rotate(90deg)');
			$("#videoFor").find(".arr").css("width", "41px");
		}
	}
	else if (callType == "A") {

		if ($("#audioFor").find(".arr").attr('src').indexOf("slideboxleft") != -1) {
			$("#audioFor").find(".arr").attr('src', path + '/images/temp/slideboxright.png');
			$("#audioFor").find(".arrow").css("left", "-2px");
			//$("#audioFor").find(".arr").css("width","36px");
		}
		else {
			$("#audioFor").find(".arr").attr('src', path + '/images/temp/slideboxleft.png');
			//$("#audioFor").find(".arrow").css("left","-37px");
			$("#audioFor").find(".arrow").css("left", "0px");
			//	$("#audioFor").find(".arr").css('transform','rotate(90deg)');
			$("#audioFor").find(".arr").css("width", "41px");
		}
	}

	$(".userdisconnect").hide();
	if (user == "owner") {
		$(".userdisconnect").show();
	}
	if ($("#chatFor").is(":visible") == true) {
		$("img.avchatImg").attr('src', path + '/images/temp/' + $("img.avchatImg").attr('toglsrc'));
	} else {
		$("img.avchatImg").attr('src', path + '/images/temp/' + $("img.avchatImg").attr('defsrc'));
	}
	checkChatWindowSize();
	$("#" + callId + "m").find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
	resizeRemoteStream();
	var imgs = lighttpdpath + "//userimages//" + userIdglb + "." + userImgType;
	if ($("#participants_" + userIdglb).length == 0) {
		var todisplay = "none";
		if (user == "owner") {
			todisplay = "block";
		}
		var muteDisplay = "none";
		if (stazaofMute.indexOf(userIdglb.toString()) > -1) {
			muteDisplay = "block"
		}

		var wbprop1 = "none";
		var ssprop1 = "none";
		//console.log("whiteboardArray"+whiteboardArray)
		if (whiteboardArray.indexOf(userIdglb.toString()) > -1) {
			wbprop1 = "block"
		}
		if (screenshareArray.indexOf(userIdglb.toString()) > -1) {
			ssprop1 = "block"
		}

		// $("#" + callId + "mm").append("<div class='videoUser' id='participants_" + userIdglb + "' ondblclick='enterText(" + userIdglb + ");' onclick='switchVideo(" + userIdglb + ")' style='position:relative;cursor:pointer;width:100%;height:45px;display:inline-block'><img title= '" + userFullname + "' style=\"margin-left: 10px;float:left;height:35px;width:35px;margin-top:5px;margin-bottom:5px\" class=\"img-circle rounded-circle\" onerror=\"userImageOnErrorReplace(this);\" src=\"" + imgs + "\">"

		// 	+ '<div id="hosta_' + userIdglb + '" style="position:absolute;left:32px;display:' + todisplay + ';" onclick="oncallswitch();">'
		// 	+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/myprojects.png" projecttype="my_project" projectid="6031" style="" >'
		// 	+ '</div>'


		// 	+ "<div style='margin-top:13px;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;width:60%'>" + userFullname + ""
		// 	+ "<p id='videochat_" + userIdglb + "' style='display:none;color:#08B627;font-size:11px'></p>"

		// 	+ "</div>"
		// 	+ "<div /*style='padding-top:12px'*/>"
		// 	//+'<span id="raudiohostpar_'+userIdglb+'" title="Audio is muted" style="display:'+muteDisplay+';margin-right: 5px"><img src="'+path+'/images/temp/mic1.png"></span>'	
		// 	+ "<div style='position:absolute;background-color:#fff;right:0;padding-left:10px'>"
		// 	+ '<span class="userdisconnect1" onclick="hangup();" id="user_pannel" style="margin-top: 10px;margin-right: 5px;float: right;"><img src="' + path + '/images/temp/phonedis.svg" style="height:24px;width:24px"></span>'
		// 	+ "<span id='whiteboardu_" + userIdglb + "' title='White Board' style='margin-top: 10px;margin-right: 5px;float:right;display:" + wbprop1 + "'><img style='width:24px' src='" + path + "/images/temp/whiteboard.svg' /></span>"
		// 	+ "<span id='sharescreenu_" + userIdglb + "' title='Screen Share' style='margin-top: 10px;margin-right: 5px;float:right;display:" + ssprop1 + "'><img style='width:24px' src='" + path + "/images/temp/share.svg' /></span>"
		// 	+ '<span id="raudiohostpar_' + userIdglb + '" title="Audio is muted" style="margin-top: 10px;margin-right: 5px;display:' + muteDisplay + ';float:right"><img style="width:24px" src="' + path + '/images/temp/mic.svg"></span>'
		// 	+ "<span id= 'rvideohost_" + userIdglb + "' title='Video is paused' style='margin-top: 10px;margin-right: 5px;float:right;display:none'><img  style='width:24px' src='" + path + "/images/temp/pause.svg' /></span>"
		// 	+ "</div>"
		// 	+ "</div>"
		// 	+ "</div>"
		// );
	}

	if (user != "owner") {
		$("div[id^=host_]").css("border", "none");
		$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");
		$("div[id^=hostp_]").css("display", "none");
		$("#hostp_" + whoAdmin).css("display", "block");
		$("div[id^=hostu_]").css("border", "none");
		$("#hostu_" + whoAdmin).css("border", "3px solid #FFCA00");
		$("div[id^=hosta_]").css("display", "none");
		$("#hosta_" + whoAdmin).css("display", "block");
		$("div[id^=hostb_]").css("display", "none");
		$("#hostb_" + whoAdmin).css("display", "block");
	}
	$("#" + callId + "mm").find(".videoUser").sort(Ascending_sort).appendTo($("#" + callId + "mm"));
}

function openGroupVideoChatBox(id) {
	var imgSrc = $('#userId_' + id).find('img').attr('data-src');
	var userName = $('#userId_' + id).find('.user_box_name').text();
	var UI = "";

	// $('#videocallchat').html('');
	// $('#videocallcontainer4').html('');
	// $('#audiocallchat').html('');
	// $('#cmecallcontainer4').html('')

	// var ui = "<div  id=\"" + callId + "m\"  conId = '" + vConvId + "' type='group' indexRg='0'  class=\"row\" style='margin-left:1px;margin-right:1px;    border: 1px solid #ccc;'>"
	// 	+ "<div  class='' style='display:flex;height:33.2vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;background-color:white;border-radius:0px;overflow:auto' class='well'>"
	// 				/*+"<div><img id='backVchat' title='contacts' onclick='chatFor()' style='margin-bottom: 1vh; cursor: pointer; float: right; margin-top: 1.3vh; height: 18px;    min-height: 18px; min-width: 16px; width: 16px;margin-right: 10px;' src='"+path+"/images/back1.png'\></div>"	
	// 				*/+ "<div id=\"" + callId + "mm\" style='height:33.2vh;'>"

	// 	+ "</div>"
	// 	// +'<div style="display: flex;align-items: flex-end;    margin-left: -1px"><img class="" style="width:9px;height:24px;" title="Host" src="'+path+'/images/temp/updown.png" >'
	// 	+ '</div>'
	// 	/*+"<span style='height:10vh;padding-top:2.3vh;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;'>"+NameforChatRoom+""
	// 		+"<p class='userStatus_"+id+"' style='display:none;color:#08B627;font-size:11px'>&nbsp;&nbsp;</p>"
	// 		+"<p class='userPresStatus_"+id+"' style='display:block;color:#08B627;font-size:11px'>&nbsp;&nbsp;</p>"
	// 	+"</span>"*/


	// 	// +"<img onclick=\"endVideoCall('"+name+"');\" class='video' title='End Call' style='margin-bottom: 1vh; cursor: pointer; float: right; margin-top: 3.3vh; height: 18px; min-height: 18px; min-width: 16px; width: 16px; margin-right: 10px;' src='"+path+"/images/videoDisconn.png'\>"
	// 	+ '<div style="display: flex;/* align-items: flex-end; *//* position: relative; *//* margin-left: -1px; *//* top: 9%; *//* left: 14%; *//* right: -60%; */float: inline-end;justify-content: center;align-items: center;    background: transparent;    /* float: revert; */"><img id="checkArr" onclick="resizeIt();" class="" style="width:30px;height:24px;    cursor: pointer;    position: absolute;    margin-top: 12px;z-index:1" title="" src="' + path + '/images/temp/downarr.svg"></div>'


	// 	+ "<div id='videoChat' style='font-size:11px;font-weight: bold;display:block;margin-left: 2vh; margin-top: -2vh;;float: left;color: green; padding-bottom: 5px;'></div>"
	// 	+ "<div id='chat-dialog' style='border: 1px solid #cccccc;overflow: auto;    background-color: #fff;height:56vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;border-radius:0px;border-width:1px 0px;    padding-top: 22px;' id='conv' class='well'>"
	// 	+ "</div>"

	// 	/*+"<div id='videoText' >"
	// 		+"<div class='col-xs-10 col-md-11'id='callText' style='border-right: 1px solid #c1c5c8;height:8vh;padding:0;width:87%'>"
	// 			+"<textarea id='videoChatText' type='text'   style='height:8vh;min-height:6.5vh;padding:2px;border-radius:0%;width:100%;border:none'  onkeypress='sendingVideoChat(event,this)' placeholder='Type message here...' class='form-control'></textarea>"
	// 		+"</div>"*/
	// 	/*+"<div align='center' class='col-xs-2 col-md-1 chatpost' style='margin-left:0px;height:8vh;width:20%;padding-left:0px;padding-right:0px;border-left:1px solid #ccc'>"
	// 		+"<img class='' src='"+path+"/images/post.png' onclick='sendingVideoChat(event,this)' style='margin-top:2vh;vertical-algin:middle;display:flex'>"
	// 	+"</div>"*/
	// 	/*	+'<div class=" chatpost" style="margin-left:0px;background-color:white;height:10vh;float: right;width: 40px;text-align: center;">'
	// 			+'<img id="postimg" class="" src="'+path+'/images/workspace/post.png" onclick="sendingVideoChat(event,this)" style="vertical-align: middle; margin-top: 10px;">'
	// 		+'</div>'*/

	// 	+ '<div id="videoText" style="">'
	// 	+ '<div class="col-sm-12 col-xs-12" style="/* height: 50px; */padding-left:0px;padding-right:0px;">'
	// 	// + ' <textarea id="videoChatText" class="main_commentTxtArea" onkeypress="sendingVideoChat(event,this)" placeholder="Type message here..."  spellcheck="false" style="background-image: none;    width: 82%;"></textarea>'
	// 	+ ' <div id="convoPost" align="center" class="main_commentTxtArea_btnDiv" style="width:18%">'
	// 	+ '<div style="width: 56%;" onclick="sendingVideoChat(event,this)"><img class="img-responsive Post_cLabelTitle" src="' + path + '/images/temp/post.png" title="Post" style="margin-top: 10px;cursor:pointer;" alt="Image"></div>'
	// 	+ ' </div>'

	// 	+ "</div>"
	// 	+ "</div>";

	// $('#chatFor').html(ui);

	//showGroupUsers(id); 
	UI='<div id="updatecallmsg" class="compactcme3  mt-1 wsScrollBar" style="overflow-x: hidden;overflow-y: auto;"></div>'
		+"<div  id='' style='' class='form-group bg-white'>"+
		"<div id ='' class='px-2 pt-2 pb-1 m-0  border border-left-0 border-right-0 border-bottom-0' style='width: 99%;'>"+
			"<textarea id='videoChatText' gid='' onpaste ='onPaste(event,this);' onkeypress='sendingVideoChat(event,this);commentAutoGrow(event,this,\"chat\");' onkeyup='commentAutoGrow(event,this,\"chat\");'   onblur ='commentAutoGrow(event,this,\"chat\");' oninput ='commentAutoGrow(event,this,\"chat\");' type='text' style='font-size:14px;resize: none;'  placeholder='Type message here...' class='cmetextarea rounded'></textarea>"+
		"</div>"+
		'<div class="col-12  px-1 d-flex justify-content-between mt-0">'+
			'<div class="d-flex justify-content-between mt-1">'+
				'<div class="d-none py-1 px-2  convoSubIcons " onclick="">'+
					'<img src="/images/conversation/mic.svg" class="image-fluid" title="Record audio" style="height:22px;cursor:pointer;">'+
				'</div>'+
				'<div class="d-none py-1 px-2 position-relative convoSubIcons ">'+
					'<img src="/images/conversation/attach.svg" class="image-fluid " style="height:20px;cursor:pointer;" title="Upload" onclick="">'+
				'</div>'+
				'<div class="d-none py-1 px-2 convoSubIcons ">'+
					'<img src="/images/conversation/image.svg" class="image-fluid" title="From Gallery" style="height:20px;cursor:pointer;">'+
				'</div>'+
				'<div class="d-none py-1 px-2 convoSubIcons ">'+
					'<img src="/images/conversation/cam.svg" onclick="" class="image-fluid" title="Camera" style="height:20px;cursor:pointer;">'+
				'</div>'+
				'<div class="d-none py-1 px-2 convoSubIcons" onmouseover="changeSmileyellow(\'main\');" onmouseout="changeSmilewhite(\'main\');">'+
					'<img src="/images/conversation/smile.svg" id="" title="" class="image-fluid" style="height:22px;cursor:pointer;">'+
				'</div>'+
				'<div class="d-none py-1 px-2 convoSubIcons">'+
					'<img src="/images/conversation/at.svg" id="" title="" class="image-fluid atImgExt" style="height:20px;cursor:pointer;" onclick="">'+
				'</div>'+
				'<div class="d-none py-1 px-2 convoSubIcons">'+
					'<img src="/images/conversation/hash.svg" id="" title="" class="image-fluid hashImgExt" style="height:19px;cursor:pointer;" onclick="">'+
				'</div>'+
			'</div>'+
			'<div class="d-flex justify-content-between">'+
				'<div class="p-1 mx-2">'+
					'<img class="chatpost" src="/images/conversation/post1.svg" title="Post" onclick="sendingVideoChat(event,this);" style="height:26px;cursor:pointer;">'+
					'<input type="hidden" id="" value="groupchat" />'+
				'</div>'+
			'</div>'+
		'</div>'+
	"</div>"

	
				
				// +'<div class="avTimer mr-2 float-right" id="aTimer" style="color: #fff;font-weight: bold;padding: 5px;"></div>'

	if(callType != "A"){
		$('#videocallchat').html(UI);
		// $('#videocallcontainer4').prepend(UI2);
	}
	else{
		$('#audiocallchat').html(UI);
		// $('#cmecallcontainer4').prepend(UI2);
	}
	fileSharing('updatecallmsg');  // Function call to initiate file sharing.


	var windowWidth = $(window).width();
	if (windowWidth > 750) {
		$("#callText").css("width", "90%");
		$("#postimg").css("margin-top", "13px");
	}
	else {
		$("#callText").css("width", "87%");
		$("#postimg").css("margin-top", "10px");
	}

	$('textarea.chatbox2').focus(function () {
		$(this).css('border', '1px solid blue');
	});

	$('textarea.chatbox2').blur(function () {
		$(this).css('border', '1px solid violet');
	});
	var userpres;
	if ($('#' + id + "-" + cmeDomain + "-colabus-comm").find('.user_box_status').hasClass("user_box_status_online")) {
		userpres = "online";
		$('.userPresStatus_' + id).css({ "color": "#08B627" });
	} else {
		userpres = "offline";
		$('.userPresStatus_' + id).css({ "color": "gray" });
	}
	$('.userPresStatus_' + id).show().text(userpres);
}


function sendingVideoChat(ev, obj) {

	//var jid= callerId+"@"+cmeDomain+".colabus.com"; 
	//console.log("---");
	var status = 'N';
	var d = new Date();

	if (ev.which == 13 || ev.which == 1) {

		if (ev.which == 13) {
			ev.preventDefault();
			var body = $(obj).val();
		}
		if (ev.which == 1) {
			ev.preventDefault();
			var body = $('#videoChatText').val();
		}

		if ($.trim(body).length < 1)
			return;
		
		//var Id=id.split("-")[0];
		// console.log("Id--"+Id+"--userId--"+userIdglb);
		var t = getTimeIn12Format(d);
		var msg = textTolink(body);
		var imgName = lighttpdpath + "/userimages/" + userIdglb + "." + userImgType + "?" + d.getTime();

		// $("#updatecallmsg").append(              // preparing the chat message structure to be appended on chatBox
		// 	"<div class='chat-outgoing-overall row'>" +
		// 	"<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>" +
		// 	"<img   src='' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%; float:right;margin-left:-2.3vh;margin-top: 1vh;margin-bottom:2vh;'/></div>" +
		// 	"<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right'>" +
		// 	"<div style='margin: 10px 36px;' class='chat-outgoing-message singlebubble2 row' >" +
		// 	msg +
		// 	"</div>" +
		// 	"<div class='chat-outgoing-time row' style='margin-left: -36px;'><span>" + t + "</span></div></div>");



		//    Gab.scroll_chat(Gab.jid_to_id(jid));
		// $('#' + callId + "m").find('textarea').val('');
		// $("#" + callId + "m").find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
		// console.log("body---"+body);
		var windowWidth1 = $(window).width();

		if (windowWidth1 < 750) { // to resize the chat conversation UI for smaller size
			//$(".chatOutbubble").css({'padding-right':'2.3em'});
		} else {
			//$(".chatOutbubble").css({'padding-right':'0.3em'});
		}

		//  var n = d.getTimezoneOffset();
		var m = d.getMonth();
		var time = d.getHours() + ":" + d.getMinutes() + "###" + d.getDate() + "-" + (Number(m) + 1) + "-" + d.getFullYear();
		var timeZone = getTimeOffset(d);
		// console.log("Timezone----"+timeZone);
		//var convId=$("#"+id).attr('conId');


		// console.log(status);
		//$("#chat-dialog").mCustomScrollbar("update");
		body = body.replace("'", "CHR(39)");


		// $.ajax({
		// 	url: path + "/ChatAuth",
		// 	type: "post",
		// 	data: { act: "insertChatMsg", convId: '0', sender: userIdglb, message: body, time: time, timeZone: timeZone, msgStatus: "Y", msgType: "calltext", docId: '0', callId: callId, docExt: '', docPath: '', device: 'web' },
		// 	mimeType: "textPlain",
		// 	success: function (result) {
		var localOffsetTime = getTimeOffset(new Date());
		let jsonbody = {
			"convId" : "0",
			"sender" : userIdglb,
			"msg" : body,
			"msgTime" : time,
			"localTZ" : localOffsetTime,
			"message_status" : "Y",
			"msg_reply_id" : "",
			"chatFortype" : "",
			"callId" : callId,
			"msg_type" : "calltext",
			"docId" : "",
			"docExt" : "",
			"docPath" : ""
		}
		checksession();
		$.ajax({
			url: apiPath + "/" + myk + "/v1/insertChatMsg",
			type: "POST",
			dataType: 'text',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				timerControl("");
			},
			success: function (result) {
				checkSessionTimeOut(result);
				var msgid = result.split('@@')[1];

				//var message = $msg({to : jid, "type" : "chat" }).c('body').t(body).up().c('active', {xmlns: "http://jabber.org/protocol/chatstates"}); // preapring the chat stanza to send		 
				var jsonText = '{"calltype":"' + callType + '","type":"videochat","callid":"' + callId + '","msg":"' + body + '","msgid":"' + msgid + '"}';
				var message = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);; // preapring the chat stanza to send		 

				serverConnection.send(message);  // sending the chat message

				
				let UI = "<div id='" + msgid + "' class='media chat mr-3'>" +
				"<div class='media-body mt-2 chatId' uid='" + userIdglb + "'>" +
					"<div onclick='replyForChat(this)' ondblclick='replyMsg()' messageType='text' msg='" + body + "' class='px-3 py-2 chat-outgoing-message singlebubble2 bubble2 float-right' style='margin-left: 30%;background-color: #007b97;word-break: break-word;'>"
					// if (chatType == "reply") {
					// 	UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
					// 		"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
					// }
					UI += "" + body + "</div>" +
							"<div class='float-right mt-1' style='text-align: right;width: 100%;font-size: 11px;color: gray;'>" + t + "</div>" +
						"</div>" +
						"<img src='" + imgName + "' class='align-self-start ml-3 mt-2 rounded-circle' title='" + userFullname + "' onerror='userImageOnErrorReplace(this)'; style='display: none;width: 40px;'>" +
					"</div>"

					$("#updatecallmsg").append(UI);
					$('#updatecallmsg').animate({ scrollTop: 20000 }, 'normal');
					$("#videoChatText").val('');

			}
		});
		//onloadGroup();
	} else {
		// console.log("inside sending");
		var notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('composing', { xmlns: "http://jabber.org/protocol/chatstates" });
		serverConnection.send(notify);
	}
}
function videoChatUI(message) {

	var signal = JSON.parse($(message).children('json').text());
	callId = signal.callid;
	var body = signal.msg;
	var msgid = signal.msgid;

	if (true) {
		var d = new Date();
		var t = getTimeIn12Format(d);
		var full_jid = $(message).attr('from');
		var from = $(message).attr('from');
		var jidImage = Strophe.getBareJidFromJid(full_jid);
		var jid_id = Gab.jid_to_id(jidImage);
		var id = jid_id.split("-")[0];
		var imageurl;
		var nick = Strophe.getResourceFromJid(from);
		var senderName;
		var userid;
		if (nick !== null) {
			senderName = nick.split('_')[2];
			userid = nick.split('_')[1];
			imageurl = $('#userId_' + userid).find('img').attr('data-src');
		}
		var imgType = $('#userId_' + id).attr('imgtype');//edited
		var type;
		var link;
		if ((body.indexOf('xml') > 0) && (body.indexOf('href') > 0) && (body.indexOf('filesharing') > 0)) {
			// var $xml = $.parseXML(body);
			// link = $($xml).find('href').attr('value');
			// type = 'filesharing';
			// var filename = $($xml).find('href').attr('fileName');


			if ($("#" + msgid).length == 0) {
				var UI = '';
				var $xml = $.parseXML(body);
				link = $($xml).find('href').attr('value');
				type = 'filesharing';
				filename = $($xml).find('href').attr('fileName');
				// if (msg_action == "forward") {
				// 	var splitt = link.split('uploadedDocuments/')[1];
				// 	var splitting = splitt.split('?')[0];
				// 	var file_id = splitting.split('.')[0];
				// 	var file_ext = splitting.split('.')[1];
				// 	// console.log("link--" + link + "splitting--" + splitting);

				// 	if (filename.indexOf('CH(38)') > 0) {
				// 		filename = filename.replace("CH(38)", "&");
				// 	}
				// 	//    $("#conversationContactMsg_"+uid).html(filename);
				// 	//    $('#conversationContactMsg_'+uid).css({'font-weight':' bold'});
				// 	//var imgUrl = lighttpdPath+"/userimages/"+id+"."+imgType ;
				// 	// var test = ChangeImage1(imgUrl,filename);
				// 	UI = "<div id='" + msgId + "' class='media py-1 chat' style='display: -webkit-box;'>" +
				// 			"<img src='" + imgName + "' class='align-self-start mt-4 mr-3 rounded-circle' title='" + senderName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
				// 			"<div class='media-body chatId' uid='" + userid + "'>" +
				// 				"<div style='font-size: 13px;color: gray;'>" + senderName + "</div>"

				// 	UI += "<div onclick='replyForChat(this)' from='group' ondblclick='replyMsg()' messageType='file' msg='" + filename + "' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;'>"
				// 	if (msg_action == "forward") {
				// 		UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
				// 			"<img title='Forwarded' class='forwardIconCls' src='/images/temp/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>"
				// 	}

				// 	if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
				// 		UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  style='color:#fff;'>" + filename + "</a>" +
				// 			"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
				// 				+ "<div id='' class='defaultWordBreak float-left position-relative'>"
				// 					+ "<img id=''  ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + link + "' class='downCalIcon '>"
				// 				+ "</div>"
				// 			+ "</div>"
				// 	}
				// 	else {
				// 		file_ext = file_ext.toLowerCase();
				// 		UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + filename + "</a>" +
				// 			"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
				// 				+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
				// 					+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
				// 				+ "</div>"
				// 			+ "</div>"
				// 	}

				// 	UI +=	"</div>" +
				// 		"<div class='chat-income-time float-left p-0 pt-1'>" + t + "</div>" +
				// 		"</div>" +
				// 	"</div>"
				// }
				// else {
					var splitting = link.split('//')[3];
					var file_id = splitting.split('.')[0];
					var file_ext = splitting.split('.')[1];
					// console.log("filename--" + filename + "link--" + link);
					// console.log("$xml--" + $xml + "file_id--" + file_id);
					if (filename.indexOf('CH(38)') > 0) {
						filename = filename.replace("CH(38)", "&");
					}
					//    $("#conversationContactMsg_"+uid).html(filename);
					//    $('#conversationContactMsg_'+uid).css({'font-weight':' bold'});
					//var imgUrl = lighttpdPath+"/userimages/"+id+"."+imgType ;
					// var test = ChangeImage1(imgUrl,filename);
					UI = "<div id='" + msgid + "' class='media ml-3 py-1 chat' style='display: -webkit-box;'>" +
							"<img src='" + imageurl + "' class='align-self-start mt-4 mr-3 rounded-circle' title='" + senderName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
							"<div class='media-body chatId' uid='" + userid + "'>" +
								"<div style='font-size: 13px;color: gray;'>" + senderName + "</div>"

					UI += "<div onclick='' from='group' ondblclick='replyMsg()' messageType='file' msg='" + filename + "' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;'>"


					if (file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "gif" || file_ext.toLowerCase() == "svg") {
						UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  style='color:#fff;'>" + filename + "</a>" +
							"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + file_id + "' style=''>"
								+ "<div id='' class='defaultWordBreak float-left position-relative'>"
									+ "<img id=''  ondblclick=\"expandImage(" + file_id + ",'" + file_ext + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + link + "' class='downCalIcon '>"
								+ "</div>"
							+ "</div>"
					}
					else {
						file_ext = file_ext.toLowerCase();
						UI += "<a target='_blank' path='" + link + "' class='ChatText' ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" style='color:#fff;'>" + filename + "</a>" +
							"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + file_id + "'>"
								+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
									+ "<img  ondblclick=\"viewdocument(" + file_id + ",'" + file_ext + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + file_ext + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + file_id + "' class='downCalIcon '>"
								+ "</div>"
							+ "</div>"
					}

					UI +=	"</div>" +
						"<div class='chat-income-time float-left p-0 pt-1'>" + t + "</div>" +
						"</div>" +
					"</div>"
				// }

				$('#updatecallmsg').append(UI);
				playSound('glass');
				$("#updatecallmsg").animate({ scrollTop: 20000 }, 'normal');
			}
		// }
		//var test = ChangeImage1(imgUrl,body);
		//var msg = textTolink(body);
		// console.log("jid_id--"+jid_id);
		// if (type == "filesharing") {

			// var img = getImageType(link.split('.')[1]);

			// $('#updatecallmsg').append(
			// 	"<div id='"+msgid+"' class='single-videochat-income-overall row ml-3' style='outline:none' tabindex='0'>" +
			// 	"<div class='chat-income-imageframe col-xs-2' title='" + senderName + "' style='display:block;margin-left: 6%;'>" +
			// 	"<img  src='" + imageurl + "'  title='" + senderName + "' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>" +
			// 	"<div class='col-xs-10' style = 'margin-left: -2.2em;' >" +
			// 	"<div class='chat-income-message singlebubble row'>" +
			// 	"<a target='_blank' href='" + link + "' >" + filename + "</a>" +
			// 	"</div></div>" +
			// 	"<div class='chat-income-time row' style='margin-left:3vh;width:99%'>" + t + "</div></div>");
			

			// $("#updatecallmsg").animate({ scrollTop: 20000 }, 'normal');
			// playSound('glass');
		} else {
			// $('#' + callId + 'm' + ' #chat-dialog').append(
			// 	"<div class='single-videochat-income-overall row' style='outline:none' tabindex='0'>" +
			// 	"<div title='" + senderName + "' class='chat-income-imageframe col-xs-2' style='display:block;margin-left: 6%;'>" +
			// 	"<img  src='" + imageurl + "'  title='" + senderName + "' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>" +
			// 	"<div class='col-xs-10' style = 'margin-left: -2.2em;' >" +
			// 	"<div class='chat-income-message singlebubble row'>" +
			// 	body +
			// 	"</div></div>" +
			// 	"<div class='chat-income-time row' style='margin-left:3vh'>" + t + "</div></div>");


				let UI = "<div id='"+msgid+"' class='media chat ml-3'>" +
				"<img src='" + imageurl + "' class='align-self-start mt-4 mr-3 rounded-circle' title='" + senderName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
				"<div class='media-body chatId' uid='" + userid + "'>" +
				"<div class=''>" + senderName + "</div>" +
				"<div onclick='' ondblclick='replyMsg()' messageType='text' msg='" + body + "' class='float-left chat-income-message singlebubble bubble' style='max-width: 70%;word-break: break-word;'>"
				// if (replyId != undefined && replyId != 0) {
				// 	UI += "<div class='replyNameTop'>" + replyName + "</div>" +
				// 		"<div class='replyMsgBottom'>" + mainChatThread + "</div>"
				// }
				UI += "" + body + "</div>" +
					"<div class='chat-income-time float-left p-0 pt-1'>" + t + "</div>" +
					"</div>" +
					"</div>"

			$('#updatecallmsg').append(UI);
			$("#updatecallmsg").animate({ scrollTop: 20000 }, 'normal');
			playSound('glass');
		}

	}
}

function insertCallUserHistory(id, callType, callId) {
	let jsonbody = {
			"user_id" : userIdglb, /// who is the host
			"toUser" : id, /// who going to be add
			"company_id" : companyIdglb,
			"call_id" : callId
	}
	$.ajax({
		url: apiPath + "/" + myk + "/v1/insertCallUserHistory",
		type: "PUT",
		dataType: 'text',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			// console.log(result);
		}

	});
	// $.ajax({
	// 	url: path + "/ChatAuth",
	// 	type: "post",
	// 	data: { act: "insertCallUserHistory", userId: userIdglb, id: id, companyId: companyIdglb, callType: callType, callId: callId, device: 'web' },
	// 	mimeType: "textPlain",
	// 	success: function (result) {
	// 		sessionTimeOutMethod(result);
	// 		if (result != "SESSION_TIMEOUT") {
	// 		}
	// 	}
	// });
}

// $('#cme4').on('click', function () {
// 	$("#cmemessage").addClass("d-none");
// 	$("#cmecontent1").addClass("d-none");
// 	$("#cmecontact").addClass("d-none");
// 	$("#cmecontent2").addClass("d-none");
// 	$("#cmegroup").addClass("d-none");
// 	$("#cmecontent3").addClass("d-none");
// 	$("#cmecall").removeClass("d-none");
// 	$("#cmecontent4").removeClass("d-none");
// 	// $('#chat-frame2,#chat-frame,#chat-frame3').hide();
// 	// $('#chat-frame4').fadeIn();
// 	parId = "callChat";
// 	// var windowWidth1 = $(window).width();
// 	// if (windowWidth1 < 750) {
// 	// 	$("#chat-frame").hide();
// 	// 	$("#chat-frame2").hide();
// 	// 	$("#chat-frame3").hide();
// 	// 	$("#chat-frame4").show();
// 	// 	$("#chat-content").hide();
// 	// 	$("#newgroupChatHead").css({ 'padding-top': '1.5vh', 'margin-left': '4vh' });
// 	// } else {
// 	// 	$("#chat-frame").hide();
// 	// 	$("#chat-frame3").hide();
// 	// 	$("#chat-frame2").hide();
// 	// 	$("#chat-frame4").show();
// 	// 	$("#chat-content").hide();
// 	// }
// 	getCallHistory("firstClick");


// });

// function getCallHistory(type) {
// 	$("#cmemessage").addClass("d-none");
// 	$("#cmecontent1").addClass("d-none");
// 	$("#cmecontact").addClass("d-none");
// 	$("#cmecontent2").addClass("d-none");
// 	$("#cmegroup").addClass("d-none");
// 	$("#cmecontent3").addClass("d-none");
// 	$("#cmecall").removeClass("d-none");
// 	$("#cmecontent4").removeClass("d-none");

// 	var localOffsetTime = getTimeOffset(new Date());

// 	let jsonbody = {
// 		"localTZ": localOffsetTime,
// 		"user_id": userIdglb,
// 		"index": "0",
// 		"limit": "50"
// 	}
// 	$.ajax({
// 		url: apiPath + "/" + myk + "/v1/getCallHistory",
// 		type: "POST",
// 		contentType: "application/json",
// 		data: JSON.stringify(jsonbody),
// 		error: function (jqXHR, textStatus, errorThrown) {
// 			checkError(jqXHR, textStatus, errorThrown);
// 			timerControl("");
// 		},
// 		success: function (result) {
// 			console.log(result);
// 			callHistoryUi(result);
// 		}
// 	});


// }

// function callHistoryUi(json) {
// 	var UI = "";
// 	for (var i =0; i <= json.length-1 ; i++) {
// 		var userName = " ";
// 		var callType = json[i].call_type;
// 		var messageId = json[i].messageId;
// 		var uList = json[i].uList;
// 		var userList = json[i].uList;
// 		var duration = json[i].duration.substring(1, json[i].duration.length);
// 		var imgUrl;
// 		var created_t = json[i].created_time.split("#@#")[1] + " " + json[i].created_time.split("#@#")[0];
// 		var d = new Date();
// 		var imgTime = d.getTime();
// 		if (uList.includes("],")) {
// 			uList = uList.split("],");
// 			var uId = uList[0].split(",")[0].split("[")[1];
// 			var uExt = uList[0].split(",")[2];
// 			imgUrl = lighttpdpath + "/userimages/" + uId + "." + uExt + "?" + imgTime;
// 			//console.log("imgUrl.length---"+imgUrl);
// 			var calluser = uList[0].split(",")[1];
// 			for (j = 0; j < uList.length; j++) {
// 				var user = uList[j].split(",")[1];
// 				if (userName.indexOf(user) < 0) {
// 					userName = userName + " " + user + ", ";
// 				}

// 			}
// 			userName = userName.substring(0, userName.length - 2);
// 		} else {
// 			uList = uList.split("]");
// 			var uId = uList[0].split(",")[0].split("[")[1];
// 			var uExt = uList[0].split(",")[2];
// 			imgUrl = lighttpdpath + "/userimages/" + uId + "." + uExt + "?" + imgTime;
// 			//console.log("imgUrl.length---"+imgUrl);

// 			var calluser = uList[0].split(",")[1];
// 			userName = userName + " " + calluser + ", ";

// 			userName = userName.substring(0, userName.length - 2);
// 		}

// 		if (json[i].io_type == "Incoming") {

// 			if (json[i].msg_type == "calldisconnect") {
// 				src = callType == "AV" ? '/images/cme/incomingVC.png' : '/images/cme/incomingAC.png';
// 				msg = "Connected";
// 			} else if (json[i].msg_type == "callnotresponded") {
// 				src = callType == "AV" ? '/images/cme/incomingMVC.png' : '/images/cme/incomingMAC.png';
// 				msg = "Missed Call";
// 			} else if (json[i].msg_type == "callrequestcancelled") {
// 				src = callType == "AV" ? '/images/cme/incomingMVC.png' : '/images/cme/incomingMAC.png';
// 				msg = "Missed Call";
// 			} else if (json[i].msg_type == "calldeclined") {
// 				src = callType == "AV" ? '/images/cme/incomingMVC.png' : '/images/cme/incomingMAC.png';
// 				msg = "Rejected";
// 			} else {
// 				src = callType == "AV" ? '/images/cme/incomingVC.png' : '/images/cme/incomingAC.png';
// 				msg = "Connected";
// 			}

// 		} else {

// 			if (json[i].msg_type == "calldisconnect") {
// 				src = callType == "AV" ? '/images/cme/outgoingVC.png' : '/images/cme/outgoingAC.png';
// 				msg = "Connected";
// 			} else if (json[i].msg_type == "callnotresponded") {
// 				src = callType == "AV" ? '/images/cme/outgoingMVC.png' : '/images/cme/outgoingMAC.png';
// 				msg = "Didn't Connect";
// 			} else if (json[i].msg_type == "callrequestcancelled") {
// 				src = callType == "AV" ? '/images/cme/outgoingMVC.png' : '/images/cme/outgoingMAC.png';
// 				msg = "Cancelled";
// 			} else if (json[i].msg_type == "calldeclined") {
// 				src = callType == "AV" ? '/images/cme/outgoingMVC.png' : '/images/cme/outgoingMAC.png';
// 				msg = "Request Rejected";
// 			} else {
// 				src = callType == "AV" ? '/images/cme/outgoingVC.png' : '/images/cme/outgoingAC.png';
// 				msg = "Connected";
// 			}
// 		}

// 		UI+='<div id="user_16" c_id="'+json[i].cm_id+'"  class="media border p-0 cursor position-relative" style="color: black;display: -webkit-box;" onclick=""> '
// 			+'<img src="'+imgUrl+'" title="'+calluser+'" onerror="userImageOnErrorReplace(this);" class="mr-3 my-3 ml-2 rounded-circle" style="width:45px;">'
// 			+'<div class="media-body ml-1 pr-1">'
// 				+'<div class="pt-1 user_box_name defaultExceedCls pname" style="font-family: OpenSansRegular;font-size: 13px;" title="'+userName+'">'+userName+'</div>'
// 				+'<div class="font-italic d-flex pt-2" style="font-size: 11px;justify-content: space-between;">'
// 					+'<div>'
// 						+'<img style="height: 15px;width: 15px;margin-top: -7px;" src="'+src+'" >'
// 						+'<span style="padding-left:10px">'+msg+'</span>'
// 					+'</div>'
// 					+'<div><span style="">'+duration+'</span></div>'
// 				+'</div>'
// 				+'<div class="py-1 align-top font-italic">'
// 				+'<span class="defaultExceedCls">'+created_t+'</span>'
// 				+'</div>'
// 			+'</div>'
// 		+'</div>'
// 	}

// 	$("#cmecontent4").append(UI);


// }

function getCallHistory(type) {
	$("#cmemessage").addClass("d-none");
	$("#cmecontent1").addClass("d-none");
	$("#cmecontact").addClass("d-none");
	$("#cmecontent2").addClass("d-none");
	$("#cmegroup").addClass("d-none");
	$("#cmecontent3").addClass("d-none");
	$("#cmecall").removeClass("d-none");
	$("#cmecontent4").removeClass("d-none");

	var timeZone = getTimeOffset(new Date());
	$("#loadingBar").show();
	timerControl("start");

	$("#loadMsg").hide();

	if (type == "firstClick") {
		indexCall = 0;
		limitCall = 50;
		$("#callhistory").html(" ");
	} else {

		limitCall = 50;

	}
	//alert(index+"----"+limit);
	var localOffsetTime = getTimeOffset(new Date());

	let jsonbody = {
		"localTZ": localOffsetTime,
		"user_id": userIdglb,
		"index": indexCall,
		"limit": limitCall
	}
	$('#loadingBar').addClass('d-flex');
	timerControl("start");
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getCallHistory",
		type: "POST",
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			timerControl("");
		},
		success: function (result) {
			//console.log(result);
			checkSessionTimeOut(result);
			timerControl("");
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			// if (result != "SESSION_TIMEOUT") {
				// result="No latest msg"
				if (result.length == 0) {
					$("#callhistory").html(" ");
					$("#callhistory").append("<div style='text-align: center;font-size: 12px;'>No data Found</div>");
					// $("#loadingBar").hide();
					return;
				}
				var json = result;
				var UI = "";
				
				//for(i=0; i<jsonData.length; i++){
					for (var i =0; i <= json.length-1 ; i++) {
						var userName = " ";
						var callType = json[i].call_type;
						var messageId = json[i].messageId;
						// console.log("messageId--"+messageId);
						var uList = json[i].uList;
						var userList = json[i].uList;
						var duration = json[i].duration.substring(1, json[i].duration.length);
						var imgUrl;
						var created_t = json[i].created_time.split("#@#")[1] + " " + json[i].created_time.split("#@#")[0];
						var d = new Date();
						var imgTime = d.getTime();
						if (uList.includes("],")) {
							uList = uList.split("],");
							var uId = uList[0].split(",")[0].split("[")[1];
							var uExt = uList[0].split(",")[2];
							imgUrl = lighttpdpath + "/userimages/" + uId + "." + uExt + "?" + imgTime;
							//console.log("imgUrl.length---"+imgUrl);
							var calluser = uList[0].split(",")[1];
							for (j = 0; j < uList.length; j++) {
								var user = uList[j].split(",")[1];
								if (userName.indexOf(user) < 0) {
									userName = userName + " " + user + ", ";
								}
				
							}
							userName = userName.substring(0, userName.length - 2);
						} else {
							uList = uList.split("]");
							var uId = uList[0].split(",")[0].split("[")[1];
							var uExt = uList[0].split(",")[2];
							imgUrl = lighttpdpath + "/userimages/" + uId + "." + uExt + "?" + imgTime;
							//console.log("imgUrl.length---"+imgUrl);
				
							var calluser = uList[0].split(",")[1];
							userName = userName + " " + calluser + ", ";
				
							userName = userName.substring(0, userName.length - 2);
						}
				
						if (json[i].io_type == "Incoming") {
				
							if (json[i].msg_type == "calldisconnect") {
								src = callType == "AV" ? '/images/cme/incomingVC.png' : '/images/cme/incomingAC.png';
								msg = "Connected";
							} else if (json[i].msg_type == "callnotresponded") {
								src = callType == "AV" ? '/images/cme/incomingMVC.png' : '/images/cme/incomingMAC.png';
								msg = "Missed Call";
							} else if (json[i].msg_type == "callrequestcancelled") {
								src = callType == "AV" ? '/images/cme/incomingMVC.png' : '/images/cme/incomingMAC.png';
								msg = "Missed Call";
							} else if (json[i].msg_type == "calldeclined") {
								src = callType == "AV" ? '/images/cme/incomingMVC.png' : '/images/cme/incomingMAC.png';
								msg = "Rejected";
							} else {
								src = callType == "AV" ? '/images/cme/incomingVC.png' : '/images/cme/incomingAC.png';
								msg = "Connected";
							}
				
						} else {
				
							if (json[i].msg_type == "calldisconnect") {
								src = callType == "AV" ? '/images/cme/outgoingVC.png' : '/images/cme/outgoingAC.png';
								msg = "Connected";
							} else if (json[i].msg_type == "callnotresponded") {
								src = callType == "AV" ? '/images/cme/outgoingMVC.png' : '/images/cme/outgoingMAC.png';
								msg = "Didn't Connect";
							} else if (json[i].msg_type == "callrequestcancelled") {
								src = callType == "AV" ? '/images/cme/outgoingMVC.png' : '/images/cme/outgoingMAC.png';
								msg = "Cancelled";
							} else if (json[i].msg_type == "calldeclined") {
								src = callType == "AV" ? '/images/cme/outgoingMVC.png' : '/images/cme/outgoingMAC.png';
								msg = "Request Rejected";
							} else {
								src = callType == "AV" ? '/images/cme/outgoingVC.png' : '/images/cme/outgoingAC.png';
								msg = "Connected";
							}
						}
				
						UI+='<div id="call_'+json[i].cm_id+'" c_id="'+json[i].cm_id+'" uList="'+json[i].uList+'" class="media border p-0 cursor position-relative" style="color: black;display: -webkit-box;" onclick="openCallHistoryBox('+json[i].cm_id+')"> '
							+'<img data-src="'+imgUrl+'" src="/images/profile/userImage.svg" title="'+calluser+'" onerror="userImageOnErrorReplace(this);" class="lozad mr-3 my-3 ml-2 rounded-circle" style="width:45px;">'
							+'<div class="media-body ml-1 pr-1">'
								+'<div class="pt-1 user_box_name defaultExceedCls pname" style="font-family: OpenSansRegular;font-size: 13px;" title="'+userName+'">'+userName+'</div>'
								+'<div class="font-italic d-flex pt-2" style="font-size: 11px;justify-content: space-between;">'
									+'<div>'
										+'<img style="height: 15px;width: 15px;margin-top: -7px;" src="'+src+'" >'
										+'<span style="padding-left:10px">'+msg+'</span>'
									+'</div>'
									+'<div><span style="">'+duration+'</span></div>'
								+'</div>'
								+'<div class="py-1 align-top font-italic">'
								+'<span class="defaultExceedCls">'+created_t+'</span>'
								+'</div>'
							+'</div>'
						+'</div>'
					}

					// $("#callhistory").html("");
					$("#callhistory").append(UI);
					observer.observe();//---- to lazy load user images

				indexCall = indexCall + 50;
				// showongoingCalls();
				if (type == "firstClick") {
					showongoingCalls();
				} else {
					$("#loadingBar").hide();
					timerControl("");
				}


			// }
		}
	});

}

function getCallHistoryByCallId(callId) {

	//$("#loadingBar").show();
	//$("#callhistory").html(" ");
	$("#loadMsg").hide();

	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"callId": callId,
		"localTZ": localOffsetTime,
		"user_id": userIdglb
	}
	// $.ajax({
	// 	url: apiPath + "/" + myk + "/v1/getCallHistoryByCallId",
	// 	type: "POST",
	// 	dataType: 'json',
	// 	contentType: "application/json",
	// 	data: JSON.stringify(jsonbody),
	// 	error: function (jqXHR, textStatus, errorThrown) {
	// 		checkError(jqXHR, textStatus, errorThrown);
	// 		timerControl("");
	// 	},
	// 	success: function (result) {

	// 	}
	// });


	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getCallHistoryByCallId",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			// console.log(result);
			checkSessionTimeOut(result);
			// if (result != "SESSION_TIMEOUT") {
				if (result == "No latest msg") {
					$("#callhistory").html(" ");
					$("#callhistory").append("<div style='text-align: center;font-size: 12px;'>No Call History</div>");
					// $("#loadingBar").hide();
					return;
				}
				var jsonData = result;
				var Ui = "";
				var src;
				var msg;
				var d = new Date();
				var imgTime = d.getTime();
				for (i = 0; i < jsonData.length; i++) {
					var userName = " ";
					var callType = jsonData[i].callType;
					var messageId = jsonData[i].messageId;
					var uList = jsonData[i].uList;
					var userList = jsonData[i].uList;
					var duration = jsonData[i].duration.substring(1, jsonData[i].duration.length);
					var imgUrl;


					//console.log(messageId+"--uList---"+uList);
					//console.log("uList.includes('],')---"+uList.includes("],"));
					if (uList.includes("],")) {
						uList = uList.split("],");
						var uId = uList[0].split(",")[0].split("[")[1];
						var uExt = uList[0].split(",")[2];
						imgUrl = lighttpdpath + "/userimages/" + uId + "." + uExt + "?" + imgTime;
						// console.log("imgUrl.length---"+imgUrl);
						var calluser = uList[0].split(",")[1];
						for (j = 0; j < uList.length; j++) {
							var user = uList[j].split(",")[1];
							if (userName.indexOf(user) < 0) {
								userName = userName + " " + user + ", ";
							}

						}
						userName = userName.substring(0, userName.length - 2);
					} else {
						uList = uList.split("]");
						var uId = uList[0].split(",")[0].split("[")[1];
						var uExt = uList[0].split(",")[2];
						imgUrl = lighttpdpath + "/userimages/" + uId + "." + uExt + "?" + imgTime;
						//console.log("imgUrl.length---"+imgUrl);

						var calluser = uList[0].split(",")[1];
						userName = userName + " " + calluser + ", ";

						userName = userName.substring(0, userName.length - 2);
					}

					if (jsonData[i].ioType == "Incoming") {

						if (jsonData[i].messageType == "calldisconnect") {
							src = callType == "AV" ? path + '/images/temp/incomingVC.png' : path + '/images/temp/incomingAC.png';
							msg = "Connected";
						} else if (jsonData[i].messageType == "callnotresponded") {
							src = callType == "AV" ? path + '/images/temp/incomingMVC.png' : path + '/images/temp/incomingMAC.png';
							msg = "Missed Call";
						} else if (jsonData[i].messageType == "callrequestcancelled") {
							src = callType == "AV" ? path + '/images/temp/incomingMVC.png' : path + '/images/temp/incomingMAC.png';
							msg = "Missed Call";
						} else if (jsonData[i].messageType == "calldeclined") {
							src = callType == "AV" ? path + '/images/temp/incomingMVC.png' : path + '/images/temp/incomingMAC.png';
							msg = "Rejected";
						} else {
							src = callType == "AV" ? path + '/images/temp/incomingVC.png' : path + '/images/temp/incomingAC.png';
							msg = "Connected";
						}

					} else {

						if (jsonData[i].messageType == "calldisconnect") {
							src = callType == "AV" ? path + '/images/temp/outgoingVC.png' : path + '/images/temp/outgoingAC.png';
							msg = "Connected";
						} else if (jsonData[i].messageType == "callnotresponded") {
							src = callType == "AV" ? path + '/images/temp/outgoingMVC.png' : path + '/images/temp/outgoingMAC.png';
							msg = "Didn't Connect";
						} else if (jsonData[i].messageType == "callrequestcancelled") {
							src = callType == "AV" ? path + '/images/temp/outgoingMVC.png' : path + '/images/temp/outgoingMAC.png';
							msg = "Cancelled";
						} else if (jsonData[i].messageType == "calldeclined") {
							src = callType == "AV" ? path + '/images/temp/outgoingMVC.png' : path + '/images/temp/outgoingMAC.png';
							msg = "Request Rejected";
						} else {
							src = callType == "AV" ? path + '/images/temp/outgoingVC.png' : path + '/images/temp/outgoingAC.png';
							msg = "Connected";
						}
					}
					var toolTip = "";
					if (msg != "Connected") {
						toolTip = "You are not allowed to see the call history.";
						Ui += '<div uList="' + userList + '" title="' + toolTip + '"  id="call_' + messageId + '"  style="float:left;width:99%;border-bottom: 1px solid #ced2d5; padding-bottom: 1%; padding-top: 0%;margin-left: 40px;">'

					} else {
						Ui += '<div uList="' + userList + '" title="' + toolTip + '" onclick="openCallHistoryBox(' + messageId + ')" id="call_' + messageId + '"  style="float:left;width:99%;cursor:pointer;border-bottom: 1px solid #ced2d5; padding-bottom: 1%; padding-top: 0%;margin-left: 40px;">'
					}

					Ui += '<div class="row conversation-contact" style="height: 70px; min-height: 70px;    margin-top: 0px;">'
						+ '<div class="col-xs-2">'
						+ '<img src="' + imgUrl + '" onerror="userImageOnErrorReplace(this);" title="' + calluser + '" class="img-square conversationContactImage" style="height: 59px; margin-left: -10px; margin-top: 4px; width: 59px; border-radius: 50%;">'
						+ '</div>'
						+ '<div class="col-xs-8 callHistoryRow" style="height: 70px; padding-left: 30px;">'
						+ '<div class="row" style="height: 20px; min-height: 20px;">'
						+ '<div title="' + userName + '" class="col-xs-12  user_box_name conversationContactName defaultWordBreak" style="font-family: OpenSansRegular; margin: 4px 0px 0px 15px; height: 20px; font-size: 13px; padding-left: 0px;width:100%">'

						+ '' + userName + '</div>'
						+ '</div>'
						+ '<div class="row" style="font-family: OpenSansItalic;">'
						+ '<div class="col-xs-12 coversationContactTime" style="font-size: 10px; min-height: 20px; height: 20px;margin-top: 5px;">'

					Ui += '<span><img style="height: 15px;width: 15px;margin-top: -8px;" src="' + src + '" ></span>'
						+ '<span style="padding-left:10px">' + msg + '</span>'
						+ '</div>'
						+ '</div>'
						+ '<div class="row" style="font-family: OpenSansItalic;">'
						+ '<div class="col-xs-12 coversationContactTime" style="font-size: 10px; min-height: 20px; height: 20px;;">'


					Ui += '<span style="">' + changeDateTime(jsonData[i].created_time) + '</span>'
						+ '</div>'
						+ '</div>'
						+ '</div>'
						+ '<div class="col-xs-2 callHistoryDuration" style="margin-top: 28px;">' + duration + '</div>'
						/*if(jsonData[i].callType=="A"){
							Ui+='<img style="width: 35px;height: 25px;" src="'+path+'/images/temp/Phonegreen.png">'
						}else{
							Ui+='<img style="width: 35px;height: 25px;" src="'+path+'/images/temp/vedioGreen.png">'
						}
							
						Ui+='</div>'*/

						+ '</div>'
						+ '</div>'

				}
				//$("#callhistory").html("");
				$("#callhistory").prepend(Ui);
				//$("#loadingBar").hide();
			// }
		}
	});


}
function openCallHistoryBox(obj) {
	// console.log("enter------------------");
	callHistoryUii(obj);
	$("#cmecontentconv").children().remove();
	$("#cmecontentconv-mob").children().remove();
	var localOffsetTime = getTimeOffset(new Date());

	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	// var $Id = $("#" + obj + "v").find('#chat-dialog');
	let jsonbody = {
		"call_id" : obj,
    	"localTZ" : localOffsetTime
	}
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getCallHistoryBox",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
	// $.ajax({
	// 	url: path + "/ChatAuth",
	// 	type: "post",
	// 	data: { act: "getCallHistoryBox", userId: userIdglb, callId: obj, indexRg: 0, noOfMsgReq: 20, localTZone: timeZone, device: 'web' },
	// 	mimeType: "textPlain",
	// 	success: function (result) {
			checkSessionTimeOut(result);
			// if (result != "SESSION_TIMEOUT") {
				if (result == 'No latest msg') {
					$Id.append("<div style='text-align: center'>No History</div>");
				} else {
					jsonData = result;

					for (i = jsonData.length - 1; i >=0 ; i--) {
						var UI = "";
						var t = "";
						var d = new Date();
						var time = jsonData[i].created_time;
						// time = time.replace("CHR(26)", ":");
						var T = time.split("#@#")[1];
						var date = time.split("#@#")[0];
						//  var format = time.split(" ")[2];  
						var messageType = jsonData[i].msg_type;
						var fileExt = jsonData[i].file_ext;
						var ioType = jsonData[i].io_type;
						var duration = jsonData[i].duration;
						var callType = jsonData[i].call_type;
						var m = d.getMonth();
						m = months[m];
						var date1 = d.getDate();
						date1 = (date1 > 10) ? (date1) : ('0' + date1);

						var D = date1 + "-" + m + "-" + d.getFullYear();

						if (date == D) {
							t = "Today  " + T;
						} else {
							t = T + " , " + date;
						}



						//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"').replace("CHR(26)",":");

						var userName = jsonData[i].userName;
						var message = jsonData[i].msg;
						message = replaceSpecialCharacter(message);
						var body = textTolink(message);
						var type = jsonData[i].type;
						var messageId = jsonData[i].cm_id;
						var message_status = jsonData[i].message_status;
						var userImgType = jsonData[i].user_image_type;
						var imgTime = d.getTime();
						//console.log("message_status---"+message_status);
						if (messageType == "calltext") {
							if (jsonData[i].msg_fr_uid == userIdglb) {
								UI = "<div id='" + messageId + "' class='media chat mr-3 d-block'>" +
								"<div class='media-body chatId' uid='" + userIdglb + "'>" +
									"<div onclick='' ondblclick='replyMsg()' messageType='text' msg='" + body + "' class='mt-2 px-3 py-2 chat-outgoing-message singlebubble2 bubble2 float-right' style='margin-left: 30%;background-color: #007b97;word-break: break-word;'>"
								// if (chatType == "reply") {
								// 	UI += "<div style='border-left: 4px solid " + color + "' class='replyNameTop'>" + replyName + "</div>" +
								// 		"<div style='border-left: 4px solid " + color + "' class='replyMsgBottom'>" + mainChatThread + "</div>"
								// }
								UI += "" + body + "</div>" +
										"<div class='float-right mt-1' style='text-align: right;width: 100%;font-size: 11px;color: gray;'>" + t + "</div>" +
									"</div>" +
									// "<img src='" + imgName + "' class='align-self-start ml-3 mt-2 rounded-circle' title='" + userFullname + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px;'>" +
								"</div>"


								// UI = "<div id=" + messageId + " class='chat-outgoing-overall row'>"
								// 	+ "<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>"
								// 	+ "<img src='" + lighttpdpath + "/userimages/" + userIdglb + "." + userImgType + "?" + imgTime + "'  title=\"" + userName + "\" onerror='userImageOnErrorReplace(this);' style='float:right;width: 40px; height: 38px; border-radius: 50%; margin-left:-2.3vh;margin-top: 1vh;margin-bottom:2vh;'/></div>"
								// 	+ "<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right'>"
								// 	+ "<div class='chat-outgoing-message singlebubble2 row'>";

								// if (messageType == "calltext") {
								// 	UI += body;
								// } else if (messageType == "media") {
								// 	//console.log("media------------");
								// 	UI += "<span style='color:#fff;' src='" + jsonData[i].filePath + "' onclick='cmeopenVideo(this)' callType ='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
								// } else if (messageType == "highlight") {
								// 	UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ", 1);'>" + message + "</a>";
								// } else if (messageType == "transcript") {
								// 	//console.log("inside chat type");
								// 	UI += "<a  style='color:#fff;' href='#'  onclick='readTextFile(\"" + jsonData[i].file_id + "\",\"" + fileExt + "\",\"" + jsonData[i].filePath + "\",\"" + jsonData[i].cm_id + "\",\"" + message + "\",  \"1\");''>" + message + "</a>";
								// 	UI += "<textarea id='transcript_" + jsonData[i].messageId + "' style='display:none;'></textarea>";
								// 	UI += "<div class='UpdateCancelBtns' style='display:inline-flex;float:right;'>";
								// 	UI += "<img id='update_" + jsonData[i].messageId + "' title='update' style='display:none;' src='" + path + "/images/temp/hupdate.png'  onclick='updateTheTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ",&quot;" + message + "&quot;);'/>";
								// 	UI += "<img id='cancel_" + jsonData[i].messageId + "' title='Cancel' style='display:none;padding-left: 5px;' src='" + path + "/images/temp/hcancel.png'  onclick='cancelTheEditFile(" + jsonData[i].messageId + ");'/>";
								// 	UI += "</div>";
								// } else {
								// 	UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
								// }
								// UI += "</div>"
								// 	+ "<div class='chat-outgoing-time row'><span>" + t + "</span>";
								// if (message_status == "sent") {
								// 	UI += "<span class='chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div></div>";
								// } else if (message_status == "seen") {
								// 	UI += "<span class='chat-outgoing-status chat-outgoing-status-seen'><i> Read</i></span></div></div></div>";
								// } else if (message_status == "delivered") {
								// 	UI += "<span class='chat-outgoing-status chat-outgoing-status-deliver'><i> Delivered</i></span></div></div></div>";
								// }
								// $("#cmecontentconv-mob").append(UI);
								// $("#cmecontentconv").append(UI);

							}
							else {
								let UI = "<div id='" + messageId + "' class='media chat float-left ml-3 mt-2 w-100'>" +
										"<img src='" + lighttpdpath + "/userimages/" + jsonData[i].msg_fr_uid + "." + userImgType + "?" + imgTime + "' class='align-self-start mt-4 mr-3 rounded-circle' title='" + userName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
										"<div class='media-body chatId'>" +
										"<div class='' style='font-size: 13px;color: gray;'>" + userName + "</div>" +
										"<div onclick='' ondblclick='replyMsg()' messageType='text' msg='" + body + "' class='float-left chat-income-message singlebubble bubble' style='max-width: 70%;word-break: break-word;'>"
									// if (replyId != undefined && replyId != 0) {
									// 	UI += "<div class='replyNameTop'>" + replyName + "</div>" +
									// 		"<div class='replyMsgBottom'>" + mainChatThread + "</div>"
									// }
									UI += "" + body + "</div>" +
										"<div class='chat-income-time float-left p-0 pt-1'>" + t + "</div>" +
										"</div>" +
										"</div>"

								// UI = "<div id=" + messageId + " class='single-chat-income-overall row' style='outline:none' tabindex='0'>"
								// //if(messageType == "calltext"){ 
								// UI += "<div class='chat-income-imageframe col-xs-2 videochatimage' style='display:block;margin-left:22px' >"
								// 	+ "<img  src='" + lighttpdpath + "/userimages/" + jsonData[i].sender + "." + userImgType + "?" + imgTime + "'  title=\"" + userName + "\" onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>";
								//  }else{
								/* UI+="<div class='chat-income-imageframe col-xs-2' style='display:none' >"
											+"<img  src=''  onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 20px;float: right;margin-top:1vh'/></div>" ;
										 }*/


								// UI += "<div class='col-xs-10' style = 'margin-left: -2.2em;' >"
								// 	+ "<div class='chat-income-message singlebubble row'>";
								// if (messageType == "calltext") {
								// 	UI += body;
								// } else if (messageType == "media") {
								// 	//console.log("media+-----------");
								// 	UI += "<span style='color:#fff;' src='" + jsonData[i].filePath + "' onclick='cmeopenVideo(this)' callType ='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
								// } else if (messageType == "highlight") {
								// 	UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + messageId + ");'>" + message + "</a>";
								// } else if (messageType == "transcript") {
								// 	//UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"</a>" ;
								// 	UI += "<a  style='color:#fff;' href='#'  onclick='readTextFile(\"" + jsonData[i].file_id + "\",\"" + fileExt + "\",\"" + jsonData[i].filePath + "\",\"" + jsonData[i].cm_id + "\",\"" + message + "\", \"1\");'>" + message + "</a>";
								// 	UI += "<textarea id='transcript_" + messageId + "' style='display:none;'></textarea>";
								// 	UI += "<div class='UpdateCancelBtns' style='display:inline-flex;float:right;'>";
								// 	UI += "<img id='update_" + messageId + "' title='update' style='display:none;' src='" + path + "/images/temp/hupdate.png'  onclick='updateTheTextFile(&quot;" + jsonData[i].filePath + "&quot;," + messageId + ",&quot;" + message + "&quot;);'/>";
								// 	UI += "<img id='cancel_" + messageId + "' title='Cancel' style='display:none;padding-left: 5px;' src='" + path + "/images/temp/hcancel.png'  onclick='cancelTheEditFile(" + messageId + ");'/>";
								// 	UI += "</div>";
								// } else {
								// 	UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
								// }
								// UI += "</div>"
								// 	+ "<div class='chat-income-time row'>" + t + "</div></div></div>";
								$("#cmecontentconv-mob").append(UI);
								$("#cmecontentconv").append(UI);
							}
							$("#cmecontentconv-mob").append(UI);
								$("#cmecontentconv").append(UI);
							// $Id.append(UI);
						} else {


							// UI = "<div align='center' class='px-5 mt-3' style='float:left;position:relative;width:100%;margin:0px'>"
							// 	+ "<div class='callmsg p-2 w-100' style='max-width: 350px;font-size:12px; background-color:#709DAF;border-radius: 9px;position:relative;'>";
							if (messageType == "media") {
								if (jsonData[i].msg_fr_uid == userIdglb) {
									UI += "<div id='" + messageId + "' class='media py-1 chat mr-3 d-block w-100 ' style='position:relative;'>" +
												"<div class='media-body mr-3 chatId' uid='" + userIdglb + "'>"
								UI += "<div  from='group' messageType='file' msg='" + message + "' class='mt-2 defaultWordBreak chat-income-message singlebubble bubble float-right' style='margin-left: 30%;background-color: #007b97;'>"

								UI += "<a target='_blank' path='" + jsonData[i].filePath + "'  style='color:#fff;'>" + message + "</a>" 

								UI+= "<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_"+jsonData[i].file_id+"'>"
											+"<div id='videoOpenUi' onclick=\"openVideo("+jsonData[i].file_id+",'"+fileExt+"','1', '" + jsonData[i].filePath + "');\" class='defaultWordBreak float-left position-relative border vDoc_'>"
												+"<video id='uploadedVideo' class='float-left' style='cursor: pointer; height:80px; width:130px;background-color: #000;'  id='view_"+jsonData[i].file_id+"'><source src='"+jsonData[i].filePath+"'></video>"
												+"<img class='videobutton' src='images/conversation/videoplay.svg' style='cursor:pointer;'>"
												//+"<img id='uploadedVideo' onclick=\"openVideo("+jsonData[i].file_id+",'"+ext+"',"+prjid+");\" class='float-left mt-1 downCalIcon' style='cursor: pointer; height:80px; width:100px;' src='"+lighttpdpath+"//projectDocuments//"+docId+"."+ext+"' id='view_"+docId+"'>"
												
											+"</div>"
											+"</div>"	
											+"</div>"
									// "<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + jsonData[i].file_id + "'>"
									// 	+ "<div id='" + jsonData[i].file_id + "'  onclick='cmeopenVideo(this)' ondblclick=\"Download(this)\" src='" + jsonData[i].filePath + "' callType ='" + callType + "' extenstion = '" + fileExt + "' class='defaultWordBreak w-25 float-right position-relative'>"
									// 		+ "<img  class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + fileExt + ".svg' onerror='imageOnFileNotErrorReplace(this)'  class='downCalIcon '>"
									// 	+ "</div>"
									// + "</div>"
									// + "</div>"

									UI += "<div class='float-right mt-1' style='text-align: right;width: 100%;font-size: 11px;color: gray;'>" + t + "</div>" +
										"</div>" +
										// "<img src='" + imgName + "' class='align-self-start ml-3 mt-2 rounded-circle' title='" + sender_name + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px;'>" +
									"</div>"

									
								}
								else{
									UI = "<div id='" + messageId + "' class='media py-1 chat ml-3 d-block w-100 float-left' style='position:relative;'>" +
											"<img src='" + lighttpdpath + "/userimages/" + jsonData[i].msg_fr_uid + "." + userImgType + "?" + imgTime + "' class='float-left align-self-start mt-4 mr-3 rounded-circle' title='" + userName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
											"<div class='media-body chatId'>" +
												"<div style='font-size: 13px;color: gray;width: 70%;max-width: 300px;'>" + userName + "</div>"
												
									UI += "<div from='group' messageType='file' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;'>"

									UI += "<a target='_blank' path='" + jsonData[i].filePath + "' class='ChatText' style='color:#fff;'>" + message + "</a>" 

									UI+= "<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_"+jsonData[i].file_id+"'>"
											+"<div id='videoOpenUi' onclick=\"openVideo("+jsonData[i].file_id+",'"+fileExt+"','1', '" + jsonData[i].filePath + "');\" class='defaultWordBreak float-left position-relative border vDoc_'>"
												+"<video id='uploadedVideo' class='float-left' style='cursor: pointer; height:80px; width:130px;background-color: #000;'  id='view_"+jsonData[i].file_id+"'><source src='"+jsonData[i].filePath+"'></video>"
												+"<img class='videobutton' src='images/conversation/videoplay.svg' style='cursor:pointer;'>"
												//+"<img id='uploadedVideo' onclick=\"openVideo("+jsonData[i].file_id+",'"+ext+"',"+prjid+");\" class='float-left mt-1 downCalIcon' style='cursor: pointer; height:80px; width:100px;' src='"+lighttpdpath+"//projectDocuments//"+docId+"."+ext+"' id='view_"+docId+"'>"
												
											+"</div>"
											+"</div>"	
											+"</div>"	

											UI += "<div class='chat-income-time float-left p-0 pt-1' style='margin-left: 56px;'>" + t + "</div>" +
										"</div>" +
									"</div>"


									// UI += "<div id='" + messageId + "' class='media py-1 chat ml-3 d-block w-100 float-left' style='position:relative;'>" +
									// 		"<img src='" + lighttpdpath + "/userimages/" + jsonData[i].msg_fr_uid + "." + userImgType + "?" + imgTime + "' class='float-left align-self-start mt-4 mr-3 rounded-circle' title='" + userName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
									// 		"<div class='media-body chatId'>" +
									// 			"<div style='font-size: 13px;color: gray;width: 70%;max-width: 300px;'>" + userName + "</div>"
												
									// UI += "<div from='group' messageType='file' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;'>"

									// UI += "<a target='_blank' path='" + jsonData[i].filePath + "' class='ChatText' style='color:#fff;'>" + message + "</a>" +
									// 		"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + jsonData[i].file_id + "' style=''>"
									// 			+ "<div id='" + jsonData[i].file_id + "'  onclick='cmeopenVideo(this)' ondblclick=\"Download(this)\" src='" + jsonData[i].filePath + "' callType ='" + callType + "' extenstion = '" + fileExt + "' class='defaultWordBreak float-left position-relative'>"
									// 					+ "<img  class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + fileExt + ".svg' onerror='imageOnFileNotErrorReplace(this)' class='downCalIcon '>"
									// // + "<img id=''  ondblclick=\"expandImage(" + jsonData[i].file_id + ",'" + fileExt + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + jsonData[i].filePath + "' class='downCalIcon '>"
									// 			+ "</div>"
									// 		+ "</div>"
									// 		+ "</div>"

									// 	UI += "<div class='chat-income-time float-left p-0 pt-1' style='margin-left: 56px;'>" + t + "</div>" +
									// 	"</div>" +
									// "</div>"

							
								}


								// UI += "<span style='color:#fff;cursor:pointer' src='" + jsonData[i].filePath + "' onclick='cmeopenVideo(this)' callType ='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";

								// UI += "<span id='"+ jsonData[i].file_id +"' style='color:#fff;cursor:pointer' src='" + jsonData[i].filePath + "' onclick='Download(this)' callType ='" + callType + "' extenstion = '" + fileExt + "' style='cursor:pointer'><img style='width:16px;margin-left:4px' src='" + path + "/images/temp/download.png'></span>";

							} else if (messageType == "highlight") {
								UI = "<div align='center' class='px-5 mt-3' style='float:left;position:relative;width:100%;margin:0px'>"
								+ "<div class='callmsg p-2 w-100' style='max-width: 350px;font-size:12px; background-color:#709DAF;border-radius: 9px;position:relative;'>";
								UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + messageId + ", 1);'>" + message + "</a>";
								UI += "</div><div style='font-size: 11px'>" + t + "</div>";
									+"</div>";
							} else if (messageType == "transcript") {
								UI = "<div align='center' class='px-5 mt-3' style='float:left;position:relative;width:100%;margin:0px'>"
								+ "<div class='callmsg p-2 w-100' style='max-width: 350px;font-size:12px; background-color:#709DAF;border-radius: 9px;position:relative;'>";
								//console.log("inside chat type");
								UI += "<a  style='color:#fff;' href='#'  onclick='readTextFile(\"" + jsonData[i].file_id + "\",\"" + fileExt + "\",\"" + jsonData[i].filePath + "\",\"" + jsonData[i].cm_id + "\",\"" + message + "\", \"1\");'>" + message + "</a>";
								UI += "</div><div style='font-size: 11px'>" + t + "</div>";
									+"</div>";
								// UI += "<textarea id='transcript_" + messageId + "' style='display:none;'></textarea>";
								// UI += "<div class='UpdateCancelBtns' style='display:inline-flex;float:right;'>";
								// UI += "<img id='update_" + messageId + "' title='update' style='display:none;' src='" + path + "/images/temp/hupdate.png'  onclick='updateTheTextFile(&quot;" + jsonData[i].filePath + "&quot;," + messageId + ",&quot;" + message + "&quot;);'/>";
								// UI += "<img id='cancel_" + messageId + "' title='Cancel' style='display:none;padding-left: 5px;' src='" + path + "/images/temp/hcancel.png'  onclick='cancelTheEditFile(" + messageId + ");'/>";
								// UI += "</div>";
							} else {
								if (jsonData[i].msg_fr_uid == userIdglb) {
									UI += "<div id='" + messageId + "' class='media py-1 chat mr-3 d-block w-100' style='position:relative;'>" +
									"<div class='media-body mt-2 mr-3 chatId' uid='" + userIdglb + "'>"
									UI += "<div  from='group' messageType='file' msg='" + message + "' class='mt-2 defaultWordBreak chat-income-message singlebubble bubble float-right' style='margin-left: 30%;background-color: #007b97;'>"

									if (fileExt.toLowerCase() == "png" || fileExt.toLowerCase() == "jpg" || fileExt.toLowerCase() == "jpeg" || fileExt.toLowerCase() == "gif" || fileExt.toLowerCase() == "svg") {

										UI += "<a target='_blank' path='" + jsonData[i].filePath + "' class='ChatText' ondblclick=\"expandImage(" + jsonData[i].file_id + ",'" + fileExt + "','Document');event.stopPropagation();\" style='color:#fff;'>" + message + "</a>" +
												"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + jsonData[i].file_id + "' style=''>"
													+ "<div id='' class='defaultWordBreak float-right position-relative'>"
														+ "<img id='' ondblclick=\"expandImage(" + jsonData[i].file_id + ",'" + fileExt + "','Document');event.stopPropagation();\"  class='float-left rounded mt-1 mr-0' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + jsonData[i].filePath + "' class='downCalIcon '>"
													+ "</div>"
												+ "</div>"
												+ "</div>"
									}
									else {
										fileExt = fileExt.toLowerCase();
										UI += "<a target='_blank' path='" + jsonData[i].filePath + "' class='ChatText' ondblclick=\"viewdocument(" + jsonData[i].file_id + ",'" + fileExt + "','Document');\" style='color:#fff;'>" +message+ "</a>" +
											"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + jsonData[i].file_id + "'>"
												+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-right position-relative'>"
													+ "<img  ondblclick=\"viewdocument(" + jsonData[i].file_id + ",'" + fileExt + "','Document');\" class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + fileExt + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + jsonData[i].file_id + "' class='downCalIcon '>"
												+ "</div>"
											+ "</div>"
											+ "</div>"
									}
										// "<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + jsonData[i].file_id + "'>"
										// 	+ "<div id='" + jsonData[i].file_id + "' ondblclick=\"expandImage(" + jsonData[i].file_id + ",'" + fileExt + "','Document');\" callType ='" + callType + "' extenstion = '" + fileExt + "' class='defaultWordBreak w-25 float-right position-relative'>"
										// 		+ "<img  class='float-right mt-1' style='cursor: pointer;width:42px;margin-right: -36px;' src='images/document/" + fileExt + ".svg' onerror='imageOnFileNotErrorReplace(this)'  class='downCalIcon '>"
										// 	+ "</div>"
										// + "</div>"
										// + "</div>"

										UI += "<div class='float-right mt-1' style='text-align: right;width: 100%;font-size: 11px;color: gray;'>" + t + "</div>" +
											"</div>" +
											// "<img src='" + imgName + "' class='align-self-start ml-3 mt-2 rounded-circle' title='" + sender_name + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px;'>" +
										"</div>"
								}
								else{
									UI = "<div id='" + messageId + "' class='media py-1 chat d-block ml-3 w-100 float-left' style='position:relative;'>" +
											"<img src='" + lighttpdpath + "/userimages/" + jsonData[i].msg_fr_uid + "." + userImgType + "?" + imgTime + "' class='align-self-start mt-4 mr-3 rounded-circle float-left' title='" + userName + "' onerror='userImageOnErrorReplace(this)'; style='width: 40px'>" +
											"<div class='media-body chatId' uid='" + jsonData[i].msg_fr_uid + "'>" +
												"<div style='font-size: 13px;color: gray;width: 75%;'>" + userName + "</div>"
									UI += "<div onclick='' ondblclick='' messageType='file' msg='" + message + "' class=' defaultWordBreak float-left chat-income-message singlebubble bubble' style='max-width: 70%;'>"

									if (fileExt.toLowerCase() == "png" || fileExt.toLowerCase() == "jpg" || fileExt.toLowerCase() == "jpeg" || fileExt.toLowerCase() == "gif" || fileExt.toLowerCase() == "svg") {

										UI += "<a target='_blank' path='" + jsonData[i].filePath + "' ondblclick=\"expandImage(" + jsonData[i].file_id + ",'" + fileExt + "','Document');event.stopPropagation();\" class='ChatText' style='color:#fff;'>" + message + "</a>" +
										"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 d-flex justify-content-start' id='doc_" + jsonData[i].file_id + "' style=''>"
											+ "<div id='' class='defaultWordBreak float-left position-relative'>"
												+ "<img id=''  ondblclick=\"expandImage(" + jsonData[i].file_id + ",'" + fileExt + "','Document');event.stopPropagation();\"  class='float-left rounded ml-0 mt-1' style='cursor: pointer;height:80px;width:100px;object-fit: cover;' src='" + jsonData[i].filePath + "' class='downCalIcon '>"
											+ "</div>"
										+ "</div>"
									}
									else {
										fileExt = fileExt.toLowerCase();
										UI += "<a target='_blank' path='" + jsonData[i].filePath + "' class='ChatText' ondblclick=\"viewdocument(" + jsonData[i].file_id + ",'" + fileExt + "','Document');\" style='color:#fff;'>" + message + "</a>" +
											"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + jsonData[i].file_id + "'>"
												+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
													+ "<img  ondblclick=\"viewdocument(" + jsonData[i].file_id + ",'" + fileExt + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + fileExt + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + jsonData[i].file_id + "' class='downCalIcon '>"
												+ "</div>"
											+ "</div>"
									}
									// UI += "<a target='_blank' path='" + jsonData[i].filePath + "' onclick='' class='ChatText' ondblclick=\"expandImage(" + jsonData[i].file_id + ",'" + fileExt + "','Document');\" callType ='" + callType + "' extenstion = '" + fileExt + "' style='color:#fff;'>" + message + "</a>" +
									// 			"<div class='docListViewBorderCls mr-0 ml-0 w-50 border-bottom-0 ' id='doc_" + jsonData[i].file_id + "'>"
									// 				+ "<div id='viewordownloadUi' class='defaultWordBreak w-25 float-left position-relative'>"
									// 					+ "<img  ondblclick=\"expandImage(" + jsonData[i].file_id + ",'" + fileExt + "','Document');\" class='float-left mt-1' style='cursor: pointer;width:42px;' src='images/document/" + fileExt + ".svg' onerror='imageOnFileNotErrorReplace(this)' id='view_" + jsonData[i].file_id + "' class='downCalIcon '>"
									// 				+ "</div>"
									// 			+ "</div>"

										UI +=	"</div>" +
										"<div class='chat-income-time float-left p-0 pt-1' style='margin-left: 54px;'>" + t + "</div>" +
										"</div>" +
									"</div>"
								}
								// UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
							}
							// UI += "</div><div style='font-size: 11px'>" + t + "</div>";
							// +"</div>";

							// $Id.append(UI);
							$("#cmecontentconv-mob").append(UI);
							$("#cmecontentconv").append(UI);
							var windowHeight = $(window).height();
							if (windowHeight < 600) {
								$(".callmsg").css({ 'width': '100%' });
							} else {
								$(".callmsg").css({ 'width': '50%' });
							}
						}
						
						if ($("#cmecontent").hasClass("cmecontainer-mob")) {
							$('#cmegroup').addClass('d-none');
							$('#cmecontent3').addClass('d-none');
							$("#cmemessage").addClass("d-none");
							$("#cmecontent1").addClass("d-none");
							$("#cmecontact").addClass("d-none");
							$("#cmecontent2").addClass("d-none");
							$('#cmecall').addClass('d-none');
							$('#cmecontent4').addClass('d-none');

							$("#cmecontactconv-mob").removeClass("d-none");
							$("#cmecontentconv-mob").removeClass("d-none");
							$("#cmecontactconv").addClass("d-none");
							$("#cmecontentconv").addClass("d-none");
				
							// $("#cmecontentconv-mob").append(UI);
						}
						else{
							$('#cmegroup').addClass('d-none');
							$('#cmecontent3').addClass('d-none');
							$("#cmemessage").addClass("d-none");
							$("#cmecontent1").addClass("d-none");
							$("#cmecontact").addClass("d-none");
							$("#cmecontent2").addClass("d-none");
							// $('#cmecall').addClass('d-none');
							// $('#cmecontent4').addClass('d-none');

							$("#cmecontactconv-mob").addClass("d-none");
							$("#cmecontentconv-mob").addClass("d-none");
							$("#cmecontactconv").removeClass("d-none");
							$("#cmecontentconv").removeClass("d-none");

							// $("#cmecontentconv").append(UI);
						}
					}
				}
				//console.log("result---"+result);
			// }
		}
	});
	//resizeChatWindow();

	// chatWindow();

}

function callHistoryUii(id) {
	$('#cmecontactconv-mob').html('');
	$('#cmecontactconv').html('');
	var uList = $("#call_" + id).attr('uList');
	var UI = "";
	var d = new Date();
	imgTime = d.getTime();
	if (uList.includes("],")) {
		var userdata = [];
		uList = uList.split("],");
		for (i = 0; i < uList.length; i++) {
			var userList = uList[i];
			if (userList.includes("]")) {

			} else {
				userList = userList + "]";
			}
			var data = userList.split(",");

			var user = data[1];
			var userid = data[0].substring(1, data[0].length);
			if (!userdata.includes(userid)) {
				userdata.push(userid);
				var imgType = data[2].substring(0, data[2].length - 1);
				var source = lighttpdpath + "/userimages/" + userid + "." + imgType + "?" + imgTime;
				UI += "<img  title='" + user + "' style=\"width: 40px;\" class=\"img-circle ml-2 rounded-circle\"  src='" + source + "' onerror=\"userImageOnErrorReplace(this);\">"
			}
		}

	} else {


		var userList = uList;
		if (userList.includes("]")) {

		} else {
			userList = userList + "]";
		}
		var data = userList.split(",");

		var user = data[1];
		var userid = data[0].substring(1, data[0].length);
		var imgType = data[2].substring(0, data[2].length - 1);
		var source = lighttpdpath + "/userimages/" + userIdglb + "." + imgType + "?" + imgTime;
		UI += "<img  title='" + user + "'  class=\"rounded-circle ml-2 \" style=\"width: 40px;\"  src='" + source + "' onerror=\"userImageOnErrorReplace(this);\">"
	}
	


	var ui = "<div  id=\"" + id + "v\" class=\"d-flex align-items-center\" style='border-bottom: 1px solid #e3e3e3;height: 49px;'>"

		+ "<div class='' style='width:100%;border-radius:0px'>"
		+ "" + UI + ""
		+"</div>"
		+'<div id="" class="chatMoreOptionsDivFullScreen align-items-center justify-content-between mr-2" style="display: none;">'
			+ '<img id="" class="cmeMinMax cmeExpandCollapse mx-2" src="images/cme/cme_min.svg" title="" onclick="cmeMinMax(this);event.stopPropagation();" style="display: none;width: 20px;height:20px;">'
			+ '<img id="" class="cmeFullCompact cmeExpandCollapse mx-2 mr-2" src="images/cme/fullscreen_min.svg" title="Collapse" onclick="cmeExpandCollapse(this);event.stopPropagation();" style="width: 20px;height:20px;">'
		+ '</div>'
		+"<img id='' onclick='backToCall()' src='images/cme/leftarrow.svg' title='Calls' class='backmsg ml-auto mr-3 cursor' style='width: 22px;'> ";
		+"</div>"


		if ($("#cmecontent").hasClass("cmecontainer-mob")) {
			$('#cmecontactconv-mob').html(ui);
		}
		else{
			$('#cmecontactconv').html(ui);
			$('.backmsg').hide();
			$('.chatMoreOptionsDivFullScreen').addClass('d-flex');
		}


	// 	+ "<img class='backChat' onclick='goBack()'  style='margin-bottom: 1vh; cursor: pointer; float: right; margin-top: 3.3vh; height: 18px;    min-height: 18px; min-width: 16px; width: 16px;margin-right: 10px;' src='" + path + "/images/back1.png'\>"
	// 	+ '<div style="float: right; position: relative;padding-top: 2.4vh;display:none;">'
	// 	+ '<img id="chatMoreOptions" src="' + path + '/images/more.png" style="cursor: pointer;float: right;margin-right: 10px;" onclick="chatMoreOptions()">'
	// 	+ '<div id="chatMoreOptionsDiv" class=" flex-column align-items-center" align="center" style="display: none;position: absolute;padding: 8px;z-index: 1;background-color: #fff;right: 26px;border: 1px solid #bfbfbf;">'
	// 	+ "<img onclick='startCall(" + id + ",\"true\",\"video\")' class='video' title='Video Call'  style='cursor:pointer;margin-top: 5px;width: 35px;height: 25px;' src='" + path + "/images/temp/vedioGreen.png'\>"
	// 	+ "<img onclick='startCall(" + id + ",\"true\",\"audio\")' class='video' title='Audio Call' style='cursor:pointer;margin-top: 10px;width: 40px;height: 30px;' src='" + path + "/images/temp/Phonegreen.png'\>"
	// 	+ "<img onclick='showChatFromRecomdView(\"" + id + "\")' class='video' title='Messages' style='cursor:pointer;margin-top: 3px;width: 40px;height: 40px;' src='" + path + "/images/temp/chat2.png'\>"
	// 	+ "<img onclick='showRecommendationsInChat(\"" + id + "m\")' class='video' title='Recommendations' style='cursor:pointer;margin-top: 5px;width: 30px;height: 30px;' src='" + path + "/images/temp/recomd.png'\>"
	// 	+ "<img onclick='startCall(" + id + ",\"true\",\"screenshare\")' class='video' title='Screen Share'  style='display:none;cursor:pointer;width: 38px;height: 26px;margin-top: 12px;margin-bottom: 4px;' src='" + path + "/images/temp/screenshare.png'\>"
	// 	+ '</div>'
	// 	+ '</div>'
	// 	+ "</div>"

	// 	+ "<div id='chat-dialog' style='height:90vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;border-radius:0px;' id='conv' class='well'>"
	// 	+ "</div>"
	// 	+ "<div id='chat-recomd-dialog' style='display:none;height:90vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;border-radius:0px;' id='conv' class='well'>"
	// 	+ "</div>"

	// 	+ "<div id='video-dialog' style='display:none;height:82vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;border-radius:0px;' id='conv' class='well'>"
	// 	+ "</div>"

	// 	+ "<div id='text-box' style=' height: 8vh; border-radius:0;display:none' class='well row form-group'>"
	// 	+ "<div class='col-xs-10 col-md-11' style='margin:0;height:8vh;padding:0;border-right:1px solid #c1c5c8'>"
	// 	+ "<textarea id='text' type='text'  onkeypress='sending(event,this)' style='height:6.5vh;min-height:6.5vh;padding:2px;border-radius:0%;margin-top:4px;margin-left:4px;width:99%'  placeholder='Type message here...' class='form-control'></textarea>"
	// 	+ "</div>"
	// 	+ "<div class='col-xs-2 col-md-1 chatpost' style='margin-left:0px;background-color:white;height:10vh'>"
	// 	//+"<img class='chatpostI' src='"+path+"/images/workspace/post.png' onclick='sending(event,this)' style='height:5vh;min-height:5vh;width:4vh;margin-left:-0.7vh,margin-top:1.5vh'>"
	// 	+ "<img class='chatpostI' src='" + path + "/images/workspace/post.png' onclick='sending(event,this)' style='margin-top:10px,vertical-algin:middle'>"
	// 	+ "</div>"
	// 	+ "</div>"

	// 	+ "</div>";
	// $('#chat-content').html('').html(ui);

}


/*--------------Record Functionality ------------------*/

var recorderAv = null;
var streamArray = [];
function recordVideo(callAction) {
	//console.log("callAction---:"+callAction);
	if (callType == "AV") {
		var obj = $('#vrecordImg');
		if ($(obj).attr('src').indexOf('record1.svg') != -1) {
			recordStatus = "on";
			/*
			var jsonText = '{"calltype":"'+callType+'","type":"recordstatus","status":"on","screenstatus":"'+shareScreen+'","callid":"'+callId+'"}';
			notify = $msg({to: jid,"type" : "normal"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
			serverConnection.send(notify);
			*/
			$("#previewDiv").hide();
			$(obj).attr('src', path + '/images/cme/' + $(obj).attr('toglsrc'));
			$('#recordStatusDiv').find('span').text("REC");
			$('#recordStatusDiv').show();

			streamArray = [];
			streamArray.push(localVideo.captureStream());

			$('div#local-video1').find('video').each(function () {
				streamArray.push(this.captureStream());
			});

			recorderAv = new MultiStreamRecorder(streamArray);
			recorderAv.record();
			$('body').children('canvas').remove();

			/*
			 * 
			 * setTimeout(function(){ 
					addStreamToRecord(10);
				}, 5000);
				setTimeout(function(){ 
					addStreamToRecord(10);
				}, 10000);
				
			*
			*/

			//recstream = localVideo.captureStream();
			//drawToCanvas();


		} else {
			recordStatus = "off";
			$(obj).attr('src', path + '/images/cme/' + $(obj).attr('defsrc'));
			/*
			var jsonText = '{"calltype":"'+callType+'","type":"recordstatus","status":"off","screenstatus":"'+shareScreen+'","callid":"'+callId+'"}';
	notify = $msg({to: jid,"type" : "normal"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
	serverConnection.send(notify);
	*/
			$('#recordStatusDiv').hide();


			recorderAv.stop(function (blob) {

				openPreviewVideo(blob, callType);

				var blob = recorderAv.blob;
				console.log("blob:"+blob);
				//convertStreams(recorderAv.blob);
				recorderAv = null;
				/*			    
					var jsonText = '{"calltype":"'+callType+'","type":"recordfile","callid":"'+callId+'"}';
				notify = $msg({to: jid,"type" : "normal"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
				serverConnection.send(notify);
				*/
				var res = saveRecordedBlob(blob);

				if (callAction != undefined && res != "") {
					console.log("callAction---"+callAction);
					if (callAction == 'hangup') {
						if (user == "owner") {
							var jsonText = '{"calltype":"' + callType + '","type":"hostdisconnect","callid":"' + callId + '"}';
							notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
							serverConnection.send(notify);
						} else {
							disconnectReqVideoCall();
						}

					}
					hangupwithrecord();
				}
			});

		}
	} else if (callType == "A") {
		var obj = $('#arecordImg');
		if ($(obj).attr('src').indexOf('record1.svg') != -1) {
			recordStatus = "on";


			$("#previewDiv").hide();
			$(obj).attr('src', path + '/images/cme/' + $(obj).attr('toglsrc'));
			$('#recordStatusDiv').find('span').text("REC");
			$('#recordStatusDiv').show();

			recordedChunks = [];
			streamArray = [];
			streamArray.push(localVideo.captureStream());
			$('div#remote-audio').find('video').each(function () {
				streamArray.push(this.captureStream());
			});

			initAudioRecord();


		} else {

			recordStatus = "off";
			$(obj).attr('src', path + '/images/cme/' + $(obj).attr('defsrc'));

			$('#recordStatusDiv').hide();

			recorderAv.stop();
			//console.log("recordedChunks length:::"+recordedChunks.length);
			var blob = new Blob(recordedChunks);

			openPreviewVideo(blob, callType);

			recorderAv = null;
			recordedChunks = [];
			streamArray = [];
			var res = saveRecordedBlob(blob);
			if (callAction != undefined && res != "") {
				//console.log("callAction---"+callAction);
				if (callAction == 'hangup') {
					if (user == "owner") {
						var jsonText = '{"calltype":"' + callType + '","type":"hostdisconnect","callid":"' + callId + '"}';
						notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
						serverConnection.send(notify);
					} else {
						disconnectReqVideoCall();
					}

				}
				hangupwithrecord();
			}
		}

	}
}

function initAudioRecord() {
	recorderAv = null;
	var audioMixer = new MultiStreamsMixer(streamArray);
	var options = {
		type: 'audio',
		mimeType: 'audio/webm',
		timeSlice: 1000,
		ondataavailable: handleDataAvailable
	}

	recorderAv = new MediaStreamRecorder(audioMixer.getMixedStream(), options);
	recorderAv.record();
	$('body').children('canvas').remove();
}

var recordedChunks = [];
function handleDataAvailable(data) {
	recordedChunks.push(data);
}

function addStreamToRecord(userIdglb) {
	if (callType == "AV") {
		recorderAv.addStreams(document.getElementById("localVideo_" + userIdglb).captureStream());
	} else {
		recorderAv.stop();
		streamArray.push(document.getElementById("remoteAudio_" + userIdglb).captureStream());
		initAudioRecord();
	}
}


function resetRecordStreams() {
	if (typeof recorderAv != "undefined" && recorderAv != null) {
		// console.log("---reset streams--->");
		streamArray = [];
		streamArray.push(localVideo.captureStream());
		if (callType == "AV") {
			$('div#local-video1').find('video').each(function () {
				streamArray.push(this.captureStream());
			});
			recorderAv.resetVideoStreams(streamArray);   //----- resetting the streams for recording
		} else {
			$('div#remote-audio').find('video').each(function () {
				streamArray.push(this.captureStream());
			});
			recorderAv.stop();
			initAudioRecord();
		}

	}
}


function saveRecordedBlob(blob) {
	//console.log('save record--------->');

	var formData = new FormData();
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + ' ' + time;
	var fileName = "";
	//var toId=jid.split("@")[0];


	if (callType == "AV")
		fileName = "Record " + dateTime + ".webm";
	else
		fileName = "Record " + dateTime + ".webm";

	formData.append('file', blob, fileName);
	formData.append('place', "uploadRecordedData");
	formData.append('resourceId', callId);
	formData.append('company_id', companyIdglb);
	formData.append('user_id', userIdglb);
	formData.append('menuTypeId', vConvId);
	// var device = 'web';

	// $.ajax({
	// 	url: path + '/ChatAuth?act=uploadRecordedData&fromUser=' + userIdglb + '&companyId=' + companyIdglb + '&callId=' + callId + '&convId=' + vConvId + '&device=' + device,
	// 	type: 'POST',
	// 	dataType: 'text',
	// 	data: formData,
	// 	//async : false,
	// 	error: function (jqXHR, textStatus, errorThrown) {
	// 		checkError(jqXHR, textStatus, errorThrown);
	// 		$("#loadingBar").hide();
	// 		timerControl("");
	// 	},
	// 	contentType: false,
	// 	processData: false,
	// 	success: function (result) {
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/upload",
		type: 'POST',
		processData: false,
		contentType: false,
		cache: false,
		data: formData,
		dataType: 'text',
		// data: formData,
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			// $('#loadingBar').addClass('d-none').removeClass('d-flex');
		},
		success: function (result) {
			checkSessionTimeOut(result);
			// if (result != "SESSION_TIMEOUT") {
				var ext = result.split("@@")[1];
				var fileSource = lighttpdpath + "/videoRecords/" + result.split("@@")[0] + "." + ext;
			// }

		}
	});
	return true;

}

function cmeopenVideo(obj) {
	var fileExt = $(obj).attr("extenstion");
	var source = $(obj).attr("src");
	var callType = $(obj).attr("callType");
	if (callType == "AV") {
		document.getElementById('recAudio').pause();
		$("#previewDiv").show().children('#recAudio').hide().parent().children('#recVideo').attr('src', source).show();
		document.getElementById('recVideo').play();
	} else {
		document.getElementById('recVideo').pause();
		$("#previewDiv").show().children('#recVideo').hide().parent().children('#recAudio').attr('src', source).show();
		document.getElementById('recAudio').play();
	}

}

function openPreviewVideo(source, callType) {
	if (callType == "AV") {
		document.getElementById('recAudio').pause();
		$("#previewDiv").show().children('#recAudio').hide();
		$("#previewDiv").children('#recVideo').show();
		document.getElementById('recVideo').src = window.URL.createObjectURL(source);
		document.getElementById('recVideo').play();

	} else {
		document.getElementById('recVideo').pause();
		$("#previewDiv").show().children('#recVideo').hide();
		$("#previewDiv").children('#recAudio').show();
		document.getElementById('recAudio').src = window.URL.createObjectURL(source);
		document.getElementById('recAudio').play();
	}
}


// function closePreviewDiv() {
// 	document.getElementById('recAudio').pause();
// 	document.getElementById('recVideo').pause();
// 	$("#previewDiv").hide().children('#recVideo').hide().parent().children('#recAudio').hide();
// }

/*
var inputCtx = $( '.input-canvas canvas' )[ 0 ].getContext( '2d' );
var outputCtx = $( '.output-canvas canvas' )[ 0 ].getContext( '2d' );
var cwidth = 300;
var cheight = 225;
var recstream = null;
 
function drawToCanvas() {
	   // draw video to input canvas
	   inputCtx.drawImage( localVideo, 0, 0, 140, 100 );
	   inputCtx.drawImage( remoteVideo, 160, 0, 140, 100 );

	   // get pixel data from input canvas
	   var pixelData = inputCtx.getImageData( 0, 0, cwidth, cheight );

	   var avg, i;

	   // simple greyscale transformation
	   for( i = 0; i < pixelData.data.length; i += 4 ) {
		   avg = ( pixelData.data[ i ] + pixelData.data[ i + 1 ] + pixelData.data[ i + 2 ] ) / 3;
		   pixelData.data[ i ] = avg;
		   pixelData.data[ i + 1 ] = avg;
		   pixelData.data[ i + 2 ] = avg;
	   }

	   outputCtx.putImageData( pixelData, 0, 0 );
	   requestAnimationFrame( drawToCanvas );
   }
*/

//---------------------sending the docId for Transcript stanza creation starts here.....

function sendingDocIdTranscript() {
	// console.log("inside the sendingDocIdTranscript......."+transDocId);
	var jsonText = '{"calltype":"' + callType + '","type":"audioVideoTranscript","callid":"' + callId + '","docId":"' + transDocId + '"}';
	var message = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);; // preapring the chat stanza to send		
				    // console.log("message:::>>>"+JSON.stringify(jsonText));
	serverConnection.send(message);   // sending the highlight message

}
//sending the transcript text via stanza starts here...........

function sendingTextOutputTranscript() {
	// console.log("inside the stanza  sendingTextOutputTranscript.......");
	var jsonText = '{"calltype":"' + callType + '","type":"aVTextTranscript","callid":"' + callId + '","docId":"' + transDocId + '","userId":"' + userIdglb + '","uFirstName":"' + userFullname + '","tTimer":"' + tranTimer + '","transText":"' + glbRecResult + '"}';
	sendData(jsonText);
	// var message = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);; // preapring the chat stanza to send		
	// 			    // console.log("message:::>>>"+JSON.stringify(jsonText));
	// serverConnection.send(message);   // sending the highlight message

}

function showFileAfterCallConnect() {
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/updateCallConnect?callId="+callId+"&docId="+transDocId,
		type: "PUT",
		dataType: 'text',
		contentType: "application/json",
		// data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			console.log(result);
			checkSessionTimeOut(result);
			// if (result != "SESSION_TIMEOUT") {
				if (result = "success") {
					console.log("Connected");
				} else {
					console.log("Not Connected");
				}
			// }
		}

	});

}

function chatBack(obj) {
	if (obj == "on") {
		ongoingCall = "on";
		if (callType == "AV") {
			$("#chatVideo").show();
			$("#chatAudio").hide();
			$("#cmecallcontact").hide();
			// $("#changescreen").show();
			$(".cmeMainScreenDivs").show();
			var chatVideo = document.getElementById("chatVideo");
			chatVideo.srcObject = document.getElementById('remoteVideo').srcObject;
		} else {
			$("#chatVideo").hide();
			$("#cmecallcontact").hide();
			// $("#changescreen").show();
			$(".cmeMainScreenDivs").show();

			var url = $("#aRemoteImage").find(".callConnectImage").attr("src");
			var title = $("#aRemoteImage").find(".callConnectImage").attr("title");
			$("#chatAudio").attr("src", url);
			$("#chatAudio").attr("title", title);
			$("#chatAudio").show();
			$("img.callConnectImage").removeClass("hoverZoomLink");
		}
		$('#avDragElement').css({ 'right': '18px', 'top': '10vh', 'left': '' });
		$('#avDragElement').draggable({
			drag: function () {
				$(this).css('right', '');
			}
		});
		$("#videoContent").hide();
		$("#chatContent").show();
		$("#chatback1").show();

	} else {
		ongoingCall = "off";
		document.getElementById("chatVideo").srcObject = null;
		$("#chatVideo").hide();
		$("#chatAudio").hide();
		$("#chatContent").hide();
		$("#chatback1").hide();
		// highlightVideo();
		$("#videoContent").show();
		$("#videoChatText").trigger("blur");
		
		$("#chatFor").hide();
		if (callType == "AV") {
			$("#cmecallcontact").show();
			// $("#changescreen").hide();
			$(".cmeMainScreenDivs").hide();
			$("#videoFor").find(".arrow").css("left", "-2px");
			$("#videoFor").find(".arr").attr('src', path + '/images/temp/slideboxright.png');
			$("#videoFor").find(".arrow").css("left", "-2px");
			$("#videoFor").find(".arr").css("width", "41px");
			$("#avCallContainer").css({ 'width': '100%' });
			$("#videoChatText").focus();
		}
		if (callType == "A") {
			$("#cmecallcontact").show();
			// $("#changescreen").hide();
			$(".cmeMainScreenDivs").hide();
			$("#audioFor").find(".arrow").css("left", "-2px");
			$("#audioFor").find(".arr").attr('src', path + '/images/temp/slideboxright.png');
			$("#audioFor").find(".arrow").css("left", "-2px");
			$("#audioFor").find(".arr").css("width", "41px");
			$("#avCallContainer").css({ 'width': '100%' });
			$("#videoChatText").focus();
		}

	}
}


function resizeVideo() {
	var height = $(window).height();
	var vHeight = height - $("#local-video1").height();
	if (userConnected.length > 1) {

		if ($("#vtoggle img").attr('src').indexOf('remoteVup.png') > 0) {
			$("#video-content").css({ 'height': '' + vHeight + 'px' });
			// $("#local-video1").show();
			$(".slide").show();
			$("#vtoggle img").css({ 'margin-top': '-42px' });
			$("#vtoggle").show();
		}

	} else {
		//$("#video-content").css({'height':'100%'});
		// $("#local-video1").hide();
		$("#video-content").css("height", "100%");
		$(".slide").hide();
		$("#vtoggle").show();
		$("#scroll1").show();
		$("#scroll2").show();

	}
}

function resizeAudio() {

	// var height = $( window ).height();
	//var vHeight = height - $("#remote-audio").height();
	if (userConnected.length > 1) {

		if ($("#atoggle img").attr('src').indexOf('remoteVup.png') > 0) {
			// $("#video-content").css({'height':''+vHeight+'px'});
			// $("#remote-audio").css("display", "grid");comment
			$(".slide").show();
			$("#aRemoteImage").css({ 'margin-top': '9vh' });
			$("#atoggle img").css({ 'margin-top': '-42px' });
			$("#atoggle").show();
		}

	} else {
		//$("#video-content").css({'height':'100%'});
		// $("#remote-audio").css("display", "grid");
		//$(".slide").hide();
		$(".slide").show();
		$("#aRemoteImage").css({ 'margin-top': '9vh' });
		$("#atoggle").show();
		$("#aRemoteImage").css({ 'margin-top': '9vh' });
		$("#atoggle img").css({ 'margin-top': '-42px' });
		$("#audioFor").find("#ascroll1").show();
		$("#audioFor").find("#ascroll2").show();


	}
}

function toggleRemoteVideo() {
	var height = $(window).height();
	var vHeight = height - $("#local-video1").height();
	if ($("#local-video1").is(':visible')) {
		$("#local-video1").hide();
		$(".slide").hide();
		$("#video-content").css({ 'height': 'calc(101vh - 82px - 63px)' });
		$("#vtoggle img").attr("src", path + '/images/temp/remoteVdown.png');
		$("#vtoggle img").css({ 'margin-top': '0px' });
		$("#scroll1").hide();
		$("#scroll2").hide();

	} else {
		$("#local-video1").css("display", "");
		$(".slide").show();
		$("#video-content").css({ 'height': '' + vHeight + 'px' });
		$("#vtoggle img").attr("src", path + '/images/temp/remoteVup.png');
		$("#vtoggle img").css({ 'margin-top': '-42px' });
		$("#scroll1").show();
		$("#scroll2").show();
	}

}

function toggleRemoteAudio() {

	if ($("#remote-audio").is(':visible')) {
		$("#remote-audio").hide();
		$(".slide").hide();
		$("#aRemoteImage").css({ 'margin-top': '9vh' });
		$("#atoggle img").attr("src", path + '/images/temp/remoteVdown.png');
		$("#atoggle img").css({ 'margin-top': '0px' });
		$("#audioFor").find("#ascroll1").hide();
		$("#audioFor").find("#ascroll2").hide();
	} else {
		// $("#remote-audio").css("display", "grid");
		$(".slide").show();
		$("#aRemoteImage").css({ 'margin-top': '9vh' });
		$("#atoggle img").attr("src", path + '/images/temp/remoteVup.png');
		$("#atoggle img").css({ 'margin-top': '-42px' });
		$("#audioFor").find("#ascroll1").show();
		$("#audioFor").find("#ascroll2").show();
	}
}

function closeTSEditPopUp(){
	$('#cmetranscriptEdit').hide();
	$("#cmePopupHL").show();
	$("#transparentDiv").show();
	$("#ckeditor-height").remove();
}

function closeTSEditPopUp2(){
	$('#cmetranscriptEdit').hide();
	$("#transparentDiv").hide();
	$("#ckeditor-height").remove();
}

function viewTranscript(allText, msgId, file, messageTitle, type) {
	// alert("inside viewts");
	$("#cmePopupHL").hide();
	$("#transparentDiv").hide();
	$('#cmetranscriptEdit').html('');
	var UI='<div class="SBactiveSprintlistCls" style="top:60px !important;" id="SBactiveSprintlistId" selectedstories="">'
				+'<div class="mx-auto w-50" style="max-width: 600px;">'
					+'<div class="modal-content container px-0" style="max-height: 500px !important;">'
						+'<div class="modal-header p-2 pl-3 d-flex align-items-center" style="border-bottom: 1px solid #b4adad !important;">'
							+'<span id="" class="hListHeaderItems modal-title" style="color:black;font-size: 16px !important;" onclick="">Meeting Transcript</span>'  
							// +'<span id="" class="hProjHeader modal-title" style="display: none;color:black;font-size: 16px !important;" onclick="">Workspace</span>'  
							// +'<span id="groups" class="modal-title cursor ml-4" style="font-size:14px;" onclick="groupPopup()">GROUPS</span>'
							// +'<span class="modal-title" style="width:20%;font-size:14px;">Start Date</span>'
							// +'<span class="modal-title" style="width:20%;font-size:14px;">End Date</span>'
							// +'<input  id="searchText6" class="border rounded d-none searchHeader" style="width:86%;font-weight: normal;font-size: 13px;height: auto;padding: 3px 8px;"  placeholder="Search" onkeyup="searchContactsAV()">'
							+'<img id="" src="/images/conversation/edit.svg" class="mr-4 cursor" title="edit" onclick="showEditButton();" style="width: 22px;">'
							// +'<img id="" src="images/cme/leftarrow_blue.svg" title="Back" onclick="backToHighlights()" class="hProjHeader ml-auto mr-4 cursor "  style="display: none;width: 22px;">'
							// +'<img id="" src="images/temp/associate.png" title="Share To Workspace" onclick="getAllProjectsForHighlights()" class="hListHeaderItems ml-auto mr-4 cursor "  style="width: 22px;">'
						+'</div>'
						+'<div class="" style="">'
						if(type == '1'){
							UI+='<button type="button" onclick="closeTSEditPopUp2();event.stopPropagation();" class="close p-0 AgileCloseButton" style="top: 5px;right: 10px;color: black;outline: none;">&times;</button>'
						}
						else{
							UI+='<button type="button" onclick="closeTSEditPopUp();event.stopPropagation();" class="close p-0 AgileCloseButton" style="top: 5px;right: 10px;color: black;outline: none;">&times;</button>'
						}
						UI+='</div>'
						+'<div class="modal-body11 wsScrollBar mb-2 px-3 mt-2 p-0 m-0" style="height: 420px;">'
							+ "<div class='' style=''>"
								+ "<textarea  id='transBody' readonly style='height: 100%;width:100%; overflow:auto; padding:0px 5px;resize: none;'>" + allText + "</textarea>"
							+ "</div>"
						+'</div>'
						+'<div id="" class="modal-footer11 pb-3 d-flex justify-content-end">'
							// +"<div id=\"\" onclick=\"closeaddGroup();\" style=\" width: 70px !important;float: right;\" class=\"createBtn mt-0 mr-2\">Cancel</div>"
							// +"<div id=\"editgrpbtn\" onclick=\"createGrpId();\" style=\" width: 70px !important;float: right;\" class=\" mt-0 createBtn SAVE_cLabelHtml\">Save</div>"
							+'<div id="tsfooter" style="display: none;">'
								+'<img id="transEdit" src="/images/task/remove.svg" title="Cancel" onclick="showEditButton();" class="mr-2 cursor "  style="width: 22px;">'
								+'<img id="" src="/images/task/tick.svg" title="Save" onclick="updateTheTextFiles(\'' + file + '\', \'' + msgId + '\', \'' + messageTitle + '\', \'' + type + '\');" class="mr-4 cursor "  style="width: 22px;">'
							+'</div>'
						+'</div>'
					+'</div>'
				+'</div>'
			+'</div>'

	$('#cmetranscriptEdit').html(UI).show();

	ckeditor2();
	$("#transparentDiv").show();

	//console.log("messageTitle---"+messageTitle+"--msgId--"+msgId+"--file--"+file);
	// $('#ideaTransparent').removeClass('ideaTransparent').addClass('transparentDivChat');
	// if (!($('#TranscriptPopUp').is(':visible'))) {

	// 	var UI = "<div class='modal-dialog row'  id='TranscriptPopUp' style='width: 98%; z-index: 10000; font-family: OpenSansRegular; margin-top: 4%;margin-left: 1%;margin-right: 1%;'>"
	// 		+ "<div class='modal-content hModalContent'>"
	// 		+ "<div class='modal-header' style='padding:1.5vh'>"
	// 		+ "<button type='button' class='close' onclick='closeTranscriptPopUp()'  style='margin-top:-5px;' data-dismiss='modal'>&times;</button>"
	// 		+ "<span  class='hListHeaderItems' style='margin-top: 0.5vh;float:left;font-size: 15px; font-family: opensansbold; font-weight: bold;'>&nbsp;Meeting Transcript</span>"
	// 		+ '<span class="hProjHeader" style="display:none;margin-top: 0.5vh;float:left;font-size: 15px; font-family: opensansbold; font-weight: bold;">Workspace</span>'
	// 		+ '<img class="hListHeaderItems" src="' + path + '/images/temp/hedit.png" title="edit" onclick="showEditButton()" style="cursor:pointer;float: right;margin-right: 10px;height: 20px;width: 20px;margin-top: 4px;">'

	// 		+ "</div>"
	// 		+ "<div class='modal-body' style='padding: 0px 15px;'>"
	// 		+ "<div class='row'>"
	// 		+ "<div class='col-sm-12' style='padding:5px'>"

	// 		+ "<textarea  id='transBody' readonly style='height:68vh;width:100%; overflow:auto; padding:0px 5px'>" + allText + "</textarea>"

	// 		+ "</div>"
	// 		+ "</div>"
	// 		+ "<div class='row'>"
	// 		+ "<div id='transEdit' style='float: right;width: 50%;margin-bottom: 1%;display:none'><img  title='update' style='cursor: pointer;margin-bottom: 2%;display: block;height: 25px;float: right;margin-right: 7%;' src='" + path + "/images/temp/hupdate.png' onclick='updateTheTextFile(\"" + file + "\"," + msgId + ",\"" + messageTitle + "\")' />"
	// 		+ "<img  title='Cancel' style='cursor: pointer;margin-bottom: 2%;display: block;height: 25px;float: right;margin-right: 10px;' src='" + path + "/images/temp/hcancel.png'  onclick='showEditButton()' />"
	// 		+ "</div>"
	// 		+ "</div>"
	// 		+ "</div>"


	// 		+ " </div>"
	// 		+ "</div>";
	// }
	// //new ui closed
	// $('#viewTranscriptPopUp').html(UI);
	//readTextFile();
}

function closeTranscriptPopUp() {
	$('#viewTranscriptPopUp').html('');
	$('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
}
var tempData;
function showEditButton() {

	if ($("#transEdit").is(':visible')) {
		$("#tsfooter").hide();
		$('#transBody').prop('readonly', true);
		$("#transBody").html('').val(tempData);
	} else {
		tempData = $("#transBody").val();
		$("#tsfooter").show();
		$("#transBody").removeAttr("readonly");
	}
}

function crossPlatformResponse() {
	var Id = $("#incoming").attr('callerid');
	var jid = Id + "@" + cmeDomain + ".colabus.com";
	stopCalltimer();
	var jsonText = '{"calltype":"' + callType + '","type":"callcrossdevice","callid":"' + callId + '"}';
	notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);

	$('#sound').html("");
	$(".incomingCall").hide();

	videoAlertNotifn("Currently Video/Audio Calling Feature is availble between same Device Type (e.g Web  to Web or Mobile to Mobile).");
	sendAvailablePresence();
	closeVideoCall();

	Id = Id + "-" + cmeDomain + "-colabus-comm";
	if ($('#' + Id).is(':visible')) {
		openNewChatBox(Id, "video");
	}
	clearCallFlash();


}

function callCrossDeviceAccepted(id) {
	if (userConnected.length < 1) {
		closeVideoCall();
		sendAvailablePresence();
	} else {
		$("#notification").hide();
	}

	videoAlertNotifn("Currently Video/Audio Calling Feature is availble between same Device Type (e.g Web  to Web or Mobile to Mobile).");
	var res = updateCallResponse(id, "callnotresponded");
}

function userDisconnect(id) {
	//$("#RemoteDiv_"+id).remove();
	//$("#participants_"+id).remove();
	if (userConnected.length < 2) {
		updateCallStatus(callId, "closed");
		closeVideoCall();
	}
	var ix = switchAdminArray.indexOf(id.toString());
	if (ix > -1) {
		switchAdminArray.splice(ix, 1);
	}
	var jid = id + "@" + cmeDomain + ".colabus.com";
	stopCalltimer();

	// Below stanza is to the removed user 
	var jsonText = '{"calltype":"' + callType + '","type":"userdisconnect","callid":"' + callId + '"}';
	notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);

	// Below stanza is to the other users.. sending to the room
	var jsonText = '{"calltype":"' + callType + '","type":"removeRemoteUser","removeUserId":"' + id + '","callid":"' + callId + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);

	if (callType == "AV") {
		if (userConnected.length < 2) {
			//$("#local-video").css("display","block");
			document.getElementById('localVideo').srcObject = localStream;
			document.getElementById('remoteVideo').srcObject = remoteStream[userConnected[0]];
		}

	}

	//Remove user from local screen 
	var recieverName = $('div#' + id + ' div.user_box_name').text();
	//if(userConnected.length > 1){
	callDisconnectAccepted("removeRemoteUser", recieverName, id);
	//}
	// autogrowParticipants();
	//console.log(id);
	var idx = muteArray.indexOf(id.toString());

	if (idx > -1) {
		muteArray.splice(idx, 1);
	}
	var idx1 = createMuteUI.indexOf(id.toString());
	// console.log(muteArray);
	if (idx1 > -1) {
		createMuteUI.splice(idx1, 1);
	}
	var idx_of = stazaofMute.indexOf(id.toString());
	if (idx_of > -1) {
		stazaofMute.splice(idx_of, 1);
	}

	var idx_wb = whiteboardArray.indexOf(id.toString());
	if (idx_wb > -1) {
		whiteboardArray.splice(idx_wb, 1);
	}
	var idx_ss = screenshareArray.indexOf(id.toString());
	if (idx_ss > -1) {
		screenshareArray.splice(idx_ss, 1);
	}
}


function customNextScroll() {
	/*	document.getElementById('remote-audio').scrollLeft += 20;
		
		var clone = $(".item:visible:first").clone();
		var id = $(".item:visible:first").attr("id").split("_")[1];
		
		//$("div[id^=RemoteDiv_]").hide();
		//var divlength = $("#local-video1 .item").length
		var next=$("#RemoteDiv_"+id).next().attr('id').split("_")[1];
		
		switchVideo(next);*/




	if (callType == "A") {
		var x = $('.item').filter(function () {
			//   return $(this).css('background').indexOf('rgb(11, 72, 96)') > -1
			return $(this).css('background').indexOf('rgb(11, 72, 96)') > -1
			//  alert($(this).css('color'))rgb(11, 72, 96)
		});
		var id = "";
		if (typeof x != "undefined") {
			try {
				id = x.attr('id').split("_")[1];
				var next = $("#RemoteDiv_" + id).next().attr('id')
				if (typeof next != 'undefined') {
					next = next.split("_")[1];
					document.getElementById('remote-audio').scrollLeft += 100;
					switchVideo(next);
				}
				else {
					document.getElementById('remote-audio').scrollLeft += 100;
				}
			} catch (e) {
				console.log(e);
			}
		}
		else {
			id = $(".item:visible:first").attr("id").split("_")[1];
			var next = $("#RemoteDiv_" + id).next().attr('id').split("_")[1];
			if (typeof next != 'undefined') {
				//document.getElementById('remote-audio').scrollLeft += 20;
				switchVideo(next);
			}

		}
	}
	else {
		var x = $('.item').filter(function () {
			//   return $(this).css('background').indexOf('rgb(11, 72, 96)') > -1
			return $(this).css('border').indexOf('rgb(76, 146, 167)') > -1
			//  alert($(this).css('color'))rgb(11, 72, 96)
		});
		var id = "";
		if (typeof x != "undefined") {
			try {
				id = x.attr('id').split("_")[1];
				var next = $("#RemoteDiv_" + id).next().attr('id')
				if (typeof next != 'undefined') {
					next = next.split("_")[1];
					document.getElementById('local-video1').scrollLeft += 100;
					switchVideo(next);
				}
				else {
					document.getElementById('local-video1').scrollLeft += 100;
				}
			} catch (e) {
				console.log(e);
			}
		}
		else {
			id = $(".item:visible:first").attr("id").split("_")[1];
			var next = $("#RemoteDiv_" + id).next().attr('id').split("_")[1];
			if (typeof next != 'undefined') {
				//document.getElementById('remote-audio').scrollLeft += 20;
				switchVideo(next);
			}
		}
	}


	/*var clone = $(".item:visible:first").clone();
	var id = $(".item:visible:first").attr("id").split("_")[1];
	alert(id);
	if(callType=="AV"){
		var w = document.getElementById("local-video1").clientWidth;
		var videosrc = document.getElementById('localVideo_'+id).srcObject;
	}else{
		var w = document.getElementById("remote-audio").clientWidth;
		var videosrc = document.getElementById('remoteAudio_'+id).srcObject;
	}
	
	$(".item:visible:first").remove();
	if(callType=="AV"){
		$("#local-video1").append(clone);
		document.getElementById('localVideo_'+id).srcObject = videosrc;
	}else{
		$("#remote-audio").append(clone);
		document.getElementById('remoteAudio_'+id).srcObject = videosrc;
	}*/
	//$("div[id^=RemoteDiv_]").hide();
	//var divlength = $("#local-video1 .item").length
	//$("#RemoteDiv_"+id).next().attr('id');

	/*	if(w<364){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+2;
		}else if(w>364 && w<547){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+3;
		}else if(w>547 && w<729){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+4;
		}else if(w>729 && w<911){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+5;
		}else if(w>911 && w<1093){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+6;
		}else if(w>1094 && w<1276){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+7;
		}else if(w>1094 && w<1276){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+8;
		}else if(w>1276 ){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+10;
		}
		
		for(var i= dataIndex;i<endIndex; i++){
			 $( ".item" ).eq(i).show();
		}*/

}

function customPrevScroll() {

	/*document.getElementById('remote-audio').scrollLeft -= 20;
	
	
	
	var id = $(".item:visible:first").attr("id").split("_")[1];
	
	//$("div[id^=RemoteDiv_]").hide();
	//var divlength = $("#local-video1 .item").length
	var next=$("#RemoteDiv_"+id).prev().attr('id').split("_")[1];
	
	switchVideo(next);*/

	if (callType == "A") {
		var x = $('.item').filter(function () {
			//   return $(this).css('background').indexOf('rgb(11, 72, 96)') > -1
			return $(this).css('background').indexOf('rgb(11, 72, 96)') > -1
			//  alert($(this).css('color'))rgb(11, 72, 96)
		});
		var id = "";
		if (typeof x.html() != "undefined") {
			try {
				id = x.attr('id').split("_")[1];
				var next = $("#RemoteDiv_" + id).prev().attr('id');
				if (typeof next != 'undefined') {
					next = next.split("_")[1];
					document.getElementById('remote-audio').scrollLeft -= 100;
					switchVideo(next);
				}
				else {
					document.getElementById('remote-audio').scrollLeft -= 100;
				}
			}
			catch (e) {
				console.log(e);
			}

		}
		else {
			id = $(".item:visible:first").attr("id").split("_")[1];
			var next = $("#RemoteDiv_" + id).next().attr('id').split("_")[1];
			if (typeof next != 'undefined') {
				document.getElementById('remote-audio').scrollLeft -= 100;
				switchVideo(next);
			}
		}

	}
	else {
		var x = $('.item').filter(function () {
			//   return $(this).css('background').indexOf('rgb(11, 72, 96)') > -1
			return $(this).css('border').indexOf('rgb(76, 146, 167)') > -1
			//  alert($(this).css('color'))rgb(11, 72, 96)
		});
		var id = "";
		if (typeof x.html() != "undefined") {
			try {
				id = x.attr('id').split("_")[1];
				var next = $("#RemoteDiv_" + id).prev().attr('id');
				if (typeof next != 'undefined') {
					next = next.split("_")[1];
					document.getElementById('local-video1').scrollLeft -= 100;
					switchVideo(next);
				}
				else {
					document.getElementById('local-video1').scrollLeft -= 100;
				}
			} catch (e) {
				console.log(e);
			}
		}
		else {
			id = $(".item:visible:first").attr("id").split("_")[1];
			var next = $("#RemoteDiv_" + id).next().attr('id').split("_")[1];
			if (typeof next != 'undefined') {
				document.getElementById('local-video1').scrollLeft -= 100;
				switchVideo(next);
			}
		}
	}

	/*var clone = $(".item:visible:last").clone();
	var id = $(".item:visible:last").attr("id").split("_")[1];
	if(callType=="AV"){
		var w = document.getElementById("local-video1").clientWidth;
		var videosrc = document.getElementById('localVideo_'+id).srcObject;
	}else{
		var w = document.getElementById("remote-audio").clientWidth;
		var videosrc = document.getElementById('remoteAudio_'+id).srcObject;
	}
	
	$(".item:visible:last ").remove();
	if(callType=="AV"){
		$("#local-video1").prepend(clone);
		document.getElementById('localVideo_'+id).srcObject = videosrc;
	}else{
		$("#remote-audio").prepend(clone);
		document.getElementById('remoteAudio_'+id).srcObject = videosrc;
	}
	
	$("div[id^=RemoteDiv_]").hide();*/
	//var divlength = $("#local-video1 .item").length
	/*$("#RemoteDiv_"+id).next().show();*/
	/*	if(w<364){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+2;
		}else if(w>364 && w<547){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+3;
		}else if(w>547 && w<729){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+4;
		}else if(w>729 && w<911){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+5;
		}else if(w>911 && w<1093){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+6;
		}else if(w>1094 && w<1276){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+7;
		}else if(w>1094 && w<1276){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+8;
		}else if(w>1276 ){
			var dataIndex = $(".item:visible:first").index()+1;
			var endIndex= $(".item:visible:first").index()+10;
		}
		
		for(var i= dataIndex;i<endIndex; i++){
			 $( ".item" ).eq(i).show();
		}*/

}


function resizeRemoteStream() {
	if (callType == "AV") {
		var w = document.getElementById("local-video1").clientWidth;
	} else {
		var w = document.getElementById("remote-audio").clientWidth;
	}
	if (w < 364) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 2;
	} else if (w > 364 && w < 547) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 3;
	} else if (w > 547 && w < 729) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 4;
	} else if (w > 729 && w < 911) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 5;
	} else if (w > 911 && w < 1093) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 6;
	} else if (w > 1094 && w < 1276) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 7;
	} else if (w > 1094 && w < 1276) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 8;
	} else if (w > 1276) {
		var dataIndex = $(".item:visible:first").index() + 1;
		var endIndex = $(".item:visible:first").index() + 10;
	}

	for (var i = dataIndex; i < endIndex; i++) {
		$(".item").eq(i).show();
	}
}
/*function getUserList(){session timed
	  
	  
	  var name=""; 
	  var imgType="";
	  var useRegType="";
	  var upgradePlan="";
	  var useRegType1="";
	  var duser= "";
	  var user_status ="";
	  var id="";
 
	  for(var i=0;i<userName.length;i++){ // using userName from colabus to filter users from bogus data from chat server
		  
			  name= userName[i].name;
			  imgType= userName[i].imgType;
			  useRegType=userName[i].useRegType;
			  upgradePlan=userName[i].upgradePlan
			  id=userName[i].userId;
			  if(imgType=="" || imgType == null){
				 imgType='png';
			  }
			  name= name.replace("CHR(90)"," ").replace("CHR(90)"," ").replace("CHR(90)"," ").replace("CHR(90)"," ");
		 
	  if(typeof useRegType !='undefined' && useRegType != null && useRegType!= ""){
				  if(useRegType.indexOf('_')!= -1){
				useRegType1=useRegType.split('_')[1];
			}else{
				useRegType1 == useRegType;
			}
			if( (useRegType1.toLowerCase()=="social" ||useRegType1.toLowerCase()=="standard") && upgradePlan==0){
					duser = 'socialUser';	
			}
	   }
		 
		  var contact= $("<li class='"+duser+"' title=' ' disabled='true' count='0' style='height:61px;float:left;width:300px;cursor:pointer;border-bottom: 1px solid #ced2d5; '  imgType='" + imgType + "' onclick='openNewChatBox(this)' >" +
					 "<div id='"+id+"'class='roster-contact offline row' style='padding-top:5px;width:100%;' >" + 
					 "<div class='col-xs-3' style='width: 15%; padding-left: 8px;padding-top:0.5%'> <img src='"+lighttpdpath+"/userimages/"+id+"."+imgType+"?"+imgTime+"' title='"+name+"' onerror='userImageOnErrorReplace(this);' style='height: 34px; min-height: 45px; width:45px;min-width: 45px;border-radius:50%'/></div>" +
					 "<div class='user_box_status' style='margin-left: 46px; margin-top: 5px;position:absolute'></div>"+
					 "<div class='user_box_name col-xs-4' style='text-align: left;font-family: OpenSansRegular; font-size: 14px; width: 50%; padding-top: 7px;;margin-left:2px' value='" +name +"'>" + name +"</div>"+
					 //"<div class=' col-xs-1' style='background: #ff3333; color: #000; float: left; text-align: center; padding: 1%; border-radius: 50%; width: 3%; margin-top: 1%; visibility: hidden;'></div>"+
					 "<div class='hover-call-icons' style='display:none;position: absolute;right: 3%;'>"+
					   "<div class='col-xs-2 videocall' style='min-width: 35px;padding-top:14px;float: right;padding-right: 2px;padding: ;padding-left: 0px;max-width: 13%;margin-right: 1%;'><img src='"+path+"/images/temp/vedioGreen.png'  onclick='start("+id+",\"true\",\"video\")' style='width: 35px;height: 25px;'/></div>"+
					   "<div class='col-xs-2 audiocall' style='min-width: 35px;padding-top:10px;padding-left: 2px;float: right;padding-right: 2px;'><img src='"+path+"/images/temp/Phonegreen.png' onclick='start("+id+",\"true\",\"audio\")'  style='width: 40px;height: 32px;float: right;'/></div>"+
					   "<div class='col-xs-2' style='min-width: 35px;padding-top:12px;padding-left: 2px;float: right;padding-right: 6px;display:none;'><img src='"+path+"/images/temp/screenshare.png' onclick='start("+id+",\"true\",\"screenshare\")'  style='width: 38px;height: 26px;float: right;'/></div>"+
					 "</div>"+ 
				    
					 "</div>"+ 
					 "</li>");
		  $("#videoUserLIst ul").append(contact);
		  $("#videoUserLIst").show();
		  $("#videoUserLIst li").on("mouseover",function(){
			  $(this).find('.hover-call-icons').show();
		  }).on("mouseout", function(){
			  $(this).find('.hover-call-icons').hide();
		  });
	  }
}*/



function fetchRecomdnTaskDetailsView(taskId) {
	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();

	$('#ideaTransparent').removeClass('ideaTransparent').addClass('transparentDivChat');

	var UI = "<div class='modal-dialog row'  id='HightlightPopUp' style='width: 98%; z-index: 10000; font-family: OpenSansRegular; margin-top: 4%;margin-left: 1%;margin-right: 1%;'>"
		+ "<div class='modal-content hModalContent'>"
		+ "<div class='modal-header' style='padding:1.5vh'>"
		+ "<button type='button' class='close' onclick='closeRecomdTaskDetailPopUp()' id='popupClose' style='margin-top:-5px;' data-dismiss='modal'>&times;</button>"
		+ "</div>"
		+ "<div class='modal-body' style='padding: 0px 15px;margin-bottom: 10px;'>"
		+ "<div class='row'>"
		+ "<div class='col-sm-12' >"
		+ "<div class='row'>"
		+ "<div class='col-sm-12' id='hListDiv' class='hListDiv defaultScrollDiv' style='height: 60vh; overflow:auto; padding: 0px;max-height: 500px;0px;'></div>"
		+ "</div>"
		+ "</div>"
		+ "</div>"
		+ "</div>"

		+ " </div>"
		+ "</div>";
	//new ui closed
	$('#recomdTaskDetailPopUp').html(UI);
	$('#hListDiv').html(prepareHighlightTaskUI(taskId)).show();
	fetchWorkspaceProjects('view', 'manualTask');

	$('#chatTaskDivArea_' + taskId).show();
	$('#saveAndUpdateHlightTaskId').hide();
	$('#cancelHlightTaskUI').attr('onclick', 'closeRecomdTaskDetailPopUp();').css('margin-right', '6px');
	$('#hlightAddedUserList').parent().css('height', '110px');
	$('#hlightAddedUserList').css('height', '80px');
	$('#hlightLatestUserComment').parent().show();

	$('#recordStart').hide();

	$.ajax({
		url: path + '/blogAction.do',
		type: "POST",
		data: { act: 'fetchHighlightTaskDetailsView', taskId: taskId, device: 'web' },
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			$("#loadingBar").hide();
			timerControl("");
		},
		success: function (result) {
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				//checkSessionTimeOut(result);
				//console.log("result:"+result);
				PrepareUIforTaskViewNewUI(result);

				if ($('#taskParticipantsInpFiled').val() != '') {
					var taskParticipant = $('#taskParticipantsInpFiled').val();
					var taskParticipantnew = jQuery.parseJSON(taskParticipant);
					$('#hlightAddedUserList ul').html("");
					for (var i = 0; i < taskParticipantnew.length; i++) {
						commonUIForUserList(taskParticipantnew[i].user_id, taskParticipantnew[i].fname);
					}
				}

				if ($('#hlightproject').val() == 0) {
					$('#hlProjListDDContainerSub').find('.HighlighProjListNewUI:eq(0)').trigger('click');
				} else {
					$('#hlProjListDDContainerSub').find('#proj_' + $('#hlightproject').val()).trigger('click');
				}

				var jsonData = jQuery.parseJSON(result);
				var latestCommentData = jsonData[0].taskComment;
				if (latestCommentData.length > 0) {
					$('#hlightLatestUserComment').find('p').text(latestCommentData[0].taskComment);
					$('#hlightLatestUserComment').find('footer').text(latestCommentData[0].userName);
				} else {
					$('#hlightLatestUserComment').find('p').hide();
					$('#hlightLatestUserComment').find('footer').text("No Comments Found");
				}

				$("#loadingBar").hide();
				timerControl("");
			}
		}
	});

}

function closeRecomdTaskDetailPopUp() {
	$('#recomdTaskDetailPopUp').html('');
	$('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
	resizeRemoteStream();
}

/*function DownloadHList(obj){
	
	var filepathDoc=$(obj).attr("src");
	var fileNames=$(obj).attr("id");
	
	var url = path+"/reportAction.do?action=DownloadHlist&filepathDoc="+filepathDoc+"&fileNames="+fileNames;
	
	window.open(url);
	
	
}*/

function Download(obj) {
	var filepath = $(obj).attr("src");
	var fileName = $(obj).attr("id");
	var url = apiPath + "/" + myk + "/v1/download?fileName="+fileName+"&Path=//videoRecords//&place=Downloadvideo";
	window.open(url);
}

function DownloadNew(obj) {
	var filepathNew = $(obj).attr("src");
	var fileName = $(obj).attr("id");

	var url = apiPath + "/" + myk + "/v1/download?fileName="+fileName+"&Path=//uploadedDocuments//&place=Downloadimage";
	// var url = path + "/reportAction.do?action=Downloadimage&filepathNew=" + filepathNew + "&fileName=" + fileName;
	window.open(url);


}
function DownloadTxt(obj) {

	var filepathTxt = $(obj).attr("src");
	var fileName = $(obj).attr("id");

	// var url = path + "/reportAction.do?action=Downloadtxt&filepathTxt=" + filepathTxt + "&fileName=" + fileName;

	var url = apiPath + "/" + myk + "/v1/download?fileName="+fileName+"&Path=//uploadedDocuments//&place=Downloadtxt";


	window.open(url);


}
function DownloadDoc(obj) {

	var filepathDoc = $(obj).attr("src");
	var fileName = $(obj).attr("id");
	var extension = $(obj).attr("extenstion");

	// var url = path + "/reportAction.do?action=Downloaddoc&filepathDoc=" + filepathDoc + "&fileName=" + fileName;
	var url = apiPath + "/" + myk + "/v1/download?fileName="+fileName+"&Path=//uploadedDocuments//&place=Downloaddoc&docId="+fileName+"."+extension;
	window.open(url);
}

function ChangeImagecall(imgUrl, senderMsg, badge_url) {
	//console.log("badge_url-->"+badge_url);				    
	var image = new Image();
	image.src = imgUrl;

	image.onload = function () {
		/*  Below code is for CME alone. */
		//notifyMe(imgUrl, senderMsg); 

		/*  Below code is for Colabus CME . */
		try {
			window.opener.callnotifyMe(imgUrl, senderMsg, badge_url);
		} catch (e) {
			console.log(e);
		}
	};
	image.onerror = function () {
		/*  Below code is for CME alone. */
		//notifyMe(lighttpdpath+"/userimages/userImage.png", senderMsg); 

		/*  Below code is for Colabus CME . */
		try {
			window.opener.callnotifyMe(lighttpdpath + "/userimages/userImage.png", senderMsg, badge_url);
		} catch (e) {
			console.log(e);
		}

	};
	//return true; 
}




//call confirm function
function callConfirm(info, type, execFunc, FUN, display, param, param2) {
	if (!info || !execFunc || !type) {
		throw new Error("Please enter all the required config options!");
	}

	if (type == 'clear') {
		name = "BoxConfirmClear";
	} else if (type == 'close') {
		name = "BoxConfirmClose";
	} else if (type == 'delete') {
		name = "BoxConfirmDelete";
	} else {
		name = "BoxConfirmReset";
	}

	//console.log(execFunc) ;
	retFunc = execFunc;
	var html = '<div id="BoxOverlay" ></div>' +
		'<div id="confirmDiv" class="alertMainCss">' +
		'  <div id="confirmCloseDiv"></div> ' +
		//'  <img src="images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+confirmfun
		'  <div style="box-shadow: 0px 0px 20px #000000;" >' +
		'   <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">' +
		'    <div id="confirm-BoxContenedor" class="' + name + '">' +
		'     <span id="confirmContent">' + info + '</span>' +
		'     <div id="confirm-Buttons" class="alertBtnCss">' +
		'       <input id="BoxConfirmBtnOk" type="submit" value="YES">' +
		'       <input id="BoxConfirmBtnCancel" type="submit" value="NO">' +
		'<input id="BoxConfirmBtnfor" type="submit" style="display:' + display + '" value="CANCEL">' +
		'     </div>' +
		'    </div>' +
		'  </div>' +
		' </div>' +
		'</div>';

	$('body').find('#BoxOverlay').remove();
	$('body').find('#confirmDiv').remove();
	//setTimeout(function(){  
	$('body').append(html);
	//$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').attr("onclick",retFunc);

	$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').click(function () {
		hideConfirmBox();
		// top.frames["menuFrame"].window[retFunc]();
		window[retFunc](param);
	});
	$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function () {
		//stop();
		hideConfirmBox();
		window[FUN](param2);

	});
	$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnfor').click(function () {
		//stop();
		hideConfirmBox();


	});
	$('body').find('#confirmDiv').children('#confirmCloseDiv').click(function () {
		hideConfirmBox();
	});
	//},50);    
}

//for disconeect all call
//admin switch codes

function Alldisconnect() {

	var jsonText = '{"calltype":"' + callType + '","type":"hostdisconnect","callid":"' + callId + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
	updateCallStatus(callId, "closed");
	hangupwithrecord();
	$("#showhost").hide();
	foraudiocall = 0;
	forvideocall = 0
	whiteboardArray = [];
	screenshareArray = [];
	muteArray = [];
	createMuteUI = [];
	stazaofMute = [];
	shareStaus = "off";
	$(".arr").attr('src', path + '/images/temp/slideboxright.png');
	// $(".arrow").css("left","-37px");

	callId = "";

}

function switchtohost() {

	callConfirm(("Do you want to end your call and assign someone as host?"), "delete", "showhost", "hideConfirmBox", "none");
}
function showhost() {
	// console.log("userConnected--->/ "+userConnected);
	var html = "";
	html += '<div id="BoxOverlay" ></div>' +
		'<div id="confirmDiv" class="alertMainCss">' +
		'  <div id="confirmCloseDiv" onclick="hideConfirmBox();"></div> ' +
		//'  <img src="images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+confirmfun
		'  <div style="box-shadow: 0px 0px 20px #000000;" >' +
		'   <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">' +
		'    <div id="confirm-BoxContenedor" class="participant">' +
		'     <div id="confirmContent" style="    color: black;border-bottom: 1px solid;">Participants</div>'



	for (i = 0; i < userConnected.length; i++) {

		html += '     <div id="confirmContent" style="margin-top:10px;cursor: pointer;" onclick="transferAdmin(' + userConnected[i] + ')">' + (i + 1) + ". " + $("#participants_" + userConnected[i]).find("img").attr("title") + '</div>'

		//$("#participants_"+userConnected[i]).find("img").attr("title");



	}
	html += '     <div id="confirm-Buttons" class="alertBtnCss">' +
		'       <input id="BoxConfirmBtnOk" style="visibility:hidden" type="submit" value="OK">' +
		'       <input id="BoxConfirmBtnCancel" style="" onclick="hideConfirmBox();" type="submit" value="CANCEL">' +
		'     </div>' +
		'    </div>' +
		'  </div>' +
		' </div>' +
		'</div>';

	$('body').find('#BoxOverlay').remove();
	$('body').find('#confirmDiv').remove();
	//setTimeout(function(){  copy.png
	$('body').append(html);
	//
}
function hideConfirmBox() {
	$('#BoxOverlay').hide();
	$('#confirmDiv').hide();
}

function transferAdmin(id) {
	$('#BoxOverlay').hide();
	$('#confirmDiv').hide();
	var jsonText = '{"calltype":"' + callType + '","type":"calldisconnect","callid":"' + callId + '","transferid":"' + id + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
	hostUpdate(callId, id);
	hangupwithrecord();
	user = "";
	$("#showhost").hide();
	foraudiocall = 0;
	forvideocall = 0;
	whiteboardArray = [];
	screenshareArray = [];
	muteArray = [];
	createMuteUI = [];
	stazaofMute = [];
	shareStaus = "off";
	$(".arr").attr('src', path + '/images/temp/slideboxright.png');
	// $(".arrow").css("left","-37px");

}
function oncallswitch() {
	callConfirm(("Do you want to assign someone as host?"), "delete", "switchhostoncall", "hideConfirmBox", "none");
}


function switchhostoncall() {
	var html = "";
	html += '<div id="BoxOverlay" ></div>' +
		'<div id="confirmDiv" class="alertMainCss">' +
		'  <div id="confirmCloseDiv" onclick="hideConfirmBox();"></div> ' +
		//'  <img src="images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+confirmfun
		'  <div style="box-shadow: 0px 0px 20px #000000;" >' +
		'   <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">' +
		'    <div id="confirm-BoxContenedor" class="participant">' +
		'     <div id="confirmContent" style="    color: black;border-bottom: 1px solid;">Participants</div>'



	for (i = 0; i < userConnected.length; i++) {

		html += '     <div id="confirmContent" style="margin-top:10px;cursor: pointer;" onclick="TAdmin(' + userConnected[i] + ')">' + (i + 1) + ". " + $('div#userId_' + userConnected[i]).find('.user_box_name').text() + '</div>'

		//$("#participants_"+userConnected[i]).find("img").attr("title");



	}
	html += '     <div id="confirm-Buttons" class="alertBtnCss">' +
		'       <input id="BoxConfirmBtnOk" style="visibility:hidden" type="submit" value="OK">' +
		'       <input id="BoxConfirmBtnCancel" style="" onclick="hideConfirmBox();" type="submit" value="CANCEL">' +
		'     </div>' +
		'    </div>' +
		'  </div>' +
		' </div>' +
		'</div>';

	$('body').find('#BoxOverlay').remove();
	$('body').find('#confirmDiv').remove();
	//setTimeout(function(){  
	$('body').append(html);
}



function TAdmin(pid) {
	$('#host_'+userIdglb).css('border', 'none');
	$('#BoxOverlay').hide();
	$('#confirmDiv').hide();
	var jsonText = '{"calltype":"' + callType + '","type":"hostswitch","callid":"' + callId + '","hostid":"' + pid + '","uid":"' + userIdglb + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
	user = "";
	whoAdmin = pid;
	hostUpdate(callId, pid);
	$("#showhost").hide();
	$(".addnewuser").hide();
	$(".userdisconnect").hide();
	var uName = $('div#userId_' + whoAdmin).find('.user_box_name').text();
	
	if($('.callConnectImage').attr('title') == uName){
		$('.callConnectImage').css("border", "4px solid #FFCA00");
	}
	else{
		$('.callConnectImage').css("border", "none");
	}
	$("div[id^=host_]").css("border", "none");
	$("#host_" + pid).css("border", "3px solid #FFCA00");
	$("div[id^=hostp_]").css("display", "none");
	$("#hostp_" + pid).css("display", "block");

	$("div[id^=hosta_]").css("display", "none");
	$("#hosta_" + pid).css("display", "block");
	$("div[id^=hostu_]").css("border", "none");
	$("#hostu_" + pid).css("border", "3px solid #FFCA00");
	$("div[id^=hostb_]").css("display", "none");
	$("#hostb_" + pid).css("display", "block");
	disableEnableWBoptions();
}

function stanzaofData() {

	var jsonText = '{"calltype":"' + callType + '","type":"stanzaofData","callid":"' + callId + '","stazaofMute":"' + stazaofMute + '","whiteboard":"' + whiteboardArray + '","screen":"' + screenshareArray + '"}';
	notify = $msg({ to: NameforChatRoom + '@conference.' + cmeDomain + '.colabus.com', "type": "groupchat", "roomType": "conference" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
	// console.log("sending stanza"+jsonText);
	$("div[id^=raudiopar_]").css("display", "none");
	$("div[id^=raudiotiles_]").css("display", "none");
	for (i = 0; i < stazaofMute.length; i++) {
		$("#raudiopar_" + stazaofMute[i]).show();
		$("#raudiotiles_" + stazaofMute[i]).show();
	}
	$("div[id^=sharescreen_]").css("display", "none");
	$("div[id^=sharescreenu_]").css("display", "none");
	$("#sharescreen_" + screenshareArray[0]).show();
	$("#whiteboard_" + whiteboardArray[0]).show();
}
$(window).resize(resizeChatCall);
function resizeChatCall() {
	var windowWidth = $(window).width();
	//console.log("ww-->"+windowWidth);

	//autogrowParticipants();
	if (windowWidth > 750) {
		$("#callText").css("width", "90%");
		$("#postimg").css("margin-top", "13px");
		$("#" + callId + "m").find("#chat-dialog").css("height", "58vh");
		if (typeof $("#checkArr").html() != "undefined") {
			if (($("#checkArr").attr('src').indexOf("uparr.svg")) > -1) {
				var css = $("#chatFor").find("#chat-dialog").attr('style')
				css += ';height:' + 8 + "vh !important";
				$("#chatFor").find("#chat-dialog").removeAttr('style');
				$("#chatFor").find("#chat-dialog").attr('style', css);
			}
			else {
				var css = $("#chatFor").find("#chat-dialog").attr('style')
				css += ';height:' + 60 + "vh !important";
				$("#chatFor").find("#chat-dialog").removeAttr('style');
				$("#chatFor").find("#chat-dialog").attr('style', css);
			}
		}

	}

	else {
		$("#callText").css("width", "87%");
		$("#postimg").css("margin-top", "10px");
		$("#" + callId + "m").find("#chat-dialog").css("height", "56vh");
		if (typeof $("#checkArr").html() != "undefined") {
			if (($("#checkArr").attr('src').indexOf("uparr.svg")) > -1) {
				var css = $("#chatFor").find("#chat-dialog").attr('style')
				css += ';height:' + 5 + "vh !important";
				$("#chatFor").find("#chat-dialog").removeAttr('style');
				$("#chatFor").find("#chat-dialog").attr('style', css);
			}
			else {
				var css = $("#chatFor").find("#chat-dialog").attr('style')
				css += ';height:' + 57 + "vh !important";
				$("#chatFor").find("#chat-dialog").removeAttr('style');
				$("#chatFor").find("#chat-dialog").attr('style', css);
			}
		}

	}
	if (windowWidth > 750 && windowWidth < 1365) {
		//console.log("donno");
		$("#" + callId + "m").find("#chat-dialog").css("height", "56vh");
		if (typeof $("#checkArr").html() != "undefined") {
			if (($("#checkArr").attr('src').indexOf("uparr.svg")) > -1) {
				var css = $("#chatFor").find("#chat-dialog").attr('style')
				css += ';height:' + 5 + "vh !important";
				$("#chatFor").find("#chat-dialog").removeAttr('style');
				$("#chatFor").find("#chat-dialog").attr('style', css);
			}
			else {
				var css = $("#chatFor").find("#chat-dialog").attr('style')
				css += ';height:' + 57 + "vh !important";
				$("#chatFor").find("#chat-dialog").removeAttr('style');
				$("#chatFor").find("#chat-dialog").attr('style', css);
			}
		}

	}


}


/*function setBandwidth(sdp) {
	alert(sdp);
	sdp = sdp.replace(/a=mid:audio\r\n/g, 'a=mid:audio\r\nb=AS:' + audioBandwidth + '\r\n');
	sdp = sdp.replace(/a=mid:video\r\n/g, 'a=mid:video\r\nb=AS:' + videoBandwidth + '\r\n');
	alert(sdp);
	return sdp;
}


function updateBandwidthRestriction(sdp, bandwidth) {
  let modifier = 'AS';
  if (adapter.browserDetails.browser === 'firefox') {
	bandwidth = (bandwidth >>> 0) * 1000;
	modifier = 'TIAS';
  }
  if (sdp.indexOf('b=' + modifier + ':') === -1) {
	// insert b= after c= line.
	sdp = sdp.replace(/c=IN (.*)\r\n/, 'c=IN $1\r\nb=' + modifier + ':' + bandwidth + '\r\n');
  } else {
	sdp = sdp.replace(new RegExp('b=' + modifier + ':.*\r\n'), 'b=' + modifier + ':' + bandwidth + '\r\n');
  }
  return sdp;
}*/


function setMediaBitrates(sdp) {
	return setMediaBitrate(setMediaBitrate(sdp, "video", 233), "audio", 100);
}

function setMediaBitrate(sdp, media, bitrate) {
	var lines = sdp.split("\n");
	var line = -1;
	for (var i = 0; i < lines.length; i++) {
		if (lines[i].indexOf("m=" + media) === 0) {
			line = i;
			break;
		}
	}
	if (line === -1) {
		//console.debug("Could not find the m line for", media);
		return sdp;
	}
	//console.debug("Found the m line for", media, "at line", line);

	// Pass the m line
	line++;

	// Skip i and c lines
	while (lines[line].indexOf("i=") === 0 || lines[line].indexOf("c=") === 0) {
		line++;
	}

	// If we're on a b line, replace it
	if (lines[line].indexOf("b") === 0) {
		//console.debug("Replaced b line at line", line);
		lines[line] = "b=AS:" + bitrate;
		return lines.join("\n");
	}

	// Add a new b line
	//console.debug("Adding new b line before line", line);
	var newLines = lines.slice(0, line)
	newLines.push("b=AS:" + bitrate)
	newLines = newLines.concat(lines.slice(line, lines.length))
	return newLines.join("\n")
}

function enterText(id) {
	// if(id == userIdglb){
	// 	$("#videoChatText").focus();
	// }
	// else{
		// $("#videoChatText").val('');
		$("#videoChatText").val("@" + $('div#userId_' + id).find('.user_box_name').text() + " " + $("#videoChatText").val());
		$("#videoChatText").trigger("blur");
		$("#videoChatText").focus();
		// $("#videoChatText").trigger("keyup");
		// commentAutoGrow("","","chat");

	// }
}

function Ascending_sort(a, b) {
	return ($(b).text().toUpperCase()) <
		($(a).text().toUpperCase()) ? 1 : -1;
}
function resizeIt() {
	$("#checkArr").attr('src', path + '/images/temp/uparr.svg');
	$("#" + callId + "mm").css("height", "85.2vh");
	$("#checkArr").css("margin-top", "-12px")
	$("#" + callId + "mm").parent().css("height", "85.2vh");
	var windoww = $(window).width();
	if (windoww > 750) {
		var css = $("#chatFor").find("#chat-dialog").attr('style')
		css += ';height:' + 8 + "vh !important";
		$("#chatFor").find("#chat-dialog").removeAttr('style');
		$("#chatFor").find("#chat-dialog").attr('style', css);
	}
	else {
		var css = $("#chatFor").find("#chat-dialog").attr('style')
		css += ';height:' + 5 + "vh !important";
		$("#chatFor").find("#chat-dialog").removeAttr('style');
		$("#chatFor").find("#chat-dialog").attr('style', css);
	}

	$("#checkArr").attr('onclick', 'backToNormal();');
}
function backToNormal() {
	$("#checkArr").attr('src', path + '/images/temp/downarr.svg');
	$("#" + callId + "mm").css("height", "33.2vh");
	$("#" + callId + "mm").parent().css("height", "33.2vh");
	$("#checkArr").css("margin-top", "12px")

	var windoww = $(window).width();
	if (windoww > 750) {
		var css = $("#chatFor").find("#chat-dialog").attr('style')
		css += ';height:' + 60 + "vh !important";
		$("#chatFor").find("#chat-dialog").removeAttr('style');
		$("#chatFor").find("#chat-dialog").attr('style', css);
	}
	else {
		var css = $("#chatFor").find("#chat-dialog").attr('style')
		css += ';height:' + 57 + "vh !important";
		$("#chatFor").find("#chat-dialog").removeAttr('style');
		$("#chatFor").find("#chat-dialog").attr('style', css);
	}
	$("#checkArr").attr('onclick', 'resizeIt();');
}


function uploadSpeed() {
	var imageAddr = "http://www.tranquilmusic.ca/images/cats/Cat2.JPG" + "?n=" + Math.random();
	var startTime, endTime;
	var downloadSize = 5616998;
	var download = new Image();
	startTime = (new Date()).getTime();
	download.src = imageAddr;
	download.onload = function () {
		endTime = (new Date()).getTime();
		var duration = (endTime - startTime) / 1000; //Math.round()
		var bitsLoaded = downloadSize * 8;
		var speedBps = (bitsLoaded / duration).toFixed(2);
		var speedKbps = (speedBps / 1024).toFixed(2);
		var speedMbps = (speedKbps / 1024).toFixed(2);
		if (callType == "A") {
			$("#rttspeedaudio").html(speedMbps + " Mbps");
		}
		else {
			$("#rttspeedvideo").html(speedMbps + " Mbps");
		}

	}
}
function showConnecting(id, uSer) {
	$("#RemoteDiv_" + id).remove();
	var imageSrc = $('#userId_' + id).find('img').attr('data-src');
	var userName = $('div#userId_' + id).find('.user_box_name').text();
	var Ui = "";
	// Ui += "<div id='connuser' class='' style='height:60px;padding-top: 7px;padding-bottom: 7px;width: 100%; background-color: rgba(113, 118, 126, 0.6);' > "
	// 	+ "<div class='col-xs-2' style='padding-top:0.5%'> <img src='" + imageSrc + "' onerror='userImageOnErrorReplace(this);' style='height: 34px; min-height: 45px; width:45px;min-width: 45px;border-radius:39px'/></div>"
	// 	+ "<div class='user_box_name col-xs-4' id='statuschange' style='font-family: OpenSansRegular;font-weight: bold;color:#fff; font-size: 14px; width: 50%; padding-top: 7px;padding-left: 20px;' value='" + userName + "'>" + userName + " is joining the call</div>"
	// if (uSer == "owner") {
	// 	Ui += "<div class='col-xs-2' onclick='cancelAudioVideoCall(" + id + ")' style='cursor:pointer;min-width: 35px;padding-top:11px;float: right;padding-right: 8px;padding-left: 0px;max-width: 13%;;'><img src='" + path + "/images/temp/audioRed.png'   style='width: 25px;height: 25px;float:right'/></div>"

	// }
	// Ui += "</div>";

	Ui += 
	'<div id="connuser" class="d-flex justify-content-end align-items-center" style="height:60px;width: 100%; background-color: rgba(113, 118, 126, 0.6);">'
		+'<div class="p-2 "><img src="' + imageSrc + '" title="'+name+'" onerror="userImageOnErrorReplace(this);" style="height: 34px; min-height: 45px; width:45px;min-width: 45px;border-radius:39px"></div>'
		+'<div class="p-2 user_box_name col-xs-4" id="statuschange" style="font-family: OpenSansRegular;font-weight: bold;color:#fff; font-size: 14px; width: 50%;" value="' + userName + '">' + userName + ' is joining the call</div>'
		if (uSer == "owner") {
			Ui +='<div class="p-2 " style="cursor:pointer;min-width: 35px;float: right;max-width: 13%;;"><img onclick="cancelAudioVideoCall(' + id + ')" src="images/cme/audioRed.svg"  style="width: 25px;height: 25px;float:right"></div>'
		}
	Ui +='</div>'
	$("#notification").html('').append(Ui);
	$("#notification").show(500);


}

//code for autojoin

function addTocall(callid) {
	var timeZone = getTimeOffset(new Date());
	var presentdate = globalPresentDate;
	//$("#loadingBar").show();
	//timerControl("start");
	if (callId == "" || typeof callId == "undefined") {
		var online_status = "";
		// if($('#'+id).hasClass('online') === true){
		// $.ajax({
		// 	url: path + "/ChatAuth",
		// 	type: "post",
		// 	data: { act: "getHost", userid: userIdglb, localTZone: timeZone, device: 'web', presentdate: presentdate, callid: callid, companyId: companyIdglb },
		// 	mimeType: "textPlain",
		// 	success: function (result) {
		let jsonbody = {
			"company_id": companyIdglb,
			"callId": callid
		}
		checksession();
		$.ajax({
			url: apiPath + "/" + myk + "/v1/getHost",
			// url: "http://localhost:8080/v1/getHost",
			type: "POST",
			dataType: 'json',
			contentType: "application/json",
			data: JSON.stringify(jsonbody),
			error: function (jqXHR, textStatus, errorThrown) {
				checkError(jqXHR, textStatus, errorThrown);
				timerControl("");
			},
			success: function (result) {
				console.log(result);
				checkSessionTimeOut(result);
				// if (result != "SESSION_TIMEOUT") {
					if (result != "") {
						
						var jsonData = result;
						if (jsonData[0].call_status == "inprogress") {
							var hostid = jsonData[0].host_id + "@@@" + callid;
							
							callConfirm(("Would you like to join the call ?"), "delete", "confirmJoin", "hideConfirmBox", "none", hostid);



						} else {
							$("#loadingBar").hide();
							timerControl("");
							videoAlertNotifn("Call is not yet started or already  ended.");
							showongoingCalls();
						}
					} else {
						$("#loadingBar").hide();
						timerControl("");
						videoAlertNotifn("Try again.");
						showongoingCalls();
					}



				// }
			}
		});

	} else {
		console.log('else block');
		if (callId == callid) {
			$("#loadingBar").hide();
			timerControl("");
			videoAlertNotifn("You are already a participant of this call.");
			chatBack('off');
		} else {
			$("#loadingBar").hide();
			timerControl("");
			videoAlertNotifn("You are in another call already.");
		}

	}

}


function addvideoTiles(tileId, userName, bgdisplay, displayprop1, dis, propm){
	var ui='';
	ui ='<div  class="item mt-2" id = "RemoteDiv_' + tileId + '" align="center" audiomute="" onhold="" videomute="" style="display:none;width: -webkit-fill-available;">'
			+ '<div id="bgTrans_' + tileId + '" class="" style="z-index: 1;position: absolute;opacity: 0.8;width: 100%;float: left;background-color: grey;height: 106px;display: ' + bgdisplay + ';">'

				+ '<div style="color: white;position: absolute;top: 34%;left: 30%;">Connecting...</div>'

			+ '</div>'
			// + ' <div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 180px;position:absolute;z-index:1000">'
			// + ' <div class="defaultWordBreak" style="width:150px;float:left;font-size: 12px; color: lightgrey; margin-top: 2px;    white-space: pre;">' + '  ' + userName + '</div>'
			// + '<div class="" style="width:30px;float:right;display:none" onclick="userDisconnect(' + tileId + ')" >'
			// 	+'<img src="' + path + '/images/cme/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/>'
			// +'</div>'
			// + '</div>'
			// +'<div id="tile_' + tileId + '" style="display: inline;position: relative;">'
			// 		// +'<div id="tileshow_' + tileId + '" class="flex-column rounded" style="display: none;z-index: 1;color: white;position: absolute;top: -12px;left: 14px;background-color: #316381;width: 200px;">'
			// 		// 	+'<div id="" class="d-flex justify-content-start pl-1">Host</div>'
			// 		// 	+'<div id="" class="d-flex align-items-center">'
			// 		// 		+'<div id="" class="mr-auto pl-1">' + userName + '</div>'
			// 		// 		+'<img class="px-2 cursor" title="Message" src="' + path + '/images/menus/conversation.svg" style="width: 32px;">' 
			// 		// 		+'<img class="userdisconnect cursor mr-2" onclick="userDisconnect(' + tileId + ')" title="Disconnect" src="' + path + '/images/temp/phonedis.svg" style="width: 18px;">' 
			// 		// 	+'</div>'
			// 		// +'</div>'
			// +'</div>'
			+ ' <div onmouseover="showTile(' + tileId + ');" onmouseleave="hideTile(' + tileId + ');">'
			+ ' <video id="localVideo_' + tileId + '" class="vcall1" ondblclick="enterText(' + tileId + ');"   onclick="switchVideo(' + tileId + ')" autoplay ' + propm + ' style="cursor:pointer;height: 100%; background: transparent;border-radius: 2px;border: none;"></video>'
			// + ' <div onmouseover="showTile(' + tileId + ');" onmouseleave="hideTile(' + tileId + ');" style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">'
			
				// + ' <div style="padding-top:12px;/*! display: none; */">'
					// + ' <div id="host_' + tileId + '" style="display:' + dis + ';"><img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  ></div>'
					+'<div style="position: relative;">'
					+ ' <span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display:' + displayprop1 + ';">'
					+ '<img class="vcall2" style="position: absolute;width:22px;" src="' + path + '/images/cme/mute.svg"></span>'
					if(tileId == userIdglb){
						ui+='<img id="sharescreenu_' + tileId + '" class="vcall3 cursor" onclick="" title="Screen Share" src="' + path + '/images/cme/screen2.svg" style="display: none;width: 22px;position: absolute;">' 
							+'<img id="whiteboardu_' + tileId + '" class="vcall4 cursor" onclick="" title="White Board" src="' + path + '/images/cme/whiteboard2.svg" style="display: none;width: 22px;position: absolute;">' 
					}
					else{
						ui+='<img id="sharescreen_' + tileId + '" class="vcall3 cursor" onclick="" title="Screen Share" src="' + path + '/images/cme/screen2.svg" style="display: none;width: 22px;position: absolute;">' 
						+'<img id="whiteboard_' + tileId + '" class="vcall4 cursor" onclick="" title="White Board" src="' + path + '/images/cme/whiteboard2.svg" style="display: none;width: 22px;position: absolute;">' 
					}
					+'</div>'

					// ui+='<div onmouseover="showTile(' + tileId + ');" onmouseleave="hideTile(' + tileId + ');"  >'
						// +'<img id="host_' + tileId + '" class=""  title="'+userName+'" src="' + imgurl + '" title="" style="height: 60px;cursor: pointer;border-radius: 50%;width: 60px;" onerror="userImageOnErrorReplace(this);" > ' 
						ui+='<div id="tile_' + tileId + '" style="display: inline;position: relative;">'
							// +'<div id="tileshow_' + tileId + '" class="flex-column rounded" style="display: none;z-index: 1;color: white;position: absolute;top: -12px;left: 14px;background-color: #316381;width: 200px;">'
							// 	+'<div id="" class="d-flex justify-content-start pl-1">Host</div>'
							// 	+'<div id="" class="d-flex align-items-center">'
							// 		+'<div id="" class="mr-auto pl-1">' + userName + '</div>'
							// 		+'<img class="px-2 cursor" title="Message" src="' + path + '/images/menus/conversation.svg" style="width: 32px;">' 
							// 		+'<img class="userdisconnect cursor mr-2" onclick="userDisconnect(' + tileId + ')" title="Disconnect" src="' + path + '/images/temp/phonedis.svg" style="width: 18px;">' 
							// 	+'</div>'
							// +'</div>'
						+'</div>'
					// +'</div>'
				// + '</div>'
				// + ' <div class="meterGraph" >'
				// 	+ '  <div class="meter" ></div>'
				// + ' </div>'
			+'</div></div>'

		return ui;
}

function addTiles(type, tileId, bgdisplay, displayprop1, imgurl, userName){
	var ui='';
	if(type=='2'){
		var dis = tileId;
		var border = '';
		if( dis == 'block'){
			border = '3px solid #FFCA00';
		}
		else{
			border = 'none';
		}
		var muteDisplay1 = bgdisplay;
		ui='<div class="item mx-auto mt-2" id = "RemoteDiv_' + userIdglb + '" screenshare="" audiomute="" onhold="" videomute="" style="width: -webkit-fill-available;">'+
				// '<div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 178px;position:absolute;z-index:1000">'+
				// 	'<div class="defaultWordBreak" style="white-space:pre;width:147px;float:left;text-align: initial;font-size: 12px; color: lightgrey; margin-top: 2px;">' + '  ' + userFullname + '</div>'+
				// 	'<div class="" style="width:30px;float:right;display:none"  onclick="hangup();" >'+
				// 		'<img src="' + path + '/images/temp/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/>'+
				// 	'</div>'+
				// '</div>'
				+'<video id="remoteAudio_' + userIdglb + '" ondblclick="enterText(' + userIdglb + ');" onclick="switchVideo(' + userIdglb + ')" autoplay  style="cursor:pointer;padding-left:5px;width: 80px;height: 46px ; background: transparent;    border: none;display:none"></video>'
				+ '<div  id="aRemoteImage_' + userIdglb + '" onclick="switchVideo(' + userIdglb + ')" style="border-style: hidden;" class="" align="center">'
					+ '<div  style="display: none;">'
						+ '<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  >'
					+ '</div>'
					+'<span id="raudiohost_' + userIdglb + '" title="Audio is muted" style="display:' + muteDisplay1 + ';">'+
							'<img style="margin-left: 47px;margin-top: 35px;width:21px;position: absolute;" src="' + path + '/images/cme/mute.svg">'+
					'</span>'
					+ '<img id="hostu_' + userIdglb + '" title="' + userFullname + '" src="' + imgurl + '" title="" style="border-style: hidden !important;border: '+border+';height: 60px;cursor: pointer;border-radius: 50%;width: 60px;" onerror="userImageOnErrorReplace(this);" > '
					// + '<div style="/*! display: none; */">'
						
					// '</div>'
				+ '</div>'+
				'<div style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">'
					// + '<div style="padding-top:12px;/*! display: none; */">'+
					// 	'<span id="raudiohost_' + userIdglb + '" title="Audio is muted" style="display:' + muteDisplay1 + ';margin-right: 51px;">'+
					// 		'<img style="    margin-top: 5px;width:18px" src="' + path + '/images/temp/mic2.png">'+
					// 	'</span>'+
					// '</div>'
					// + '<div class="meterGraph" ><div class="meter" ></div>'+
					// '</div>'
				+'</div></div>';
	}
	else{
		
	ui='<div class="item mx-auto mt-2" id = "RemoteDiv_' + tileId + '" screenshare="" audiomute="" onhold="" videomute="" style="width: -webkit-fill-available;">'
		if(type=='1'){
			ui +='<div id="bgTrans_' + tileId + '" class="" style="z-index: 1;position: absolute;opacity: 0.8;float: left;background-color: grey;height: 67px;left: 16px;display: ' + bgdisplay + ';">'
		}
		else{
			ui += '<div id="bgTrans_' + tileId + '" class="" style="z-index: 1;position: absolute;opacity: 0.8;float: left;background-color: grey;height: 67px;left: 16px;display: block;">'
		}
		ui += '<div style="color: white;position: absolute;top: 34%;left: 30%;">Connecting...</div>'
		+ '</div>'
		// + '<div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 178px;position:absolute;z-index:1000">'+
		// 	'<div class="defaultWordBreak" style="white-space:pre;width:147px;float:left;text-align: initial;font-size: 12px; color: lightgrey; margin-top: 2px;">' + '  ' + userName + '</div>'+
		// 	'<div class="" style="width:30px;float:right;display:none"  onclick="userDisconnect(' + tileId + ')" ><img src="' + path + '/images/temp/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/></div>'+
		// '</div>'
		+'<video id="remoteAudio_' + tileId + '" ondblclick="enterText(' + tileId + ');" onclick="switchVideo(' + tileId + ')" autoplay  style="cursor:pointer;padding-left:5px;width: 80px;height: 46px ; background: transparent;    border: none;display:none"></video>' +
		'<div  id="aRemoteImage_' + tileId + '" onclick="switchVideo(' + tileId + ')" style="border-style: hidden !important;" class="" align="center">' +
			'<div  style="display:none;">' +
				'<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  >' +
			'</div>' 
			if(type == '1'){
                ui+='<span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display:' + displayprop1 + ';">'

			}else{
				ui+='<span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display: none;">'
			}
			ui+='<img style="width:20px;margin-top: 38px;margin-left: 20px;position: absolute;" src="' + path + '/images/cme/mute.svg"></span>'
			if(tileId == userIdglb){
			ui+='<img id="sharescreenu_' + tileId + '" class="cursor" onclick="" title="Screen Share" src="' + path + '/images/cme/screen2.svg" style="display: none;width: 20px;position: absolute;margin-left: 12px;">' 
				+'<img id="whiteboardu_' + tileId + '" class="cursor" onclick="" title="White Board" src="' + path + '/images/cme/whiteboard2.svg" style="display: none;width: 20px;position: absolute;margin-left: 20px;margin-top: 19px;">' 
			}
			else{
			ui+='<img id="sharescreen_' + tileId + '" class="cursor" onclick="" title="Screen Share" src="' + path + '/images/cme/screen2.svg" style="display: none;width: 20px;position: absolute;margin-left: 12px;">' 
			+'<img id="whiteboard_' + tileId + '" class="cursor" onclick="" title="White Board" src="' + path + '/images/cme/whiteboard2.svg" style="display: none;width: 20px;position: absolute;margin-left: 20px;margin-top: 19px;">' 
			}
			ui+='<div onmouseover="showTile(' + tileId + ');" onmouseleave="hideTile(' + tileId + ');"  >'
				+'<img id="host_' + tileId + '" class=""  title="'+userName+'" src="' + imgurl + '" title="" style="border-style: hidden !important;height: 60px;cursor: pointer;border-radius: 50%;width: 60px;" onerror="userImageOnErrorReplace(this);" > ' 
				+'<div id="tile_' + tileId + '" style="display: inline;position: relative;">'
					// +'<div id="tileshow_' + tileId + '" class="flex-column rounded" style="display: none;z-index: 1;color: white;position: absolute;top: -12px;left: 14px;background-color: #316381;width: 200px;">'
					// 	+'<div id="" class="d-flex justify-content-start pl-1">Host</div>'
					// 	+'<div id="" class="d-flex align-items-center">'
					// 		+'<div id="" class="mr-auto pl-1">' + userName + '</div>'
					// 		+'<img class="px-2 cursor" title="Message" src="' + path + '/images/menus/conversation.svg" style="width: 32px;">' 
					// 		+'<img class="userdisconnect cursor mr-2" onclick="userDisconnect(' + tileId + ')" title="Disconnect" src="' + path + '/images/temp/phonedis.svg" style="width: 18px;">' 
					// 	+'</div>'
					// +'</div>'
				+'</div>'
			+'</div>'
		+'</div>'
		+'<div style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">' +
			// '<div style="padding-top:12px;/*! display: none; */">'
			// if(type == '1'){
            //     ui+='<span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display:' + displayprop1 + ';margin-right: 51px;">'

			// }else{
			// 	ui+='<span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display: none;margin-right: 51px;">'
			// }
			// ui+='<img style="width:18px ;   margin-top: 5px;" src="' + path + '/images/temp/mic2.png"></span>'+
			// '</div>' +
			// '<div class="meterGraph" ><div class="meter" ></div>'+
			// '</div>'
		'</div></div>';
	}

		return ui;
}

function showTile(id){
	$('#tileshow_' + id).addClass('d-flex');
}

function hideTile(id){
	$('#tileshow_' + id).removeClass('d-flex');
}

function AdduserAccepted(callid) {
	//alert(callid);
	createAnotherCall(callid, callType, 'ExUser');
	showConnecting(callid, user);
	if (callType == "A") {
		// alert(cType);
		if ($("#remote-audio").find("#RemoteDiv_" + callid).length == 0) {
			var tileId = callid;
			var imgurl = $('div#userId_' + tileId).find('img').attr('data-src');
			var userName = $('div#userId_' + tileId).find('.user_box_name').text();
			var ui = addTiles('', tileId,'','', imgurl, userName);
			$("#remote-audio").prepend(ui);


			
				// '<div class="item" id = "RemoteDiv_' + tileId + '" screenshare="" audiomute="" onhold="" videomute="" style="float: left;>'
				// 	+ '<div id="bgTrans_' + tileId + '" class="" style="z-index: 1;position: absolute;opacity: 0.8;float: left;background-color: grey;height: 137px;display: block;">'
				// 		+ '<div style="color: white;position: absolute;top: 34%;left: 30%;">Connecting...</div>'
				// 	+ '</div>'
				// 	// + '<div class="vName" style="color: white;height: 22px;top: 114px;background-color: rgba(113, 118, 126, 0.7);width: 178px;position:absolute;z-index:1000">'+
				// 	// 	'<div class="defaultWordBreak" style="white-space:pre;width:147px;float:left;text-align: initial;font-size: 12px; color: lightgrey; margin-top: 2px;">' + '  ' + userName + '</div>'+
				// 	// 	'<div class="" style="width:30px;float:right;display:none"  onclick="userDisconnect(' + tileId + ')" ><img src="' + path + '/images/temp/phonedis.svg"   style="width: 17px;height: 17px;float:right;margin:3px;cursor:pointer;"/></div>'+
				// 	// '</div>'
				// 	+'<video id="remoteAudio_' + tileId + '" ondblclick="enterText(' + tileId + ');" onclick="switchVideo(' + tileId + ')" autoplay  style="cursor:pointer;padding-left:5px;width: 180px;height: 135px ; background: transparent;    border: none;display:none"></video>' +
				// 	'<div  id="aRemoteImage_' + tileId + '" onclick="switchVideo(' + tileId + ')" style="margin-top: 1vh;margin-left:1vh;" class="" align="center">' +
				// 		'<div id="host_' + tileId + '" style="display:none;">' +
				// 			'<img class="Owned_by_cLabelTitle shareUserIcon" title="Host" src="' + path + '/images/temp/usernew.svg" projecttype="my_project" projectid="6031"  >' +
				// 		'</div>' +
				// 		'<img  title="' + userName + '" src="' + imgurl + '" title="" style="height: 100px;cursor: pointer;border-radius: 50%;width: 100px;min-width: 100px;" onerror="userImageOnErrorReplace(this);" > ' +
				// 	'</div>'+
				// 	'<div style="color: white;height: 22px;top: 0px;width: 178px;position:absolute;z-index:1000;">' +
				// 		'<div style="padding-top:12px;/*! display: none; */">'+
				// 			'<span id="raudiotiles_' + tileId + '" title="Audio is muted" style="display: none;margin-right: -8px;"><img style="width:18px ;   margin-top: 5px;" src="' + path + '/images/temp/mic2.png"></span>'+
				// 		'</div>' +
				// 		'<div class="meterGraph" ><div class="meter" ></div>'+
				// 		'</div>'+
				// 	'</div></div>');



		}
		var pcount = parseInt(tilePreparationArray.length);
		$(".pcnt").html(pcount);
		$("#host_" + whoAdmin).css("border", "3px solid #FFCA00");
		$("#host_" + userIdglb).css("border", "3px solid #FFCA00");
		tilesPopup();
	}
}
function declinereq(callid) {
	var hostid = callid;
	var jid = hostid + "@" + cmeDomain + ".colabus.com";
	var jsonText = '{"calltype":"' + callType + '","type":"deqExternalUser","callid":"' + callId + '","hostidrequest":"' + userIdglb + '"}';
	notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
	serverConnection.send(notify);
	if (queueArray.indexOf(hostid.toString()) > -1) {
		queueArray.splice(queueArray.indexOf(hostid.toString()), 1);
		handleQueue();
	}

}

function showongoingCalls() {
	/*var result='[{"callid":"121","hostid":"16","participants":"16,1433,5102"},{"callid":"122","hostid":"1433","participants":"16,1433,5102"}]';
	 prepareOngoingcallUI(result);*/
	var timeZone = getTimeOffset(new Date());
	var presentdate = globalPresentDate;
	$("#loadingBar").show();
	timerControl("start");
	var localOffsetTime = getTimeOffset(new Date());
	let jsonbody = {
		"company_id": companyIdglb
	}
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/getongoingcall",
		type: "POST",
		dataType: 'json',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			checkSessionTimeOut(result);
			// console.log("ongoing"+result);
			if (result.length != 0) {
				prepareOngoingcallUI(result);
			}
			else {
				$("#callhistory2").html("");
				// $("#callhistory2").append('<div style="text-align: center;font-size: 12px;padding: 8px 0px 9px 0px;">No Ongoing call</div>');
			}
		}
	});


	// $.ajax({
	// 	url: path + "/ChatAuth",
	// 	type: "post",
	// 	data: { act: "getongoingcall", userid: userIdglb, localTZone: timeZone, device: 'web', presentdate: presentdate, companyId: companyIdglb },
	// 	mimeType: "textPlain",
	// 	success: function (result) {
	// 		//console.log(result);
	// 		sessionTimeOutMethod(result);
	// 		if (result != "SESSION_TIMEOUT") {
	// 			if (result != "") {
	// 				prepareOngoingcallUI(result);
	// 			}
	// 			else {
	// 				$("#ongoingcallhistory").html("");
	// 				$("#ongoingcallhistory").append('<div style="text-align: center;font-size: 12px;padding: 8px 0px 9px 0px;">No Ongoing call</div>');
	// 			}

	// 		}
	// 	}
	// });
}
function prepareOngoingcallUI(result) {
	// console.log("prepareOngoingcallUI");
	$("#callhistory2").html('');

	var jsonData = result;
	var agileUi = "";
	for (i = 0; i < jsonData.length; i++) {
		var imgUrl = $('#userId_' + jsonData[i].host_id).find('img').attr('data-src');
		var style = "";
		var userName = "";
		var uList = jsonData[i].uList;

		if (uList.includes("],")) {
			uList = uList.split("],");
			var uId = uList[0].split(",")[0].split("[")[1];
			var uExt = uList[0].split(",")[2];
			// imgUrl = lighttpdpath+"/userimages/"+uId+"."+uExt+"?"+imgTime;
			// console.log("imgUrl.length---"+imgUrl);
			var calluser = uList[0].split(",")[1];
			for (j = 0; j < uList.length; j++) {
				var user = uList[j].split(",")[1];
				if (userName.indexOf(user) < 0) {
					userName = userName + " " + user + ", ";
				}
			}
			userName = userName.substring(0, userName.length - 2);
		} else {
			uList = uList.split("]");
			var uId = uList[0].split(",")[0].split("[")[1];
			var uExt = uList[0].split(",")[2];
			//imgUrl = lighttpdpath+"/userimages/"+uId+"."+uExt+"?"+imgTime;
			var calluser = uList[0].split(",")[1];
			userName = userName + " " + calluser + ", ";
			userName = userName.substring(0, userName.length - 2);
		}
		var callImg = "";
		if (jsonData[i].call_type == "A") {
			callImg = path + "/images/temp/Phonegreen.png";
			style = "width: 40px;height: 32px;";
		}
		else {
			callImg = path + "/images/temp/vedioGreen.png";
			style = "width: 35px;height: 25px;	margin-top: 5px;";

		}
		// agileUi += '<li ulist="' + jsonData[i].call_id + '" title="" lisid=' + jsonData[i].uIdList + ' id="ongoingcall_' + jsonData[i].call_id + '" style="float:left;width:99%;cursor:pointer;border-bottom: 1px solid #ced2d5; padding-bottom: 1%; padding-top: 0%;list-style-type:none;">'
		// 	+ '<div class="row conversation-contact" style="height: 70px; min-height: 70px;    margin-top: 0px;"><div class="col-xs-2">'
		// 		+ '<img src="' + imgUrl + '" onerror="userImageOnErrorReplace(this);"  class="img-square conversationContactImage" style="height: 59px; margin-left: -10px; margin-top: 4px; width: 59px; border-radius: 50%;"></div>'
		// 	+ '<div class="col-xs-8 callHistoryRow" style="height: 70px;padding-left: 30px;">'
		// 		+ '<div class="row" style="font-family: OpenSansItalic;visibility: hidden;">'
		// 			+ '<div class="col-xs-12 coversationContactTime" style="font-size: 10px; min-height: 20px; height: 20px;margin-top: 5px;">'
		// 				+ '<span><img style="height: 15px;width: 15px;margin-top: -8px;" src="/images/temp/incomingAC.png"></span>'
		// 		+ '<span style="padding-left:10px">Connected</span></div></div>'
		// 		+ '<div class="row" style="height: 20px;min-height: 20px;">'
		// 		+ '<div title=" ' + userName + '" class="col-xs-12  user_box_name conversationContactName defaultWordBreak" style="font-family: OpenSansRegular;margin: 4px 0px 0px 15px;height: 20px;font-size: 13px;padding-left: 0px;width:100%;margin-top: 0px;">' + userName + '</div></div>'
		// 		+ '<div class="row" style="font-family: OpenSansItalic;visibility: hidden;">'
		// 			+ '<div class="col-xs-12 coversationContactTime" style="font-size: 10px; min-height: 20px; height: 20px;">'
		// 	+ '<span style="">06:41 AM 2020-Nov-18</span></div></div></div>'
		// 	+ '<div class="col-xs-2 callHistoryDuration" style="margin-top: 17px;/* font-size: 10px; */padding-left: 7px;">'

		// 	// +'<button id="colabussignup" class="btn btn-info lang-signup" type="button" onclick="addTocall('+jsonData[i].call_id+')" style="width: 53px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;margin-left: -3px;" >Join</button>'

		// 	+ '<img src="' + callImg + '" onclick="addTocall(' + jsonData[i].call_id + ')" title="Join call" style="' + style + '">'

		// 	+ '</div></div></li>'


		agileUi += "<div ulist='" + jsonData[i].call_id + "' title='' lisid='" + jsonData[i].uIdList + "' id='ongoingcall_" + jsonData[i].call_id + "' class='media border border-left-0 border-right-0 p-0 roster-contact offline position-relative'  style=''> " +
						"<img src='" + imgUrl + "' title='" + userName + "' onerror='userImageOnErrorReplace(this);' class='userimage mr-3 mt-2 ml-2 rounded-circle mb-2 cursor' style='width:45px;'>" +
						// "<div class='user_box_status' style=''></div>" +
						"<div class='media-body'>" 
						+'<img  src="' + callImg + '" onclick="addTocall(' + jsonData[i].call_id + ')" title="Join call" class="image-fluid cursor audiocall mt-3" style="' + style + 'position: absolute;margin-left: 250px;">'
						+"<h6 class='user_box_name pt-4 pb-2 pname defaultExceedCls cursor' title='" + userName + "' value='" + userName + "' style='font-family: OpenSansRegular;font-size: 15px;width: 235px;'>" + userName + "</h6>" 
							// '<div id="contactfloatoptionsID" class=" mt-3 mr-2" style="">' 
								// +'<div class="d-flex align-items-center">'
								// +'<img src="/images/temp/vedioGreen.png" title="Video Call" onclick="event.stopPropagation();startCall('+user_id+', \'true\', \'video\');" id="" class="image-fluid cursor videocall" style="height: 25px;width:35px;">'
							// 	+'</div>'
							// +'</div>'
						+"</div>" 
						+"</div>"


			// agileUi +='<div id="contactfloatoptionsID" class=" mt-3 mr-2" style="">' 
			// 				+'<div class="d-flex align-items-center">'
			// 				+'<img  src="' + callImg + '" onclick="addTocall(' + jsonData[i].call_id + ')" title="Join call" class="image-fluid cursor audiocall" style="' + style + '">'
			// 				// +'<img src="/images/temp/vedioGreen.png" title="Video Call" onclick="event.stopPropagation();startCall('+user_id+', \'true\', \'video\');" id="" class="image-fluid cursor videocall" style="height: 25px;width:35px;">'
			// 				+'</div>'
			// 			+'</div>'
			// 		+"</div>"





	}
	//agileUi+='</div>'
	//$("#ongoingcallhistory").html("");
	$("#callhistory2").append(agileUi);
	// $("#loadingBar").hide();
	timerControl("");


}
function updateCallStatus(callid, status) {
	//alert(status);
	checksession()
	$.ajax({
		url: apiPath + "/" + myk + "/v1/updatecallstatus?callId="+callid+"&status="+status,
		type: "PUT",
		dataType: 'text',
		contentType: "application/json",
		// data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			// console.log(result);
			checkSessionTimeOut(result);
		}
	})

	// $.ajax({
	// 	url: path + "/ChatAuth",
	// 	type: "post",
	// 	data: { act: "updatecallstatus", callid: callid, status: status },
	// 	mimeType: "textPlain",
	// 	success: function (result) {
	// 		//console.log(result);
	// 		sessionTimeOutMethod(result);
	// 		if (result != "SESSION_TIMEOUT") {

	// 		}
	// 	}
	// });

}


function hostUpdate(callid, host_id) {
	//alert(status);
	// $.ajax({
	// 	url: path + "/ChatAuth",
	// 	type: "post",
	// 	data: { act: "hostUpdate", callid: callid, host_id: host_id },
	// 	mimeType: "textPlain",
	// 	success: function (result) {
	let jsonbody = {
		"callid" : callid,
		"host_id" : host_id
	}
	checksession();
	$.ajax({
		url: apiPath + "/" + myk + "/v1/hostUpdate",
		type: "PUT",
		dataType: 'text',
		contentType: "application/json",
		data: JSON.stringify(jsonbody),
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			//console.log(result);
			checkSessionTimeOut(result);
			// if (result != "SESSION_TIMEOUT") {

			// }
		}
	});

}

function confirmJoin(id) {
	var hostid = id.split("@@@")[0];
	var callid = id.split("@@@")[1];
	if ($('#userId_' + hostid).hasClass('online') === true) {
		var jid = hostid + "@" + cmeDomain + ".colabus.com";
		var jsonText = '{"calltype":"' + callType + '","type":"addExternalUser","callid":"' + callid + '","idrequest":"' + userIdglb + '"}';
		notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		serverConnection.send(notify);
		$('.callConnectImage').attr('src', $('div#userId_' + hostid).find('img').attr('data-src'));
		$('.callConnectImage').attr('title', $('div#userId_' + hostid).find('.user_box_name').text());
		$('.callConnectImage').css('border', '4px solid yellow');
		$('.callerScreenStatus').text("Connecting...");
		$('.callConnectText').text($('div#userId_' + hostid).find('.user_box_name').text());
		$('#callerScreen img.callHangUp').attr('onclick', 'cancelAudioVideoCall(' + hostid + ')');
		//$(".callConnectImage").attr('src',imgurl);
		pageReady();
		$("#host_" + hostid).css("border", "3px solid #FFCA00");
		var uName = $('div#userId_' + hostid).find('.user_box_name').text();
			if($('.callConnectImage').attr('title') == uName){
				$('.callConnectImage').css("border", "4px solid #FFCA00");
			}
			else{
				$('.callConnectImage').css("border", "none");
			}
		var imgCnt = 0;
		var tilesId = $("#ongoingcall_" + callid).attr('lisid');
		$(".callSubImage").css('visibility', 'hidden');
		if (typeof tilesId != "undefined" && tilesId != "" && tilesId != "null" && tilesId != null) {
			var prepareImg = tilesId.split(",");
			var imgCnt = 0;
			for (i = prepareImg.length - 1; i >= 0; i--) {
				imgCnt++;
				var tileId = prepareImg[i];
				var userName = $('div#userId_' + tileId).find('.user_box_name').text();
				var imgType = $('#userId_' + tileId).attr('imgtype');
				var imgUrl = lighttpdpath + "/userimages/" + tileId + "." + imgType;
				$("#callImageconnect_" + imgCnt).attr("title", userName).attr("src", imgUrl).css('visibility', 'visible');
				//alert(imgUrl);
				if (imgCnt == 2) {
					break;
				}
			}

		}
	} else {
		$("#loadingBar").hide();
		timerControl("");
		$('#videoContent #callerScreen #cmecallcontact').hide();
		$("#changescreen").show();
		$("#chatContent").show();
		videoAlertNotifn("Call is inactive.");
		updateCallStatus(callid, "closed");
		showongoingCalls();
	}


}
function handleQueue() {
	var name = $('div#userId_' + queueArray[0]).find('.user_box_name').text();
	if (queueArray.length > 0) {
		callConfirm(("Would you like to add " + name + " in the call ?"), "delete", "AdduserAccepted", "declinereq", "none", queueArray[0], queueArray[0]);
		var count = queueArray.length - 1;
		/*Ui += "<div id='connuser' class='' style='height:60px;padding-top: 7px;padding-bottom: 7px;width: 100%; background-color: rgba(113, 118, 126, 0.6);' > "
		   +"<div class='col-xs-2' style='padding-top:0.5%'> <img  onerror='userImageOnErrorReplace(this);' style='height: 34px; min-height: 45px; width:45px;min-width: 45px;border-radius:39px'/></div>"
		   +"<div class='user_box_name col-xs-4' id='statuschange' style='font-family: OpenSansRegular;font-weight: bold;color:#fff; font-size: 14px; width: 50%; padding-top: 7px;padding-left: 20px;' value=''>"+count+" peoples are in th queue.</div>"
		   +"</div>";
		 $("#notification").html('').append(Ui);
		 $("#notification").show(500);*/
	} else {
		$("#notification").html('');
		$("#notification").hide();
		// $("#notification").hide(500);
	}
}

function addConfUsers(){
	callConf="on";
	ongoingCall ="on";
	getAllConnectedUsersSpeed();
	addconfon=true;
	$("#transparentDiv").show();
	// var popUp = '';
	let ui = '';
	ui='<div class="SBactiveSprintlistCls" style="top:100px !important;" id="SBactiveSprintlistId" selectedstories="">'
		+'<div class="mx-auto w-50" style="max-width: 500px;">'
			+'<div class="modal-content container px-0" style="max-height: 500px !important;">'
				+'<div class="modal-header  p-2 pl-3 d-flex align-items-center" style="border-bottom: 1px solid #b4adad !important;">'
					+'<span id="contacts" class="modal-title" style="color:black;font-size: 16px !important;" onclick="contactPopup()">CONTACTS</span>'  
					// +'<span id="groups" class="modal-title cursor ml-4" style="font-size:14px;" onclick="groupPopup()">GROUPS</span>'
					// +'<span class="modal-title" style="width:20%;font-size:14px;">Start Date</span>'
					// +'<span class="modal-title" style="width:20%;font-size:14px;">End Date</span>'
					+'<input  id="searchText6" class="searchHeader border-0" style="width:86%;font-weight: normal;font-size: 13px;height: auto;padding: 3px 8px;"  placeholder="Search" onkeyup="searchContactsAV()">'
					+'<img id="contsearch2" src="images/menus/search2.svg" class="contactheader ml-auto mr-4 cursor" onclick="hideContact();" title="Search" style="width: 22px;">'
					// +'<img id="backcontact" src="images/cme/leftarrow_blue.svg" class=" ml-auto mr-4 cursor d-none" onclick="backContact()" title="Back"  style="width: 22px;">'
				+'</div>'
				+'<div class="" style="">'
					+'<button type="button" onclick="closeContact();event.stopPropagation();" class="close p-0 AgileCloseButton" style="top: 5px;right: 10px;color: black;outline: none;">&times;</button>'
				+'</div>'
				+'<div class="modal-body m-0 p-0" style="">'
						+'<div id="contactList" class=" p-0 wsScrollBar" style="font-size:12px;max-height: 400px;height: 400px;overflow: auto;"></div>'
						// +'<div id="groupList" class=" p-0 wsScrollBar d-none" style="font-size:12px;max-height: 400px;height: 400px;overflow: auto;"></div>'
				+'</div>'
			+'</div>'
		+'</div>'
	+'</div>'

	$('#cmePopup').html(ui).show();
	$('#cmecontent2').children().clone().appendTo('#contactList');
	if(callType=="AV"){
		$('#contactList').children().each(function () {
			$(this).attr('onclick', '');
			$(this).removeAttr('id');
			$(this).find('.audiocall').hide();
		});
	}
	else{
		$('#contactList').children().each(function () {
			$(this).attr('onclick', '');
			$(this).removeAttr('id');
			$(this).find('.videocall').hide();
		});
	}
	hideContact();
	// hideContact();

	// $('#searchText6').focus();

	// $('#cmecontent3').children().clone().appendTo('#groupList');
	// $('#groupList').children().each(function () {
	// 	$(this).attr('onclick', 'forwardMsgTo(this)');
	// 	$(this).removeAttr('id');
	// });
}

// function hideContact() {
// 	$('#contacts').addClass('d-none');
// 	$('#contsearch').addClass('d-none');
// 	$('#searchText5').removeClass('d-none');
// 	$('#backcontact').removeClass('d-none');
// 	$('#searchText5').focus();
// 	$('#groups').addClass('d-none');
// }

// function backContact() {
// 	$("#searchText5").val("").trigger("keyup");
// 	$('#contacts').removeClass('d-none');
// 	$('#contsearch').removeClass('d-none');
// 	$('#searchText5').addClass('d-none');
// 	$('#backcontact').addClass('d-none');
// 	$('#groups').removeClass('d-none');
// }


function searchContactsAV() {        // for search of users in single chat
	var txt = $('#searchText6').val().toLowerCase();

		$('#contactList').children().each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding
			var val = $(this).find('.user_box_name').attr('value').toLowerCase();
			if (val.indexOf(txt) != -1) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	
}




