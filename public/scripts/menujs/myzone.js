function loadMyzoneTabData(type){
	if(type == 'myNote'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}else if(type == 'myBlog'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}else if(type == 'myGallery'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}else if(type == 'myTask'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}else if(type == 'assignedTask'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}else if(type == 'historyTask'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}else if(type == 'myDocument'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}else if(type == 'myMessage'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}else if(type == 'myAllScrums'){
	   window.location.href = path+"/Redirect.do?pAct="+type; 
	}
}
 
var glbtaskType="";
var glbtabType="";
function loadTaskListViewData(taskType,tabType){ 
   glbtaskType=taskType;
   glbtabType=tabType;
   globVarClickPlace = "";
   flagView = "";
   $("body").css("overflow-x", "hidden");
   $("li.tabNameCls").each(function(i){
	   var id=this.id;
	   $('#'+id).removeClass("subtabActive");
	   $('#'+id).addClass("subtabInactive");
   });
   $('#newTaskUI').hide();
   $('#commonHtmlTaskUI').remove();
   $("#newTaskUI").css("height","");
   var topH = $("#tabMainDiv").innerHeight();
   $(".contentCls").css("top",topH);
   $('.newrowTab').hide();
   if(taskType == 'myTask'){
	  if(tabType == 'nTask'){
		window.location.href =path+"/Redirect.do?pAct="+taskType+"&taskSubTabType="+taskType;
	  }else{
		 $('#mytaskTab').removeClass("subtabInactive");
		 $('#mytaskTab').addClass("subtabActive");
		 $("#taskType").html("My Task");
		 loadMyTaskListViewData('firstClick');
	  }
   }else if(taskType == 'assignedTask'){
	  if(tabType == 'nTask'){
		window.location.href =path+"/Redirect.do?pAct="+taskType+"&taskSubTabType="+taskType;
	  }else{
		$('#assignedtaskTab').removeClass("subtabInactive");
		$('#assignedtaskTab').addClass("subtabActive");
		$("#taskType").html("Assigned");
		$(".newrowTab").hide();
		loadAssignedTaskListViewData('firstClick');
	  }  
	  
   }else if(taskType == 'historyTask'){
	   if(tabType == 'nTask'){
		 window.location.href =path+"/Redirect.do?pAct="+taskType+"&taskSubTabType="+taskType;
	   }else{
		  $('#historytaskTab').removeClass("subtabInactive");
		  $('#historytaskTab').addClass("subtabActive");
		  $("#taskType").html("History");
		  loadHistoryTaskListViewData('firstClick');
	   }	 
		   
   }else{
	   if(tabType == 'nTask'){
		 window.location.href =path+"/Redirect.do?pAct="+taskType+"&taskSubTabType="+tabType;
	   }else{
	   $('#mytaskTab').removeClass("subtabInactive");
	   $('#mytaskTab').addClass("subtabActive");
	   $("#taskType").html("My Task")
	   loadMyTaskListViewData('firstClick');
	   }
   }
 }
 
 

