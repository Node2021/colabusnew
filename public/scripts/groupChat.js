/**
 * @author Abhishek 
 * This js file is used to handle the Group chat functionality
 */

var jsonData='';
var NameforChatRoom='';
var jsonDataGroupUsers='';
var InstantGroupId='';
var IDforgroup = '';
var flag = true; 
var MUCID='';
var ImageUploadFromSystem = false;
var ImageIsUploaded = false;


/*    This is onload function but due to problem in connection we are calling it from chat.js after connecting to the server   */
function onloadGroup(){
	
	//Groupie.connection = $('#sound').data('conn'); // value set in chat.js
	//console.log('Groupie--------onload---'+Groupie.connection);
	//Groupie.nickname = nickName;
	 
	 $('.groupChat').click(function(){
		 
		// GetgroupChatRooms(); 
		//listOfConnectedRooms(); 
		/* $("div").animate({left: '250px'});*/
		 
		 $('#chat-frame,#chat-frame3').hide();
		 $('#chat-frame2').fadeIn(); 
		// ScrollChat('groupChat-box');
	 });
	
	 // single chat on click
     $('#singleChat').click(function(){
		 
		 $('#chat-frame').fadeIn();
		 $('#chat-frame2,#chat-frame3').hide(); 
		 ScrollChat('chat-box');
	 });

	 
	 $('#addGroup').click(function(){
		 groupCreationPopUp();
		 
	 });
	
	// Groupie.connection.addHandler(Groupie.on_public_message, null ,"message","groupchat" );
	 
	 
	 // search in group chat bar
	    $('#searchText2').on('keyup',function(){
		    var txt = $('#searchText2').val().toLowerCase();

			 if(txt.length>0){
				  $('#searchCancel2').show();
			 }else{
				  $('#searchCancel2').hide();
			 } 
			 $("#groupChat-box li").filter(function() {
				var text=  $(this).find('.cme_group_name').text().toLowerCase();
				
				if(text.indexOf(txt) != -1){
					 $(this).show();
				}else{
					 $(this).hide();
				}
			 });	  
   	 });

//				  
//	         $('#groupChat-box li').each(function(){
//	        	 console.log($(this));
//	        	 var val1 = $(this).find('.user_box_name').text().toLowerCase();
//	        	 var val = $(this).find('#groupChat-box li').text().toLowerCase();
//			      console.log("val=="+val);
//			      console.log("val1=="+val1);
//					 if(val.indexOf(txt)!= -1)
//			         {
//						 console.log("user_box_name");
//					   $(this).show();
//					 }else{
//					   $(this).hide();
//					 }
//			 });			
//		
//		 });	 
	 
	 $('#searchCancel2').on('click',function(){
		 $('#searchText2').val('');
		 $('#searchCancel2').hide();
		 $('#chat-box li').show();
		 $('#groupChat-box li').show();
	 });
	 
	 window.onbeforeunload = function () { // called when chat box will close
		 
		 /*** Below line of code- comment for CME alone and Uncomment for Colabus CME. **/
		 window.opener.releaseChatSession();
		 
		 disconnect();
		   // return "Do you really want to close+GroupChat?";
		}; 
}


var Groupie = {
		   connection : null,
		   room :  	'mucr.devchat.colabus.com',
		   nickname : '',
		   //NS_MUC : "http://jabber.org/protocol/muc",
		   joined : null,
		   participants : null,
		   
		   /*
		    * @ method is called when group message comes
		    */
	/*	   on_public_message : function(message){
		     var from = $(message).attr('from'); 
		     var room = Strophe.getBareJidFromJid(from);
		     var nick = Strophe.getResourceFromJid(from);
		     var Id = room.split('@')[0];
		     var id= Id+"m";
		     var d = new Date();	
		     var body = $(message).children('body').text();
		     console.log("Hi U got new group Message...first line of message");
		     console.log("Group message"+body);
	         var name = nick.split('_')[2];
	         var senderImgType = nick.split('_')[3];
	         var userid= nick.split('_')[1]; 
	      
	         
	         var Name = $('#groupChat-box ul').find('li#'+Id+' .user_box_name').text();
				
		     /*	if(!$('#chat-content').children('#'+Id+'m').is(':visible')){
			        var i = $('#'+Id).attr('count');
			            i = parseInt(i) + 1;   
					        $('#'+Id).attr('count',i);
					        $('#'+Id).find('.chatnotification').css('visibility','visible').html(i);
					        playSound('glass');
				}*/
				
				
		/*		
				if(!$('#'+id+' #chat-dialog').is(':visible')){
			       showPersonalConnectionMessages();
					        playSound('glass');
				}else{
		         resizeChatWindow();
	             //  getPreviousGroupMessages(Id);
	             //  updateSeenMessages(Id,userId);
		     	
		            var t=getTimeIn12Format(d);
				    if(("user_"+userId+"_"+userFullName+"_"+ userImgType ) != nick){
				    	 	
				    $('#'+id+' #chat-dialog').append(	
				        "<div class='chat-income-overall row' style='outline:none' tabindex='0'>"+
						"<div style='width:100%;float:left'>"+ 
						"<div class='chat-income-imageframe col-xs-1' style='margin-top: 23px;'>"+
						"<img  src='"+lighttpdPath+"/userimages/"+userid+"."+senderImgType+"'  onerror='userImageOnErrorReplace(this);' class='incomeframeImage' style='width: 40px; height: 38px; border-radius: 20px;margin-left:-7vh'/></div>"+ 
						"<div class='col-xs-10 chatIncomeBubble' style = 'padding-left: 0vh;margin-left:-1em' >"+
						"<div class='row' align='left' style='font-family: helvetica; font-size: 13px; width: 105%; padding-left: 34px;'>"+name+"</div>"+
						"<div class='chat-income-message bubble row'>"+body+
						"</div></div>"+
						"<div class='chat-income-time row' style='padding-left: 34px;'>"+t+"</div></div></div>");
				    		
				    	playSound('glass');
				 }
			
				 var h = $('#'+id).find('#chat-dialog')[0].scrollHeight;
				 $('#'+id).find('#chat-dialog').scrollTop(h);
				}
		     	
		     	$("#chat-dialog").mCustomScrollbar("update");
		     	
		     	 // This method is from a plug-in ifvisible.min.js which is imported in chat.jsp for more details check chat_desktop_notification.js
           /*    	
                if((!$('#'+id+' #chat-dialog').is(':visible'))||(ifvisible.now('hidden'))){
               	   window.opener.notifyMe(lighttpdPath+"/userimages/"+userid+"."+senderImgType , body);
               	}  */
              // showPersonalConnectionMessages();             
		     	
		   /*       return true;
			  
		   } */

		    // Not in use
		/*    on_presence : function(presence){
		     
		      var from = $(presence).attr('from');
		      var room = Strophe.getBareJidFromJid(from); 
		   
		}*/
		   
		 };



/*
 *@Method is used to show upload options from group creation and group edit popUp 
 */
function imageUploadDiv(){
	
	return "<div style=' bottom: 0px; position:absolute; left:10vh;' id='for_Group_ImageUpload'>"+
		      " <iframe id='upload_chat_image' name='upload_chat_image' src='' style='display:none'></iframe>"+
		      "<form action='' enctype='multipart/form-data' method='post' name='chatImgUploadForm' id='chatImgUploadForm' >"+					
					 "<label class='topicImageUpload' style='margin-top:-3px;width:160px;height:20px;position:absolute;display:none;'>" +
					 "<a style='color:#FFFFFF;margin-left:-5px;' href='#'></a>"	+	
					 "<input type='file' title='upload image'  onchange='readURL(this);' class='topic_file' value='' name='uploadchatImage' id='uploadchatImage'>" +			
					  "<input type='hidden' value='' name='chatUploadId' id='chatUploadId'>"+
					"</label>"+
				 "</form>"+
		
		     "<img id='chat_link_new' src='"+path+"/images/upload.png' class='projUploadIcon'>"+
		
		     "<div style='width: 170px; min-width: 100px; background-color: rgb(255, 255, 255); height: auto; z-index: 5010; border-radius: 5px; box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.6); border: 1px solid rgb(161, 161, 161); float: right; padding-left: 5px; display: none; position: absolute; top: 0%; left:35px; ' id='chat_Upload'>"+
			         "<div style='float:left;margin-left:-20px;margin-top:10px;position:absolute;'>"+
			           "<img src='"+path+"/images/arrowLeft.png' style='float:left;'>"+
			         "</div>"+
			         "<div style='height:28px' class='optionList'>"+
			          "<span style='margin-left:18px; margin-top: 5px;width:75%' class='OptionsFont Upload' id='uploadProjectImage1'>Upload from system</span>"+
			       "</div>"+
			         "<div onclick='getUserAlbums()' style='width:100%;height:23px;margin-bottom:3px;cursor:pointer;float:left'>"+
			           "<span style='margin-left: 18px; margin-top: 1px;width:75%' class='OptionsFont Attach_Link'>Upload from gallery</span>"+
			         "</div>"+
			      "</div>"+
		  "</div>";
}

/*
 * Method is used to get the image from system, to preview and to upload
 */
function readURL(input) {
	
    if (input.files && input.files[0]) {
        var reader = new FileReader();
   
        reader.onload = function (e) {
            $('#hideDiv1').attr('src', e.target.result);
           
        };
       ImageUploadFromSystem = true;
       reader.readAsDataURL(input.files[0]);
       $('#hideDiv1').data('file', input.files[0]);
       ImageIsUploaded = true;    
    }
}



/*this function Is used to create a pop up for group creation*/
function groupCreationPopUp(){
	//sessionStorage.chatBoxOnLogin1 = "login";
	$('#ideaTransparent').removeClass('ideaTransparent').addClass('transparentDivChat');
	if(!($('#groupUserPopUp').is(':visible'))){
		$("#loadingBar").show();	
		timerControl("start");
	  	$("#loadMsg").hide();
		
		
		var UI="<div class='modal-dialog row'  id='groupUserPopUp' style='width: 85%; z-index: 10000; font-family: OpenSansRegular;margin-top: 5%; margin-right: auto !important; margin-bottom: auto !important; margin-left: auto !important;'>"//position: fixed;top: 5%;left: 7%;
	  	    +"<div class='modal-content'>"
	                +"<div class='modal-header' style='padding:0.9vh'>"
	         	 +"<button type='button' class='close' onclick='closePopupGroup()' id='popupClose' data-dismiss='modal'>&times;</button>"
	         	 
	         	 +"<h4 id='groupHead'style='font-size: 14px; font-family: opensansregular; font-weight: bold;'>&nbsp;Create New Group</h4>"
			 +"<div style='margin: 0px; padding: 5px 0px;' id='backUsers'>"
	         	+"<div style='float:right;margin-right:-4px;cursor: pointer;'><img style='width:20px;height:20px'  class='img-responsive' src='"+path+"/images/back.png'/></div>" 
			 +"<div style='float:left;' ><h3  style='font-size: 14px; font-family: opensansregular; font-weight: bold; margin: 5px 0px 0px 0px;'>Users</h3></div>"	
			+"</div>"
	                +"</div>"
	            +"<div class='modal-body' style='padding-bottom: 0px;' >"
	         	+"<div class='row'>"
			+"<div class='col-sm-7'  id ='GroupParticipaintsContainer'>"
				+"<div class='row' style='margin-bottom: 3vh;'>"
				+"<div class='col-sm-3'>Group Name :</div>"
				+"<div class='col-sm-8'><input type='text' name='groupName' class='form-control' id='usr'> </div>"	 	
				+"</div>"	
				+"<div class='row' style='height: 9.7vh;margin-bottom: -1vh;'>"
					+"<div class='col-sm-3'>Group Image :</div>"
					+"<div class='col-xs-3' style='padding-right: 4vh;margin-top:1vh'><img class='img-responsive' id='hideDiv1' src='"+path+"/images/chat_groupiconRoom.png' style='height: 6vh; min-width: 6vh; width: 6vh; min-height:6vh;border-radius: 50%;'>"+imageUploadDiv()+"</div>"	 
				+"</div>"	
				+"<hr style='margin-bottom: 1vh;'>"
				+"<div class='row'>"
				+"<div class='col-xs-9' style='font-family: OpenSansRegular; font-size: 14px;'>Group Participants:</div>"
				+"<div class='col-xs-3'>"
				+"<img id='addGroupUsers' src='"+path+"/images/add.png' style='display:none;cursor:pointer;float:right'>"
				+"</div>"
				+"</div>"
				    +"<div class='row'>"
					+"<div class='col-sm-12' id='addedgroupUsers' style=''><ul class='popUpForGroup' style='margin-right:0px;height:32.4vh; overflow:auto;margin-left: -38px;position:relative;'></ul></div>"
					+"</div>"
			/*	+"<div class='row' style='border-top:1px solid'>"
				+"<button type='button' class='btn btn-primary' style='float: right; background-color: rgb(0, 191, 243); height: 30px; width: 72px; margin-top: 3%; margin-right: 2vh;'  onclick='createGroup();'>Create</button>"
				+"</div>"
					*/
			+"</div>"
			+"<div class='col-sm-5 ' id='groupUsersContainer' style='border-left: 1px solid #BFBFBF;'>"
				+"<div class='row' style='border-bottom: 1px solid #e5e5e5;padding-bottom: 3vh;'>"
				+"<div class='col-sm-6'>"
				+"<div class='form-group row'>"
					+"<select class='form-control' id='UserProject' onchange='getProjectUsers(this)' style='font-weight: normal; font-size: 13px; margin-left: -2vh;'>"
					+"</select>"
			    +"</div>"
				+"</div>"
				+"<div class='col-sm-6'>"
		+"<div class='row' style='padding-left:20px'>"	
		+"<div class='serachMainDivCls ' id='searchMainDiv' style='height: 33px; margin-top: 0vh; width: 100%;margin-left:-12px'>"
		+"<input type='text' placeholder='Search' id='searchGroupUser' class='serachBoxInputBoxCommonCls landingPage_Search_cLabelPlaceholder' style='font-weight:normal;font-size:13px; padding-top: 2%;'>"
		+"<div class='searchCloseIconMainDiv'>"
		+"<img title='Clear' id='taskCancel' src='"+path+"/images/xforsearch.png' class='serachBoxClearIcon Clear_cLabelTitle' style='float: right; margin-top: 11px; margin-right: 5px; cursor: pointer; display: none;'></div>"
		+"<div class='searchIconMainDiv' id='gSearchIcon' style='float:right;width:35px'><img src='"+path+"/images/searchTwo.png' class='serachBoxSearchIcon1' style='margin-left:9px;margin-top:8px;'>"
		+"</div>"
		+"</div>"
		+"</div>"		
				+"</div>"
			+"</div>"
	
				+"<div class='row' style='height:50vh;background-color:white;overflow-y:scroll;'>"
					+"<div class='col-sm-12' id='GroupUsers'  style='margin-left: -22px; overflow-x: hidden; padding: 0px 1vh 0px 0px;'><ul></ul>"
					+ "</div>"
					+ "</div>" 
				/*	+"<div class='row' style='width: 100%; height: 9%; float: left; margin-top: 3.4vh; margin-left: 1vh;'>" 
				    +"<div id='checkAll' class='removeAll' onclick='checkAllUsers(this);' style='float: left; width: 18px; height: 18px; cursor: pointer;'></div>" 
				    +"<div style='float: left; text-align: left; margin-left: 1%; margin-top: -0.5%;'>All</div>" 
				    +"<img style='float: right; cursor: pointer; margin-right: 4%; margin-top:-0.5vh;display: none;' name='addParticipants' src='"+path+"/images/add.png' title='Add Participants' onclick='prepareUIforAddGroup();' class='addParticipant' id='addGroupParticipants'>"
			   + "</div>" */
			+"</div>"
		+"</div>"
	        +"</div>"
	       +"<div class='modal-footer' style='padding: 1vh 2vh 2vh 1vh;'>"
			+"<div class='row' style='margin-top:2vh'>"
			+"<div class='col-sm-7'>"
				+"<button type='button' id='createGruoups' class='btn btn-primary' style='background-color: rgb(0, 191, 243); height: 30px; width: 72px; margin-top: -1%;'  onclick='createGroup();'>Create</button>"
			+"</div>"
			+"<div class='col-sm-5' id='groupUsersAll'>"
			+"<div style='width: 100%; height: 9%; float: left;  margin-bottom: -0.5vh;'>" 
		    +"<div id='checkAll' class='removeAll' onclick='checkAllUsers(this);' style='float: left; width: 18px; height: 18px; cursor: pointer;'></div>" 
		    +"<div style='float: left; text-align: left; margin-left: 1%; margin-top: -0.5%;'>All</div>" 
		    +"<img  style='float: right; cursor: pointer; margin-right: 4%; margin-top:-0.5vh;display: none;' name='addParticipants' src='"+path+"/images/add.png' title='Add Participants' onclick='prepareUIforAddGroup();' class='addParticipant' id='addGroupParticipants'>"
	   + "</div>" 
			+"</div>"	
		+"</div>" 
	        +"</div>" 
	     +" </div>"
		   +"</div>";
	    }
	//new ui closed
	$('#groupPopUp').html(UI);
	getInstantGroupId();
	$("#addGroupUsers").click(function() {
	    $("#groupUsersContainer").show();
	    $("#GroupParticipaintsContainer").hide();
	    $("#groupUsersAll").show();
	    $("#createGruoups").hide();
	    $("#groupHead").hide();
	    $("#backUsers").show();
	    $("#popupClose").attr("onclick","switchBackUser()");
		 
     });
	
	$("#backUsers").click(function() {
		$("#groupUsersContainer").hide();
	    $("#GroupParticipaintsContainer").show();
	    $("#groupUsersAll").hide();
	    $("#createGruoups").show();
	    $("#groupHead").show();
	    $("#backUsers").hide();
	    $("#popupClose").attr("onclick","closeEditPopup()");
	 });
	
	//loadGroupUsers();
	getAllProjects();
	getProjectUsers();
	$("#loadingBar").hide();	
	timerControl("");
	resizeChatWindow();
	 // for searching the Users in the popUp
	 $('#searchGroupUser').on('keyup',function(){
		    var txt = $('#searchGroupUser').val().toLowerCase();
		    if(txt.length > 0){
		    	$("#taskCancel").show(); 	 
		    	$('#GroupUsers li:visible').each(function(){
		    		var val = $(this).children().children(':eq(1)').text().toLowerCase();
				    if(val.indexOf(txt)!= -1){
						$(this).show();
					}else{
						$(this).hide();
					}
					$("#GroupUsers").mCustomScrollbar("update");	 
		    	});			
	       }else{
	    		$("#taskCancel").hide();
	    	   $('#GroupUsers li').show();
	    	   $('#addedgroupUsers li').each(function(){
	    		   var id = $(this).attr('id').split('part_')[1];
	    		   $('#GroupUsers ul').find('#'+id).hide();
	    	   });
	    	   $("#GroupUsers").mCustomScrollbar("update");
	       }
	});	
	 
	 // on click to cancel button
	$('#taskCancel').click(function(){
		 $('#searchGroupUser').val('');
		 $('#GroupUsers ul li').css('display','block');
		 
		 $('#addedgroupUsers li').each(function(){	   
	   		  var id = $(this).attr('id').split('part_')[1];
	   		  $('#GroupUsers ul').find('#'+id).hide();
	   	  });
		 
		 $("#GroupUsers").mCustomScrollbar("update");
		 $("#taskCancel").hide();
	}); 
	 
	 
	 
	 onclickFunctionForUploadImage();
}

/*
 * @Methods to handle image upload 
 */
function onclickFunctionForUploadImage(){
	//alert("inside1");
	 $("#chat_link_new").on('click', function(e){
	        e.preventDefault();
	        e.stopPropagation();
	        $("#chat_Upload").slideToggle("slow");
	        
	      //  $("#filename:hidden").trigger('click');
	        
	    });
	 
	 $("#uploadProjectImage1").on('click', function(e){
		 //alert("inside2");
	        e.preventDefault();
	        e.stopPropagation();       
	        $("#uploadchatImage:hidden").trigger('click');
	        
	    });
	    
	    
	    $('#filename:hidden').change(function(e){
	    	var upload = $('#filename').val();  
	    	$('#workspaceImgUploadForm').data('obj',this);
	        $('#hideDiv1').data('imgName',upload);
	        $('#Project_Upload').hide();
	    }); 
	 
	 /*   $('#groupUserPopUp').click(function(){
	    	
	        $("#chat_Upload").hide();
	    });*/
	
}


/*
 * @get Group Id for creation of group(called when group creation popup is opened)
 */
function getInstantGroupId(){
	   
    $.ajax({
	          url:path+"/ChatAuth",
			  type:"post",
	          data:{act:'createMUCRoom',ownerId:userId,groupType:"chat"},
	          mimeType: "textPlain",
	          success: function(result){
	        	  //checkSessionTimeOut(result);
	        	  InstantGroupId = result;
	        	  $('#groupUserPopUp').attr('groupId',result);
	        	  
	        	//  $("#loadingBar").hide();	
	  		    //  timerControl("");
			  }
	       });
	 
}

/*
 * @Deleteing the groupId if Group creation popUp is closed
 */
function closePopupGroup(){
	var mucId = $('#groupUserPopUp').attr('groupid');
	//console.log("mucID-----"+mucId);
	ImageUploadFromSystem = false;
	ImageIsUploaded = false;
	
	$.ajax({
		url: path+"/ChatAuth",
		type:"post",
		data:{act:"deleteMUCRoom",mucId:mucId},
		mimeType: "textPlain",
		success:function(result){
			//checkSessionTimeOut(result);	
		}
	});
	
	$('#groupPopUp').html('');
	$('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
}

/*
 * @Loading all the colabus users for group creation purpose.
 */
function loadGroupUsers(){
	
	 $.ajax({
    		url: path+"/connectAction.do",
			type:"POST",
			data:{act:"loadCompanyContacts",limit:'18446744073709551615',index:'0'},
			mimeType: "textPlain",
			success:function(result){
			    // checkSessionTimeOut(result);
			      result=result.split("~##@@##~")[0];			
					if(result =='[]'){
						
					}else{
						jsonDataGroupUsers=jQuery.parseJSON(result);
						result=prepareGroupUsersUI();
						$('#GroupUsers ul').html('').append(result);
						
						/*$("div#GroupUsers").mCustomScrollbar('destroy');
						
						$("div#GroupUsers").mCustomScrollbar({
							scrollButtons:{
								enable:true
							},
						});
						
						$('div#GroupUsers .mCSB_container').css('margin-right','15px');
				        $('div#GroupUsers .mCSB_scrollTools .mCSB_buttonUp').css({'background-position':'-80px 0px','opacity':'0.6'});
				    	$('div#GroupUsers .mCSB_scrollTools .mCSB_buttonDown').css({'background-position':'-80px -20px','opacity':'0.6'});
				    	$('div#GroupUsers .mCSB_scrollTools .mCSB_draggerRail').css('background','#B1B1B1');
				    	$('div#GroupUsers .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','#6C6C6C');
				        $('div#GroupUsers .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','#B1B1B1');
				        $('div#GroupUsers .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','#B1B1B1');  
				*/
					
					}
					 $("#loadingBar").hide();	
					 timerControl("");
					RemoveExistingUsersInEditMode();
					
			}
	 });
}

/*
 * @method is used to create Group(To get values from group creation popUp)
 */
var roleForGroup="";
function createGroup(){
	roleForGroup='';
	IDforgroup = '';
    var name = $('#groupUserPopUp').find('input[type=text]').val();
	
    if(name == ''){
        	parent.alertFun("Please Enter the group Name.",'warning');
    }else{  	
	    	$("#loadingBar").show();	
	    	timerControl("start");
	    	$("#loadMsg").hide();
    	
	    	if(ImageIsUploaded){  // checking weather image got uploaded or not
	    		var mucId = $('#groupUserPopUp').attr('groupId');
	        	ChangeGroupImage(mucId);
	    	}	    	
		    $('#addedgroupUsers ul li').each(function(){
		    	var id = $(this).attr('id').split('_')[2];
		    	var role = $(this).attr('role');
		    	roleForGroup+=role+",";
		    	IDforgroup +=id+",";		    	
		    });
		    		    
		    roleForGroup = roleForGroup.substring(0,roleForGroup.length-1);	
		    IDforgroup = IDforgroup.substring(0,IDforgroup.length-1);
		       addgroupChatRooms(name);		       
		       
  }
    
}

/*
 * @ Method is used to create chat room at the server.
 */ /*
function addgroupChatRooms(NameforChatRoom){
    $.ajax({
	          url:path+"/ChatAuth",
			  type:"post",
	          data:{act:'insertMUCRoomName',mucName:NameforChatRoom,mucId:InstantGroupId,ownerId:userId},
	          mimeType: "textPlain",
	          success: function(result){
	        	//  checkSessionTimeOut(result);
	        	//  alert(result);
	        	    
	        	  //Groupie.connection.send($pres().c('priority').t('-1'));
                  Groupie.connection.send(
	        		       $pres({
	        			      to: InstantGroupId+'@mucr.devchat.colabus.com' + "/" + "user_"+userId+"_"+userFullName +"_"+userImgType
	        			   }).c('x', {xmlns: Groupie.NS_MUC }));
	        	  
                iq = $iq({
                	    to: InstantGroupId+'@mucr.devchat.colabus.com',
                	    type: 'set'
                	}).c("query", {
                	    xmlns: Groupie.NS_MUC+"#owner"
                	});
                	iq.c("x", {
                	    xmlns: "jabber:x:data",
                	    type: "submit"
                	});
                	var ownereId = userId+'@mucr.devchat.colabus.com'
                	//send configuration you want
                	iq.c('field', { 'var': 'FORM_TYPE' }).c('value').t('http://jabber.org/protocol/muc#roomconfig').up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_roomname' }).c('value').t(NameforChatRoom).up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_roomdesc' }).c('value').t(NameforChatRoom).up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_enablelogging' }).c('value').t('0').up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_changesubject' }).c('value').t('0').up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_maxusers' }).c('value').t(0).up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_presencebroadcast' }).c('value').t('moderator').c('value').t('participant').up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_publicroom' }).c('value').t('1').up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_persistentroom' }).c('value').t('1').up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_whois' }).c('value').t('anyone').up().up();
                	iq.c('field', { 'var': 'muc#roomconfig_roomowners' }).c('value').t(ownereId).up().up();

                	Groupie.connection.sendIQ(iq.tree(), function () { console.log('success'); }, function (err) { console.log('error', err); });
 
                if(IDforgroup !=''){ // if additional Users Got added than add them in the database else Load the group name
	        	       AddUsersToGroups(NameforChatRoom);
                   }else{
                	   listOfConnectedRooms();
                	   //GetgroupChatRooms();
                	   $('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent'); // transparent Div
                	   $("#transparentDiv").hide();
                	   $('#groupPopUp').html(''); // hiding the Group creation popUp
                   }
			  }
	       });
}

/*
 * @Method is used to add Users To the Group
 */
function AddUsersToGroups(NameforChatRoom){
	 var mucId = $('#groupUserPopUp').attr('groupId'); 
	// alert("mucId:"+mucId) ;  
	 var listFlag=true;
	 if(typeof mucId =='undefined' || mucId =="null"){
	    mucId = InstantGroupId;
	    listFlag=false;
	 }
	 //alert("mucId:"+mucId) ;
	    
	 $.ajax({
         url:path+"/ChatAuth",
		  type:"post",
         data:{act:'updateMUCRoomUsers',mucId:mucId,mucName:NameforChatRoom,userId:userId,selMembers:IDforgroup,delMembers:'',roleForGroup:roleForGroup},
         mimeType: "textPlain",
         success: function(result){
        	// checkSessionTimeOut(result);
        	// alert("add user:"+result);
          $('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
       	  if(listFlag)
       	    listOfConnectedRooms();
       	  //GetgroupChatRooms();
		  }
      });
}

/*
 * @Method is used to change the Group Image
 */
function ChangeGroupImage(mucId){

  if(ImageUploadFromSystem){	// true if image is uploaded from the System
	
		var fData = new FormData();
		    fData.append('file' , $('#hideDiv1').data('file'));
		
		 $.ajax({
	         url:path+"/AppAuth?act=CommonUpload&gId="+mucId+"&type=chatGroupImg&mode=system",
			 type:"post",
	         data:fData,
	         mimeType: "textPlain",
	         processData: false,
			 contentType: false, 
			 cache: false,
	         success: function(result){
	        	// checkSessionTimeOut(result);
			  }
	      });
  
 }else{    // true if Image is uploaded from the Gallery
	 
	 $.ajax({
         url:path+"/AppAuth",
		 type:"post",
         data:{act:'CommonUpload',gId:mucId,type:'chatGroupImg',mode:'gallery',imgExt:fileExtension,photoId:uPhotoId,lovType:uDefaultType},
         mimeType: "textPlain",
         success: function(result){
        	// checkSessionTimeOut(result);
		  }
      });
	 
 }

}

/*
 * @Method is used for Listing of the Users
 */
function GetgroupChatRooms(){
    $.ajax({
	          url:path+"/ChatAuth",
			  type:"post",
	          data:{act:'getMUCRoomList',userId:userId},
	          mimeType: "textPlain",
	          success: function(result){
	        	 // checkSessionTimeOut(result);
	        	  //console.log("Groups Data:"+result);
				  jsonData = jQuery.parseJSON(result);
				  
				  ImageUploadFromSystem = false;
				  ImageIsUploaded = false;
				  prepareUI();
			  }
	       });
}

/*
 * Preparing the UI for Groups as well as sending the presence to all groups so that server can broadcast the messages
 */
function prepareUI(){
	var html='';
	var groupName='';
	var groupId='';
	var groupOwner='';
    var groupType='';
    var groupImgType='';
    var groupRole='';
    $('#groupChat-box ul').html('');
    var imageurl ="";
	if(jsonData.length>0){
		
		for(i=0; i< jsonData.length;i++){
	
			groupName = jsonData[i].groupName;	 
		    groupId  = jsonData[i].groupId;
			groupOwner = jsonData[i].groupOwner;
			groupRole = jsonData[i].groupRole;
			groupType = jsonData[i].groupType;
			groupImgType = jsonData[i].groupImageType;
			if(groupImgType =="null" || groupImgType =="" || groupImgType ==null){
				imageurl="";
			}else{
				imageurl = lighttpdPath+"/GroupChatImages/"+groupId+"."+groupImgType;
			}
	    	html="<li count='0'  style='float:left;width:99%;cursor:pointer;border-bottom: 1px solid #ced2d5;padding-top: 1%;' id='" + groupId + "' groupOwner='" + groupOwner + "' groupType='" + groupType + "' >" +
	             "<div class='roster-contact offline row' >" + 
				   "<div class='col-xs-3' style='width: 10%; height: 37px; padding-left: 8px;'> <img src='"+imageurl+"' title='"+ groupName +"' style='height: 34px; min-height: 34px; min-width: 34px;width:34px;border-radius:50%;' onerror='groupImageOnErrorReplace(this);'/></div>" +
			       "<div onclick='openNewGroupChatBox(this)' class='col-xs-6 cme_group_name' style='font-family: OpenSansRegular; font-size: 14px; margin: 6px 6px 6px -17px;margin-left:1px' >" + groupName +
			     "</div>";
			     
			   if(groupOwner == userId || groupRole=='admin' ){$("#backUsers").click(function() {
					$("#groupUsersContainer").hide();
				    $("#GroupParticipaintsContainer").show();
				    $("#groupUsersAll").hide();
				    $("#createGruoups").show();
				    $("#groupHead").show();
				    $("#backUsers").hide();
				 });
				   html+="<div class='col-xs-2 imgGroupedit'  onclick='openGroupEditSettings(this);' style='float: right;    text-align: right;'>"+
			       "<img id='setting_Group'  src='"+path+"/images/more.png' style='padding-top:4px;padding-right: 7px'>"+
				   "</div>";
			      }
			      html+= '</div>'				     
			    +"<div id= \"option_"+groupId+"\" class=\"actFeedOptionsDiv popup \" style=\"display:none;margin-right:3px;position: absolute; margin-top: -43px;\">"
				    +"<div style=\"margin-top: -2px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\">"
				        +"<img src=\""+path+"/images/arrow.png\">"
				    +"</div>";
				        
				 html+="<div id=\"rmhover\"  style=\"width:99%;height:auto;padding:0 0 3px ;cursor:pointer;float:left\"> "
				        +"<span class=\"\" id="+groupId+" onclick=\'openEditSettings(this);\' style=\"float: left; height: 20px;margin-left: 5px; overflow: hidden; width: 60px;\">Update</span>"
				      +"</div>";
				         
				 //UI+="<div onclick=\'deleteGroup("+mucId+")\' style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\">"
				 html+="<div  style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\">"
				         +"<span onclick=\'deleteGroup("+groupId+")\' style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Delete</span>"
				       +" </div>";
				      +" </div>";
	     
		    $('#groupChat-box ul').append(html);
		    
       	   // Groupie.connection.send($pres().c('priority').t('-1'));  // sending the presence stanza
          /*  Groupie.connection.send(
      		       $pres({
      			      to: groupId+'@mucr.devchat.colabus.com' + "/" + "user_"+userId+"_"+userFullName+"_"+ userImgType
      			   }).c('x', {xmlns: Groupie.NS_MUC })); */
      			// Gab.connection.send($pres().c('status').t('availble'));
			     //Gab.connection.send($pres());
			     
      			   
		}
		
		/* $("div#groupChat-box").mCustomScrollbar('destroy');
	     	$("div#groupChat-box").mCustomScrollbar({
	    		scrollButtons:{
	    			enable:true
	    		},
	    	});
	    	
	        $('div#groupChat-box .mCSB_container').css('margin-right','15px');
	    	$('div#groupChat-box .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
	    	$('div#groupChat-box .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
	    	$('div#groupChat-box .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
	    	$('div#groupChat-box .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
	    	$('div#groupChat-box .mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
	    	$('div#groupChat-box .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
	        $('div#groupChat-box .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
	        $('div#groupChat-box .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
*/
		  $("#loadingBar").hide();	
		  timerControl("");
		  $('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
		  $('#groupPopUp').html('');
	}

}

function groupImageOnErrorReplace(obj){
 	//$(obj).attr('src',path+"/images/chat_groupiconRoom.png");
	Initial(obj);
}

function Initial(obj){
	$(obj).initial({
		name:$(obj).attr("title"),
		height:64,
		width:64,
		charCount:2,
		textColor:"#fff",
		fontSize:30,
		fontWeight:700
	}); 
}

/*
 * @Method is Used for preparing the Users list
 */
function prepareGroupUsersUI(){
	var UI='';
	var UserId='';
	var name='';
	var UserImageType='';
	
	for(i= 0 ;i < jsonDataGroupUsers.length; i++){
		
		UserId= jsonDataGroupUsers[i].userId;
		name = jsonDataGroupUsers[i].Name;
		UserImageType= jsonDataGroupUsers[i].UserImageType;

		if(UserId == userId)
 		   continue;
		/*	
	UI += "<li style=\"height:80%; width:98%; cursor:pointer;float:left; padding-bottom: 7px;padding-top: 7px; border-bottom: 1px solid #BFBFBF;\" ondblclick='UIondblClick(this);' class='activeGroupUser' id=\"participant_"+UserId+"\">"
	  +"<img onerror=\"userImageOnErrorReplace(this);\" style=\"height:30px; width: 30px;border-radius:30px; float: left;\" src=\""+lighttpdPath+"/userimages/"+UserImageType+"\">"
	  +"<div style=\"float: left; color: rgb(132, 132, 132); overflow: hidden; margin-top: 2%; font-size: 13px; width: 68%; padding-left: 8%;\" id=\"participantName_"+UserId+"\">"+name+"</div>"
	  +"<div id=\"userUnCheck_"+UserId+"\" class='userUnCheck groupUser' onclick='userCheck(this)' style=\"width: 10%; height: 18px; margin-left: 1vh; float: left; margin-top: 4%;\"></div>"
	  +"</li>";
	} */
		UI += "<li style='height:80%; width:100%; cursor:pointer;float:left; padding-bottom: 7px;padding-top: 7px; border-bottom: 1px solid #BFBFBF;' ondblclick='UIondblClick(this);' class='activeGroupUser' id='participant_"+UserId+"'>"
		  +"<div class='row'>" 
		  +"<div class='col-xs-2'><img onerror='userImageOnErrorReplace(this);' title='"+name+"' style='height:30px; width: 30px;border-radius:50%; margin-left:5px;float: left;' src='"+lighttpdPath+"/userimages/"+UserImageType+"'></div>"
		  +"<div class='col-xs-8' style='float: left; color: rgb(132, 132, 132); margin-top: 2%; font-size: 13px;' id='participantName_"+UserId+"'>"+name+"</div>"
		  +"<div id='userUnCheck_"+UserId+"' class='col-xs-2 userUnCheck groupUser' onclick='userCheck(this)' style='height: 18px; margin-right:5px; margin-top: 4%; width: 10%; float: right;'></div>"
		  +"</div>"
		  +"</li>";
	}
	return UI;
}

/*
 * @Used to check and Uncheck the Users
 */
function userCheck(obj){
	$(obj).toggleClass('userCheck').toggleClass('userUnCheck');
	
	if($('#GroupUsers ul li').find('div.groupUser').hasClass('userCheck'))
		$('#addGroupParticipants').css('display','block');
	else
		$('#addGroupParticipants').css('display','none');
		
}

/*
 * @Method is used to check all the users
 */
function checkAllUsers(obj){
	
	$(obj).toggleClass('removeAll').toggleClass('checkAll');
	
	if($(obj).hasClass('removeAll')){
	    $('#GroupUsers ul li').find('div.groupUser').removeClass('userCheck').addClass('userUnCheck');
	    $('#addGroupParticipants').css('display','none');
	  }else{
		$('#GroupUsers ul li').find('div.groupUser').removeClass('userUnCheck').addClass('userCheck');
		$('#addGroupParticipants').css('display','block');
      }
}


/*
 * @Method is used to prepare UI for the added Users
 */
function prepareUIforAddGroup(){
	
	$('li.activeGroupUser').children().find('div.groupUser').each(function(){
		if($(this).hasClass('userCheck') && ($(this).parent().css('display') != 'none')){
			var name=$(this).prev().text();
			var id = $(this).parent().parent().attr('id');
			$('#'+id).hide();
			$('#'+id).children().children().next().removeClass('userCheck').addClass('userUnCheck');	
			var Id = id.split("_")[1];
		
			var srcpath = $("#participant_"+Id).find('img').attr("src");
			/* UI  = "<li id='part_"+id+"' role='N' class='chatUserCls' >"
	  		   +"<div title='"+name+"' style='width: auto; font-size: 9px; margin-top: 0.2em; float: left;'>"+name+"</div>"
	  		   +"<div style='float: left; width: auto; margin-right: 10px; margin-left: 6px;'><img Id='"+id+"' onclick='removeGroupParticipant(this);' src='"+path+"/images/close.png' style='width: 8px; height: 8px; cursor: pointer;margin-top:4px'></div>"
	  		   +"</li>";*/
			UI  = "<li id='part_"+id+"' role='user' class='chatUserCls' style='margin-left: 0px;border-radius: 0px;width:100%;height:33px'>"
	     			+"<div title='"+name+"' style='width: auto; font-size: 12px; margin-top: 4px; float: left;'><img src='"+srcpath+"' onerror='groupImageOnErrorReplace(this);' style='width: 25px; height: 25px; cursor: pointer;margin-top:-4px;border-radius: 50%;'>&nbsp&nbsp"+name+"</div>"
	     			+"<div class='imgGroupedit' style='float: right; width: auto; margin-right: 5px; margin-left: 6px;margin-top: 4px;position:relative;'>"
	     				+"<div id= \"option_"+id+"\" class=\"actFeedOptionsDiv newpopup \" style=\"width:119px;display:none; margin-right: -5px; margin-top: -14px;\">"
	     					+"<div style=\"margin-top: -2px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\">"
	     						+"<img src=\""+path+"/images/arrow.png\">"
	     					+"</div>"
			        
	     					+"<div id=\"rmhover\"  style=\"width:99%;height:auto;padding:0 0 3px ;cursor:pointer;float:left\"> "
	     						+"<span class=\"\" Id='"+id+"' onclick='addRoleToParticipants(this);' style=\"float: left; height: 20px;margin-left: 5px; overflow: visible; width: 111px;\">Mark as Admin</span>"
	     					+"</div>"
			         
			 //UI+="<div onclick=\'deleteGroup("+mucId+")\' style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\">"
	     					+"<div  style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\">"
	     						+"<span Id='"+id+"' onclick='removeGroupParticipant(this);' style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Delete</span>"
	     					+" </div>"
	     				+" </div>"
	     				+" <img  Id='"+id+"' onclick='showMore("+Id+")'  src='"+path+"/images/more.png' style='margin-left: 10px; height: 19px; cursor: pointer;'>"
			      +" </div>"
			  +"</li>";
	    
		/* UI="<li style='width: 35%;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:2%;margin-bottom:2%;' id='part_"+id+"'>" +
			"<div title='"+name+"' style='float: left;padding-left: 5%; width: 80%;text-overflow: ellipsis;white-space: nowrap; overflow:hidden;font-size:12px; '>"+name+"</div>" +
			"<img onclick='removeGroupParticipant(this);' Id='"+id+"' style='float: right;margin:6px 5px 0px 10px;cursor:pointer;' class='Delete_cLabelTitle' src='"+path+"/images/calender/emailDel.png' title='Delete'>" +
	        "</li>";*/
			
		 $('div#addedgroupUsers ul').append(UI);
		 $('#addGroupParticipants').hide();
		 
		}	
	});
	
	if($('#GroupUsers ul ').find('li:visible').length == 0){
		//alert('removeAll');
		$('#checkAll').removeClass('checkAll').addClass('removeAll');
	}
	 $("#GroupUsers").mCustomScrollbar("update");
	 
	/* $("div#addedgroupUsers").mCustomScrollbar('destroy');
	 $("div#addedgroupUsers").mCustomScrollbar({
			scrollButtons:{
				enable:true
			},
		});
		
		$('div#addedgroupUsers .mCSB_container').css('margin-left','-39px');
        $('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonUp').css({'background-position':'-80px 0px','opacity':'0.6'});
    	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonDown').css({'background-position':'-80px -20px','opacity':'0.6'});
    	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_draggerRail').css('background','#B1B1B1');
    	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','#6C6C6C');
        $('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','#B1B1B1');
        $('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','#B1B1B1');  
*/
	  /*  $('div#addedgroupUsers .mCSB_container').css('margin-right','15px');
		$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
		$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
		$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
		$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
		$('div#addedgroupUsers .mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
		$('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
	    $('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
        $('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
*/
        
        resizeChatWindow();
        
}

/*
 * To remove the added group users
 */
function removeGroupParticipant(obj){
	
	 var id = $(obj).parent().attr('id');
	 var Id = $(obj).attr('Id');
	 var newId = Id.split('_')[1];
	
	 $("#option_participant_"+newId).remove();
	 $('div#addedgroupUsers ul').children('#part_'+Id).remove();
	 $('#userUnCheck_'+newId).removeClass('userCheck').addClass('userUnCheck');
	 $('li#'+Id).show();	
	 $("#GroupUsers").mCustomScrollbar("update");
	 //$("#addedgroupUsers").mCustomScrollbar("update");
	 
}

function addRoleToParticipants(obj){
	
	 var id = $(obj).parent().attr('id');
	 var Id = $(obj).attr('Id');
	 var newId = Id.split('_')[1];
	 $("#option_participant_"+newId).hide();
	 $('div#addedgroupUsers ul').children('#part_'+Id).attr('role','admin');
}

/*
 * For adding the users from the users list
 */
function UIondblClick(obj){	
	var Id =$(obj).attr('id').split('_')[1];
	var name=$(obj).children().children('#participantName_'+Id).text();
	var srcpath = $(obj).find('img').attr("src");
	
    var id = $(obj).attr('id');
    $('#'+id).hide();

     UI  = "<li id='part_"+id+"' role='user' class='chatUserCls' style='margin-left: 0px;border-radius: 0px;width:100%;height:33px'>"
     	 +"<div title='"+name+"' style='width: auto; font-size: 12px; margin-top: 4px; float: left;'><img src='"+srcpath+"' onerror='groupImageOnErrorReplace(this);' style='width: 25px; height: 25px; cursor: pointer;margin-top:-4px;border-radius: 50%;'>&nbsp&nbsp"+name+"</div>"
     	 	+"<div class='imgGroupedit' style='float: right; width: auto; margin-right: 5px; margin-left: 6px;margin-top: 4px;position:relative;'>"
     	 		+"<div id= \"option_"+id+"\" class=\"actFeedOptionsDiv newpopup \" style=\"width:119px;display:none; margin-right: -5px; margin-top: -14px;\">"
     	 			+"<div style=\"margin-top: -2px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\">"
     	 				+"<img src=\""+path+"/images/arrow.png\">"
     	 			+"</div>"
	        
     	 			+"<div id=\"rmhover\"  style=\"width:99%;height:auto;padding:0 0 3px ;cursor:pointer;float:left\"> "
     	 				+"<span class=\"\" Id='"+id+"' onclick='addRoleToParticipants(this);' style=\"float: left; height: 20px;margin-left: 5px; overflow: visible; width: 111px;\">Mark as Admin</span>"
     	 			+"</div>"
	         
	 //UI+="<div onclick=\'deleteGroup("+mucId+")\' style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\">"
     	 			+"<div  style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\">"
     	 				+"<span Id='"+id+"' onclick='removeGroupParticipant(this);' style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Delete</span>"
     	 			+" </div>"
     	 		+" </div>"
     	 		+"<img Id='"+id+"' onclick='showMore("+Id+")'  src='"+path+"/images/more.png' style='margin-left: 10px; height: 19px; cursor: pointer;'>"
	      +"</div>"
	   +"</li>";
     
	/* UI="<li style='width: 35%;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:2%;margin-bottom:2%;margin-top: 1%;' id='part_"+id+"'>" +
		"<div title='"+name+"' style='float: left;padding-left: 5%; width: 80%;text-overflow: ellipsis;white-space: nowrap; overflow:hidden;font-size:12px; '>"+name+"</div>" +
		"<img onclick='removeGroupParticipant(this);' Id='"+id+"' style='float: right;margin:6px 5px 0px 10px;cursor:pointer;' class='Delete_cLabelTitle' src='"+path+"/images/calender/emailDel.png' title='Delete'>" +
	    "</li>";*/
	
 $('div#addedgroupUsers ul').append(UI);
 

/* $("div#addedgroupUsers").mCustomScrollbar('destroy');
 $("div#addedgroupUsers").mCustomScrollbar({
		scrollButtons:{
			enable:true
		},
	});
	
    $('div#addedgroupUsers .mCSB_container').css('margin-left','-39px');
	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
    $('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
 $('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
*/
}

/*
 *@Method is used to get all the projects available in projects 
 */
/*function getAllProjects(){  //------>used in taskuicodenew.js
	
	$.ajax({
			url:path+"/calenderAction.do",
			type:"POST",
			data:{act:"fetchAllProjects"},
			mimeType: "textPlain",
			success:function(result){
				//checkSessionTimeOut(result);
				//console.log(result);
			var projDetails = result;
			var project = projDetails.replace("CHR(39)","'").replace("CHR(40)",'"').replace("CHR(50)",'\n').replace("CH(52)","'").split('##@##');
			var projects="<option id=\"0\" class=\"drpDwnOptions Select\" value=\"select\" >select</option>";
			
			$('#UserProject').append(projects);
			
			for (var i=0;i<project.length-1;i++){ 
				var pro =  project[i].split('^&*');
				var projName =pro[0];
				var projId = pro[1];
				projects ="<option id=\""+projId+"\" class=\"drpDwnOptions\" value=\""+projName+"\" >"+projName+"</option>";
				$('#UserProject').append(projects);
			}
			$('#UserProject').append("<option id=\"allUsers\" class=\"drpDwnOptions All_Users\" selected=\"selected\" value=\"All_Users\">All Users</option>");
		
			//$('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
			
			
			}
		
	   });
}*/

/*
 * @For getting the project users on select
 */
function getProjectUsers(){
	
	var jsonData='';
	var selectedName = $('#UserProject').find(':selected').text();
	var UI='';
	var UserId='';
	var name='';
	var UserImageType='';
	
	//if(selectedName != 'All Users'){
		var projId = $('#UserProject').find(':selected').attr('id');
		$.ajax({ 
		       url: path+'/connectAction.do',
		       type:"POST",
		       data:{act:'fetchParticipantsJSON',projId:projId},
		       mimeType: "textPlain",
		       success:function(result){
		      // alert(result);
		    	 //  checkSessionTimeOut(result);
		    	   jsonData = $.parseJSON(result);
		    	  // UI = prepareGroupUsersUI();
	    		/*for(i= 0 ;i < jsonData.length; i++){
	    			
	    			UserId= jsonData[i].userId;
	    			name = jsonData[i].userName;
	    			UserImageType= jsonData[i].userImg;

		    			if(UserId == userId)
		    	 		   continue;
		    		showProjectPop	
		    	   UI += "<li style=\"height:80%; width: 98%;cursor:pointer;float:left; padding-bottom: 7px;padding-top: 7px; border-bottom: 1px solid #BFBFBF;\" ondblclick='UIondblClick(this);' class='activeGroupUser' id=\"participant_"+UserId+"\">"
		    		  +"<img onerror=\"userImageOnErrorReplace(this);\" style=\"height:30px; width: 30px;border-radius:30px; float: left;\" src=\""+UserImageType+"\">"
		    		  +"<div style=\"width: 170px;float:left;color: #848484;overflow: hidden;padding-left: 5px;margin-top: 7px;font-size:14px;\" id=\"participantName_"+UserId+"\">"+name+"</div>"
		    		  +"<div id=\"userUnCheck_"+UserId+"\" class='userUnCheck groupUser' onclick='userCheck(this)' style=\"float: left;width:30px;height: 18px;margin-top: 6px;\"></div>"
		    		  +"</li>";
		    		}  */
		    	   
		    	   for(i= 0 ;i < jsonData.length; i++){
		    			
		    			UserId= jsonData[i].userId;
		    			name = jsonData[i].userName;
		    			UserImageType= jsonData[i].userImg;

		    			if(UserId == userId)
		    	 		   continue;
		    			
		    	/*	UI += "<li style='height:80%; width:100%; cursor:pointer;float:left; padding-bottom: 7px;padding-top: 7px; border-bottom: 1px solid #BFBFBF;' ondblclick='UIondblClick(this);' class='activeGroupUser' id='participant_"+UserId+"'>"
		    		  +"<img onerror='userImageOnErrorReplace(this);' style='height:30px; width: 30px;border-radius:30px; float: left;' src='"+lighttpdPath+"userimages'"+UserImageType+"'></div>"
		    		  +"<div style='float: left; color: rgb(132, 132, 132); overflow: hidden; margin-top: 2%; font-size: 13px; width: 68%; padding-left: 8%;' id='participantName_"+UserId+"'>"+name+"</div>"
		    		  +"<div id='userUnCheck_"+UserId+" ' class='userUnCheck groupUser' onclick='userCheck(this)' style='width: 10%; height: 18px; margin-left: 1vh; float: left; margin-top: 4%;'></div>"		    		 
		    		  +"</li>";
		    		*/
		    		
		    		UI+= "<li style='height:80%; width:100%; cursor:pointer;float:left; padding-bottom: 7px;padding-top: 7px; border-bottom: 1px solid #BFBFBF;' ondblclick='UIondblClick(this);' class='activeGroupUser' id='participant_"+UserId+"'>"
			  		  +"<div class='row'>" 
			  		  +"<div class='col-xs-2'><img onerror='userImageOnErrorReplace(this);' title='"+name+"' style='height:30px; width: 30px;border-radius:50%; margin-left:5px;float: left;' src='"+UserImageType+"'></div>"
			  		  +"<div class='col-xs-8' style='float: left; color: rgb(132, 132, 132);margin-top: 2%; font-size: 13px;' id='participantName_"+UserId+"'>"+name+"</div>"
			  		  +"<div id='userUnCheck_"+UserId+"' class='col-xs-2 userUnCheck groupUser' onclick='userCheck(this)' style='height: 18px; margin-right:5px; margin-top: 4%; width: 10%; float: right;'></div>"
			  		  +"</div>"
			  		  +"</li>";
		    		
		    		
		    		}
		    	   
		    	   
	    		$('#GroupUsers ul').html('').append(UI);
	    		 
	    		/*$("div#GroupUsers").mCustomScrollbar({
					scrollButtons:{
						enable:true
					},
				});
				
			    $('div#GroupUsers .mCSB_container').css('margin-right','15px');
				$('div#GroupUsers .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
				$('div#GroupUsers .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
				$('div#GroupUsers .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
				$('div#GroupUsers .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
				$('div#GroupUsers .mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
				$('div#GroupUsers .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
			    $('div#GroupUsers .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
	            $('div#GroupUsers .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
	       		*/
	    		
	    		$('#addedgroupUsers li').each(function(){
	    			   
		    		  var id = $(this).attr('id').split('part_')[1];
		    		  $('#GroupUsers ul').find('#'+id).hide();
		    		  $("#GroupUsers").mCustomScrollbar("update");
		    	   }); 
	    		
	    		$("#GroupUsers").mCustomScrollbar("update"); 
	    		
		    	
		       }
		       
		       
		 });
    //}else{
    //	loadGroupUsers();
    //}
}

/*
 * @Opening the respective chatbox on click of particular users
 */
function openNewGroupChatBox(obj){
	restoreValue();
	var id= obj.id; 
	if(id == ""){
		var id= $(obj).parent().parent().attr('id'); 
	}
	var name = $(obj).text();
	$('#'+id).attr('count','0'); 
	$('#'+id).find('.chatnotification').html('').css('visibility','hidden');
    var imgSrc = $('#'+id).find('img').attr('src');
	var userName = $('#'+id).find('.cme_group_name').text();
	$("#conversationList ul").find("#"+id).find(".chatnotification").html("");
    var ui= "<div  id=\""+id+"m\" type='group' indexRg='0' class=\"row\">"
	+"<div id='chatNameHead' class='row grouphead' style='display:block;height:10vh;min-height:50px;width:100%;background-color:white;margin-left:0vh;border-radius:0px;border-bottom:0px solid'\" class=\"well\">"
	    +"<div class='col-xs-1 col-md-1'>"
	     +"<img style=\"height:6vh;min-height:6vh;min-width:6vh;width:6vh;margin-top:2vh;margin-bottom:1vh\" class=\"img-circle \" onerror=\"userImageOnErrorReplace(this);\" src=\""+imgSrc+"\">"
		+"</div>"
		
		+'<div class="col-xs-8 col-md-9" id="newgroupChatHead" onclick="showUserPopup()" style="margin-left: 1vh; padding-bottom: 1vh; padding-top: 1.5vh;cursor:pointer">'
		  +"<div style='font-size: 13px; font-family: OpenSansRegular; font-weight: bold; margin-left: -2vh;'>"+userName+"</div>"
		  +"<div  id='users' style='margin-left: -2vh;font-family: OpenSansRegular; font-size: 13px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; -1.8vh;'></div>"
		+'</div>'
		+"<div class='col-xs-1 col-md-1' style='padding: 0vh; float: right; padding-right: 3px;'>"
	     +"<img class='backChat' onclick='goBack()' title='Groups' style='margin-bottom: 1vh; cursor: pointer; float: right; margin-top: 3.3vh; width: 20px; min-width: 20px; min-height: 20px; height: 20px;'  onerror=\"userImageOnErrorReplace(this);\" src='"+path+"/images/back1.png'\>"
		+"</div>"
    +"</div>"
    
    +"<div id='chatOptionHead' class='' style='display:none;height:10.2vh;padding-top:0%;min-height:50px;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;background-color:white;border-radius:0px; padding-right:2vh;' class='well'>"
    	+"<img data-toggle='modal' data-target='#contactPopup' onclick='forwardMsg()'  title= 'Forward' style=\"cursor:pointer;margin-left: 10px;float:right;margin-top:3vh;margin-bottom:2vh;width:25px;margin-right:10px\" src='"+path+"/images/forward.svg'\>"
    	+"<img onclick='copyMsg()'  title= 'Copy' style=\"cursor:pointer;margin-left: 10px;float:right;margin-top:2.5vh;margin-bottom:2vh;width: 25px;margin-right:10px\" src='"+path+"/images/copy.svg'\>"
    	+"<img onclick='replyMsg()'  title= 'Reply' style=\"cursor:pointer;margin-left: 10px;float:right;margin-top:3vh;margin-bottom:2vh;width:25px;margin-right:10px\" src='"+path+"/images/reply.svg'\>"
    +"</div>"
    
     +"<div id='groupUser' style='display:none;height:90vh;overflow-y: scroll;'>"
    +"</div>"
	+"<div id='chat-dialog' style=\"height:80vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;border-radius:0px\" id=\"conv\" class=\"well\">"
	+"</div>"

+"<div id='text-box' style=' height: 10vh; border-radius:0; class='well row form-group'>"
+"<div id ='textborder' class='' style='height:10vh;margin:0;padding:0;border-right:1px solid #c1c5c8;float:left;width: calc(100% - 40px);'>"
+"<textarea id='text' type='text' oncut ='sendingGroupText(event,this,\"groupChat\")'  onpaste ='sendingGroupText(event,this,\"groupChat\")' onkeyup='sendingGroupText(event,this,\"groupChat\")' onkeypress='sendingGroupText(event,this)' style='overflow:hidden;height:6.5vh;padding:2px;border-radius:0%;margin-top:4px;margin-left:4px;width:98%'  placeholder='Type message here...' class='form-control'></textarea>"
+"</div>"
+"<div class=' chatpost' onclick='sendingGroupText(event,this)' style='margin-left:0px;background-color:white;height:10vh;float: right;width: 40px;text-align: center;'>"
+"<img class='chatpostI' src='"+path+"/images/workspace/post.png'  style='margin-top:2vh,vertical-algin:middle'>"
+"</div>"
+"</div>"
+"</div>";
  
    $('#chat-content').html('').html(ui);
    
    showGroupUsers(id); 
    
    getPreviousGroupMessages(id);
    
    updateSeenMessages(id,userId, "groupchat");
    
    $(obj).children().children().next().children().children().next().css('visibility','hidden'); // to hide the chat notification as well as chat box is open
    
	$('textarea.chatbox2').focus(function(){
		$(this).css('border','1px solid blue');
	});
	
	$('textarea.chatbox2').blur(function(){
		$(this).css('border','1px solid violet');
	});
	var windowWidth1 = $(window).width();
	 if(windowWidth1<750){
		$("#text").css({'width':'98%'});
	 }else{
		$(".chatpostI").css({'margin-top':'2.4vh'});
		$("#text").css({'width':'99%'});
	 }
	chatWindow();
	$("#chat-content").show(); 
	chatHeight = $("#chat-dialog").height();
	divheight =  $("#text").height();
	
}

// Not used
/*function openGroupChatBox(obj){
	
      var id= $(obj).parents('li').attr('id');
      var name = $(obj).text();
      var open = $('#'+id+'m').css('display');
      
       this condition to get the id and name from postlogin.jsp to open the chatBox from notification clicks
      if(id == null){
    	  id = $(obj).attr('gId');
    	  name = $(obj).attr('name');
      }
    
       this condition to close the popUp if it is open while again opening the chat box
      if(!$('#reportCommentPopDiv_'+id).is(':visible')){
    	  $('#reportCommentPopDiv_'+id).hide();
      }
    
      var len = $('.message-panel:visible').length;
      
     
		  if( !document.getElementById(id+'m')){
			var ui=
			   "<div id='"+id+"m' class='message-panel' value='open' type='group' indexRg='0'>"+
			   " <div id='reportCommentPopDiv_"+id+"' class='hideEpicOptionDiv' style='display: none;position: relative; margin-top: -122px; float: right; right: 0; top: 0px; padding: 10px; width: 227px ! important;box-shadow: 0 0 5px 2px rgba(0, 0, 0, 0.35);'>"+  
               " <div id='CommentPopDiv' style='border: 1px solid #bfbfbf; resize: none; height: 92px; width: 99%; border-radius: 2px; font-family: tahoma; font-size: 13px; overflow:auto;' readonly=''><ul style='padding-left: 4%;'></ul></div>"+  
               " <div class='' style='height: auto; position: relative; width: 30px; margin-right: 10px;padding-left: 15px;'>" +   	
               " <img style='transform: rotate(90deg);-ms-transform: rotate(90deg); -webkit-transform: rotate(90deg); position: absolute; top: 2px;' src='"+path+"/images/idea/arrow.png'>"+   
               " </div></div>"+
               "<div id='header'>" +
	               "<div onclick='showGroupUsers(this)' style='margin-top: 4px;margin-left: 8px;color:#ffffff; width: 196px; float:left;'>"+name+"[Group]</div>" +
	               "<div style='height: 23px; float: left; width: 22px;'><img id='min_"+id+"' type='group' onclick='minimizeTabs(this)' title='minimize' style='cursor: pointer; height: 15px; width: 15px; margin-top: 4px;' src='"+path+"/images/chat_mini.png'></div>"+
	               "<div class='cancel' style='height: 23px; float: left; width: 17px;' onclick='closepopup(this)'><img title='cancel' style='cursor: pointer; height: 15px; width: 15px; margin-top: 4px;' src='"+path+"/images/chat_close.png'></div>" +
	           "</div>"+
               "<div id='chat-dialog'></div>"+
	           "<div id='message-box'><textarea class='chatbox' disabled='disabled' onkeypress='sendingGroupText(event,this)'>Please Wait....</textarea></div>"+
	           loadingBar(id+'m')+
	           "</div>";
			$('#box').show().append(ui);
			  
			getPreviousGroupMessages(id);
			}
			else{
			//	alert(len+'-------'+num);
				 if(len < (num-1) && open !='block'){
					// alert('coming');
					  $('#box').show();
					  $("div#"+id+"m").show();
					  $("div#"+id+"m").find("textarea").focus();
					  
					    this function call is to again show the boxes in case they are hidden by hide button	
					     TabsDefaultProperties(id);
					}
					else{
					
					  if( !document.getElementById(id+'-devchat-colabus-comM') && open != 'block'){
					     Name =  $("div#"+id+"m ").find('#header').children().html();
					     count++;
					     $('#h_chat').show().find('span').html(count);
						  var ml = "<li id='"+id+"-devchat-colabus-comM' style=' height: 20px; padding-left: 12px; display:block' >"
									   +"<a href='#'><span id='"+id+"' class='name' type='group' onclick='switchPopup(this)'> "+Name+"</span>"
									   +"<span style='float: right; width: 19%; margin-top: 5px;'><img class='imgClass' onclick='closeHiddenpopup(this)' src='"+path+"/images/close.png'></span>"
									 +"</a></li>"; 
								 
					     $('#hiddenUser ul' ).append(ml);
					     
					      this function call is to again show the boxes in case they are hidden by hide button	
						    TabsDefaultProperties(id);
				     }    
				 }
			}
   if(len >= num-1 && open!='block' && dis != 'block'){	  
	  
       if( !document.getElementById(id+'-devchat-colabus-comM')){ 
    	  $('#'+id+'m').hide(); 
    	  var marginLeft = window.innerWidth - ((240 * num+1) + 150); 
    	  count = count+1;
	      $('#h_chat').show().css('margin-left',marginLeft).find('span').html(count);
	      $('#hiddenUser').css('margin-left',marginLeft-114);
	      
		  var ml = "<li id='"+id+"-devchat-colabus-comM' style=' height: 20px; padding-left: 12px; display:block' >"
				   +"<a href='#'><span id='"+id+"' class='name' type='group'  onclick='switchPopup(this)'> "+name+"[G]</span>"
				   +"<span style='float: right; width: 19%; margin-top: 5px;'><img class='imgClass' onclick='closeHiddenpopup(this)' src='"+path+"/images/close.png'></span>"
				   +"</a></li>"; 
	 
		          $('#hiddenUser ul' ).append(ml);
		         
		           this function call is to again show the boxes in case they are hidden by hide button	
				    TabsDefaultProperties(id);
		          
	         	}
   }         
      
			$('#'+id+'m').find("textarea").focus();
			updateSeenMessages(id,userId);
			

			 $('#hiddenUser ul').find('li').hover(function(){ 
		          $(this).css('backgroundColor','#5B74AE');	 
			   },
			   function(){ 
		         $(this).css('backgroundColor','#fff');	 
			});
			
			 $('.imgClass').hover(function(){ 
		          $(this).css('backgroundColor','#EC0B10');	 
			   },
			   function(){ 
		         $(this).css('backgroundColor','');	 
			});
}*/

function closeChatpopup(obj){
	
	$(obj).parents('.message-panel').hide();
	
}

/*
 * @Sending the messages to group Members
 */
 /*function  sendingGroupText(ev,obj){
   
	var id = $(obj).parent().parent().attr('id');
	//alert('id---'+id);
 
    if(ev.which == 13){   
	   // alert('sending');
    	var jid = id.split('m')[0]+'@mucr.devchat.colabus.com';
    	
		    ev.preventDefault();
		var body = $(obj).val();
		if($.trim(body).length<1)
		return;
		var message = $msg({to : jid, "type" : "groupchat" }).c('body').t(body);		 
		    Groupie.connection.send(message);
	        $("div#"+id).find("textarea").val('');
	        
	    var d = new Date();
	    var t=getTimeIn12Format(d);
	    
	   
	    $("#"+id).find('#chat-dialog').append(
	         "<div class='chat-outgoing-overall groupOutGoingChat row'>"+
             "<div class='chat-outgoing-imageframe col-xs-1'>"+ 
		     "<img src='"+lighttpdPath+"/userimages/"+userId+"."+userImgType+"'  onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 20px; margin-top: 1vh;margin-bottom:2vh;'/></div>"+
		     "<div class='col-xs-11' style='margin-top: -3vh;margin-left:-2vh; float: right; margin-right: -21px;'>"+
		     "<div class='chat-outgoing-message bubble2 row'>"+
		      body + 
	         "</div>"+ 
		     "<div class='chat-outgoing-time row'><span>"+t+"</span></div></div></div>");
	    
		 var m = d.getMonth();
		 var time=d.getHours()+":"+d.getMinutes()+"###"+d.getDate() +"-" + (Number(m)+1) + "-"+d.getFullYear();
		 var timeZone=getTimeOffset(d);
		     id= id.split('m')[0];
		 //    console.log("Timezone----"+timeZone+"---id--"+id+"---time----"+time);
		     $("#chat-dialog").mCustomScrollbar("update");		
		     $("#chat-content").show(); 
		 	var windowWidth1 = $(window).width();
			 if(windowWidth1<750){
				 
				 $(".incomeframeImage").css({'margin-left':'-7vh'});
				 $(".groupOutGoingChat").css({'margin-left':'-3vh'});
				 $(".chatIncomeBubble").css({'margin-left':'-1em'});
				  $("#chat-content").show(); 
			 }else{
				
				 $(".incomeframeImage").css({'margin-left':'-4vh'});
				 $(".groupOutGoingChat").css({'margin-left':'0vh'});
				 $(".chatIncomeBubble").css({'margin-left':'-1em'});
			 }

		     
		$.ajax({
		     url: path+"/ChatAuth",
			 type:"post",
		     data:{act:"insertGChatMsg",groupId:id,sender:userId,message:body,time:time,timeZone:timeZone},
		     success:function(result){
		    	// checkSessionTimeOut(result);
			 } 
		
		});
		 //onloadGroup();
     }

} */
/*
 * @getting the time in 12 hour format
 */
function getTimeIn12Format(d){
	
	var hours= d.getHours();
	var m = d.getMinutes();

	var suffix = hours >= 12 ? "PM":"AM";

	m = m < 10 ? '0'+ m : m;

	hours = ((hours + 11) % 12 + 1);
	hours = hours < 10 ? '0'+hours : hours

    return   (hours +':'+m+" "+ suffix);
}

/*
 * @Edit popup 
 */
function openEditSettings(obj){
	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();
	 
	var name = $(obj).prevAll('.user_box_name').text();
	var mucId = $(obj).parents('li').attr('id');	
    var ImgUrl = $(obj).parents('li').find('img').attr('src');
   
	
	/* this condition for connect module to get name and mucId*/
	if(name== null || mucId == null){
		name = $(obj).prev().text();
		mucId = $(obj).prev().attr('id').split('_')[1];		
	}
	 
	$('#groupPopUp').html('');	
	$('#ideaTransparent').removeClass('ideaTransparent').addClass('transparentDivChat');
	
	/*	var UI="<div id='groupUserPopUp' groupId='"+mucId+"' style='z-index: 10000; position: absolute; background-color: #fff; width: 85%; height: 80%; margin-top: 6%; font-family: OpenSansRegular; border-radius: 4px;'>" +
				"<div style='width: 60%; border-bottom: 1px solid #bfbfbf; height: 100%; margin-left: 1.5%; float: left;'>" +
					"<div style='width: 100%; border-bottom: 1px solid #bfbfbf; height: 40%; float: left;'>" +
					
					   "<div style='font-weight: bold; width: 100%; height: 17%; position: relative;'><span style='font-size: 15px; bottom: 0px; position: absolute;'>"+name+"</span></div>" +
					   "<div style='width:100%; height:27%;padding-top: 5%;'><div style='font-size: 15px; float:left; padding-right: 5%'>Group Name :</div><div ><input type='text' name='groupName' style='height: 10%; width: 70%; border: 1px solid #bfbfbf; border-radius: 3px;'></div></div>" +
					   "<div style='height: 55%; width: 100%; padding-bottom: 1%;'><div style='font-size: 15px; float: left; padding-top: 9%;padding-right: 5%;'><span>Group Image :</span></div>" +
					     "<div style='height: 100%; float: left; position: relative; width: 19%;'><img id='hideDiv' onerror='groupImageOnErrorReplace(this);' src='"+ImgUrl+"' style='width:80px; height:78px; float:left; position:absolute; bottom:0px;margin-bottom:1vh'>"+imageUploadDiv()+"</div>" +
					   "</div>" +
					   
					"</div>" +
					"<div style='width: 100%; border-bottom: 1px solid #bfbfbf; height: 50%; float: left;'>" +
						"<div style='width: 100%; height: 20%;' ><div style='padding-top: 2%;'>Group Participants</div></div>" +
						"<div id='addedgroupUsers' style='width: 100%; height: 80%; overflow:auto;'><ul></ul></div>" +
					"</div>" +
					"<div style='width: 100%;  height: 10%; float: left;'>" +
					    "<div style='float:right; width: 25%;'>" +
					    "<div onclick='updateGroup();' class='createBtn2' style=\"color: #ffffff;\">Update</div>" +
					    "<div style='flot; margin-left: -3vh;at: left; width: 25px; margin-top: 15%; cursor: pointer;' id='deleteGroup' onclick='deleteGroup("+mucId+")'><img style='width:15px; height:15px;' src='"+path+"/images/Delete.png' title='Delete'></div>"+
					    "</div>" +
					"</div>" +
				"</div>" +
				"<div style='width: 35%; height: 100%; float: left; margin-left: 2%; border-left:1px solid #bfbfbf;padding-left: 1%;'>" +
				    "<div style='width: 100%; height: 9%; float: left; border-bottom:1px solid #bfbfbf;'>" +
				        "<select id='UserProject' class='drpDwnSelected2' onchange='getProjectUsers(this)' style='font-weight:normal;font-size:13px;width:45%; margin-top:4%; float: left;'></select>"+
				        
				        '<div style="margin-top: 4%; height: 28px; width: 45%; margin-left: 3%;" class="serachMainDivCls">'+
						   '<input type="text" style="width: 78%;font-weight:normal;font-size:13px; padding-top: 3%;" class="serachBoxInputBoxCommonCls landingPage_Search_cLabelPlaceholder" id="searchGroupUser" placeholder="Search">'+
							'<div class="searchCloseIconMainDiv">'+
								'<img style="float: right; margin-top: 8px; margin-right: 5px; cursor: pointer; display: none;" class="serachBoxClearIcon Clear_cLabelTitle" src="'+path+'/images/xforsearch.png" id="taskCancel" title="Clear">'+
							'</div>'+
						  '<div style="" class="searchIconMainDiv">'+
							'<img style="margin-left:3px;margin-top:2px;" class="serachBoxSearchIcon"  src="'+path+'/images/searchTwo.png">'+
						  '</div>'+
					    '</div>'+
				        
				        
				  //      "<div style=\"float:left;border:1px solid #cccccc;padding:1%;border-radius:3px; margin-top: 4%; height: 62%; margin-left: 2%; width: 40%;\"><input type=\"text\" style=\"width:75%;border:none;\" placeholder=\"Search\" value='' class=\"userSearch\" id=\"searchGroupUser\"><img style=\"padding-top:2%; float:right;cursor:pointer;\" src=\""+path+"/images/idea/search.png\" id=\"GroupSearchImg\"></div>" +
				        "<img onclick='closeEditPopup();' style='float: right;cursor: pointer; margin-top: 4%;' id='popupClose' src='"+path+"/images/close.png'>"+
				    "</div>" +
				    "<div id='GroupUsers' style='width: 100%; height: 81%; float: left; overflow: auto;'><ul style='padding:0;'></ul></div>" +
				    "<div style='width: 100%; height: 9%; float: left; overflow: auto; border-top: 1px solid #bfbfbf;'>" +
					    "<div id='checkAll' class='removeAll' onclick='checkAllUsers(this);' style='float: left;width:18px;height: 18px;margin-top: 6%; cursor: pointer;'></div>" +
					    "<div style='float: left; margin-top: 5.2%; margin-left:1%; text-align: left;'>All</div>" +
					    "<img style='float: right; cursor: pointer; margin-top: 4%; margin-right: 4%; display: none;' name='addParticipants' src='"+path+"/images/add.png' title='Add Participants' onclick='prepareUIforAddGroup();' class='addParticipant' id='addGroupParticipants'>" +
				    "</div>" +
				"</div>" +
				"</div>"; */
	
	var UI="<div class='modal-dialog row'  id='groupUserPopUp' groupId='"+mucId+"' style='width: 85%; z-index: 10000; font-family: OpenSansRegular;margin-top: 5%; margin-right: auto !important; margin-bottom: auto !important; margin-left: auto !important;'>"
	    +"<div class='modal-content'>"
            +"<div class='modal-header' style='padding:0.9vh'>"
     	 +"<button type='button' class='close' onclick='closeEditPopup();' id='popupClose' data-dismiss='modal'>&times;</button>"
	 +"<h4 id='groupHead' style='font-size: 14px; font-family: opensansregular; font-weight: bold;'>&nbsp;Update Group</h4>"
	 	+"<div style='margin: 0px; padding: 5px 0px;' id='backUsers'>"
	 	+"<div style='float:right;margin-right:-4px;cursor: pointer;'><img style='height:20px;width:20px' class='img-responsive' src='"+path+"/images/back.png'/></div>" 
	 	+"<div style='float:left;' ><h3  style='font-size: 14px; font-family: opensansregular; font-weight: bold; margin: 5px 0px 0px 0px;'>Users</h3></div>"	
	 	+"</div>"
            +"</div>"
        +"<div class='modal-body' style='padding-bottom: 0px;'>"
     	+"<div class='row'>"
	+"<div class='col-sm-7' id ='GroupParticipaintsContainer'>"
		+"<div class='row' style='margin-bottom: 3vh;'>"
		+"<div class='col-sm-3'>Group Name :</div>"
		+"<div class='col-sm-8'><input type='text' name='groupName' class='form-control' id='usr'> </div>"	 	
		+"</div>"	
		+"<div class='row' style='height: 9.7vh;margin-bottom: -1vh;'>"
			+"<div class='col-sm-3'>Group Image :</div>"
			+"<div class='col-xs-3' style='padding-right: 4vh;padding-top:1vh'><img class='img-responsive' id='hideDiv1' onerror='groupImageOnErrorReplace(this);' src='"+ImgUrl+"' style='height: 6vh; min-width: 6vh; width: 6vh; min-height: 6vh;border-radius: 50%;'>"+imageUploadDiv()+"</div>"	 
			
		+"</div>"	
		+"<hr style='margin-bottom: 1vh;'>"
		+"<div class='row'>"
		+"<div class='col-xs-9' style='font-family: OpenSansRegular; font-size: 14px;'>Group Participants:</div>"
		+"<div class='col-xs-3'>"
		+"<img id='addGroupUsers' src='"+path+"/images/add.png' style='display:none;cursor:pointer;float:right'>"
		+"</div>"
		+"</div>"
		
		+"<div class='row' >"
		
			+"<div class='col-sm-12' id='addedgroupUsers' style=''><ul class='popUpForGroup' style='margin-right: 0px;height:32.4vh;overflow:auto;margin-left: -38px;position:relative;'></ul></div>"
			+"</div>"
			
	+"</div>"
	+"<div class='col-sm-5' id='groupUsersContainer' style='border-left: 1px solid rgb(191, 191, 191);'>"
		+"<div class='row' style='border-bottom: 1px solid #e5e5e5;padding-bottom: 3vh;'>"
		+"<div class='col-sm-6'>"
		+"<div class='form-group row'>"
			+"<select class='form-control' id='UserProject' onchange='getProjectUsers(this)' style='font-weight: normal; font-size: 13px; margin-left: -2vh;'>"
			+"</select>"
	    +"</div>"
		+"</div>"
		+"<div class='col-sm-6'>"
+"<div class='row' style='padding-left:20px'>"	
+"<div class='serachMainDivCls' id='searchMainDiv' style='height: 33px; margin-top: 0vh; width: 100%;margin-left:-12px'>"
+"<input type='text' placeholder='Search' id='searchGroupUser' class='serachBoxInputBoxCommonCls landingPage_Search_cLabelPlaceholder' style='width: 78%;font-weight:normal;font-size:13px; padding-top: 2%;'>"
+"<div class='searchCloseIconMainDiv'>"
+"<img title='Clear' id='taskCancel' src='"+path+"/images/xforsearch.png' class='serachBoxClearIcon Clear_cLabelTitle' style='float: right; margin-top: 11px; margin-right: 5px; cursor: pointer; display: none;'></div>"
+"<div class='searchIconMainDiv' id='gSearchIcon' style='float:right;width:35px' ><img src='"+path+"/images/searchTwo.png' class='serachBoxSearchIcon1' style='margin-left:9px;margin-top:8px;'>"
+"</div>"
+"</div>"
+"</div>"		
		+"</div>"
	+"</div>"

		+"<div class='row' style='height:50vh;background-color:white;overflow-y:scroll;'>"
			+"<div class='col-sm-12' id='GroupUsers' style='margin-left: -22px; overflow-x: hidden; padding: 0px 1vh 0px 0px;'><ul></ul>"
			+ "</div>"
			+ "</div>" 
		/*	+"<div class='row' style='width: 100%; height: 9%; float: left; margin-top: 3.4vh; margin-left: 1vh;'>" 
		    +"<div id='checkAll' class='removeAll' onclick='checkAllUsers(this);' style='float: left; width: 18px; height: 18px; cursor: pointer;'></div>" 
		    +"<div style='float: left; text-align: left; margin-left: 1%; margin-top: -0.5%;'>All</div>" 
		    +"<img style='float: right; cursor: pointer; margin-right: 4%; margin-top:-0.5vh;display: none;' name='addParticipants' src='"+path+"/images/add.png' title='Add Participants' onclick='prepareUIforAddGroup();' class='addParticipant' id='addGroupParticipants'>"
	   + "</div>" */
	+"</div>"
+"</div>"
    +"</div>"
    +"<div class='modal-footer' style='padding: 1vh 2vh 2vh 1vh;'>"
	+"<div class='row' style='margin-top:2vh'>"
	+"<div class='col-sm-7' id='updateGruoups'>"
	+"<img id=' ' style='width:15px; margin-top:-1vh;height:15px;cursor:pointer;' src='"+path+"/images/Delete.png' title='Delete' onclick='deleteGroup("+mucId+")'>"
	+"&nbsp;&nbsp;"
		+"<button type='button' class='btn btn-primary' style='background-color: #00bff3; margin-top:-1vh;height: 30px; width: 72px;' onclick='updateGroup()'>Update</button>"
	+"</div>"
	+"<div class='col-sm-5' id='groupUsersAll'>"
	+"<div style='width: 100%; height: 9%; float: left;  margin-bottom: -0.5vh;'>" 
    +"<div id='checkAll' class='removeAll' onclick='checkAllUsers(this);' style='float: left; width: 18px; height: 18px; cursor: pointer;'></div>" 
    +"<div style='float: left; text-align: left; margin-left: 1%; margin-top: -0.5%;'>All</div>" 
    +"<img style='float: right; cursor: pointer; margin-top:-0.5vh;margin-right: 4%; display: none;' name='addParticipants' src='"+path+"/images/add.png' title='Add Participants' onclick='prepareUIforAddGroup();' class='addParticipant' id='addGroupParticipants'>"
+ "</div>" 
	+"</div>"
    +"</div>"  
    
    +"</div>"
 +" </div>"
  

   +"</div>";

	
	$('#groupPopUp').html(UI);
	
	loadGroupUsers();
	getAllProjects();
	ExistingGroupUsers(mucId);
	resizeChatWindow();
	$('#deleteGroup').on('click', function() {
	 deleteGroup(mucId);
	  });
	//$("#loadingBar").hide();	
	//timerControl("");
	$("#addGroupUsers").click(function() {
	    $("#groupUsersContainer").show();
	    $("#GroupParticipaintsContainer").hide();
	    $("#groupUsersAll").show();
	    $("#updateGruoups").hide();
	    $("#groupHead").hide();
	    $("#backUsers").show();
	    $("#popupClose").attr("onclick","switchBackUser()");
     });
	
	$("#backUsers").click(function() {
		$("#groupUsersContainer").hide();
	    $("#GroupParticipaintsContainer").show();
	    $("#groupUsersAll").hide();
	    $("#updateGruoups").show();
	    $("#groupHead").show();
	    $("#backUsers").hide();
	    $("#popupClose").attr("onclick","closeEditPopup()");
	 });
	onclickFunctionForUploadImage();
}

function closeEditPopup(){
	$('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
	$('#groupPopUp').html('');
	 ImageUploadFromSystem = false;
	  ImageIsUploaded = false;
}

/*
 * @For listing of the group users already present in the group
 */
var groupUsers='';
function ExistingGroupUsers(mucId){
     var jsonData='';
     var groupId='';
     var groupName='';
     var groupOwners='';
     var imgtype='';
     var Users= '';
     var name='';
     var UserId='';
     var userImgType='';
     var groupRole="";
	 $('div#addedgroupUsers ul').html('');
	 $("#addedgroupUsers").mCustomScrollbar("destroy");
	 
	$.ajax({
			url:path+"/ChatAuth",
			type:"POST",
			data:{act:"getMUCRoomDetails",mucId:mucId},
			mimeType: "textPlain",
			success:function(result){
			//	checkSessionTimeOut(result);
		    jsonData = $.parseJSON(result);
		    
		    groupId= jsonData[0].groupId;
		    groupName= jsonData[0].groupName;
		    groupOwners= jsonData[0].groupOwner;
		    groupUsers= jsonData[0].groupUsers;
		    
		    //$('#sound').data('existingUsers',groupUsers);  // To check deleted users and added Users
		    $('#groupUserPopUp').data('groupId',groupId);
		    
		    $('div#groupUserPopUp').find('input[type=text]').not('#searchGroupUser').val(groupName);
		    
	        for(i=0 ; i< groupUsers.length; i++){
	        	
	        	name= groupUsers[i].userName;
	        	groupRole= groupUsers[i].groupRole;
	        	UserId="participant_"+ groupUsers[i].userId;
	            var id = groupUsers[i].userId;
	            imgtype=  groupUsers[i].userImageType;
	            var srcpath = $("#"+id).find('img').attr("src");
        	/*  var UI  = "<li id='user_"+UserId+"' class='prjUserCls' >"
	  		   +"<div style='width:auto; float:left;'>"+name+"</div>"
	  		   +"<div style='float: left; width: auto; margin-right: 10px; margin-left: 6px;'><img onclick='removeUser("+id+")' src='"+path+"/images/close.png' style='width: 7px; height: 7px; cursor: pointer;'></div>"
	  		   +"</li>"; 	
	        	*/
	            var editText="Mark as admin";
	            /*if(groupRole == "admin"){
	        		editText="Unmark as admin";
	        	}else{
	        		editText="Mark as admin";
	        	}*/
	        	
	        if(userId == groupUsers[i].userId){
	        	var imgurl = lighttpdPath+"/userimages/"+id+"."+imgtype ;
	        	
	        	/*UI="<li style='width: 35%;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:2%;margin-bottom:2%; margin-top: 1%;' id='part_"+UserId+"'>" +
			 		"<div title='"+name+"' style='float: left;padding-left: 5%; width: 80%;text-overflow: ellipsis;white-space: nowrap; overflow:hidden;font-size:12px; '>"+name+"</div>" +
			        "</li>";*/
	        	 UI  = "<li id='part_"+UserId+"' role='admin' class='chatUserCls' style='margin-left: 0px;border-radius: 0px;width:100%;height:33px'>"
	 	     	+"<div  style='width: auto; font-size: 12px; margin-top: 4px; float: left;'><img title='"+name+"' onerror='groupImageOnErrorReplace(this);' src='"+imgurl+"' style='width: 25px; height: 25px; cursor: pointer;margin-top:-4px;border-radius: 50%;'>&nbsp&nbsp"+name+"</div>"
	 	     	+"<div class='imgGroupedit' style='display:none;float: right; width: auto; margin-right: 5px; margin-left: 6px;margin-top: 4px;'><img  Id='"+UserId+"' onclick='showMore("+id+")'  src='"+path+"/images/more.png' style='margin-left: 10px;height: 19px; cursor: pointer;'></div>"

	 		   +"</li>";
	        	 
	        	 
	        	 $('div#addedgroupUsers ul').append(UI);
	        	 continue;
	        } 
	        	
	        UI  = "<li id='part_"+UserId+"' role='"+groupRole+"' class='chatUserCls' style='margin-left: 0px;border-radius: 0px;width:100%;height:33px'>"
	     		+"<div  style='width: auto; font-size: 12px; margin-top: 4px; float: left;'><img title='"+name+"' onerror='groupImageOnErrorReplace(this);' src='"+srcpath+"' style='width: 25px; height: 25px; cursor: pointer;margin-top:-4px;border-radius: 50%;'>&nbsp&nbsp"+name+"</div>"
	     		+"<div class='imgGroupedit' style='float: right; width: auto; margin-right: 5px; margin-left: 6px;margin-top: 4px;position: relative;'>"
	     			+"<div id= \"option_"+UserId+"\" class=\"actFeedOptionsDiv newpopup \" style=\"width:133px;display:none;margin-right:-5px; margin-top: -14px;\">"
	     				+"<div style=\"margin-top: -2px; float: right; position: absolute; right: -16px;\" class=\"workSpace_arrow_right\">"
	     					+"<img src=\""+path+"/images/arrow.png\">"
	     				+"</div>"
		        
	     				+"<div id=\"rmhover\"  style=\"width:99%;height:auto;padding:0 0 3px ;cursor:pointer;float:left\"> "
	     					+"<span class=\"\" Id='"+UserId+"' onclick='addRoleToParticipants(this);' style=\"float: left; height: 20px;margin-left: 5px; overflow: visible; width: 120px;\">"+editText+"</span>"
	     				+"</div>"
		         
		 //UI+="<div onclick=\'deleteGroup("+mucId+")\' style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\">"
	     				+"<div style=\"width:99%;height:auto;padding: 3px 0;cursor:pointer;float:left;border-top:1px solid #ccc;\">"
	     					+" <span Id='"+UserId+"' onclick='removeGroupParticipant(this);' style=\"float: left; height: 20px;margin-left: 6px; overflow: hidden; width: 60px;\">Delete</span>"
	     				+" </div>"
	     			+" </div>" 
	     			+" <img Id='"+UserId+"'  onclick='showMore("+id+")'  src='"+path+"/images/more.png' style='margin-left: 10px; height: 19px; cursor: pointer;'>"
		      +" </div>"
		 +"</li>";
	     
	       	/* UI="<li style='width: 35%;border: 1px solid #C0C0C0;border-radius:3px;border:1px solid #C0C0C0;font-weight: normal;height: 20px;float:left;margin-right:2%; margin-bottom:2%; margin-top: 1%;' id='part_"+UserId+"'>" +
		 		"<div title='"+name+"' style='float: left;padding-left: 5%; width: 80%;text-overflow: ellipsis;white-space: nowrap; overflow:hidden;font-size:12px; '>"+name+"</div>" +
		 		"<img onclick='removeGroupParticipant(this);' Id='"+UserId+"' style='float: right;margin:3%;cursor:pointer;' class='Delete_cLabelTitle' src='"+path+"/images/calender/emailDel.png' title='Delete'>" +
		        "</li>";*/
		        	
	    	    $('div#addedgroupUsers ul').append(UI);
	    	 
	          }
	        RemoveExistingUsersInEditMode();
	        
	       /* $("div#addedgroupUsers").mCustomScrollbar({
				scrollButtons:{
					enable:true
				},
			});
	        
	        
	        $('div#addedgroupUsers .mCSB_container').css('margin-left','-37px');
	        $('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonUp').css({'background-position':'-80px 0px','opacity':'0.6'});
	    	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_buttonDown').css({'background-position':'-80px -20px','opacity':'0.6'});
	    	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_draggerRail').css('background','#B1B1B1');
	    	$('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','#6C6C6C');
	        $('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','#B1B1B1');
	        $('div#addedgroupUsers .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','#B1B1B1');  
	*/
		  
		
		}
	});
	
	 $('#searchGroupUser').on('keyup',function(){
		    var txt = $('#searchGroupUser').val().toLowerCase();
		    
		    if(txt.length > 0){
		    	
		     $("#taskCancel").show();	
	         $('#GroupUsers li:visible').each(function(){
			 
			     var val = $(this).children().children(':eq(1)').text().toLowerCase();
			     
					 if(val.indexOf(txt)!= -1){
					   $(this).show();
					 }else{
					   $(this).hide();
					 }	 
			$("#GroupUsers").mCustomScrollbar("update");	 
			 });		
	        
	       }else{
	    	   $("#taskCancel").hide();
	    	   $('#GroupUsers li').show();
	    	   
	    	   $('#addedgroupUsers li').each(function(){
	   
	    		  var id = $(this).attr('id').split('part_')[1];
	    		  $('#GroupUsers ul').find('#'+id).hide();
	    	   });
	    	   $("#GroupUsers").mCustomScrollbar("update"); 
	       }
	});	
	 
	 $('#taskCancel').click(function(){
		 $('#searchGroupUser').val('');
		 $('#GroupUsers ul li').css('display','block');
		 
		 $('#addedgroupUsers li').each(function(){	   
   		  var id = $(this).attr('id').split('part_')[1];
   		  $('#GroupUsers ul').find('#'+id).hide();
   	      });
		 
		 $("#GroupUsers").mCustomScrollbar("update");
		 $("#taskCancel").hide();
	});  
	 
	 
}

/*
 * @For removing the group users If you are thr Group Admin
 */
function RemoveExistingUsersInEditMode(){
	
	/* To hide the users in edit mode which are already listed */	
	var list = $('#addedgroupUsers ul li');
	if(list.length > 0){
		
		for(i=0 ;i < list.length ; i++)
			{
			  var id = $(list[i]).attr('id');
			  var newId = id.substr(5,id.length);
			  $('#GroupUsers ul').find('#'+newId).hide();
			}
		$("#GroupUsers").mCustomScrollbar("update");
	}
}

/*function deleteGroupAlert(mucId){
	 MUCID = mucId;
	 confirmFun(getValues(customalertData,"Alert_delete"),"delete","deleteGroup");
     	   
}*/

/*
 * @for deleting the group from database as well from the server
 */ /*
function deleteGroup(mucId){
//	var mucId = MUCID;
	$("#loadingBar").show();	
	timerControl("start");
	$.ajax({
		url:path+"/ChatAuth",
		type:"POST",
		data:{act:"deleteMUCRoom",mucId:mucId},
		mimeType: "textPlain",
		success:function(result){
			//checkSessionTimeOut(result);
		   // alert("Room "+result);
		    $('#groupPopUp').html('');
		    
		   /* <iq type="set" id="4D0C27DD-D597-4D2B-BAA9-31738E5C55C9" from="497@devchat.colabus.com" to="162@mucr.devchat.colabus.com">
		    <query xmlns="http://jabber.org/protocol/muc#owner"><destroy><reason/></destroy></query>
		    </iq>*/
/*
		  var deleteIq = $iq({ to: mucId+'@mucr.devchat.colabus.com',
			  			 type: 'set'
		               }).c("query", {
		                   	    xmlns: Groupie.NS_MUC+"#owner"
		                   	}).c("destroy").c("reason");
		    
			Groupie.connection.sendIQ(deleteIq, function () { console.log('success--deleted'); }, function (err) { console.log('error--deletion', err); });
			
			$('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
		   listOfConnectedRooms();
		    //GetgroupChatRooms();
		    $('#chat-content').html('');
		     $('#chat-content').hide();
		    $('#chat-frame2').show();
		}
});

}

/*
 * @For adding deleting users , change name for the existing group
 */
function updateGroup(){
	roleForGroup='';
	$("#loadingBar").show();	
	timerControl("start");
	$("#loadMsg").hide();
	
	var existed_users = groupUsers;
    var ExistedUsersArray = '';
    var exRoleArray="";
    var current_users='';
    var UserId = '';
    
    if(ImageIsUploaded){
    	var mucId = $('#groupUserPopUp').attr('groupId');
    	ChangeGroupImage(mucId);	
    }
    
	for(i=0; i < existed_users.length; i++){
		  var Id= existed_users[i].userId;
		  //var role = $("#part_participant_"+existed_users[i].userId).attr('role');
		  var exRole = existed_users[i].groupRole;
		    if(i != existed_users.length-1){
		    	ExistedUsersArray+=Id+',';
		    	exRoleArray+=exRole+',';
		    }
		    else{
		    	ExistedUsersArray+=Id;	
		    	exRoleArray+=exRole;
		    }
		}
	//console.log('ExistedUsersArray--'+ExistedUsersArray);
	
	var users = $('#addedgroupUsers ul li');
   
    //console.log('users.length---'+users.length);

	for(i=0; i<users.length;i++){
	    UserId = $(users[i]).attr('id');
	    
	    var role = $("#"+UserId).attr('role');
	    UserId = UserId.split('_')[2];
	    //console.log(userId);  
    
	    if(i != users.length-1){
	      roleForGroup+=role+",";
	      current_users+=UserId+',';
	    }
	    else{
	      roleForGroup+=role;
	      current_users+=UserId;	
	    }

	}
	
	UpdatedUsers(ExistedUsersArray,current_users,roleForGroup,exRoleArray);
	resizeChatWindow();
}

/*
 * @Updating the group Users
 */
function UpdatedUsers(existingUser,currentUsers,roleForGroup,exRoleArray){
	   var newlyAdded = '';
	   var deletedUsers = '';
	   var roleGroup="";
	   var uRole="";
	   var exRole="";
	   console.log("existingUser----"+existingUser+"--"+exRoleArray);
	   console.log("currentUsers----"+currentUsers+"--"+roleForGroup);	
		
	   cUsers = currentUsers.split(',');
	   eUsers = existingUser.split(',');
	   uRole= roleForGroup.split(',');
	   exRole=exRoleArray.split(',');
	   console.log("uRole----"+uRole);
	   console.log("exRole----"+exRole);	
	   
	   for(i=0; i<cUsers.length; i++)
	   {
		 var ctr=0;
		 var ctrRole=0;
		 for(j=0; j<eUsers.length;j++){
			 
	     if(cUsers[i] == eUsers[j])
		    {
			  ctr++; 
		    }
	     }
		 
		 if(uRole[i] == exRole[i])
		    {
			 ctrRole++; 
		    }
		 if(ctr == 0 || ctrRole == 0){
			 newlyAdded+= cUsers[i]+',';
		 	 roleGroup+= uRole[i]+',';
		 	 console.log("roleGroup---"+roleGroup+"--newlyAdded--"+newlyAdded);
		 }
	    }
		 
		 
	   
	   for(i=0; i<eUsers.length; i++)
	   {
		 var ctr = 0;
		 for(j=0; j<cUsers.length;j++){  
	     if(eUsers[i] == cUsers[j])
		    {
			  ctr++; 
		    }
	     }
		 if(ctr == 0)
			 if(eUsers[i] != userId)
			 deletedUsers+= eUsers[i]+',';
	   }
	   roleGroup = roleGroup.substr(0,roleGroup.length-1);
	   newlyAdded = newlyAdded.substr(0,newlyAdded.length-1);
	   deletedUsers = deletedUsers.substr(0,deletedUsers.length-1);

	   console.log("newlyAdded----"+newlyAdded);
	   console.log("deletedUsers---"+deletedUsers);
	   console.log("roleGroup---"+roleGroup);
	   UpdateUserdatabase(newlyAdded,deletedUsers,roleGroup);
	}
	
/*
 * @Sending the group users to the database
 */
function UpdateUserdatabase(newlyAdded,deletedUsers,roleForGroup){
	var mucId = $('#groupUserPopUp').data('groupId');
	var mucName = $('div#groupUserPopUp').find('input[type=text]').val();
//	console.log("mucName-------"+mucName);
   if(mucName == ''){
	     $("#loadingBar").hide();	
		 timerControl("");
     	parent.alertFun("Group Name Can't be Empty.",'warning');
    }else{
		$.ajax({
			url:path+"/ChatAuth",
			type:"POST",
			data:{act:"updateMUCRoomUsers",mucId:mucId,mucName:mucName,userId:userId,selMembers:newlyAdded,delMembers:deletedUsers,roleForGroup:roleForGroup},
			mimeType: "textPlain",
			success:function(result){
			//	checkSessionTimeOut(result);
			    //alert("Room "+result);
			    $('#groupPopUp').html('');
			   	listOfConnectedRooms();
			    //GetgroupChatRooms();
			    $('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
	
			   }
		    });	
    }
	
}	 

/* for getting Previous History of chat messages */
function getPreviousGroupMessages(mucId){
	
	//console.log("mucId---------------"+mucId);
	var jsonData = "";
	var index = $('#'+mucId+'m').attr('indexRg');
	var d = new Date();
	var timeZone=getTimeOffset(d);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	 
	$("#loadingBar").show();	
	timerControl("start");
	$("#loadMsg").hide();
	
	$.ajax({
		url:path+"/ChatAuth",
		type:"POST",
		data:{act:"getGHisMsg",groupId:mucId,indexRg:index,noOfMsgReq:'20',localTZone:timeZone},
		mimeType: "textPlain",
		success:function(result){
			//checkSessionTimeOut(result);
			 $('#'+mucId+'m').attr('indexRg',(Number(index)+20));	
			 var $Id= $("#"+mucId+"m").find('#chat-dialog');
			// alert(result);
		 
			 if(result == 'No latest msg'){
		        	
	         }else{
		          jsonData=jQuery.parseJSON(result);
		        for(i=0; i<jsonData.length; i++)
		        	{
		        	   var UI="";
		        	   var t="";
		        	   var d = new Date();
		        	   var time = jsonData[i].time;
		        	       time = time.replace("CHR(26)",":");
		        	   var T =  time.split("#@#")[1];   
		        	   var date=  time.split("#@#")[0];  
		        	   var m = d.getMonth();
		        	       m = months[m];
		        	   var date1 = d.getDate();
		        	       date1 = (date1>10) ? (date1) : ('0'+date1);
		        	   
		        	   var D=date1+"-"+ m +"-"+d.getFullYear();
		        	  // console.log(T+"-------"+date+"----D----"+D);
		        	   if(date == D){
		        		   t="Today  "+T;
		        	   }else{
		        		   t=T+" , "+ date;
		        	   }
		        	  // var message=replaceSpecialCharacter(jsonData[i].message);
		        	   var message=jsonData[i].message;
		        	   var message=replaceSpecialCharacter(message);
		        	   var userFullName = jsonData[i].senderName;
		        	   var Id = jsonData[i].sender;
		               var senderImgType =jsonData[i].senderImgType;
		        	   var msg = textTolink(message);
		        	   var messageId=jsonData[i].messageId;
		        	   var msgReplyId=jsonData[i].msg_reply_id;
		        	   if(jsonData[i].sender == userId){
		        		   UI=	"<div id="+messageId+" class='chat-outgoing-overall groupOutGoingChat row chatId'>"+
		       			        			"<div class='chat-outgoing-imageframe col-xs-1'>"+ 
		       			        				"<img userId='"+Id+"' src='"+lighttpdPath+"/userimages/"+Id+"."+userImgType+"'  title='"+userFullName+"' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%; margin-top: 1vh;margin-bottom:2vh;'/></div>"+
		       			        			"<div class='col-xs-11 groupOutbubble' style='margin-top: -3vh;margin-left:-2vh;float: right; margin-right: -21px'>"+
		       			        				"<div onclick='replyForChat(this)' class='chat-outgoing-message bubble2 row'>";
							        		   if(msgReplyId != 0){
						        		   		    var msgthread=jsonData[i].replyThread;
						        		   			var mainChatThread;
						        		   			var replyName;
									        		   		 
						        		   			if(msgthread[0].replyfrom == userId){
						        		   				replyName="You";
						        		   			}else{
						        		   				replyName=msgthread[0].userName;
						        		   			}
					       		   				
						        		   			mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
						        		   			mainChatThread = textTolink(mainChatThread,'reply');
							        		   			 
						        		   			UI+="<div class='replyNameTop'>"+replyName+"</div>"+
								       			        "<div class='replyMsgBottom'>"+mainChatThread+"</div>"+
								       			        "<div messageType='text' class='replyToReply ChatText'>"+
								        			    msg + 
								        			    "</div>";
								        		}else{
								        			UI+= "<div messageType='text'  class='ChatText'>"+
							        			    msg + 
							        			    "</div>";
								        		}
							        		   
							        		   UI+="</div>"+ 
		       			        					"<div class='chat-outgoing-time row'><span>"+t+"</span></div></div></div>";
		        	   }
		        	   else{
		        			   UI=  "<div id="+messageId+" class='chat-income-overall row chatId' style='outline:none' tabindex='0'>"+
										"<div class='chat-income-imageframe col-xs-1' style='margin-top: 23px;'>"+
											"<img  userId='"+Id+"' src='"+lighttpdPath+"/userimages/"+Id+"."+senderImgType+"'  onerror='userImageOnErrorReplace(this);' title='"+userFullName+"' class='incomeframeImage' style='width: 40px; height: 38px; border-radius: 50%;margin-left:-7vh '/></div>"+ 
										"<div class='col-xs-10 chatIncomeBubble' style = 'padding-left: 0vh;margin-left:-1em' >"+
											"<div class='row' align='left' style='font-family: helvetica; font-size: 13px; width: 105%; padding-left: 34px;'>"+userFullName+"</div>"+
												"<div onclick='replyForChat(this)' class='chat-income-message bubble row'>";
								 		if(msgReplyId != 0){
				        		   		    var msgthread=jsonData[i].replyThread;
				        		   			var mainChatThread;
				        		   			var replyName;
						        		   		 
				        		   			if(msgthread[0].replyfrom == userId){
				        		   				replyName="You";
				        		   			}else{
				        		   				replyName=msgthread[0].userName;
				        		   			}
				        		   			
				        		   			mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
				        		   			mainChatThread = textTolink(mainChatThread,'reply');
				        		   			
				        		   			UI+="<div class='replyNameTop'>"+replyName+"</div>"+
								   			   "<div class='replyMsgBottom'>"+mainChatThread+"</div>"+
								   			   "<div messageType='text'  class='replyToReply ChatText'>"+
					        			   	   msg + 
					        			   	   "</div>";
					        		   }else{
					        			   UI+= "<div messageType='text'  class='ChatText'>"+
					        			   	 msg + 
					        			   	 "</div>";
					        		   }
							 		
								 		UI+="</div>"+
							 			 	"<div class='chat-income-time row'  style='padding-left: 34px;'>"+t+"</div></div></div>";
		        	   }
		        	          $Id.append(UI);
		        	}
		        
		         
		        	 if(jsonData.length>=20 ){
			        	   $Id.prepend("<div id='lem' style='background-color: #d9d4ce; text-align:center; height: 8%;margin-bottom:2%; padding-top: 1%;' onclick='getGroupNextPreviousMessages("+mucId+")'><a href='#' style='color: blue;font-family: helvetica; '>Load earlier messages</a></div>");
			           }
		        	 
		        	 //   index = parseInt(index) + 20;
			        //	$("#"+id+"m").attr('index',index);
	     }
			    // gerneralScrollBarFunction("chat-dialog");    
			 
                 var height =  $Id[0].scrollHeight;
                     $Id.scrollTop(height);
                     
                   $('#loader_'+mucId+'m').hide();   
		           $('#'+mucId+"m").find('textarea').removeAttr('disabled').text("").focus();
		           $("#"+mucId+"m").attr('cnt','2');
			 
		           
		       	var windowWidth1 = $(window).width();
				 if(windowWidth1<750){
					 
					 $(".incomeframeImage").css({'margin-left':'-7vh'});
					 $(".groupOutGoingChat").css({'margin-left':'-3vh'});
					 $(".chatIncomeBubble").css({'margin-left':'-1em'});
				 }else{
					
					 $(".incomeframeImage").css({'margin-left':'-4vh'});
					 $(".groupOutGoingChat").css({'margin-left':'0vh'});
					 $(".chatIncomeBubble").css({'margin-left':'-1em'});
				 }
		   }
	    });	
	 
	 $("#loadingBar").hide();	
	 timerControl("");
}

/* 
 * @Load another 20 users
 */
function getGroupNextPreviousMessages(mucId){
	var jsonData;
	var index =  $("#"+mucId+"m").attr('indexRg');
//	var imgType = $("#"+id).attr('imgType');
	var count =  $("#"+mucId+"m").attr('cnt');
	    count = parseInt(count);
	var $Id= $("#"+mucId+"m").find('#chat-dialog');
	    $Id.children('#lem').remove();
	var timeZone=getTimeOffset(new Date());  
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		
	    $("#loadingBar").show();	
	    timerControl("start");
	  	$("#loadMsg").hide();
	    
		 $.ajax({
		     url:path+"/ChatAuth",
			 type:"post",
		     data:{act:"getGHisMsg",groupId:mucId,indexRg:index,noOfMsgReq:'20',localTZone:timeZone},
		     mimeType: "textPlain",
		     success:function(result){
		    	// checkSessionTimeOut(result);
		    	   $("#"+mucId+"m").attr('indexRg',Number(index)+20);
		           if(result == 'No latest msg'){ 
		        	
		           }else{
			          jsonData=jQuery.parseJSON(result);
			        for(i = jsonData.length-1; i >= 0; i--)
			        	{
			        	   var UI="";
			        	   var t="";
			        	   var d = new Date();
			        	   var time = jsonData[i].time;
			        	       time = time.replace("CHR(26)",":");
			        	   var T =  time.split("#@#")[1];   
			        	   var date=  time.split("#@#")[0];  
			        	   var m = d.getMonth();
			        	       m = months[m];
		        	       var date1 = d.getDate();
			        	       date1 = (date1>10) ? (date1) : ('0'+date1);
			        	   
			        	   var D=date1+"-"+ m +"-"+d.getFullYear();   
			        	
			        	   if(date == D){
			        		   t="Today  "+T;
			        	   }else{
			        		   t=T+" , "+ date;
			        	   }
			        	  // var message=jsonData[i].message.replace("CHR(26)",":");
			        	   var message=jsonData[i].message;
		        	       var message=replaceSpecialCharacter(message);
			        	   var userFullName = jsonData[i].senderName;
			        	   var Id = jsonData[i].sender;
			               var senderImgType =jsonData[i].senderImgType;
			               var msg = textTolink(message);
			               var messageId=jsonData[i].messageId;
			               var msgReplyId=jsonData[i].msg_reply_id;
			        	   if(jsonData[i].sender == userId){
				        		   UI=	"<div id="+messageId+" class='chat-outgoing-overall groupOutGoingChat row chatId'>"+
			        			        	"<div class='chat-outgoing-imageframe col-xs-1'>"+ 
			        			        		"<img userId='"+Id+"' src='"+lighttpdPath+"/userimages/"+Id+"."+userImgType+"'  title='"+userFullName+"' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%; margin-top: 1vh;margin-bottom:2vh;'/></div>"+
			        			        	"<div class='col-xs-11 groupOutbubble' style='margin-top: -3vh;margin-left:-2vh;float: right; margin-right: -21px'>"+
			        			        		"<div onclick='replyForChat(this)' class='chat-outgoing-message bubble2 row'>";
								        		   if(msgReplyId != 0){
							        		   		    var msgthread=jsonData[i].replyThread;
							        		   			var mainChatThread;
							        		   			var replyName;
									        		   		 
							        		   			if(msgthread[0].replyfrom == userId){
							        		   				replyName="You";
							        		   			}else{
							        		   				replyName=msgthread[0].userName;
							        		   			}
							        		   			
							        		   			mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
							        		   			mainChatThread = textTolink(mainChatThread,'reply');
							        		   			 
							        		   			UI+="<div class='replyNameTop'>"+replyName+"</div>"+
										   			    	"<div class='replyMsgBottom'>"+mainChatThread+"</div>"+
										   			    	"<div messageType='text'  class='replyToReply ChatText'>"+
									        			   	msg + 
									        			    "</div>";
								        		   }else{
								        			   UI+="<div messageType='text'  class='ChatText'>"+
								        			   	msg + 
								        			    "</div>";
								        		   }
								       
								        		   UI+="</div>"+ 
								        		   		"<div class='chat-outgoing-time row'><span>"+t+"</span></div></div></div>";

			        	   }else{
		        		   		UI=  "<div id="+messageId+" class='chat-income-overall row chatId' style='outline:none' tabindex='0'>"+
								"<div class='chat-income-imageframe col-xs-1' style='margin-top: 23px;'>"+
								"<img  userId='"+Id+"' src='"+lighttpdPath+"/userimages/"+Id+"."+senderImgType+"'  onerror='userImageOnErrorReplace(this);' title='"+userFullName+"' class='incomeframeImage' style='width: 40px; height: 38px; border-radius: 50%;margin-left:-7vh '/></div>"+ 
								"<div class='col-xs-10 chatIncomeBubble' style = 'padding-left: 0vh;margin-left:-1.5em' >"+
								"<div class='row'align='left' style='font-family: helvetica; font-size: 13px; width: 105%; padding-left: 34px;'>"+userFullName+"</div>"+
								"<div onclick='replyForChat(this)' class='chat-income-message bubble row'>";
		        		   		if(msgReplyId != 0){
		        		   		    var msgthread=jsonData[i].replyThread;
		        		   			var mainChatThread;
		        		   			var replyName;
				        		   		 
		        		   			if(msgthread[0].replyfrom == userId){
		        		   				replyName="You";
		        		   			}else{
		        		   				replyName=msgthread[0].userName;
		        		   			}
		        		   			
		        		   			mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
		        		   			mainChatThread = textTolink(mainChatThread,'reply');
		        		   			 
		        		   			UI+="<div class='replyNameTop'>"+replyName+"</div>"+
					   			    	"<div class='replyMsgBottom'>"+mainChatThread+"</div>"+
					   			    	"<div messageType='text'  class='replyToReply ChatText'>"+
				        			   	 msg + 
				        			   	 "</div>";
		        		   		
		        		   		}else{
		        		   			UI+= "<div messageType='text' class='ChatText'>"+
			        			   	 msg + 
			        			   	 "</div>";
		        		   		}
		        		   		
		        		   		UI+= "</div>" +
		        		   			 "<div class='chat-income-time row' style='padding-left: 34px;'>"+t+"</div>"+
		        		   			 "</div>"+
		        		   			 "</div>";
			        	   }
			        	          $Id.prepend(UI);
			        	}
			        
			         if(jsonData.length>=20 ){
			        	   $Id.prepend("<div id='lem' style='background-color: #d9d4ce; text-align:center; height: 8%;margin-bottom:2%;padding-top: 1%;' onclick='getGroupNextPreviousMessages("+mucId+")'><a href='#' style='color: blue; font-family: helvetica;'>Load earlier messages</a></div>");
			           }
			         
			         //   index = parseInt(index) + 20;
			        //	$("#"+id+"m").attr('index',index);
			         //gerneralScrollBarFunction("chat-dialog"); 
			             
			        	 var height =  $Id[0].scrollHeight;
			        	     height = height/count++;
	                         $Id.scrollTop(height);
	                         $("#"+mucId+"m").attr('cnt',count);
	                         $('#loader_'+mucId+'m').hide(); 
	                         var windowWidth1 = $(window).width();
	        				 if(windowWidth1<750){
	        					 
	        					 $(".incomeframeImage").css({'margin-left':'-7vh'});
	        					 $(".groupOutGoingChat").css({'margin-left':'-3vh'});
	        					 $(".chatIncomeBubble").css({'margin-left':'-1em'});
	        				 }else{
	        					
	        					 $(".incomeframeImage").css({'margin-left':'-4vh'});
	        					 $(".groupOutGoingChat").css({'margin-left':'0vh'});
	        					 $(".chatIncomeBubble").css({'margin-left':'-1em'});
	        				 }
		      }
		  
		 } 
  }); 
		 
		 $("#loadingBar").hide();	
		 timerControl("");	 
}

$(window).resize(resizeChatWindow);
function resizeChatWindow(){
	
	 var windowWidth = $(window).width();
	 if(windowWidth < 750){
		
		 $(".chatHead").css({'height': '48px','min-height':'48px'});
		 $(".active").css({'height':'48px','min-height':'48px','padding-top':'13px'});
		 $(".inActive1").css({'height':'48px','min-height':'48px','padding-top':'13px'});
		 $(".inActive2").css({'height':'48px','min-height':'48px','padding-top':'13px'});
		 $(".groupInactive").css({'height':'48px','min-height':'48px','padding-top':'13px'});
		 $("#searchMainDiv").css({'margin-left':'-12px','margin-top':'-8px','width':'100%'});
		 $(".serachBoxSearchIcon").css({'margin-left':'12px'});
		 $("#UserProject").css({'margin-left':'-12px','margin-top':'0px'});
		 $("#GroupUsers").css({'margin-left':'-32px','overflow-x':'hidden','padding':'0 1vh 0 0'});
		 //$(".modal-content").css({'margin':'4vh -6vh 0vh -4vh'});
		 $("#addGroupUsers").show();
		 $("#groupUsersContainer").hide();
		 $("#groupUsersAll").hide();
		 $(".name").css("margin-left","0px");
		 $("#groupUsersAll").css({'float':'none'});
		 $(".chatHead").css({'margin-left':'-20px'});
		 $("#searchGroupUser").css({'width':'64%'});
		 $("#GroupParticipaintsContainer").show();
		 $("#createGruoups").show();
		 $("#groupHead").show();
		 $("#backUsers").hide();
		 $("#updateGruoups").show();
		 $("#searchText2,#searchText3,#searchText3").css({'height':'48px'});
		 $("#conversationList,#chat-box").css({'height':'91vh'});
		 $("#newgroupChatHead").css({'padding-top':'1.5vh','margin-left':'4vh'});
		 $(".incomeframeImage").css({'margin-left':'-7vh'});
		 $(".groupOutGoingChat").css({'margin-left':'-3vh'});
		 $(".chatIncomeBubble").css({'margin-left':'-1em'});
		 $(".searchHeader").css({'height':'48px','min-height':'48px'});
		 $("#searchText").css({'height':'48px'});
		 $(".backChat").css({'margin-top':'2.4vh'});
		 $("#searchText2,#searchText3").css({'height':'48px'});
		 $(".uploadFromGalleryPopUpCls").css({'z-index':'70000','width':'69%','height':'71%'}); 
		 $(".chatBody#uploadDiv").css({'width':'94%'});
		 $(".chatBody#userProfileImagesContentDiv").css({'width':'95%'});
		 $("#userPrfHeader").css({'width':'82%'});
		 $(".backChat").css('visibility','visible');
		 //$("#loadingBar").css({'margin-left': '-30px'});
		 /*** Below line of code- comment for CME alone and Uncomment for Colabus CME. **/
		 $("#chatMsgSearch").css({'padding-left':'6vh'});
		 
		 $(".groupList").css({'padding':'2% 2% 2% 7%'});
		 $("#backChat").show();
		 $("#chat-content").css({'height':'100vh','overflow-y':'hidden'});
		// $("#chat-dialog").css({'height':'82vh'});
		 $(".incomeframeImage").css({'margin-left':'-7vh'});
		 $("#text").css({'height':'6.5vh','width':'98%'});
		 $("#text-box").css({'border-bottom': '0px none', 'height': '8vh', 'background-color': 'white', 'border-radius': '0px'});
		 $(".chatpostI").css({'vertical-align':'middle','margin-top':'10px'});
		$(".popup").css({'margin-right':'3px','margin-top':'-43px'});
		// $(".newpopup").css({'margin-right':'-1vh','margin-top':'-6.7vh'});
		 $(".callmsg").css({'width':'75%'});
		 $(".searchBack").css({'margin-top':'14px'});
		 /*$(".owl-next").css({'margin-top':'-36.5%'}); 
		 $(".owl-prev").css({'margin-top':'-36.5%'}); 
		 $(".owl-controls").show();*/
		 //$(".vName").css({'top':'20.5%'}); 
		 $(".callHistoryRow").css({'padding-left':'30px'});
		 $(".callHistoryDuration").css({'font-size':'10px'});
		 $(".videochatimage").css({'margin-left':'22px'});
		 $("#backUsers").css({'margin':' 2px 0px 10px 4px','padding':'15px 0px'});
		 if(($('#chat-frame3').is(':visible')) || ($('#chat-frame2').is(':visible')) || ($('#chat-frame').is(':visible'))){
		 	$('#chat-content').hide();
		 }
		 if($("#videoContent").is(":visible") == true){
		   
		   if($("#chatFor").is(":visible") == true) {
		      if($("#highlightFor").is(":visible") == true) {
			    $("#chatFor").css({'width':'30%','display':'block'});
			    $("#avCallContainer").css({'width':'40%'});
		  	  }else{
		  	    $("#chatFor").css({'width':'50%','display':'block'});
		  	    $("#avCallContainer").css({'width':'50%'});
		  	  }	
			}
		    if($("#chatFor").is(":visible") == true) {
			      if($("#transcriptFor").is(":visible") == true) {
				    $("#chatFor").css({'width':'30%','display':'block'});
				    $("#avCallContainer").css({'width':'40%'});
			  	  }else{
			  	    $("#chatFor").css({'width':'50%','display':'block'});
			  	    $("#avCallContainer").css({'width':'50%'});
			  	  }	
			}
		    if($("#highlightFor").is(":visible") == true) {
			    if($("#chatFor").is(":visible") == true) {
			        $("#highlightFor").css({'width':'30%','display':'block'});
			    	$("#avCallContainer").css({'width':'40%'});
			  	}else{
			  	    $("#highlightFor").css({'width':'50%','display':'block'});
			        $("#avCallContainer").css({'width':'50%'});
			  	}
			}
		    if($("#transcriptFor").is(":visible") == true) {
			    if($("#chatFor").is(":visible") == true) {
			        $("#transcriptFor").css({'width':'30%','display':'block'});
			    	$("#avCallContainer").css({'width':'40%'});
			  	}else{
			  	    $("#transcriptFor").css({'width':'50%','display':'block'});
			        $("#avCallContainer").css({'width':'50%'});
			  	}
			}
			
		   	
		  }else{
		  	$("#chatFor, #highlightFor").css({'width':'30%','display':'none'});
		  	$("#chatFor, #transcriptFor").css({'width':'30%','display':'none'});
			$("#avCallContainer").css({'width':'100%'});
		  }
		 $("#globalNotification").css({'left':'10%','right':'10%'});
	}else{
		//console.log("windowWidth--"+windowWidth);
		 //$(".chatnotification").html('').css('visibility','hidden');
		
		 $("#chat-frame").hide();
		 $("#chat-frame2").hide();
		 $("#chat-frame4").hide();
		 $("#chat-frame3").show();
		 $("#chat-content").hide(); 
		 $(".chatBody.serachBoxSearchIcon").css({'margin-left':'21px'});
		 $(".serachBoxSearchIcon1").css({'margin-left':'9px'});
		 $("#searchMainDiv").css({'margin-left':'-2.5%','margin-top':'1px','width':'97%'});
		 $(".chatBody#UserProject").css({'margin-left':'-1vh','margin-top':'0vh'});
		 $("#GroupUsers").css({'margin-left':'-22px','overflow-x':'hidden','padding':'0 1vh 0 0'});
		 $("#groupUsersContainer").show();
		 $("#groupUsersAll").show();
		 $("#GroupParticipaintsContainer").show();
		 $("#addGroupUsers").hide();
		 $("#createGruoups").show();
		 $("#backUsers").hide();
		 $("#groupUsersAll").css({'float':'right'});
		 $(".backChat").css('visibility','hidden');
		 $("#groupHead").show();
		 if($("#chat-content").is(":visible") == true){
			 $("#chat-content").css('display','none');
			 $("#chatNameHead").hide();
			 $("#chatOptionHead").hide();
			 $("#chat-dialog").hide();
			 $("#text-box").hide();
			 $("#chat-frame3").show();
		 }
		 $("#conversationList,#groupChat-box,#chat-box").css({'height':'91vh'});
		 $("#newgroupChatHead").css({'padding-top':'2.5vh','margin-left':'0vh'});
		 $(".incomeframeImage").css({'margin-left':'-4vh'});
		 $(".groupOutGoingChat").css({'margin-left':'0vh'});
		 $(".inActive1").css({'height':'10vh','min-height':'10vh','padding-top':'3.6vh'});
		 $(".inActive2").css({'height':'10vh','min-height':'10vh','padding-top':'3.6vh'});
		 $(".groupInactive").css({'height':'10vh','min-height':'10vh','padding-top':'3.6vh'});
		 $(".chatIncomeBubble").css({'margin-left':'-1em'});
		 $(".searchHeader").css({'height':'10vh','min-height':'10vh'});
		 $(".backChat").css({'margin-top':'3.4vh'});
		 $("#searchText2,#searchText3,#searchText").css({'height':'10vh'});
		 $(".chatBody.uploadFromGalleryPopUpCls").css({'z-index':'70000','width':'66%','height':'71%'});
		 $(".chatBody#uploadDiv").css({'width':'98%'});
		 $(".chatBody#userProfileImagesContentDiv").css({'width':'100%'});
		 $("#searchText2,#searchText,#searchText3").css({'width':'93%'});
		 $("#userPrfHeader").css({'width':'90%'});
		 $("#searchGroupUser").css({'width':'73%'});
		 $(".chatpostI").css({'margin-top':'2.4vh'});
		 $(".chatpost").css({'margin-left':'0px'});
		 //$("#loadingBar").css({'margin-left': '-48px'});
		 /*** Below line of code- comment for CME alone and Uncomment for Colabus CME. **/
		 $("#chatMsgSearch").css({'padding-left':'6vh'});
         $(".name").css("margin-left","-15px");
		 $(".popup").css({'margin-right':'3px','margin-top':'-43px'});
		 //$(".newpopup").css({'margin-right':'-4vh','margin-top':'-1vh'});
		 $(".groupList").css({'padding':'1.4% 45% 1.4% 3%'});
		 $("#backChat").show();
		 $("#chat-content").css({'height':'100vh'});
		 //$("#chat-dialog").css({'height':'82vh'});
		 $(".chatHead").css({'height': '10vh','min-height':'8vh','margin-left':'-20px'});
		 $(".active").css({'height':'10vh','min-height':'10vh','padding-top':'3.6vh','padding-left':'2vh'});
		 $("#text-box").css({'border-bottom': '0px none', 'height': '8vh', 'background-color': 'white', 'border-radius': '0px'});
		 $("#text").css({'height':'6.9vh','width':'99%'});
		 $(".callmsg").css({'width':'50%'});
		 $(".searchBack").css({'margin-top':'3vh'});
		 /*$(".owl-next").css({'margin-top':'-10.5%'}); 
		 $(".owl-prev").css({'margin-top':'-10.5%'}); */
		 //$(".vName").css({'top':'21.5%'});
		 $(".callHistoryRow").css({'padding-left':'18px'});
		 $(".callHistoryDuration").css({'font-size':'12px'});
		 $(".videochatimage").css({'margin-left':'0px'});
		 $("#globalNotification").css({'left':'30%','right':'30%'});
		 //chatWindow();
		 
		  if($("#videoContent").is(":visible") == true ){
		    if($("#chatFor").is(":visible") == true) {
		      $("#chatFor").css({'width':'30%','display':'block'});
		  	  if($("#highlightFor").is(":visible") == true) {
			    $("#avCallContainer").css({'width':'40%'});
		  	  }else if($("#transcriptFor").is(":visible") == true) {
			    $("#avCallContainer").css({'width':'40%'});
		  	  }else{
		  	    $("#avCallContainer").css({'width':'70%'});
		  	  }	
			}
		    if($("#highlightFor").is(":visible") == true) {
			    $("#highlightFor").css({'width':'30%','display':'block'});
			    if($("#chatFor").is(":visible") == true) {
				    $("#avCallContainer").css({'width':'40%'});
			  	}else{
			  	    $("#avCallContainer").css({'width':'70%'});
			  	}
			}else if($("#transcriptFor").is(":visible") == true) {
			    $("#transcriptFor").css({'width':'30%','display':'block'});
			    if($("#chatFor").is(":visible") == true) {
				    $("#avCallContainer").css({'width':'40%'});
			  	}else{
			  	    $("#avCallContainer").css({'width':'70%'});
					
					//$(".arrow").css("left","-2px");
			  	}
			}
		  }else{
		  	$("#chatFor,#highlightFor").css({'width':'30%','display':'none'});
		  	$("#chatFor,#transcriptFor").css({'width':'30%','display':'none'});
			$("#avCallContainer").css({'width':'100%'});
		  }
		  
		  
	}
	if(windowWidth < 600){
		console.log("ww---"+windowWidth);
		$("#chatFor,#highlightFor").css({'display':'none'});
		$("#chatFor,#transcriptFor").css({'display':'none'});
		$("#avCallContainer").css({'width':'100%'});
		if(callType=="AV"){
			$("#videoFor").find(".arr").attr('src',path+'/images/video/slideboxright.png')
			$("#videoFor").find(".arrow").css("left","-2px");
		}
		else if(callType=="A"){
			$("#audioFor").find(".arr").attr('src',path+'/images/video/slideboxright.png');
				$("#audioFor").find(".arrow").css("left","-2px");
		}
		
	}
	/*
	if(windowWidth < screen.width-100){
	   if($("#whiteboardFor").is(":visible") == true ){
	   		toggleWhiteBoard();
	   }
	}	
	var windowHeight = $(window).height();
	if(windowHeight < screen.height-100){
	   if($("#whiteboardFor").is(":visible") == true ){
	   		toggleWhiteBoard();
	   }
	}
	*/
	if($("#chat-dialog").is(':visible')){
		 $("#chat-dialog").find("#chat-dialog").css({'height':'82vh'});
	if( typeof $("#checkArr").html()!="undefined"){
		 if(($("#checkArr").attr('src').indexOf("downarr.svg"))>-1){
			var windoww = $(window).width();
		   if(windoww>750){
			var css=$("#chatFor").find("#chat-dialog").attr('style')
             css+=';height:'+60+"vh !important";
	         $("#chatFor").find("#chat-dialog").removeAttr('style');
	          $("#chatFor").find("#chat-dialog").attr('style',css);
		     }
			else{
				   var css=$("#chatFor").find("#chat-dialog").attr('style')
		             css+=';height:'+57+"vh !important";
			         $("#chatFor").find("#chat-dialog").removeAttr('style');
			          $("#chatFor").find("#chat-dialog").attr('style',css);
			}
		 }
	     else{
		    if(windoww>750){
			var css=$("#chatFor").find("#chat-dialog").attr('style')
             css+=';height:'+8+"vh !important";
	         $("#chatFor").find("#chat-dialog").removeAttr('style');
	          $("#chatFor").find("#chat-dialog").attr('style',css);
    		 }
			else{
				   var css=$("#chatFor").find("#chat-dialog").attr('style')
		             css+=';height:'+5+"vh !important";
			         $("#chatFor").find("#chat-dialog").removeAttr('style');
			          $("#chatFor").find("#chat-dialog").attr('style',css);
			}
	      }
}
		// autogrowParticipants();
		 chatHeight = $("#chat-dialog").height();
		 divheight =  $("#text").height();
		 resizeChatBox();
	}
	 
   }

/*
 * @For showing the Group Users in the chat box title bar opened on click of any particular Group Users
 */
function showGroupUsers(id){
	
	//var id = $(obj).parents('.message-panel').attr('id').split('m')[0];
	//var left = $(obj).offset().left;
	//var top = $(obj).offset().top;

	// if(!$('#reportCommentPopDiv_'+id).is(':visible')){
	     
		// $("#loadingBar").show();	
		// timerControl("start");
		
		// $("div#CommentPopDiv").mCustomScrollbar("destroy"); 
		// $('#reportCommentPopDiv_'+id).show();
          var jsonData='';
          var UI='';
          var name='';
          var groupUser='';
          var groupUserId = '';
          var users = '';
          var tiles = '';
          
			$.ajax({
					url:path+"/ChatAuth",
					type:"POST",
					data:{act:"getMUCRoomDetails",mucId:id},
					mimeType: "textPlain",
					success:function(result){
					//	checkSessionTimeOut(result);
						//alert(result);
				    jsonData = $.parseJSON(result);
				    groupUser =jsonData[0].groupUsers;
				    groupUserId = jsonData[0].groupOwner;
				    
				    for(i=0;i<groupUser.length;i++){
				    	     name=groupUser[i].userName;
				    	     userid=groupUser[i].userId;
				    	     imgType=groupUser[i].userImageType
				    	     UI+="<li class='groupList' style='list-style-type:none;border-bottom: 1px solid #cccccc;padding:2% 2% 2% 7%;'>"+
				    	         "<div class='row' >" + 
								 "<div class='col-xs-3' style='width: 10%; height: 37px; padding-left: 8px;'> <img title='" +name+"' src='"+lighttpdPath+"/userimages/"+userid+"."+imgType+"' onerror='userImageOnErrorReplace(this);' style='height: 34px; min-height: 34px; width:34px;min-width: 34px;border-radius: 50%;'/></div>" +
							     "<div class='col-xs-6' style='font-family: OpenSansRegular; font-size: 14px; width: 70%; margin: 6px 6px 6px -17px;margin-left:1px' value='" +name +"'>" + name +"</div>"+
				    	         "<div>"+
				    	         "</li>";
				    	     
				    	if(groupUserId == groupUser[i].userId){
				             users += '<span style="color: blue;">'+name+'[GroupAdmin]</span> | ';
				             tiles +=  name+'| ';
				    		// UI+="<li style='height:10%; border-bottom: 1px solid #cccccc;padding:5%;'>"+name+"<span style='float:right;'><img style='height: 15px; width: 15px; margin-top: 2px;' src='"+path+"/images/idea/myprojects.png'><span></li>";	
				    	}else{
				    		 users += name+'| ';
				    		 tiles += name+'| ';
				    		//  UI+="<li style='height:10%;border-bottom: 1px solid #cccccc;padding:5%;'>"+name+"</li>";
				    	}
				    }

				   $('#'+id+'m').find('div#users').html(users).attr('title',tiles);
				   $('#'+id+'m').find('div#groupUser').html(UI);
				   
				//    $('#reportCommentPopDiv_'+id).children().find('ul').html(UI);
				    
				/*	$("div#CommentPopDiv").mCustomScrollbar({
						scrollButtons:{
							enable:true
						},
					});
					
				    $('div#CommentPopDiv .mCSB_container').css('margin-right','15px');
					$('div#CommentPopDiv .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
					$('div#CommentPopDiv .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
					$('div#CommentPopDiv .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
					$('div#CommentPopDiv .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
					$('div#CommentPopDiv .mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
					$('div#CommentPopDiv .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
				    $('div#CommentPopDiv .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
		            $('div#CommentPopDiv .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
		       		parent.$('#loadingBar').hide();
		       		parent.timerControl(" ");*/
				}
			});		
		          
	/* }else{
		 $('#reportCommentPopDiv_'+id).hide();
	 }*/
	
}
//GroupUsers
function openGroupEditSettings(obj){
			$("li").off("click");	     		 		  			  
				var mucId = $(obj).parents('li').attr('id');
				var name = $(obj).parents('li').attr('groupName');										
			 $('.hideOptionDiv').hide();
			 
			 if ($('#option_'+mucId).is(":hidden")) {
					$('.actFeedOptionsDiv').hide();
					$('.actFeedReplyOptionsDiv').hide();
					$('#option_'+mucId).slideToggle(function(){
						$(this).css('overflow','');
					});
				}else{
					$('#option_'+mucId).slideToggle('slow');
				}  			  			  			 		     			   
		      }    		   		   		   
	function showUserPopup(){
		if($('#groupUser').is(':visible')){
			$('#chat-dialog').show();
			$('#text-box').show();
			$('.grouphead').css({'border-bottom':'0px solid'});
			$('#groupUser').hide();
		}else{
			$('#chat-dialog').hide();
			$('#text-box').hide();
			$('.grouphead').css({'border-bottom':'1px solid'});
			$('#groupUser').show();
		}
		
	}	
	
function openNewGroupChatBox1(obj){
	
	var ui= "<div  id=\""+obj+"m\" type='group'  >"
	+"<div  class='row grouphead' style='height:10vh;min-height:50px;width:100%;background-color:white;margin-left:0vh;border-radius:0px;border-bottom:0px solid'\" class=\"well\">"
	   
		
		+"<div class='' style='padding: 0vh; margin-left: 3vh;'>"
	     +"<img class='backChat' onclick='goBack()' title='Groups' style='margin-bottom: 1vh; cursor: pointer; float: right; margin-top: 3.3vh; width: 20px; min-width: 20px; min-height: 20px; height: 20px;'  onerror=\"userImageOnErrorReplace(this);\" src='"+path+"/images/back1.png'\>"
		+"</div>"
    +"</div>"
     +"<div id='groupUser' style='display:none;height:90vh;overflow-y: scroll;'>"
    +"</div>"
	+"<div id='chat-dialog' style=\"height:80vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;border-radius:0px\" id=\"conv\" class=\"well\">"
	+"</div>"

+"<div id='text-box' style=' height: 10vh; border-radius:0; class='well row form-group'>"
+"<div id ='textborder' class='' style='height:10vh;margin:0;padding:0;border-right:1px solid #c1c5c8'>"
+"<textarea id='text' type='text' oncut ='sendingGroupText(event,this,\"groupChat\")'  onpaste ='sendingGroupText(event,this,\"groupChat\")' onkeyup='sendingGroupText(event,this,\"groupChat\")'  onkeypress='sendingGroupText(event,this,\"groupChat\")' style='overflow:hidden;height:6.5vh;padding:2px;border-radius:0%;margin-top:4px;margin-left:4px;width:97%'  placeholder='Type message here...' class='form-control'></textarea>"
+"</div>"
+"<div class=' chatpost' onclick='sendingGroupText(event,this,\"groupChat\")' style='margin-left:0px;background-color:white;height:10vh'>"
+"<img class='chatpostI' src='"+path+"/images/workspace/post.png'  style='margin-top:10px,vertical-algin:middle'>"
+"</div>"
+"</div>"
+"</div>";
  
	
	
	
    $('#chatFor').html('').html(ui);
   
   
	$('textarea.chatbox2').focus(function(){
		$(this).css('border','1px solid blue');
	});
	
	$('textarea.chatbox2').blur(function(){
		$(this).css('border','1px solid violet');
	});


	
}			

function showMore(id){
	$(".mCSB_container").css({'height':'100%'});
	var role = $("#part_participant_"+id).attr('role');
	
	if(role == "admin"){
		$("span #participant_"+id).text('Unmark as admin');
	}else{
		$("#participant_"+id).text('Mark as admin');
	}
	if($("#option_participant_"+id).is(':visible')){
		$("#option_participant_"+id).hide();
		$(".newpopup").hide();
	}else{
		$(".newpopup").hide();
		
		if($(".popUpForGroup").length > 0){
    		var totalOffset = $('#addedgroupUsers').offset().top - $("#part_participant_"+id).offset().top;
    		var totalInnerHeight = $('#addedgroupUsers').innerHeight() - totalOffset;
    		
    		if(totalInnerHeight < $('#addedgroupUsers').innerHeight()){
    			$("#option_participant_"+id).css({"margin-top":"-14px","width": "133px"});
    			$("#option_participant_"+id).show();
    			$(".workSpace_arrow_right").css("margin-top","-2px");
    		}else{
    			$("#option_participant_"+id).css({"margin-top":"-27px","width": "133px"});
    			$("#option_participant_"+id).show();
    			$(".workSpace_arrow_right").css("margin-top","11px");
    		}
    	}
	}
}

function switchBackUser(){
	$("#groupUsersContainer").hide();
    $("#GroupParticipaintsContainer").show();
    $("#groupUsersAll").hide();
    $("#createGruoups").show();
    $("#groupHead").show();
    $("#backUsers").hide();	
    $("#popupClose").attr("onclick","closeEditPopup()");
}

function adduserfunction(){
	$("#groupUsersContainer").show();
    $("#GroupParticipaintsContainer").hide();
    $("#groupUsersAll").show();
    $("#createGruoups").hide();
    $("#groupHead").hide();
    $("#backUsers").show();
}
