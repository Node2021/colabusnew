 function viewSentimentGraph(proId,id,reportType,score){
	 	var avgSentimentScore="";
    	$('.sCal').css({'display':'block'});
    	
    	console.log('senderType:'+senderType+'   ---sentimentFilterFlag:'+sentimentFilterFlag)
    	// sentimentFilterFlag : by default this flag is "false". It is "true" only in case of filter is active inside the graph 
		if(senderType =="jira" && (sentimentFilterFlag == false)){ 
			console.log('sentimentFilterFlag2:'+sentimentFilterFlag)
    		$('#chart-filter_'+proId).css('visibility','visible');
			$('#chart-filter_'+proId).find('.jiraMainFilter').children('option[value="all"]').attr('selected','selected');
			$('#chart-filter_'+proId).find('.jiraSubFilter').attr('disabled',true).children('option[value="0"]').attr('selected','selected');
		}
    	
    	
    	if(reportType == "user"){
    			sProjId = proId;
    			sUserId = id;
    			fetchSentimentGraphData(proId, id, reportType);
    	}else{
		    	sProjId = proId;
		    	sUserId = id;
		    	// sentimentFilterFlag : by default this flag is "false". It is "true" only in case of filter is active inside the graph 
		    	if( ! sentimentFilterFlag && $("#chart-container_"+proId).is(':visible')){  // sentimentFilterFlag (This flag is used to avoid hiding of graph view when filter is used.No need to hide.)  
		    		$("#chart-container_"+proId).css({'display':'none'});
		    		$(".pStyle").removeAttr("style");
		    		$(".borderStyle").css({'border':'0px solid rgb(191, 191, 191)','border-bottom-color':'rgb(191, 191, 191)','background-color':''});
		    	}else{
		    		fetchSentimentGraphData(proId, id, reportType);
		    	}
	    }
   }
    
  function fetchSentimentGraphData(proId, id, reportType){
	  sentimentFilterFlag = false;
	  var avgSentimentScore="";
	  var fType = $(".sCalBcActive").attr('id');
  	  
  	 var jiraFilterMain = "";
	 var jiraFilterSub ="";
	
	 if(senderType =="jira"){ // this is for jira sentiment report to read the filter value inside the graph.
		$('#chart-filter_'+proId).css('visibility','visible');
		jiraFilterMain = $('#chart-filter_'+proId).find('.jiraMainFilter').val();
		jiraFilterSub = $('#chart-filter_'+proId).find('.jiraSubFilter').val();
	 }	
  	
	  
	  if(reportType == "user"){
	    	
	    	$("#chart-container_"+proId).css({'display':'block'});
	    	$(".borderStyle").css({'border':'0px solid rgb(191, 191, 191)','border-bottom-color':'rgb(191, 191, 191)','background-color':''});
	    	$("#lis_"+proId+'_'+id).css({'border':'1px solid rgb(191, 191, 191)','border-bottom-color':'white','background-color':'white'});
	    	//if(reportType=="user"){
	    		$(".chartcontain").css({'height':'54vh','padding-top':'0%'});
	    		$('#avgScore_'+proId).css({'display':'block'});
		    	//$('#score_'+proId).html(score)
		    	//var t = $("#tmName_"+proId+'_'+id).text();
		    	//$('#tmname').html(t);
		    	
		    		
	    	//}
	    	/*if(reportType=="project"){
	    		$('#avgScore_'+proId).css({'display':'block'});
	    		$('#score_'+proId).html(score);
	    		$(".pStyle").removeAttr("style");
	    		$(".chartcontain").css({'height':'54vh','padding-top':'0%'});
	    		$("#sysScore_"+proId).css({'margin-left':'38%'});
	    		$("#proj_"+proId).css({ 'border': '1px solid rgb(191, 191, 191)','margin-top':'2%','margin-left':'2%','background-color':'white','height':'98%','padding-left':'3%','border-bottom-color':'white','width':'63%'});
	    	}else{ */
	    		$(".pStyle").removeAttr("style");
	    		$("#sysScore_"+proId).css({'margin-left':'4.4vw'});
	    	//}
	    		

    			
	    	var localOffsetTime=getTimeOffset(new Date());
	    	$("#loadingBar").show();
			timerControl("start");
			$.ajax({
				url: path + "/reportAction.do" ,
				type:"POST",
				data:{action:"fetchSentimentAnalysis",reportType:reportType,localOffsetTime:localOffsetTime,projectId:proId,fType:fType,user:id,jiraFilterMain:jiraFilterMain,jiraFilterSub:jiraFilterSub},
				error: function(jqXHR, textStatus, errorThrown) {
				checkError(jqXHR,textStatus,errorThrown);
				$("#loadingBar").hide();
				timerControl("");
					}, 
				    success:function(result){
				    
					    checkSessionTimeOut(result);
					    //console.log(result);
					    var subArray = [];
					    var subDateArray=[];
					    var bgArray=[];
					    var nArray=[];
					   
							    
					    if(result.length >1){
						   var jData=jQuery.parseJSON(result);
						   avgSentimentScore = jData[0].avgSentimentScore;
						   var jsonData = jData[0].userData;
					        
					        for(var i=0; i<jsonData.length; i++){
					        	
					        	subDateArray[i]=jsonData[i].createdTime;
					        	nArray[i]=jsonData[i].notifyId;
								if(jsonData[i].sentiment_score == 0 || jsonData[i].sentiment_score == 0.0){
							       jsonData[i].sentiment_score =  0.005;
							    }
					        	if(jsonData[i].sentiment_score >=0.25 && jsonData[i].sentiment_score <=1){
					        	   bgArray[i]='#008000';
					        	   subArray[i]=Math.abs(jsonData[i].sentiment_score);
					        	}else if(jsonData[i].sentiment_score >=-0.25 && jsonData[i].sentiment_score <=0.25){
					        	   subArray[i]=Math.abs(jsonData[i].sentiment_score);
					        	   bgArray[i]='#FFC200';
					        	}else{
					        	   subArray[i]=Math.abs(jsonData[i].sentiment_score);
					        	   bgArray[i]='#ff0000';
					        	}
					        }
					        
					        
					        
				        	var ssub = [];
					      /*	var t= "";
					      	for(var i=0; i<subDateArray.length; i++){
					      			if(t != subDateArray[i]){
					      				ssub.push(subDateArray[i]);
					      			}else{
					      				ssub.push("");
					      			}
					      			t = subDateArray[i];
					      	}
				      	*/
				      	
							      	
					        $('#score_'+proId).html(avgSentimentScore);
					        if(avgSentimentScore >=0.25 && avgSentimentScore <=1){
						        	   $('#score_'+proId).css({'background-color':'#008000'});
						        	}else if(avgSentimentScore >=-0.25 && avgSentimentScore <0.25){
						        		$('#score_'+proId).css({'background-color':'#FFC200'});
						        	}else{
						        		$('#score_'+proId).css({'background-color':'#ff0000'});
									}
					        creatChart(subArray,ssub,bgArray,proId,nArray,subDateArray);
				        }else{
				        	creatChart(subArray,ssub,bgArray,proId,nArray,subDateArray);
				        }
				        sentimentPerformance(reportType,proId,id);
				        $('.viewBorder').css({'border-right':'1px solid rgb(191, 191, 191)'});
				        $('.view').html("");
					    $("#loadingBar").hide();
						timerControl("");
	   			    }
	    	});
	
	}else{
	  
		$("#chart-container_"+proId).css({'display':'block'});
		$("#chart-container_"+proId).css({'display':'block'});
		$(".borderStyle").css({'border':'0px solid rgb(191, 191, 191)','border-bottom-color':'rgb(191, 191, 191)'});
		$("#lis_"+proId+'_'+id).css({'border':'1px solid rgb(191, 191, 191)','border-bottom-color':'white'});
		
		$('#avgScore_'+proId).css({'display':'block'});
		$(".pStyle").removeAttr("style");
		$(".chartcontain").css({'height':'54vh','padding-top':'0%'});
		$("#sysScore_"+proId).css({'margin-left':'4.4vw'});
		$("#proj_"+proId).css({ 'border': '1px solid rgb(191, 191, 191)','background-color':'white','height':'116px','width':'8.3vw','margin-left':'-2vw','padding-left':'0.9vw','border-bottom-color':'white','margin-top': '-8px'});
	    
		var localOffsetTime=getTimeOffset(new Date());
		$("#loadingBar").show();
		timerControl("start");
	
		$.ajax({
			url: path + "/reportAction.do" ,
			type:"POST",
			data:{action:"fetchSentimentAnalysis",reportType:reportType,localOffsetTime:localOffsetTime,projectId:proId,fType:fType,jiraFilterMain:jiraFilterMain,jiraFilterSub:jiraFilterSub},
			error: function(jqXHR, textStatus, errorThrown) {
				checkError(jqXHR,textStatus,errorThrown);
				$("#loadingBar").hide();
				timerControl("");
				}, 
			    success:function(result){
			    
					    checkSessionTimeOut(result);
					    var subArray = [];
					    var subDateArray=[];
					    var bgArray=[];
					    var nArray=[];
					    
					    
					    if(result.length >1){
						    var jData=jQuery.parseJSON(result);
						   
						    avgSentimentScore = jData[0].avgSentimentScore;
						    var jsonData = jData[0].projData;
							
					        for(var i=0; i<jsonData.length; i++){
					        	//subArray[i]=jsonData[i].sentiment_score;
					        	subDateArray[i]=jsonData[i].createdTime;
					        	nArray[i]=jsonData[i].notifyId;
								if(jsonData[i].sentiment_score == 0 || jsonData[i].sentiment_score == 0.0){
					        		jsonData[i].sentiment_score =  0.005;
					        	}
					        	if(jsonData[i].sentiment_score >=0.25 && jsonData[i].sentiment_score <=1){
					        	   bgArray[i]='#008000';
					        	   subArray[i]=Math.abs(jsonData[i].sentiment_score);
					        	}else if(jsonData[i].sentiment_score >=-0.25 && jsonData[i].sentiment_score <=0.25){
					        	   bgArray[i]='#FFC200';
					        	   subArray[i]=Math.abs(jsonData[i].sentiment_score);
					        	}else{
					        	   bgArray[i]='#ff0000';
					        	   subArray[i]=Math.abs(jsonData[i].sentiment_score);
					        	}
					        }
					      	var ssub = [];
					   /*   	var t= "";
					      	for(var i=0; i<subDateArray.length; i++){
					      			if(t != subDateArray[i]){
					      				ssub.push(subDateArray[i]);
					      			}else{
					      				ssub.push("");
					      			}
					      			t = subDateArray[i];
					      	} */
					       
					        $('#score_'+proId).html(avgSentimentScore);
					        if(avgSentimentScore >=0.25 && avgSentimentScore <=1){
				        	   $('#score_'+proId).css({'background-color':'#008000'});
				        	}else if(avgSentimentScore >=-0.25 && avgSentimentScore <0.25){
				        		$('#score_'+proId).css({'background-color':'#FFC200'});
				        	}else{
				        		$('#score_'+proId).css({'background-color':'#ff0000'});
							}
					        
					        creatChart(subArray,ssub,bgArray,proId,nArray,subDateArray);
				        }else{
				        	creatChart(subArray,ssub,bgArray,proId,nArray,subDateArray);
				        }
				        $('.viewBorder').css({'border-right':'1px solid rgb(191, 191, 191)'});
				        $('.view').html("");
				        sentimentPerformance(reportType,proId);
					    $("#loadingBar").hide();
						timerControl("");
 			   	 }
		});
	}
  }
 
 
    
    var $chart;
    function creatChart(subArray,ssub,bgArray,proId,nArray,subDateArray)
    {
     	if (typeof $chart !== "undefined") 
     	{
			$chart.destroy();
		}
			//$('#proj_'+proId).css({'width':'62%','padding-left':' 5%','height':'100%','background':'aliceblue'});
  			//$('#chart_'+proId).css({'background':'aliceblue'})
		$('.chartcontain').css({'display':'none'});
		$('#chart-container_'+proId).css({'display':'block'});
   			// var max = subArray.reduce(function(a, b) {
					//	    return Math.max(a, b);
					//	});
			//  var min = subArray.reduce(function(a, b) {
				////		    return Math.min(a, b);
					//	}); 
			//$('#max_'+userid).after(' <a href="#">'+max+'</a>');  
			//$('#min_'+userid).after(' <a href="#">'+min+'</a>');   	 
		var ctx = document.getElementById('myChart_'+proId).getContext('2d');
		ctx.height = 100;
		chart = new Chart(ctx,
		{
		
		    // The type of chart we want to create
		    type: 'bar',
			
			// The data for our dataset
		    data:
		    {
				labels: subDateArray,
				datasets: 
				[{
		            label:nArray ,
		            backgroundColor: bgArray,
		    		data: subArray,
				}]
			},
			options: 
			{
				
				
				
				responsive : true,
          		showTooltips: true,
				onClick:click,
				maintainAspectRatio: false,
				scales: 
				{
						xAxes: 
							[{
								barPercentage: 0.3,
								gridLines: 
									{
										display:false
									},
								ticks: 
									{
							            stepSize: 1,
							            min: 0,
							            minRotation: 30
									}
							}],
						yAxes: 
							[{
								display: true,
                            	ticks: 
                            	{
									min:0,
									stepValue: 0.1,
									max: 1
								}
							}]
				},
				legend: 
				{
						display: false,
				},
				tooltips: 
				{
				
						callbacks: 
						{
							label: function(tooltipItem) 
							{
								
								return tooltipItem.yLabel ;
							}
						}
				}
				
			}
		});
		$chart = chart;
    }
    
    var $chartp;
    function creatChartP(subArray,subDateArray,proId)
    {
    
     	if (typeof $chartp !== "undefined") 
     	{
			$chartp.destroy();
		}
		  	 
		var ctx1 = document.getElementById('myChartp_'+proId).getContext('2d');
		ctx1.height = 100;
		chartp = new Chart(ctx1, 
		{
		
		    type: 'doughnut',
			data:
		    {
				labels: subArray,
				datasets: 
				[{
		            //label:nArray ,
		            backgroundColor: ['green','red','rgb(255, 194, 0)'],
		    		data: subDateArray,
				}]
			},
			options: 
			{
				responsive: true,
   				maintainAspectRatio: false,
   				pieceLabel: {
   					render:'value',
				    fontColor:'black',
				    overlap:true,
				     
				  },
   				legend: {
   					
            		position: 'right',
            		labels:{
						boxWidth :10,
						}
                },
				title: 
				{
			        display: true,
			        text: ' '
			    },
			    tooltips: {
			    
		            callbacks: {
		                label: function(tooltipItem, data) {
		                    var allData = data.datasets[tooltipItem.datasetIndex].data;
		                    var tooltipLabel = data.labels[tooltipItem.index];
		                    var tooltipData = allData[tooltipItem.index];
		               
		                    var total = 0;
		                     for (var i=0; i<allData.length; i++) {
							                    total = +total + +allData[i];
							                }
		                   
		                    var tooltipPercentage = Math.round((tooltipData / total) * 100);
		                    
		                    
		                    return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
		                } 
		            }
		        }
			}
		});
		$chartp = chartp;
    }
    
    function click(e) {
	    var activePoints = $chart.getElementsAtEvent(e);
	    var firstPoint = activePoints[0];
		var label1 = $chart.data.labels[firstPoint._index];
		var value2 = $chart.data.datasets[firstPoint._datasetIndex].label[firstPoint._index];
		//alert(value2);
	    var value = $chart.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
	   // alert(label1 + ": " + value);
	    getDetails(value2);
	}
	
	function getDetails(notId){
	
		$.ajax({
			url : path + "/workspaceAction.do",
			type : "POST",
			data : {
						act : "getDetails",
						notId : notId,
						
					},
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
			success : function(result){
			$('.view').html("");
			$('.view').html(result);
			$('.viewBorder').css({'border-right':'1px solid rgb(191, 191, 191)'});
			$('#notifListMain_'+notId).css({'border-top':'1px solid rgb(191, 191, 191)','width':'96%'});
			$('.notfContentCls').each(function(){
				var title = $(this).attr('title');													 
				title= unicodeTotitle(title);				
				$(this).attr('title',title);
				$(this).html(title);
				
			});
			
			}
		});
	
	}
	function selectScal(obj){
         $('.sBac').removeClass("sCalBcActive");
	     $(obj).addClass("sCalBcActive");
	     $("#chart-container_"+sProjId).css({'display':'none'});
	     
	     if(sProjId != "" && sUserId != ""){
	     	$('#lis_'+sProjId+'_'+sUserId).trigger('click');
	     }else{
	     	$('#projImgMainDiv_'+sProjId).trigger('click');
	     }
	     
     }
     
     function sentimentPerformance(reportType,proId,id){
     	 
					$.ajax({
						url: path + "/reportAction.do" ,
						type:"POST",
						data:{action:"sentimentPerformance",reportType:reportType,projectId:proId,userid:id},
						error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$("#loadingBar").hide();
						timerControl("");
							}, 
						    success:function(result){
						   
						  	if(reportType == "project"){
						  		$('.projectP').css({'display':'block'});
						    	$('.userP').css({'display':'none'});
							    	if(result.length >1)
						    	{
						    		var subArray = ['Done','Not Done', 'Exceeded'];
								    var subDateArray=[];
								    var doneArray=[];
								    var notDoneArray=[];
								    var excededArray=[];
								    var jsonData=jQuery.parseJSON(result);
							        for(var i=0; i<jsonData.length; i++)
							        {
							        	//subArray[i]=jsonData[i].doneTask;
							        	doneArray[i]=jsonData[i].doneTask;
							        	notDoneArray[i]=jsonData[i].notDoneTask;
							        	excededArray[i]=jsonData[i].excededTask;
							        }
							        var subDateArray =[doneArray,notDoneArray,excededArray]
								}
								$("#head_"+proId).html("").text("Overall Project Performance (Tasks) :");
						    	$(".projectP").css({'height':'27vh','margin-bottom':'0%'});
						    	$('.taskU').css({'display':'none'});
						    	creatChartP(subArray,subDateArray,proId);
						    	}else{
						    	
						    		$('.projectP').css({'display':'block'});
						    		$('.userP').css({'display':'none'});
						    		$('.taskU').css({'display':'block'});
						    		
						    		$("#scHead_"+proId).css({'display':'block'});
						    		$("#ScHead_"+proId).css({'display':'block'});
						    		$("#head_"+proId).html("").text("Performance (Tasks) :");
						    		$("#TotalTask_"+proId).html("").text("Total Tasks");
									$("#TaskCompleted_"+proId).html("").text("Tasks Completed");
									$("#TaskNotComp_"+proId).html("").text("Project Contribution Index");
									$("#TaskBeyond_"+proId).html("").text("Performance Index ");
									$(".viewPef").css({'height':'20vh','margin-top':'-2%'});
									$(".projectP").css({'height':'22vh','margin-bottom':'0%'});
									$("#head_"+proId).css({'margin-bottom':'-5%'});
									if(result.length >1)
						    	{
						    		var subArray = ['Done','Not Done'];
								    var subDateArray=[];
								    var doneArray="";
								    var notDone="";
								    var totalTask ="";
								    var jsonData=jQuery.parseJSON(result);
							        for(var i=0; i<jsonData.length; i++)
							        {
							        	//subArray[i]=jsonData[i].doneTask;
							        	doneArray=jsonData[i].doneTask;
							        	totalTask=jsonData[i].totalTask;
							        	var taskBeoyendEndDate = jsonData[i].taskScore;
									    var projContri = jsonData[i].projectContribution;
							        	
							        }
							        notDone = totalTask - doneArray;
							        var subDateArray =[doneArray,notDone]
								}
						    	
						    	creatChartP(subArray,subDateArray,proId);
									
								/*	if(result.length >1){
									alert("result---"+result);
										    var jsonData=jQuery.parseJSON(result);
									        for(var i=0; i<jsonData.length; i++){
									         var taskCopleted = jsonData[i].doneTask;
									        // var taskNotCopleted = jsonData[i].notDoneTask;
									         var taskBeoyendEndDate = jsonData[i].taskScore;
									         var totalTask = jsonData[i].totalTask;
									         var projContri = jsonData[i].projectContribution;
									       // alert(taskCopleted +"--"+taskNotCopleted+"--"+taskBeoyendEndDate+"--"+totalTask+"--projContri--"+projContri);
									       
							    		}
							    			$("#totalTask_"+proId).text(totalTask);
							    			$("#taskBeyond_"+proId).text(taskBeoyendEndDate);
									        $("#taskCompleted_"+proId).text(taskCopleted);
									        $("#taskNotComp_"+proId).text(projContri);
									        //$(".taskNotComp_"+proId).css({'display':'none'});
									        
							    		} */
							    		
									        
									    if(projContri=="null"){
									       	 projContri = 0.00;
									    }
									    if(taskBeoyendEndDate=="null"){
									       	 taskBeoyendEndDate = 0.00;
									     }
							    		$("#taskBeyond_"+proId).text(taskBeoyendEndDate);
							    		$("#taskNotComp_"+proId).text(projContri);
						    	} 
						    	
						    }
					 });
     
     }
     
      function viewStatusGraph(proId,id,reportType){
    	$('.sCal').css({'display':'block'});
    	var fType = $(".sCalBcActive").attr('id');
    	if(reportType == "user"){
		    	sProjId = proId;
		    	sUserId = id;
		    	$("#chart-container_"+proId).css({'display':'block'});
		    	$(".borderStyle").css({'border':'0px solid rgb(191, 191, 191)','border-bottom-color':'rgb(191, 191, 191)','background-color':''});
		    	$("#lis_"+proId+'_'+id).css({'border':'1px solid rgb(191, 191, 191)','border-bottom-color':'white','background-color':'white'});
		    	$(".chartcontain").css({'height':'54vh','padding-top':'0%'});
		    	$(".pStyle").removeAttr("style");
		    	
		    	
		    	var localOffsetTime=getTimeOffset(new Date());
		    	$("#loadingBar").show();
				timerControl("start");
				
					$.ajax({
						url: path + "/reportAction.do" ,
						type:"POST",
						data:{action:"fetchStatusAnalysis",reportType:reportType,localOffsetTime:localOffsetTime,projectId:proId,fType:fType,userid:id},
						error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$("#loadingBar").hide();
						timerControl("");
							}, 
						    success:function(result){
						    
								    checkSessionTimeOut(result);
								    var subEstArray = [];
								    var subActArray = [];
								    var subYArray=[];
								    var subXArray=[];
								   	if(result.length >1){
									    var jData=jQuery.parseJSON(result);
									   	var jsonData = jData[0].projData;
										for(var i=0; i<jsonData.length; i++){
											if(fType == "1D" || fType == "1W" || fType == "1M"  || fType =="2M" || fType == "3M" ){
								        		subXArray[i]=jsonData[i].task.split(" ")[0]+"...";
								        	}else{
								        		subXArray[i]=jsonData[i].task;
								        	}
								        	subEstArray[i]=jsonData[i].estHours;
								        	subActArray[i]=jsonData[i].actHours;
								        }
								      	creatStatusChart(subXArray,subEstArray,subActArray,proId);
							        }else{
							        	creatStatusChart(subXArray,subEstArray,subActArray,proId);
							        }
							       
							        $('.viewBorder').css({'border-right':'1px solid rgb(191, 191, 191)'});
							        $('.view').html("");
							        statusPerformance(reportType,proId,id);
							        details(reportType,proId,id);
								    $("#loadingBar").hide();
									timerControl("");
			   			   	 }
		    		});
    	
    	}else{
		    	sProjId = proId;
		    	sUserId = id;
		    	
		    	if($("#chart-container_"+proId).is(':visible')){
		    		$("#chart-container_"+proId).css({'display':'none'});
		    		$(".pStyle").removeAttr("style");
		    		$(".borderStyle").css({'border':'0px solid rgb(191, 191, 191)','border-bottom-color':'rgb(191, 191, 191)','background-color':''});
		    	}else{
		    	
		    		$("#chart-container_"+proId).css({'display':'block'});
		    		$("#chart-container_"+proId).css({'display':'block'});
		    		$(".borderStyle").css({'border':'0px solid rgb(191, 191, 191)','border-bottom-color':'rgb(191, 191, 191)'});
		    		$("#lis_"+proId+'_'+id).css({'border':'1px solid rgb(191, 191, 191)','border-bottom-color':'white'});
		    		
				    		
				    		$(".pStyle").removeAttr("style");
				    		$(".chartcontain").css({'height':'54vh','padding-top':'0%'});
				    		$("#proj_"+proId).css({ 'border': '1px solid rgb(191, 191, 191)','background-color':'white','height':'115px','width':'8.1vw','margin-left':'-2vw','padding-left':'0.9vw','border-bottom-color':'white'});
				    	
		    	var localOffsetTime=getTimeOffset(new Date());
		    	$("#loadingBar").show();
				timerControl("start");
				
					$.ajax({
						url: path + "/reportAction.do" ,
						type:"POST",
						data:{action:"fetchStatusAnalysis",reportType:reportType,localOffsetTime:localOffsetTime,projectId:proId,fType:fType},
						error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$("#loadingBar").hide();
						timerControl("");
							}, 
						    success:function(result){
						    
								    checkSessionTimeOut(result);
								    var subEstArray = [];
								    var subActArray = [];
								    var subYArray=[];
								    var subXArray=[];
								   	if(result.length >1){
									    var jData=jQuery.parseJSON(result);
									   	var jsonData = jData[0].projData;
										for(var i=0; i<jsonData.length; i++){
								        	subXArray[i]=jsonData[i].userName;
								        	subEstArray[i]=jsonData[i].estHours;
								        	subActArray[i]=jsonData[i].actHours;
								        }
								      	creatStatusChart(subXArray,subEstArray,subActArray,proId);
							        }else{
							        	creatStatusChart(subXArray,subEstArray,subActArray,proId);
							        }
							       
							        $('.viewBorder').css({'border-right':'1px solid rgb(191, 191, 191)'});
							        $('.view').html("");
							        statusPerformance(reportType,proId);
							        details(reportType,proId);
								    $("#loadingBar").hide();
									timerControl("");
			   			   	 }
		    		});
		    	}
	    	}
	    	
		    	
    	
    }
     
    function details(reportType,proId,id){
    	var localOffsetTime=getTimeOffset(new Date());
		$("#loadingBar").show();
		timerControl("start");
			
			if(reportType == "user"){
				$.ajax({
					url: path + "/reportAction.do" ,
					type:"POST",
					data:{action:"details",reportType:reportType,localOffsetTime:localOffsetTime,projectId:proId,userid:id},
					error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					$("#loadingBar").hide();
					timerControl("");
						}, 
					    success:function(result){
					   
					    var estHours="";
					    var actHours="";
					    if(result.length >1)
						    	{
						    		
								    var jsonData=jQuery.parseJSON(result);
							        for(var i=0; i<jsonData.length; i++)
							        {
							        	estHours=jsonData[i].estHours;
							        	actHours=jsonData[i].actHours;
							        	
							        }
							  	
								$('.userP').css({'display':'block'});
								$('.taskU').css({'display':'block'});
								$('#scHead_'+proId).css({'display':'none'});
								$('#ScHead_'+proId).css({'display':'none'});
								$("#TaskBeyond_"+proId).html("").text("Estimated Hours");
								$("#TaskNotComp_"+proId).html("").text("Actual Hours");
					    		
							    
							     if(estHours=="null"){
									       	 estHours ="0";
								}
								 if(actHours=="null"){
									       	 actHours = "0";
								}
							    
					    		$("#taskBeyond_"+proId).text(estHours);
							    $("#taskNotComp_"+proId).text(actHours);
					    }
					    }
					    
					});
			
			
			
			}else{
				$.ajax({
					url: path + "/reportAction.do" ,
					type:"POST",
					data:{action:"details",reportType:reportType,localOffsetTime:localOffsetTime,projectId:proId},
					error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					$("#loadingBar").hide();
					timerControl("");
						}, 
					    success:function(result){
					   
					    var estHours="";
					    var actHours="";
					    var resourses="";
					    var noOfTasks="";
					   	if(result.length >1)
						    	{
						    		
								    var jsonData=jQuery.parseJSON(result);
							        for(var i=0; i<jsonData.length; i++)
							        {
							        	estHours=jsonData[i].estHours;
							        	actHours=jsonData[i].actHours;
							        	resourses=jsonData[i].resourses;
							        	noOfTasks=jsonData[i].noOfTasks;
							        }
							   
								$('.userP').css({'display':'block'});
								$('.taskU').css({'display':'block'});
								$('#scHead_'+proId).css({'display':'none'});
								$('#ScHead_'+proId).css({'display':'none'});
								$("#TotalTask_"+proId).html("").text(getValues(companyLabels,"Resources"));
								$("#TaskCompleted_"+proId).html("").text(getValues(companyLabels,"Total_Tasks"));
								$("#TaskBeyond_"+proId).html("").text(getValues(companyLabels,"Estimated_Hour"));
								$("#TaskNotComp_"+proId).html("").text(getValues(companyLabels,"Actual_Hour"));
					    		$("#totalTask_"+proId).text(": "+resourses);
							    $("#taskCompleted_"+proId).text(": "+noOfTasks);
							    
							     if(estHours=="null"){
									       	 estHours ="0";
								}
								 if(actHours=="null"){
									       	 actHours = "0";
								}
							    
					    		$("#taskBeyond_"+proId).text(estHours);
							    $("#taskNotComp_"+proId).text(actHours);
					    }
					    }
					    
					});
    		}
    }
    
  
    function creatStatusChart(subXArray,subEstArray,subActArray,proId)
    {
     	if (typeof $chart !== "undefined") 
     	{
			$chart.destroy();
		}
		
		$('.chartcontain').css({'display':'none'});
		$('#chart-container_'+proId).css({'display':'block'});
   		var ctx = document.getElementById('myChart_'+proId).getContext('2d');
		ctx.height = 100;
		chart = new Chart(ctx, 
		{
		
		    type: 'bar',
			
			data:
		    {
				labels: subXArray,
				datasets: 
				[{
					label:"Estimated Hours",
		            backgroundColor: "#EE82EE",
		    		data: subEstArray,
		    		padding:10,
				},{
					label:"Actual Hours",
		            backgroundColor: "#6495ED",
		    		data: subActArray,
				}]
			},
			options: 
			{
				
				responsive : true,
          		showTooltips: true,
				maintainAspectRatio: false,
				scales: 
				{
						xAxes: 
							[{
								
								barPercentage: 0.9,
								gridLines: 
									{
										display:false
									},
								ticks: 
									{
										autoSkip:false,
							            stepSize: 1,
							            min: 0,
							            minRotation: 30
									}
							}],
						yAxes: 
							[{
								
								display: true,
                            	
                            	ticks: {
                
							                beginAtZero: true  ,
							            }
							}]
				},
				legend: 
				{
						display: true,
						labels:{
						boxWidth :10,
						}
				},
				tooltips: 
				{
				
						callbacks: 
						{
							label: function(tooltipItem) 
							{
								
								return tooltipItem.yLabel ;
							}
						}
				}
				
			}
		});
		$chart = chart;
    }  
    
    
    function statusPerformance(reportType,proId,id){
     	 
					$.ajax({
						url: path + "/reportAction.do" ,
						type:"POST",
						data:{action:"sentimentPerformance",reportType:reportType,projectId:proId,userid:id},
						error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$("#loadingBar").hide();
						timerControl("");
							}, 
						    success:function(result){
						    $('.userP').css({'display':'block','margin-top':'7%'});
							$('.taskU').css({'display':'block'});
						  	if(reportType == "project"){
						  		$('.projectP').css({'display':'block'});
						    	$('.userP').css({'display':'none'});
						    	$('#TotalTask_'+proId).css({'display':'block'});
								$('#totalTask_'+proId).css({'display':'block'});
							    	if(result.length >1)
						    	{
						    		var subArray = ['Done','Not Done', 'Exceeded'];
								    var subDateArray=[];
								    var doneArray=[];
								    var notDoneArray=[];
								    var excededArray=[];
								    var jsonData=jQuery.parseJSON(result);
							        for(var i=0; i<jsonData.length; i++)
							        {
							        	//subArray[i]=jsonData[i].doneTask;
							        	doneArray[i]=jsonData[i].doneTask;
							        	notDoneArray[i]=jsonData[i].notDoneTask;
							        	excededArray[i]=jsonData[i].excededTask;
							        }
							        var subDateArray =[doneArray,notDoneArray,excededArray]
								}
								$("#head_"+proId).html("").text("Overall Project Performance (Tasks) :");
						    	$(".projectP").css({'height':'27vh','margin-bottom':'0%'});
						    	
						    	creatChartP(subArray,subDateArray,proId);
						    	}else{
						    	
						    	$('#TotalTask_'+proId).css({'display':'none'});
								$('#totalTask_'+proId).css({'display':'none'});
								$("#head_"+proId).html("").text("Performance (Tasks) :");
									if(result.length >1)
						    	{
						    		var subArray = ['Done','Not Done'];
								    var subDateArray=[];
								    var doneArray="";
								    var notDone="";
								    var totalTask ="";
								    var jsonData=jQuery.parseJSON(result);
							        for(var i=0; i<jsonData.length; i++)
							        {
							        	//subArray[i]=jsonData[i].doneTask;
							        	doneArray=jsonData[i].doneTask;
							        	totalTask=jsonData[i].totalTask;
							        	var taskBeoyendEndDate = jsonData[i].taskScore;
									    var projContri = jsonData[i].projectContribution;
							        	
							        }
							        notDone = totalTask - doneArray;
							        var subDateArray =[doneArray,notDone]
							        $("#TaskCompleted_"+proId).html("").text("Total Tasks");
						    		$("#taskCompleted_"+proId).text(": "+totalTask);
								}
						    	
						    	creatChartP(subArray,subDateArray,proId);
									
								
									   
						    	} 
						    	
						    }
					 });
     
     }
      var limit = 20;
      var index = 0;
      var sortType ="";
      var senderType ="";
      var loadData="false";
     function loadProjectsJson(sortType1,senderType1){
      loadData ="true";
      sortType = sortType1;
      senderType = senderType1;
      
      var filterType = $('#filterStatus').val();
  	  var sType = $('#sortStatus').val();
  	  var Type = $('#statusSearchBox').val().toLowerCase();
  	 // alert("filterType--"+filterType+"--sType--"+sType+"--Type--"+Type);
      $("#loadingBar").show();
      timerControl("start");
	  var isiPad = navigator.userAgent.match(/iPad/i) != null;
	  if (isiPad) {
		 ipadVal = "YES";
	  } else {
		ipadVal = "NO";
	  }	
	  $.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : {
					act : "loadMyProjDetailsUsingJson",
					userId : userId,
					sortValue : "",
					sortHideProjValue : "",
					ipadVal : ipadVal,
					sortType: sortType,
					senderType: senderType,
					limit: limit,
					index: index,
					filterType:filterType,
					sType:sType,
					Type:Type,
				},
		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
		success : function(result){
		
			checkSessionTimeOut(result);
			jsonData = jQuery.parseJSON(result);
			
            if(jsonData.length >0){
	            index = limit + index;
	          }else{
            	index = 0;
            	act = "false";
            
            }
            
            scrollLimit=0;
             $(window).scrollTop(0);
            if(sortType == "sentimentscore"){
	          		//$('#workspaceSearchDiv').css({'display':'none'});
            	if(senderType=='jira'){
            		data =" > "+$("#jiraSentianly").addClass("Jira_Sentiment_cLabelHtml").html();
            	}else{
            		data =" > "+$("#sentianly").addClass("Sentiment_cLabelHtml").html();
            	}
	          	
		        $('#standardReport').hide();
				$('#managementReport').css("display","none");
			    $("div#reportContentDiv").hide();
				$("div#semanticAnalysis").show();
				$('li.importCsv').hide();
			    $("#semanticAnalysisHeader").hide();
			   	$('#semanticAnalysisContainer').hide();
			   	$("div#semanticAnalysis").css({'margin-top':'1%'})
			   	$(".breadcrumMain").css({'border-bottom':'0px solid #ccc'});
			   //	if(loadData=="false"){
			    $("div#chgTxt").html("");
			   	$("div#chgTxt").append(data);
			   	$("#searchStatusProject").css("display","block");
			   	$(".topTabSearchIconCls").css("display","block");
			   	$("div#semanticAnalysis").html("");
			   	//}
			   
			   	prepareStatusOrSentimentUI(sortType,jsonData);
			   	loadCustomLabelAnalytics('TodayRepo');
		        	
	        }else if(sortType == "status"){
	        
	        	data =" > "+$("#statusprj").addClass("Project_Status_cLabelHtml").html();
		   		$('#standardReport').hide();
		   		$('#managementReport').css("display","none");
		   		$('div#reportContentDiv').hide();
		   		$('div#statusContainer').show();
		   		$('div#projectstatusDetailHeader').hide();
		   		$("div#statusContainer").css({'margin-top':'1%'})
          		// if(loadData=="false"){
          		 jsonScroll ="";
	           	 $("div#statusContainer").html("");	 
	           	 $("div#chgTxt").html("");
	           	 $("div#chgTxt").append(data);
	           	 $("#searchStatusProject").css("display","block");
	           	 $(".topTabSearchIconCls").css("display","block");
	           // }
	           
	            
	        	prepareStatusOrSentimentUI(sortType,jsonData);
	        	loadCustomLabelAnalytics('TodayRepo');
				
	        }
	        $("#loadingBar").hide();
		    timerControl("");
		}
	});
  }
  
  function prepareUIforteam(data,projStatus){
 
  	var UI = "";
  	if(projStatus == "PO" || projStatus== "undefined" ){
  		for (var j = 0; j < data.length ; j++) {
	  			var imgUrl = data[j].imgName
	  			var userid = data[j].userId
	  			var userName = data[j].userName
	  			var sentimentscore = data[j].sentimentscore
	  			
	  			
	  		 UI += '<li id="lis_'+data[j].projectid+"_"+data[j].userId+'" class="borderStyle item" onclick =viewSentimentGraph('+data[j].projectid+','+data[j].userId+',"user",'+sentimentscore+') style="cursor:pointer;width:66px;height:115px;margin-top:0px;list-style:none;" >'
		  		   +'<div style="width: 51px;margin-left: 10%;">'
			  		   
			  		   	     +'<div id="sysScore_'+data[j].projectid+"_"+data[j].userId+'" style="display:none;color:black;font-size:9px;font-weight:bold;text-align:center;background:#FFC200;height:20%;width:26%;margin-top:-7%;margin-left:53%;position:absolute;border-radius:50%"></div>'
			  		   		
			  		   		+"<img class='imageHover' title='"+userName+"' onerror='userImageOnErrorReplace(this);' src='"+imgUrl+"' style='height:50px;width:50px;border-radius:50%;border: 1px solid rgb(191, 191, 191);'></img>"
			  		   		+"<div id='tmName_"+data[j].projectid+"_"+data[j].userId+"' class='defaultExceedCls' style='text-align:center;width:56px;font-family: OpenSansRegular;font-size: 11px;font-weight: bold;color:black'>"+userName+"</div>"	
			  		   
		  		   +"</div>"
	  		    +"</li>"
	  			  
	  	}
	}else{
	
		for (var j = 0; j < data.length ; j++) {
	  			var imgUrl = data[j].imgName
	  			var userid = data[j].userId
	  			var userName = data[j].userName
	  			var sentimentscore = data[j].sentimentscore
	  			var status = data[j].status
	  		if(userid == userId){
			  		 UI += '<li id="lis_'+data[j].projectid+"_"+data[j].userId+'" class="borderStyle item" onclick =viewSentimentGraph('+data[j].projectid+','+data[j].userId+',"user",'+sentimentscore+') style="cursor:pointer;width:66px;height:18vh;margin-top:0.1vh;list-style:none;" >'
				  		   +'<div style="width: 51px;margin-left: 10%;">'
					  		   
					  		   	     +'<div id="sysScore_'+data[j].projectid+"_"+data[j].userId+'" style="display:none;color:black;font-size:9px;font-weight:bold;text-align:center;background:#FFC200;height:20%;width:26%;margin-top:-7%;margin-left:53%;position:absolute;border-radius:50%"></div>'
					  		   		
					  		   		+"<img class='imageHover' title='"+userName+"' onerror='userImageOnErrorReplace(this);' src='"+imgUrl+"' style='height:50px;width:50px;border-radius:50%;border: 1px solid rgb(191, 191, 191);'></img>"
					  		   		+"<div id='tmName_"+data[j].projectid+"_"+data[j].userId+"' class='defaultExceedCls' style='text-align:center;width:56px;font-family: OpenSansRegular;font-size: 11px;font-weight: bold;color:black'>"+userName+"</div>"	
					  		   
				  		   +"</div>"
			  		    +"</li>"
			  			  
			  }else{
				  	 UI += '<li id="lis_'+data[j].projectid+"_"+data[j].userId+'" class="borderStyle item"  style="width:66px;height:18vh;margin-top:0.1vh;list-style:none;" >'
					  		   +'<div style="width: 51px;margin-left: 10%;">'
						  		   
						  		   	     +'<div id="sysScore_'+data[j].projectid+"_"+data[j].userId+'" style="display:none;color:black;font-size:9px;font-weight:bold;text-align:center;background:#FFC200;height:20%;width:26%;margin-top:-7%;margin-left:53%;position:absolute;border-radius:50%"></div>'
						  		   		
						  		   		+"<img class='imageHover' title='"+userName+"' onerror='userImageOnErrorReplace(this);' src='"+imgUrl+"' style='height:50px;width:50px;border-radius:50px;border: 1px solid rgb(191, 191, 191);'></img>"
						  		   		+"<div id='tmName_"+data[j].projectid+"_"+data[j].userId+"' class='defaultExceedCls' style='text-align:center;width:56px;font-family: OpenSansRegular;font-size: 11px;font-weight: bold;color:black'>"+userName+"</div>"	
						  		   
					  		   +"</div>"
				  		    +"</li>"
			  	
			  	}
	
		}	
	}
	return UI;
  }
  
  function prepareUIforStatusteam(data,senderType,projStatus){
  	var UI = "";
  	if(senderType == "mgm"){
  		for (var j = 0; j < data.length ; j++) {
  			var imgUrl = data[j].imgName
  			var userid = data[j].userId
  			var userName = data[j].userName
  			var sentimentscore = data[j].sentimentscore
  			 
  			
  		 UI += '<li id="lis_'+data[j].projectid+"_"+data[j].userId+'" class="borderStyle" onclick =viewStatusGraph('+data[j].projectid+','+data[j].userId+',"user") style="cursor:pointer;width:66px;height:18vh;margin-top:0.1vh;list-style:none;" >'
	  		   +'<div style="width: 51px;margin-left: 10%;">'
		  		   
		  		   	     +'<div id="sysScore_'+data[j].projectid+"_"+data[j].userId+'" style="display:none;color:black;font-size:9px;font-weight:bold;text-align:center;background:#FFC200;height:20%;width:26%;margin-top:-7%;margin-left:53%;position:absolute;border-radius:50%"></div>'
		  		   		
		  		   		+"<img class='imageHover' title='"+userName+"' onerror='userImageOnErrorReplace(this);' src='"+imgUrl+"' style='height:50px;width:50px;border-radius:50%;border: 1px solid rgb(191, 191, 191);margin-top:4vh'></img>"
		  		   		+"<div id='tmName_"+data[j].projectid+"_"+data[j].userId+"' class='defaultExceedCls' style='text-align:center;width:56px;font-family: OpenSansRegular;font-size: 11px;font-weight: bold;color:black'>"+userName+"</div>"	
		  		   
	  		   +"</div>"
  		    +"</li>"
  			  
  	}
  	
  	
  	}else{
  	if(projStatus == "PO"){
  	for (var j = 0; j < data.length ; j++) {
  			var imgUrl = data[j].imgName
  			var userid = data[j].userId
  			var userName = data[j].userName
  			var sentimentscore = data[j].sentimentscore
  			 
  			
  		 UI += '<li id="lis_'+data[j].projectid+"_"+data[j].userId+'" class="borderStyle" onclick =viewStatusGraph('+data[j].projectid+','+data[j].userId+',"user") style="cursor:pointer;width:66px;height:18vh;margin-top:0.1vh;list-style:none;" >'
	  		   +'<div style="width: 51px;margin-left: 10%;">'
		  		   
		  		   	     +'<div id="sysScore_'+data[j].projectid+"_"+data[j].userId+'" style="display:none;color:black;font-size:9px;font-weight:bold;text-align:center;background:#FFC200;height:20%;width:26%;margin-top:-7%;margin-left:53%;position:absolute;border-radius:50%"></div>'
		  		   		
		  		   		+"<img class='imageHover' title='"+userName+"' onerror='userImageOnErrorReplace(this);' src='"+imgUrl+"' style='height:50px;width:50px;border-radius:50%;border: 1px solid rgb(191, 191, 191);margin-top:4vh'></img>"
		  		   		+"<div id='tmName_"+data[j].projectid+"_"+data[j].userId+"' class='defaultExceedCls' style='text-align:center;width:56px;font-family: OpenSansRegular;font-size: 11px;font-weight: bold;color:black'>"+userName+"</div>"	
		  		   
	  		   +"</div>"
  		    +"</li>"
  			  
  	}
	}else{
	for (var j = 0; j < data.length ; j++) {
	  			var imgUrl = data[j].imgName
	  			var userid = data[j].userId
	  			var userName = data[j].userName
	  			var sentimentscore = data[j].sentimentscore
		  	if(userid == userId){	 
		  			
		  		 UI += '<li id="lis_'+data[j].projectid+"_"+data[j].userId+'" class="borderStyle" onclick =viewStatusGraph('+data[j].projectid+','+data[j].userId+',"user") style="cursor:pointer;width:66px;height:18vh;margin-top:0.1vh;list-style:none;" >'
			  		   +'<div style="width: 51px;margin-left: 10%;">'
				  		   
				  		   	     +'<div id="sysScore_'+data[j].projectid+"_"+data[j].userId+'" style="display:none;color:black;font-size:9px;font-weight:bold;text-align:center;background:#FFC200;height:20%;width:26%;margin-top:-7%;margin-left:53%;position:absolute;border-radius:50%"></div>'
				  		   		
				  		   		+"<img class='imageHover' title='"+userName+"' onerror='userImageOnErrorReplace(this);' src='"+imgUrl+"' style='height:50px;width:50px;border-radius:50%;border: 1px solid rgb(191, 191, 191);margin-top:4vh'></img>"
				  		   		+"<div id='tmName_"+data[j].projectid+"_"+data[j].userId+"' class='defaultExceedCls' style='text-align:center;width:56px;font-family: OpenSansRegular;font-size: 11px;font-weight: bold;color:black'>"+userName+"</div>"	
				  		   
			  		   +"</div>"
		  		    +"</li>"
		  	}else{
		  				UI += '<li id="lis_'+data[j].projectid+"_"+data[j].userId+'" class="borderStyle"  style="width:66px;height:18vh;margin-top:0.1vh;list-style:none;" >'
				  		   +'<div style="width: 51px;margin-left: 10%;">'
					  		   
					  		   	     +'<div id="sysScore_'+data[j].projectid+"_"+data[j].userId+'" style="display:none;color:black;font-size:9px;font-weight:bold;text-align:center;background:#FFC200;height:20%;width:26%;margin-top:-7%;margin-left:53%;position:absolute;border-radius:50%"></div>'
					  		   		
					  		   		+"<img class='imageHover' title='"+userName+"' onerror='userImageOnErrorReplace(this);' src='"+imgUrl+"' style='height:50px;width:50px;border-radius:50px;border: 1px solid rgb(191, 191, 191);margin-top:4vh'></img>"
					  		   		+"<div id='tmName_"+data[j].projectid+"_"+data[j].userId+"' class='defaultExceedCls' style='text-align:center;width:56px;font-family: OpenSansRegular;font-size: 11px;font-weight: bold;'>"+userName+"</div>"	
					  		   
				  		   +"</div>"
			  		    +"</li>"
			  	
			  	}		  
	  	}
		
	}
	}	
	return UI;
  }

 var scrollLimit=0;
 function prepareStatusOrSentimentUI(sortType,jsonData){	 
    jsonScroll = jsonData;
    var defImage = lighttpdpath + "/projectimages/dummyLogo.png";
    var Ui ="";
    var team ="";
	var teamUI="";
	var projStatus ="";
	var ctxPath = "";
	var storyData="";
	var j = 0;
  		if(sortType == "sentimentscore"){
	    //alert('scrollLimit:'+scrollLimit);
		        	var sentiment_code = "";
			        var projSentimentSrc = "";
			        var status_code = "";
		        	for( var i=scrollLimit;i< jsonData.length;  i++ ){
		        	    teamUI ="";
		        	    team = jsonData[i].teamMembers;
		        	    storyData = jsonData[i].storyData; // this is used for Jira analysis report
		        	    ctxPath = jsonData[i].ctxPath;
		        	    if(jsonData[i].sentimentScore >=0.25 && jsonData[i].sentimentScore <=1){
			        	   sentiment_code = '#008000';
			        	}else if(jsonData[i].sentimentScore >=-0.25 && jsonData[i].sentimentScore <0.25){
			        	   sentiment_code = '#FFC200';
			        	}else{
			        	   sentiment_code ='#ff0000';
			        	}
				        
				        if(jsonData[i].projOverallStatus =="99"){
			        	   status_code ='#00783B';
			        	}else if(jsonData[i].projOverallStatus == "100"){
			        		status_code ='#00A652';
			        	}else if(jsonData[i].projOverallStatus == "101"){
			        		status_code ='#00D466';
			        	}else if(jsonData[i].projOverallStatus == "102"){
			        		status_code ='#F1DB14';
			        	}else if(jsonData[i].projOverallStatus == "103"){
			        		status_code ='#FCB040';
			        	}else{
			        		status_code ='#C52C31';
			        	}
				        
	        			projSentimentSrc="";
		          		if(jsonData[i].projStatus !==""){
		        			if(jsonData[i].projStatus =="PO"){
				        	   projSentimentSrc= ctxPath+'/images/myprojects.png';
				        	   // $('#A_'+jsonData[l].projectID).attr('title', 'owned by');
				        	}else{
				        	    projSentimentSrc= ctxPath+'/images/sharedprojects.png';
				        	 	//$('#A_'+jsonData[l].projectID).attr('title', 'shared by');
				        	}
			        	}else{
			        	   projSentimentSrc="";
			        	}
		        	    
		        	 	Ui += '	<div class="row projImgMainDiv_'+jsonData[i].projectID+'"  style="background:white;margin-right:1vw;border-bottom: 1px solid #ccc;height:115px;"> '
	       	        	+'<div class="col-lg-1 col-md-1 col-sm-1 col-xs-6" id="projImgMainDiv_'+jsonData[i].projectID+'"  onclick =viewSentimentGraph('+jsonData[i].projectID+',"","project",'+jsonData[i].sentimentScore+') style="cursor:pointer;height:115px;padding-top: 7px;padding-left:2vw"> ' 
		       	        	+'<div class="pStyle row" id="proj_'+jsonData[i].projectID+'">'
		       	        	+'<div id="sysScore_'+jsonData[i].projectID+'" style="background-color:'+sentiment_code+';color:white;font-size:9px;font-weight:bold;text-align:center;height:19px;width:19px;margin-top:1.4vh;margin-left:4.4vw;position:absolute;border-radius:2em"></div>'
		       	        	+'<div style="margin-top:3vh;margin-left:10px;position:absolute"><img id="A_'+jsonData[i].projectID+'"  class="shareUserIcon"  title=\"\" src=\"'+projSentimentSrc+'\"  projectType=\"my_project\"  projectId=\"'+jsonData[i].projectID+'\"   /></div>'
		       	    		+'<img onerror="imageOnProjErrorReplace(this)" src="'+jsonData[i].imageUrl+'" style="width:60px;height:60px;margin-left:11%;margin-top:3vh;margin-bottom:1%;border: 1px solid rgb(191, 191, 191);"  ></img>'
		       	        	+'<div id="proStatus_'+jsonData[i].projectID+'"  style="background-color:'+status_code+';float: left;width: 60px;margin-top: -1%;margin-left:11%;height: 5px;clear: both;" ></div>'
		       	        	+'<div class="defaultExceedCls" style="width:62px;text-align:center;font-family: OpenSansRegular;font-size: 11px;font-weight:bold;margin-left:10%" id="projName_'+jsonData[i].projectID+'">'+jsonData[i].ProjTitle+'</div>'
		       	       		+'</div>'
		       	        +'</div>'
	       	        	+'<div class="col-lg-11 col-md-11 col-sm-11 col-xs-6"  style="height:115px;"> ' 
		       	        	+'<div id="b_'+jsonData[i].projectID+'" class="row" style="float: left;width: 100%;height: 115px;">'
			       	        	+'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:115px;">'
				       	        	+'<ul style="height:115px;display: inline-flex;width:105%" id="teamMember_'+jsonData[i].projectID+'"  class="sliderUL owl-carousel owl-theme" >'
				       	        	+ prepareUIforteam(team,"undefined")
				       	        	+'</ul>'
			       	        	+'</div>'
		       	        	+'</div>'
	       	        	+'</div>'
	       	        +'</div> ';
	       	       	
	       	       Ui+='<div class="row chartcontain"  id="chart-container_'+jsonData[i].projectID+'" style="background:white;height:54vh;margin-right:1%;border-bottom: 1px solid #ccc;display:none;z-index:100">'
		       	       	    +'<div id ="avgScore_'+jsonData[i].projectID+'" class="row " style="padding-top:1%;">'
		       	       	    	+'<div align="right"  id="chart-filter_'+jsonData[i].projectID+'"  class="col-md-3" style="visibility:hidden;">'
		       	       	    	+ prepareJiraStoryUI(storyData, jsonData[i].projectID)
		       	       	    	+'</div>'
		       	       	    	+'<div class="col-md-2 Average_Score_cLabelHtml" style="margin-left:-2%;height: 5vh;padding-top: 1vh;text-align-last: end;"> '
		       	       	    	+'</div>'
		       	       	    	+'<div class="col-md-2" style="padding-left: 1px;">'+'<div id="score_'+jsonData[i].projectID+'" style="height: 32px;color:white;font-size: 11px;text-align: center;border-radius: 50%;width: 32px;padding-top: 7px; ">'
		       	       	    	+'</div>'
		       	       	    	+'</div>'
		       	       	    	+'<div  class="col-md-2" style="cursor: pointer" ><img class="Export_cLabelTitle" title ="" onclick="exportSentimentPopup('+jsonData[i].projectID+')" style= "height:32px;width:32px;" src="'+path+'/images/shareconf.png"></img><img id="confarrow_'+jsonData[i].projectID+'" style="display:none"  src="'+path+'/images/arrow-left_new.png"></img><div class="popupconf" style="position: absolute;z-index: 100;margin-left: 23%;margin-top: -17%;" id="confluencePopup_'+jsonData[i].projectID+'"></div>'
		       	       	    	+'</div>'
		       	       	    +'</div>'
		       	       	    +'<div class="row" style="height:46vh">'
		       	       	    	+'<div class="col-md-8 viewBorder"  style="height:46vh">'
				       	       	    +'<div id="chart_'+jsonData[i].projectID+'" class="col-md-12"  style="padding-top:2%;position: relative; height:45vh">'
				       	       	    +'<canvas id="myChart_'+jsonData[i].projectID+'" ></canvas>'
				       	       	    +'</div>'
			       	       	    +'</div>'
			       	       	    +'<div  class="col-md-4 viewPef" style="height:21vh">'
			       	       	    	+'<div class ="row " style="display:block;height:1vh;width:60vh;margin-bottom:4%">'
			       	       	    		+'<div class="col-md-12" >'
			       	       	    			+'<div class="Prj_Performance_cLabelHtml" id="head_'+jsonData[i].projectID+'" style="height:1%;font-size: 15px;font-family: opensanscondbold;"> </div>'
			       	       	    		+'</div>'
			       	       	    	
			       	       	    	+'</div>'
			       	       	    	+'<div class ="row projectP" style="display:block;height:20vh;width:50vh;margin-bottom:4%">'
			       	       	    		+'<canvas  id="myChartp_'+jsonData[i].projectID+'" ></canvas>'
			       	       	    	+'</div>'
			       	       	    	+'<div class ="row userP" style="display:none">'
			       	       	    		+'<div class="col-md-7">'
			       	       	    			+'<div id="TotalTask_'+jsonData[i].projectID+'" class="Total_Tasks_cLabelHtml" style="font-size: 12px;font-family: OpenSansRegular;" >Total Tasks </div>'
			       	       	    			+'<div id ="TaskCompleted_'+jsonData[i].projectID+'" class="Tasks_Completed_cLabelHtml" style="font-size: 12px;font-family: OpenSansRegular;" >Tasks Completed </div>'
			       	       	    			+'</div>'
			       	       	    		+'<div class="col-md-5">'
			       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;" >: <span id="totalTask_'+jsonData[i].projectID+'"></span></div>'
			       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;" >: <span id ="taskCompleted_'+jsonData[i].projectID+'"></span></div>'
			       	       	    			+'</div>'
			       	       	    	+'</div>'
			       	       	    	+'<div class ="row taskU" style="display:none">'
			       	       	    		+'<div class="col-md-7">'
			       	       	    			+'<div id="scHead_'+jsonData[i].projectID+'" class="Index_cLabelHtml" style="font-size: 15px;font-family: opensanscondbold;" > </div>'
			       	       	    			+'<div id="TaskBeyond_'+jsonData[i].projectID+'" class="Task_BeyondEnd_cLabelHtml" style="font-size: 12px;font-family: OpenSansRegular;" > </div>'
			       	       	    			+'<div id="TaskNotComp_'+jsonData[i].projectID+'" class="Tasks_Pending_cLabelHtml" style="font-size: 12px;font-family: OpenSansRegular;">Tasks Pending   </div>'
			       	       	    		+'</div>'
			       	       	    		+'<div class="col-md-5 taskU" style="display:none">'
			       	       	    			+'<div id="ScHead_'+jsonData[i].projectID+'"></br></div>'
			       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;" >: <span  id="taskBeyond_'+jsonData[i].projectID+'"></span></div>'
			       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;"> : <span id="taskNotComp_'+jsonData[i].projectID+'"></span></div>'
			       	       	    		+'</div>'
			       	       	    	+'</div>'
			       	       	    	+'<div class ="row " style="height:23vh;width:60Vh">'
			       	       	    		+'<div class="col-md-12 view">'
			       	       	    		+'</div>'
			       	       	    	+'</div>'
			       	       	    +'</div>'
		       	       	    +'</div>'
	       	       	    +'</div>';
	       	        //alert(j);
	       	        
	       	        if( j == "50"){
	       	       		act ="true";
	       	       		scrollLimit++;
		       	       	
	       	        	break;
	       	        }
	       	        scrollLimit++;
	       	        j++;
	       	    }
		        
	        	$("div#semanticAnalysis").append(Ui);	 
	        	$('.cButton').css({'margin-top':'-30%'});
				$("div#semanticAnalysis .imageHover").removeClass("hoverZoomLink");
				sliderFun();	
		       		
				if(redirectType !=''){
					if(RepPeriod == 'Y'){
						$('.sBac').removeClass("sCalBcActive");
					    $('#1W').addClass("sCalBcActive");
					    RepPeriod= "";
					}
					$("div#semanticAnalysis").children("div:first-child").find("div[id^=projImgMainDiv_]").trigger("click");
					//showStatusSearchAndSort();
					redirectType= "";
				}
			 
			}else if(sortType == "status"){
				var projectUsersStatus = "";
		        var status_code = "";
		        var projStatusSrc = "";
	        	 for(var i =scrollLimit; i < jsonData.length;  i++){
	        	    teamUI ="";
	        	    team = jsonData[i].teamMembers;
	        	    ctxPath = jsonData[i].ctxPath;
	   					
	   					if(jsonData[i].sentimentScore =="99"){
			        	   status_code ='#00783B';
			        	}else if(jsonData[i].sentimentScore == "100"){
			        		status_code ='#00A652';
			        	}else if(jsonData[i].sentimentScore == "101"){
			        		status_code ='#00D466';
			        	}else if(jsonData[i].sentimentScore == "102"){
			        		status_code ='#F1DB14';
			        	}else if(jsonData[i].sentimentScore == "103"){
			        		status_code ='#FCB040';
			        	}else{
			        		status_code ='#C52C31';
			        	}
		          
		          		projStatusSrc="";
		          		if(jsonData[i].projStatus !==""){
		        			if(jsonData[i].projStatus =="PO"){
				        	   projStatusSrc= ctxPath+'/images/myprojects.png';
				        	   // $('#A_'+jsonData[l].projectID).attr('title', 'owned by');
				        	}else{
				        	    projStatusSrc= ctxPath+'/images/sharedprojects.png';
				        	 	//$('#A_'+jsonData[l].projectID).attr('title', 'shared by');
				        	}
			        	}else{
			        	   projStatusSrc="";
			        	}
		          
		           
	        	  Ui += '<div class="row projImgMainDiv_'+jsonData[i].projectID+'"  style="background-color:white;height:115px;margin-right:0vw;margin-left:0vw;border-bottom: 1px solid #ccc;"> '
       	        	+'<div class="col-lg-1 col-md-1 col-sm-1 col-xs-6" id="projImgMainDiv_'+jsonData[i].projectID+'"  onclick =viewStatusGraph('+jsonData[i].projectID+',"","project","status") style="cursor:pointer;height:115px;padding-left:2vw"> ' 
	       	        	+'<div class="pStyle row" id="proj_'+jsonData[i].projectID+'">'
	       	        	+'<div style="margin-top:3vh;margin-left:10px;position:absolute"><img id="A_'+jsonData[i].projectID+'"  class="shareUserIcon"  title=\"\" src=\"'+projStatusSrc+'\"  projectType=\"my_project\"  projectId=\"'+jsonData[i].projectID+'\"   /></div>'
	       	    		+'<img onerror="imageOnProjErrorReplace(this)" src="'+jsonData[i].imageUrl+'" style="width:60px;height:60px;margin-left:11%;margin-top:3vh;margin-bottom:1%;border: 1px solid rgb(191, 191, 191);"  ></img>'
	       	        	+'<div id="sysScore_'+jsonData[i].projectID+'"  style="background-color:'+status_code+'; float: left;width: 60px;margin-top: -1%;margin-left:11%;height: 5px;clear: both;" ></div>'
	       	        	+'<div class="defaultExceedCls" style="width:62px;text-align:center;font-family: OpenSansRegular;font-size: 11px;font-weight:bold;margin-left:10%" id="projName_'+jsonData[i].projectID+'">'+jsonData[i].ProjTitle+'</div>'
	       	       		+'</div>'
	       	        +'</div>'
       	        	+'<div class="col-lg-11 col-md-11 col-sm-11 col-xs-6"  style="height:115px;"> ' 
	       	        	+'<div id="b_'+jsonData[i].projectID+'" class="row" style="float: left;width: 100%;height: 115px;">'
		       	        	+'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:115px;margin-left:-2px">'
			       	        	+'<ul style="height:115px;display: inline-flex;width:105%" id="teamMember_'+jsonData[i].projectID+'" class="sliderUL owl-carousel owl-theme" >'
			       	        	+ prepareUIforStatusteam(team,senderType,projStatus)
			       	        	+'</ul>'
		       	        	+'</div>'
	       	        	+'</div>'
       	        	+'</div>'
       	        +'</div> ';
       	       	
       	       	Ui+='<div class="row chartcontain"  id="chart-container_'+jsonData[i].projectID+'" style="background:white;height:54vh;margin-left:1%;margin-right:1%;border-bottom: 1px solid #ccc;display:none;z-index:100">'
	       	       	    
	       	       	    +'<div class="row" style="height:46vh">'
	       	       	    	+'<div class="col-md-8 viewBorder"  style="height:50vh;margin-top:1%">'
			       	       	    +'<div id="chart_'+jsonData[i].projectID+'" class="col-md-12"  style="padding-top:2%;position: relative; height:50vh">'
			       	       	    +'<canvas id="myChart_'+jsonData[i].projectID+'" ></canvas>'
			       	       	    +'</div>'
		       	       	    +'</div>'
		       	       	    +'<div  class="col-md-4 viewPef" style="height:21vh;margin-top:2%">'
		       	       	    	+'<div class ="row " style="display:block;height:1vh;width:60vh;margin-bottom:4%">'
		       	       	    		+'<div class="col-md-12" >'
		       	       	    			+'<div id="head_'+jsonData[i].projectID+'" class="Prj_Performance_cLabelHtml" style="height:1%;font-size: 15px;font-family: opensanscondbold;"> </div>'
		       	       	    		+'</div>'
		       	       	    		
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row projectP" style="display:block;height:20vh;width:50vh;margin-bottom:4%">'
		       	       	    		+'<canvas  id="myChartp_'+jsonData[i].projectID+'" ></canvas>'
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row userP" style="display:none">'
		       	       	    		+'<div class="col-md-7">'
		       	       	    			+'<div id="TotalTask_'+jsonData[i].projectID+'" class="Total_Tasks_cLabelHtml" style="font-size: 12px;font-family: OpenSansRegular;" > </div>'
		       	       	    			+'<div id ="TaskCompleted_'+jsonData[i].projectID+'" class="Tasks_Completed_cLabelHtml" style="font-size: 12px;font-family: OpenSansRegular;" > </div>'
		       	       	    			+'</div>'
		       	       	    		+'<div class="col-md-5">'
		       	       	    			+'<div id="totalTask_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" ></div>'
		       	       	    			+'<div id ="taskCompleted_'+jsonData[i].projectID+'" style="font-size: 12px;font-family: OpenSansRegular;" ></div>'
		       	       	    			+'</div>'
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row taskU" style="display:none">'
		       	       	    		+'<div class="col-md-7">'
		       	       	    			+'<div id="TaskBeyond_'+jsonData[i].projectID+'" class="Task_BeyondEnd_cLabelHtml" style="font-size: 12px;font-family: OpenSansRegular;" > </div>'
		       	       	    			+'<div id="TaskNotComp_'+jsonData[i].projectID+'" class="Tasks_Pending_cLabelHtml" style="font-size: 12px;font-family: OpenSansRegular;"> </div>'
		       	       	    		+'</div>'
		       	       	    		+'<div class="col-md-5 taskU" style="display:none">'
		       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;" >: <span  id="taskBeyond_'+jsonData[i].projectID+'"></span></div>'
		       	       	    			+'<div style="font-size: 12px;font-family: OpenSansRegular;"> : <span id="taskNotComp_'+jsonData[i].projectID+'"></span></div>'
		       	       	    		+'</div>'
		       	       	    	+'</div>'
		       	       	    	+'<div class ="row " style="height:23vh;width:60Vh">'
		       	       	    		+'<div class="col-md-12 view">'
		       	       	    		+'</div>'
		       	       	    	+'</div>'
		       	       	    +'</div>'
	       	       	    +'</div>'
       	       	    +'</div>';
       	       	
       	       	   if( j == "50"){
       	       	  		act ="true";
       	       	  		scrollLimit++;
		       	       	break;
	       	        }
	       	        scrollLimit++;
	       	       j++;
	       	        
       	    }
	        	 
    	 	$("div#statusContainer").css({'background-color':'white'});
       		$("div#statusContainer").append(Ui);	 
        	$('.cButton').css({'margin-top':'-30%'});
			sliderFun();
			
			$("div#statusContainer .imageHover").removeClass("hoverZoomLink");
			
			 if(redirectType !=''){
				$("div#statusContainer").children("div:first-child").find("div[id^=projImgMainDiv_]").trigger("click");
				redirectType= "";
			 }
	  					 
       }
  }
  function sliderFun(){
	  //alert("init slider---->22");
	  $(".sliderUL").owlCarousel({
		  		  pagination: true,
				  loop:false,
				  mouseDrag: false,
				  navigation : true,
			      items : 10, //10 items above 1000px browser width
			      itemsMobile : false ,// itemsMobile disabled - inherit from itemsTablet option
			      rewindNav:false
	
	  });
	    $('.owl-theme').removeClass('sliderUL');// This class will be present only for newly prepared row. So once slider is initialized then removing this class. 
	    $('.owl-wrapper').css({"width":"100%","height":"115px"});
		$('.owl-prev').text('').css({"float":"left","margin-top":"-115px"}).append("<div class=\"hoverlargeImgRemove\" style=\"border-right: 1px solid #aaa7a7;width: 27px;height: 115px;    margin-left: -73px;cursor:pointer;\"><img  src=\""+path+"/images/leftSliderArrow.png\" style=\"opacity: 0.8;margin-left: 4px;margin-top: 45px;width: 18px;height: 25px;\"> </div>" );
		$('.owl-next').text('').css({"float":"right","margin-top":"-115px"}).append("<div class=\"hoverlargeImgRemove\" style=\"border-left: 1px solid #aaa7a7;width: 27px;height: 115px;    margin-left: -3vw;cursor:pointer;\"><img  src=\""+path+"/images/rightSliderArrow.png\" style=\"opacity: 0.8;margin-left: 4px;margin-top: 45px;width: 18px;height: 25px;\"> </div>" );
		$('.owl-controls').show();
		$('.owl-pagination').remove();
		$('.owl-item').css({"width":"75px"});
		$(".owl-wrapper").css({'display':'inline-flex'});
		$(".owl-wrapper-outer").css({"width":"100%","max-width":"100%","overflow":"hidden","margin-left":"-3vw","margin-right":"12px","margin-right":"115px"});
}
  function statusOrSentimentSort(obj){
  	 loadProjectsJson(sortType,senderType);	  
  }
  function ClearStatusSearch(obj){
	   $(obj).hide();
	   $("#ClearStatusSearch").hide();
	   $("#statusSearchBox").val('');
	   loadProjectsJson(sortType,senderType);
   }
   function showClear(obj){
	  $("#ClearStatusSearch").show();
   }
   function removeClas(){
    $(".imageHover").removeClass("hoverZoomLink");
   }
   
  //--------- Jira sentiment analysis report related scripts------>
   
     function prepareJiraStoryUI(data, projId){
	 
	  var UI = "";
	  UI +='<select projId ="'+projId+'" class="jiraMainFilter" style="width: 75px;" onchange = "filterSentimentByType(this)">'
		        +'<option value="all" selected>All</option>'
		        +'<option value="story">Story</option>'
		    +'</select>'
		    +'<select projId ="'+projId+'" class="jiraSubFilter"  style="margin-left: 10px;width: 104px;" onchange = "filterSentimentByStory(this)">'
		        +'<option value="0" selected>All</option>';
		    
		for (var j = 0; j < data.length ; j++) {
			UI+='<option value="'+data[j].storyId+'">'+data[j].storyName+'</option>';
		}
	  	UI+='</select>';
	  	
	  	return UI;
  }   
   
   function filterSentimentByType(obj){
	   if($(obj).val()=="all"){
		   $(obj).next("select").attr('disabled',true);
		   $(obj).next("select").children('option[value="0"]').attr('selected','selected');
		   //var projId = $(obj).attr('projId');
		   sentimentFilterFlag = true;
		   if(sProjId != "" && sUserId != ""){
				$('#lis_'+sProjId+'_'+sUserId).trigger('click');
		     }else{
		    	 $('#projImgMainDiv_'+sProjId).trigger('click');
		     }
		   
	   }else{
		   $(obj).next("select").attr('disabled',false);
		   
	   }
	}  
   
   var sentimentFilterFlag = false;
   function filterSentimentByStory(obj){
	   if($(obj).prev('select').val() =="all"){
		 return false;  
	   }   
	   //var projId = $(obj).attr('projId');
	   sentimentFilterFlag = true;
	   // setting above flag to true bcoz sentiment graph ui is already opened below line it will trigger the click function but it will hide the ui. In this scenarios no need to hide and show again so setting this flag to avoid hide and show of graph UI.  
	    if(sProjId != "" && sUserId != ""){
	     	$('#lis_'+sProjId+'_'+sUserId).trigger('click');
	     }else{
	     	$('#projImgMainDiv_'+sProjId).trigger('click');
	     }
	    
	}  
   
   function exportSentimentPopup(obj){
	   
	   		   
		   	   if($("#confulencePopup").is(":visible")){
		   		$("#confulencePopup").remove();
		   		$($("#confarrow_"+obj)).css('display','none');
		   		return;
		   	   }
			   
		   	$("#confulencePopup").remove();
			   
			   var UI = "";
			   
			   UI+='<div id="confulencePopup" style="width: 315px;height: 203px;border: 1px solid rgb(161, 161, 161);box-shadow:0px 1px 4px 0px rgba(168, 168, 168, 0.6);color:#64696F;border-radius: 2px;background-color:rgba(0, 0, 0, 0.61);background-color: rgb(255, 255, 255);position: absolute;z-index: 1000;margin-left: 60%;">'
				
				   		+'<div style="width:100%;border-right: 1px solid white;height: 100%;">'
							+'<div style="width:32%;border-right: 1px solid  #DDDDDD;height: 100%;float:left">'
							   +'<ul style="list-style: none;font-size: 15px;padding-left: 5px;">'
								   +'<li class="Confluence_cLabelHtml" style="padding: 5px;"></li>'
								   +'<li style="padding: 5px;display:none">Pdf</li>'
							   +'</ul>'
						   +'</div>'
						   +'<div style="width:68%;height: 100%;float:right">'
								
								+'<div style="width:100%;height:260px;">'	  
					   				
					   				+'<div style="display:block;" id="pageData"></div>'
					   				+'<div style="display:none;margin-bottom: 8px" id="newPageData" >'
					   					+'<div class="New_page_cLabelHtml" style="font-family: opensanscondbold;font-size: 14px;color: #6e6f6f;padding-left: 10px;border-bottom: 1px solid #DDDDDD;padding-bottom: 3px;    padding-top: 3px;width:70%;float:left"></div>'
					   					+'<div id="Y" onclick="addNewPage(this)" style="font-family: opensanscondbold;font-size: 14px;color: #6e6f6f;padding-left: 37px;border-bottom: 1px solid #DDDDDD;padding-bottom: 3px;    padding-top: 3px;width:30%;float:right"><img class="Back_cLabelTitle" title="" style="height:17px;width:17px" src="'+path+'/images/back.png"></img></div>'
					   					+'<div class="Select_space_cLabelHtml" style="padding-top: 31px;padding-left: 10px;font-family: OpenSansSemibold;    color: #797E81;font-size: 12px;"></div>'
						   				+'<select class="form-control" id ="spaceData" style="margin-top:5px;margin-bottom:5px;width:90%;margin-left: 10px;    padding: 3px;height: 30px;"></select>'
						   				+'<div class="Enter_page_title_cLabelHtml" style="padding-top: 3px;padding-left: 10px;font-family: OpenSansSemibold;    color: #797E81;font-size: 12px;" ></div>'
									   	+'<input class="form-control Enter_page_title_cLabelPlaceholder" id="pagetitle" style="margin-top:5px;width:90%;margin-left: 10px;height: 30px;padding: 6px;" type="text" placeholder="">' 
					   				+'</div>'
					   				+'<div style="width:100%;border-top: 1px solid #DDDDDD;">'	  
					   				+'<div class="createBtn pagebutton Post_cLabelHtml" id="existpage" style="margin-right: 6%; text-transform: uppercase;" onclick="exportSentiment('+obj+')"></div>'
								+'</div>'
								+'</div>'
								
						   +'</div>'
					   +'</div>'
					+'</div>';
			   $("#confluencePopup_"+obj).prepend(UI);
			   $("#confarrow_"+obj).css('display','inline');
		   
		   getConfulencePages();
		   loadCustomLabelAnalytics('TodayRepo');
			   
		
   }
   
   function getConfulencePages(){
	   var UI = "";
	   var spaceUi="";
		  $.ajax({
			   url:path + '/adminUserAction.do',
			   type: "post",
			   async: false,
			   data:{act:"getConfulencePages",
			   
			   },
			   error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						},	 
			   success:function(result){
				   console.log("result---"+result);
				   var ui="";
				   if(result =="[]"){
					   ui = "No pages found";
					}else{
					   var pagedata = jQuery.parseJSON(result.split("##")[0]).results;	
					   var spacedata = jQuery.parseJSON(result.split("##")[1]).results;	
					   for(var i =0;i<pagedata.length;i++){
						   ui+='<li id="page_'+pagedata[i].id+'" onclick="selectPage(this)" style="padding: 3px;">'+pagedata[i].title+'</li>'
					   }
					   UI+='<div class="Existing_pages_cLabelHtml" style="font-family: opensanscondbold;font-size: 14px;color: #6e6f6f;padding-left: 10px;border-bottom: 1px solid #DDDDDD;padding-bottom: 3px;    padding-top: 3px;width:70%;float:left"></div>'
						   +'<div id="N" onclick="addNewPage(this)" style="font-family: opensanscondbold;font-size: 14px;color: #6e6f6f;padding-left: 37px;border-bottom: 1px solid #DDDDDD;padding-bottom: 3px;    padding-top: 3px;width:30%;float:right"><img class="Add_new_page_cLabelTitle" title="" style="height:17px;width:17px" src="'+path+'/images/add.png"></img></div>'
						   +'<div style="width:100%;height:124px;overflow:auto;border-bottom: 1px solid white;border-top: 1px solid white;">'
								+'<ul class="pagelist" style="list-style: none;font-size: 13px;padding-left: 5%;">'
								   +''+ui+''
								+'</ul>'
						   +'</div>';
					   
					}
				   $("#pageData").html(UI);
				   for(var i =0;i<spacedata.length;i++){
					   spaceUi+='<option value="'+spacedata[i].key+'">'+spacedata[i].name+'</option>'
				   }
				   $("#spaceData").html(spaceUi);
				   loadCustomLabelAnalytics('TodayRepo');
			   }
			}); 
		  return UI;
	  }
   function exportSentiment(obj){
	   var space = "";
	   var title="";
	   var pageid="";
	   var canvas = document.getElementById("myChart_"+obj);
	   var dataURLImage = canvas.toDataURL("image/png");
	   var type = $(".pagebutton").attr("id");
	   if(type=="existpage"){
		   title=$(".pageselect").text();
		   pageid=$(".pageselect").attr("id");
		   if(pageid=="undefined" || pageid== undefined ){
			   alertFun('Please select page','warning');
			   return;
		   }
		   
	   }else{
		  space = $( "#spaceData option:selected" ).val();
		  title = $("#pagetitle").val(); 
	   }
	   
	   if(title == ""){
		   alertFun('Please enter page title','warning');
		   return;
	   }
	   	var data = atob( dataURLImage.substring( "data:image/png;base64,".length ) ),
	    asArray = new Uint8Array(data.length);
	
		for( var i = 0, len = data.length; i < len; ++i ) {
		    asArray[i] = data.charCodeAt(i);    
		}
		var blob = new Blob( [ asArray.buffer ], {type: "image/png"} );
		
		var data1 = new FormData();
	    data1.append('file', blob);
		//console.log("path--"+path);
		var projname = $("#projName_"+obj).text();
		console.log("projname--"+projname);
		$.ajax({
	    	url:path + '/adminUserAction.do?act=exportSentiment&space='+space+'&title='+title+'&pageid='+pageid+'&projId='+projname+'',
	    	type:'POST',
	    	processData: false,
		    contentType: false, 
		    cache: false,
			mimeType: "textPlain",
	    	data:data1,
	    	error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
	    	success:function(result){
	    		$("#confulencePopup").hide();
	    		 $("#confarrow_"+obj).css('display','none');
	    		if(result != "success"){
	    			alertFun('Please Integrate the confluence with the colabus','warning');
	    		}else{
	    			
	    			alertFun('Exported to confluence','warning');
	    			
	    		}
	    			
		    	
	    	}
		});
   }
   
   function selectPage(obj){
	   $(".pageselect").css('background-color','white');
	   $('.pagelist li.pageselect').removeClass('pageselect');
	   $(obj).addClass("pageselect");
	   $(obj).css('background-color','rgb(204, 204, 204)');
	   
	}
   
   function addNewPage(obj){
	   var type= $(obj).attr("id");
	   $(".pageselect").css('background-color','white');
	   $('.pagelist li.pageselect').removeClass('pageselect');
	   if(type=="N"){
		   $(".pagebutton").attr("id","newpage");
		   $("#pageData").hide();
		   $("#newPageData").show();
	   }else{
		   $(".pagebutton").attr("id","existpage");
		   $("#newPageData").hide();
		   $("#pageData").show();
	   }
   }