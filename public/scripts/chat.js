/**
 * @author Abhishek 
 * This js file is used to handle the single chat functionality
 */


var num = 0;
var count = 0;
var Name = "";
var isActive = true;
var NameTitle = '';
var interval = '';
var userName = '';
var onlineUser = '';
var chatDomain = "";
var parId = "chatconv";
var roster_count = 0;
function connect() {

	var conn = new Strophe.Connection("https://" + chatDomain + ".colabus.com:7443/http-bind/");
	var c = userId + "@" + chatDomain + ".colabus.com";
	conn.connect(c, "welcome", function (status) {

		if (status === Strophe.Status.CONNECTED) {
			console.log("connected....");

			$(document).trigger("connected");
			// $('#chat-box ul').html('');
			$("#loadingBar").hide();
			window.opener.gLocation();
			window.opener.changeImg('online');
			sendDeliverStanza();
		}
		else if (status === Strophe.Status.DISCONNECTED) {  // If chat get disconnected It will agin call the connect method to Connect to server.
			//$(document).trigger("disconnected");
			//alert('not connected');

			// disconnect();
			//$('#chat-box ul').html('');
			window.opener.changeImg('offline');
			connect();
		} else if (status === Strophe.Status.AUTHFAIL) {
			alertFun('User is not registered with the chat server. ', 'warning');
			$("#loadingBar").hide();
		} else if (status === Strophe.Status.ERROR || status === Strophe.Status.CONNFAIL) {
			// alertFun('Chat server connection failed. ','warning');
			$("#loadingBar").hide();
		}

	});

	Gab.connection = conn;
	serverConnection = conn;
	$('#sound').data('conn', conn);  // Saving the Connection Object to be used in groupChat.js
	onloadGroup();   // this is group onload function once the connection is done (groupChat.js)

	/*setInterval(function(){
		 if(window.opener.globalChatConnection==null){
			  window.opener.globalChatConnection = serverConnection;
		 }
		 if(peerConnection!=null){
			window.opener.globalPeerConnection = peerConnection;
			window.opener.globalChatJID = jid;
		 }
	},2000);
	*/
}

/*
 * @Method is used to disconnect from the server.
 */
function disconnect() {
	console.log("Disconnected...")
	try {
		Gab.connection.disconnect();
		Gab.connection = null;
		w.close();
	} catch (e) {
		console.log('Null value handled!');
	}


}

function disconnectFromMain() {  //-------------- this function is called from postlogin.jsp when chat window is closed, to update user status to offline and to close the connection.
	console.log("Disconnected...")

	globalChatConnection.connection.disconnect();
	globalChatConnection.connection = null;
}

/* function disconnectReqVideoCallWenChatWindowClosed(){  //-------------- this function is called from postlogin.jsp when chat window is closed, to update user status to offline, to disconnect video call  and to close the connection.
 
	 //console.log("Disconnect video call...")
	 
	 //var jsonText = '{"calltype":"AV","type":"calldisconnect"}';
	 //notify = $msg({to: globalChatJID,"type" : "normal"}).c('json',{xmlns: "urn:xmpp:json:0"}).t(jsonText);
	 //globalChatConnection.send(notify);
	 disconnectFromMain();
		 
 } */


/*
 * This is a bind function which will get called when Server will get connected through Web App
 */

$(document).on('connected', function () {
	var iq = $iq({ type: 'get' }).c('query', { xmlns: 'jabber:iq:roster' }); // preparing the roster request Information Query to send to server

	Gab.connection.sendIQ(iq, Gab.on_roster); // Sending the roster request.
	Gab.connection.addHandler(Gab.on_roster_changed, "jabber:iq:roster", "iq", "set");  // This function will get called when a person will go online or offline.
	Gab.connection.addHandler(Gab.on_message, null, "message", "chat"); // This function Will get called when any message will come.	  
	Gab.connection.addHandler(Gab.on_public_message, null, "message", "groupchat");
	Gab.connection.addHandler(Gab.on_normal_message, null, "message", "normal");
});

function listOfConnectedRooms() {

	$.ajax({
		url: path + "/ChatAuth",
		type: "post",
		data: { act: 'getMUCRoomList', userId: userId, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			// checkSessionTimeOut(result);
			//console.log("result---"+result);
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				if (result == 'No Data') {

				} else {
					jsonData = jQuery.parseJSON(result);
					ImageUploadFromSystem = false;
					ImageIsUploaded = false;
					sendGroupPresence();
					prepareUI();
				}
			}
		}
	});
}

function sendGroupPresence() {
	var groupId = '';
	if (jsonData.length > 0) {
		for (i = 0; i < jsonData.length; i++) {
			groupId = jsonData[i].groupId;
			Gab.connection.send(
				$pres({
					to: groupId + '@mucr.' + chatDomain + '.colabus.com' + "/" + "user_" + userId + "_" + userFullName + "_" + userImgType
				}).c('x', { xmlns: "http://jabber.org/protocol/muc" }));
			Gab.connection.send($pres().c('status').t('available'));
		}
	}
}

var chatUserArray = [];
function processUserList(presence, from, status) {
	var resourceId = from.split("/")[1];
	var resourceUserId = from.split("@")[0];
	if (!resourceId.includes("_")) {
		var tagMap = {};
		var i = null;
		for (i = 0; chatUserArray.length > i; i += 1) {
			tagMap[chatUserArray[i].resourceId] = chatUserArray[i];
		}

		var hasTag = function (tagName) {
			return tagMap[tagName];
		};

		if (typeof (hasTag(resourceId)) == 'undefined') {

			chatUserArray.push({
				resourceId: resourceId,
				userId: resourceUserId,
				status: status
			});

		} else {

			var aIndex = chatUserArray.getIndexOfResourceId(resourceId);
			chatUserArray[aIndex].status = status;

		}
		//console.log(JSON.stringify(chatUserArray));
	}
}

Array.prototype.getIndexOfResourceId = function (el) {
	var arr = this;
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].resourceId == el) {
			return i;
		}
	}
	return -1;
}

Array.prototype.getUserStatus = function (el) {
	var arr = this;
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].userId == el) {
			if (arr[i].status == "dnd") {
				return true;
			}
		}
	}
	return false;
}

Array.prototype.getUserOnlineStatus = function (el) {
	var arr = this;
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].userId == el) {

			if (arr[i].status == "online" || arr[i].status == "dnd") {
				return true;
			}
		}
	}
	return false;
}


var Gab = {  // TO understand this Core part of chat Go through the book. 

	connection: null,
	room: 'conference.' + chatDomain + '.colabus.com',
	room: 'mucr.' + chatDomain + '.colabus.com',
	jid_to_id: function (jid) {
		return Strophe.getBareJidFromJid(jid).replace("@", "-").replace(".", "-").replace(".", "-");
	},

	on_roster: function (iq) {
		$("#loadingBar").show();
		//console.log("inside on roster ..");
		if (roster_count == 0) {
			roster_count++;
			for (i = 0; i < userName.length; i++) {

				var name = "";
				var imgType = "";
				var useRegType = "";
				var upgradePlan = "";
				var useRegType1 = "";
				var flag = false;
				var duser = "";
				var user_status = "";
				var imgurl = "";
				var useRoleId = "";
				var useCreatedById = "";
				var guestUserClass = "";
				var useLoginId = "";
				$(iq).find('item').each(function () {
					var jid = $(this).attr('jid');
					//console.log("on roster id "+jid);
					var id = jid.split('@')[0];
					if (id == userName[i].userId) {
						name = userName[i].name
						imgType = userName[i].imgType;
						useRegType = userName[i].useRegType;
						upgradePlan = userName[i].upgradePlan;
						useRoleId = userName[i].userRoleId;
						useCreatedById = userName[i].userCreatedBy;
						useLoginId = userName[i].userLoginId;
						if (imgType == "" || imgType == "null" || imgType == null) {
							imgurl = "";
						} else {
							imgurl = lighttpdPath + "/userimages/" + id + "." + imgType + "?" + imgTime;
						}
						//flag = true;
						name = name.replace("CHR(90)", " ").replace("CHR(90)", " ").replace("CHR(90)", " ").replace("CHR(90)", " ");

						var id = jid.split('@')[0];
						//console.log('id---'+id+"----name-- "+name);
						// transform jid to id
						var jid_id = Gab.jid_to_id(jid);
						if (typeof useRegType != 'undefined' && useRegType != null && useRegType != "") {
							if (useRegType.indexOf('_') != -1) {
								useRegType1 = useRegType.split('_')[1];
							} else {
								useRegType1 == useRegType;
							}
							if ((useRegType1.toLowerCase() == "social" || useRegType1.toLowerCase() == "standard") && upgradePlan == 0) {
								duser = 'socialUser';
							}
						}

						if (useRoleId == 8 && useCreatedById == userId) {
							guestUserClass = "guest_user";
						}
						var contact = $("<li class='" + duser + "' title='' disabled='true' count='0' style='height:61px;float:left;width:99%;cursor:pointer;border-bottom: 1px solid #ced2d5; ' id='" + jid_id + "' imgType='" + imgType + "' onclick='openNewChatBox(this)' >" +
							"<div id='" + id + "'class='roster-contact offline row' style='padding-top:5px;width:100%;' >" +
							"<div class='col-xs-3' style='width: 15%; padding-left: 8px;padding-top:0.5%'> <img title='" + name + "'  src='" + imgurl + "' onerror='userImageOnErrorReplace(this);' style='height: 34px; min-height: 45px; width:45px;min-width: 45px;border-radius:50%'/></div>" +
							"<div class='user_box_status' style='margin-left: 46px; margin-top: 5px;position:absolute'></div>" +
							"<div class='user_box_name col-xs-4 defaultWordBreak' style='height:35px;font-family: OpenSansRegular; font-size: 14px; width: 62%; padding-top: 7px;;margin-left:2px' value='" + name + "'>" + name + "</div>" +
							//"<div class=' col-xs-1' style='background: #ff3333; color: #000; float: left; text-align: center; padding: 1%; border-radius: 50%; width: 3%; margin-top: 1%; visibility: hidden;'></div>"+
							"<div class='hover-call-icons' style='display:none;position: absolute;right: 3%;'>" +
							"<div class='col-xs-2 videocall' style='min-width: 35px;padding-top:14px;float: right;padding-right: 2px;padding: ;padding-left: 0px;max-width: 13%;margin-right: 1%;'><img src='" + path + "/images/video/vedioGreen.png'  onclick='startCall(" + id + ",\"true\",\"video\")' style='width: 35px;height: 25px;'/></div>" +
							"<div class='col-xs-2 audiocall' style='min-width: 35px;padding-top:10px;padding-left: 2px;float: right;padding-right: 2px;'><img src='" + path + "/images/video/Phonegreen.png' onclick='startCall(" + id + ",\"true\",\"audio\")'  style='width: 40px;height: 32px;float: right;'/></div>" +
							"<div class='col-xs-2 " + guestUserClass + "' style='min-width: 35px;padding-top:12px;padding-left: 2px;float: right;padding-right: 10px;display:none;'><img title='Copy Invite Link' src='" + path + "/images/copy.png' onclick='event.stopPropagation();GenerateCopyLinkForGuestUser(" + id + "," + useCreatedById + ",\"" + useLoginId + "\")'  style='width: 25px;float: right;background-color: #fff;'/></div>" +
							"</div>" +

							"</div>" +
							"<input type='hidden' class='roster-jid' value=" +
							jid +
							"></li>");

						Gab.insert_contact(contact);
						$('li.socialUser').removeAttr('onclick');
						$('li').find('.guest_user').show();
						$('li.socialUser').attr('title', 'Option not available');




					}


				});

			}
		}
		Gab.connection.addHandler(Gab.on_presence, null, "presence");
		Gab.connection.send($pres());

		$("#loadingBar").hide();
		timerControl("");
		listOfConnectedRooms();

		$("#convoListContainer li").on("mouseover", function () {
			$(this).find('.hover-call-icons').show();
			if ($(this).find("div.roster-contact").hasClass("dnd")) {
				$(this).find('.hover-call-icons').find('.videocall').find('img').css({ "cursor": "not-allowed", "opacity": "0.5" }).attr("onclick", "");
				$(this).find('.hover-call-icons').find('.audiocall').find('img').css({ "cursor": "not-allowed", "opacity": "0.5" }).attr("onclick", "");
			} else {
				var vuserId = $(this).find("div.roster-contact").attr("id");
				$(this).find('.hover-call-icons').find('.videocall').find('img').css({ "cursor": "", "opacity": "" }).attr("onclick", "startCall(" + vuserId + ",\"true\",\"video\")");
				$(this).find('.hover-call-icons').find('.audiocall').find('img').css({ "cursor": "", "opacity": "" }).attr("onclick", "startCall(" + vuserId + ",\"true\",\"audio\")");
			}
		}).on("mouseout", function () {
			$(this).find('.hover-call-icons').hide();
		});
	},

	on_presence: function (presence) {
		var ptype = $(presence).attr('type');
		var from = $(presence).attr('from');
		var jid_id = Gab.jid_to_id(from);
		//console.log("from--"+from+" jid_id--"+jid_id +"--ptype---"+ptype); 
		// console.log(from.split("@")[0]+"-----"+userConnected)
		var chatStatus = "";
		var contact = $('#chat-box li#' + jid_id + ' .roster-contact');
		if (ptype !== 'unavailable' && ptype !== 'error') {
			var show = $(presence).find("show").text();

			if (show === "" || show === "chat") {
				contact.removeClass('offline');
				contact.removeClass('pOnline');
				contact.addClass('online');
				contact.removeClass('dnd');
				chatStatus = "online";
			} else if (show === "dnd") {
				contact.addClass('dnd');
				chatStatus = "dnd";
				contact.removeClass('offline');
				contact.removeClass('pOnline');
				contact.addClass('online');
			}

		} else if (ptype === 'unavailable') {
			contact.removeClass('online');
			contact.removeClass('dnd');
			contact.addClass("offline");

			/*if(peerConnection!=null){//------------this is for video call to disconnect call in case of user unavailable --- videoCall.js
			var pid = jid_id.split('-')[0]+"@"+chatDomain+".colabus.com";
			  if(peerConnection.iceConnectionState=="failed" && jid == pid ){
			   callFailedDueToOffline();
			}
		 }*/
			chatStatus = "offline";

		} else if (ptype === 'away') {
			contact.removeClass('online');
			contact.addClass('away');
			contact.removeClass('dnd');
			chatStatus = "offline";

		} else if (ptype === 'error') {
			contact.removeClass('dnd');
			chatStatus = "offline";

		}

		processUserList(presence, from, chatStatus);
		var li = contact.parent();
		li.remove();
		Gab.insert_contact(li);
		presenseUpdate(jid_id, chatStatus, li);

		var jid_id = Gab.jid_to_id(from);
		$('#chat-' + jid_id).data('jid', Strophe.getBareJidFromJid(from));

		var statusOnline = chatUserArray.getUserOnlineStatus(jid_id.split('-')[0]);
		//console.log("statusOnline:"+statusOnline);

		return true;
	},

	on_roster_changed: function (iq) {
		//console.log("inside roster changed");
		$(iq).find('item').each(function () {
			var sub = $(this).attr('subscription');
			var jid = $(this).attr('jid');
			var name = $(this).attr('name') || jid;
			var jid_id = Gab.jid_to_id(jid);
			//console.log("Sub "+sub+ "Jid "+jid+ "name "+name+ "jid_id "+jid_id);					  
			if (sub === 'remove') {
				//contact is being remove
				$('#' + jid_id).remove();
			} else {
				//contact is being added or modified
				var contact_html = "<li id='" + jid_id + "'>" +
					"<div class='" +
					($('#' + jid_id).attr('class') || "roster-contact offline") +
					"'>" +
					"<div class='roster-name'>" + name +
					"</div><input type='hidden' class='roster-jid'>" + jid +
					"></div></li>";

				if ($('#' + jid_id).length > 0) {
					$('#' + jid_id).replaceWith(contact_html);
					//namesFromColabusOffContacts();
				} else {
					Gab.insert_contact(contact_html);
				}
			}
		});
		return true;
	},

	//When msg will come, this code will execute
	on_message: function (message) {
		var full_jid = $(message).attr('from');
		var jid = Strophe.getBareJidFromJid(full_jid);
		var jid_id = Gab.jid_to_id(jid);
		var d = new Date();
		var time = d.getHours() + ":" + d.getMinutes();
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var msgtime = tConvert(time) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();
		//console.log("msgtime----"+msgtime);
		var flag = true;
		var imgType = $('#' + jid_id).attr('imgType');
		//console.log("imgType" +imgType);
		var type = 'none';
		var link = '';
		var filename = '';
		var popup = $('#' + jid_id + 'm').attr('value');
		var atpos = jid.indexOf("@");
		//console.log("jid_id"+jid_id);
		var id = jid_id.split("-")[0];
		var composing = $(message).find("composing");
		var msgStatus = $(message).children('json').text();
		var msgId = $(message).attr('msgid');

		var compose = false;
		var body = $(message).find('html > body');
		if (msgStatus != "") {
			compose = true;
			msgStatus = JSON.parse($(message).children('json').text());
			if (msgStatus.status == "seen") {
				$('#chat-dialog div#' + msgId + ' .chat-outgoing-status').html("<i> Read</i>").removeClass("chat-outgoing-status-sent").removeClass("chat-outgoing-status-deliver").addClass("chat-outgoing-status-seen");;
			} else if (msgStatus.status == "delivered") {
				$('#chat-dialog div#' + msgId + ' .chat-outgoing-status').html("<i> Delivered</i>").removeClass("chat-outgoing-status-sent").addClass("chat-outgoing-status-deliver");
			} else if (msgStatus.status == "allseen") {
				$('#chat-dialog  .chat-outgoing-status').html("<i> Read</i>").removeClass("chat-outgoing-status-sent").removeClass("chat-outgoing-status-deliver").addClass("chat-outgoing-status-seen");
			} else if (msgStatus.status == "alldelivered") {
				$('#chat-dialog  .chat-outgoing-status-sent').html("<i> Delivered</i>").removeClass("chat-outgoing-status-sent").addClass("chat-outgoing-status-deliver");
			}
		}


		var imgUrl = lighttpdPath + "/userimages/" + id + "." + imgType;
		if (composing.length > 0) {
			compose = true;
			$("#conversationContactMsg_" + Strophe.getNodeFromJid(jid)).hide();
			$("#conversationContactMsg1_" + Strophe.getNodeFromJid(jid)).show();
			$("#conversationContactMsg1_" + Strophe.getNodeFromJid(jid)).text("typing...").delay(3200).hide(100);
			$("#conversationContactMsg_" + Strophe.getNodeFromJid(jid)).delay(3200).show(100);
			$(".userPresStatus_" + Strophe.getNodeFromJid(jid)).hide();
			$(".userStatus_" + Strophe.getNodeFromJid(jid)).show();
			$(".userStatus_" + Strophe.getNodeFromJid(jid)).text("typing...").delay(3200).hide(100);
			$(".userPresStatus_" + Strophe.getNodeFromJid(jid)).delay(3200).show(100);

		}
		else {
			if (body.length === 0) {
				body = $(message).find('body');
				if (body.length > 0) {
					body = body.text();
				}
				else {
					body = null;
				}
			}
			else {

				body = body.contents();
				var span = $('<span></span>');
				body.each(function () {
					if (document.importNode) {
						$(document.importNode(this, true)).appendTo(span);
					} else {
					}
				});
				body = span;

			}
		}

		if (compose == false) {

			if ((!$('#' + jid_id + 'm' + ' #chat-dialog').is(':visible'))) {

				if ($("#conversationList ul #" + jid_id).length == 0) {
					//console.log("not exist");
					showPersonalConnectionMessages();
				}
				else {
					//console.log("it exist");
					if (body) {
						$("#conversationList ul #" + jid_id).find(".coversationContactTime").html(msgtime);
						$("#conversationContactMsg_" + id).html(body);
						$('#conversationContactMsg_' + id).css({ 'font-weight': ' bold' });
						$("#conversationContactMsg1_" + id).hide();
						$("#conversationContactMsg_" + id).show();
						var chatUi = $("#conversationList ul").find("#" + jid_id).clone();
						$("#conversationList ul").find("#" + jid_id).remove();
						$("#conversationList ul").prepend(chatUi);

						var countValue = $("#conversationMsgCount_" + id).html();
						$("#conversationMsgCount_" + id).css({ 'visibility': ' visible' });

						/*** Below line of code---- comment for CME alone and Uncomment for Colabus CME. **/
						var totalMsg = window.opener.getCount("notvisible");

						//console.log("totalMsg befor---"+totalMsg);
						if (countValue == undefined) {
							countValue = 1;
						} else {
							countValue++;
						}
						$("#conversationMsgCount_" + id).html(countValue);
					}
					//console.log("countValue after---"+countValue);
				}
				//var response = showPersonalConnectionMessages();
				//if(response == "sucess"){
				// window.opener.totalMsge();
				//playSound('glass');
				if (body) {
					if ((body.indexOf('xml') > 0) && (body.indexOf('href') > 0) && (body.indexOf('filesharing') > 0)) {
						var $xml = $.parseXML(body);
						filename = $($xml).find('href').attr('fileName');
						if (filename.indexOf('CH(38)') > 0) {
							filename = filename.replace("CH(38)", "&");
						}
						var imgUrl = lighttpdPath + "/userimages/" + id + "." + imgType;
						ChangeImage1(imgUrl, filename);
						$("#conversationContactMsg_" + id).html(filename);
						$('#conversationContactMsg_' + id).css({ 'font-weight': ' bold' });
					} else {
						ChangeImage1(imgUrl, body);
					}
				}
				//}
				if (body) {
					seenMsg(jid_id, "delivered", msgId);
				}
			}
			else {
				var conId = $("#" + jid_id + "m").attr("conid");
				updateSeenMessages(conId, userId, "chat");
				if (body) {
					seenMsg(jid_id, "seen", msgId);
				}
				//console.log("else block next");
			}
			playSound('glass');
			// this code block is to handle file sharing 
			try {
				if (body) {
					if ((body.indexOf('xml') > 0) && (body.indexOf('href') > 0) && (body.indexOf('filesharing') > 0)) {
						//console.log("if block");
						var $xml = $.parseXML(body);
						link = $($xml).find('href').attr('value');
						type = 'filesharing';
						filename = $($xml).find('href').attr('fileName');
						if (filename.indexOf('CH(38)') > 0) {
							filename = filename.replace("CH(38)", "&");
						}
						//    $("#conversationContactMsg_"+id).html(filename);
						//    $('#conversationContactMsg_'+id).css({'font-weight':' bold'});
						//var imgUrl = lighttpdPath+"/userimages/"+id+"."+imgType ;
						// var test = ChangeImage1(imgUrl,filename);
					}
				}
			} catch (eMsg) {
				console.log(eMsg);
			}

			if (body) {
				var t = getTimeIn12Format(d);
				$('#chat-' + jid_id + '.chat_event').remove();
				var id = jid_id.split("-")[0];
				if (flag == true && type == "none") {
					var msg = textTolink(body);
					var replyId = $(message).children('replyForMsgId').text();
					//var msggId = $(message).children('msgid').text();
					var msgAction = $(message).children('chatFortype').text();

					if (replyId != undefined && replyId != 0 && msgAction != undefined && msgAction != "forward") {
						var replyUserId = $(message).children('replyForUserId').text();
						var replyName;
						if (replyUserId == userId) {
							replyName = "You"
						} else {
							replyName = $("#" + replyUserId).find(".user_box_name").text();
						}
						var mainChatThread = $(message).children('replymsg').text();
						if ($("#chat-dialog").find($("#" + msgId)).length == 0) {
							$('#' + jid_id + 'm' + ' #chat-dialog').append(
								"<div id='" + msgId + "' class='single-chat-income-overall row chatId' style='outline:none' tabindex='0'>" +
								"<div class='chat-income-imageframe col-xs-2' style='display:none'>" +
								"<img  userId='" + id + "' src='" + lighttpdPath + "/userimages/" + id + "." + imgType + "?" + imgTime + "' class='incomeframeImage'  onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>" +
								"<div class='col-xs-10' style = 'margin-left: -2.2em;' >" +
								"<div  onclick='replyForChat(this)' class='chat-income-message singlebubble row'>" +
								"<div class='replyNameTop'>" + replyName + "</div>" +
								"<div class='replyMsgBottom'>" + mainChatThread + "</div>" +
								"<div messageType='text'  class='replyToReply ChatText' >" +
								msg +
								"</div>" +
								"</div><div class='chat-income-time row' style='width:99%'>" + t + "</div></div></div>");
						}
					} else if (msgAction != undefined && msgAction == "forward") {
						if ($("#chat-dialog").find($("#" + msgId)).length == 0) {
							$('#' + jid_id + 'm' + ' #chat-dialog').append(

								"<div id='" + msgId + "' class='single-chat-income-overall row chatId' style='outline:none' tabindex='0'>" +
								"<div class='chat-income-imageframe col-xs-2' style='display:none'>" +
								"<img  userId='" + id + "' src='" + lighttpdPath + "/userimages/" + id + "." + imgType + "?" + imgTime + "' class='incomeframeImage' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>" +
								"<div class='col-xs-10' style = 'margin-left: -2.2em;' >" +
								"<div  onclick='replyForChat(this)' class='chat-income-message singlebubble row'>" +
								"<div style='margin-left: -8px;margin-top: -5px;'>" +
								"<img title='Forwarded' class='forwardIconCls' src='" + path + "/images/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>" +
								"<div messageType='text' class='ChatText' style='margin-left: -8px;margin-top: 4px;'>" +
								msg +
								"</div>" +
								"</div><div class='chat-income-time row' style='width:99%'>" + t + "</div></div></div>");
						}
					} else {
						if ($("#chat-dialog").find($("#" + msgId)).length == 0) {
							$('#' + jid_id + 'm' + ' #chat-dialog').append(
								"<div id='" + msgId + "' class='single-chat-income-overall row chatId' style='outline:none' tabindex='0'>" +
								"<div class='chat-income-imageframe col-xs-2' style='display:none'>" +
								"<img  userId='" + id + "' src='" + lighttpdPath + "/userimages/" + id + "." + imgType + "?" + imgTime + "' class='incomeframeImage' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>" +
								"<div class='col-xs-10' style = 'margin-left: -2.2em;' >" +
								"<div  onclick='replyForChat(this)' class='chat-income-message singlebubble row'>" +
								"<div messageType='text' class='ChatText' >" +
								msg +
								"</div>" +
								"</div><div class='chat-income-time row' style='width:99%'>" + t + "</div></div></div>");
						}
					}
				} else {

					if (flag == true && type == "filesharing") {

						var img = getImageType(link.split('.')[1]);
						if ($("#chat-dialog").find($("#" + msgId)).length == 0) {
							$('#' + jid_id + 'm' + ' #chat-dialog').append(
								"<div class='single-chat-income-overall row' style='outline:none' tabindex='0'>" +
								"<div class='chat-income-imageframe col-xs-2' style='display:none'>" +
								"<img  src='" + lighttpdPath + "/userimages/" + id + "." + imgType + "?" + imgTime + "'  onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>" +
								"<div class='col-xs-10' style = 'margin-left: -2.2em;' >" +
								"<div class='chat-income-message singlebubble row'>" +
								"<a style='color:#fff' target='_blank' href='" + link + "' >" + filename + "</a>" +
								"</div><div class='chat-income-time row' style='width:99%'>" + t + "</div></div></div>");

						}
					}
				}

				//$("#chat-dialog").animate({ scrollTop: $(document).height() }, "slow");
				if ($('#' + jid_id + 'm' + ' #chat-dialog').is(':visible')) {
					var scrollUpdate = document.getElementById("chat-dialog");
					scrollUpdate.scrollTop = scrollUpdate.scrollHeight;
				}

				//$('#' + jid_id+'m' + ' .single-chat-income-overall:last').focus();				
				// This method is from a plug-in ifvisible.min.js which is imported in chat.jsp for more details check chat_desktop_notification.js       
				if (($('#' + jid_id + 'm' + ' #chat-dialog').is(':visible')) && ($('#conversationList').is(':visible'))) {

					//showPersonalConnectionMessages();

					/*** Below line of code- comment for CME alone and Uncomment for Colabus CME. **/
					var totalMsg = window.opener.getCount("notvisible");

					$("#conversationList ul #" + jid_id).find(".coversationContactTime").html(msgtime);
					if (type == "filesharing") {
						$("#conversationContactMsg_" + id).html(filename);

					} else {
						$("#conversationContactMsg_" + id).html(body);
					}
					$("#conversationContactMsg1_" + id).hide();
					$("#conversationContactMsg_" + id).show();
					$('#conversationContactMsg_' + id).css({ 'font-weight': ' bold' });
				}

			}
		}
		return true;
	},

	on_public_message: function (message) {
		var from = $(message).attr('from');
		var body = $(message).children('body').text();
		var room = Strophe.getBareJidFromJid(from);
		var Id = room.split('@')[0];
		var nick = Strophe.getResourceFromJid(from);
		//var roomType = $(message).attr('roomType');		
		var FromName = from.split('/')[1];
		var origin = from.split('/')[0];
		//console.log("origin--"+origin)
		var roomType;
		if (origin.indexOf("@mucr.devchat.colabus.com") > -1 || origin.indexOf("@mucr.chat.colabus.com") > -1) {
			roomType = "groupmsg";
		} else {
			roomType = "conference";
		}
		var userid;
		var senderName;
		var senderImgType;
		if (nick !== null) {
			senderName = nick.split('_')[2];
			senderImgType = nick.split('_')[3];
			userid = nick.split('_')[1];
		}
		var d = new Date();

		var time = d.getHours() + ":" + d.getMinutes();
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var msgtime = tConvert(time) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

		var id = Id + "m";
		var t = getTimeIn12Format(d);
		var msg = textTolink(body);

		//console.log("roomType---"+roomType);
		if (roomType != "conference") {
			var d = new Date();

			var time = d.getHours() + ":" + d.getMinutes();
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			var msgtime = tConvert(time) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

			var id = Id + "m";
			var t = getTimeIn12Format(d);
			var msg = textTolink(body);

			//if((!$('#'+id+' #chat-dialog').is(':visible'))||(ifvisible.now('hidden'))){
			if (userid != userId) {
				playSound('glass');
				//window.opener.totalMsge();
				var imgUrl = lighttpdPath + "/userimages/" + userid + "." + senderImgType;
				ChangeImage1(imgUrl, body);
			}
			$("#conversationList ul").find("#" + Id).find(".coversationGroupContactMsg").html(body);
			$("#conversationList ul").find("#" + Id).find(".coversationGroupContactMsg").show();
			$("#conversationList ul").find("#" + Id).find(".coversationGroupContactTime").html(msgtime);
			if (!$('#' + id + ' #chat-dialog').is(':visible')) {
				//showPersonalConnectionMessages();

				var chatUi = $("#conversationList ul").find("#" + Id).clone();
				$("#conversationList ul").find("#" + Id).remove();
				$("#conversationList ul").prepend(chatUi);
				var countValue = $("#conversationList ul").find("#" + Id).find(".chatnotification").html();
				$("#conversationList ul").find("#" + Id).find(".chatnotification").css({ 'visibility': ' visible' });
				if (countValue == undefined) {
					countValue = 1;
				} else {
					countValue++;
				}
				$("#conversationList ul").find("#" + Id).find(".chatnotification").html(countValue);

				/*** Below line of code- comment for CME alone and Uncomment for Colabus CME. **/
				var totalMsg = window.opener.getCount("notvisible");

			} else {
				if (userid != userId) {
					var msgId = $(message).attr('msgid');
					var replyId = $(message).children('replyForMsgId').text();

					if (replyId != undefined && replyId != 0) {
						var replyUserId = $(message).children('replyForUserId').text();
						var replyName;
						if (replyUserId == userId) {
							replyName = "You"
						} else {
							replyName = $("#" + replyUserId).find(".user_box_name").text();
						}
						var mainChatThread = $(message).children('replymsg').text();

						$('#' + Id + 'm' + ' #chat-dialog').append(
							"<div id='" + msgId + "' class='chat-income-overall row chatId' style='outline:none' tabindex='0'>" +
							"<div style='width:100%;float:left'>" +
							"<div class='chat-income-imageframe col-xs-1' style='margin-top: 23px;'>" +
							"<img  userId='" + userid + "' src='" + lighttpdPath + "/userimages/" + userid + "." + senderImgType + "?" + imgTime + "'  title='" + senderName + "' onerror='userImageOnErrorReplace(this);' class='incomeframeImage' style='width: 40px; height: 38px; border-radius: 50%;'/>" +
							"</div>" +
							"<div class='col-xs-10 chatIncomeBubble' style = 'padding-left: 0vh;margin-left:-1em' >" +
							"<div class='row' align='left' style='font-family: helvetica; font-size: 13px; width: 105%; padding-left: 34px;'>" + senderName + "</div>" +
							"<div onclick='replyForChat(this)' class='chat-income-message bubble row'>" +
							"<div class='replyNameTop'>" + replyName + "</div>" +
							"<div class='replyMsgBottom'>" + mainChatThread + "</div>" +
							"<div messageType='text'  class='replyToReply ChatText'>" +
							msg +
							"</div>" +
							"</div>" +
							"<div class='chat-income-time row' style='padding-left: 34px;'>" + t + "</div>" +
							"</div>" +
							"</div>" +
							"</div>");

					} else {
						$('#' + Id + 'm' + ' #chat-dialog').append(
							"<div id='" + msgId + "' class='chat-income-overall row chatId' style='outline:none' tabindex='0'>" +
							"<div style='width:100%;float:left'>" +
							"<div class='chat-income-imageframe col-xs-1' style='margin-top: 23px;'>" +
							"<img  userId='" + userid + "' src='" + lighttpdPath + "/userimages/" + userid + "." + senderImgType + "?" + imgTime + "'  title='" + senderName + "' onerror='userImageOnErrorReplace(this);' class='incomeframeImage' style='width: 40px; height: 38px; border-radius: 50%;'/>" +
							"</div>" +
							"<div class='col-xs-10 chatIncomeBubble' style = 'padding-left: 0vh;margin-left:-1em' >" +
							"<div class='row' align='left' style='font-family: helvetica; font-size: 13px; width: 105%; padding-left: 34px;'>" + senderName + "</div>" +
							"<div onclick='replyForChat(this)' class='chat-income-message bubble row'>" +
							"<div messageType='text'  class='ChatText'>" +
							msg +
							"</div>" +
							"</div>" +
							"<div class='chat-income-time row' style='padding-left: 34px;'>" + t + "</div>" +
							"</div>" +
							"</div>" +
							"</div>");

					}
					var windowWidth1 = $(window).width();
					if (windowWidth1 < 750) {
						$(".incomeframeImage").css({ 'margin-left': '-7vh' });
					} else {
						$(".incomeframeImage").css({ 'margin-left': '-4vh' });
					}
					playSound('glass');
				}
				var h = $('#' + id).find('#chat-dialog')[0].scrollHeight;
				$('#' + id).find('#chat-dialog').scrollTop(h);
			}

			$("#chat-dialog").mCustomScrollbar("update");
		} else {
			//console.log("insdie on group");
			var full_jid = $(message).attr('from');
			var jid = Strophe.getBareJidFromJid(full_jid);
			var jid_id = Gab.jid_to_id(jid);
			var type = 'none';
			var id = jid_id.split("-")[0];
			//console.log(userConnected);
			if (userid != userId) {
				//console.log("------inside userif");
				var composing = $(message).find("composing");
				if (!(composing.length > 0)) {
					gotVideoRoomMsg(message, userid);
					if (userConnected.indexOf(parseInt(userid)) < 0) {
						gotMessageFromServer(parseInt(userid), message);
					}

				} else {
					$("#videochat_" + userid).show();
					$("#videochat_" + userid).html(" is typing...").delay(2200).hide(100);
				}
			}

		}

		return true;
	},

	on_normal_message: function (message) {
		//console.log("inside normal");
		var full_jid = $(message).attr('from');
		var jid = Strophe.getBareJidFromJid(full_jid);
		var jid_id = Gab.jid_to_id(jid);
		var type = 'none';
		var id = jid_id.split("-")[0];
		var imgType = $('#' + jid_id).attr('imgType');
		var imgUrl = lighttpdPath + "/userimages/" + id + "." + imgType;
		//console.log("inside normal1");
		var jsonMsg = $(message).children('json').text();
		//console.log("ijsonMsg--"+jsonMsg);
		var composing = $(message).find("composing");

		var compose = false;
		if (composing.length > 0) {
			compose = true;
			$(".userPresStatus_" + Strophe.getNodeFromJid(jid)).hide();
			$(".userStatus_" + Strophe.getNodeFromJid(jid)).show();
			$(".userStatus_" + Strophe.getNodeFromJid(jid)).text("typing...").delay(3200).hide(100);
			$(".userPresStatus_" + Strophe.getNodeFromJid(jid)).delay(3200).show(100);
		}
		if (compose == false) {
			gotMessageFromServer(parseInt(id), message);
		}

		return true;
	},


	presence_value: function (elem) {

		if (elem.hasClass('online')) {
			return 2;
		} else if (elem.hasClass('away')) {
			return 1;
		}
		return 0;
	},

	//For insert contact to list
	insert_contact: function (elem) {
		//console.log("inside contacts");
		var pname = elem.find('.user_box_name').attr('value');
		if (pname != undefined) {
			var jid = elem.find('.roster-jid').val();
			var pres = Gab.presence_value(elem.find('.roster-contact'));
			var contacts = $('#chat-box li');
			//scrollbarSinglechat(); 

			if (contacts.length > 0) {
				//console.log("inside contacts1");
				var inserted = false;

				contacts.each(function () {

					var cmp_pres = Gab.presence_value($(this).find('.roster-contact'));
					var cmp_jid = $(this).find('.roster-jid').val();
					var cmp_name = $(this).find('.user_box_name').attr('value');
					//console.log(cmp_pres+"--"+pres);
					if (pres > cmp_pres) {
						$(this).before(elem);
						inserted = true;
						return false;
					} else {

						var n = pname.localeCompare(cmp_name);
						if (n == -1) {
							$(this).before(elem);
							inserted = true;
							return false;
						}
					}
				});

				if (!inserted) {
					//console.log("inside not inserted");
					$('#chat-box ul').append(elem);
					//namesFromColabusOffContacts();
					// scrollbarSinglechat(); 
				}
				//namesFromColabusOffContacts();

			} else {
				// console.log("inside  inserted");
				$('#chat-box ul').append(elem);
				//namesFromColabusOffContacts();
				//scrollbarSinglechat(); 
			}
		}

	}

}

/*function scrollbarSinglechat(){
	
	 $("div#chat-box").mCustomScrollbar('destroy');
	 $("div#chat-box").mCustomScrollbar({
			scrollButtons:{
				enable:true
			},
		});
		
		$('div#chat-box .mCSB_container').css('margin-right','15px');
		$('div#chat-box .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
		$('div#chat-box .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
		$('div#chat-box .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
		$('div#chat-box .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
		$('div#chat-box .mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
		$('div#chat-box .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
		$('div#chat-box .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
		$('div#chat-box .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');

	
}

function gerneralScrollBarFunction(id){
	
	 $("div#"+id+"").mCustomScrollbar('destroy');
	 $("div#"+id+"").mCustomScrollbar({
			scrollButtons:{
				enable:true
			},
		});
		
		$('div#'+id+' .mCSB_container').css('margin-right','15px');
		$('div#'+id+' .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
		$('div#'+id+' .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
		$('div#'+id+' .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
		$('div#'+id+' .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
		$('div#'+id+' .mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
		$('div#'+id+' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
		$('div#'+id+' .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
		$('div#'+id+' .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');

}*/

$(function () {
	num = Math.round(window.innerWidth / 250) - 1;


	//	resizeChatWindow();

	$('#searchText,#searchText2,#searchText3').val(''); // To clear the search value

	$('#searchText').on('keyup', function () {        // for search of users in single chat
		var txt = $('#searchText').val().toLowerCase();
		if (txt.length > 0) {
			$('#searchCancel').show();
		} else {
			$('#searchCancel').hide();
		}


		$('#chat-box li').each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding

			var val = $(this).find('.user_box_name').attr('value').toLowerCase();

			if (val.indexOf(txt) != -1) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});

	});

	$('#searchCancel').on('click', function () {
		$('#searchText').val('');
		$('#searchCancel').hide();
		$('#chat-box li').show();
	});

	//connect();
	var mainframe = $(document).height();
	var h = mainframe - 108;
	var H = mainframe - 267;

	//  $('.chat-box').css('height',h+"px");
	//     $('#chat-box, #groupChat-box').css('height',H+"px");

	$('#disconnect').on('click', function () {
		Gab.connection.disconnect();
		Gab.connection = null;
	});

	/*To show chat name on tab while another tab is open*/
	/* $(window).focus(function(){
		 isActive = true;
		 clearInterval(interval);
		 $('#t').text(colabusTitle); 
	 });*/

	$(window).blur(function () {
		isActive = false;
	});

	window.onbeforeunload = function () {

		/*** Below line of code- comment for CME alone and Uncomment for Colabus CME. **/
		try {
			window.opener.releaseChatSession();
		} catch (e) {
			console.log('Null value handled!');
		}


		//disconnect();
		//return "Do you really want to close+chat?";
	};

});


/*function changeTitle(){
	var blink =  $('#t').text() == colabusTitle ? (NameTitle+" messaged You") : colabusTitle;
	$('#t').text(blink);
}*/

/*
 * @In this function we are getting the username registered in colabus so that we will load only those users in chat
 */
function namesFromColabus() {
	window.opener.gLocation();
	$("#loadingBar").show();
	$("#loadMsg").show();
	chatDomain = checkChatDomain();
	$.ajax({

		url: path + "/connectAction.do",
		type: "post",
		data: { act: "loadCompanyContactsforChat", companyId: companyId, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				//checkSessionTimeOut(result); 
				userName = jQuery.parseJSON(result);
				$('#sound').data('userName', userName);
				connect();
			}
		}
	});

}

function namesFromColabusOffContacts() {
	//console.log("inside off");
	$.ajax({
		url: path + "/connectAction.do",
		type: "post",
		data: { act: "loadCompanyOffContactsforChat", companyId: companyId, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			$("#loadingBar").hide();
			$("#loadMsg").hide();
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				//checkSessionTimeOut(result);
				onlineUser = jQuery.parseJSON(result);
				var udata = '';
				var userStatus = false;
				var userOstats = false;
				for (var i = 0; i < onlineUser.length; i++) {
					offuserId = onlineUser[i].offuserId;
					user_status = onlineUser[i].user_status;
					userStatus = $('#' + offuserId + '-' + chatDomain + '-colabus-com').find(".roster-contact").hasClass("offline");
					userOstats = $('#' + offuserId + '-' + chatDomain + '-colabus-com').find(".roster-contact").hasClass("offline");
					//console.log("userStatus--"+userStatus);
					var convContact1 = $('#conversationList li#' + offuserId + '-' + chatDomain + '-colabus-com' + ' .conversationContactRow');
					var convObs1 = convContact1.find("div.user_box_status");
					if (user_status == "Y" && userStatus == true) {
						$('#' + offuserId + '-' + chatDomain + '-colabus-com').find(".roster-contact").removeClass("offline").addClass("pOnline");
						$('#' + offuserId + '-' + chatDomain + '-colabus-com').find(".user_box_status").addClass("user_box_status_away");
						udata = $('#' + offuserId + '-' + chatDomain + '-colabus-com').clone();
						$('#' + offuserId + '-' + chatDomain + '-colabus-com').remove();
						$('#chat-box ul').find('.offline:first').parent('li').before(udata);
						userStatus = false;
						var elem = $('#' + offuserId + '-' + chatDomain + '-colabus-com').find('pOnline').sort(sortMe);
						$('#chat-box ul').append(elem);
						convObs1.removeClass("user_box_status_offline");
						convObs1.removeClass("user_box_status_online");
						convObs1.addClass("user_box_status_away");
					}
				}
			}


		}
	});

}

/* @This method is used to send a message to selected chat user
 * 
 */
var tempHeight = 31;
var chatFortype = "normal";
function sending(ev, obj, type) {
	if (imageShre != undefined && imageShre != '') {
		$('.chatpost').children().attr('onclick', "postCanvasToURLImageForClipboardInChat()"); // Function call to initiate clipboard sharing.
	}

	if (ev.which == 8) {
		var textValue = $('#text-box').children().find("textarea").val();
		if (textValue == "") {
			defaultChatSize();
		} else {
			resizeChatBox();
		}

		return;
	}

	if (type !== "video") {
		var id = $(obj).parent().parent().parent().attr('id');  // getting the id from chat box
		var jid = id.split('-')[0] + "@" + chatDomain + ".colabus.com";
		var status = 'N';
		var d = new Date();

		if (ev.which == 13 || ev.which == 1) {

			if (ev.which == 13) {
				ev.preventDefault();
				var body = $(obj).val();

			}
			if (ev.which == 1) {
				ev.preventDefault();
				var body = $('#text-box').children().find("textarea").val();

			}
			if ($.trim(body).length < 1)
				return;
			//var message = $msg({to : jid, "type" : "chat" }).c('body').t(body).up().c('active', {xmlns: "http://jabber.org/protocol/chatstates"}); // preapring the chat stanza to send		 
			/*var message = $msg({to : jid, "type" : "chat" }).c('body').t(body); // preapring the chat stanza to send		 
			
			Gab.connection.send(message); */ // sending the chat message
			var Id = id.split("-")[0];
			//console.log("Id--"+Id+"--userId--"+userId);
			var t = getTimeIn12Format(d);
			var msg = textTolink(body);

			if (chatFortype == "reply") {

				var mainChatThread = replymsg;
				var replyName;
				if ($(".replythread").hasClass("chat-outgoing-message")) {
					replyName = "You";
					$(".singlebubble2").css('background-color', '#007b97');
					$(".singlebubble2").removeClass("replythread");
					$("#text").attr("placeholder", "Type message here...");
				} else {
					replyName = $("#" + Id).find(".user_box_name").text();
					$(".singlebubble").css('background-color', '#0b4860');
					$(".singlebubble").removeClass("replythread");
					$("#text").attr("placeholder", "Type message here...");
				}
				$("#" + id).find('#chat-dialog').append(              // preparing the chat message structure to be appended on chatBox
					"<div class='chat-outgoing-overall row chatId'>" +
					"<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>" +
					"<img userId ='" + userId + "' src='" + lighttpdPath + "/userimages/" + userId + "." + userImgType + "?" + imgTime + "' class='incomeframeImage' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%; float:right;margin-left:-2.3vh;margin-top: 1vh;margin-bottom:2vh;'/></div>" +
					"<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right'>" +
					"<div  onclick='replyForChat(this)' class='chat-outgoing-message singlebubble2 row'>" +
					"<div class='replyNameTop'>" + replyName + "</div>" +
					"<div class='replyMsgBottom'>" + mainChatThread + "</div>" +
					"<div messageType='text' class='replyToReply ChatText' >" +
					msg +
					"</div>" +
					"</div>" +
					"<div class='chat-outgoing-time row'><span>" + t + "</span><span class='chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div>");
			} else {
				$("#" + id).find('#chat-dialog').append(              // preparing the chat message structure to be appended on chatBox
					"<div class='chat-outgoing-overall row chatId'>" +
					"<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>" +
					"<img userId ='" + userId + "' src='" + lighttpdPath + "/userimages/" + userId + "." + userImgType + "?" + imgTime + "' class='incomeframeImage' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%; float:right;margin-left:-2.3vh;margin-top: 1vh;margin-bottom:2vh;'/></div>" +
					"<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right'>" +
					"<div  onclick='replyForChat(this)' class='chat-outgoing-message singlebubble2 row'>" +
					"<div messageType='text'  class='ChatText' >" +
					msg +
					"</div>" +
					"</div>" +
					"<div class='chat-outgoing-time row'><span>" + t + "</span><span class='chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div>");
			}

			var divRef = $("#" + id).find('#chat-dialog  .chat-outgoing-overall:last');
			//    Gab.scroll_chat(Gab.jid_to_id(jid));
			$('#' + id).find('textarea').val('');
			$("#" + id).find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
			//console.log("body---"+body);
			var windowWidth1 = $(window).width();

			if (windowWidth1 < 750) { // to resize the chat conversation UI for smaller size
				//$(".chatOutbubble").css({'padding-right':'2.3em'});
			} else {
				//$(".chatOutbubble").css({'padding-right':'0.3em'});
			}

			//  var n = d.getTimezoneOffset();
			var m = d.getMonth();
			var time = d.getHours() + ":" + d.getMinutes() + "###" + d.getDate() + "-" + (Number(m) + 1) + "-" + d.getFullYear();
			var timeZone = getTimeOffset(d);
			// console.log("Timezone----"+timeZone);
			var convId = $("#" + id).attr('conId');

			var ID = id.substr(0, id.length - 1);
			//  alert(ID);

			if ($("#" + ID).find('div.user_box_status').hasClass('user_box_status_online')) { // status for notification
				status = 'N';
			}
			// console.log(status);
			$("#chat-dialog").mCustomScrollbar("update");
			body = body.replace("'", "CHR(39)");

			var time1 = d.getHours() + ":" + d.getMinutes();
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();
			defaultChatSize();
			/*chatHeight = $("#chat-dialog").height();
			divheight =  $("#text").height();
			resizeChatBox();*/


			$.ajax({                        // This ajax call is to insert the chat message into the database
				url: path + "/ChatAuth",
				type: "post",
				data: { act: "insertChatMsg", convId: convId, sender: userId, message: body, time: time, timeZone: timeZone, msgStatus: status, msgType: "text", docId: '0', docExt: '', docPath: '', replyid: replyForMsgId, chatFortype: chatFortype, device: 'web' },
				mimeType: "textPlain",
				success: function (result) {
					sessionTimeOutMethod(result);
					if (result != "SESSION_TIMEOUT") {
						var msgId = result.toString().split("@@")[1];
						$(divRef).attr("id", msgId);
						body = body.replace("CHR(39)", "'");

						var message = $msg({ to: jid, "type": "chat", "msgid": msgId }).c('body').t(body)	 // preparing the one to one chat stanza to send	
							.up().c('replyForMsgId').t(replyForMsgId) //should take the value of msgid of the reply thread
							.up().c('replyForUserId').t(replyForUserId) //should take the value of userid of the reply thread
							.up().c('replymsg').t(replymsg) //should be the content of reply thread
							.up().c("chatFortype").t(chatFortype); //should take the value of chatFortype value for the reply thread
						Gab.connection.send(message);
						restoreValue();
					}
				}

			});

			if ($("#conversationContactMsg_" + Id).length) {
				$("#conversationList ul ").find("#coversationContactTime_" + Id).html(msgtime);
				$("#conversationContactMsg_" + Id).html(body);
				$("#conversationContactMsg1_" + Id).hide();
				$("#conversationContactMsg_" + Id).show();
				var newjid = Id + '-' + chatDomain + '-colabus-com';
				var newchatUi = $("#conversationList ul").find("#" + newjid).clone();
				$("#conversationList ul").find("#" + newjid).remove();
				$("#conversationList ul").prepend(newchatUi);
			} else {
				showPersonalConnectionMessages();
			}

			$(this).parent().data('composing', false);

		} else {
			resizeChatBox();


			var notify = $msg({ to: jid, "type": "chat" }).c('composing', { xmlns: "http://jabber.org/protocol/chatstates" });
			Gab.connection.send(notify);

		}
	}
	else {
		var jid = obj + "@" + chatDomain + ".colabus.com";
		var jsonText = '{"calltype":"AV","type":"callrequest"}';
		var notify = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
		Gab.connection.send(notify);
	}
}
function sendingGroupText(ev, obj) {
	if (ev.which == 8) {
		var textValue = $('#text-box').children().find("textarea").val();
		if (textValue == "") {
			defaultChatSize();
		} else {
			resizeChatBox();
		}
		return;
	}
	if (ev.which == 13 || ev.which == 1) {
		if (ev.which == 13) {
			var id = $(obj).parent().parent().parent().attr('id');
			var jid = id.split('m')[0] + '@mucr.' + chatDomain + '.colabus.com';
			ev.preventDefault();
			var body = $(obj).val();
		}
		if (ev.which == 1) {
			var id = $(obj).parent().parent().attr('id');
			var jid = id.split('m')[0] + '@mucr.' + chatDomain + '.colabus.com';
			ev.preventDefault();
			var body = $('#text-box').children().find("textarea").val();
		}
		if ($.trim(body).length < 1)
			return;

		var d = new Date();
		var t = getTimeIn12Format(d);
		var msg = textTolink(body);
		//var title= $('#'+userId).find('.user_box_name').text();

		if (chatFortype == "reply") {

			var mainChatThread = replymsg;
			var replyName;
			if ($(".replythread").hasClass("chat-outgoing-message")) {
				replyName = "You";
				$(".bubble2").css('background-color', '#007b97');
				$(".bubble2").removeClass("replythread").removeClass('replyGroupBG2');
				$("#text").attr("placeholder", "Type message here...");
			}
			else {
				replyName = $('.bubble').filter(".replythread").prev().text();
				$(".bubble").css('background-color', '#0b4860');
				$(".bubble").removeClass("replythread").removeClass('replyGroupBG');
				$("#text").attr("placeholder", "Type message here...");
			}
			$("#" + id).find('#chat-dialog').append(
				"<div class='chat-outgoing-overall groupOutGoingChat row chatId'>" +
				"<div class='chat-outgoing-imageframe col-xs-1'>" +
				"<img userId='" + userId + "' title= '" + userFullName + "' src='" + lighttpdPath + "/userimages/" + userId + "." + userImgType + "?" + imgTime + "'  onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%; margin-top: 1vh;margin-bottom:2vh;'/></div>" +
				"<div class='col-xs-11 groupOutbubble' style='margin-top: -3vh;margin-left:-2vh; float: right; margin-right: -21px;'>" +
				"<div onclick='replyForChat(this)' class='chat-outgoing-message bubble2 row'>" +
				"<div class='replyNameTop'>" + replyName + "</div>" +
				"<div class='replyMsgBottom'>" + mainChatThread + "</div>" +
				"<div messageType='text'  class='replyToReply ChatText'>" +
				msg +
				"</div>" +
				"</div>" +
				"<div class='chat-outgoing-time row'><span>" + t + "</span></div></div></div>");

		} else {
			$("#" + id).find('#chat-dialog').append(
				"<div class='chat-outgoing-overall groupOutGoingChat row chatId'>" +
				"<div class='chat-outgoing-imageframe col-xs-1'>" +
				"<img userId='" + userId + "' title= '" + userFullName + "' src='" + lighttpdPath + "/userimages/" + userId + "." + userImgType + "?" + imgTime + "'  onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%; margin-top: 1vh;margin-bottom:2vh;'/></div>" +
				"<div class='col-xs-11 groupOutbubble' style='margin-top: -3vh;margin-left:-2vh; float: right; margin-right: -21px;'>" +
				"<div onclick='replyForChat(this)' class='chat-outgoing-message bubble2 row'>" +
				"<div messageType='text' class='ChatText'>" +
				msg +
				"</div>" +
				"</div>" +
				"<div class='chat-outgoing-time row'><span>" + t + "</span></div></div></div>");
		}
		var divRef = $("#" + id).find('#chat-dialog  .chat-outgoing-overall:last');
		var m = d.getMonth();
		var time = d.getHours() + ":" + d.getMinutes() + "###" + d.getDate() + "-" + (Number(m) + 1) + "-" + d.getFullYear();
		var timeZone = getTimeOffset(d);
		id = id.split('m')[0];
		//    console.log("Timezone----"+timeZone+"---id--"+id+"---time----"+time);
		$('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
		$("#chat-content").show();
		$("div#" + id + "m").find("textarea").val('');
		defaultChatSize();
		var windowWidth1 = $(window).width();
		if (windowWidth1 < 750) {

			$(".incomeframeImage").css({ 'margin-left': '-7vh' });
			$(".groupOutGoingChat").css({ 'margin-left': '-3vh' });
			$(".chatIncomeBubble").css({ 'margin-left': '-1em' });
			$("#chat-content").show();

		} else {

			$(".incomeframeImage").css({ 'margin-left': '-4vh' });
			$(".groupOutGoingChat").css({ 'margin-left': '0vh' });
			$(".chatIncomeBubble").css({ 'margin-left': '-1em' });

		}

		var time1 = d.getHours() + ":" + d.getMinutes();
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

		$.ajax({
			url: path + "/ChatAuth",
			type: "post",
			data: { act: "insertGChatMsg", groupId: id, sender: userId, message: body, time: time, timeZone: timeZone, replyid: replyForMsgId, device: 'web' },
			mimeType: "textPlain",
			success: function (result) {
				// checkSessionTimeOut(result);
				sessionTimeOutMethod(result);
				if (result != "SESSION_TIMEOUT") {
					var msgId = result.toString().split("@@")[1];
					$(divRef).attr("id", msgId);

					var message = $msg({ to: jid, "type": "groupchat", "msgid": msgId }).c('body').t(body) //preparing the Group chat stanza to send	
						.up().c('replyForUserId').t(replyForUserId) //should take the value of userid of the reply thread
						.up().c('replymsg').t(replymsg) //should be the content of reply thread
						.up().c('replyForMsgId').t(replyForMsgId); // should take the value of msgid of the reply thread 
					Gab.connection.send(message);
					restoreValue();
				}
			}
		});

		if ($('#conversationList').is(':visible')) {
			if ($("#" + id).length > 0) {
				$("#conversationList ul").find("#" + id).find(".coversationGroupContactMsg").html(body);
				$("#conversationList ul").find("#" + id).find(".coversationGroupContactMsg").show();
				$("#conversationList ul").find("#" + id).find(".coversationGroupContactTime").html(msgtime);
				var chatUi = $("#conversationList ul").find("#" + id).clone();
				$("#conversationList ul").find("#" + id).remove();
				$("#conversationList ul").prepend(chatUi);
			} else {
				showPersonalConnectionMessages();
			}

		}
		//onloadGroup();
	}
	resizeChatBox();

}
/*
 * @This function is to return the date object in 12 hours format
 */
function getTimeIn12Format(d) {

	var hours = d.getHours();
	var m = d.getMinutes();

	var suffix = hours >= 12 ? "PM" : "AM";

	m = m < 10 ? '0' + m : m;

	hours = ((hours + 11) % 12 + 1);
	hours = hours < 10 ? '0' + hours : hours;
	return (hours + ':' + m + " " + suffix);
}

var chatHeight;
var divheight;
function openNewChatBox(obj, type) {
	$("#chat-box").find(".clipboard").removeClass("clipboard");
	$(obj).addClass("clipboard");

	restoreValue();
	if (addconfon == true) {
		return;
	}
	if (type !== "video") {
		var id = obj.id;
	} else {
		id = obj;
	}
	id = id.split("-")[0];
	var name = "";

	if (!isNaN(id)) {   // used from tasks but now not in use
		name = $(obj).attr('name');
		id = id + "-" + chatDomain + "-colabus-com";
	}
	var sid = id.split("-")[0];
	$('#' + id).attr('count', '0');  // setting count for loading of previous messages
	$('#' + id).find('.chatnotification').html('').css('visibility', 'visible');

	var imgSrc = $('#' + id).find('img').attr('src');
	var userName = $('#' + id).find('.user_box_name').text();
	var ui = "<div  id=\"" + id + "m\" class=\"row\">"

		+ "<div id='chatNameHead' class='' style='display:block;height:10.2vh;padding-top:0%;min-height:50px;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;background-color:white;border-radius:0px' class='well'>"
		+ "<img id='userImg' style=\"margin-left: 10px;float:left;height:6vh;width:6vh;margin-top:2vh;margin-bottom:2vh\" class=\"img-circle \" onerror=\"userImageOnErrorReplace(this);\" src=\"" + imgSrc + "\">"
		+ "<div class='user_box_status offline' style='margin-left: -8px; margin-top: 2vh;'></div>"
		+ "<span style='height:10vh;padding-top:2vh;float:left;font-size:13px;font-family: OpenSansRegular;font-weight:bold;padding-left:8px;'>" + userName + ""
		+ "<p class='userStatus_" + sid + "' style='display:none;color:#08B627;font-size:11px'>&nbsp;&nbsp;</p>"
		+ "<p class='userPresStatus_" + sid + "' style='display:block;color:#08B627;font-size:11px'>&nbsp;&nbsp;</p>"
		+ "</span>"
		+ "<img class='backChat' onclick='goBack()'  style='margin-bottom: 1vh; cursor: pointer; float: right; margin-top: 3.3vh; height: 18px;    min-height: 18px; min-width: 16px; width: 16px;margin-right: 10px;' src='" + path + "/images/back1.png'\>"
		+ '<div style="float: right; position: relative;padding-top: 2.4vh;">'
		+ '<img id="chatMoreOptions" src="' + path + '/images/more.png" style="cursor: pointer;float: right;margin-right: 10px;" onclick="chatMoreOptions()">'
		+ '<div id="chatMoreOptionsDiv" align="center" style="display:none;position: absolute;padding: 8px;z-index: 1;background-color: #fff;right: 26px;border: 1px solid #bfbfbf;">'
		+ "<img onclick='startCall(" + sid + ",\"true\",\"video\")' class='video' title='Video Call'  style='cursor:pointer;margin-top: 5px;width: 35px;height: 25px;' src='" + path + "/images/video/vedioGreen.png'\>"
		+ "<img onclick='startCall(" + sid + ",\"true\",\"audio\")' class='video' title='Audio Call' style='cursor:pointer;margin-top: 10px;width: 40px;height: 30px;' src='" + path + "/images/video/Phonegreen.png'\>"
		+ "<img onclick='showChatFromRecomdView(\"" + id + "\")' class='video' title='Messages' style='cursor:pointer;margin-top: 3px;width: 40px;height: 40px;' src='" + path + "/images/video/chatBack2.png'\>"
		+ "<img onclick='showRecommendationsInChat(\"" + id + "m\")' class='video' title='Recommendations' style='cursor:pointer;margin-top: 5px;width: 30px;height: 30px;' src='" + path + "/images/video/recomd.png'\>"
		+ "<img onclick='startCall(" + sid + ",\"true\",\"screenshare\")' class='video' title='Screen Share'  style='display:none;cursor:pointer;width: 38px;height: 26px;margin-top: 12px;margin-bottom: 4px;' src='" + path + "/images/video/screenshare.png'\>"
		+ '</div>'
		+ '</div>'
		+ "</div>"
		+ "<div id='chatOptionHead' class='' style='display:none;height:10.2vh;padding-top:0%;min-height:50px;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;background-color:white;border-radius:0px; padding-right:2vh;' class='well'>"
		+ "<img data-toggle='modal' data-target='#contactPopup' onclick='forwardMsg()' title= 'Forward' style=\"cursor:pointer;margin-left: 10px;float:right;margin-top:3vh;margin-bottom:2vh;width:25px;margin-right:10px\" src='" + path + "/images/forward.svg'\>"
		+ "<img onclick='copyMsg()' title= 'Copy' style=\"cursor:pointer;margin-left: 10px;width: 25px;float:right;margin-top:2.5vh;margin-bottom:2vh;margin-right:10px\" src='" + path + "/images/copy.svg'\>"
		+ "<img onclick='replyMsg()'  title= 'Reply' style=\"cursor:pointer;margin-left: 10px;float:right;margin-top:3vh;margin-bottom:2vh;width:25px;margin-right:10px\"  src='" + path + "/images/reply.svg'\>"
		+ "</div>"
		+ "<div id='chat-dialog' style='height:80vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;border-radius:0px;' id='conv' class='well'>"
		+ "</div>"
		+ "<div id='chat-recomd-dialog' style='display:none;height:90vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;border-radius:0px;' id='conv' class='well'>"
		+ "</div>"

		+ "<div id='video-dialog' style='display:none;height:82vh;padding-top:0%;padding-bottom:0%;margin-top:0vh;margin-bottom:0vh;width:100%;border-radius:0px;' id='conv' class='well'>"
		+ "</div>"

		+ "<div id='text-box' style=' height: 10vh; border-radius:0; class='well row form-group'>"
		+ "<div id ='textborder' class='' style='margin:0;height:10vh;padding:0;border-right:1px solid #c1c5c8;float: left;    width: calc(100% - 40px);'>"
		+ "<textarea id='text' type='text' oncut ='sending(event,this)'  onpaste ='sending(event,this)' onkeyup = 'sending(event,this)' onkeypress='sending(event,this)' style='overflow:hidden;min-height:7vh;padding:2px;border-radius:0%;margin-top:4px;margin-left:4px;width:99%'  placeholder='Type message here...' class='form-control'></textarea>"
		+ "</div>"
		+ "<div class=' chatpost' style='margin-left:0px;background-color:white;height:10vh;float: right;width: 40px;text-align: center;'>"
		//+"<img class='chatpostI' src='"+path+"/images/workspace/post.png' onclick='sending(event,this)' style='height:5vh;min-height:5vh;width:4vh;margin-left:-0.7vh,margin-top:1.5vh'>"
		+ "<img class='chatpostI' src='" + path + "/images/workspace/post.png' onclick='sending(event,this)' style='margin-top:15px,vertical-algin:middle'>"
		+ "</div>"
		+ "</div>"

		+ "</div>";


	getConversationId(id);
	$('#chat-content').html('').html(ui);
	fileSharing(id + 'm');  // Function call to initiate file sharing.
	$(obj).children().find('.chatnotification').css('visibility', 'hidden'); // to hide the chat notification as well as chat box is open
	$(obj).children().find('.chatnotification').html("");
	$('textarea.chatbox').focus(function () {
		$(this).css('border', '1px solid blue');
	});
	$('textarea.chatbox').blur(function () {
		$(this).css('border', '1px solid violet');
	});
	if (type !== "video") {
		resizeChatWindow();
	}
	$('#chat-content').find('div.user_box_status').attr('class', 'user_box_status ' + $("li div#" + sid).find('div.user_box_status').attr("class").split(" ")[1] + '');
	var userpres;
	if ($('#' + id + "m").find('.user_box_status').hasClass("user_box_status_online")) {
		userpres = "online";
		$('.userPresStatus_' + sid).css({ "color": "#08B627" });
	} else {
		userpres = "offline";
		$('.userPresStatus_' + sid).css({ "color": "gray" });
	}
	$('.userPresStatus_' + sid).show().text(userpres);
	chatWindow();
	$("#chat-content").show();
	chatHeight = $("#chat-dialog").height();
	divheight = $("#text").height();
}

function chatMoreOptions() {
	if ($('#chatMoreOptionsDiv').is(':hidden')) {
		$('#chatMoreOptionsDiv').show();
	} else {
		$('#chatMoreOptionsDiv').hide();
	}
}

function playSound(filename) {
	$('#sound').html('<audio  autoplay="autoplay"><source src="' + path + '/sound/' + filename + '.mp3" type="audio/mpeg" /><source src="' + path + '/sound/' + filename + '.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="' + path + '/sound/' + filename + '.mp3" /></audio>');
}

function changeColor(obj) {
	$("#" + obj.id).css("backgroundColor", "lightblue");
}

function changeColor2(obj) {
	$("#" + obj.id).css("backgroundColor", "white");
}

/*
 * @method is used to get unique ConversationId for sender and reciever
 */
function getConversationId(id) {
	var Id = id.split("-")[0];
	$.ajax({
		url: path + "/ChatAuth",
		type: "post",
		data: { act: "checkConvId", fromUser: userId, toUser: Id, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				$("#" + id + "m").attr("conId", result);
				$("#" + id + "m").attr("index", 0);
				getPreviousMessages(result, id);  // loading previous first 20 messages
				updateSeenMessages(result, userId, "chat");
			}

		}
	});
}

function updateSeenMessages(conId, userId, type) {
	//	console.log("conId-----"+conId+"----userId---"+userId);
	$.ajax({
		url: path + "/ChatAuth",
		type: "post",
		data: { act: "updateMsgStatusOfConv", convId: conId, userId: userId, convType: type, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			// checkSessionTimeOut(result);
			//console.log('success-----------'+result);
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				/*** Below line of code- comment for CME alone and Uncomment for Colabus CME. **/
				window.opener.totalMsge();
			}

			/*var obj = window.opener.document.getElementById("chatunreadMessages");
				$(obj).trigger('onclick');*/
		}
	});

}

/*
 * @Method is used to load first 20 previous messages
 */
function getPreviousMessages(conId, id) {
	var jsonData = 0;
	var index = $("#" + id + "m").attr('index');
	var $Id = $("#" + id + "m").find('#chat-dialog');
	var imgType = $("#" + id).attr('imgType');
	var sourceImg = $("#userImg").attr('src');
	var Id = id.split("-")[0];
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	seenMsg(id, "allseen", conId);
	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();

	$.ajax({
		url: path + "/ChatAuth",
		type: "post",
		data: { act: "getPastMsg", userId: userId, convId: conId, indexRg: index, noOfMsgReq: 20, localTZone: timeZone, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			// checkSessionTimeOut(result);
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				if (result == 'No latest msg') {

				} else {
					jsonData = jQuery.parseJSON(result);

					for (i = 0; i < jsonData.length; i++) {
						var UI = "";
						var t = "";
						var d = new Date();
						var time = jsonData[i].time.replace("CHR(26)", ":");
						time = time.replace("CHR(26)", ":");
						var T = time.split("#@#")[1];
						var date = time.split("#@#")[0];
						//  var format = time.split(" ")[2];  
						var messageType = jsonData[i].messageType;
						var fileExt = jsonData[i].fileExt;
						var ioType = jsonData[i].ioType;
						var duration = jsonData[i].duration;
						var callType = jsonData[i].callType;
						var m = d.getMonth();
						m = months[m];
						var date1 = d.getDate();
						date1 = (date1 > 10) ? (date1) : ('0' + date1);

						var D = date1 + "-" + m + "-" + d.getFullYear();

						if (date == D) {
							t = "Today  " + T;
						} else {
							t = T + " , " + date;
						}



						//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"').replace("CHR(26)",":");

						var message = jsonData[i].message;
						message = replaceSpecialCharacter(message);
						var body = textTolink(message);
						var type = jsonData[i].type;
						var messageId = jsonData[i].messageId;
						var message_status = jsonData[i].message_status;
						var msgReplyId = jsonData[i].msg_reply_id;
						var msgAction = jsonData[i].msg_action;
						//console.log("message_status---"+message_status);
						if (type == "chat") {
							if (jsonData[i].sender == userId) {

								UI = "<div id=" + messageId + " class='chat-outgoing-overall row chatId'>"
									+ "<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>"
									+ "<img userId ='" + jsonData[i].sender + "' src='" + lighttpdPath + "/userimages/" + jsonData[i].sender + "." + userImgType + "?" + imgTime + "' onerror='userImageOnErrorReplace(this);' style='float:right;width: 40px; height: 38px; border-radius: 50%; margin-left:-2.3vh;margin-top: 1vh;margin-bottom:2vh;'/></div>"
									+ "<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right'>"
									+ "<div onclick='replyForChat(this)' class='chat-outgoing-message singlebubble2 row' >";

								if (msgReplyId != 0 && msgAction != "forward") {
									var msgthread = jsonData[i].replyThread;
									var mainChatThread;
									var replyName;

									if (msgthread[0].replyfrom == userId) {
										replyName = "You";
									} else {
										replyName = msgthread[0].userName;
									}

									mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
									mainChatThread = textTolink(mainChatThread, 'reply');

									UI += "<div class='replyNameTop'>" + replyName + "</div>" +
										"<div class='replyMsgBottom'>" + mainChatThread + "</div>" +
										"<div messageType='" + messageType + "' class='replyToReply ChatText'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else if (messageType == "transcript") {
										//console.log("inside chat type");
										UI += "<a  style='color:#fff;' href='#' onclick='readTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ");'>" + message + "</a>";
										UI += "<textarea id='transcript_" + jsonData[i].messageId + "' style='display:none;'></textarea>";
										UI += "<div class='UpdateCancelBtns' style='display:inline-flex;float:right;'>";
										UI += "<img id='update_" + jsonData[i].messageId + "' title='update' style='display:none;' src='" + path + "/images/video/hupdate.png'  onclick='updateTheTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ",&quot;" + message + "&quot;);'/>";
										UI += "<img id='cancel_" + jsonData[i].messageId + "' title='Cancel' style='display:none;padding-left: 5px;' src='" + path + "/images/video/hcancel.png'  onclick='cancelTheEditFile(" + jsonData[i].messageId + ");'/>";
										UI += "</div>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";

								} else if (msgAction == "forward") {
									UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
										"<img title='Forwarded' class='forwardIconCls' src='" + path + "/images/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>" +
										"<div class='ChatText' style='margin-left: -8px;margin-top: 4px;'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else if (messageType == "transcript") {
										//console.log("inside chat type");
										UI += "<a  style='color:#fff;' href='#' onclick='readTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ");'>" + message + "</a>";
										UI += "<textarea id='transcript_" + jsonData[i].messageId + "' style='display:none;'></textarea>";
										UI += "<div class='UpdateCancelBtns' style='display:inline-flex;float:right;'>";
										UI += "<img id='update_" + jsonData[i].messageId + "' title='update' style='display:none;' src='" + path + "/images/video/hupdate.png'  onclick='updateTheTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ",&quot;" + message + "&quot;);'/>";
										UI += "<img id='cancel_" + jsonData[i].messageId + "' title='Cancel' style='display:none;padding-left: 5px;' src='" + path + "/images/video/hcancel.png'  onclick='cancelTheEditFile(" + jsonData[i].messageId + ");'/>";
										UI += "</div>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";

								} else {
									UI += "<div messageType='" + messageType + "' class='ChatText'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else if (messageType == "transcript") {
										//console.log("inside chat type");
										UI += "<a  style='color:#fff;' href='#' onclick='readTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ");'>" + message + "</a>";
										UI += "<textarea id='transcript_" + jsonData[i].messageId + "' style='display:none;'></textarea>";
										UI += "<div class='UpdateCancelBtns' style='display:inline-flex;float:right;'>";
										UI += "<img id='update_" + jsonData[i].messageId + "' title='update' style='display:none;' src='" + path + "/images/video/hupdate.png'  onclick='updateTheTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ",&quot;" + message + "&quot;);'/>";
										UI += "<img id='cancel_" + jsonData[i].messageId + "' title='Cancel' style='display:none;padding-left: 5px;' src='" + path + "/images/video/hcancel.png'  onclick='cancelTheEditFile(" + jsonData[i].messageId + ");'/>";
										UI += "</div>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";
								}
								UI += "</div>"
									+ "<div class='chat-outgoing-time row'><span>" + t + "</span>";
								if (message_status == "sent") {
									UI += "<span class='chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div></div>";
								} else if (message_status == "seen") {
									UI += "<span class='chat-outgoing-status chat-outgoing-status-seen'><i> Read</i></span></div></div></div>";
								} else if (message_status == "delivered") {
									UI += "<span class='chat-outgoing-status chat-outgoing-status-deliver'><i> Delivered</i></span></div></div></div>";
								}

							}
							else {

								UI = "<div id=" + messageId + " class='single-chat-income-overall row chatId' style='outline:none' tabindex='0'>"

									+ "<div class='chat-income-imageframe col-xs-2' style='display:none' >"
									+ "<img  userId ='" + jsonData[i].sender + "' src='" + sourceImg + "'  class='incomeframeImage' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>"
									+ "<div class='col-xs-10' style = 'margin-left: -2.2em;' >"
									+ "<div onclick='replyForChat(this)' class='chat-income-message singlebubble row'>";

								if (msgReplyId != 0 && msgAction != "forward") {
									var msgthread = jsonData[i].replyThread;

									var mainChatThread;
									var replyName;


									if (msgthread[0].replyfrom == userId) {
										replyName = "You";
									} else {
										replyName = msgthread[0].userName;
									}

									mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
									mainChatThread = textTolink(mainChatThread, 'reply');

									UI += "<div class='replyNameTop'>" + replyName + "</div>" +
										"<div class='replyMsgBottom'>" + mainChatThread + "</div>" +
										"<div messageType='" + messageType + "' class='replyToReply ChatText'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else if (messageType == "transcript") {
										//UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"</a>" ;
										UI += "<a  style='color:#fff;' href='#' onclick='readTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ");'>" + message + "</a>";
										UI += "<textarea id='transcript_" + jsonData[i].messageId + "' style='display:none;'></textarea>";
										UI += "<div class='UpdateCancelBtns' style='display:inline-flex;float:right;'>";
										UI += "<img id='update_" + jsonData[i].messageId + "' title='update' style='display:none;' src='" + path + "/images/video/hupdate.png'  onclick='updateTheTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ",&quot;" + message + "&quot;);'/>";
										UI += "<img id='cancel_" + jsonData[i].messageId + "' title='Cancel' style='display:none;padding-left: 5px;' src='" + path + "/images/video/hcancel.png'  onclick='cancelTheEditFile(" + jsonData[i].messageId + ");'/>";
										UI += "</div>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";

								} else if (msgAction == "forward") {
									UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
										"<img title='Forwarded' class='forwardIconCls' src='" + path + "/images/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>" +
										"<div messageType='" + messageType + "'  class='ChatText' style='margin-left: -8px;margin-top: 4px;'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else if (messageType == "transcript") {
										//UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"</a>" ;
										UI += "<a  style='color:#fff;' href='#' onclick='readTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ");'>" + message + "</a>";
										UI += "<textarea id='transcript_" + jsonData[i].messageId + "' style='display:none;'></textarea>";
										UI += "<div class='UpdateCancelBtns' style='display:inline-flex;float:right;'>";
										UI += "<img id='update_" + jsonData[i].messageId + "' title='update' style='display:none;' src='" + path + "/images/video/hupdate.png'  onclick='updateTheTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ",&quot;" + message + "&quot;);'/>";
										UI += "<img id='cancel_" + jsonData[i].messageId + "' title='Cancel' style='display:none;padding-left: 5px;' src='" + path + "/images/video/hcancel.png'  onclick='cancelTheEditFile(" + jsonData[i].messageId + ");'/>";
										UI += "</div>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";

								} else {
									UI += "<div messageType='" + messageType + "'  class='ChatText'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else if (messageType == "transcript") {
										//UI+= "<a target='_blank' style='color:#fff;' href='"+jsonData[i].filePath+"' >"+message+"</a>" ;
										UI += "<a  style='color:#fff;' href='#' onclick='readTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ");'>" + message + "</a>";
										UI += "<textarea id='transcript_" + jsonData[i].messageId + "' style='display:none;'></textarea>";
										UI += "<div class='UpdateCancelBtns' style='display:inline-flex;float:right;'>";
										UI += "<img id='update_" + jsonData[i].messageId + "' title='update' style='display:none;' src='" + path + "/images/video/hupdate.png'  onclick='updateTheTextFile(&quot;" + jsonData[i].filePath + "&quot;," + jsonData[i].messageId + ",&quot;" + message + "&quot;);'/>";
										UI += "<img id='cancel_" + jsonData[i].messageId + "' title='Cancel' style='display:none;padding-left: 5px;' src='" + path + "/images/video/hcancel.png'  onclick='cancelTheEditFile(" + jsonData[i].messageId + ");'/>";
										UI += "</div>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";
								}
								UI += "</div>"
									+ "<div class='chat-income-time row'>" + t + "</div></div></div>";
							}
						} else if (type == "call") {

							UI = "<div align='center' class='row' style='float:left;position:relative;width:100%;margin:0px'>"
								+ "<div class='callmsg' style='font-size:12px; margin-top: 2vh;width: 50%; color:  #323232; padding: 9px; padding-left: 4px; background-color:#D8DDE1;border-radius: 9px;position:relative;'>";
							var src = "";
							var msg = "";
							if (ioType == "Incoming") {

								if (messageType == "calldisconnect") {
									src = callType == "AV" ? path + '/images/video/incomingVC.png' : path + '/images/video/incomingAC.png';
									msg = "Connected";
								} else if (messageType == "callnotresponded") {
									src = callType == "AV" ? path + '/images/video/incomingMVC.png' : path + '/images/video/incomingMAC.png';
									msg = "Missed Call";
								} else if (messageType == "callrequestcancelled") {
									src = callType == "AV" ? path + '/images/video/incomingMVC.png' : path + '/images/video/incomingMAC.png';
									msg = "Missed Call";
								} else if (messageType == "calldeclined") {
									src = callType == "AV" ? path + '/images/video/incomingMVC.png' : path + '/images/video/incomingMAC.png';
									msg = "Rejected";
								} else {
									src = callType == "AV" ? path + '/images/video/incomingVC.png' : path + '/images/video/incomingAC.png';
									msg = "Connected";
								}

							} else {

								if (messageType == "calldisconnect") {
									src = callType == "AV" ? path + '/images/video/outgoingVC.png' : path + '/images/video/outgoingAC.png';
									msg = "Connected";
								} else if (messageType == "callnotresponded") {
									src = callType == "AV" ? path + '/images/video/outgoingMVC.png' : path + '/images/video/outgoingMAC.png';
									msg = "Didn't Connect";
								} else if (messageType == "callrequestcancelled") {
									src = callType == "AV" ? path + '/images/video/outgoingMVC.png' : path + '/images/video/outgoingMAC.png';
									msg = "Cancelled";
								} else if (messageType == "calldeclined") {
									src = callType == "AV" ? path + '/images/video/outgoingMVC.png' : path + '/images/video/outgoingMAC.png';
									msg = "Request Rejected";
								} else {
									src = callType == "AV" ? path + '/images/video/outgoingVC.png' : path + '/images/video/outgoingAC.png';
									msg = "Connected";
								}
							}
							UI += "<img style='height:22px;width:22px;margin-top: -10px;' src='" + src + "'>";
							UI += "<span>&nbsp;&nbsp;" + msg + "<br></span>";
							UI += "<span style='font-size:11px'> &nbsp;" + T + ",&nbsp;" + date + "</span><br>";
							if (duration != "") {
								if (duration.indexOf("-") != -1) {
									duration = duration.substring(1, duration.length);
								}
								UI += "<span>&nbsp;&nbsp;Duration: " + duration + "</span>";
								UI += "<div style='position: absolute;right: 0;top: 5px;'>"
								if (jsonData[i].hlightExist == 'Y') {
									UI += '<img title="Highlights" class="hrExist_' + jsonData[i].messageId + '" style="cursor:pointer;margin-right:10px;" action="expand" onclick=fetchCallRelatedData(this,' + jsonData[i].messageId + ',"highlight") src="' + path + '/images/video/hlightExist.png">';
								}
								if (jsonData[i].recordExist == 'Y') {
									UI += '<img title="Records" class="hrExist_' + jsonData[i].messageId + '" style="cursor:pointer;margin-right:10px;" action="expand" onclick=fetchCallRelatedData(this,' + jsonData[i].messageId + ',"media") src="' + path + '/images/video/recordExist.png">';
								}
								UI += "</div>"
							}
							UI += "</div></div></div>";
							UI += "<div align='center' id='callData_" + jsonData[i].messageId + "' class='row' style='float:left;position:relative;width:100%;margin:0px'></div>";

						}
						$Id.append(UI);

					}

					// appending template for load previous messages to load next 20 messages, check UI
					if (jsonData.length >= 20) {
						$Id.prepend("<div id='lem' style='background-color: #d9d4ce; text-align:center; height: 8%; padding-top: 1%;margin-bottom:2%;' onclick='loadEarlierMessages(this)'><a href='#' style='color: blue;font-family: helvetica; '>Load earlier messages</a></div>");
					}

					index = parseInt(index) + 20;
					$("#" + id + "m").attr('index', index);

					var windowWidth1 = $(window).width();

					if (windowWidth1 < 750) { // to resize the chat conversation UI for smaller size
						// $(".chatOutbubble").css({'padding-right':'2.3em'});
						$(".callmsg").css({ 'width': '94%' });
					} else {
						// $(".chatOutbubble").css({'padding-right':'0.3em'});
						$(".callmsg").css({ 'width': '50%' });
					}

				}


				//gerneralScrollBarFunction("chat-dialog");

				var height = $Id[0].scrollHeight;
				$Id.scrollTop(height);

				$('#' + id + "m").find('textarea').removeAttr('disabled').text("").focus();
				$('#loader_' + id + 'm').hide();
				$("#" + id + "m").attr('cnt', '2');
			}
		}
	});

	$("#loadingBar").hide();
	timerControl("");
}

/*
 * This method Is to load the previous messages after loading of 20 messages beacuse
 * its difficult to append messages So I am using one more function.
 */
function getNextPreviousMessages(conId, id) {
	var jsonData;
	var index = $("#" + id + "m").attr('index');
	var imgType = $("#" + id).attr('imgType');

	var count = $("#" + id + "m").attr('cnt');
	count = parseInt(count);
	var $Id = $("#" + id + "m").find('#chat-dialog');
	$Id.children('#lem').remove();
	var timeZone = getTimeOffset(new Date());
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();

	$.ajax({
		url: path + "/ChatAuth",
		type: "post",
		data: { act: "getPastMsg", userId: userId, convId: conId, indexRg: index, noOfMsgReq: 20, localTZone: timeZone, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			// checkSessionTimeOut(result);
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				if (result == 'No latest msg') {

				} else {
					jsonData = jQuery.parseJSON(result);
					for (i = jsonData.length - 1; i >= 0; i--) {
						var UI = "";
						var t = "";
						var d = new Date();
						var time = jsonData[i].time;
						time = time.replace("CHR(26)", ":");

						var T = time.split("#@#")[1];
						var date = time.split("#@#")[0];

						var m = d.getMonth();
						m = months[m];
						var date1 = d.getDate();
						date1 = (date1 > 10) ? (date1) : ('0' + date1);
						var D = date1 + "-" + m + "-" + d.getFullYear();

						if (date == D) {
							t = "Today  " + T;
						} else {
							t = T + " , " + date;
						}

						var ioType = jsonData[i].ioType;
						var duration = jsonData[i].duration;
						var type = jsonData[i].type;
						var callType = jsonData[i].callType;
						var messageType = jsonData[i].messageType;
						//var message=jsonData[i].message.replace("CHR(26)",":");
						//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
						var message = jsonData[i].message;
						var message = replaceSpecialCharacter(message);
						var body = textTolink(message);
						var fileExt = jsonData[i].fileExt;
						var messageId = jsonData[i].messageId;
						var msgReplyId = jsonData[i].msg_reply_id;
						var msgAction = jsonData[i].msg_action;
						if (type == "chat") {
							if (jsonData[i].sender == userId) {

								UI = "<div id=" + messageId + " class='chat-outgoing-overall row chatId'>"
									+ "<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>"
									+ "<img userId ='" + jsonData[i].sender + "' src='" + lighttpdPath + "/userimages/" + jsonData[i].sender + "." + userImgType + "?" + imgTime + "' onerror='userImageOnErrorReplace(this);' style='float:right;width: 40px; height: 38px; border-radius: 50%; margin-left:-2.3vh;margin-top: 1vh;margin-bottom:2vh;'/></div>"
									+ "<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right'>"
									+ "<div onclick='replyForChat(this)' class='chat-outgoing-message singlebubble2 row'>";
								if (msgReplyId != "0" && msgAction != "forward") {
									var msgthread = jsonData[i].replyThread;
									var mainChatThread;
									var replyName;

									if (msgthread[0].replyfrom == userId) {
										replyName = "You";
									} else {
										replyName = msgthread[0].userName;
									}

									mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
									mainChatThread = textTolink(mainChatThread, 'reply');

									UI += "<div class='replyNameTop'>" + replyName + "</div>" +
										"<div class='replyMsgBottom'>" + mainChatThread + "</div>" +
										"<div messageType='" + messageType + "'  class='replyToReply ChatText'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";

								} else if (msgAction == "forward") {
									UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
										"<img title='Forwarded' class='forwardIconCls' src='" + path + "/images/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>" +
										"<div class='ChatText' style='margin-left: -8px;margin-top: 4px;'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";

								} else {
									UI += "<div messageType='" + messageType + "' class='ChatText'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";
								}

								UI += "</div>"
									+ "<div class='chat-outgoing-time row'><span>" + t + "</span></div></div></div>";

							}
							else {

								UI = "<div id=" + messageId + " class='single-chat-income-overall row chatId' style='outline:none' tabindex='0'>"

									+ "<div class='chat-income-imageframe col-xs-2' style='display:none' >"
									+ "<img  userId ='" + jsonData[i].sender + "' src='' class='incomeframeImage' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%;float: right;margin-top:1vh'/></div>"
									+ "<div class='col-xs-10' style = 'margin-left: -2.2em;' >"
									+ "<div onclick='replyForChat(this)' class='chat-income-message singlebubble row'>";
								if (msgReplyId != 0 && msgAction != "forward") {
									var msgthread = jsonData[i].replyThread;

									var mainChatThread;
									var replyName;


									if (msgthread[0].replyfrom == userId) {
										replyName = "You";
									} else {
										replyName = msgthread[0].userName;
									}

									mainChatThread = replaceSpecialCharacter(msgthread[0].replymsg);
									mainChatThread = textTolink(mainChatThread, 'reply');

									UI += "<div class='replyNameTop'>" + replyName + "</div>" +
										"<div class='replyMsgBottom'>" + mainChatThread + "</div>" +
										"<div messageType='" + messageType + "' class='replyToReply ChatText'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";

								} else if (msgAction == "forward") {
									UI += "<div style='margin-left: -8px;margin-top: -5px;'>" +
										"<img title='Forwarded' class='forwardIconCls' src='" + path + "/images/forwardIcon.png'><span style='font-size: 12px;font-style: italic;color:#aaa;'>Forwarded</span></div>" +
										"<div messageType='" + messageType + "'  class='ChatText' style='margin-left: -8px;margin-top: 4px;'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";

								} else {
									UI += "<div messageType='" + messageType + "'  class='ChatText'>";
									if (messageType == "text") {
										UI += body;
									} else if (messageType == "media") {
										UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
									} else if (messageType == "highlight") {
										UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
									} else {
										UI += "<a target='_blank' style='color:#fff;' href='" + jsonData[i].filePath + "' >" + message + "." + fileExt + "</a>";
									}
									UI += "</div>";
								}

								UI += "</div>"
									+ "<div class='chat-income-time row'>" + t + "</div></div></div>";
							}
						} else if (type == "call") {

							UI = "<div align='center' class='row' style='float:left;position:relative;width:100%;margin:0px'>"
								+ "<div class='callmsg' style='font-size:12px; margin-top: 2vh;width: 50%; color:  #323232; padding: 9px; padding-left: 4px; background-color:#D8DDE1;border-radius: 9px;position:relative;'>";
							var src = "";
							var msg = "";
							if (ioType == "Incoming") {

								if (messageType == "calldisconnect") {
									src = callType == "AV" ? path + '/images/video/incomingVC.png' : path + '/images/video/incomingAC.png';
									msg = "Connected";
								} else if (messageType == "callnotresponded") {
									src = callType == "AV" ? path + '/images/video/incomingMVC.png' : path + '/images/video/incomingMAC.png';
									msg = "Missed Call";
								} else if (messageType == "callrequestcancelled") {
									src = callType == "AV" ? path + '/images/video/incomingMVC.png' : path + '/images/video/incomingMAC.png';
									msg = "Missed Call";
								} else if (messageType == "calldeclined") {
									src = callType == "AV" ? path + '/images/video/incomingMVC.png' : path + '/images/video/incomingMAC.png';
									msg = "Rejected";
								} else {
									src = callType == "AV" ? path + '/images/video/incomingVC.png' : path + '/images/video/incomingAC.png';
									msg = "Connected";
								}

							} else {

								if (messageType == "calldisconnect") {
									src = callType == "AV" ? path + '/images/video/outgoingVC.png' : path + '/images/video/outgoingAC.png';
									msg = "Connected";
								} else if (messageType == "callnotresponded") {
									src = callType == "AV" ? path + '/images/video/outgoingMVC.png' : path + '/images/video/outgoingMAC.png';
									msg = "Didn't Connect";
								} else if (messageType == "callrequestcancelled") {
									src = callType == "AV" ? path + '/images/video/outgoingMVC.png' : path + '/images/video/outgoingMAC.png';
									msg = "Cancelled";
								} else if (messageType == "calldeclined") {
									src = callType == "AV" ? path + '/images/video/outgoingMVC.png' : path + '/images/video/outgoingMAC.png';
									msg = "Request Rejected";
								} else {
									src = callType == "AV" ? path + '/images/video/outgoingVC.png' : path + '/images/video/outgoingAC.png';
									msg = "Connected";
								}
							}
							UI += "<img style='height:22px;width:22px;margin-top: -10px;' src='" + src + "'>";
							UI += "<span>&nbsp;&nbsp;" + msg + "<br></span>";
							UI += "<span style='font-size:11px'> &nbsp;" + T + ",&nbsp;" + date + "</span><br>";
							if (duration != "") {
								if (duration.indexOf("-") != -1) {
									duration = duration.substring(1, duration.length);
								}
								UI += "<span>&nbsp;&nbsp;Duration: " + duration + "</span>";
								UI += "<div style='position: absolute;right: 0;top: 5px;'>"
								if (jsonData[i].hlightExist == 'Y') {
									UI += '<img title="Highlights" class="hrExist_' + jsonData[i].messageId + '" style="cursor:pointer;margin-right:10px;" action="expand" onclick=fetchCallRelatedData(this,' + jsonData[i].messageId + ',"highlight") src="' + path + '/images/video/hlightExist.png">';
								}
								if (jsonData[i].recordExist == 'Y') {
									UI += '<img title="Records" class="hrExist_' + jsonData[i].messageId + '" style="cursor:pointer;margin-right:10px;" action="expand" onclick=fetchCallRelatedData(this,' + jsonData[i].messageId + ',"media") src="' + path + '/images/video/recordExist.png">';
								}
								UI += "</div>"
							}
							UI += "</div></div></div>";
							UI += "<div align='center' id='callData_" + jsonData[i].messageId + "' class='row' style='float:left;position:relative;width:100%;margin:0px'></div>";

						}
						$Id.prepend(UI);

					}
					if (windowWidth1 < 750) { // to resize the chat conversation UI for smaller size
						$(".callmsg").css({ 'width': '94%' });
					} else {
						$(".callmsg").css({ 'width': '50%' });
					}
					if (jsonData.length >= 20) {
						$Id.prepend("<div id='lem' style='background-color: #d9d4ce; text-align:center; height: 8%; margin-bottom:2%;padding-top: 1%;' onclick='loadEarlierMessages(this)'><a href='#' style='color: blue; font-family: helvetica;'>Load earlier messages</a></div>");
					}

					//gerneralScrollBarFunction("chat-dialog"); 

					index = parseInt(index) + 20;
					$("#" + id + "m").attr('index', index);

					var height = $Id[0].scrollHeight;
					height = height / count++;
					$Id.scrollTop(height);
					$("#" + id + "m").attr('cnt', count);

					var windowWidth1 = $(window).width();

					if (windowWidth1 < 750) { // to resize the chat conversation UI for smaller size
						// $(".chatOutbubble").css({'padding-right':'2.3em'});
						$(".callmsg").css({ 'width': '94%' });
					} else {
						// $(".chatOutbubble").css({'padding-right':'0.3em'});
						$(".callmsg").css({ 'width': '50%' });
					}

				}
			}

		}
	});
	$("#loadingBar").hide();
	timerControl("");
}


function fetchCallRelatedData(obj, call_id, type) {
	var action = $(obj).attr('action');
	if (action == 'expand') {
		$('img.hrExist_' + call_id).attr('action', 'expand');
		$(obj).attr('action', 'collapse');

		var jsonData;
		var timeZone = getTimeOffset(new Date());
		var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

		$("#loadingBar").show();
		timerControl("start");
		$("#loadMsg").hide();
		$("#callData_" + call_id).html("");
		$.ajax({
			url: path + "/ChatAuth",
			type: "post",
			data: { act: "fetchCallRelatedData", userId: userId, call_id: call_id, type: type, localTZone: timeZone, device: 'web' },
			mimeType: "textPlain",
			success: function (result) {
				// checkSessionTimeOut(result);
				sessionTimeOutMethod(result);
				if (result != "SESSION_TIMEOUT") {
					if (result == 'No latest msg') {

					} else {
						jsonData = jQuery.parseJSON(result);
						for (i = jsonData.length - 1; i >= 0; i--) {
							var UI = "";
							var t = "";
							var d = new Date();
							var time = jsonData[i].time;
							time = time.replace("CHR(26)", ":");

							var T = time.split("#@#")[1];
							var date = time.split("#@#")[0];

							var m = d.getMonth();
							m = months[m];
							var date1 = d.getDate();
							date1 = (date1 > 10) ? (date1) : ('0' + date1);
							var D = date1 + "-" + m + "-" + d.getFullYear();

							if (date == D) {
								t = "Today  " + T;
							} else {
								t = T + " , " + date;
							}

							var ioType = jsonData[i].ioType;
							var duration = jsonData[i].duration;
							var type = jsonData[i].type;
							var callType = jsonData[i].callType;
							var messageType = jsonData[i].messageType;
							//var message=jsonData[i].message.replace("CHR(26)",":");
							//var message=jsonData[i].message.replace('CH(51)','"').replace('CH(51)','"');
							var message = jsonData[i].message;
							var message = replaceSpecialCharacter(message);
							var body = textTolink(message);
							var fileExt = jsonData[i].fileExt;

							UI = "<div align='center' class='row' style='float:left;position:relative;width:100%;margin:0px'>"
								+ "<div class='callmsg' style='font-size: 12px; margin-top: 2vh; width: 50%; color: rgb(50, 50, 50); padding: 0px; border-radius: 9px; position: relative;'>"
								+ "  <div class='col-xs-10' style = 'width: 100%;padding: 0px;' align='center' >"
								+ "    <div class='chat-income-message singlebubble row' style='float: none;margin-left: 0px;background-color:#709DAF;'>";
							if (messageType == "media") {
								UI += "<span src='" + jsonData[i].filePath + "' onclick='openVideo(this)' extenstion = '" + fileExt + "' style='cursor:pointer'>" + message + "." + fileExt + "</span>";
							} else if (messageType == "highlight") {
								UI += "<a style='color:#fff;' href='#' onclick='viewHighlight(" + jsonData[i].messageId + ");'>" + message + "</a>";
							}
							UI += " </div>"
								+ " <div class='chat-income-time row' style='float: none;padding: 0px;'>" + t + "</div>"
							UI += "   </div>"
								+ "</div></div>";

							$("#callData_" + call_id).prepend(UI);

						}
						var windowWidth1 = $(window).width();

						if (windowWidth1 < 750) { // to resize the chat conversation UI for smaller size
							// $(".chatOutbubble").css({'padding-right':'2.3em'});
							$(".callmsg").css({ 'width': '94%' });
						} else {
							// $(".chatOutbubble").css({'padding-right':'0.3em'});
							$(".callmsg").css({ 'width': '50%' });
						}

						document.getElementById("callData_" + call_id).scrollIntoView(true);

					}
					$("#loadingBar").hide();
					timerControl("");
				}
			}
		});

	} else {
		$(obj).attr('action', 'expand');
		$("#callData_" + call_id).html("");
	}
}



/*
 * @Method is Used to load Earlier messages
 */
function loadEarlierMessages(obj) {
	var Id = $(obj).parent().parent().attr('id');
	Id = Id.substring(0, Id.length - 1);
	var conId = $(obj).parent().parent().attr('conId');
	getNextPreviousMessages(conId, Id);
}


function imageOnErrorReplaceChat(obj) {
	$(obj).attr("src", path + "/images/userimg.png");
}

/*
 * This function is to get offset for diff - diff countries.
 */
function getTimeOffset(date) {

	//DST Prototypes for detecting DST
	Date.prototype.stdTimezoneOffset = function () {
		var jan = new Date(this.getFullYear(), 0, 1);
		var jul = new Date(this.getFullYear(), 6, 1);
		return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
	}

	Date.prototype.dst = function () {
		return this.getTimezoneOffset() < this.stdTimezoneOffset();
	}


	// for changing java script UTC to GMT with detecting DST
	var d = date;

	var s = d.toString().split('GMT')[1].split('(')[0];

	var a = s;
	a = s.substr(1, 2);
	var b = s.substr(3, 4);
	if (a > 0) {
		if (d.dst()) {
			a = parseInt(a) + 1;
		}
		a = a * 60 * 60;
	}
	b = b * 60;
	a = a + b;
	if (s < 0) {
		a = '-' + a;
	}


	return a;
}

/*Codes for file sharing Drag & drop */
function fileSharing(id) {

	var obj = $('#' + id);
	//alert(id);
	//alert($(obj).attr('placeholder'));

	obj.on('dragenter', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
	obj.on('dragover', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
	obj.on('drop', function (e) {
		e.preventDefault();
		var files = e.originalEvent.dataTransfer.files;
		handleFileUpload(files, obj, id);
	});
	$(document).on('dragenter', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
	$(document).on('dragover', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
	$(document).on('drop', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});
}


function handleFileUpload(files, obj, id) {

	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();

	for (var i = 0; i < files.length; i++) {

		if (window.File && window.FileReader && window.FileList && window.Blob) {
			sendFileToServer(files[i], obj, id);
		}
		else {
			alertFun('Browser does not support File APIs ', 'warning');
		}

	}
}

function sendFileToServer(obj, Id, id) {


	var name = obj.name;
	var size = obj.size;
	//var extn = name.split(".")[1];
	//var file_name = name.split(".")[0];

	var extn = name.substring(name.lastIndexOf(".") + 1, name.length);
	var file_name = name.substring(0, name.lastIndexOf("."));

	//alert(Name+"---------"+obj.name+"-----"+repoPath);
	if (size > 10000000) {
		parent.alertFun('Upload failed. File size exceeded', 'warning');
		return false;
	} else {
		var fd = new FormData();
		fd.append('file', obj);
		sumbitDragDrop(fd, Id, extn, file_name, id);
	}
}

function getImageType(extn) {

	var img = 'chat_doc.png';
	extn = extn.toLowerCase();

	if (extn.toLowerCase() == 'png' || extn.toLowerCase() == 'jpeg' || extn.toLowerCase() == 'jpg') {
		img = "chat_img.png";
	} else if (extn.toLowerCase() == 'se') {
		img = "chat_mp3.png";
	} else if (extn.toLowerCase() == 'mp4' || extn.toLowerCase() == 'flv' || extn.toLowerCase() == 'avi') {
		img = "chat_vid.png";
	}

	return img;
}

/*
 * This function is to send the file to repository
 */
function sumbitDragDrop(formData, Id, extn, file_name, id) {
	parent.$("#loadingBar").show();
	parent.timerControl("start");

	var toUserId = $(Id).attr('id').split('-')[0];
	var c_jid = toUserId + "@" + chatDomain + ".colabus.com";
	var img = getImageType(extn);
	//alert(userId);
	var conid = $(Id).attr('conid');
	var status = 'N';
	if ($('#' + toUserId + '-' + chatDomain + '-colabus-com').find('div.user_box_status').hasClass('user_box_status_online')) {
		status = 'N';
	}
	var device = 'web';


	$.ajax({
		url: path + '/ChatFileSharing?userId=' + userId + '&conId=' + conid + '&device=' + device + '&status=' + status + '',
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		cache: false,
		mimeType: "textPlain",
		success: function (result) {
			//	checkSessionTimeOut(result);
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				var msgId = result.split("@@")[1];
				result = result.split("@@")[0];
				$(Id).find('#chat-dialog').append(

					"<div id = " + msgId + " class='chat-outgoing-overall row'>" +
					"<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>" +
					"<img src='" + lighttpdPath + "/userimages/" + userId + "." + userImgType + "?" + imgTime + "'  onerror='userImageOnErrorReplace(this);' style='float:right;width:40px; height:38px; border-radius: 50%;margin-left:-2.3vh; margin-top: 1vh;margin-bottom:2vh;'/></div>" +
					"<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right;'>" +
					"<div class='chat-outgoing-message singlebubble2 row'>" +
					"<a target='_blank' href='" + result + "' style='color:#fff;'>" + file_name + '.' + extn + "</a>" +
					"</div>" +
					"<div class='chat-outgoing-time row'><span>" + getTimeIn12Format(new Date()) + "</span><span class='chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div>");
				if (file_name.indexOf('&') > 0) {

					file_name = file_name.replace("&", "CH(38)");
				}
				body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = '" + file_name + '.' + extn + "' value='" + result + "'><filesharing /></href>";
				if ($("#videoContent").is(":hidden")) {
					var message = $msg({ to: c_jid, "type": "chat", "msgid": msgId }).c('body').t(body).up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
					Gab.connection.send(message);
				} else {
					var jsonText = '{"calltype":"' + callType + '","type":"videochat","msg":"' + body + '","callid":"' + callId + '"}';
					var message = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
					serverConnection.send(message);
				}

				var time1 = d.getHours() + ":" + d.getMinutes();
				const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

				if ($('#conversationList').is(':visible')) {
					$("#conversationList ul ").find("#coversationContactTime_" + toUserId).html(msgtime);
					/*if(type == "filesharing"){
						$("#conversationContactMsg_"+Id).html(filename);
					    
					}else{
						$("#conversationContactMsg_"+Id).html(body);
					}*/
					var fname = file_name + '.' + extn;
					$("#conversationContactMsg_" + toUserId).html(fname);
					$("#conversationContactMsg1_" + toUserId).hide();
					$("#conversationContactMsg_" + toUserId).show();

				}
				var newjid = toUserId + '-' + chatDomain + '-colabus-com';
				var newchatUi = $("#conversationList ul").find("#" + newjid).clone();
				$("#conversationList ul").find("#" + newjid).remove();
				$("#conversationList ul").prepend(newchatUi);

				$(Id).find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
			}

		}
	});

	$("#loadingBar").hide();
	timerControl("");
}

function addgroupChatRooms(NameforChatRoom) {
	$.ajax({
		url: path + "/ChatAuth",
		type: "post",
		data: { act: 'insertMUCRoomName', mucName: NameforChatRoom, mucId: InstantGroupId, ownerId: userId, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			//  checkSessionTimeOut(result);
			//  alert(result);
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				//Groupie.connection.send($pres().c('priority').t('-1'));
				Gab.connection.send(
					$pres({
						to: InstantGroupId + '@mucr.' + chatDomain + '.colabus.com' + "/" + "user_" + userId + "_" + userFullName + "_" + userImgType
					}).c('x', { xmlns: Gab.NS_MUC }));

				iq = $iq({
					to: InstantGroupId + '@mucr.' + chatDomain + '.colabus.com',
					type: 'set'
				}).c("query", {
					xmlns: Gab.NS_MUC + "#owner"
				});
				iq.c("x", {
					xmlns: "jabber:x:data",
					type: "submit"
				});
				var ownereId = userId + '@mucr.' + chatDomain + '.colabus.com'
				//send configuration you want
				iq.c('field', { 'var': 'FORM_TYPE' }).c('value').t('http://jabber.org/protocol/muc#roomconfig').up().up();
				iq.c('field', { 'var': 'muc#roomconfig_roomname' }).c('value').t(NameforChatRoom).up().up();
				iq.c('field', { 'var': 'muc#roomconfig_roomdesc' }).c('value').t(NameforChatRoom).up().up();
				iq.c('field', { 'var': 'muc#roomconfig_enablelogging' }).c('value').t('0').up().up();
				iq.c('field', { 'var': 'muc#roomconfig_changesubject' }).c('value').t('0').up().up();
				iq.c('field', { 'var': 'muc#roomconfig_maxusers' }).c('value').t(0).up().up();
				iq.c('field', { 'var': 'muc#roomconfig_presencebroadcast' }).c('value').t('moderator').c('value').t('participant').up().up();
				iq.c('field', { 'var': 'muc#roomconfig_publicroom' }).c('value').t('1').up().up();
				iq.c('field', { 'var': 'muc#roomconfig_persistentroom' }).c('value').t('1').up().up();
				iq.c('field', { 'var': 'muc#roomconfig_whois' }).c('value').t('anyone').up().up();
				iq.c('field', { 'var': 'muc#roomconfig_roomowners' }).c('value').t(ownereId).up().up();

				Gab.connection.sendIQ(iq.tree(), function () { console.log('success'); }, function (err) { console.log('error', err); });

				if (IDforgroup != '') { // if additional Users Got added than add them in the database else Load the group name
					AddUsersToGroups(NameforChatRoom);
				} else {
					listOfConnectedRooms();
					//GetgroupChatRooms();
					$('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent'); // transparent Div
					$("#transparentDiv").hide();
					$('#groupPopUp').html(''); // hiding the Group creation popUp
				}
			}
		}
	});
}

function deleteGroup(mucId) {
	//	var mucId = MUCID;
	var html = '<div id="BoxOverlay" ></div>' +
		'<div id="confirmDiv" class="alertMainCss" style="left:55%;width:300px">' +
		'  <div id="confirmCloseDiv" title="' + getValues(companyLabels, "Close") + '" ></div> ' +
		//'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
		'  <div style="box-shadow: 0px 0px 20px #000000;" >' +
		'   <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">' +
		'    <div id="confirm-BoxContenedor" class="' + name + '">' +
		'     <span id="confirmContent">Do you want to delete?</span>' +
		'     <div id="confirm-Buttons" class="alertBtnCss">';

	html += ' <input id="BoxConfirmBtnOk" type="submit" value="' + getValues(companyLabels, "OK") + '"  >';


	html += '<input id="BoxConfirmBtnCancel" type="submit" value="' + getValues(companyLabels, "Cancel") + '"  >';

	html += '</div>' +
		'    </div>' +
		'  </div>' +
		' </div>' +
		'</div>';

	$('body').find('#BoxOverlay').remove();
	$('body').find('#confirmDiv').remove();
	//setTimeout(function(){  
	$('body').append(html);
	//$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').attr("onclick",retFunc);

	$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').click(function () {
		hideConfirmBox();
		$("#loadingBar").show();
		timerControl("start");
		$("#loadMsg").hide();
		$.ajax({
			url: path + "/ChatAuth",
			type: "POST",
			data: { act: "deleteMUCRoom", mucId: mucId, device: 'web' },
			mimeType: "textPlain",
			success: function (result) {
				//checkSessionTimeOut(result);
				// alert("Room "+result);
				sessionTimeOutMethod(result);
				if (result != "SESSION_TIMEOUT") {
					$('#groupPopUp').html('');

					/* <iq type="set" id="4D0C27DD-D597-4D2B-BAA9-31738E5C55C9" from="497@devchat.colabus.com" to="162@mucr.devchat.colabus.com">
					 <query xmlns="http://jabber.org/protocol/muc#owner"><destroy><reason/></destroy></query>
					 </iq>*/

					var deleteIq = $iq({
						to: mucId + '@mucr.' + chatDomain + '.colabus.com',
						type: 'set'
					}).c("query", {
						xmlns: Gab.NS_MUC + "#owner"
					}).c("destroy").c("reason");

					Gab.connection.sendIQ(deleteIq, function () { console.log('success--deleted'); }, function (err) { console.log('error--deletion', err); });

					$('#ideaTransparent').removeClass('transparentDivChat').addClass('ideaTransparent');
					listOfConnectedRooms();
					$('#chat-content').html('');
					$('#chat-content').hide();
					$('#chat-frame2').show();
				}
			}
		});
		//top.frames["menuFrame"].window[retFunc]();

	});
	$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function () {
		hideConfirmBox();
		//top.frames["menuFrame"].window[execCancel]();
		return;
	});




}

$('.groupChat').click(function () {
	//listOfConnectedRooms();
	parId = "chatgroup";
});

$('.backChatMain').click(function () {
	$('#chat-frame2').hide();
	$('#chat-frame').hide();
	$('#chat-frame4').hide();
	$('#chat-content').hide();
	$('#chat-frame3').show();
	parId = "chatconv";
});



$(".chatSearch").click(function () {
	$('.chatHead').hide().fadeOut();
	$('.searchHeader').show().fadeIn();
	$("input").focus();
})

$(".searchBack").click(function () {
	$("#searchText2,#searchText3,#searchText").val("").trigger("keyup");
	$('.searchHeader').hide().fadeOut();
	$('.chatHead').show().fadeIn();
})


function ChangeImage1(imgUrl, senderMsg) {

	var image = new Image();
	image.src = imgUrl;
	image.onload = function () {
		/*  Below code is for CME alone. */
		//notifyMe(imgUrl, senderMsg); 

		/*  Below code is for Colabus CME . */
		try {
			window.opener.notifyMe(imgUrl, senderMsg);
		} catch (e) {
			console.log(e);
		}

	};
	image.onerror = function () {
		/*  Below code is for CME alone. */
		//notifyMe(lighttpdPath+"/userimages/userImage.png", senderMsg); 

		/*  Below code is for Colabus CME . */
		try {
			window.opener.notifyMe(imgUrl, senderMsg);
		} catch (e) {
			console.log(e);
		}
	};
	return true;
}

function textTolink(body, msgType) {
	if (body.indexOf("http") >= 0 || body.indexOf("https") >= 0 || body.indexOf("www") >= 0) {
		var temp = "";
		body = body.replace("\n", " \n");
		var dataArray = body.split("\n");
		var subdataArray = "";
		var textColor = "color:white;";
		if (typeof msgType != 'undefined' && msgType == "reply") {
			textColor = "color:inherit";
		}
		for (var j = 0; j < dataArray.length; j++) {
			subdataArray = dataArray[j].split(" ");
			for (var i = 0; i < subdataArray.length; i++) {
				if (subdataArray[i].indexOf("http") >= 0)
					temp += " <a style=\"text-decoration:underline; " + textColor + "\" target=\"_blank\" href=\"" + subdataArray[i] + "\">" + subdataArray[i] + "</a> ";
				else if (subdataArray[i].indexOf("www") >= 0)
					temp += " <a style=\"text-decoration:underline; " + textColor + "\" target=\"_blank\" href=\"http://" + subdataArray[i] + "\">" + subdataArray[i] + "</a> ";
				else if (subdataArray[i].indexOf("https") >= 0)
					temp += " <a style=\"text-decoration:underline; " + textColor + "\" target=\"_blank\" href=\"" + subdataArray[i] + "\">" + subdataArray[i] + "</a> ";
				else
					temp += " " + subdataArray[i];

			} temp += "</br>";
		}

	} else {
		temp = body
	}
	return temp;
}

function sortMe(a, b) {
	return a.className < b.className;
}

function chatWindow() {
	var windowWidth1 = $(window).width();
	if (parId == "chatconv") {
		if (windowWidth1 < 750) {
			$("#chat-frame").hide();
			$("#chat-frame2").hide();
			$("#chat-frame3").hide();
			$("#chat-content").show();
			$("#newgroupChatHead").css({ 'padding-top': '1.5vh', 'margin-left': '4vh' });
			$(".chatpostI").css({ 'vertical-align': 'middle', 'margin-top': '15px' })
		} else {
			$("#chat-frame").hide();
			$("#chat-frame2").hide();
			$("#chat-frame3").show();
			$("#chat-content").show();
		}
	} else if (parId == "chatcontact") {
		if (windowWidth1 < 750) {
			$("#chat-frame").hide();
			$("#chat-frame2").hide();
			$("#chat-frame3").hide();
			$("#chat-content").show();
		} else {
			$("#chat-frame2").hide();
			$("#chat-frame3").hide();
			$("#chat-frame").show();
			$("#chat-content").show();
		}
	} else if (parId == "chatgroup") {
		if (windowWidth1 < 750) {
			$("#chat-frame").hide();
			$("#chat-frame2").hide();
			$("#chat-frame3").hide();
			$("#chat-content").show();
			$("#newgroupChatHead").css({ 'padding-top': '1.5vh', 'margin-left': '4vh' });
			$(".chatpostI").css({ 'vertical-align': 'middle', 'margin-top': '15px' })
		} else {
			$("#chat-frame").hide();
			$("#chat-frame3").hide();
			$("#chat-frame2").show();
			$("#chat-content").show();
		}
	} else if (parId == "callChat") {
		if (windowWidth1 < 750) {
			$("#chat-frame").hide();
			$("#chat-frame2").hide();
			$("#chat-frame3").hide();
			$("#chat-frame4").hide();
			$("#chat-content").show();
			$("#newgroupChatHead").css({ 'padding-top': '1.5vh', 'margin-left': '4vh' });
		} else {
			$("#chat-frame").hide();
			$("#chat-frame3").hide();
			$("#chat-frame2").hide();
			$("#chat-frame4").show();
			$("#chat-content").show();
		}
	}
}


function goBack() {
	var windowWidth = $(window).width();
	if (parId == "chatconv") {
		$("#chat-frame").hide();
		$("#chat-frame2").hide();
		$("#chat-content").css('display', 'none');
		$("#chatNameHead").hide();
		$("#chatOptionHead").hide();
		$("#chat-dialog").hide();
		$("#text-box").hide();
		$("#chat-frame3").show();
		showPersonalConnectionMessages();
	} else if (parId == "chatcontact") {
		$("#chat-frame3").hide();
		$("#chat-frame2").hide();
		$("#chat-content").hide();
		$("#chat-frame").show();
		showPersonalConnectionMessages();
	} else if (parId == "chatgroup") {
		$("#chat-frame3").hide();
		$("#chat-frame").hide();
		$("#chat-content").hide();
		$("#chat-frame2").show();
		showPersonalConnectionMessages();
	} else if (parId == "callChat") {
		$("#chat-frame3").hide();
		$("#chat-frame").hide();
		$("#chat-content").hide();
		$("#chat-frame2").hide();
		$("#chat-frame4").show();
	} else {
		$("#chat-frame").hide();
		$("#chat-frame2").hide();
		$("#chat-content").css('display', 'none');
		$("#chatNameHead").hide();
		$("#chatOptionHead").hide();
		$("#chat-dialog").hide();
		$("#text-box").hide();
		$("#chat-frame3").show();
		showPersonalConnectionMessages();
	}
}

function presenseUpdate(jid_id, userPres, li) {
	var convObj = $("#convoListContainer3 #" + jid_id).find(".user_box_status");
	var contactObj = $("#convoListContainer #" + jid_id).find(".user_box_status");
	var chatcontentObj = $("#" + jid_id + "m").find(".user_box_status");
	var id = jid_id.split("-")[0];
	if (userPres == "online" || userPres == "dnd") {
		convObj.removeClass("user_box_status_offline");
		contactObj.removeClass("user_box_status_offline");
		chatcontentObj.removeClass("user_box_status_offline");
		convObj.addClass("user_box_status_online");
		contactObj.addClass("user_box_status_online");
		chatcontentObj.addClass("user_box_status_online");
		$('.userPresStatus_' + id).css({ "color": "#08B627" });
		$('.userPresStatus_' + id).show().text("online");
	} else if (userPres == "offline") {
		var statusOnline = chatUserArray.getUserOnlineStatus(jid_id.split('-')[0]);
		if (statusOnline == true) {
			convObj.removeClass("user_box_status_offline");
			contactObj.removeClass("user_box_status_offline");
			chatcontentObj.removeClass("user_box_status_offline");
			convObj.addClass("user_box_status_online");
			contactObj.addClass("user_box_status_online");
			chatcontentObj.addClass("user_box_status_online");
			$('#chat-box li#' + jid_id + ' .roster-contact').removeClass("offline");
			$('#chat-box li#' + jid_id + ' .roster-contact').addClass("online");
			$('.userPresStatus_' + id).css({ "color": "#08B627" });
			$('.userPresStatus_' + id).show().text("online");
		} else {
			convObj.removeClass("user_box_status_online");
			contactObj.removeClass("user_box_status_online");
			chatcontentObj.removeClass("user_box_status_online");
			convObj.addClass("user_box_status_offline");
			contactObj.addClass("user_box_status_offline");
			chatcontentObj.addClass("user_box_status_offline");
			$('.userPresStatus_' + id).css({ "color": "gray" });
			$('.userPresStatus_' + id).show().text("offline");
			$('#chat-box #' + jid_id).remove();
			var contacts1 = $('#chat-box li div.offline');
			var pname1 = li.find('.user_box_name').attr('value');
			if (pname1 != undefined) {
				contacts1.each(function () {
					var cmp_name1 = $(this).find('.user_box_name').attr('value');
					var n1 = pname1.localeCompare(cmp_name1);
					if (n1 == -1) {
						$(this).parent().before(li);
						return false;
					}
				});
			}
		}

	}

	$("#convoListContainer li").on("mouseover", function () {
		$(this).find('.hover-call-icons').show();
		if ($(this).find("div.roster-contact").hasClass("dnd")) {
			$(this).find('.hover-call-icons').find('.videocall').find('img').css({ "cursor": "not-allowed", "opacity": "0.5" }).attr("onclick", "");
			$(this).find('.hover-call-icons').find('.audiocall').find('img').css({ "cursor": "not-allowed", "opacity": "0.5" }).attr("onclick", "");
		} else {
			var vuserId = $(this).find("div.roster-contact").attr("id");
			$(this).find('.hover-call-icons').find('.videocall').find('img').css({ "cursor": "", "opacity": "" }).attr("onclick", "startCall(" + vuserId + ",\"true\",\"video\")");
			$(this).find('.hover-call-icons').find('.audiocall').find('img').css({ "cursor": "", "opacity": "" }).attr("onclick", "startCall(" + vuserId + ",\"true\",\"audio\")");
		}
	}).on("mouseout", function () {
		$(this).find('.hover-call-icons').hide();
	});

}

function checValidUser() {
	var firstName;
	var domain = getDomain();
	var guestUserId = $("#guestUserId").val().trim();
	if (guestUserId == "") {
		alertFun('User id cannot be empty', 'warning');
		return false;
	}

	if (checkSplCharforUserId(guestUserId) == true) {
		alertFun('User id should not contain special characters', 'warning');
		return false;
	}


	if (domain.indexOf("testcme.colabus.com") == -1 && domain.indexOf("cme.colabus.com") == -1) {
		if (domain.indexOf("localhost:8080") != -1) {
			domain = "http://localhost:8080/ColabusCme";
		} else if (domain.indexOf("newtest.colabus.com") != -1 || domain.indexOf("beta.colabus.com") != -1 || domain.indexOf("beta1.colabus.com") != -1) {
			domain = "https://testcme.colabus.com";
		} else if (domain.indexOf("www.colabus.com") != -1 || domain.indexOf("colabus.com") != -1) {
			domain = "https://cme.colabus.com";
		}
	}

	$.ajax({
		url: path + "/ChatAuth",
		type: "post",
		data: { act: 'checkGuestUser', guestUserId: guestUserId, companyId: companyId, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				if (result == "failure") {
					$("#msg").html("User already exist");
				} else {
					$("#msg").html("");
					$("#guestUrl").html("" + domain + "/colAuth?pAct=guestRegister&guestUser=" + encodeURIComponent(guestUserId) + "&user=" + userId + "");
					$("#guestUrl").show();
					$('#gLinkButton').hide();
					$('#cLinkButton').show();

				}
			}
		}
	});
}

function clearModal() {
	$("#guestUserId").val("");
	$("#guestUrl").hide();
	$("#guestUrl").html("");
	$("#msg").html("");
	$('#cLinkButton').hide();
	$('#gLinkButton').show();

}


function copyLink() {
	$("#guestUrl").select();
	document.execCommand("copy");
	$("#globalNotification").show().html("Copied!").delay(1000).hide(100);
}

function GenerateCopyLinkForGuestUser(userid, userCreatedById, useLoginId) {
	var domain = getDomain();

	if (domain.indexOf("testcme.colabus.com") == -1 && domain.indexOf("cme.colabus.com") == -1) {
		if (domain.indexOf("localhost:8080") != -1) {
			domain = "http://localhost:8080/ColabusCme";
		} else if (domain.indexOf("newtest.colabus.com") != -1 || domain.indexOf("beta.colabus.com") != -1 || domain.indexOf("beta1.colabus.com") != -1) {
			domain = "https://testcme.colabus.com";
		} else if (domain.indexOf("www.colabus.com") != -1 || domain.indexOf("colabus.com") != -1) {
			domain = "https://cme.colabus.com";
		}
	}

	var guestUrl = domain + "/colAuth?pAct=guestRegister&guestUser=" + encodeURIComponent(useLoginId) + "&user=" + userCreatedById;

	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val(guestUrl).select();
	document.execCommand("copy");
	$temp.remove();
	$("#globalNotification").show().html("Copied!").delay(1000).hide(100);
}

function checkSplCharforUserId(name) {
	var splChar = /[!,`~#$%\\\^&*(){}[\]<>?/|\"'+=:;]/;
	if (!splChar.test(name)) {
		return false;
	} else {
		return true;
	}
}

function seenMsg(id, type, msgid) {

	try {
		if (type == "alldelivered") {
			if (msgid.indexOf(',') > -1) {
				var msgIdArray = msgid.split(",");
				for (var i = 0; i < msgIdArray.length; i++) {

					var jid = id[i].split('-')[0] + "@" + chatDomain + ".colabus.com";
					var msgId = msgIdArray[i];
					var jsonText = '{"status":"' + type + '"}';
					var message = $msg({ to: jid, "type": "chat", "msgid": msgId }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
					Gab.connection.send(message);
					//console.log("jid test----"+jid);
				}
			}
		} else {
			var jid = id.split('-')[0] + "@" + chatDomain + ".colabus.com";
			var jsonText = '{"status":"' + type + '"}';
			var message = $msg({ to: jid, "type": "chat", "msgid": msgid }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
			Gab.connection.send(message);
		}

	} catch (emsg) {
		console.log(emsg);
	}
	/*if(type=="allseen"){
  type="seen";
}*/
	$.ajax({
		url: path + "/ChatAuth",
		type: "post",
		data: { act: "updateMsgStatus", msgid: msgid, type: type, userId: userId, device: 'web' },
		mimeType: "textPlain",
		success: function (result) {
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
			}
		}
	});

}

function sendDeliverStanza() {
	var sender = '';
	var groupMessCount = '';
	var cid;
	var cidArray = "";
	var jid_id;
	var jidArray = [];
	for (i = 0; i < jsonDataConversation.length; i++) {
		sender = jsonDataConversation[i].sender;
		groupMessCount = jsonDataConversation[i].unreadMsgCount;
		if (groupMessCount != 0) {
			cid = jsonDataConversation[i].cid;
			cidArray = cidArray + cid + ",";
			jid_id = sender + "-" + chatDomain + "-colabus-com";
			jidArray.push(jid_id);
		}

	}
	cidArray = cidArray.substring(0, cidArray.length - 1);
	seenMsg(jidArray, "alldelivered", cidArray);
}


function readTextFile(file, transId, messageTitle) {
	//	 console.log("FILE===>"+file+"transId===>"+transId+"messageTitle---"+messageTitle);
	var fileDate = new Date();
	var fileTime = fileDate.getTime();
	var rawFile = new XMLHttpRequest();
	file = file + "?" + fileTime;

	rawFile.open("GET", file, true);
	rawFile.onreadystatechange = function () {
		if (rawFile.readyState === 4) {
			if (rawFile.status === 200 || rawFile.status == 0) {
				var allText = rawFile.responseText;
				//console.log(allText);
				viewTranscript(allText, transId, file, messageTitle);
			}
		}
	}
	rawFile.send();
}


function updateTheTextFile(filepth, msgId, messageTitle) {

	/* console.log("TransBtnId===>"+msgId+"filePath===>"+filepth+" messageTitle===>"+messageTitle);
	 console.log("editTranscript====>"+editTranscript);
	 console.log("editTranscript===="+editTranscript.indexOf("\n"));*/

	var editTranscript = $("#transBody").val();
	$("#loadingBar").show();
	timerControl("start");
	$("#loadMsg").hide();

	$.ajax({
		url: path + "/ChatAuth",
		type: "POST",
		data: { act: "updateExistingFileTranscript", editTranscript: editTranscript, messageTitle: messageTitle, device: 'web' },
		error: function (jqXHR, textStatus, errorThrown) {
			checkError(jqXHR, textStatus, errorThrown);
			timerControl("");
		},
		success: function (result) {
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				$("#loadingBar").hide();
				timerControl("");

				if (result != "success") {
					console.log("Transcript file write error");
				} else {
					console.log("Transcript Successfully");
				}
				chatAlertFun('Transcript Updated Successfully', 'warning');
				$("#transEdit").hide();
				$('#transBody').prop('readonly', true);
			}

		}
	});
}

function cancelTheEditFile(canmsgId) {
	$('.jqte').css({ 'display': 'none' });
	$('#update_' + canmsgId).css({ 'display': 'none' });
	$('#cancel_' + canmsgId).css({ 'display': 'none' });
	$("textarea#transcript_" + canmsgId).css({ 'display': 'none' });
}

function changeDateTime(time) {
	var t = new Date(time);
	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	var hours = t.getHours();
	var minutes = t.getMinutes();
	var Ampm = "AM";

	if (hours >= 12) { // the value can equal or lower then 12 it's am 
		Ampm = "PM";
		hours = hours - 12; //the value can substract by 12 it's pm 
	}

	if (hours < 10) {
		hours = '0' + hours;
	}
	if (minutes < 10) {
		minutes = '0' + minutes;
	}
	return hours + ":" + minutes + " " + Ampm + " " + t.getFullYear() + '-' + monthNames[t.getMonth()] + '-' + ("0" + t.getDate()).slice(-2);
}


function resizeChatBox() {
	var textValue = $('#text-box').children().find("textarea").val();
	if (textValue == "") {
		defaultChatSize();
		return;
	}
	var scroll_height = $("#text").get(0).scrollHeight;
	var divheight1 = $("#text").height();
	var scrollbHeihgt = scroll_height + 19;
	var postMargin = scrollbHeihgt / 3;
	var diffDiv;
	if (divheight1 != divheight) {
		diffDiv = divheight1 - divheight;
	}


	if (scroll_height < 104 && scroll_height > 31) {
		var chatHeight1 = chatHeight - diffDiv;
		$("#text").css('height', scroll_height + 'px');
		$("#textborder").css('height', scrollbHeihgt + 'px');
		$("#chat-dialog").css('height', chatHeight1 + 'px');
		$(".chatpost").css('height', scrollbHeihgt + 'px');
		$(".chatpostI").css('margin-top', postMargin + 'px');
	}

	if (scroll_height >= 104) {

		var divheight1 = $("#text").height();
		if (divheight1 != divheight) {
			diffDiv = divheight1 - divheight;
		} else {
			diffDiv = 38;
		}

		var chatHeight1 = chatHeight - diffDiv;
		postMargin = 84 / 2.4;
		$("#text").css('overflow', 'auto');
		$("#text").css('height', '84px');
		$("#textborder").css('height', scrollbHeihgt + 'px');
		$("#chat-dialog").css('height', chatHeight1 + 'px');
		$(".chatpost").css('height', '93px');
		$(".chatpostI").css('margin-top', postMargin + 'px');
	}
	$("#chatFor").find("#chat-dialog").css({ 'height': '56vh' });
}

function defaultChatSize() {
	$("#chat-dialog").css({ 'height': '80vh' });
	$("#text").css({ 'height': '7vh' });


	$("#text").css('overflow', 'hidden');
	var windowWidth1 = $(window).width();
	if (windowWidth1 < 750) {
		$("#textborder").css({ 'height': '54px' });
		$(".chatpostI").css({ 'margin-top': '15px' });
		$(".chatpost").css({ 'height': '10vh' });
	} else {
		$("#textborder").css({ 'height': '70px' });
		$(".chatpostI").css({ 'margin-top': '23px' });
		$(".chatpost").css({ 'height': '8vh' });
	}
}

var replyForMsgId = 0;
var replyForUserId = "";
var replymsg = "";
var globalObj = "";
function replyForChat(obj) {
	$(".singlebubble2").css('background-color', '#007b97');
	$(".singlebubble").css('background-color', '#0b4860');
	$(".bubble2").css('background-color', '#007b97');
	$(".bubble").css('background-color', '#0b4860');
	globalObj = obj;

	if ($(obj).hasClass("replythread")) {
		$(".singlebubble").removeClass("replythread");
		$(".singlebubble2").removeClass("replythread");
		$(".bubble").removeClass("replythread").removeClass('replyGroupBG');
		$(".bubble2").removeClass("replythread").removeClass('replyGroupBG2');
		$("#chatOptionHead").hide();
		$("#chatNameHead").show();
		$("#text").attr("placeholder", "Type message here...");
		chatFortype = "normal";
		replyForMsgId = 0;
		replyForUserId = "";
		replymsg = "";
	} else {
		$(".singlebubble").removeClass("replythread");
		$(".singlebubble2").removeClass("replythread");
		$(".bubble").removeClass("replythread").removeClass('replyGroupBG');
		$(".bubble2").removeClass("replythread").removeClass('replyGroupBG2');
		$("#chatOptionHead").show();
		$("#chatNameHead").hide();
		if ($(obj).hasClass('bubble2')) {
			$(obj).addClass("replythread").addClass('replyGroupBG2');
		} else {
			$(obj).addClass("replythread").addClass('replyGroupBG');
		}
		$(obj).css('background-color', '#339DEA');
	}
}

function restoreValue() {
	replyForMsgId = 0;
	replyForUserId = "";
	replymsg = "";
	chatFortype = "";

}

function replyMsg() {
	chatFortype = "reply";
	replyForMsgId = $(globalObj).parents(".chatId").attr("id"); //should take the value of msgid of the reply thread
	if ($(".replythread").hasClass("chat-outgoing-message")) {
		//replyForUserId = should take the value of userid of the reply thread
		replyForUserId = userId;
	} else {
		replyForUserId = $(globalObj).parents(".chatId").find("img.incomeframeImage").attr('userId');
	}
	replymsg = $(globalObj).find(".ChatText").text(); //should be the content of reply thread
	$("#chatOptionHead").hide();
	$("#chatNameHead").show();
	$("#text").attr("placeholder", "Reply...").focus();
}

var forwardMsgType = "";
var forwardDocSrc = "";
function forwardMsg() {
	chatFortype = "forward";
	replymsg = $(globalObj).find(".ChatText").text(); //should be the content of reply thread
	replyForMsgId = $(globalObj).parents(".chatId").attr("id"); //should take the value of msgid of the reply thread
	if ($(".replythread").hasClass("chat-outgoing-message")) {
		//replyForUserId = should take the value of userid of the reply thread
		replyForUserId = userId;
	} else {
		replyForUserId = $(globalObj).parents(".chatId").find("img.incomeframeImage").attr('userId');
	}
	var messagetype = $(globalObj).find(".ChatText").attr("messagetype");
	if (messagetype != "null" && messagetype == "file") {
		forwardMsgType = messagetype;
		forwardDocSrc = $(globalObj).find(".ChatText").find('a').attr('href');
	} else {
		forwardMsgType = "";
	}
	$("#chatOptionHead").hide();
	$("#chatNameHead").show();
	$("#contactList").html("");
	var ui = forwardContactUi;
	$("#contactList").html(ui);
	FwdUserSearchIconBack();
}

function forwardContactUi() {
	//console.log("Users:"+userName);
	var uList = userName;
	uList.sort(GetSortedData("name"));
	for (var i = 0; i < uList.length; i++) {
		if (uList[i].userId != userId) {
			name = uList[i].name;
			imgType = uList[i].imgType;
			useRegType = uList[i].useRegType;
			upgradePlan = uList[i].upgradePlan
			if (imgType == "" || imgType == "null" || imgType == null) {
				imgurl = "";
			} else {
				imgurl = lighttpdPath + "/userimages/" + uList[i].userId + "." + imgType + "?" + imgTime;
			}
			var contactUi = "<li  count='0' style='list-style: none;height:60px;float:left;width:99%;cursor:pointer;border-bottom: 1px solid #ced2d5; '   >" +
				"<div onclick='forwardMsgTo(this)' data-dismiss='modal' id='con_" + uList[i].userId + "' class='roster-contact offline row' style='padding-top:5px;width:100%;margin-left: -10px;' >" +
				"<div class='col-xs-3' style='width: 15%; padding-left: 8px;padding-top:0.5%'> <img title='" + name + "'  src='" + imgurl + "' onerror='userImageOnErrorReplace(this);' style='height: 34px; min-height: 45px; width:45px;min-width: 45px;border-radius:50%'/></div>" +
				"<div class='user_box_status' style='margin-left: 46px; margin-top: 5px;position:absolute'></div>" +
				"<div class='user_box_name col-xs-4 defaultWordBreak' style='height:35px;font-family: OpenSansRegular; font-size: 14px; width: 72%; padding-top: 12px;;margin-left:22px;padding-right:0px;' value='" + name + "'>" + name + "</div>" +
				"</div>" +
				"</li>";
			$("#contactList").append(contactUi);
		}
	}
}
function GetSortedData(prop) {
	return function (a, b) {
		if (a[prop] > b[prop]) {
			return 1;
		} else if (a[prop] < b[prop]) {
			return -1;
		}
		return 0;
	}
}
function forwardMsgTo(obj) {
	var id = $(obj).attr('id');
	if (id != "undefined") {
		id = id.split("_")[1];  // getting the id from chat box

		var jid = id + "@" + chatDomain + ".colabus.com";
		var status = 'N';
		var d = new Date();
		var Id = id;
		var t = getTimeIn12Format(d);
		var msg = textTolink(replymsg);

		if (forwardMsgType != "" && forwardMsgType == "file") {
			msg = "<a target='_blank' style='color:#fff;' href='" + forwardDocSrc + "' >" + msg + "</a>";
		}

		$("#" + id).find('#chat-dialog').append(              // preparing the chat message structure to be appended on chatBox
			"<div class='chat-outgoing-overall row chatId'>" +
			"<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>" +
			"<img userId ='" + userId + "' src='" + lighttpdPath + "/userimages/" + userId + "." + userImgType + "?" + imgTime + "' class='incomeframeImage' onerror='userImageOnErrorReplace(this);' style='width:40px; height:38px; border-radius: 50%; float:right;margin-left:-2.3vh;margin-top: 1vh;margin-bottom:2vh;'/></div>" +
			"<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right'>" +
			"<div  onclick='replyForChat(this)' class='chat-outgoing-message singlebubble2 row'>" +
			"<div class='ChatText' >" +
			msg +
			"</div>" +
			"</div>" +
			"<div class='chat-outgoing-time row'><span>" + t + "</span><span class='chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div>");

		var divRef = $("#" + id).find('#chat-dialog  .chat-outgoing-overall:last');
		var m = d.getMonth();
		var time = d.getHours() + ":" + d.getMinutes() + "###" + d.getDate() + "-" + (Number(m) + 1) + "-" + d.getFullYear();
		var timeZone = getTimeOffset(d);
		var convId;

		$.ajax({
			url: path + "/ChatAuth",
			type: "post",
			async: false,
			data: { act: "checkConvId", fromUser: userId, toUser: id, device: 'web' },
			mimeType: "textPlain",
			success: function (result) {
				sessionTimeOutMethod(result);
				if (result != "SESSION_TIMEOUT") {
					convId = result;
				}
			}
		});

		var ID = id;

		if ($("#" + ID).find('div.user_box_status').hasClass('user_box_status_online')) { // status for notification
			status = 'N';
		}

		replymsg = replymsg.replace("'", "CHR(39)");


		var time1 = d.getHours() + ":" + d.getMinutes();
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

		if (forwardMsgType == "file") {

			$.ajax({                        // This ajax call is to insert the one to one chat message into the database
				url: path + "/ChatAuth",
				type: "post",
				data: { act: "insertFwdDocMsg", convId: convId, sender: userId, time: time, timeZone: timeZone, msgStatus: status, replyid: replyForMsgId, chatFortype: chatFortype, device: 'web' },
				mimeType: "textPlain",
				success: function (result) {
					sessionTimeOutMethod(result);
					if (result != "SESSION_TIMEOUT") {
						var msgId = result.toString().split("@@")[1];
						$(divRef).attr("id", msgId);
						replymsg = replymsg.replace("CHR(39)", "'");

						var body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = '" + replymsg + "' value='" + forwardDocSrc + "'><filesharing /></href>";
						var message = $msg({ to: jid, "type": "chat", "msgid": msgId }).c('body').t(body)
							.up().c("replyForMsgId").t(replyForMsgId)//should take the value of msgid of the forward thread
							.up().c("chatFortype").t(chatFortype) //should take the value of chatFortype value for the forward thread
							.up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
						Gab.connection.send(message);
						restoreValue();
						closeContactList();
						$('#' + id + '-devchat-colabus-com').trigger("onclick");
					}
				}
			});

		} else {
			$.ajax({                        // This ajax call is to insert the one to one chat message into the database
				url: path + "/ChatAuth",
				type: "post",
				data: { act: "insertChatMsg", convId: convId, sender: userId, message: replymsg, time: time, timeZone: timeZone, msgStatus: status, msgType: "text", docId: '0', docExt: '', docPath: '', replyid: '0', chatFortype: chatFortype, device: 'web' },
				mimeType: "textPlain",
				success: function (result) {
					sessionTimeOutMethod(result);
					if (result != "SESSION_TIMEOUT") {
						var msgId = result.toString().split("@@")[1];
						$(divRef).attr("id", msgId);
						replymsg = replymsg.replace("CHR(39)", "'");

						var message = $msg({ to: jid, "type": "chat", "msgid": msgId }).c('body').t(replymsg) // preparing the chat stanza to send
							.up().c("replyForMsgId").t(replyForMsgId)//should take the value of msgid of the forward thread
							.up().c("chatFortype").t(chatFortype); //should take the value of chatFortype value for the forward thread
						Gab.connection.send(message);
						restoreValue();
						closeContactList();
						$('#' + id + '-devchat-colabus-com').trigger("onclick");
					}
				}
			});

		}

	}
}

function closeContactList() {
	if ($('.chat-income-message').hasClass("replythread") || $('.chat-outgoing-message').hasClass("replythread")) {
		$(".singlebubble").removeClass("replythread");
		$(".singlebubble2").removeClass("replythread");
		$(".bubble").removeClass("replythread").removeClass('replyGroupBG');
		$(".bubble2").removeClass("replythread").removeClass('replyGroupBG2');
		$(".singlebubble2").css('background-color', '#007b97');
		$(".singlebubble").css('background-color', '#0b4860');
		$(".bubble2").css('background-color', '#007b97');
		$(".bubble").css('background-color', '#0b4860');
		$("#chatOptionHead").hide();
		$("#chatNameHead").show();
		$("#text").attr("placeholder", "Type message here...");
		$("#contactList").html("");
		chatFortype = "normal";
		replyForMsgId = 0;
		replyForUserId = "";
		replymsg = "";
		forwardMsgType = "";
		forwardDocSrc = "";

	}
}

function FwdUserSearchIcon() {
	$('.FwdContactHeader, #fwdUserSearch').hide().fadeOut();
	$('.FwdSearchHeader').show().fadeIn();
	$(".FwdSearchHeader input").focus();
}

function FwdUserSearchIconBack() {
	$("#searchText5").val("").trigger("keyup");
	$('.FwdSearchHeader').hide().fadeOut();
	$('.FwdContactHeader, #fwdUserSearch').show().fadeIn();
}

function FwdUserSearch() {        // for search of users in single chat
	var txt = $('#searchText5').val().toLowerCase();

	$('#contactList li').each(function () {  // we are getting all the users and than showing the users with match of entered chars rest users hiding

		var val = $(this).find('.user_box_name').attr('value').toLowerCase();

		if (val.indexOf(txt) != -1) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}

function copyMsg() {
	chatFortype = "copy";
	replymsg = $(globalObj).find(".ChatText").text();
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val(replymsg).select();
	document.execCommand("copy");
	$temp.remove();
	$("#globalNotification").show().html("Copied!").delay(1000).hide(100);
	restoreValue();
	replyForChat(globalObj);
}

/* ***************	clipboard content into CME IM for one to one Chat ********************* */
(function ($) {
	var defaults;
	$.event.fix = (function (originalFix) {
		return function (event) {
			event = originalFix.apply(this, arguments);
			if (event.type.indexOf('copy') === 0 || event.type.indexOf('paste') === 0) {
				event.clipboardData = event.originalEvent.clipboardData;
			}
			return event;
		};
	})($.event.fix);
	defaults = {
		callback: $.noop,
		matchType: /image.*/
	};

	return $.fn.pasteImageReader = function (options) {
		if (typeof options === "function") {
			options = {
				callback: options
			};
		}
		options = $.extend({}, defaults, options);
		return this.each(function () {
			var $this, element;
			element = this;
			$this = $(this);
			return $this.bind('paste', function (event) {
				var clipboardData, found;
				found = false;
				clipboardData = event.clipboardData;
				return Array.prototype.forEach.call(clipboardData.types, function (type, i) {
					var file, reader;
					if (found) {
						return;
					}
					if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
						file = clipboardData.items[i].getAsFile();
						reader = new FileReader();
						reader.onload = function (evt) {
							return options.callback.call(element, {
								dataURL: evt.target.result,
								event: evt,
								file: file,
								name: file.name
							});
						};
						reader.readAsDataURL(file);
						return found = true;
					}
				});
			});
		});
	};
})(jQuery);

var dataURL, filename, fileShare, imageShre;
$("html").pasteImageReader(function (results) {
	filename = results.filename;
	dataURL = results.dataURL;
	fileShare = dataURItoBlobForChat(dataURL);
	imageShre = fileShare;
	var img = document.createElement('img');
	img.src = dataURL;
	var w = img.width;
	var h = img.height;
	$width.val(w)
	$height.val(h);
	$('#text').attr('placeholder', '');
	return $("#text").css({
		backgroundImage: "url(" + dataURL + ")"
	}).data({ 'width': w, 'height': h });
});

var $width, $height;
$(function () {
	$width = $('#width');
	$height = $('#height');
	$('.target').on('click', function () {
		var $this = $(this);
		var bi = $this.css('background-image');
		$('.active').removeClass('active');
		$this.addClass('active');
	})
})

function postCanvasToURLImageForClipboardInChat() {
	var id = $('.clipboard').attr('id');
	id = id.split("-")[0];
	var obj = $('#' + id);
	var toUserId = $(obj).attr('id').split('-')[0];
	var c_jid = toUserId + "@" + chatDomain + ".colabus.com";
	var conid = $('#chat-content').children().attr('conid');
	var status = 'N';

	if ($('#' + toUserId + '-' + chatDomain + '-colabus-com').find('div.user_box_status').hasClass('user_box_status_online')) {
		status = 'N';
	}

	var file = imageShre;
	var formData = new FormData();
	var today = new Date();
	var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date + '_' + time;
	formData.append('file', file, "image_" + dateTime + ".png");
	var device = 'web';

	$.ajax({
		url: path + '/ChatFileSharing?userId=' + userId + '&conId=' + conid + '&device=' + device + '&status=' + status + '',
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		cache: false,
		mimeType: "textPlain",
		success: function (result) {
			sessionTimeOutMethod(result);
			if (result != "SESSION_TIMEOUT") {
				var msgId = result.split("@@")[1];
				result = result.split("@@")[0];
				$("#text").val('');
				$("textarea.text").val('');
				$('#chat-content').find('#chat-dialog').append(
					"<div id = " + msgId + " class='chat-outgoing-overall row'>" +
					"<div class='chat-outgoing-imageframe col-xs-1' style='display:none'>" +
					"<img src='" + lighttpdPath + "/userimages/" + userId + "." + userImgType + "?" + imgTime + "'  onerror='userImageOnErrorReplace(this);' style='float:right;width:40px; height:38px; border-radius: 50%;margin-left:-2.3vh; margin-top: 1vh;margin-bottom:2vh;'/></div>" +
					"<div class='col-xs-11 chatOutbubble' style='margin-top:-3vh;margin-left:-2vh;float:right;'>" +
					"<div class='chat-outgoing-message singlebubble2 row'>" +
					"<a target='_blank' href='" + result + "' style='color:#fff;'>image_" + dateTime + ".png</a>" +
					"</div>" +
					"<div class='chat-outgoing-time row'><span>" + getTimeIn12Format(new Date()) + "</span><span class='chat-outgoing-status chat-outgoing-status-sent'><i> Sent</i></span></div></div>");

				body = "<?xml version='1.0' encoding='UTF-8'?> <href fileName = 'image_" + dateTime + ".png' value='" + result + "'><filesharing /></href>";
				if ($("#videoContent").is(":hidden")) {
					var message = $msg({ to: c_jid, "type": "chat", "msgid": msgId }).c('body').t(body).up().c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
					Gab.connection.send(message);
				} else {
					var jsonText = '{"calltype":"' + callType + '","type":"videochat","msg":"' + body + '","callid":"' + callId + '"}';
					var message = $msg({ to: jid, "type": "normal" }).c('json', { xmlns: "urn:xmpp:json:0" }).t(jsonText);
					serverConnection.send(message);
				}

				var time1 = d.getHours() + ":" + d.getMinutes();
				const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var msgtime = tConvert(time1) + ", " + d.getFullYear() + "-" + monthNames[d.getMonth()] + "-" + d.getDate();

				if ($('#conversationList').is(':visible')) {
					$("#conversationList ul ").find("#coversationContactTime_" + toUserId).html(msgtime);
					$("#conversationContactMsg_" + toUserId).html("image_" + dateTime + ".png");
					$("#conversationContactMsg1_" + toUserId).hide();
					$("#conversationContactMsg_" + toUserId).show();
				}

				var newjid = toUserId + '-' + chatDomain + '-colabus-com';
				var newchatUi = $("#conversationList ul").find("#" + newjid).clone();
				$("#conversationList ul").find("#" + newjid).remove();
				$("#conversationList ul").prepend(newchatUi);
				$(obj).find('#chat-dialog').animate({ scrollTop: 20000 }, 'normal');
				$('#text').css('background-image', 'none');
				$("#text").val('');
				$('#text').attr('placeholder', 'Type message here...');
				imageShre = "";
				$('.chatpost').children().attr('onclick', "sending(event,this)");
			}
		}
	});
}

function dataURItoBlobForChat(dataURI) {
	var byteString;
	if (dataURI.split(',')[0].indexOf('base64') >= 0)
		byteString = atob(dataURI.split(',')[1]);
	else
		byteString = unescape(dataURI.split(',')[1]);
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
	var ia = new Uint8Array(byteString.length);
	for (var i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i);
	}
	return new Blob([ia], { type: mimeString });
}

/* *************** Code Ends For clipboard content into CME IM ********************* */
