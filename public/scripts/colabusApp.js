    


 
	 var xhrPool = [];
	 var abort = null;
	 
    /*Onclick of menu icons */
    $(function(){
	    $('.menuClick').click(function(){
			var action = $(this).attr("id");
			openMenuMethod(action);
		});
	    

	    abort = function(){ //---------- this is the function to store ajax requests
	    	console.log("Ajax Request Lenght:"+xhrPool.length);
			if(xhrPool.length > 0){
				$.each(xhrPool, function(idx, jqXHR) {
			    	console.log('ajax aborted');
			      	jqXHR.abort();
			    });
			}
		}
	});
    
    function openMenuMethod(mAct){
		loadSubPageContent(mAct);
		// window.location.href = path+"/Redirect.do?pAct="+mAct;
	}
	
	/*Scroll function for popUp */
	function popUpScrollBar(scrollDiv){
	    /*$('div#'+popId).mCustomScrollbar({
			scrollButtons:{
				enable:true
			},
			theme:"minimal-dark",
			callbacks:{
	           onTotalScroll:function(){
	        },
	        onTotalScrollOffset:100
		  }
		});
		if($('div#'+popId+' .mCSB_container').css('display','block')){
		   $('div#'+popId+' .mCSB_container').css('margin-right','15px');
		}
		
		$('div#'+popId+' .mCSB_scrollTools .mCSB_buttonUp').css('display','block');
		$('div#'+popId+' .mCSB_scrollTools .mCSB_buttonDown').css('display','block');
		$('div#'+popId+' .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
		$('div#'+popId+' .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
		$('div#'+popId+' .mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
		$('div#'+popId+' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','#DAD9DE');
	    $('div#'+popId+' .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
        $('div#'+popId+' .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
        $('div#'+popId+' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('width','5px');*/
        
        $("div#"+scrollDiv).mCustomScrollbar("destroy");
	    $("div#"+scrollDiv).mCustomScrollbar({
		  scrollButtons:{
			 enable:true
		  },
		    theme:"minimal-dark",
		    mouseWheel:{ invert: true },
		    mouseWheel:{ preventDefault: true },
			scrollInertia: 15,
			mouseWheelPixels: 15 ,
		    contentTouchScroll: true,
			callbacks:{
	           onTotalScroll:function(){
	        },
	        onTotalScrollOffset:100,
		  advanced:{
	           autoExpandVerticalScroll:true,
	           autoScrollOnFocus:false
	       }
	       }
		}); 
		if($('div#'+scrollDiv+' .mCSB_scrollTools').css('display') == "none"){
		    $('div#'+scrollDiv+' .mCSB_container').css('margin-right','0px');
		 }else{			
		    $('div#'+scrollDiv+' .mCSB_container').css('margin-right','15px');
		 } 
		$('div#'+scrollDiv+' .mCSB_scrollTools .mCSB_buttonUp').css('background-position','-80px 0px');
		$('div#'+scrollDiv+' .mCSB_scrollTools .mCSB_buttonDown').css('background-position','-80px -20px');
		$('div#'+scrollDiv+' .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
		$('div#'+scrollDiv+' .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
		$('div#'+scrollDiv+' .mCSB_scrollTools .mCSB_draggerRail').css('background','rgba(255,255,244,0.6)');
		$('div#'+scrollDiv+' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
	    $('div#'+scrollDiv+' .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
        $('div#'+scrollDiv+' .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','rgba(0,0,0,0.6)');
   
	}
	
	function contentScrollBar(popId){
	    $('div#'+popId).mCustomScrollbar({
			scrollButtons:{
				enable:true
			},
			theme:"minimal-dark",
			callbacks:{
	           onTotalScroll:function(){
	        },
	        onTotalScrollOffset:100
		  }
		});
		$('div#'+popId+' .mCSB_container').css('margin-right','15px');
		$('div#'+popId+' .mCSB_scrollTools .mCSB_buttonUp').css('opacity','0.6');
		$('div#'+popId+' .mCSB_scrollTools .mCSB_buttonDown').css('opacity','0.6');
		$('div#'+popId+' .mCSB_scrollTools .mCSB_draggerRail').css('background','#fff');
		$('div#'+popId+' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('background','#7A929E');
	    $('div#'+popId+' .mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar').css('background','#7A929E');
        $('div#'+popId+' .mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar').css('background','#7A929E');
        $('div#'+popId+' .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar').css('width','3px');
	}
	
	function imageOnProjNotErrorReplace(obj){
		$(obj).attr('src',lighttpdpath+"/projectimages/dummyLogo.png");
		/*Initial(obj);*/
	}
    
	function imageOnFileNotErrorReplace(obj){
		$(obj).attr('src',"/images/document/unknown.svg");
	}

    function userImageOnErrorReplace(obj){
       /*if(lighttpdPath !=''){
          $(obj).attr('src',lighttpdPath+"/userimages/userImage.png");
       }else{
    	   alert("inside this function 2");
          $(obj).attr('src',"images/userImage.png");
       } */
    	Initial(obj);
    }
    
    function Initial(obj){
    	$(obj).initial({
    		name:$(obj).attr("title"),
    		height:64,
    		width:64,
    		charCount:2,
    		textColor:"#fff",
    		fontSize:30,
    		fontWeight:700
    	}); 
    }
    
    function imageOnCompanyErrorReplace(obj){
	    if(lighttpdPath !=''){  
	    	$(obj).attr('src', lighttpdPath+"/organization_images/colabuslogo.png");
	    }else{
	       $(obj).attr('src',"images/colabuslogo.png");
	    }	
    }
      
    function imageOnErrorReplaceNotes(obj){
		$(obj).attr('src',lighttpdPath+"/notes/thumbnails/cover1.png");
	}
	function albumImgOnErrorReplace(obj){
		$(obj).attr('src',lighttpdPath+"/gallery/thumbnails/album.png");
	}
	function albumImgOnErrorReplaceProfile(obj){
		$(obj).attr('src',lighttpdPath+"/gallery/thumbnails/userImage.png");
	}
    function blogImgOnErrorReplace(obj){
    	$(obj).attr('src',lighttpdPath+"blog/thumbnails/blog.png");
    }
   
    
    function doLogOut(act){
    	
    	//updateMykronusWork('stop');
        //sessionStorage.Mykronus = "N";
    	
		sessionStorage.chatCount="";
        localStorage.setItem("chatFlashStatusFlag","false");
        localStorage.setItem("disableCassandraFlag","false");
	    handleSignoutClick();
    	var loginType = window.location.href;
    	
    	if(loginType.indexOf("Google") == '-1'){
    		console.log('----if');
			
			window.history.forward();
			sessionStorage.clear();
			window.location.href = hostname+"/login";
		    
			$("div.userProfileMainDiv").hide();
		    //if(sessionStorage.name == "colabusChat" || sessionStorage.name == "chatBoxClosed" ){
		    try{
		    	 sessionStorage.name = "";
		    	 sessionStorage.click="";
		    	 sessionStorage.chatCount="";
		    	 window.open('', 'Colabus Chat').close();    
		    } catch (e) {
		        console.log(e);
		    }
		    //}
		   
		    disconnect();
		    
    	}else{
    		console.log('----else');
	    	var googlePopup=window.open('https://accounts.google.com/logout','','toolbar=0,status=0,width=0,height=0,scrollbars=1');
	    	setTimeout(function(){
				googlePopup.close();
		  	   	document.logoutForm.action = path+"/colAuth?pAct="+act+"";
			    document.logoutForm.submit();
			    $("div.userProfileMainDiv").hide();
			    // This code block is added to close the chat box automatically on logout if chat box is open 
			    //if(sessionStorage.name == "colabusChat" || sessionStorage.name == "chatBoxClosed" ){
			    try{
			     //  window.open('${path}/jsp/chat.jsp', 'Colabus Chat', 'toolbar=no,location=no, status=no, menubar=no,scrollbars=yes, resizable=yes,width=10px,height=10px').close();    
			    	sessionStorage.name = "";
			    	sessionStorage.click="";
			    	sessionStorage.chatCount=""; 
			    	window.open('', 'Colabus Chat').close();    
			    } catch (e) {
			        console.log(e);
			    }	
			    //}
			    disconnect();
		    },2000);
	    }
	    sessionStorage.name = "";
		
		sessionStorage.chatCount="";
		sessionStorage.click="";
    }

	function checksession(){
		try{
			var d1= new Date(sessionexp);
			console.log("sessionexp time--->"+d1); 

			var d = new Date();				//------current day and time
			console.log("current day and time--->"+d);
			
			if(d>=d1){
				abort();
				alert("Session Has Timed Out!");
				doLogOut('logout');
			}else if(d<d1){
				console.log("current time is lesser than expire time // session active");
			}
		}catch(e){
			console.log("checksession---->"+e);
		}
	}	

    function getValues(xmldata, tagName) {
   		var value = "";
   		var jsonData = xmldata;
		var data = jsonData;
		value = data[tagName];
			
        return value;
     }

	/* function IsJsonString(str) {
	console.log("str----->>>"+str);
	    try {
	        JSON.parse(str);
	    } catch (e) {
	        return false;
	    }
	    return true;
	} */
    
    function loadModule(obj){
       openMenuMethod($(obj).attr('loadType'));
    }
    
	function checkSessionTimeOut(result){
	    if(result == 'SESSION_TIMEOUT'){
	    	abort();//------ this is called to abort all pending ajax requests
	    	alert('Session Has Timed Out!');
			doLogOut('seslogout');
			
			return false;
		}else if(result == 'CONNECTION_TIMEOUT'){
		    window.location = path+'/jsp/postLogin.jsp?errorCode=CT';
			return false;
		    //checkError(null,result,'');
			//alert('Oops! An Error Occured.\n\nConnection to the server failed.');
			//doLogOut('seslogout');
			//disconnect();
			//window.location = path+"/login.jsp";
			//return false;
		}else if(result == 'EXCEPTION'){
		    window.location = path+'/jsp/postLogin.jsp?errorCode=EXC';
			return false;
		}
	}
   

   function gotoLogin(){
      window.location = path+"/login.jsp";
   }
   
   function checkError(jqXHR,textStatus,errorThrown){
	 	//window.location = path+'/jsp/error.jsp'
	 	/*console.log('jqXHR:');
        console.log(jqXHR);
        console.log('textStatus:');
        console.log(textStatus);
        console.log('errorThrown:');
        console.log(errorThrown);*/
        //alert(jqXHR.status+'<---->'+errorThrown);
        
	 	//else if(textStatus ==='abort'){
	 	 // alert('Request aborted.');
	 	//}//else {
         // alert('Uncaught Error.\n' + jqXHR.responseText);
        //}
		if(jqXHR.status == "500"){
			$('#app').html('');
	       	var errorMsg = '<h1>Oops! An Error Occured</h1>'
			   +'<h5>Please try again later</h5>'
			   +'<h6>Go to <a onclick="redirect();" href="#">Home</a> page.</h6>';
			
			$('#app').html(errorMsg);
			
		}
   }  

	function getTimeOffset(date){
		//DST Prototypes for detecting DST
		Date.prototype.stdTimezoneOffset = function() {
		    var jan = new Date(this.getFullYear(), 0, 1);
		    var jul = new Date(this.getFullYear(), 6, 1);
		    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
		}
		Date.prototype.dst = function() {
		    return this.getTimezoneOffset() < this.stdTimezoneOffset();
		}
		// for changing java script UTC to GMT with detecting DST
		var d = date;
	    var s = d.toString().split('GMT')[1].split('(')[0];
	    var a = s;
		a = s.substr(1,2);
		var b = s.substr(3,4); 
		if(a > 0){
		  if (d.dst()) {
		    a = parseInt(a)+1;
		  }
		  a= a*60*60;
		}
		b = b*60;
		a = a+b;
		if(s < 0){
		  a ='-'+a;
		} 
	   return a;	
	}
	
  function loadProjectData(projectId,settingsType){
     mAct="landwMenuProj";
     if(typeof settingsType == 'undefined'){
    	 settingsType = "";
     }
	 var rid = roleid.split("\"")[1];
     //window.location.href = path+"/Redirect.do?pAct="+mAct+"&projectId="+projectId+"&projActionType="+settingsType; 
     window.location = hostname+"/postlogin?pAct="+mAct+"&roleid="+rid;
  }
  function loadMyzoneData(menuObj, mAct, place){
	 
	if($(menuObj).hasClass('menuLinkActive')){ //------checking menu is already active or not 
		if(mAct=='mz' || mAct=='myTask'){
			fetchmzTasks("historyTask","firstClick");
			mzOptionActive('.breadCrum3');
		}else{
			comingsoon();
		}
	}else{
		if(place!="breadcrum"){menuLinkActive(menuObj);}
		switchZone(mAct);
		//window.location.href ="${path}/Redirect.do?pAct="+mAct+"&projId="+projectId+"&projType="+projType+"&projName="+projtName+"&projArchStatus="+archivedstatus+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus;
		loadSubPageContent(mAct);
		if(mAct=='mz' || mAct=='myTask'){
			mzOptionActive('.breadCrum3');
		}
	}

  }
  function createNewProject(){
      var projFlag = true;
	   if(companyType == "Social" ){
	      $("#loadingBar").show();
		  timerControl("start");
	      $.ajax({
	          url: path + "/workspaceAction.do",
	          type:'POST',
	          async: false,
	          data:{act:'checkNoOfProjects'},
	          error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
	          success:function(result){
	              checkSessionTimeOut(result);
	              //alert(result);
	              var data = result.split("@#@#");
	          	  //alert("-----"+data[0]+"---------"+data[1]);
	          	  $("#loadingBar").hide();
				  timerControl("");
				  if(data[0]=='error'){
	          	     alertFun(getValues(companyAlerts,"Alert_Error"),'warning');
	          	    
	          	  }else if(data[1]=='0'){
	          	  	$('#Txtcontnt').text('You have no more new Workspaces.');	
	          	  	$('#upgradeProjectDiv, #transparentDiv').show();
	          	  	$('#continuebutton').hide();
	          	    
	          	  }else{
	          	    $('#upgradeProjectDiv, #transparentDiv, #continuebutton').show();
	          	    
	          	    if(data[1] == 1){
        	   			 $('#Txtcontnt').text('You have '+data[1]+' more new Workspace.');	
	          	    }else{
	          	     	$('#Txtcontnt').text('You have '+data[1]+' more new Workspaces.');	
	          	    }
	          	 }
			 }
	      });
	   }
	 else{
		newproj();   // This function call will fetch the users according to company Id and show it for add users.
	}  	
  }
  
  function newproj(){
  		if($('#createWorkflowTempDivNew').css('display') == 'block'){
		  			$("#createWorkflowTempDivNew").hide();
		}
	     $('#upgradeProjectDiv, #continuebutton').hide();
	    $("#transparentDiv").show();
	  	$("div#newProjContent").css('display','block');
	  	clearFormValues();
	  	fetchUsers();
  
  }
 
   function buyProject(){
      var projToBuy = $('#inZoneDropDown option:selected').text();
      
      /*if(projToBuy == '' || projToBuy == '0'){
          alertFun("Please enter number of projects to buy.",'warning');
		  $("#projToBuy").focus();
		  return false;		
      }
      if(isNaN(projToBuy)){
        alertFun(getValues(companyAlerts,"Alert_numericValues"),'warning');
		return false;
      } */
      $("#loadingBar").show();
	  timerControl("start");
	  $.ajax({
			url: path+"/workspaceAction.do",
			type:"POST",
			data:{act:"buyProject",projToBuy:projToBuy},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
		        checkSessionTimeOut(result);
		        if(result =="success"){  	
 		    	  alertFun(getValues(companyAlerts,"Alert_SuccessWS"),'warning');
 		    	  closeUpgradeProjDiv();
 		    	}else if(result=="failed"){
 		    	  alertFun(getValues(companyAlerts,"Alert_fail"),'warning');
 		    	}
	            $("#loadingBar").hide();
				timerControl("");
			}
	  });
  }
 
 function closeUpgradeProjDiv(){
    $('#upgradeProjectDiv, #transparentDiv').hide();
    //$('#projToBuy').val('');
    $('#inZoneDropDown option:eq(0)').attr('selected','selected');
 }
  
  function clearFormValues(){
	  
		 $('#privacyImg').attr('src', path+'/images/private.png').attr('value','private');
	     $("input#projName ,textarea#comment, input#upload ").val('');
	  	 $('#newProjContent').find('div.projUploadDiv').html('');
	  	 $('#hideDiv').attr('src', lighttpdpath+"/projectimages/dummyLogo.png");
	     $('#userList ul').html('');	 
	     $('#inviteUsers').val('');
	     $('#userforProj').hide();
  }
  
  

  
  
  
  
  
   
  function loadContactData(){
      mAct="comContact";
      window.location.href = path+"/Redirect.do?pAct="+mAct; 
  }
  
  function loadAnalyticsData(reportType,sentimentProjName,RepPeriod){
     mAct="reportMenu";
     if(typeof reportType =='undefined'){
    	 reportType = "";
     }
     if(typeof sentimentProjName =='undefined'){
    	 sentimentProjName = "";
     }
     if(typeof RepPeriod =='undefined'){
    	 RepPeriod = "";
     }
     window.location.href = path+"/Redirect.do?pAct="+mAct+"&reportType="+reportType+"&sentimentProjName="+sentimentProjName+"&RepPeriod="+RepPeriod; 
  }
  
  function loadProjectAdminData(){
     mAct="apMenu";
     window.location.href = path+"/Redirect.do?pAct="+mAct; 
  }
  
  function loadSysAdminData(){
     mAct="auMenu";
     window.location.href = path+"/Redirect.do?pAct="+mAct; 
  }
  
  var glbVartime="";
   /**  For showing timebar **/
   var secondCount = 0;
   function timerControl(status){
   if(window.handle){clearInterval(window.handle)};	
	var timercount = 0;
	if( status == "start" ){		
		$("span#timerSpan").show();
		handle = setInterval(function(){
			timercount++;
			secondCount++;
			if( secondCount >= 60 ){
				secondCount = 0;
			}else{
				$("span#timerSpan").text(getMinute(timercount)+':'+secondCount);
				glbVartime = $("span#timerSpan").text();
				//The below condition is added to know the global search takes still more long to get searched
				//if(glbVartime == "0:10" && searchTypeAlertConfirm != null && searchTypeAlertConfirm == "searchFileContntToo"){
				   //confirmReset("This may take several minutes. Do you want to continue ? ",'delete','searchafteralertpopUp','searchExceptFileContent');
				//}
			}			
		},1000);							
	}else{
		$("span#timerSpan").hide();
		$("span#timerSpan").text("");	
		timercount = 0;	
		secondCount = 0;		
	}		
	}
		
	function getMinute(m){
	   return Math.floor(m/60);
	}
	/***For showing timebar ***/
	
  /**************Functions for custom alert ***************/
	
  function confirmFun(info,type,execFunc){	  
     if(!info || !execFunc || !type){
            throw new Error("Please enter all the required config options!");
     }
     //alert(execFunc)
     if(type=='clear'){
        name="BoxConfirmClear";
     }else if(type=='close'){
        name="BoxConfirmClose";
     }else if(type=='delete'){
        name="BoxConfirmDelete";
     }else{
        name="BoxConfirmReset";
     }
     
    
    retFunc=execFunc ; 
    var html='<div id="BoxOverlay" ></div>'+
    		 '<div id="confirmDiv" class="alertMainCss">'+
    		 '  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
             //'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
             '  <div style="box-shadow: 0px 0px 20px #000000;" >'+
             '   <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">'+
             '    <div id="confirm-BoxContenedor" class="'+name+'">'+
             '     <span id="confirmContent">'+info+'</span>'+   
             '     <div id="confirm-Buttons" class="alertBtnCss">'+
             '       <input id="BoxConfirmBtnOk" type="submit" value="'+getValues(companyLabels,"OK")+'"  >'+
             '       <input id="BoxConfirmBtnCancel" type="submit" value="'+getValues(companyLabels,"Cancel")+'"  >'+
             '     </div>'+
             '    </div>'+
             '  </div>'+
             ' </div>'+
             '</div>';
          
          $('body').find('#BoxOverlay').remove();
          $('body').find('#confirmDiv').remove();
          //setTimeout(function(){  
                $('body').append(html);
                //$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').attr("onclick",retFunc);
               
               $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').click(function(){
                 hideConfirmBox();
                // top.frames["menuFrame"].window[retFunc]();
				window[retFunc]();
		       });
		        $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
		          hideConfirmBox();
		           if(execFunc == 'OptionalDrives'){		        	   
		   	         glbFileContents();	
		           }
		        });
		        $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
		          hideConfirmBox();
		        });
       //},50);    
}

  function confirmReset(info,type,execFunc,execCancel, label1, label2){
     if(!info || !execFunc || !type || !execCancel){
            throw new Error("Please enter all the required config options!");
     }
     if(type=='clear'){
        name="BoxConfirmClear";
     }else if(type=='close'){
        name="BoxConfirmClose";
     }else if(type=='delete'){
        name="BoxConfirmDelete";
     }else{
        name="BoxConfirmReset";
     }
     retFunc=execFunc ; 
     var html='<div id="BoxOverlay" ></div>'+
    		 '<div id="confirmDiv" class="alertMainCss">'+
    		 '  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
             //'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
             '  <div style="box-shadow: 0px 0px 20px #000000;" >'+
             '   <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">'+
             '    <div id="confirm-BoxContenedor" class="'+name+'">'+
             '     <span id="confirmContent">'+info+'</span>'+   
             '     <div id="confirm-Buttons" class="alertBtnCss">';
             if(typeof(label1) != "undefined" && label1 != null && label1 != ""){
             	  html+=' <input id="BoxConfirmBtnOk" type="submit" value="'+label1+'"  >';
             }else{
             	 html+=' <input id="BoxConfirmBtnOk" type="submit" value="'+getValues(companyLabels,"OK")+'"  >';
             }
             if(typeof(label2) != "undefined" && label2 != null && label2 != ""){
             	 html+= '<input id="BoxConfirmBtnCancel" type="submit" value="'+label2+'"  >';
             }else{
             	 html+= '<input id="BoxConfirmBtnCancel" type="submit" value="'+getValues(companyLabels,"Cancel")+'"  >';
             }
              html+='</div>'+
             '    </div>'+
             '  </div>'+
             ' </div>'+
             '</div>';
          
          $('body').find('#BoxOverlay').remove();
          $('body').find('#confirmDiv').remove();
          //setTimeout(function(){  
                $('body').append(html);
                //$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').attr("onclick",retFunc);
               
                $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').click(function(){
                   hideConfirmBox();
                   //top.frames["menuFrame"].window[retFunc]();
                   window[retFunc]();
		        });
		        $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
		           hideConfirmBox();
		           //top.frames["menuFrame"].window[execCancel]();
		           window[execCancel]();
		        });
		        $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
		           hideConfirmBox();
		           //top.frames["menuFrame"].window[execCancel]();
		           //window[execCancel]();
		        });
      // },50);    
  }
  function alertFunNew(info,type,validation,param1){
	if(!info ||  !type ){
		throw new Error("Please enter all the required config options!");
 	}
	let messageType="";
	let messageTypeSrc="";
	if(type=='error'){
		messageType="Error";
		messageTypeSrc="images/menus/error.svg";
	}else if(type=='warning'){
		
		messageType="Warning";
		messageTypeSrc="images/menus/warning.svg";
	}
	if(type=='clear'){
        name="BoxConfirmClear";
     }else if(type=='close'){
        name="BoxConfirmClose";
     }else if(type=='delete'){
        name="BoxConfirmDelete";
     }else{
        name="BoxConfirmReset";
     }
	let html='<div id="BoxOverlay" ></div>'+
			'<div id="confirmDiv" class="alertMainCss">'+
			'  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
			//'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
			'  <div style="" >'+
			'   <div style=" padding: 10px 20px 0;">'+
			'    <div id="confirm-BoxContenedor" class="'+name+'">'
	html+='	   <div class="d-flex align-items-center" >'+
					  '		  <img src="'+messageTypeSrc+'" style="width: 30px;height: 30px;">'+
					  '		  <span style="margin-left: 10px;font-size: 16px;">'+messageType+'</span>'+
					  '	  </div>'
	 html+='<div id="confirmContent" style="margin-top: 10px;padding-left: 5px;">'+info+'</div>'
	
					+'     <div id="confirm-Buttons" class="alertBtnCss">';
					html+=' <input id="BoxConfirmBtnOk" type="submit" value="'+getValues(companyLabels,"OK")+'"  >';

					html+='</div>'+
					'    </div>'+
					'  </div>'+
					' </div>'+
					'</div>';
					console.log(html)	
					$('body').find('#BoxOverlay').remove();
					$('body').find('#confirmDiv').remove();
					//setTimeout(function(){  
						  $('body').append(html);	
						 
						  $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').click(function(){
							hideConfirmBox();
							if(validation=='min'){
								$(param1).focus();
							}
							//top.frames["menuFrame"].window[retFunc]();
							//window[retFunc](param1,param2,param3);
						 });
						 $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
							hideConfirmBox();
							//top.frames["menuFrame"].window[execCancel]();
							//window[execCancel]();
						 });
						 $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
							hideConfirmBox();
							//top.frames["menuFrame"].window[execCancel]();
							//window[execCancel]();
						 });				  
						  

	 				  

  }
  
  function conFunNew(info,type,execfunc,execCancel,param1,param2){
	if(!info ||  !type ){
		throw new Error("Please enter all the required config options!");
 	}
	let messageType="";
	let messageTypeSrc="";
	let name="";
	if(type=='error'){
		messageType="Error";
		messageTypeSrc="images/menus/error.svg";
	}else if(type=='warning'){
		
		messageType="Warning";
		messageTypeSrc="images/menus/warning.svg";
	}
	if(type=='clear'){
        name="BoxConfirmClear";
     }else if(type=='close'){
        name="BoxConfirmClose";
     }else if(type=='delete'){
        name="BoxConfirmDelete";
     }else{
        name="BoxConfirmReset";
     }
	 retFunc=execfunc ; 
	let html='<div id="BoxOverlay" ></div>'+
			'<div id="confirmDiv" class="alertMainCss">'+
			'  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
			//'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
			'  <div style="" >'+
			'   <div style=" padding: 10px 20px 0;">'+
			'    <div id="confirm-BoxContenedor" class="'+name+'">'
	html+='	   <div class="d-flex align-items-center" >'+
					  '		  <img src="'+messageTypeSrc+'" style="width: 30px;height: 30px;">'+
					  '		  <span style="margin-left: 10px;font-size: 16px;">'+messageType+'</span>'+
					  '	  </div>'
	 html+='<div id="confirmContent" style="margin-top: 10px;padding-left: 5px;">'+info+'</div>'
	
					+'     <div id="confirm-Buttons" class="alertBtnCss">';
					html+=' <input id="BoxConfirmBtnOk" type="submit" value="'+getValues(companyLabels,"OK")+'"  >';
					html+= '<input id="BoxConfirmBtnCancel" type="submit" value="'+getValues(companyLabels,"Cancel")+'"  >';

					html+='</div>'+
					'    </div>'+
					'  </div>'+
					' </div>'+
					'</div>';
					console.log(html)	
					$('body').find('#BoxOverlay').remove();
					$('body').find('#confirmDiv').remove();
					//setTimeout(function(){  
						  $('body').append(html);	
						 
						  $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').click(function(){
							hideConfirmBox();
							//top.frames["menuFrame"].window[retFunc]();
							//window[retFunc](param1,param2,param3);
							window[retFunc](param1,param2);
						 });
						 $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
							hideConfirmBox();
							//top.frames["menuFrame"].window[execCancel]();
							window[execCancel]();
						 });
						 $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
							hideConfirmBox();
							//top.frames["menuFrame"].window[execCancel]();
							//window[execCancel]();
						 });				  
						  

	 				  

  }	

  function conFunNew11(info,type,execfunc,execCancel,param1,param2,param3,param4){
	if(!info ||  !type ){
		throw new Error("Please enter all the required config options!");
 	}
	let messageType="";
	let messageTypeSrc="";
	let name="";
	if(type=='error'){
		messageType="Error";
		messageTypeSrc="images/menus/error.svg";
	}else if(type=='warning'){
		
		messageType="Warning";
		messageTypeSrc="images/menus/warning.svg";
	}
	if(type=='clear'){
        name="BoxConfirmClear";
     }else if(type=='close'){
        name="BoxConfirmClose";
     }else if(type=='delete'){
        name="BoxConfirmDelete";
     }else{
        name="BoxConfirmReset";
     }
	 retFunc=execfunc ; 
	let html='<div id="BoxOverlay" ></div>'+
			'<div id="confirmDiv" class="alertMainCss">'+
			'  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
			'  <div style="" >'+
			'   <div style=" padding: 10px 20px 0;">'+
			'    <div id="confirm-BoxContenedor" class="'+name+'">'
	html+='	   <div class="d-flex align-items-center" >'+
					  '		  <img src="'+messageTypeSrc+'" style="width: 30px;height: 30px;">'+
					  '		  <span style="margin-left: 10px;font-size: 16px;">'+messageType+'</span>'+
					  '	  </div>'
	 html+='<div id="confirmContent" style="margin-top: 10px;padding-left: 5px;">'+info+'</div>'
	
					+'     <div id="confirm-Buttons" class="alertBtnCss">';
					html+=' <input id="BoxConfirmBtnOk" type="submit" value="'+getValues(companyLabels,"OK")+'"  >';
					html+= '<input id="BoxConfirmBtnCancel" type="submit" value="'+getValues(companyLabels,"Cancel")+'"  >';

					html+='</div>'+
					'    </div>'+
					'  </div>'+
					' </div>'+
					'</div>';
					console.log(html)	
					$('body').find('#BoxOverlay').remove();
					$('body').find('#confirmDiv').remove();
					//setTimeout(function(){  
						  $('body').append(html);	
						 
						  $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').click(function(){
							hideConfirmBox();
							window[retFunc](param1,param2,param3,param4);
						 });
						 $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
							hideConfirmBox();
							window[execCancel]();
						 });
						 $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
							hideConfirmBox();
						 });				  
						  

	 				  

  }	

  function confirmResetNew(info,type,execFunc,execCancel, label1, label2,param1,param2,param3){
	if(!info || !execFunc || !type || !execCancel){
		   throw new Error("Please enter all the required config options!");
	}
	var messageType="";
	var messageTypeSrc="";
	if(type=='clear'){
	   name="BoxConfirmClear";
	   messageType="";
	}else if(type=='close'){
	   name="BoxConfirmClose";
	   messageType="";
	}else if(type=='delete'){
	   name="BoxConfirmDelete";
	   messageType="";
	}else if(type=='warning'){
		name="BoxConfirmReset";
		messageType="Warning";
		messageTypeSrc="images/menus/warning.svg";
	}else if(type=='error'){
		name="BoxConfirmReset";
		messageType="Error";
		messageTypeSrc="images/menus/error.svg";
	}else {
		messageType="";
	   name="BoxConfirmReset";
	}
	retFunc=execFunc ; 
	var html='<div id="BoxOverlay" ></div>'+
			'<div id="confirmDiv" class="alertMainCss">'+
			'  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
			//'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
			'  <div style="" >'+
			'   <div style=" padding: 10px 20px 0;">'+
			'    <div id="confirm-BoxContenedor" class="'+name+'">'
			if(messageType !=""){
				html+='	   <div class="d-flex align-items-center" >'+
					  '		  <img src="'+messageTypeSrc+'" style="width: 30px;height: 30px;">'+
					  '		  <span style="margin-left: 10px;font-size: 16px;">'+messageType+'</span>'+
					  '	  </div>'
			}
			
				html+='<div id="confirmContent" style="margin-top: 10px;padding-left: 5px;">'+info+'</div>'+   
					'     <div id="confirm-Buttons" class="alertBtnCss">';
					if(typeof(label1) != "undefined" && label1 != null && label1 != ""){
						html+=' <input id="BoxConfirmBtnOk" type="submit" value="'+label1+'"  >';
					}else{
						html+=' <input id="BoxConfirmBtnOk" type="submit" value="'+getValues(companyLabels,"OK")+'"  >';
					}
					if(typeof(label2) != "undefined" && label2 != null && label2 != ""){
						html+= '<input id="BoxConfirmBtnCancel" type="submit" value="'+label2+'"  >';
					}else{
						html+= '<input id="BoxConfirmBtnCancel" type="submit" value="'+getValues(companyLabels,"Cancel")+'"  >';
					}
					html+='</div>'+
			'    </div>'+
			'  </div>'+
			' </div>'+
			'</div>';
		 
		 $('body').find('#BoxOverlay').remove();
		 $('body').find('#confirmDiv').remove();
		 //setTimeout(function(){  
			   $('body').append(html);
			   //$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').attr("onclick",retFunc);
			  
			   $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').click(function(){
				  hideConfirmBox();
				  //top.frames["menuFrame"].window[retFunc]();
				  window[retFunc](param1,param2,param3);
			   });
			   $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
				  hideConfirmBox();
				  //top.frames["menuFrame"].window[execCancel]();
				  window[execCancel]();
			   });
			   $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
				  hideConfirmBox();
				  //top.frames["menuFrame"].window[execCancel]();
				  //window[execCancel]();
			   });
	 // },50);    
 }

  function hideConfirmBox(){
	   $('#BoxOverlay').hide();
	   $('#confirmDiv').hide();
  }
 
  function confirmCopyMove(info,type,execFuncCopy,execFuncMove){
     if(!info || !execFuncCopy || !type ){
            throw new Error("Please enter all the required config options!");
     }
     if(type=='clear'){
        name="BoxConfirmClear";
     }else if(type=='close'){
        name="BoxConfirmClose";
     }else if(type=='delete'){
        name="BoxConfirmDelete";
     }else{
        name="BoxConfirmReset";
     }
   
     var html='<div id="BoxOverlay" ></div>'+
    		 '<div id="confirmDiv" class="alertMainCss">'+
    		 '  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
             //'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
             ' <div style="box-shadow: 0px 0px 20px #000000;" >'+
             '  <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">'+
             '   <div id="confirm-BoxContenedor" class="'+name+'">'+
             '    <span id="confirmContent">'+info+'</span>'+   
             '    <div id="confirm-Buttons" class="alertBtnCss">'+
             '       <input id="BoxConfirmBtnCopy" type="submit" value="'+getValues(companyLabels,"Copy")+'"  >';
             if(typeof execFuncMove!=='undefined'){
              html+='       <input id="BoxConfirmBtnMove" type="submit" value="'+getValues(companyLabels,"Move")+'"  >'
             }
             html+='       <input id="BoxConfirmBtnCancel" type="submit" value="'+getValues(companyLabels,"Cancel")+'"  >'+
             '    </div>'+
             '   </div>'+
             '  </div>'+
             ' </div>'+
             '</div>';
          
          $('body').find('#BoxOverlay').remove();
          $('body').find('#confirmDiv').remove();
          //setTimeout(function(){  
                $('body').append(html);
                //$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').attr("onclick",retFunc);
               
                $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCopy').click(function(){
                   hideConfirmBox();
                   //top.frames["menuFrame"].window[retFunc]();
                   window[execFuncCopy]();
		        });
		        $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnMove').click(function(){
                   hideConfirmBox();
                   //top.frames["menuFrame"].window[retFunc]();
                   window[execFuncMove]();
		        });
		        $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
		           hideConfirmBox();
		           
		        });
		        $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
		           hideConfirmBox();
		          
		        });
      // },50);    
  }


  function confirmCopyMoveCancel(info,type,execFuncCopy,execFuncMove,label1,label2,label3){
     if(!info || !execFuncCopy || !type ){
            throw new Error("Please enter all the required config options!");
     }
     if(type=='clear'){
        name="BoxConfirmClear";
     }else if(type=='close'){
        name="BoxConfirmClose";
     }else if(type=='delete'){
        name="BoxConfirmDelete";
     }else{
        name="BoxConfirmReset";
     }
   
     var html='<div id="BoxOverlay" ></div>'+
    		 '<div id="confirmDiv" class="alertMainCss">'+
    		 '  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
             //'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
             ' <div style="box-shadow: 0px 0px 20px #000000;" >'+
             '  <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">'+
             '   <div id="confirm-BoxContenedor" class="'+name+'">'+
             '    <span id="confirmContent">'+info+'</span>'+   
             '    <div id="confirm-Buttons" class="alertBtnCss">'+
             '       <input id="BoxConfirmBtnCopy" type="submit" value="'+label1+'"  >';
             if(typeof execFuncMove!=='undefined'){
              html+='       <input id="BoxConfirmBtnMove" type="submit" value="'+label2+'"  >'
             }
             html+='       <input id="BoxConfirmBtnCancel" type="submit" value="'+label3+'"  >'+
             '    </div>'+
             '   </div>'+
             '  </div>'+
             ' </div>'+
             '</div>';
          
          $('body').find('#BoxOverlay').remove();
          $('body').find('#confirmDiv').remove();
          //setTimeout(function(){  
                $('body').append(html);
                //$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').attr("onclick",retFunc);
               
                $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCopy').click(function(){
                   hideConfirmBox();
                   //top.frames["menuFrame"].window[retFunc]();
                   window[execFuncCopy]();
		        });
		        $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnMove').click(function(){
                   hideConfirmBox();
                   //top.frames["menuFrame"].window[retFunc]();
                   window[execFuncMove]();
		        });
		        $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
		           hideConfirmBox();
		           
		        });
		        $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
		           hideConfirmBox();
		          
		        });
      // },50);    
  }
  
  function confirmCopyMoveCancelNew(info,type,execFuncCopy,execFuncMove,label1,label2,label3, param1, param2){
	if(!info || !execFuncCopy || !type ){
		   throw new Error("Please enter all the required config options!");
	}
	var messageType="";
	var messageTypeSrc="";
	if(type=='clear'){
	   name="BoxConfirmClear";
	   messageType="";
	}else if(type=='close'){
	   name="BoxConfirmClose";
	   messageType="";
	}else if(type=='delete'){
	   name="BoxConfirmDelete";
	   messageType="";
	}else if(type=='warning'){
		name="BoxConfirmReset";
		messageType="Warning";
		messageTypeSrc="images/menus/warning.svg";
	}else if(type=='error'){
		name="BoxConfirmReset";
		messageType="Error";
		messageTypeSrc="images/menus/error.svg";
	}else {
		messageType="";
	   name="BoxConfirmReset";
	}
  
	
		 
			var html='<div id="BoxOverlay" ></div>'+
			'<div id="confirmDiv" class="alertMainCss">'+
			'  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
			//'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
			'  <div style="" >'+
			'   <div style=" padding: 10px 20px 0;">'+
			'    <div id="confirm-BoxContenedor" class="'+name+'">'
			if(messageType !=""){
				html+='	   <div class="d-flex align-items-center" >'+
					  '		  <img src="'+messageTypeSrc+'" style="width: 30px;height: 30px;">'+
					  '		  <span style="margin-left: 10px;font-size: 16px;">'+messageType+'</span>'+
					  '	  </div>'
			}
			
				html+='<div id="confirmContent" style="margin-top: 10px;padding-left: 5px;">'+info+'</div>'+   
					'     <div id="confirm-Buttons" class="alertBtnCss">';
					if(typeof(label1) != "undefined" && label1 != null && label1 != ""){
						html+=' <input id="BoxConfirmBtnCopy" type="submit" value="'+label1+'"  >';
					}

					if(typeof(label2) != "undefined" && label2 != null && label2 != ""){
						html+=' <input id="BoxConfirmBtnMove" type="submit" value="'+label2+'"  >';
					}


					if(typeof(label3) != "undefined" && label3 != null && label3 != ""){
						html+= '<input id="BoxConfirmBtnCancel" type="submit" value="'+label3+'"  >';
					}
					html+='</div>'+
			'    </div>'+
			'  </div>'+
			' </div>'+
			'</div>';

		 $('body').find('#BoxOverlay').remove();
		 $('body').find('#confirmDiv').remove();
		 //setTimeout(function(){  
			   $('body').append(html);
			   //$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').attr("onclick",retFunc);
			  
			   $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCopy').click(function(){
				  hideConfirmBox();
				  //top.frames["menuFrame"].window[retFunc]();
				  window[execFuncCopy](param1);
			   });
			   $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnMove').click(function(){
				  hideConfirmBox();
				  //top.frames["menuFrame"].window[retFunc]();
				  window[execFuncMove](param1);
			   });
			   $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
				  hideConfirmBox();
				  
			   });
			   $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
				  hideConfirmBox();
				 
			   });
	 // },50);    
 }



  function alertFun(info,type){
     if(!info || !type){
            throw new Error(getValues(companyAlerts,"Alert_EntAllReqOpt"));
     }
     if(type=='warning'){
        name="BoxAlert";
     }else if(type=='error'){
        name="BoxError";
     }else{
        name="BoxSuccess";
     }
     
     var html='<div id="BoxOverlay" ></div>'+
    		 '<div id="alertDiv" class="alertMainCss">'+
    		 '  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
             '  <div style="box-shadow: 0 0 20px #000000;" >'+
             '   <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">'+
             '    <div id="confirm-BoxContenedor" class="'+name+'">'+
             '     <span id="confirmContent">'+info+'</span>'+   
             '     <div id="confirm-Buttons" class="alertBtnCss">'+
             '       <input id="BoxAlertBtnOk" type="submit" value="'+getValues(companyLabels,"OK")+'" >'+
             '     </div>'+
             '    </div>'+
             '  </div>'+
             ' </div>'+
             '</div>';
             
          $('body').find('#BoxOverlay').remove();
          $('body').find('#alertDiv').remove();
          //setTimeout(function(){  
                $('body').append(html);
                $('body').find('#alertDiv').find('#confirm-Buttons').children('#BoxAlertBtnOk').click(function(){
		          alertOk();
		        });
		        
		        $('body').find('#alertDiv').children('#confirmCloseDiv').click(function(){
		          alertOk();
		        });
       //},50);            
             
 
 }
  function alertOk(){
	   $('#BoxOverlay').hide();
	   $('#alertDiv').hide();
  }
  
 
 /*------------------ special character replace method --------------------------*/
 
 function replaceSpecialCharacter(input){
    input = input.replaceAll( 'CHR(39)','&#39;');
    input = input.replaceAll( 'CH(58)','&#180;');
    input = input.replaceAll( 'CHR(40)','&quot;');
    input = input.replaceAll( 'CH(40)','&quot;');
    input = input.replaceAll( 'CH(39)','&quot;');
    input = input.replaceAll( 'CH(50)','\n');
    input = input.replaceAll( 'CHR(50)','\n');
    input = input.replaceAll( 'CH(57)','\n');
    input = input.replaceAll( 'CH(51)','&quot;');
    input = input.replaceAll( 'CH(70)','\\');
    input = input.replaceAll( 'CH(52)','&#39;');
    input = input.replaceAll( 'CH(54)','\t');
    input = input.replaceAll( "CH(101)","&#37;"); 
    input = input.replaceAll( "CHR(26)","&#58;"); 
    input = input.replaceAll( "CH(70)","&#92;");
    input = input.replaceAll( "chr(55)","&#32;");
    input = input.replaceAll( "<","&lt;");
    input = input.replaceAll( ">","&gt;");
    
   
    return input;
 }
  function replaceSpecialCharacterForTitle(input){
    input = input.replaceAll( 'CHR(39)','\'');
   	input = input.replaceAll( 'CH(58)','&#180;');
    input = input.replaceAll( 'CHR(40)','"');
    input = input.replaceAll( 'CH(40)','"');
    input = input.replaceAll( 'CHR(41)',' ');
    input = input.replaceAll( 'CH(39)','"');
    input = input.replaceAll( 'CH(50)','\n');
    input = input.replaceAll( 'CHR(50)','\n');
    input = input.replaceAll( 'CH(57)','\n');
    input = input.replaceAll( 'CH(51)','"');
    input = input.replaceAll( 'CH(70)','\\');
    input = input.replaceAll( 'CH(52)','\'');
    input = input.replaceAll( 'CH(54)','\t');
    input = input.replaceAll( "CH(101)","%"); 
    input = input.replaceAll( "CHR(26)",":"); 
  	input = input.replaceAll( "chr(55)"," ");
    input = input.replaceAll( "&lt;","<");
    input = input.replaceAll( "&gt;",">");
    
   
    return input;
 }
  function replaceSpecialCharacterForInput(input){
    input = input.replaceAll( 'CHR(39)','\'');
    input = input.replaceAll( 'CH(58)','&#180;');
    input = input.replaceAll( 'CHR(40)','"');
    input = input.replaceAll( 'CH(40)','"');
    input = input.replaceAll( 'CH(39)','"');
    input = input.replaceAll( 'CH(50)','\n');
    input = input.replaceAll( 'CHR(50)','\n');
    input = input.replaceAll( 'CH(57)','\n');
    input = input.replaceAll( 'CH(51)','"');
    input = input.replaceAll( 'CH(70)','\\');
    input = input.replaceAll( 'CH(52)','\'');
    input = input.replaceAll( 'CH(54)','\t');
    input = input.replaceAll( "CH(101)","%"); 
    input = input.replaceAll( "CHR(26)",":"); 
    input = input.replaceAll( "CH(70)","&#92;");
    input = input.replaceAll( "chr(55)","&#32;");
    input = input.replaceAll( "&lt;","<");
    input = input.replaceAll( "&gt;",">");
    input = input.replaceAll( "&#39;","'");
    input = input.replaceAll( "&quot;","\"");
    
   
    return input;
 }
  /****************Function Starts for NLP**********************/ 
  function unicodeTonotificationValue(text) {
	   return text.replace(/\\u[\dA-F]{4}/gi, 
	          function (match) {
	               return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
	          });
	}

function unicodeTotitle(text) {
	   return text.replace(/\\u[\dA-F]{4}/gi, 
	          function (match) {
	               return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
	          });
	}
/****************Function Ends for NLP**********************/ 
  
  
    /****************Function for Notification Redirection**********************/ 
     
    function loadMyZoneDetailedData(mAct,myZoneType,myZoneNotId,myZoneNotMenu,presentId,myZoneNotSharedType,myZoneNotLayoutType,myZoneNotType,myZoneNotName,myzonecommentCount,notificationId){
		//updateHistoryStatus(myZoneNotId,myZoneType);
		$("#loadingBar").show();	
		timerControl("start");
	    $.ajax({
			url: path+"/landPageNotAction.do",
			type:"POST",
			data:{act:"updateNoteStatusNotifications", notIds:notificationId,type:myZoneType},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
			    if(myZoneType == 'notesShare'){
					window.location = path+"/Redirect.do?pAct=myNote&myZoneNotId="+myZoneNotId+"&myZoneType="+myZoneType;
				}else if(myZoneType == 'galleryShare' ){
					window.location = path+"/Redirect.do?pAct=myGallery&myZoneNotId="+myZoneNotId+"&myZoneType="+myZoneType;
				}else if(myZoneType == 'blogShare' ){
					window.location = path+"/Redirect.do?pAct=myBlog&myZoneNotId="+myZoneNotId+"&myZoneType="+myZoneType;
				}else if(myZoneType == 'blogCommentShare'){
				    window.location = path+"/Redirect.do?pAct=myBlog&myZoneNotId="+presentId+"&myZoneType="+myZoneType+"&myZoneNotSharedType="+myZoneNotSharedType+"&myZoneNotName="+myZoneNotName+"&blogArticleId="+myZoneNotLayoutType+"&blogCommentId="+myZoneNotId;
				}else if(myZoneType == 'galleryCommentShare'){
				    window.location = path+"/Redirect.do?pAct=myGallery&myZoneNotId="+presentId+"&myZoneType="+myZoneType;
				}
				$("#loadingBar").hide();	
				timerControl("");
			}
		});
		
	} 
	
	 function detailView(id,pId,fname,ftype,fpath,stype,cmtId,mType,notificationId){
  		var fdType="";
  		var fdTypeId="";
  		if(mType == 'Foldershare'){
  		    fdType="folder";
  		    fdTypeId=id;
  		}else if(mType == 'Comments'){
  		    fdType="per_doc_comments";
  		    fdTypeId=cmtId;
  		}else if(mType == 'Fileshare'){
  		    fdType="file";
  		    fdTypeId=fpath;
  		}
  		
  		$("#loadingBar").show();	
		timerControl("start");
	    $.ajax({
			url: path+"/landPageNotAction.do",
			type:"POST",
			data:{act:"updateNoteStatusNotifications", notIds:notificationId,type:fdType},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
			    window.location = path+"/Redirect.do?pAct=myDocument&id="+id+"&pId="+pId+"&fname="+fname+"&ftype="+ftype+"&fpath="+fpath+"&stype="+stype+"&cmtId="+cmtId+"&mType="+mType+"";
			    $("#loadingBar").hide();	
				timerControl("");
			 }
			});
	}
 
    function openNotificationDetails(mAct,wrkSpaceNotType,wrkSpaceNotId,wrkSpaceMenuType,wrkSapceProjId,wrkspaceProjName,wrkSpaceProjType,wrkSpaceSubMenuType,wrkspaceImgType,id,projectArchStatus,projectUsersStatus,notificationId,notifStatus){
	   var histype="";
	   var from ="gSearch";
	   if(wrkSpaceSubMenuType == 'workSpaceActivityFeed'){
	      histype='WsFeedsNot';
       }else if(wrkSpaceSubMenuType == 'workspaceDocShare'){
          histype='WsFolderNot';
       }   
       wrkspaceProjName=wrkspaceProjName.replaceAll('CHR(41)',' ').replaceAll("CHR(26)",":").replaceAll("CH(39)","'").replaceAll("CH(40)",'"').replaceAll("CH(51)",'"').replaceAll("CH(52)","'");
       
        $("#loadingBar").show();	
		timerControl("start");
		
		updateNotifRecentProject(wrkSapceProjId);
		
	    $.ajax({
			url: path+"/landPageNotAction.do",
			type:"POST",
			data:{act:"updateNoteStatusNotifications", notIds:notificationId,type:histype},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
			   if(wrkSpaceSubMenuType == 'workSpaceActivityFeed'){
			       window.location = path+"/Redirect.do?pAct=LoadActivityFeed&projId="+wrkSapceProjId+"&projType="+wrkSpaceProjType+"&projName="+wrkspaceProjName+"&projImgSrc="+wrkspaceImgType+"&projArchStatus="+projectArchStatus+"&projType="+wrkSpaceProjType+"&projectUsersStatus="+projectUsersStatus+"&notFolid="+id+"&feedId="+id+"&notifStatus="+notifStatus+"&from="+from+"";
			   }else if(wrkSpaceSubMenuType == 'workspaceDocShare'){
			     window.location = path+"/Redirect.do?pAct=loadDocumentjsp&projId="+wrkSapceProjId+"&projType="+wrkSpaceProjType+"&projName="+wrkspaceProjName+"&projImgSrc="+wrkspaceImgType+"&projArchStatus="+projectArchStatus+"&projType="+wrkSpaceProjType+"&projectUsersStatus="+projectUsersStatus+"&notFolid="+id+"&docId="+id+"";
			   }
			    $("#loadingBar").hide();	
				timerControl("");
			 }
			});
	}
  	function updateNotifRecentProject(projId){

		$.ajax({
			url : path + "/workspaceAction.do",
			type : "POST",
			data : {act : "updateRecentProjectData",projectId : projId},
			error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						},
			success : function(result){
				checkSessionTimeOut(result);
			}
		});
	}
  	
    function loadDetailNotData(menuType,connNotUseId,createdName,notificationType,
    		notTypeId,pId,notCmtDocId,projectUsersStatus,projectArchStatus,projectType,
    		taskSourceId,tasksprintId,taskSprintGroupId,notificationId,ppId,notftaskCommentedBy,
    		notifTaskCmtType,notfEpicIds){   	
    	var from ="gSearch";
    	if(menuType == 'connect'){
	        mAct="conMessage";
	        window.location = path+"/Redirect.do?pAct=conMessage&param7="+notificationType+"&notifMessId="+connNotUseId+"&notifMsgUserName="+createdName;
		}else if(menuType == 'calendar'){
			  var calProjId=connNotUseId;
	          var projCaltitle=$('#notifProjImgNew_'+calProjId).attr('title');
	          if(typeof(projCaltitle) != "undefined"){
	          	projCaltitle=projCaltitle.replaceAll('CHR(41)',' ').replaceAll("CHR(26)",":").replaceAll("CH(39)","'").replaceAll("CH(40)",'"').replaceAll("CH(51)",'"').replaceAll("CH(52)","'");
	          }
	          var imgSrc=$('#notifProjImgNew_'+calProjId).attr('src');
	          if(notifTaskCmtType == 'workspace_Task'){
	          	connNotUseId =	notTypeId;
	          }
		      $("#loadingBar").show();	
			  timerControl("start");
		      $.ajax({
				url: path+"/landPageNotAction.do",
				type:"POST",
				data:{act:"updateNoteStatusNotifications", notIds:notificationId,type:notificationType},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
					checkSessionTimeOut(result);
				  if(notifTaskCmtType == 'task_comment'){
						 //if(notificationType == 'Sprint' || notificationType == 'Idea' || notificationType == 'WorkspaceDocument' || notificationType == 'Document' || notificationType == 'Email' || notificationType == 'Tasks' || notificationType == 'Event' || notificationType == 'Workflow' || notificationType == 'Highlight'){
							  window.location = path+"/Redirect.do?pAct=calMenuView&calNotificationType="+notificationType+"&notificationTypeId="+notTypeId+"&id="+connNotUseId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
						 //}
				  }else if(notificationType == 'Sprint'){
					 updateNotifRecentProject(calProjId);
				     if(tasksprintId == "0" || taskSprintGroupId == "0"){
				       window.location = path+"/Redirect.do?pAct=storyMenu&projId="+calProjId+"&projType="+projectType+"&projName="+projCaltitle+"&projImgSrc="+imgSrc+"&projectUsersStatus="+projectUsersStatus+"&projArchStatus="+projectArchStatus+"&agileNotificationType="+notificationType+"&agileNotificationId="+notTypeId+"&pId="+pId+"&epicCommDocId="+notCmtDocId+"&taskSourceId="+taskSourceId+"&tasksprintId="+tasksprintId+"&taskSprintGroupId="+taskSprintGroupId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				     }else{
				       window.location = path+"/Redirect.do?pAct=sprintMenu&projId="+calProjId+"&projType="+projectType+"&projName="+projCaltitle+"&projImgSrc="+imgSrc+"&projectUsersStatus="+projectUsersStatus+"&projArchStatus="+projectArchStatus+"&agileNotificationType="+notificationType+"&agileNotificationId="+notTypeId+"&pId="+pId+"&epicCommDocId="+notCmtDocId+"&taskSourceId="+taskSourceId+"&tasksprintId="+tasksprintId+"&taskSprintGroupId="+taskSprintGroupId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				     }
				  }else if(notificationType == 'Idea'){
					 updateNotifRecentProject(calProjId);
					 //window.location = path+"/Redirect.do?pAct=iMenu&projId="+calProjId+"&projType="+projectType+"&projName="+projCaltitle+"&projArchStatus="+projectArchStatus+"&projImgSrc="+imgSrc+"&projectUsersStatus="+projectUsersStatus+"&ideaType="+type+" &ideaNotificationType="+notificationType+"&ideaNotificationId="+notTypeId+"&ideaCmtDocId="+notCmtDocId+"&taskSourceId="+taskSourceId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				  	 window.location = path+"/Redirect.do?pAct=loadTaskjsp&projId="+calProjId+"&projType="+projectType+"&projName="+projCaltitle+"&projArchStatus="+projectArchStatus+"&projImgSrc="+imgSrc+"&projectUsersStatus="+projectUsersStatus+"&id="+connNotUseId+"&NotificationType="+notificationType+"&NotificationId="+notTypeId+"&NotificationTaskId="+notCmtDocId+"&notfTaskSourceId="+taskSourceId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;

				  }else if(notificationType == 'WorkspaceDocument'){
					  updateNotifRecentProject(calProjId);
				      //window.location = path+"/Redirect.do?pAct=loadDocumentjsp&projId="+calProjId+"&projType="+projectType+"&projName="+projCaltitle+"&projArchStatus="+projectArchStatus+"&projImgSrc="+imgSrc+"&projectUsersStatus="+projectUsersStatus+"&NotificationType="+notificationType+"&NotificationId="+notTypeId+"&NotificationTaskId="+notCmtDocId+"&notfTaskSourceId="+taskSourceId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				  	  window.location = path+"/Redirect.do?pAct=loadTaskjsp&projId="+calProjId+"&projType="+projectType+"&projName="+projCaltitle+"&projArchStatus="+projectArchStatus+"&projImgSrc="+imgSrc+"&projectUsersStatus="+projectUsersStatus+"&NotificationType="+notificationType+"&NotificationId="+notTypeId+"&id="+connNotUseId+"&NotificationTaskId="+notCmtDocId+"&notfTaskSourceId="+taskSourceId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				     
				  }else if(notificationType == 'Document'){
				     //window.location = path+"/Redirect.do?pAct=myDocument&NotificationType="+notificationType+"&NotificationId="+notTypeId+"&notfTaskId="+notCmtDocId+"&taskSourceId="+taskSourceId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				     window.location = path+"/Redirect.do?pAct=calMenuView&calNotificationType="+notificationType+"&notificationTypeId="+notTypeId+"&id="+connNotUseId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				     
				  }else if(notificationType == 'Email'){
					  updateNotifRecentProject(calProjId);
					  window.location = path+"/Redirect.do?pAct=loadEmailjsp&projId="+calProjId+"&projType="+projectType+"&projName="+projCaltitle+"&projArchStatus="+projectArchStatus+"&projImgSrc="+imgSrc+"&projectUsersStatus="+projectUsersStatus+"&NotificationType="+notificationType+"&NotificationId="+notTypeId+"&NotificationTaskId="+notCmtDocId+"&taskSourceId="+taskSourceId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				  }else if(notificationType == 'Tasks'){
				     if(taskSourceId == "0"){
				       window.location = path+"/Redirect.do?pAct=calMenuView&calNotificationType="+notificationType+"&notificationTypeId="+notTypeId+"&id="+connNotUseId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				     }else{
				    	 updateNotifRecentProject(calProjId);
				    	 /*if(notifTaskCmtType == 'task_user_comments' || notifTaskCmtType == 'cal_task'){
				    		 from = '';
				    	 }*/
						 window.location = path+"/Redirect.do?pAct=LoadActivityFeed&projId="+calProjId+"&projType="+projectType+"&projName="+projCaltitle+"&projArchStatus="+projectArchStatus+"&projImgSrc="+imgSrc+"&projectUsersStatus="+projectUsersStatus+"&NotificationType="+notificationType+"&NotificationTaskId="+notCmtDocId+"&notfTaskSourceId="+taskSourceId+"&feedId="+taskSourceId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType+"&from="+from+"";
				 	 }
				  }else if(notificationType == 'Highlight'){
					     window.location = path+"/Redirect.do?pAct=calMenuView&calNotificationType="+notificationType+"&notificationTypeId="+notTypeId+"&id="+notTypeId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				  } else{
				    window.location = path+"/Redirect.do?pAct=calMenuView&calNotificationType="+notificationType+"&notificationTypeId="+notTypeId+"&id="+connNotUseId+"&notftaskCommentedBy="+notftaskCommentedBy+"&notifTaskCmtType="+notifTaskCmtType;
				  }
				  $("#loadingBar").hide();	
				  timerControl("");
				}
			});
		 }else if(menuType =='agile'){
		 	  var agileProjId=connNotUseId;
		      var epicId =notTypeId;
	          var notifEpicId=epicId;
	          if($('#epic_'+epicId).children('ol').length >0){
	            $('#epic_'+epicId+' ol').children('li').each(function(){
	                     notifEpicId=notifEpicId+':'+$(this).attr('id').split('_')[1];
	            });
	          }
	          var projIdeatitle=$('#notifProjImgNew_'+agileProjId).attr('title');
	          projIdeatitle=projIdeatitle.replaceAll('CHR(41)',' ').replaceAll("CHR(26)",":").replaceAll("CH(39)","'").replaceAll("CH(40)",'"').replaceAll("CH(51)",'"').replaceAll("CH(52)","'");
	          var imgSrc=$('#notifProjImgNew_'+agileProjId).attr('src');
	          $('div#headertitle').text("");
	          updateNotifRecentProject(agileProjId);
	          if(notificationType =='agile_story'){
			     window.location = path+"/Redirect.do?pAct=sprintMenu&projId="+agileProjId+"&projType="+projectType+"&projName="+projIdeatitle+"&projImgSrc="+imgSrc+"&projectUsersStatus="+projectUsersStatus+"&projArchStatus="+projectArchStatus+"&agileNotificationType="+notificationType+"&agileNotificationId="+notTypeId+"&pId="+pId+"&epicCommDocId="+notCmtDocId+"&taskSourceId="+taskSourceId+"&tasksprintId="+tasksprintId+"&taskSprintGroupId="+taskSprintGroupId;
			  }else{
			     updateHistoryStatus(notCmtDocId,notificationType,menuType,agileProjId,projectType,projIdeatitle,projectArchStatus,imgSrc,projectUsersStatus,'',notificationType,epicId,pId,notificationId,ppId,notfEpicIds);
	    	  }
	      }else if(menuType =='idea'){
		      var projId=connNotUseId;
		      var projName=$('#notifProjImgNew_'+projId).attr('title');
		      projName=projName.replaceAll('CHR(41)',' ').replaceAll("CHR(26)",":").replaceAll("CH(39)","'").replaceAll("CH(40)",'"').replaceAll("CH(51)",'"').replaceAll("CH(52)","'");
		      var projImgSrc=$('#notifProjImgNew_'+projId).attr('src');
		      $('div#headertitle').text("");
	     	  var type=pId.split('_')[0];
	     	  var iNotType="";
	     	  if(notificationType == 'idea_share' || notificationType == 'topic_share'){
	     	     iNotType="ti_share";
	     	  }else if(notificationType == 'idea_comment' ||  notificationType == 'comments'){
	     	     iNotType="idea_comment";
	     	  }else if(notificationType == 'idea_document_link' ||  notificationType == 'idea_document' || notificationType == 'document' ||  notificationType == 'idea_link'){
	     	     iNotType="idea_document_link";
	     	  }
	     	  updateNotifRecentProject(projId);
	     	  updateHistoryStatus(notCmtDocId,iNotType,menuType,projId,projectType,projName,projectArchStatus,projImgSrc,projectUsersStatus,type,notificationType,notTypeId,'',notificationId); 
	     }else if(menuType =='projStatusComment'){
	          $("#loadingBar").show();  
              timerControl("start");
              $.ajax({
                url: path+"/landPageNotAction.do",
                type:"POST",
                data:{act:"updateNoteStatusNotifications", notIds:notificationId,type:notificationType},
                error: function(jqXHR, textStatus, errorThrown) {
                        checkError(jqXHR,textStatus,errorThrown);
                        $("#loadingBar").hide();
                        timerControl("");
                        }, 
                success:function(result){
                    checkSessionTimeOut(result);
			          var projName=$('#notifProjImgNew_'+connNotUseId).attr('title');
		              //projName=projName.replaceAll('CHR(41)',' ').replaceAll("CHR(26)",":").replaceAll("CH(39)","'").replaceAll("CH(40)",'"').replaceAll("CH(51)",'"').replaceAll("CH(52)","'");
		              var projImgSrc=$('#notifProjImgNew_'+connNotUseId).attr('src');
			           if(projectUsersStatus == 'PO'){
			               window.location = path+"/Redirect.do?pAct=projectStatusRedForPO&pId="+connNotUseId+"&stype="+menuType+"&notfUserId="+connNotUseId+"&projImgSrc="+projImgSrc+"&id="+notCmtDocId;
			           }else{
			               window.location = path+"/Redirect.do?pAct=projectStatusRedForSA&pId="+connNotUseId+"&stype="+menuType+"&notfUserId="+connNotUseId+"&projImgSrc="+projImgSrc+"&id="+notCmtDocId;
			           }
			           $("#loadingBar").hide();    
		                  timerControl("");
		                }
		       });
	     }	    
	 }
     
     function loadWorkSpaceApproveDetail(projId,userId,projectUsersStatus,notType,prjImagePath){
        window.location = path+"/Redirect.do?pAct=landwMenuProj&projectId="+projId+"&notifUserId="+userId+"&projectType="+projectUsersStatus+"&notType="+notType+"&prjImagePath="+prjImagePath;
     }
     
     function loadWorkSpaceDeniedDetail(projId,userId,prjImagePath,dUserId){
        $("#loadingBar").show();	
		timerControl("start");
		$("div#transparentDiv").show();
     	$("div#denyUserReasonBox").show();
     	$("img#denyUserPrjImg").attr('src',prjImagePath);
     	$.ajax({
			url: path+"/landPageNotAction.do",
			type:"POST",
			data:{act:"getDeniedUserReason", projId:projId},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
			result = result.split("*&*&*&");
			$("span#deniedUserContainer").html(result[0]);
			$("span#denyUserProjectName").html(result[1])
			$("#loadingBar").hide();	
			timerControl("");
			}
		});
     }
     
     function closeDenyUserReason(){
     	$("div#transparentDiv").hide();
     	$("div#denyUserReasonBox").hide();
     }
     
     function redirectToEmail(gEmailMsgId,emailId,NotificationType,ProjName,ProjId,ProjType,projImgSrc,projectArchStatus,projectUsersStatus,notificationId){
     	$("#loadingBar").show();	
		timerControl("start");
		updateNotifRecentProject(ProjId);
	    $.ajax({
			url: path+"/landPageNotAction.do",
			type:"POST",
			data:{act:"updateNoteStatusNotifications", notIds:notificationId,type:NotificationType},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
     			window.location = path+"/Redirect.do?pAct=loadEmailjsp&projId="+ProjId+"&projType="+ProjType+"&projName="+ProjName+"&projImgSrc="+projImgSrc+"&projArchStatus="+projectArchStatus+"&projectUsersStatus="+projectUsersStatus+"&notEmailId="+emailId+"&gEmailMsgId="+gEmailMsgId;
      		}
     	 });
      }
     
     function updateHistoryStatus(notCmtDocId,notType,menuType,projId,projectType,projName,projectArchStatus,projImgSrc,projectUsersStatus,type,notificationType,notTypeId,pId,notificationId,ppId,notfEpicIds){
        $("#loadingBar").show();	
		timerControl("start");
	    $.ajax({
			url: path+"/landPageNotAction.do",
			type:"POST",
			data:{act:"updateNoteStatusNotifications", notIds:notificationId,type:notType},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
			  if(menuType =='idea'){
			     window.location = path+"/Redirect.do?pAct=iMenu&projId="+projId+"&projType="+projectType+"&projName="+projName+"&projArchStatus="+projectArchStatus+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus+"&ideaType="+type+" &ideaNotificationType="+notificationType+"&ideaNotificationId="+notTypeId+"&ideaCmtDocId="+notCmtDocId;
			  }else if(notificationType =='agile_sprint_comment'){
                 window.location = path+"/Redirect.do?pAct=sprintMenu&projId="+projId+"&projType="+projectType+"&projName="+projName+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus+"&projArchStatus="+projectArchStatus+"&agileNotificationType="+notificationType+"&agileNotificationId="+notTypeId+"&pId="+pId+"&epicCommDocId="+notCmtDocId;
              }else if(menuType =='agile'){
			     window.location = path+"/Redirect.do?pAct=storyMenu&projId="+projId+"&projType="+projectType+"&projName="+projName+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus+"&projArchStatus="+projectArchStatus+"&agileNotificationType="+notificationType+"&agileNotificationId="+notTypeId+"&pId="+pId+"&epicCommDocId="+notCmtDocId+"&ppId="+ppId+"&notfEpicIds="+notfEpicIds;
			  }
			  $("#loadingBar").hide();	
			  timerControl("");
			}
		});
	 }
    
      /*function updateNotificationStatus(type){
       var addNotifId = 'notfSaveBtn';
       var notificationId='';
       var checkednotificationId='';
	   var notifCheckedCount = $('.nUserChecked').length;
	   if(addNotifId != null && addNotifId != 0 && addNotifId == 'notfSaveBtn') {
	     $('div.nUserChecked').each(function() {
	        notificationId = $(this).attr('id').split('_')[1];
	        if(notificationId){
	           checkednotificationId=checkednotificationId+notificationId+','
	        }
	     });
	   }
	   updateNotificationCheckedStatus(checkednotificationId,type);
	 }
	 function updateNotificationCheckedStatus(checkednotificationId){
        $("#loadingBar").show();	
		timerControl("start");
	    $.ajax({
			url: path+"/landPageNotAction.do",
			type:"POST",
			data:{act:"updateNotificationCheckedStatus", checkednotificationId:checkednotificationId},
			success:function(result){
			    if(result == "success"){
			      if(type == "landingPage"){
			        fetchNotificationData();
			      }else{
			        showCompleteNotifications();
			      }  
			    }
			}
		});
     }*/
     
     function changeNotificationCheckold(obj,type,historyStatus){
        var classObj = $(obj).attr('id');
        var notificationId = classObj.split('_')[1];
        //$("#loadingBar").show();	
		//timerControl("start");
		if(type == "landingPage"){
			        //fetchNotificationData();
			 $("#notifListMain_"+notificationId).remove();
			 if(!isiPad){
			     $("#NotfDataRight").mCustomScrollbar("update");
			 }
		}else{
		   //showCompleteNotifications();
			 var checkedClassStatus=$("#notid_"+notificationId).hasClass('unreadNotification');
			 if(checkedClassStatus){
			     $("#notid_"+notificationId).addClass('readNotification');
			     $("#notid_"+notificationId).removeClass('unreadNotification');
			 }else{
			     $("#notid_"+notificationId).removeClass('readNotification');
			     $("#notid_"+notificationId).addClass('unreadNotification');
			 }
		} 
	    $.ajax({
			url: path+"/landPageNotAction.do",
			type:"POST",
			data:{act:"updateNotificationCheckedStatus", checkednotificationId:notificationId,historyStatus:historyStatus},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
			}
		});
        /*var classObj = $(obj).attr('class');
        $(obj).removeClass();
        if(classObj == 'userUnChecked'){ 
		  $(obj).addClass('nUserChecked');
		  }
	    else {
		  $(obj).addClass('userUnChecked');
		  }
	    if($(".nUserChecked").length > 0){
	     	$("#notfSaveBtn").css("display","block");
		    $("#notfSaveBtn").attr('onclick','updateNotificationStatus("'+type+'")');
		}	
		else{
			$("#notfSaveBtn").css("display","none");
			$("#notfSaveBtn").attr('onclick','');
		}*/
     }
    
     function redirectToSysAdmin(notificationId,notificationType){
       $("#loadingBar").show();	
	   timerControl("start");
       $.ajax({
			url: path+"/landPageNotAction.do",
			type:"POST",
			data:{act:"updateNoteStatusNotifications", notIds:notificationId,type:notificationType},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
                window.location = path+"/Redirect.do?pAct=auMenu&notfType="+notificationType;
                $("#loadingBar").hide();	
	   			timerControl("");
            }
       });
     }
     
	 function closeCompNot(){
		$("div#userCompNotifications").hide();
		$('#transparentDiv').hide();
		var prjAdmin="loadAdminProjectNotificationCount";
		//top.frames["menuFrame"].window[prjAdmin]();
	}
	
	function loadIntegration(mAct){
		//window.location.href = path+"/Redirect.do?pAct="+mAct;
		switchZone('colabusIntegration');
		
	}

	
	function loadProjectDescription(){
			//$('#loadingBar').show();
			//timerControl("start");
			$("div#breadCrumProjDescDiv").show();
		 	$("div#transparentDiv").show();
			$('#projDescTitle').text(projtName);
			
				$.ajax({
					url: path+"/ActivityFeedAction.do",
					type:"POST",
					data:{act:"loadWrkSpaceProjDescription",projId:projectId},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
					    checkSessionTimeOut(result);
					    var jsonData = jQuery.parseJSON(result);
					    var desc = jsonData[0].desc;
					    desc = replaceSpecialCharacter(desc);
						desc = desc.replaceAll( "&lt;","<");
    					desc = desc.replaceAll( "&gt;",">");
					    $("div#projDescContainer").html(desc);
						if(!isiPad){
						   popUpScrollBar("projDescContainer");
			 			}
						$('#loadingBar').hide();
						timerControl("");
					}
				}); 
	}
	
	function closeProjDesc(){
	  $('#breadCrumProjDescDiv').hide();
	  $("div#transparentDiv").hide();
	  $('#projDescContainer').mCustomScrollbar("destroy");
	}
	
	
	
	/* Gmail email integration code */
	
	var timer="";
	var child="";
	var reConfigchild="";
	var configueStatus="false";
	function clickGoogleEmail(type,inputEmailId,colabusFetchType){
	    $('#loadingBar').show();
	    projectId='';
	    timerControl("start");
	    var mAct="wsGoogleEmail";
	 	$.ajax({
			url:path+'/GEServlet',
			type:'POST',
			dataType:'text',
			data:{action:'chkTkn'},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
					tkn = result;
					if(tkn !="success"){
					 //if(type == 'reconfig'){
					   //reConfigchild=window.open('https://accounts.google.com/Logout?continue=https%3A%2F%2Faccounts.google.com%2FServiceLogin%3Fsacu%3D1&il=true&zx=icxpgruz0yao','','toolbar=0,status=0,width=0,height=0,scrollbars=1');
					   //setTimeout(function(){
					     //reConfigchild.close();
					     $.ajax({
							url:path+'/GEServlet',
							type:'POST',
							dataType:'text',
							data:{action:'getGEAccessData',projectId:projectId,type:type,inputEmailId:inputEmailId,emailFetchType:emailFetchType},
							error: function(jqXHR, textStatus, errorThrown) {
					                checkError(jqXHR,textStatus,errorThrown);
					                $("#loadingBar").hide();
									timerControl("");
									}, 
							success:function(result){
								checkSessionTimeOut(result);
							       var data=result.split("@@@");
							       $('#gDrive').attr('url',''+data[0]+'');
							       child = window.open(''+data[0]+'','','toolbar=0,status=0,width=900,height=500,scrollbars=1');
								   timer = setInterval(checkChild, 500);
								}				
						  });  
					    //},2000);
					 /*}else{
					  $.ajax({
							url:path+'/GEServlet',
							type:'POST',
							dataType:'text',
							data:{action:'getGEAccessData',projectId:projectId,type:type},
							success:function(result){
							      var data=result.split("@@@");
							       $('#gDrive').attr('url',''+data[0]+'');
								   child = window.open(''+data[0]+'','','toolbar=0,status=0,width=900,height=500,scrollbars=1');
						           timer = setInterval(checkChild, 500);
								}				
							}); 
					 }*/
					 
					}else{
					   // confirmFun('Email is already configured.Do you want to reconfigure email?',"delete","reConfiureEmail");
						reConfiureEmail();
						$('#loadingBar').hide();
	  			       timerControl("");
	 				}
	 				
			   }
		   });
	}
	
	 function checkChild() {
	   var mAct="wsGoogleEmail";
	   if (child.closed) {
	    	clearInterval(timer);
	    	//$('#transparentDiv').hide();
	    	$.ajax({
				url:path+'/GEServlet',
				type:'POST',
				dataType:'text',
				data:{action:'checkConfiguredEmailId'},
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
				success:function(result){
					checkSessionTimeOut(result);
			    if(result == 'same mail'){
			      $.ajax({
					url:path+'/GEServlet',
					type:'POST',
					dataType:'text',
					data:{action:'chkTkn'},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                $("#loadingBar").hide();
							timerControl("");
							}, 
					success:function(result){
						checkSessionTimeOut(result);
						if(result=='error'){
						    $('#loadingBar').hide();
	  			   		    timerControl(""); 
						}else{
						   alertFun(getValues(companyAlerts,"Alert_EmailConfig"),'warning');
						   $("#configuredEmailImg").attr('src',path+'/images/onSwitch.png').attr('value','config');
		                   $("#configDiv").show();
	                	   $("#notconfigDiv").hide();
	 					   $('#loadingBar').hide();
	  			   		   timerControl("");
	 					}
				    }
			    });
			  }else if(result == 'diff mail'){
			    confirmReset(getValues(companyAlerts,"Alert_ReconfigDiffMail"),"close","reconfigDifferentMailOk","closereconfigDifferentMail");
			    timerControl(""); 
			  }else if(result=='error'){
				  $('#loadingBar').hide();
	  			  timerControl(""); 
			  }
			}
		});
	    }
	}
	
	function reconfigDifferentMailOk(){
 	   timerControl("start"); 
	   $.ajax({
			url:path+'/GEServlet',
			type:'POST',
			dataType:'text',
			data:{action:'chkTkn'},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
				if(result=='error'){
				   $('#loadingBar').hide();
 			   	   timerControl(""); 
				}else{
				    $.ajax({
						url:path+'/GEServlet',
						type:'POST',
						dataType:'text',
						data:{action:'insertLoggedUserDetails'},
						error: function(jqXHR, textStatus, errorThrown) {
				                checkError(jqXHR,textStatus,errorThrown);
				                $("#loadingBar").hide();
								timerControl("");
								}, 
						success:function(result){
							checkSessionTimeOut(result);
						    alertFun(getValues(companyAlerts,"Alert_EmailConfig"),'warning');
						    $("#configuredEmailImg").attr('src',path+'/images/onSwitch.png').attr('value','config');
		                    $("#configDiv").show();
	                	    $("#notconfigDiv").hide();
							$('#loadingBar').hide();
			 			    timerControl("");
						}
					});
				   
				}
			}
		});
	}
	function closereconfigDifferentMail(){
	  $('#loadingBar').hide();
 	  timerControl("");
	  $.ajax({
			url:path+'/GEServlet',
			type:'POST',
			dataType:'text',
			data:{action:'logout'},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
			   if(result == "success"){
			     configueStatus="true";
			     clickGoogleEmail('','');
			     //clickGoogleEmail('');
			   }
			}
	   });
	}
	
	function reconfigGoogleEmail(){
	     confirmFun(getValues(companyAlerts,"Alert_EmailCofigExp"),"delete","reConfiureEmail");
	}
	
	function reConfiureEmail(){
	  $.ajax({
			url:path+'/GEServlet',
			type:'POST',
			dataType:'text',
			data:{action:'logout'},
			error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					}, 
			success:function(result){
				checkSessionTimeOut(result);
			   if(result == "success"){
			     clickGoogleEmail('reconfig','');
			     //clickGoogleEmail('');
			   }
			}
	   });
	}

	function sendToSlack(projId,notifType,NotifId){
			// $.ajax({
			//    url:path + '/adminUserAction.do',
			//    type: "post",
			//    data:{act:"sendMessage", projId:projId, notifType: notifType, NotifId:NotifId},
			//   error: function(jqXHR, textStatus, errorThrown) {
		    //             checkError(jqXHR,textStatus,errorThrown);
		    //             $("#loadingBar").hide();
			// 			timerControl("");
			// 			}, 
			//    success:function(result){
				  // checkSessionTimeOut(result);
				  //if(result == "true"){
				  //	alertFun("Message successsfully sent to slack",'warning');
				  //	$("div#getMessage").html("");
				  //	$("#loadingBar").hide();
           			//timerControl("");
				  //}
				  //else {
				  //	alertFun("Message sending failed",'warning');
				  //	$("#loadingBar").hide();
                   // timerControl("");
				  //}
			//    }
			// }); 
	}
	var hashResult = "";
	 function ajaxCallforHashCode(menuType,feedId,type){
	 			hashResult = "";
	 			if (typeof prjid == "undefined") {
					prjid = "";
	 			}
				$.ajax({
		     	    url: apiPath+"/"+myk+"/v1/getProjectTagName?user_id="+userIdglb+"&company_id="+companyIdglb+"&project_id="+prjid+"",
					type:"GET",
					//data:{act:"featchProjectTagName", projId: projectId,menuType:menuType,type:type,feedId:feedId},
					error: function(jqXHR, textStatus, errorThrown) {
			                checkError(jqXHR,textStatus,errorThrown);
			                }, 
					success:function(result){
						
						var jsonData = result;
						result = preparingUIforHash(jsonData,projectId,type,feedId);
						hashResult = jsonData;
						if(type == 'main'){
							if(result == ''){
						   		$('.proTagNameId ul').html('No data found.');
							}else{
								$('.proTagNameId ul').html(result);
							}
						}else if(type == 'reply'){
							if(result == ''){
					   			$('.proTagReplyNameId ul').html('No data found.');
					   		}else{
						   		$('.proTagReplyNameId ul').html(result);
						   	}
						}else if(type == 'hashSearch'){
						   	if(result == ''){
								   $('#hashTagSearchInputBox ul').html('No data found.');
							}else{
									$('#hashTagSearchInputBox ul').html(result);
							}
						}else if(type == 'mytask'){
							if(result == ''){
						   		$('#hashTrendingCodesInTaskId ul').html('No data found.');
							}else{
								$('#hashTrendingCodesInTaskId ul').html(result);
							}
						}else if(type == 'projStatusComment'){
                            if(result == ''){
                                $('#hashSpeicalCharDis ul').html('No data found.');
                            }else{
                                $('#hashSpeicalCharDis ul').html(result);
                            }
                        }else if(type == 'editField'){
							if(result == ''){
					   			$('.proTagReplyNameId ul').html('No data found.');
					   		}else{
						   		$('.proTagReplyNameId ul').html(result);
						   	}
						}else if(type == 'cassandraSearch'){
						   	if(result == ''){
								   $('#hashTagSearchInputBox1 ul').html('No data found.');
							}else{
									$('#hashTagSearchInputBox1 ul').html(result);
							}
						}else{
							if(result == ''){
						   		$('.proTagSubReplyNameId ul').html('No data found.');
						   	}else{
							   $('.proTagSubReplyNameId ul').html(result);
							}
						}
					}
				}); 
 
 	 }
	 function preparingUIforHash(jsonData,projectId,type,feedId){
	 	var html="";
 		for(i=0;i<jsonData.length;i++){
 				var projectTagName=jsonData[i].project_tag_name;
				if(projectTagName == ""){
					projectTagName = '-';
				} 
 				html+="	<li style =\"font-size: 12px;overflow: hidden;color: black; margin: 4% !important;cursor:pointer;border-bottom: 1px solid #cccccc;\" onclick=\"addProjHashTag('"+projectTagName+"','"+type+"','"+feedId+"')\">"+projectTagName+"</li>";
 		}
 		
 		return html;
	}
	function addProjHashTag(projHashTagName,type,feedId){
		if(type == "main"){
	       $('.proTagNameId').hide();
	       var data=$('#main_commentTextarea').val();
	       var focusIndex = document.getElementById('main_commentTextarea').selectionStart;
	       var bTxt= data.substring(0,elementhashindex);
	       var aTxt = data.substring(focusIndex,data.length);
	       data = bTxt+projHashTagName+" "+aTxt;
	       $('#main_commentTextarea').val(data).focus();
	       
	    }else if(type == "reply"){
	      /*$( .proTagReplyNameId').hide();
	       var inpVal = document.getElementById('replyBlock_'+feedId).value;
	       var inpHashValPos= inpVal.slice(0,document.getElementById('replyBlock_'+feedId).selectionStart).length;
	       var output = [inpVal.slice(0, inpHashValPos), projHashTagName, inpVal.slice(inpHashValPos)].join('')+" ";
	       $('#replyBlock_'+feedId).val(output).focus();*/
	       
	       $('.proTagReplyNameId').hide();
	       var data=document.getElementById('replyBlock_'+feedId).value;
	       var focusIndex = document.getElementById('replyBlock_'+feedId).selectionStart;
	       var bTxt= data.substring(0,elementhashindex);
	       var aTxt = data.substring(focusIndex,data.length);
	       data = bTxt+projHashTagName+" "+aTxt;
	       $('#replyBlock_'+feedId).val(data).focus();
	    }else if(type == "editField"){
	    	$('.proTagReplyNameId').hide();
	    	 var data=$('#EdidCmtBlock_'+feedId).val();
		     var focusIndex = document.getElementById('EdidCmtBlock_'+feedId).selectionStart;
		     var bTxt= data.substring(0,elementhashindex);
		     var aTxt = data.substring(focusIndex,data.length);
		     data = bTxt+projHashTagName+" "+aTxt;
		    $('#EdidCmtBlock_'+feedId).val(data).focus();
	    }else if(type == "hashSearch"){
	    	$('#hashTagSearchInputBox').hide();
	    	 var data=$('#glbSearchInputBox').val();
	    	 var focusIndex = document.getElementById('glbSearchInputBox').selectionStart;
	    	 var bTxt= data.substring(0,elementhashindex);
	    	 var aTxt = data.substring(focusIndex,data.length);
	    	 data = bTxt+projHashTagName+" "+aTxt;
	    	$('#glbSearchInputBox').val(data).focus();
	    }else if(type == "mytask"){
	    	 $('.proTagNameId').hide();
	    	 var data=$('#myTaskComment').val();
	    	 var focusIndex = document.getElementById('myTaskComment').selectionStart;
	    	 var bTxt= data.substring(0,elementhashindex);
	    	 var aTxt = data.substring(focusIndex,data.length);
	    	 data = bTxt+projHashTagName+" "+aTxt;
	    	$('#myTaskComment').val(data).focus();
	    }else if(type == "mytaskNewUI"){
	    	 $('.proTagNameId').hide();
	    	 var data=$('#UserCommentNewUI').val();
	    	 var focusIndex = document.getElementById('UserCommentNewUI').selectionStart;
	    	 var bTxt= data.substring(0,elementhashindex);
	    	 var aTxt = data.substring(focusIndex,data.length);
	    	 data = bTxt+projHashTagName+" "+aTxt;
	    	$('#UserCommentNewUI').val(data).focus();
	    }else if(type == 'projStatusComment'){
             $('.proTagNameId').hide();
             var data=$('#statusComment').val();
             var focusIndex = document.getElementById('statusComment').selectionStart;
             var bTxt= data.substring(0,elementhashindex);
             var aTxt = data.substring(focusIndex,data.length);
             data = bTxt+projHashTagName+" "+aTxt;
            $('#statusComment').val(data).focus();              
        }else if(type == "cassandraSearch"){
	    	$('#hashTagSearchInputBox1').hide();
	    	 var data=$('#idToDisplaySpeechText').val();
	    	 var focusIndex = document.getElementById('idToDisplaySpeechText').selectionStart;
	    	 var bTxt= data.substring(0,elementhashindex);
	    	 var aTxt = data.substring(focusIndex,data.length);
	    	 data = bTxt+projHashTagName+" "+aTxt;
	    	$('#idToDisplaySpeechText').val(data).focus();
	    }else{
	       $('.proTagSubReplyNameId:visible').hide();
	       var data=document.getElementById('replyBlock_'+feedId).value;
	       var focusIndex = document.getElementById('replyBlock_'+feedId).selectionStart;
	       var bTxt= data.substring(0,elementhashindex);
	       var aTxt = data.substring(focusIndex,data.length);
	       data = bTxt+projHashTagName+" "+aTxt;
	       $('#replyBlock_'+feedId).val(data).focus();
	    }
	    
	    elementhashindex='';
		hashIndex ="";
				 
	 }	
  
	function sendToSpark(projId,notifType,NotifId){
			$.ajax({
			   url:path + '/adminUserAction.do',
			   type: "post",
			   data:{act:"sendSparkMessage", projId:projId, notifType: notifType, NotifId:NotifId},
			  error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						}, 
			   success:function(result){
				  // checkSessionTimeOut(result);
				  //if(result == "true"){
				  //	alertFun("Message successsfully sent to slack",'warning');
				  //	$("div#getMessage").html("");
				  //	$("#loadingBar").hide();
           			//timerControl("");
				  //}
				  //else {
				  //	alertFun("Message sending failed",'warning');
				  //	$("#loadingBar").hide();
                   // timerControl("");
				  //}
			   }
			}); 
	}
	
	
	//To register the user to the xmpp server while inviting the user 
	
	 function xmppRegister(newUserId){
		var domainUrl = window.location.href;
		if((domainUrl.indexOf("newtest") > -1) || (domainUrl.indexOf("www.colabus.com") > -1) || (domainUrl.indexOf("testcme.colabus.com") > -1) || (domainUrl.indexOf("cme.colabus.com") > -1)){
	  	$.ajax({
				url: path+"/connectAction.do",
				type:"POST",
				data:{act:"xmppNewCompanyUser",
						  //fname:fname,
						 // lname:lname,
						 // email:email,
						  newUserId:newUserId
							 },
				error: function(jqXHR, textStatus, errorThrown) {
		                checkError(jqXHR,textStatus,errorThrown);
		                $("#loadingBar").hide();
						timerControl("");
						},	 
				success:function(result){
				//alert("result---"+result);
				}
		   });
		 }
	 
	 
	 }
	
	
	/*ends here*/
	var projDDListData="";
	function showProjectDDList(limit){
		limit = typeof (limit) != 'undefined' ? limit : "";
		return new Promise( (resolve, reject) => {
			
					timerControl("start"); 
					$("#loadingBar").show();
					let jsonbody = { 
						"user_id" : userIdglb,
						// "ipadVal" : "",
						"sortType" : "",
						"type" : "", 
						"txt" : "", 
						"companyId" : companyIdglb, 
						"workspaceType" : "recent",   
						// "sortHideProjValue" : "",   
						"filterVal" : "",
						"limit":limit
					}
					checksession();
					$.ajax({
						url: apiPath+"/"+myk+"/v1/projectList",/// local : http://localhost:8080/v1/projectList server : apiPath+"/"+myk+"/v1/loadProjDDList"
						type:"POST",
						dataType:'json',
						contentType:"application/json",
						data: JSON.stringify(jsonbody),
						error: function(jqXHR, textStatus, errorThrown) {
								checkError(jqXHR,textStatus,errorThrown);
								$("#loadingBar").hide();
								timerControl("");
								}, 
						success : function(result){
						checkSessionTimeOut(result);
							
							$("#loadingBar").hide();
							timerControl("");

							resolve(result);//-----return promise
						}
					}); 
				
			
		});
 	}

	 //added copy of show the projectlist

	function showProjectDDListButton(limit){
		limit = typeof (limit) != 'undefined' ? limit : "";
		
				
			if(!$('#projListDDContainer').is(':visible')){
				if(projDDListData ==""){
					
					let jsonbody = { 
						"user_id" : userIdglb,
						// "ipadVal" : "",
						"sortType" : "",
						"type" : "", 
						"txt" : "", 
						"companyId" : companyIdglb, 
						"workspaceType" : "recent",   
						// "sortHideProjValue" : "",   
						"filterVal" : "",
						"limit":limit
					}
					checksession();
					$('#projListDDContainerSub').html("");
					//timerControl("start"); 
					$('#loadingBar').addClass('d-flex');
					$.ajax({
						url: apiPath+"/"+myk+"/v1/projectList",/// local : http://localhost:8080/v1/projectList server : apiPath+"/"+myk+"/v1/loadProjDDList"
						type:"POST",
						dataType:'json',
						contentType:"application/json",
						data: JSON.stringify(jsonbody),
						error: function(jqXHR, textStatus, errorThrown) {
								checkError(jqXHR,textStatus,errorThrown);
								$("#loadingBar").hide();
								timerControl("");
								}, 
						success : function(result){
						checkSessionTimeOut(result);
							projDDListData = result;
							if(projDDListData == ''){
							$('#projListDDContainerSub').html("<span>No projects found.</span>");
							}else{
								prepareProjListDdUI();
								prepareProjListOtherUI();
							}
							$('#loadingBar').addClass('d-none').removeClass('d-flex');
							//timerControl("");

						
						}
					}); 
				}
				$('#projListDDContainer').show();
				$('#projListDDImg').attr('src','/images/menus/dropup.svg').addClass('menuLinkActive');    
			}else{
				$('#projListDDContainer').hide();
				$('#projListDDImg').attr('src', '/images/menus/dropdown.svg').removeClass('menuLinkActive');
			}

	
 	}













  function prepareProjListDdUI() {
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	var UI = "";
	var recentFlag = false;
	var projArray = projDDListData[0].publicProjectList;
	for (var i = 0; i < projArray.length ; i++) {
	    	var jsonData = projArray;
			// var projectID = jsonData[i].project_id;
		    // var ProjTitle = jsonData[i].project_name; ///projTitle
			// var funcFlag = "";//jsonData[i].funcFlag
			// var funClick = "";//jsonData[i].funClick
			// var imageUrl = jsonData[i].imageUrl; // url for images
			// var imageClass = jsonData[i].imageClass;
			// var projName = jsonData[i].project_name;
			// var projectArchiveStatus = jsonData[i].project_status;
			// var cancelL = jsonData[i].cancelL;
			// var ctxPath = jsonData[i].ctxPath;
		    // projSettingL = jsonData[i].projSettingL;
			// var projectUsersStatus = jsonData[i].proj_user_status;
			// projName=projName.replaceAll("ch(20)","'");

			var projectID = jsonData[i].project_id;
	    	var ProjTitle = jsonData[i].project_name;//jsonData[i].projTitle
			var funcFlag = "";//jsonData[i].funcFlag
			var funClick = "";//jsonData[i].funClick
			var imageUrl = jsonData[i].imageUrl; // url for images imageUrl
			var imageClass = "";///jsonData[i].imageClass
			var projName = jsonData[i].project_name; ///project_name ///projName
			var projectArchiveStatus = jsonData[i].project_user_status;///project_user_status///status
			var cancelL = "";///jsonData[i].cancelL
			var ctxPath = jsonData[i].ctxPath;
	    	projSettingL = "";///jsonData[i].projSettingL
			var projectUsersStatus = jsonData[i].project_user_role; ///project_user_role//proj_user_status
			var projAccessStatus = jsonData[i].projAccessStatus;
			projName=projName.replaceAll("ch(20)","'");


			if(jsonData[i].project_status=="I"){
				ProjTitle = "Click to unlock"
			}


			// if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="PO" && jsonData[i].project_status=="A" )){
			// 	funcFlag = "psubscribed";
			// 	funClick = "readMoreProjects(this);";
			// }
	
			if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="I" )){
				// funcFlag = "pLocked";
				// funClick = "readMoreProjects(this);";	
				imageUrl = imageUrl+(""+ctxPath+"/images/Projects/locked_project.png");
				imageClass ="display:none";
				// funClick = "unlockProject(this);";
			}
	
			// if( ( projectUsersStatus=="TM" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="SU" && jsonData[i].project_status=="Y" )){
			// 	funcFlag = "psubscribed";
			// 	funClick = "readMoreProjects(this);";
			// }
				let projArchStatus =jsonData[i].project_status;
				let projectType = "";
				if(projectUsersStatus == "PO"){
					projectType="MyProjects";
				}else{
					projectType="SharedProjects";
				} 
				let prjImagePath =jsonData[i].imageUrl;

		    if((jsonData[i].project_status !="I" && typeof projectId == 'undefined') || (jsonData[i].projStatus !="I" && projectId != projectID) ){
		        recentFlag = true;
		        
		        UI += "	 <div id=\"proj_"+projectID+"\" class='d-flex justify-content-left w-100 p-1 text-dark' style=\"border-bottom:1px solid #cccccc;height: 40px;cursor:pointer;\" onclick='showProjectDDListButton(); projectRedirection( "+"\""+projectID+"\","+"\""+ProjTitle.replaceAll("'","CHR(39)")+"\","+"\""+projectUsersStatus+"\","+"\""+projArchStatus+"\","+"\""+projectType+"\","+"\""+prjImagePath+"\""+",\"projecttoproject\")'>"
		         if(projectUsersStatus=="PO"){
				  UI+=" 	<div id=\"projectName_"+ projectID+ "\"  style=\"display:none;\" >"+ projName+ "</div>"
					+ " 	<div id=\"projectType_"+ projectID+ "\"  style=\"display:none;\" >MyProjects</div>"
					+ " 	<div id=\"projectid"+ projectID+ "\" value=\""+projectID+"\"  style=\"display:none;\" ></div>"
					+ " 	<div id=\"projectArchiveStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectArchiveStatus+ "</div>"
					+ " 	<div id=\"projectUsersStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectUsersStatus+ "</div>";
	             }else{
	             	
	              UI+="		<div id=\"projectName_"+ projectID+ "\"  style=\"display:none;\" >"+ projName+ "</div>"
					+ " 	<div id=\"projectType_"+ projectID+ "\"  style=\"display:none;\" >SharedProjects</div>"
					+ " 	<div id=\"projectid"+ projectID+ "\" value=\""+projectID+"\"  style=\"display:none;\" ></div>"
					+ " 	<div id=\"projectArchiveStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectArchiveStatus+ "</div>"
					+ " 	<div id=\"projectUsersStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectUsersStatus+ "</div>";
	             }
		        UI += "	    <img id=\"projectImageId_"+ projectID+ "\" class='lozad' style=\"height: 27px;width: 27px;margin-top: 1%;border: 1px solid #cccccc;\" title=\""+ProjTitle+"\" onerror=\"userImageOnErrorReplace(this);\" src='/images/workspace/projectImage.svg' data-src=\""+ imageUrl+"?"+d.getTime()+ "\" >"
		        UI += "	    <span class='defaultExceedCls ml-2 mt-1' style=\"width:78%;font-size:13px;\" title=\""+ProjTitle+"\">"+projName+"</span>"
		        UI += "	 </div>"
		    
		    }
	}
    UI=UI.replaceAll("ch(20)","'").replaceAll("ch(30)","chr(dbl)").replaceAll("ch(50)","[").replaceAll("ch(51)","]").replaceAll("ch(curly)","{").replaceAll("ch(clcurly)","}").replaceAll("ch(backslash)","\\");	
	$('#projListDDContainerSub').append(UI);
	observer.observe();//---- to lazy load user images
  }

  function prepareProjListOtherUI(){
	var UI = "";
	var recentFlag = false;
	var projArray = projDDListData[0].myProjectList;
	for (var i = 0; i < projArray.length ; i++) {
	    	var jsonData = projArray;
			// var projectID = jsonData[i].project_id;
		    // var ProjTitle = jsonData[i].projTitle;
			// var funcFlag = jsonData[i].funcFlag;
			// var funClick = jsonData[i].funClick;
			// var imageUrl = jsonData[i].imageUrl; // url for images
			// var imageClass = jsonData[i].imageClass;
			// var projName = jsonData[i].project_name;
			// var projectArchiveStatus = jsonData[i].project_status;
			// var cancelL = jsonData[i].cancelL;
			// var ctxPath = jsonData[i].ctxPath;
		    // projSettingL = jsonData[i].projSettingL;
			// var projectUsersStatus = jsonData[i].proj_user_status;
			// projName=projName.replaceAll("ch(20)","'");



			var projectID = jsonData[i].project_id;
	    	var ProjTitle = jsonData[i].project_name;//jsonData[i].projTitle
			var funcFlag = "";//jsonData[i].funcFlag
			var funClick = "";//jsonData[i].funClick
			var imageUrl = jsonData[i].imageUrl; // url for images imageUrl
			var imageClass = "";///jsonData[i].imageClass
			var projName = jsonData[i].project_name; ///project_name ///projName
			var projectArchiveStatus = jsonData[i].project_user_status;///project_user_status///status
			var cancelL = "";///jsonData[i].cancelL
			var ctxPath = jsonData[i].ctxPath;
	    	projSettingL = "";///jsonData[i].projSettingL
			var projectUsersStatus = jsonData[i].project_user_role; ///project_user_role//proj_user_status
			var projAccessStatus = jsonData[i].projAccessStatus;
			projName=projName.replaceAll("ch(20)","'");
			let projArchStatus =jsonData[i].project_status;
			let projectType = "";
			if(projectUsersStatus == "PO"){
			projectType="MyProjects";
			}else{
				projectType="SharedProjects";
			} 
			let prjImagePath =jsonData[i].imageUrl;

			if(jsonData[i].project_status=="I"){
				ProjTitle = "Click to unlock"
			}


			// if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="PO" && jsonData[i].project_status=="A" )){
			// 	funcFlag = "psubscribed";
			// 	funClick = "readMoreProjects(this);";
			// }
	
			if( ( projectUsersStatus=="PO" && jsonData[i].project_status=="I" )){
				// funcFlag = "pLocked";
				// funClick = "readMoreProjects(this);";	
				imageUrl = imageUrl+(""+ctxPath+"/images/Projects/locked_project.png");
				imageClass ="display:none";
				// funClick = "unlockProject(this);";
			}
	
			// if( ( projectUsersStatus=="TM" && jsonData[i].project_status=="Y" ) || ( projectUsersStatus=="SU" && jsonData[i].project_status=="Y" )){
			// 	funcFlag = "psubscribed";
			// 	funClick = "readMoreProjects(this);";
			// }




		    if((jsonData[i].project_status !="I" && typeof projectId == 'undefined') || (jsonData[i].projStatus !="I" && projectId != projectID) ){
		        recentFlag = true;
		        
		        UI += "	 <div id=\"proj_"+projectID+"\" class='d-flex justify-content-left w-100 p-1 text-dark' style=\"border-bottom:1px solid #cccccc;height: 40px;cursor:pointer;\"  onclick='showProjectDDListButton(); projectRedirection( "+"\""+projectID+"\","+"\""+ProjTitle.replaceAll("'","CHR(39)")+"\","+"\""+projectUsersStatus+"\","+"\""+projArchStatus+"\","+"\""+projectType+"\","+"\""+prjImagePath+"\""+",\"projecttoproject\")'>"
		         if(projectUsersStatus=="PO"){
				  UI+=" 	<div id=\"projectName_"+ projectID+ "\"  style=\"display:none;\" >"+ projName+ "</div>"
					+ " 	<div id=\"projectType_"+ projectID+ "\"  style=\"display:none;\" >MyProjects</div>"
					+ " 	<div id=\"projectid"+ projectID+ "\" value=\""+projectID+"\"  style=\"display:none;\" ></div>"
					+ " 	<div id=\"projectArchiveStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectArchiveStatus+ "</div>"
					+ " 	<div id=\"projectUsersStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectUsersStatus+ "</div>";
	             }else{
	             	
	              UI+="		<div id=\"projectName_"+ projectID+ "\"  style=\"display:none;\" >"+ projName+ "</div>"
					+ " 	<div id=\"projectType_"+ projectID+ "\"  style=\"display:none;\" >SharedProjects</div>"
					+ " 	<div id=\"projectid"+ projectID+ "\" value=\""+projectID+"\"  style=\"display:none;\" ></div>"
					+ " 	<div id=\"projectArchiveStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectArchiveStatus+ "</div>"
					+ " 	<div id=\"projectUsersStatus_"+ projectID+ "\"  style=\"display:none;\" >"+ projectUsersStatus+ "</div>";
	             }
		        UI += "	    <img id=\"projectImageId_"+ projectID+ "\" class='lozad' style=\"height: 27px;width: 27px;margin-top: 1%;border: 1px solid #cccccc;\" onerror=\"userImageOnErrorReplace(this);\" title=\""+ProjTitle+"\" src='/images/workspace/projectImage.svg' data-src=\""+ imageUrl+"?"+d.getTime()+ "\" >"
		        UI += "	    <span class='defaultExceedCls ml-2 mt-1' style=\"width:78%;font-size:13px;\"  title=\""+ProjTitle+"\">"+projName+"</span>"
		        UI += "	 </div>"
		    
		    }
	}
    UI=UI.replaceAll("ch(20)","'").replaceAll("ch(30)","chr(dbl)").replaceAll("ch(50)","[").replaceAll("ch(51)","]").replaceAll("ch(curly)","{").replaceAll("ch(clcurly)","}").replaceAll("ch(backslash)","\\");	
	$('#projListDDContainerSub1').append(UI);
	observer.observe();//---- to lazy load user images
  }

 function loadDDProj(obj){
 	var projectId = obj.id.split('_')[1];
	var projType = $("#projectType_" + projectId).text();
	var projName = $("#projectName_" + projectId).text();
	var projectUsersStatus=$("#projectUsersStatus_"+ projectId).text();
	var projtitle =  $("#projectImageId_" + projectId).attr("title");
	var projArchStatus = $("div#projectArchiveStatus_" + projectId).text();
	var projImgSrc = $("img#projectImageId_" + projectId).attr('src');
	var mAct = "LoadActivityFeed";
	var projNme=projName.replaceAll("CHR(39)","'").replaceAll('CHR(41)',' ').replaceAll("CH(39)","'").replaceAll("CH(40)",'"').replaceAll("CH(51)",'"').replaceAll("CH(52)","'");
    checksession();
	timerControl("start"); 
	$("#loadingBar").show();
	$.ajax({
		url : apiPath+"/"+myk+"/v1/updateRecentProjectData?projectId="+projectId+"&userId="+userIdglb,
		type : "PUT",
		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					},
		success : function(result){
	      /* window.location.href = path+"/Redirect.do?pAct="+mAct+"&projId=" + projectId
			+ "&projType=" + projType + "&projName=" + projName
			+ "&projArchStatus=" + projArchStatus + "&projImgSrc=" + projImgSrc + "&projectUsersStatus=" + projectUsersStatus;
			 */
		if(result == "success"){	
		  /* window.sessionStorage.projectidglb = projectId;
		  window.sessionStorage.projectnameglb = projNme;
		  window.sessionStorage.projectimageglb = projImgSrc;	
		  var rid = roleid.split("\"")[1];	 */
		  loadSubPageContent(mAct,projectId,projNme,projectUsersStatus,projArchStatus,projType,projImgSrc);
		   
		  //window.location = hostname+"/postlogin?pAct="+mAct+"&roleid="+rid;
		} 
		  $("#loadingBar").hide();
          timerControl("");
		}
	});
 }
 
 function alignDDList(obj){
	 var t=$(obj).offset().top;
	 var oh=$(window).height()-t;
	 var h=$('#projListDDContainer').height();
	 if(oh < h){
		 $('#projListDDContainer').height(oh-30);
	 }else{
		 $('#projListDDContainer').height(300);
	 }
 }
 
 function getDomain(){
 	var domain = location.protocol + "//" + location.host;
 	return domain;
 }

//--------below code is used for imagecrop---
 var imageCrop =""; 
 function initCrop(imgSrc){
	$('#profilepictransparentDiv').show();
    $('#croppingImg').attr('src', imgSrc);
	imageCrop = $('#croppingImg');
    $('#cropImg').css('display','block');
	imageCrop.cropper({
	        aspectRatio: 1/ 1
	});
	var cropObj = imageCrop.data('cropper');
	$(".vanilla-rotate").on("click",function(){
  	     cropObj.rotate(parseInt($(this).data('deg')));
  	  });
  	  $(".vanilla-zoom-in").on("click",function(){
  	     cropObj.zoom(0.1);
  	  });
  	  $(".vanilla-zoom-out").on("click",function(){
  	     cropObj.zoom(-0.1);
  	  });
 }

 function  closeCropper() {
	 $('#profilepictransparentDiv').hide();
	 $('input[type="file"]').val('');
     $('#croppingImg').cropper("destroy");
     $('#croppingImg').attr('src','');
     $('#cropImg').hide();
     $("#upload-result").off("click");
     $(".vanilla-rotate,.vanilla-zoom").off("click");
 }
 
 
function roman_to_Int(str1) {
	str1= str1.toLowerCase();
	if(str1 == null) return -1;
	var num = char_to_int(str1.charAt(0));
	var pre, curr;
	
	for(var i = 1; i < str1.length; i++){
		curr = char_to_int(str1.charAt(i));
		pre = char_to_int(str1.charAt(i-1));
		if(curr <= pre){
			num += curr;
		} else {
			num = num - pre*2 + curr;
		}
	}
	return num;
}

function char_to_int(c){
	switch (c){
		case 'i': return 1;
		case 'v': return 5;
		case 'x': return 10;
		case 'l': return 50;
		case 'c': return 100;
		case 'd': return 500;
		case 'm': return 1000;
		default: return -1;
	}
}	
function daysInMonth(m, y) { // m is 0 indexed: 0-11
    switch (m) {
        case 1 :
            return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
        case 8 : case 3 : case 5 : case 10 :
            return 30;
        default :
            return 31
    }
}

function tConvert (time) {
	  // Check correct time format and split into components
	  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/)   || [time];

	  if (time.length > 1) { // If time format correct
	    time = time.slice (1);  // Remove full string match value
	    time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
	    time[0] = +time[0] % 12 || 12; // Adjust hours
	  }
	  return time.join (''); // return adjusted time or original string
	}


/* **************** Function ends for Document upload ******************* */

 function isNonLatinCharacters(s) {
	    return /[^\u0000-\u007F]/.test(s);
	}
 
 /* **************** If text has link such as http:// or https:// or www. this function is used************ */
 
  function TextLink(data){
	  data = data.replaceAll("&#58;",":");
	  if(data.includes("http://") || data.includes("https://") || data.includes("www.")){  
		    var firstString="";
		    var secondString="";
		    if(data.indexOf("http://") > -1){
		    	if(data.indexOf("http://") >0 ){
		    		firstString = data.substring(0, data.indexOf("http://"));
		    	}
		        secondString = data.substring(data.indexOf("http://"));
		    
		    }else if(data.indexOf("https://") > -1){
		    	if(data.indexOf("https://") >0 ){
		    		firstString = data.substring(0, data.indexOf("https://"));
		    	}
		        secondString = data.substring(data.indexOf("https://"));
		    
		    }else if(data.indexOf("www.") > -1 ){
		    	if(data.indexOf("www.") >0 ){
		    		firstString = data.substring(0, data.indexOf("www."));
			    }
		    	secondString = data.substring(data.indexOf("www."));
		    }
		    secondString = secondString.split(" ");
		    var link = secondString[0];
		    var linkRef = link;
		    if(data.indexOf("http://") < 0 && data.indexOf("https://") < 0 ){
		    	linkRef = "http://"+linkRef;
		    }
		    var words = '<a style=\"text-decoration:underline; color:blue;\" target=\"_blank\" href="' + linkRef + '">' + link + '</a>';
		    secondString[0] = words;
		    var joinName = secondString.join(' ');
		    var Names = firstString.concat(joinName);
		    return Names;
	  
	  }else if(data.includes(".com") || data.includes(".in") || data.includes(".co") || data.includes(".net")){  
		    var firstString="";
		    var secondString="";
		    
		    if(data.lastIndexOf(".com") > -1  && data.lastIndexOf(".com") > 0){
		    	firstString = data;
		    }else if(data.lastIndexOf(".in") > -1 && data.lastIndexOf(".in") > 0){
		        firstString = data;
		    }else if(data.lastIndexOf(".co") > -1 && data.lastIndexOf(".co") > 0){
		    	firstString = data;
		    }else if(data.lastIndexOf(".net") > -1 && data.lastIndexOf(".net") > 0){
		    	firstString = data;
		    }
		    
		    var link = firstString;
		    var linkRef =  "http://"+link;
		    var words = '<a style=\"text-decoration:underline; color:blue;\" target=\"_blank\" href="' + linkRef + '">' + link + '</a>';
		    firstString = words;
		    var joinName = firstString;
		    return joinName;

	  }else{
		  return data;
	  }
  }


  function loadProjectTabData(menuObj, mAct){ // -------triggered on click of menus inside workspace.
	
	if($(menuObj).hasClass('menuLinkActive')){ //------checking menu is already active or not 
		if(mAct=='landingpageTOtz' || mAct=='tz'){
			loadActivityFeed('', 'firstClick'); //----instead of switching just calling the function to load data
		}else if(mAct=='loadTaskPage'){
			fetchTasks('','firstClick');
		}else if(mAct=='ideasMenu'){
			
		}else if(mAct=='agile'){
		
		}else if(mAct=='sprint'){
		
		}else if(mAct=='wsdocument'){
			
		}else if(mAct=='contacts'){

		}else if(mAct=='wsTeam'){

		}else if(mAct=='colabusIntegration'){

		}else if(mAct=='emailMenu'){
			$(".breadCrumMenuName").text("Email");
			emailLayout();
          	getEmail();
		}else{
			comingsoon();
		}
	}else{
		menuLinkActive(menuObj);
		switchZone(mAct);
		//window.location.href ="${path}/Redirect.do?pAct="+mAct+"&projId="+projectId+"&projType="+projType+"&projName="+projtName+"&projArchStatus="+archivedstatus+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus;
		loadSubPageContent(mAct,prjid,projtName,projectUsersStatus,archivedstatus,projType,projImgSrc);
		
	}
}

function menuLinkActive(menuObj){
	$('a.menuLink').removeClass('menuLinkActive');
	$(menuObj).addClass('menuLinkActive');
}

function projectRedirection(projId,projName,projUserStatus,projArchStatus,projectType,prjImagePath,place){//--- triggered onclick of recent project in recent section in landing page.
    $("#loadingBar").show();
    timerControl("start");
	
    var mAct ='';
	if(place=="landingtoproject"){
		mAct="landingpageTOtz";
	}else if(place=="projecttoproject"){
		mAct=pageAct;
	}else{
		if(userroleid =='6' || userroleid =='7'){  //-------------------> validation only for Terafina or business solutions
			mAct="sprintMenu";
		}else{
			mAct="tz";//LoadActivityFeed    temporary
		}
	}
    
	if(mAct=="tz" || mAct=="landingpageTOtz"){
		index = 0;
		limitAct = 0;
	}
	from="";
    var projNme=projName.replaceAll("CHR(39)","'").replaceAll('CHR(41)',' ').replaceAll("CH(39)","'").replaceAll("CH(40)",'"').replaceAll("CH(51)",'"').replaceAll("CH(52)","'");
    checksession();	  
	$.ajax({
		url : apiPath+"/"+myk+"/v1/updateRecentProjectData?projectId="+projId+"&userId="+userIdglb,
		type : "PUT",
		error: function(jqXHR, textStatus, errorThrown) {
	                checkError(jqXHR,textStatus,errorThrown);
	                $("#loadingBar").hide();
					timerControl("");
					},
		success : function(result){
		if(result == "success"){
			//window.location = hostname+"/postlogin?pAct="+mAct+"&projId="+projId+"&projType="+projectType+"&projName="+projNme+"&projImgSrc="+prjImagePath+"&projectUsersStatus="+projUserStatus+"&projArchStatus="+projArchStatus;
		  //window.sessionStorage.projectidglb = projId;
		  //window.sessionStorage.projectnameglb = projName;
		  //window.sessionStorage.projectimageglb = prjImagePath;
		  //window.sessionStorage.projUserStatus = projUserStatus; 
		  //var rid = roleid.split("\"")[1];	
		  $('img.breadcrumImg').attr('src', '').attr('src', prjImagePath).attr('title', projNme);
          $('span.breadCrumProjName').text(projNme);
		  loadSubPageContent(mAct,projId,projNme,projUserStatus,projArchStatus,projectType,prjImagePath);
          //window.location = hostname+"/postlogin?pAct="+mAct+"&roleid="+rid;
		}	
		  
		  $("#loadingBar").hide();
          timerControl("");
		}
	}); 
    
          
  }

  function confirmFunNew(info,type,execFunc,param1,param2,param3,param4,param5){	  
     if(!info || !execFunc || !type){
            throw new Error("Please enter all the required config options!");
     }
     
     if(type=='clear'){
        name="BoxConfirmClear";
     }else if(type=='close'){
        name="BoxConfirmClose";
     }else if(type=='delete'){
        name="BoxConfirmDelete";
     }else{
        name="BoxConfirmReset";
     }
     
    
    retFunc=execFunc; 
    var html='<div id="BoxOverlay" ></div>'+
    		 '<div id="confirmDiv" class="alertMainCss">'+
    		 '  <div id="confirmCloseDiv" title="'+getValues(companyLabels,"Close")+'" ></div> '+
             //'  <img src="${path}/images/Idea/taskCancel.png"  title="close" style="float: right;cursor: pointer;margin:2px;" >'+
             '  <div style="box-shadow: 0px 0px 20px #000000;" >'+
             '   <div style="box-shadow: 0 0 0 0 #000000; padding: 20px 20px 0;">'+
             '    <div id="confirm-BoxContenedor" class="'+name+'">'+
             '     <span id="confirmContent">'+info+'</span>'+   
             '     <div id="confirm-Buttons" class="alertBtnCss">'+
             '       <input id="BoxConfirmBtnOk" type="submit" value="'+getValues(companyLabels,"OK")+'"  >'+
             '       <input id="BoxConfirmBtnCancel" type="submit" value="'+getValues(companyLabels,"Cancel")+'"  >'+
             '     </div>'+
             '    </div>'+
             '  </div>'+
             ' </div>'+
             '</div>';
          
          $('body').find('#BoxOverlay').remove();
          $('body').find('#confirmDiv').remove();
          //setTimeout(function(){  
                $('body').append(html);
                //$('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').attr("onclick",retFunc);
               
               $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnOk').click(function(){
                 hideConfirmBox();
                // top.frames["menuFrame"].window[retFunc]();
                window[retFunc](param1,param2,param3,param4,param5);
		       });
		        $('body').find('#confirmDiv').find('#confirm-Buttons').children('#BoxConfirmBtnCancel').click(function(){
		          hideConfirmBox();
		           if(execFunc == 'OptionalDrives'){		        	   
		   	         glbFileContents();	
		           }
		        });
		        $('body').find('#confirmDiv').children('#confirmCloseDiv').click(function(){
		          hideConfirmBox();
		        });
       //},50);    
}

function changeNotificationCheck(obj,type,historyStatus){
	checksession();
	var notificationId = $(obj).attr('notifid');
	$('#loadingBar').addClass('d-flex').removeClass('d-none');
	$.ajax({
		url: apiPath+"/"+myk+"/v1/updateNotificationCheckedStatus?checkednotificationId="+notificationId+"&historyStatus="+historyStatus,
		type:"PUT",
		error: function(jqXHR, textStatus, errorThrown) {
				checkError(jqXHR,textStatus,errorThrown);
				$('#loadingBar').addClass('d-none').removeClass('d-flex');
				}, 
		success:function(result){
			if(type == 'landingPage' && notifFilterTab == "unread"){
				$("#notifListMain_"+notificationId).html("<div class='py-0 alert alert-dismissible m mb-2 w-75 mx-auto' style='text-align:center;color: blue;font-size: 12px;'>Notification Updated</div>").delay(1000).hide(500); //.delay(500)
				addvalue = addvalue-$("#notificationContainer").height()+400;
			}else if(type == 'landingPage' && notifFilterTab != "unread"){
				$("#markAsReadButton_"+notificationId).text("Notification Updated").delay(1000).hide(500);
				setTimeout( function(){
					$("#markAsReadButton_"+notificationId).remove();
				},1500);
			} else if(type == 'desktopNotification'){
				$("#dNotification_"+notificationId).remove();
			}
			$('#loadingBar').addClass('d-none').removeClass('d-flex');
			
		}
	});
	
 }

 function generateRandomColor(){
	const randomColor = Math.floor(Math.random()*16777215).toString(16);
	return "#" + randomColor;
 }