/*
 *  jquery-hortree - v1.0.1
 *  Render an horizontal hierarchical tree from a JSON schema.
 *  https://github.com/alesmit/jquery-hortree
 *
 *  Made by Alessandro Mitelli
 *  Under MIT License
 */
; (function ($, window, document, undefined) {

    "use strict";

    var entryIdIndex = 0;

    function branch(list) {
        var html = [];
        html.push('<div class="hortree-branch">');
        $(list).each(function () {
            html.push(branchList(this));
        });
        html.push('</div>');
        return html.join("\n");
    }

    function branchList(elem) {

        entryIdIndex++;
        var html = [];
        html.push('<div class="hortree-entry" data-entry-id="' + entryIdIndex + '">');

        if (elem.tooltip && elem.tooltip.toString().trim() !== '') {

            html.push('<div class="hortree-label hortree-tooltip">');
            html.push('<span class="hortree-tooltip-text">' + elem.tooltip + '</span>');
            html.push(elem.description);
            html.push('</div>');

        } else {
            let style = "";
            if (elem.pointer_event == 'Y') {
                style = "pointer-events:none;"
            }

            html.push('<div class="hortree-label" onclick="showTaskUI(\'' + elem.id + '\',\'' + elem.description + '\');" id="block_' + elem.id + '" style="z-index:0;' + style + 'border:2px solid #f6b847">'

                // +'<div  class="mx-2" >'+ elem.description +'</div>'

                // +'<div>'

                //     +'<img id="" src="images/conversation/three_dots.svg" onclick="showOptions('+elem.id+');event.stopPropagation();" class="ml-1 position-relative" style="width:20px;cursor:pointer;" draggable="false">'
                // +'</div>'
                // +'<div class="position-absolute rounded wfOp p-1" id="WfOption_'+elem.id+'"  style="text-align: left;width: 102px;border: 1px solid rgb(193, 197, 200);background-color: rgb(255, 255, 255);color: black;font-size: 12px;box-shadow: rgba(0, 0, 0, 0.18) 0px 6px 12px;display: none;left: 24px;top: 0px;"><div  onclick="addStep('+elem.id+');event.stopPropagation();" class="pl-2 pb-1 " style="height:24px;"><span style="cursor:pointer;">Add Steps</span></div><div class="pl-2 pb-1 " style="height:24px;"><span style="cursor:pointer;">Color</span></div><div onclick="removeStep('+elem.id+');event.stopPropagation();"  class="pl-2 pb-1 mb-2 " style="height:24px;" onclick="openProjects(5051);"><span style="cursor:pointer;">Delete</span></div></div>'

                + '<div class="d-flex justify-content-end">'
                + '<div style="position:absolute;top:-15px;left:-5px" >'
                + '<img  id="active_block_' + elem.id + '" onclick="makeActiveInactive(this,\'' + elem.id + '\');event.stopPropagation();" title="Active" style="width:10px;cursor:pointer" src="images/task/activestep.svg"> '

                + '</div>'
                + '<div class="ml-2">'

                + '<img id="documentMand_' + elem.id + '" src="/images/idea/document.svg"  class="ml-1 position-relative"   title="Document Mandatory"  style="width:16px;cursor:pointer;display:none" draggable="false"  onclick="checkDocumentMandate(' + elem.id + ');event.stopPropagation();">'
                + '<img id="docstep_' + elem.id + '" src="/images/task/attach.svg" onclick="" class="ml-1 position-relative stepdoc"      title="Document "   style="width:16px;cursor:pointer;display:none" draggable="false">'

                + '<img id="back_' + elem.id + '" src="/images/task/arrow_left.svg " onclick="reOrder(\'' + elem.id + '\',\'backward\');event.stopPropagation();" class="ml-1 position-relative"   title="Move left"  style="width:15px;cursor:pointer;display:none" draggable="false">'
                + '<img id="forward_' + elem.id + '" src="/images/task/arrow_right.svg"  class="position-relative"   title="Move right"  style="width:15px;cursor:pointer;margin-left:0px;display:none" draggable="false" onclick="reOrder(\'' + elem.id + '\',\'forward\');event.stopPropagation();">'
                + '<img id="depWF_' + elem.id + '" src="/images/task/workflow.svg"  class="position-relative" id="ac_' + elem.id + '"  title="Dependency Workflow"  style="width:18px;cursor:pointer;margin-top:-1px;margin-left: 5px;display:none" draggable="false" onclick="fetchWorkflowDependency(\'' + elem.id + '\',\'sublev\');event.stopPropagation();">'
                + '</div>'
                + '<div>'

                + '<img id="" src="images/conversation/three_dots.svg" onclick="showOptions(\'' + elem.id + '\');event.stopPropagation();" class="ml-1 position-relative"    style="width:20px;cursor:pointer;" draggable="false">'
                + '</div>'
                + '<div class="position-absolute rounded wfOp p-1  justify-content-start  align-items-start" id="WfOption_' + elem.id + '"  onmouseleave="showColor(\'hide\');" style="text-align: left;width: 144px;border: 1px solid rgb(193, 197, 200);background-color: rgb(255, 255, 255);color: black;font-size: 12px;box-shadow: rgba(0, 0, 0, 0.18) 0px 6px 12px;display: none;left: 160px;top: 0px;"><div   onclick="addStep(\'' + elem.id + '\');event.stopPropagation();" class="pl-2 pb-1 stepop " style="height:26px;"><span style="cursor:pointer;">Add Steps</span></div>'


                + '<div  class="pl-2 pb-1 stepop" style="height:26px;" onclick="checkDocumentMandate(\'' + elem.id + '\');event.stopPropagation();"><span style="cursor:pointer;">Mandatory Attachment</span></div><div onclick="showReorder(\'' + elem.id + '\');event.stopPropagation();"  class="pl-2 pb-1 stepop" style="height:26px;" ><span  style="cursor:pointer;">Reorder</span></div><div  class="pl-2 pb-1 " style="height:26px;   "  onmouseover="showColor(\'show\');"><span style="cursor:pointer;">Attach Worklow</span>'


                    +'<div id="depAddDiv" class="defaultScrollDiv depAddDiv" style="/* margin-top: 21px; */position: absolute;color: black;background: rgb(255, 255, 255);border: 1px solid rgb(166, 166, 166);padding: 4px;left: 143px;height: 67px;top: 84px;overflow: hidden;border-radius: 2px;font-size: 12px;width: 110px;z-index: 12;text-align: left;display:none"><div style="/* padding: 2px 0px; */display: flex;" onclick="addNewDependency(\''+elem.id+'\');event.stopPropagation();">	<span class="defaultExceedCls " style="width: 100%;cursor: pointer;float:left;" title="Create New"><span style=" width: 15px;height: 15px;float: left;margin-top: -1px;" ></span>Create New</span></div><div style="/* padding: 2px 0px; */display: flex;margin-top:-4px" onclick="fetchWorkflowDependency(\'' + elem.id + '\');event.stopPropagation();">	<span class="defaultExceedCls " style="width: 100%;cursor: pointer;float:left;" title="Existing"><span style=" width: 15px;height: 15px;float: left;margin-top: -1px;"></span>Existing</span></div></div>'
                
                
                
                +'</div><div class="pl-2 pb-1 stepop" onclick="fetchGroups(\'returnui\',this);event.stopPropagation();" style="height:26px;"><span style="cursor:pointer;">Group</span>'


                //  +'<div class="groupDiv d-flex  justify-content-end " style="cursor:pointer ; width:90px;"><div class="group_text"><div class="d-flex"><div>Group </div></div></div></div>'
                + '</div>'



                + '<div onclick="removeStep(\'' + elem.id + '\');event.stopPropagation();"  class="pl-2 pb-1 mb-2 stepop" style="height:26px;"><span style="cursor:pointer;">Delete</span></div></div>'

                + '</div>'

                + '<div class="d-flex">'

                + '<div  class="mx-2 my-2 defaultExceedMultilineCls" style="line-height: 11pt;" id="stepdesc_' + elem.id + '"  title="' + elem.description + '"  onclick="showInputStep(\'' + elem.id + '\',this);event.stopPropagation();" >' + elem.description + '</div>'
                + '<input type="text" onblur ="hideInputStep(\'' + elem.id + '\',this);event.stopPropagation();"  id="input_' + elem.id + '" style="display:none;background: inherit;border:0;    padding-left: 8px;" />'
                + '</div>'

                + '</div>');

        }
        //alert(fetchGroups('returnui'));
        if (elem.children.length) {
            html.push(branch(elem.children));
        }

        html.push('</div>');

        return html.join("\n");
    }

    /**
     * prevent entries overlap:
     * each branch height should be equal to the total heights of its children entries
     */
    function assignBranchHeight() {

        // get entries unsorted
        var unsortedEntries = [];
        $('.hortree-entry').each(function () {
            var entryId = $(this).attr('data-entry-id');
            unsortedEntries.push({
                entryId: parseInt(entryId),
                entry: $(this)
            })
        });

        // sort entries by rendering order
        var entries = unsortedEntries.slice(0);
        entries.sort(function (a, b) {
            return a.entryId - b.entryId;
        });

        // get it in reverse order
        entries.reverse();

        // iterate each entry
        for (var i = 0; i < entries.length; i++) {
            var entry = entries[i].entry;
            var children = entry.children('.hortree-branch');

            // if this entry has a branch (it has children entries)
            if (!!children.length) {

                // sum each child entry height
                var h = 0;
                children.each(function () {
                    h += $(this).height();
                });

                // and give it to the parent
                entry.height(h);

            }
        }

    }

    /**
     * draw lines schema between entries
     * @param options
     */
    function drawLines(options) {


        // get native element position
        function getPos(el) {
            var lx = 0,
                ly = 0;

            while (el !== null) {

                lx += el.offsetLeft;
                ly += el.offsetTop;
                el = el.offsetParent;

                if ($('.hortree-wrapper').is(el)) {
                    break;
                }
            }
            console.log(ly)
            return { x: lx, y: ly };
        }

        // draw lines for each hierarchical tree in the page
        $('.hortree-wrapper').each(function () {
            var tree = $(this);

            var offsetY = 0; // offset Y is set on first element iteration
            var elementIndex = 0;

            // iterate over entries content
            tree.find('.hortree-label').each(function () {

                // set Y offset pos where to render each line
                if (elementIndex === 0) {
                    var offsetTop = $(this).offset().top;
                    offsetY = (offsetTop * -1) + 20;
                }

                // if it has children then draw line
                if (!!$(this).siblings('.hortree-branch').length) {

                    // parent position
                    var parentPos = getPos($(this).get(0));
                    console.log($(this).get(0))
                    // get position of each child
                    $(this).siblings('.hortree-branch')
                        .children('.hortree-entry')
                        .children('.hortree-label')
                        .each(function () {

                            var childPos = getPos($(this).get(0));

                            // draw line between two points

                            if ($(this).attr('id').split("_")[1] != 'undefined') {
                                tree.line(
                                    parentPos.x + $(this).width() - 10,
                                    parentPos.y + 41,
                                    childPos.x,
                                    childPos.y + 41,
                                    {
                                        zindex: options.lineZindex,
                                        color: options.lineColor,
                                        stroke: options.lineStrokeWidth
                                    });
                            }

                        })

                }

                elementIndex++;

            })

        });
    }

    /**
     * set each entry height based on its label content
     */
    function assignEntryHeight() {
        $('.hortree-label').each(function () {
            var h = $(this).height();
            $(this).parent('.hortree-entry').height(h);
        });
    }

    /**
     * The plugin
     * @param opts
     */
    $.fn.hortree = function (opts) {
        opts = opts || {};

        // default options
        var defaults = {
            lineStrokeWidth: 2,
            lineZindex: 8,
            lineColor: '#4b86b7',
            data: [],
            onComplete: function () {
                // onComplete callback
            }
        };

        var options = $.extend(defaults, opts);

        if (!$.fn.line) {
            throw new Error("You must load jquery.line.js library! Get it here: https://github.com/tbem/jquery.line");
        }
        else if (!options.data) {
            throw new Error("No data specified!");
        }
        else if (!(options.data instanceof Array)) {
            throw new Error("Data should be an array");
        }
        else if (!options.data.length) {
            console.warn("Data is empty");
        }

        var html = [];
        html.push('<div class="hortree-wrapper">');
        html.push(branch(options.data));
        html.push('</div>');

        this.html(html.join('\n'));

        assignEntryHeight();
        assignBranchHeight();
        drawLines(options);

        // execute onComplete callback
        if (!!options.onComplete && typeof options.onComplete === 'function') {
            options.onComplete.apply();
        }

    }

})(jQuery, window, document);
