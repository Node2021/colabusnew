(function ($) {
    'use strict';

    // [ JS Active Code Index ]

    // :: 1.0 Owl Carousel Active Code
    // :: 2.0 Slick Active Code
    // :: 3.0 Footer Reveal Active Code
    // :: 4.0 ScrollUp Active Code
    // :: 5.0 CounterUp Active Code
    // :: 6.0 onePageNav Active Code
    // :: 7.0 Magnific-popup Video Active Code
    // :: 8.0 Sticky Active Code
    // :: 9.0 Preloader Active code

    // :: 1.0 Owl Carousel Active Code
	
		/* This code is for the counter the banner in the index.html at first from the header on each reload.. */
			
			var n = localStorage.getItem('on_load_counter');
			 
			if (n === null || n >= 6) {
				n = 0;
			}
			 
			n++;
			 
			localStorage.setItem("on_load_counter", n);
			var x = $('#counter_'+n).html();
			
			$('#counter_'+n).html(x);
			$('#counter_'+n).css('display','block');
			
			/* Banner for the images to list */
			 
			 
			var bannerimg = localStorage.getItem('on_load_counter1');
			 
			if (bannerimg === null || bannerimg >= 6) {
				bannerimg = 0;
			}
			 
			bannerimg++;
			 
			localStorage.setItem("on_load_counter1", bannerimg);
			var banner = $('#counter_banner_'+bannerimg).html();
			$('div#counter_banner_'+bannerimg).children("img").html(banner);
	        $('div#counter_banner_'+bannerimg).children("img").css('display','block');
			
			
			/* ------------------Ends here-------------- */
			
			
	
	
    if ($.fn.owlCarousel) {
        $(".welcome_slides").owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout:10000,
            autoplayHoverPause: true,
            smartSpeed: 1500,
            nav: true,
            navText: ["<i class='pe-7s-angle-left'</i>", "<i class='pe-7s-angle-right'</i>"]
        });
        $(".app_screenshots_slides").owlCarousel({
            items: 1,
            loop: true,
            singleItem:true,
            autoplay: true,
            autoplayTimeout:10000,
            autoplayHoverPause: true,
            smartSpeed: 1500,
            margin: 30,
            dots: true
        	/*items: 1,
        	loop: true,
        	autoplay: true,
        	margin: 10,
        	dots: true,
        	smartSpeed: 1500,*/
        });
    }

    // :: 2.0 Slick Active Code
    if ($.fn.slick) {
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 500,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            speed: 500,
            asNavFor: '.slider-for',
            dots: true,
            centerMode: true,
            focusOnSelect: true,
            slide: 'div',
            autoplay: true,
            autoplayTimeout:10000,
            autoplayHoverPause: true,
            centerMode: true,
            centerPadding: '30px',
            mobileFirst: true,
            prevArrow: '<i class="fa fa-angle-left"></i>',
            nextArrow: '<i class="fa fa-angle-right"></i>'
        });
    }

    // :: 3.0 Footer Reveal Active Code
    if ($.fn.footerReveal) {
        $('footer').footerReveal({
            shadow: true,
            shadowOpacity: 0.3,
            zIndex: -101
        });
    }

    // :: 4.0 ScrollUp Active Code
    if ($.fn.scrollUp) {
        $.scrollUp({
            scrollSpeed: 1500,
            scrollText: '<i class="fa fa-angle-up" title="Back To Top"></i>'
        });
    }

    // :: 5.0 CounterUp Active Code
    if ($.fn.counterUp) {
        $('.counter').counterUp({
            delay: 10,
            time: 2000
        });
    }

    // :: 6.0 onePageNav Active Code
    if ($.fn.onePageNav) {
        $('#nav').onePageNav({
            currentClass: 'active',
            scrollSpeed: 2000,
            easing: 'easeOutQuad'
        });
    }

    // :: 7.0 Magnific-popup Video Active Code
    if ($.fn.magnificPopup) {
        $('.video_btn').magnificPopup({
            disableOn: 0,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: true,
            fixedContentPos: false
        });
    }

    $('a[href="#"]').click(function ($) {
        $.preventDefault()
    });

    var $window = $(window);

    if ($window.width() > 767) {
        new WOW().init();
    }

    // :: 8.0 Sticky Active Code
    $window.on('scroll', function () {
        if ($window.scrollTop() > 48) {
            $('.header_area').addClass('sticky slideInDown');
        } else {
            $('.header_area').removeClass('sticky slideInDown');
        }
    });

    // :: 9.0 Preloader Active code
    $window.on('load', function () {
        $('#preloader').fadeOut('slow', function () {
            $(this).remove();
        });
    });
	$window.on('scroll',function () {
    if ($(window).scrollTop() > 48) {
		$('.navbar-brand img').attr('src', 'img/bg-img/images/colabuslogo2.png');
    } else {
        $('.navbar-brand img').attr('src', 'img/bg-img/images/logo1.png');
    }
}); 

	/* $('.center').slick({
	  autoplay:true,
	  arrows:true,
	  prevArrow: $(".left-arrow"),
	  nextArrow: $(".right-arrow"),
	  centerMode: true,
	  infinite: true,
	  centerPadding: '60px',
	  slidesToShow: 1,
	  speed: 500,
	  variableWidth: true,
	  asNavFor: ".slidingVertical" */
	 /*  responsive: [
                            {
                              breakpoint: 1200,
                              settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                              }
                            },
                            {
                              breakpoint: 1008,
                              settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                              }
                            },
                            {
                              breakpoint: 992,
                              settings: "unslick"
                            }

                          ] */
/*   }); */
  
$('.slidingVertical').slick({
	  vertical: true,
	  autoplay: true,
	  autoplayTimeout:10000,
	  autoplayHoverPause: true,
	  speed: 500,
	  arrows:false,
	  slidesToShow: 1
}); 

/* This is for the freqently asked question */
/* const items = document.querySelectorAll(".accordion a");

function toggleAccordion(){
  this.classList.toggle('active');
  this.nextElementSibling.classList.toggle('active');
}

items.forEach(item => item.addEventListener('click', toggleAccordion)); */

/* This is for the freqently asked question */
$('.accordion-link').click(function() {
  var $this = $(this).toggleClass('active');
  var $panel = $this.next().toggleClass('show');
  $('.accordion-link.active').not(this).removeClass('active').next().removeClass('show');
});

		/* -------------------- Videos PlayOption--------------------------- */
			
			
		$.fn.bmdIframe = function( options ) {
        var self = this;
        var settings = $.extend({
            classBtn: '.bmd-modalButton',
            defaultW: 640,
            defaultH: 360
        }, options );
      
        $(settings.classBtn).on('click', function(e) {
          var allowFullscreen = $(this).attr('data-bmdVideoFullscreen') || false;
          
          var dataVideo = {
            'src': $(this).attr('data-bmdSrc'),
            'height': $(this).attr('data-bmdHeight') || settings.defaultH,
            'width': $(this).attr('data-bmdWidth') || settings.defaultW
          };
          
          if ( allowFullscreen ) dataVideo.allowfullscreen = "";
          
          // stampiamo i nostri dati nell'iframe
          $(self).find("iframe").attr(dataVideo);
        });
      
        // se si chiude la modale resettiamo i dati dell'iframe per impedire ad un video di continuare a riprodursi anche quando la modale è chiusa
        this.on('hidden.bs.modal', function(){
          $(this).find('iframe').html("").attr("src", "");
        });
      
        return this;
    };
	
	jQuery(document).ready(function(){
		jQuery("#myModal").bmdIframe();
	});
			
		
			
			
			
	
})(jQuery);