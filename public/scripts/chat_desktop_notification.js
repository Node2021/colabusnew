/**
 * @author Abhishek
 * For details check this notifications "https://codeforgeek.com/2014/10/desktop-notification-like-facebook-using-html5/"
 * And "https://github.com/serkanyersen/ifvisible.js" for checking weather window is blur or not
 */

function notifyMe(imgPath,message) {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    alert(getValues(companyAlerts,"Alert_NoDesktopVersion"));
  }

  // Let's check if the user is okay to get some notification
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
  var options = {
        body: message,
        icon: imgPath,
        dir : "ltr"
    };
  var notification = new Notification("Hi there, You got a Message",options);
   notification.onclick = function () {
   	   notification.close();
       openChatWindow();
       //resetChatFlash();
    }; 
}

  // Otherwise, we need to ask the user for permission
  // Note, Chrome does not implement the permission static property
  // So we have to check for NOT 'denied' instead of 'default'
  else if (Notification.permission !== 'denied') {
	  Notification.requestPermission(function (permission) {
      // Whatever the user answers, we make sure we store the information
	      if (!('permission' in Notification)) {
	        Notification.permission = permission;
	      }
	
	      // If the user is okay, let's create a notification
	      if (permission === "granted") {
		        var options = {
		              body: message,
		              icon: imgPath,
		              dir : "ltr"
		        };
		        var notification = new Notification("Hi there, You got a Message",options);
		        notification.onclick = function () {
		        	notification.close();
			        openChatWindow();
			        //resetChatFlash();
		        }
	    };
    });
    
  }

  // At last, if the user already denied any notification, and you
  // want to be respectful there is no need to bother them any more.
  return true;
}

// for call notification we used this same with some modification 



function callnotifyMe(imgPath,message,badge_url) {
  // Let's check if the browser supports notifications
  console.log(badge_url);

  if (!("Notification" in window)) {
    alert(getValues(companyAlerts,"Alert_NoDesktopVersion"));
  }

  // Let's check if the user is okay to get some notification
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
  var options = {
        body: message,
        icon: badge_url,
        dir : "ltr",
		//badge:badge_url
    };
  var notification = new Notification("You have Incoming Call ",options);
   notification.onclick = function () {
   	   notification.close();
       openChatWindow();
       //resetChatFlash();
    }; 
}

  // Otherwise, we need to ask the user for permission
  // Note, Chrome does not implement the permission static property
  // So we have to check for NOT 'denied' instead of 'default'
  else if (Notification.permission !== 'denied') {
	  Notification.requestPermission(function (permission) {
      // Whatever the user answers, we make sure we store the information
	      if (!('permission' in Notification)) {
	        Notification.permission = permission;
	      }
	
	      // If the user is okay, let's create a notification
	      if (permission === "granted") {
		        var options = {
		                  body: message,
        				  icon: badge_url,
       					  dir : "ltr",
		//badge:badge_url
		        };
		        var notification = new Notification("You have Incoming Call",options);
		        notification.onclick = function () {
		        	notification.close();
			        openChatWindow();
			        //resetChatFlash();
		        }
	    };
    });
    
  }

  // At last, if the user already denied any notification, and you
  // want to be respectful there is no need to bother them any more.
  return true;
}














