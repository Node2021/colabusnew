$.i18n( {
    locale: 'en'
});

jQuery(function($) {
  
  $.i18n().load( {
	  'en': './scripts/lib/i18n/en.json',
	  'pt': './scripts/lib/i18n/pt.json',
	  'ru': './scripts/lib/i18n/ru.json'		  
  }).done(function(){
	  console.log("Lang:"+window.navigator.language); 
	  var browserLang = window.navigator.language.toLowerCase();
	  
	  if(browserLang.indexOf("-") > -1){
		  browserLang = browserLang.substring(0,browserLang.indexOf('-'));
	  }
	  
	  $.i18n().locale = browserLang;
	  $('body').i18n();
	  
	  $("#userName").attr("placeholder",$.i18n('colabus-username'));
	  $("#userPwd").attr("placeholder",$.i18n('colabus-password'));
	  $("#NotSelected").attr("value",$.i18n('colabus-Select-Plan'));
	  $("#Enterprise_plus").attr("value",$.i18n('colabus-Plan-Enterprise'));
	  $("#Business").attr("value",$.i18n('colabus-Plan-Business'));
	  $("#address").attr("placeholder",$.i18n('colabus-Address'));
	  $("#ent_timezone").attr("placeholder",$.i18n('colabus-Timezone'));
	  $("#compPhoneNumber").attr("placeholder",$.i18n('colabus-Timezone'));
	  $("#forgotUname").attr("placeholder",$.i18n('colabus-username'));
	  $("#forgotEmail").attr("placeholder",$.i18n('colabus-email'));
	  $("#new_password").attr("placeholder",$.i18n('colabus-NewPass'));
	  $("#confirm_password").attr("placeholder",$.i18n('colabus-ConfirmPass'));
	  $("#loginButton").attr("title",$.i18n('colabus-login'));
	  $("#colabussignup").attr("title",$.i18n('colabus-signup'));
	  
 });
  
});