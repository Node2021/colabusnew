var isFirefox  = navigator.userAgent.indexOf('Firefox') > -1;
var isChrome  = navigator.userAgent.indexOf('Chrome') > -1;

	function recognizationToggleStart(feedId,type,postLoginType,commMenuTypeId){
		if(isChrome){
			synth.cancel();
			localStorage.setItem("disableCassandraFlag","true");
			commType = type;
			commFeedId = feedId;
			commSubFeedId = feedId;
			commSub2SubFeedId = feedId;
			globalPostLoginType = postLoginType;
			menuTypeIdComm = commMenuTypeId;
			console.log("Toggle Start --> "+type);
			switch (type){
			case 'globalVoicePost': 
			case 'globalVoiceCall':
				if (recognizing) {
					console.log('is currently recognizing');
					needToRestart = true;
					recognition.stop();
				}else{
					console.log('is currently not recognizing');
					needToRestart = true;
					restartTheRecognization();
				}
				break;
			default:{
				if (recognizing) {
					console.log('is currently recognizing');
					needToRestart = true;
					recognition.stop();
				}else{
					console.log('is currently not recognizing');
					needToRestart = false;
					restartTheRecognization();
				}
			}
		}
	  }
 }

	
	  function recognizationToggleStop(){
		 if(isChrome){
			needToRestart = false;
			localStorage.setItem("disableCassandraFlag","false");
			$('#note-textarea').val("");  
			$('#hLightVoiceAddNew').val("");
			commentsVoiceSpeech = "";
			subcommentVoiceSpeech = "";
			subReplyCommentVoiceSpeech = "";
			sub2SubReplyCommentVoiceSpeech = "";
			taskUserCommentVoiceSpeech = "";
			recognition.stop();
		   }
	   }
	
	
var speechSynthesisVoices = null;
function loadVoices(){
	 var synUtterance = new SpeechSynthesisUtterance();
	 var voices = speechSynthesis.getVoices();
	 window.speechSynthesis.onvoiceschanged = function() {
		 speechSynthesisVoices =  window.speechSynthesis.getVoices();
		 
	 };
 }
 

//The below coding is used for text to voice speech 
function textToVoiceSynthesizer(message,textDisplayFlag){
	needToRestart = false;
	recognition.stop();
	synth.volume = 1;
	synth.rate = 0.5;
	synth.pitch = 1;
	 
	 utterThis = new SpeechSynthesisUtterance();
	 utterThis.text = message;
	 if(speechSynthesisVoices != null){
	 for(i = 0; i < speechSynthesisVoices.length ; i++) {
		 if(speechSynthesisVoices[i].name ===  "Google US English") {
			 utterThis.voice = speechSynthesisVoices[i];
		  }
	   }
	 }
	 
	 utterThis.onstart = function(){
		 console.log('speak is started');
		 var headerValue = $('#idToDisplaySpeechText').val();
		 $('#cassandraContent').text(message);
		 if(typeof(textDisplayFlag) != undefined && textDisplayFlag != null && textDisplayFlag == false){
			 $('.cassandraCallout').hide();
			 $('#cassandraHeader').text();
		 }else{
			 $('.cassandraCallout').show();
			 $('#cassandraHeader').text(headerValue).attr("title",headerValue);
		 }
		 resumeInfinity();
	 }
	 
	 utterThis.onpause = function(){
		 console.log('speak is paused');
	 }
	 
	 utterThis.onresume = function(){
		 console.log('speak is resumed');
		 
	 }
	 
	 utterThis.onend = function(event) {
		 console.log('speak is completed');
		 needToRestart = true;
		 restartTheRecognization();
		 clearTimeout(timeoutResumeInfinity);
	 }
	 
	 if(utterThis.text == "audio-capture"){
		 console.log("-->"+utterThis.text);
		 reset();
	 }else{
		 synth.speak(utterThis);
	 }

	 
}


function resumeInfinity() {
	window.speechSynthesis.resume();
	timeoutResumeInfinity = setTimeout(resumeInfinity, 1000);
}



/*Common Validation File for voice on 09-07-2018*/ 

var synth ="";
var speechtotxt = "";
var highLightNameSpeechNew = "";
var highLightDescSpeechNew = "";
var commentsVoiceSpeech = "";
var subcommentVoiceSpeech = "";
var subReplyCommentVoiceSpeech = "";
var sub2SubReplyCommentVoiceSpeech = "";
var taskUserCommentVoiceSpeech = "";
var commFeedId = "";
var commType = "";
var commSubFeedId = "";
var commSub2SubFeedId ="";
var globalPostLoginType = "";
var menuTypeIdComm = "";
var validaterId = 0;
var lastValueWrkspace = "";

var needToRestart = false;


var recognizing = false;
var recognition = "";
var transDocId = "0";
var glbRecResult = "";
var tranTimer;

if(isChrome){
	recognition = new webkitSpeechRecognition();
	recognition.continuous = true;
	
	//speechSynthesis for voice Synthesis synth global variable 
	synth = window.speechSynthesis;
	if(synth) { 
		loadVoices(); 
	}
	
	recognition.onstart = function(){
		recognizing = true;
		console.log('voice listening start');
		if (commType != 'globalVoicePost' && commType != 'globalVoiceCall'){	
			needToRestart = false;
		}
	}
	
	recognition.onend = function(){
		recognizing = false;
		console.log('voice listening end');
		reset();
		if (needToRestart){
			console.log("Restarted the recognization");
			restartTheRecognization();
		}
	}		
	
	
	var recInt =null;
 
	
	recognition.onresult = function (event) {
		for (var i = event.resultIndex; i < event.results.length; ++i) {
		  if (event.results[i].isFinal) {
			   if(commType == 'mainComment' || commType =="wsDocument" || commType =="wsIdea" ||  commType =="agile" || commType == "subComment" 
				   || commType == "subReplyComment" || commType == "sub2SubReplycomment" || commType == "hLightTaskSpeechType" 
				   || commType == "document" || commType == "blog" || commType =="taskComment" 
				   || commType =="globalVoicePost" || commType == "highLightSpeechType" || commType == "globalVoiceCall"){
				   
				  if(commType =="globalVoicePost"){
					   $('#idToDisplaySpeechText').val(event.results[i][0].transcript).attr('title',event.results[i][0].transcript);
					   recInt = setTimeout(function(){
						   validationForSpeechInitializer('');
					   },2000);
				   }else if(commType == "mainComment"){
					  $('#main_commentTextarea').val($('#main_commentTextarea').val()+ event.results[i][0].transcript);
						 commentsUsingVoiceToText('mainThread');
				  }else if(commType == "wsDocument" || commType =="wsIdea" || commType =="agile" || commType == "document" || commType == "blog"){
					  $('#main_commentTextarea').val($('#main_commentTextarea').val()+ event.results[i][0].transcript);
						 commentsUsingVoiceToText('commonMainThread');
				  }else if(commType == "subComment"){
					  $('#replyBlock_'+commFeedId).val($('#replyBlock_'+commFeedId).val()+ event.results[i][0].transcript);
						 commentsUsingVoiceToText('subThread');	
				  }else if(commType == "subReplyComment"){
					  $('#replyBlock_'+commSubFeedId).val($('#replyBlock_'+commSubFeedId).val()+ event.results[i][0].transcript);
						 commentsUsingVoiceToText('subReplyThread');	
				  }else if(commType == "sub2SubReplycomment"){
					  $('#replyBlock_'+commSub2SubFeedId).val($('#replyBlock_'+commSub2SubFeedId).val()+ event.results[i][0].transcript);
						 commentsUsingVoiceToText('sub2SubReplyThread');	
				  }else if(commType == "taskComment"){
					  $('#UserCommentNewUI').val($('#UserCommentNewUI').val()+ event.results[i][0].transcript);
						 commentsUsingVoiceToText('userTaskComment');	
				  }else if(commType == "globalVoiceCall"){
					   if(transDocId != "0"){
						   var callresult = event.results[i][0].transcript;
							glbRecResult = callresult;
							//console.log("Result:::>>"+glbRecResult);
							showTranscript(glbRecResult,userFirstName,'');
							sendingTextOutputTranscript();
							textusingAvCall(glbRecResult);
//		          			  transcriptVideo(callresult);
					   }else{
						   console.log("No result");
					   }
				  }else if(commType == "hLightTaskSpeechType"){
					  $('#note-textarea').val($('#note-textarea').val()+ event.results[i][0].transcript);
					  recInt = setTimeout(function(){
						  chooseTheValidationFunc(validaterId);
					  },2000);
				  }else if(commType == "highLightSpeechType"){
					  if($("#hMsg").attr("highLightMsg") == "active"){
						   $('#hMsg').val($('#hMsg').val()+ event.results[i][0].transcript);
						   highLightUsingVoice('msgName');
					   }else{
						   $('#hLightDesc').val($('#hLightDesc').val()+ event.results[i][0].transcript);
						   highLightUsingVoice('msgDesc'); 
					   }
				  }else{
					  //else part
				  }
			  }else{
				  ///else
			  }
		  }
		}
	  }
	
	recognition.onerror = function (event) {
		recognizing = false;
		console.log('Speech recognition error detected: ' + event.error);
		
		 switch (event.error) {
			 case 'no-speech':
				 if (commType != 'globalVoicePost' && commType != 'globalVoiceCall'){
					 needToRestart = false;
				 }
			   break;
			 case 'aborted':
			   recognizationToggleStop();
			   break; 
			 case 'audio-capture': 	
			 // case 'network':
			 // case 'not-allowed':
			 case 'service-not-allowed':
			 case 'bad-grammar':
			   textToVoiceSynthesizer(event.error);
			   break;
			 case 'language-not-supported':
			   console.log('permission denied by user or browser');
			   textToVoiceSynthesizer(event.error);
			   break;
	   }
		console.log('Additional information: ' + event.message);
		//recognizationToggleStop();
	};
   
	function reset() {
//	      console.log("Reset the speech button");
	  recognizing = false;
	  speechtotxt = $('#note-textarea').val(); 
	  highLightSpeechNew = $('#hLightVoiceAddNew').val();
	  //chooseTheValidationFunc(validaterId);
	  $('div#answerMsg').show();
	  $('#hLigthTaskVoiceStart').css('display','block');
		$('#hLigthTaskVoiceStop').css('display','none');
	  //$('#recordStart').css('box-shadow','');
	  //$('#recordStart').css({'background-color':'','border-radius':'none','width': '','padding': ''});
	  $('#record-started').css('display','none');
	  $('#record-stopped').css('display','block');
	  $('#hLightVoiceStartNew').css('display','block');
		$('#hLightVoiceStopNew').css('display','none');
		$('#mainThreadVoiceStart').css('display','block');
		$('#mainThreadVoicestop').css('display','none');
		$('#subThreadVoiceStart_'+commFeedId).css('display','block');
	  $('#subThreadVoicestop_'+commFeedId).css('display','none');
	  $('#subReplyThreadVoiceStart_'+commSubFeedId).css('display','block');
	  $('#subReplyThreadVoicestop_'+commSubFeedId).css('display','none');
	  $('#sub2SubReplyThreadVoiceStart_'+commSub2SubFeedId).css('display','block');
	  $('#sub2SubReplyThreadVoicestop_'+commSub2SubFeedId).css('display','none');
	  //All main comments except conversation
	  $('#mainThreadVoiceWsDocStart_'+menuTypeIdComm).css('display','block');
	  $('#mainThreadVoiceWsDocStop_'+menuTypeIdComm).css('display','none');
	  /*For Main comments WsIdeas*/
	  $('#mainThreadVoiceMultiStart').css('display','block');
	  $('#mainThreadVoiceMultiStop').css('display','none');
	  /*For task comments*/
	  $('#voiceCommentTaskStart').css('display','block');
	  $('#voiceCommentTaskStop').css('display','none');
	}
	
	
	
	function restartTheRecognization(){
		//console.log("commType --> "+commType);
		//alert("commSubFeedId commSub2SubFeedId "+commSubFeedId+ " "+commSub2SubFeedId);
		switch (commType){
		case 'globalVoicePost':
			recognition.start();
			$('#record-stopped').css('display','none');
			 $('#record-started').css('display','block');
		 break;
		case 'mainComment':
			recognition.start();
			$('#mainThreadVoicestop').css('display','block');
			$('#mainThreadVoiceStart').css('display','none');
		break;
		case 'subComment':
			recognition.start();
			$('#subThreadVoicestop_'+commFeedId).css('display','block');
			$('#subThreadVoiceStart_'+commFeedId).css('display','none');
		break;
		case 'subReplyComment':
			recognition.start();
			$('#subReplyThreadVoicestop_'+commSubFeedId).css('display','block');
			$('#subReplyThreadVoiceStart_'+commSubFeedId).css('display','none');
		break;
		case 'sub2SubReplycomment':
			recognition.start();
			$('#sub2SubReplyThreadVoicestop_'+commSub2SubFeedId).css('display','block');
			$('#sub2SubReplyThreadVoiceStart_'+commSub2SubFeedId).css('display','none');
		break;
		case 'wsDocument': case 'agile':  case 'document': case 'blog':
			recognition.start();
			$('#mainThreadVoiceWsDocStop_'+menuTypeIdComm).css('display','block');
			$('#mainThreadVoiceWsDocStart_'+menuTypeIdComm).css('display','none');
		break;
		case 'wsIdea': case 'blog':  
			recognition.start();
			$('#mainThreadVoiceMultiStart').css('display','none');
			$('#mainThreadVoiceMultiStop').css('display','block');
		break;
		case 'taskComment':
			recognition.start();
			$('#voiceCommentTaskStart').css('display','none');
			$('#voiceCommentTaskStop').css('display','block');
		break;
		case 'globalVoiceCall':
//				 console.log("Inside the globalVoiceCall");
			 recognition.start();
		break;
		case 'highLightSpeechType': 
			 recognition.start();
			 $('#hLightVoiceAddNew').val("");  
			 $('#hLightVoiceStopNew').css('display','block');
			 $('#hLightVoiceStartNew').css('display','none');
		break;
		case 'hLightTaskSpeechType':
			recognition.start();
			$('#note-textarea').val("");  
			$('#hLigthTaskVoiceStop').css('display','block');
			$('#hLigthTaskVoiceStart').css('display','none');
		break;
		default: 
			console.log("restartTheRecognization--default");
		}
	}
	

	
 /*   
	function onResultVoiceToText(resultText) {
		console.log("commType ---> "+commType+" onResult Text --> "+resultText);
			  if(commType =='globalVoicePost'){
					 $('#idToDisplaySpeechText').val(resultText);
					 recInt = setTimeout(function(){
						 validationForSpeechInitializer();
					 },2000);
				 }else if(commType == "mainComment"){
					$('#main_commentTextarea').val($('#main_commentTextarea').val()+resultText);
						commentsUsingVoiceToText('mainThread');
				}else if(commType == "subComment"){
					$('#replyBlock_'+commFeedId).val($('#replyBlock_'+commFeedId).val()+resultText);
					   commentsUsingVoiceToText('subThread');	
				}else if(commType == "subReplyComment"){
					$('#replyBlock_'+commSubFeedId).val($('#replyBlock_'+commSubFeedId).val()+resultText);
					   commentsUsingVoiceToText('subReplyThread');	
				}else if(commType == "sub2SubReplycomment"){
					$('#replyBlock_'+commSub2SubFeedId).val($('#replyBlock_'+commSub2SubFeedId).val()+resultText);
					   commentsUsingVoiceToText('sub2SubReplyThread');	
				}else if(commType == "wsDocument" || commType =="wsIdea" || commType =="agile" || commType == "document" || commType == "blog"){
					$('#main_commentTextarea').val($('#main_commentTextarea').val()+resultText);
					   commentsUsingVoiceToText('commonMainThread');
				}else if(commType == "taskComment"){
					$('#UserCommentNewUI').val($('#UserCommentNewUI').val()+resultText);
					   commentsUsingVoiceToText('userTaskComment');	
				}else if(commType == "hLightTaskSpeechType"){
					$('#note-textarea').val($('#note-textarea').val()+resultText);
					recInt = setTimeout(function(){
						chooseTheValidationFunc(validaterId);
					},2000);
				}else if(commType == "highLightSpeechType"){
					if($("#hMsg").attr("highLightMsg") == "active"){
						 $('#hMsg').val($('#hMsg').val()+resultText);
						 highLightUsingVoice('msgName');
					 }else{
						 $('#hLightDesc').val($('#hLightDesc').val()+resultText);
						 highLightUsingVoice('msgDesc'); 
					 }
				}else{
					Console.log("commType end-->");
				}
		}*/
	
	function queryNext(index,status){
		 var myArray = ["Ok You want to create this as a task. For which workspace do you want to create this task?","Ok Whom do you want to assign this task to?","Ok What is due date for this task?","Ok What priority you want to assign for this task?","Ok"];
		 var myArray1 = ["Y","Y","Y","Y","Y"];
		 $('.quesErrDiv').css('border','1px solid #D1D7D7');
		 $("div#queryMsg").html(myArray[index]);
		textToVoiceSynthesizer(myArray[index]);
		//alert(index+"----"+status);
		 if(index == "0"){
			 $("#projectOptions").parents("#project").css('border','1px solid red');
			 //selectVoiceWorkspace();
		   }else if(index == "1"){
			 $('#participantsDivNewUI').show();
			$("#projectOptions").parents("#project").css('border','1px solid #409bb5');
		 }else if(index == "2"){
			   $("#endDateCalNewUI").css('border','1px solid red');
		   }else if(index == "3"){
			 $("#priority").css('border','1px solid red');
			$("#endDateCalNewUI").css('border','1px solid #409bb5');
		 }else if(index == "4"){
			$("#priority").css('border','1px solid #409bb5');
		 }else{
			 $('#projectOptions , #endDateCalNewUI , #priority').css('border','1px solid #409bb5');
		 }
		/*else if(index == "1"){
			   $("#hlightdatepicker").parents("div.quesErrDiv").css('border','1px solid red');
		   }else if(index == "2"){
			 $("#priority").css('border','1px solid red');
		 }else if(index == "3"){
			 $("#hlighttextarea").parents("div.quesErrDiv").css('border','1px solid red');
		 }else{
			 $('.quesErrDiv').css('border','1px solid #D1D7D7');
		 }*/
		 
		if(status == 'N' || index==myArray.length-1){
			$("#query-btn-next").hide();
		}else{
			$("#query-btn-next").show();
		}
		
		var newIndex = index+1;
		validaterId = index;
		$("#query-btn-next").attr('onclick','queryNext('+newIndex+',"'+myArray1[newIndex]+'")');
	}  
	function selectVoiceWorkspace(){ 
		showProjectListViewNewUI();
		////getAllProjects();
	}
	function chooseTheValidationFunc(validationIndex) {
		//toggleStartStop();
		var skipArray = ["skip","next","continue","don't know"];
		var exitArray = ["cancel","close","exit"];
		var createArray = ["task","create"];
		var assignArray = ["assign to","participants","assignee","team members","user","assign"];
		var dueArray = ["due by","due date","last date","due","date"];
		if(typeof $('#note-textarea').val() =='undefined'){
			return;
		}
		var noteText = $('#note-textarea').val().toLowerCase();
		if(checkTheKeyAvailability(noteText,exitArray)){
			$('#newUIForDeleteTask').trigger('click');
			return;
		}else{
			if (checkTheKeyAvailability(noteText,skipArray) || checkTheKeyAvailability(noteText,createArray)){
				if(checkTheKeyAvailability(noteText,createArray) || validaterId > 3){
					$('#wsTaskSaveContainer').trigger('click');
				}else{
					$('#query-btn-next').trigger("click");
				}
				return;
			}
		}
		
		if(checkTheKeyAvailability(noteText,assignArray)){
			queryNext('1','Y');
			//showParticipantsNewUI();
			//$('#participantsDivNewUI').show();
			//addTaskParticipants();
			//$('#saveAndCloseTimeAndDate').addClass('speechParticipantNewClss');
		}else{
			if(validaterId == 1){
			validateVoiceParticipants();
			//showParticipantsNewUI();
			return;
			}
		}
		if(checkTheKeyAvailability(noteText,dueArray)){
			queryNext('2','Y');
			//calenderViewPopUp();
			//$("#calViewNewUIDiv").show();
		  if(validaterId == 3){
			validateVoiceDate();
			//calenderViewPopUp();
			return;
		  }
		}

		switch (validationIndex) {
		case 0:
			validateProjectSelect();
			break;
		case 1:
			validateVoiceParticipants();
			break;
		case 2:
			validateVoiceDate();
			break;
		case 3:
			validateVoicePriority();
			break;
		
		}
		
	}
	
	function checkTheKeyAvailability(keyString, keysArray){
		for (var j = 0; j < keysArray.length; j++){
			if (keyString.includes(keysArray[j])){ 
			 return true; 
		  }
		}
		return false;
	}
	function validateProjectSelect(){
		var noteText = $('#note-textarea').val().toLowerCase();
//console.log("validateProjectSelect--->"+noteText);
		var result=false;
//console.log("validateProjectSelect-------------");
		var optionExists = $("#project").find("#projectOptions option[attValue='"+noteText+"']").length ;
//console.log("optionExists:"+optionExists);
		if($("#project").find("#projectOptions option[attValue='"+noteText+"']").length > 0){
			$("#project").find("#projectOptions option[attValue='"+noteText+"']").attr('selected','selected');
			var pid = $("#project").find("#projectOptions option:selected").attr("id");
			$("#project").find("#projectOptions option:selected").trigger('onchange');
			//alert("pid:"+pid);
			//loadSprintUsers(pid);
			//validateVoiceParticipants();
			$('#participantsDivNewUI').show();
			result = true;
			$('#query-btn-next').trigger("click");
			   $('#note-textarea').val("");
		   }
		//$('#participantsDivNewUI').show();
		if(!result && noteText!=''){
			textToVoiceSynthesizer('Sorry, I could not find a workspace that matches your request. Can you please try again?');
		}
		
	}

	function validateVoiceParticipants(){
		showParticipantsNewUI();
		$('#participantsDivNewUI').show();
		
		var noteText = $('#note-textarea').val().toLowerCase();
//alert("noteText::"+noteText);
		//$('#saveAndCloseTimeAndDate').removeClass('speechParticipantNewClss');
		$('#hlighttextarea').val(noteText.toLowerCase()).focus(); 
		//var ufltrName = $('#hlighttextarea').val();
		var result="Invalid";
		//filterHighlightTaskUsers();
		//hideAddedParticipantsInList();
		
		//$('#participantsDivNewUI').find('[id^=participant_]').each(function(){
			var a = $('div.participants').find('span.hidden_searchName:contains("'+noteText+'")').length;
//alert("A:"+a);
			if($('div.participants').find('span.hidden_searchName:contains("'+noteText+'")').length > 0){
				var id = $('div.participants').find('span.hidden_searchName:contains("'+noteText+'")').parent().attr('id').split('_')[1];
				//console.log("id---:"+id);
				$('#participant_'+id).find('#particpantName_'+id).trigger('onclick');
				result="valid";
				textToVoiceSynthesizer('Add more Participants');
			}
		//});
		
		if(result == "Invalid"){
			$('#participantsDivNewUI').show();
			textToVoiceSynthesizer('Sorry, I could not find such a participant. Choose the right participant');
		}
		$('#note-textarea').val(""); 
		
	}
	
	/*function validateVoiceParticipants(){
		var noteText = $('#note-textarea').val().toLowerCase();
		//$('#saveAndCloseTimeAndDate').removeClass('speechParticipantNewClss');
		$('#hlighttextarea').val(noteText.toLowerCase()).focus(); 
		var ufltrName = $('#hlighttextarea').val();
		var result="Invalid";
		$('#participantsDivNewUI').show();
		filterHighlightTaskUsers();
		hideAddedParticipantsInList();
		$('div.participants').find('span.hidden_searchName:contains("'+ufltrName+'")').each(function(){
					var id = $(this).parent().attr('id').split('_')[1];
					$('#participantsDivNewUI').find('#particpantName_'+id).trigger('click');
					//$('#hlighttextarea').blur();
					result="valid";
					//textToVoiceSynthesizer('Add more Participants');
				}
		);
		if(result == "Invalid"){
			$('#participantsDivNewUI').show();
			$('#userListContainer').append('<div id="innerDiv18" style="float: left;margin-top: -154%;width: 100%;" align="center"> No Participant</div>');
			textToVoiceSynthesizer('Sorry, I could not find such a participant. Choose the right participant');
		}else{
			//$('#participantsDivNewUI').css('display','none');
		}

		$('#note-textarea').val(""); 
	}*/
	
	//method for voice date validation date month year
	function voiceDateValidation(txtToVoice){
		var Regex='/^[^a-zA-Z]*$/';
		var d = new Date();
		var tomorrowDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
		var dateNum = "";
		var datemonth = "";
		 var month = d.getMonth()+1;
		 var day = d.getDate();
		 var numDaysInMonth="";
		 var result="Invalid";
		 var months = ['january','february','march','april','may','june','july','august','september','october','november','december'];
		var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		 if(txtToVoice!= '' && txtToVoice.toLowerCase() == 'today'){
			 //result = (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + '-' + d.getFullYear();
		//monthNames[startsplit[0]-1] + ' ' +  startsplit[1] + ', ' + startsplit[2];
			result = monthNames[month-1]+' '+(day < 10 ? '0' : '') + day+', '+d.getFullYear();
		 }else if(txtToVoice!= '' && txtToVoice.toLowerCase() == 'tomorrow'){
			 //result = ((tomorrowDate.getMonth()+1) < 10 ? '0' : '') + (tomorrowDate.getMonth()+1) + '-' + (tomorrowDate.getDate() < 10 ? '0' : '') + tomorrowDate.getDate() + '-' + tomorrowDate.getFullYear();
			result = monthNames[(tomorrowDate.getMonth()+1)-1]+' '+ (tomorrowDate.getDate() < 10 ? '0' : '') + tomorrowDate.getDate() +', '+d.getFullYear();
		 }else{
		//alert("len-->"+txtToVoice.split(' ').length);
		//alert("txtToVoice:"+txtToVoice);
			 if(txtToVoice.split(' ').length == 1 && txtToVoice != ''){
				dateNum = parseInt(txtToVoice, 10); 
					if(dateNum > 0){
						result = dateNum;
					}else{
						result = roman_to_Int(txtToVoice);
					}
					numDaysInMonth = daysInMonth(d.getMonth(), d.getFullYear());
					if(result > numDaysInMonth || result <= 0 || result < d.getDate()){
						textToVoiceSynthesizer('Sorry we did not get the date right. Say it again');
						result = 'Invalid';
					}else{
						//result = (month < 10 ? '0' : '') + month + '-' + (result < 10 ? '0' : '') + result + '-' + d.getFullYear();
						result = monthNames[((month < 10 ? '0' : '') + month)-1]+' '+ (result < 10 ? '0' : '') + result +', '+d.getFullYear();
					}
					
					
			}else if(txtToVoice.split(' ').length == 2){
				txtToVoice = txtToVoice.toLowerCase();
				var str_array = txtToVoice.split(' ');
				var y = '';
				for(var i = 0; i < str_array.length; i++) {
				   y = parseInt(str_array[i], 10); 
				   if(y > 0){
					   dateNum = y ;
				   }else{
					   datemonth = months.indexOf(str_array[i]);
					   datemonth = parseInt(datemonth)+1;
				   }
				}
				if(parseInt(datemonth)-1 >= d.getMonth() && parseInt(datemonth)-1 < 12){
					numDaysInMonth = daysInMonth(parseInt(datemonth)-1, d.getFullYear());
				}
				//var y = parseInt(x, 10); 
			   if(datemonth < month){
				   textToVoiceSynthesizer('Sorry wrong month. Say the correct month again');
				   result = 'Invalid';
			   }else if(datemonth == month && dateNum < day){
				   textToVoiceSynthesizer('Sorry the date is already passed. Say current date or future date');
				   result = 'Invalid';
			   }else if(dateNum > numDaysInMonth || dateNum <= 0 ){
					textToVoiceSynthesizer('Sorry wrong date. Say it again');
					result = 'Invalid';
			   }else{
				   //result = (datemonth < 10 ? '0' : '') + datemonth + '-' + (dateNum < 10 ? '0' : '') + dateNum + '-' + d.getFullYear();
				   result = monthNames[((datemonth < 10 ? '0' : '') + datemonth)-1]+' '+ (dateNum < 10 ? '0' : '') + dateNum +', '+d.getFullYear();
			   }
			}else if(txtToVoice.split(' ').length == 3){
				txtToVoice = txtToVoice.toLowerCase();
				var str_array = txtToVoice.split(' ');
				var y = '';var dateYear='';
				for(var i = 0; i < str_array.length; i++) {
				   y = parseInt(str_array[i], 10); 
				   if(y > 1000){
					   dateYear=y;
				   }else if(y > 0){
					   dateNum = y ;
				   }else{
					   datemonth = months.indexOf(str_array[i]);
					   datemonth = parseInt(datemonth)+1;
				   }
				}
				if(parseInt(datemonth)-1 >= d.getMonth() && parseInt(datemonth)-1 < 12 && dateYear >= d.getFullYear()){
					numDaysInMonth = daysInMonth(datemonth,dateYear);
				}
				//var y = parseInt(x, 10); 
			   if(datemonth < month && dateYear == d.getFullYear()){
				   textToVoiceSynthesizer('Sorry due date cannot be from previous year. Say the correct year again');
				   result = 'Invalid';
			   }else if(datemonth == month && dateNum < day && dateYear == d.getFullYear()){
				   textToVoiceSynthesizer('Sorry wrong date. Say it again');
				   result = 'Invalid';
			   }else if(dateNum > numDaysInMonth && dateYear == d.getFullYear()){
					textToVoiceSynthesizer('Sorry wrong date. Say it again');
					result = 'Invalid';
			   }else if(dateYear < d.getFullYear()){
				   textToVoiceSynthesizer('Sorry due date cannot be from previous year. Say the correct year again');
				   result = 'Invalid';
			   }else{
				   //result = (datemonth < 10 ? '0' : '') + datemonth + '-' + (dateNum < 10 ? '0' : '') + dateNum + '-' + dateYear;
				   result = monthNames[((datemonth < 10 ? '0' : '') + datemonth)-1]+' '+ (dateNum < 10 ? '0' : '') + dateNum +', '+d.getFullYear();
			   }
			}
		 }
		//alert("result:"+result);
		return result;
	}
	
	function validateVoiceDate(){
		calenderViewPopUp();
		//$("#calViewNewUIDiv").show();
		var noteText = $('#note-textarea').val().toLowerCase();
		var voiceDate = voiceDateValidation(noteText);// method for voice date validation
		var d = new Date();
		 var month = d.getMonth()+1;
		 var day = d.getDate();
		 //var currentDate = (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + '-' + d.getFullYear();
		var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var currentDate = monthNames[((month < 10 ? '0' : '') + month)-1]+' '+ (day < 10 ? '0' : '') + day +', '+d.getFullYear();
			  
		 if(voiceDate == 'Invalid'){
			 textToVoiceSynthesizer('Sorry due date is invalid. I going to set the current date as default');
			 $('#endDateCalNewUI , #endOfTask').text(currentDate);
			$('#note-textarea').val("");
		}else{
			 $('#endDateCalNewUI , #endOfTask').text(voiceDate);
			$('#saveAndCloseTimeAndDate').find('#savecalpop').trigger('onclick');
			$('#query-btn-next').trigger("click");
			$('#note-textarea').val("");
			 
		 }
		 
	 }
	function validateVoicePriority(){
		calenderViewPopUp();
		//$("#calViewNewUIDiv").show();
		var noteText = $('#note-textarea').val().toLowerCase();
		var pIndex = 0;
		var result="Invalid";
		if(noteText=="very important" || noteText == 1){
			result="valid";
			pIndex = 1;
		}else if(noteText=="important" || noteText == 2){
			result="valid";
			pIndex = 2;
		}else if(noteText=="medium" || noteText == 3){
			result="valid";
			pIndex = 3;
		}else if(noteText=="low" || noteText == 4){
			result="valid";
			pIndex = 4;
		} else if(noteText=="very low" || noteText == 5){
			result="valid";
			pIndex = 5;
		} 
		if(noteText > 5){
			textToVoiceSynthesizer('Sorry wrong priority. Say the correct priority number between 1 to 5');
		}else if(result == 'Invalid'){
			textToVoiceSynthesizer('Sorry wrong priority. Say the correct priority again');
		}else{
			$('#priority option[value='+pIndex+']').attr('selected','selected');
			if(pIndex == 1){
				$('#priorityHrOfTask').attr('src',path+'/images/calender/VeryImportant.png');
			}else if(pIndex == 2){
				$('#priorityHrOfTask').attr('src',path+'/images/calender/Important.png');
			}else if(pIndex == 3){
				$('#priorityHrOfTask').attr('src',path+'/images/calender/medium.png');	
			}else if(pIndex == 4){
				$('#priorityHrOfTask').attr('src',path+'//images/calender/Low.png');
			}else if(pIndex == 5){
				$('#priorityHrOfTask').attr('src',path+'/images/calender/VeryLow.png');	
			}else{
				$('#priorityHrOfTask').attr('src',path+'/images/calender/none.png');	
			}
			
			$('#saveAndCloseTimeAndDate').find('#savecalpop').trigger('onclick');
			$('#query-btn-next').trigger("click");
			
		}
		$('#note-textarea').val(""); 
	}
	
	
	
	var glbConditionInsideVal="";
	var firsttime;
	var WeekTrend;
	//post login.jsp the input speech recognization 
function validationForSpeechInitializer(place){ 
	var text = $('#idToDisplaySpeechText').val().toLowerCase(); 
	var YesArray=["yes","ok","go ahead", "sure"];
	var NoArray=["no","not","cancel","dont"];
	var NoValueArray=[""," "];
	var acceptArray = ["casandra","casendra","cassandra","santra","sandra","priya"]; 
	var exitArray = ["exit","close","stop","shut up"]; 
	var homeArray=["home","landing","return"];
	var listArray = ["list","show","go to","open"]; 
	var contactArray = ["contacts","Team","user"];
	var colabusHelpArray=["colabus","colab us","colab-us","collab us","collab-us","co lab us"];
	var teamzoneHelpArray=["team zone"];
	var WorkspaceArray=["workspace","project"];
	var usersArray=["user"];
	var taskArray=["task"];
	var taskmyArray = ["mytask","mytasks","my task","my tasks","myzone task","myzone tasks"];
	var completionCodeArray=["completion codes","completion quotes"];
	//var FunctionArray=["task","idea","sprint task","document","messages"];
	var documentArray=["document"];
	var documentMyArray = ["my document","mydocument","myzone document","my documents","myzone documents","my zone documents"];
	var ideaArray=["idea"];
	var galleryArray=["gallery","my gallery"];
	var journalsArray=["journal","blog","my journal","my blog","my journals"];
	var notesArray=["note","my note","my notes"];
	var recommendationArray=["recommendation"];
	var messageArray=["message","my message","my messages"];
	var HelpDefinitionArray=["what","define","explain","can"];
	var HelpHowToArray=["how"];
	
	//====What functionality
	var HelpSearchArray=["global search"];
	var HelpProfilePicArray=["profile picture","my dp","my picture"];
	var HelpPasswordArray=["password"];
	var HelpNotificationArray=["notification preference"];
	var HelpNotificationHistoryArray=["prior notification","previous notification","notification history","history of notification","notifications","notification"];
	var HelpMenuHelpArray=["help","menu help","help info","topic info"];
	var HelpHomeScreenArray=["home","home screen","main page"];
	var HelpSwitchWorkspaceArray=["switch between workspace","switch workspace","move between workspace","change to different workspace"];
	var taskTrendArray=["task trend"];
	var trendArray=["trend","score"];
	//=========
	var createArray=["create", "generate","add","new"];
	var myzoneHelpArray=["my zone"];
	var enterprisezoneHelpArray=["enterprise zone"];
	var existingWorkspaceArray =["existing"];
	var searchArray = ["search","find","locate"];
	var createWorkflowArray=["workflow"];
	var createWorkflowTemplateArray=["workflow template"];
	var createEventArray=["event"];
	//var analyticsWorkspace=["workspace"];
	var analyticsUtilization=["utilization","utilisation"];
	var analyticsTodayReport = ["today report","daily status report","todays report"];
	var analyticsSentiment=["sentiment"];
	var analyticsGreen=["green","not red","not in red","no red"];
	var analyticsRed=["red","not green","not in green","no green"];
	var analyticsRecent=["recent","last visited","latest"];
	var sentimentSearch=["score of project"];
	var analyticsResourceUtilizationPercentage=["resource utilization percentage","resource utilisation percentage","utilisation percentage"];
	var analyticsWorkspaceStatusReport = ["workspace status report","workspace status","workspace report"];
	var conversationArray=["conversation"];
	var iconsArray=["icon"];
	var statusArray=["status"];
	
	//Agile
	var epicArray=['epic'];
	var featureArray=['feature'];
	var storyArray=['story','stories'];
	var sprintGroupArray=["sprint group"];
	var scrumArray=["scrum","my scrums","my scrum"];
	
	//Chat
	var whiteBoardArray=["whiteboard","white board"];
	var audioVideoArray=["audio","video"];
	var shareArray=["share"];
	var instantArray=["instant"];
	var meetingArray=["meeting"];
	
	//common phrases
	var reorderArray=['reorder','re-order','rearrange','re-arrange','move','shuffle','drag','drop'];
	var typeArray=['type'];
	
	//List of what is?
	var teamMemberArray = ["team members","team member"];
	var emailsArray = ["email functionality","email"];
	var sprintArray = ["sprint"];
	var enterpriseAnalyticsArray = ["analytics"];
	var enterpriseSystemArray = ["system admin","sys admin"];
	var enterpriseIntergrationArray = ["integrations","integration"];
	
	// TeamZone Workspace Level
	var WorkspaceRedirect=["workspace","project"];
	var recentWorkspaceConver = ["conversation","recent conversation","conversations","recent conversations"];
	var recentWorkspaceTask = ["task","recent task"];
	var recentWorkspaceDocument = ["document","recent document","documents","recent documents"];
	var recentWorkspaceTeam = ["team","recent team","teams","recent teams"];
	var recentWorkspaceEmail = ["email","recent email","recent emails","emails"];
	var recentWorkspaceIdea = ["idea","recent idea","ideas","recent ideas"];
	var recentWorkspaceAgileDefinition = ["agile definition","recent agile definition","agile","recent agile","definition","recent definition"];
	var recentWorkspaceAgileSprint = ["sprint","recent sprint","sprints","recent sprints"];
	var recentWorkspaces = ["recent workspace","recent project"];
	var chatCme = ["c me","see me","chat","colabus chat","messenger","instant messenger","chat message"];
	var myProfileUser = ["my profile","profile","profile options","user profile","profile settings","profile setting"];
	var pricingArray = ["pricing plan work","pricing"];
	var actionableArray = ["actionable insights","ai capability","actionable","ai"];
	var benefitsArray = ["benefits","advantages"];
	var readyfeatureArray = ["enterprise ready"];
	var onboardingArray = ["onboarding process","onboarding"];
	var getStarted = ["get started","start"];
	var updateTask = ["update"];
	var settingArray = ["project setting","workspace setting"];
	var approvalArray = ["approval"];
	var optionalDriveArray = ["optional drives","optional drive"];
	var detailsArray = ["detail","details"];
	var countArray = ["count"];
	var whatsUp = ["list inputs","how is it going","list my inputs","today's inputs","whats happening"];
		
	globalVoiceMessage="";
	if(place != undefined && place != null && place != '' && place == "enterEvent"){
		if(checkTheKeyAvailability(text,YesArray) || checkTheKeyAvailability(text,NoArray)){
			text = text;
		}else{
			text = acceptArray[0]+' '+text;
		}
	}

	if (checkTheKeyAvailability(text,acceptArray)){
		glbConditionInsideVal='';
	  
		 if(checkTheKeyAvailability(text,exitArray)){ 
			validationForSpeechExit(); 
		 }else if(checkTheKeyAvailability(text,whatsUp)){
				redirectWrkSpace('','','whatsup');
		 }
		 else if(checkTheKeyAvailability(text,HelpHowToArray)){//HelpHowToArray-->>how
			if(checkTheKeyAvailability(text,createArray)){//-->create/generate/add/new
				if(checkTheKeyAvailability(text,usersArray)){
					 if(checkTheKeyAvailability(text,WorkspaceArray)){
						 textToVoiceSynthesizer('If you are the project owner you can add users under the Workspace Settings menu. Add the user in the appropriate section based on their role for the workspace. Note, you can only add people who are already users of the system. To add users to the system, contact your system admin. '); 
					 }else{
						 textToVoiceSynthesizer('Hmm... I am not able to help you with that request');
					 }
				}else if(checkTheKeyAvailability(text,WorkspaceArray)){
					 glbConditionInsideVal = 'workspace';
					 textToVoiceSynthesizer('Click on Start New Workspace menu in TeamZone. Specify the name for your workspace. A Unique code is generated for this workspace. Add a description and select the users who would be a part of this workspace. Want to create a workspace?');
				}else if(checkTheKeyAvailability(text,taskArray)){
					if(checkTheKeyAvailability(text,conversationArray)){
						textToVoiceSynthesizer('Select the specific conversation posting. Use the TASK menu under the MORE option. Specify the parameters for the task. The task name will automatically be filled in with the conversation post. When a task is created you will see a task pin associated with the conversation.');
					}else if(checkTheKeyAvailability(text,meetingArray)){
						textToVoiceSynthesizer('Select the action item in the meeting highlights. Select the Task menu under the More option for the action item. Optionally, specify the Workspace, Completion Date, Priority and the persons to assign this task to.');
					}else if(checkTheKeyAvailability(text,completionCodeArray)){
						textToVoiceSynthesizer('Task completion codes are created at the workspace level using the Workspace Settings menu by the project owner. Codes applicable to the entire enterprise can be created by the System Administrator using the Sysadmin menu.');
					}else{
						glbConditionInsideVal = 'task';
						textToVoiceSynthesizer('There are various ways to create a task. Under the Task menu, you can create a regular task.  The minimum requirements for a task are the task name. A task must have at least one person assigned to it to be sufficiently defined. In addition, one can specify optional parameters like start and completion date, priority and estimated completion time. A task can be assigned to more than one person. Want to create a task?');
					}
				}else if(checkTheKeyAvailability(text,createWorkflowTemplateArray)){
					 textToVoiceSynthesizer('Workflow Templates are created by the workspace owner under the workspace settings. These are available to all the team members of the workspace. You can also create your own templates from a workflow. Select the check box for save as template when you create a workflow. ');
				}else if(checkTheKeyAvailability(text,createWorkflowArray)){
					 textToVoiceSynthesizer('A workflow is created under the Task menu. Select the Workflow option. To add additional steps, click on the + button. You can change the order of the steps by selecting the forward or back buttons in the step. Type over the Step name to specify your custom task name. ');
				}else if(checkTheKeyAvailability(text,epicArray)){
					 textToVoiceSynthesizer('Under the Agile Definition menu, enter your text in the Epic definition box. Epics are shown in red color edge text boxes.');	 
				}else if(checkTheKeyAvailability(text,featureArray)){
					 textToVoiceSynthesizer('Under the Agile Definition menu select the Add Feature menu in the More option for a specific Epic. Enter the Feature in the text box. Features are shown in purple color edge text boxes.');
				}else if(checkTheKeyAvailability(text,storyArray)){
					 textToVoiceSynthesizer('Under the Agile Definition menu select the Add Story menu in the More option for a specific Epic or Feature. Enter the Story definition in the text box. Stories are shown in yellow color edge text boxes. ');
				}else{ 
					 textToVoiceSynthesizer('How can I help you. Try saying, how to create a workspace or how to create a workflow template. ');
				}
		   }else{
				 if(checkTheKeyAvailability(text,getStarted)){
					 textToVoiceSynthesizer(" For team related activities, choose an existing workspace or create a new workspace. Once you are in a workspace, you will be able to add conversations, create tasks and workflows, work with documents, manage ideas and projects. You can invoke the voice assistant at any time by saying the keyword and commands like 'How do I'.. 'What is'.. 'Create'.. 'Go To'.. For example. Say Cassandra create workspace.");
				 }else if(checkTheKeyAvailability(text,HelpSearchArray)){
					 textToVoiceSynthesizer('Specify the search phrase In the search bar for Global Search at the top of the screen. Click on the desired item from the search results to navigate to the source of the item. Would you like to do a deep search?  Try saying Cassandra, search and then the phrase.');
				 }else if(checkTheKeyAvailability(text,pricingArray)){
					 textToVoiceSynthesizer('Subscription plan based on per user per month fee. Larger organizations will be provided customized pricing.');
				 }else if(checkTheKeyAvailability(text,HelpProfilePicArray)){
					 textToVoiceSynthesizer('Select the Profile Options menu. You can upload a picture from your computer phone, or select a prior loaded picture from the gallery, Profile Pictures folder. ');
				 }else if(checkTheKeyAvailability(text,HelpPasswordArray)){
					 textToVoiceSynthesizer('Under the Profile Options menu select the Options tab and type in your new password. ');
				 }else if(checkTheKeyAvailability(text,HelpNotificationArray)){
					 textToVoiceSynthesizer('Under the Profile Options menu select the Options tab. Select your desired notification preference - Every notification, Daily digest or No notification. ');
				 }else if(checkTheKeyAvailability(text,HelpNotificationHistoryArray)){
					 textToVoiceSynthesizer('Select the Notifications menu next to your profile picture at the top of the screen. All the notifications are listed by date. Select the specific notification that you wish to see. ');
				 }else if(checkTheKeyAvailability(text,HelpMenuHelpArray)){
					 textToVoiceSynthesizer('On every screen there are i buttons. Depending upon the context you will be led by step by step');
				 }else if(checkTheKeyAvailability(text,HelpHomeScreenArray)){
					 textToVoiceSynthesizer('Try saying, Home or Return, or click on the company logo on the top left of the screen to return to the home page. You can also access this from the main menu. ');
				 }else if(checkTheKeyAvailability(text,HelpSwitchWorkspaceArray)){
					 textToVoiceSynthesizer('Select the list of workspaces next to the current workspace icon. Choose the workspace you want to switch to. ');
				 }else if(checkTheKeyAvailability(text,conversationArray)){
					 textToVoiceSynthesizer('Use the microphone in the options menu for a conversation thread. Record your message and post it.');
				 }else if(checkTheKeyAvailability(text,whiteBoardArray)){//white board
					 textToVoiceSynthesizer('During an audio or video call, select the white board icon. Both sides can draw and share their ideas simultaneously on a white board using the tools from the tool bar. White boards can be saved as files.');
// Reorder clause 					
				 }else if(checkTheKeyAvailability(text,reorderArray)){
					 if((checkTheKeyAvailability(text,epicArray)) || (checkTheKeyAvailability(text,featureArray)) || (checkTheKeyAvailability(text,storyArray))){
						 textToVoiceSynthesizer('Use the drag and drop feature by selecting the story box at the colored end. Alternately, use the more menu option for a story to reorder it');
					 }else if(checkTheKeyAvailability(text,ideaArray)){//white board
						 textToVoiceSynthesizer('Use the drag and drop feature by selecting the idea box at the colored end. Alternately, use the more menu option for an idea to reorder it');
					 }else{
						 textToVoiceSynthesizer('Hmmmm... I am not able to help you with that request');
					 }
				 }else if(checkTheKeyAvailability(text,audioVideoArray)){//audio video
					 if(checkTheKeyAvailability(text,shareArray)){
						 textToVoiceSynthesizer('You may need to install the Colab us extension for your browser if you are using Google Chrome. Once installed, you will be able to share your screen with others using the C Me communicator from Colabus. Select the share screen icon and specify the screen or desktop to share. You can stop the screen sharing by toggling the screen sharing icon.');
					 }else{
						 textToVoiceSynthesizer('From the C Me communicator, select the person and select the type of call. You will need to enable your microphone and camera on your computer or phone');
					 }
				 }else if(checkTheKeyAvailability(text,instantArray)){//instant message
					 if(checkTheKeyAvailability(text,messageArray)){
							 textToVoiceSynthesizer('Use the C Me communicator. Select the person and type in your message');
					 }else{
							textToVoiceSynthesizer('Hmm... I am not able to help you with that request');
					 }
				 }else{
					 textToVoiceSynthesizer('How can I help you. Try saying, how to create a workspace or how to create a workflow template');
				 }
			}
// what help voice commands begin...
		 }else if(checkTheKeyAvailability(text,HelpDefinitionArray)){
			if(checkTheKeyAvailability(text,taskArray)){
				// what - with the different type of task (e.g 'task icon', task type, task status, my zone task 
				 if(checkTheKeyAvailability(text,iconsArray) || checkTheKeyAvailability(text,typeArray)){ //task icon, task type 
					 textToVoiceSynthesizer('Task Type icons show you the source for the task. Regular tasks are shown as a task pin. Hovering the mouse over the icon will show the source of creation of the task as a tooltip.');
				 }else if(checkTheKeyAvailability(text,taskTrendArray)){ //task trend
					 textToVoiceSynthesizer('Algorithms determine the trend of the task based on the inputs of persons performing the task. This will provide a visual guidance to take a closer look at tasks that may have problems.');
				 }else if(checkTheKeyAvailability(text,statusArray)){ //task status
					 textToVoiceSynthesizer('A completed task is shown with a green check mark. Tasks in progress are in orange. Tasks overdue are shown in Red. Tasks with an inverted yellow triangle mean that the task has not be sufficiently defined, and hence unassigned');
				 }else if(checkTheKeyAvailability(text,myzoneHelpArray)){ //My tasks: (or 'tasks in myzone' , 'myzone tasks')
					 textToVoiceSynthesizer('Tasks that pertain to you are shown under MyZone Tasks. Here you can see tasks that are assigned to you or were created by you for others, regardless of the workspace. You can also view the entire history of tasks that are associated with you.');
				 }else if(checkTheKeyAvailability(text,storyArray)){  //what's task for a story
					 textToVoiceSynthesizer('Task is set of related activities to be completed to fulfil the story layer of a feature or an epic. It can have start date, completion date, priority and a list of assignees.');
				 }else if(checkTheKeyAvailability(text,taskArray)){  //what's task
					 textToVoiceSynthesizer('A task is an activity or piece of work as part of larger project. A task must have at least one person assigned to it to be sufficiently defined. In addition, one can specify optional parameters like start and completion date, priority and estimated completion time. A task can be assigned to more than one person.');
				 }else{
					 textToVoiceSynthesizer('Hmmmm... what can i do with task. Try saying, what is task icon or task trend'); 
				 }
				// what continues....
		  }else if(checkTheKeyAvailability(text,colabusHelpArray)){
				 textToVoiceSynthesizer('Colabus is an integrated platform for content, messaging, task, project and resource management. Through effective collaboration, it enables digital transformation to become an agile business');  
		  }else if(checkTheKeyAvailability(text,WorkspaceArray) && checkTheKeyAvailability(text,statusArray) ){
				 // this was introduced to progress with the voice reply for project status - 10/30/2018
				 if(text.indexOf('workspace') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				 }else if(text.indexOf('project') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				 }
				 
				 if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','readstatus');
		  }else if(checkTheKeyAvailability(text,WorkspaceArray) && (checkTheKeyAvailability(text,trendArray) || checkTheKeyAvailability(text,analyticsSentiment)) ){
				 if(text.indexOf('workspace') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				 }else if(text.indexOf('project') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				 }

				 if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
					
				 /*whats the trend or sentiment of existing project */
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','totaltaskscount');
				glbConditionInsideVal='totaltaskscount';
				firsttime='Y';
		 }else if(checkTheKeyAvailability(text,teamzoneHelpArray)){
			 textToVoiceSynthesizer('Team Zone is where you can collaborate and work with your team members. You can start new projects, view and update existing projects, add or view team members who can participate on projects and quickly access recent projects');
		 }else if(checkTheKeyAvailability(text,myzoneHelpArray)){
			 textToVoiceSynthesizer('My Zone is where you can access things that pertain to you, regardless of the workspace');
		 }else if(checkTheKeyAvailability(text,enterprisezoneHelpArray)){
			 textToVoiceSynthesizer('Enterprise Zone is for things that span across workspaces or have system wide impact. You can get reports and dashboards, perform analytics using global metrics. System Admin is a console to view and update platform level settings that affect system parameters for the entire enterprise');
		 }else if(checkTheKeyAvailability(text,conversationArray)){
			 textToVoiceSynthesizer('Conversations is a convenient method for team communication. This is better than using emails. Conversations are threaded and better organized to keep track of things');
		 }else if(checkTheKeyAvailability(text,analyticsRecent)){
			 textToVoiceSynthesizer('The last few workspaces visited are listed and is a convenient way to access them.');
		 }else if(checkTheKeyAvailability(text,documentArray)){
			 textToVoiceSynthesizer('Colabus offers a cloud based storage for documents. Optionally, you can configure other cloud storage systems and on-premise systems that a team uses for storing documents for a workspace');
		 }else if(checkTheKeyAvailability(text,ideaArray)){
			 textToVoiceSynthesizer('The Ideas module provides a convenient way to manage ideas within a workspace separately. Ideas can be broken down into multiple levels of sub-ideas and rearranged. Research, documentation, comments and tasks relevant to the idea can be cataloged.');
		 }else if(checkTheKeyAvailability(text,epicArray)){
			 textToVoiceSynthesizer('An Epic is a higher abstraction of requirements. It can be composed of Features and Stories. In Colabus, the Epic shows the summarized status of stories - Number of stories in Backlog, In-progress, Blocked, Completed, On-hold and Cancelled.');
		 }else if(checkTheKeyAvailability(text,sprintGroupArray)){
			 textToVoiceSynthesizer('Multiple sprints for a project can be worked on simultaneously using sprint groups, which is a grouping of sprints that belong to a specific track. For example you can have groups like Web Development, Mobile Development, Marketing etc.');
		 }else if(checkTheKeyAvailability(text,galleryArray)){
			 textToVoiceSynthesizer('You can upload photos in the Gallery. Under the Profile Pictures folder, you can access default images to use for workspace icons or your profile picture. Your uploaded profile and workspace pictures are also stored here');
		 }else if(checkTheKeyAvailability(text,journalsArray)){
			 textToVoiceSynthesizer('You can create your own journals or blogs that you can choose to share with others in READ or WRITE modes. These can contain text, pictures or videos. You can embed Youtube videos too');
		 }else if(checkTheKeyAvailability(text,notesArray)){
			 textToVoiceSynthesizer('You can have different notebooks to take notes and make to-do lists. These notebooks can be shared with select persons in either Read or Write mode.');
		 }else if(checkTheKeyAvailability(text,recommendationArray)){
			 textToVoiceSynthesizer('The Recommendations show relevant artifacts like meeting minutes, documents, conversations, tasks, recordings etc. that may be relevant during the call or interaction with the other person.');
		 }else if(checkTheKeyAvailability(text,createWorkflowTemplateArray)){
			 textToVoiceSynthesizer('A workflow template is a sample workflow that consists of set of predefined steps. These templates can be designed by project owners. And team members can choose a template and customize the steps according to the business needs.');
		 }else if(checkTheKeyAvailability(text,createWorkflowArray)){
			 textToVoiceSynthesizer('A workflow is a series of tasks in a specific order. Workflows are ideal for repetitive task sequences to ensure consistency. A particular task step in a workflow may also invoke another workflow.');
		 }else if(checkTheKeyAvailability(text,teamMemberArray)){
			 textToVoiceSynthesizer('There are three types of team members for a workspace. Workspace or project owners who are the administrators for the workspace, Team members who are active participants and Observers who are passive participants.');
		 }else if(checkTheKeyAvailability(text,readyfeatureArray)){
			 textToVoiceSynthesizer("Colabus has been designed for the enterprise. Some of the enterprise ready features are encrypted security, single sign on, on-premise storage and private cloud storage options, role based access control etc. We have adopted the best practices based on enterprise ready IO.");
		 }else if(checkTheKeyAvailability(text,featureArray)){
			 textToVoiceSynthesizer("A feature is a collection of related stories that can act as a strategy layer to the execution. A feature can be categorized as 'Backlog', 'Build', 'Deploy' and 'Completed' depending on the layer it relates to an epic.");
		 }else if(checkTheKeyAvailability(text,storyArray)){
			 textToVoiceSynthesizer("A story is often written from the perspective of an end user of the system of a need. A story may result in multiple tasks. The execution of a story is accomplished during a sprint.");
		 }else if(checkTheKeyAvailability(text,sprintArray)){
			 textToVoiceSynthesizer("Sprint is definite period of time within which a set of stories can be completed. In general, a sprint can last from 1 week to 6 months period.");
		 }else if(checkTheKeyAvailability(text,HelpNotificationArray)){
			 textToVoiceSynthesizer("Notification preference allows you to choose type of notification you wish to receive from Colab us. This can be set using the  Profile Option menu by selecting one of the options -  All messages, important or Daily Digest.");
		 }else if(checkTheKeyAvailability(text,actionableArray)){
			 textToVoiceSynthesizer("Colabus learns how your organization works, providing insights on organizational performance, key influencers within groups, project delay estimates, phase gate compliance projections and other key metrics to maximize productivity of your team.");
		 }else if(checkTheKeyAvailability(text,benefitsArray)){
			 textToVoiceSynthesizer("Colabus provides enhanced operational visibility, knowledge retention, over 40% productivity gains, elimination of email chains, real time execution rhythm, actionable insights and tools to make good decisions faster.");
		 }else if(checkTheKeyAvailability(text,onboardingArray)){
			 textToVoiceSynthesizer("One person needs to use the registration process to sign up the company. After this other users can be created by the system admin. There is the capability to upload all the users using CSV files. Users of the system can use context sensitive help on all the screens. Also help is available using the voice assistant.");
		 }else{ 
			   textToVoiceSynthesizer('Tell me what can i do for you. Try saying, what is team zone or what does sprint group mean.'); 
		 }
//what ends...here  
// Redirecting To create a new workspace , existing workspace, to various reports (e.g sentiment analysis, workspace status, 
		 }else if(checkTheKeyAvailability(text,analyticsResourceUtilizationPercentage)){
			 textToVoiceSynthesizer('Okay. Here is the report of resource utilization percentage ');
			 loadAnalyticsData("utilizationPerc");
		 }else if(checkTheKeyAvailability(text,taskmyArray)){
			 textToVoiceSynthesizer('Okay. I am going to the Task Creation form'); 
			 loadMyzoneData('myTask','voiceCreate'); 
		 }else if(checkTheKeyAvailability(text,analyticsUtilization)){
			 textToVoiceSynthesizer('Okay. Here is the report of utilization'); 
			 loadAnalyticsData("utilization");
		 }else if(checkTheKeyAvailability(text,analyticsTodayReport)){
			 textToVoiceSynthesizer('Okay. I am going to today report or Daily status report'); 
			 loadAnalyticsData("todayrep");
// navigate to sentiment analysis based on recent, green and red begins here...
		 }else if(checkTheKeyAvailability(text,analyticsSentiment)){
			 if(checkTheKeyAvailability(text,analyticsRed)){
				 textToVoiceSynthesizer('Okay. Here is the sentiment report with the order of red to green'); 
				 loadAnalyticsData("sentimentRtoG"); 
			 }else if(checkTheKeyAvailability(text,sentimentSearch)){
				 if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				 }else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				 }
				 
				 if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				 }
						
				 redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','sentimentscore');
			 }else if(checkTheKeyAvailability(text,analyticsRecent)){
				 textToVoiceSynthesizer('Okay. Here is the sentiment score of recently visited Workspace or project');
				 loadAnalyticsData("sentimentRecent"); 
			 }else if(checkTheKeyAvailability(text,analyticsGreen)){
				 textToVoiceSynthesizer('Okay. Here is the sentiment report in the order of green to red ');
				 loadAnalyticsData("sentimentGtoR"); 
			 }else{
				 textToVoiceSynthesizer('Which sentiment you are looking for. Try saying, sentiment in red or sentiment of recently updated projects');
			 }
// End of navigate to sentiment analysis based on recent, green and red...
// Listing and navigating to tasks, document, journals, message 
		 }else if(checkTheKeyAvailability(text,listArray)){ // entire goto list begins here. this will match to "list","show","go to","open"
			 if (checkTheKeyAvailability(text,settingArray)){ // matching to "project setting","workspace setting"
					if(text.indexOf('workspace setting') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace setting')+17, text.length).trim();
					}else if(text.indexOf('project setting') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project setting')+15, text.length).trim();
					}
					
					if(text.indexOf(lastValueWrkspace) != -1){
						if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
						else
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
					}
						
					redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','projectDetails');
			 }
		 else if (checkTheKeyAvailability(text,detailsArray)){ // matching to workspace details of a project or workspace
			 if(checkTheKeyAvailability(text,WorkspaceRedirect)){
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','projectDetails');
			 }
		 } 
		else if (checkTheKeyAvailability(text,statusArray)){ // matching to status details of team zone.
			 if(checkTheKeyAvailability(text,WorkspaceRedirect)){
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','status');
			 }
		} 
		else if ((checkTheKeyAvailability(text,teamMemberArray)) || (checkTheKeyAvailability(text,recentWorkspaceTeam))){ // Matching to team members of team zone.
			 if(checkTheKeyAvailability(text,WorkspaceRedirect)){
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','inviteUser');
			 }
		}
		else if (checkTheKeyAvailability(text,approvalArray)){ // matching to approval details of team zone.
			 if(checkTheKeyAvailability(text,WorkspaceRedirect)){
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','apporval');
			 }
		 } 
		 else if (checkTheKeyAvailability(text,createWorkflowTemplateArray)){ // matching to workflow template details of team zone.
			 if(checkTheKeyAvailability(text,WorkspaceRedirect)){
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','workFlow');
			 }
		 }   
		 else if (checkTheKeyAvailability(text,completionCodeArray)){ // matching to task completion codes details of team zone.
			 if(checkTheKeyAvailability(text,WorkspaceRedirect)){
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','taskCodes');
			 }
		 } 
		 else if (checkTheKeyAvailability(text,optionalDriveArray)){ // matching to optional drives details of team zone.
			 if(checkTheKeyAvailability(text,WorkspaceRedirect)){
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','optionalDrives');
			 }
		 } 
		 else if (checkTheKeyAvailability(text,enterpriseIntergrationArray)){ // matching to integration details of team zone.
			 if(checkTheKeyAvailability(text,WorkspaceRedirect)){ 
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','integration');
			 }
		 }    
		 else if(checkTheKeyAvailability(text,createArray)){
			 textToVoiceSynthesizer('Okay. I am going to the Start New workspace'); 
			 createNewProject(); 
		 }else if(checkTheKeyAvailability(text,existingWorkspaceArray)){
			 textToVoiceSynthesizer('Okay. I am going to the Existing workspace'); 
			 loadProjectData(''); 
		 }else if(checkTheKeyAvailability(text,analyticsWorkspaceStatusReport)){
			 textToVoiceSynthesizer('Okay. Here is the report of workspace status report ');
			 loadAnalyticsData("workspacestatus");
		 }else if(checkTheKeyAvailability(text,recentWorkspaceConver)){
			   if(checkTheKeyAvailability(text,WorkspaceRedirect)){
					if(text.indexOf('workspace') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
					}else if(text.indexOf('project') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
					}
					
					if(text.indexOf(lastValueWrkspace) != -1){
						if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
						else
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
					}
					redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','LoadActivityFeed');
			 }else{
					 redirectWrkSpace('','LoadActivityFeed','');
			 }
		 }else if(checkTheKeyAvailability(text,recentWorkspaces)){
				   redirectWrkSpace('','LoadActivityFeed','');
		 }else if(checkTheKeyAvailability(text,recentWorkspaceTeam)){
			  if(checkTheKeyAvailability(text,WorkspaceRedirect)){
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','loadTeamjsp');
			 }else{
				 redirectWrkSpace('','loadTeamjsp','');
			 }
		 }else if(checkTheKeyAvailability(text,recentWorkspaceEmail)){
				if(checkTheKeyAvailability(text,WorkspaceRedirect)){
					if(text.indexOf('workspace') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
					}else if(text.indexOf('project') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
					}
					
					if(text.indexOf(lastValueWrkspace) != -1){
						if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
						else
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
					}
					redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','loadEmailjsp');
			   }else{
				 redirectWrkSpace('','loadEmailjsp','');
			   }
		}else if(checkTheKeyAvailability(text,recentWorkspaceIdea)){
				if(checkTheKeyAvailability(text,WorkspaceRedirect)){
					if(text.indexOf('workspace') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
					}else if(text.indexOf('project') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
					}
					
					if(text.indexOf(lastValueWrkspace) != -1){
						if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
						else
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
					}
					redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','iMenu');
				}else{
					redirectWrkSpace('','iMenu','');
				}
		}else if(checkTheKeyAvailability(text,recentWorkspaceAgileDefinition)){
				if(checkTheKeyAvailability(text,WorkspaceRedirect)){
					if(text.indexOf('workspace') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
					}else if(text.indexOf('project') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
					}
					
					if(text.indexOf(lastValueWrkspace) != -1){
						if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
						else
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
					}
					redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','storyMenu');
				}else{
					redirectWrkSpace('','storyMenu','');
				} 
		}else if(checkTheKeyAvailability(text,recentWorkspaceAgileSprint)){
				if(checkTheKeyAvailability(text,WorkspaceRedirect)){
					if(text.indexOf('workspace') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
					}else if(text.indexOf('project') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
					}
					
					if(text.indexOf(lastValueWrkspace) != -1){
						if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
						else
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
					}
					redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','sprintMenu');
				}else{
					redirectWrkSpace('','sprintMenu','');
				}
		}else if(checkTheKeyAvailability(text,taskmyArray)){
				 textToVoiceSynthesizer('Okay. I am going to the tasks'); 
				 loadMyzoneData('myTask'); 
		}else if(checkTheKeyAvailability(text,recentWorkspaceTask)){
				if(checkTheKeyAvailability(text,WorkspaceRedirect)){
					if(text.indexOf('workspace') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
					}else if(text.indexOf('project') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
					}
					
					if(text.indexOf(lastValueWrkspace) != -1){
						if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
						else
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
					}
					redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','loadTaskjsp');
				}else{
					redirectWrkSpace('','loadTaskjsp','');
				}
		}else if(checkTheKeyAvailability(text,documentMyArray)){
				textToVoiceSynthesizer('Okay. I am going to the documents. You can open your favourite document folders and upload any files to the document folders'); 
				loadMyzoneData('myDocument');
		}else if(checkTheKeyAvailability(text,recentWorkspaceDocument)){
				if(checkTheKeyAvailability(text,WorkspaceRedirect)){
					if(text.indexOf('workspace') != -1){
						lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
					}else if(text.indexOf('project') != -1){
						lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
					}
					
					if(text.indexOf(lastValueWrkspace) != -1){
						if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
						else
							lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
					}
					redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','loadDocumentjsp');
				}else{
					redirectWrkSpace('','loadDocumentjsp','');
				}
		}else if(checkTheKeyAvailability(text,WorkspaceRedirect)){
				if(text.indexOf('workspace') != -1){
					lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
				}else if(text.indexOf('project') != -1){
					lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
				}
				
				if(text.indexOf(lastValueWrkspace) != -1){
					if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
					else
						lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
				}
				redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','LoadActivityFeed');
		}else if(checkTheKeyAvailability(text,galleryArray)){
				textToVoiceSynthesizer('Okay. I am going to the gallery'); 
				loadMyzoneData('myGallery');
		}else if(checkTheKeyAvailability(text,journalsArray)){
				textToVoiceSynthesizer('Okay. I am going to the journals'); 
				loadMyzoneData('myBlog');
		}else if(checkTheKeyAvailability(text,messageArray)){
				textToVoiceSynthesizer('Okay. I am going to the message'); 
				loadMyzoneData('myMessage');
		}else if(checkTheKeyAvailability(text,scrumArray)){
				textToVoiceSynthesizer('Okay. I am going to the all my scrums'); 
				loadMyzoneData('myAllScrums');
		}else if(checkTheKeyAvailability(text,notesArray)){
				textToVoiceSynthesizer('Okay. I am going to the notes'); 
				loadMyzoneData('myNote');
		}else if(checkTheKeyAvailability(text,contactArray)){
				 textToVoiceSynthesizer('Okay. I am going to the Contacts'); 
				 loadContactData();
		}else if(checkTheKeyAvailability(text,homeArray)){
				 textToVoiceSynthesizer('Okay. Going back to the. home page');
				 openMenuMethod('home');
		}else if(checkTheKeyAvailability(text,enterpriseAnalyticsArray)){
				 textToVoiceSynthesizer('Okay. I am going to the analytics'); 
				 loadAnalyticsData();
		}else if(checkTheKeyAvailability(text,enterpriseSystemArray)){
				 textToVoiceSynthesizer('Okay. I am going to the system admin'); 
				 loadSysAdminData();
		}else if(checkTheKeyAvailability(text,enterpriseIntergrationArray)){
				 textToVoiceSynthesizer('Okay. I am going to the intergrations'); 
				 loadIntegration('colabusIntegration');
		}else if(checkTheKeyAvailability(text,HelpNotificationHistoryArray)){
				 textToVoiceSynthesizer('Okay. I am going to the notification history'); 
				 window.location.href = path+"/Redirect.do?pAct=notifications";
		}else if(checkTheKeyAvailability(text,myProfileUser)){
				 textToVoiceSynthesizer('Okay. I am going to the my profile'); 
				 openProfile();
		}else if(checkTheKeyAvailability(text,chatCme)){
				 textToVoiceSynthesizer('Okay. I am going to the C me'); 
				 $('#chatNotMain').trigger('click');
		}else{ 
				 textToVoiceSynthesizer('What do you want me to list. Try saying, list tasks or go to my task'); 
		}
		}else if(checkTheKeyAvailability(text,WorkspaceArray)){
			 if(checkTheKeyAvailability(text,createArray)){
				 textToVoiceSynthesizer('Okay. I am going to the Start New workspace'); 
				 createNewProject(); 
			 }else if(checkTheKeyAvailability(text,existingWorkspaceArray)){
				 textToVoiceSynthesizer('Okay. I am going to the Existing workspace'); 
				 loadProjectData(''); 
			 }else if(checkTheKeyAvailability(text,analyticsRed)){
				 textToVoiceSynthesizer('Okay. Here is the list of projects or workspace in the order of red to green'); 
				 loadAnalyticsData("workspaceRtoG");
			 }else if(checkTheKeyAvailability(text,analyticsRecent)){
				 textToVoiceSynthesizer('Okay. Here is the status of recently visited workspaces or project. ');
				 loadAnalyticsData("workspaceRecent"); 
			 }else if(checkTheKeyAvailability(text,analyticsGreen)){
				 textToVoiceSynthesizer('Okay. Here is the status report of workspaces  in the order of green to red ');
				 loadAnalyticsData("workspaceGtoR"); 
			 }else{
				 textToVoiceSynthesizer('Sorry. I cant understand. Try saying, projects in RED or Status of projects recently updated.');
			 }
	   }else if(checkTheKeyAvailability(text,contactArray)){
			 textToVoiceSynthesizer('Okay. I am going to the Contacts'); 
			 loadContactData();
	   }else if(checkTheKeyAvailability(text,homeArray)){
			 textToVoiceSynthesizer('Okay. Going back to the home page');
			 openMenuMethod('home');
	   }else if(checkTheKeyAvailability(text,searchArray)){
				globalSearchFunctionCall();
	   }else if(checkTheKeyAvailability(text,createArray)){//cassandra create
				textToVoiceSynthesizer("To create a new workspace, say 'create workspace' or to open an existing workspace say 'go to existing workspace'. For example. Say Cassandra. Create workspace.");
	   }else if(checkTheKeyAvailability(text,NoValueArray)){
			 textToVoiceSynthesizer('Please type your query ',false);
	   }else{
			 textToVoiceSynthesizer('Sorry. I cant understand. Please Try it again'); 
	   }
	}else{ 
	   $('#idToDisplaySpeechText').val('').removeAttr('title');
	}
	
	 if(checkTheKeyAvailability(text,WorkspaceArray) && firsttime == 'N') {
		 if(text.indexOf('workspace') != -1){
			lastValueWrkspace = text.substring(text.indexOf('workspace')+9, text.length).trim();
		}else if(text.indexOf('project') != -1){
			lastValueWrkspace = text.substring(text.indexOf('project')+7, text.length).trim();
		}
		 
		 if(text.indexOf(lastValueWrkspace) != -1){
			if(text.substring(text.indexOf(lastValueWrkspace)).startsWith("#"))
				lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace)+1, text.length).trim();
			else
				lastValueWrkspace = text.substring(text.indexOf(lastValueWrkspace), text.length).trim();
		}
		redirectWrkSpace(lastValueWrkspace,'goToWorkSpaceVoice','totaltaskscount');
	}
	
	if(checkTheKeyAvailability(text,YesArray)){
			switch(glbConditionInsideVal){
			case 'workspace':
				{
				 textToVoiceSynthesizer('Okay. I am going to the Start New workspace for you',false); 
				 createNewProject(); 
				 glbConditionInsideVal='';
				 break;
				 }
			case 'task':
				{
				textToVoiceSynthesizer('Okay. I am going to the Task Creation form',false); 
				loadMyzoneData('myTask','voiceCreate'); 
				textToVoiceSynthesizer('go ahead and enter the required details to create task. The minimum requirements for a task are the task name. Completion date. and one or more persons to be assigned',false);
				glbConditionInsideVal='';
				break;
				 }
			case 'totaltaskscount':
				{
				loadAnalyticsData("searchByName",lastValueWrkspace,WeekTrend); 
				glbConditionInsideVal='';
				firsttime='N';
				break;
				}
			}
		}
	
	
	if(checkTheKeyAvailability(text,NoArray) || checkTheKeyAvailability(text,NoValueArray)){
		/*$('#newProjContent').hide();
		$('#transparentDiv').hide();*/
		firsttime='Y';
	}
} 



	function globalSearchFunctionCall(){
		var searchTxt=$('#idToDisplaySpeechText').val();
		var searchTxtArray = ["search","find","locate"];
		var resultSearch = '';
		searchTxtArray.every(function(item){
		let n = searchTxt.indexOf(item);
		if(n != -1){
			let nlength = item.length;
			resultSearch = searchTxt.substr(n+nlength,searchTxt.length);
			return false;
		}else{
			return true;
			}
		});
		$("#glbSearchInputBox").val(resultSearch);
		textToVoiceSynthesizer('Okay. I am searching'+resultSearch);
		globalSearchRedirect();
	}
	function validationForSpeechExit(){
		recognizationToggleStop();
			synth.volume = 1;
			synth.rate = 0.5;
			synth.pitch = 1;
			utterThis = new SpeechSynthesisUtterance("Thank You. Bye");
			for(i = 0; i < speechSynthesisVoices.length ; i++) {
				if(speechSynthesisVoices[i].name ===  "Google US English") {
				utterThis.voice = speechSynthesisVoices[i];
			 }
			}
		
			synth.speak(utterThis);
		
			utterThis.onend = function(event) {
		   console.log('Utterance has finished being spoken');
			   //console.log('Utterance has finished being spoken');
		   $('#idToDisplaySpeechText').val('').removeAttr('title');
		}
   }
}


/* This function is for the creation of Highlights through voice */
function highLightUsingVoice(type){
	 //var StartVoice=["text","cassandra","cassendra"];
	 var hLightnextArray = ["next","continue","skip"];
	 var hLightCreateArray = ["create","add","save","post"];
	 var hLightCancelArray = ["cancel","clear"];
	 var hLightTimerArray = ["timer start","timer stop","timer lock","timer unlock"];

	  if(type == 'msgName'){
		  highLightNameSpeechNew = $('#hMsg').val();
	  }else{
			highLightDescSpeechNew = $('#hLightDesc').val();
	  }
	 var replaceKeyValuesHname = $('#hMsg').val($('#hMsg').val().replaceAll('next','').replaceAll('continue','').replaceAll('skip','').replaceAll('create','').replaceAll('add','').replaceAll('cancel','').replaceAll('clear','').replaceAll('timer start','').replaceAll('timer stop','').replaceAll('save','').replaceAll('timer lock','').replaceAll('timer unlock','').replaceAll('post',''));
	 var replaceKeyValuesHdesc = $('#hLightDesc').val($('#hLightDesc').val().replaceAll('next','').replaceAll('continue','').replaceAll('skip','').replaceAll('create','').replaceAll('add','').replaceAll('cancel','').replaceAll('clear','').replaceAll('timer start','').replaceAll('timer stop','').replaceAll('save','').replaceAll('timer lock','').replaceAll('timer unlock','').replaceAll('post','')); 
	  if(checkTheKeyAvailability(highLightNameSpeechNew,hLightCreateArray)){
			  replaceKeyValuesHname;
			  $('#hLightSave').trigger('click');
			return;
	  }else if(checkTheKeyAvailability(highLightDescSpeechNew,hLightCreateArray)){
			  replaceKeyValuesHdesc;
			$('#hLightSave').trigger('click');
			return;
	  }else if(checkTheKeyAvailability(highLightNameSpeechNew,hLightTimerArray)){
			  replaceKeyValuesHname;
			  $('#hLightLockTime').trigger('click');
			return;
	  }else if(checkTheKeyAvailability(highLightDescSpeechNew,hLightTimerArray)){
			  replaceKeyValuesHdesc;
			$('#hLightLockTime').trigger('click');
			return;
	  }else if(checkTheKeyAvailability(highLightNameSpeechNew,hLightCancelArray)){
			  replaceKeyValuesHname;
			  $('#hLightCancel').trigger('click');
			return;
	  }else if(checkTheKeyAvailability(highLightDescSpeechNew,hLightCancelArray)){
			  replaceKeyValuesHdesc;
			$('#hLightCancel').trigger('click');
			return;
	  }else if(checkTheKeyAvailability(highLightNameSpeechNew,hLightnextArray)){
			  replaceKeyValuesHname;
			$("#hMsg").css('border','');
			$("#hMsg").attr('highLightMsg','');
			$("#hLightDesc").attr('highLightMsg','active');
			$("#hLightDesc").css('border','1px solid red').focus();
	  }else if(checkTheKeyAvailability(highLightDescSpeechNew,hLightnextArray)){
			  replaceKeyValuesHdesc;
			$("#hLightDesc").css('border','');
	  }else{
			  
	  }
}



function commentsUsingVoiceToText(threadTypes){
	 var commentSaveArray = ["cassandra create","cassandra save","cassandra post","casandra create","casandra save","casandra post","text post"];
	 var commentClearArray = ["cancel","clear"];
	 var replaceKeyValuesCommMain = "";
	 var replacekeyValuesCommSub = "";
	 var replacekeyValuesCommsubReply = "";
	 var replaceKeyValuesCommSub2SubReply = "";
	 var replaceKeyValuestaskUserComm = "";
	 
	 
	 
	 if(threadTypes == "mainThread" || threadTypes == "commonMainThread"){
		 commentsVoiceSpeech = $('#main_commentTextarea').val().toLowerCase();
		 replaceKeyValuesCommMain = $('#main_commentTextarea').val(commentsVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post|text post/g,''));
	 }else if(threadTypes == "subThread"){
		 subcommentVoiceSpeech = $('#replyBlock_'+commFeedId).val().toLowerCase();
		 replacekeyValuesCommSub = $('#replyBlock_'+commFeedId).val(subcommentVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post|text post/g,''));
	 }else if(threadTypes == "subReplyThread"){
		 subReplyCommentVoiceSpeech = $('#replyBlock_'+commSubFeedId).val().toLowerCase();
		 replacekeyValuesCommsubReply = $('#replyBlock_'+commSubFeedId).val(subReplyCommentVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post|text post/g,''));
	 }else if(threadTypes == "sub2SubReplyThread"){
		 sub2SubReplyCommentVoiceSpeech = $('#replyBlock_'+commSub2SubFeedId).val().toLowerCase();
		 replaceKeyValuesCommSub2SubReply = $('#replyBlock_'+commSub2SubFeedId).val(sub2SubReplyCommentVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post|text post/g,''));
	 }else if(threadTypes == "userTaskComment"){
		 taskUserCommentVoiceSpeech = $('#UserCommentNewUI').val().toLowerCase();
		 replaceKeyValuestaskUserComm = $('#UserCommentNewUI').val(taskUserCommentVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post|text post/g,''));
	 }else{
		 console.log("Thread Type Block End --> ThreadType");
	 }

	 
//		 replaceKeyValuesCommMain = $('#main_commentTextarea').val(commentsVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post|text post/g,''));
	/* replacekeyValuesCommSub = $('#replyBlock_'+commFeedId).val(subcommentVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post/g,''));
	 replacekeyValuesCommsubReply = $('#replyBlock_'+commSubFeedId).val(subReplyCommentVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post/g,''));
	 replaceKeyValuesCommSub2SubReply = $('#replyBlock_'+commSub2SubFeedId).val(sub2SubReplyCommentVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post/g,''));
	 replaceKeyValuestaskUserComm = $('#UserCommentNewUI').val(taskUserCommentVoiceSpeech.replace(/cassandra save|cassandra create|cassandra post|casandra save|casandra create|casandra post/g,''));*/
	 
	 
	 if(checkTheKeyAvailability(commentsVoiceSpeech,commentSaveArray)){
			 replaceKeyValuesCommMain;
			 $('#mainConvoPost').trigger('click');
			 recognizationToggleStop();
			return;
	  }else if(checkTheKeyAvailability(subcommentVoiceSpeech,commentSaveArray)){
		   replacekeyValuesCommSub;
		   $('#subConvoPost_'+commFeedId).trigger('click');
		   recognizationToggleStop();
		   return;
	  }else if(checkTheKeyAvailability(subReplyCommentVoiceSpeech,commentSaveArray)){
		   replacekeyValuesCommsubReply;
		   $('#subReplyConvoPost_'+commSubFeedId).trigger('click');
		   recognizationToggleStop();
		   return;
	  }else if(checkTheKeyAvailability(sub2SubReplyCommentVoiceSpeech,commentSaveArray)){
		   replaceKeyValuesCommSub2SubReply;
		   $('#subToSubReplyConvoPost_'+commSub2SubFeedId).trigger('click');
		   recognizationToggleStop();
		   return;
	  }else if(checkTheKeyAvailability(taskUserCommentVoiceSpeech,commentSaveArray)){
		   replaceKeyValuestaskUserComm;
		   $('#wsTaskSaveContainer').trigger('click');
		   recognizationToggleStop();
		   return;
	  }else{
		  console.log("Block end--> Comment VTT");
	  }
	
}


var projectListComm = "";
function showProjectListComm(){
	
		$.ajax({
			url : path + "/workspaceAction.do",
			type : "POST",
			data : {
					act : "loadProjDDList",
					userId : userId,
					sortValue : "",
					sortHideProjValue : "",
					ipadVal : "",
					sortType: "",
					txt: "",
					type: "",
				},
				beforeSend: function (jqXHR, settings) {
					xhrPool.push(jqXHR);
				},	
			error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					$("#loadingBar").hide();
					timerControl("");
					}, 
			success : function(result){
			checkSessionTimeOut(result);
			   projectListComm = result;
				//alert("Result::>"+projectListComm);

				var data = projectListComm.split('#@#');
				var projAllList = "";
				if(data[0] !='[]' && data[1] !='[]'){
					projAllList = data[0].substring(0, data[0].length-1)+","+data[1].substring(1,data[1].length);
				}else if(data[0] !='[]'){
					projAllList = data[0];
				}else if(data[1] !='[]'){
					projAllList = data[1];
				}
				projectListComm = projAllList;
			}
		});
}

function redirectWrkSpace(voiceProjName,typeConvo,glbLevelActType) {
	//alert("inside redirect: >"+voiceProjName+"< :");
	var actTypeWrkSpc = typeConvo;
	var globalLevelActType = glbLevelActType;
	var projList = JSON.parse(projectListComm); 
	//console.log("projList:: "+JSON.stringify(projList));
	var flag=false;
	voiceProjName= voiceProjName.trim();
	if(glbLevelActType=="whatsup"){
		// alert("glbLevelActType==> "+glbLevelActType);
		UsrWelcomeQuery();
		glbLevelActType =" ";
		return;
	  }
	if(typeConvo == "goToWorkSpaceVoice"){
		$.each(projList,function(i,proj){
			 var projTitle = proj.ProjTitle;
			 var projCode = proj.projCode;
			 if(voiceProjName!="" && (projTitle.toLowerCase() == voiceProjName || projCode == voiceProjName.replaceAll(" ",""))){
				 //console.log("ProjTitle::>>"+projTitle+"-"+voiceProjName);					 
					flag =true;
					 var projectId = proj.projectID;
					 var projectUsersStatus = proj.projectUsersStatus;
					 if(projectUsersStatus=="PO"){
						 projType = "MyProjects";
					 }else{
						 projType = "SharedProjects";
					 }
					var projName = proj.projName.replaceAll("ch(20)","'");
					var projArchStatus = proj.projectArchiveStatus;
					var projImgSrc = proj.imageUrl;
					
					// below if checks for the glbLevelActType matching with status, project details, team members, optional drives and integration
					if (glbLevelActType=="status" || glbLevelActType=="projectDetails" || glbLevelActType== "inviteUser" || glbLevelActType== "apporval" || glbLevelActType== "workFlow" || glbLevelActType== "taskCodes" || glbLevelActType== "optionalDrives" || glbLevelActType== "integration") {
						loadProjectData(projectId,glbLevelActType);
					}else if(glbLevelActType=="readstatus"){
						voiceValidationQuery(projectId, projTitle);
					}else if(glbLevelActType=="totaltaskscount"){
						projectTasksCount(projectId, projTitle);
					}else if(glbLevelActType=="sentimentscore"){
						textToVoiceSynthesizer('Okay. Here is the sentiment score of project');
						loadAnalyticsData("searchByName",voiceProjName); 
					}else{

						timerControl("start"); 
						$("#loadingBar").show();
						$.ajax({
							url : path + "/workspaceAction.do",
							type : "POST",
							data : {act : "updateRecentProjectData",projectId : projectId},
							error: function(jqXHR, textStatus, errorThrown) {
										checkError(jqXHR,textStatus,errorThrown);
										$("#loadingBar").hide();
										timerControl("");
										},
							success : function(result){
							  checkSessionTimeOut(result);
							  textToVoiceSynthesizer("Ok.");
							  window.location.href = path+"/Redirect.do?pAct="+globalLevelActType+"&projId=" + projectId
								+ "&projType=" + projType + "&projName=" + projName
								+ "&projArchStatus=" + projArchStatus + "&projImgSrc=" + projImgSrc + "&projectUsersStatus=" + projectUsersStatus;
							  
							  $("#loadingBar").hide();
							  timerControl("");
							 
							}
						}); 
					}
					
			 }
			 
		});
	}else{
		flag = true;
		var projTitle = projList[0].ProjTitle;
		var projectId = projList[0].projectID;
		 var projectUsersStatus = projList[0].projectUsersStatus;
		 if(projectUsersStatus=="PO"){
			 projType = "MyProjects";
		 }else{
			 projType = "SharedProjects";
		 }
		var projName = projList[0].projName.replaceAll("ch(20)","'");
		var projArchStatus = projList[0].projectArchiveStatus;
		var projImgSrc = projList[0].imageUrl;
		timerControl("start"); 
		$("#loadingBar").show();
		$.ajax({
			url : path + "/workspaceAction.do",
			type : "POST",
			data : {act : "updateRecentProjectData",projectId : projectId},
			error: function(jqXHR, textStatus, errorThrown) {
						checkError(jqXHR,textStatus,errorThrown);
						$("#loadingBar").hide();
						timerControl("");
						},
			success : function(result){
			  checkSessionTimeOut(result);
			  textToVoiceSynthesizer("Ok.");
			  window.location.href = path+"/Redirect.do?pAct="+actTypeWrkSpc+"&projId=" + projectId
				+ "&projType=" + projType + "&projName=" + projName
				+ "&projArchStatus=" + projArchStatus + "&projImgSrc=" + projImgSrc + "&projectUsersStatus=" + projectUsersStatus;
			  
			  $("#loadingBar").hide();
			  timerControl("");
			 
			}
		}); 
		
	}
	
	if(!flag){
		textToVoiceSynthesizer('Project or workspace is not matching.'); 
	}
	
}

function voiceValidationQuery(projectId,projTitle){
	$.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : {
			act : "queryFormat",
			projectId : projectId
			},
		error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					$("#loadingBar").hide();
					timerControl("");
					},
		success : function(result){
		  checkSessionTimeOut(result);
		  var jsonData = jQuery.parseJSON(result);
		  //alert(""+jsonData[0].projStatus);
		  textToVoiceSynthesizer('Current status of the project "'+jsonData[0].projName+'" is '+jsonData[0].projStatus); 
		  $("#loadingBar").hide();
		  timerControl("");
		}
	}); 	
}

function projectTasksCount(projectId,projTitle){
//alert("inside");
$.ajax({
	url : path + "/workspaceAction.do",
	type : "POST",
	data : {
		act : "projectTasks",
		projectId : projectId
		},
	error: function(jqXHR, textStatus, errorThrown) {
				checkError(jqXHR,textStatus,errorThrown);
				$("#loadingBar").hide();
				timerControl("");
				},
	success : function(result){
	  checkSessionTimeOut(result);
	  var jsonData = jQuery.parseJSON(result);
	  var TextForTrend = '';
	  //alert("json data length"+jsonData.length);
	  if(jsonData.length > 0 && jsonData[0].projectName != 'null') {
		  textToVoiceSynthesizer('  Based on the recent. activity.  the sentiment for  '+jsonData[0].projectName+' is trending ' + jsonData[0].trend + ' with a score of '+ jsonData[0].avgScore+' '); 
		  TextForTrend = 'Based on the recent activity  the sentiment for  '+jsonData[0].projectName+' is trending ' + jsonData[0].trend + ' with a score of '+ jsonData[0].avgScore+'. '; 
		  if (jsonData[0].totalInputs>0){
			   textToVoiceSynthesizer('There were ' + jsonData[0].totalInputs +' inputs ');
			   TextForTrend = TextForTrend + 'There were ' + jsonData[0].totalInputs +' inputs ';
			   if(jsonData[0].concernedInputs>0){
				   textToVoiceSynthesizer(' of. which '+ jsonData[0].concernedInputs+' may be of concern. '); 
				   TextForTrend = TextForTrend + ' of which '+ jsonData[0].concernedInputs+' may be of concern ';
				}
		   WeekTrend='Y';
		   textToVoiceSynthesizer(' Do you want to know more details of this trending... Say Yes');
		   TextForTrend = TextForTrend + '. Do you want to know more details of this trending...Say Yes ';
		  }
		  $('#cassandraContent').text(TextForTrend);
	  } else {
		  textToVoiceSynthesizer('  There are no recent activity for the project.  Anyway, do you still want to know the trending of this project. Say yes... '); 
		  firsttime='Y';
		  WeekTrend='';
	  }  
	  $("#loadingBar").hide();
	  timerControl("");
	 
	}
}); 	
}

function textusingAvCall(resultcall){
	var textCall = resultcall;
	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+' '+time;  
	tranTimer = $("#aTimer").text();
	
	$.ajax({
		url : path+"/ChatAuth",
		type : "POST",
		data : {act : "writefileText",textCall : textCall,transDocId : transDocId, dateTime :dateTime, tranTimer : tranTimer},
		error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					timerControl("");
					},
		success : function(result){
				if(result != "success"){
					console.log("Transcript file write error");
				}else{
					console.log("Transcript Successfully");
				}
		}
	  });
}

function callTransFileCreation(callId){
	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+' '+time;  
	$.ajax({
		url : path+"/ChatAuth",
		type : "POST",
		data : {act : "createTransFile",callId :callId, dateTime : dateTime, fromUser:userId },
		error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					timerControl("");
					},
		success : function(result){
//				console.log("Result for callTrans:::+++++=>"+result);
				if(result != "0"){
					transDocId = result;
					console.log("Created Successfully--");
				}
		}
	  });
}

function lastWord(words) {
	  var n = words.split(/[\s,]+/);
	  return n[n.length - 1];
}


 function transcriptVideo(){
		$('#highlightFor').css({'display':'none'});
		BackFromRecomd();
		$('#transcriptFor').toggle();
		$('#tAddDiv').css({'display':'block'});
		$("#tMsg").focus();
		if($("#transcriptFor").is(":visible") == true) {
		   $("img.avTransImg").attr('src',path+'/images/video/'+$("img.avTransImg").attr('toglsrc'));
		   $("img.avhglightImg").attr('src',path+'/images/video/'+$("img.avhglightImg").attr('defsrc'));
		   backToHighlights();
		   }else{
			  $("img.avTransImg").attr('src',path+'/images/video/'+$("img.avTransImg").attr('defsrc'));
		   }
		$('.hListHeaderItems').click(function(){
			$('#transcriptFor').css({'display':'none'});	 
		});
		checkHighlightWindowSize();
	}
 
function showTranscript(textOutput,uName,sTimer){
	tranTimer = $("#aTimer").text();
	if(uName == userFirstName){
		userFullName = uName;
	}else{
		userFullName = uName;
		tranTimer = sTimer; 
	}
	var newdiv = $("<div id='tran_"+userId+"_"+tranTimer+"' class='transcriptClass' style='float: left;font-size: 14px;padding: 10px 15px 10px 15px;color: #000;width: 100%;border-bottom:1px solid #CED2D5'>"+userFullName+" @ "+tranTimer+" : "+textOutput+"</div>");
	//$('.transcriptClass').append("<img  class='hEditIcon hEditHide' style='width: 22px;margin-left:4px;cursor:pointer;' src='"+path+"/images/video/hedit.png' title='Edit'  onclick='editHightlight("+userId+"_"+tranTimer+");' >");
	$('#tListDiv').append(newdiv);
	$("#tListDiv").animate({
		scrollTop: $('#tListDiv').get(0).scrollHeight}, 1000);
	
}

function UsrWelcomeQuery(){
	//alert("inside");
	$.ajax({
		url : path + "/workspaceAction.do",
		type : "POST",
		data : {
			act : "welcomeMessage"
			},
		error: function(jqXHR, textStatus, errorThrown) {
					checkError(jqXHR,textStatus,errorThrown);
					$("#loadingBar").hide();
					timerControl("");
					},
		success : function(result){
		  checkSessionTimeOut(result);
		  console.log(result);
		  var jsonData = jQuery.parseJSON(result);
		  //alert("json data length"+jsonData.length);
		  if(jsonData.length > 0) {
			 // alert("inside json");
			  if(jsonData[0].loginCount<=5){
				  if(jsonData[0].firstName == "null"){
					  textToVoiceSynthesizer("Hi. I am Cassandra. your Colabus assistant. You can invoke me at any time. by saying my name. followed by a command. Commands can be of the form. 'How do I'?. 'What is'?. 'Create' .or. 'Go To'. For example. 'How do I create a task?' .or. 'Go To Existing Workspace'. Make sure to permit your browser to. use the microphone.");
				  }else{
					  textToVoiceSynthesizer("Hi."+jsonData[0].firstName+" I am Cassandra. your Colabus assistant. You can invoke me at any time. by saying my name. followed by a command. Commands can be of the form. 'How do I'?. 'What is'?. 'Create' .or. 'Go To'. For example. 'How do I create a task?' .or. 'Go To Existing Workspace'. Make sure to permit your browser to. use the microphone.");
				  }
				}
			  if(jsonData[0].TotalNotification !=0 ){
				  var InitialProjectInfo = '';
				  textToVoiceSynthesizer(' You have  '+jsonData[0].TotalNotification+' inputs from ' + jsonData[0].NumberOfProjects+' projects or workspaces'); 
				  InitialProjectInfo = ' You have  '+jsonData[0].TotalNotification+' inputs from '+ jsonData[0].NumberOfProjects+' projects or workspaces';
				  if (jsonData[0].concernedInputs > 0 && (userRoleId == '2' || userRoleId == '4')){
					  textToVoiceSynthesizer(' Among them '+jsonData[0].concernedInputs+' can be of your concern.'); 
					  InitialProjectInfo = InitialProjectInfo + ' Among them '+jsonData[0].concernedInputs+' can be of your concern.';
				  }
				  $('#cassandraContent').text(InitialProjectInfo);
			  }
			  else {
				  
				  textToVoiceSynthesizer('  You have no new inputs from any of your projects  ',false); 
			  }  
		  } 
		  myCassandraTimeOutFunction();
		  $("#loadingBar").hide();
		  timerControl("");
		}
	}); 	
 }	

function myCassandraTimeOutFunction() {
$('.cassandraCallout').delay(10000).fadeOut();
}

var elementIndex='';
var index ='';

var elementhashindex='';
var hashIndex ='';
var menuType="";
var projectId="";
function voiceText(event){
 var headerValue = $('#idToDisplaySpeechText').val();
 
 if(headerValue == ""){
	 $("#ClearCassandraIdSearch").hide();
 }else{
	 $("#ClearCassandraIdSearch").show();
 }
 
 /*if(headerValue.charAt(index)=="W" || headerValue.charAt(index)=="w" || headerValue.charAt(index)=="h" || headerValue.charAt(index)=="H" 
	|| headerValue.charAt(index)=="G" || headerValue.charAt(index)=="g" || headerValue.charAt(index)=="l" || headerValue.charAt(index)=="L"){
				 var focusIndex = document.getElementById('idToDisplaySpeechText').selectionStart;
				 var aTxt = headerValue.substring(elementIndex,focusIndex).toLowerCase().trim();
				$('#cassandraSearchList li').show();
				$('#cassandraSearchInputBox').css('display','block');
				if(aTxt != ""){
					$('#cassandraSearchList li').each(function(){
						val = $(this).text().toLowerCase();
						if(val.indexOf(aTxt)!= -1 ){
							$(this).show();
						}else{
							   $(this).hide();
						}
					});
					
					if($('#cassandraSearchList li:visible').length < 1)
					   $('#cassandraSearchInputBox').hide();
					   else
						  $('#cassandraSearchInputBox').show();
		  }	 
	  }else{
			 $('#cassandraSearchInputBox').css('display','none');
			 index ="";
	  } */
 
 

	if (event.shiftKey && event.keyCode == 51 ){
		elementhashindex = document.getElementById('idToDisplaySpeechText').selectionStart;
		hashIndex = parseInt(elementhashindex)-1 ;
		$('#hashTagSearchInputBox1').css('display','block');
		$('#hashTagSearchInputBox1').css('margin-top','0px');
		if(hashResult == "" || hashResult == "[]"){
			$("#loadingBar").show();
			timerControl("start");
			ajaxCallforHashCode('','','cassandraSearch');
			$("#loadingBar").hide();
			timerControl("");
		}else{
			$('#hashTagSearchInputBox1 ul').html(preparingUIforHash(hashResult,projectId,'cassandraSearch'));
		}
	}else if( event.keyCode == 32){
				elementhashindex='';
				hashIndex ="";
				$('#hashTagSearchInputBox1:visible').hide();
	}else if(elementhashindex!=""){
			var data=$('#idToDisplaySpeechText').val();
			 if(data.charAt(hashIndex)=="#"){
				 var focusIndex = document.getElementById('idToDisplaySpeechText').selectionStart;
				var aTxt = data.substring(elementhashindex,focusIndex).toLowerCase().trim();
				$('#hashTagProjList1 li').show();
				$('#hashTagSearchInputBox1').show();
				if(aTxt !=""){
					 $('#hashTagProjList1 li').each(function(){
							val = $(this).text().toLowerCase();
							if(val.indexOf(aTxt)!= -1 ){
								$(this).show();
							}else{
								   $(this).hide();
							}
					 });
					
					if($('#hashTagProjList1 li:visible').length < 1)
					   $('#hashTagSearchInputBox1').hide();
					   else
						  $('#hashTagSearchInputBox1').show();
			 }	 
		  }else{
			 $('#hashTagSearchInputBox1').css('display','none');
			 elementhashindex = "";
			 hashIndex ="";
		  } 
	}else{
		$('#hashTagSearchInputBox1:visible').hide();
	}
 
 if(event.keyCode == 13){
	 var place = "enterEvent";
	 validationForSpeechInitializer(place);
	 //$("#ClearCassandraIdSearch").hide();
	 $('#cassandraSearchInputBox').css('display','none');
 }

 var  kw =$("#idToDisplaySpeechText").val().toLowerCase();
 if(kw!=''){
	 $("#ClearCassandraGlobalSearch").show();
 }else{
	 $("#ClearCassandraGlobalSearch").hide();
 }
}

function ClearglobalIdSearchVal(){
   $("#idToDisplaySpeechText").val("");
   $("#ClearCassandraIdSearch").hide();
}
