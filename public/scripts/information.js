/* globals hopscotch: false */

/* ============ */
/* EXAMPLE TOUR */
/* ============ */
var tzTour = null;
var mzTour = null;
var ezTour = null;
var ezAccRoleTour = null;
var notiTour = null;
var newProTour = null;
var myProTour = null;
var subscribeProTour = null;
var convoTour = null;
var convoScrollTour = null;
var taskTour = null;
var taskScrollTour = null;
var docTour = null;
var docScrollTour = null;
var teamTour = null;
var teamScrollTour = null;
var emailTour = null;
var ideaTour = null;
var defiTour = null;
var defiScrollTour = null;
var sprintTour = null;
var msgTour = null;
var notesTour = null;
var blogTour = null;
var galleryTour = null; 
var analyticsTour = null;
var analyticsTourUser = null;
var projAdmTour = null;
var myTaskTour = null;
var myTasksView = null;
var myTasksViewFrWorkFlow = null;
var myTaskEvents = null;
var emailIntegTour = null;
var chatTour=null;
var MyNotesTour=null;
var CreateNote=null;
var docTourForRepo = null;
var docScrollTourForRepo=null;

/* ========== */
/* TOUR SETUP */
/* ========== */
addClickListener = function(el, fn) {
  if (el.addEventListener) {
    el.addEventListener('click', fn, false);
  }
  else {
    el.attachEvent('onclick', fn);
  }
},
	startTzTour = function() {
		var startBtnId = 'tzInfo',
		calloutId = 'startTourCallout',
      	mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
	    	if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(tzTour);
	      	}
  		});
  	};
	
	startMzTour = function() {
		var startBtnId = 'mzInfo',
		calloutId = 'startTourCallout',
      	mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(mzTour);
	      	}
  		});
  	};
	
	startEzTour = function() {
		var startBtnId = 'ezInfo',
		calloutId = 'startTourCallout',
      	mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		if(userRoleId == "2"){
	      			hopscotch.startTour(ezTour);
	      		}else{
	      			hopscotch.startTour(ezAccRoleTour);
	      		}
	      		
	      	}
  		});
  	};
  	
  	startNotiTour = function() {
		var startBtnId = 'notiInfo',
		calloutId = 'startTourCallout',
      	mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(notiTour);
	      	}
  		});
  	};
  	
  	startNewProjTour = function() {
		var startBtnId = 'newProInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
  				mgr.removeAllCallouts();
	      		hopscotch.startTour(newProTour);
	      	}
  		});
  	};
  	  	
  	startMyProjTour = function() {
		var startBtnId = 'myProInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(myProTour);
	      	}
  		});
  	};
  	
  	startSubscribeProjTour = function() {
		var startBtnId = 'subscribeProInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(subscribeProTour);
	      	}
  		});
  	};
  	
  	startConvoTour = function() {
		var startBtnId = 'convoInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		if (vertiScrollBar == true){
	      			hopscotch.startTour(convoScrollTour);
	      		}else{
      				hopscotch.startTour(convoTour);
      			}
	      	}
  		});
  	};
  	
  	startTaskTour = function() {
		var startBtnId = 'taskInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		if (vertiScrollBar == true){
	      			hopscotch.startTour(taskScrollTour);
	      		}else{
      				hopscotch.startTour(taskTour);
      			}
	      		
	      	}
  		});
  	};
  	
  	startDocTour = function() {
		var startBtnId = 'docInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		if($("#switchRepo").is(":visible")){
		      		if (vertiScrollBar == true){
		      			hopscotch.startTour(docScrollTourForRepo);
		      		}else{
	      				hopscotch.startTour(docTourForRepo);
	      			}
	      		}else{
	      			if (vertiScrollBar == true){
		      			hopscotch.startTour(docScrollTour);
		      		}else{
	      				hopscotch.startTour(docTour);
	      			}
	      		}
	      	}
  		});
  	};
  	
  	startTeamTour = function() {
		var startBtnId = 'teamInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		if (vertiScrollBar == true){
	      			hopscotch.startTour(teamScrollTour);
	      		}else{
      				hopscotch.startTour(teamTour);
      			}
	      	}
  		});
  	};
  	
  	startEmailTour = function() {
		var startBtnId = 'emailInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(emailTour);
	      		
	      		
	      	}
  		});
  	};
  	
  	startIdeaTour = function() {
		var startBtnId = 'ideaInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(ideaTour);
	      	}
  		});
  	};
  	
  	startStoryTour = function() {
		var startBtnId = 'defInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		if (vertiScrollBar == true){
	      			hopscotch.startTour(defiScrollTour);
	      		}else{
	      			hopscotch.startTour(defiTour);
	      		}
	      	}
  		});
  	};
  	
  	startSprintTour = function() {
		var startBtnId = 'sprintInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(sprintTour);
	      		
	      		
	      	}
  		});
  	};
  	
  	startMsgTour = function() {
		var startBtnId = 'msgInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(msgTour);
	      		
	      		
	      	}
  		});
  	};
  	
  	startNotesTour = function() {
		var startBtnId = 'notesInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(notesTour);
	      	}
  		});
  	};
  	 	
  	startMyNotesTour = function() {
		var startBtnId = 'notesPages',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(MyNotesTour);
	      	}
  		});
  	};
  	 	
  	startCreateNotesTour = function() {
		var startBtnId = 'createNotes',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(CreateNote);
	      	}
  		});
  	};
  	
  	startBlogTour = function() {
		var startBtnId = 'blogInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(blogTour);
	      	}
  		});
  	};
  	
  	startGalleryTour  = function() {
		var startBtnId = 'galleryInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(galleryTour);
	      	}
  		});
  	};
  	
  	startAnalyticTour = function() {
		var startBtnId = 'anaInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		if(userRoleId == "2"){
	      			hopscotch.startTour(analyticsTour);
	      		}else{
	      			hopscotch.startTour(analyticsTourUser);
	      		}
	      	}
  		});
  	};
  	
  	startProjAdminTour = function() {
		var startBtnId = 'projAdmInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(projAdmTour);
	      	}
  		});
  	};
  	startMyTaskAdminTour = function() {
		var startBtnId = 'newTaskInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
  			if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(myTaskTour);
	      	}
  		});
  	};
  	
  	startMyTaskViewTour = function() {
		var startBtnId = 'viewTaskInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
      	    if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(myTasksView);
	      	}
  		});
  	};
  	
  	startMyTaskViewTourFrWrkFlow = function() {
		var startBtnId = 'newTaskInfoWorkFlow',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
      	    if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(myTasksViewFrWorkFlow);
	      	}
  		});
  	};
  	
  	startMyTaskEvent = function() {
		var startBtnId = 'newTaskInfoEvent',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
      	    if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(myTaskEvents);
	      	}
  		});
  	};
  	startMyEmailTour = function() {
		var startBtnId = 'emailConfigInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
      	    if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(emailIntegTour);
	      	}
  		});
  	};
  	
  	startMyChatTour = function() {
		var startBtnId = 'chatConfigInfo',
		calloutId = 'startTourCallout',
		mgr = hopscotch.getCalloutManager(),
      	state = hopscotch.getState();
      	addClickListener(document.getElementById(startBtnId), function() {
      	    if (!hopscotch.isActive) {
	      		mgr.removeAllCallouts();
	      		hopscotch.startTour(chatTour);
	      	}
  		});
  	};
  	
  	
  	function setHopscotch(loadType){
  	//alert(loadType);
  		tzTour =  {
			id: 'hopscoth-info',
		  	steps: [
		    	{
			      target: 'tzInfo',
			      title: getValues(companyLabels, "Hop_Team"),
			      content: getValues(companyLabels,"Hop_Team_Title"),
			      placement: 'right',
			      arrowOffset: 60,
			      yOffset: -75
		    	},
		    	{
			      target: 'startNewProject',
			      title: getValues(companyLabels, "Hop_Where_to_begin"),
			      content:  getValues(companyLabels, "Hop_start_off"),
			      placement: 'top',
			      yOffset: 10
		    	},
			    {
			      target: 'viewExistingProject',
			      placement: 'right',
			      title: getValues(companyLabels, "Hop_view_pro"),
			      content: getValues(companyLabels, "Hop_view_pro_cont"),
			      xOffset: -125,
			      yOffset: -15
			    },
			    {
			      target: 'landContList',
			      placement: 'bottom',
			      title: getValues(companyLabels, "Hop_keep_in_touch"),
			      content: getValues(companyLabels, "Hop_keep_in_touch_cont"),
			      yOffset: -5
			    },
			    {
			      target: 'landRecentList',
			      placement: 'top',
			      title: getValues(companyLabels, "Hop_Recently_Accessed"),
			      content: getValues(companyLabels, "Hop_Recently_Accessed_Cont"),
			      yOffset: 10
			    },
		  	],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		   mzTour = {
			    id: 'hopscoth-mzInfo',
			    steps: [
			    {
			      target: 'mzInfo',
			      title: getValues(companyLabels, "Hop_My_Zone_Info"),
			      content: getValues(companyLabels, "Hop_My_Zone_Info_Content"),
			      placement: 'right',
			      arrowOffset: 60,
			      yOffset: -75
			    },
			    {
			      target: 'task',
			      title: getValues(companyLabels, "Hop_Mz_Task"),
			      content: getValues(companyLabels, "Hop_Mz_Task_Cont"),
			      placement: 'top',
			      yOffset: 10
			    },
			    {
				 target: 'allScrum',
				 placement: 'right',
				 title: getValues(companyLabels, "HopMyScrum"),
				 content: getValues(companyLabels, "HopMyScrum_cont"),
				 xOffset: -150,
				 yOffset: -20
				 },
			    {
			      target: 'doc',
			      placement: 'right',
			      title: getValues(companyLabels, "Hop_Mz_Doc"),
			      content: getValues(companyLabels, "Hop_Mz_Doc_Cont"),
			      xOffset: -150,
			      yOffset: -15
			    },
			    {
			      target: 'msg',
			      placement: 'bottom',
			      title: getValues(companyLabels, "Hop_Con"),
			      content: getValues(companyLabels, "Hop_Con_Cont"),
			      yOffset: -5
			    },
			    {
			      target: 'note',
			      placement: 'left',
			      title: getValues(companyLabels, "Hop_Notes"),
			      content: getValues(companyLabels, "Hop_Notes_Cont"),
			      yOffset: -15
			    },
			    {
			      target: 'blog',
			      placement: 'bottom',
			      title: getValues(companyLabels, "Hop_Blog"),
			      content: getValues(companyLabels, "Hop_Blog_Cont"),
			    },
			    {
			      target: 'gallery',
			      placement: 'left',
			      title: getValues(companyLabels, "Hop_Gallery"),
			      content: getValues(companyLabels, "Hop_Gallery_Cont"),
			      yOffset: -15
			    },
			    
			  ],
			  showPrevButton: true,
			  scrollTopMargin: 100
		}
		
		  ezTour = {
		  	 id: 'hopscoth-ezInfo',
		  	 steps: [
			    {
			      target: 'ezInfo',
			      title: getValues(companyLabels, "Ez_Info_Title"),
			      content: getValues(companyLabels, "Ez_Info_Title_Cont"),
			      placement: 'right',
			      arrowOffset: 60,
			      yOffset: -75
			    },
			    {
			      target: 'analytics',
			      placement: 'left',
			      title: getValues(companyLabels, "Ez_Reports"),
			      content: getValues(companyLabels, "Ez_Report_Cont"),
			      yOffset: -15
			    },
			    {
			      target: 'proAdmin',
			      placement: 'right',
			      title: getValues(companyLabels, "HopAdmProject"),
			      content: getValues(companyLabels, "HopAdmProjectCont"),
			      xOffset: -150,
			      yOffset: -15
			    },
			    
			    {
			      target: 'systAdmin',
			      placement: 'bottom',
			      title: getValues(companyLabels, "HopMonProjects"),
			      content: getValues(companyLabels, "HopMonProjectsCont"),
			      yOffset: -5
			    },
			    {
			      target: 'integration',
			      placement: 'left',
			      title: getValues(companyLabels, "HopColMore"),
			      content: getValues(companyLabels, "HopColMoreCont"),
			      yOffset: -15
			    },
		   	],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		ezAccRoleTour = {
		  	id: 'hopscoth-ezAccRoleTour',
		  	steps: [
			    {
			      target: 'ezInfo',
			      title: getValues(companyLabels, "Ez_Info_Title"),
			      content: getValues(companyLabels, "Ez_Info_Title_Cont_Role"),
			      placement: 'right',
			      arrowOffset: 60,
			      yOffset: -75
			    },
			    {
			      target: 'analytics',
			      placement: 'left',
			      title: getValues(companyLabels, "Ez_Reports"),
			      content: getValues(companyLabels, "Ez_Report_Cont"),
			      yOffset: -15
			    },
		   	],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		notiTour = {
			id: 'hopscoth-notiInfo',
		  	steps: [
			    {
			      target: 'notiInfo',
			      title: getValues(companyLabels, "Notifications"),
			      content: getValues(companyLabels, "HopNotificationsCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 25,
			    },
		  	],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		newProTour = {
		  	id: 'hopscoth-newProInfo',
			steps: [
		    	{
			      target: 'newProInfo',
			      title: getValues(companyLabels, "HopProjectCreation"),
			      content: getValues(companyLabels, "HopProjectCreationCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -10,
			      xOffset: 5,
			    },
			    {
			      target: 'newProCodeInfo',
			      title: getValues(companyLabels, "HopProjCodet"),
			      content: getValues(companyLabels, "HopProjCodeCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 10,
			    },
			    {
			      target: 'comment',
			      title: getValues(companyLabels, "HopProjDescription"),
			      content: getValues(companyLabels, "HopProjDescriptionDetail"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 10,
			    },
			    {
			      target: 'privacyImg',
			      title: getValues(companyLabels, "HopProjPrivacy"),
			      content: getValues(companyLabels, "HopProjPrivacyDetail"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 10,
			    },
			    {
			      target: 'inviteUsers',
			      title: getValues(companyLabels, "HopProjInviteUsers"),
			      content: getValues(companyLabels, "HopProjInviteUsersDetail"),
			      placement: 'top',
			      arrowOffset: 50,
			      yOffset: -1,
			      xOffset: 10,
			    },
			    /*{
			      target: 'changeDynamic',
			      title: getValues(companyLabels, "HopProjCreate"),
			      content: getValues(companyLabels, "HopProjCreateDetails"),
			      placement: 'top',
			      arrowOffset: 50,
			      yOffset: -1,
			      xOffset: 10,
			    },*/
		  	],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		myProTour = {
		  	id: 'hopscoth-myProInfo',
		  	steps: [
		    	{
		      	  target: 'myProInfo',
		      	  title: getValues(companyLabels, "HopMyProjects"),
			      content: getValues(companyLabels, "HopMyProjectsCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
		      	  target: 'searchProject',
		      	  title: getValues(companyLabels, "HopProjectSrchNSort"),
			      content: getValues(companyLabels, "HopProjectSrchNSortCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			   /*	this option in workspace was set as display - none.
			      {
		      	  target: 'hiddenProject',
		      	  title: getValues(companyLabels, "HopProjectHidden"),
			      content: getValues(companyLabels, "HopProjectHiddenCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: 60,
			      xOffset: 1200,
			      fixedElement: true,
			    },*/
			    
			    {
		      	  target: 'csvUpload',
		      	  title: getValues(companyLabels, "HopUploadCsv"),
			      content: getValues(companyLabels, "HopUploadCsvCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -250,
			      arrowOffset: 240,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'addProject',
			      title: getValues(companyLabels, "HopProjectNew"),
			      content: getValues(companyLabels, "HopProjectCreationCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 10,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'projectImage',
			      title: getValues(companyLabels, "HopProjectNewSettings"),
			      content: getValues(companyLabels, "HopProjectNewSettingsCont"),
			      placement: 'top',
			      arrowOffset: 50,
			      yOffset: 10,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
		  	],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		subscribeProTour = {
		  	id: 'hopscoth-subscribeProInfo',
		  	steps: [
		    	{
			      target: 'subscribeProInfo',
			      //title: getValues(companyLabels, "HopProjectSubsCribe"),
			      content: getValues(companyLabels, "HopProjectSubsCribeCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 10,
			      fixedElement: true,
			    },
			    
			    {
		      	  target: 'searchProject',
			      title: getValues(companyLabels, "HopProjectSrchNSort"),
			      content: getValues(companyLabels, "HopProjectSrchNSortCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
		  	],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		convoTour = {
		  	id: 'hopscoth-convoInfo',
		  	steps: [
		    	{
			      target: 'convoInfo',
			      title: getValues(companyLabels, "HopProjectConvoStrt"),
			      content: getValues(companyLabels, "HopProjectConvoStrtCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'convoPost',
			      title: getValues(companyLabels, "HopProjectConvo"),
			      content: getValues(companyLabels, "HopProjectConvoCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -220,
			      arrowOffset: 240,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'recAudioMsg',
			      title: getValues(companyLabels, "HopProjectRecAudio"),
			      content: getValues(companyLabels, "HopProjectRecAudioCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -55,
			      xOffset: 25,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'moreOption',
			      title: getValues(companyLabels, "HopProjectMoreOptions"),
			      content: getValues(companyLabels, "HopProjectMoreOptionsCont"),
			      placement: 'left',
			      yOffset: 0,
			      xOffset: 23,
			      arrowOffset: 50,
			      fixedElement: true,
			    }
			 ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		convoScrollTour = {
		  	id: 'hopscoth-convoScrollInfo',
		  	steps: [
		    	{
			      target: 'convoInfo',
			      title: getValues(companyLabels, "HopProjectConvoStrt"),
			      content: getValues(companyLabels, "HopProjectConvoStrtCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'convoPost',
			      title: getValues(companyLabels, "HopProjectConvo"),
			      content: getValues(companyLabels, "HopProjectConvoCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -230,
			      arrowOffset: 225,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'recAudioMsg',
			      title: getValues(companyLabels, "HopProjectRecAudio"),
			      content: getValues(companyLabels, "HopProjectRecAudioCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -55,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    /*,   //---------------> commented as this was not working
			    {
			      target: 'tabContentDiv',
			      title: getValues(companyLabels, "HopProjectMoreOptions"),
			      content: getValues(companyLabels, "HopProjectMoreOptionsCont"),
			      placement: 'bottom',
			      arrowOffset: 220,
			      yOffset: 40,
			      xOffset: -100,
			     
			    }, */
			    {
			      target: 'moreOption',
			      title: getValues(companyLabels, "HopProjectMoreOptions"),
			      content: getValues(companyLabels, "HopProjectMoreOptionsCont"),
			      placement: 'left',
			      yOffset: 2,
			      xOffset: 5,
			      arrowOffset: 50,
			      fixedElement: true,
			    },
			 ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		taskTour = {
			id: 'hopscoth-taskInfo',
		  	steps: [
		    	{
			      target: 'taskInfo',
			      title: getValues(companyLabels, "HopTaskView"),
			      content: getValues(companyLabels, "HopTaskViewCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'taskSearchNSort',
			      title: getValues(companyLabels, "HopTaskSrchNSort"),
			      content: getValues(companyLabels, "HopTaskSrchNSortCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -200,
			      arrowOffset: 210,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'calViewData',
			      title: getValues(companyLabels, "HopCalView"),
			      content: getValues(companyLabels, "HopCalViewCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			   /* {
			      target: 'taskCalView',
			      title: getValues(companyLabels, "HopProjectMoreOptions"),
			      content: getValues(companyLabels, "HopProjectMoreOptionsCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -220,
			      arrowOffset: 230,
			    },*/
			    {
			      target: 'taskExpXlsx',
			      title: getValues(companyLabels, "HopExportTasks"),
			      content: getValues(companyLabels, "HopExportTasksCont"),
			      placement: 'bottom',
			      arrowOffset: 250,
			      yOffset: -5,
			      xOffset: -248,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'wrkCreateTask',
			      title: getValues(companyLabels, "Hop_Mz_Task"),
			      content: getValues(companyLabels, "Hop_Mz_Task_Cont"),
			      placement: 'left',
			      yOffset: -65,
			      xOffset: 20,
			      arrowOffset: 50,
			      fixedElement: true,
			      
			    },
			    {
			      target: 'loadDashBoard',
			      content: getValues(companyLabels, "HopTaskStatus"),
			      placement: 'left',
			      yOffset: -35,
			      xOffset: 50,
			      arrowOffset: 50,
			      fixedElement: true,
			    },
			   ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		taskScrollTour = {
			id: 'hopscoth-taskScrollInfo',
		  	steps: [
		    	{
			      target: 'taskInfo',
			      title: getValues(companyLabels, "HopTaskView"),
			      content: getValues(companyLabels, "HopTaskViewCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'taskSearchNSort',
			      title: getValues(companyLabels, "HopTaskSrchNSort"),
			      content: getValues(companyLabels, "HopTaskSrchNSortCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -220,
			      arrowOffset: 210,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'calViewData',
			      title: getValues(companyLabels, "HopCalView"),
			      content: getValues(companyLabels, "HopCalViewCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    /*
			    {
			      target: 'taskCalView',
			      title: getValues(companyLabels, "HopProjectMoreOptions"),
			      content: getValues(companyLabels, "HopProjectMoreOptionsCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -230,
			      arrowOffset: 300,
			    },
			    */
			    {
			      target: 'taskExpXlsx',
			      title: getValues(companyLabels, "HopExportTasks"),
			      content: getValues(companyLabels, "HopExportTasksCont"),
			      placement: 'bottom',
			      arrowOffset: 260,
			      yOffset: -5,
			      xOffset: -268,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'wrkCreateTask',
			      title: getValues(companyLabels, "Hop_Mz_Task"),
			      content: getValues(companyLabels, "Hop_Mz_Task_Cont"),
			      placement: 'left',
			      yOffset: -65,
			      xOffset: 10,
			      arrowOffset: 50,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'loadDashBoard',
			      content: getValues(companyLabels, "HopTaskStatus"),
			      placement: 'left',
			      yOffset: -35,
			      xOffset: 30,
			      arrowOffset: 55,
			      fixedElement: true,
			    },
			 ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		} 
		docTour = {
			id: 'hopscoth-docInfo',
		  	steps: [
		    	{
			      target: 'docInfo',
			      title: getValues(companyLabels, "HopDocCollection"),
			      content: getValues(companyLabels, "HopDocCollectionCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'searchSort',
			      title: getValues(companyLabels, "HopSrchNSortDoc"),
			      content: getValues(companyLabels, "HopSrchNSortDocCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -200,
			      arrowOffset: 210,
			      fixedElement: true,
			    },
			    
			   
			    /*{
			      target: 'switchRepo',
			      title: getValues(companyLabels, "HopSwitchRepoCon"),
			      content: getValues(companyLabels, "HopSwitchRepoDetails"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -52,
			      xOffset: 22,
			    },*/
			    
			    {
			      target: 'cNewFoldr',
			      title: getValues(companyLabels, "HopNewFlder"),
			      content: getValues(companyLabels, "HopNewFlderCont"),
			      placement: 'left',
			      yOffset: -65,
			      xOffset: 22,
			      arrowOffset: 56,
			      fixedElement: true,
			    },
			    {
			      target: 'delAll',
			      title: getValues(companyLabels, "HopDeleteAll"),
			      content: getValues(companyLabels, "HopDeleteAllCont"),
			      placement: 'left',
			      arrowOffset: 56,
			      yOffset: -70,
			      xOffset: 22,
			      fixedElement: true,
			    },
			    {
			      target: 'docMoreOptions',
			      content: getValues(companyLabels, "HopDocumentMore"),
			      placement: 'left',
			      yOffset: 20,
			      xOffset: 80,
			      arrowOffset: 6,
			      fixedElement: true,
			    }, 
			    
			  
			    
			 ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		docScrollTour = {
			id: 'hopscoth-docScrollInfo',
		  	steps: [
		    	{
			      target: 'docInfo',
			      title: getValues(companyLabels, "HopDocCollection"),
			      content: getValues(companyLabels, "HopDocCollectionCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'searchSort',
			      title: getValues(companyLabels, "HopSrchNSortDoc"),
			      content: getValues(companyLabels, "HopSrchNSortDocCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -200,
			      arrowOffset: 200,
			      fixedElement: true,
			    },
			    
			   /*{
			      target: 'switchRepo',
			      title: getValues(companyLabels, "HopSwitchRepoCon"),
			      content: getValues(companyLabels, "HopSwitchRepoDetails"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -50,
			      xOffset: 5,
			    },*/
			    
			    {
			      target: 'cNewFoldr',
			      title: getValues(companyLabels, "HopNewFlder"),
			      content: getValues(companyLabels, "HopNewFlderCont"),
			      placement: 'left',
			      yOffset: -65,
			      xOffset: 0,
			      arrowOffset: 56,
			      fixedElement: true,
			    },
			    {
			      target: 'delAll',
			      title: getValues(companyLabels, "HopDeleteAll"),
			      content: getValues(companyLabels, "HopDeleteAllCont"),
			      placement: 'left',
			      arrowOffset: 56,
			      yOffset: -65,
			      xOffset: 0,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'docMoreOptions',
			      content: getValues(companyLabels, "HopDocumentMore"),
			      placement: 'left',
			      yOffset: 20,
			      xOffset: 80,
			      arrowOffset: 6,
			      fixedElement: true,
			      
			    }, 
			    
			    
			 ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
  		
  		docTourForRepo = {
  				id: 'hopscoth-docInfo',
  			  	steps: [
  			    	{
  				      target: 'docInfo',
  				      title: getValues(companyLabels, "HopDocCollection"),
  				      content: getValues(companyLabels, "HopDocCollectionCont"),
  				      placement: 'right',
  				      arrowOffset: 50,
  				      yOffset: -65,
  				      xOffset: 5,
  				      fixedElement: true,
  				    },
  				    
  				    {
  				      target: 'searchSort',
  				      title: getValues(companyLabels, "HopSrchNSortDoc"),
  				      content: getValues(companyLabels, "HopSrchNSortDocCont"),
  				      placement: 'bottom',
  				      yOffset: -5,
  				      xOffset: -200,
  				      arrowOffset: 210,
  				      fixedElement: true,
  				    },
  				    
  				   
  				    {
  				      target: 'switchRepo',
  				      title: getValues(companyLabels, "HopSwitchRepoCon"),
  				      content: getValues(companyLabels, "HopSwitchRepoDetails"),
  				      placement: 'left',
  				      arrowOffset: 50,
  				      yOffset: -52,
  				      xOffset: 22,
  				      fixedElement: true,
  				    },
  				    
  				    {
  				      target: 'cNewFoldr',
  				      title: getValues(companyLabels, "HopNewFlder"),
  				      content: getValues(companyLabels, "HopNewFlderCont"),
  				      placement: 'left',
  				      yOffset: -65,
  				      xOffset: 22,
  				      arrowOffset: 56,
  				      fixedElement: true,
  				    },
  				    {
  				      target: 'delAll',
  				      title: getValues(companyLabels, "HopDeleteAll"),
  				      content: getValues(companyLabels, "HopDeleteAllCont"),
  				      placement: 'left',
  				      arrowOffset: 56,
  				      yOffset: -70,
  				      xOffset: 22,
  				      fixedElement: true,
  				    },
  				    {
  				      target: 'docMoreOptions',
  				      content: getValues(companyLabels, "HopDocumentMore"),
  				      placement: 'left',
  				      yOffset: 20,
  				      xOffset: 80,
  				      arrowOffset: 6,
  				      fixedElement: true,
  				    }, 
  				    
  				  
  				    
  				 ],
  			  	showPrevButton: true,
  			  	scrollTopMargin: 100
  			}
  			
  			docScrollTourForRepo = {
  				id: 'hopscoth-docScrollInfo',
  			  	steps: [
  			    	{
  				      target: 'docInfo',
  				      title: getValues(companyLabels, "HopDocCollection"),
  				      content: getValues(companyLabels, "HopDocCollectionCont"),
  				      placement: 'right',
  				      arrowOffset: 50,
  				      yOffset: -65,
  				      xOffset: 5,
  				      fixedElement: true,
  				    },
  				    
  				    {
  				      target: 'searchSort',
  				       title: getValues(companyLabels, "HopSrchNSortDoc"),
  				      content: getValues(companyLabels, "HopSrchNSortDocCont"),
  				      placement: 'bottom',
  				      yOffset: -5,
  				      xOffset: -200,
  				      arrowOffset: 200,
  				      fixedElement: true,
  				    },
  				    
  				   {
  				      target: 'switchRepo',
  				      title: getValues(companyLabels, "HopSwitchRepoCon"),
  				      content: getValues(companyLabels, "HopSwitchRepoDetails"),
  				      placement: 'left',
  				      arrowOffset: 50,
  				      yOffset: -50,
  				      xOffset: 5,
  				      fixedElement: true,
  				    },
  				    
  				    {
  				      target: 'cNewFoldr',
  				      title: getValues(companyLabels, "HopNewFlder"),
  				      content: getValues(companyLabels, "HopNewFlderCont"),
  				      placement: 'left',
  				      yOffset: -65,
  				      xOffset: 0,
  				      arrowOffset: 56,
  				      fixedElement: true,
  				    },
  				    {
  				      target: 'delAll',
  				      title: getValues(companyLabels, "HopDeleteAll"),
  				      content: getValues(companyLabels, "HopDeleteAllCont"),
  				      placement: 'left',
  				      arrowOffset: 56,
  				      yOffset: -65,
  				      xOffset: 0,
  				      fixedElement: true,
  				    },
  				    
  				    {
  				      target: 'docMoreOptions',
  				      content: getValues(companyLabels, "HopDocumentMore"),
  				      placement: 'left',
  				      yOffset: 20,
  				      xOffset: 80,
  				      arrowOffset: 6,
  				      fixedElement: true,
  				      
  				    }, 
  				    
  				    
  				 ],
  			  	showPrevButton: true,
  			  	scrollTopMargin: 100
  			}
		
		teamTour = {
			id: 'hopscoth-teamInfo',
		  	steps: [
		    	{
			      target: 'teamInfo',
			      title: getValues(companyLabels, "HopTeamDetail"),
			      content: getValues(companyLabels, "HopTeamDetailCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'searchSortImg',
			      title: getValues(companyLabels, "HopSrchForTeamMembr"),
			      content: getValues(companyLabels, "HopSrchForTeamMembrCont"),
			       placement: 'bottom',
			      yOffset: -5,
			      xOffset: -230,
			      arrowOffset: 240,
			      fixedElement: true,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		teamScrollTour = {
			id: 'hopscoth-teamScrollInfo',
		  	steps: [
		    	{
			      target: 'teamInfo',
			      title: getValues(companyLabels, "HopTeamDetail"),
			      content: getValues(companyLabels, "HopTeamDetailCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'searchSortImg',
			      title: getValues(companyLabels, "HopSrchForTeamMembr"),
			      content: getValues(companyLabels, "HopSrchForTeamMembrCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -240,
			      arrowOffset: 230,
			      fixedElement: true,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		emailTour = {
			id: 'hopscoth-emailInfo',
		  	steps: [
		    	{
			      target: 'emailInfo',
			      title: getValues(companyLabels, "HopEmailDetail"),
			      content: getValues(companyLabels, "HopEmailDetailCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			      
			    },
			    
			    {
			      target: 'searchNSortEmail',
			      title: getValues(companyLabels, "HopSrchNSrtEmails"),
			      content: getValues(companyLabels, "HopSrchNSrtEmailsCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -230,
			      arrowOffset: 240,
			      fixedElement: true,
			    },/*
			    {
			      target: 'emailDelete',
			      title: getValues(companyLabels, "HopDeleteAll"),
			      content: getValues(companyLabels, "HopDeleteAllContEmail"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 10,
			    },*/
			    
			    {
			      target: 'emailCompose',
			      title: getValues(companyLabels, "HopComposeAnEmail"),
			      content: getValues(companyLabels, "HopComposeAnEmailCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -230,
			      arrowOffset: 220,
			      fixedElement: true,
			    },
			    {
			      target: 'emailRefresh',
			      title: getValues(companyLabels, "HopRefrsh"),
			      content: getValues(companyLabels, "HopRefrshCont"),
			      placement: 'left',
			      yOffset: -65,
			      xOffset: 5,
			      arrowOffset: 50,
			      fixedElement: true,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		emailTourForOthers = {
			id: 'hopscoth-emailInfo',
		  	steps: [
		    	{
			      target: 'emailInfo',
			      title: getValues(companyLabels, "HopEmailDetail"),
			      content: getValues(companyLabels, "HopEmailDetailCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'searchNSortEmail',
			      title: getValues(companyLabels, "HopSrchNSrtEmails"),
			      content: getValues(companyLabels, "HopSrchNSrtEmailsCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -230,
			      arrowOffset: 240,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'emailCompose',
			      title: getValues(companyLabels, "HopComposeAnEmail"),
			      content: getValues(companyLabels, "HopComposeAnEmailCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -230,
			      arrowOffset: 220,
			      fixedElement: true,
			    },
			    {
			      target: 'emailRefresh',
			      title: getValues(companyLabels, "HopRefrsh"),
			      content: getValues(companyLabels, "HopRefrshCont"),
			      placement: 'left',
			      yOffset: -65,
			      xOffset: 5,
			      arrowOffset: 50,
			      fixedElement: true,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		ideaTour = {
			id: 'hopscoth-ideaInfo',
		  	steps: [
		    	{
			      target: 'ideaInfo',
			      title: getValues(companyLabels, "HopIdeas"),
			      content: getValues(companyLabels, "HopIdeasCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'postIdea',
			      title: getValues(companyLabels, "HopGotAnIdea"),
			      content: getValues(companyLabels, "HopGotAnIdeaCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -280,
			      arrowOffset: 270,
			      fixedElement: true,
			    },
			   
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		defiTour = {
			id: 'hopscoth-defiInfo',
		  	steps: [
		    	{
			      target: 'defInfo',
			      title: getValues(companyLabels, "HopEpicFeatureStories"),
			      content: getValues(companyLabels, "HopEpicFeatureStoriesCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'imgIdeaSearch',
			      title: getValues(companyLabels, "HopSrchNSrtStories"),
			      content: getValues(companyLabels, "HopSrchNSrtStoriesCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -240,
			      arrowOffset: 240,
			      fixedElement: true,
			    },
			    {
			      target: 'agileScrumBoard',
			      title: getValues(companyLabels, "HopScrmBoard"),
			      content: getValues(companyLabels, "HopScrmBoardCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'efsStages',
			      title: getValues(companyLabels, "HopCreateStage"),
			      content: getValues(companyLabels, "HopCreateStageCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -240,
			      arrowOffset: 230,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'csvUpload',
			      title: getValues(companyLabels, "HopUpldCSV"),
			      content: getValues(companyLabels, "HopUpldCSVCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -255,
			      arrowOffset: 240,
			      fixedElement: true,
			    },
			    {
			      target: 'agileDashboard',
			      title: getValues(companyLabels, "HopDeshboard"),
			      content: getValues(companyLabels, "HopDeshboardCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 20,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'storyExpandAll',
			      title: getValues(companyLabels, "HopWantToHaveLookAllStoriesOnce"),
			      content: getValues(companyLabels, "HopWantToHaveLookAllStoriesOnceCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -240,
			      arrowOffset: 240,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'storyPrev',
			      title: getValues(companyLabels, "HopPrevious"),
			      content: getValues(companyLabels, "HopPreviousCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			   
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		
		defiScrollTour = {
			id: 'hopscoth-defiScrollnfo',
		  	steps: [
		    	{
			      target: 'defInfo',
			      title: getValues(companyLabels, "HopEpicFeatureStories"),
			      content: getValues(companyLabels, "HopEpicFeatureStoriesCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'imgIdeaSearch',
			      title: getValues(companyLabels, "HopSrchNSrtStories"),
			      content: getValues(companyLabels, "HopSrchNSrtStoriesCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -240,
			      arrowOffset: 240,
			      fixedElement: true,
			    },
			    {
			      target: 'agileScrumBoard',
			      title: getValues(companyLabels, "HopScrmBoard"),
			      content: getValues(companyLabels, "HopScrmBoardCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'efsStages',
			      title: getValues(companyLabels, "HopCreateStage"),
			      content: getValues(companyLabels, "HopCreateStageCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -240,
			      arrowOffset: 230,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'csvUpload',
			      title: getValues(companyLabels, "HopUpldCSV"),
			      content: getValues(companyLabels, "HopUpldCSVCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -240,
			      arrowOffset: 230,
			      fixedElement: true,
			    },
			    {
			      target: 'agileDashboard',
			      title: getValues(companyLabels, "HopDeshboard"),
			      content: getValues(companyLabels, "HopDeshboardCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'storyExpandAll',
			      title: getValues(companyLabels, "HopWantToHaveLookAllStoriesOnce"),
			      content: getValues(companyLabels, "HopWantToHaveLookAllStoriesOnceCont"),
			      placement: 'bottom',
			      yOffset: -5,
			      xOffset: -240,
			      arrowOffset: 230,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'storyPrev',
			      title: getValues(companyLabels, "HopPrevious"),
			      content: getValues(companyLabels, "HopPreviousCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			   
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		sprintTour = {
			id: 'hopscoth-sprintInfo',
		  	steps: [
		    	{
			      target: 'sprintInfo',
			      title: getValues(companyLabels, "HopSprint"),
			      content: getValues(companyLabels, "HopSprintCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'sprintHop',
			      title: getValues(companyLabels, "HopSprintGroup"),
			      content: getValues(companyLabels, "HopSprintGroupCont"),
			      placement: 'right',
			      yOffset: -23,
			      xOffset: -10,
			      arrowOffset: 20,
			      fixedElement: true,
			    },
			    {
			      target: 'sprintGroupDiv',
			      content: getValues(companyLabels, "HopSprintGroups"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -45,
			      xOffset: -35,
			      fixedElement: true,
			    },
			    {
			      target: 'CompleteDashBoard',
			      content: getValues(companyLabels, "HopSprintGroupSearch"),
			      placement: 'left',
			      arrowOffset: 30,
			      yOffset: -34,
			      xOffset: -20,
			      fixedElement: true,
			    },
			    {
			      target: 'CompleteDashBoard',
			      content: getValues(companyLabels, "HopSprintDashboard"),
			      placement: 'left',
			      arrowOffset: 30,
			      yOffset: -34,
			      xOffset: 18,
			      fixedElement: true,
			    },
			    {
			      target: 'hiddenGroupViewImgDiv',
			      content: getValues(companyLabels, "HopSprintHide"),
			      placement: 'left',
			      arrowOffset: 30,
			      yOffset: -34,
			      xOffset: 15,
			      fixedElement: true,
			    },
			    {
			      target: 'storyPrev',
			      content: getValues(companyLabels, "HopSprintBack"),
			      placement: 'left',
			      arrowOffset: 30,
			      yOffset: -40,
			      xOffset: 20,
			      fixedElement: true,
			    },
			    {
			      target: 'Calender',
			      content: getValues(companyLabels, "HopSprintCalendar"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: 190,
			      xOffset: 450,
			      fixedElement: true,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		msgTour  = {
			id: 'hopscoth-msgInfo',
		  	steps: [
		    	{
			      target: 'msgInfo',
			      title: getValues(companyLabels, "HopStayConnect"),
			      content: getValues(companyLabels, "HopStayConnectCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		notesTour  = {
			id: 'hopscoth-notesInfo',
		  	steps: [
		    	{
			      target: 'notesInfo',
			      title: getValues(companyLabels, "HopMakeNote"),
			      content: getValues(companyLabels, "HopMakeNoteCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'noteimgSearch',
			      title: getValues(companyLabels, "HopSrchNSrtNote"),
			      content: getValues(companyLabels, "HopSrchNSrtNoteCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'noteAddIcon',
			      title: getValues(companyLabels, "HopAddNote"),
			      content: getValues(companyLabels, "HopAddNoteCont"),
			      placement: 'bottom',
			      arrowOffset: 240,
			      yOffset: -5,
			      xOffset: -250,
			      fixedElement: true,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		 		
  		MyNotesTour  = {
  				id: 'hopscoth-notesPages',
  			  	steps: [
  			    	{
  			    		  target: 'notesPages',
  					      title: getValues(companyLabels, "HopMakeNote"),
  					      content: getValues(companyLabels, "HopMakeNoteCont"),
  					      placement: 'right',
  					      arrowOffset: 50,
  					      yOffset: -65,
  					      xOffset: 5,
  					      fixedElement: true,
  				    },
  				     				    
  				  ],
  			  	showPrevButton: true,
  			  	scrollTopMargin: 100
  			}
  			
  		CreateNote  = {
  				id: 'hopscoth-createNotes',
  			  	steps: [
  			    	{
  			    		  target: 'createNotes',
  					      title: getValues(companyLabels, "HopMakeNote"),
  					      content: getValues(companyLabels, "HopMakeNoteCont"),
  					      placement: 'right',
  					      arrowOffset: 50,
  					      yOffset: -65,
  					      xOffset: 5,
  					      fixedElement: true,
  				    },
  				  {
  				      target: 'noteimgSearch',
  				      title: getValues(companyLabels, "HopSrchNSrtNote"),
  				      content: getValues(companyLabels, "HopSrchNSrtNoteCont"),
  				      placement: 'left',
  				      arrowOffset: 50,
  				      yOffset: -65,
  				      xOffset: 5,
  				      fixedElement: true,
  				    },
  				    
  				    {
  				      target: 'noteAddIcon',
  				      title: getValues(companyLabels, "HopAddNote"),
  				      content: getValues(companyLabels, "HopAddNoteCont"),
  				      placement: 'bottom',
  				      arrowOffset: 240,
  				      yOffset: -5,
  				      xOffset: -250,
  				      fixedElement: true,
  				    },
  				  ],
  			  	showPrevButton: true,
  			  	scrollTopMargin: 100
  			}
  		 		  				
  		
		blogTour = {
			id: 'hopscoth-blogInfo',
		  	steps: [
		    	{
			      target: 'blogInfo',
			      title: getValues(companyLabels, "HopPostUrBlog"),
			      content: getValues(companyLabels, "HopPostUrBlogCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'blogSearchIcon',
			      title: getValues(companyLabels, "HopSrchNSrtBlog"),
			      content: getValues(companyLabels, "HopSrchNSrtBlogCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'addBlogIcon',
			      title: getValues(companyLabels, "HopNwBlog"),
			      content: getValues(companyLabels, "HopNwBlogCont"),
			      placement: 'bottom',
			      arrowOffset: 240,
			      yOffset: -5,
			      xOffset: -250,
			      fixedElement: true,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		galleryTour = {
			id: 'hopscoth-galleryInfo',
		  	steps: [
		    	{
			      target: 'galleryInfo',
			      title: getValues(companyLabels, "HopGallery"),
			      content: getValues(companyLabels, "HopGalleryCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'galleryimgSearch',
			      title: getValues(companyLabels, "HopSrchNSrtAlbum"),
			      content: getValues(companyLabels, "HopSrchNSrtAlbumCont"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      fixedElement: true,
			    },
			    
			    {
			      target: 'addAlb',
			      title: getValues(companyLabels, "HopCreateNwAlbum"),
			      content: getValues(companyLabels, "HopCreateNwAlbumCont"),
			      placement: 'bottom',
			      arrowOffset: 240,
			      yOffset: -5,
			      xOffset: -250,
			      fixedElement: true,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		analyticsTour = {
			id: 'hopscoth-anaInfo',
		  	steps: [
		    	{
			      target: 'anaInfo',
			      title: getValues(companyLabels, "HopReport"),
			      content: getValues(companyLabels, "HopReportCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			    },
			    
			    {
			      target: 'todaysRep',
			      title: getValues(companyLabels, "HopTodayReport"),
			      content: getValues(companyLabels, "HopTodayReportCont"),
			      placement: 'bottom',
			      arrowOffset: 50,
			      yOffset: 10,
			      xOffset: 5,
			    },
			    
			    {
			      target: 'storyRep',
			      title: getValues(companyLabels, "HopStoryReport"),
			      content: getValues(companyLabels, "HopStoryReportCont"),
			      placement: 'top',
			      arrowOffset: 240,
			      yOffset: -5,
			      xOffset: -200,
			    },
			    /*{
			      target: 'proMgmtRep',
			      title: getValues(companyLabels, "HopProjtMgtReport"),
			      content: getValues(companyLabels, "HopProjtMgtReportCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -5,
			      xOffset: 0,
			    },*/
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
  		
  		analyticsTourUser = {
  				id: 'hopscoth-anaInfo',
  			  	steps: [
  			    	{
  				      target: 'anaInfo',
  				      title: getValues(companyLabels, "HopReport"),
  				      content: getValues(companyLabels, "HopReportCont"),
  				      placement: 'right',
  				      arrowOffset: 50,
  				      yOffset: -65,
  				      xOffset: 5,
  				    },
  				    
  				    {
  				      target: 'todaysRep',
  				      title: getValues(companyLabels, "HopTodayReport"),
  				      content: getValues(companyLabels, "HopTodayReportCont"),
  				      placement: 'bottom',
  				      arrowOffset: 50,
  				      yOffset: 10,
  				      xOffset: 5,
  				    },
  				  ],
  			  	showPrevButton: true,
  			  	scrollTopMargin: 100
  		}
  		
		projAdmTour = {
			id: 'hopscoth-proAdmInfo',
		  	steps: [
		    	{
			      target: 'projAdmInfo',
			      title: getValues(companyLabels, "HopAdminProjt"),
			      content: getValues(companyLabels, "HopAdminProjtCont"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			    },
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		//*****For Task***********
		
		myTaskTour = {
			id: 'hopscoth-newTaskInfo',
		  	steps: [
		    	{
			      target: 'selectTask',
			      content:  getValues(companyLabels, "HopMyTaskType"),
			      placement: 'right',
			      arrowOffset: 30,
			      yOffset: 65,
			      xOffset:233,
			    },
			    {
			      target: 'eventDescription',
			      content: getValues(companyLabels, "HopMyTaskDescription"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			    },
			    {
			      target: 'dependencyTask',
			      content: getValues(companyLabels, "HopMyTaskDepedency"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			    },
			    {
			      target: 'taskDocCreateImg',
			      content: getValues(companyLabels, "HopMyTaskImgDescription"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -63,
			      xOffset: 1,
			    },
			    {
			      target: 'taskEstContainer',
			      content: getValues(companyLabels, "HopMyTaskWorkingHours"),
			      placement: 'bottom',
			      arrowOffset: 250,
			      yOffset: -50,
			      xOffset: 275,
			    },
			    {
			      target: 'wsTaskUserContainer',
			      content: getValues(companyLabels, "HopMyTaskSelectAdd"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -1,
			      xOffset: 5,
			    },
			    {
			      target: 'participantHeader',
			      content: getValues(companyLabels, "HopMyTaskAddRemove"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -25,
			      xOffset: 1,
			    },
			    {
			    /*
			      target: 'taskImage',
			      content: getValues(companyLabels, "HopMyTaskWorkFlowDescription"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      */
			      
			      
			      target: 'prtcpntSection',
			      content: getValues(companyLabels, "HopMyTaskDelete"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: -5,
			      
			    },
			    /*
			    {
			      target: 'prtcpntSection',
			      content: getValues(companyLabels, "HopMyTaskDelete"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      
			      target: 'selectTask',
			      content: getValues(companyLabels, "HopMyTaskWorkFlowDescription"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -60,
			      xOffset: -225,
			    },
			    */
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100,
		  	onClose: function() {
        		$("#newTaskInfo").attr("onclick","setHopscotch('myTaskHop');");
      		}
		}
		
		
		//*****TaskViewTour*******
		
		myTasksView = {
			id: 'hopscoth-viewTaskInfo',
		  	steps: [
		    	{
			      target: 'progForHop',
			      content:  getValues(companyLabels, "HopMyTaskProgress"),
			      placement: 'bottom',
			      arrowOffset: 50,
			      yOffset: -10,
			      xOffset:-90,
			    },
			    
			    {
			      target: 'taskToggleForHop',
			      content:  getValues(companyLabels, "HopMyTaskProgressComment"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -57,
			      xOffset:-9,
			    },
			    
			    {
			      target: 'taskUserActTimeInd',
			      content:  getValues(companyLabels, "HopMyTaskWorking"),
			      placement: 'bottom',
			      arrowOffset: 50,
			      yOffset: 5,
			      xOffset: -120,
			    },
			    {
			      target: 'taskEstHrDetails',
			      content:  getValues(companyLabels, "HopMyTaskDeatails"),
			      placement: 'left',
			      arrowOffset:50,
			      yOffset: -50,
			      xOffset:8,
			    },
			    
			    
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
  		
  		//*******Email Integration************
  		emailIntegTour = {
  				id: 'hopscoth-emailIntegration',
  			  	steps: [
  			    	{
  				      target: 'colabusEmailId',
  				      content:  getValues(companyLabels, "EmailConfig"),
  				      placement: 'right',
  				      arrowOffset: 30,
  				      yOffset: -85,
  				      xOffset:-100,
  				    },
  				  {
    				   target: 'custEmailConfig',
    				   content:  getValues(companyLabels, "OtherEmailConfig"),
    				   placement: 'right',
    				   arrowOffset: 30,
    				   yOffset: -47,
    				   xOffset:207,
    				 },
  				  ],
  			  	showPrevButton: true,
  			  	scrollTopMargin: 100
  			}
  			  		 		
  		/* -------------Chat Tour -------------*/
  		 		
  		chatTour = {
  				id: 'hopscoth-chatIntegration',
  			  	steps:[
  			  	     /*
  			    	{
  				      target: 'chatHeader',
  				      content:  getValues(companyLabels, "chatConfig"),
  				      placement: 'bottom',
  				      arrowOffset: 30,
  				      yOffset: 40,
  				      xOffset:10,
  				    },
  				    */
  				  {
    				      target: 'chatHeader',
    				      content:('Welcome to Colabus IM !'),
    				      placement: 'bottom',
    				      arrowOffset: 30,
    				      yOffset: 40,
    				      xOffset:10,
    				    },
  				  
  				  ],
  			  	showPrevButton: true,
  			  	scrollTopMargin: 100
  			}
  			 				
		//*******Event***********
		
		myTaskEvents = {
			id: 'hopscoth-newTaskInfoEvent',
		  	steps: [
		    	{
			      target: 'selectTask',
			      content:  getValues(companyLabels, "HopEventType"),
			      placement: 'right',
			      arrowOffset: 30,
			      yOffset: 68,
			      xOffset: 240,
			    },
			    
			    {
			      target: 'eventDescription',
			      content:  getValues(companyLabels, "HopEventDescription"),
			       placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			    },
			    
			    {
			      target: 'taskDocCreateImg',
			      content:  getValues(companyLabels, "HopMyTaskEventImgDescription"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			    },
			    {
			      target: 'taskEstContainer',
			      content:  getValues(companyLabels, "HopMyEventWorkingHours"),
			      placement: 'bottom',
			      arrowOffset: 250,
			      yOffset: -50,
			      xOffset: 275,
			    },
			    
			    {
			      target: 'wsTaskUserContainer',
			      content: getValues(companyLabels, "HopMyEventSelectAdd"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -1,
			      xOffset: 5,
			     },
			     
			     {
			      target: 'selectLocationImg',
			      content: getValues(companyLabels, "HopEventLocation"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -62,
			      xOffset: -7,
			    },
			    
			    {
			      target: 'participantHeader',
			      content: getValues(companyLabels, "HopMyEventkAddRemove"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -25,
			      xOffset: 1,
			    },
			    
			    {
			    
			      target: 'prtcpntSection',
			      content: getValues(companyLabels, "HopMyEventDelete"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: -65,
			      xOffset: 5,
			      
			    },
			    
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		myTasksViewFrWorkFlow = {
			id: 'hopscoth-myTasksViewFrWorkFlow',
		  	steps: [
		    	{
			      target: 'selectTask',
			      content:  getValues(companyLabels, "HopWorkTaskType"),
			      placement: 'bottom',
			      arrowOffset: 6,
			      yOffset: -8,
			      xOffset:-18,
			    },
			    {
			      target: 'workFlowName',
			      content: getValues(companyLabels, "HopMyWorkFlowTaskType"),
			      placement: 'right',
			      arrowOffset: 40,
			      yOffset: -55,
			    
			   
			    },
			   {
			      target: 'wsTaskUserContainer',
			      content: getValues(companyLabels, "HopMyTaskSelectAdd"),
			      placement: 'left',
			      arrowOffset: 50,
			      yOffset: -1,
			      xOffset: 5,
			    },
			    {
			      target: 'taskWFDetails',
			      content: getValues(companyLabels, "HopMyTaskAddRemove"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset:-13,
			      xOffset: -25,
			    },
			    {
			      target: 'taskWFDetails',
			      content: getValues(companyLabels, "HopMyTaskDelete"),
			      placement: 'right',
			      arrowOffset: 50,
			      yOffset: 240,
			      xOffset: -27,
			    },
			   
			  ],
		  	showPrevButton: true,
		  	scrollTopMargin: 100
		}
		
		if(loadType == 'home'){
			startTzTour();
		  	startMzTour();
		  	startEzTour();
			startNotiTour();
			startNewProjTour();
		}else if(loadType == 'projects'){
		  	startNewProjTour();
		  	startMyProjTour();
		  	startSubscribeProjTour();
		}else if(loadType == 'activityFeed'){
		  	startConvoTour();
		}else if(loadType == 'task'){
		  	startTaskTour();
		}else if(loadType == 'document' || loadType == 'myDocument'){
		  	startDocTour();
		}else if(loadType == 'team'){
		  	startTeamTour();
		}else if(loadType == 'email'){
		  	startEmailTour();
		}else if(loadType == 'idea'){
		  	startIdeaTour();
		}else if(loadType == 'story'){
		  	startStoryTour();
		}else if(loadType == 'sprint'){
		  	startSprintTour();
		  	//alert(loadType);
		}else if(loadType == 'myMessage'){
		  	startMsgTour();
		}else if(loadType == 'myNote'){
		  	startNotesTour();
		}else if(loadType == 'myNotes'){
			 startMyNotesTour();
		}else if(loadType=='CreateNotes'){
			 startCreateNotesTour();
		}else if(loadType == 'myBlog'){
		  	startBlogTour();
		}else if(loadType == 'myGallery'){
		  	startGalleryTour();
		}else if(loadType == 'report'){
		  	startAnalyticTour();
		}else if(loadType == 'prjAdmin'){
		  	startProjAdminTour();
		}else if(loadType == 'myTaskHop'){
		    startMyTaskAdminTour();
		}else if(loadType == 'myTaskHopEdit'){
			startMyTaskViewTour();
		}else if(loadType == 'myTaskHopWorkflow'){
			startMyTaskViewTourFrWrkFlow();
		}else if(loadType =='myTaskHopEvent') {
		    startMyTaskEvent();
	    }else if(loadType =='emailIntegration') {
		    startMyEmailTour();
	    }else if(loadType =='chatIntegration') {
	    	startMyChatTour();
	    }
	    
  	  }

  	
  	
  	

