


var googleUser = {};

function startApp() {
  
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id:  client_id ,  //This will change depent Upon Googl e account
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  };



  function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
            var profile = googleUser.getBasicProfile(); 
            var id_token = googleUser.getAuthResponse().id_token;
            console.log(id_token)
            googleSignIn(id_token);
        }, function(error) {
          console.log(error);
        });
  }

  function googleSignIn(id_token){
    $.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='+id_token)
        .then(function (response) {
            var idToken = id_token;
            var iss = response.iss;
            var at_hash = response.at_hash;
            var aud = response.aud;
            var sub = response.sub;
            var email_verified = response.email_verified;
            var azp = response.azp;
            var email = response.email;
            var iat = response.iat;
            var exp = response.exp;
            var given_name = response.given_name;
            var family_name = response.family_name;
            var alg = response.alg;
            var kid = response.kid;
            console.log("<!---- "+sub+"-----"+email+"-----"+given_name+"----"+family_name+" -->");
            if(aud == client_id && email_verified == "true"){
                $('#loadingBar').show();
                timerControl("start");
                // verifyUser(sub,email,given_name,family_name,'google');
                checkUserExsitOrNot('google',sub,email,given_name,family_name);
            }


        })
        .catch(function (error) {
            alert(error);
        })
        .then(function () {
            // always executed
        });
  }


  
// console.log(app_id);
  window.fbAsyncInit = function() {
    FB.init({
      appId      : app_id,
      xfbml      : true,
      version    : 'v13.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));


   FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
        //display user data
        getFbUserData();
    }
});

function fbLogin() {
  FB.login(function (response) {
      if (response.authResponse) {
          // Get and display the user profile data
          console.log(app_id);
          getFbUserData();
      } else {
          document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
      }
  }, {scope: 'email'});
}

function getFbUserData(){
  FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
  function (response) {
        var fbId = response.id;
        var fbEmail = response.email;
        var fbFirst_name = response.first_name;
        var fbLast_name = response.last_name;
        // verifyUser(fbId, fbEmail, fbFirst_name, fbLast_name, "facebook");
        console.log("<!---- "+fbId+"-----"+fbEmail+"-----"+fbFirst_name+"----"+fbLast_name+" -->");
        checkUserExsitOrNot('facebook',fbId,fbEmail,fbFirst_name,fbLast_name);

    });
}


var userIdNewForOpenId = "";
  function checkUserExsitOrNot(type, id,email,given_name,family_name){

    let updateTaskHours = {
        "type" : type,
        "token" : id
      }
      
      console.log("up---->"+JSON.stringify(updateTaskHours));
      
      $.ajax({
        url: apiPath + "/" + myk + "/v1/googlelogin",
        type:"post",
        dataType:"json",
        contentType:"application/json",
        data:JSON.stringify(updateTaskHours),
        error: function(jqXHR, textStatus, errorThrown) {
                      checkError(jqXHR,textStatus,errorThrown);
                      $("#loadingBar").hide();
              timerControl("");
        },
        success:function(result){
          console.log(result);
          console.log(result[0].result);
          if(result[0].result=='success'){
            console.log(result[0].user_id);
            userIdNewForOpenId = result[0].user_id;
            getUserDetails(userIdNewForOpenId,type);
          }else{
            registerUserNew(type, id,email,given_name,family_name);
          }
        }
      });

  }

  function registerUserNew(type1, id,email,given_name,family_name){
          // var type = $('#selectionReg option:selected').attr("id")
          // var upgradePlan = $('#selPackage option:selected').attr("value");/
          // var fname = $("input#userFirstName").val();
          // var email = $("input#email").val();
          // var userId = $("input#userId").val();
          // var fnlen = $.trim(fname).length;
          // var address = $("textarea#address").val();
          var characterReg = /^\s*[a-zA-Z0-9,',_,\-,.,@,\s]+\s*$/;
          // var orgName = $("input#org_name").val();
      //    var country = $("#stan_country option:selected").text();		 
          //console.log("Country::"+country);
          // var lname = $("input#userLastName").val().trim();
          // var lnlen = $.trim(lname).length;
          var compPhoneNumber = "";
          // var timezone = $('select#ent_timezone option:selected').val();			  
          var country = $("#stan_country option:selected").text();			  
          var localOffsetTime=getTimeOffset(new Date());
          var PhoneReg = /^\s*[0-9,+,-,\s]+\s*$/;
          // var promocode = $("input#social_promocode").val().trim();
          // var regPlace = $('#regPlace').val();
          // var uType = '${uType}';
        //   if(type == "NotSelected"){
        //     alert("Please select your plan");
        //     //$("input#selectionReg").css('border','1px solid red');
        //     return false;
        //   }
          
        //   if(id=="" || id==null){
        //   alert("Userid cannot be empty");
        //     $("input#userId").focus();
        //     //$("input#userId").css('border','1px solid red');
        //   return false;
        // }
          
        //   if(fname == null || fname == ""){
        //   alert("First Name cannot be empty!");
        //   $("input#userFirstName").focus();
        //   //$("input#userFirstName").css('border','1px solid red');
        //   return false;
        //   }
          
        //   if(fnlen==0) {
        //   alert("First Name should not start with space");
        //   $("input#userFirstName").focus();
        //   //$("input#userFirstName").css('border','1px solid red');
        //   return false;
        // } 
          
        //   if(lname == null || lname == ""){
        //   alert("Last Name cannot be empty!");
        //   $("input#userLastName").focus();
        //   //$("input#userLastName").css('border','1px solid red');
        //   return false;
        //   }
          
        //   if(fnlen==0) {
        //   alert("Last Name should not start with space");
        //   $("input#userFirstName").focus();
        //   //$("input#userFirstName").css('border','1px solid red');
        //   return false;
        // }  
          
        
        //   // here in Regular Expression the permission to add ' (single quote) has been removed for testing user id
        //   characterReg = /^\s*[a-zA-Z0-9,_,\-,.,@,\s]+\s*$/;
        //   if(!characterReg.test(fname)) {
        //     alert("Special characters are not allowed",'warning');
        //     $("input#userFirstName").focus();
        //     //$("input#userFirstName").css('border','1px solid red');
        //     return false;
        //   }
          
        //   if(!characterReg.test(userId)) {			  
        //     alert("Special characters are not allowed",'warning');
        //     $("input#userId").focus();
        //     //$("input#userId").css('border','1px solid red');
        //     return false;
        //   }
          
          
        //   if(validateEmailJs(userId,true,false) ) {
        //     $("input#email").val(userId);
        //   }else if(!isNaN(userId)){
        //       $('#compPhoneNumber').val(userId);
        //   } 
          
          // var existCompanyId = 0;
          // if(type !="Social"){
          //     if(orgName == ""){
          //       alert("Organization Name cannot be empty",'warning');
          //     $("input#org_name").focus();
          //     //$("input#org_name").css('border','1px solid red');
          //     return false;
          //     }
          //     characterReg = /^\s*[a-zA-Z0-9,&,_,.,@,\s]+\s*$/;
          //     if(!characterReg.test(orgName)) {
          //     alert("Special characters are not allowed",'warning');
          //     $("input#org_name").focus();
          //     //$("input#org_name").css('border','1px solid red');
          //     return false;
          //     }
              
          //     if(companyExistFlag){
          //       existCompanyId  = $('#hiddenCompanyName').val();
          //       //alert(existCompanyId);
          //       if(existCompanyId!='0'){
          //           if($('#companyList option[value='+existCompanyId+']').text()!=orgName){
          //             goforWarning();
          //             return false;
          //           }
          //       }else if(warnFlag){
          //         goforWarning();
          //         return false;
          //       }
          //     }
          // }
          
          /*if(!PhoneReg.test(compPhoneNumber)){
              alert("Phone number cannot have special characters");
          return false;
          }*/
          
          // if($('#phnNumber').val().trim() !=""){
          //   compPhoneNumber = $('#phnNumber').val().trim();
          // }
        var registerDevice="web";

        // console.log(regPlace+"<-regPlace-->"+registerDevice+"<-registerDevice-->"+compPhoneNumber+"<-compPhoneNumber-->"+existCompanyId+"<-existCompanyId-->"+orgName+"<-orgName-->"+userId+"<-userId-->"+fname+"<-fname-->"+lname+"<-lname-->"+type+"<-type-->"+country+"<-country-->"+email+"<-email-->");

        // $('#loadingBar').show();
                // timerControl("start");

        let jsonbody1={
          "accType" : "Business",
          "fname" : given_name,
          "lname" : family_name,
          "uType" : "",
          "registerDevice" : registerDevice,
          "email" : email,
          "userId"  : id,
          "orgName" : "Company12345",
          "address" : "",
          "country" : country,
          "compPhoneNumber" : "",
          "regPrice" : "",
          "localOffsetTime" : localOffsetTime,
          "promocode" : "",
          "regPlace"  : "EmailRegister",
          "existCompanyId" : 0,
          "companyCreateFlag" : false,
          "upgradePlan" : "0",
          "host" : "testbeta.colabus.com",
          "loginType" : type1,
          "token" : id
        }
        $.ajax({
            url: apiPath + "/" + myk + "/v1/register",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(jsonbody1),
            error: function (jqXHR, textStatus, errorThrown) {
            checkError(jqXHR, textStatus, errorThrown);
            $('#loadingBar').hide();
            },
            success: function (result) { 
              console.log(result);
              if(result.status == "success"){
                getUserDetails(result.userId,type1);
              }
              // $('#loadingBar').hide();
            }
          });
          
    }

  function getUserDetails(userId,type1){
    $.ajax({
      url: apiPath + "/" + myk + "/v1/getUserDetails?userId="+userId+"&loginDevice=web&loginType="+type1,
      type: "POST",
      dataType: 'json',
      contentType: "application/json",
      // data: JSON.stringify(jsonbody1),
      error: function (jqXHR, textStatus, errorThrown) {
      checkError(jqXHR, textStatus, errorThrown);
      $('#loadingBar').hide();
      },
      success: function (result) { 
        console.log(result);
       
                var jsondata = JSON.stringify(result);
								var userlogininfo = JSON.parse(jsondata);
								var ctype= userlogininfo.userRType;
								var rid = userlogininfo.roleId;
								window.sessionStorage.user = JSON.stringify(result);
								window.sessionStorage.oldpassword = userPwd;
								window.location.href = hostname+"/postlogin?pAct=home&roleid="+rid+"";
								$("#loadingBar").hide();
								timerControl("");
        // $('#loadingBar').hide();
      }
    });
  }