function globalSearchOnkeyEvent(event){
    if(event.keyCode == '13' ){
          var glbtxt = $("#glbSearchInputBox").val();
          //alert("glbtxt::>>>"+glbtxt);
          if(glbtxt == "" || glbtxt == null){
         $("#globalSearchBox").css({'border':'1px solid red','border-radius':'3px','box-shadow': '0 1px 4px 0 rgba(168, 168, 168, 0.6) inset'});
          $("#globalSearchBox").focus();
      }else{
          globalSearchRedirect();
      }
    }
 }
 var elementhashindex='';
 var hashIndex ='';
 function filterGlobalSearch(event){
     
     var menuType="";
     var projectId="";
     if (event.shiftKey && event.keyCode == 51 ) {
         elementhashindex = document.getElementById('glbSearchInputBox').selectionStart;
         hashIndex = parseInt(elementhashindex)-1 ;
         $('#hashTagSearchInputBox').css('display','block');
         $('#hashTagSearchInputBox').css('margin-top','32px');
         if(hashResult == "" || hashResult == "[]"){
             $("#loadingBar").show();
             timerControl("start");
             ajaxCallforHashCode('','','hashSearch');
             $("#loadingBar").hide();
             timerControl("");
         }else{
             $('#hashTagSearchInputBox ul').html(preparingUIforHash(hashResult,projectId,'hashSearch'));
         }
         
     }else if( event.keyCode == 32){
                 elementhashindex='';
                 hashIndex ="";
                 $('#hashTagSearchInputBox:visible').hide();
     }else if(elementhashindex!=""){
                         var data=$('#glbSearchInputBox').val();
                         //console.log('hashIndex:'+data.charAt(hashIndex)+":");
                          if(data.charAt(hashIndex)=="#"){
                                  var focusIndex = document.getElementById('glbSearchInputBox').selectionStart;
                                 var aTxt = data.substring(elementhashindex,focusIndex).toLowerCase().trim();
                                 $('#hashTagProjList li').show();
                                 $('#hashTagSearchInputBox').show();
                                 if(aTxt !=""){
                                     $('#hashTagProjList li').each(function(){
                                         val = $(this).text().toLowerCase();
                                         if(val.indexOf(aTxt)!= -1 ){
                                             $(this).show();
                                         }else{
                                                $(this).hide();
                                         }
                                     });
                                     
                                     
                                     if($('#hashTagProjList li:visible').length < 1)
                                        $('#hashTagSearchInputBox').hide();
                                        else
                                           $('#hashTagSearchInputBox').show();
                              }	 
                       }else{
                          $('#hashTagSearchInputBox').css('display','none');
                          elementhashindex = "";
                          hashIndex ="";
                       } 
             }else{
                 $('#hashTagSearchInputBox:visible').hide();
             }
     var  kw =$("#glbSearchInputBox").val().toLowerCase();
     if(kw!=''){
     $("#ClearglobalSearch").show();
     }else{
     $("#ClearglobalSearch").hide();
     }
 }
 
 function ClearglobalSearchVal(){
    $("#glbSearchInputBox").val("");
    $("#ClearglobalSearch").hide();
    $("#hashTagSearchInputBox").hide();
 }
 
 function globalSearchRedirect(){
   /*alert("inside");*/
   var mAct="globalSearch";
   var glbtxtReplace="";
   var glbtxt = $("#glbSearchInputBox").val();
     if(glbtxt == "" || glbtxt == null){
         /*$("#globalSearchBox").css({'border':'1px solid red','border-radius':'3px','box-shadow': '0 1px 4px 0 rgba(168, 168, 168, 0.6) inset'});
          $("#globalSearchBox").focus();*/
       return false;
      }else{
          $('#loadingBar').show();
           timerControl("start");
          globalSearchTxt = glbtxt;
         glbtxtReplace=glbtxt.replaceAll("#","%23");
         window.location.href =path+"/Redirect.do?pAct="+mAct+"&searchVal="+glbtxtReplace;
      }
   
 }
 
 function loadGlobalSearchResults(count){
      $('#loadingBar').show();
      timerControl("start");
      var txt = globalSearchTxt;
      if(txt == "" || txt == null){
         $("#globalSearchBox").css({'border':'1px solid red','border-radius':'3px','box-shadow': '0 1px 4px 0 rgba(168, 168, 168, 0.6) inset'});
          $("#globalSearchBox").focus();
          return false;
     }else{
          $.ajax({
             url: path+'/globalSearchAction.do',
             type:"POST",
             data:{act:"fetchSearchResults",txt:txt,searchTypeAlertConfirm:searchTypeAlertConfirm},
             error: function(jqXHR, textStatus, errorThrown) {
                     checkError(jqXHR,textStatus,errorThrown);
                     $("#loadingBar").hide();
                     timerControl("");
                     }, 
             success:function(result){
                 checkSessionTimeOut(result);
                 prepareSearchUI(result, count, 'Repository');
                 $('#loadingBar').hide();
                  timerControl("");
             }
         }); 
     }
  }

  var abort = "";
  function  fetchCount(){ 
      //var txt = $('#globalSearchBox').val().trim();
      /*alert("inside fetchCount");	
      alert("searchAlertConfirm:::::"+searchAlertConfirm);
      alert("searchTypeAlertConfirm:::::"+searchTypeAlertConfirm);*/
      //$('#loadingBar1').show();
      //timerControl("start");
      var txt = globalSearchTxt;
      var gtype = 'workspace';
      
      if(txt == "" || txt == null ){
         $("#globalSearchDiv").removeClass('searchIconDiv').addClass('searchError');
          $("#globalSearchBox").focus();
          return false;
     }else{
         $('#loadingBar').show();
          timerControl("start");
          var count = 1;
          abort = $.ajax({
              url: path+'/globalSearchAction.do',
              type:"POST",
              data:{act: "fetchCount",txt:txt,gtype:gtype,searchTypeAlertConfirm:searchTypeAlertConfirm,searchAlertConfirm:searchAlertConfirm},
              error: function(jqXHR, textStatus, errorThrown) {
                     checkError(jqXHR,textStatus,errorThrown);
                     $("#loadingBar").hide();
                     timerControl("");
                     }, 
              success:function(result){
                  //console.log("Result:::"+result);
                  //alert("Result::::"+result);
                  checkSessionTimeOut(result);
                  $('#searchResults').show();
                 //$('#loadingBar').show();
                 /*var windowWidth = $(swindow).width()-90; 
                 var windowHeight = parent.$('#menuFrame').height();
                  $("div#searchResults").css("height",windowHeight);
                   $("div#searchResults").css("width",windowWidth);
                   var contHeight = windowHeight - $('#serachResultsHeader').height();
                   $('div#searchResultsCont').css('height',contHeight);*/
                   $('#transparentDiv').show();
                  prepareSearchResults(result);
                  //$("#loadingBar").hide();
                  //timerControl("");
                  $('#transparentDiv').hide();
                  $('#loadingBar1').hide();
                  var repCount = '';
                  var conCount = '';
                  var wrkCount = '';
                  var ideasCount = '';
                  var agileCount = '';
                  var calCount = '';
                  var myZCount = '';
                  var confCount = '';
                  
                  var repoCount = result.split('*.*')[0];
                  if(repoCount != "[]"){
                      var jsonRepoData = JSON.parse(repoCount);
                      if(jsonRepoData){
                         var json = eval(jsonRepoData);
                         for(var i = 0;i < json.length;i++){
                             repCount = json[i].count;	
                         }
                     }
                  }
                  
                  var connectCount = result.split('*.*')[1];
                  if(connectCount != "[]"){
                      var jsonConData = JSON.parse(connectCount);
                     if(jsonConData){
                         var json = eval(jsonConData);
                         for(var i = 0;i < json.length;i++){
                             conCount = json[i].count;
                         }
                     }
                  }
                  
                  var wrkspaceCount = result.split('*.*')[2];
                  if(wrkspaceCount != "[]"){
                      var jsonWrkData = JSON.parse(wrkspaceCount);
                     if(jsonWrkData){
                         var json = eval(jsonWrkData);
                         for(var i = 0;i < json.length;i++){
                             wrkCount = json[i].count;
                         }
                     }
                  }
                  
                  var ideasCount = result.split('*.*')[3];
                  if(ideasCount != "[]"){
                      var jsonIdeasData = JSON.parse(ideasCount);
                     if(jsonIdeasData){
                         var json = eval(jsonIdeasData);
                         for(var i = 0;i < json.length;i++){
                             ideasCount = json[i].count;
                         }
                     }
                 }
                 
                 var agileCount = result.split('*.*')[4];
                 if(agileCount != "[]"){
                     var jsonAgileData = JSON.parse(agileCount);
                      if(jsonAgileData){
                         var json = eval(jsonAgileData);
                         for(var i = 0;i < json.length;i++){
                             agileCount = json[i].count;	
                         }
                     }
                  }
                  
                  var myZoneCount = result.split('*.*')[6];
                 if(myZoneCount != "[]"){
                     var jsonMyZoneCountData = JSON.parse(myZoneCount);
                     if(jsonMyZoneCountData){
                         var json = eval(jsonMyZoneCountData);
                         for(var i = 0; i < json.length; i++){
                             myZCount = json[i].count;
                         }
                     }
                  }
                  var calendarCount = result.split('*.*')[5];
                  if(calendarCount != "[]"){
                     var jsonCalendarData = JSON.parse(calendarCount);
                     if(jsonCalendarData){
                         var json = eval(jsonCalendarData);
                         for(var i = 0; i < json.length; i++){
                             calCount = json[i].count;
                         }
                     }
                  }
                  
                  var wrkspaceEmailCount = result.split('*.*')[7];
                 if(wrkspaceEmailCount != "[]"){
                     var wrkspaceEmailCountJson = JSON.parse(wrkspaceEmailCount);
                     if(wrkspaceEmailCountJson){
                         var json = eval(wrkspaceEmailCountJson);
                         for(var i = 0; i < json.length; i++){
                             wrkspaceEmail = json[i].count;
                         }
                     }
                  }
                 
                 var confluenceCount = result.split('*.*')[8];
                 if(confluenceCount != "[]"){
                     var confluenceCountJson = JSON.parse(confluenceCount);
                     if(confluenceCountJson){
                         var json = eval(confluenceCountJson);
                         for(var i = 0; i < json.length; i++){
                             confCount = json[i].count;
                         }
                     }
                  }
                    
                 
                  if(repCount != 0){
                      loadGlobalSearchResults(count);
                  }else if(conCount != 0){
                      fetchConnectResults(txt, count);
                  }else if(wrkCount != 0){
                      fetchWorkspaceResults(txt, count);
                  }else if(myZCount != 0){
                      fetchMyZoneResults(txt, count);	
                  }else if(ideasCount != 0){
                      fetchIdeasResults(txt, count);
                  }else if(agileCount != 0){
                      fetchAgileResults(txt, count);
                  }else if(calCount != 0){
                      fetchCalendarResults(txt, count);	
                  }else if(wrkspaceEmail != 0){
                      fetchwrkspaceEmail(txt, count);	
                  }else if(confCount != 0){
                      fetchConfData(txt, count);
                  }else{
                      if(searchAlertConfirm == "" && cancelSearchOD == ""){         
                         confirmReset(getValues(companyAlerts,"Alert_Drives"),"delete","OptionalDrives",'searchCancelOptionalDrives');
                         $('.alertMainCss').css({'width': '30.5%'});
                         $('.alertMainCss').css({'left': '45%'});
                         $('#BoxConfirmBtnOk').val('Yes');
                         $('#BoxConfirmBtnCancel').val('No');
                         $('#BoxConfirmBtnOk').css({'width': '19%'});
                             
                     }
                     if(searchAlertConfirm == 'OptionalDrives' && searchTypeAlertConfirm == "" && searchAllContentVar == ""){
                         confirmReset(getValues(companyAlerts,"Alert_ContSearch"),'delete','searchAllContent','searchExceptFileContent');
                         $('.alertMainCss').css({'width': '30.5%'});
                         $('.alertMainCss').css({'left': '45%'});
                         $('#BoxConfirmBtnOk').val('Yes');
                         $('#BoxConfirmBtnCancel').val('No');
                         $('#BoxConfirmBtnOk').css({'width': '19%'});
                     }
                      /*if(searchTypeAlertConfirm == "" && searchAllContentVar == ""){
                          confirmReset("Do you want to continue search in File Contents too?  <br><br>  Please note that search in File Contents may take several minutes.",'delete','searchAllContent','searchExceptFileContent');
                         $('.alertMainCss').css({'width': '30.5%'});
                         $('.alertMainCss').css({'left': '45%'});
                         $('#BoxConfirmBtnOk').val('Yes');
                         $('#BoxConfirmBtnCancel').val('No');
                         $('#BoxConfirmBtnOk').css({'width': '19%'});
                     }*/
                      //$('#loadingBar').hide();
                      //timerControl("");
                      $('#searchResultsCont').html('No results found');
                      $('#searchResultsCont').css({'color':'black', 'font-weight':'bold'});
                  }
                  
              }
          });
      }
      
  }
  function searchafteralertpopUp(){
    $('#loadingBar').hide();
    timerControl("");
    $("#confirmDiv").hide();
  }
  var searchTypeAlertConfirm="";
  var searchAllContentVar="";
  function searchAllContent(){
      searchTypeAlertConfirm = "searchFileContntToo";
      fetchCount();
  }
  
  var searchAlertConfirm = "";	
  var cancelSearchOD="";
  function OptionalDrives(){
      //alert("inside OptionalDrives function");
      searchAlertConfirm = "OptionalDrives";
      fetchCount();
  }
  
  
  function searchExceptFileContent(){
      searchAllContentVar = "searchAllContent" ;
      $('#loadingBar').hide();
      $('#loadingBar1').hide();
      timerControl("");
      //searchTypeAlertConfirm = "searchFileContntToo";
      $("#confirmDiv").hide();
      abort.abort();
  }
  function prepareSearchResults(result){
      $('#loadingBar').show();
      timerControl("start");
      //alert("Result length:::"+result.length);
      var repoCount = result.split('*.*')[0];
      var connectCount = result.split('*.*')[1];
      var wrkspaceCount = result.split('*.*')[2];
      var ideasCount = result.split('*.*')[3];
      var agileCount = result.split('*.*')[4];
      var calendarCount = result.split('*.*')[5];
      var myZoneCount = result.split('*.*')[6];
      var wrkspaceEmail = result.split('*.*')[7];		
      var confluenceCount = result.split('*.*')[8];
      var goDriveCount = result.split('*.*')[9];
      var goProjectDriveCount = result.split('*.*')[10];
      var oneDriveCount = result.split('*.*')[11];
      var oneDriveProjectCount = result.split('*.*')[12]; 		
      //alert("inside proj::::countUI"+oneDriveProjectCount);
      //alert("wrkspaceEmail:::>>"+wrkspaceEmail);
      $("#searchResultsCont").html('');
      var searchResRepo = '';
      var searchResConnect = '';
      var searchResWrkspace = '';
      var searchResIdeas = '';
      var searchResAgile = '';
      var searchResCalendar = '';
      var searchResMyZone = '';
      var searchResEmail = '';
      var searchResConfluence = '';
      var searchResgoogleDrive = '';
      var searchProjectgoogleDrive='';
      var searchProjectOneDrive='';
      var searchOneDrive='';
      
      //var searchAlertConfirm = "OptionalDrives";
      
      var count = 0;
      var prevModule = '';		
          if(goDriveCount == "" || goProjectDriveCount == "" || oneDriveCount == "" || oneDriveProjectCount == ""){
      var jsonRepoData = JSON.parse(repoCount);
     if(jsonRepoData){
         var json = eval(jsonRepoData);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                  count = count + 1;
                 searchResRepo = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Myzone | Documents</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             +"<div style=\"width: 15%;\" title=\"Share Type\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 35%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                             +"<div style=\"width: 19.5%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 17%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                     +"</div>"
                 $("#searchResultsCont").append(searchResRepo);
             }
         }
     }
     var jsonConnectData = JSON.parse(connectCount);
     if(jsonConnectData){
         var json = eval(jsonConnectData);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                 count = count + 1;
                 searchResConnect = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | "+json[i].moduleName+"</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             +"<div style=\"width: 23%;\" title=\"Name\" class=\"tabContentHeaderName\">Name</div>"
                                             +"<div style=\"width: 22%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Department</div>"
                                             +"<div style=\"width: 19%;\" title=\"Mobile\"  class=\"tabContentHeaderName\">Mobile</div>"
                                             +"<div style=\"width: 15%;\" title=\"Work\"  class=\"tabContentHeaderName\">Work</div>"
                                             +"<div style=\"width: 20%;\" title=\"Email\"  class=\"tabContentHeaderName\">Email</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                     $("#searchResultsCont").append(searchResConnect);
                 }
             }
         }
     
     var jsonWrkspaceData = JSON.parse(wrkspaceCount);
     if(jsonWrkspaceData){
         var json = eval(jsonWrkspaceData);
         for(var i = 0;i < json.length;i++){
         if(json[i].count != 0) {
             count = count + 1;
             searchResWrkspace = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | Existing Workspace</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             + "<div style=\"width: 16.5%;\" title=\"Share Type\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 6%;\"  class=\"tabContentHeaderName\" ></div>"
                                             +"<div style=\"width: 15%;\" title=\"Workspace\"   class=\"tabContentHeaderName\" >Workspace Name</div>"
                                             +"<div style=\"width: 22.7%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                             +"<div style=\"width: 16%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 16%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>" 
                                     +"</div>" 
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                                 $("#searchResultsCont").append(searchResWrkspace);
                 }
             }
         }
     
     var jsonIdeaData = JSON.parse(ideasCount);
     if(jsonIdeaData){
         var json = eval(jsonIdeaData);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                 count = count + 1;
                 searchResIdeas = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | Existing Workspace | Idea</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             + "<div style=\"width: 14%;\" title=\"Type\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 43%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                             +"<div style=\"width: 14.7%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 15.8%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>" 
                                     +"</div>" 
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                                 $("#searchResultsCont").append(searchResIdeas);
             }
         }
     }
     
     var jsonAgileData = JSON.parse(agileCount);
     if(jsonAgileData){
         var json = eval(jsonAgileData);
         for(var i = 0;i < json.length;i++){
         if(json[i].count != 0) {
             count = count + 1;
             searchResAgile = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | Existing Workspace | Agile</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             + "<div style=\"width: 14.3%;\" title=\"Share Type\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 6%;\"  class=\"tabContentHeaderName\" ></div>"
                                             +"<div style=\"width: 16%;\" title=\"Workspace\"   class=\"tabContentHeaderName\" >Project</div>"
                                             +"<div style=\"width: 22%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                             +"<div style=\"width: 19%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 16%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>" 
                                     +"</div>" 
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                                 $("#searchResultsCont").append(searchResAgile);
                 }
             }
         }
     var jsonCalendarData = JSON.parse(calendarCount);
     if(jsonCalendarData){
         var json = eval(jsonCalendarData);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                  count = count + 1;
                 searchResCalendar = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Myzone | "+json[i].moduleName+"</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             +"<div style=\"width: 13.3%;\" title=\"Share Type\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 38.3%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                             +"<div style=\"width: 20%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 15%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                     +"</div>"
                 $("#searchResultsCont").append(searchResCalendar);
             }
         }
     }
     
     var jsonMyZoneData = JSON.parse(myZoneCount);
     if(jsonMyZoneData){
         var json = eval(jsonMyZoneData);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                 count = count + 1;
                 searchResMyZone = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">"+json[i].moduleName+"</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             + "<div style=\"width: 14%;\" title=\"Type\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 14.8%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 41.7%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Title</div>"
                                             +"<div style=\"width: 16%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>" 
                                     +"</div>" 
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left:3%float: left;width: 97%\"></div>"
                                 +"</div>"
                                 $("#searchResultsCont").append(searchResMyZone);
             }
         }
     }
     //Email
     var jsonwrkspaceEmail = JSON.parse(wrkspaceEmail);
     if(jsonwrkspaceEmail){
         var json = eval(jsonwrkspaceEmail);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                 count = count + 1;
                 searchResEmail = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | "+json[i].moduleName+"</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div  style=\"padding-left: 3px;\">"
                                             +"<div style=\"width: 14.4%;\" title=\"Name\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 16.4%;\" title=\"Name\" class=\"tabContentHeaderName\">Workspace Name</div>"
                                             +"<div style=\"width: 36.8%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Subject</div>"
                                             +"<div style=\"width: 17%;\" title=\"Name\" class=\"tabContentHeaderName\">From</div>"
                                             +"<div style=\"width: 10%;\" title=\"Email\"  class=\"tabContentHeaderName\">Date</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                     $("#searchResultsCont").append(searchResEmail);
                 }
             }
         }
     
     //Confluence
     var jsonConfluence = JSON.parse(confluenceCount);
     if(jsonConfluence){
         var json = eval(jsonConfluence);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                 count = count + 1;
                 searchResConfluence = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">"+json[i].moduleName+"</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             +"<div style=\"width: 14.7%;\" title=\"Name\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 65%;\" title=\"Name\" class=\"tabContentHeaderName\">Title</div>"
                                             //+"<div style=\"width: 31%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Subject</div>"
//												+"<div style=\"width: 26%;\" title=\"Name\" class=\"tabContentHeaderName\">From</div>"
                                             +"<div style=\"width: 20%;\" title=\"Email\"  class=\"tabContentHeaderName\">Status</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                                 //alert(searchResConfluence);
                     $("#searchResultsCont").append(searchResConfluence);
                 }
             }
         }
         var count = 0;
         var videoUI = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \"Video\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Media Files</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">1</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"      
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             +"<div style=\"width: 14.7%;\" title=\"Type\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 65%;\" title=\"Title\" class=\"tabContentHeaderName\">Title</div>"
                                             //+"<div style=\"width: 31%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Subject</div>"
//												+"<div style=\"width: 26%;\" title=\"Name\" class=\"tabContentHeaderName\">From</div>"
                                             //+"<div style=\"width: 20%;\" title=\"Email\"  class=\"tabContentHeaderName\">Status</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                                 //alert(searchResConfluence);
                     $("#searchResultsCont").append(videoUI);
      }else{

           var jsonRepoData = JSON.parse(repoCount);
          if(jsonRepoData){
              var json = eval(jsonRepoData);
              for(var i = 0;i < json.length;i++){
                  if(json[i].count != 0){
                       count = count + 1;
                      searchResRepo = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                          +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                              +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                                  +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                                  +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Myzone | Documents</li>" 
                                                  +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                      +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                      +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                      +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                                  +"</li>"       
                                              +"</div>"  
                                              
                                          +"</div>"
                                          +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                              +"<div class=\"tabContentSubHeader\">"
                                                  +"<div style=\"width: 15%;\" title=\"Share Type\" class=\"tabContentHeaderName\">Type</div>"
                                                  +"<div style=\"width: 35%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                                  +"<div style=\"width: 19.5%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                                  +"<div style=\"width: 17%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                                  +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                              +"</div>"
                                          +"</div>" 
                                          
                                          +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                          +"</div>"
                      $("#searchResultsCont").append(searchResRepo);
                  }
              }
          }
          var jsonConnectData = JSON.parse(connectCount);
          if(jsonConnectData){
              var json = eval(jsonConnectData);
              for(var i = 0;i < json.length;i++){
                  if(json[i].count != 0){
                      count = count + 1;
                      searchResConnect = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                          +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                              +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                                  +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                                  +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | "+json[i].moduleName+"</li>" 
                                                  +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                      +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                      +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                      +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                                  +"</li>"       
                                              +"</div>"  
                                              
                                          +"</div>"
                                          +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                              +"<div class=\"tabContentSubHeader\">"
                                                  +"<div style=\"width: 23%;\" title=\"Name\" class=\"tabContentHeaderName\">Name</div>"
                                                  +"<div style=\"width: 22%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Department</div>"
                                                  +"<div style=\"width: 19%;\" title=\"Mobile\"  class=\"tabContentHeaderName\">Mobile</div>"
                                                  +"<div style=\"width: 15%;\" title=\"Work\"  class=\"tabContentHeaderName\">Work</div>"
                                                  +"<div style=\"width: 20%;\" title=\"Email\"  class=\"tabContentHeaderName\">Email</div>"
                                              +"</div>"
                                          +"</div>" 
                                          
                                          
                                      +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                      +"</div>"
                          $("#searchResultsCont").append(searchResConnect);
                      }
                  }
              }
          
          var jsonWrkspaceData = JSON.parse(wrkspaceCount);
          if(jsonWrkspaceData){
              var json = eval(jsonWrkspaceData);
              for(var i = 0;i < json.length;i++){
              if(json[i].count != 0) {
                  count = count + 1;
                  searchResWrkspace = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                          +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                              +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                                  +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                                  +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | Existing Workspace</li>" 
                                                  +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                      +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                      +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                      +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                                  +"</li>"       
                                              +"</div>"  
                                              
                                          +"</div>"
                                          +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                              +"<div class=\"tabContentSubHeader\">"
                                                  + "<div style=\"width: 16.5%;\" title=\"Share Type\" class=\"tabContentHeaderName\">Type</div>"
                                                  +"<div style=\"width: 6%;\"  class=\"tabContentHeaderName\" ></div>"
                                                  +"<div style=\"width: 15%;\" title=\"Workspace\"   class=\"tabContentHeaderName\" >Workspace Name</div>"
                                                  +"<div style=\"width: 22.7%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                                  +"<div style=\"width: 16%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                                  +"<div style=\"width: 16%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                                  +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                              +"</div>" 
                                          +"</div>" 
                                      +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                      +"</div>"
                                      $("#searchResultsCont").append(searchResWrkspace);
                      }
                  }
              }
          
          var jsonIdeaData = JSON.parse(ideasCount);
          if(jsonIdeaData){
              var json = eval(jsonIdeaData);
              for(var i = 0;i < json.length;i++){
                  if(json[i].count != 0){
                      count = count + 1;
                      searchResIdeas = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                          +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                              +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                                  +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                                  +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | Existing Workspace | Idea</li>" 
                                                  +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                      +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                      +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                      +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                                  +"</li>"       
                                              +"</div>"  
                                              
                                          +"</div>"
                                          +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                              +"<div class=\"tabContentSubHeader\">"
                                                  + "<div style=\"width: 14%;\" title=\"Type\" class=\"tabContentHeaderName\">Type</div>"
                                                  +"<div style=\"width: 43%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                                  +"<div style=\"width: 14.7%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                                  +"<div style=\"width: 15.8%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                                  +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                              +"</div>" 
                                          +"</div>" 
                                      +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                      +"</div>"
                                      $("#searchResultsCont").append(searchResIdeas);
                  }
              }
          }
          
          var jsonAgileData = JSON.parse(agileCount);
          if(jsonAgileData){
              var json = eval(jsonAgileData);
              for(var i = 0;i < json.length;i++){
              if(json[i].count != 0) {
                  count = count + 1;
                  searchResAgile = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                          +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                              +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                                  +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                                  +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | Existing Workspace | Agile</li>" 
                                                  +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                      +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                      +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                      +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                                  +"</li>"       
                                              +"</div>"  
                                              
                                          +"</div>"
                                          +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                              +"<div class=\"tabContentSubHeader\">"
                                                  + "<div style=\"width: 14.3%;\" title=\"Share Type\" class=\"tabContentHeaderName\">Type</div>"
                                                  +"<div style=\"width: 6%;\"  class=\"tabContentHeaderName\" ></div>"
                                                  +"<div style=\"width: 16%;\" title=\"Workspace\"   class=\"tabContentHeaderName\" >Project</div>"
                                                  +"<div style=\"width: 22%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                                  +"<div style=\"width: 19%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                                  +"<div style=\"width: 16%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                                  +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                              +"</div>" 
                                          +"</div>" 
                                      +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                      +"</div>"
                                      $("#searchResultsCont").append(searchResAgile);
                      }
                  }
              }
          var jsonCalendarData = JSON.parse(calendarCount);
          if(jsonCalendarData){
              var json = eval(jsonCalendarData);
              for(var i = 0;i < json.length;i++){
                  if(json[i].count != 0){
                       count = count + 1;
                      searchResCalendar = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                          +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                              +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                                  +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                                  +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Myzone | "+json[i].moduleName+"</li>" 
                                                  +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                      +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                      +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                      +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                                  +"</li>"       
                                              +"</div>"  
                                              
                                          +"</div>"
                                          +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                              +"<div class=\"tabContentSubHeader\">"
                                                  +"<div style=\"width: 13.3%;\" title=\"Share Type\" class=\"tabContentHeaderName\">Type</div>"
                                                  +"<div style=\"width: 38.3%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Title</div>"
                                                  +"<div style=\"width: 20%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Path</div>"
                                                  +"<div style=\"width: 15%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                                  +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                              +"</div>"
                                          +"</div>" 
                                          
                                          +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                          +"</div>"
                      $("#searchResultsCont").append(searchResCalendar);
                  }
              }
          }
          
          var jsonMyZoneData = JSON.parse(myZoneCount);
          if(jsonMyZoneData){
              var json = eval(jsonMyZoneData);
              for(var i = 0;i < json.length;i++){
                  if(json[i].count != 0){
                      count = count + 1;
                      searchResMyZone = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                          +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                              +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                                  +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                                  +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">"+json[i].moduleName+"</li>" 
                                                  +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                      +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                      +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                      +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                                  +"</li>"       
                                              +"</div>"  
                                              
                                          +"</div>"
                                          +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                              +"<div class=\"tabContentSubHeader\">"
                                                  + "<div style=\"width: 14%;\" title=\"Type\" class=\"tabContentHeaderName\">Type</div>"
                                                  +"<div style=\"width: 14.8%;\" title=\"Title\"   class=\"tabContentHeaderName\" >Path</div>"
                                                  +"<div style=\"width: 41.7%;\" title=\"Path\"   class=\"tabContentHeaderName\" >Title</div>"
                                                  +"<div style=\"width: 16%;\" title=\"Created By\"  class=\"tabContentHeaderName\">Created By</div>"
                                                  +"<div style=\"width: 12%;\" title=\"Created Timestamp\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                              +"</div>" 
                                          +"</div>" 
                                      +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left:3%float: left;width: 97%\"></div>"
                                      +"</div>"
                                      $("#searchResultsCont").append(searchResMyZone);
                  }
              }
          }
          //Email
          var jsonwrkspaceEmail = JSON.parse(wrkspaceEmail);
          if(jsonwrkspaceEmail){
              var json = eval(jsonwrkspaceEmail);
              for(var i = 0;i < json.length;i++){
                  if(json[i].count != 0){
                      count = count + 1;
                      searchResEmail = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                          +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                              +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                                  +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                                  +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">Teamzone | "+json[i].moduleName+"</li>" 
                                                  +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                      +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                      +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                      +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                                  +"</li>"       
                                              +"</div>"  
                                              
                                          +"</div>"
                                          +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                              +"<div  style=\"padding-left: 3px;\">"
                                                  +"<div style=\"width: 14.4%;\" title=\"Name\" class=\"tabContentHeaderName\">Type</div>"
                                                  +"<div style=\"width: 16.4%;\" title=\"Name\" class=\"tabContentHeaderName\">Workspace Name</div>"
                                                  +"<div style=\"width: 36.8%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Subject</div>"
                                                  +"<div style=\"width: 17%;\" title=\"Name\" class=\"tabContentHeaderName\">From</div>"
                                                  +"<div style=\"width: 10%;\" title=\"Email\"  class=\"tabContentHeaderName\">Date</div>"
                                              +"</div>"
                                          +"</div>" 
                                          
                                          
                                      +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                      +"</div>"
                          $("#searchResultsCont").append(searchResEmail);
                      }
                  }
              }
          
          //Confluence
          var jsonConfluence = JSON.parse(confluenceCount);
          if(jsonConfluence){
              var json = eval(jsonConfluence);
              for(var i = 0;i < json.length;i++){
                  if(json[i].count != 0){
                      count = count + 1;
                      searchResConfluence = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                          +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                              +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                                  +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                                  +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">"+json[i].moduleName+"</li>" 
                                                  +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                      +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                      +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                      +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                                  +"</li>"       
                                              +"</div>"  
                                              
                                          +"</div>"
                                          +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                              +"<div class=\"tabContentSubHeader\">"
                                                  +"<div style=\"width: 14.7%;\" title=\"Name\" class=\"tabContentHeaderName\">Type</div>"
                                                  +"<div style=\"width: 65%;\" title=\"Name\" class=\"tabContentHeaderName\">Title</div>"
                                                  //+"<div style=\"width: 31%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Subject</div>"
// 													+"<div style=\"width: 26%;\" title=\"Name\" class=\"tabContentHeaderName\">From</div>"
                                                  +"<div style=\"width: 20%;\" title=\"Email\"  class=\"tabContentHeaderName\">Status</div>"
                                              +"</div>"
                                          +"</div>" 
                                          
                                          
                                      +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 3%;float: left;width: 97%\">"+"</div>"
                                      +"</div>"
                                      //alert(searchResConfluence);
                          $("#searchResultsCont").append(searchResConfluence);
                      }
                  }
              }
          
     
     
     //oneDrive
     var jsongDrive = JSON.parse(oneDriveCount);
     if(jsongDrive){
         
         var json = eval(jsongDrive);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                 count = count + 1;
                 searchOneDrive = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">"+json[i].moduleName+"</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             +"<div style=\"width: 14.7%;\" title=\"Name\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 38%;\" title=\"Name\" class=\"tabContentHeaderName\">Title</div>"
                                             +"<div style=\"width: 18%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 15%;\" title=\"Name\" class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: -1%;\" title=\"Email\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 4%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                                 //alert("searchResgoogleDrive:::"+searchResgoogleDrive);
                     $("#searchResultsCont").append(searchOneDrive);
                 }
             }
         }
     
     
     //googleDriveUser		
     var jsongDrive = JSON.parse(goDriveCount);//goProjectDriveCount
     if(jsongDrive){
         
         var json = eval(jsongDrive);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                 count = count + 1;
                 searchResgoogleDrive = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">"+json[i].moduleName+"</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             +"<div style=\"width: 14.7%;\" title=\"Name\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 38%;\" title=\"Name\" class=\"tabContentHeaderName\">Title</div>"
                                             +"<div style=\"width: 18%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 15%;\" title=\"Name\" class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: -1%;\" title=\"Email\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 4%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                                 //alert("searchResgoogleDrive:::"+searchResgoogleDrive);
                     $("#searchResultsCont").append(searchResgoogleDrive);
                 }
             }
         }
     
     //googleDriveProject		
     var jsongDrive = JSON.parse(goProjectDriveCount);//goProjectDriveCount
     if(jsongDrive){
         
         var json = eval(jsongDrive);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                 count = count + 1;
                 searchProjectgoogleDrive = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">"+json[i].moduleName+"</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             +"<div style=\"width: 14.7%;\" title=\"Name\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 17%;\" title=\"Name\" class=\"tabContentHeaderName\">Workspace Name</div>"
                                             +"<div style=\"width: 21%;\" title=\"Name\" class=\"tabContentHeaderName\">Title</div>"
                                             +"<div style=\"width: 18%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 15%;\" title=\"Name\" class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: -1%;\" title=\"Email\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 4%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                                 //alert("searchResgoogleDrive:::"+searchResgoogleDrive);
                     $("#searchResultsCont").append(searchProjectgoogleDrive);
                 }
             }
         }
     
     //oneDriveProjet UI
     var jsongDrive = JSON.parse(oneDriveProjectCount);//oneDriveProject
     if(jsongDrive){
         
         var json = eval(jsongDrive);
         for(var i = 0;i < json.length;i++){
             if(json[i].count != 0){
                 count = count + 1;
                 searchProjectOneDrive = "<div id = \"mainSearchCont_"+count+"\" style=\"float: left; width: 100%;\">"
                                     +"<div id=\"mainSrchGridHeader_"+count+"\"  style = \"width: 100%;float: left;\" class=\"notificationgrid\" > "     
                                         +"<div style=\"margin-left:10px;float: left;width: 96%;\">"       
                                             +"<img  id=\"mainSrchGridPlus_"+count+"\" mName = \""+json[i].moduleName+"\" class = \"repGridClass\" src=\"images/notfcn-minus.png\" onclick=\"getSrchResOnClick('"+count+"',this);\" style=\"float:left;height:15px;width:15px;margin:15px 10px 0 0;\"/> "   
                                             +"<li style=\"list-style-type:none;float:left;margin-right:5px;font-weight:bold;font-size: 13px;margin:15px 10px 0 0;\">"+json[i].moduleName+"</li>" 
                                             +"<li style=\"list-style-type:none;float:left;font-weight:bold;margin: 15px 0 0 0\">"
                                                 +"<div style=\"float: left;\">"+'(&nbsp;'+"</div>"
                                                 +"<div style=\"float: left;color:#3399FF\">"+json[i].count+"</div>"
                                                 +"<div style=\"float: left;\">"+'&nbsp;)'+"</div> "
                                             +"</li>"       
                                         +"</div>"  
                                         
                                     +"</div>"
                                     +"<div id = \"mainSrchHeader_"+count+"\" class=\"tabContentHeader\" style=\"margin-left:3%;float: left;width: 97%;\">"       
                                         +"<div class=\"tabContentSubHeader\">"
                                             +"<div style=\"width: 14.7%;\" title=\"Name\" class=\"tabContentHeaderName\">Type</div>"
                                             +"<div style=\"width: 17%;\" title=\"Name\" class=\"tabContentHeaderName\">Workspace Name</div>"
                                             +"<div style=\"width: 21%;\" title=\"Name\" class=\"tabContentHeaderName\">Title</div>"
                                             +"<div style=\"width: 18%;\" title=\"Department\"   class=\"tabContentHeaderName\" >Path</div>"
                                             +"<div style=\"width: 15%;\" title=\"Name\" class=\"tabContentHeaderName\">Created By</div>"
                                             +"<div style=\"width: -1%;\" title=\"Email\"  class=\"tabContentHeaderName\">Created Timestamp</div>"
                                         +"</div>"
                                     +"</div>" 
                                     
                                     
                                 +"<div id = \"srchCont_"+count+"\" class = \"repClass\" style = \"margin-left: 4%;float: left;width: 97%\">"+"</div>"
                                 +"</div>"
                                 //alert("searchResgoogleDrive:::"+searchResgoogleDrive);
                     $("#searchResultsCont").append(searchProjectOneDrive);
                 }
             }
         }
      }
              
      $('div[id^=mainSearchCont_]:not(:first)').find('div[id^=srchCont_]').hide();
     $('div[id^=mainSearchCont_]:not(:first)').find('div[id^=mainSrchHeader_]').hide();
     $('div[id^=mainSearchCont_]:not(:first)').find('img[id^=mainSrchGridPlus_]').attr('src','images/notfcn-plus.png');
  }
  
  function openConfluenceSpace(obj){
      //alert("yes clicked");
      var mAct = $(obj).attr('id');
      window.open(mAct,"_blank");
  }
  
  
  function prepareSearchUI(result, count, module){
      //alert("inside prepareSearchUI:::"+result+" "+"count::::"+count+" "+"module::::"+module);
      /*Variables for repository module*/
      var moduleName = "";
      var searchRepoOwn = "";
      var searchSharedRepo = "";
      var searchOwnRepoDocs = "";
      var searchSharedDocs = "";
      var myZoneDocumentTaskComments = "";
      /*Variables for connect module*/
      var compContacts = "";
      var personalContacts = "";
      
      /*Variables for workspace module*/
      var ownFoldersWrkSpace = "";
     var sharedFoldersWrkSpace = "";
     var ownDocumentsWrkSpace = "";
     var workspaceTasksWrkSpace =  "";
     var teamDataWrkSpace = "";
     var activityFeedWrkSpace =  "";
     var docCommentsWrkSpace = "";
     var workspaceEmailWrkSpace = "";
     var sharedDocsWrkSpace = "";
     var projectsWrkSpace = "";
     var availProjects = "";
     var wrkspaceTaskComments="";
     var wrkspaceDocumentTaskComments = '';
     
     var assignedDocTasksWrkspaceCont = '';
     var eventTasksWrkspaceCont = '';
     var assignedEventTasksWrkspaceCont = '';
     var ideaTasksWorkspaceCont = '';
     var assignedIdeaTasksWorkspaceCont = '';
     var taskWrkspaceCont = '';
     var assignedTaskWrkspaceCont = '';
     var sprintTasksWorkspaceCont = '';
     var assignedSprintTaskWorkspaceCont = '';
     var workflowTasksWorkspaceCont = '';
     var assginedWorkflowTasksWorkspaceCont = '';
     
     
     /*Variables for idea module*/
     var myTopicsCont = '';
     var topicsSharedCont = '';
     var sharedIdeaTopicCont = '';
     var ideaProjectsCont = '';
     var ideasTopAndProCont = '';
     var ideasOnTopicShareCont = '';
     var ideaCommentsOwnCont = '';
     var ideaCommentsOnTopicShareCont = '';
     var ideaTasksOwnCont = '';
     var ideaTaskCommentsCont = '';
     var ideaTasksOnTopicShareCont = '';
     var ideaLinksOwnCont = '';
     var ideaLinksOnTopicShareCont = '';
     var ideaStoriesOwnCont = '';
     var ideaStoriesOnTopicShareCont = '';
     var sharedIdeasCont = '';
     var ownIdeaDocsCont = '';
     var docsOnTopicShareCont = '';
     
     /*Variables for agile module*/
     var agileProjectsCont = '';
     var agileEFSCont = '';
     var agileEFSCommentsCont = '';
     var agileEFSLinksCont = '';
     var agileEFSTasksCont = '';
     var agileTasksCommentCont = '';
     var agileSprintGroupsCont = '';
     var agileSprintCont = '';
     var agileSprintStagesCont = '';
             
     /*var ideasMyTopic = '';
     var projectIdeasCont = '';
     var myIdeasCont = '';
     var sharedIdeasCont = '';
     var myCommIdeasCont = '';
     var sharedCommIdeasCont = '';
     var myTaskIdeasCont = '';
     var sharedTaskIdeasCont = '';
     
     var myDocIdeasCont = '';
     var sharedDocIdeasCont = '';
     var myLinkIdeasCont = '';
     var sharedLinkIdeasCont = '';
     var myStoryIdeasCont = '';
     var sharedStoryIdeasCont = '';*/
     
     /*Variables for calendar module*/
     var myTaskCalCont = '';
     var assignedTaskCalCont = '';
     var myWrkspaceTaskCalCont = '';
     var assignedWrkspacetaskCalCont = '';
     var myWrkflowTaskCalCont = '';
     var assignedWrkflowtaskCalCont = '';
     var myIdeaTaskCalCont = '';
     var assignedIdeaTaskCalCont = '';
     var mySprintTaskCalCont = '';
     var assignedSprintTaskCont = '';
     var meraEventsCont = ''
     var assignedEventsCalCont = '';
     
     var myDocTaskCalCont = '';
     var assignedDocTaskCalCont = '';
     var calTaskCommentsCont = '';
     var calTaskDocumentsCont = '';
     var calTaskLevelDocumentsCont = '';
     
     /*Variables for blog */
     var myNotesCont = '';
     var myNotesContentCont = '';
     var sharedNotesCont = '';
     var sharedNotesContentCont = '';
     var myBlogCont = '';
     var myBlogContentCont = '';
     var myBlogCommentsCont = '';
     var sharedBlogCont = '';
     var sharedBlogContentCont = '';
     var sharedBlogCommentsCont = '';
     var myAlbumsCont = '';
     var myAlbumsCommentsCont = '';
     var sharedAlbumsCont = '';
     var sharedAlbumsCommentsCont = '';
     
     /*Variables for confluence */
     var myConfluenceSpace = '';
     var myConfluenceType = '';
     var myConfluenceTitle = '';
     var myConfluenceStatus = '';
     
     var messageCont = '';
             
             
      var conSearch = "";
      var wrkSearch = '';
      var prevCount = "";
      var prevWCount = "";
      var prevICount = "";
      var prevACount = '';
      var prevTCount = '';
      var prevMCount = '';
      
      var myGoogleDrive = '';		
      var projGoogleDrive = '';
      var myOneDrive='';
      
      $('#repDet').css('display','block');
     $('#srchCont_'+count).html('');
     
     
     if(module == 'Myzone | OneDrive' && result == "[]"){
         //alert("module1:::"+module);
             $('#srchCont_'+count).html('No Results found.');
             $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
         }else{
         if(module == 'Myzone | OneDrive'){
         //alert("module2:::"+module);
         //alert("result1::"+result);
             var jsonGoogleDriveRes = JSON.parse(result);
                 if(jsonGoogleDriveRes){
                     var jsonRes = eval(jsonGoogleDriveRes);
                     for(var i = 0;i < jsonRes.length;i++){
                         jsonRes[i].listType = replaceSpecialCharacters(jsonRes[i].listType);							
                         jsonRes[i].Title = replaceSpecialCharacters(jsonRes[i].Title);
                         var oneDriveDocId=jsonRes[i].oneDriveId;
                         myOneDrive = "<div onclick = \"fetchMyzoneDetailsData(this);\"   listType=\"myZoneOneDrive\"  oneDriveId = \""+oneDriveDocId+"\"   class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                     
                                     
                                     +"<div style=\"width: 13.7%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" " + "</div>"
                                     +"<div style=\"width: 54%;\" title=\""+jsonRes[i].Title+"\" class=\"searchDetails\">"+jsonRes[i].Title+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].ownerName+"\" class=\"searchDetails\">"+jsonRes[i].ownerName+" " + "</div>"
                                     +"<div style=\"width: 16%;\" title=\""+jsonRes[i].spiltCdate+"\" class=\"searchDetails\">"+jsonRes[i].spiltCtime+","+jsonRes[i].spiltCdate+" "+ "</div>"
                                     //+"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].msgType+"\" class=\"searchDetails\">"++" "+ "</div>"
                                     //+"<div style=\"width: 14%;\" title=\""+jsonRes[i].CreatedBy+"\" class=\"searchDetails\">"+jsonRes[i].CreatedBy+" " + "</div>"
                                     //+"<div style=\"width: 40%;\" title=\""+jsonRes[i].msg+"\" class=\"searchDetails\">"+jsonRes[i].msg+" " + "</div>"
                                     //+"<div style=\"width: 15%;\" title=\""+jsonRes[i].gname+"\" class=\"searchDetails readAccess\">"+jsonRes[i].gname+" " + "</div>"
                                     //+"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                             $('#srchCont_'+count).append(myOneDrive);
                         
                     }
                 }
             }
         }
     
     
     
     if(module == 'Teamzone | GoogleDrive' && result == "[]"){
         //alert("module1:::"+module);
             $('#srchCont_'+count).html('No Results found.');
             $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
         }else{
         if(module == 'Teamzone | GoogleDrive'){
         //alert("module2:::"+module);
         //alert("result1::"+result);
             var jsonGoogleDriveRes = JSON.parse(result);
                 if(jsonGoogleDriveRes){
                     var jsonRes = eval(jsonGoogleDriveRes);
                     for(var i = 0;i < jsonRes.length;i++){
                         jsonRes[i].type = replaceSpecialCharacters(jsonRes[i].listType);							
                         jsonRes[i].title = replaceSpecialCharacters(jsonRes[i].Title);
                         jsonRes[i].spiltCtime = replaceSpecialCharacters(jsonRes[i].spiltCtime);
                         jsonRes[i].gdProjdocid = replaceSpecialCharacters(jsonRes[i].gdProjdocid);
                         var gdProjmsgId=jsonRes[i].gdProjdocid;
                         //alert("inside JS gdProjmsgId::::"+gdProjmsgId);
                         var proJId =jsonRes[i].projId;
                         var projName=jsonRes[i].projectName;
                         var projStatus=jsonRes[i].projectStatus;
                         var imagetype=jsonRes[i].imageType;
                         var archiveStatus=jsonRes[i].project_status;
                         var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                         //alert("gdProjmsgId::::"+gdProjmsgId);
                         //jsonRes[i].status = replaceSpecialCharacters(jsonRes[i].status); 
                         
                         //alert("emailMsgId::"+emailMsgId);
                         // "<div  onclick = \"openWrkSpaceSearchDetails(this);\"  id=\""+jsonRes[i].folder_id+"\"  mAct = \"Document\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].projectArchStatus+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                         projGoogleDrive = "<div onclick = \"openWrkSpaceSearchDetails(this);\"  gdProjdoc = \""+jsonRes[i].gdProjdocid+"\" projectId = \""+proJId+"\" projectName = \""+projName+"\" projectUserStatus = \""+projStatus+"\" projectImgType = \""+imagetype+"\" projectArchiveStat = \""+archiveStatus+"\" mAct = \"gdProjDocuments\" class=\"notificationgrid  reprtsContCls\"  style = \"margin: 3px 0 0 0;width: 100%\">"
                                     
                                     +"<div style=\"width: 9.7%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" " + "</div>"
                                     
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 13.7%;\" title=\""+jsonRes[i].projectName+"\" class=\"searchDetails\">"+jsonRes[i].projectName+" " + "</div>"
                                     +"<div style=\"width: 38%;\" title=\""+jsonRes[i].Title+"\" class=\"searchDetails\">"+jsonRes[i].Title+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].ownerName+"\" class=\"searchDetails\">"+jsonRes[i].ownerName+" " + "</div>"
                                     +"<div style=\"width: 16%;\" title=\""+jsonRes[i].spiltCdate+"\" class=\"searchDetails\">"+jsonRes[i].spiltCtime+","+jsonRes[i].spiltCdate+" "+ "</div>"
                                     //+"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].msgType+"\" class=\"searchDetails\">"++" "+ "</div>"
                                     //+"<div style=\"width: 14%;\" title=\""+jsonRes[i].CreatedBy+"\" class=\"searchDetails\">"+jsonRes[i].CreatedBy+" " + "</div>"
                                     //+"<div style=\"width: 40%;\" title=\""+jsonRes[i].msg+"\" class=\"searchDetails\">"+jsonRes[i].msg+" " + "</div>"
                                     //+"<div style=\"width: 15%;\" title=\""+jsonRes[i].gname+"\" class=\"searchDetails readAccess\">"+jsonRes[i].gname+" " + "</div>"
                                     //+"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                             $('#srchCont_'+count).append(projGoogleDrive);
                         
                     }
                 }
             }
         }
     
     if(module == 'Myzone | GoogleDrive' && result == "[]"){
         //alert("module1:::"+module);
             $('#srchCont_'+count).html('No Results found.');
             $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
         }else{
         if(module == 'Myzone | GoogleDrive'){
         //alert("module2:::"+module);
         //alert("result1::"+result);
             var jsonGoogleDriveRes = JSON.parse(result);
                 if(jsonGoogleDriveRes){
                     var jsonRes = eval(jsonGoogleDriveRes);
                     for(var i = 0;i < jsonRes.length;i++){
                         jsonRes[i].type = replaceSpecialCharacters(jsonRes[i].listType);							
                         jsonRes[i].title = replaceSpecialCharacters(jsonRes[i].Title);
                         jsonRes[i].spiltCtime = replaceSpecialCharacters(jsonRes[i].spiltCtime);
                         jsonRes[i].gUserdocid = replaceSpecialCharacters(jsonRes[i].gUserdocid);
                         var gmsgId=jsonRes[i].gUserdocid;
                         //jsonRes[i].status = replaceSpecialCharacters(jsonRes[i].status); 
                         
                         //alert("emailMsgId::"+emailMsgId);
                         //"<div onclick=\"fetchMyzoneDetailsData(this)\" mainId=\""+jsonRes[i].cid+"\" childId=\""+msgid2+"\"  listType=\"chat\" msgType = \""+jsonRes[i].msgType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                         myGoogleDrive = "<div onclick = \"fetchMyzoneDetailsData(this);\"  gId = \""+gmsgId+"\" listType=\"myZoneGdrive\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                     
                                     +"<div style=\"width: 13.7%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" " + "</div>"
                                     +"<div style=\"width: 54%;\" title=\""+jsonRes[i].Title+"\" class=\"searchDetails\">"+jsonRes[i].Title+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].ownerName+"\" class=\"searchDetails\">"+jsonRes[i].ownerName+" " + "</div>"
                                     +"<div style=\"width: 16%;\" title=\""+jsonRes[i].spiltCdate+"\" class=\"searchDetails\">"+jsonRes[i].spiltCtime+","+jsonRes[i].spiltCdate+" "+ "</div>"
                                     //+"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].msgType+"\" class=\"searchDetails\">"++" "+ "</div>"
                                     //+"<div style=\"width: 14%;\" title=\""+jsonRes[i].CreatedBy+"\" class=\"searchDetails\">"+jsonRes[i].CreatedBy+" " + "</div>"
                                     //+"<div style=\"width: 40%;\" title=\""+jsonRes[i].msg+"\" class=\"searchDetails\">"+jsonRes[i].msg+" " + "</div>"
                                     //+"<div style=\"width: 15%;\" title=\""+jsonRes[i].gname+"\" class=\"searchDetails readAccess\">"+jsonRes[i].gname+" " + "</div>"
                                     //+"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                             $('#srchCont_'+count).append(myGoogleDrive);
                         
                     }
                 }
             }
         }
     
     if(module == 'Confluence' && result == "[]"){
         //alert("module1:::"+module);
             $('#srchCont_'+count).html('No Results found.');
             $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
         }else{
         if(module == 'Confluence'){
         //alert("module2:::"+module);
         //alert("result1::"+result);
             var jsonConfluenceRes = JSON.parse(result);
                 if(jsonConfluenceRes){
                     var jsonRes = eval(jsonConfluenceRes);
                     for(var i = 0;i < jsonRes.length;i++){
                         jsonRes[i].type = replaceSpecialCharacters(jsonRes[i].type);							
                         jsonRes[i].title = replaceSpecialCharacters(jsonRes[i].title);
                         jsonRes[i].status = replaceSpecialCharacters(jsonRes[i].status); 
                         
                         //alert("emailMsgId::"+emailMsgId);
                         myConfluenceSpace = "<div onclick = \"openConfluenceSpace(this);\"  mAct = \""+jsonRes[i].tinyUrl+"\"  id = \""+jsonRes[i].tinyUrl+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                     //+"<div style=\"width: 9%;margin-left: 1%;\" title=\"Confluence\" class=\"searchDetails\">Confluence</div>"
                                     +"<div style=\"width: 14.7%;\" title=\""+jsonRes[i].type+"\" class=\"searchDetails\">"+jsonRes[i].type+" " + "</div>"
                                     +"<div style=\"width: 63%;\" title=\""+jsonRes[i].title+"\" class=\"searchDetails\">"+jsonRes[i].title+" " + "</div>"
                                     +"<div style=\"width: 20%;\" title=\""+jsonRes[i].status+"\" class=\"searchDetails readAccess\">"+jsonRes[i].status+" " + "</div>"
                                     +"</div>"
                             $('#srchCont_'+count).append(myConfluenceSpace);
                         
                     }
                 }
             }
         }
     
     if(module == 'Video'){
     
         //alert("module2:::"+module);
         //alert("result1::"+result);
                         var Urllink = "https://colabus.kpoint.com/app/search?qtext="+result+"";
                         var myVideoSpace = "<div onclick = \"openConfluenceSpace(this);\"  mAct = \""+Urllink+"\"  id = \""+Urllink+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                     +"<div style=\"width: 9%;margin-left: 1%;display:none;\" title=\"Media\" class=\"searchDetails\">Media File</div>"
                                     +"<div style=\"width: 14.7%;padding-left:10px;\" class=\"searchDetails\">Media</div>"
                                     +"<div id = \"linkUrl\" style=\"width: 63%;\" title=\""+Urllink+"\" class=\"searchDetails\">"+Urllink+ "</div>"
                                     +"<div style=\"width: 20%;\" class=\"searchDetails readAccess\"></div>"
                                     +"</div>"
                             $('#srchCont_'+count).html(myVideoSpace);
                         
                     
                 
             }
     
     if(module == 'Email' && result == "[]"){
     //alert("module1:::"+module);
         $('#srchCont_'+count).html('No Results found.');
         $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
     }else{
     if(module == 'Email'){
     //alert("module2:::"+module);
     //alert("result1::"+result);
         var jsonWorkspaceEmail = JSON.parse(result);
             if(jsonWorkspaceEmail){
                 var jsonRes = eval(jsonWorkspaceEmail);
                 for(var i = 0;i < jsonRes.length;i++){
                     jsonRes[i].email_subject = replaceSpecialCharacters(jsonRes[i].email_subject);
                     
                     jsonRes[i].email_message_id = replaceSpecialCharacters(jsonRes[i].email_message_id);
                     var emailMsgId = jsonRes[i].email_message_id ;
                     //alert("emailMsgId::"+emailMsgId);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     workspaceEmailWrkSpace = "<div onclick = \"openWrkSpaceSearchDetails(this);\" mAct = \"Email\"  id = \""+jsonRes[i].email_id+"\"  emailMsgId = \""+jsonRes[i].email_message_id+"\"  projectUserStatus=\"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 9%;margin-left: 1%;\" title=\"Email\" class=\"searchDetails\">Email</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 36%;\" title=\""+jsonRes[i].email_subject+"\" class=\"searchDetails\">"+jsonRes[i].email_subject+" " + "</div>"
                                 //+"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].email_from_id+"\" class=\"searchDetails readAccess\">"+jsonRes[i].email_from_id+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].date+"\" class=\"searchDetails\">"+jsonRes[i].date+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(workspaceEmailWrkSpace);
                     
                 }
             }
         }
     }
     
     if(module == 'Repository' && result == "[]*-*[]*-*[]*-*[]"){
          $('#srchCont_'+count).html('No Results found.');
         $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
     }else{
         if(module == 'Repository'){
             var ownFolders = result.split('*-*')[0];
             var sharedFolders = result.split('*-*')[1];
             var ownDocs = result.split('*-*')[2];
             var documentTaskComments = result.split('*-*')[3];
             //var sharedDocs = result.split('*-*')[3];
             var jsonOwnFolders = JSON.parse(ownFolders);
             if(jsonOwnFolders){
                 var jsonRes = eval(jsonOwnFolders);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Repository'){
                     
                     searchRepoOwn = "<div onclick = \"detailViewNew(this);\"  mType = \""+jsonRes[i].mType+"\" lType = \"ownFolders\" folderId = \""+jsonRes[i].folder_id+"\" folderName=\""+jsonRes[i].folder_name+"\" parent_folder_id = \""+jsonRes[i].parent_folder_id+"\"  folder_path = \""+jsonRes[i].parent_folder_id+"\" share_user_type = \"W\" fType = \"parshared_to\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 14%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 jsonRes[i].folder_name = replaceSpecialCharacters(jsonRes[i].folder_name);
                                 jsonRes[i].parent_folder_name = replaceSpecialCharacters(jsonRes[i].parent_folder_name);
                                 searchRepoOwn += "<div style=\"width: 33%;\" title=\""+jsonRes[i].folder_name+"\" class=\"searchDetails\">"+jsonRes[i].folder_name+"</div>"
                                 +"<div style=\"width: 19%;\" title=\""+jsonRes[i].parent_folder_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_folder_name+"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 13.5%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                             +"</div>"
                             $('#srchCont_'+count).append(searchRepoOwn);
                     }
                 }
             }
             var jsonSharedFolders = JSON.parse(sharedFolders);
             if(jsonSharedFolders){
                 var jsonRes = eval(jsonSharedFolders);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Repository'){
                     searchSharedRepo = "<div onclick = \"detailViewNew(this);\"  mType = \""+jsonRes[i].mType+"\"  lType = \"sharedFolders\" folderId = \""+jsonRes[i].folder_id+"\" folderName=\""+jsonRes[i].folder_name+"\" parent_folder_id = \""+jsonRes[i].parent_folder_id+"\" folder_path = \""+jsonRes[i].folder_path+"\" share_user_type = \""+jsonRes[i].shareType+"\" fType = \"subshared_to\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 14%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 if(jsonRes[i].listType == 'Document'){
                                     jsonRes[i].document_name =  replaceSpecialCharacters(jsonRes[i].document_name);
                                 }else{
                                     jsonRes[i].folder_name = replaceSpecialCharacters(jsonRes[i].folder_name);
                                 }
                                 jsonRes[i].parent_folder_name = replaceSpecialCharacters(jsonRes[i].parent_folder_name);
                                 if(jsonRes[i].listType == 'Document'){
                                     searchSharedRepo += "<div style=\"width: 23%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+"</div>"
                                 }else{
                                     searchSharedRepo += "<div style=\"width: 23%;\" title=\""+jsonRes[i].folder_name+"\" class=\"searchDetails\">"+jsonRes[i].folder_name+"</div>"
                                 }
                                 searchSharedRepo += "<div style=\"width: 19%;\" title=\""+jsonRes[i].parent_folder_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_folder_name+"</div>"
                                 +"<div style=\"width: 20.5%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 18%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 /*if(jsonRes[i].listType == 'Document'){
                                     if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                         searchSharedRepo += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                             +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                         +"</div>"
                                     }else{
                                         searchSharedRepo += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                     }
                                 }*/
                             +"</div>"
                             $('#srchCont_'+count).append(searchSharedRepo);
                     }
                 }
             } 
             var jsonOwnDocs = JSON.parse(ownDocs);
             if(jsonOwnDocs){
                 var jsonRes = eval(jsonOwnDocs);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Repository'){
                     searchOwnRepoDocs = "<div onclick = \"detailViewNew(this);\"  mType = \""+jsonRes[i].mType+"\"  lType = \"ownDocs\" documentId = \""+jsonRes[i].document_id+"\" documentName = \""+jsonRes[i].document_name+"\" folderPath = \""+jsonRes[i].folder_path+"\" folderId = \""+jsonRes[i].folder_id+"\" folderName=\""+jsonRes[i].folder_name+"\" folder_path = \""+jsonRes[i].folder_path+"\" parent_folder_id = \""+jsonRes[i].parent_folder_id+"\" share_user_type = \""+jsonRes[i].shareType+"\" fType = \"docshare\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 14%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                                 jsonRes[i].parent_folder_name = replaceSpecialCharacters(jsonRes[i].parent_folder_name);
                                 searchOwnRepoDocs += "<div style=\"width: 22.7%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+"</div>"
                                 +"<div style=\"width: 19%;\" title=\""+jsonRes[i].parent_folder_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_folder_name+"</div>"
                                 +"<div style=\"width: 20.7%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 /*if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                     searchOwnRepoDocs += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                         +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                     +"</div>"
                                 }else{
                                     searchOwnRepoDocs += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                 }*/
                             searchOwnRepoDocs += "</div>"
                             $('#srchCont_'+count).append(searchOwnRepoDocs);
                     }
                 }
             } 
             
             var jsonDocumentTaskComments = JSON.parse(documentTaskComments);
             if(jsonDocumentTaskComments){
                 var jsonRes = eval(jsonDocumentTaskComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Repository'){
                     
                         myZoneDocumentTaskComments = "<div onclick = \"loadDetailNotData('calendar',"+jsonRes[i].task_id+",'','Document',"+jsonRes[i].task_id+",'',"+jsonRes[i].task_id+",'','','',"+jsonRes[i].source_id+",'0','0','','',"+jsonRes[i].user_id+",'workspace_Task','');\"  projectId = \""+jsonRes[i].project_id+"\" projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" proj_user_status = \""+jsonRes[i].proj_user_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                     +"<div style=\"width: 14%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 22.7%;\" title=\""+jsonRes[i].task_comment+"\" class=\"searchDetails\">"+jsonRes[i].task_comment+" " + "</div>"
                                     +"<div style=\"width: 19%;\" title=\"\" class=\"searchDetails\"></div>"
                                     //+"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                     +"<div style=\"width: 20.7%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                             $('#srchCont_'+count).append(myZoneDocumentTaskComments);
                         }
                     }
             }
             
             /*var jsonSharedDocs = JSON.parse(sharedDocs);
             if(jsonSharedDocs){
                 var jsonRes = eval(jsonSharedDocs);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Repository'){
                     searchSharedDocs = "<div onclick = \"detailViewNew(this);\"   mType = \""+jsonRes[i].mType+"\"  lType = \"sharedDocs\" documentId = \""+jsonRes[i].document_id+"\" documentName = \""+jsonRes[i].document_name+"\" folderPath = \""+jsonRes[i].folder_path+"\" folderId = \""+jsonRes[i].folder_id+"\" folderName=\""+jsonRes[i].folder_name+"\" folder_path = \""+jsonRes[i].folder_path+"\" parent_folder_id = \""+jsonRes[i].parent_folder_id+"\" share_user_type = \""+jsonRes[i].share_type_user+"\" fType = \"docshare\"  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                                 jsonRes[i].parent_folder_name = replaceSpecialCharacters(jsonRes[i].parent_folder_name);
                     
                                 searchSharedDocs += "<div style=\"width: 20%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+"</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].parent_folder_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_folder_name+"</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                             +"</div>"
                             $('#srchCont_'+count).append(searchSharedDocs);
                     }
                 }
             }*/
             
         }
     }
     
     if(module == 'Connect' && result == "[]"){
          $('#srchCont_'+count).html('No Results found.');
         $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
     }else{
         if(module == 'Connect'){
             if(prevCount != count){
                 prevCount = count;
             }
             var compContacts = result;
             //var personalContacts = result.split('*_*')[1];
             var jsonCompContacts = JSON.parse(compContacts);
             if(jsonCompContacts){
                 var jsonRes = eval(jsonCompContacts);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Connect'){
                         compContacts = "<div onclick = \"loadContacts(this);\"  utype = \""+jsonRes[i].listType+"\" uId = \""+jsonRes[i].id+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                     +"<div style=\"width: 22%;margin-left:2%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails\">"+jsonRes[i].fullName+"</div>"
                                     +"<div style=\"width: 20.6%;\" title=\""+jsonRes[i].department+"\" class=\"searchDetails\">"+jsonRes[i].department+"</div>"
                                     if(jsonRes[i].phone == 'undefined'){
                                         compContacts += "<div style=\"width: 18%;\" title=\"\" class=\"searchDetails readAccess\"></div>"
                                     }else{
                                         compContacts += "<div style=\"width: 18%;\" title=\""+jsonRes[i].phone+"\" class=\"searchDetails readAccess\">"+jsonRes[i].phone+"</div>"
                                     }
                                     compContacts += "<div style=\"width: 14%;\" title=\""+jsonRes[i].workPhone+"\" class=\"searchDetails\">"+jsonRes[i].workPhone+"</div>"
                                     +"<div style=\"width: 20%;\" title=\""+jsonRes[i].email+"\" class=\"searchDetails\">"+jsonRes[i].email+" " + "</div>"
                                     +"</div>"
                                     $('#srchCont_'+count).append(compContacts);
                     }
                 }
             }
             
             /*var jsonPersonalContacts = JSON.parse(personalContacts);
             if(jsonPersonalContacts){
                 var jsonRes = eval(jsonPersonalContacts);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Connect'){
                         personalContacts = "<div onclick = \"loadContacts(this);\"  utype = \""+jsonRes[i].listType+"\" uId = \""+jsonRes[i].id+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails\">"+jsonRes[i].fullName+"</div>"
                                     +"<div style=\"width: 23%;\" title=\""+jsonRes[i].department+"\" class=\"searchDetails\">"+jsonRes[i].department+"</div>"
                                     if(jsonRes[i].phone == 'undefined'){
                                         personalContacts += "<div style=\"width: 25%;\" title=\"\" class=\"searchDetails readAccess\"></div>"
                                     }else{
                                         personalContacts += "<div style=\"width: 25%;\" title=\""+jsonRes[i].phone+"\" class=\"searchDetails readAccess\">"+jsonRes[i].phone+"</div>"
                                     }
                                     personalContacts += "<div style=\"width: 15%;\" title=\""+jsonRes[i].workPhone+"\" class=\"searchDetails\">"+jsonRes[i].workPhone+"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].email+"\" class=\"searchDetails\">"+jsonRes[i].email+" " + "</div>"
                                     +"</div>"
                                     $('#srchCont_'+count).append(personalContacts);
                     }
                 }
             }*/
         }
     }
     if(module == 'Workspace' && result == "[]*|*[]*|*[]*|*[]*|*[]*|*[]*|*[]*|*[]*|*[]*|*[]"){
          $('#srchCont_'+count).html('No Results found.');
         $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
     }else{
     
         if(module == 'Workspace'){
         
             if(prevWCount != count){
                 prevWCount = count;
             }
             //console.log("result--"+result);
             var ownFolders = result.split('*|*')[0];
             var sharedFolders =  result.split('*|*')[1];
             var ownDocuments = result.split('*|*')[2];
             var workspaceTasks =  result.split('*|*')[3];
             var assignedDocTasksWrkspace =  result.split('*|*')[4];
             var teamData = result.split('*|*')[5];
             var activityFeed =  result.split('*|*')[6];
             //alert("activityFeed>>>>"+activityFeed);
             var docComments =  result.split('*|*')[7];
             //var workspaceEmail = result.split('*|*')[8];
             var projects = result.split('*|*')[8];
             var taskComments=result.split('*|*')[9];
             var workspaceTaskComments=result.split('*|*')[10];
             var workspaceDocumentTaskComments=result.split('*|*')[11];
             
             /*var wrkSpaceShareDocs = result.split('*|*')[8];
             var availProjects = result.split('*|*')[10];
             var assignedDocTasksWrkspace =  result.split('*|*')[11];
             var eventTasksWrkspace =  result.split('*|*')[12];
             var assignedEventTasksWrkspace =  result.split('*|*')[13];
             var ideaTasksWorkspace =  result.split('*|*')[14];
             var assignedIdeaTasksWorkspace =  result.split('*|*')[15];
             var taskWrkspace =  result.split('*|*')[16];
             var assignedTaskWrkspace =  result.split('*|*')[17];
             var sprintTasksWorkspace =  result.split('*|*')[18];
             var assignedSprintTaskWorkspace =  result.split('*|*')[19];
             var workflowTasksWorkspace =  result.split('*|*')[20];
             var assginedWorkflowTasksWorkspace =  result.split('*|*')[21];*/
             
             var jsonOwnFolders = JSON.parse(ownFolders);
             if(jsonOwnFolders){
                 var jsonRes = eval(jsonOwnFolders);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].folder_name = replaceSpecialCharacters(jsonRes[i].folder_name);
                     jsonRes[i].parent_folder_name = replaceSpecialCharacters(jsonRes[i].parent_folder_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                       
                       if((typeof jsonRes[i].document_name != "undefined") &&(jsonRes[i].document_name.indexOf("mov") > -1 || jsonRes[i].document_name.indexOf("MOV") > -1)){
                           var ext = jsonRes[i].document_name.split(".")[1];
                           ownFoldersWrkSpace = "<div   onclick = \"openVideo("+jsonRes[i].document_id+",'"+ext+"',"+jsonRes[i].project_id+");\"  id=\""+jsonRes[i].folder_id+"\"  mAct = \"Document\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].projectArchStatus+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">";
                     }else{
                         ownFoldersWrkSpace += "<div   onclick = \"openWrkSpaceSearchDetails(this);\"  id=\""+jsonRes[i].folder_id+"\"  mAct = \"Document\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].projectArchStatus+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">";
                     }
                         ownFoldersWrkSpace += "<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].folder_name+"\" class=\"searchDetails\">"+jsonRes[i].folder_name+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].parent_folder_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_folder_name+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(ownFoldersWrkSpace);
                     }
                 }
             }
             
             var jsonSharedFolders = JSON.parse(sharedFolders);
             if(jsonSharedFolders){
                 var jsonRes = eval(jsonSharedFolders);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == ('Workspace')){
                     if(jsonRes[i].listType == "Document"){
                         jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                     }else{
                         jsonRes[i].folder_name = replaceSpecialCharacters(jsonRes[i].folder_name);
                     }
                     jsonRes[i].parent_folder_name = replaceSpecialCharacters(jsonRes[i].parent_folder_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                      //sharedFoldersWrkSpace = "<div onclick = \"openWrkSearchDetails(this);\" folderId = \""+jsonRes[i].folder_id+"\" folderName=\""+jsonRes[i].folder_name+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" parent_folder_id = \""+jsonRes[i].parent_folder_id+"\" proj_user_status = \""+jsonRes[i].proj_user_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 
                                   if(jsonRes[i].listType == "Document"){
                                     if((typeof jsonRes[i].document_name != "undefined") &&(jsonRes[i].document_name.indexOf("mov") > -1 || jsonRes[i].document_name.indexOf("MOV") > -1)){	  										
                                         var ext = jsonRes[i].document_name.split(".")[1];
                                         sharedFoldersWrkSpace = "<div  onclick = \"openVideo("+jsonRes[i].document_id+",'"+ext+"',"+jsonRes[i].project_id+");\"  id=\""+jsonRes[i].folder_id+"\"  mAct = \"Document\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].projectArchStatus+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                       }else{
                                           sharedFoldersWrkSpace = "<div  onclick = \"openWrkSpaceSearchDetails(this);\"  id=\""+jsonRes[i].folder_id+"\"  mAct = \"Document\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].projectArchStatus+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                       }
                                 }else{
                                     sharedFoldersWrkSpace = "<div  onclick = \"openWrkSpaceSearchDetails(this);\"  id=\""+jsonRes[i].folder_id+"\"  mAct = \"Document\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].projectArchStatus+"\"class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 }
                                 sharedFoldersWrkSpace += "<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 
                                 if(jsonRes[i].listType == "Document"){
                                     sharedFoldersWrkSpace += "<div style=\"width: 21.5%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+" " + "</div>"
                                 }else{
                                     sharedFoldersWrkSpace += "<div style=\"width: 21.5%;\" title=\""+jsonRes[i].folder_name+"\" class=\"searchDetails\">"+jsonRes[i].folder_name+" " + "</div>"
                                 }
                                 sharedFoldersWrkSpace += "<div style=\"width: 15%;\" title=\""+jsonRes[i].parent_folder_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_folder_name+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 /*if(jsonRes[i].listType == 'Document'){
                                     if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                         sharedFoldersWrkSpace += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                             +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                         +"</div>"
                                     }else{
                                         sharedFoldersWrkSpace += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                     }
                                 }*/
                                 +"</div>"
                         $('#srchCont_'+count).append(sharedFoldersWrkSpace);
                     }
                 }
             }
             var jsonOwnDocuments = JSON.parse(ownDocuments);
             if(jsonOwnDocuments){
                 var jsonRes = eval(jsonOwnDocuments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                     jsonRes[i].parent_folder_name = replaceSpecialCharacters(jsonRes[i].parent_folder_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                       
                       if((typeof jsonRes[i].document_name != "undefined") &&(jsonRes[i].document_name.indexOf("mov") > -1 || jsonRes[i].document_name.indexOf("MOV") > -1)){
                           var ext = jsonRes[i].document_name.split(".")[1];
                           ownDocumentsWrkSpace = "<div  onclick = \"openVideo("+jsonRes[i].document_id+",'"+ext+"',"+jsonRes[i].project_id+");\" id=\""+jsonRes[i].folder_id+"\"  mAct = \"Document\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].projectArchStatus+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                       +"<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                     +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].parent_folder_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_folder_name+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     /*if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                         ownDocumentsWrkSpace += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                             +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                             +"</div>"
                                     }else{
                                         ownDocumentsWrkSpace += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                     }*/
                                     +"</div>"
                       }else{
                         ownDocumentsWrkSpace = "<div  onclick = \"openWrkSpaceSearchDetails(this);\"  id=\""+jsonRes[i].folder_id+"\"  mAct = \"Document\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].projectArchStatus+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                     +"<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                     +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].parent_folder_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_folder_name+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     /*if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                         ownDocumentsWrkSpace += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                             +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                             +"</div>"
                                     }else{
                                         ownDocumentsWrkSpace += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                     }*/
                                     +"</div>"
                     }
                         $('#srchCont_'+count).append(ownDocumentsWrkSpace);
                     }
                 }
             }
               
             var jsonWorkspaceTasks = JSON.parse(workspaceTasks);
             if(jsonWorkspaceTasks){
                 var jsonRes = eval(jsonWorkspaceTasks);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].task_name = replaceSpecialCharacters(jsonRes[i].task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     workspaceTasksWrkSpace = "<div onclick = \"openWrkSpaceSearchDetails(this);\" sublistType=\""+jsonRes[i].sublistType+"\"  taskSourceId=\""+jsonRes[i].source_id+"\"  typeOfTask = \"workspaceDocumentTask\" id = \""+jsonRes[i].task_id+"\" mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectUserStatus=\""+jsonRes[i].proj_user_status+"\"  projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 
                                 if(jsonRes[i].source_id == "0" && jsonRes[i].task_type == "Tasks"){
                                   workspaceTasksWrkSpace+="<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"

                                 }else{
                                     workspaceTasksWrkSpace+="<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].sublistType+"\" class=\"searchDetails\">"+jsonRes[i].sublistType+" "+ "</div>"
                                    
                                 }
                                 workspaceTasksWrkSpace+="<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                 
                                 
                                 +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].task_name+"\" class=\"searchDetails\">"+jsonRes[i].task_name+" " + "</div>"
                                 +"<div style=\"width: 15%;\"  class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(workspaceTasksWrkSpace);
                     }
                 }
             }
             
             var jsonAssignedDocTasksWrkspace = JSON.parse(assignedDocTasksWrkspace);
             if(jsonAssignedDocTasksWrkspace){
                 var jsonRes = eval(jsonAssignedDocTasksWrkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].doc_task_name = replaceSpecialCharacters(jsonRes[i].task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                                if(jsonRes[i].source_id == "0" && jsonRes[i].task_type == "Tasks"){
                                   assignedDocTasksWrkspaceCont = "<div onclick = \"openWrkSpaceSearchDetails(this);\" sublistType=\""+jsonRes[i].sublistType+"\" taskSourceId=\""+jsonRes[i].source_id+"\" typeOfTask = \"workspaceDocumentTask\" id = \""+jsonRes[i].task_id+"\" mAct = \"Task\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                   assignedDocTasksWrkspaceCont+="<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"

                                 }else{
                                     assignedDocTasksWrkspaceCont = "<div onclick = \"openWrkSpaceSearchDetails(this);\" sublistType=\""+jsonRes[i].sublistType+"\" taskSourceId=\""+jsonRes[i].source_id+"\" typeOfTask = \"workspaceDocumentTask\" id = \""+jsonRes[i].task_id+"\" mAct = \"Task\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                     assignedDocTasksWrkspaceCont+="<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].sublistType+"\" class=\"searchDetails\">"+jsonRes[i].sublistType+" "+ "</div>"
                                    
                                 }
                                  assignedDocTasksWrkspaceCont+="<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].task_name+"\" class=\"searchDetails\">"+jsonRes[i].task_name+" " + "</div>"
                                 +"<div style=\"width: 15%;\"  class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(assignedDocTasksWrkspaceCont);
                     }
                 }
             }
             
             var jsonTeamData = JSON.parse(teamData);
             if(jsonTeamData){
                 var jsonRes = eval(jsonTeamData);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                       jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     
                     teamDataWrkSpace = "<div onclick = \"openWrkSpaceSearchDetails(this);\"  id = \""+jsonRes[i].user_id+"\" mAct = \"Team\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\"\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\"\" class=\"searchDetails\"></div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(teamDataWrkSpace);
                     }
                 }
             }
             var jsonActivityFeed = JSON.parse(activityFeed);				
             if(jsonActivityFeed){
                 var jsonRes = eval(jsonActivityFeed);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].activityfeed = replaceSpecialCharacters(jsonRes[i].activityfeed);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     //jsonRes[i].activityfeed = jsonRes[i].activityfeed.replaceAll("CH(26)", ":").replaceAll("CHR(39)", "\'");
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     activityFeedWrkSpace = "<div onclick = \"openActivitySearchDetails(this)\" projectId = \""+jsonRes[i].project_id+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\" actFeedId = \""+jsonRes[i].activityfeed_id+"\" projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].activityfeed+"\" class=\"searchDetails\">"+jsonRes[i].activityfeed+" " + "</div>"
                                 +"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(activityFeedWrkSpace);
                     }
                 }
             }
             var jsonDocComments = JSON.parse(docComments);
             if(jsonDocComments){
                 var jsonRes = eval(jsonDocComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].doc_comment = replaceSpecialCharacters(jsonRes[i].doc_comment);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     docCommentsWrkSpace = "<div  onclick = \"openWrkSpaceSearchDetails(this);\"   mAct = \"Document\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].projectArchStatus+"\"  class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].doc_comment+"\" class=\"searchDetails\">"+jsonRes[i].doc_comment+" " + "</div>"
                                 +"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(docCommentsWrkSpace);
                     }
                 }
             }
             /*var jsonWorkspaceEmail = JSON.parse(workspaceEmail);
             if(jsonWorkspaceEmail){
                 var jsonRes = eval(jsonWorkspaceEmail);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].email_subject = replaceSpecialCharacters(jsonRes[i].email_subject);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     workspaceEmailWrkSpace = "<div onclick = \"openWrkSpaceSearchDetails(this);\" mAct = \"Email\"  id = \""+jsonRes[i].email_id+"\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 9%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].email_subject+"\" class=\"searchDetails\">"+jsonRes[i].email_subject+" " + "</div>"
                                 +"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(workspaceEmailWrkSpace);
                     }
                 }
             }*/
             
             var jsonProjects = JSON.parse(projects);
             if(jsonProjects){
                 var jsonRes = eval(jsonProjects);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     projectsWrkSpace = "<div onclick = \"openProjectDetails(this, "+jsonRes[i].project_id+");\" mType = \"projects\"  projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" proj_user_status = \""+jsonRes[i].proj_user_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(projectsWrkSpace);
                     }
                 }
             }
             
             var jsonTaskComments = JSON.parse(taskComments);
             
             if(jsonTaskComments){
                 var jsonRes = eval(jsonTaskComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                     var projectType="";
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                       if(jsonRes[i].proj_user_status == 'PO'){
                           projectType="MyProjects";
                       }else{
                           projectType="SharedProjects";
                       }
                       
                       wrkspaceTaskComments = "<div onclick = \"loadDetailNotData('calendar',"+jsonRes[i].project_id+",'','Tasks',"+jsonRes[i].task_id+",'',"+jsonRes[i].task_id+",'"+jsonRes[i].proj_user_status+"','"+jsonRes[i].projectArchStatus+"','"+projectType+"',"+jsonRes[i].source_id+",'0','0','','',"+jsonRes[i].user_id+",'cal_task','');\" projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" proj_user_status = \""+jsonRes[i].proj_user_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img id=\"notifProjImgNew_"+jsonRes[i].project_id+"\" onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" title=\""+jsonRes[i].project_name+"\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].task_comment+"\" class=\"searchDetails\">"+jsonRes[i].task_comment+" " + "</div>"
                                 +"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(wrkspaceTaskComments);
                     }
                 }
             }
             //Document Task Comments
             var jsonworkspaceDocumentTaskComments = JSON.parse(workspaceDocumentTaskComments);
             if(jsonworkspaceDocumentTaskComments){
                 var jsonRes = eval(jsonworkspaceDocumentTaskComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                     var projectType="";
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                       if(jsonRes[i].proj_user_status == 'PO'){
                           projectType="MyProjects";
                       }else{
                           projectType="SharedProjects";
                       }
                       
                       wrkspaceDocumentTaskComments = "<div onclick = \"loadDetailNotData('calendar',"+jsonRes[i].project_id+",'','WorkspaceDocument',"+jsonRes[i].task_id+",'',"+jsonRes[i].task_id+",'"+jsonRes[i].proj_user_status+"','"+jsonRes[i].projectArchStatus+"','"+projectType+"',"+jsonRes[i].source_id+",'0','0','','',"+jsonRes[i].user_id+",'workspace_Task','');\"  projectId = \""+jsonRes[i].project_id+"\" projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" proj_user_status = \""+jsonRes[i].proj_user_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img id=\"notifProjImgNew_"+jsonRes[i].project_id+"\" onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" title=\""+jsonRes[i].project_name+"\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 //+"<div id=\"loadDetailNotDataprojId\" projectId = \""+jsonRes[i].project_id+"\" style = \"display:none;\"></div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].task_comment+"\" class=\"searchDetails\">"+jsonRes[i].task_comment+" " + "</div>"
                                 +"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(wrkspaceDocumentTaskComments);
                     }
                 }
             }
             
             var jsonworkspaceTaskComments = JSON.parse(workspaceTaskComments);
             if(jsonworkspaceTaskComments){
                 var jsonRes = eval(jsonworkspaceTaskComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                     var projectType="";
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                       if(jsonRes[i].proj_user_status == 'PO'){
                           projectType="MyProjects";
                       }else{
                           projectType="SharedProjects";
                       }
                       
                       wrkspaceTaskComments = "<div onclick = \"loadDetailNotData('calendar',"+jsonRes[i].project_id+",'','Tasks',"+jsonRes[i].task_id+",'',"+jsonRes[i].task_id+",'"+jsonRes[i].proj_user_status+"','"+jsonRes[i].projectArchStatus+"','"+projectType+"',"+jsonRes[i].source_id+",'0','0','','',"+jsonRes[i].user_id+",'workspace_Task','');\"  projectId = \""+jsonRes[i].project_id+"\" projectUserStatus=\""+jsonRes[i].proj_user_status+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" proj_user_status = \""+jsonRes[i].proj_user_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 11%;margin-left: 2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;\" class=\"searchDetails\">"
                                     +"<img id=\"notifProjImgNew_"+jsonRes[i].project_id+"\" onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" title=\""+jsonRes[i].project_name+"\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 //+"<div id=\"loadDetailNotDataprojId\" projectId = \""+jsonRes[i].project_id+"\" style = \"display:none;\"></div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 21.5%;\" title=\""+jsonRes[i].task_comment+"\" class=\"searchDetails\">"+jsonRes[i].task_comment+" " + "</div>"
                                 +"<div style=\"width: 15%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(wrkspaceTaskComments);
                     }
                 }
             }
             
             /*var jsonAvailProjects = JSON.parse(availProjects);
             if(jsonAvailProjects){
                 var jsonRes = eval(jsonAvailProjects);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     availProjects = "<div onclick = \"openProjectDetails(this,  "+jsonRes[i].project_id+");\" mType = \"availProjects\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" proj_user_status = \""+jsonRes[i].proj_user_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 16%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(availProjects);
                     }
                 }
             }*/
             
             
             
             /*var jsonSharedDocs = JSON.parse(wrkSpaceShareDocs);
             if(jsonSharedDocs){
                 var jsonRes = eval(jsonSharedDocs);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                     jsonRes[i].folder_name = replaceSpecialCharacters(jsonRes[i].folder_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     sharedDocsWrkSpace = "<div onclick = \"openWrkSearchDetails(this);\" mType = \"sharedFolders\" folderId = \""+jsonRes[i].folder_id+"\" folderName=\""+jsonRes[i].folder_name+"\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" parent_folder_id = \""+jsonRes[i].parent_folder_id+"\" proj_user_status = \""+jsonRes[i].proj_user_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].folder_name+"\" class=\"searchDetails\">"+jsonRes[i].folder_name+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(sharedDocsWrkSpace);
                     }
                 }
             }
             */
             
             
             /*var jsonEventTasksWrkspace = JSON.parse(eventTasksWrkspace);
             if(jsonEventTasksWrkspace){
                 var jsonRes = eval(jsonEventTasksWrkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].event_name = replaceSpecialCharacters(jsonRes[i].event_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     eventTasksWrkspaceCont = "<div  onclick = \"openWrkSpaceSearchDetails(this);\"  typeOfTask = \"eventTask\" id = \""+jsonRes[i].event_id+"\" mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].event_name+"\" class=\"searchDetails\">"+jsonRes[i].event_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\"class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(eventTasksWrkspaceCont);
                     }
                 }
             }
             
             var jsonAssignedEventTasksWrkspace = JSON.parse(assignedEventTasksWrkspace);
             if(jsonAssignedEventTasksWrkspace){
                 var jsonRes = eval(jsonAssignedEventTasksWrkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].event_name = replaceSpecialCharacters(jsonRes[i].event_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     assignedEventTasksWrkspaceCont = "<div  onclick = \"openWrkSpaceSearchDetails(this);\" typeOfTask = \"eventTask\" id = \""+jsonRes[i].event_id+"\"  mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].event_name+"\" class=\"searchDetails\">"+jsonRes[i].event_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(assignedEventTasksWrkspaceCont);
                     }
                 }
             }
             
             var jsonIdeaTasksWorkspace = JSON.parse(ideaTasksWorkspace);
             if(jsonIdeaTasksWorkspace){
                 var jsonRes = eval(jsonIdeaTasksWorkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].idea_task_name = replaceSpecialCharacters(jsonRes[i].idea_task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     ideaTasksWorkspaceCont = "<div  onclick = \"openWrkSpaceSearchDetails(this);\" typeOfTask = \"ideaTask\"  id = \""+jsonRes[i].idea_task_id+"\"  mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].idea_task_name+"\" class=\"searchDetails\">"+jsonRes[i].idea_task_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\"  class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(ideaTasksWorkspaceCont);
                     }
                 }
             }
             
             var jsonAssignedIdeaTasksWorkspace = JSON.parse(assignedIdeaTasksWorkspace);
             if(jsonAssignedIdeaTasksWorkspace){
                 var jsonRes = eval(jsonAssignedIdeaTasksWorkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].idea_task_name = replaceSpecialCharacters(jsonRes[i].idea_task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     assignedIdeaTasksWorkspaceCont = "<div  onclick = \"openWrkSpaceSearchDetails(this);\" typeOfTask = \"ideaTask\" id = \""+jsonRes[i].idea_task_id+"\" mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].idea_task_name+"\" class=\"searchDetails\">"+jsonRes[i].idea_task_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\"  class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(assignedIdeaTasksWorkspaceCont);
                     }
                 }
             }
             
             var jsonTaskWrkspace = JSON.parse(taskWrkspace);
             if(jsonTaskWrkspace){
                 var jsonRes = eval(jsonTaskWrkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].task_name = replaceSpecialCharacters(jsonRes[i].task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     taskWrkspaceCont = "<div  onclick = \"openWrkSpaceSearchDetails(this);\" typeOfTask = \"Tasks\" id = \""+jsonRes[i].task_id+"\"  mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].task_name+"\" class=\"searchDetails\">"+jsonRes[i].task_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(taskWrkspaceCont);
                     }
                 }
             }
             
             var jsonAssignedTaskWrkspace = JSON.parse(assignedTaskWrkspace);
             if(jsonAssignedTaskWrkspace){
                 var jsonRes = eval(jsonAssignedTaskWrkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].task_name = replaceSpecialCharacters(jsonRes[i].task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     assignedTaskWrkspaceCont = "<div onclick = \"openWrkSpaceSearchDetails(this);\" typeOfTask = \"Tasks\" id = \""+jsonRes[i].task_id+"\"  mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].task_name+"\" class=\"searchDetails\">"+jsonRes[i].task_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(assignedTaskWrkspaceCont);
                     }
                 }
             }
             
             var jsonSprintTasksWorkspace = JSON.parse(sprintTasksWorkspace);
             if(jsonSprintTasksWorkspace){
                 var jsonRes = eval(jsonSprintTasksWorkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].sprint_task_name = replaceSpecialCharacters(jsonRes[i].sprint_task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     sprintTasksWorkspaceCont = "<div  onclick = \"openWrkSpaceSearchDetails(this);\" typeOfTask = \"sprintTask\" id = \""+jsonRes[i].sprint_task_id+"\"   mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].sprint_task_name+"\" class=\"searchDetails\">"+jsonRes[i].sprint_task_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(sprintTasksWorkspaceCont);
                     }
                 }
             }
             
             var jsonAssignedSprintTaskWorkspace = JSON.parse(assignedSprintTaskWorkspace);
             if(jsonAssignedSprintTaskWorkspace){
                 var jsonRes = eval(jsonAssignedSprintTaskWorkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].sprint_task_name = replaceSpecialCharacters(jsonRes[i].sprint_task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     assignedSprintTaskWorkspaceCont = "<div onclick = \"openWrkSpaceSearchDetails(this);\" typeOfTask = \"sprintTask\" id = \""+jsonRes[i].sprint_task_id+"\" mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].sprint_task_name+"\" class=\"searchDetails\">"+jsonRes[i].sprint_task_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(assignedSprintTaskWorkspaceCont);
                     }
                 }
             }
             
             var jsonWorkflowTasksWorkspace = JSON.parse(workflowTasksWorkspace);
             if(jsonWorkflowTasksWorkspace){
                 var jsonRes = eval(jsonWorkflowTasksWorkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].workflow_task_name = replaceSpecialCharacters(jsonRes[i].workflow_task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     workflowTasksWorkspaceCont = "<div onclick = \"openWrkSpaceSearchDetails(this);\"  typeOfTask = \"workflowTask\" workFlowId = \""+jsonRes[i].workflow_id+"\" id = \""+jsonRes[i].workflow_task_id+"\" mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].workflow_task_name+"\" class=\"searchDetails\">"+jsonRes[i].workflow_task_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(workflowTasksWorkspaceCont);
                     }
                 }
             }
             
             var jsonAssginedWorkflowTasksWorkspace = JSON.parse(assginedWorkflowTasksWorkspace);
             if(jsonAssginedWorkflowTasksWorkspace){
                 var jsonRes = eval(jsonAssginedWorkflowTasksWorkspace);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Workspace'){
                     jsonRes[i].workflow_task_name = replaceSpecialCharacters(jsonRes[i].workflow_task_name);
                     jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                     
                     var d = new Date();
                       var time = d.getTime();
                     var projImgType = jsonRes[i].project_image_type;
                     if(projImgType == ''){
                           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                       }else{
                           var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                       }
                     assginedWorkflowTasksWorkspaceCont = "<div onclick = \"openWrkSpaceSearchDetails(this);\"  typeOfTask = \"workflowTask\" workFlowId = \""+jsonRes[i].workflow_id+"\" id = \""+jsonRes[i].workflow_task_id+"\" mAct = \"Task\" projectId = \""+jsonRes[i].project_id+"\" projectName = \""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" projectArchiveStat = \""+jsonRes[i].archive_status+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 8%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 6%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" title=\""+jsonRes[i].workflow_task_name+"\" class=\"searchDetails\">"+jsonRes[i].workflow_task_name+" " + "</div>"
                                 +"<div style=\"width: 16%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 11%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                         $('#srchCont_'+count).append(assginedWorkflowTasksWorkspaceCont);
                     }
                 }
             }*/
             
         }
     }
     
     if(module == 'Ideas' && result == "[]^_^[]^_^[]^_^[]^_^[]^_^[]^_^[]^_^[]"){
         $('#srchCont_'+count).html('No Results found.');
         $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
     }else{
         if(module == 'Ideas'){
             if(prevICount != count){
                 prevICount = count;
             }
             var ideaProjects = result.split('^_^')[0];
             var ideasTopAndPro = result.split('^_^')[1];
             var ideaCommentsOwn = result.split('^_^')[2];
             var ideaTasksOwn = result.split('^_^')[3];
             var ideaLinksOwn = result.split('^_^')[4];
             var ideaStoriesOwn = result.split('^_^')[5];
             var sharedIdeas = result.split('^_^')[6];
             var ownIdeaDocs = result.split('^_^')[7];
             var ideaTaskComments = result.split('^_^')[8]; 
             
             
             
             var jsonIdeaProjects = JSON.parse(ideaProjects);
             if(jsonIdeaProjects){
                 var jsonRes = eval(jsonIdeaProjects);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         ideaProjectsCont =  "<div taskSourceId=\""+jsonRes[i].project_id+"\" ideaTaskId=\""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectId = \""+jsonRes[i].project_id+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\"  onclick = \"redirectToIdeas(this)\" ideaType=\"Idea_list\"  type = \"Project\" mAct = \"project_share\" topicId = \""+jsonRes[i].project_id+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 //"<div class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+ "</div>"
                                 +"<div style=\"width: 14%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaProjectsCont);
                     }
                 }
             }
             
             var jsonIdeasTopAndPro = JSON.parse(ideasTopAndPro);
             if(jsonIdeasTopAndPro){
                 var jsonRes = eval(jsonIdeasTopAndPro);
                 for(var i = 0;i < jsonRes.length;i++){
                 
                     if(jsonRes[i].moduleName == 'Ideas'){
                         idea_title = jsonRes[i].idea_title.replaceAll('CHR(26)',':').replaceAll("CHR(39)", "\'");
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideasTopAndProCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_id+"\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick = \"redirectToIdeas(this)\" ideaType=\"Idea_list\" type = \"Topic\" mAct = \"idea_share\" ideaId = \""+jsonRes[i].idea_id+"\" topicId = \""+jsonRes[i].parent_id+"\" parentIdeaId = \""+jsonRes[i].parent_idea_id+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].idea_title+"\" class=\"searchDetails\">"+idea_title+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideasTopAndProCont);
                     }
                 }
             }
             
             var jsonIdeaCommentsOwn = JSON.parse(ideaCommentsOwn);
             if(jsonIdeaCommentsOwn){
                 var jsonRes = eval(jsonIdeaCommentsOwn);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         idea_comment = replaceSpecialCharacters(jsonRes[i].idea_comment);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideaCommentsOwnCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ppId=\""+jsonRes[i].parent_id+"\" parentIdeaIds=\""+jsonRes[i].parentIdeaIds+"\" ideaTaskId=\""+jsonRes[i].idea_comment_id+"\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\"  onclick = \"redirectToIdeas(this)\" ideaType=\"idea_comment\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].idea_comment+"\" class=\"searchDetails\">"+idea_comment+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaCommentsOwnCont);
                     }
                 }
             }
             
             var jsonIdeaTasksOwn = JSON.parse(ideaTasksOwn);
             if(jsonIdeaTasksOwn){
                 var jsonRes = eval(jsonIdeaTasksOwn);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         idea_task_name = replaceSpecialCharacters(jsonRes[i].idea_task_name);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideaTasksOwnCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ppId=\""+jsonRes[i].root_idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_task_id+"\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick = \"redirectToIdeas(this)\" class=\"notificationgrid  reprtsContCls\" ideaType=\"Idea\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].idea_task_name+"\" class=\"searchDetails\">"+idea_task_name+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaTasksOwnCont);
                     }
                 }
             }
             //new
             var jsonIdeaTaskComments = JSON.parse(ideaTaskComments);
             if(jsonIdeaTaskComments){
                 var jsonRes = eval(jsonIdeaTaskComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         idea_task_name = replaceSpecialCharacters(jsonRes[i].idea_task_name);
                         task_comment = replaceSpecialCharacters(jsonRes[i].task_comment);
                         jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                         ideaTaskCommentsCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_task_id+"\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\"  onclick = \"loadDetailNotData('calendar',"+jsonRes[i].project_id+",'','Idea',"+jsonRes[i].idea_task_id+",'',"+jsonRes[i].idea_task_id+",'"+jsonRes[i].proj_user_status+"','"+jsonRes[i].projectArchStatus+"','"+jsonRes[i].projectType+"',"+jsonRes[i].source_id+",'0','0','','',"+jsonRes[i].user_id+",'idea_task',''); \" class=\"notificationgrid  reprtsContCls\" ideaType=\"Idea\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 4%;display:none;\" class=\"searchDetails\">"
                                     +"<img id=\"notifProjImgNew_"+jsonRes[i].project_id+"\" onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" title=\""+jsonRes[i].project_name+"\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].task_comment+"\" class=\"searchDetails\">"+task_comment+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaTaskCommentsCont);
                     }
                 }
             }
             
             var jsonIdeaLinksOwn = JSON.parse(ideaLinksOwn);
             if(jsonIdeaLinksOwn){
                 var jsonRes = eval(jsonIdeaLinksOwn);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         link_text = replaceSpecialCharacters(jsonRes[i].link_text);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideaLinksOwnCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_link_id+"\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick = \"redirectToIdeas(this)\" ideaType=\"idea_document\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].link_text+"\" class=\"searchDetails\">"+link_text+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaLinksOwnCont);
                     }
                 }
             }
             
             var jsonIdeaStoriesOwn = JSON.parse(ideaStoriesOwn);
             if(jsonIdeaStoriesOwn){
                 var jsonRes = eval(jsonIdeaStoriesOwn);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideaStoriesOwnCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_story_id+"\"  projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick = \"redirectToIdeas(this)\" class=\"notificationgrid  reprtsContCls\" ideaType=\"Idea_list\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+epic_title+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaStoriesOwnCont);
                     }
                 }
             }
             
             var jsonSharedIdeas = JSON.parse(sharedIdeas);
             if(jsonSharedIdeas){
                 var jsonRes = eval(jsonSharedIdeas);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         if(jsonRes[i].listType == 'Ideas'){
                             idea_title = jsonRes[i].idea_title.replaceAll('CHR(26)',':').replaceAll("CHR(39)", "\'");
                             jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                             ideasOnTopicShareCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].parent_id+"\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" ideaType=\"Idea_list\" onclick = \"redirectToIdeas(this)\" type = \"Topic\" mAct = \"idea_share\" ideaId = \""+jsonRes[i].idea_id+"\" topicId = \""+jsonRes[i].parent_id+"\" parentIdeaId = \""+jsonRes[i].parent_idea_id+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].idea_title+"\" class=\"searchDetails\">"+idea_title+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideasOnTopicShareCont);
                         }else if(jsonRes[i].listType == 'Document'){
                             document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                             jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                             docsOnTopicShareCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_documents_id+"\" ideaType=\"idea_document\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick = \"redirectToIdeas(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+document_name+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 /*if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                     docsOnTopicShareCont += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                         +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                     +"</div>"
                                 }else{
                                     docsOnTopicShareCont += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                 }*/
                                 +"</div>"
                                 $('#srchCont_'+count).append(docsOnTopicShareCont);
                         }else if(jsonRes[i].listType == 'Link'){
                             link_text = replaceSpecialCharacters(jsonRes[i].link_text);
                             jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                             ideaLinksOnTopicShareCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_link_id+"\" ideaType=\"idea_document\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick = \"redirectToIdeas(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].link_text+"\" class=\"searchDetails\">"+link_text+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaLinksOnTopicShareCont);
                         }else if(jsonRes[i].listType == 'Story'){
                             epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                             jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                             ideaStoriesOnTopicShareCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_story_id+"\" ideaType=\"Idea_list\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick = \"redirectToIdeas(this)\"  class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+epic_title+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaStoriesOnTopicShareCont);
                         }else if(jsonRes[i].listType == 'Task'){
                             idea_task_name = replaceSpecialCharacters(jsonRes[i].idea_task_name);
                             jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                             ideaTasksOnTopicShareCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_task_id+"\" ideaType=\"Idea\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\"  onclick = \"redirectToIdeas(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].idea_task_name+"\" class=\"searchDetails\">"+idea_task_name+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaTasksOnTopicShareCont);
                         }else if(jsonRes[i].listType == 'Comments'){
                             idea_comment = replaceSpecialCharacters(jsonRes[i].idea_comment);
                             jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                             ideaCommentsOnTopicShareCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_comment_id+"\" ideaType=\"idea_comment\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick = \"redirectToIdeas(this)\"  class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].idea_comment+"\" class=\"searchDetails\">"+idea_comment+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaCommentsOnTopicShareCont);
                         }
                     }
                 }
             }
             
             var jsonOwnIdeaDocs = JSON.parse(ownIdeaDocs);
             if(jsonOwnIdeaDocs){
                 var jsonRes = eval(jsonOwnIdeaDocs);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ownIdeaDocsCont =  "<div taskSourceId=\""+jsonRes[i].idea_id+"\" ideaTaskId=\""+jsonRes[i].idea_documents_id+"\" ideaType=\"idea_document\" projectId = \""+jsonRes[i].parent_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].parent_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick = \"redirectToIdeas(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 41%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+document_name+ "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 /*if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                     ownIdeaDocsCont += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                         +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                     +"</div>"
                                 }else{
                                     ownIdeaDocsCont += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                 }*/
                                 +"</div>"
                                 $('#srchCont_'+count).append(ownIdeaDocsCont);
                     }
                 }
             }
             
             /*var jsonMyTopics = JSON.parse(myTopics);
             if(jsonMyTopics){
                 var jsonRes = eval(jsonMyTopics);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].topic_name = replaceSpecialCharacters(jsonRes[i].topic_name);
                         myTopicsCont =  "<div onclick = \"redirectToTopics(this)\" type = \"Topic\" mAct = \"topic_share\"  topicId = \""+jsonRes[i].topic_id+"\"  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 //"<div class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].topic_name+"\" class=\"searchDetails\">"+jsonRes[i].topic_name+ "</div>"
                                 +"<div style=\"width: 20%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(myTopicsCont);
                     }
                 }
             }
             
             var jsonTopicsShared = JSON.parse(topicsShared);
             if(jsonTopicsShared){
                 var jsonRes = eval(jsonTopicsShared);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].topic_name = replaceSpecialCharacters(jsonRes[i].topic_name);
                         topicsSharedCont =  "<div onclick = \"redirectToTopics(this)\" type = \"Topic\" mAct = \"topic_share\"  topicId = \""+jsonRes[i].topic_id+"\"  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 //"<div class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].topic_name+"\" class=\"searchDetails\">"+jsonRes[i].topic_name+ "</div>"
                                 +"<div style=\"width: 20%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(topicsSharedCont);
                     }
                 }
             }
             
             var jsonSharedIdeaTopic = JSON.parse(sharedIdeaTopic);
             if(jsonSharedIdeaTopic){
                 var jsonRes = eval(jsonSharedIdeaTopic);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].topic_name = replaceSpecialCharacters(jsonRes[i].topic_name);
                         sharedIdeaTopicCont =  "<div onclick = \"redirectToTopics(this)\" type = \"Topic\" mAct = \"topic_share\"  topicId = \""+jsonRes[i].topic_id+"\"  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 //"<div class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].topic_name+"\" class=\"searchDetails\">"+jsonRes[i].topic_name+ "</div>"
                                 +"<div style=\"width: 20%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(sharedIdeaTopicCont);
                     }
                 }
             }
             
             
             
             var jsonIdeasOnTopicShare = JSON.parse(ideasOnTopicShare);
             if(jsonIdeasOnTopicShare){
                 var jsonRes = eval(jsonIdeasOnTopicShare);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].idea_title = jsonRes[i].idea_title.replaceAll('CHR(26)',':').replaceAll("CHR(39)", "\'");
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideasOnTopicShareCont =  "<div onclick = \"redirectToTopics(this)\" type = \"Topic\" mAct = \"idea_share\" ideaId = \""+jsonRes[i].idea_id+"\" topicId = \""+jsonRes[i].parent_id+"\" parentIdeaId = \""+jsonRes[i].parent_idea_id+"\" class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].idea_title+"\" class=\"searchDetails\">"+jsonRes[i].idea_title+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideasOnTopicShareCont);
                     }
                 }
             }
             
             
             var jsonIdeaCommentsOnTopicShare = JSON.parse(ideaCommentsOnTopicShare);
             if(jsonIdeaCommentsOnTopicShare){
                 var jsonRes = eval(jsonIdeaCommentsOnTopicShare);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].idea_comment = replaceSpecialCharacters(jsonRes[i].idea_comment);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideaCommentsOnTopicShareCont =  "<div   class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].idea_comment+"\" class=\"searchDetails\">"+jsonRes[i].idea_comment+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaCommentsOnTopicShareCont);
                     }
                 }
             }
             
             
             var jsonIdeaTasksOnTopicShare = JSON.parse(ideaTasksOnTopicShare);
             if(jsonIdeaTasksOnTopicShare){
                 var jsonRes = eval(jsonIdeaTasksOnTopicShare);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].idea_task_name = replaceSpecialCharacters(jsonRes[i].idea_task_name);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideaTasksOnTopicShareCont =  "<div   class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].idea_task_name+"\" class=\"searchDetails\">"+jsonRes[i].idea_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaTasksOnTopicShareCont);
                     }
                 }
             }
             
             
             
             var jsonIdeaLinksOnTopicShare = JSON.parse(ideaLinksOnTopicShare);
             if(jsonIdeaLinksOnTopicShare){
                 var jsonRes = eval(jsonIdeaLinksOnTopicShare);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].link_text = replaceSpecialCharacters(jsonRes[i].link_text);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideaLinksOnTopicShareCont =  "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].link_text+"\" class=\"searchDetails\">"+jsonRes[i].link_text+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaLinksOnTopicShareCont);
                     }
                 }
             }
             
             
             
             var jsonIdeaStoriesOnTopicShare = JSON.parse(ideaStoriesOnTopicShare);
             if(jsonIdeaStoriesOnTopicShare){
                 var jsonRes = eval(jsonIdeaStoriesOnTopicShare);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         ideaStoriesOnTopicShareCont =  "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+jsonRes[i].epic_title+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(ideaStoriesOnTopicShareCont);
                     }
                 }
             }
             
             
             
             var jsonDocsOnTopicShare = JSON.parse(docsOnTopicShare);
             if(jsonDocsOnTopicShare){
                 var jsonRes = eval(jsonDocsOnTopicShare);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Ideas'){
                         jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                         jsonRes[i].parent_name = replaceSpecialCharacters(jsonRes[i].parent_name);
                         docsOnTopicShareCont =  "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].parent_name+"\" class=\"searchDetails\">"+jsonRes[i].parent_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                     docsOnTopicShareCont += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                         +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                     +"</div>"
                                 }else{
                                     docsOnTopicShareCont += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                 }
                                 +"</div>"
                                 $('#srchCont_'+count).append(docsOnTopicShareCont);
                     }
                 }
             }*/
             
             
         }
     }
     
     if(module == 'Agile' && result == "[]"){
         $('#srchCont_'+count).html('No Results found.');
         $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
     }else{
         if(module == 'Agile'){
             if(prevACount != count){
                 prevACount = count;
             }
             var agileProjects = result.split('*_*')[0];
             var agileEFS = result.split('*_*')[1];
             var agileEFSComments = result.split('*_*')[2];
             var agileEFSLinks = result.split('*_*')[3];
             var agileEFSTasks = result.split('*_*')[4];
             var agileSprintGroups = result.split('*_*')[5];
             var agileSprint = result.split('*_*')[6];
             var agileSprintStages = result.split('*_*')[7];
             var agileDocs = result.split('*_*')[8];
             var agileTasksComments = result.split('*_*')[9];
             
             
             var jsonAgileProjects = JSON.parse(agileProjects);
             if(jsonAgileProjects){
                 var jsonRes = eval(jsonAgileProjects);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         jsonRes[i].project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                         agileProjectsCont = "<div pId=\"\" ppId=\"\" taskGroupId=\"\" taskSprintId=\"\" taskSourceId=\""+jsonRes[i].project_id+"\" agileTaskId=\""+jsonRes[i].project_id+"\" ideaType=\"idea_document\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick=\"redirectToAgile(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                     +"<div style=\"width: 11%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+jsonRes[i].project_name+" " + "</div>"
                                     +"<div style=\"width: 21%;\" class=\"searchDetails\"></div>"
                                     +"<div style=\"width: 18%;\" class=\"searchDetails\"></div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(agileProjectsCont);
                     }
                 }
             }
             
             var jsonAgileEFS = JSON.parse(agileEFS);
             if(jsonAgileEFS){
                 var jsonRes = eval(jsonAgileEFS);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                         agileEFSCont =   "<div pId=\""+jsonRes[i].parent_epic_id+"\" ppId=\""+jsonRes[i].pparent_epic_id+"\" taskGroupId=\"\" taskSprintId=\"\"  taskSourceId=\""+jsonRes[i].epic_id+"\" agileTaskId=\""+jsonRes[i].epic_id+"\" ideaType=\"idea_document\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick=\"redirectToAgile(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                     +"<div style=\"width: 10%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+project_name+" " + "</div>"
                                     +"<div style=\"width: 21%;\" title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+epic_title+" " + "</div>"
                                     +"<div style=\"width: 18%;\" class=\"searchDetails\"></div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                                 $('#srchCont_'+count).append(agileEFSCont);
                     }
                 }
             }
             
             var jsonAgileEFSComments = JSON.parse(agileEFSComments);
             if(jsonAgileEFSComments){
                 var jsonRes = eval(jsonAgileEFSComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         epic_comment = replaceSpecialCharacters(jsonRes[i].epic_comment);
                         epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                         agileEFSCommentsCont =  "<div pId=\""+jsonRes[i].parent_epic_id+"\" ppId=\""+jsonRes[i].pparent_epic_id+"\" taskGroupId=\"\" taskSprintId=\"\"  taskSourceId=\""+jsonRes[i].epic_id+"\" agileTaskId=\""+jsonRes[i].epic_comment_id+"\" ideaType=\"agile_comment\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\"onclick=\"redirectToAgile(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                     +"<div style=\"width: 10%;\" title=\""+jsonRes[i].sublistType+"\" class=\"searchDetails\">"+jsonRes[i].sublistType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+project_name+" " + "</div>"
                                     +"<div style=\"width: 21%;\" title=\""+jsonRes[i].epic_comment+"\" class=\"searchDetails\">"+epic_comment+" " + "</div>"
                                     +"<div style=\"width: 18%;\"  title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+epic_title+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                                 $('#srchCont_'+count).append(agileEFSCommentsCont);
                     }
                 }
             }
             
             var jsonAgileEFSLinks = JSON.parse(agileEFSLinks);
             if(jsonAgileEFSLinks){
                 var jsonRes = eval(jsonAgileEFSLinks);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         link_text = replaceSpecialCharacters(jsonRes[i].link_text);
                         epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                         agileEFSLinksCont =  "<div pId=\""+jsonRes[i].parent_epic_id+"\" ppId=\""+jsonRes[i].pparent_epic_id+"\" taskGroupId=\"\" taskSprintId=\"\"  taskSourceId=\""+jsonRes[i].epic_id+"\" agileTaskId=\""+jsonRes[i].epic_link_id+"\" ideaType=\"agile_document\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick=\"redirectToAgile(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                     +"<div style=\"width: 10%;\" title=\""+jsonRes[i].sublistType+"\" class=\"searchDetails\">"+jsonRes[i].sublistType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+project_name+" " + "</div>"
                                     +"<div style=\"width: 21%;\" title=\""+jsonRes[i].link_text+"\" class=\"searchDetails\">"+link_text+" " + "</div>"
                                     +"<div style=\"width: 18%;\"  title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+epic_title+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                                 $('#srchCont_'+count).append(agileEFSLinksCont);
                     }
                 }
             }
             
             var jsonAgileEFSTasks = JSON.parse(agileEFSTasks);
             if(jsonAgileEFSTasks){
                 var jsonRes = eval(jsonAgileEFSTasks);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         sprint_task_name = replaceSpecialCharacters(jsonRes[i].sprint_task_name);
                         epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                         agileEFSTasksCont =  "<div pId=\""+jsonRes[i].parent_epic_id+"\" ppId=\""+jsonRes[i].pparent_epic_id+"\" taskGroupId=\""+jsonRes[i].sprint_group_id+"\" taskSprintId=\""+jsonRes[i].sprint_id+"\" taskSourceId=\""+jsonRes[i].epic_id+"\" agileTaskId=\""+jsonRes[i].sprint_task_id+"\" ideaType=\"Sprint\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick=\"redirectToAgile(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                     +"<div style=\"width: 10%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+project_name+" " + "</div>"
                                     +"<div style=\"width: 21%;\" title=\""+jsonRes[i].sprint_task_name+"\" class=\"searchDetails\">"+sprint_task_name+" " + "</div>"
                                     +"<div style=\"width: 18%;\"  title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+epic_title+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                                 $('#srchCont_'+count).append(agileEFSTasksCont);
                     }
                 }
             }
             //new
             var jsonAgileTasksComments = JSON.parse(agileTasksComments);
             if(jsonAgileTasksComments){
                 var jsonRes = eval(jsonAgileTasksComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         sprint_task_name = replaceSpecialCharacters(jsonRes[i].sprint_task_name);
                         epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         task_comment = replaceSpecialCharacters(jsonRes[i].task_comment);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       //loadDetailNotData(menuType, connNotUseId,createdName,notificationType, notTypeId, pId, notCmtDocId,projectUsersStatus,projectArchStatus,projectType,taskSourceId,tasksprintId,taskSprintGroupId,notificationId,ppId,notftaskCommentedBy,notifTaskCmtType,notfEpicIds){
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           //('calendar',"+jsonRes[i].project_id+",'','Tasks',"+jsonRes[i].task_id+",'',"+jsonRes[i].task_id+",'"+jsonRes[i].proj_user_status+"','"+jsonRes[i].projectArchStatus+"','"+projectType+"',"+jsonRes[i].source_id+",'0','0','','',"+jsonRes[i].user_id+",'cal_task','');											
                         agileTasksCommentCont =  "<div pId=\""+jsonRes[i].parent_epic_id+"\" ppId=\""+jsonRes[i].pparent_epic_id+"\" taskGroupId=\""+jsonRes[i].sprint_group_id+"\" taskSprintId=\""+jsonRes[i].sprint_id+"\" taskSourceId=\""+jsonRes[i].epic_id+"\" agileTaskId=\""+jsonRes[i].sprint_task_id+"\" ideaType=\"Sprint\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick=\"loadDetailNotData('calendar',"+jsonRes[i].project_id+",'','Sprint',"+jsonRes[i].sprint_task_id+",'',"+jsonRes[i].sprint_task_id+",'"+jsonRes[i].proj_user_status+"','"+jsonRes[i].projectArchStatus+"','"+jsonRes[i].projectType+"',"+jsonRes[i].epic_id+","+jsonRes[i].sprint_id+","+jsonRes[i].sprint_group_id+",'','"+jsonRes[i].project_name+"',"+jsonRes[i].user_id+",'agile_task','');\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                     +"<div style=\"width: 10%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                         +"<img id=\"notifProjImgNew_"+jsonRes[i].project_id+"\" onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" title=\""+jsonRes[i].project_name+"\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+project_name+" " + "</div>"
                                     +"<div style=\"width: 21%;\" title=\""+jsonRes[i].task_comment+"\" class=\"searchDetails\">"+task_comment+" " + "</div>"
                                     +"<div style=\"width: 18%;\"  title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+epic_title+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                                 $('#srchCont_'+count).append(agileTasksCommentCont);
                     }
                 }
             }
             
             var jsonAgileSprintGroups = JSON.parse(agileSprintGroups);
             if(jsonAgileSprintGroups){
                 var jsonRes = eval(jsonAgileSprintGroups);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         sprint_group_name = replaceSpecialCharacters(jsonRes[i].sprint_group_name);
                         project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                         agileSprintGroupsCont =  "<div pId=\"\" ppId=\"\" taskGroupId=\""+jsonRes[i].sprint_group_id+"\" taskSprintId=\"\" taskSourceId=\""+jsonRes[i].sprint_group_id+"\" agileTaskId=\""+jsonRes[i].sprint_group_id+"\" ideaType=\"agile_sprintgroup\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick=\"redirectToAgile(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                     +"<div style=\"width: 10%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+project_name+" " + "</div>"
                                     +"<div style=\"width: 21%;\"  title=\""+jsonRes[i].sprint_group_name+"\" class=\"searchDetails\">"+sprint_group_name+" " + "</div>"
                                     +"<div style=\"width: 18%;\" class=\"searchDetails\"></div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                                 $('#srchCont_'+count).append(agileSprintGroupsCont);
                     }
                 }
             }
             
             var jsonAgileSprint = JSON.parse(agileSprint);
             if(jsonAgileSprint){
                 var jsonRes = eval(jsonAgileSprint);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         sprint_group_name = replaceSpecialCharacters(jsonRes[i].sprint_group_name);
                         project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         sprint_title = replaceSpecialCharacters(jsonRes[i].sprint_title);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                         agileSprintCont =  "<div pId=\"\" ppId=\"\" taskGroupId=\"\" taskSprintId=\""+jsonRes[i].sprint_id+"\" taskSourceId=\""+jsonRes[i].sprint_id+"\" agileTaskId=\""+jsonRes[i].sprint_id+"\" ideaType=\"agile_sprint\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick=\"redirectToAgile(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                     +"<div style=\"width: 10%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+project_name+" " + "</div>"
                                     +"<div style=\"width: 21%;\" title=\""+jsonRes[i].sprint_title+"\" class=\"searchDetails\">"+sprint_title+" " + "</div>"
                                     +"<div style=\"width: 18%;\" title=\""+jsonRes[i].sprint_group_name+"\" class=\"searchDetails\">"+sprint_group_name+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                                 $('#srchCont_'+count).append(agileSprintCont);
                     }
                 }
             }
             
             var jsonAgileSprintStages = JSON.parse(agileSprintStages);
             if(jsonAgileSprintStages){
                 var jsonRes = eval(jsonAgileSprintStages);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         sprint_group_name = replaceSpecialCharacters(jsonRes[i].sprint_group_name);
                         project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         stage_name = replaceSpecialCharacters(jsonRes[i].stage_name);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                         agileSprintCont =  "<div pId=\"\" ppId=\"\" taskGroupId=\""+jsonRes[i].sprint_group_id+"\" taskSprintId=\"\" taskSourceId=\""+jsonRes[i].sprint_group_id+"\" agileTaskId=\""+jsonRes[i].stage_id+"\" ideaType=\"agile_sprintstage\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick=\"redirectToAgile(this)\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                     +"<div style=\"width: 10%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                         +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                     +"</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+project_name+" " + "</div>"
                                     +"<div style=\"width: 21%;\" title=\""+jsonRes[i].stage_name+"\" class=\"searchDetails\">"+stage_name+" " + "</div>"
                                     +"<div style=\"width: 18%;\" title=\""+jsonRes[i].sprint_group_name+"\" class=\"searchDetails\">"+sprint_group_name+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                                 $('#srchCont_'+count).append(agileSprintCont);
                     }
                 }
             }
             
             var jsonAgileDocs = JSON.parse(agileDocs);
             if(jsonAgileDocs){
                 var jsonRes = eval(jsonAgileDocs);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Agile'){
                         document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                         project_name = replaceSpecialCharacters(jsonRes[i].project_name);
                         epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         var d = new Date();
                           var time = d.getTime();
                         var projImgType = jsonRes[i].project_image_type;
                         if(projImgType == ''){
                               var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
                           }else{
                               var wrkspaceImgType = lighttpdPath+"//projectimages//"+jsonRes[i].project_id+"."+projImgType+"?"+time;
                           }
                         agileDocsCont = "<div pId=\""+jsonRes[i].parent_epic_id+"\" ppId=\""+jsonRes[i].pparent_epic_id+"\" taskGroupId=\"\" taskSprintId=\"\"  taskSourceId=\""+jsonRes[i].epic_id+"\" agileTaskId=\""+jsonRes[i].epic_documents_id+"\" ideaType=\"agile_document\" projectId = \""+jsonRes[i].project_id+"\" projectType = \""+jsonRes[i].projectType+"\" projectArchStatus = \""+jsonRes[i].projectArchStatus+"\" projectUserStatus = \""+jsonRes[i].proj_user_status+"\"   projectName =\""+jsonRes[i].project_name+"\" projectImgType = \""+jsonRes[i].project_image_type+"\" onclick=\"redirectToAgile(this)\" class=\"notificationgrid  reprtsContCls\" style = \"0;margin-left:1.5%;margin: 3px 0 0 0;margin-left:1.5%;width: 100%\">"
                                 +"<div style=\"width: 10%;\" title=\""+jsonRes[i].sublistType+"\" class=\"searchDetails\">"+jsonRes[i].sublistType+" "+ "</div>"
                                 +"<div style=\" height: 70%; margin-top: 6px;width: 3%;\" class=\"searchDetails\">"
                                     +"<img onerror = \"imageOnProjErrorReplace(this)\"style = \"border-radius: 15px;float: left;height: 30px;margin: 0;width: 30px;\" src = \""+wrkspaceImgType+"\" class=\"searchDetails\"/>"
                                 +"</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].project_name+"\" class=\"searchDetails\">"+project_name+" " + "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+document_name+" " + "</div>"
                                 +"<div style=\"width: 18%;\" title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+epic_title+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 /*if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                     agileDocsCont += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                         +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                         +"</div>"
                                 }else{
                                     agileDocsCont += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                 }*/
                                 +"</div>"
                         $('#srchCont_'+count).append(agileDocsCont);
                     }
                 }
             }
         }
     }
     
     if(module == 'Task' && result == "[]*_*[]*_*[]*_*[]*_*[]"){
          $('#srchCont_'+count).html('No Results found.');
         $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
     }else{
         if(module == 'Task'){
             if(prevTCount != count){
                 prevTCount = count;
             }
             var myTaskCal = result.split("*_*")[0];
             var assignedTaskCal = result.split("*_*")[1];
             var calTaskComments = result.split("*_*")[2];
             var calTaskDocuments = result.split("*_*")[3];
             var calTaskLevelDocuments = result.split("*_*")[4];
             
             /*var myWrkspaceTaskCal = result.split("*_*")[2];
             var assignedWrkspacetaskCal = result.split("*_*")[3];
             var myWrkflowTaskCal = result.split("*_*")[4];
             var assignedWrkflowtaskCal = result.split("*_*")[5];
             var myIdeaTaskCal = result.split("*_*")[6];
             var assignedIdeaTaskCal = result.split("*_*")[7];
             var mySprintTaskCal = result.split("*_*")[8];
             var assignedSprintTask = result.split("*_*")[9];
             var meraEvents = result.split("*_*")[10];
             var assignedEventsCal = result.split("*_*")[11];
             var myDocTaskCal = result.split("*_*")[12];
             var assignedDocTaskCal = result.split("*_*")[13];*/
             
             var jsonMyTaskCal = JSON.parse(myTaskCal);
             if(jsonMyTaskCal){
                 var jsonRes = eval(jsonMyTaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].task_name = replaceSpecialCharacters(jsonRes[i].task_name);
                         myTaskCalCont = "<div onclick=\"calTaskRedirect(this)\" source_id=\""+jsonRes[i].source_id+"\" tasktype=\""+jsonRes[i].tasktype+"\" task_id=\""+jsonRes[i].task_id+"\"  class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 12%;margin-left:2%;\" title=\""+jsonRes[i].sublistType+"\" class=\"searchDetails\">"+jsonRes[i].sublistType+" "+ "</div>"
                                 +"<div style=\"width: 37.5%;float:left\"><div style=\"float:left;width:8%\"><img style = \"float: left;height:  30px;margin-top: 7px;width: 30px;\" src = \""+jsonRes[i].taskImg+"\"/></div>"
                                 +"<div  style=\"width: 87%;\"  title=\""+jsonRes[i].task_name+"\" class=\"searchDetails\">"+jsonRes[i].task_name+ "</div></div>"
                                 +"<div style=\"width: 19%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(myTaskCalCont);
                     }
                 }
             }
             
             var jsonAssignedTaskCal = JSON.parse(assignedTaskCal);
             if(jsonAssignedTaskCal){
                 var jsonRes = eval(jsonAssignedTaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].task_name = replaceSpecialCharacters(jsonRes[i].task_name);
                         assignedTaskCalCont = "<div  onclick=\"calTaskRedirect(this)\" source_id=\""+jsonRes[i].source_id+"\" tasktype=\""+jsonRes[i].tasktype+"\" task_id=\""+jsonRes[i].task_id+"\"  class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 12%;margin-left:2%;\" title=\""+jsonRes[i].sublistType+"\" class=\"searchDetails\">"+jsonRes[i].sublistType+" "+ "</div>"
                                 +"<div style=\"width: 37.5%;float:left\"><div style=\"float:left;width:8%\"><img style = \"float: left;height:  30px;margin-top: 7px;width: 30px;\" src = \""+jsonRes[i].taskImg+"\" /></div>"
                                 +"<div style=\"width: 87%;\"  title=\""+jsonRes[i].task_name+"\" class=\"searchDetails\">"+jsonRes[i].task_name+ "</div></div>"
                                 +"<div style=\"width: 19%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(assignedTaskCalCont);
                     }
                 }
             }
             
             var jsonCalTaskComments = JSON.parse(calTaskComments);
             if(jsonCalTaskComments){
                 var jsonRes = eval(jsonCalTaskComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].task_name = replaceSpecialCharacters(jsonRes[i].task_name);
                         jsonRes[i].task_comment = jsonRes[i].task_comment.replaceAll('CHR(26)',':').replaceAll("CHR(39)", "\'");
                         calTaskCommentsCont = "<div  onclick=\"calTaskRedirect(this)\"  source_id=\""+jsonRes[i].source_id+"\" tasktype=\""+jsonRes[i].tasktype+"\" task_id=\""+jsonRes[i].task_id+"\"  class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%\">"
                                 +"<div style=\"width: 12%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 37%;\" title=\""+jsonRes[i].task_comment+"\" class=\"searchDetails\">"+jsonRes[i].task_comment+ "</div>"
                                 +"<div style=\"width: 19%;\" title=\"\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(calTaskCommentsCont);
                     }
                 }
             }
             
             /*var jsonCalTaskDocuments = JSON.parse(calTaskDocuments);
             if(jsonCalTaskDocuments){
                 var jsonRes = eval(jsonCalTaskDocuments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].task_name = replaceSpecialCharacters(jsonRes[i].task_name);
                         jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                         calTaskDocumentsCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].task_name+"\" class=\"searchDetails\">"+jsonRes[i].task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                     calTaskDocumentsCont += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                         +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                     +"</div>"
                                 }else{
                                     calTaskDocumentsCont += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                 }
                                 +"</div>"
                                 $('#srchCont_'+count).append(calTaskDocumentsCont);
                     }
                 }
             }
             
             var jsonCalTaskLevelDocuments = JSON.parse(calTaskLevelDocuments);
             if(jsonCalTaskLevelDocuments){
                 var jsonRes = eval(jsonCalTaskLevelDocuments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].task_name = replaceSpecialCharacters(jsonRes[i].task_name);
                         jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                         calTaskLevelDocumentsCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 14%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].task_name+"\" class=\"searchDetails\">"+jsonRes[i].task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 if(jsonRes[i].matchType == "Both" || jsonRes[i].matchType == "Content"){
                                     calTaskLevelDocumentsCont += "<div style=\"width: 6%; float: left; height: 100%; margin-top: -5px;\" class=\"searchDetails\">"
                                         +"<img style = \"height: 50%; float: left;\" src = \"images/Idea/Document.png\" class=\"searchDetails\"/>"
                                     +"</div>"
                                 }else{
                                     calTaskLevelDocumentsCont += "<div style=\"width: 6%;\" class=\"searchDetails\"></div>"
                                 }
                                 +"</div>"
                                 $('#srchCont_'+count).append(calTaskLevelDocumentsCont);
                     }
                 }
             }*/
             /*var jsonMyWrkspaceTaskCal = JSON.parse(myWrkspaceTaskCal);
             if(jsonMyWrkspaceTaskCal){
                 var jsonRes = eval(jsonMyWrkspaceTaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].doc_task_name = replaceSpecialCharacters(jsonRes[i].doc_task_name);
                         jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                         myWrkspaceTaskCalCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/DocumentBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].doc_task_name+"\" class=\"searchDetails\">"+jsonRes[i].doc_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+"</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(myWrkspaceTaskCalCont);
                     }
                 }
             }
             
             var jsonAssignedWrkspacetaskCal = JSON.parse(assignedWrkspacetaskCal);
             if(jsonAssignedWrkspacetaskCal){
                 var jsonRes = eval(jsonAssignedWrkspacetaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].doc_task_name = replaceSpecialCharacters(jsonRes[i].doc_task_name);
                         jsonRes[i].document_name = replaceSpecialCharacters(jsonRes[i].document_name);
                         assignedWrkspacetaskCalCont ="<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/DocumentBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].doc_task_name+"\" class=\"searchDetails\">"+jsonRes[i].doc_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].document_name+"\" class=\"searchDetails\">"+jsonRes[i].document_name+"</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(assignedWrkspacetaskCalCont);
                     }
                 }
             }
             
             var jsonMyWrkflowTaskCal = JSON.parse(myWrkflowTaskCal);
             if(jsonMyWrkflowTaskCal){
                 var jsonRes = eval(jsonMyWrkflowTaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].workflow_task_name = replaceSpecialCharacters(jsonRes[i].workflow_task_name);
                         jsonRes[i].workflow_name = replaceSpecialCharacters(jsonRes[i].workflow_name);
                         myWrkflowTaskCalCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/WorkFlowBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].workflow_task_name+"\" class=\"searchDetails\">"+jsonRes[i].workflow_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].workflow_name+"\" class=\"searchDetails\">"+jsonRes[i].workflow_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(myWrkflowTaskCalCont);
                     }
                 }
             }
             
             var jsonAssignedWrkflowtaskCal = JSON.parse(assignedWrkflowtaskCal);
             if(jsonAssignedWrkflowtaskCal){
                 var jsonRes = eval(jsonAssignedWrkflowtaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].workflow_task_name = replaceSpecialCharacters(jsonRes[i].workflow_task_name);
                         jsonRes[i].workflow_name = replaceSpecialCharacters(jsonRes[i].workflow_name);
                         assignedWrkflowtaskCalCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/WorkFlowBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].workflow_task_name+"\" class=\"searchDetails\">"+jsonRes[i].workflow_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].workflow_name+"\" class=\"searchDetails\">"+jsonRes[i].workflow_name+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(assignedWrkflowtaskCalCont);
                     }
                 }
             }
             
             var jsonMyIdeaTaskCal = JSON.parse(myIdeaTaskCal);
             if(jsonMyIdeaTaskCal){
                 var jsonRes = eval(jsonMyIdeaTaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].idea_task_name = replaceSpecialCharacters(jsonRes[i].idea_task_name);
                         jsonRes[i].idea_title = jsonRes[i].idea_title.replaceAll('CHR(26)',':').replaceAll("CHR(39)", "\'");
                         myIdeaTaskCalCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/IdeaBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].idea_task_name+"\" class=\"searchDetails\">"+jsonRes[i].idea_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].idea_title+"\" class=\"searchDetails\">"+jsonRes[i].idea_title+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(myIdeaTaskCalCont);
                     }
                 }
             }
             
             var jsonAssignedIdeaTaskCal = JSON.parse(assignedIdeaTaskCal);
             if(jsonAssignedIdeaTaskCal){
                 var jsonRes = eval(jsonAssignedIdeaTaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].idea_task_name = replaceSpecialCharacters(jsonRes[i].idea_task_name);
                         jsonRes[i].idea_title = jsonRes[i].idea_title.replaceAll('CHR(26)',':').replaceAll("CHR(39)", "\'");
                         assignedIdeaTaskCalCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/IdeaBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].idea_task_name+"\" class=\"searchDetails\">"+jsonRes[i].idea_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].idea_title+"\" class=\"searchDetails\">"+jsonRes[i].idea_title+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(assignedIdeaTaskCalCont);
                     }
                 }
             }
             
             var jsonMySprintTaskCal = JSON.parse(mySprintTaskCal);
             if(jsonMySprintTaskCal){
                 var jsonRes = eval(jsonMySprintTaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].sprint_task_name = replaceSpecialCharacters(jsonRes[i].sprint_task_name);
                         jsonRes[i].epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         mySprintTaskCalCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/SprintBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].sprint_task_name+"\" class=\"searchDetails\">"+jsonRes[i].sprint_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+jsonRes[i].epic_title+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(mySprintTaskCalCont);
                     }
                 }
             }
             
             var jsonAssignedSprintTask = JSON.parse(assignedSprintTask);
             if(jsonAssignedSprintTask){
                 var jsonRes = eval(jsonAssignedSprintTask);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].sprint_task_name = replaceSpecialCharacters(jsonRes[i].sprint_task_name);
                         jsonRes[i].epic_title = replaceSpecialCharacters(jsonRes[i].epic_title);
                         assignedSprintTaskCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/SprintBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].sprint_task_name+"\" class=\"searchDetails\">"+jsonRes[i].sprint_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].epic_title+"\" class=\"searchDetails\">"+jsonRes[i].epic_title+ "</div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(assignedSprintTaskCont);
                     }
                 }
             }
             
             var jsonMeraEvents = JSON.parse(meraEvents);
             if(jsonMeraEvents){
                 var jsonRes = eval(jsonMeraEvents);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].event_name = replaceSpecialCharacters(jsonRes[i].event_name);
                         meraEventsCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/EventBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].event_name+"\" class=\"searchDetails\">"+jsonRes[i].event_name+ "</div>"
                                 +"<div style=\"width: 20%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(meraEventsCont);
                     }
                 }
             }
             
             var jsonAssignedEventsCalCont = JSON.parse(assignedEventsCal);
             if(jsonAssignedEventsCalCont){
                 var jsonRes = eval(jsonAssignedEventsCalCont);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].event_name = replaceSpecialCharacters(jsonRes[i].event_name);
                         assignedEventsCalCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/EventBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].event_name+"\" class=\"searchDetails\">"+jsonRes[i].event_name+ "</div>"
                                 +"<div style=\"width: 20%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(assignedEventsCalCont);
                     }
                 }
             }
             
             var jsonMyDocTaskCal = JSON.parse(myDocTaskCal);
             if(jsonMyDocTaskCal){
                 var jsonRes = eval(jsonMyDocTaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].doc_task_name = replaceSpecialCharacters(jsonRes[i].doc_task_name);
                         myDocTaskCalCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/DocumentBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].doc_task_name+"\" class=\"searchDetails\">"+jsonRes[i].doc_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(myDocTaskCalCont);
                     }
                 }
             }
             
             var jsonAssignedDocTaskCal = JSON.parse(assignedDocTaskCal);
             if(jsonAssignedDocTaskCal){
                 var jsonRes = eval(jsonAssignedDocTaskCal);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'Task'){
                         jsonRes[i].doc_task_name = replaceSpecialCharacters(jsonRes[i].doc_task_name);
                         assignedDocTaskCalCont = "<div  class=\"notificationgrid  reprtsContCls\" style = \"border: none;margin: 3px 0 0 6px;width: 100%\">"
                                 +"<div style=\"width: 12%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                 +"<img style = \"height: 18px;width: 18px;float: left;margin-top: 12px;\" src = \"images/calender/DocumentBlack.png\" class=\"searchDetails\"/>"
                                 +"<div style=\"width: 20%;\" title=\""+jsonRes[i].doc_task_name+"\" class=\"searchDetails\">"+jsonRes[i].doc_task_name+ "</div>"
                                 +"<div style=\"width: 20%;\" class=\"searchDetails\"></div>"
                                 +"<div style=\"width: 21%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                 +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                 +"</div>"
                                 $('#srchCont_'+count).append(assignedDocTaskCalCont);
                     }
                 }
             }*/
             
             
         }
     }
     
     if(module == 'My Zone' && result == "[]"){
         $('#srchCont_'+count).html('No Results found.');
         $('#srchCont_'+count).css({'width':'98%','font-size':'13px','font-family':'Tahoma','font-weight':'bold','margin-left':'25px','color':'black'});
     }else{
         if(module == 'My Zone'){
             if(prevMCount != count){
                 prevMCount = count;
             }
             var myNotes = result.split('*_*')[0];
             var myNotesContent = result.split('*_*')[1];
             var sharedNotes = result.split('*_*')[2];
             var sharedNotesContent = result.split('*_*')[3];
             var myBlog = result.split('*_*')[4];
             var myBlogContent = result.split('*_*')[5];
             var myBlogComments = result.split('*_*')[6];
             var sharedBlog = result.split('*_*')[7];
             var sharedBlogContent = result.split('*_*')[8];
             var sharedBlogComments = result.split('*_*')[9];
             var myAlbums = result.split('*_*')[10];				
             var myAlbumsComments = result.split('*_*')[11];
             //alert("myAlbumsComments>>>"+myAlbumsComments);
             var sharedAlbums = result.split('*_*')[12];				
             var sharedAlbumsComments = result.split('*_*')[13];
             var messages = result.split('*_*')[14];
             //alert("messages::::"+messages);
             
             
             var jsonMyNotes = JSON.parse(myNotes);
             if(jsonMyNotes){
                 var jsonRes = eval(jsonMyNotes);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].note_name = replaceSpecialCharacters(jsonRes[i].note_name);
                         myNotesCont = "<div onclick=\"fetchMyzoneDetailsData(this)\" mainId=\""+jsonRes[i].note_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%;margin-left:3%;\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 19%;\" title=\""+jsonRes[i].note_name+"\" class=\"searchDetails\">"+jsonRes[i].note_name+" " + "</div>"
                                     +"<div style=\"width: 19%;\" class=\"searchDetails\"></div>"
                                     +"<div style=\"width: 20%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(myNotesCont);
                     }
                 }
             }
             
             var jsonSharedNotes = JSON.parse(sharedNotes);
             if(jsonSharedNotes){
                 var jsonRes = eval(jsonSharedNotes);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].note_name = replaceSpecialCharacters(jsonRes[i].note_name);
                         sharedNotesCont = "<div onclick=\"fetchMyzoneDetailsData(this)\" mainId=\""+jsonRes[i].note_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;width: 100%;margin-left:3%;\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].note_name+"\" class=\"searchDetails\">"+jsonRes[i].note_name+" " + "</div>"
                                     +"<div style=\"width: 40%;\" class=\"searchDetails\"></div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(sharedNotesCont);
                     }
                 }
             }
             
             var jsonMyNotesContent = JSON.parse(myNotesContent);
             if(jsonMyNotesContent){
                 var jsonRes = eval(jsonMyNotesContent);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].note_name = replaceSpecialCharacters(jsonRes[i].note_name);
                         jsonRes[i].page_desc = replaceSpecialCharacters(jsonRes[i].page_desc);
                         myNotesContentCont = "<div onclick=\"fetchMyzoneDetailsData(this)\" mainId=\""+jsonRes[i].note_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].note_name+"\" class=\"searchDetails\">"+jsonRes[i].note_name+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].page_desc+"\" class=\"searchDetails\">"+jsonRes[i].page_desc+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(myNotesContentCont);
                     }
                 }
             }
             
             var jsonSharedNotesContent = JSON.parse(sharedNotesContent);
             if(jsonSharedNotesContent){
                 var jsonRes = eval(jsonSharedNotesContent);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].note_name = replaceSpecialCharacters(jsonRes[i].note_name);
                         jsonRes[i].page_desc = replaceSpecialCharacters(jsonRes[i].page_desc);
                         sharedNotesContentCont = "<div onclick=\"fetchMyzoneDetailsData(this)\" mainId=\""+jsonRes[i].note_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].note_name+"\" class=\"searchDetails\">"+jsonRes[i].note_name+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].page_desc+"\" class=\"searchDetails\">"+jsonRes[i].page_desc+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(sharedNotesContentCont);
                     }
                 }
             }
             
             var jsonMyBlog = JSON.parse(myBlog);
             if(jsonMyBlog){
                 var jsonRes = eval(jsonMyBlog);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].blog = replaceSpecialCharacters(jsonRes[i].blog);
                         myBlogCont = "<div onclick=\"fetchMyzoneDetailsData(this)\"  mainId=\""+jsonRes[i].blog_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].blog+"\" class=\"searchDetails\">"+jsonRes[i].blog+" " + "</div>"
                                     +"<div style=\"width: 40%;\" class=\"searchDetails\"></div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(myBlogCont);
                     }
                 }
             }
             
             var jsonSharedBlog = JSON.parse(sharedBlog);
             if(jsonSharedBlog){
                 var jsonRes = eval(jsonSharedBlog);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].blog = replaceSpecialCharacters(jsonRes[i].blog);
                         sharedBlogCont = "<div onclick=\"fetchMyzoneDetailsData(this)\"  mainId=\""+jsonRes[i].blog_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].blog+"\" class=\"searchDetails\">"+jsonRes[i].blog+" " + "</div>"
                                     +"<div style=\"width: 40%;\" class=\"searchDetails\"></div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(sharedBlogCont);
                     }
                 }
             }
             
             var jsonMyBlogContent = JSON.parse(myBlogContent);
             if(jsonMyBlogContent){
                 var jsonRes = eval(jsonMyBlogContent);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].blog = replaceSpecialCharacters(jsonRes[i].blog);
                         jsonRes[i].page_desc = replaceSpecialCharacters(jsonRes[i].page_desc);
                         myBlogContentCont = "<div onclick=\"fetchMyzoneDetailsData(this)\"  mainId=\""+jsonRes[i].blog_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].blog+"\" class=\"searchDetails\">"+jsonRes[i].blog+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].page_desc+"\" class=\"searchDetails\">"+jsonRes[i].page_desc+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(myBlogContentCont);
                     }
                 }
             }
             
             var jsonSharedBlogContent = JSON.parse(sharedBlogContent);
             if(jsonSharedBlogContent){
                 var jsonRes = eval(jsonSharedBlogContent);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].blog = replaceSpecialCharacters(jsonRes[i].blog);
                         jsonRes[i].page_desc = replaceSpecialCharacters(jsonRes[i].page_desc);
                         sharedBlogContentCont = "<div onclick=\"fetchMyzoneDetailsData(this)\"  mainId=\""+jsonRes[i].blog_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].blog+"\" class=\"searchDetails\">"+jsonRes[i].blog+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].page_desc+"\" class=\"searchDetails\">"+jsonRes[i].page_desc+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(sharedBlogContentCont);
                     }
                 }
             }
             
             var jsonMyAlbums = JSON.parse(myAlbums);
             if(jsonMyAlbums){
                 var jsonRes = eval(jsonMyAlbums);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].album_name = replaceSpecialCharacters(jsonRes[i].album_name);
                         jsonRes[i].album_desc = replaceSpecialCharacters(jsonRes[i].album_desc);
                         myAlbumsCont = "<div onclick=\"fetchMyzoneDetailsData(this)\"  mainId=\""+jsonRes[i].album_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].album_name+"\" class=\"searchDetails\">"+jsonRes[i].album_name+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].album_desc+"\" class=\"searchDetails\">"+jsonRes[i].album_desc+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(myAlbumsCont);
                     }
                 }
             }
             
             var jsonSharedAlbums = JSON.parse(sharedAlbums);
             if(jsonSharedAlbums){
                 var jsonRes = eval(jsonSharedAlbums);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].album_name = replaceSpecialCharacters(jsonRes[i].album_name);
                         jsonRes[i].album_desc = replaceSpecialCharacters(jsonRes[i].album_desc);
                         sharedAlbumsCont = "<div onclick=\"fetchMyzoneDetailsData(this)\" mainId=\""+jsonRes[i].album_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].album_name+"\" class=\"searchDetails\">"+jsonRes[i].album_name+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].album_desc+"\" class=\"searchDetails\">"+jsonRes[i].album_desc+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(sharedAlbumsCont);
                     }
                 }
             }
             
             var jsonMyBlogComments = JSON.parse(myBlogComments);
             if(jsonMyBlogComments){
                 var jsonRes = eval(jsonMyBlogComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].blog = replaceSpecialCharacters(jsonRes[i].blog);
                         jsonRes[i].blog_post = replaceSpecialCharacters(jsonRes[i].blog_post);
                         myBlogCommentsCont = "<div onclick=\"fetchMyzoneDetailsData(this)\" blogName=\""+jsonRes[i].blog+"\" blogArticleId=\""+jsonRes[i].page_id+"\" blogCommentId=\""+jsonRes[i].blog_post_id+"\" shared_type=\""+jsonRes[i].shared_type+"\" mainId=\""+jsonRes[i].blog_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].blog+"\" class=\"searchDetails\">"+jsonRes[i].blog+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].blog_post+"\" class=\"searchDetails\">"+jsonRes[i].blog_post+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(myBlogCommentsCont);
                     }
                 }
             }
             
             var jsonSharedBlogComments = JSON.parse(sharedBlogComments);
             if(jsonSharedBlogComments){
                 var jsonRes = eval(jsonSharedBlogComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].blog = replaceSpecialCharacters(jsonRes[i].blog);
                         jsonRes[i].blog_post = replaceSpecialCharacters(jsonRes[i].blog_post);
                         sharedBlogCommentsCont = "<div onclick=\"fetchMyzoneDetailsData(this)\" blogName=\""+jsonRes[i].blog+"\"  blogArticleId=\""+jsonRes[i].page_id+"\" blogCommentId=\""+jsonRes[i].blog_post_id+"\" shared_type=\""+jsonRes[i].shared_type+"\" mainId=\""+jsonRes[i].blog_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].blog+"\" class=\"searchDetails\">"+jsonRes[i].blog+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].blog_post+"\" class=\"searchDetails\">"+jsonRes[i].blog_post+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(sharedBlogCommentsCont);
                     }
                 }
             }
             
             var jsonMyAlbumsComments = JSON.parse(myAlbumsComments);
             if(jsonMyAlbumsComments){
                 var jsonRes = eval(jsonMyAlbumsComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].photoComment = replaceSpecialCharacters(jsonRes[i].photoComment);
                         jsonRes[i].album_name = replaceSpecialCharacters(jsonRes[i].album_name);
                         myAlbumsCommentsCont = "<div onclick=\"fetchMyzoneDetailsData(this)\" mainId=\""+jsonRes[i].album_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].album_name+"\" class=\"searchDetails\">"+jsonRes[i].album_name+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].photoComment+"\" class=\"searchDetails\">"+jsonRes[i].photoComment+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(myAlbumsCommentsCont);
                     }
                 }
             }
             
             var jsonMessages = JSON.parse(messages);				
             if(jsonMessages){					
                 var jsonRes = eval(jsonMessages);
                 for(var i = 0;i < jsonRes.length;i++){
                     
                     var msgType = jsonRes[i].msgType.charAt(0).toUpperCase()+ jsonRes[i].msgType.slice(1);												
                     var moduleName = jsonRes[i].moduleName;
                     var msgid1 = jsonRes[i].msgid;						
                     var msgid2 = msgid1.split('_')[1];
                     var gName = jsonRes[i].gname;
                     //alert("gName>>>"+gName);
                     //alert("msgid2>>>>"+msgid2);
                     if(jsonRes[i].moduleName == 'My Zone'){
                         //var myZoneNotId = $(obj).attr('mainId');
                         messageCont = "<div onclick=\"fetchMyzoneDetailsData(this)\" mainId=\""+jsonRes[i].cid+"\" childId=\""+msgid2+"\"  listType=\"chat\" msgType = \""+jsonRes[i].msgType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                          /* messageCont =	"<div onclick=\"fetchMyzoneDetailsData(this)\" mainId=\""+msgid2+"\" listType=\"chat\" msgType = \""+jsonRes[i].msgType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"*/
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].msgType+"\" class=\"searchDetails\">"+msgType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].msgType+"\" class=\"searchDetails\">"+msgType+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].msg+"\" class=\"searchDetails\">"+jsonRes[i].msg+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].gname+"\" class=\"searchDetails readAccess\">"+jsonRes[i].gname+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     /*+"<div style=\"width: 15%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+localDate+" " + "</div>"*/
                                     +"</div>"
                         $('#srchCont_'+count).append(messageCont);
                     }
                 }
             }
             chatRedirect="chatRedirectHighlightClass";
             var jsonSharedAlbumsComments = JSON.parse(sharedAlbumsComments);
             if(jsonSharedAlbumsComments){
                 var jsonRes = eval(jsonSharedAlbumsComments);
                 for(var i = 0;i < jsonRes.length;i++){
                     if(jsonRes[i].moduleName == 'My Zone'){
                         jsonRes[i].photoComment = replaceSpecialCharacters(jsonRes[i].photoComment);
                         jsonRes[i].album_name = replaceSpecialCharacters(jsonRes[i].album_name);
                         sharedAlbumsCommentsCont = "<div onclick=\"fetchMyzoneDetailsData(this)\"  mainId=\""+jsonRes[i].album_id+"\" listType = \""+jsonRes[i].listType+"\" class=\"notificationgrid  reprtsContCls\" style = \"margin: 3px 0 0 0;margin-left:3%;width: 100%\">"
                                     +"<div style=\"width: 13%;margin-left:2%;\" title=\""+jsonRes[i].listType+"\" class=\"searchDetails\">"+jsonRes[i].listType+" "+ "</div>"
                                     +"<div style=\"width: 14%;\" title=\""+jsonRes[i].album_name+"\" class=\"searchDetails\">"+jsonRes[i].album_name+" " + "</div>"
                                     +"<div style=\"width: 40%;\" title=\""+jsonRes[i].photoComment+"\" class=\"searchDetails\">"+jsonRes[i].photoComment+" " + "</div>"
                                     +"<div style=\"width: 15%;\" title=\""+jsonRes[i].fullName+"\" class=\"searchDetails readAccess\">"+jsonRes[i].fullName+" " + "</div>"
                                     +"<div style=\"width: 13%;\" title=\""+jsonRes[i].createdTime+"\" class=\"searchDetails\">"+jsonRes[i].createdTime+" " + "</div>"
                                     +"</div>"
                         $('#srchCont_'+count).append(sharedAlbumsCommentsCont);
                     }
                 }
             }
         }
     }
     if(searchAlertConfirm == "" && cancelSearchOD == ""){			
         //alert("-------OK--------");			
         //confirmFun('Do you want to continue search in Cloud Storage too? <br><br>  Please note that search in Cloud Storage may take several minutes.',"delete","OptionalDrives");
         confirmReset(getValues(companyAlerts,"Alert_Drives"),"delete","OptionalDrives",'searchCancelOptionalDrives');
         $('.alertMainCss').css({'width': '30.5%'});
         $('.alertMainCss').css({'left': '45%'});
         $('#BoxConfirmBtnOk').val('Yes');
         $('#BoxConfirmBtnCancel').val('No');
         $('#BoxConfirmBtnOk').css({'width': '19%'});
             
     }
     if(searchAlertConfirm == 'OptionalDrives' && searchTypeAlertConfirm == "" && searchAllContentVar == ""){
         confirmReset(getValues(companyAlerts,"Alert_ContSearch"),'delete','searchAllContent','searchExceptFileContent');
         $('.alertMainCss').css({'width': '30.5%'});
         $('.alertMainCss').css({'left': '45%'});
         $('#BoxConfirmBtnOk').val('Yes');
         $('#BoxConfirmBtnCancel').val('No');
         $('#BoxConfirmBtnOk').css({'width': '19%'});
     }
 }
  function searchCancelOptionalDrives(){
    cancelSearchOD = "searchODCancel";
    if( cancelSearchOD != "" && searchTypeAlertConfirm == "" && searchAllContentVar == ""){
         //alert("inside type");
         confirmReset(getValues(companyAlerts,"Alert_ContSearch"),'delete','searchAllContent','searchExceptFileContent');
         $('.alertMainCss').css({'width': '30.5%'});
         $('.alertMainCss').css({'left': '45%'});
         $('#BoxConfirmBtnOk').val('Yes');
         $('#BoxConfirmBtnCancel').val('No');
         $('#BoxConfirmBtnOk').css({'width': '19%'});
     }
  }
  function glbFileContents(){
      
         //alert("inside type");
         confirmReset(getValues(companyAlerts,"Alert_ContSearch"),'delete','searchAllContent','searchExceptFileContent');
         $('.alertMainCss').css({'width': '30.5%'});
         $('.alertMainCss').css({'left': '45%'});
         $('#BoxConfirmBtnOk').val('Yes');
         $('#BoxConfirmBtnCancel').val('No');
         $('#BoxConfirmBtnOk').css({'width': '19%'});
         $('#loadingBar').hide();		
  }
  
  function calTaskRedirect(obj){
    var notificationType="Tasks";
    var task_id= $(obj).attr('task_id');
    var taskType= $(obj).attr('tasktype');
    var taskSourceId= $(obj).attr('source_id');
    if(taskType == "Document"){
       window.location = path+"/Redirect.do?pAct=myDocument&NotificationType="+notificationType+"&NotificationId="+task_id+"&notfTaskId="+task_id+"&taskSourceId="+taskSourceId;
    }else{
       window.location = path+"/Redirect.do?pAct=calMenuView&calNotificationType="+notificationType+"&notificationTypeId="+task_id+"&id="+task_id;
    }
           
  }
  
 function closeSearchNotifications(){
     $('#searchResults').hide();
     $('#transparentDiv').hide();
 }
 
 function getSrchResOnClick(count,obj){
     var moduleName = $(obj).attr('mName');
     //alert("moduleName::"+moduleName);
     //alert("count::"+count);		
     searchAlertConfirm= "OptionalDrives";
     var txt = globalSearchTxt;
     
     if($('#srchCont_'+count).is(':visible')){
         $('#srchCont_'+count).hide();
         $('#mainSrchHeader_'+count).hide();
         $('#mainSrchGridPlus_'+count).attr('src','images/notfcn-plus.png');
         
     }else{
         if(moduleName == 'Repository'){
             $('#srchCont_'+count).html('');
             loadGlobalSearchResults(count);
         }else if(moduleName == 'Connect'){
             $('#srchCont_'+count).html('');
             fetchConnectResults(txt, count);
         }else if(moduleName == 'Workspace'){
             $('#srchCont_'+count).html('');
             fetchWorkspaceResults(txt, count);
         }else if(moduleName == 'Email'){
             $('#srchCont_'+count).html('');
             fetchwrkspaceEmail(txt, count);
         }else if(moduleName == 'Ideas'){
             $('#srchCont_'+count).html('');
             fetchIdeasResults(txt, count);
         }else if(moduleName == 'Agile'){
             $('#srchCont_'+count).html('');
             fetchAgileResults(txt, count);
         }else if(moduleName == 'Task'){
             $('#srchCont_'+count).html('');
             fetchCalendarResults(txt, count);
         }else if(moduleName == 'My Zone'){
             $('#srchCont_'+count).html('');
             fetchMyZoneResults(txt, count);
         }else if(moduleName == "Confluence"){
             $('#srchCont_'+count).html('');
             fetchConfData(txt, count);
         }else if(moduleName == "Video"){
             $('#srchCont_'+count).html('');
             prepareSearchUI(txt, count, 'Video');
             //fetchConfData(txt, count);
         }else if(moduleName =="Myzone | GoogleDrive"){
             $('#srchCont_'+count).html('');
             fetchgoogleData(txt, count);
         }else if(moduleName =="Teamzone | GoogleDrive"){
             $('#srchCont_'+count).html('');
             fetchProjgoogleData(txt, count);
         }else if(moduleName =="Myzone | OneDrive"){
             $('#srchCont_'+count).html('');
             fetchOneDriveData(txt, count);
         }else if(moduleName =="Teamzone | OneDrive"){
             $('#srchCont_'+count).html('');
             fetchOneDriveProjectData(txt, count);
         }			
         $('#srchCont_'+count).show();
         $('#mainSrchHeader_'+count).show();
         $('#mainSrchGridPlus_'+count).attr('src','images/notfcn-minus.png');
     }
 }
 
 function fetchConnectResults(txt,count){
     $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchConnectResults",txt:txt},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
             checkSessionTimeOut(result);
             prepareSearchUI(result,count, 'Connect');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
 }
 
 function fetchWorkspaceResults(txt,count){
     $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchWorkspaceResults",txt:txt,searchTypeAlertConfirm:searchTypeAlertConfirm},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
             //console.log("::result:::"+result);
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Workspace');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
 }
 
 function addToFavSrch(obj){
     var classN = $(obj).attr("class");
     if(classN == "favDefImgCss"){
         $(obj).removeClass("favDefImgCss").addClass("favImgCss");
         $(obj).css({'margin':'9px -10px 0 0'});
         addToFavorites();
     }else if(classN == "favImgCss"){
         $(obj).removeClass("favImgCss").addClass("favDefImgCss");
         $(obj).css({'margin':'11px -10px 0 0'});
         removeFromFavorites();
     }
 }
 
 function detailViewNew(obj){
     $('#loadingBar').show();
       timerControl("start");
     var fId = $(obj).attr('folderId');
     var pFid = $(obj).attr('parent_folder_id');
     var fName = $(obj).attr('folderName');
     fName = fName.replaceAll(' ', "$@");
     var ftype = $(obj).attr('fType');
     var fPath = $(obj).attr('folder_path');
     var mType = $(obj).attr('mType');
     var fpath = $(obj).attr('parent_folder_id');
     var stype = $(obj).attr('share_user_type');
     var cmtId = '';
     var mType = $(obj).attr('mType');
     closeCompNotification();
      closeSearchNotifications();
      if(mType == "Foldershare"){
          window.location =  path+"/Redirect.do?pAct=myDocument&id="+fId+"&pId="+pFid+"&fname="+fName+"&ftype="+ftype+"&fpath="+fPath+"&stype="+stype+"&cmtId="+cmtId+"&mType="+mType+"";
      }else if(mType == "Fileshare"){
          window.location=  path+"/Redirect.do?pAct=myDocument&id="+fId+"&pId="+pFid+"&fname="+fName+"&ftype="+ftype+"&fpath="+pFid+"&stype="+stype+"&cmtId="+cmtId+"&mType="+mType+"";
      }
 }
   function addToFavorites(){
       var txt = globalSearchTxt;
       $.ajax({
           url: path+'/globalSearchAction.do',
           type: "POST",
           data:{act:"addToFavorites",txt:txt},
           error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
           success:function(){
           }
       });
   }
   function removeFromFavorites(){
       
   }
   function loadContacts(obj){
       closeCompNotification();
      closeSearchNotifications();
      var utype = $(obj).attr('utype');
      var uId=$(obj).attr('uId');
          $('#loadingBar').show();
           timerControl("start");
           window.location =  path+"/Redirect.do?pAct=comContact&notfId="+uId;
       
   }
   function openActivitySearchDetails(obj){
       $('#loadingBar').show();
       timerControl("start");
       var d = new Date();
       var time = d.getTime();
       var wrkSpaceNotType = 'wrkSpaceNotifications';
       var wrkSpaceNotId = '';
       var wrkSpaceMenuType = 'menuActFeed';
       var projectId = $(obj).attr('projectId');
       var projectName = $(obj).attr('projectName');
       var projUserStat = $(obj).attr('projectUserStatus');
       var projImgType = $(obj).attr('projectImgType');
       var wrkSpaceProjType='';
       var wrkspaceImgType='';
       var from ="gSearch";
       if(projUserStat == 'PO'){
            wrkSpaceProjType = 'MyProjects';
       }else{
            wrkSpaceProjType = 'SharedProjects';
       }
       var wrkSpaceSubMenuType = 'workSpaceActivityFeed';
       if(projImgType == ''){
            wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
       }else{
            wrkspaceImgType = lighttpdPath+"//projectimages//"+projectId+"."+projImgType+"?"+time;
       }
       var actFeedId = $(obj).attr('actFeedId');
       closeCompNotification();
          closeSearchNotifications();
          window.location = path+"/Redirect.do?pAct=LoadActivityFeed&projId="+projectId+"&projType="+wrkSpaceProjType+"&projName="+projectName+"&projImgSrc="+wrkspaceImgType+"&projectUsersStatus="+projUserStat+"&notFolid="+actFeedId+"&feedId="+actFeedId+"&from="+from+"";
          
      //	menuFrame.location.href  =  path+"/colAuth?pAct=wMenuProj&param1="+wrkSpaceNotType+"&param2="+wrkSpaceNotId+"&param3="+wrkSpaceMenuType+"&param4="+projectId+"&notProjName="+projectName+"&notProjType="+wrkSpaceProjType+"&wrkSpaceSubMenuType="+wrkSpaceSubMenuType+"&wrkspaceImgType="+wrkspaceImgType+"&id="+actFeedId+"";
   }
   
   function openWrkSpaceSearchDetails(obj){
   //alert("inside openWrkSpaceSearchDetails");
         $('#loadingBar').show();
       timerControl("start");
       var d = new Date();
       var time = d.getTime();
       var projectId = $(obj).attr('projectId');
       
       var projectName = $(obj).attr('projectName');
       var projectArchiveStatus = $(obj).attr('projectArchiveStat');
       var emailMsgId = $(obj).attr('emailMsgId')
       //alert("emailMsgId::>>>>>"+emailMsgId);
       var projType = '';
       if(projectArchiveStatus == 'PO'){
            projType = 'MyProjects';
       }else{
            projType = 'SharedProjects';
       }
       var projectImgType = $(obj).attr('projectImgType');
       
       var mAct = $(obj).attr('mAct'); 		
       var gdProjdocId = $(obj).attr('gdProjdoc'); 		
       
       var projectUserStatus = $(obj).attr('projectUserStatus');
       var taskSourceId=$(obj).attr('taskSourceId');
       var sublistType=$(obj).attr('sublistType');
       if(mAct == 'Task'){
           var typeOfTaskWs = $(obj).attr('typeOfTask');
       }
       if(typeOfTaskWs == 'workflowTask'){
           var workFlowId = $(obj).attr('workFlowId');
       }else{
           var workFlowId = '';
       }
       var redId = $(obj).attr('id'); 
       if(projectImgType == ''){
           var projImgSrc = lighttpdPath+"//projectimages//dummyLogo.png";
           
       }else{
           var projImgSrc = lighttpdPath+"//projectimages//"+projectId+"."+projectImgType+"?"+time;
           
       }
       
       closeCompNotification();
          closeSearchNotifications();
          if(mAct == 'Email'){
              var loadJsp = '/Redirect.do?pAct=loadEmailjsp';
       }else if(mAct == 'Task'){
           var loadJsp="";
           if(sublistType == "Conversation Task"){
              loadJsp = '/Redirect.do?pAct=LoadActivityFeed';
           }else if(sublistType == "Document Task"){
              loadJsp = '/Redirect.do?pAct=loadDocumentjsp';
           }else{
              loadJsp = '/Redirect.do?pAct=loadTaskjsp'; 
           }
           
       }else if(mAct == 'Team'){
           var loadJsp = '/Redirect.do?pAct=loadTeamjsp';
       }else if(mAct == 'Document'){
           var loadJsp = '/Redirect.do?pAct=loadDocumentjsp';
           
       //GlobalSearch	
       }else if(mAct == 'gdProjDocuments'){  			 		
            var loadJsp = '/Redirect.do?pAct=loadgoogleProj';	
           //window.location = path+loadJsp+"&projId="+projectId+"&projType="+projType+"&projName="+projectName+"&projArchStatus="+projectArchiveStatus+"&projImgSrc="+projImgSrc+"&projectUserStatus="+projectUserStatus+"&id="+gdProjdocId+"&notFolid="+gdProjdocId+"&emailMsgId="+emailMsgId;
                    
       }if(mAct == 'Task'){
           var notificationType="Tasks";
           window.location = path+loadJsp+"&projId="+projectId+"&projType="+projType+"&projName="+projectName+"&projArchStatus="+projectArchiveStatus+"&projImgSrc="+projImgSrc+"&projectUserStatus="+projectUserStatus+"&id="+redId+"&NotificationType="+notificationType+"&NotificationTaskId="+redId+"&notfTaskSourceId="+taskSourceId;
       }else if(mAct == 'gdProjDocuments'){
           var gtype='workspace'; 			
           window.location = path+loadJsp+"&projId="+projectId+"&projType="+projType+"&projName="+projectName+"&projArchStatus="+projectArchiveStatus+"&projImgSrc="+projImgSrc+"&projectUserStatus="+projectUserStatus+"&id="+gdProjdocId+"&notFolid="+gdProjdocId+"&emailMsgId="+emailMsgId+"&gtype="+gtype;
           //window.location = path+loadJsp+"&projId="+projectId+"&projType="+projType+"&projName="+projectName+"&projArchStatus="+projectArchiveStatus+"&projImgSrc="+projImgSrc+"&projectUserStatus="+projectUserStatus+"&id="+redId+"&notFolid="+redId+"&emailMsgId="+emailMsgId;
       }else{
           window.location = path+loadJsp+"&projId="+projectId+"&projType="+projType+"&projName="+projectName+"&projArchStatus="+projectArchiveStatus+"&projImgSrc="+projImgSrc+"&projectUserStatus="+projectUserStatus+"&id="+redId+"&notFolid="+redId+"&emailMsgId="+emailMsgId;
       }
 }
   
 function openWrkSearchDetails(obj){
        $('#loadingBar').show();
       timerControl("start");
       var d = new Date();
       var time = d.getTime();
       var wrkSpaceNotType = 'wrkSpaceNotifications';
       var wrkSpaceNotId = '';
       var wrkSpaceMenuType = 'menuDocument';
       var projectId = $(obj).attr('projectId');
       var projectName = $(obj).attr('projectName');
       var projUserStat = $(obj).attr('projectUserStatus');
       var projImgType = $(obj).attr('projectImgType');
       if(projUserStat == 'PO'){
           var wrkSpaceProjType = 'MyProjects';
       }else{
           var wrkSpaceProjType = 'SharedProjects';
       }
       var wrkSpaceSubMenuType = 'workspaceDocShare';
       if(projImgType == ''){
           var wrkspaceImgType = lighttpdPath+"//projectimages//dummyLogo.png";
       }else{
           var wrkspaceImgType = lighttpdPath+"//projectimages//"+projectId+"."+projImgType+"?"+time;
       }
       var id = $(obj).attr('folderId');
       var pId = $(obj).attr('parent_folder_id');
       var fname = $(obj).attr('folderName');
       var ftype = 'subshared_to';
       var fpath =  $(obj).attr('folderId');
       var stype = $(obj).attr('proj_user_status');
       var wsPresentId = $(obj).attr('folderId');
       closeCompNotification();
          closeSearchNotifications();
          menuFrame.location.href  =  path+"/colAuth?pAct=wMenuProj&param1="+wrkSpaceNotType+"&param2="+wrkSpaceNotId+"&param3="+wrkSpaceMenuType+"&param4="+projectId+"&notProjName="+projectName+"&notProjType="+wrkSpaceProjType+"&wrkSpaceSubMenuType="+wrkSpaceSubMenuType+"&wrkspaceImgType="+wrkspaceImgType+"&id="+id+"&pId="+pId+"&fname="+fname+"&ftype="+ftype+"&fpath="+fpath+"&stype="+stype+"&wsPresentId="+wsPresentId+"";
   }
 
 function openWrkTeamDetails(obj){
     $('#loadingBar').show();
       timerControl("start");
     var jsp = 'loadTeamjsp';
     var projectId = $(obj).attr('proId');
     var projName = $(obj).attr('proName');
     var archivedstatus = $(obj).attr('fId');
     
     var imgType = $(obj).attr('pFid');
     var projType = '';
     var d = new Date();
       var time = d.getTime();
     if(imgType == ''){
           var projImgSrc = lighttpdPath+"//projectimages//dummyLogo.png";
       }else{
           var projImgSrc = lighttpdPath+"//projectimages//"+projectId+"."+imgType+"?"+time;
       }
     projName = projName.replaceAll("'","CHR(39)");
     closeCompNotification();
          closeSearchNotifications();
          menuFrame.location.href  =  path+"/colAuth?pAct=wMenuProj&param1="+wrkSpaceNotType+"&param2="+wrkSpaceNotId+"&param3="+wrkSpaceMenuType+"&param4="+wrkSapceProjId+"&notProjName="+wrkspaceProjName+"&notProjType="+wrkSpaceProjType+"&wrkSpaceSubMenuType="+wrkSpaceSubMenuType+"&wrkspaceImgType="+wrkspaceImgType+"&id="+id+"&pId="+pId+"&fname="+fname+"&ftype="+ftype+"&fpath="+fpath+"&stype="+stype+"&wsPresentId="+wsPresentId+"";
   }
 
 function openProjectDetails(obj, projectId){
       $('#loadingBar').show();
       timerControl("start");
       closeCompNotification();
          closeSearchNotifications();
          var type = $(obj).attr('mType');
          if(type == 'availProjects'){
              menuFrame.location.href  =  path+"/colAuth?pAct=wMenuProj&wrkSpaceNotType=availProjects&projectIdGs="+projectId+"";
          }else{
             var mAct="wMenuProj";
              window.location.href = path+"/Redirect.do?pAct="+mAct+"&projectId="+projectId;
          }
   }
   
   function fetchIdeasResults(txt, count){
       $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchIdeasResults",txt:txt,searchTypeAlertConfirm:searchTypeAlertConfirm},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Ideas');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
   
   function fetchAgileResults(txt, count){
       $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchAgileResults",txt:txt,searchTypeAlertConfirm:searchTypeAlertConfirm},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Agile');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
   
 function fetchCalendarResults(txt, count){
       $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchCalendarResults",txt:txt,searchTypeAlertConfirm:searchTypeAlertConfirm},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Task');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
 
 function fetchwrkspaceEmail(txt, count){
       $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchwrkspaceEmail",txt:txt,searchTypeAlertConfirm:searchTypeAlertConfirm},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
         //alert("result::"+result);
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Email');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
   
 function fetchConfData(txt, count){
       $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchConfContent",txt:txt,searchTypeAlertConfirm:searchTypeAlertConfirm},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
         //alert("result::"+result);
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Confluence');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
 
 //fetchOnedrive  
 function fetchOneDriveData(txt, count){
     //alert("inside fetchOneDriveData");
       $("#loadingBar").show();	
     timerControl("start");
     
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchOneDriveContent",txt:txt},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
         //alert("fetchOneDriveData:>User>>:"+result);
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Myzone | OneDrive');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
 
 //fetchProjectOneDriveData
 function fetchOneDriveProjectData(txt, count){
     //alert("inside fetchOneDriveData");
       $("#loadingBar").show();	
     timerControl("start");
     
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchOneDriveProjectContent",txt:txt},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
         //alert("fetchOneDriveData:>Project>>:"+result);
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Teamzone | OneDrive');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
 //fetchgoogleData fetchProjgoogleData    
 function fetchgoogleData(txt, count){		
       $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchgdContent",txt:txt},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
         //alert("fetchgoogleData:>User>>:"+result);
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Myzone | GoogleDrive');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
 
 var gtype='workspace';
 function fetchProjgoogleData(txt, count){
     //alert("inside fetchgoogleData Project")
       $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchProjgdContent",txt:txt,gtype:gtype},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
         //alert("fetchProjgoogleData:>Global> Project>:"+result);
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'Teamzone | GoogleDrive');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
 
 function fetchMyZoneResults(txt, count){
       $("#loadingBar").show();	
     timerControl("start");
     $.ajax({
         url: path+'/globalSearchAction.do',
         type: "POST",
         data:{act:"fetchMyZoneResults",txt:txt},
         error: function(jqXHR, textStatus, errorThrown) {
                 checkError(jqXHR,textStatus,errorThrown);
                 $("#loadingBar").hide();
                 timerControl("");
                 }, 
         success:function(result){
             //alert("Result:::"+result);
             checkSessionTimeOut(result);
             prepareSearchUI(result, count, 'My Zone');
             $('#loadingBar').hide();
              timerControl("");
         }
     });
   }
 function removeSearchBoxCss(obj){
      var type = $(obj).attr('class');
      if(type == 'searchError'){
          $('#globalSearchDiv').removeClass('searchError').addClass('searchCss');
      }
  }
  
 function replaceSpecialCharacters(strToReplace){
     strToReplace = strToReplace.replaceAll("CH(50)", "\n");
     strToReplace = strToReplace.replaceAll("CHR(40)", "\"");
     strToReplace = strToReplace.replaceAll("CH(51)", "\\");
     strToReplace = strToReplace.replaceAll("CH(70)", "\\");
     strToReplace = strToReplace.replaceAll("CH(52)", "\'");
     strToReplace = strToReplace.replaceAll("CHR(33)", "/");
     strToReplace = strToReplace.replaceAll("CHR(39)", "\'");
     strToReplace = strToReplace.replaceAll("CHR(26)", ":");
     strToReplace = strToReplace.replaceAll("$@", ' ');
     return strToReplace;
 }
 
 function closeCompNotification(){
     $("div#userCompNotifications").hide();
     $('#transparentDiv').hide();
 }
 
 function redirectToAgile(obj){
    $('#loadingBar').show();
       timerControl("start");
     var projectType='';
     var projImgSrc = '';
     var type='';
     var notificationType='';
     var notTypeId='';
     var notCmtDocId='';
     var d = new Date();
       var time = d.getTime();
       var projId = $(obj).attr('projectId');
       var pId = $(obj).attr('pId') != null && $(obj).attr('pId') != "" ? $(obj).attr('pId') : "0";
       var ppId = $(obj).attr('ppId') != null && $(obj).attr('ppId') != "" ? $(obj).attr('ppId') : "0";
       var projName = $(obj).attr('projectName');
       var projectArchStatus = $(obj).attr('projectArchStatus');
       var projectType = $(obj).attr('projectType');
       var projectImgType = $(obj).attr('projectImgType');
       var projectUsersStatus = $(obj).attr('projectUserStatus');
       
       if(projectImgType == ''){
           projImgSrc = lighttpdPath+"//projectimages//dummyLogo.png";
       }else{
           projImgSrc = lighttpdPath+"//projectimages//"+projId+"."+projectImgType+"?"+time;
       }
       
       var ideaType = $(obj).attr('ideaType');
       var agileTaskId= $(obj).attr('agileTaskId');
       var taskSourceId= $(obj).attr('taskSourceId');
       var taskGroupId = $(obj).attr('taskGroupId');
       var taskSprintId= $(obj).attr('taskSprintId');
       var notfEpicIds = "";
       
       notfEpicIds = ppId + ',' + pId + ',' + taskSourceId;
   
       closeCompNotification();
       closeSearchNotifications();
       
     if(ideaType == "Sprint"){
      if(taskSprintId == "0" || taskSprintId== "0"){
          window.location = path+"/Redirect.do?pAct=storyMenu&projId="+projId+"&projType="+projectType+"&projName="+projName+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus+"&projArchStatus="+projectArchStatus+"&agileNotificationType="+ideaType+"&agileNotificationId="+agileTaskId+"&pId="+taskSourceId+"&epicCommDocId="+agileTaskId+"&taskSourceId="+taskSourceId+"&tasksprintId="+taskSprintId+"&taskSprintGroupId="+taskGroupId;
      }else{
          window.location = path+"/Redirect.do?pAct=sprintMenu&projId="+projId+"&projType="+projectType+"&projName="+projName+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus+"&projArchStatus="+projectArchStatus+"&agileNotificationType="+ideaType+"&agileNotificationId="+agileTaskId+"&pId="+taskSourceId+"&epicCommDocId="+agileTaskId+"&taskSourceId="+taskSourceId+"&tasksprintId="+taskSprintId+"&taskSprintGroupId="+taskGroupId;
      }
     }else if(ideaType == "agile_sprintgroup" || ideaType == "agile_sprint" || ideaType == "agile_sprintstage"){
          window.location = path+"/Redirect.do?pAct=sprintMenu&projId="+projId+"&projType="+projectType+"&projName="+projName+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus+"&projArchStatus="+projectArchStatus+"&agileNotificationType="+ideaType+"&agileNotificationId="+taskSourceId+"&pId="+pId+"&ppId="+ppId+"&epicCommDocId="+agileTaskId;
          
     }else{
         window.location = path+"/Redirect.do?pAct=storyMenu&projId="+projId+"&projType="+projectType+"&projName="+projName+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus+"&projArchStatus="+projectArchStatus+"&agileNotificationType="+ideaType+"&agileNotificationId="+taskSourceId+"&pId="+pId+"&ppId="+ppId+"&epicCommDocId="+agileTaskId+"&notfEpicIds="+notfEpicIds;
        
     }
     
 }
 
 function redirectToIdeas(obj){
     $('#loadingBar').show();
       timerControl("start");
     var projectType='';
     var projImgSrc = '';
     var type='';
     var notificationType='';
     var notTypeId='';
     var notCmtDocId='';
     var d = new Date();
       var time = d.getTime();
       var projId = $(obj).attr('projectId');
       var projName = $(obj).attr('projectName');
       var projectArchStatus = $(obj).attr('projectArchStatus');
       var projectType = $(obj).attr('projectType');
       var projectImgType = $(obj).attr('projectImgType');
       var projectUsersStatus = $(obj).attr('projectUserStatus');
       
       
     var ppId = $(obj).attr('ppId') != null && $(obj).attr('ppId') != "" ? $(obj).attr('ppId') : '0';
     //var parentIdeaIds = $(obj).attr('parentIdeaIds') != null && $(obj).attr('parentIdeaIds') != "" ? $(obj).attr('parentIdeaIds') : '0';
       
       if(projectImgType == ''){
           projImgSrc = lighttpdPath+"//projectimages//dummyLogo.png";
       }else{
           projImgSrc = lighttpdPath+"//projectimages//"+projId+"."+projectImgType+"?"+time;
       }
       
       var ideaType = $(obj).attr('ideaType');
       var ideaTaskId= $(obj).attr('ideaTaskId');
       var taskSourceId= $(obj).attr('taskSourceId');
       closeCompNotification();
       closeSearchNotifications();
       if(ideaType == 'Idea'){
          window.location = path+"/Redirect.do?pAct=iMenu&projId="+projId+"&projType="+projectType+"&projName="+projName+"&projArchStatus="+projectArchStatus+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus+"&ideaType="+type+" &ideaNotificationType="+ideaType+"&ideaNotificationId="+ideaTaskId+"&ideaCmtDocId="+ideaTaskId+"&taskSourceId="+taskSourceId+"&ppId="+ppId;
       }else{
        window.location = path+"/Redirect.do?pAct=iMenu&projId="+projId+"&projType="+projectType+"&projName="+projName+"&projArchStatus="+projectArchStatus+"&projImgSrc="+projImgSrc+"&projectUsersStatus="+projectUsersStatus+"&ideaType="+type+" &ideaNotificationType="+ideaType+"&ideaNotificationId="+taskSourceId+"&ideaCmtDocId="+ideaTaskId+"&taskSourceId="+taskSourceId+"&ppId="+ppId;
     }
 }
 
 function fetchMyzoneDetailsData(obj){		
    $('#loadingBar').show();
    timerControl("start");
    var myZoneType = $(obj).attr('listType');	  
    var myZoneNotId = $(obj).attr('mainId');	   
    var blogArticleId = $(obj).attr('blogArticleId');
    var blogCommentId = $(obj).attr('blogCommentId');
    var shared_type = $(obj).attr('shared_type');
    var msgtype = $(obj).attr('msgtype');
    var childId = $(obj).attr('childId');
    var gmsgId= $(obj).attr('gId');
    var oneDriveDocId= $(obj).attr('oneDriveId');
   // alert("gmsgId::::"+gmsgId);
    
    //alert("childId::>>"+childId);
    var blogName=$(obj).attr('blogName');
    if(myZoneType == "Notes" || myZoneType == "NoteContent"){
       window.location = path+"/Redirect.do?pAct=myNote&myZoneNotId="+myZoneNotId+"&myZoneType="+myZoneType;
    }else if(myZoneType == 'Blog' || myZoneType == 'BlogContent'){
       window.location = path+"/Redirect.do?pAct=myBlog&myZoneNotId="+myZoneNotId+"&myZoneType="+myZoneType;
    }else if(myZoneType == 'Album' ){
       window.location = path+"/Redirect.do?pAct=myGallery&myZoneNotId="+myZoneNotId+"&myZoneType="+myZoneType;
    }else if(myZoneType == 'BlogComment'){
       myZoneType="blogCommentShare";
       window.location = path+"/Redirect.do?pAct=myBlog&myZoneNotId="+myZoneNotId+"&myZoneType="+myZoneType+"&myZoneNotSharedType="+shared_type+"&myZoneNotName="+blogName+"&blogArticleId="+blogArticleId+"&blogCommentId="+blogCommentId;
    }else if(myZoneType == 'AlbumComment'){
       myZoneType="galleryCommentShare";
       window.location = path+"/Redirect.do?pAct=myGallery&myZoneNotId="+myZoneNotId+"&myZoneType="+myZoneType;
    }else if(myZoneType == 'chat'){
            window.location = path+"/Redirect.do?pAct=conMessage&param7="+myZoneType+"&notifMessId="+myZoneNotId+"&msgtype="+msgtype+"&childId="+childId;
        /*window.location = path+"/Redirect.do?pAct=conMessage&param7="+myZoneType+"&notifMessId="+myZoneNotId;*/
    }else if(myZoneType == 'myZoneGdrive'){
       // alert("inside");
        window.location = path+"/Redirect.do?pAct=userGdrive&param7="+myZoneType+"&googleMessId="+gmsgId;
    }else if(myZoneType == 'myZoneOneDrive'){		 
        window.location = path+"/Redirect.do?pAct=userOnedrive&param7="+myZoneType+"&oneDriveMessId="+oneDriveDocId;
        
    }
 }
 function imageOnProjErrorReplace(obj) {
     $(obj).attr('src', lighttpdPath + "/projectimages/dummyLogo.png");
 }
 
 function openVideo(id,ext,projectId, src){
     var source = "";
        
    if(projectId == "0"){
        source =  lighttpdPath + "/uploadedDocuments/"+id+"."+ext;
    }
    else if(projectId == "1"){
        source =  src;
    }else{
        source =  lighttpdPath + "/projectDocuments/"+id+"."+ext;
    }
        
    if( ext.toLowerCase() == "mp4" || ext.toLowerCase() == "m4v" || ext.toLowerCase() == "mov" || ext.toLowerCase() == "flv" || 
           ext.toLowerCase() == "f4v" || ext.toLowerCase() == "ogg" || ext.toLowerCase() == "ogv" || ext.toLowerCase() == "wmv" ||
        ext.toLowerCase() == "vp6" || ext.toLowerCase() == "vp5" || ext.toLowerCase() == "mpg" || ext.toLowerCase() == "avi" ||
        ext.toLowerCase() == "mpeg" || ext.toLowerCase() == "webm"){
           document.getElementById('recAudio').pause();
           $("#previewDiv").show().children('#recAudio').hide().parent().children('#recVideo').attr('src',source).show();
           document.getElementById('recVideo').play();
    }else{
           document.getElementById('recVideo').pause();
           $("#previewDiv").css({"height":""});
           $("#previewDiv").show().children('#recVideo').hide().parent().children('#recAudio').attr('src',source).show();
           $('#recAudio').css({"height":"","width":"50%"});
           document.getElementById('recAudio').play();
    }
}
