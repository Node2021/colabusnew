var hoursHand = document.getElementById('hoursHand'),
  minutesHand = document.getElementById('minutesHand'),
  secondsHand = document.getElementById('secondsHand'),
  date,
  hours,
  minutes,
  seconds;
  //document.getElementById("clck").style.border = "3px solid #595B94";
  // document.querySelector("mark").style.borderTop = "4px solid black";
  // document.getElementByClassName("marks").style.borderTop="4px solid black";
  document.getElementsByClassName("marks")[0].style.borderTop = "4px solid black";
  document.getElementsByClassName("marks")[1].style.borderTop = "4px solid black";
  document.getElementsByClassName("marks")[2].style.borderTop = "4px solid black";
  document.getElementsByClassName("marks")[3].style.borderTop = "4px solid black";
function updateHands() {
  date = new Date();
  hours = date.getHours();
  minutes = date.getMinutes();
  seconds = date.getSeconds();

  if (hours > 12) {
    hoursHand.style.webkitTransform = 'translate(-50%, 25%) rotate(' + ((hours - 12) * 30 + minutes * 0.5) + 'deg)';
    hoursHand.style.transform = 'translate(-50%, 25%) rotate(' + ((hours - 12) * 30 + minutes * 0.5) + 'deg)';
  } else if (hours <= 12) {
    hoursHand.style.webkitTransform = 'translate(-50%, 25%) rotate(' + ((hours * 30) + minutes * 0.5) + 'deg)';
    hoursHand.style.transform = 'translate(-50%, 25%) rotate(' + ((hours * 30) + minutes * 0.5) + 'deg)';
  }

  minutesHand.style.webkitTransform = 'translate(-50%, 25%) rotate(' + (minutes) * 6 + 'deg)';
  minutesHand.style.transform = 'translate(-50%, 25%) rotate(' + (minutes) * 6 + 'deg)';

  secondsHand.style.webkitTransform = 'translate(-50%, 25%) rotate(' + (seconds) * 6 + 'deg)';
  secondsHand.style.transform = 'translate(-50%, 25%) rotate(' + (seconds) * 6 + 'deg)';
}
updateHands();

window.setInterval(function() {
  updateHands();
}, 1000);