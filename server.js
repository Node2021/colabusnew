require('dotenv').config();
const express = require("express");
const bodyParser = require("body-parser");
const session = require('express-session');
const cors = require('cors');

//const app = express();
const router = express.Router();

const app = require("express")();

const corsOptions ={
    origin:'http://localhost:3000', 
    credentials:true,            //access-control-allow-credentials:true
    optionSuccessStatus:200
}
app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static("public"));
app.set("view engine", "ejs");

app.use(
    session({
        name: 'sid',
        saveUninitialized: false,
        resave: false,
        secret: `it's a secret!`,
        cookie: {
            maxAge: 1000 * 32400 ,
            sameSite: true,
            secure: true
        }
    }), 
) 

// Read the host address and the port from the environment
///const hostname = process.env.HOST;
const port = process.env.PORT;

app.get("/", function(req, res){
    let cType = req.query.cType;
    let key_id = req.query.key_id;
    let key_value = req.query.key_value;
    res.render("login.ejs",{
        lighttpdpath1: process.env.lighttpdpath,
        sessionmaxAge:req.session.cookie.maxAge,
        sessionid:req.session.id,
        cType:cType,
        key_id:key_id,
        key_value:key_value
    });
    
    
});

app.get("/login", function(req, res){
    
  
    res.render("login.ejs",{
        lighttpdpath1: process.env.lighttpdpath,
        sessionmaxAge:req.session.cookie.maxAge,
        sessionid:req.session.id,
        key_id:"",
        key_value:""
    });
   
});

app.get("/register", function(req, res){
    res.render("register.ejs",{
       lighttpdpath1: process.env.lighttpdpath,
       sessionmaxAge:req.session.cookie.maxAge,
       sessionid:req.session.id
   });
   //res.sendFile(path.join(__dirname + '/views/registration.html'));
});

app.get('/postlogin',function(req,res){
    
    let sessionval = { 
        sessionmaxAge:req.session.cookie.maxAge,
        sessionexpire:req.session.cookie.expires
    }
    let cType = req.query.cType;
    let rid = req.query.roleid;
    let act = req.query.pAct;
    ///console.log("sessionexp:"+req.session.cookie.expires)
    ////console.log(req.session.cookie.maxAge+"<--->"+req.session.cookie.expires);
    res.cookie("sessionData", sessionval); 
    res.render("postloginnew.ejs",{
        lighttpdpath1: process.env.lighttpdpath,
        sessionmaxAge:sessionval.sessionmaxAge,
        sessionexp:sessionval.sessionexpire,
        sessionid:req.session.id,
        cType:cType,
        roleid:rid,
        pAct:act
    });
});

app.get('/autoLogin',function(req,res){
    let cType = req.query.cType;
    let key_id = req.query.key_id;
    let key_value = req.query.key_value;
    ///console.log("sessionexp:"+req.session.cookie.expires)
    ////console.log(req.session.cookie.maxAge+"<--->"+req.session.cookie.expires);
    res.render("login.ejs",{
        lighttpdpath1: process.env.lighttpdpath,
        sessionmaxAge:req.session.cookie.maxAge,
        sessionid:req.session.id,
        cType:cType,
        key_id:key_id,
        key_value:key_value
    });
});

app.get("/chat", function(req, res){
    let sessionval = { 
        sessionmaxAge:req.session.cookie.maxAge,
        sessionexpire:req.session.cookie.expires
    }
    
    ///console.log("sessionexp:"+req.session.cookie.expires)
    ////console.log(req.session.cookie.maxAge+"<--->"+req.session.cookie.expires);
    res.cookie("sessionData", sessionval); 
    res.render("chat.ejs",{
       lighttpdpath1: process.env.lighttpdpath,
       sessionmaxAge:sessionval.sessionmaxAge,
       sessionexp:sessionval.sessionexpire,
       sessionid:req.session.id
   });
   //res.sendFile(path.join(__dirname + '/views/registration.html'));
});

const server=app.listen(port, () => {
    console.log(`Server running at http://${port}/`);
});
var io = require('socket.io')(server);

io.on('connection', socket => {
   
    socket.on('newfeed', messege => {
         socket.broadcast.emit('newfeed',messege);
    })
       socket.on('delete', messege => {
         socket.broadcast.emit('delete',messege);
    })
})

//---- testing commit in vs code